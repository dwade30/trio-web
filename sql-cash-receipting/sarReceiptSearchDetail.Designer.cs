﻿namespace TWCR0000
{
	/// <summary>
	/// Summary description for sarReceiptSearchDetail.
	partial class sarReceiptSearchDetail
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(sarReceiptSearchDetail));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReceipt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTeller = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRef = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAcct1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAcct2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAcct3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAcct4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAcct5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAcct6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblControlTitle1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblControlTitle2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblControlTitle3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldControl1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldControl2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldControl3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldComment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTransTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldConvenienceFeeAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldConvenienceFeeAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.sarRSReceiptDetailOB = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReceipt = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTeller = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblRef = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lnHeader = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReceipt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTeller)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRef)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcct1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcct2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcct3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcct4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcct5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcct6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblControlTitle1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblControlTitle2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblControlTitle3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControl1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControl2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControl3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldComment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldConvenienceFeeAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldConvenienceFeeAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReceipt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTeller)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRef)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldDate,
            this.fldReceipt,
            this.fldType,
            this.fldTeller,
            this.fldName,
            this.fldRef,
            this.fldAmount,
            this.fldAcct1,
            this.fldAcct2,
            this.fldAcct3,
            this.fldAcct4,
            this.fldAcct5,
            this.fldAcct6,
            this.fldAmt1,
            this.fldAmt2,
            this.fldAmt3,
            this.fldAmt4,
            this.fldAmt5,
            this.fldAmt6,
            this.lblControlTitle1,
            this.lblControlTitle2,
            this.lblControlTitle3,
            this.fldControl1,
            this.fldControl2,
            this.fldControl3,
            this.fldComment,
            this.fldTransTime,
            this.fldConvenienceFeeAccount,
            this.fldConvenienceFeeAmount,
            this.sarRSReceiptDetailOB});
			this.Detail.Height = 1.739666F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldDate
			// 
			this.fldDate.CanGrow = false;
			this.fldDate.Height = 0.1875F;
			this.fldDate.Left = 0.0625F;
			this.fldDate.Name = "fldDate";
			this.fldDate.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldDate.Text = "Date";
			this.fldDate.Top = 0F;
			this.fldDate.Width = 0.875F;
			// 
			// fldReceipt
			// 
			this.fldReceipt.CanGrow = false;
			this.fldReceipt.Height = 0.1875F;
			this.fldReceipt.Left = 1.5625F;
			this.fldReceipt.Name = "fldReceipt";
			this.fldReceipt.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldReceipt.Text = "Receipt";
			this.fldReceipt.Top = 0F;
			this.fldReceipt.Width = 0.5625F;
			// 
			// fldType
			// 
			this.fldType.CanGrow = false;
			this.fldType.Height = 0.1875F;
			this.fldType.Left = 2.125F;
			this.fldType.Name = "fldType";
			this.fldType.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
			this.fldType.Text = "Type";
			this.fldType.Top = 0F;
			this.fldType.Width = 0.4375F;
			// 
			// fldTeller
			// 
			this.fldTeller.CanGrow = false;
			this.fldTeller.Height = 0.1875F;
			this.fldTeller.Left = 2.5625F;
			this.fldTeller.Name = "fldTeller";
			this.fldTeller.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
			this.fldTeller.Text = "Teller";
			this.fldTeller.Top = 0F;
			this.fldTeller.Width = 0.5F;
			// 
			// fldName
			// 
			this.fldName.CanGrow = false;
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 3.0625F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldName.Text = "Name";
			this.fldName.Top = 0F;
			this.fldName.Width = 1.5625F;
			// 
			// fldRef
			// 
			this.fldRef.CanGrow = false;
			this.fldRef.Height = 0.1875F;
			this.fldRef.Left = 5.3125F;
			this.fldRef.Name = "fldRef";
			this.fldRef.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldRef.Text = "Ref";
			this.fldRef.Top = 0F;
			this.fldRef.Width = 0.8125F;
			// 
			// fldAmount
			// 
			this.fldAmount.CanGrow = false;
			this.fldAmount.Height = 0.1875F;
			this.fldAmount.Left = 6.125F;
			this.fldAmount.Name = "fldAmount";
			this.fldAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldAmount.Text = "Amount";
			this.fldAmount.Top = 0F;
			this.fldAmount.Width = 0.8125F;
			// 
			// fldAcct1
			// 
			this.fldAcct1.Height = 0.1875F;
			this.fldAcct1.Left = 4.625F;
			this.fldAcct1.Name = "fldAcct1";
			this.fldAcct1.Style = "font-family: \'Tahoma\'";
			this.fldAcct1.Text = "Account";
			this.fldAcct1.Top = 0.1979167F;
			this.fldAcct1.Width = 1.145833F;
			// 
			// fldAcct2
			// 
			this.fldAcct2.Height = 0.1875F;
			this.fldAcct2.Left = 4.625F;
			this.fldAcct2.Name = "fldAcct2";
			this.fldAcct2.Style = "font-family: \'Tahoma\'";
			this.fldAcct2.Text = "Account";
			this.fldAcct2.Top = 0.375F;
			this.fldAcct2.Width = 1.145833F;
			// 
			// fldAcct3
			// 
			this.fldAcct3.Height = 0.1875F;
			this.fldAcct3.Left = 4.625F;
			this.fldAcct3.Name = "fldAcct3";
			this.fldAcct3.Style = "font-family: \'Tahoma\'";
			this.fldAcct3.Text = "Account";
			this.fldAcct3.Top = 0.5625F;
			this.fldAcct3.Width = 1.145833F;
			// 
			// fldAcct4
			// 
			this.fldAcct4.Height = 0.1875F;
			this.fldAcct4.Left = 4.625F;
			this.fldAcct4.Name = "fldAcct4";
			this.fldAcct4.Style = "font-family: \'Tahoma\'";
			this.fldAcct4.Text = "Account";
			this.fldAcct4.Top = 0.75F;
			this.fldAcct4.Width = 1.145833F;
			// 
			// fldAcct5
			// 
			this.fldAcct5.Height = 0.1875F;
			this.fldAcct5.Left = 4.625F;
			this.fldAcct5.Name = "fldAcct5";
			this.fldAcct5.Style = "font-family: \'Tahoma\'";
			this.fldAcct5.Text = "Account";
			this.fldAcct5.Top = 0.9375F;
			this.fldAcct5.Width = 1.145833F;
			// 
			// fldAcct6
			// 
			this.fldAcct6.Height = 0.1875F;
			this.fldAcct6.Left = 4.625F;
			this.fldAcct6.Name = "fldAcct6";
			this.fldAcct6.Style = "font-family: \'Tahoma\'";
			this.fldAcct6.Text = "Account";
			this.fldAcct6.Top = 1.125F;
			this.fldAcct6.Width = 1.145833F;
			// 
			// fldAmt1
			// 
			this.fldAmt1.Height = 0.1875F;
			this.fldAmt1.Left = 6.125F;
			this.fldAmt1.Name = "fldAmt1";
			this.fldAmt1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldAmt1.Text = "0.00";
			this.fldAmt1.Top = 0.1875F;
			this.fldAmt1.Width = 0.8125F;
			// 
			// fldAmt2
			// 
			this.fldAmt2.Height = 0.1875F;
			this.fldAmt2.Left = 6.125F;
			this.fldAmt2.Name = "fldAmt2";
			this.fldAmt2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldAmt2.Text = "0.00";
			this.fldAmt2.Top = 0.375F;
			this.fldAmt2.Width = 0.8125F;
			// 
			// fldAmt3
			// 
			this.fldAmt3.Height = 0.1875F;
			this.fldAmt3.Left = 6.125F;
			this.fldAmt3.Name = "fldAmt3";
			this.fldAmt3.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldAmt3.Text = "0.00";
			this.fldAmt3.Top = 0.5625F;
			this.fldAmt3.Width = 0.8125F;
			// 
			// fldAmt4
			// 
			this.fldAmt4.Height = 0.1875F;
			this.fldAmt4.Left = 6.125F;
			this.fldAmt4.Name = "fldAmt4";
			this.fldAmt4.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldAmt4.Text = "0.00";
			this.fldAmt4.Top = 0.75F;
			this.fldAmt4.Width = 0.8125F;
			// 
			// fldAmt5
			// 
			this.fldAmt5.Height = 0.1875F;
			this.fldAmt5.Left = 6.125F;
			this.fldAmt5.Name = "fldAmt5";
			this.fldAmt5.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldAmt5.Text = "0.00";
			this.fldAmt5.Top = 0.9375F;
			this.fldAmt5.Width = 0.8125F;
			// 
			// fldAmt6
			// 
			this.fldAmt6.Height = 0.1875F;
			this.fldAmt6.Left = 6.125F;
			this.fldAmt6.Name = "fldAmt6";
			this.fldAmt6.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldAmt6.Text = "0.00";
			this.fldAmt6.Top = 1.125F;
			this.fldAmt6.Width = 0.8125F;
			// 
			// lblControlTitle1
			// 
			this.lblControlTitle1.Height = 0.1875F;
			this.lblControlTitle1.HyperLink = null;
			this.lblControlTitle1.Left = 1F;
			this.lblControlTitle1.Name = "lblControlTitle1";
			this.lblControlTitle1.Style = "font-family: \'Tahoma\'";
			this.lblControlTitle1.Text = "Control 1:";
			this.lblControlTitle1.Top = 0.25F;
			this.lblControlTitle1.Visible = false;
			this.lblControlTitle1.Width = 0.9375F;
			// 
			// lblControlTitle2
			// 
			this.lblControlTitle2.Height = 0.1875F;
			this.lblControlTitle2.HyperLink = null;
			this.lblControlTitle2.Left = 1F;
			this.lblControlTitle2.Name = "lblControlTitle2";
			this.lblControlTitle2.Style = "font-family: \'Tahoma\'";
			this.lblControlTitle2.Text = "Control 2:";
			this.lblControlTitle2.Top = 0.4375F;
			this.lblControlTitle2.Visible = false;
			this.lblControlTitle2.Width = 0.9375F;
			// 
			// lblControlTitle3
			// 
			this.lblControlTitle3.Height = 0.1875F;
			this.lblControlTitle3.HyperLink = null;
			this.lblControlTitle3.Left = 1F;
			this.lblControlTitle3.Name = "lblControlTitle3";
			this.lblControlTitle3.Style = "font-family: \'Tahoma\'";
			this.lblControlTitle3.Text = "Control 3:";
			this.lblControlTitle3.Top = 0.625F;
			this.lblControlTitle3.Visible = false;
			this.lblControlTitle3.Width = 0.9375F;
			// 
			// fldControl1
			// 
			this.fldControl1.CanGrow = false;
			this.fldControl1.Height = 0.1875F;
			this.fldControl1.Left = 1.9375F;
			this.fldControl1.Name = "fldControl1";
			this.fldControl1.Style = "font-family: \'Tahoma\'";
			this.fldControl1.Text = null;
			this.fldControl1.Top = 0.25F;
			this.fldControl1.Visible = false;
			this.fldControl1.Width = 2.125F;
			// 
			// fldControl2
			// 
			this.fldControl2.CanGrow = false;
			this.fldControl2.Height = 0.1875F;
			this.fldControl2.Left = 1.9375F;
			this.fldControl2.Name = "fldControl2";
			this.fldControl2.Style = "font-family: \'Tahoma\'";
			this.fldControl2.Text = null;
			this.fldControl2.Top = 0.4375F;
			this.fldControl2.Visible = false;
			this.fldControl2.Width = 2.125F;
			// 
			// fldControl3
			// 
			this.fldControl3.CanGrow = false;
			this.fldControl3.Height = 0.1875F;
			this.fldControl3.Left = 1.9375F;
			this.fldControl3.Name = "fldControl3";
			this.fldControl3.Style = "font-family: \'Tahoma\'";
			this.fldControl3.Text = null;
			this.fldControl3.Top = 0.625F;
			this.fldControl3.Visible = false;
			this.fldControl3.Width = 2.125F;
			// 
			// fldComment
			// 
			this.fldComment.Height = 0.1975F;
			this.fldComment.Left = 0.25F;
			this.fldComment.Name = "fldComment";
			this.fldComment.Style = "font-family: \'Tahoma\'";
			this.fldComment.Text = null;
			this.fldComment.Top = 0.8125F;
			this.fldComment.Visible = false;
			this.fldComment.Width = 3.8125F;
			// 
			// fldTransTime
			// 
			this.fldTransTime.Height = 0.1875F;
			this.fldTransTime.Left = 0.9375F;
			this.fldTransTime.Name = "fldTransTime";
			this.fldTransTime.Style = "font-family: Tahoma; font-size: 9.75pt";
			this.fldTransTime.Text = "Time";
			this.fldTransTime.Top = 0F;
			this.fldTransTime.Width = 0.6875F;
			// 
			// fldConvenienceFeeAccount
			// 
			this.fldConvenienceFeeAccount.Height = 0.1875F;
			this.fldConvenienceFeeAccount.Left = 4.619F;
			this.fldConvenienceFeeAccount.Name = "fldConvenienceFeeAccount";
			this.fldConvenienceFeeAccount.Style = "font-family: \'Tahoma\'";
			this.fldConvenienceFeeAccount.Text = "Account";
			this.fldConvenienceFeeAccount.Top = 1.316F;
			this.fldConvenienceFeeAccount.Width = 1.145833F;
			// 
			// fldConvenienceFeeAmount
			// 
			this.fldConvenienceFeeAmount.Height = 0.1875F;
			this.fldConvenienceFeeAmount.Left = 6.119034F;
			this.fldConvenienceFeeAmount.Name = "fldConvenienceFeeAmount";
			this.fldConvenienceFeeAmount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldConvenienceFeeAmount.Text = "0.00";
			this.fldConvenienceFeeAmount.Top = 1.316F;
			this.fldConvenienceFeeAmount.Width = 0.8125F;
			// 
			// sarRSReceiptDetailOB
			// 
			this.sarRSReceiptDetailOB.CloseBorder = false;
			this.sarRSReceiptDetailOB.Height = 0.125F;
			this.sarRSReceiptDetailOB.Left = 0F;
			this.sarRSReceiptDetailOB.Name = "sarRSReceiptDetailOB";
			this.sarRSReceiptDetailOB.Report = null;
			this.sarRSReceiptDetailOB.Top = 1.562F;
			this.sarRSReceiptDetailOB.Width = 6.9375F;
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblDate,
            this.lblReceipt,
            this.lblTeller,
            this.lblType,
            this.lblName,
            this.lblRef,
            this.lblAccount,
            this.lblAmount,
            this.lnHeader,
            this.lblTime});
			this.GroupHeader1.Height = 0.1875F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 0F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.lblDate.Text = "Date";
			this.lblDate.Top = 0F;
			this.lblDate.Width = 0.9375F;
			// 
			// lblReceipt
			// 
			this.lblReceipt.Height = 0.1875F;
			this.lblReceipt.HyperLink = null;
			this.lblReceipt.Left = 1.5625F;
			this.lblReceipt.Name = "lblReceipt";
			this.lblReceipt.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.lblReceipt.Text = "Receipt";
			this.lblReceipt.Top = 0F;
			this.lblReceipt.Width = 0.5625F;
			// 
			// lblTeller
			// 
			this.lblTeller.Height = 0.1875F;
			this.lblTeller.HyperLink = null;
			this.lblTeller.Left = 2.5625F;
			this.lblTeller.Name = "lblTeller";
			this.lblTeller.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.lblTeller.Text = "Teller";
			this.lblTeller.Top = 0F;
			this.lblTeller.Width = 0.5F;
			// 
			// lblType
			// 
			this.lblType.Height = 0.1875F;
			this.lblType.HyperLink = null;
			this.lblType.Left = 2.125F;
			this.lblType.Name = "lblType";
			this.lblType.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.lblType.Text = "Type";
			this.lblType.Top = 0F;
			this.lblType.Width = 0.4375F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.1875F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 3.0625F;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.lblName.Text = "Name";
			this.lblName.Top = 0F;
			this.lblName.Width = 1.5625F;
			// 
			// lblRef
			// 
			this.lblRef.Height = 0.1875F;
			this.lblRef.HyperLink = null;
			this.lblRef.Left = 5.3125F;
			this.lblRef.Name = "lblRef";
			this.lblRef.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.lblRef.Text = "Ref/Acct";
			this.lblRef.Top = 0F;
			this.lblRef.Width = 0.8125F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 4.625F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.lblAccount.Text = "Account";
			this.lblAccount.Top = 0F;
			this.lblAccount.Width = 0.6875F;
			// 
			// lblAmount
			// 
			this.lblAmount.Height = 0.1875F;
			this.lblAmount.HyperLink = null;
			this.lblAmount.Left = 6.125F;
			this.lblAmount.Name = "lblAmount";
			this.lblAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblAmount.Text = "Amount";
			this.lblAmount.Top = 0F;
			this.lblAmount.Width = 0.8125F;
			// 
			// lnHeader
			// 
			this.lnHeader.Height = 0F;
			this.lnHeader.Left = 0F;
			this.lnHeader.LineWeight = 1F;
			this.lnHeader.Name = "lnHeader";
			this.lnHeader.Top = 0.2F;
			this.lnHeader.Width = 7F;
			this.lnHeader.X1 = 0F;
			this.lnHeader.X2 = 7F;
			this.lnHeader.Y1 = 0.2F;
			this.lnHeader.Y2 = 0.2F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1875F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0.9375F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblTime.Text = "Time";
			this.lblTime.Top = 0F;
			this.lblTime.Width = 0.625F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// sarReceiptSearchDetail
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.010417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReceipt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTeller)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRef)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcct1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcct2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcct3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcct4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcct5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcct6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblControlTitle1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblControlTitle2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblControlTitle3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControl1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControl2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControl3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldComment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldConvenienceFeeAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldConvenienceFeeAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReceipt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTeller)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRef)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReceipt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTeller;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRef;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAcct1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAcct2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAcct3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAcct4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAcct5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAcct6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt6;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarRSReceiptDetailOB;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblControlTitle1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblControlTitle2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblControlTitle3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldControl1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldControl2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldControl3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldComment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTransTime;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReceipt;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTeller;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRef;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldConvenienceFeeAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldConvenienceFeeAmount;
	}
}
