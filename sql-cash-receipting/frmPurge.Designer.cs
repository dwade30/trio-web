﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmPurge.
	/// </summary>
	partial class frmPurge : BaseForm
	{
		public Global.T2KDateBox txtDate;
		public fecherFoundation.FCButton cmdPurge;
		public fecherFoundation.FCLabel lblDate;
		public fecherFoundation.FCLabel lblPurgeInstructions;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.txtDate = new Global.T2KDateBox();
            this.cmdPurge = new fecherFoundation.FCButton();
            this.lblDate = new fecherFoundation.FCLabel();
            this.lblPurgeInstructions = new fecherFoundation.FCLabel();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileContinue = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileQuit = new fecherFoundation.FCToolStripMenuItem();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPurge)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 301);
            this.BottomPanel.Size = new System.Drawing.Size(320, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtDate);
            this.ClientArea.Controls.Add(this.cmdPurge);
            this.ClientArea.Controls.Add(this.lblDate);
            this.ClientArea.Controls.Add(this.lblPurgeInstructions);
            this.ClientArea.Size = new System.Drawing.Size(320, 241);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(320, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(147, 30);
            this.HeaderText.Text = "End Of Year";
            // 
            // txtDate
            // 
            this.txtDate.Location = new System.Drawing.Point(176, 30);
            this.txtDate.Mask = "##/##/####";
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new System.Drawing.Size(120, 22);
            this.txtDate.TabIndex = 1;
            // 
            // cmdPurge
            // 
            this.cmdPurge.AppearanceKey = "acceptButton";
            this.cmdPurge.Location = new System.Drawing.Point(30, 142);
            this.cmdPurge.Name = "cmdPurge";
            this.cmdPurge.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPurge.Size = new System.Drawing.Size(90, 48);
            this.cmdPurge.TabIndex = 3;
            this.cmdPurge.Text = "Purge";
            this.cmdPurge.Click += new System.EventHandler(this.cmdPurge_Click);
            // 
            // lblDate
            // 
            this.lblDate.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.lblDate.Location = new System.Drawing.Point(30, 44);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(75, 16);
            this.lblDate.TabIndex = 0;
            this.lblDate.Text = "ENTER DATE";
            this.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPurgeInstructions
            // 
            this.lblPurgeInstructions.Location = new System.Drawing.Point(30, 90);
            this.lblPurgeInstructions.Name = "lblPurgeInstructions";
            this.lblPurgeInstructions.Size = new System.Drawing.Size(259, 32);
            this.lblPurgeInstructions.TabIndex = 2;
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFile});
            this.MainMenu1.Name = null;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = 0;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileContinue,
            this.Seperator,
            this.mnuFileQuit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuFileContinue
            // 
            this.mnuFileContinue.Index = 0;
            this.mnuFileContinue.Name = "mnuFileContinue";
            this.mnuFileContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuFileContinue.Text = "Save & Continue";
            this.mnuFileContinue.Click += new System.EventHandler(this.mnuFileContinue_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuFileQuit
            // 
            this.mnuFileQuit.Index = 2;
            this.mnuFileQuit.Name = "mnuFileQuit";
            this.mnuFileQuit.Text = "Exit";
            this.mnuFileQuit.Click += new System.EventHandler(this.mnuFileQuit_Click);
            // 
            // frmPurge
            // 
            this.ClientSize = new System.Drawing.Size(320, 409);
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmPurge";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "End of Year";
            this.Load += new System.EventHandler(this.frmPurge_Load);
            this.Activated += new System.EventHandler(this.frmPurge_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPurge_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPurge_KeyPress);
            this.Resize += new System.EventHandler(this.frmPurge_Resize);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPurge)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}
