﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;
using Wisej.Web;
using System.IO;
using fecherFoundation.VisualBasicLayer;
using System.Collections.Generic;
using System.Data.Common;
using TWSharedLibrary;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmDailyReceiptAudit.
	/// </summary>
	public partial class frmDailyReceiptAudit : BaseForm
	{
		public frmDailyReceiptAudit()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.cmdPassword = new System.Collections.Generic.List<fecherFoundation.FCButton>();
			this.cmdPassword.AddControlArrayElement(cmdPassword_1, FCConvert.ToInt16(1));
			this.cmdPassword.AddControlArrayElement(cmdPassword_0, FCConvert.ToInt16(0));
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.cmbCLAlign.SelectedIndex = 0;
			this.cmbUTAlign.SelectedIndex = 0;
			this.cmbARAlign.SelectedIndex = 0;
            this.Shown += FrmDailyReceiptAudit_Shown;
            this.Closed += FrmDailyReceiptAudit_Closed;
		}

        private void FrmDailyReceiptAudit_Closed(object sender, EventArgs e)
        {
            //closeoutCallBack = null;
        }

        private void FrmDailyReceiptAudit_Shown(object sender, EventArgs e)
        {
            CheckPermissions();
            clsDRWrapper rsCheck = new clsDRWrapper();
            clsDRWrapper rsMV = new clsDRWrapper();
            if (!boolLoaded)
            {
                PreviewTellerCloseOut_2(false);
                rsCheck.OpenRecordset("SELECT * FROM (" + modGlobal.Statics.strCurrentDailyAuditSQL + ") as CurrentDailyAudit");
                if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
                {
                    if (modGlobalConstants.Statics.gboolBD)
                    {
                        FillJournalCombo();
                        FillMonthCombo();
                    }
                    if (modGlobalConstants.Statics.gboolMV)
                    {
                        rsMV.OpenRecordset("SELECT ID FROM Archive WHERE ReceiptType = 99 AND ISNULL(DailyCloseOut,0) = 0", modExtraModules.strCRDatabase);
                        if (rsMV.RecordCount() == 0)
                        {
                            boolMVTransactions = false;
                        }
                        else
                        {
                            boolMVTransactions = true;
                        }
                    }
                    StartUp();
                    boolLoaded = true;
                }
                else
                {
                    boolMVTransactions = false;
                    MessageBox.Show("There have been no transactions since the last Audit.", "No Eligible Accounts", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //MDIParent.InstancePtr.Focus();
                    Close();
                }
            }
            //FC:FINAL:DSE #i578 Set correct initial value
            this.cmbAudit.SelectedIndex = 0;
        }

        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmDailyReceiptAudit InstancePtr
		{
			get
			{
				return (frmDailyReceiptAudit)Sys.GetInstance(typeof(frmDailyReceiptAudit));
			}
		}

		protected frmDailyReceiptAudit _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		string strTellerID = "";
		bool boolToMainMenu;
		bool boolLoaded;
		bool boolClicking;
		// to show when a click is real or when a value is being changed my the program
		bool boolMVTransactions;
		bool boolSavePressed;
		clsDRWrapper rsReceipts = new clsDRWrapper();
		int lngEnd;
		bool blnCLLandscape;
		bool blnUTLandscape;
		bool blnARLandscape;
		bool boolDeptCloseOut;
		string strDeptDiv;
		bool boolARDB;
		bool boolUTDB;
		bool boolCLDB;
		bool boolARAudit;
		bool boolUTAudit;
		bool boolCLAudit;
		public bool boolTellerOnly;
		public string strCloseOutSQL = "";
		public bool boolCloseOut;
		public int lngJournalNumber;
		public bool boolNonEntry;
		// the Daily Audit reports use this to show the footnote label at the bottom or the audit and the teller audit
		public int lngCloseoutKey;
		public bool boolPerformingPartialCO;
		public string strMissingReceipts = string.Empty;
		public bool boolMissingReceipts;
		public bool boolUsePrevYears;
        private bool canPerformDailyAudit = false;
        private Action closeoutCallBack;
		public void Init(Action closeoutAction)
        {
            this.closeoutCallBack = closeoutAction;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsDBChk = new clsDRWrapper();
				boolCLDB = rsDBChk.DBExists("Collections");
				boolUTDB = rsDBChk.DBExists("UtilityBilling");
				boolARDB = rsDBChk.DBExists("AccountsReceivable");
				if (modGlobalConstants.Statics.gboolMV)
				{
					chkCloseOutMV.Enabled = true;
					if (modGlobal.Statics.gboolCloseMVPeriod)
					{
						chkCloseOutMV.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkCloseOutMV.CheckState = Wisej.Web.CheckState.Unchecked;
					}
				}
				else
				{
					chkCloseOutMV.Enabled = false;
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				//MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing Form", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MsgBoxStyle.Critical | MsgBoxStyle.OkOnly, "Error Initializing Form");
            }
		}

		private void cmbJournal_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbJournal.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 80, 0);
		}

		private void cmbJournal_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						if (!cmbJournal.DroppedDown)
						{
							cmbJournal.DroppedDown = true;
						}
						break;
					}
			}
			//end switch
		}

		private void cmbJournal_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (cmbJournal.SelectedIndex == -1)
			{
				if (cmbJournal.Items.Count > 1)
				{
					// Cancel = True
					MessageBox.Show("Please enter a Journal Number.", "Input", MessageBoxButtons.OK, MessageBoxIcon.Question);
				}
				else
				{
					MessageBox.Show("You have no Journals to choose, you must Add a New Journal.", "Input", MessageBoxButtons.OK, MessageBoxIcon.Information);
					e.Cancel = false;
					cmbAudit.Focus();
				}
			}
		}

		private void cmbMonth_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbMonth.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 100, 0);
		}

		private void cmbMonth_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						if (!cmbMonth.DroppedDown)
						{
							cmbMonth.DroppedDown = true;
						}
						break;
					}
			}
			//end switch
		}

		private void cmbMonth_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (cmbMonth.SelectedIndex == -1)
			{
				e.Cancel = true;
				MessageBox.Show("Please enter an Accounting Month.", "Input", MessageBoxButtons.OK, MessageBoxIcon.Question);
			}
		}

		private void cmdDone_Click(object sender, System.EventArgs e)
		{
			try
			{
				if (cmbAudit.SelectedIndex < 0)
				{
					MessageBox.Show("Please choose an Audit option.", "No Option Selected", MessageBoxButtons.OK,
						MessageBoxIcon.Information);
					cmbAudit.Focus();
					return;
				}

				var chosenAuditOption = (AuditOptions) cmbAudit.ItemData(cmbAudit.SelectedIndex);
				List<string> batchReports = new List<string>();
				// On Error GoTo ERROR_HANDLER
				string strAuditPrinter = "";
				int lngJournal;
				clsDRWrapper rsCR = new clsDRWrapper();
				clsDRWrapper rsAutoCreate = new clsDRWrapper();
				bool boolAutoPost = false;
				// kk01282015 trocrs-32
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				// This will stop users from minimizing the preview and run a regular audit
				modGlobal.Statics.gboolDailyAuditLandscape = FCConvert.CBool(cmbAlignment.SelectedIndex == 1);
				cmdDone.Enabled = false;
				cmdPassword[0].Enabled = false;
				fraPreAudit.Enabled = false;
				fraDept.Enabled = false;
				// kk trout-348
				// this will force any reports closed when running the audit
				frmReportViewer.InstancePtr.Unload();
				// MAL@20080423: Add check for override to always print portrait orientation (for CL and UT reports)
				blnCLLandscape = FCConvert.CBool(cmbCLAlign.SelectedIndex == 1);
				blnUTLandscape = FCConvert.CBool(cmbUTAlign.SelectedIndex == 1);
				blnARLandscape = FCConvert.CBool(cmbARAlign.SelectedIndex == 1);
				// MAL@20080514: Save orientation options for future reference
				modRegistry.SaveRegistryKey("CLAuditLandscape", FCConvert.ToString(blnCLLandscape), "CR");
				modRegistry.SaveRegistryKey("UTAuditLandscape", FCConvert.ToString(blnUTLandscape), "CR");
				modRegistry.SaveRegistryKey("ARAuditLandscape", FCConvert.ToString(blnARLandscape), "CR");
				modRegistry.SaveRegistryKey("MainAuditLandscape",
					FCConvert.ToString(modGlobal.Statics.gboolDailyAuditLandscape), "CR");
				lngCloseoutKey = 0;
				// kk 111612 trocr-348  Get the dept to close out
				strDeptDiv = txtDept.Text;
				if (chosenAuditOption == AuditOptions.AuditPreview)
				{
					boolCloseOut = false;
					if (modGlobal.Statics.gboolPendingTransactions)
					{
						switch (MessageBox.Show("There is a pending audit, would you like to view these?",
							"View Pending Transactions", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
						{
							case DialogResult.Yes:
							{
								SetupSQL_2(true);
								break;
							}
							case DialogResult.No:
							{
								SetupSQL_2(false);
								break;
							}
							default:
							{
								return;
							}
						}

						//end switch
					}
					else
					{
						SetupSQL();
					}

					// Fake a teller close out
					PreviewTellerCloseOut_2(true);
					// audit preview
					arDailyAuditReport.InstancePtr.intReportHeader = 0;
					frmReportViewer.InstancePtr.Init(arDailyAuditReport.InstancePtr, "", 0, false, false, "Pages",
						modGlobal.Statics.gboolDailyAuditLandscape);
				}
				//FC:FINAL:DSE Use selected option text (because of removing / adding / changing options in cmbAudit)
				//else if (cmbAudit.SelectedIndex == 3)
				else if (chosenAuditOption == AuditOptions.FinalizePartialCloseout ||
				         chosenAuditOption == AuditOptions.PrintAuditReportAndCloseout)
				{
					// Check to see which audits should be printed
					CheckPrintingAuditOptions();
					if (modGlobalConstants.Statics.gboolBD)
					{
						if (cmbJournal.SelectedIndex != -1)
						{
							if (cmbJournal.SelectedIndex == 0)
							{
								// this will get a new journal number from BD and use it
								lngJournalNumber = 0;
							}
							else
							{
								lngJournalNumber =
									FCConvert.ToInt32(Math.Round(
										Conversion.Val(cmbJournal.Items[cmbJournal.SelectedIndex].ToString())));
							}

							if (modGlobal.Statics.gboolPasswordProtectAudit)
							{
								ShowFrame(fraAuditPassword);
								fraPreAudit.Visible = false;
								fraDept.Visible = false;
								// kk trout-348
								Global.Extensions.CombineAndPrintPDFs(batchReports);
								return;
							}
							else
							{
								boolCloseOut = true;
								// check to make sure the BD database is not corrupt
								if (CheckBDDatabase() < 0)
								{
									// kk01282015 trocrs-32  Add option to automatically post system generated journals
									boolAutoPost =
										modSecurity.ValidPermissions_8(null, modGlobal.CRSECURITYAUTOPOSTJOURNAL,
											false) && modBudgetaryAccounting.AutoPostAllowed(modBudgetaryAccounting
											.AutoPostType.aptCRDailyAudit);
									if (boolAutoPost)
									{
										modGlobal.Statics.tPostInfo = new clsBudgetaryPosting();
										// Instantiate the collection.  The journal is added in sarAccountingSummary
									}

									// close out records before running the report then look for the closeout ID
									lngCloseoutKey = CloseOut(modGlobal.Statics.gboolPendingTransactions);
									SetupSQL();
									// regular audit
									arDailyAuditReport.InstancePtr.intReportHeader = 0;
									modDuplexPrinting.DuplexPrintReport(arDailyAuditReport.InstancePtr, "", true,
										modGlobal.Statics.gboolDailyAuditLandscape, batchReports: batchReports);
									strAuditPrinter = arDailyAuditReport.InstancePtr.Document.Printer.PrinterName;
									//arDailyAuditReport.InstancePtr.Hide();
									if (modGlobalConstants.Statics.gboolCL && boolCLDB)
									{
										// This will closeout all of the Collections records
										// run the audit reports
										modGlobal.Statics.glngCurrentCloseOut = modGlobal.CLCloseOut_2(false,
											modGlobal.Statics.gboolPendingTransactions, strDeptDiv);
										if (boolCLAudit)
										{
											rptCLDailyAuditMaster.InstancePtr.StartUp(true, true, blnCLLandscape, true);
											modDuplexPrinting.DuplexPrintReport(rptCLDailyAuditMaster.InstancePtr,
												strAuditPrinter, blnCLLandscape, batchReports: batchReports);
										}
									}

									if (modGlobalConstants.Statics.gboolUT && boolUTDB)
									{
										// This will closeout all of the Utility records
										// run the audit reports
										modGlobal.Statics.glngCurrentCloseOut = modGlobal.UTCloseOut_2(false,
											modGlobal.Statics.gboolPendingTransactions, strDeptDiv);
										if (boolUTAudit)
										{
											rptCLDailyAuditMaster.InstancePtr.StartUp(true, true, blnCLLandscape, true);
											modDuplexPrinting.DuplexPrintReport(rptUTDailyAuditMaster.InstancePtr,
												strAuditPrinter, blnUTLandscape, batchReports: batchReports);
										}
									}

									if (modGlobalConstants.Statics.gboolAR && boolARDB)
									{
										// This will closeout all of the Collections records
										// run the audit reports
										modGlobal.Statics.glngARCurrentCloseOut = modGlobal.ARCloseOut_2(false,
											modGlobal.Statics.gboolPendingTransactions, strDeptDiv);
										rsAutoCreate.OpenRecordset("SELECT * FROM Customize",
											modExtraModules.strARDatabase);
										if (rsAutoCreate.EndOfFile() != true && rsAutoCreate.BeginningOfFile() != true)
										{
											if (FCConvert.ToBoolean(
												rsAutoCreate.Get_Fields_Boolean("AutoCreateJournal")))
											{
												lngJournal = modARCalculations.CreateDemandJournal();
											}
										}

										if (boolARAudit)
										{
											// Accounts Receivable Audit Master
											rptARDailyAuditMaster.InstancePtr.StartUp(true, blnARLandscape);
											modDuplexPrinting.DuplexPrintReport(rptARDailyAuditMaster.InstancePtr,
												strAuditPrinter, blnARLandscape, batchReports: batchReports);
										}

										//rptARDailyAuditMaster.InstancePtr.Hide();
									}

									if (!boolDeptCloseOut)
									{
										// kk trocr-348 111612  Can't check for missing receipts when by dept
										CheckForMissingReceipts_2(true, strAuditPrinter, batchReports: batchReports);
									}

									// kk01282015 trocrs-32  Add option to automatically post system generated journals
									if (boolAutoPost)
									{
										if (modGlobal.Statics.tPostInfo.JournalCount > 0)
										{
											if (MessageBox.Show(
												"The receipt entries have been saved into journal " +
												Strings.Format(
													modGlobal.Statics.tPostInfo.Get_JournalsToPost(0).JournalNumber,
													"0000") + ".  Would you like to post the journal?",
												"Post Entries?", MessageBoxButtons.YesNo,
												MessageBoxIcon.Question) == DialogResult.Yes)
											{
												modGlobal.Statics.tPostInfo.AllowPreview = false;
												modGlobal.Statics.tPostInfo.PostJournals();
											}
										}

										modGlobal.Statics.tPostInfo = null;
									}
								}

								boolPerformingPartialCO = false;
								Close();
							}
						}
						else
						{
							MessageBox.Show("Please enter a Journal number before proceeding.", "Journal Number Error",
								MessageBoxButtons.OK, MessageBoxIcon.Information);
							cmbJournal.Focus();
						}
					}
					else
					{
						if (modGlobal.Statics.gboolPasswordProtectAudit)
						{
							ShowFrame(fraAuditPassword);
							fraPreAudit.Visible = false;
							fraDept.Visible = false;
							// kk trout-348
							Global.Extensions.CombineAndPrintPDFs(batchReports);
							return;
						}
						else
						{
							boolCloseOut = true;
							if (FCConvert.ToBoolean(CheckBDDatabase()))
							{
								// close out records before running the report then look for the closeout ID
								lngCloseoutKey = CloseOut(modGlobal.Statics.gboolPendingTransactions);
								SetupSQL();
								// regular audit
								arDailyAuditReport.InstancePtr.intReportHeader = 0;
								modDuplexPrinting.DuplexPrintReport(arDailyAuditReport.InstancePtr, string.Empty, true,
									modGlobal.Statics.gboolDailyAuditLandscape, batchReports: batchReports);
								strAuditPrinter = arDailyAuditReport.InstancePtr.Document.Printer.PrinterName;
								//arDailyAuditReport.InstancePtr.Hide();
								if (modGlobalConstants.Statics.gboolCL && boolCLDB)
								{
									// This will closeout all of the Collections records
									// run the audit reports
									modGlobal.Statics.glngCurrentCloseOut = modGlobal.CLCloseOut_2(false,
										modGlobal.Statics.gboolPendingTransactions, strDeptDiv);
									if (boolCLAudit)
									{
										rptCLDailyAuditMaster.InstancePtr.StartUp(true, true, blnCLLandscape, true);
										modDuplexPrinting.DuplexPrintReport(rptCLDailyAuditMaster.InstancePtr,
											strAuditPrinter, blnCLLandscape, batchReports: batchReports);
									}
								}

								if (modGlobalConstants.Statics.gboolUT && boolUTDB)
								{
									// This will closeout all of the Collections records
									// run the audit reports
									modGlobal.Statics.glngCurrentCloseOut = modGlobal.UTCloseOut_2(false,
										modGlobal.Statics.gboolPendingTransactions, strDeptDiv);
									if (boolUTAudit)
									{
										rptCLDailyAuditMaster.InstancePtr.StartUp(true, true, blnCLLandscape, true);
										modDuplexPrinting.DuplexPrintReport(rptUTDailyAuditMaster.InstancePtr,
											strAuditPrinter, blnUTLandscape, batchReports: batchReports);
									}
								}

								if (modGlobalConstants.Statics.gboolAR && boolARDB)
								{
									// This will closeout all of the Collections records
									// run the audit reports
									modGlobal.Statics.glngARCurrentCloseOut = modGlobal.ARCloseOut_2(false,
										modGlobal.Statics.gboolPendingTransactions, strDeptDiv);
									rsAutoCreate.OpenRecordset("SELECT * FROM Customize",
										modExtraModules.strARDatabase);
									if (rsAutoCreate.EndOfFile() != true && rsAutoCreate.BeginningOfFile() != true)
									{
										if (FCConvert.ToBoolean(rsAutoCreate.Get_Fields_Boolean("AutoCreateJournal")))
										{
											lngJournal = modARCalculations.CreateDemandJournal();
										}
									}

									if (boolARAudit)
									{
										// Accounts Receivable Audit Master
										rptARDailyAuditMaster.InstancePtr.StartUp(true, blnARLandscape);
										modDuplexPrinting.DuplexPrintReport(rptARDailyAuditMaster.InstancePtr,
											strAuditPrinter, blnARLandscape, batchReports: batchReports);
									}

									//rptARDailyAuditMaster.InstancePtr.Hide();
								}

								if (!boolDeptCloseOut)
								{
									// kk trocr-348 111612  Can't check for missing receipts when by dept
									CheckForMissingReceipts_2(true, strAuditPrinter, batchReports: batchReports);
								}
							}

							Close();
						}
					}
				}
				//FC:FINAL:DSE Use selected option text (because of removing / adding / changing options in cmbAudit)
				//else if (cmbAudit.SelectedIndex == 2)
				else if (chosenAuditOption == AuditOptions.CloseoutIndividualTeller)
				{
					// CHECK TO SEE IF THE TELLER HAS CLOSED OUT ALREADY TODAY
					if (TellerClosedOutToday_2(txtTellerID.Text))
					{
						MessageBox.Show(
							"This teller has already been closed out during this audit period." + "\r\n" +
							"This teller cannot be closed out again.", "Teller Close Out Error", MessageBoxButtons.OK,
							MessageBoxIcon.Hand);
					}
					else
					{
						boolTellerOnly = true;
						boolCloseOut = true;
						// Teller Report
						lngCloseoutKey = CloseOutTeller_8(txtTellerID.Text, true);
						SetupSQL();
						arDailyTellerAuditReport.InstancePtr.UserData = txtTellerID.Text;
						frmReportViewer.InstancePtr.Init(arDailyTellerAuditReport.InstancePtr);
					}
				}
				//FC:FINAL:DSE Use selected option text (because of removing / adding / changing options in cmbAudit)
				//else if (cmbAudit.SelectedIndex == 1)
				else if (chosenAuditOption == AuditOptions.TellerPreview)
				{
					SetupSQL();
					// Teller Preview
					boolTellerOnly = true;
					boolCloseOut = false;
					arDailyTellerAuditReport.InstancePtr.UserData = txtTellerID.Text;
					frmReportViewer.InstancePtr.Init(arDailyTellerAuditReport.InstancePtr);
				}
				//FC:FINAL:DSE Use selected option text (because of removing / adding / changing options in cmbAudit)
				//else if (cmbAudit.SelectedIndex == 4)
				else if (chosenAuditOption == AuditOptions.PerformPartialCloseout)
				{
					modGlobalFunctions.AddCYAEntry_8("CR", "Performing A Partial Audit");
					boolPerformingPartialCO = true;
					boolCloseOut = true;
					// close out records before running the report then look for the closeout ID
					lngCloseoutKey = CloseOut();
					SetupSQL();
					// regular audit
					arDailyAuditReport.InstancePtr.intReportHeader = 0;
					frmReportViewer.InstancePtr.Init(arDailyAuditReport.InstancePtr, "", 0, false, false, "Pages",
						modGlobal.Statics.gboolDailyAuditLandscape, showModal: true);
					// Unload arDailyAuditReport
					if (modGlobalConstants.Statics.gboolCL && boolCLDB)
					{
						// This will closeout all of the Collections records
						// run the audit reports
						modGlobal.Statics.glngCurrentCloseOut =
							modGlobal.CLCloseOut_3(boolPerformingPartialCO, strDeptDiv);
						// this should return the partial code of -100
						if (boolCLAudit)
						{
							rptCLDailyAuditMaster.InstancePtr.StartUp(true, true, blnCLLandscape, true);
						}
					}

					if (modGlobalConstants.Statics.gboolUT && boolUTDB)
					{
						// This will closeout all of the Collections records
						// run the audit reports
						modGlobal.Statics.glngCurrentCloseOut = modGlobal.UTCloseOut(boolPerformingPartialCO,
							modGlobal.Statics.gboolPendingTransactions, strDeptDiv);
						if (boolUTAudit)
						{
							// Utility Daily Audit Master
							// MAL@20080423: Check to print Portrait
							// Tracker Reference: 13318
							// rptUTDailyAuditMaster.StartUp True, gboolDailyAuditLandscape, 2, , boolUTAudit, strAuditPrinter
							rptUTDailyAuditMaster.InstancePtr.StartUp(true, blnUTLandscape, 2, true, boolUTAudit,
								strAuditPrinter);
							modDuplexPrinting.DuplexPrintReport(rptUTDailyAuditMaster.InstancePtr, strAuditPrinter,
								blnUTLandscape, batchReports: batchReports);
						}

						//rptUTDailyAuditMaster.InstancePtr.Hide();
					}

					if (modGlobalConstants.Statics.gboolAR && boolARDB)
					{
						// This will closeout all of the AR records
						// run the audit reports
						modGlobal.Statics.glngARCurrentCloseOut = modGlobal.ARCloseOut(boolPerformingPartialCO,
							modGlobal.Statics.gboolPendingTransactions, strDeptDiv);
						rsAutoCreate.OpenRecordset("SELECT * FROM Customize", modExtraModules.strARDatabase);
						if (rsAutoCreate.EndOfFile() != true && rsAutoCreate.BeginningOfFile() != true)
						{
							if (FCConvert.ToBoolean(rsAutoCreate.Get_Fields_Boolean("AutoCreateJournal")))
							{
								lngJournal = modARCalculations.CreateDemandJournal();
							}
						}

						if (boolARAudit)
						{
							// Accounts Receivable Audit Master
							rptARDailyAuditMaster.InstancePtr.StartUp(true, blnARLandscape);
						}

						//rptARDailyAuditMaster.InstancePtr.Hide();
					}

					boolPerformingPartialCO = false;
					// CheckForMissingReceipts False
				}

				boolTellerOnly = FCConvert.CBool(chosenAuditOption == AuditOptions.TellerPreview ||
				                                 chosenAuditOption == AuditOptions.CloseoutIndividualTeller);
				if (modGlobal.Statics.gboolAllowPartialAudit)
				{
					modGlobal.Statics.gboolPendingTransactions = modGlobal.CheckForPendingTransactions();
					// this will reset the variable if needed
					SetupAuditChoices();
					closeoutCallBack?.Invoke();
				}

				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				// MAL@20080616: Change to not run the Cross Check report if this is an Audit Preview
				// Tracker Reference: 14266
				if (chosenAuditOption != AuditOptions.AuditPreview && chosenAuditOption != AuditOptions.TellerPreview)
				{
					CrossCheckTest();
				}

				Global.Extensions.CombineAndPrintPDFs(batchReports);
				//FC:FINAL:AM:#i520 - don't close the form; its members are used in the report
				//FC:FINAL:BBE:#335 - Instead of closing the form, hide it
				Hide();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				cmdDone.Enabled = true;
				cmdPassword[0].Enabled = true;
				fraPreAudit.Enabled = true;
				if (boolDeptCloseOut)
				{
					fraDept.Enabled = true;
					// kk trout-348
				}

				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				boolPerformingPartialCO = false;
				// MAL@20080325: Handle Error 0 with Better Description: Tracker Reference: 12786
				frmWait.InstancePtr.Unload();
				if (Information.Err(ex).Number == 0)
				{
					//MessageBox.Show("Unable to print report to the selected printer." + "\r\n" + "Please select another printer and try again.", "Daily Audit Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					FCMessageBox.Show(
						"Unable to print report to the selected printer." + "\r\n" +
						"Please select another printer and try again.", MsgBoxStyle.OkOnly | MsgBoxStyle.Exclamation,
						"Daily Audit Report");
					/*? Resume; */
				}
				else
				{
					//MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Daily Audit Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					FCMessageBox.Show(
						"Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " +
						Information.Err(ex).Description + ".", MsgBoxStyle.Critical | MsgBoxStyle.OkOnly,
						"Daily Audit Report");
				}
			}
			finally
			{
				cmdDone.Enabled = true;
				cmdPassword[0].Enabled = true;
				fraPreAudit.Enabled = true;
				if (boolDeptCloseOut)
				{
					fraDept.Enabled = true;
					// kk trout-348
				}

				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				boolPerformingPartialCO = false;
			}
		}

		public void cmdDone_Click()
		{
			cmdDone_Click(cmdDone, new System.EventArgs());
		}

		private void cmdDone_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Return:
					{
						KeyCode = (Keys)0;
						cmdDone_Click();
						break;
					}
			}
			//end switch
		}

		private void cmdPassword_Click_2(short Index)
		{
			cmdPassword_Click(Index);
		}

		private void cmdPassword_Click(short Index, object sender, System.EventArgs e)
		{
			string strPW = "";
			string strAuditPrinter = "";
			bool boolAutoPost = false;
			// kk01282015 trocrs-32
			if (Index == 0)
			{
				modGlobal.Statics.gboolDailyAuditLandscape = FCConvert.CBool(cmbAlignment.SelectedIndex == 1);
				// MAL@20080423: Add check for override to always print portrait orientation (for CL and UT reports)
				blnCLLandscape = FCConvert.CBool(cmbCLAlign.SelectedIndex == 1);
				blnUTLandscape = FCConvert.CBool(cmbUTAlign.SelectedIndex == 1);
				blnARLandscape = FCConvert.CBool(cmbARAlign.SelectedIndex == 1);
				// MAL@20080514: Save orientation options for future reference
				modRegistry.SaveRegistryKey("CLAuditLandscape", FCConvert.ToString(blnCLLandscape), "CR");
				modRegistry.SaveRegistryKey("UTAuditLandscape", FCConvert.ToString(blnUTLandscape), "CR");
				modRegistry.SaveRegistryKey("ARAuditLandscape", FCConvert.ToString(blnARLandscape), "CR");
				modRegistry.SaveRegistryKey("MainAuditLandscape", FCConvert.ToString(modGlobal.Statics.gboolDailyAuditLandscape), "CR");
				strPW = Strings.UCase(txtAuditPassword.Text);
				//arDailyAuditReport.InstancePtr.Hide();
				// vbPorter upgrade warning: intTemp As short, int --> As DialogResult
				DialogResult intTemp;
				if (modGlobal.Statics.gstrAuditPassword == strPW)
				{
                    List<string> batchReports = new List<string>();
					fraAuditPassword.Visible = false;
					// this will force any reports closed when running the audit
					frmReportViewer.InstancePtr.Unload();
					ShowFrame(fraPreAudit);
					if (boolDeptCloseOut)
					{
						fraDept.Visible = true;
						// kk trout-348
					}
					boolCloseOut = true;
					CheckPrintingAuditOptions();
					txtAuditPassword.Text = "";
					if (FCConvert.ToBoolean(CheckBDDatabase()))
					{
						// kk01282015 trocrs-32  Add option to automatically post system generated journals
						boolAutoPost = modSecurity.ValidPermissions_8(null, modGlobal.CRSECURITYAUTOPOSTJOURNAL, false) && modBudgetaryAccounting.AutoPostAllowed(modBudgetaryAccounting.AutoPostType.aptCRDailyAudit);
                        if (modGlobalConstants.Statics.gboolBD)
                        {
                            if (boolAutoPost)
                            {
                                modGlobal.Statics.tPostInfo = new clsBudgetaryPosting();
                                // Instantiate the collection.  The journal is added in sarAccountingSummary
                            }
                        }
                        else
                        {
                            boolAutoPost = false;
                        }

                        // close out records before running the report then look for the closeout ID
						lngCloseoutKey = CloseOut(modGlobal.Statics.gboolPendingTransactions);
						SetupSQL();
						// regular audit
						arDailyAuditReport.InstancePtr.intReportHeader = 0;
						modDuplexPrinting.DuplexPrintReport(arDailyAuditReport.InstancePtr, string.Empty, true, modGlobal.Statics.gboolDailyAuditLandscape, batchReports: batchReports);
						strAuditPrinter = arDailyAuditReport.InstancePtr.Document.Printer.PrinterName;
						//arDailyAuditReport.InstancePtr.Hide();
						if (modGlobalConstants.Statics.gboolCL && boolCLDB)
						{
							// This will closeout all of the Collections records
							// run the audit reports
							modGlobal.Statics.glngCurrentCloseOut = modGlobal.CLCloseOut_2(false, modGlobal.Statics.gboolPendingTransactions, strDeptDiv);
							if (boolCLAudit)
							{
								// RECL and PPCL
								rptCLDailyAuditMaster.InstancePtr.StartUp(true, true, blnCLLandscape, true);
								modDuplexPrinting.DuplexPrintReport(rptCLDailyAuditMaster.InstancePtr, strAuditPrinter, blnCLLandscape, batchReports: batchReports);
							}
							//rptCLDailyAuditMaster.InstancePtr.Hide();
						}
						if (modGlobalConstants.Statics.gboolUT && boolUTDB)
						{
							// This will closeout all of the Collections records
							// run the audit reports
							modGlobal.Statics.glngCurrentCloseOut = modGlobal.UTCloseOut_2(false, modGlobal.Statics.gboolPendingTransactions, strDeptDiv);
							// If boolUTAudit Then
							// Utility Daily Audit Master
							rptUTDailyAuditMaster.InstancePtr.StartUp(true, blnUTLandscape, 2, true, boolUTAudit, strAuditPrinter);
							// DuplexPrintReport rptUTDailyAuditMaster, strAuditPrinter
							modDuplexPrinting.DuplexPrintReport(rptUTDailyAuditMaster.InstancePtr, strAuditPrinter, blnUTLandscape, batchReports: batchReports);
						}
						if (modGlobalConstants.Statics.gboolAR && boolARDB)
						{
							// This will closeout all of the Collections records
							// run the audit reports
							modGlobal.Statics.glngARCurrentCloseOut = modGlobal.ARCloseOut_2(false, modGlobal.Statics.gboolPendingTransactions, strDeptDiv);
							if (boolARAudit)
							{
								// Accounts Receivable Audit Master
								rptARDailyAuditMaster.InstancePtr.StartUp(true, blnARLandscape);
								modDuplexPrinting.DuplexPrintReport(rptARDailyAuditMaster.InstancePtr, strAuditPrinter, blnARLandscape, batchReports: batchReports);
							}
							//rptARDailyAuditMaster.InstancePtr.Hide();
						}
						// kk01282015 trocrs-32  Add option to automatically post system generated journals
						if (boolAutoPost)
						{
							if (modGlobal.Statics.tPostInfo.JournalCount > 0)
							{
								if (MessageBox.Show("The receipt entries have been saved into journal " + Strings.Format(modGlobal.Statics.tPostInfo.Get_JournalsToPost(0).JournalNumber, "0000") + ".  Would you like to post the journal?", "Post Entries?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
								{
									modGlobal.Statics.tPostInfo.AllowPreview = false;
									modGlobal.Statics.tPostInfo.PostJournals();
								}
							}
							modGlobal.Statics.tPostInfo = null;
						}
					}
					if (!boolDeptCloseOut)
					{
						// kk trocr-348 111612  Can't check for missing receipts when by dept
						CheckForMissingReceipts_2(true, strAuditPrinter, batchReports: batchReports);
					}
					boolTellerOnly = FCConvert.CBool(cmbAudit.SelectedIndex == 1 || cmbAudit.SelectedIndex == 2);
					if (modGlobal.Statics.gboolAllowPartialAudit)
					{
						modGlobal.Statics.gboolPendingTransactions = modGlobal.CheckForPendingTransactions();						
                        SetupAuditChoices();
                        closeoutCallBack?.Invoke();
                    }
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
					// MAL@20080616: Change to not run the Cross Check report if this is an Audit Preview
					// Tracker Reference: 14266
					if (cmbAudit.SelectedIndex == 0 || cmbAudit.SelectedIndex == 1)
					{
						// Do Nothing
					}
					else
					{
						CrossCheckTest();
					}
                    Global.Extensions.CombineAndPrintPDFs(batchReports);
					Close();
				}
				else
				{
					intTemp = MessageBox.Show("This password is incorrect.  Would you like to try again?", "Incorrect Password", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);
					switch (intTemp)
					{
						case DialogResult.Yes:
							{
								txtAuditPassword.SelectionStart = 0;
								txtAuditPassword.SelectionLength = txtAuditPassword.Text.Length;
								txtAuditPassword.Focus();
								break;
							}
						default:
							{
								fraAuditPassword.Visible = false;
								ShowFrame(fraPreAudit);
								if (boolDeptCloseOut)
								{
									fraDept.Visible = true;
									// kk trout-348
								}
								break;
							}
					}
					//end switch
				}
			}
			else
			{
				Close();
			}
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
		}

		public void cmdPassword_Click(short Index)
		{
			cmdPassword_Click(Index, cmdPassword[Index], new System.EventArgs());
		}

		private void cmdPassword_Click(object sender, System.EventArgs e)
		{
			short index = cmdPassword.GetIndex((FCButton)sender);
			cmdPassword_Click(index, sender, e);
		}

		private void frmDailyReceiptAudit_Activated(object sender, System.EventArgs e)
		{
			
		}

		private void FillMonthCombo()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsM = new clsDRWrapper();
				// adds the months to the combo box
				cmbMonth.Clear();
				cmbMonth.AddItem("01 - January");
				cmbMonth.AddItem("02 - February");
				cmbMonth.AddItem("03 - March");
				cmbMonth.AddItem("04 - April");
				cmbMonth.AddItem("05 - May");
				cmbMonth.AddItem("06 - June");
				cmbMonth.AddItem("07 - July");
				cmbMonth.AddItem("08 - August");
				cmbMonth.AddItem("09 - September");
				cmbMonth.AddItem("10 - October");
				cmbMonth.AddItem("11 - November");
				cmbMonth.AddItem("12 - December");
				rsM.OpenRecordset("SELECT ActualSystemDate FROM Archive WHERE ISNULL(DailyCloseOut,0) = 0 ORDER BY ActualSystemDate desc", modExtraModules.strCRDatabase);
				if (!rsM.EndOfFile())
				{
					cmbMonth.SelectedIndex = ((DateTime)rsM.Get_Fields_DateTime("ActualSystemDate")).Month - 1;
				}
				else
				{
					// default month
					cmbMonth.SelectedIndex = DateTime.Now.Month - 1;
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				//MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Period Combo", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MsgBoxStyle.Critical | MsgBoxStyle.OkOnly, "Error Filling Period Combo");
            }
		}

		private void FillJournalCombo()
		{
			// adds all of the active journals to the combo box
			clsDRWrapper rsBD = new clsDRWrapper();
			rsBD.DefaultDB = "TWBD0000.vb1";
			cmbJournal.Clear();
			cmbJournal.Items.Insert(0, "New Journal");
			rsBD.OpenRecordset("SELECT JournalNumber, Period, Description FROM JournalMaster WHERE Type = 'CW' AND Status = 'E' ORDER BY JournalNumber");
			if (rsBD.BeginningOfFile() != true && rsBD.EndOfFile() != true)
			{
				do
				{
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
					cmbJournal.AddItem(modGlobal.PadToString_8(rsBD.Get_Fields("JournalNumber"), 4) + " - " + modGlobalFunctions.PadStringWithSpaces(FCConvert.ToString(rsBD.Get_Fields("Period")), 2, false) + " - " + rsBD.Get_Fields_String("Description"));
					rsBD.MoveNext();
				}
				while (!rsBD.EndOfFile());
			}
			if (cmbJournal.Items.Count > 0)
				cmbJournal.SelectedIndex = 0;
		}

		private void frmDailyReceiptAudit_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Return:
					{
						KeyCode = (Keys)0;
						Support.SendKeys("{tab}", false);
						break;
					}
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						boolToMainMenu = true;
						Close();
						break;
					}
				case Keys.F10:
					{
						KeyCode = (Keys)0;
						break;
					}
			}
			//end switch
		}

		private void frmDailyReceiptAudit_Load(object sender, System.EventArgs e)
		{
			clsDRWrapper rsPD = new clsDRWrapper();
			clsDRWrapper rsDef = new clsDRWrapper();
			if (modGlobalConstants.Statics.gboolUT)
			{
				rsDef.OpenRecordset("SELECT * FROM UtilityBilling", modExtraModules.strUTDatabase);
				if (!rsDef.EndOfFile())
				{
					modUTCalculations.Statics.gboolShowMapLotInUTAudit = FCConvert.CBool(rsDef.Get_Fields_Boolean("ShowMapLotInUTAudit"));
				}
			}
			rsDef = null;
			rsDef = new clsDRWrapper();
			rsDef.OpenRecordset("SELECT * FROM CashRec", modExtraModules.strCRDatabase);
			if (rsDef.RecordCount() > 0)
			{
				// kk trocr-348 111212 Option for closeout by Dept
				boolDeptCloseOut = FCConvert.CBool(rsDef.Get_Fields_Int32("CloseOutByDept"));
			}
			// this will size the form to the MDIParent
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			if (modGlobalConstants.Statics.gboolCL && boolCLDB)
			{
				// if there is no CR then check the pending discounts
				rsPD.OpenRecordset("SELECT * FROM PendingDiscount", modExtraModules.strCLDatabase);
				if (!rsPD.EndOfFile())
				{
					// pending discounts
					switch (MessageBox.Show("There are pending discounts from the BL module.  Would you like to finalize the billing amounts?", "Pending Discounts", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
					{
						case DialogResult.Yes:
							{
								modGlobalFunctions.AddCYAEntry_8("CL", "Finalized pending discounts.");
								modGlobalFunctions.AddCYAEntry_8("CR", "Finalized pending discounts.");
								modCLDiscount.FinalizePendingDiscounts();
								break;
							}
						default:
							{
								modGlobalFunctions.AddCYAEntry_8("CL", "Disregarded pending discounts.");
								modGlobalFunctions.AddCYAEntry_8("CR", "Disregarded pending discounts.");
								break;
							}
					}
					//end switch
				}
				else
				{
					// none, so keep rockin
				}
			}
			//FC:FINAL:DSE #i578 Code moved to Activated event
			//this.cmbAudit.SelectedIndex = 0;
		}

        private void ReCheckAuditChoices()
        {

        }

        private void SetupAuditChoices()
        {
            int itemData = -1;
            bool partialCloseOutAdded = false;
            bool optionFourEnabled = false;
            partialCloseOutAdded = true;
            if (cmbAudit.SelectedIndex >= 0)
            {
                itemData = cmbAudit.ItemData(cmbAudit.SelectedIndex);
            }
            cmbAudit.Clear();
            cmbAudit.Items.Add("Audit Preview");
            cmbAudit.ItemData(0, 0);
            int currentIndex = 0;
            if (modGlobal.Statics.gstrTeller != "N")
            {
                cmbAudit.Items.Add("Teller Preview");
                currentIndex++;
                cmbAudit.ItemData(currentIndex, 2);
                if (modGlobal.Statics.gboolCashTellersOutSeperately)
                {
                    cmbAudit.Items.Add("Close Out Individual Teller");
                    currentIndex++;
                    cmbAudit.ItemData(currentIndex, 3);
                }
            }

            if (canPerformDailyAudit && !modGlobal.Statics.gboolPendingTransactions)
            {
                cmbAudit.Items.Add("Print Audit Report and Close Out");
                currentIndex++;
                cmbAudit.ItemData(currentIndex, 1);
            }

            if (modGlobal.Statics.gboolPendingTransactions)
            {
                if (canPerformDailyAudit)
                {
                    cmbAudit.Items.Add( "Finalize Partial Close Out");
                    currentIndex++;
                    cmbAudit.ItemData(currentIndex, 1);
                }
            }
            else
            {
                if (modGlobal.Statics.gboolAllowPartialAudit)
                {
                    optionFourEnabled = true;
                }
            }

            if (optionFourEnabled)
            {
                if (partialCloseOutAdded)
                {
                    cmbAudit.Items.Add("Perform Partial Close Out");
                }
                else
                {
                    cmbAudit.Items.Add("Save Partial Audit");
                }
                currentIndex++;
                cmbAudit.ItemData(currentIndex, 4);
            }

            for(int x = 0; x < cmbAudit.ListCount; x++)
            {
                if (cmbAudit.ItemData(x) == itemData)
                {
                    cmbAudit.SelectedIndex = x;
                    return;
                }
            }

            cmbAudit.SelectedIndex = 0;
        }
		private void StartUp()
		{
			string strID = "";
			modGlobal.Statics.gboolDailyAuditLandscape = FCConvert.CBool(modRegistry.GetRegistryKey("MainAuditLandscape", "CR"));
			if (modGlobal.Statics.gboolDailyAuditLandscape)
			{
				// Landscape
				cmbAlignment.SelectedIndex = 1;
			}
			else
			{
				// Portrait
				cmbAlignment.SelectedIndex = 0;
			}
            SetupAuditChoices();			
			if (modGlobal.Statics.gstrTeller == "N")
			{
				lblTellerID.Enabled = false;
				txtTellerID.Enabled = false;
				cmbTellerType.Enabled = false;
				lblTellerType.Enabled = false;
			}
			else
			{
				lblTellerID.Enabled = true;
				txtTellerID.Enabled = true;
				cmbTellerType.Enabled = true;
				lblTellerType.Enabled = true;
			}
			// MAL@20080505: Check Printing Options
			// Tracker Reference: 13318
			CheckPrintingAuditOptions();
			modGlobal.Statics.gblnPrintCLAuditLandscape = FCConvert.CBool(modRegistry.GetRegistryKey("CLAuditLandscape", "CR"));
			modGlobal.Statics.gblnPrintUTAuditLandscape = FCConvert.CBool(modRegistry.GetRegistryKey("UTAuditLandscape", "CR"));
			if (boolCLAudit)
			{
				lblCLAudit.Enabled = true;
				cmbCLAlign.Enabled = true;
				if (modGlobal.Statics.gblnPrintCLAuditLandscape)
				{
					cmbCLAlign.SelectedIndex = 1;
				}
				else
				{
					cmbCLAlign.SelectedIndex = 0;
				}
			}
			else
			{
				lblCLAudit.Enabled = false;
				cmbCLAlign.Enabled = false;
			}
			if (boolUTAudit)
			{
				lblUTAudit.Enabled = true;
				cmbUTAlign.Enabled = true;
				if (modGlobal.Statics.gblnPrintUTAuditLandscape)
				{
					cmbUTAlign.SelectedIndex = 1;
				}
				else
				{
					cmbUTAlign.SelectedIndex = 0;
				}
			}
			else
			{
				lblUTAudit.Enabled = false;
				cmbUTAlign.Enabled = false;
			}
			if (boolARAudit)
			{
				lblARAudit.Enabled = true;
				cmbARAlign.Enabled = true;
				if (modGlobal.Statics.gblnPrintARAuditLandscape)
				{
					cmbARAlign.SelectedIndex = 1;
				}
				else
				{
					cmbARAlign.SelectedIndex = 0;
				}
			}
			else
			{
				lblARAudit.Enabled = false;
				cmbARAlign.Enabled = false;
			}
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "CRLastTellerID", ref strID);
			txtTellerID.Text = strID;
			ShowFrame(fraPreAudit);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
			if (e.CloseReason == FCCloseReason.FormControlMenu || boolToMainMenu)
			{
				boolToMainMenu = false;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				PreviewTellerCloseOut_2(false);
			}
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			boolToMainMenu = true;
			Close();
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsCheck = new clsDRWrapper();
			bool boolOK = false;
			int intTemp = 0;
			//FC:FINAL:DSE Use selected option text (because of removing / adding / changing options in cmbAudit)
			string selectedOptAudit = "";
			if (cmbAudit.SelectedIndex != -1)
			{
				selectedOptAudit = cmbAudit.Text;
			}
			if (!boolSavePressed)
			{
				// this will invalidate any keystroke after the first one is pressed
				boolSavePressed = false;
				cmdSave.Enabled = false;
				boolOK = true;
				// kk 06062013 trocrs-9  No validation if F12 pressed while in Dept text box
				//FC:FINAL:DSE Use selected option text (because of removing / adding / changing options in cmbAudit)
				//if (cmbAudit.SelectedIndex == 0 || cmbAudit.SelectedIndex == 3)
				if (selectedOptAudit == "Audit Preview" || selectedOptAudit == "Print Audit Report and Close Out" || selectedOptAudit == "Finalize Partial Close Out")
				{
					if (txtDept.Text != "")
					{
						if (!ValidateDept_2(txtDept.Text))
						{
							// if this is not a valid Dept
							MessageBox.Show("Please enter a valid Department.", "Invalid Department", MessageBoxButtons.OK, MessageBoxIcon.Information);
							boolOK = false;
							txtDept.Focus();
							txtDept.SelectionStart = 0;
							txtDept.SelectionLength = txtDept.Text.Length;
						}
					}
				}
				//FC:FINAL:DSE Use selected option text (because of removing / adding / changing options in cmbAudit)
				//if (cmbAudit.SelectedIndex == 1 || cmbAudit.SelectedIndex == 2)
				if (selectedOptAudit == "Teller Preview" || selectedOptAudit == "Close Out Individual Teller")
				{
					//if (txtTellerID.Text != "")
					//{
						// kk06032015 trocrs-41 Allow Teller Closeout to run if teller LoggedIn flag is false     ' If ValidateTellerID(Left$(Trim$(txtTellerID), 3)) < 2 Then
						intTemp = modGlobal.ValidateTellerID(Strings.Left(Strings.Trim(txtTellerID.Text), 3));
						if (intTemp < 2 || intTemp == 3)
						{
							// if this is a valid TellerID
							// then save it is the registry
							modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "CRLastTellerID", strTellerID);
						}
						else
						{
							// incorrect Teller ID
							MessageBox.Show("Please enter a valid Teller ID.", "Invalid Teller ID", MessageBoxButtons.OK, MessageBoxIcon.Information);
							boolOK = false;
							txtTellerID.Focus();
							txtTellerID.SelectionStart = 0;
							txtTellerID.SelectionLength = txtTellerID.Text.Length;
						}
					//}
				}
				if (boolOK)
				{
					rsCheck.OpenRecordset("SELECT * FROM (" + modGlobal.Statics.strCurrentDailyAuditSQL + ") as CurrentDailyAudit", modExtraModules.strCRDatabase);
					if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
					{
						if (fraAuditPassword.Visible)
						{
							cmdPassword_Click_2(0);
						}
						else
						{
							cmdDone_Click();
						}
					}
					else
					{
						boolMVTransactions = false;
						MessageBox.Show("There have been no transactions since the last Audit.", "No Eligible Accounts", MessageBoxButtons.OK, MessageBoxIcon.Information);
						//MDIParent.InstancePtr.Focus();
						Close();
					}
				}
				cmdSave.Enabled = true;
				boolSavePressed = false;
			}
		}

		private void optAudit_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			string selectedOptAudit = cmbAudit.Items[Index].ToString();
			if (selectedOptAudit == "Audit Preview")
			{
				// preview
				lblJournal.Enabled = false;
				cmbJournal.Enabled = false;
				lblMonth.Enabled = false;
				cmbMonth.Enabled = false;
				txtTellerID.Enabled = false;
				lblTellerID.Enabled = false;
				cmbTellerType.Enabled = false;
				lblTellerType.Enabled = false;
				cmbTellerType.SelectedIndex = 2;
				fraTellerID.Visible = false;
				// kk 111212 trocr-348  Add dept closeout
				if (boolDeptCloseOut)
				{
					txtDept.Enabled = true;
					// kk 110912 trocr-348  Add Dept Audit
					lblDept.Enabled = true;
					fraDept.Visible = true;
					// move the Dept id box to the proper place, then show it
					//fraDept.Top = fraPreAudit.Top + /*fraPreviewAudit.Top*/ + cmbAudit.Top;
					//fraDept.Left = fraPreAudit.Left + /*fraPreviewAudit.Left*/ +cmbAudit.Left + cmbAudit.Width;
					fraDept.ZOrder(ZOrderConstants.BringToFront);
					fraDept.Visible = true;
				}
				boolClicking = true;
				boolClicking = false;
			}
			if (selectedOptAudit == "Print Audit Report and Close Out" || selectedOptAudit == "Finalize Partial Close Out")
			{
				// audit
				if (modGlobalConstants.Statics.gboolBD)
				{
					lblJournal.Enabled = true;
					cmbJournal.Enabled = true;
					lblMonth.Enabled = true;
					cmbMonth.Enabled = true;
				}
				txtTellerID.Enabled = false;
				lblTellerID.Enabled = false;
				cmbTellerType.Enabled = false;
				lblTellerType.Enabled = false;
				cmbTellerType.SelectedIndex = 2;
				fraTellerID.Visible = false;
				// kk 111212 trocr-348  Add dept closeout
				if (boolDeptCloseOut)
				{
					txtDept.Enabled = true;
					// kk 110912 trocr-348  Add Dept Audit
					lblDept.Enabled = true;
					fraDept.Visible = true;
					// move the Dept id box to the proper place, then show it
					//fraDept.Top = fraPreAudit.Top + fraAuditInfo.Top + cmbAudit.Top + cmbAudit.Height + (lblMonth.Top - (lblJournal.Top + lblJournal.Height));
					//fraDept.Left = fraPreAudit.Left + fraAuditInfo.Left + lblJournal.Left;
					fraDept.ZOrder(ZOrderConstants.BringToFront);
					fraDept.Visible = true;
				}
				boolClicking = true;
				boolClicking = false;
			}
			if (selectedOptAudit == "Close Out Individual Teller" || selectedOptAudit == "Teller Preview")
			{
				// teller preview/audit
				lblJournal.Enabled = false;
				cmbJournal.Enabled = false;
				lblMonth.Enabled = false;
				cmbMonth.Enabled = false;
				txtDept.Enabled = false;
				// kk 110912 trocr-348  Add Dept Audit
				lblDept.Enabled = false;
				fraDept.Visible = false;
				txtTellerID.Enabled = true;
				lblTellerID.Enabled = true;
				if (Index == 1)
				{
					if (TellerClosedOutToday_2(txtTellerID.Text))
					{
						cmbTellerType.Enabled = true;
						lblTellerType.Enabled = true;
					}
					else
					{
						cmbTellerType.Enabled = false;
						lblTellerType.Enabled = false;
					}
				}
				else
				{
					cmbTellerType.SelectedIndex = 0;
				}
				// move the teller id box to the proper place, then show it
				if (Index == 1)
				{
					//fraTellerID.Top = fraPreAudit.Top /*+ fraPreviewTeller.Top*/ + cmbAudit.Top;
					//fraTellerID.Left = fraPreAudit.Left /*+ fraPreviewTeller.Left*/ + cmbAudit.Left + cmbAudit.Width;
					boolClicking = true;
					boolClicking = false;
				}
				else
				{
					//fraTellerID.Top = fraPreAudit.Top /*+ fraCloseOutTeller.Top*/ + cmbAudit.Top;
					//fraTellerID.Left = fraPreAudit.Left /*+ fraCloseOutTeller.Left*/ + cmbAudit.Left + cmbAudit.Width;
					boolClicking = true;
					boolClicking = false;
				}
				fraTellerID.ZOrder(ZOrderConstants.BringToFront);
				fraTellerID.Visible = true;
			}
			if (selectedOptAudit == "Perform Partial Close Out" || selectedOptAudit == "Save Partial Audit")
			{
				lblJournal.Enabled = false;
				cmbJournal.Enabled = false;
				lblMonth.Enabled = false;
				cmbMonth.Enabled = false;
				txtTellerID.Enabled = false;
				lblTellerID.Enabled = false;
				cmbTellerType.Enabled = false;
				lblTellerType.Enabled = false;
				txtDept.Enabled = false;
				// kk 110912 trocr-348  Add Dept Audit
				lblDept.Enabled = false;
				fraDept.Visible = false;
				cmbTellerType.SelectedIndex = 2;
				fraTellerID.Visible = false;
				boolClicking = true;
				boolClicking = false;
			}
		}

		private void optAudit_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbAudit.SelectedIndex;
			optAudit_CheckedChanged(index, sender, e);
		}

		private void txtAuditPassword_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						cmdPassword_Click_2(1);
						break;
					}
			}
			//end switch
		}

		private void txtAuditPassword_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				cmdPassword_Click_2(0);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtDept_Enter(object sender, System.EventArgs e)
		{
			txtDept.SelectionStart = 0;
			txtDept.SelectionLength = txtDept.Text.Length;
		}

		private void txtDept_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
			// check for the return ID
				case Keys.Return:
					{
                        //FC:FINAL:SBE - global fix - validate event should not be triggered twice (issue #4605)
						//txtDept_Validate(false);
						break;
					}
				case Keys.F12:
					{
						txtDept_Validate(false);
						break;
					}
			}
			//end switch
		}

		private void txtDept_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// change the letters to uppercase
			if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtDept_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strDept = "";
			if (txtDept.Text != "")
			{
				strDept = txtDept.Text;
				//FC:FINAL:DSE Use selected option text (because of removing / adding / changing options in cmbAudit)
				//if (cmbAudit.SelectedIndex == 0 || cmbAudit.SelectedIndex == 3)
				if (cmbAudit.Text == "Audit Preview" || cmbAudit.Text == "Print Audit Report and Close Out" || cmbAudit.Text == "Finalize Partial Close Out")
				{
					if (!ValidateDept(ref strDept))
					{
						// if this is not a valid Dept
						MessageBox.Show("Please enter a valid Department.", "Invalid Department", MessageBoxButtons.OK, MessageBoxIcon.Information);
						e.Cancel = true;
						txtDept.SelectionStart = 0;
						txtDept.SelectionLength = txtDept.Text.Length;
					}
				}
			}
		}

		public void txtDept_Validate(bool Cancel)
		{
			txtDept_Validating(txtDept, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void txtTellerID_Enter(object sender, System.EventArgs e)
		{
			txtTellerID.SelectionStart = 0;
			txtTellerID.SelectionLength = txtTellerID.Text.Length;
		}

		private void txtTellerID_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
			// check for the return ID
				case Keys.Return:
					{
                        //FC:FINAL:SBE - global fix - validate event should not be triggered twice (issue #4605)
						//txtTellerID_Validate(false);
						break;
					}
			}
			//end switch
		}

		private void txtTellerID_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// change the letters to uppercase
			if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private bool ValidateTellerId()
		{
			int intTemp = 0;
			strTellerID = Strings.Left(Strings.Trim(txtTellerID.Text), 3);
			//FC:FINAL:DSE Use selected option text (because of removing / adding / changing options in cmbAudit)
			//if (cmbAudit.SelectedIndex == 1 || cmbAudit.SelectedIndex == 2)
			if (cmbAudit.Text == "Teller Preview" || cmbAudit.Text == "Close Out Individual Teller")
			{
				// kk06032015 trocrs-41 Allow Teller Closeout to run if teller LoggedIn flag is false     ' If ValidateTellerID(strTellerID) < 2 Then
				intTemp = modGlobal.ValidateTellerID(strTellerID);
				if (intTemp < 2 || intTemp == 3)
				{
					// if this is a valid TellerID
					// then save it is the registry
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "CRLastTellerID", strTellerID);
				}
				else
				{
					// incorrect Teller ID
					MessageBox.Show("Please enter a valid Teller ID.", "Invalid Teller ID", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return false;
				}
			}

			return true;
		}

		private void ShowFrame(FCFrame fraFrame)
		{
			// this will place the frame in the middle of the form
			//fraFrame.Top = FCConvert.ToInt32((this.Height - fraFrame.Height) / 3.0);
			//fraFrame.Left = FCConvert.ToInt32((this.Width - fraFrame.Width) / 2.0);
			fraFrame.Visible = true;
		}

		private bool TellerClosedOutToday_2(string strID)
		{
			return TellerClosedOutToday(ref strID);
		}

		private bool TellerClosedOutToday(ref string strID)
		{
			bool TellerClosedOutToday = false;
			// this function will return true if the teller has performed a teller closeout
			// during this daily closeout period
			// kk10292014 trocr-437  Rewrite this function to look at the closeout records
			// Previous method looked at the Archive records, but that didn't
			// detect when a closeout done when no transactions were processed by the teller
			// "SELECT * FROM Archive WHERE TellerID = '" & strID & "' AND TellerCloseout <> 0 AND DailyCloseOut = 0"
			clsDRWrapper rsTest = new clsDRWrapper();
			int lngKey = 0;
			string strCOUserID = "";
			
			if (boolDeptCloseOut)
			{
				clsDRWrapper rsOperator = new clsDRWrapper();
				string deptDiv = "";
				string operators = "";

				// Find the Dept/Div and who can close out
				strCOUserID = "";

				rsOperator.OpenRecordset("Select * From Operators WHERE Code = '" + strID + "'", "SystemSettings");
				if (!rsOperator.BeginningOfFile() && !rsOperator.EndOfFile())
				{
					deptDiv = rsOperator.Get_Fields_String("DeptDiv");
					rsOperator.OpenRecordset("Select * From Operators WHERE DeptDiv = '" + deptDiv + "'", "SystemSettings");
					while (!rsOperator.EndOfFile())
					{
						operators += rsTest.Get_Fields("SecurityID") + ",";
						rsOperator.MoveNext();
					}
					if (operators != "")
					{
						operators = Strings.Left(strCOUserID, strCOUserID.Length - 1);
						operators = " Id IN (" + strCOUserID + ")";

						rsTest.OpenRecordset("SELECT UserID FROM Users WHERE ClientIdentifier = '" + StaticSettings.gGlobalSettings.ClientIdentifier + "' and " + operators + " ORDER BY UserID", "SystemSettings");
						while (!rsTest.EndOfFile())
						{
							// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
							strCOUserID += "'" + rsTest.Get_Fields("UserID") + "',";
							rsTest.MoveNext();
						}
						if (strCOUserID != "")
						{
							strCOUserID = Strings.Left(strCOUserID, strCOUserID.Length - 1);
							strCOUserID = " AND TellerID IN (" + strCOUserID + ")";
						}
					}
				}
			}
			else
			{
				strCOUserID = "";
			}
			// Find the last Regular Daily Closeout
			rsTest.OpenRecordset("SELECT TOP 1 ID from CloseOut WHERE Type = 'R'" + strCOUserID + " ORDER BY ID DESC");
			// <-  XXXXX has to include Dept query stuff XXXX
			if (rsTest.EndOfFile() != true && rsTest.BeginningOfFile() != true)
			{
				lngKey = FCConvert.ToInt32(rsTest.Get_Fields_Int32("ID"));
			}
			else
			{
				lngKey = 0;
			}
			// Check for a teller closeout done since the last regular daily closeout
			rsTest.OpenRecordset("SELECT * FROM CloseOut WHERE TellerID = '" + strID + "' AND Type = 'T' AND ID > " + FCConvert.ToString(lngKey));
			if (rsTest.EndOfFile() != true && rsTest.BeginningOfFile() != true)
			{
				TellerClosedOutToday = true;
			}
			else
			{
				TellerClosedOutToday = false;
			}
			return TellerClosedOutToday;
		}

		private int CloseOutTeller_8(string strTID, bool boolShowError = false, bool boolFinalize = false, bool boolPreview = false)
		{
			return CloseOutTeller(ref strTID, boolShowError, boolFinalize, boolPreview);
		}

		private int CloseOutTeller_71(string strTID, bool boolShowError = false, bool boolPreview = false)
		{
			return CloseOutTeller(ref strTID, boolShowError, false, boolPreview);
		}

		private int CloseOutTeller(ref string strTID, bool boolShowError = false, bool boolFinalize = false, bool boolPreview = false)
		{
			int CloseOutTeller = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will close out a teller and will return false if an error occurs or the teller has already closed out today
				// this function will also create a teller close out record in the closeout table and store the
				// ID if the closeout record in the tellercloseout field of the archive table
				clsDRWrapper rs = new clsDRWrapper();
				int lngKey = 0;
				string strSQL = "";
				if (TellerClosedOutToday(ref strTID))
				{
					if (!boolPreview)
					{
						if (boolShowError)
							MessageBox.Show("This teller has already been closed out during this audit period." + "\r\n" + "This teller cannot be closed out again.", "Teller Close Out Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						CloseOutTeller = -1;
					}
					// else let this run
				}
				else
				{
					if (boolPerformingPartialCO || boolPreview)
					{
						if (boolPreview)
						{
							lngKey = -99;
						}
						else
						{
							lngKey = -100;
							// partial audit code
						}
					}
					else
					{
						// this will create a closeout record and return the ID
						lngKey = CreateCloseOutRecord_6(strTID, true);
					}
					if (boolPreview)
					{
						// This will toggle the empty teller records with a values <> 0 so that they will be picked up in the audit preview
						if (strTID == "")
						{
							strSQL = "UPDATE Archive Set TellerCloseOut = " + FCConvert.ToString(lngKey) + " WHERE rTRIM(isnull(TellerID, '')) = '' AND ISNULL(TellerCloseOut,0) = 0";
						}
						else
						{
							strSQL = "UPDATE Archive Set TellerCloseOut = " + FCConvert.ToString(lngKey) + " WHERE TellerID = '" + strTID + "' AND ISNULL(TellerCloseOut,0) = 0";
						}
					}
					else if (boolFinalize)
					{
						// this will finalize a partial daily audit
						if (strTID == "")
						{
							strSQL = "UPDATE Archive Set TellerCloseOut = " + FCConvert.ToString(lngKey) + " WHERE rTRIM(isnull(TellerID, '')) = '' AND TellerCloseOut = -100";
						}
						else
						{
							strSQL = "UPDATE Archive Set TellerCloseOut = " + FCConvert.ToString(lngKey) + " WHERE TellerID = '" + strTID + "' AND TellerCloseOut = -100";
						}
					}
					else
					{
						// this will set all of the TellerCloseOut field of the Archive Records to the ID value
						if (strTID == "")
						{
							strSQL = "UPDATE Archive Set TellerCloseOut = " + FCConvert.ToString(lngKey) + " WHERE rTRIM(isnull(TellerID, '')) = '' AND ISNULL(TellerCloseOut,0) = 0";
						}
						else
						{
							strSQL = "UPDATE Archive Set TellerCloseOut = " + FCConvert.ToString(lngKey) + " WHERE TellerID = '" + strTID + "' AND ISNULL(TellerCloseOut,0) = 0";
						}
					}
					rs.Execute(strSQL, "TWCR0000.vb1");
					CloseOutTeller = lngKey;
				}
				return CloseOutTeller;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
                if (boolShowError)
                {
                    //MessageBox.Show("ERROR while updating TellerCloseOut.", "Teller Close Out Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    FCMessageBox.Show("ERROR while updating TellerCloseOut.", MsgBoxStyle.Critical | MsgBoxStyle.OkOnly, "Teller Close Out Error");
                }
            }
			return CloseOutTeller;
		}

		private int CloseOut(bool boolFinalize = false)
		{
			int CloseOut = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will close out all tellers that are still open and have not closed out during
				// the same day and the records from before the closeout of the tellers that closed out earlier
				// returns True if the tellers closed out, a closeout record is created and the number
				// is stored in the DailyCloseOut field of the archive table
				clsDRWrapper rs = new clsDRWrapper();
				clsDRWrapper rsMV = new clsDRWrapper();
				string strSQL = "";
				int lngKey = 0;
				string strDeptSQL = "";
				string strMVDeptSQL = "";
				bool boolMVDept = false;
				CloseOut = 0;
				// kk 102512 trocr-348  Close Out by Department
				if (boolDeptCloseOut)
				{
					rs.OpenRecordset("SELECT Code FROM Operators WHERE DeptDiv = '" + strDeptDiv + "'", "SystemSettings");
					while (!rs.EndOfFile())
					{
						if (strDeptSQL == "")
						{
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							strDeptSQL = " AND TellerID IN ('" + rs.Get_Fields("Code") + "'";
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							strMVDeptSQL = " AND OpID IN ('" + rs.Get_Fields("Code") + "'";
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							strDeptSQL += ",'" + rs.Get_Fields("Code") + "'";
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							strMVDeptSQL += ",'" + rs.Get_Fields("Code") + "'";
						}
						rs.MoveNext();
					}
					if (strDeptSQL != "")
					{
						// kk 06062013
						strDeptSQL += ")";
						strMVDeptSQL += ")";
					}
				}
				if (boolFinalize)
				{
					strSQL = "SELECT TellerID FROM Archive WHERE DailyCloseOut = -100" + strDeptSQL + " GROUP BY TellerID";
				}
				else
				{
					strSQL = "SELECT TellerID FROM Archive WHERE ISNULL(DailyCloseOut,0) = 0" + strDeptSQL + " GROUP BY TellerID";
				}
				rs.OpenRecordset(strSQL);
				// close all the tellers that have activity today
				while (!rs.EndOfFile())
				{
					CloseOutTeller_8(rs.Get_Fields_String("TellerID"), false, boolFinalize);
					rs.MoveNext();
				}
				if (modGlobalConstants.Statics.gboolMV && chkCloseOutMV.CheckState == Wisej.Web.CheckState.Checked)
				{
					// And boolMVTransactions Then   'this is not allowed in a partial close out
					// this is where I will close out the MV period
					boolMVDept = true;
					if (boolDeptCloseOut)
					{
						// kk 11262012 trocr-348  Add closeout by dept
						rsMV.OpenRecordset("SELECT TOP 1 ID FROM ActivityMaster WHERE OldMVR3 < 1" + strMVDeptSQL, modExtraModules.strMVDatabase);
						if (rsMV.RecordCount() == 0)
						{
							boolMVDept = false;
						}
						else
						{
							boolMVDept = true;
						}
					}
					if (boolMVDept)
					{
						if (strTellerID == "")
						{
							TryAgain:
							;
							strTellerID = frmTellerID.InstancePtr.Init();
							if (strTellerID == "" || strTellerID.Length < 3)
							{
								// I only check for a correct format of the teller id...the MV Close out routine will check the perimssions of the OP ID
								if (MessageBox.Show("The operator ID was not entered correctly.  Would you like to retry?", "Invalid ID", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
								{
									goto TryAgain;
								}
							}
						}
						if (!(strTellerID == "" || strTellerID.Length < 3))
						{
							modCloseoutMVPeriod.CloseMVPeriod(ref strTellerID);
						}
						else
						{
							modGlobalFunctions.AddCYAEntry_80("CR", "Did not perform MV Closeout", DateTime.Today.ToShortDateString(), DateTime.Today.ToShortTimeString(), strTellerID);
							MessageBox.Show("The Motor Vehicle period close out was not performed.", "Missing Valid Op ID", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
					}
				}
				// close all of the archive records that are teller closed out and not daily closed out
				if (boolPerformingPartialCO)
				{
					lngKey = -100;
					// the partial audit code
				}
				else
				{
					lngKey = CreateCloseOutRecord();
				}
				if (lngKey != 0)
				{
					if (boolFinalize)
					{
						// this will look up the partial transactions
						strSQL = "UPDATE Archive SET DailyCloseOut = " + FCConvert.ToString(lngKey) + " WHERE ISNULL(TellerCloseOut,0) <> 0 AND DailyCloseOut = -100" + strDeptSQL;
					}
					else
					{
						strSQL = "UPDATE Archive SET DailyCloseOut = " + FCConvert.ToString(lngKey) + " WHERE ISNULL(TellerCloseOut,0) <> 0 AND ISNULL(DailyCloseOut,0) = 0" + strDeptSQL;
					}
					rs.Execute(strSQL, "TWCR0000.vb1");
					CloseOut = lngKey;
				}
				else
				{
					CloseOut = 0;
				}
				return CloseOut;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				CloseOut = 0;
                //MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Close Out Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MsgBoxStyle.Critical | MsgBoxStyle.OkOnly, "Close Out Error");
            }
			return CloseOut;
		}

		private int CreateCloseOutRecord_6(string strTID, bool boolTeller = false, bool boolShowError = false)
		{
			return CreateCloseOutRecord(strTID, boolTeller, boolShowError);
		}

		private int CreateCloseOutRecord(string strTID = "", bool boolTeller = false, bool boolShowError = false)
		{
			int CreateCloseOutRecord = 0;
			clsDRWrapper rsCO = new clsDRWrapper();
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will create a new closeout record and return the ID
				// the type is passed into the function from where it is called
				rsCO.OpenRecordset("SELECT * FROM CloseOut WHERE ID = 0");
				rsCO.AddNew();
				if (boolTeller)
				{
					rsCO.Set_Fields("Type", "T");
					// teller
					if (strTID != "")
					{
						rsCO.Set_Fields("TellerID", strTID);
					}
					else
					{
						rsCO.Set_Fields("TellerID", "");
					}
				}
				else
				{
					rsCO.Set_Fields("Type", "R");
					// regular (daily)
					rsCO.Set_Fields("TellerID", modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID() + " ");
					// save the user ID for close outs
				}
				rsCO.Set_Fields("CloseoutDate", DateTime.Now);
				rsCO.Update(false);
				CreateCloseOutRecord = FCConvert.ToInt32(Math.Round(Conversion.Val(rsCO.Get_Fields_Int32("ID"))));
				// returns the ID
				rsCO = null;
				return CreateCloseOutRecord;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				CreateCloseOutRecord = 0;
				rsCO = null;
			}
			return CreateCloseOutRecord;
		}

		private void SetupSQL_2(bool boolShowPending)
		{
			SetupSQL(boolShowPending);
		}

		private void SetupSQL(bool boolShowPending = false)
		{
			int lngKey = 0;
			int lngTellerKey;
			string strDeptSQL = "";
			clsDRWrapper rs = new clsDRWrapper();
			if (boolShowPending)
			{
				lngKey = -100;
				// pending transaction ID
			}
			else
			{
				lngKey = lngCloseoutKey;
			}
			// kk 102512 trocr-348  Add Close Out by Department
			if (boolDeptCloseOut)
			{
				rs.OpenRecordset("SELECT Code FROM Operators WHERE DeptDiv = '" + strDeptDiv + "'", "SystemSettings");
				if (!rs.EndOfFile())
				{
					while (!rs.EndOfFile())
					{
						if (strDeptSQL == "")
						{
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							strDeptSQL = " AND Archive.TellerID IN ('" + rs.Get_Fields("Code") + "'";
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							strDeptSQL += ",'" + rs.Get_Fields("Code") + "'";
						}
						rs.MoveNext();
					}
					if (strDeptSQL != "")
					{
						// kk 06062013
						strDeptSQL += ")";
					}
				}
			}
			if (cmbAudit.Text == "Audit Preview")
			{
				// 0 Audit Preview
				strCloseOutSQL = " ISNULL(TellerCloseout,0) <> 0 AND ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(lngKey) + strDeptSQL;
			}
			//FC:FINAL:DSE Use selected option text (because of removing / adding / changing options in cmbAudit)

			else if (cmbAudit.Text == "Print Audit Report and Close Out" || cmbAudit.Text == "Finalize Partial Close Out")
			{
				// 1 Audit Report w/CloseOut
				strCloseOutSQL = " ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(lngKey);
			}
				//FC:FINAL:DSE Use selected option text (because of removing / adding / changing options in cmbAudit)
				else if (cmbAudit.Text == "Teller Preview")
			{
				// 2 Teller Preview
				if (cmbTellerType.SelectedIndex == 0)
				{
					// 0 Post
					strCloseOutSQL = " ISNULL(TellerCloseOut,0) = 0 AND ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(lngKey);
				}
				else if (cmbTellerType.SelectedIndex == 1)
				{
					// 1 Pre
					strCloseOutSQL = " ISNULL(TellerCloseOut,0) <> 0 AND ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(lngKey);
				}
				else
				{
					// 2 Both
					strCloseOutSQL = " ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(lngCloseoutKey);
				}
			}
					//FC:FINAL:DSE Use selected option text (because of removing / adding / changing options in cmbAudit)
					else if (cmbAudit.Text == "Close Out Individual Teller")
			{
				// 3 Teller Report w/CloseOut
				strCloseOutSQL = " ISNULL(TellerCloseOut,0) = " + FCConvert.ToString(lngKey) + " AND ISNULL(DailyCloseOut,0) = 0";
			}
			else
			{
				strCloseOutSQL = " TellerCloseOut = -100 AND ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(lngKey);
			}
		}

        private void CheckPermissions()
        {
            clsDRWrapper clsChildList;
            string strPerm = "";
            int lngChild = 0;
            clsChildList = new clsDRWrapper();
            int lngFuncID = modGlobal.CRSECURITYDAILYRECEIPTAUDIT;
            modGlobalConstants.Statics.clsSecurityClass.Get_Children(ref clsChildList, ref lngFuncID);
            while (!clsChildList.EndOfFile())
            {
                lngChild = FCConvert.ToInt32(clsChildList.GetData("childid"));
                strPerm = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(lngChild));
                switch (lngChild)
                {
                    case modGlobal.CRSECURITYPERFORMDAILYAUDIT:
                    {
                        // is this user allowed to perform the daily audit
                        if (strPerm == "F")
                        {
                            canPerformDailyAudit = true;
                            //optAudit[1].Enabled = true;
                            if (!cmbAudit.Items.Contains("Print Audit Report and Close Out"))
                            {
                                cmbAudit.Items.Insert(0, "Print Audit Report and Close Out");
                            }
                        }
                        else
                        {
                            canPerformDailyAudit = false;
                            if (cmbAudit.Items.Contains("Print Audit Report and Close Out"))
                            {
                                cmbAudit.Items.Remove("Print Audit Report and Close Out");
                            }
                        }
                        break;
                    }
                }
                //end switch
                clsChildList.MoveNext();
            }
        }
		public void HandlePartialPermission(ref int lngFuncID)
		{
			
		}

		private short CheckBDDatabase()
		{
			short CheckBDDatabase = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function is just checking to see if I can make a good connection to the BD database
				// it will return true if it makes a good connection and can access data
				clsDRWrapper rsBD = new clsDRWrapper();
				StreamReader ts;
				// vbPorter upgrade warning: lngCT As int	OnWrite(short, double)
				double lngCT = 0;
				string strTemp = "";
				if (modGlobalConstants.Statics.gboolBD)
				{
					rsBD.OpenRecordset("SELECT * FROM StandardAccounts", modExtraModules.strBDDatabase);
					if (!rsBD.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (FCConvert.ToInt32(rsBD.Get_Fields("Account")) != 0 || FCConvert.ToInt32(rsBD.Get_Fields("Account")) == 0)
						{
							CheckBDDatabase = -1;
						}
					}
					
					rsBD.Reset();
				}
				else
				{
					CheckBDDatabase = -1;
					return CheckBDDatabase;
				}
				return CheckBDDatabase;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				CheckBDDatabase = FCConvert.ToInt16(Information.Err(ex).Number);
				// MsgBox "Error accessing the Budgetary database.  Please run a compact and repair on the Budgetary Database and try again.", vbCritical, "Database Access"
				//MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Checking BD Database", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MsgBoxStyle.Critical | MsgBoxStyle.OkOnly, "Error Checking BD Database");
            }
			return CheckBDDatabase;
		}

		private void CheckForMissingReceipts_2(bool boolAutoPrint, string strPrinter = "", List<string> batchReports = null)
		{
			CheckForMissingReceipts(ref boolAutoPrint, strPrinter, batchReports: batchReports);
		}

		private void CheckForMissingReceipts(ref bool boolAutoPrint, string strPrinter = "", List<string> batchReports = null)
		{
			string strSQL;
			int lngMRN = 0;
			int lngStart = 0;
			// check to find out if there are any missing receipts...if not then do not print the report
			strSQL = "SELECT DISTINCT Receipt.ReceiptNumber FROM Receipt INNER JOIN Archive ON Receipt.ReceiptKey = Archive.ReceiptNumber WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(lngCloseoutKey) + "  GROUP BY Receipt.ReceiptNumber ORDER BY Receipt.ReceiptNumber";
			rsReceipts.OpenRecordset(strSQL, modExtraModules.strCRDatabase);
			if (rsReceipts.EndOfFile() != true && rsReceipts.BeginningOfFile() != true)
			{
				lngStart = FCConvert.ToInt32(Math.Round(Conversion.Val(rsReceipts.Get_Fields_Int32("ReceiptNumber"))));
				rsReceipts.MoveLast();
				lngEnd = FCConvert.ToInt32(Math.Round(Conversion.Val(rsReceipts.Get_Fields_Int32("ReceiptNumber"))));
				if ((lngEnd - lngStart) + 1 <= rsReceipts.RecordCount())
				{
					// find the difference between the last and the first counting the last and see if it matches the total count as a preliminary check
					lngMRN = -1;
					boolMissingReceipts = false;
				}
				else
				{
					lngMRN = lngStart;
					boolMissingReceipts = true;
				}
			}
			else
			{
				lngMRN = -1;
				boolMissingReceipts = false;
			}
			strMissingReceipts = "";
			if (boolMissingReceipts)
			{
				rsReceipts.MoveFirst();
				// fill the string
				while (!(lngMRN <= 0))
				{
					lngMRN = GetNextMissingReceiptNumber(ref lngMRN);
					if (lngMRN != -1)
					{
						strMissingReceipts += FCConvert.ToString(lngMRN) + ",";
						boolMissingReceipts = true;
					}
					lngMRN += 1;
				}
				if (Strings.Trim(strMissingReceipts) != "")
				{
					strMissingReceipts = Strings.Left(strMissingReceipts, strMissingReceipts.Length - 1);
				}
				else
				{
					strMissingReceipts = "";
				}
				if (strMissingReceipts != "")
				{
					// MAL@20080325: Tracker Reference: 12786
					if (boolAutoPrint)
					{
						modDuplexPrinting.DuplexPrintReport(sarMissingReceipts.InstancePtr, strPrinter, batchReports: batchReports);
					}
					else
					{
						if (MessageBox.Show("There are missing receipt numbers.  Would you like to print the missing receipt number report now?", "Missing Receipt Numbers", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
						{
							// DoEvents
							// this will show the missing receipt report
							frmReportViewer.InstancePtr.Init(sarMissingReceipts.InstancePtr);
						}
					}
				}
			}
			// MAL@20080918: Set deliberate connection reset to try to avoid error
			// Tracker Reference: 15407
			rsReceipts.Reset();
		}

		private int GetNextMissingReceiptNumber_2(int lngNum)
		{
			return GetNextMissingReceiptNumber(ref lngNum);
		}

		private int GetNextMissingReceiptNumber(ref int lngNum)
		{
			int GetNextMissingReceiptNumberRet = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// if there is another missing receipt number, then this function will return it
				// else it will return -1
				bool blnIsMissing;
				clsDRWrapper rsCheck = new clsDRWrapper();
				// MAL@20080321: Rework check for missing receipts
				// Tracker Reference: 12786
				if (lngNum <= 0)
				{
					GetNextMissingReceiptNumberRet = -1;
				}
				else
				{
					rsCheck.OpenRecordset("SELECT * FROM Receipt WHERE ReceiptNumber = " + FCConvert.ToString(lngNum), modExtraModules.strCRDatabase);
					if (rsCheck.RecordCount() > 0)
					{
						rsCheck.MoveFirst();
						rsCheck.FindFirstRecord("ReceiptNumber", lngNum);
						if (rsCheck.NoMatch)
						{
							GetNextMissingReceiptNumberRet = lngNum;
						}
						else
						{
							if (lngNum + 1 < lngEnd)
							{
								rsCheck = null;
								GetNextMissingReceiptNumberRet = GetNextMissingReceiptNumber_2(lngNum + 1);
							}
							else
							{
								GetNextMissingReceiptNumberRet = -1;
							}
						}
					}
				}
				return GetNextMissingReceiptNumberRet;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				//MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In FetchData", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MsgBoxStyle.Critical | MsgBoxStyle.OkOnly ,"Error In FetchData");
            }
			return GetNextMissingReceiptNumberRet;
		}

		private void CheckPrintingAuditOptions()
		{
			clsDRWrapper rsAudit = new clsDRWrapper();
			rsAudit.OpenRecordset("SELECT * FROM CashRec", modExtraModules.strCRDatabase);
			if (!rsAudit.EndOfFile())
			{
				rsAudit.Edit();
				boolCLAudit = FCConvert.ToBoolean(rsAudit.Get_Fields_Boolean("PrintCLAudit"));
				boolUTAudit = FCConvert.ToBoolean(rsAudit.Get_Fields_Boolean("PrintUTAudit"));
				boolARAudit = FCConvert.ToBoolean(rsAudit.Get_Fields_Boolean("PrintARAudit"));
				rsAudit.Update();
			}
			rsAudit = null;
		}

		private void CrossCheckTest()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsCC = new clsDRWrapper();
				bool boolRun = false;
				rsCC.OpenRecordset("SELECT ID, AutoRunCrossCheck, LastCrossCheck FROM CashRec", modExtraModules.strCRDatabase);
				if (!rsCC.EndOfFile())
				{
					if (Conversion.Val(rsCC.Get_Fields_DateTime("LastCrossCheck")) == 0)
					{
						rsCC.Edit();
						rsCC.Set_Fields("LastCrossCheck", DateTime.Today);
						rsCC.Update();
					}
					string vbPorterVar = rsCC.Get_Fields_String("AutoRunCrossCheck");
					if (vbPorterVar == "D")
					{
						if (DateAndTime.DateDiff("D", (DateTime)rsCC.Get_Fields_DateTime("LastCrossCheck"), DateTime.Today) >= 1)
						{
							boolRun = true;
						}
						else
						{
							boolRun = false;
						}
					}
					else if (vbPorterVar == "W")
					{
						if (DateAndTime.DateDiff("W", (DateTime)rsCC.Get_Fields_DateTime("LastCrossCheck"), DateTime.Today) >= 1)
						{
							boolRun = true;
						}
						else
						{
							boolRun = false;
						}
					}
					else if (vbPorterVar == "M")
					{
						if (DateAndTime.DateDiff("M", (DateTime)rsCC.Get_Fields_DateTime("LastCrossCheck"), DateTime.Today) >= 1)
						{
							boolRun = true;
						}
						else
						{
							boolRun = false;
						}
					}
					else
					{
						boolRun = false;
					}
					if (boolRun)
					{
						// Cross Check Payments
						frmReportViewer.InstancePtr.Init(rptPaymentCrossCheck.InstancePtr);
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				//MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Cross Check Test", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MsgBoxStyle.Critical | MsgBoxStyle.OkOnly, "Cross Check Test");
            }
		}

		private void PreviewTellerCloseOut_2(bool boolToggle)
		{
			PreviewTellerCloseOut(ref boolToggle);
		}

		private void PreviewTellerCloseOut(ref bool boolToggle)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strSQL;
				clsDRWrapper rsTempTeller = new clsDRWrapper();
				strSQL = "SELECT TellerID FROM Archive WHERE ISNULL(DailyCloseOut,0) = 0 GROUP BY TellerID";
				rsTempTeller.OpenRecordset(strSQL);
				if (boolToggle)
				{
					// close all the tellers that have activity today
					while (!rsTempTeller.EndOfFile())
					{
						CloseOutTeller_71(rsTempTeller.Get_Fields_String("TellerID"), false, true);
						rsTempTeller.MoveNext();
					}
				}
				else
				{
					strSQL = "UPDATE Archive Set TellerCloseOut = 0 WHERE TellerCloseOut = -99";
					rsTempTeller.Execute(strSQL, "TWCR0000.vb1");
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				//MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Teller Close Out - Preview", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MsgBoxStyle.Critical | MsgBoxStyle.OkOnly, "Teller Close Out - Preview");
            }
		}

		private bool ValidateDept_2(string strDept)
		{
			return ValidateDept(ref strDept);
		}

		private bool ValidateDept(ref string strDept)
		{
			bool ValidateDept = false;
			// checks the teller table and returns true if the TellerID is found otherwise false
			clsDRWrapper rsVal = new clsDRWrapper();
			rsVal.DefaultDB = modExtraModules.strCRDatabase;
			rsVal.OpenRecordset("SELECT TOP 1 DeptDiv FROM Operators WHERE upper(DeptDiv) = '" + Strings.UCase(strDept) + "'", "SystemSettings");
			if (rsVal.RecordCount() == 0)
			{
				ValidateDept = false;
			}
			else
			{
				ValidateDept = true;
			}
			return ValidateDept;
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuFileSave_Click(sender, e);
		}

        private enum AuditOptions
        {
            AuditPreview = 0,
            PrintAuditReportAndCloseout = 1,
            FinalizePartialCloseout = 1,
            TellerPreview = 2,
            CloseoutIndividualTeller = 3,
            PerformPartialCloseout = 4
        }
	}
}
