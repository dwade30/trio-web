﻿namespace TWCR0000
{
	/// <summary>
	/// Summary description for arReceiptSearch.
	/// </summary>
	partial class arReceiptSearch
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(arReceiptSearch));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.sarRSDetail = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lnDetailTotal = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblCashTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalCash = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCreditTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCheckTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalCheck = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.lblDisclaimer = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.dlg1 = new GrapeCity.ActiveReports.SectionReportModel.ARControl();
			this.fldTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblReportType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.groupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.groupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.sarRSSummary = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.sarRSMVSummary = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCashTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCash)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCreditTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheckTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDisclaimer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.sarRSDetail,
            this.lblTotal,
            this.fldTotal,
            this.lnDetailTotal,
            this.lblCashTotal,
            this.fldTotalCash,
            this.lblCreditTotal,
            this.fldTotalCredit,
            this.lblCheckTotal,
            this.fldTotalCheck});
			this.Detail.Height = 1.15625F;
			this.Detail.Name = "Detail";
			// 
			// sarRSDetail
			// 
			this.sarRSDetail.CloseBorder = false;
			this.sarRSDetail.Height = 0.125F;
			this.sarRSDetail.Left = 0F;
			this.sarRSDetail.Name = "sarRSDetail";
			this.sarRSDetail.Report = null;
			this.sarRSDetail.Top = 0.0625F;
			this.sarRSDetail.Width = 7F;
			// 
			// lblTotal
			// 
			this.lblTotal.Height = 0.15F;
			this.lblTotal.HyperLink = null;
			this.lblTotal.Left = 5.15F;
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Style = "font-family: \'Tahoma\'";
			this.lblTotal.Text = "Total:";
			this.lblTotal.Top = 0.3F;
			this.lblTotal.Width = 0.65F;
			// 
			// fldTotal
			// 
			this.fldTotal.Height = 0.15F;
			this.fldTotal.Left = 5.8F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotal.Text = "0.00";
			this.fldTotal.Top = 0.3F;
			this.fldTotal.Width = 0.95F;
			// 
			// lnDetailTotal
			// 
			this.lnDetailTotal.Height = 0F;
			this.lnDetailTotal.Left = 5.15F;
			this.lnDetailTotal.LineWeight = 1F;
			this.lnDetailTotal.Name = "lnDetailTotal";
			this.lnDetailTotal.Top = 0.3F;
			this.lnDetailTotal.Width = 1.6F;
			this.lnDetailTotal.X1 = 5.15F;
			this.lnDetailTotal.X2 = 6.75F;
			this.lnDetailTotal.Y1 = 0.3F;
			this.lnDetailTotal.Y2 = 0.3F;
			// 
			// lblCashTotal
			// 
			this.lblCashTotal.Height = 0.1875F;
			this.lblCashTotal.HyperLink = null;
			this.lblCashTotal.Left = 5.125F;
			this.lblCashTotal.Name = "lblCashTotal";
			this.lblCashTotal.Style = "font-family: \'Tahoma\'";
			this.lblCashTotal.Text = "Cash:";
			this.lblCashTotal.Top = 0.5F;
			this.lblCashTotal.Width = 0.65625F;
			// 
			// fldTotalCash
			// 
			this.fldTotalCash.Height = 0.1875F;
			this.fldTotalCash.Left = 5.8125F;
			this.fldTotalCash.Name = "fldTotalCash";
			this.fldTotalCash.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalCash.Text = "0.00";
			this.fldTotalCash.Top = 0.5F;
			this.fldTotalCash.Width = 0.9375F;
			// 
			// lblCreditTotal
			// 
			this.lblCreditTotal.Height = 0.1875F;
			this.lblCreditTotal.HyperLink = null;
			this.lblCreditTotal.Left = 5.125F;
			this.lblCreditTotal.Name = "lblCreditTotal";
			this.lblCreditTotal.Style = "font-family: \'Tahoma\'";
			this.lblCreditTotal.Text = "Credit:";
			this.lblCreditTotal.Top = 0.875F;
			this.lblCreditTotal.Width = 0.65625F;
			// 
			// fldTotalCredit
			// 
			this.fldTotalCredit.Height = 0.1875F;
			this.fldTotalCredit.Left = 5.8125F;
			this.fldTotalCredit.Name = "fldTotalCredit";
			this.fldTotalCredit.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalCredit.Text = "0.00";
			this.fldTotalCredit.Top = 0.875F;
			this.fldTotalCredit.Width = 0.9375F;
			// 
			// lblCheckTotal
			// 
			this.lblCheckTotal.Height = 0.1875F;
			this.lblCheckTotal.HyperLink = null;
			this.lblCheckTotal.Left = 5.125F;
			this.lblCheckTotal.Name = "lblCheckTotal";
			this.lblCheckTotal.Style = "font-family: \'Tahoma\'";
			this.lblCheckTotal.Text = "Check:";
			this.lblCheckTotal.Top = 0.6875F;
			this.lblCheckTotal.Width = 0.65625F;
			// 
			// fldTotalCheck
			// 
			this.fldTotalCheck.Height = 0.1875F;
			this.fldTotalCheck.Left = 5.8125F;
			this.fldTotalCheck.Name = "fldTotalCheck";
			this.fldTotalCheck.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalCheck.Text = "0.00";
			this.fldTotalCheck.Top = 0.6875F;
			this.fldTotalCheck.Width = 0.9375F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblDisclaimer});
			this.ReportFooter.Height = 0.1979167F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// lblDisclaimer
			// 
			this.lblDisclaimer.Height = 0.2F;
			this.lblDisclaimer.HyperLink = null;
			this.lblDisclaimer.Left = 0.3F;
			this.lblDisclaimer.Name = "lblDisclaimer";
			this.lblDisclaimer.Style = "font-family: \'Tahoma\'; text-align: center";
			this.lblDisclaimer.Text = "* - Indicates a receipt with multiple payments.";
			this.lblDisclaimer.Top = 0F;
			this.lblDisclaimer.Visible = false;
			this.lblDisclaimer.Width = 6.35F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtTitle,
            this.txtMuniName,
            this.txtDate,
            this.txtPage,
            this.dlg1,
            this.fldTime,
            this.lblReportType});
			this.PageHeader.Height = 0.84375F;
			this.PageHeader.Name = "PageHeader";
			// 
			// txtTitle
			// 
			this.txtTitle.Height = 0.25F;
			this.txtTitle.Left = 0F;
			this.txtTitle.Name = "txtTitle";
			this.txtTitle.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.txtTitle.Text = "Title";
			this.txtTitle.Top = 0F;
			this.txtTitle.Width = 7F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'";
			this.txtMuniName.Text = "MuniName";
			this.txtMuniName.Top = 0F;
			this.txtMuniName.Width = 2.875F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 4.5F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtDate.Text = "Date";
			this.txtDate.Top = 0F;
			this.txtDate.Width = 2.5F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 5.9375F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtPage.Text = "Page";
			this.txtPage.Top = 0.1875F;
			this.txtPage.Width = 1.0625F;
			// 
			// dlg1
			// 
			this.dlg1.Height = 0.3333333F;
			this.dlg1.Left = 0F;
			this.dlg1.Name = "dlg1";
			this.dlg1.Top = 0F;
			this.dlg1.Visible = false;
			this.dlg1.Width = 0.3333333F;
			// 
			// fldTime
			// 
			this.fldTime.Height = 0.1875F;
			this.fldTime.Left = 0F;
			this.fldTime.Name = "fldTime";
			this.fldTime.Style = "font-family: \'Tahoma\'";
			this.fldTime.Text = "MuniName";
			this.fldTime.Top = 0.1875F;
			this.fldTime.Width = 1.25F;
			// 
			// lblReportType
			// 
			this.lblReportType.Height = 0.5F;
			this.lblReportType.HyperLink = null;
			this.lblReportType.Left = 0F;
			this.lblReportType.Name = "lblReportType";
			this.lblReportType.Style = "font-family: \'Tahoma\'; text-align: center";
			this.lblReportType.Text = null;
			this.lblReportType.Top = 0.34375F;
			this.lblReportType.Width = 7F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// groupHeader2
			// 
			this.groupHeader2.Height = 0F;
			this.groupHeader2.Name = "groupHeader2";
			// 
			// groupFooter2
			// 
			this.groupFooter2.CanShrink = true;
			this.groupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.sarRSMVSummary});
			this.groupFooter2.Height = 0.1562501F;
			this.groupFooter2.KeepTogether = true;
			this.groupFooter2.Name = "groupFooter2";
			// 
			// sarRSSummary
			// 
			this.sarRSSummary.CloseBorder = false;
			this.sarRSSummary.Height = 0.09375F;
			this.sarRSSummary.Left = 0F;
			this.sarRSSummary.Name = "sarRSSummary";
			this.sarRSSummary.Report = null;
			this.sarRSSummary.Top = 0F;
			this.sarRSSummary.Width = 7F;
			// 
			// sarRSMVSummary
			// 
			this.sarRSMVSummary.CloseBorder = false;
			this.sarRSMVSummary.Height = 0.09375F;
			this.sarRSMVSummary.Left = 0F;
			this.sarRSMVSummary.Name = "sarRSMVSummary";
			this.sarRSMVSummary.Report = null;
			this.sarRSMVSummary.Top = 0F;
			this.sarRSMVSummary.Width = 6.865F;
			// 
			// groupHeader1
			// 
			this.groupHeader1.Name = "groupHeader1";
			// 
			// groupFooter1
			// 
			this.groupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.sarRSSummary});
			this.groupFooter1.Height = 0.125F;
			this.groupFooter1.KeepTogether = true;
			this.groupFooter1.Name = "groupFooter1";
			// 
			// arReceiptSearch
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.groupHeader2);
			this.Sections.Add(this.groupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.groupFooter1);
			this.Sections.Add(this.groupFooter2);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCashTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCash)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCreditTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheckTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDisclaimer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarRSDetail;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnDetailTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCashTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCash;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCreditTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCredit;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCheckTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCheck;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		public GrapeCity.ActiveReports.SectionReportModel.Label lblDisclaimer;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.ARControl dlg1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReportType;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter2;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarRSSummary;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarRSMVSummary;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
	}
}
