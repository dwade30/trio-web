﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Enums;
using SharedApplication.CashReceipts.Models;
using SharedApplication.Extensions;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCR0000
{
    public partial class arReceiptSearch : BaseSectionReport
    {
        bool boolPrinted;
        public IReceiptSearchService reportService;
        public IEnumerable<ReceiptSearchReportResult> reportData;

        public arReceiptSearch()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            this.Name = "Receipt Search";
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                sarRSDetail.Dispose();
				sarRSMVSummary.Dispose();
				sarRSSummary.Dispose();
				this.Document.Dispose();
            }
            base.Dispose(disposing);
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
        {
            if (boolPrinted)
            {
                eArgs.EOF = true;
            }
            else
            {
                boolPrinted = true;
                eArgs.EOF = false;
            }
        }

        private void ActiveReport_PageStart(object sender, EventArgs e)
        {
            // SHOW THE PAGE NUMBER
            txtPage.Text = "Page " + this.PageNumber;
        }

        private void ActiveReport_ReportEnd(object sender, EventArgs e)
        {
            if (this.Document.Pages.Count == 0)
            {
                MessageBox.Show("No pages to view.  Please run this report again.", "No Pages", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                this.Close();
            }
        }

        public void Init(IReceiptSearchService reportService)
        {
	        this.reportService = reportService;
	        this.reportData = reportService.GetReportData();
        }

        private void ActiveReport_ReportStart(object sender, EventArgs e)
        {
	        if (reportData.Count() == 0)
	        {
				NoRecords();
				return;
	        }

            lblReportType.Text = GetReportType();
            txtMuniName.Text = modGlobalConstants.Statics.MuniName;
            txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
            fldTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
            txtTitle.Text = "Receipt Search Report";

            SetupTotals();

            if (reportService.ReportSetup.MotorVehicleTransactionType == MotorVehicleTransactionTypes.All)
            {
	            sarRSMVSummary.Visible = true;
	            sarRSMVSummary.Report = new sarReceiptSearchMVSummary();
            }

			switch (reportService.ReportSetup.ReportDetailLevel)
            {
                case SummaryDetailBothOption.Summary:
                    {
                        sarRSSummary.Report = new sarReceiptSearchSummary();
                        break;
                    }
                case SummaryDetailBothOption.Detail:
                    {
                        sarRSDetail.Report = new sarReceiptSearchDetail();
                        break;
                    }
                case SummaryDetailBothOption.Both:
                    {
                        sarRSSummary.Report = new sarReceiptSearchSummary();
                        sarRSDetail.Report = new sarReceiptSearchDetail();
                        break;
                    }
            }
           
        }

        private void SetupTotals()
        {
            if (reportService.ReportSetup.ReportDetailLevel != SummaryDetailBothOption.Summary)
            {
				var totals = new ReceiptSearchReportAmountSummary
				{
					Amount1Total = reportData.Select(x => x.Amount1).DefaultIfEmpty(0).Sum(),
					Amount2Total = reportData.Select(x => x.Amount2).DefaultIfEmpty(0).Sum(),
					Amount3Total = reportData.Select(x => x.Amount3).DefaultIfEmpty(0).Sum(),
					Amount4Total = reportData.Select(x => x.Amount4).DefaultIfEmpty(0).Sum(),
					Amount5Total = reportData.Select(x => x.Amount5).DefaultIfEmpty(0).Sum(),
					Amount6Total = reportData.Select(x => x.Amount6).DefaultIfEmpty(0).Sum(),
					CashTotal = reportData.Select(x => x.CashPaidAmount).DefaultIfEmpty(0).Sum(),
					CardTotal = reportData.Select(x => x.CardPaidAmount).DefaultIfEmpty(0).Sum(),
					CheckTotal = reportData.Select(x => x.CheckPaidAmount).DefaultIfEmpty(0).Sum()
				};

				fldTotal.Text =
					(totals.Amount1Total + totals.Amount2Total + totals.Amount3Total + totals.Amount4Total +
					 totals.Amount5Total + totals.Amount6Total).ToString("#,##0.00");

				fldTotalCash.Text = totals.CashTotal.ToString("#,##0.00");
				fldTotalCheck.Text = totals.CheckTotal.ToString("#,##0.00");
				fldTotalCredit.Text = totals.CardTotal.ToString("#,##0.00");

				fldTotal.Top = sarRSDetail.Top + sarRSDetail.Height + 100 / 1440f;
                lblTotal.Top = fldTotal.Top;
                lnDetailTotal.Y1 = fldTotal.Top;
                lnDetailTotal.Y2 = fldTotal.Top;
                lblCashTotal.Top = lblTotal.Top + lblTotal.Height + 100 / 1440f;
                fldTotalCash.Top = lblTotal.Top + lblTotal.Height + 100 / 1440f;
                lblCheckTotal.Top = lblCashTotal.Top + lblCashTotal.Height;
                fldTotalCheck.Top = lblCashTotal.Top + lblCashTotal.Height;
                lblCreditTotal.Top = lblCheckTotal.Top + lblCheckTotal.Height;
                fldTotalCredit.Top = lblCheckTotal.Top + lblCheckTotal.Height;
                fldTotal.Visible = true;
                lblTotal.Visible = true;
                lnDetailTotal.Visible = true;
                lblCashTotal.Visible = true;
                fldTotalCash.Visible = true;
                lblCheckTotal.Visible = true;
                fldTotalCheck.Visible = true;
                lblCreditTotal.Visible = true;
                fldTotalCredit.Visible = true;
            }
            else
            {
                lnDetailTotal.Visible = false;
                lblTotal.Visible = false;
                fldTotal.Visible = false;
                lblCashTotal.Visible = false;
                fldTotalCash.Visible = false;
                lblCheckTotal.Visible = false;
                fldTotalCheck.Visible = false;
                lblCreditTotal.Visible = false;
                fldTotalCredit.Visible = false;

                sarRSDetail.Visible = false;
			}
        }

        private string GetEqualRangeHeaderText(string caption, string value1 = "", string value2 = "")
        {
	        string result = "";

	        if (value1 != "" && value2 != "" && value1 != value2)
	        {
		        result = caption + " Between " + value1 + " and " + value2;
	        }
			else if (value1 != "")
	        {
		        result = caption + " = " + value1;
	        }
	        else if (value2 != "")
	        {
		        result = caption + " = " + value2;
			}
	        
			return result;
        }

        private string AppendHeaderText(string header, string captionToAdd)
        {
	        string result = header;

	        if (captionToAdd != "")
	        {
		        if (result != "")
			        result += ", ";

		        result = result + captionToAdd;
	        }

	        return result;
        }

        private string ReturnMotorVehicleTransactionTypeDescription()
        {
	        switch (reportService.ReportSetup.MotorVehicleTransactionType)
	        {
				case MotorVehicleTransactionTypes.All:
					return "All Types w/Summary";
				case MotorVehicleTransactionTypes.Correction:
					return "ECO / ECR";
				case MotorVehicleTransactionTypes.DuplicateRegistration:
					return "DPR";
				case MotorVehicleTransactionTypes.DuplicateStickers:
					return "DPS";
				case MotorVehicleTransactionTypes.LostPlateStickers:
					return "LPS";
				case MotorVehicleTransactionTypes.GrossVehicleWeightChange:
					return "GVW";
				case MotorVehicleTransactionTypes.NewRegistration:
					return "NRR";
				case MotorVehicleTransactionTypes.NewRegistrationTransfer:
					return "NRT";
				case MotorVehicleTransactionTypes.ReRegistration:
					return "RRR";
				case MotorVehicleTransactionTypes.ReRegistrationTransfer:
					return "RRT";
				default:
					return "";
			}
        }

        private string ReturnAmountSearchTypeHeading(AmountSearchOptions option)
        {
	        switch (option)
	        {
				case AmountSearchOptions.Equals:
					return "Equals";
				case AmountSearchOptions.GreaterThan:
					return "Greater Than";
				case AmountSearchOptions.LessThan:
					return "Less Than";
				case AmountSearchOptions.NotEqual:
					return "Not Equal";
				default:
					return "";
	        }
        }

		private string GetReportType()
        {
            string strTemp = "";

			strTemp = AppendHeaderText(strTemp, GetEqualRangeHeaderText("Actual Date",
				reportService.ReportSetup.ActualDateStart != null
                     ? reportService.ReportSetup.ActualDateStart.Value.ToShortDateString()
                     : "",
				reportService.ReportSetup.ActualDateEnd != null
                     ? reportService.ReportSetup.ActualDateEnd.Value.ToShortDateString()
                     : ""));

			strTemp = AppendHeaderText(strTemp, GetEqualRangeHeaderText("Close Out Date",
				reportService.ReportSetup.CloseOutDateStartDescription, reportService.ReportSetup.CloseOutDateEndDescription));

			strTemp = AppendHeaderText(strTemp, GetEqualRangeHeaderText("Teller ID", reportService.ReportSetup.TellerId));

			strTemp = AppendHeaderText(strTemp, GetEqualRangeHeaderText("Receipt Type",
				reportService.ReportSetup.ReceiptTypeCodeStart > 0 ? reportService.ReportSetup.ReceiptTypeCodeStart.ToString() : "",
				reportService.ReportSetup.ReceiptTypeCodeEnd > 0 ? reportService.ReportSetup.ReceiptTypeCodeEnd.ToString() : ""));

			strTemp = AppendHeaderText(strTemp, GetEqualRangeHeaderText("Receipt Number",
				reportService.ReportSetup.ReceiptNumberStart > 0 ? reportService.ReportSetup.ReceiptNumberStart.ToString() : "",
				reportService.ReportSetup.ReceiptNumberEnd > 0 ? reportService.ReportSetup.ReceiptNumberEnd.ToString() : ""));

			strTemp = AppendHeaderText(strTemp, GetEqualRangeHeaderText("Reference", reportService.ReportSetup.Reference));

			if (reportService.ReportSetup.Name.Trim() != "")
			{
				strTemp = AppendHeaderText(strTemp, "Name " + (reportService.ReportSetup.NameSearchType == NameSearchOptions.BeginsWith ? "Begins With" : "Contains") + " " + reportService.ReportSetup.Name);
			}

			strTemp = AppendHeaderText(strTemp, GetEqualRangeHeaderText("Control 1", reportService.ReportSetup.Control1));

			strTemp = AppendHeaderText(strTemp, GetEqualRangeHeaderText("Control 2", reportService.ReportSetup.Control2));

			strTemp = AppendHeaderText(strTemp, GetEqualRangeHeaderText("Control 3", reportService.ReportSetup.Control3));

			strTemp = AppendHeaderText(strTemp, GetEqualRangeHeaderText("Account Number", reportService.ReportSetup.GLAccount));

			strTemp = AppendHeaderText(strTemp, GetEqualRangeHeaderText("Motor Vehicle Type", ReturnMotorVehicleTransactionTypeDescription()));

			if (reportService.ReportSetup.ReceiptTotal != null)
			{
				strTemp = AppendHeaderText(strTemp, "Receipt Total " + ReturnAmountSearchTypeHeading(reportService.ReportSetup.ReceiptTotalSearchType) + " " + reportService.ReportSetup.ReceiptTotal);
			}

			if (reportService.ReportSetup.TenderedTotal != null)
			{
				strTemp = AppendHeaderText(strTemp, "Tendered Total " + ReturnAmountSearchTypeHeading(reportService.ReportSetup.TenderedTotalSearchType) + " " + reportService.ReportSetup.TenderedTotal);
			}

			if (reportService.ReportSetup.PaidBy.Trim() != "")
			{
				strTemp = AppendHeaderText(strTemp, "Paid By " + (reportService.ReportSetup.PaidBySearchType == NameSearchOptions.BeginsWith ? "Begins With" : "Contains") + " " + reportService.ReportSetup.PaidBy);
			}

			strTemp = AppendHeaderText(strTemp, GetEqualRangeHeaderText("Check Number", reportService.ReportSetup.CheckNumber));

			strTemp = AppendHeaderText(strTemp, GetEqualRangeHeaderText("Bank", reportService.ReportSetup.BankDescription));

			strTemp = AppendHeaderText(strTemp, GetEqualRangeHeaderText("Comment", reportService.ReportSetup.Comment));

			if (reportService.ReportSetup.PaymentType != PaymentMethod.None)
			{
				strTemp = AppendHeaderText(strTemp, GetEqualRangeHeaderText("Payment Type", reportService.ReportSetup.PaymentType.ToString()));
			}
			
			if (reportService.ReportSetup.ReceiptTypes.Count() > 0)
			{
				string strTypes = "Receipt Types: ";

				foreach (var type in reportService.ReportSetup.ReceiptTypes)
				{
					strTypes = strTypes + type.ToString() + ", ";
				}

				if (strTypes != "")
				{
					strTypes = strTypes.Left(strTypes.Length - 2);
				}

				strTemp = AppendHeaderText(strTemp, strTypes);
			}
			
            return strTemp;
        }

		public void NoRecords()
		{
			MessageBox.Show("There are no records matching the criteria selected.", "Receipt Search", MessageBoxButtons.OK, MessageBoxIcon.Information);
			this.Close();
		}
	}
}
