﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmTellerID.
	/// </summary>
	partial class frmGetNewCRBank : BaseForm
	{

		protected override void Dispose(bool disposing)
		{
			
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGetNewCRBank));
            this.txtRouting = new fecherFoundation.FCTextBox();
            this.txtBankName = new fecherFoundation.FCTextBox();
            this.cmdSave = new fecherFoundation.FCButton();
            this.javaScript1 = new Wisej.Web.JavaScript(this.components);
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.fcLabel2 = new fecherFoundation.FCLabel();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 232);
            this.BottomPanel.Size = new System.Drawing.Size(250, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fcLabel2);
            this.ClientArea.Controls.Add(this.fcLabel1);
            this.ClientArea.Controls.Add(this.txtRouting);
            this.ClientArea.Controls.Add(this.txtBankName);
            this.ClientArea.Size = new System.Drawing.Size(250, 172);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(250, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(118, 30);
            this.HeaderText.Text = "Add Bank";
            // 
            // txtRouting
            // 
            this.txtRouting.BackColor = System.Drawing.SystemColors.Window;
            this.javaScript1.SetJavaScript(this.txtRouting, resources.GetString("txtRouting.JavaScript"));
            this.txtRouting.Location = new System.Drawing.Point(40, 117);
            this.txtRouting.Name = "txtRouting";
            this.txtRouting.Size = new System.Drawing.Size(170, 40);
            this.txtRouting.TabIndex = 27;
            this.txtRouting.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            // 
            // txtBankName
            // 
            this.txtBankName.BackColor = System.Drawing.SystemColors.Window;
            this.txtBankName.Location = new System.Drawing.Point(40, 44);
            this.txtBankName.Name = "txtBankName";
            this.txtBankName.Size = new System.Drawing.Size(170, 40);
            this.txtBankName.TabIndex = 26;
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(75, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(100, 48);
            this.cmdSave.TabIndex = 1;
            this.cmdSave.Text = "OK";
            // 
            // fcLabel1
            // 
            this.fcLabel1.AutoSize = true;
            this.fcLabel1.Location = new System.Drawing.Point(40, 21);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(47, 17);
            this.fcLabel1.TabIndex = 28;
            this.fcLabel1.Text = "NAME";
            // 
            // fcLabel2
            // 
            this.fcLabel2.AutoSize = true;
            this.fcLabel2.Location = new System.Drawing.Point(40, 94);
            this.fcLabel2.Name = "fcLabel2";
            this.fcLabel2.Size = new System.Drawing.Size(132, 17);
            this.fcLabel2.TabIndex = 29;
            this.fcLabel2.Text = "ROUTING NUMBER";
            // 
            // frmGetNewCRBank
            // 
            this.ClientSize = new System.Drawing.Size(250, 340);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmGetNewCRBank";
            this.ScrollBars = Wisej.Web.ScrollBars.None;
            this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
            this.Text = "Add Bank";
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        public FCTextBox txtRouting;
        public FCTextBox txtBankName;
        private FCButton cmdSave;
        private JavaScript javaScript1;
        private System.ComponentModel.IContainer components;
        private FCLabel fcLabel2;
        private FCLabel fcLabel1;
    }
}
