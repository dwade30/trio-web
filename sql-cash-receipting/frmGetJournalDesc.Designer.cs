﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmGetJournalDesc.
	/// </summary>
	partial class frmGetJournalDesc : BaseForm
	{
		public fecherFoundation.FCCheckBox chkEntryDescription;
		public fecherFoundation.FCTextBox txtDesc;
		public Global.T2KDateBox txtDate;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.chkEntryDescription = new fecherFoundation.FCCheckBox();
			this.txtDesc = new fecherFoundation.FCTextBox();
			this.txtDate = new Global.T2KDateBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			//this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkEntryDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 298);
			this.BottomPanel.Size = new System.Drawing.Size(530, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.chkEntryDescription);
			this.ClientArea.Controls.Add(this.txtDesc);
			this.ClientArea.Controls.Add(this.txtDate);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(530, 238);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(530, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(289, 30);
			this.HeaderText.Text = "Enter Journal Information";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// chkEntryDescription
			// 
			this.chkEntryDescription.Checked = true;
			this.chkEntryDescription.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkEntryDescription.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(143)))), ((int)(((byte)(164)))));
			this.chkEntryDescription.Location = new System.Drawing.Point(30, 164);
			this.chkEntryDescription.Name = "chkEntryDescription";
			this.chkEntryDescription.Size = new System.Drawing.Size(272, 27);
			this.chkEntryDescription.TabIndex = 4;
			this.chkEntryDescription.Text = "Use description for journal entries";
			this.ToolTip1.SetToolTip(this.chkEntryDescription, null);
			this.chkEntryDescription.Visible = false;
			// 
			// txtDesc
			// 
			this.txtDesc.MaxLength = 25;
			this.txtDesc.AutoSize = false;
            this.txtDesc.CharacterCasing = CharacterCasing.Upper;
            this.txtDesc.BackColor = System.Drawing.SystemColors.Window;
			this.txtDesc.Location = new System.Drawing.Point(250, 30);
			this.txtDesc.Name = "txtDesc";
			this.txtDesc.Size = new System.Drawing.Size(203, 40);
			this.txtDesc.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.txtDesc, "Text for journal description and journal entry description.");
			// 
			// txtDate
			// 
			this.txtDate.Location = new System.Drawing.Point(250, 90);
			this.txtDate.Mask = "##/##/####";
			this.txtDate.Name = "txtDate";
			this.txtDate.Size = new System.Drawing.Size(115, 40);
			this.txtDate.TabIndex = 3;
			this.txtDate.Text = "  /  /";
			this.ToolTip1.SetToolTip(this.txtDate, null);
			// 
			// Label2
			// 
			this.Label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(143)))), ((int)(((byte)(164)))));
			this.Label2.Location = new System.Drawing.Point(30, 104);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(183, 23);
			this.Label2.TabIndex = 2;
			this.Label2.Text = "DATE SAVED IN JOURNAL";
			this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.Label2, null);
			// 
			// Label1
			// 
			this.Label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(143)))), ((int)(((byte)(164)))));
			this.Label1.Location = new System.Drawing.Point(30, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(183, 23);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "JOURNAL DESCRIPTION";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.Label1, null);
			// 
			// MainMenu1
			// 
			//this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
			//this.mnuFile});
			//this.MainMenu1.Name = null;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = 0;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileSave,
				this.mnuFileSeperator,
				this.mnuFileExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuFileSave
			// 
			this.mnuFileSave.Index = 0;
			this.mnuFileSave.Name = "mnuFileSave";
			this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFileSave.Text = "Save";
			this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// mnuFileSeperator
			// 
			this.mnuFileSeperator.Index = 1;
			this.mnuFileSeperator.Name = "mnuFileSeperator";
			this.mnuFileSeperator.Text = "-";
			// 
			// mnuFileExit
			// 
			this.mnuFileExit.Index = 2;
			this.mnuFileExit.Name = "mnuFileExit";
			this.mnuFileExit.Text = "Save & Exit";
			this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(234, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(63, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save";
			this.ToolTip1.SetToolTip(this.cmdSave, null);
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// frmGetJournalDesc
			// 
			this.ClientSize = new System.Drawing.Size(530, 406);
			this.ControlBox = false;
			this.KeyPreview = true;
			//this.Menu = this.MainMenu1;
			this.Name = "frmGetJournalDesc";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Enter Journal Information";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmGetJournalDesc_Load);
			this.Activated += new System.EventHandler(this.frmGetJournalDesc_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmGetJournalDesc_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmGetJournalDesc_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkEntryDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
	}
}
