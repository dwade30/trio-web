﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmMOSESSetup.
	/// </summary>
	public partial class frmMOSESSetup : BaseForm
	{
		public frmMOSESSetup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmMOSESSetup InstancePtr
		{
			get
			{
				return (frmMOSESSetup)Sys.GetInstance(typeof(frmMOSESSetup));
			}
		}

		protected frmMOSESSetup _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               07/19/2006              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               01/02/2006              *
		// ********************************************************
		int lngColMOSESID;
		int lngColMOSESDesc;
		int lngColCRType;
		bool boolLoaded;
		string strTypeString = "";

		private void LoadGrid()
		{
			int intErr = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsLoad = new clsDRWrapper();
				intErr = 1;
				GRID.Rows = 1;
				rsLoad.OpenRecordset("SELECT * FROM MosesTypes ORDER BY MOSESType", modExtraModules.strCRDatabase);
				while (!rsLoad.EndOfFile())
				{
					//Application.DoEvents();
					GRID.AddItem("");
					GRID.TextMatrix(GRID.Rows - 1, lngColMOSESID, FCConvert.ToString(rsLoad.Get_Fields_Int32("MOSESType")));
					GRID.TextMatrix(GRID.Rows - 1, lngColMOSESDesc, FCConvert.ToString(rsLoad.Get_Fields_String("MOSESTypeDescription")));
					GRID.TextMatrix(GRID.Rows - 1, lngColCRType, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("CRType"))));
					rsLoad.MoveNext();
				}
				CreateTypeString();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Information - " + FCConvert.ToString(intErr), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SaveInfo()
		{
			int intErr = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// This will save all of the types information
				// anything left blank/zero will default to type 190
				clsDRWrapper rsSave = new clsDRWrapper();
				int lngCT;
				intErr = 1;
				rsSave.OpenRecordset("SELECT * FROM MOSESTypes", modExtraModules.strCRDatabase);
				for (lngCT = 1; lngCT <= GRID.Rows - 1; lngCT++)
				{
					//Application.DoEvents();
					rsSave.FindFirstRecord("MOSESType", GRID.TextMatrix(lngCT, lngColMOSESID));
					if (rsSave.NoMatch)
					{
						rsSave.AddNew();
					}
					else
					{
						rsSave.Edit();
					}
					rsSave.Set_Fields("CRType", FCConvert.ToString(Conversion.Val(GRID.TextMatrix(lngCT, lngColCRType))));
					rsSave.Update();
					rsSave.MoveNext();
				}
				intErr = 1000;
				SetMOSESTypeTitles();
				intErr = 1010;
				MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving Information - " + FCConvert.ToString(intErr), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Format_Grid(bool boolResize)
		{
			FormatGrid(ref boolResize);
		}

		private void FormatGrid(ref bool boolResize)
		{
			int lngW = 0;
			if (!boolResize)
			{
				GRID.Rows = 1;
			}
			GRID.Cols = 3;
			lngW = GRID.WidthOriginal;
			GRID.ExtendLastCol = true;
			GRID.ColWidth(lngColMOSESID, FCConvert.ToInt32(lngW * 0.18));
			GRID.ColWidth(lngColMOSESDesc, FCConvert.ToInt32(lngW * 0.63));
			GRID.ColWidth(lngColCRType, FCConvert.ToInt32(lngW * 0.15));
			GRID.ExtendLastCol = true;
			GRID.ColAlignment(lngColMOSESID, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GRID.TextMatrix(0, lngColMOSESID, "MOSES ID");
			GRID.TextMatrix(0, lngColMOSESDesc, "MOSES Description");
			GRID.TextMatrix(0, lngColCRType, "CR Type");
			GRID.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
		}

		private void frmMOSESSetup_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				LoadGrid();
				boolLoaded = true;
			}
		}

		private void frmMOSESSetup_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
		}

		private void frmMOSESSetup_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmMOSESSetup.FillStyle	= 0;
			//frmMOSESSetup.ScaleWidth	= 5880;
			//frmMOSESSetup.ScaleHeight	= 4260;
			//frmMOSESSetup.LinkTopic	= "Form2";
			//frmMOSESSetup.LockControls	= -1  'True;
			//frmMOSESSetup.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			this.Text = "MOSES Type Setup";
			lngColMOSESID = 0;
			lngColMOSESDesc = 1;
			lngColCRType = 2;
		}

		private void frmMOSESSetup_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			//if (MDIParent.InstancePtr.Visible && MDIParent.InstancePtr.Enabled)
			//{
			//    MDIParent.InstancePtr.Focus();
			//}
			boolLoaded = false;
		}

		private void frmMOSESSetup_Resize(object sender, System.EventArgs e)
		{
			Format_Grid(true);
		}

		private void GRID_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = GRID[e.ColumnIndex, e.RowIndex];
            int lngMR;
			int lngMC;
			lngMR = GRID.GetFlexRowIndex(e.RowIndex);
			lngMC = GRID.GetFlexColIndex(e.ColumnIndex);
			if (lngMR > 0 && lngMC > 0)
			{
				//ToolTip1.SetToolTip(GRID, GRID.TextMatrix(lngMR, lngMC));
				cell.ToolTipText =  GRID.TextMatrix(lngMR, lngMC);
			}
		}

		private void GRID_RowColChange(object sender, System.EventArgs e)
		{
			if (GRID.Col == lngColMOSESDesc || GRID.Col == lngColMOSESID)
			{
				GRID.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			else if (GRID.Col == lngColCRType)
			{
				GRID.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				GRID.ComboList = strTypeString;
			}
		}

		private void mnuFileContinue_Click(object sender, System.EventArgs e)
		{
			SaveInfo();
			Close();
		}

		private void mnuFileQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void CreateTypeString()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsType = new clsDRWrapper();
				string strTemp = "";
				rsType.OpenRecordset("SELECT TypeCode, TypeTitle FROM TYPE WHERE TypeTitle <> '' ORDER BY TypeCode", modExtraModules.strCRDatabase);
				strTemp = "|#0;000" + "\t" + "Default";
				while (!rsType.EndOfFile())
				{
					strTemp += "|";
					strTemp += "#" + rsType.Get_Fields_Int32("TypeCode") + ";" + rsType.Get_Fields_Int32("TypeCode") + "\t" + rsType.Get_Fields_String("TypeTitle");
					rsType.MoveNext();
				}
				strTypeString = strTemp;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Type String", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetMOSESTypeTitles()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsM = new clsDRWrapper();
				int lngCT;
				// This routine will set all of the CR types that are used for MOSES
				// to have the same fee titles and set the MOSES flag on each so that
				// they cannot be edited
				// rset all the types
				rsM.Execute("UPDATE Type SET MOSES = 0", "TWCR0000.vb1");
				// only set the ones cthat are associated with a MOSES type
				// so that they cannot be edited and the fees are correct
				for (lngCT = 1; lngCT <= GRID.Rows - 1; lngCT++)
				{
					if (Conversion.Val(GRID.TextMatrix(lngCT, lngColCRType)) != 0)
					{
						rsM.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(Conversion.Val(GRID.TextMatrix(lngCT, lngColCRType))), modExtraModules.strCRDatabase);
						if (!rsM.EndOfFile())
						{
							rsM.Edit();
							rsM.Set_Fields("Title1", "Excise Tax");
							// MAL@20071126: Change wording
							// Tracker Reference: 11101
							// rsM.Fields("Title2") = "State Paid"
							rsM.Set_Fields("Title2", "State Fee");
							rsM.Set_Fields("Title3", "Agent Fee");
							rsM.Set_Fields("Title4", "Sales Tax");
							rsM.Set_Fields("Title5", "");
							rsM.Set_Fields("Title6", "");
							rsM.Set_Fields("Title1Abbrev", "Excise");
							rsM.Set_Fields("Title2Abbrev", "State");
							rsM.Set_Fields("Title3Abbrev", "Agent");
							rsM.Set_Fields("Title4Abbrev", "Sales");
							rsM.Set_Fields("Title5Abbrev", "");
							rsM.Set_Fields("Title6Abbrev", "");
							rsM.Set_Fields("MOSES", true);
							rsM.Update();
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Setting MOSES Flag", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			SaveInfo();
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			SaveInfo();
		}
	}
}
