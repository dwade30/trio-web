﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmTellerID.
	/// </summary>
	partial class frmTellerID : BaseForm
	{
		public fecherFoundation.FCTextBox txtOpId;
		public fecherFoundation.FCLabel lblInstuctions;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.txtOpId = new fecherFoundation.FCTextBox();
            this.lblInstuctions = new fecherFoundation.FCLabel();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 100);
            this.BottomPanel.Size = new System.Drawing.Size(250, 0);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtOpId);
            this.ClientArea.Controls.Add(this.lblInstuctions);
            this.ClientArea.Location = new System.Drawing.Point(0, 0);
            this.ClientArea.Size = new System.Drawing.Size(250, 188);
            this.ClientArea.Controls.SetChildIndex(this.lblInstuctions, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtOpId, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(250, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(140, 30);
            this.HeaderText.Text = "Operator ID";
            // 
            // txtOpId
            // 
            this.txtOpId.BackColor = System.Drawing.SystemColors.Window;
            this.txtOpId.Location = new System.Drawing.Point(30, 60);
            this.txtOpId.MaxLength = 3;
            this.txtOpId.Name = "txtOpId";
            this.txtOpId.Size = new System.Drawing.Size(191, 40);
            this.txtOpId.TabIndex = 1;
            this.txtOpId.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtOpId.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtOpId_KeyPress);
            // 
            // lblInstuctions
            // 
            this.lblInstuctions.Location = new System.Drawing.Point(30, 30);
            this.lblInstuctions.Name = "lblInstuctions";
            this.lblInstuctions.Size = new System.Drawing.Size(191, 24);
            this.lblInstuctions.TabIndex = 1001;
            this.lblInstuctions.Text = "PLEASE INPUT YOUR MOTOR VEHICLE OPERATOR ID";
            this.lblInstuctions.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // frmTellerID
            // 
            this.ClientSize = new System.Drawing.Size(250, 128);
            this.ControlBox = false;
            this.KeyPreview = true;
            this.Name = "frmTellerID";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
            this.Text = "Operator ID";
            this.Load += new System.EventHandler(this.frmTellerID_Load);
            this.Activated += new System.EventHandler(this.frmTellerID_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmTellerID_KeyPress);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion
	}
}
