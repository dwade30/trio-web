﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmMultiTownSelect.
	/// </summary>
	partial class frmMultiTownSelect : BaseForm
	{
		public fecherFoundation.FCComboBox cmbResCode;
		public fecherFoundation.FCLabel lblResCode;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmbResCode = new fecherFoundation.FCComboBox();
			this.lblResCode = new fecherFoundation.FCLabel();
			this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileContinue = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileQuit = new fecherFoundation.FCToolStripMenuItem();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 189);
			this.BottomPanel.Size = new System.Drawing.Size(398, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbResCode);
			this.ClientArea.Controls.Add(this.lblResCode);
			this.ClientArea.Size = new System.Drawing.Size(398, 129);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(398, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(147, 30);
			this.HeaderText.Text = "Select Town";
			// 
			// cmbResCode
			// 
			this.cmbResCode.AutoSize = false;
			this.cmbResCode.BackColor = System.Drawing.SystemColors.Window;
			this.cmbResCode.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbResCode.FormattingEnabled = true;
			this.cmbResCode.Location = new System.Drawing.Point(143, 30);
			this.cmbResCode.Name = "cmbResCode";
			this.cmbResCode.Size = new System.Drawing.Size(123, 40);
			this.cmbResCode.Sorted = true;
			this.cmbResCode.TabIndex = 1;
			this.cmbResCode.SelectedIndexChanged += new System.EventHandler(this.cmbResCode_SelectedIndexChanged);
			// 
			// lblResCode
			// 
			this.lblResCode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(143)))), ((int)(((byte)(164)))));
			this.lblResCode.Location = new System.Drawing.Point(30, 44);
			this.lblResCode.Name = "lblResCode";
			this.lblResCode.Size = new System.Drawing.Size(97, 19);
			this.lblResCode.TabIndex = 0;
			this.lblResCode.Text = "TOWN CODE";
			this.lblResCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFile
			});
			// 
			// mnuFile
			// 
			this.mnuFile.Index = 0;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileContinue,
				this.Seperator,
				this.mnuFileQuit
			});
			this.mnuFile.Text = "File";
			// 
			// mnuFileContinue
			// 
			this.mnuFileContinue.Index = 0;
			this.mnuFileContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFileContinue.Text = "Save & Continue";
			this.mnuFileContinue.Click += new System.EventHandler(this.mnuFileContinue_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Text = "-";
			// 
			// mnuFileQuit
			// 
			this.mnuFileQuit.Index = 2;
			this.mnuFileQuit.Text = "Exit";
			this.mnuFileQuit.Click += new System.EventHandler(this.mnuFileQuit_Click);
			// 
			// frmMultiTownSelect
			// 
			this.ClientSize = new System.Drawing.Size(398, 297);
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Menu = this.MainMenu1;
			this.Name = "frmMultiTownSelect";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Select Town";
			this.Load += new System.EventHandler(this.frmMultiTownSelect_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmMultiTownSelect_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmMultiTownSelect_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
