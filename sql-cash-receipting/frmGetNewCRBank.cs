﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.Extensions;
using SharedApplication.Receipting;

namespace TWCR0000
{
    /// <summary>
    /// Summary description for frmTellerID.
    /// </summary>
    public partial class frmGetNewCRBank : BaseForm, IModalView<INewCRBankViewModel>
    {


        public frmGetNewCRBank()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
           
        }

        public frmGetNewCRBank(INewCRBankViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }


        private void InitializeComponentEx()
        {
            this.cmdSave.Click += CmdSave_Click;
            this.Load += FrmGetNewCRBank_Load;
            this.txtBankName.KeyPress += TxtBankName_KeyPress;
            this.txtRouting.KeyPress += TxtRouting_KeyPress;
        }

        private void TxtRouting_KeyPress(object sender, KeyPressEventArgs e)
        {
            Keys keyAscii = (Keys)Strings.Asc(e.KeyChar);
            if (keyAscii == Keys.Return || keyAscii == Keys.Enter)
            {
                Save();
            }
        }

        private void TxtBankName_KeyPress(object sender, KeyPressEventArgs e)
        {
            Keys keyAscii = (Keys)Strings.Asc(e.KeyChar);
            if (keyAscii == Keys.Return || keyAscii == Keys.Enter)
            {
                txtRouting.Focus();
            }
        }

        private void FrmGetNewCRBank_Load(object sender, EventArgs e)
        {

        }

        private void CmdSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void Save()
        {
            var bankName = txtBankName.Text;
            if (String.IsNullOrWhiteSpace(bankName))
            {
                MessageBox.Show("You didn't enter a bank name", "Invalid bank name", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return;
            }

            var routingNumber = txtRouting.Text;
            if (String.IsNullOrWhiteSpace(routingNumber))
            {
                MessageBox.Show("You didn't enter a routing number", "Invalid routing number", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return;
            }

            if (!ViewModel.RoutingNumberIsUnique(routingNumber))
            {
                MessageBox.Show("A bank with this routing number already exists", "Duplicate routing number",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            ViewModel.Name = txtBankName.Text;
            ViewModel.RoutingTransitnumber = txtRouting.Text;
            Close();
        }

        public INewCRBankViewModel ViewModel { get; set; }
        public void ShowModal()
        {
            this.Show(FormShowEnum.Modal);
        }
    }

}
