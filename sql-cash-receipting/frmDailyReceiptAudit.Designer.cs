﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;
using Wisej.Web;
using System.IO;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmDailyReceiptAudit.
	/// </summary>
	partial class frmDailyReceiptAudit : BaseForm
	{
		public fecherFoundation.FCComboBox cmbARAlign;
		public fecherFoundation.FCComboBox cmbAlignment;
		public fecherFoundation.FCComboBox cmbUTAlign;
		public fecherFoundation.FCComboBox cmbCLAlign;
		public fecherFoundation.FCComboBox cmbAudit;
		public fecherFoundation.FCLabel lblAudit;
		public fecherFoundation.FCComboBox cmbTellerType;
		public fecherFoundation.FCLabel lblTellerType;
		public System.Collections.Generic.List<fecherFoundation.FCButton> cmdPassword;
		public fecherFoundation.FCPanel fraDept;
		public fecherFoundation.FCTextBox txtDept;
		public fecherFoundation.FCLabel lblDept;
		public fecherFoundation.FCFrame fraPreAudit;
		public fecherFoundation.FCLabel lblARAudit;
		public fecherFoundation.FCLabel lblMainAudit;
		public fecherFoundation.FCLabel lblUTAudit;
		public fecherFoundation.FCLabel lblCLAudit;
		public fecherFoundation.FCCheckBox chkCloseOutMV;
		public fecherFoundation.FCComboBox cmbMonth;
		public fecherFoundation.FCComboBox cmbJournal;
		public fecherFoundation.FCLabel lblJournal;
		public fecherFoundation.FCLabel lblMonth;
		public fecherFoundation.FCButton cmdDone;
		public fecherFoundation.FCPanel fraTellerID;
		public fecherFoundation.FCTextBox txtTellerID;
		public fecherFoundation.FCLabel lblTellerID;
		public fecherFoundation.FCFrame fraAuditPassword;
		public fecherFoundation.FCButton cmdPassword_1;
		public fecherFoundation.FCTextBox txtAuditPassword;
		public fecherFoundation.FCButton cmdPassword_0;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDailyReceiptAudit));
			this.cmbARAlign = new fecherFoundation.FCComboBox();
			this.cmbAlignment = new fecherFoundation.FCComboBox();
			this.cmbUTAlign = new fecherFoundation.FCComboBox();
			this.cmbCLAlign = new fecherFoundation.FCComboBox();
			this.cmbAudit = new fecherFoundation.FCComboBox();
			this.lblAudit = new fecherFoundation.FCLabel();
			this.cmbTellerType = new fecherFoundation.FCComboBox();
			this.lblTellerType = new fecherFoundation.FCLabel();
			this.fraDept = new fecherFoundation.FCPanel();
			this.txtDept = new fecherFoundation.FCTextBox();
			this.lblDept = new fecherFoundation.FCLabel();
			this.fraTellerID = new fecherFoundation.FCPanel();
			this.txtTellerID = new fecherFoundation.FCTextBox();
			this.lblTellerID = new fecherFoundation.FCLabel();
			this.fraPreAudit = new fecherFoundation.FCFrame();
			this.lblARAudit = new fecherFoundation.FCLabel();
			this.lblUTAudit = new fecherFoundation.FCLabel();
			this.lblCLAudit = new fecherFoundation.FCLabel();
			this.lblMainAudit = new fecherFoundation.FCLabel();
			this.chkCloseOutMV = new fecherFoundation.FCCheckBox();
			this.cmbMonth = new fecherFoundation.FCComboBox();
			this.cmbJournal = new fecherFoundation.FCComboBox();
			this.lblJournal = new fecherFoundation.FCLabel();
			this.lblMonth = new fecherFoundation.FCLabel();
			this.cmdDone = new fecherFoundation.FCButton();
			this.cmdSave = new fecherFoundation.FCButton();
			this.fraAuditPassword = new fecherFoundation.FCFrame();
			this.cmdPassword_1 = new fecherFoundation.FCButton();
			this.txtAuditPassword = new fecherFoundation.FCTextBox();
			this.cmdPassword_0 = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraDept)).BeginInit();
			this.fraDept.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraTellerID)).BeginInit();
			this.fraTellerID.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraPreAudit)).BeginInit();
			this.fraPreAudit.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkCloseOutMV)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAuditPassword)).BeginInit();
			this.fraAuditPassword.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPassword_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPassword_0)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Controls.Add(this.cmdDone);
			this.BottomPanel.Location = new System.Drawing.Point(0, 586);
			this.BottomPanel.Size = new System.Drawing.Size(834, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraPreAudit);
			this.ClientArea.Controls.Add(this.fraAuditPassword);
			this.ClientArea.Size = new System.Drawing.Size(854, 616);
			this.ClientArea.Controls.SetChildIndex(this.fraAuditPassword, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraPreAudit, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(854, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(206, 28);
			this.HeaderText.Text = "Daily Receipt Audit";
			// 
			// cmbARAlign
			// 
			this.cmbARAlign.Items.AddRange(new object[] {
            "Portrait",
            "Landscape"});
			this.cmbARAlign.Location = new System.Drawing.Point(185, 496);
			this.cmbARAlign.Name = "cmbARAlign";
			this.cmbARAlign.Size = new System.Drawing.Size(283, 40);
			this.cmbARAlign.TabIndex = 16;
			// 
			// cmbAlignment
			// 
			this.cmbAlignment.Items.AddRange(new object[] {
            "Portrait",
            "Landscape"});
			this.cmbAlignment.Location = new System.Drawing.Point(185, 316);
			this.cmbAlignment.Name = "cmbAlignment";
			this.cmbAlignment.Size = new System.Drawing.Size(283, 40);
			this.cmbAlignment.TabIndex = 10;
			// 
			// cmbUTAlign
			// 
			this.cmbUTAlign.Items.AddRange(new object[] {
            "Portrait",
            "Landscape"});
			this.cmbUTAlign.Location = new System.Drawing.Point(185, 436);
			this.cmbUTAlign.Name = "cmbUTAlign";
			this.cmbUTAlign.Size = new System.Drawing.Size(283, 40);
			this.cmbUTAlign.TabIndex = 14;
			// 
			// cmbCLAlign
			// 
			this.cmbCLAlign.Items.AddRange(new object[] {
            "Portrait",
            "Landscape"});
			this.cmbCLAlign.Location = new System.Drawing.Point(185, 376);
			this.cmbCLAlign.Name = "cmbCLAlign";
			this.cmbCLAlign.Size = new System.Drawing.Size(283, 40);
			this.cmbCLAlign.TabIndex = 12;
			// 
			// cmbAudit
			// 
			this.cmbAudit.Location = new System.Drawing.Point(185, 30);
			this.cmbAudit.Name = "cmbAudit";
			this.cmbAudit.Size = new System.Drawing.Size(283, 40);
			this.cmbAudit.TabIndex = 1;
			this.cmbAudit.SelectedIndexChanged += new System.EventHandler(this.optAudit_CheckedChanged);
			// 
			// lblAudit
			// 
			this.lblAudit.Location = new System.Drawing.Point(20, 44);
			this.lblAudit.Name = "lblAudit";
			this.lblAudit.Size = new System.Drawing.Size(40, 16);
			this.lblAudit.TabIndex = 17;
			this.lblAudit.Text = "TYPE";
			// 
			// cmbTellerType
			// 
			this.cmbTellerType.Items.AddRange(new object[] {
            "Post Close Out",
            "Pre Close Out",
            "Both"});
			this.cmbTellerType.Location = new System.Drawing.Point(185, 90);
			this.cmbTellerType.Name = "cmbTellerType";
			this.cmbTellerType.Size = new System.Drawing.Size(283, 40);
			this.cmbTellerType.TabIndex = 3;
			// 
			// lblTellerType
			// 
			this.lblTellerType.Location = new System.Drawing.Point(20, 104);
			this.lblTellerType.Name = "lblTellerType";
			this.lblTellerType.Size = new System.Drawing.Size(78, 16);
			this.lblTellerType.TabIndex = 2;
			this.lblTellerType.Text = "TELLER TYPE";
			// 
			// fraDept
			// 
			this.fraDept.AppearanceKey = "groupBoxNoBorders";
			this.fraDept.Controls.Add(this.txtDept);
			this.fraDept.Controls.Add(this.lblDept);
			this.fraDept.Location = new System.Drawing.Point(493, 17);
			this.fraDept.Name = "fraDept";
			this.fraDept.Size = new System.Drawing.Size(248, 73);
			this.fraDept.TabIndex = 3;
			this.fraDept.Visible = false;
			// 
			// txtDept
			// 
			this.txtDept.BackColor = System.Drawing.SystemColors.Window;
			this.txtDept.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtDept.Cursor = Wisej.Web.Cursors.Default;
			this.txtDept.Enabled = false;
			this.txtDept.Location = new System.Drawing.Point(121, 13);
			this.txtDept.MaxLength = 3;
			this.txtDept.Name = "txtDept";
			this.txtDept.Size = new System.Drawing.Size(107, 40);
			this.txtDept.TabIndex = 1;
			this.txtDept.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtDept.Enter += new System.EventHandler(this.txtDept_Enter);
			this.txtDept.Validating += new System.ComponentModel.CancelEventHandler(this.txtDept_Validating);
			this.txtDept.KeyDown += new Wisej.Web.KeyEventHandler(this.txtDept_KeyDown);
			this.txtDept.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtDept_KeyPress);
			// 
			// lblDept
			// 
			this.lblDept.Enabled = false;
			this.lblDept.Location = new System.Drawing.Point(20, 27);
			this.lblDept.Name = "lblDept";
			this.lblDept.Size = new System.Drawing.Size(81, 16);
			this.lblDept.TabIndex = 2;
			this.lblDept.Text = "DEPARTMENT";
			// 
			// fraTellerID
			// 
			this.fraTellerID.AppearanceKey = "groupBoxNoBorders";
			this.fraTellerID.Controls.Add(this.txtTellerID);
			this.fraTellerID.Controls.Add(this.lblTellerID);
			this.fraTellerID.Location = new System.Drawing.Point(493, 17);
			this.fraTellerID.Name = "fraTellerID";
			this.fraTellerID.Size = new System.Drawing.Size(248, 90);
			this.fraTellerID.TabIndex = 2;
			this.fraTellerID.Visible = false;
			// 
			// txtTellerID
			// 
			this.txtTellerID.BackColor = System.Drawing.SystemColors.Window;
			this.txtTellerID.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtTellerID.Cursor = Wisej.Web.Cursors.Default;
			this.txtTellerID.Enabled = false;
			this.txtTellerID.Location = new System.Drawing.Point(107, 13);
			this.txtTellerID.MaxLength = 3;
			this.txtTellerID.Name = "txtTellerID";
			this.txtTellerID.Size = new System.Drawing.Size(108, 40);
			this.txtTellerID.TabIndex = 1;
			this.txtTellerID.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtTellerID.Enter += new System.EventHandler(this.txtTellerID_Enter);
			this.txtTellerID.KeyDown += new Wisej.Web.KeyEventHandler(this.txtTellerID_KeyDown);
			this.txtTellerID.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtTellerID_KeyPress);
			// 
			// lblTellerID
			// 
			this.lblTellerID.Enabled = false;
			this.lblTellerID.Location = new System.Drawing.Point(20, 27);
			this.lblTellerID.Name = "lblTellerID";
			this.lblTellerID.Size = new System.Drawing.Size(60, 16);
			this.lblTellerID.TabIndex = 2;
			this.lblTellerID.Text = "TELLER ID";
			// 
			// fraPreAudit
			// 
			this.fraPreAudit.Controls.Add(this.fraDept);
			this.fraPreAudit.Controls.Add(this.fraTellerID);
			this.fraPreAudit.Controls.Add(this.lblARAudit);
			this.fraPreAudit.Controls.Add(this.lblUTAudit);
			this.fraPreAudit.Controls.Add(this.cmbARAlign);
			this.fraPreAudit.Controls.Add(this.lblCLAudit);
			this.fraPreAudit.Controls.Add(this.cmbUTAlign);
			this.fraPreAudit.Controls.Add(this.lblMainAudit);
			this.fraPreAudit.Controls.Add(this.cmbCLAlign);
			this.fraPreAudit.Controls.Add(this.cmbAlignment);
			this.fraPreAudit.Controls.Add(this.chkCloseOutMV);
			this.fraPreAudit.Controls.Add(this.cmbTellerType);
			this.fraPreAudit.Controls.Add(this.cmbMonth);
			this.fraPreAudit.Controls.Add(this.lblTellerType);
			this.fraPreAudit.Controls.Add(this.cmbJournal);
			this.fraPreAudit.Controls.Add(this.lblJournal);
			this.fraPreAudit.Controls.Add(this.lblMonth);
			this.fraPreAudit.Controls.Add(this.cmbAudit);
			this.fraPreAudit.Controls.Add(this.lblAudit);
			this.fraPreAudit.Location = new System.Drawing.Point(30, 30);
			this.fraPreAudit.Name = "fraPreAudit";
			this.fraPreAudit.Size = new System.Drawing.Size(759, 556);
			this.fraPreAudit.TabIndex = 1001;
			this.fraPreAudit.Text = "Select Audit Process";
			this.fraPreAudit.Visible = false;
			// 
			// lblARAudit
			// 
			this.lblARAudit.Location = new System.Drawing.Point(20, 510);
			this.lblARAudit.Name = "lblARAudit";
			this.lblARAudit.Size = new System.Drawing.Size(108, 16);
			this.lblARAudit.TabIndex = 15;
			this.lblARAudit.Text = "ACCTS RCV AUDIT";
			// 
			// lblUTAudit
			// 
			this.lblUTAudit.Location = new System.Drawing.Point(20, 450);
			this.lblUTAudit.Name = "lblUTAudit";
			this.lblUTAudit.Size = new System.Drawing.Size(93, 16);
			this.lblUTAudit.TabIndex = 13;
			this.lblUTAudit.Text = "UTILITIES AUDIT";
			// 
			// lblCLAudit
			// 
			this.lblCLAudit.Location = new System.Drawing.Point(20, 390);
			this.lblCLAudit.Name = "lblCLAudit";
			this.lblCLAudit.Size = new System.Drawing.Size(122, 16);
			this.lblCLAudit.TabIndex = 11;
			this.lblCLAudit.Text = "COLLECTIONS AUDIT";
			// 
			// lblMainAudit
			// 
			this.lblMainAudit.Location = new System.Drawing.Point(20, 330);
			this.lblMainAudit.Name = "lblMainAudit";
			this.lblMainAudit.Size = new System.Drawing.Size(73, 16);
			this.lblMainAudit.TabIndex = 9;
			this.lblMainAudit.Text = "MAIN AUDIT";
			// 
			// chkCloseOutMV
			// 
			this.chkCloseOutMV.Location = new System.Drawing.Point(20, 270);
			this.chkCloseOutMV.Name = "chkCloseOutMV";
			this.chkCloseOutMV.Size = new System.Drawing.Size(172, 22);
			this.chkCloseOutMV.TabIndex = 8;
			this.chkCloseOutMV.Text = "Close Out Motor Vehicle";
			// 
			// cmbMonth
			// 
			this.cmbMonth.BackColor = System.Drawing.SystemColors.Window;
			this.cmbMonth.Enabled = false;
			this.cmbMonth.Location = new System.Drawing.Point(185, 210);
			this.cmbMonth.Name = "cmbMonth";
			this.cmbMonth.Size = new System.Drawing.Size(283, 40);
			this.cmbMonth.Sorted = true;
			this.cmbMonth.TabIndex = 7;
			this.cmbMonth.DropDown += new System.EventHandler(this.cmbMonth_DropDown);
			this.cmbMonth.Validating += new System.ComponentModel.CancelEventHandler(this.cmbMonth_Validating);
			this.cmbMonth.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbMonth_KeyDown);
			// 
			// cmbJournal
			// 
			this.cmbJournal.BackColor = System.Drawing.SystemColors.Window;
			this.cmbJournal.Enabled = false;
			this.cmbJournal.Location = new System.Drawing.Point(185, 150);
			this.cmbJournal.Name = "cmbJournal";
			this.cmbJournal.Size = new System.Drawing.Size(283, 40);
			this.cmbJournal.TabIndex = 5;
			this.cmbJournal.DropDown += new System.EventHandler(this.cmbJournal_DropDown);
			this.cmbJournal.Validating += new System.ComponentModel.CancelEventHandler(this.cmbJournal_Validating);
			this.cmbJournal.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbJournal_KeyDown);
			// 
			// lblJournal
			// 
			this.lblJournal.Enabled = false;
			this.lblJournal.Location = new System.Drawing.Point(20, 164);
			this.lblJournal.Name = "lblJournal";
			this.lblJournal.Size = new System.Drawing.Size(110, 16);
			this.lblJournal.TabIndex = 4;
			this.lblJournal.Text = "JOURNAL NUMBER";
			// 
			// lblMonth
			// 
			this.lblMonth.Enabled = false;
			this.lblMonth.Location = new System.Drawing.Point(20, 224);
			this.lblMonth.Name = "lblMonth";
			this.lblMonth.Size = new System.Drawing.Size(130, 16);
			this.lblMonth.TabIndex = 6;
			this.lblMonth.Text = "ACCOUNTING MONTH";
			// 
			// cmdDone
			// 
			this.cmdDone.AppearanceKey = "acceptButton";
			this.cmdDone.Cursor = Wisej.Web.Cursors.Default;
			this.cmdDone.Location = new System.Drawing.Point(504, 30);
			this.cmdDone.Name = "cmdDone";
			this.cmdDone.Size = new System.Drawing.Size(78, 48);
			this.cmdDone.TabIndex = 10;
			this.cmdDone.Text = "Process";
			this.cmdDone.Visible = false;
			this.cmdDone.Click += new System.EventHandler(this.cmdDone_Click);
			this.cmdDone.KeyDown += new Wisej.Web.KeyEventHandler(this.cmdDone_KeyDown);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Cursor = Wisej.Web.Cursors.Default;
			this.cmdSave.Location = new System.Drawing.Point(359, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(137, 48);
			this.cmdSave.TabIndex = 1;
			this.cmdSave.Text = "Save & Preview";
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// fraAuditPassword
			// 
			this.fraAuditPassword.Controls.Add(this.cmdPassword_1);
			this.fraAuditPassword.Controls.Add(this.txtAuditPassword);
			this.fraAuditPassword.Controls.Add(this.cmdPassword_0);
			this.fraAuditPassword.Location = new System.Drawing.Point(30, 30);
			this.fraAuditPassword.Name = "fraAuditPassword";
			this.fraAuditPassword.Size = new System.Drawing.Size(228, 137);
			this.fraAuditPassword.TabIndex = 1;
			this.fraAuditPassword.Text = "Enter Audit Password";
			this.fraAuditPassword.Visible = false;
			// 
			// cmdPassword_1
			// 
			this.cmdPassword_1.Cursor = Wisej.Web.Cursors.Default;
			this.cmdPassword_1.Location = new System.Drawing.Point(130, 90);
			this.cmdPassword_1.Name = "cmdPassword_1";
			this.cmdPassword_1.Size = new System.Drawing.Size(78, 27);
			this.cmdPassword_1.TabIndex = 2;
			this.cmdPassword_1.Text = "Cancel";
			this.cmdPassword_1.Visible = false;
			this.cmdPassword_1.Click += new System.EventHandler(this.cmdPassword_Click);
			// 
			// txtAuditPassword
			// 
			this.txtAuditPassword.BackColor = System.Drawing.SystemColors.Window;
			this.txtAuditPassword.Cursor = Wisej.Web.Cursors.Default;
			this.txtAuditPassword.InputType.Type = Wisej.Web.TextBoxType.Password;
			this.txtAuditPassword.Location = new System.Drawing.Point(20, 30);
			this.txtAuditPassword.Name = "txtAuditPassword";
			this.txtAuditPassword.PasswordChar = '*';
			this.txtAuditPassword.Size = new System.Drawing.Size(188, 40);
			this.txtAuditPassword.TabIndex = 3;
			this.txtAuditPassword.KeyDown += new Wisej.Web.KeyEventHandler(this.txtAuditPassword_KeyDown);
			this.txtAuditPassword.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtAuditPassword_KeyPress);
			// 
			// cmdPassword_0
			// 
			this.cmdPassword_0.Cursor = Wisej.Web.Cursors.Default;
			this.cmdPassword_0.Location = new System.Drawing.Point(20, 90);
			this.cmdPassword_0.Name = "cmdPassword_0";
			this.cmdPassword_0.Size = new System.Drawing.Size(78, 27);
			this.cmdPassword_0.TabIndex = 1;
			this.cmdPassword_0.Text = "OK";
			this.cmdPassword_0.Visible = false;
			this.cmdPassword_0.Click += new System.EventHandler(this.cmdPassword_Click);
			// 
			// frmDailyReceiptAudit
			// 
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(854, 676);
			this.Cursor = Wisej.Web.Cursors.Default;
			this.KeyPreview = true;
			this.Name = "frmDailyReceiptAudit";
			this.Text = "Daily Receipt Audit";
			this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.Load += new System.EventHandler(this.frmDailyReceiptAudit_Load);
			this.Activated += new System.EventHandler(this.frmDailyReceiptAudit_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmDailyReceiptAudit_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraDept)).EndInit();
			this.fraDept.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraTellerID)).EndInit();
			this.fraTellerID.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraPreAudit)).EndInit();
			this.fraPreAudit.ResumeLayout(false);
			this.fraPreAudit.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkCloseOutMV)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAuditPassword)).EndInit();
			this.fraAuditPassword.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdPassword_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPassword_0)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
	}
}
