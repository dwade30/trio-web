﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for rptPaymentCrossCheckUT.
	/// </summary>
	public partial class rptPaymentCrossCheckUT : BaseSectionReport
	{
		public static rptPaymentCrossCheckUT InstancePtr
		{
			get
			{
				return (rptPaymentCrossCheckUT)Sys.GetInstance(typeof(rptPaymentCrossCheckUT));
			}
		}

		protected rptPaymentCrossCheckUT _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPaymentCrossCheckUT	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               06/08/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               04/11/2006              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		int lngCount;
		double dblTotalAmt;
		int[] arrMissingPayments = null;
		bool boolDone;

		public rptPaymentCrossCheckUT()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "UT Payment Cross Check";
            this.ReportEnd += RptPaymentCrossCheckUT_ReportEnd;
		}

        private void RptPaymentCrossCheckUT_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = boolDone;
		}

		private void ActiveReport_Initialize()
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
		}
		//private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		//{
		//	switch (KeyCode)
		//	{
		//		case Keys.Escape:
		//			{
		//				Close();
		//				break;
		//			}
		//	} //end switch
		//}
		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			// MAL@20071012
			rptPaymentCrossCheck.InstancePtr.lblHeader2.Text = "Receipts appearing on this report do not have corresponding entries in the Tax Collections databases";
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			CreateArrayElements();
			if (lngCount == 0)
			{
				MessageBox.Show("No UT records to report.", "No UT Missing Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Close();
			}
			else
			{
				lngCount = 0;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void BindFields()
		{
			fldReceiptNumber.Text = "";
			fldDate.Text = "";
			fldName.Text = "";
			fldTeller.Text = "";
			fldPrin.Text = "";
			fldCurInt.Text = "";
			fldCost.Text = "";
			fldPLI.Text = "";
			fldType.Text = "";
			fldTax.Text = "";
			fldAccount.Text = "";
			if (lngCount <= Information.UBound(arrMissingPayments, 1))
			{
				rsData.FindFirstRecord("ID", arrMissingPayments[lngCount]);
				if (!rsData.NoMatch)
				{
					switch (FCConvert.ToInt32(rsData.Get_Fields_Int32("ReceiptType")))
					{
						case 93:
							{
								// TODO Get_Fields: Check the table for the column [Amount1] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Amount3] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Amount6] and replace with corresponding Get_Field method
								fldPrin.Text = Strings.Format(rsData.Get_Fields("Amount1") + rsData.Get_Fields("Amount3") + rsData.Get_Fields("Amount6"), "#,##0.00");
								// TODO Get_Fields: Check the table for the column [Amount2] and replace with corresponding Get_Field method
								fldCurInt.Text = Strings.Format(rsData.Get_Fields("Amount2"), "#,##0.00");
								// TODO Get_Fields: Check the table for the column [Amount4] and replace with corresponding Get_Field method
								fldCost.Text = Strings.Format(rsData.Get_Fields("Amount4"), "#,##0.00");
								fldPLI.Text = "0.00";
								// TODO Get_Fields: Check the table for the column [Amount5] and replace with corresponding Get_Field method
								fldTax.Text = Strings.Format(rsData.Get_Fields("Amount5"), "#,##0.00");
								fldType.Text = "WR";
								break;
							}
						case 94:
							{
								// TODO Get_Fields: Check the table for the column [Amount1] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Amount3] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Amount6] and replace with corresponding Get_Field method
								fldPrin.Text = Strings.Format(rsData.Get_Fields("Amount1") + rsData.Get_Fields("Amount3") + rsData.Get_Fields("Amount6"), "#,##0.00");
								// TODO Get_Fields: Check the table for the column [Amount2] and replace with corresponding Get_Field method
								fldCurInt.Text = Strings.Format(rsData.Get_Fields("Amount2"), "#,##0.00");
								// TODO Get_Fields: Check the table for the column [Amount4] and replace with corresponding Get_Field method
								fldCost.Text = Strings.Format(rsData.Get_Fields("Amount4"), "#,##0.00");
								fldPLI.Text = "0.00";
								// TODO Get_Fields: Check the table for the column [Amount5] and replace with corresponding Get_Field method
								fldTax.Text = Strings.Format(rsData.Get_Fields("Amount5"), "#,##0.00");
								fldType.Text = "SR";
								break;
							}
						case 95:
							{
								// TODO Get_Fields: Check the table for the column [Amount1] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Amount6] and replace with corresponding Get_Field method
								fldPrin.Text = Strings.Format(rsData.Get_Fields("Amount1") + rsData.Get_Fields("Amount6"), "#,##0.00");
								// TODO Get_Fields: Check the table for the column [Amount2] and replace with corresponding Get_Field method
								fldCurInt.Text = Strings.Format(rsData.Get_Fields("Amount2"), "#,##0.00");
								// TODO Get_Fields: Check the table for the column [Amount4] and replace with corresponding Get_Field method
								fldCost.Text = Strings.Format(rsData.Get_Fields("Amount4"), "#,##0.00");
								// TODO Get_Fields: Check the table for the column [Amount3] and replace with corresponding Get_Field method
								fldPLI.Text = Strings.Format(rsData.Get_Fields("Amount3"), "#,##0.00");
								// TODO Get_Fields: Check the table for the column [Amount5] and replace with corresponding Get_Field method
								fldTax.Text = Strings.Format(rsData.Get_Fields("Amount5"), "#,##0.00");
								fldType.Text = "SL";
								break;
							}
						case 96:
							{
								// TODO Get_Fields: Check the table for the column [Amount1] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Amount6] and replace with corresponding Get_Field method
								fldPrin.Text = Strings.Format(rsData.Get_Fields("Amount1") + rsData.Get_Fields("Amount6"), "#,##0.00");
								// TODO Get_Fields: Check the table for the column [Amount2] and replace with corresponding Get_Field method
								fldCurInt.Text = Strings.Format(rsData.Get_Fields("Amount2"), "#,##0.00");
								// TODO Get_Fields: Check the table for the column [Amount3] and replace with corresponding Get_Field method
								fldPLI.Text = Strings.Format(rsData.Get_Fields("Amount3"), "#,##0.00");
								// TODO Get_Fields: Check the table for the column [Amount4] and replace with corresponding Get_Field method
								fldCost.Text = Strings.Format(rsData.Get_Fields("Amount4"), "#,##0.00");
								// TODO Get_Fields: Check the table for the column [Amount5] and replace with corresponding Get_Field method
								fldTax.Text = Strings.Format(rsData.Get_Fields("Amount5"), "#,##0.00");
								fldType.Text = "WL";
								break;
							}
					}
					//end switch
					fldReceiptNumber.Text = GetActualReceiptNumber_2(FCConvert.ToInt32(rsData.Get_Fields_Int32("ReceiptNumber")));
					fldDate.Text = Strings.Format(rsData.Get_Fields_DateTime("ActualSystemDate"), "MM/dd/yyyy");
					fldName.Text = rsData.Get_Fields_String("Name");
					fldTeller.Text = rsData.Get_Fields_String("TellerID");
					fldAccount.Text = Strings.Left(FCConvert.ToString(rsData.Get_Fields_String("Ref")), Strings.InStr(1, FCConvert.ToString(rsData.Get_Fields_String("Ref")), "-", CompareConstants.vbBinaryCompare));
				}
				else
				{
					ClearBoxes();
				}
				lngCount += 1;
			}
			else
			{
				boolDone = true;
			}
		}

		private void CreateArrayElements()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsCL = new clsDRWrapper();
				string strSQL;
				string strBillNumber = "";
				int lngFM = 0;
				int lngYR = 0;
				// vbPorter upgrade warning: strCCDate As DateTime	OnWrite(string)
				DateTime strCCDate = DateTime.FromOADate(0);
				int lngType = 0;
				// vbPorter upgrade warning: strLastEOY As string	OnWrite(DateTime)	OnRead(DateTime)
				string strLastEOY;
				if (modGlobalConstants.Statics.gboolBD)
				{
					rsCL.OpenRecordset("SELECT FiscalStart FROM Budgetary", modExtraModules.strBDDatabase);
					if (!rsCL.EndOfFile())
					{
						lngFM = FCConvert.ToInt32(rsCL.Get_Fields_String("FiscalStart"));
					}
					else
					{
						lngFM = 1;
					}
				}
				else
				{
					lngFM = 1;
				}
				// frmWait.Init "Please Wait..." & vbCrLf & "Loading Receipts"
				if (DateTime.Today.ToOADate() > DateAndTime.DateValue(FCConvert.ToString(lngFM) + "/01/" + FCConvert.ToString(DateTime.Today.Year)).ToOADate() && lngFM <= DateTime.Today.Month)
				{
					lngYR = DateTime.Today.Year;
				}
				else
				{
					lngYR = DateTime.Today.Year - 1;
				}
				strLastEOY = FCConvert.ToString(DateAndTime.DateValue(FCConvert.ToString(lngFM) + "/01/" + FCConvert.ToString(lngYR)));
				rsCL.OpenRecordset("SELECT * FROM CashRec");
				if (!rsCL.EndOfFile())
				{
					lngType = FCConvert.ToInt32(Math.Round(Conversion.Val(rsCL.Get_Fields_Int32("CrossCheckDefault"))));
				}
				switch (lngType)
				{
					case 0:
						{
							// last EOY
							strCCDate = FCConvert.ToDateTime(strLastEOY);
							break;
						}
					case 1:
						{
							// last cross check report
							if (DateAndTime.DateDiff("D", (DateTime)rsCL.Get_Fields_DateTime("LastCrossCheck"), DateAndTime.DateValue(strLastEOY)) > 0)
							{
								strCCDate = FCConvert.ToDateTime(strLastEOY);
							}
							else
							{
								strCCDate = FCConvert.ToDateTime(Strings.Format(rsCL.Get_Fields_DateTime("LastCrossCheck"), "MM/dd/yyyy"));
							}
							break;
						}
					case 2:
						{
							// specific date
							if (DateAndTime.DateDiff("D", (DateTime)rsCL.Get_Fields_DateTime("CrossCheckDefaultDate"), DateAndTime.DateValue(strLastEOY)) > 0)
							{
								strCCDate = FCConvert.ToDateTime(strLastEOY);
							}
							else
							{
								strCCDate = FCConvert.ToDateTime(Strings.Format(rsCL.Get_Fields_DateTime("CrossCheckDefaultDate"), "MM/dd/yyyy"));
							}
							break;
						}
				}
				//end switch
				// strSQL = "SELECT * FROM Archive WHERE (ReceiptType = 90 OR ReceiptType = 91) AND Date > #" & CDate(lngFM & "/01/" & lngYR) & "# ORDER BY ReceiptNumber"
				//Application.DoEvents();
				// frmWait.Init "Please Wait..." & vbCrLf & "Loading Receipts"
				strSQL = "SELECT * FROM Archive WHERE (ReceiptType = 93 OR ReceiptType = 94 OR ReceiptType = 95 OR ReceiptType = 96) AND ArchiveDate > '" + FCConvert.ToString(strCCDate) + "' AND ArchiveDate > '02/01/2006' ORDER BY ReceiptNumber";
				lngCount = 0;
				rsData.OpenRecordset(strSQL, modExtraModules.strCRDatabase);
				// frmWait.Init "Please Wait..." & vbCrLf & "Checking UT Receipts", True, rsData.RecordCount, True
				while (!rsData.EndOfFile())
				{
					// frmWait.IncrementProgress
					strBillNumber = Strings.Right(Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Ref"))), Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Ref"))).Length - Strings.InStr(1, Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Ref"))), "-", CompareConstants.vbBinaryCompare));
					if (Conversion.Val(strBillNumber) != 0 && Strings.InStr(1, Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Ref"))), "-", CompareConstants.vbBinaryCompare) > 0)
					{
						// check to make sure that there is a value in the reference...otherwise skip this archive record because it probably is a conversion record
						// If rsData.Fields("AccountNumber") = 720 Then
						// frmWait.Top = -2000
						// MsgBox "stop"
						// End If
						switch (FCConvert.ToInt32(rsData.Get_Fields_Int32("ReceiptType")))
						{
							case 93:
								{
									// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Amount1] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Amount3] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Amount6] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Amount2] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Amount4] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Amount5] and replace with corresponding Get_Field method
									rsCL.OpenRecordset("SELECT * FROM PaymentRec WHERE AccountKey = " + FCConvert.ToString(modUTFunctions.GetAccountKeyUT_2(FCConvert.ToInt32(rsData.Get_Fields("AccountNumber")))) + " AND Service = 'W' AND Lien = 0 AND ReceiptNumber = " + rsData.Get_Fields_Int32("ReceiptNumber") + " AND Code = '" + rsData.Get_Fields_String("Control2") + "' AND Month(ActualSystemDate) = " + Strings.Format(rsData.Get_Fields_DateTime("ActualSystemDate"), "MM") + " AND Day(ActualSystemDate) = " + Strings.Format(rsData.Get_Fields_DateTime("ActualSystemDate"), "dd") + " AND Principal = " + FCConvert.ToString(rsData.Get_Fields("Amount1") + rsData.Get_Fields("Amount3") + rsData.Get_Fields("Amount6")) + " AND CurrentInterest = " + rsData.Get_Fields("Amount2") + " AND LienCost = " + rsData.Get_Fields("Amount4") + " AND Tax = " + rsData.Get_Fields("Amount5") + " AND BillNumber = " + strBillNumber, modExtraModules.strUTDatabase);
									break;
								}
							case 94:
								{
									// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Amount1] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Amount3] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Amount6] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Amount2] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Amount4] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Amount5] and replace with corresponding Get_Field method
									rsCL.OpenRecordset("SELECT * FROM PaymentRec WHERE AccountKey = " + FCConvert.ToString(modUTFunctions.GetAccountKeyUT_2(FCConvert.ToInt32(rsData.Get_Fields("AccountNumber")))) + " AND Service = 'S' AND Lien = 0 AND ReceiptNumber = " + rsData.Get_Fields_Int32("ReceiptNumber") + " AND Code = '" + rsData.Get_Fields_String("Control2") + "' AND Month(ActualSystemDate) = " + Strings.Format(rsData.Get_Fields_DateTime("ActualSystemDate"), "MM") + " AND Day(ActualSystemDate) = " + Strings.Format(rsData.Get_Fields_DateTime("ActualSystemDate"), "dd") + " AND Principal = " + FCConvert.ToString(rsData.Get_Fields("Amount1") + rsData.Get_Fields("Amount3") + rsData.Get_Fields("Amount6")) + " AND CurrentInterest = " + rsData.Get_Fields("Amount2") + " AND LienCost = " + rsData.Get_Fields("Amount4") + " AND Tax = " + rsData.Get_Fields("Amount5") + " AND BillNumber = " + strBillNumber, modExtraModules.strUTDatabase);
									break;
								}
							case 95:
								{
									// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Amount1] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Amount6] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Amount2] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Amount3] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Amount4] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Amount5] and replace with corresponding Get_Field method
									rsCL.OpenRecordset("SELECT * FROM PaymentRec WHERE AccountKey = " + FCConvert.ToString(modUTFunctions.GetAccountKeyUT_2(FCConvert.ToInt32(rsData.Get_Fields("AccountNumber")))) + " AND Service = 'S' AND Lien = 1 AND ReceiptNumber = " + rsData.Get_Fields_Int32("ReceiptNumber") + " AND Code = '" + rsData.Get_Fields_String("Control2") + "' AND Month(ActualSystemDate) = " + Strings.Format(rsData.Get_Fields_DateTime("ActualSystemDate"), "MM") + " AND Day(ActualSystemDate) = " + Strings.Format(rsData.Get_Fields_DateTime("ActualSystemDate"), "dd") + " AND Principal = " + FCConvert.ToString(rsData.Get_Fields("Amount1") + rsData.Get_Fields("Amount6")) + " AND CurrentInterest = " + rsData.Get_Fields("Amount2") + " AND PreLienInterest = " + rsData.Get_Fields("Amount3") + " AND LienCost = " + rsData.Get_Fields("Amount4") + " AND Tax = " + rsData.Get_Fields("Amount5") + " AND BillNumber = " + strBillNumber, modExtraModules.strUTDatabase);
									break;
								}
							case 96:
								{
									// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Amount1] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Amount6] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Amount2] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Amount3] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Amount4] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Amount5] and replace with corresponding Get_Field method
									rsCL.OpenRecordset("SELECT * FROM PaymentRec WHERE AccountKey = " + FCConvert.ToString(modUTFunctions.GetAccountKeyUT_2(FCConvert.ToInt32(rsData.Get_Fields("AccountNumber")))) + " AND Service = 'W' AND Lien = 1 AND ReceiptNumber = " + rsData.Get_Fields_Int32("ReceiptNumber") + " AND Code = '" + rsData.Get_Fields_String("Control2") + "' AND Month(ActualSystemDate) = " + Strings.Format(rsData.Get_Fields_DateTime("ActualSystemDate"), "MM") + " AND Day(ActualSystemDate) = " + Strings.Format(rsData.Get_Fields_DateTime("ActualSystemDate"), "dd") + " AND Principal = " + FCConvert.ToString(rsData.Get_Fields("Amount1") + rsData.Get_Fields("Amount6")) + " AND CurrentInterest = " + rsData.Get_Fields("Amount2") + " AND PreLienInterest = " + rsData.Get_Fields("Amount3") + " AND LienCost = " + rsData.Get_Fields("Amount4") + " AND Tax = " + rsData.Get_Fields("Amount5") + " AND BillNumber = " + strBillNumber, modExtraModules.strUTDatabase);
									break;
								}
						}
						//end switch
						if (rsCL.EndOfFile())
						{
							Array.Resize(ref arrMissingPayments, lngCount + 1);
							arrMissingPayments[lngCount] = FCConvert.ToInt32(rsData.Get_Fields_Int32("ID"));
							lngCount += 1;
						}
					}
					rsData.MoveNext();
				}
				frmWait.InstancePtr.Unload();
				rsCL.DisposeOf();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Array Elements", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ClearBoxes()
		{
			fldCost.Text = "";
			fldDate.Text = "";
			fldCurInt.Text = "";
			fldName.Text = "";
			fldPLI.Text = "";
			fldPrin.Text = "";
			fldReceiptNumber.Text = "";
			fldTeller.Text = "";
			fldType.Text = "";
			fldTax.Text = "";
		}
		// vbPorter upgrade warning: 'Return' As string	OnWrite(string, int)
		private string GetActualReceiptNumber_2(int lngRN, DateTime? dtDate = null)
		{
			return GetActualReceiptNumber(ref lngRN, dtDate);
		}

		private string GetActualReceiptNumber(ref int lngRN, DateTime? dtDateTemp = null)
		{
			DateTime dtDate = dtDateTemp ?? DateTime.Now;
			string GetActualReceiptNumber = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will accept a long of a receipt number ID in the CR table
				// if the receipt does not exist or any other error occurs, the function will
				// return 0, otherwise it returns the receipt number shown on the receipt
				clsDRWrapper rsRN = new clsDRWrapper();
				if (lngRN > 0)
				{
					if (modGlobalConstants.Statics.gboolCR)
					{
						rsRN.OpenRecordset("SELECT * FROM Receipt WHERE ReceiptKey = " + FCConvert.ToString(lngRN), modExtraModules.strCRDatabase);
						if (rsRN.EndOfFile() != true && rsRN.BeginningOfFile() != true)
						{
							GetActualReceiptNumber = FCConvert.ToString(rsRN.Get_Fields_Int32("ReceiptNumber"));
						}
						else
						{
							rsRN.OpenRecordset("SELECT * FROM LastYearReceipt WHERE ReceiptKey = " + FCConvert.ToString(lngRN) + " AND LastYearReceiptDate > '" + Strings.Format(dtDate, "MM/dd/yyyy") + " 12:00:00 AM' AND LastYearReceiptDate < '" + Strings.Format(dtDate, "MM/dd/yyyy") + " 11:59:59 PM'", modExtraModules.strCRDatabase);
							if (rsRN.EndOfFile() != true && rsRN.BeginningOfFile() != true)
							{
								GetActualReceiptNumber = FCConvert.ToString(rsRN.Get_Fields_Int32("ReceiptNumber"));
							}
							else
							{
								GetActualReceiptNumber = FCConvert.ToString(lngRN) + "*";
							}
						}
					}
					else
					{
						GetActualReceiptNumber = FCConvert.ToString(lngRN);
					}
				}
				else
				{
					GetActualReceiptNumber = FCConvert.ToString(0);
				}
				rsRN.Reset();
				return GetActualReceiptNumber;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Returning Receipt Number", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetActualReceiptNumber;
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			if (lngCount != 1)
			{
				lblFooter.Text = "Missing " + FCConvert.ToString(lngCount) + " UT transactions.";
			}
			else
			{
				lblFooter.Text = "Missing " + FCConvert.ToString(lngCount) + " UT transaction.";
			}
		}

		private void rptPaymentCrossCheckUT_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptPaymentCrossCheckUT.Caption	= "UT Payment Cross Check";
			//rptPaymentCrossCheckUT.Icon	= "rptPaymentCrossCheckUT.dsx":0000";
			//rptPaymentCrossCheckUT.Left	= 0;
			//rptPaymentCrossCheckUT.Top	= 0;
			//rptPaymentCrossCheckUT.Width	= 11880;
			//rptPaymentCrossCheckUT.Height	= 8595;
			//rptPaymentCrossCheckUT.StartUpPosition	= 3;
			//rptPaymentCrossCheckUT.SectionData	= "rptPaymentCrossCheckUT.dsx":058A;
			//End Unmaped Properties
		}
	}
}
