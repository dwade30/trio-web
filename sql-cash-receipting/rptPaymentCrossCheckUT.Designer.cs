﻿namespace TWCR0000
{
	/// <summary>
	/// Summary description for rptPaymentCrossCheckUT.
	/// </summary>
	partial class rptPaymentCrossCheckUT
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptPaymentCrossCheckUT));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReceiptNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPrin = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCurInt = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPLI = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCost = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTeller = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTax = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReceiptNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPrin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCurInt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPLI = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTeller = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblFooter = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReceiptNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrin)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCurInt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPLI)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTeller)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReceiptNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrin)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurInt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPLI)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTeller)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooter)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.ColumnDirection = GrapeCity.ActiveReports.SectionReportModel.ColumnDirection.AcrossDown;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldName,
				this.fldReceiptNumber,
				this.fldPrin,
				this.fldCurInt,
				this.fldPLI,
				this.fldCost,
				this.fldDate,
				this.fldTeller,
				this.fldType,
				this.fldTax,
				this.fldAccount
			});
			this.Detail.Height = 0.1875F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Line1,
				this.lblName,
				this.lblReceiptNumber,
				this.lblPrin,
				this.lblCurInt,
				this.lblPLI,
				this.lblCost,
				this.lblDate,
				this.lblTeller,
				this.lblHeader,
				this.lblTax,
				this.lblAccount
			});
			this.GroupHeader1.Height = 0.4583333F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblFooter
			});
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.4375F;
			this.Line1.Width = 7.5F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.5F;
			this.Line1.Y1 = 0.4375F;
			this.Line1.Y2 = 0.4375F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.1875F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 2.25F;
			this.lblName.MultiLine = false;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.lblName.Text = "Name";
			this.lblName.Top = 0.25F;
			this.lblName.Width = 1.5F;
			// 
			// lblReceiptNumber
			// 
			this.lblReceiptNumber.Height = 0.1875F;
			this.lblReceiptNumber.HyperLink = null;
			this.lblReceiptNumber.Left = 0F;
			this.lblReceiptNumber.MultiLine = false;
			this.lblReceiptNumber.Name = "lblReceiptNumber";
			this.lblReceiptNumber.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.lblReceiptNumber.Text = "Receipt #";
			this.lblReceiptNumber.Top = 0.25F;
			this.lblReceiptNumber.Width = 0.75F;
			// 
			// lblPrin
			// 
			this.lblPrin.Height = 0.1875F;
			this.lblPrin.HyperLink = null;
			this.lblPrin.Left = 4.75F;
			this.lblPrin.MultiLine = false;
			this.lblPrin.Name = "lblPrin";
			this.lblPrin.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblPrin.Text = "Prin";
			this.lblPrin.Top = 0.25F;
			this.lblPrin.Width = 0.625F;
			// 
			// lblCurInt
			// 
			this.lblCurInt.Height = 0.1875F;
			this.lblCurInt.HyperLink = null;
			this.lblCurInt.Left = 5.875F;
			this.lblCurInt.MultiLine = false;
			this.lblCurInt.Name = "lblCurInt";
			this.lblCurInt.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblCurInt.Text = "Cur Int";
			this.lblCurInt.Top = 0.25F;
			this.lblCurInt.Width = 0.625F;
			// 
			// lblPLI
			// 
			this.lblPLI.Height = 0.1875F;
			this.lblPLI.HyperLink = null;
			this.lblPLI.Left = 6.5F;
			this.lblPLI.MultiLine = false;
			this.lblPLI.Name = "lblPLI";
			this.lblPLI.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblPLI.Text = "PLI";
			this.lblPLI.Top = 0.25F;
			this.lblPLI.Width = 0.5625F;
			// 
			// lblCost
			// 
			this.lblCost.Height = 0.1875F;
			this.lblCost.HyperLink = null;
			this.lblCost.Left = 7.0625F;
			this.lblCost.MultiLine = false;
			this.lblCost.Name = "lblCost";
			this.lblCost.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblCost.Text = "Cost";
			this.lblCost.Top = 0.25F;
			this.lblCost.Width = 0.4375F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 0.75F;
			this.lblDate.MultiLine = false;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.lblDate.Text = "Date";
			this.lblDate.Top = 0.25F;
			this.lblDate.Width = 0.5F;
			// 
			// lblTeller
			// 
			this.lblTeller.Height = 0.1875F;
			this.lblTeller.HyperLink = null;
			this.lblTeller.Left = 4.375F;
			this.lblTeller.MultiLine = false;
			this.lblTeller.Name = "lblTeller";
			this.lblTeller.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.lblTeller.Text = "TLR";
			this.lblTeller.Top = 0.25F;
			this.lblTeller.Width = 0.375F;
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.25F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.lblHeader.Text = "UT CL Cross Check";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 7.5F;
			// 
			// lblTax
			// 
			this.lblTax.Height = 0.1875F;
			this.lblTax.HyperLink = null;
			this.lblTax.Left = 5.375F;
			this.lblTax.MultiLine = false;
			this.lblTax.Name = "lblTax";
			this.lblTax.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblTax.Text = "Tax";
			this.lblTax.Top = 0.25F;
			this.lblTax.Width = 0.5F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 1.75F;
			this.lblAccount.MultiLine = false;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.lblAccount.Text = "Acct";
			this.lblAccount.Top = 0.25F;
			this.lblAccount.Width = 0.5F;
			// 
			// fldName
			// 
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 2.25F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldName.Text = null;
			this.fldName.Top = 0F;
			this.fldName.Width = 2.125F;
			// 
			// fldReceiptNumber
			// 
			this.fldReceiptNumber.Height = 0.1875F;
			this.fldReceiptNumber.Left = 0F;
			this.fldReceiptNumber.Name = "fldReceiptNumber";
			this.fldReceiptNumber.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldReceiptNumber.Text = null;
			this.fldReceiptNumber.Top = 0F;
			this.fldReceiptNumber.Width = 0.75F;
			// 
			// fldPrin
			// 
			this.fldPrin.Height = 0.1875F;
			this.fldPrin.Left = 4.75F;
			this.fldPrin.Name = "fldPrin";
			this.fldPrin.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldPrin.Text = null;
			this.fldPrin.Top = 0F;
			this.fldPrin.Width = 0.625F;
			// 
			// fldCurInt
			// 
			this.fldCurInt.Height = 0.1875F;
			this.fldCurInt.Left = 5.875F;
			this.fldCurInt.Name = "fldCurInt";
			this.fldCurInt.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldCurInt.Text = null;
			this.fldCurInt.Top = 0F;
			this.fldCurInt.Width = 0.625F;
			// 
			// fldPLI
			// 
			this.fldPLI.Height = 0.1875F;
			this.fldPLI.Left = 6.5F;
			this.fldPLI.Name = "fldPLI";
			this.fldPLI.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldPLI.Text = null;
			this.fldPLI.Top = 0F;
			this.fldPLI.Width = 0.5625F;
			// 
			// fldCost
			// 
			this.fldCost.Height = 0.1875F;
			this.fldCost.Left = 7.0625F;
			this.fldCost.Name = "fldCost";
			this.fldCost.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldCost.Text = null;
			this.fldCost.Top = 0F;
			this.fldCost.Width = 0.4375F;
			// 
			// fldDate
			// 
			this.fldDate.Height = 0.1875F;
			this.fldDate.Left = 0.75F;
			this.fldDate.Name = "fldDate";
			this.fldDate.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldDate.Text = null;
			this.fldDate.Top = 0F;
			this.fldDate.Width = 0.75F;
			// 
			// fldTeller
			// 
			this.fldTeller.Height = 0.1875F;
			this.fldTeller.Left = 4.375F;
			this.fldTeller.Name = "fldTeller";
			this.fldTeller.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldTeller.Text = null;
			this.fldTeller.Top = 0F;
			this.fldTeller.Width = 0.375F;
			// 
			// fldType
			// 
			this.fldType.Height = 0.1875F;
			this.fldType.Left = 1.5F;
			this.fldType.Name = "fldType";
			this.fldType.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldType.Text = null;
			this.fldType.Top = 0F;
			this.fldType.Width = 0.25F;
			// 
			// fldTax
			// 
			this.fldTax.Height = 0.1875F;
			this.fldTax.Left = 5.375F;
			this.fldTax.Name = "fldTax";
			this.fldTax.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldTax.Text = null;
			this.fldTax.Top = 0F;
			this.fldTax.Width = 0.5F;
			// 
			// fldAccount
			// 
			this.fldAccount.Height = 0.1875F;
			this.fldAccount.Left = 1.75F;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldAccount.Text = null;
			this.fldAccount.Top = 0F;
			this.fldAccount.Width = 0.5F;
			// 
			// lblFooter
			// 
			this.lblFooter.Height = 0.1875F;
			this.lblFooter.HyperLink = null;
			this.lblFooter.Left = 0F;
			this.lblFooter.MultiLine = false;
			this.lblFooter.Name = "lblFooter";
			this.lblFooter.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.lblFooter.Text = null;
			this.lblFooter.Top = 0F;
			this.lblFooter.Width = 7.5F;
			// 
			// rptPaymentCrossCheckUT
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReceiptNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrin)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCurInt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPLI)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTeller)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReceiptNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrin)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurInt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPLI)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTeller)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooter)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReceiptNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrin;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCurInt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPLI;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTeller;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReceiptNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPrin;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCurInt;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPLI;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCost;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTeller;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTax;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFooter;
	}
}
