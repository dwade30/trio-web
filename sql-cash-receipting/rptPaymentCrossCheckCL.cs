﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for rptPaymentCrossCheckCL.
	/// </summary>
	public partial class rptPaymentCrossCheckCL : BaseSectionReport
	{
		public static rptPaymentCrossCheckCL InstancePtr
		{
			get
			{
				return (rptPaymentCrossCheckCL)Sys.GetInstance(typeof(rptPaymentCrossCheckCL));
			}
		}

		protected rptPaymentCrossCheckCL _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPaymentCrossCheckCL	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               05/18/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               10/30/2006              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		int lngCount;
		double dblTotalAmt;
		int[] arrMissingPayments = null;
		bool boolDone;

		public rptPaymentCrossCheckCL()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "CL Payment Cross Check";
            this.ReportEnd += RptPaymentCrossCheckCL_ReportEnd;
		}

        private void RptPaymentCrossCheckCL_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = boolDone;
		}

		private void ActiveReport_Initialize()
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
		}
		//private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		//{
		//	switch (KeyCode)
		//	{
		//		case Keys.Escape:
		//			{
		//				Close();
		//				break;
		//			}
		//	} //end switch
		//}
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			CreateArrayElements();
			if (lngCount == 0)
			{
				MessageBox.Show("No RE records to report.", "No RE Missing Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Close();
			}
			else
			{
				lngCount = 0;
			}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			// MAL@20071012
			rptPaymentCrossCheck.InstancePtr.lblHeader2.Text = "Receipts appearing on this report do not have corresponding entries in the Tax Collections databases";
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void BindFields()
		{
			clsDRWrapper rsCL = new clsDRWrapper();
			fldReceiptNumber.Text = "";
			fldDate.Text = "";
			fldType.Text = "";
			fldYear.Text = "";
			fldName.Text = "";
			fldTeller.Text = "";
			fldPrin.Text = "";
			fldCurInt.Text = "";
			fldCost.Text = "";
			fldPLI.Text = "";
			fldAccount.Text = "";
			if (lngCount <= Information.UBound(arrMissingPayments, 1))
			{
				if (arrMissingPayments[lngCount] > 0)
				{
					rsData.FindFirstRecord("ID", arrMissingPayments[lngCount]);
					if (!rsData.NoMatch)
					{
						if (FCConvert.ToInt32(rsData.Get_Fields_Int32("ReceiptType")) == 90)
						{
							// TODO Get_Fields: Check the table for the column [Amount1] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Amount3] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Amount6] and replace with corresponding Get_Field method
							fldPrin.Text = Strings.Format(rsData.Get_Fields("Amount1") + rsData.Get_Fields("Amount3") + rsData.Get_Fields("Amount6"), "#,##0.00");
							// TODO Get_Fields: Check the table for the column [Amount2] and replace with corresponding Get_Field method
							fldCurInt.Text = Strings.Format(rsData.Get_Fields("Amount2"), "#,##0.00");
							// TODO Get_Fields: Check the table for the column [Amount4] and replace with corresponding Get_Field method
							fldCost.Text = Strings.Format(rsData.Get_Fields("Amount4"), "#,##0.00");
							fldPLI.Text = "0.00";
							fldType.Text = "R";
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Amount1] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Amount6] and replace with corresponding Get_Field method
							fldPrin.Text = Strings.Format(rsData.Get_Fields("Amount1") + rsData.Get_Fields("Amount6"), "#,##0.00");
							// TODO Get_Fields: Check the table for the column [Amount2] and replace with corresponding Get_Field method
							fldCurInt.Text = Strings.Format(rsData.Get_Fields("Amount2"), "#,##0.00");
							// TODO Get_Fields: Check the table for the column [Amount3] and replace with corresponding Get_Field method
							fldPLI.Text = Strings.Format(rsData.Get_Fields("Amount3"), "#,##0.00");
							// TODO Get_Fields: Check the table for the column [Amount4] and replace with corresponding Get_Field method
							fldCost.Text = Strings.Format(rsData.Get_Fields("Amount4"), "#,##0.00");
							fldType.Text = "L";
						}
						fldReceiptNumber.Text = GetActualReceiptNumber_2(FCConvert.ToInt32(rsData.Get_Fields_Int32("ReceiptNumber")));
						fldDate.Text = Strings.Format(rsData.Get_Fields_DateTime("ActualSystemDate"), "MM/dd/yyyy");
						if (Strings.InStr(1, FCConvert.ToString(rsData.Get_Fields_String("Ref")), "-", CompareConstants.vbBinaryCompare) != 0)
						{
							fldYear.Text = Strings.Right(FCConvert.ToString(rsData.Get_Fields_String("Ref")), FCConvert.ToString(rsData.Get_Fields_String("Ref")).Length - Strings.InStr(1, FCConvert.ToString(rsData.Get_Fields_String("Ref")), "-", CompareConstants.vbBinaryCompare));
						}
						else
						{
							fldYear.Text = "";
						}
						fldName.Text = rsData.Get_Fields_String("Name");
						fldTeller.Text = rsData.Get_Fields_String("TellerID");
						fldAccount.Text = Strings.Left(FCConvert.ToString(rsData.Get_Fields_String("Ref")), Strings.InStr(1, FCConvert.ToString(rsData.Get_Fields_String("Ref")), "-", CompareConstants.vbBinaryCompare));
					}
					else
					{
						ClearBoxes();
					}
				}
				else
				{
					rsCL.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = " + FCConvert.ToString(arrMissingPayments[lngCount] * -1), modExtraModules.strCLDatabase);
					if (!rsCL.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
						fldPrin.Text = Strings.Format(rsCL.Get_Fields("Principal"), "#,##0.00");
						fldCurInt.Text = Strings.Format(rsCL.Get_Fields_Decimal("CurrentInterest") + rsCL.Get_Fields_Decimal("PreLienInterest"), "#,##0.00");
						fldCost.Text = Strings.Format(rsCL.Get_Fields_Decimal("LienCost"), "#,##0.00");
						fldPLI.Text = "0.00";
						fldType.Text = "R";
						fldReceiptNumber.Text = GetActualReceiptNumber_2(rsCL.Get_Fields_Int32("ReceiptNumber"));
						fldDate.Text = Strings.Format(rsCL.Get_Fields_DateTime("ActualSystemDate"), "MM/dd/yyyy");
						// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
						fldYear.Text = FCConvert.ToString(rsData.Get_Fields("Year"));
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						fldName.Text = "Missing Receipt - " + rsCL.Get_Fields("Code");
						fldTeller.Text = rsCL.Get_Fields_String("Teller");
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						fldAccount.Text = FCConvert.ToString(rsCL.Get_Fields("Account"));
					}
					else
					{
						ClearBoxes();
					}
				}
				lngCount += 1;
			}
			else
			{
				boolDone = true;
			}
			rsCL.DisposeOf();
		}

		private void CreateArrayElements()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsCL = new clsDRWrapper();
				string strSQL;
				int lngFM = 0;
				// vbPorter upgrade warning: lngYR As int	OnWrite(int, double)
				int lngYR = 0;
				// vbPorter upgrade warning: strCCDate As DateTime	OnWrite(string)
				DateTime strCCDate = DateTime.FromOADate(0);
				int lngType = 0;
				// vbPorter upgrade warning: strLastEOY As string	OnWrite(DateTime)	OnRead(DateTime)
				string strLastEOY;
				if (modGlobalConstants.Statics.gboolBD)
				{
					rsCL.OpenRecordset("SELECT FiscalStart FROM Budgetary", modExtraModules.strBDDatabase);
					if (!rsCL.EndOfFile())
					{
						lngFM = FCConvert.ToInt32(rsCL.Get_Fields_String("FiscalStart"));
					}
					else
					{
						lngFM = 1;
					}
				}
				else
				{
					lngFM = 1;
				}
				// frmWait.Init "Please Wait..." & vbCrLf & "Loading Receipts"
				if (DateTime.Today.ToOADate() > DateAndTime.DateValue(FCConvert.ToString(lngFM) + "/01/" + FCConvert.ToString(DateTime.Today.Year)).ToOADate() && lngFM <= DateTime.Today.Month)
				{
					lngYR = DateTime.Today.Year;
				}
				else
				{
					lngYR = DateTime.Today.Year - 1;
				}
				strLastEOY = FCConvert.ToString(DateAndTime.DateValue(FCConvert.ToString(lngFM) + "/01/" + FCConvert.ToString(lngYR)));
				rsCL.OpenRecordset("SELECT * FROM CashRec");
				if (!rsCL.EndOfFile())
				{
					lngType = FCConvert.ToInt32(Math.Round(Conversion.Val(rsCL.Get_Fields_Int32("CrossCheckDefault"))));
				}
				switch (lngType)
				{
					case 0:
						{
							// last EOY
							strCCDate = FCConvert.ToDateTime(strLastEOY);
							break;
						}
					case 1:
						{
							// last cross check report
							if (DateAndTime.DateDiff("D", (DateTime)rsCL.Get_Fields_DateTime("LastCrossCheck"), DateAndTime.DateValue(strLastEOY)) > 0)
							{
								strCCDate = FCConvert.ToDateTime(strLastEOY);
							}
							else
							{
								strCCDate = FCConvert.ToDateTime(Strings.Format(rsCL.Get_Fields_DateTime("LastCrossCheck"), "MM/dd/yyyy"));
							}
							break;
						}
					case 2:
						{
							// specific date
							if (DateAndTime.DateDiff("D", (DateTime)rsCL.Get_Fields_DateTime("CrossCheckDefaultDate"), DateAndTime.DateValue(strLastEOY)) > 0)
							{
								strCCDate = FCConvert.ToDateTime(strLastEOY);
							}
							else
							{
								strCCDate = FCConvert.ToDateTime(Strings.Format(rsCL.Get_Fields_DateTime("CrossCheckDefaultDate"), "MM/dd/yyyy"));
							}
							break;
						}
				}
				//end switch
				strSQL = "SELECT * FROM Archive WHERE (ReceiptType = 90 OR ReceiptType = 91) AND ArchiveDate > '" + FCConvert.ToString(strCCDate) + "' ORDER BY ReceiptNumber";
				lngCount = 0;
				rsData.OpenRecordset(strSQL, modExtraModules.strCRDatabase);
				// frmWait.Init "Please Wait..." & vbCrLf & "Checking RE Receipts", True, rsData.RecordCount, True
				while (!rsData.EndOfFile())
				{
					// frmWait.IncrementProgress
					// If rsData.Fields("AccountNUmber") = 1246 Then
					// Stop
					// End If
					lngYR = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Right(Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Ref"))), 5).Replace("-", ""))));
					if (lngYR > 10000)
					{
						lngYR /= 10;
					}
					if (lngYR > 0)
					{
						if (FCConvert.ToInt32(rsData.Get_Fields_Int32("ReceiptType")) == 90)
						{
							// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Amount1] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Amount3] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Amount6] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Amount2] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Amount4] and replace with corresponding Get_Field method
							rsCL.OpenRecordset("SELECT * FROM PaymentRec WHERE Account = " + rsData.Get_Fields("AccountNumber") + " AND BillCode = 'R' AND ([Year] / 10) = " + FCConvert.ToString(lngYR) + " AND ReceiptNumber = " + rsData.Get_Fields_Int32("ReceiptNumber") + " AND Code = '" + rsData.Get_Fields_String("Control2") + "' AND Month(ActualSystemDate) = " + Strings.Format(rsData.Get_Fields_DateTime("ActualSystemDate"), "MM") + " AND Day(ActualSystemDate) = " + Strings.Format(rsData.Get_Fields_DateTime("ActualSystemDate"), "dd") + " AND Principal = " + FCConvert.ToString(rsData.Get_Fields("Amount1") + rsData.Get_Fields("Amount3") + rsData.Get_Fields("Amount6")) + " AND CurrentInterest = " + rsData.Get_Fields("Amount2") + " AND LienCost = " + rsData.Get_Fields("Amount4"), modExtraModules.strCLDatabase);
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Amount1] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Amount6] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Amount2] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Amount3] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Amount4] and replace with corresponding Get_Field method
							rsCL.OpenRecordset("SELECT * FROM PaymentRec WHERE Account = " + rsData.Get_Fields("AccountNumber") + " AND BillCode = 'L' AND ([Year] / 10) = " + FCConvert.ToString(lngYR) + " AND ReceiptNumber = " + rsData.Get_Fields_Int32("ReceiptNumber") + " AND Code = '" + rsData.Get_Fields_String("Control2") + "' AND Month(ActualSystemDate) = " + Strings.Format(rsData.Get_Fields_DateTime("ActualSystemDate"), "MM") + " AND Day(ActualSystemDate) = " + Strings.Format(rsData.Get_Fields_DateTime("ActualSystemDate"), "dd") + " AND Principal = " + FCConvert.ToString(rsData.Get_Fields("Amount1") + rsData.Get_Fields("Amount6")) + " AND CurrentInterest = " + rsData.Get_Fields("Amount2") + " AND PreLienInterest = " + rsData.Get_Fields("Amount3") + " AND LienCost = " + rsData.Get_Fields("Amount4"), modExtraModules.strCLDatabase);
						}
						if (rsCL.EndOfFile())
						{
							Array.Resize(ref arrMissingPayments, lngCount + 1);
							arrMissingPayments[lngCount] = FCConvert.ToInt32(rsData.Get_Fields_Int32("ID"));
							lngCount += 1;
						}
					}
					rsData.MoveNext();
				}
				frmWait.InstancePtr.Unload();
				rsCL.DisposeOf();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Array Elements", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ClearBoxes()
		{
			fldCost.Text = "";
			fldDate.Text = "";
			fldCurInt.Text = "";
			fldName.Text = "";
			fldPLI.Text = "";
			fldPrin.Text = "";
			fldReceiptNumber.Text = "";
			fldTeller.Text = "";
			fldType.Text = "";
		}
		// vbPorter upgrade warning: 'Return' As string	OnWrite(string, int)
		private string GetActualReceiptNumber_2(int lngRN, DateTime? dtDate = null)
		{
			return GetActualReceiptNumber(ref lngRN, dtDate);
		}

		private string GetActualReceiptNumber(ref int lngRN, DateTime? dtDateTemp = null)
		{
			DateTime dtDate = dtDateTemp ?? DateTime.Now;
			string GetActualReceiptNumber = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will accept a long of a receipt number ID in the CR table
				// if the receipt does not exist or any other error occurs, the function will
				// return 0, otherwise it returns the receipt number shown on the receipt
				clsDRWrapper rsRN = new clsDRWrapper();
				if (lngRN > 0)
				{
					if (modGlobalConstants.Statics.gboolCR)
					{
						rsRN.OpenRecordset("SELECT * FROM Receipt WHERE ReceiptKey = " + FCConvert.ToString(lngRN), modExtraModules.strCRDatabase);
						if (rsRN.EndOfFile() != true && rsRN.BeginningOfFile() != true)
						{
							GetActualReceiptNumber = FCConvert.ToString(rsRN.Get_Fields_Int32("ReceiptNumber"));
						}
						else
						{
							rsRN.OpenRecordset("SELECT * FROM LastYearReceipt WHERE ReceiptKey = " + FCConvert.ToString(lngRN) + " AND LastYearReceiptDate > '" + Strings.Format(dtDate, "MM/dd/yyyy") + " 12:00:00 AM' AND LastYearReceiptDate < '" + Strings.Format(dtDate, "MM/dd/yyyy") + " 11:59:59 PM'", modExtraModules.strCRDatabase);
							if (rsRN.EndOfFile() != true && rsRN.BeginningOfFile() != true)
							{
								GetActualReceiptNumber = FCConvert.ToString(rsRN.Get_Fields_Int32("ReceiptNumber"));
							}
							else
							{
								GetActualReceiptNumber = FCConvert.ToString(lngRN) + "*";
							}
						}
					}
					else
					{
						GetActualReceiptNumber = FCConvert.ToString(lngRN);
					}
				}
				else
				{
					GetActualReceiptNumber = FCConvert.ToString(0);
				}
				rsRN.Reset();
				return GetActualReceiptNumber;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Returning Receipt Number", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetActualReceiptNumber;
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			if (lngCount != 1)
			{
				lblFooter.Text = "Missing " + FCConvert.ToString(lngCount) + " RE transactions.";
			}
			else
			{
				lblFooter.Text = "Missing " + FCConvert.ToString(lngCount) + " RE transaction.";
			}
		}

		private void rptPaymentCrossCheckCL_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptPaymentCrossCheckCL.Caption	= "CL Payment Cross Check";
			//rptPaymentCrossCheckCL.Icon	= "rptPaymentCrossCheckCL.dsx":0000";
			//rptPaymentCrossCheckCL.Left	= 0;
			//rptPaymentCrossCheckCL.Top	= 0;
			//rptPaymentCrossCheckCL.Width	= 11880;
			//rptPaymentCrossCheckCL.Height	= 8595;
			//rptPaymentCrossCheckCL.StartUpPosition	= 3;
			//rptPaymentCrossCheckCL.SectionData	= "rptPaymentCrossCheckCL.dsx":058A;
			//End Unmaped Properties
		}
	}
}
