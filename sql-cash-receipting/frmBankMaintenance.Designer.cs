﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmBankMaintenance.
	/// </summary>
	partial class frmBankMaintenance : BaseForm
	{
		public FCGrid vsBank;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileAddBank;
		public fecherFoundation.FCToolStripMenuItem mnuFileDeleteBank;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator2;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem mnuFileSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.vsBank = new fecherFoundation.FCGrid();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileAddBank = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileDeleteBank = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSeperator2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
            this.btnSave = new fecherFoundation.FCButton();
            this.cmdNew = new fecherFoundation.FCButton();
            this.cmdDelete = new fecherFoundation.FCButton();
            this.cmdPrintBankList = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsBank)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintBankList)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 370);
            this.BottomPanel.Size = new System.Drawing.Size(467, 96);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.vsBank);
            this.ClientArea.Size = new System.Drawing.Size(467, 310);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdPrintBankList);
            this.TopPanel.Controls.Add(this.cmdDelete);
            this.TopPanel.Controls.Add(this.cmdNew);
            this.TopPanel.Size = new System.Drawing.Size(467, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrintBankList, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(216, 30);
            this.HeaderText.Text = "Bank Maintenance";
            // 
            // vsBank
            // 
            this.vsBank.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsBank.Cols = 4;
            this.vsBank.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.vsBank.FixedCols = 0;
            this.vsBank.Location = new System.Drawing.Point(30, 30);
            this.vsBank.Name = "vsBank";
            this.vsBank.RowHeadersVisible = false;
            this.vsBank.Rows = 1;
            this.vsBank.Size = new System.Drawing.Size(417, 250);
            this.vsBank.StandardTab = false;
            this.vsBank.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsBank.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vsBank_KeyDownEdit);
            this.vsBank.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsBank_StartEdit);
            this.vsBank.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsBank_ValidateEdit);
            this.vsBank.CurrentCellChanged += new System.EventHandler(this.vsBank_RowColChange);
            this.vsBank.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vsBank_MouseDownEvent);
            this.vsBank.KeyDown += new Wisej.Web.KeyEventHandler(this.vsBank_KeyDownEvent);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileAddBank,
            this.mnuFileDeleteBank,
            this.mnuFileSeperator2,
            this.mnuFilePrint,
            this.mnuFileSave,
            this.mnuFileSaveExit,
            this.Seperator,
            this.mnuFileExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuFileAddBank
            // 
            this.mnuFileAddBank.Index = 0;
            this.mnuFileAddBank.Name = "mnuFileAddBank";
            this.mnuFileAddBank.Shortcut = Wisej.Web.Shortcut.CtrlN;
            this.mnuFileAddBank.Text = "New";
            this.mnuFileAddBank.Click += new System.EventHandler(this.mnuFileAddBank_Click);
            // 
            // mnuFileDeleteBank
            // 
            this.mnuFileDeleteBank.Index = 1;
            this.mnuFileDeleteBank.Name = "mnuFileDeleteBank";
            this.mnuFileDeleteBank.Shortcut = Wisej.Web.Shortcut.CtrlD;
            this.mnuFileDeleteBank.Text = "Delete";
            this.mnuFileDeleteBank.Click += new System.EventHandler(this.mnuFileDeleteBank_Click);
            // 
            // mnuFileSeperator2
            // 
            this.mnuFileSeperator2.Index = 2;
            this.mnuFileSeperator2.Name = "mnuFileSeperator2";
            this.mnuFileSeperator2.Text = "-";
            // 
            // mnuFilePrint
            // 
            this.mnuFilePrint.Index = 3;
            this.mnuFilePrint.Name = "mnuFilePrint";
            this.mnuFilePrint.Text = "Print Bank List";
            this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
            // 
            // mnuFileSave
            // 
            this.mnuFileSave.Index = 4;
            this.mnuFileSave.Name = "mnuFileSave";
            this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuFileSave.Text = "Save";
            this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // mnuFileSaveExit
            // 
            this.mnuFileSaveExit.Index = 5;
            this.mnuFileSaveExit.Name = "mnuFileSaveExit";
            this.mnuFileSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuFileSaveExit.Text = "Save & Exit";
            this.mnuFileSaveExit.Click += new System.EventHandler(this.mnuFileSaveExit_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 6;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuFileExit
            // 
            this.mnuFileExit.Index = 7;
            this.mnuFileExit.Name = "mnuFileExit";
            this.mnuFileExit.Text = "Exit";
            this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
            // 
            // btnSave
            // 
            this.btnSave.AppearanceKey = "acceptButton";
            this.btnSave.Location = new System.Drawing.Point(201, 30);
            this.btnSave.Name = "btnSave";
            this.btnSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnSave.Size = new System.Drawing.Size(96, 48);
            this.btnSave.Text = "Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cmdNew
            // 
            this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdNew.Location = new System.Drawing.Point(248, 29);
            this.cmdNew.Name = "cmdNew";
            this.cmdNew.Size = new System.Drawing.Size(42, 24);
            this.cmdNew.TabIndex = 1;
            this.cmdNew.Text = "New";
            this.cmdNew.Click += new System.EventHandler(this.mnuFileAddBank_Click);
            // 
            // cmdDelete
            // 
            this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDelete.Location = new System.Drawing.Point(294, 29);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(55, 24);
            this.cmdDelete.TabIndex = 2;
            this.cmdDelete.Text = "Delete";
            this.cmdDelete.Click += new System.EventHandler(this.mnuFileDeleteBank_Click);
            // 
            // cmdPrintBankList
            // 
            this.cmdPrintBankList.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintBankList.Location = new System.Drawing.Point(353, 29);
            this.cmdPrintBankList.Name = "cmdPrintBankList";
            this.cmdPrintBankList.Size = new System.Drawing.Size(102, 24);
            this.cmdPrintBankList.TabIndex = 3;
            this.cmdPrintBankList.Text = "Print Bank List";
            this.cmdPrintBankList.Click += new System.EventHandler(this.mnuFilePrint_Click);
            // 
            // frmBankMaintenance
            // 
            this.ClientSize = new System.Drawing.Size(467, 466);
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmBankMaintenance";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Bank Maintenance";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmBankMaintenance_Load);
            this.Activated += new System.EventHandler(this.frmBankMaintenance_Activated);
            this.Resize += new System.EventHandler(this.frmBankMaintenance_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmBankMaintenance_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmBankMaintenance_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsBank)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintBankList)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton btnSave;
		private FCButton cmdNew;
		private FCButton cmdPrintBankList;
		private FCButton cmdDelete;
	}
}
