﻿namespace TWCR0000
{
	/// <summary>
	/// Summary description for sarReceiptSearchSummary.
	/// </summary>
	partial class sarReceiptSearchSummary
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(sarReceiptSearchSummary));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldHeader = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldHeader2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.sarSummaryDetail = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.fldCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.lblSummaryHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblHeader2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.fldTotalCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.fldHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeader2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldHeader,
            this.fldAmount,
            this.fldHeader2,
            this.sarSummaryDetail,
            this.fldCount});
			this.Detail.Height = 1.228667F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldHeader
			// 
			this.fldHeader.Height = 0.1875F;
			this.fldHeader.Left = 0F;
			this.fldHeader.Name = "fldHeader";
			this.fldHeader.Style = "font-family: \'Tahoma\'";
			this.fldHeader.Text = null;
			this.fldHeader.Top = 0F;
			this.fldHeader.Width = 2.521F;
			// 
			// fldAmount
			// 
			this.fldAmount.Height = 0.1875F;
			this.fldAmount.Left = 5.1245F;
			this.fldAmount.Name = "fldAmount";
			this.fldAmount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldAmount.Text = null;
			this.fldAmount.Top = 0F;
			this.fldAmount.Width = 1.3125F;
			// 
			// fldHeader2
			// 
			this.fldHeader2.Height = 0.1875F;
			this.fldHeader2.Left = 2.625F;
			this.fldHeader2.MultiLine = false;
			this.fldHeader2.Name = "fldHeader2";
			this.fldHeader2.Style = "font-family: \'Tahoma\'";
			this.fldHeader2.Text = null;
			this.fldHeader2.Top = 0F;
			this.fldHeader2.Width = 1.25F;
			// 
			// sarSummaryDetail
			// 
			this.sarSummaryDetail.CloseBorder = false;
			this.sarSummaryDetail.Height = 1F;
			this.sarSummaryDetail.Left = 0F;
			this.sarSummaryDetail.Name = "sarSummaryDetail";
			this.sarSummaryDetail.Report = null;
			this.sarSummaryDetail.ReportName = "subReport1";
			this.sarSummaryDetail.Top = 0.187F;
			this.sarSummaryDetail.Width = 6.5F;
			// 
			// fldCount
			// 
			this.fldCount.Height = 0.1875F;
			this.fldCount.Left = 4.311501F;
			this.fldCount.Name = "fldCount";
			this.fldCount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldCount.Text = null;
			this.fldCount.Top = 0F;
			this.fldCount.Width = 0.6875F;
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblSummaryHeader,
            this.lblHeader,
            this.lblCount,
            this.lblAmount,
            this.Line1,
            this.lblHeader2});
			this.GroupHeader1.Height = 0.625F;
			this.GroupHeader1.KeepTogether = true;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// lblSummaryHeader
			// 
			this.lblSummaryHeader.Height = 0.3125F;
			this.lblSummaryHeader.HyperLink = null;
			this.lblSummaryHeader.Left = 0F;
			this.lblSummaryHeader.Name = "lblSummaryHeader";
			this.lblSummaryHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblSummaryHeader.Text = "Receipt Summary";
			this.lblSummaryHeader.Top = 0F;
			this.lblSummaryHeader.Width = 6.5F;
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.1875F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblHeader.Text = null;
			this.lblHeader.Top = 0.4375F;
			this.lblHeader.Width = 1F;
			// 
			// lblCount
			// 
			this.lblCount.Height = 0.1875F;
			this.lblCount.HyperLink = null;
			this.lblCount.Left = 4.312F;
			this.lblCount.Name = "lblCount";
			this.lblCount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblCount.Text = "Count";
			this.lblCount.Top = 0.4375F;
			this.lblCount.Width = 0.6875F;
			// 
			// lblAmount
			// 
			this.lblAmount.Height = 0.1875F;
			this.lblAmount.HyperLink = null;
			this.lblAmount.Left = 5.1245F;
			this.lblAmount.Name = "lblAmount";
			this.lblAmount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblAmount.Text = "Amount";
			this.lblAmount.Top = 0.4375F;
			this.lblAmount.Width = 1.25F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.625F;
			this.Line1.Width = 6.374F;
			this.Line1.X1 = 6.374F;
			this.Line1.X2 = 0F;
			this.Line1.Y1 = 0.625F;
			this.Line1.Y2 = 0.625F;
			// 
			// lblHeader2
			// 
			this.lblHeader2.Height = 0.1875F;
			this.lblHeader2.HyperLink = null;
			this.lblHeader2.Left = 2.625F;
			this.lblHeader2.Name = "lblHeader2";
			this.lblHeader2.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblHeader2.Text = null;
			this.lblHeader2.Top = 0.4375F;
			this.lblHeader2.Width = 0.9375F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.CanGrow = false;
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldTotalCount,
            this.fldTotalAmount,
            this.Line2});
			this.GroupFooter1.Height = 0.3854167F;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			// 
			// fldTotalCount
			// 
			this.fldTotalCount.Height = 0.1875F;
			this.fldTotalCount.Left = 4.312F;
			this.fldTotalCount.Name = "fldTotalCount";
			this.fldTotalCount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalCount.Text = null;
			this.fldTotalCount.Top = 0.125F;
			this.fldTotalCount.Width = 0.6875F;
			// 
			// fldTotalAmount
			// 
			this.fldTotalAmount.Height = 0.1875F;
			this.fldTotalAmount.Left = 4.999F;
			this.fldTotalAmount.Name = "fldTotalAmount";
			this.fldTotalAmount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalAmount.Text = null;
			this.fldTotalAmount.Top = 0.125F;
			this.fldTotalAmount.Width = 1.4375F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 3.875F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.062F;
			this.Line2.Width = 2.5625F;
			this.Line2.X1 = 3.875F;
			this.Line2.X2 = 6.4375F;
			this.Line2.Y1 = 0.062F;
			this.Line2.Y2 = 0.062F;
			// 
			// sarReceiptSearchSummary
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.MirrorMargins = true;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.fldHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeader2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarSummaryDetail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCount;
	}
}
