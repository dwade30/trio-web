﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmMOSESSetup.
	/// </summary>
	partial class frmMOSESSetup : BaseForm
	{
		public FCGrid GRID;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem mnuFileContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileQuit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle7 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle8 = new Wisej.Web.DataGridViewCellStyle();
			this.GRID = new fecherFoundation.FCGrid();
			//this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileContinue = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileQuit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GRID)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 454);
			this.BottomPanel.Size = new System.Drawing.Size(535, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.GRID);
			this.ClientArea.Size = new System.Drawing.Size(535, 394);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(535, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(233, 30);
			this.HeaderText.Text = "MOSES Type Setup";
			// 
			// GRID
			// 
			this.GRID.AllowSelection = false;
			this.GRID.AllowUserToResizeColumns = false;
			this.GRID.AllowUserToResizeRows = false;
			this.GRID.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.GRID.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GRID.BackColorAlternate = System.Drawing.Color.Empty;
			this.GRID.BackColorBkg = System.Drawing.Color.Empty;
			this.GRID.BackColorFixed = System.Drawing.Color.Empty;
			this.GRID.BackColorSel = System.Drawing.Color.Empty;
			this.GRID.Cols = 10;
			dataGridViewCellStyle7.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GRID.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
			this.GRID.ColumnHeadersHeight = 30;
			this.GRID.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle8.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GRID.DefaultCellStyle = dataGridViewCellStyle8;
			this.GRID.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GRID.ForeColorFixed = System.Drawing.Color.Empty;
			this.GRID.FrozenCols = 0;
			this.GRID.GridColor = System.Drawing.Color.Empty;
			this.GRID.Location = new System.Drawing.Point(30, 30);
			this.GRID.Name = "GRID";
			this.GRID.ReadOnly = true;
			this.GRID.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GRID.RowHeightMin = 0;
			this.GRID.Rows = 50;
			this.GRID.ShowColumnVisibilityMenu = false;
			this.GRID.Size = new System.Drawing.Size(473, 330);
			this.GRID.StandardTab = false;
			this.GRID.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.GRID.TabIndex = 0;
			this.GRID.CurrentCellChanged += new System.EventHandler(this.GRID_RowColChange);
			this.GRID.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.GRID_MouseMoveEvent);
			// 
			// MainMenu1
			// 
			//this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
			//this.mnuFile});
			//this.MainMenu1.Name = null;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = 0;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileSave,
				this.mnuFileContinue,
				this.Seperator,
				this.mnuFileQuit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuFileSave
			// 
			this.mnuFileSave.Index = 0;
			this.mnuFileSave.Name = "mnuFileSave";
			this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuFileSave.Text = "Save";
			this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// mnuFileContinue
			// 
			this.mnuFileContinue.Index = 1;
			this.mnuFileContinue.Name = "mnuFileContinue";
			this.mnuFileContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFileContinue.Text = "Save and Exit";
			this.mnuFileContinue.Click += new System.EventHandler(this.mnuFileContinue_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 2;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuFileQuit
			// 
			this.mnuFileQuit.Index = 3;
			this.mnuFileQuit.Name = "mnuFileQuit";
			this.mnuFileQuit.Text = "Exit";
			this.mnuFileQuit.Click += new System.EventHandler(this.mnuFileQuit_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(217, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(76, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// frmMOSESSetup
			// 
			this.ClientSize = new System.Drawing.Size(535, 562);
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			//this.Menu = this.MainMenu1;
			this.Name = "frmMOSESSetup";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "MOSES Type Setup";
			this.Load += new System.EventHandler(this.frmMOSESSetup_Load);
			this.Activated += new System.EventHandler(this.frmMOSESSetup_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmMOSESSetup_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmMOSESSetup_KeyPress);
			this.Resize += new System.EventHandler(this.frmMOSESSetup_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.GRID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
	}
}
