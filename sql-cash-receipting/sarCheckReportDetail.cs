﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using System.Linq.Expressions;
using System.Web.UI.WebControls.Expressions;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for sarCheckReportDetail.
	/// </summary>
	public partial class sarCheckReportDetail : FCSectionReport
	{
		public static sarCheckReportDetail InstancePtr
		{
			get
			{
				return (sarCheckReportDetail)Sys.GetInstance(typeof(sarCheckReportDetail));
			}
		}

		protected sarCheckReportDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	sarCheckReportDetail	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               10/01/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               06/25/2004              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsData2 = new clsDRWrapper();
		string strSQL;
		int lngChkCnt;
		bool boolUsePrevYears = false;

		public sarCheckReportDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += SarCheckReportDetail_ReportEnd;
		}

        private void SarCheckReportDetail_ReportEnd(object sender, EventArgs e)
        {
            rsData2.DisposeOf();
			rsData.DisposeOf();
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
			{
				FillDetail();
				rsData.MoveNext();
				eArgs.EOF = false;
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strMaster;
			string strArchive;
			string strReceipt;
			string strDateField;
			DateTime dateOfCloseOutEnd;
			DateTime dateOfCloseoutStart;
			string strDateRange;

			// Need a date range for Prev Year receipts because ReceiptKey isn't unique
			boolUsePrevYears = frmDailyReceiptAudit.InstancePtr.boolUsePrevYears;
			if (boolUsePrevYears)
			{
				strMaster = "LastYearCheckMaster";
				strArchive = "LastYearArchive";
				strReceipt = "LastYearReceipt";
				strDateField = "LastYearReceiptDate,";
				dateOfCloseOutEnd = GetCloseOutDate(frmDailyReceiptAudit.InstancePtr.lngCloseoutKey);
				dateOfCloseoutStart = GetPrevCloseOutDate(frmDailyReceiptAudit.InstancePtr.lngCloseoutKey);
				strDateRange = " AND LastYearReceiptDate < '" + dateOfCloseOutEnd.ToString("MM/dd/yyyy h:mm:ss tt") + "'";
				if (dateOfCloseoutStart != DateTime.MinValue)
				{
					strDateRange += " AND LastYearReceiptDate > '" + dateOfCloseoutStart.ToString("MM/dd/yyyy h:mm:ss tt") + "'";
				}
			}
			else
			{
				strMaster = "CheckMaster";
				strArchive = "Archive";
				strReceipt = "Receipt";
				strDateField = "";
				dateOfCloseOutEnd = DateTime.MinValue;
				dateOfCloseoutStart = DateTime.MinValue;
				strDateRange = "";
			}
			lngChkCnt = GetCheckCount();
			// kk05242016 trocrs-44
			strSQL = "SELECT sum(ckmast.Amount) as sumAmt, Bank.ID as BankKey " + 
			         "FROM (" + strMaster + " ckmast INNER JOIN Bank ON ckmast.BankNumber = Bank.ID) INNER JOIN (SELECT DISTINCT ReceiptNumber FROM " + strArchive + " WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + ") as Arch ON ckmast.ReceiptNumber = Arch.ReceiptNumber " +
			         "WHERE ISNULL(EFT,0) = 0 GROUP BY Bank.ID";
			rsData2.OpenRecordset(strSQL);
			switch (modGlobal.Statics.gintCheckReportOrder)
			{
				case 0:
					{
						// by receipt
						strSQL = "SELECT rcpt.ReceiptNumber as RNum, " + strDateField + " ckmast.ReceiptNumber, ckmast.CheckNumber as CNum, ckmast.Amount as Amt, Bank.RoutingTransit as BNum, Bank.ID as BKey " + 
						         "FROM ((" + strMaster + " ckmast INNER JOIN Bank ON ckmast.BankNumber = Bank.ID) INNER JOIN (SELECT DISTINCT ReceiptNumber FROM " + strArchive + " WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + ") AS Arch ON ckmast.ReceiptNumber = Arch.ReceiptNumber) INNER JOIN " + strReceipt + " rcpt ON Arch.ReceiptNumber = rcpt.ReceiptKey " + strDateRange + " " +
						         "WHERE ISNULL(ckmast.EFT,0) = 0 AND Bank.ID = " + FCConvert.ToString(Conversion.Val(this.UserData));
						break;
					}
				case 1:
					{
						// by check number
						strSQL = "SELECT rcpt.ReceiptNumber as RNum, " + strDateField + " ckmast.ReceiptNumber, ckmast.CheckNumber as CNum, ckmast.Amount as Amt, Bank.RoutingTransit as BNum, Bank.ID as BKey " + 
						         "FROM ((" + strMaster + " ckmast INNER JOIN Bank ON ckmast.BankNumber = Bank.ID) INNER JOIN (SELECT DISTINCT ReceiptNumber FROM " + strArchive + " WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + ") AS Arch ON ckmast.ReceiptNumber = Arch.ReceiptNumber) INNER JOIN " + strReceipt + " rcpt ON Arch.ReceiptNumber = rcpt.ReceiptKey " + strDateRange + " " +
								 "WHERE ISNULL(ckmast.EFT,0) = 0 AND Bank.ID = " + FCConvert.ToString(Conversion.Val(this.UserData)) + " ORDER BY CheckNumber";
						break;
					}
				case 2:
					{
						// by amount
						strSQL = "SELECT sum(ckmast.Amount) as sumAmt " + 
						         "FROM  " + strMaster + " ckmast INNER JOIN (SELECT DISTINCT ReceiptNumber FROM " + strArchive + " " + 
						         "WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + ") as Arch ON ckmast.ReceiptNumber = Arch.ReceiptNumber WHERE ISNULL(EFT,0) = 0";
						// do not group
						rsData2.OpenRecordset(strSQL);
						strSQL = "SELECT rcpt.receiptNumber as RNum, " + strDateField + " ckmast.ReceiptNumber, ckmast.CheckNumber as CNum, ckmast.Amount as Amt, Bank.RoutingTransit as BNum, Bank.ID as BKey " +
						         "FROM ((" + strMaster + " ckmast INNER JOIN Bank ON ckmast.BankNumber = Bank.ID) INNER JOIN (SELECT DISTINCT ReceiptNumber FROM " + strArchive + " WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + ") AS Arch ON ckmast.ReceiptNumber = Arch.ReceiptNumber) INNER JOIN " + strReceipt + " rcpt ON Arch.ReceiptNumber = rcpt.ReceiptKey " + strDateRange + " " +
								 "WHERE ISNULL(ckmast.EFT,0) = 0 ORDER BY isnull(Amount, 0)";
						// no bank splits
						break;
					}
			}
			//end switch
			rsData.OpenRecordset(strSQL);
			SetData();
		}

		private void FillDetail()
		{
			clsDRWrapper rsOwner = new clsDRWrapper();
			// this will actually put the data into the detail section
			// TODO Get_Fields: Field [Amt] not found!! (maybe it is an alias?)
			fldAmount.Text = Strings.Format(rsData.Get_Fields("Amt"), "#,##0.00");
			// TODO Get_Fields: Field [BNum] not found!! (maybe it is an alias?)
			fldBankNumber.Text = FCConvert.ToString(rsData.Get_Fields("BNum"));
			// TODO Get_Fields: Field [CNum] not found!! (maybe it is an alias?)
			fldCheckNumber.Text = FCConvert.ToString(rsData.Get_Fields("CNum"));
			// TODO Get_Fields: Field [RNum] not found!! (maybe it is an alias?)
			fldReceiptNumber.Text = FCConvert.ToString(rsData.Get_Fields("RNum"));
			// MAL@20071008: Corrected the way that the receipt record is selected
			// Call Reference: 117014 (& various others)
			// rsOwner.OpenRecordset "SELECT * FROM Receipt WHERE ReceiptNumber = " & rsData.Fields("RNum"), "TWCR0000.vb1"
			if (boolUsePrevYears)
			{
				rsOwner.OpenRecordset("SELECT * FROM LastYearReceipt WHERE ReceiptKey = " + rsData.Get_Fields_Int32("ReceiptNumber") + " AND LastYearReceiptDate = '" + rsData.Get_Fields_DateTime("LastYearReceiptDate").ToString("MM/dd/yyyy h:mm:ss tt") + "'" , "TWCR0000.vb1");
			}
			else
			{
				rsOwner.OpenRecordset("SELECT * FROM Receipt WHERE ReceiptKey = " + rsData.Get_Fields_Int32("ReceiptNumber"), "TWCR0000.vb1");
			}
			if (rsOwner.EndOfFile() != true && rsOwner.BeginningOfFile() != true)
			{
				fldName.Text = rsOwner.Get_Fields_String("PaidBy");
			}
			else
			{
				fldName.Text = "";
			}
			rsOwner.DisposeOf();
		}

		private void SetData()
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			switch (modGlobal.Statics.gintCheckReportOrder)
			{
				case 0:
				case 1:
					{
						if (rsData2.FindFirstRecord("BankKey", Conversion.Val(this.UserData)))
						{
							if (!rsData2.NoMatch)
							{
								// kk trocrs-7   If Not rsData.NoMatch Then
								// TODO Get_Fields: Field [sumAmt] not found!! (maybe it is an alias?)
								fldBankTotal.Text = Strings.Format(rsData2.Get_Fields("sumAmt"), "#,##0.00");
								fldCheckCount.Text = FCConvert.ToString(lngChkCnt);
								// kk05242016 trocrs-44
								// TODO Get_Fields: Field [BankKey] not found!! (maybe it is an alias?)
								rsTemp.OpenRecordset("SELECT * FROM Bank WHERE ID = " + rsData2.Get_Fields("BankKey"));
								if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
								{
									if (Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Name"))) != "")
									{
										lblBankNumberTotal.Text = rsTemp.Get_Fields_String("Name") + " Total:";
										lblBankNumber.Text = rsTemp.Get_Fields_String("Name");
									}
									else
									{
										lblBankNumberTotal.Text = "Bank Total:";
										lblBankNumber.Text = "Bank Number";
									}
								}
								else
								{
									lblBankNumberTotal.Text = "Bank Total:";
									lblBankNumber.Text = "Bank Number";
								}
							}
							else
							{
								lblBankNumberTotal.Text = "Bank Total:";
								lblBankNumber.Text = "Bank Number";
								fldBankTotal.Text = "ERROR";
								fldCheckCount.Text = "";
								// kk05242016 trocrs-44
							}
						}
						else
						{
							lblBankNumberTotal.Text = "Bank Total:";
							lblBankNumber.Text = "Bank Number";
							fldBankTotal.Text = "ERROR";
							fldCheckCount.Text = "";
							// kk05242016 trocrs-44
						}
						break;
					}
				default:
					{
						if (!rsData2.EndOfFile())
						{
							// TODO Get_Fields: Field [sumAmt] not found!! (maybe it is an alias?)
							fldBankTotal.Text = Strings.Format(rsData2.Get_Fields("sumAmt"), "#,##0.00");
							fldCheckCount.Text = FCConvert.ToString(lngChkCnt);
							// kk05242016 trocrs-44
						}
						else
						{
							fldBankTotal.Text = "ERROR";
							fldCheckCount.Text = "";
							// kk05242016 trocrs-44
						}
						lblBankNumberTotal.Text = "All Banks - Total:";
						lblBankNumber.Text = "All Banks";
						break;
					}
			}
			rsTemp.DisposeOf();
		}

		private int GetCheckCount()
		{
			int GetCheckCount = 0;
			string strTemp;
			clsDRWrapper rsChkCnt = new clsDRWrapper();
			clsDRWrapper rsRevChkCnt = new clsDRWrapper();
			//clsDRWrapper rsArchRev = new clsDRWrapper();
			bool boolNoMatch;
			int lngTmpCnt = 0;
			string strMaster;
			string strArchive;
			string strReceipt;

			boolUsePrevYears = frmDailyReceiptAudit.InstancePtr.boolUsePrevYears;
			if (boolUsePrevYears)
			{
				strMaster = "LastYearCheckMaster";
				strArchive = "LastYearArchive";
				strReceipt = "LastYearReceipt";
			}
			else
			{
				strMaster = "CheckMaster";
				strArchive = "Archive";
				strReceipt = "Receipt";
			}
			strTemp = "SELECT Count(cm.CheckNumber) as CheckCount " + 
			          "FROM " + strMaster + " cm INNER JOIN (SELECT DISTINCT ReceiptNumber FROM " + strArchive + " WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + ") as Arch ON cm.ReceiptNumber = Arch.ReceiptNumber " + 
			          "WHERE Amount > 0 AND ISNULL(EFT,0) = 0";
			if (modGlobal.Statics.gintCheckReportOrder != 2)
			{
				strTemp += " AND cm.BankNumber = " + FCConvert.ToString(Conversion.Val(this.UserData));
			}
			rsChkCnt.OpenRecordset(strTemp);
			if (!rsChkCnt.EndOfFile())
			{
				// TODO Get_Fields: Field [CheckCount] not found!! (maybe it is an alias?)
				lngTmpCnt = FCConvert.ToInt32(Math.Round(Conversion.Val(rsChkCnt.Get_Fields("CheckCount"))));
				strTemp = "SELECT r.ReceiptNumber, cm.ReceiptNumber as RKey, cm.CheckNumber, cm.Amount, cm.BankNumber as BKey " + 
				          "FROM (" + strMaster + " cm INNER JOIN (SELECT DISTINCT ReceiptNumber FROM " + strArchive + " WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + ") AS Arch ON cm.ReceiptNumber = Arch.ReceiptNumber) INNER JOIN " + strReceipt + " r ON Arch.ReceiptNumber = r.ReceiptKey " + 
				          "WHERE cm.Amount < 0 AND ISNULL(cm.EFT,0) = 0";
				if (modGlobal.Statics.gintCheckReportOrder != 2)
				{
					strTemp += " AND cm.BankNumber = " + FCConvert.ToString(Conversion.Val(this.UserData));
				}
				rsRevChkCnt.OpenRecordset(strTemp);
				while (!rsRevChkCnt.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
					// TODO Get_Fields: Field [BKey] not found!! (maybe it is an alias?)
					strTemp = "SELECT r.ReceiptNumber, cm.ReceiptNumber as RKey, cm.CheckNumber, cm.Amount, cm.BankNumber as BKey " + 
					          "FROM (" + strMaster + " cm INNER JOIN (SELECT DISTINCT ReceiptNumber FROM " + strArchive + " WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + ") AS Arch ON cm.ReceiptNumber = Arch.ReceiptNumber) INNER JOIN " + strReceipt + " r ON Arch.ReceiptNumber = r.ReceiptKey " + 
					          "WHERE cm.Amount = " + FCConvert.ToString(Conversion.Val(rsRevChkCnt.Get_Fields("Amount")) * -1) + " AND cm.CheckNumber = '" + rsRevChkCnt.Get_Fields("CheckNumber") + "' AND BankNUmber = " + rsRevChkCnt.Get_Fields("BKey") + " AND ISNULL(cm.EFT,0) = 0";
					if (modGlobal.Statics.gintCheckReportOrder != 2)
					{
						strTemp += " AND cm.BankNumber = " + FCConvert.ToString(Conversion.Val(this.UserData));
					}
					rsChkCnt.OpenRecordset(strTemp);
					if (!rsChkCnt.EndOfFile())
					{
						// kk04032017 trocrs-44  Rework - Per Brenda don't perform additional checking, just Check # and amount
						// Additional checking...  same receipt type(s) and account(s)
						// strTemp = "SELECT AccountNumber, ReceiptType, Ref, CheckPaidAmount FROM Archive WHERE ReceiptNumber = " & rsChkCnt.Fields("RKey")
						// rsChkCnt.OpenRecordset strTemp
						// If Not rsChkCnt.EndOfFile Then
						// strTemp = "SELECT AccountNumber, ReceiptType, Ref, CheckPaidAmount FROM Archive WHERE ReceiptNumber = " & .Fields("RKey")
						// rsArchRev.OpenRecordset strTemp
						// If Not rsArchRev.EndOfFile Then
						// If rsArchRev.RecordCount = rsChkCnt.RecordCount Then
						// boolNoMatch = False
						// Do While Not rsChkCnt.EndOfFile
						// rsArchRev.FindFirstRecord2 "AccountNumber,ReceiptType,Ref,CheckPaidAmount",
						// rsChkCnt.Fields("AccountNumber") & "," & rsChkCnt.Fields("ReceiptType") & "," & rsChkCnt.Fields("Ref") & "," & rsChkCnt.Fields("CheckPaidAmount") * -1,
						// ","
						// If rsArchRev.NoMatch Then
						// boolNoMatch = True
						// Exit Do
						// End If
						// rsChkCnt.MoveNext
						// Loop
						// If Not boolNoMatch Then
						lngTmpCnt -= 1;
						// End If
						// End If
						// End If
						// End If
					}
					rsRevChkCnt.MoveNext();
				}
			}
			GetCheckCount = lngTmpCnt;
            rsChkCnt.DisposeOf();
            rsRevChkCnt.DisposeOf();
			return GetCheckCount;
		}

		private DateTime GetPrevCloseOutDate(int lngDCO)
		{
			clsDRWrapper rsCO = new clsDRWrapper();
			DateTime closeOutDate = DateTime.MinValue;

			try
			{
				rsCO.OpenRecordset(
					"SELECT TOP 1 ID FROM CloseOut WHERE ID < " + lngDCO + " AND Type = 'R' ORDER BY ID DESC",
					modExtraModules.strCRDatabase);
				if (!rsCO.EndOfFile())
				{
					closeOutDate = GetCloseOutDate(rsCO.Get_Fields_Int32("ID"));
				}
			}
			catch (Exception ex)
			{
				//  MsgBox "Error #" & Err.Number & " - " & Err.Description & ".", vbCritical, "Error Getting Previous Close Out Date"
			}
			rsCO.DisposeOf();
			return closeOutDate;
		}

		private DateTime GetCloseOutDate(int lngDCO)
		{
			clsDRWrapper rsD = new clsDRWrapper();
			DateTime closeOutDate = DateTime.MinValue;

			try
			{
				rsD.OpenRecordset("SELECT CloseOutDate FROM CloseOut WHERE ID = " + lngDCO, modExtraModules.strCRDatabase);
				if (!rsD.EndOfFile())
				{
					DateTime.TryParse(rsD.Get_Fields_String("CloseOutDate"), out closeOutDate);
				}
			}
			catch (Exception e)
			{
				//  MsgBox "Error #" & Err.Number & " - " & Err.Description & ".", vbCritical, "Error Getting Close Out Date"
			}
			rsD.DisposeOf();
			return closeOutDate;
		}

		private void sarCheckReportDetail_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//sarCheckReportDetail.Caption	= "Check Detail";
			//sarCheckReportDetail.Icon	= "sarCheckReportDetail.dsx":0000";
			//sarCheckReportDetail.Left	= 0;
			//sarCheckReportDetail.Top	= 0;
			//sarCheckReportDetail.Width	= 28680;
			//sarCheckReportDetail.Height	= 15690;
			//sarCheckReportDetail.StartUpPosition	= 3;
			//sarCheckReportDetail.SectionData	= "sarCheckReportDetail.dsx":058A;
			//End Unmaped Properties
		}
	}
}
