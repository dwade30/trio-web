﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using System.IO;
using fecherFoundation.VisualBasicLayer;

namespace TWCR0000
{
	public class modMoses
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               07/19/2006              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               01/19/2007              *
		// ********************************************************
		public struct MOSESReceipt
		{
			// vbPorter upgrade warning: ReceiptNumber As int	OnRead(string)
			public int ReceiptNumber;
			// vbPorter upgrade warning: dtTransDate As string	OnWrite(string, DateTime)
			public string dtTransDate;
			public string strTransType;
			// vbPorter upgrade warning: AuthItemCode As string	OnRead(string, int)
			public string AuthItemCode;
			public string strCustName;
			public string lngDocNumber;
			public string strRegNumber;
			public string strInvetoryItemName;
			public string strInvSerialNum;
			public string strInvSerialNum2;
			public string strBlank1;
			public string strClerkID;
			// vbPorter upgrade warning: curStateFee As Decimal	OnWrite(string)	OnReadFCConvert.ToDouble(
			public Decimal curStateFee;
			// vbPorter upgrade warning: curAgentFee As Decimal	OnWrite(string)	OnReadFCConvert.ToDouble(
			public Decimal curAgentFee;
			// vbPorter upgrade warning: curExiseTax As Decimal	OnWrite(string)	OnReadFCConvert.ToDouble(
			public Decimal curExiseTax;
			// vbPorter upgrade warning: curSalesTax As Decimal	OnWrite(double, short)	OnReadFCConvert.ToDouble(
			public Decimal curSalesTax;
			public string strPaymentType;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public MOSESReceipt(int unusedParam)
			{
				this.ReceiptNumber = 0;
				this.dtTransDate = string.Empty;
				this.strTransType = string.Empty;
				this.AuthItemCode = string.Empty;
				this.strCustName = string.Empty;
				this.lngDocNumber = string.Empty;
				this.strRegNumber = string.Empty;
				this.strInvetoryItemName = string.Empty;
				this.strInvSerialNum = string.Empty;
				this.strInvSerialNum2 = string.Empty;
				this.strBlank1 = string.Empty;
				this.strClerkID = string.Empty;
				this.curStateFee = 0;
				this.curAgentFee = 0;
				this.curExiseTax = 0;
				this.curSalesTax = 0;
				this.strPaymentType = string.Empty;
			}
		};
		// Public arrMOSESStruct() As MOSESReceipt
		// vbPorter upgrade warning: intNumberOfReceipts As short	OnWriteFCConvert.ToInt32(
		public static bool ParseMosesReceipt(string strFilename, ref MOSESReceipt[] arrMOSESStruct, ref int intNumberOfReceipts)
		{
			bool ParseMosesReceipt = false;
			StreamReader ts = null;
			try
			{
				// On Error GoTo ERROR_HANDLER
				string[] txtLine = null;
				int strMaxPos = 0;
				int strCurPos = 0;
				int intCT = 0;
				bool boolFirstPass = false;
				ParseMosesReceipt = true;
				if (strFilename != "")
				{
					ts = File.OpenText(strFilename);
					Array.Resize(ref txtLine, intNumberOfReceipts + 1);
					txtLine[intNumberOfReceipts] = ts.ReadLine();
					strMaxPos = txtLine[intNumberOfReceipts].Length;
					boolFirstPass = true;
					while (!ts.EndOfStream || boolFirstPass || Statics.boolFullLine)
					{
						intCT += 1;
						boolFirstPass = false;
						// Receipt Number
						strCurPos = Strings.InStr(4, txtLine[intNumberOfReceipts], ",", CompareConstants.vbTextCompare);
						arrMOSESStruct[intCT].ReceiptNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(txtLine[intNumberOfReceipts], strCurPos))));
						txtLine[intNumberOfReceipts] = Strings.Mid(txtLine[intNumberOfReceipts], strCurPos + 1, strMaxPos);
						// Trans Date
						strMaxPos = txtLine[intNumberOfReceipts].Length;
						strCurPos = Strings.InStr(4, txtLine[intNumberOfReceipts], ",", CompareConstants.vbTextCompare);
						if (Strings.Trim(Strings.Left(txtLine[intNumberOfReceipts], strCurPos - 1)) != "")
						{
							arrMOSESStruct[intCT].dtTransDate = Strings.Trim(Strings.Left(txtLine[intNumberOfReceipts], strCurPos - 1));
						}
						else
						{
							arrMOSESStruct[intCT].dtTransDate = FCConvert.ToString(DateTime.Today);
						}
						txtLine[intNumberOfReceipts] = Strings.Mid(txtLine[intNumberOfReceipts], strCurPos + 1, strMaxPos);
						// TransType
						strMaxPos = txtLine[intNumberOfReceipts].Length;
						strCurPos = Strings.InStr(4, txtLine[intNumberOfReceipts], ",", CompareConstants.vbTextCompare);
						arrMOSESStruct[intCT].strTransType = Strings.Trim(Strings.Left(txtLine[intNumberOfReceipts], strCurPos - 1));
						txtLine[intNumberOfReceipts] = Strings.Mid(txtLine[intNumberOfReceipts], strCurPos + 1, strMaxPos);
						// Athority Item Code
						strMaxPos = txtLine[intNumberOfReceipts].Length;
						strCurPos = Strings.InStr(4, txtLine[intNumberOfReceipts], ",", CompareConstants.vbTextCompare);
						arrMOSESStruct[intCT].AuthItemCode = Strings.Trim(Strings.Left(txtLine[intNumberOfReceipts], strCurPos - 1));
						txtLine[intNumberOfReceipts] = Strings.Mid(txtLine[intNumberOfReceipts], strCurPos + 1, strMaxPos);
						// Customer Name
						strMaxPos = txtLine[intNumberOfReceipts].Length;
						strCurPos = Strings.InStr(4, txtLine[intNumberOfReceipts], ",", CompareConstants.vbTextCompare);
						arrMOSESStruct[intCT].strCustName = Strings.Trim(Strings.Left(txtLine[intNumberOfReceipts], strCurPos - 1));
						txtLine[intNumberOfReceipts] = Strings.Mid(txtLine[intNumberOfReceipts], strCurPos + 1, strMaxPos);
						// Document Number
						strMaxPos = txtLine[intNumberOfReceipts].Length;
						strCurPos = Strings.InStr(1, txtLine[intNumberOfReceipts], ",", CompareConstants.vbTextCompare);
						arrMOSESStruct[intCT].lngDocNumber = Strings.Trim(Strings.Left(txtLine[intNumberOfReceipts], strCurPos - 1));
						txtLine[intNumberOfReceipts] = Strings.Mid(txtLine[intNumberOfReceipts], strCurPos + 1, strMaxPos);
						// Registration number for Registration
						strMaxPos = txtLine[intNumberOfReceipts].Length;
						strCurPos = Strings.InStr(4, txtLine[intNumberOfReceipts], ",", CompareConstants.vbTextCompare);
						arrMOSESStruct[intCT].strRegNumber = Strings.Trim(Strings.Left(txtLine[intNumberOfReceipts], strCurPos - 1));
						txtLine[intNumberOfReceipts] = Strings.Mid(txtLine[intNumberOfReceipts], strCurPos + 1, strMaxPos);
						// Inventory Item Name
						strMaxPos = txtLine[intNumberOfReceipts].Length;
						strCurPos = Strings.InStr(4, txtLine[intNumberOfReceipts], ",", CompareConstants.vbTextCompare);
						arrMOSESStruct[intCT].strInvetoryItemName = Strings.Left(txtLine[intNumberOfReceipts], strCurPos - 1);
						txtLine[intNumberOfReceipts] = Strings.Mid(txtLine[intNumberOfReceipts], strCurPos + 1, strMaxPos);
						// Inventory Serial Number
						strMaxPos = txtLine[intNumberOfReceipts].Length;
						strCurPos = Strings.InStr(1, txtLine[intNumberOfReceipts], ",", CompareConstants.vbTextCompare);
						arrMOSESStruct[intCT].strInvSerialNum = Strings.Trim(Strings.Left(txtLine[intNumberOfReceipts], strCurPos - 1));
						txtLine[intNumberOfReceipts] = Strings.Mid(txtLine[intNumberOfReceipts], strCurPos + 1, strMaxPos);
						// Inventory Serial Number2
						strMaxPos = txtLine[intNumberOfReceipts].Length;
						strCurPos = Strings.InStr(1, txtLine[intNumberOfReceipts], ",", CompareConstants.vbTextCompare);
						arrMOSESStruct[intCT].strInvSerialNum2 = Strings.Left(txtLine[intNumberOfReceipts], strCurPos - 1);
						txtLine[intNumberOfReceipts] = Strings.Mid(txtLine[intNumberOfReceipts], strCurPos + 1, strMaxPos);
						// Blank Field
						strMaxPos = txtLine[intNumberOfReceipts].Length;
						strCurPos = Strings.InStr(1, txtLine[intNumberOfReceipts], ",", CompareConstants.vbTextCompare);
						arrMOSESStruct[intCT].strBlank1 = Strings.Left(txtLine[intNumberOfReceipts], strCurPos - 1);
						txtLine[intNumberOfReceipts] = Strings.Mid(txtLine[intNumberOfReceipts], strCurPos + 1, strMaxPos);
						// ClerkID
						strMaxPos = txtLine[intNumberOfReceipts].Length;
						strCurPos = Strings.InStr(1, txtLine[intNumberOfReceipts], ",", CompareConstants.vbTextCompare);
						arrMOSESStruct[intCT].strClerkID = Strings.Left(txtLine[intNumberOfReceipts], strCurPos - 1);
						txtLine[intNumberOfReceipts] = Strings.Mid(txtLine[intNumberOfReceipts], strCurPos + 1, strMaxPos);
						// State Fee
						strMaxPos = txtLine[intNumberOfReceipts].Length;
						strCurPos = Strings.InStr(1, txtLine[intNumberOfReceipts], ",", CompareConstants.vbTextCompare);
						arrMOSESStruct[intCT].curStateFee = FCConvert.ToDecimal(Strings.Left(txtLine[intNumberOfReceipts], strCurPos - 1));
						txtLine[intNumberOfReceipts] = Strings.Mid(txtLine[intNumberOfReceipts], strCurPos + 1, strMaxPos);
						// Agent Fee
						strMaxPos = txtLine[intNumberOfReceipts].Length;
						strCurPos = Strings.InStr(4, txtLine[intNumberOfReceipts], ",", CompareConstants.vbTextCompare);
						arrMOSESStruct[intCT].curAgentFee = FCConvert.ToDecimal(Strings.Left(txtLine[intNumberOfReceipts], strCurPos - 1));
						txtLine[intNumberOfReceipts] = Strings.Mid(txtLine[intNumberOfReceipts], strCurPos + 1, strMaxPos);
						// Excise Tax
						strMaxPos = txtLine[intNumberOfReceipts].Length;
						strCurPos = Strings.InStr(4, txtLine[intNumberOfReceipts], ",", CompareConstants.vbTextCompare);
						arrMOSESStruct[intCT].curExiseTax = FCConvert.ToDecimal(Strings.Left(txtLine[intNumberOfReceipts], strCurPos - 1));
						txtLine[intNumberOfReceipts] = Strings.Mid(txtLine[intNumberOfReceipts], strCurPos + 1, strMaxPos);
						// Sales Tax
						strMaxPos = txtLine[intNumberOfReceipts].Length;
						strCurPos = Strings.InStr(4, txtLine[intNumberOfReceipts], ",", CompareConstants.vbTextCompare);
						if (strCurPos != 0)
						{
							arrMOSESStruct[intCT].curSalesTax = FCConvert.ToDecimal(FCConvert.ToDouble(Strings.Left(txtLine[intNumberOfReceipts], strCurPos - 1)));
						}
						else
						{
							arrMOSESStruct[intCT].curSalesTax = 0;
						}
						txtLine[intNumberOfReceipts] = Strings.Mid(txtLine[intNumberOfReceipts], strCurPos + 1, strMaxPos);
						// Payment Type
						strMaxPos = txtLine[intNumberOfReceipts].Length;
						strCurPos = Strings.InStr(4, txtLine[intNumberOfReceipts], ",", CompareConstants.vbTextCompare);
						arrMOSESStruct[intCT].strPaymentType = Strings.Trim(Strings.Left(txtLine[intNumberOfReceipts], strMaxPos - 1));
						txtLine[intNumberOfReceipts] = Strings.Trim(Strings.Mid(txtLine[intNumberOfReceipts], strCurPos + 1, strMaxPos));
						intNumberOfReceipts += 1;
						Statics.boolFullLine = false;
						if (!ts.EndOfStream)
						{
							Array.Resize(ref txtLine, intNumberOfReceipts + 1);
							txtLine[intNumberOfReceipts] = ts.ReadLine();
							strMaxPos = txtLine[intNumberOfReceipts].Length;
							if (txtLine[intNumberOfReceipts] != "")
							{
								Statics.boolFullLine = true;
							}
						}
					}
				}
				return ParseMosesReceipt;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				ParseMosesReceipt = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Parsing MOSES File", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			finally
			{
				if (ts != null)
				{
					ts.Close();
				}
			}
			return ParseMosesReceipt;
		}

		public static void FillMOSESTypesTable()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// This will fill the MOSES Type Table
				clsDRWrapper rsM = new clsDRWrapper();
				string strDesc;
				int lngType;
				lngType = 1472;
				strDesc = "Boat Registration Transfer";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1478;
				strDesc = "Expanded Archery Antlerless Deer";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1479;
				strDesc = "Expanded Archery Antlered Deer";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1480;
				strDesc = "Boat Up to 10 Hsp. Registration Renewal";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1481;
				strDesc = "Boat 11-50 Hsp. Registration Renewal";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1483;
				strDesc = "Boat PWC Registration Renewal";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1484;
				strDesc = "Boat 11-50 Hsp. Registration New/Rollover";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1486;
				strDesc = "Boat Pwc Registration New/Rollover";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1487;
				strDesc = "Boat Duplicate Stickers";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1488;
				strDesc = "Boat Duplicate Registration";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1490;
				strDesc = "Boat Up To 10 Hsp. Registration New/Rollover";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1502;
				strDesc = "Nonresident ATV Registration New and Rollover";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1508;
				strDesc = "Nonresident ATV Duplicate Stickers";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1509;
				strDesc = "Nonresident ATV Duplicate Registration and Stickers";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1511;
				strDesc = "Resident Snowmobile Duplicate Registration";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1513;
				strDesc = "Resident Snowmobile Duplicate Sticker";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1515;
				strDesc = "Nonresident ATV Transfer Registration";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1525;
				strDesc = "Nonresident ATV Duplicate Registration";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1533;
				strDesc = "Nonresident ATV Registration Renewal";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1553;
				strDesc = "Nonresident ATV Registration";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1564;
				strDesc = "Nonresident Snowmobile Transfer Registration";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1565;
				strDesc = "Nonresident 3-Day Snowmobile Registration";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1566;
				strDesc = "Nonresident 10-Day Snowmobile Registration";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1567;
				strDesc = "Nonresident Season Snowmobile Registration";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1568;
				strDesc = "Nonresident Snowmobile Duplicate Registration and Sticker";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1572;
				strDesc = "Resident Snowmobile Registration - New/Rollover";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1575;
				strDesc = "Resident Snowmobile Registration - Renewal";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1580;
				strDesc = "ATV Registration Renewal";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1581;
				strDesc = "ATV Registration New/Rollover";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1582;
				strDesc = "Resident ATV Duplicate Registration";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1583;
				strDesc = "Resident ATV Duplicate Stickers";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1584;
				strDesc = "Resident ATV Transfer Registration";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1589;
				strDesc = "Boat Duplicate Registration & Stickers";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1590;
				strDesc = "Snowmobile Duplicate Registration & Stickers";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1591;
				strDesc = "Resident ATV Duplicate Registration & Stickers";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1688;
				strDesc = "Resident Atlantic Salmon";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1689;
				strDesc = "Non-Resident Atlantic Salmon";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1690;
				strDesc = "Non-Resident 3-Day Atlantic Salmon";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1691;
				strDesc = "Non-Resident Junior Atlantic Salmon";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1692;
				strDesc = "Resident Crossbow Hunting";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1693;
				strDesc = "Nonresident Crossbow Hunting";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1694;
				strDesc = "Alien Crossbow Hunting";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1695;
				strDesc = "Resident Fall Turkey Hunting Permit";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1696;
				strDesc = "Nonresident Fall Turkey Hunting Permit";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1697;
				strDesc = "Resident Spring Turkey";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1698;
				strDesc = "Nonresident Spring Turkey";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1699;
				strDesc = "Resident Superpack";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1704;
				strDesc = "Pheasant Hunting Permit";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1705;
				strDesc = "Resident Hunting & Fishing Combo (Exchanged 1-Day Fish)";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1706;
				strDesc = "Resident Hunting & Fishing Combo (Exchanged 3-Day Fish)";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1708;
				strDesc = "Nonresident Small Game Hunting";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1710;
				strDesc = "Res Service Depend Hunting and Fishing Combo";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1713;
				strDesc = "Migratory Waterfowl Hunting Permit";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1720;
				strDesc = "Alien Hunting & Fishing Combo";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1721;
				strDesc = "Alien Season Fishing";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1722;
				strDesc = "Alien Small Game Hunting";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1723;
				strDesc = "Alien Archery Hunting";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1729;
				strDesc = "Resident Serviceman Dependent Fishing";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1730;
				strDesc = "Resident Serviceman Dependent Hunting";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1744;
				strDesc = "Resident Archery Hunting & Fishing Combo";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1745;
				strDesc = "Supersport License";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1748;
				strDesc = "Nonresident 3-Day Small Game Hunting";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1749;
				strDesc = "Resident Small Game Hunting";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1750;
				strDesc = "Resident Junior Hunting";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1751;
				strDesc = "Resident Hunting & Fishing Combo";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1752;
				strDesc = "Resident Hunting";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1753;
				strDesc = "Resident Fishing";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1754;
				strDesc = "Resident Archery Hunting";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1755;
				strDesc = "Nonresident Junior Hunting";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1757;
				strDesc = "Nonresident Big Game Hunting";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1758;
				strDesc = "Nonresident Archery Hunting";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1759;
				strDesc = "Alien Big Game Hunting";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1762;
				strDesc = "1-Day Fishing";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1763;
				strDesc = "Resident Fishing (Exchanged 1-Day Fish)";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1765;
				strDesc = "Nonresident 7-Day Fishing";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1766;
				strDesc = "Nonresident Season Fishing";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1767;
				strDesc = "Nonresident 15-Day Fishing";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1768;
				strDesc = "Nonresident Season Fishing (Exchanged 15-Day Fish)";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1769;
				strDesc = "3-Day Fishing";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1771;
				strDesc = "Nonresident Junior Fishing";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1772;
				strDesc = "Nonresident Hunting & Fishing Combo";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1780;
				strDesc = "Lake & River Protection sticker - Maine Registered Boats";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1782;
				strDesc = "Coyote Night Hunting Permit";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1783;
				strDesc = "Lake & River Protection sticker - Other Registered Boats";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1786;
				strDesc = "Resident Muzzleloader Hunting";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1787;
				strDesc = "Nonresident Muzzleloader Hunting";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1788;
				strDesc = "Alien Muzzleloader Hunting";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1790;
				strDesc = "Resident Serviceman Hunt and Fish Combo";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1796;
				strDesc = "Resident Bear Hunting";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1797;
				strDesc = "Nonresident Bear Hunting";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1800;
				strDesc = "Resident Fishing (Exchanged 3-Day Fish)";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1801;
				strDesc = "Duplicate Recreational License";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1809;
				strDesc = "Complimentary Resident Muzzleloader Hunting Permit";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1810;
				strDesc = "Complimentary Pheasant Hunting Permit";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1813;
				strDesc = "Complimentary Bear Hunt";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1821;
				strDesc = "Complimentary Migratory Waterfowl Hunting Permit";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1825;
				strDesc = "Migratory Waterfowl Upgrade Card";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1851;
				strDesc = "Comp Fall Turkey";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1901;
				strDesc = "Resident Over-70 Lifetime License ";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1917;
				strDesc = "Boat 51-115 Hsp Reg Renewal";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1918;
				strDesc = "Boat 51-115 Hsp Reg (Rollover to Spouse)";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1921;
				strDesc = "Boat Over 115 Hsp Renewal";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1922;
				strDesc = "Boat Over 115 Hsp Reg (Rollover to Spouse)";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1923;
				strDesc = "Boat 51-115 Hsp Reg New/Rollover";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 1924;
				strDesc = "Boat Over 115 Hsp Reg New/Rollover";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 2304;
				strDesc = "Boat Up to 10 Hsp. Registration (Rollover to Spouse)";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 2305;
				strDesc = "Boat 11-50 Hsp. Registration (Rollover to Spouse)";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 2306;
				strDesc = "Boat PWC Registration (Rollover to Spouse)";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 2315;
				strDesc = "Nonresident ATV Registration (Rollover to Spouse)";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 2319;
				strDesc = "Nonresident Season Snowmobile Registration (Rollover to Spouse)";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 2321;
				strDesc = "Resident Snowmobile Registration (Rollover to Spouse)";
				CheckMOSESType(ref lngType, ref strDesc);
				lngType = 2322;
				strDesc = "Resident ATV Registration (Rollover to Spouse)";
				CheckMOSESType(ref lngType, ref strDesc);
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling MOSES Types", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private static void CheckMOSESType(ref int lngType, ref string strDesc)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsM = new clsDRWrapper();
				if (lngType > 0)
				{
					rsM.OpenRecordset("SELECT * FROM MOSESTypes WHERE MOSESType = " + FCConvert.ToString(lngType), modExtraModules.strCRDatabase);
					if (!rsM.EndOfFile())
					{
						rsM.Edit();
					}
					else
					{
						rsM.AddNew();
					}
					rsM.Set_Fields("MOSESTypeDescription", strDesc);
					rsM.Set_Fields("MOSESType", lngType);
					rsM.Update();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Checking MOSES Type", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static void MoveMOSESReceipt_26(string strFilePath, string strFilename, bool boolFromArchive = false)
		{
			MoveMOSESReceipt(ref strFilePath, ref strFilename, boolFromArchive);
		}

		public static void MoveMOSESReceipt(ref string strFilePath, ref string strFilename, bool boolFromArchive = false)
		{
			string strCmd = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (boolFromArchive)
				{
					// copy this file back into the data directory
					strCmd = "CopyFile " + Path.Combine(FCFileSystem.Statics.UserDataFolder, "MOSESArchive", strFilename) + ", " + Path.Combine(FCFileSystem.Statics.UserDataFolder, strFilename);
					File.Copy(Path.Combine(FCFileSystem.Statics.UserDataFolder, "MOSESArchive", strFilename), Path.Combine(FCFileSystem.Statics.UserDataFolder, strFilename), true);
					// delete from the archive
					strCmd = "DeleteFile " + Path.Combine(FCFileSystem.Statics.UserDataFolder, "MOSESArchive", strFilename);
					File.Delete(Path.Combine(FCFileSystem.Statics.UserDataFolder, "MOSESArchive", strFilename));
				}
				else
				{
					if (!Directory.Exists(Path.Combine(FCFileSystem.Statics.UserDataFolder, "MOSESArchive")))
					{
						strCmd = "CreateFolder MOSESArchive";
						Directory.CreateDirectory(Path.Combine(FCFileSystem.Statics.UserDataFolder, "MOSESArchive"));
					}
					if (Strings.UCase(Path.Combine(strFilePath, Path.GetFileName(strFilename))) != Strings.UCase(Path.Combine(FCFileSystem.Statics.UserDataFolder + "\\MOSESArchive", Path.GetFileName(strFilename))))
					{
						// copy this file into the MOSES archive
						strCmd = "CopyFile " + Path.Combine(strFilePath, Path.GetFileName(strFilename)) + ", " + Path.Combine(FCFileSystem.Statics.UserDataFolder + "\\MOSESArchive", Path.GetFileName(strFilename));
						File.Copy(Path.Combine(strFilePath, Path.GetFileName(strFilename)), Path.Combine(FCFileSystem.Statics.UserDataFolder + "\\MOSESArchive", Path.GetFileName(strFilename)), true);
						// delete wherever the original file
						strCmd = "DeleteFile " + Path.Combine(strFilePath, Path.GetFileName(strFilename));
						File.Delete(Path.Combine(strFilePath, Path.GetFileName(strFilename)));
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				// MsgBox "Error #" & Err.Number & " - " & Err.Description & ".", vbCritical, "Error Filling MOSES Types"
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".  The offending command is " + FCConvert.ToString(Convert.ToChar(34)) + strCmd + FCConvert.ToString(Convert.ToChar(34)), "Error Filling MOSES Types", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public class StaticVariables
		{
			public bool boolFullLine;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
