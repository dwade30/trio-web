﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for sarTellerCheckReportDetail.
	/// </summary>
	public partial class sarTellerCheckReportDetail : FCSectionReport
	{
		public static sarTellerCheckReportDetail InstancePtr
		{
			get
			{
				return (sarTellerCheckReportDetail)Sys.GetInstance(typeof(sarTellerCheckReportDetail));
			}
		}

		protected sarTellerCheckReportDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	sarTellerCheckReportDetail	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               08/17/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               08/17/2004              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsData2 = new clsDRWrapper();
		string strSQL;
		string strTeller = "";
		// vbPorter upgrade warning: lngBankKey As int	OnWrite(string, int)
		int lngBankKey;
		// vbPorter upgrade warning: curTotal As Decimal	OnWrite(short, Decimal)
		Decimal curTotal;
		int lngChkCnt;

		public sarTellerCheckReportDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += SarTellerCheckReportDetail_ReportEnd;
		}

        private void SarTellerCheckReportDetail_ReportEnd(object sender, EventArgs e)
        {
            rsData2.DisposeOf();
			rsData.DisposeOf();
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
			{
				FillDetail();
				rsData.MoveNext();
				eArgs.EOF = false;
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngTemp;
				string strTemp = "";
				lngTemp = Strings.InStr(1, FCConvert.ToString(this.UserData), "||", CompareConstants.vbBinaryCompare);
				if (lngTemp != 0)
				{
					lngBankKey = FCConvert.ToInt32(Strings.Left(FCConvert.ToString(this.UserData), lngTemp - 1));
					strTeller = Strings.Right(FCConvert.ToString(this.UserData), FCConvert.ToString(this.UserData).Length - (lngTemp + 1));
				}
				else
				{
					lngBankKey = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData)));
				}
				lngChkCnt = GetCheckCount();
				// kk05242016 trocrs-44
				strSQL = "SELECT sum(CheckMaster.Amount) as sumAmt, Bank.ID as BankKey FROM (CheckMaster INNER JOIN Bank ON CheckMaster.BankNumber = Bank.ID) INNER JOIN (SELECT DISTINCT ReceiptNumber FROM Archive WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND TellerID = '" + strTeller + "') as Arch ON CheckMaster.ReceiptNumber = Arch.ReceiptNumber WHERE ISNULL(EFT,0) = 0 GROUP BY Bank.ID";
				rsData2.OpenRecordset(strSQL);
				switch (modGlobal.Statics.gintCheckReportOrder)
				{
					case 0:
						{
							// by receipt
							strSQL = "SELECT Receipt.ReceiptNumber as RNum, CheckMaster.ReceiptNumber, CheckMaster.CheckNumber as CNum, CheckMaster.Amount as Amt, Bank.RoutingTransit as BNum, Bank.ID as BKey FROM ((CheckMaster INNER JOIN Bank ON CheckMaster.BankNumber = Bank.ID) INNER JOIN (SELECT DISTINCT ReceiptNumber FROM Archive WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND TellerID = '" + strTeller + "') AS Arch ON CheckMaster.ReceiptNumber = Arch.ReceiptNumber) INNER JOIN Receipt ON Arch.ReceiptNumber = Receipt.ReceiptKey WHERE ISNULL(CheckMaster.EFT,0) = 0 AND Bank.ID = " + FCConvert.ToString(Conversion.Val(this.UserData));
							break;
						}
					case 1:
						{
							// by check number
							strSQL = "SELECT Receipt.ReceiptNumber as RNum, CheckMaster.ReceiptNumber, CheckMaster.CheckNumber as CNum, CheckMaster.Amount as Amt, Bank.RoutingTransit as BNum, Bank.ID as BKey FROM ((CheckMaster INNER JOIN Bank ON CheckMaster.BankNumber = Bank.ID) INNER JOIN (SELECT DISTINCT ReceiptNumber FROM Archive WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND TellerID = '" + strTeller + "') AS Arch ON CheckMaster.ReceiptNumber = Arch.ReceiptNumber) INNER JOIN Receipt ON Arch.ReceiptNumber = Receipt.ReceiptKey WHERE ISNULL(CheckMaster.EFT,0) = 0 AND Bank.ID = " + FCConvert.ToString(Conversion.Val(this.UserData)) + " ORDER BY convert(int, CheckNumber)";
							break;
						}
					case 2:
						{
							// by amount
							strSQL = "SELECT sum(CheckMaster.Amount) as sumAmt FROM CheckMaster INNER JOIN (SELECT DISTINCT ReceiptNumber FROM Archive WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND TellerID = '" + strTeller + "') as Arch ON CheckMaster.ReceiptNumber = Arch.ReceiptNumber WHERE ISNULL(EFT,0) = 0";
							// do not group
							rsData2.OpenRecordset(strSQL);
							strSQL = "SELECT Receipt.ReceiptNumber as RNum, CheckMaster.ReceiptNumber, CheckMaster.CheckNumber as CNum, CheckMaster.Amount as Amt, Bank.RoutingTransit as BNum, Bank.ID as BKey FROM ((CheckMaster INNER JOIN Bank ON CheckMaster.BankNumber = Bank.ID) INNER JOIN (SELECT DISTINCT ReceiptNumber FROM Archive WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND TellerID = '" + strTeller + "') AS Arch ON CheckMaster.ReceiptNumber = Arch.ReceiptNumber) INNER JOIN Receipt ON Arch.ReceiptNumber = Receipt.ReceiptKey WHERE ISNULL(CheckMaster.EFT,0) = 0 ORDER BY Amount";
							// kk    convert(double, Amount)"    'no bank splits
							break;
						}
				}
				//end switch
				curTotal = 0;
				rsData.OpenRecordset(strSQL);
				SetData();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Showing Check Detail", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillDetail()
		{
			// this will actually put the data into the detail section
			// TODO Get_Fields: Field [Amt] not found!! (maybe it is an alias?)
			fldAmount.Text = Strings.Format(rsData.Get_Fields("Amt"), "#,##0.00");
			// TODO Get_Fields: Field [Amt] not found!! (maybe it is an alias?)
			curTotal += FCConvert.ToDecimal(rsData.Get_Fields("Amt"));
			// TODO Get_Fields: Field [BNum] not found!! (maybe it is an alias?)
			fldBankNumber.Text = FCConvert.ToString(rsData.Get_Fields("BNum"));
			// TODO Get_Fields: Field [CNum] not found!! (maybe it is an alias?)
			fldCheckNumber.Text = FCConvert.ToString(rsData.Get_Fields("CNum"));
			// TODO Get_Fields: Field [RNum] not found!! (maybe it is an alias?)
			fldReceiptNumber.Text = FCConvert.ToString(rsData.Get_Fields("RNum"));
			// rsData.Fields ("BKey")
			// rsData.Fields ("BName")
		}

		private void SetData()
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			switch (modGlobal.Statics.gintCheckReportOrder)
			{
				case 0:
				case 1:
					{
						if (rsData2.FindFirstRecord("BankKey", lngBankKey))
						{
							if (!rsData2.NoMatch)
							{
								// kk 020513 trocrs-7   If Not rsData.NoMatch Then
								// TODO Get_Fields: Field [sumAmt] not found!! (maybe it is an alias?)
								fldBankTotal.Text = Strings.Format(rsData2.Get_Fields("sumAmt"), "#,##0.00");
								fldCheckCount.Text = FCConvert.ToString(lngChkCnt);
								// kk01192017 trocrs-44
								// TODO Get_Fields: Field [BankKey] not found!! (maybe it is an alias?)
								rsTemp.OpenRecordset("SELECT * FROM Bank WHERE ID = " + rsData2.Get_Fields("BankKey"));
								if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
								{
									if (Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Name"))) != "")
									{
										lblBankNumberTotal.Text = rsTemp.Get_Fields_String("Name") + " Total:";
										lblBankNumber.Text = rsTemp.Get_Fields_String("Name");
									}
									else
									{
										lblBankNumberTotal.Text = "Bank Total:";
										lblBankNumber.Text = "Bank Number";
									}
								}
								else
								{
									lblBankNumberTotal.Text = "Bank Total:";
									lblBankNumber.Text = "Bank Number";
								}
							}
							else
							{
								lblBankNumberTotal.Text = "Bank Total:";
								lblBankNumber.Text = "Bank Number";
								fldBankTotal.Text = "ERROR";
								fldCheckCount.Text = "";
								// kk01192017 trocrs-44
							}
						}
						else
						{
							lblBankNumberTotal.Text = "Bank Total:";
							lblBankNumber.Text = "Bank Number";
							fldBankTotal.Text = "ERROR";
							fldCheckCount.Text = "";
							// kk01192017 trocrs-44
						}
						break;
					}
				default:
					{
						if (!rsData2.EndOfFile())
						{
							// TODO Get_Fields: Field [sumAmt] not found!! (maybe it is an alias?)
							fldBankTotal.Text = Strings.Format(rsData2.Get_Fields("sumAmt"), "#,##0.00");
							fldCheckCount.Text = FCConvert.ToString(lngChkCnt);
							// kk01192017 trocrs-44
						}
						else
						{
							fldBankTotal.Text = "ERROR";
							fldCheckCount.Text = "";
							// kk01192017 trocrs-44
						}
						lblBankNumberTotal.Text = "All Banks - Total:";
						lblBankNumber.Text = "All Banks";
						break;
					}
			}
			rsTemp.DisposeOf();
		}

		private int GetCheckCount()
		{
			int GetCheckCount = 0;
			string strTemp;
			clsDRWrapper rsChkCnt = new clsDRWrapper();
			clsDRWrapper rsRevChkCnt = new clsDRWrapper();
			//clsDRWrapper rsArchRev = new clsDRWrapper();
			bool boolNoMatch;
			int lngTmpCnt;
			lngTmpCnt = 0;
			strTemp = "SELECT Count(cm.CheckNumber) as CheckCount " + "FROM CheckMaster cm INNER JOIN (SELECT DISTINCT ReceiptNumber FROM Archive WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND TellerID = '" + strTeller + "') as Arch ON cm.ReceiptNumber = Arch.ReceiptNumber " + "WHERE Amount > 0 AND ISNULL(EFT,0) = 0";
			if (modGlobal.Statics.gintCheckReportOrder != 2)
			{
				strTemp += " AND cm.BankNumber = " + FCConvert.ToString(Conversion.Val(this.UserData));
			}
			rsChkCnt.OpenRecordset(strTemp);
			if (!rsChkCnt.EndOfFile())
			{
				// TODO Get_Fields: Field [CheckCount] not found!! (maybe it is an alias?)
				lngTmpCnt = FCConvert.ToInt32(Math.Round(Conversion.Val(rsChkCnt.Get_Fields("CheckCount"))));
				strTemp = "SELECT r.ReceiptNumber, cm.ReceiptNumber as RKey, cm.CheckNumber, cm.Amount, cm.BankNumber as BKey " + "FROM (CheckMaster cm INNER JOIN (SELECT DISTINCT ReceiptNumber FROM Archive WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + ") AS Arch ON cm.ReceiptNumber = Arch.ReceiptNumber) INNER JOIN Receipt r ON Arch.ReceiptNumber = r.ReceiptKey " + "WHERE cm.Amount < 0 AND ISNULL(cm.EFT,0) = 0";
				if (modGlobal.Statics.gintCheckReportOrder != 2)
				{
					strTemp += " AND cm.BankNumber = " + FCConvert.ToString(Conversion.Val(this.UserData));
				}
				rsRevChkCnt.OpenRecordset(strTemp);
				while (!rsRevChkCnt.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
					// TODO Get_Fields: Field [BKey] not found!! (maybe it is an alias?)
					strTemp = "SELECT r.ReceiptNumber, cm.ReceiptNumber as RKey, cm.CheckNumber, cm.Amount, cm.BankNumber as BKey " + "FROM (CheckMaster cm INNER JOIN (SELECT DISTINCT ReceiptNumber FROM Archive WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + ") AS Arch ON cm.ReceiptNumber = Arch.ReceiptNumber) INNER JOIN Receipt r ON Arch.ReceiptNumber = r.ReceiptKey " + "WHERE cm.Amount = " + FCConvert.ToString(Conversion.Val(rsRevChkCnt.Get_Fields("Amount")) * -1) + " AND cm.CheckNumber = '" + rsRevChkCnt.Get_Fields("CheckNumber") + "' AND BankNUmber = " + rsRevChkCnt.Get_Fields("BKey") + " AND ISNULL(cm.EFT,0) = 0";
					if (modGlobal.Statics.gintCheckReportOrder != 2)
					{
						strTemp += " AND cm.BankNumber = " + FCConvert.ToString(Conversion.Val(this.UserData));
					}
					rsChkCnt.OpenRecordset(strTemp);
					if (!rsChkCnt.EndOfFile())
					{
						// kk04032017 trocrs-44  Rework - Per Brenda don't perform additional checking, just Check # and amount
						// Additional checking...  same receipt type(s) and account(s)
						// strTemp = "SELECT AccountNumber, ReceiptType, Ref, CheckPaidAmount FROM Archive WHERE ReceiptNumber = " & rsChkCnt.Fields("RKey")
						// rsChkCnt.OpenRecordset strTemp
						// If Not rsChkCnt.EndOfFile Then
						// strTemp = "SELECT AccountNumber, ReceiptType, Ref, CheckPaidAmount FROM Archive WHERE ReceiptNumber = " & .Fields("RKey")
						// rsArchRev.OpenRecordset strTemp
						// If Not rsArchRev.EndOfFile Then
						// If rsArchRev.RecordCount = rsChkCnt.RecordCount Then
						// boolNoMatch = False
						// Do While Not rsChkCnt.EndOfFile
						// rsArchRev.FindFirstRecord2 "AccountNumber,ReceiptType,Ref,CheckPaidAmount",
						// rsChkCnt.Fields("AccountNumber") & "," & rsChkCnt.Fields("ReceiptType") & "," & rsChkCnt.Fields("Ref") & "," & rsChkCnt.Fields("CheckPaidAmount") * -1,
						// ","
						// rsArchRev.FindFirstRecord2 "AccountNumber,ReceiptType,Ref,CheckPaidAmount",
						// rsChkCnt.Fields("AccountNumber") & "," & rsChkCnt.Fields("ReceiptType") & "," & rsChkCnt.Fields("Ref") & "," & rsChkCnt.Fields("CheckPaidAmount") * -1,
						// ","
						// If rsArchRev.NoMatch Then
						// boolNoMatch = True
						// Exit Do
						// End If
						// rsChkCnt.MoveNext
						// Loop
						// If Not boolNoMatch Then
						lngTmpCnt -= 1;
						// End If
						// End If
						// End If
						// End If
					}
					rsRevChkCnt.MoveNext();
				}
			}
			GetCheckCount = lngTmpCnt;
            rsChkCnt.DisposeOf();
            rsRevChkCnt.DisposeOf();
			return GetCheckCount;
		}

		private void sarTellerCheckReportDetail_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//sarTellerCheckReportDetail.Caption	= "Check Detail";
			//sarTellerCheckReportDetail.Icon	= "sarTellerCheckReportDetail.dsx":0000";
			//sarTellerCheckReportDetail.Left	= 0;
			//sarTellerCheckReportDetail.Top	= 0;
			//sarTellerCheckReportDetail.Width	= 14850;
			//sarTellerCheckReportDetail.Height	= 7275;
			//sarTellerCheckReportDetail.StartUpPosition	= 3;
			//sarTellerCheckReportDetail.SectionData	= "sarTellerCheckReportDetail.dsx":058A;
			//End Unmaped Properties
		}
	}
}
