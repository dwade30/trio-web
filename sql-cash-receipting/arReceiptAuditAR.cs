﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for sarTypesAR.
	/// </summary>
	public partial class sarTypesAR : BaseSectionReport
	{
		public sarTypesAR()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "sarTypesAR";
            this.ReportEnd += SarTypesAR_ReportEnd;
		}

        private void SarTypesAR_ReportEnd(object sender, EventArgs e)
        {
			rsTypes.DisposeOf();
            rsTypeTotals.DisposeOf();

		}

        public static sarTypesAR InstancePtr
		{
			get
			{
				return (sarTypesAR)Sys.GetInstance(typeof(sarTypesAR));
			}
		}

		protected sarTypesAR _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			rsTypes.Dispose();
			rsTypeTotals.Dispose();
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	sarTypesAR	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               10/01/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               09/08/2005              *
		// ********************************************************
		clsDRWrapper rsTypes = new clsDRWrapper();
		clsDRWrapper rsTypeTotals = new clsDRWrapper();
		int intType;
		int intOldType;
		bool BoolFirstType;
		bool boolNoRecords;
		public string strTID = string.Empty;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			Fields.Add("Binder2");
			Fields["Binder2"].Value = Strings.Format(intType, "000");
			BoolFirstType = true;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsTypes.EndOfFile() != true && rsTypes.BeginningOfFile() != true)
			{
				intOldType = intType;
				// FindNextFullRecord
				if (!BoolFirstType)
				{
					rsTypes.MoveNext();
				}
				else
				{
					BoolFirstType = false;
				}
				if (rsTypes.EndOfFile())
				{
					eArgs.EOF = true;
				}
				else
				{
					eArgs.EOF = false;
				}
			}
			else
			{
				intOldType = intType;
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_Initialize()
		{
			SetupForm();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			ActiveReport_Initialize();
			string strSQL;
			strTID = Strings.Trim(FCConvert.ToString(this.UserData));
			// boolNoRecords = True
			strSQL = "SELECT * FROM DefaultBillTypes WHERE TypeTitle <> '' ORDER BY TypeCode";
			rsTypes.OpenRecordset(strSQL, modExtraModules.strARDatabase);
			// FindNextFullRecord
			// Set sarTypeDetailOb.Object = New sarTypeDetailAR
			// sarTypeDetailOb.Report.UserData = Format(intType, "000")
		}

		private void FindNextFullRecord()
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			do
			{
				if (!boolNoRecords)
				{
					rsTypes.MoveNext();
				}
				if (rsTypes.EndOfFile() != true)
				{
					// if there are no more to get then exit this loop/sub    'check how many records are of the next type
					if (frmDailyReceiptAudit.InstancePtr.boolTellerOnly)
					{
						rsTemp.OpenRecordset("SELECT * FROM Archive WHERE ReceiptType = 97" + " AND ARBillType = " + rsTypes.Get_Fields_Int32("TypeCode") + " AND TellerID = '" + frmDailyReceiptAudit.InstancePtr.txtTellerID.Text + "' AND " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL, modGlobal.DEFAULTDATABASE);
					}
					else
					{
						rsTemp.OpenRecordset("SELECT * FROM Archive WHERE ReceiptType = 97" + " AND ARBillType = " + rsTypes.Get_Fields_Int32("TypeCode") + " AND " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL, modGlobal.DEFAULTDATABASE);
					}
					if (rsTemp.EndOfFile() != true)
					{
						// if there are records then
						boolNoRecords = false;
						break;
						// exit the loop and continue with reporting
					}
					if (boolNoRecords)
						rsTypes.MoveNext();
					// check the next record
				}
			}
			while (!rsTypes.EndOfFile());
		}

		private void SetupTitles()
		{
			// this will show the basic titles once and the dynamic titles for each type
			string strSQL = "";
			clsDRWrapper rsHeader = new clsDRWrapper();
			// strSQL = "SELECT * FROM DefaultBillTypes WHERE TypeCode = " & intType & " ORDER BY TypeCode"
			// 
			// rsHeader.OpenRecordset strSQL, strARDatabase
			lblName.Visible = false;
			lblAcct.Visible = false;
			lblBill.Visible = false;
			lblReceipt.Visible = false;
			lblDate.Visible = false;
			lblTeller.Visible = false;
			lblNC.Visible = false;
			lblPayType.Visible = false;
			lblCode.Visible = false;
			// If rsHeader.EndOfFile <> True And rsHeader.BeginningOfFile <> True Then
			lblTypeHeader.Text = "097(" + modGlobal.PadToString_8(rsTypes.Get_Fields_Int32("TypeCode"), 3) + ") - " + rsTypes.Get_Fields_String("TypeTitle");
			// MAL@20081118: Change to hard code the titles
			// Tracker Reference: 14057
			lblTitle1.Text = "Principal";
			lblTitle2.Text = "Interest";
			lblTitle3.Text = "Sales Tax";
			// lblTitle2.Text = "Sales Tax"
			// lblTitle3.Text = "Interest"
			lblTitle4.Text = "";
			lblTitle5.Text = "";
			lblTitle6.Text = "";
			// lblTitle1.Text = rsHeader.Fields("Title1Abbrev")
			// lblTitle2.Text = rsHeader.Fields("Title2Abbrev")
			// lblTitle3.Text = rsHeader.Fields("Title3Abbrev")
			// lblTitle4.Text = rsHeader.Fields("Title4Abbrev")
			// lblTitle5.Text = rsHeader.Fields("Title5Abbrev")
			// lblTitle6.Text = rsHeader.Fields("Title6Abbrev")
			if (Strings.Trim(lblTitle1.Text) == "")
			{
				lblTitle1.Text = ".......";
			}
			if (Strings.Trim(lblTitle2.Text) == "")
			{
				lblTitle2.Text = ".......";
			}
			if (Strings.Trim(lblTitle3.Text) == "")
			{
				lblTitle3.Text = ".......";
			}
			if (Strings.Trim(lblTitle4.Text) == "")
			{
				lblTitle4.Text = ".......";
			}
			if (Strings.Trim(lblTitle5.Text) == "")
			{
				lblTitle5.Text = ".......";
			}
			if (Strings.Trim(lblTitle6.Text) == "")
			{
				lblTitle6.Text = ".......";
			}
			// End If
			// fldConvenienceFeeTotal.Text = 0
			fldTotals1.Text = FCConvert.ToString(0);
			fldTotals2.Text = FCConvert.ToString(0);
			fldTotals3.Text = FCConvert.ToString(0);
			fldTotals4.Text = FCConvert.ToString(0);
			fldTotals5.Text = FCConvert.ToString(0);
			fldTotals6.Text = FCConvert.ToString(0);
			fldTotals7.Text = FCConvert.ToString(0);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			sarTypeDetailOb.Report = null;
			Fields["Binder2"].Value = Strings.Format(intType, "000");
			intType = FCConvert.ToInt32(rsTypes.Get_Fields_Int32("TypeCode"));
			if (frmDailyReceiptAudit.InstancePtr.boolTellerOnly)
			{
				rsTemp.OpenRecordset("SELECT * FROM Archive WHERE ReceiptType = 97" + " AND ARBillType = " + rsTypes.Get_Fields_Int32("TypeCode") + " AND TellerID = '" + frmDailyReceiptAudit.InstancePtr.txtTellerID.Text + "' AND " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL, modGlobal.DEFAULTDATABASE);
			}
			else
			{
				rsTemp.OpenRecordset("SELECT * FROM Archive WHERE ReceiptType = 97" + " AND ARBillType = " + rsTypes.Get_Fields_Int32("TypeCode") + " AND " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL, modGlobal.DEFAULTDATABASE);
			}
			if (rsTemp.EndOfFile() != true)
			{
				// if there are records then
				HideFields(false);
				SetupTitles();
				SetupTotals();
				sarTypeDetailOb.Report = new sarTypeDetailAR();
				sarTypeDetailOb.Report.UserData = Strings.Format(intType, "000");
			}
			else
			{
				HideFields(true);
				// Do Nothing - Move On to Next Type
			}
			// FindNextFullRecord
			frmWait.InstancePtr.IncrementProgress();
		}

		private void HideFields(bool blnHide)
		{
			if (blnHide)
			{
				lblTypeHeader.Visible = false;
				lblDate.Visible = false;
				lblName.Visible = false;
				lblPayType.Visible = false;
				lblAcct.Visible = false;
				lblBill.Visible = false;
				lblReceipt.Visible = false;
				lblTeller.Visible = false;
				lblNC.Visible = false;
				lblCode.Visible = false;
				lblTitle1.Visible = false;
				lblTitle2.Visible = false;
				lblTitle3.Visible = false;
				lblTitle4.Visible = false;
				lblTitle5.Visible = false;
				lblTitle6.Visible = false;
				lblTotal.Visible = false;
				Line2.Visible = false;
				lblTitleTotal.Visible = false;
				fldTotals1.Visible = false;
				fldTotals2.Visible = false;
				fldTotals3.Visible = false;
				fldTotals4.Visible = false;
				fldTotals5.Visible = false;
				fldTotals6.Visible = false;
				fldTotals7.Visible = false;
				fldConvenienceFeeTotal.Visible = false;
				lblConvenienceFeeTitle.Visible = false;
				sarTypeDetailOb.Visible = false;
				lblTypeHeader.Top = 0;
				lblDate.Top = 0;
				lblName.Top = 0;
				lblPayType.Top = 0;
				lblAcct.Top = 0;
				lblBill.Top = 0;
				lblReceipt.Top = 0;
				lblTeller.Top = 0;
				lblNC.Top = 0;
				lblCode.Top = 0;
				lblTitle1.Top = 0;
				lblTitle2.Top = 0;
				lblTitle3.Top = 0;
				lblTitle4.Top = 0;
				lblTitle5.Top = 0;
				lblTitle6.Top = 0;
				lblTotal.Top = 0;
				lblConvenienceFeeTitle.Top = 0;
				Line2.Y1 = 0;
				Line2.Y2 = 0;
				lblTitleTotal.Top = 0;
				fldTotals1.Top = 0;
				fldTotals2.Top = 0;
				fldTotals3.Top = 0;
				fldTotals4.Top = 0;
				fldTotals5.Top = 0;
				fldTotals6.Top = 0;
				fldTotals7.Top = 0;
				fldConvenienceFeeTotal.Top = 0;
				sarTypeDetailOb.Top = 0;
			}
			else
			{
				lblTypeHeader.Visible = true;
				lblDate.Visible = false;
				lblName.Visible = false;
				lblPayType.Visible = false;
				lblAcct.Visible = false;
				lblBill.Visible = false;
				lblReceipt.Visible = false;
				lblTeller.Visible = false;
				lblNC.Visible = false;
				lblCode.Visible = false;
				lblTitle1.Visible = true;
				lblTitle2.Visible = true;
				lblTitle3.Visible = true;
				lblTitle4.Visible = true;
				lblTitle5.Visible = true;
				lblTitle6.Visible = true;
				lblTotal.Visible = true;
				Line2.Visible = true;
				lblTitleTotal.Visible = true;
				fldTotals1.Visible = true;
				fldTotals2.Visible = true;
				fldTotals3.Visible = true;
				fldTotals4.Visible = true;
				fldTotals5.Visible = true;
				fldTotals6.Visible = true;
				fldTotals7.Visible = true;
				sarTypeDetailOb.Visible = true;
				lblTypeHeader.Top = 90 / 1440F;
				lblDate.Top = 90 / 1440F;
				lblName.Top = 90 / 1440F;
				lblPayType.Top = 360 / 1440F;
				lblAcct.Top = 360 / 1440F;
				lblBill.Top = 360 / 1440F;
				lblReceipt.Top = 360 / 1440F;
				lblTeller.Top = 360 / 1440F;
				lblNC.Top = 360 / 1440F;
				lblCode.Top = 360 / 1440F;
				lblTitle1.Top = 360 / 1440F;
				lblTitle2.Top = 360 / 1440F;
				lblTitle3.Top = 360 / 1440F;
				lblTitle4.Top = 360 / 1440F;
				lblTitle5.Top = 360 / 1440F;
				lblTitle6.Top = 360 / 1440F;
				lblTotal.Top = 360 / 1440F;
				Line2.Y1 = 1170 / 1440F;
				Line2.Y2 = 1170 / 1440F;
				lblTitleTotal.Top = 1170 / 1440F;
				fldTotals1.Top = 1170 / 1440F;
				fldTotals7.Top = 1170 / 1440F;
				sarTypeDetailOb.Top = 810 / 1440F;
				// If gstrEPaymentPortal = "P" Then
				// fldConvenienceFeeTotal.Visible = True
				// lblConvenienceFeeTitle.Visible = True
				// lblConvenienceFeeTitle.Top = 360
				// Else
				fldConvenienceFeeTotal.Visible = false;
				lblConvenienceFeeTitle.Visible = false;
				lblConvenienceFeeTitle.Top = 0;
				fldConvenienceFeeTotal.Top = 0;
				// End If
				SetupForm();
			}
		}

		private void SetupTotals()
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strSQL = "";
			// If boolNoRecords = False Then
			if (!rsTypes.EndOfFile())
			{
				if (frmDailyReceiptAudit.InstancePtr.boolTellerOnly)
				{
					strSQL = "SELECT COUNT(*) AS ReceiptCount, Sum(Amount1) as Total1, Sum(Amount2) as Total2, Sum(Amount3) as Total3, Sum(Amount4) as Total4, Sum(Amount5) as Total5, Sum(Amount6) as Total6, Sum(ConvenienceFee) as ConvenienceFeeTotal FROM Archive WHERE ReceiptType = 97 AND ARBillType = " + FCConvert.ToString(intType) + " AND TellerID = '" + frmDailyReceiptAudit.InstancePtr.txtTellerID.Text + "' AND " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND (AffectCash = 1 OR AffectCashDrawer = 1)";
				}
				else
				{
					strSQL = "SELECT COUNT(*) AS ReceiptCount, Sum(Amount1) as Total1, Sum(Amount2) as Total2, Sum(Amount3) as Total3, Sum(Amount4) as Total4, Sum(Amount5) as Total5, Sum(Amount6) as Total6, Sum(ConvenienceFee) as ConvenienceFeeTotal FROM Archive WHERE ReceiptType = 97 AND ARBillType = " + FCConvert.ToString(intType) + " AND " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND (AffectCash = 1 OR AffectCashDrawer = 1)";
				}
				rsTypeTotals.OpenRecordset(strSQL);
				// rsTemp.OpenRecordset "SELECT * FROM DefaultBillTypes WHERE TypeCode = " & intType, strARDatabase
				// TODO Get_Fields: Field [ReceiptCount] not found!! (maybe it is an alias?)
				lblTitleTotal.Text = "A/R (" + rsTypes.Get_Fields_String("TypeTitle") + ") - (" + rsTypeTotals.Get_Fields("ReceiptCount") + ")";
				// TODO Get_Fields: Field [Total1] not found!! (maybe it is an alias?)
				fldTotals1.Text = Strings.Format(rsTypeTotals.Get_Fields("Total1"), "#,##0.00");
				// TODO Get_Fields: Field [Total2] not found!! (maybe it is an alias?)
				fldTotals2.Text = Strings.Format(rsTypeTotals.Get_Fields("Total2"), "#,##0.00");
				// TODO Get_Fields: Field [Total3] not found!! (maybe it is an alias?)
				fldTotals3.Text = Strings.Format(rsTypeTotals.Get_Fields("Total3"), "#,##0.00");
				// TODO Get_Fields: Field [Total4] not found!! (maybe it is an alias?)
				fldTotals4.Text = Strings.Format(rsTypeTotals.Get_Fields("Total4"), "#,##0.00");
				// TODO Get_Fields: Field [Total5] not found!! (maybe it is an alias?)
				fldTotals5.Text = Strings.Format(rsTypeTotals.Get_Fields("Total5"), "#,##0.00");
				// TODO Get_Fields: Field [Total6] not found!! (maybe it is an alias?)
				fldTotals6.Text = Strings.Format(rsTypeTotals.Get_Fields("Total6"), "#,##0.00");
				// TODO Get_Fields: Field [ConvenienceFeeTotal] not found!! (maybe it is an alias?)
				fldConvenienceFeeTotal.Text = Strings.Format(rsTypeTotals.Get_Fields("ConvenienceFeeTotal"), "#,##0.00");
				if (Strings.Trim(fldTotals1.Text) == "")
				{
					fldTotals1.Text = "0.00";
				}
				if (Strings.Trim(fldTotals2.Text) == "")
				{
					fldTotals2.Text = "0.00";
				}
				if (Strings.Trim(fldTotals3.Text) == "")
				{
					fldTotals3.Text = "0.00";
				}
				if (Strings.Trim(fldTotals4.Text) == "")
				{
					fldTotals4.Text = "0.00";
				}
				if (Strings.Trim(fldTotals5.Text) == "")
				{
					fldTotals5.Text = "0.00";
				}
				if (Strings.Trim(fldTotals6.Text) == "")
				{
					fldTotals6.Text = "0.00";
				}
				// If Trim(fldConvenienceFeeTotal.Text) = "" Then
				// fldConvenienceFeeTotal.Text = "0.00"
				// End If
				// TODO Get_Fields: Field [Total1] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [Total2] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [Total3] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [Total4] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [Total5] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [Total6] not found!! (maybe it is an alias?)
				fldTotals7.Text = Strings.Format(rsTypeTotals.Get_Fields("Total1") + rsTypeTotals.Get_Fields("Total2") + rsTypeTotals.Get_Fields("Total3") + rsTypeTotals.Get_Fields("Total4") + rsTypeTotals.Get_Fields("Total5") + rsTypeTotals.Get_Fields("Total6"), "#,##0.00");
				if (Strings.Trim(fldTotals7.Text) == "")
				{
					fldTotals7.Text = "0.00";
				}
			}
		}

		private void SetupForm()
		{
			bool boolLand = false;
			if (frmDailyReceiptAudit.InstancePtr.boolTellerOnly)
			{
				boolLand = FCConvert.CBool(arDailyTellerAuditReport.InstancePtr.PageSettings.Orientation == GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape);
			}
			else
			{
				boolLand = FCConvert.CBool(arDailyAuditReport.InstancePtr.PageSettings.Orientation == GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape);
			}
			if (!boolLand)
			{
				// portrait
				lblName.Width = 3500 / 1440F;
				lblAcct.Width = 720 / 1440F;
				lblBill.Width = 540 / 1440F;
				lblReceipt.Width = 930 / 1440F;
				lblDate.Width = 1530 / 1440F;
				lblTeller.Width = 540 / 1440F;
				lblNC.Width = 720 / 1440F;
				lblPayType.Width = 630 / 1440F;
				lblCode.Width = 630 / 1440F;
				lblTitle1.Width = 1390 / 1440F;
				lblTitle2.Width = 1390 / 1440F;
				lblTitle3.Width = 1390 / 1440F;
				lblTitle4.Width = 1390 / 1440F;
				lblTitle5.Width = 1390 / 1440F;
				lblTitle6.Width = 1390 / 1440F;
				lblTotal.Width = 1440 / 1440F;
				lblConvenienceFeeTitle.Width = 1390 / 1440F;
				fldTotals1.Width = 1390 / 1440F;
				fldTotals2.Width = 1390 / 1440F;
				fldTotals3.Width = 1390 / 1440F;
				fldTotals4.Width = 1390 / 1440F;
				fldTotals5.Width = 1390 / 1440F;
				fldTotals6.Width = 1390 / 1440F;
				fldTotals7.Width = 1440 / 1440F;
				fldConvenienceFeeTotal.Width = 1390 / 1440F;
				lblTitleTotal.Width = 4230 / 1440F;
				sarTypeDetailOb.Width = 10800 / 1440F;
				this.PrintWidth = 10800 / 1440F;
				// Line1.X2 = 10800
				Line2.X2 = 10800 / 1440F;
				lblName.Left = 0;
				lblAcct.Left = 720 / 1440F;
				lblBill.Left = 1440 / 1440F;
				lblReceipt.Left = 1980 / 1440F;
				lblDate.Left = 3500 / 1440F;
				lblTeller.Left = 2910 / 1440F;
				lblNC.Left = 3360 / 1440F;
				lblPayType.Left = 90 / 1440F;
				lblCode.Left = 3990 / 1440F;
				lblTitle1.Left = 4620 / 1440F;
				lblTitle2.Left = 5560 / 1440F;
				lblTitle3.Left = 6320 / 1440F;
				lblTitle4.Left = 7080 / 1440F;
				lblTitle5.Left = 7840 / 1440F;
				lblTitle6.Left = 8600 / 1440F;
				lblConvenienceFeeTitle.Left = 4040 / 1440F;
				lblTotal.Left = 9360 / 1440F;
				fldTotals1.Left = 4620 / 1440F;
				fldTotals2.Left = 5560 / 1440F;
				fldTotals3.Left = 6320 / 1440F;
				fldTotals4.Left = 7080 / 1440F;
				fldTotals5.Left = 7840 / 1440F;
				fldTotals6.Left = 8600 / 1440F;
				fldTotals7.Left = 9360 / 1440F;
				fldConvenienceFeeTotal.Left = 4040 / 1440F;
				fldTotals2.Top = fldTotals1.Top + 270 / 1440F;
				fldTotals4.Top = fldTotals2.Top;
				fldTotals6.Top = fldTotals2.Top;
				fldConvenienceFeeTotal.Top = fldTotals2.Top;
				lblTitle2.Top = lblTitle1.Top + 270 / 1440F;
				lblTitle4.Top = lblTitle2.Top;
				lblTitle6.Top = lblTitle2.Top;
				lblConvenienceFeeTitle.Top = lblTitle2.Top;
				// Me.GroupHeader1.Height = lblTitle2.Top + lblTitle2.Height
			}
			else
			{
				// landscape
				lblName.Width = 3500 / 1440F;
				lblAcct.Width = 720 / 1440F;
				lblBill.Width = 540 / 1440F;
				lblReceipt.Width = 930 / 1440F;
				lblDate.Width = 1530 / 1440F;
				lblTeller.Width = 540 / 1440F;
				lblNC.Width = 720 / 1440F;
				lblPayType.Width = 630 / 1440F;
				lblCode.Width = 630 / 1440F;
				lblTitle1.Width = 1320 / 1440F;
				lblTitle2.Width = 1320 / 1440F;
				lblTitle3.Width = 1320 / 1440F;
				lblTitle4.Width = 1320 / 1440F;
				lblTitle5.Width = 1320 / 1440F;
				lblTitle6.Width = 1320 / 1440F;
				lblTotal.Width = 1370 / 1440F;
				lblConvenienceFeeTitle.Width = 1200 / 1440F;
				fldTotals1.Width = 1320 / 1440F;
				fldTotals2.Width = 1320 / 1440F;
				fldTotals3.Width = 1320 / 1440F;
				fldTotals4.Width = 1320 / 1440F;
				fldTotals5.Width = 1320 / 1440F;
				fldTotals6.Width = 1320 / 1440F;
				fldConvenienceFeeTotal.Width = 1200 / 1440F;
				fldTotals7.Width = 1370 / 1440F;
				lblTitleTotal.Width = 4230 / 1440F;
				sarTypeDetailOb.Width = 15120 / 1440F;
				this.PrintWidth = 15120 / 1440F;
				lblName.Left = 0;
				lblAcct.Left = 720 / 1440F;
				lblBill.Left = 1440 / 1440F;
				lblReceipt.Left = 1980 / 1440F;
				lblDate.Left = 3500 / 1440F;
				lblTeller.Left = 2910 / 1440F;
				lblNC.Left = 3360 / 1440F;
				lblPayType.Left = 90 / 1440F;
				lblCode.Left = 3990 / 1440F;
				lblConvenienceFeeTitle.Left = 4620 / 1440F;
				lblTitle1.Left = 5820 / 1440F;
				lblTitle2.Left = 7140 / 1440F;
				lblTitle3.Left = 8460 / 1440F;
				lblTitle4.Left = 9780 / 1440F;
				lblTitle5.Left = 11100 / 1440F;
				lblTitle6.Left = 12420 / 1440F;
				lblTotal.Left = 13740 / 1440F;
				fldConvenienceFeeTotal.Left = 4620 / 1440F;
				fldTotals1.Left = 5820 / 1440F;
				fldTotals2.Left = 7140 / 1440F;
				fldTotals3.Left = 8460 / 1440F;
				fldTotals4.Left = 9780 / 1440F;
				fldTotals5.Left = 11100 / 1440F;
				fldTotals6.Left = 12420 / 1440F;
				fldTotals7.Left = 13740 / 1440F;
				lblTitleTotal.Left = 0;
				lblTypeHeader.Left = 720 / 1440F;
				Line2.X2 = 15120 / 1440F;
				fldTotals2.Top = fldTotals1.Top;
				fldTotals4.Top = fldTotals2.Top;
				fldTotals6.Top = fldTotals2.Top;
				fldConvenienceFeeTotal.Top = fldTotals2.Top;
			}
		}

		private void sarTypesAR_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//sarTypesAR.Caption	= "Daily Receipt Audit";
			//sarTypesAR.Icon	= "sarTypesAR.dsx":0000";
			//sarTypesAR.Left	= 0;
			//sarTypesAR.Top	= 0;
			//sarTypesAR.Width	= 11880;
			//sarTypesAR.Height	= 8595;
			//sarTypesAR.StartUpPosition	= 3;
			//sarTypesAR.SectionData	= "sarTypesAR.dsx":058A;
			//End Unmaped Properties
		}
	}
}
