﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using Global;
using Wisej.Web;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmEPaymentAddress.
	/// </summary>
	partial class frmEPaymentAddress : BaseForm
	{
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtCountry;
		public fecherFoundation.FCTextBox txtZip4;
		public fecherFoundation.FCTextBox txtZip;
		public fecherFoundation.FCTextBox txtState;
		public fecherFoundation.FCTextBox txtCity;
		public fecherFoundation.FCTextBox txtAddress1;
		public fecherFoundation.FCTextBox txtName;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Frame1 = new fecherFoundation.FCFrame();
			this.txtCountry = new fecherFoundation.FCTextBox();
			this.txtZip4 = new fecherFoundation.FCTextBox();
			this.txtZip = new fecherFoundation.FCTextBox();
			this.txtState = new fecherFoundation.FCTextBox();
			this.txtCity = new fecherFoundation.FCTextBox();
			this.txtAddress1 = new fecherFoundation.FCTextBox();
			this.txtName = new fecherFoundation.FCTextBox();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label8 = new fecherFoundation.FCLabel();
			this.Label7 = new fecherFoundation.FCLabel();
			this.Label6 = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 498);
			this.BottomPanel.Size = new System.Drawing.Size(507, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Size = new System.Drawing.Size(507, 438);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(507, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(102, 30);
			this.HeaderText.Text = "Address";
			// 
			// Frame1
			// 
			this.Frame1.AppearanceKey = "groupBoxNoBorders";
			this.Frame1.Controls.Add(this.txtCountry);
			this.Frame1.Controls.Add(this.txtZip4);
			this.Frame1.Controls.Add(this.txtZip);
			this.Frame1.Controls.Add(this.txtState);
			this.Frame1.Controls.Add(this.txtCity);
			this.Frame1.Controls.Add(this.txtAddress1);
			this.Frame1.Controls.Add(this.txtName);
			this.Frame1.Controls.Add(this.Label3);
			this.Frame1.Controls.Add(this.Label8);
			this.Frame1.Controls.Add(this.Label7);
			this.Frame1.Controls.Add(this.Label6);
			this.Frame1.Controls.Add(this.Label5);
			this.Frame1.Controls.Add(this.Label2);
			this.Frame1.Controls.Add(this.Label1);
			this.Frame1.Location = new System.Drawing.Point(30, 30);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(452, 331);
			this.Frame1.TabIndex = 0;
			// 
			// txtCountry
			// 
			this.txtCountry.AutoSize = false;
			this.txtCountry.BackColor = System.Drawing.SystemColors.Window;
			this.txtCountry.Location = new System.Drawing.Point(146, 270);
			this.txtCountry.Name = "txtCountry";
			this.txtCountry.Size = new System.Drawing.Size(276, 40);
			this.txtCountry.TabIndex = 13;
			// 
			// txtZip4
			// 
			this.txtZip4.MaxLength = 4;
			this.txtZip4.AutoSize = false;
			this.txtZip4.BackColor = System.Drawing.SystemColors.Window;
			this.txtZip4.Location = new System.Drawing.Point(382, 210);
			this.txtZip4.Name = "txtZip4";
			this.txtZip4.Size = new System.Drawing.Size(40, 40);
			this.txtZip4.TabIndex = 11;
			// 
			// txtZip
			// 
			this.txtZip.MaxLength = 5;
			this.txtZip.AutoSize = false;
			this.txtZip.BackColor = System.Drawing.SystemColors.Window;
			this.txtZip.Location = new System.Drawing.Point(308, 210);
			this.txtZip.Name = "txtZip";
			this.txtZip.Size = new System.Drawing.Size(56, 40);
			this.txtZip.TabIndex = 9;
			// 
			// txtState
			// 
			this.txtState.MaxLength = 2;
			this.txtState.AutoSize = false;
			this.txtState.BackColor = System.Drawing.SystemColors.Window;
			this.txtState.Location = new System.Drawing.Point(146, 210);
			this.txtState.Name = "txtState";
			this.txtState.Size = new System.Drawing.Size(61, 40);
			this.txtState.TabIndex = 7;
			// 
			// txtCity
			// 
			this.txtCity.AutoSize = false;
			this.txtCity.BackColor = System.Drawing.SystemColors.Window;
			this.txtCity.Location = new System.Drawing.Point(146, 150);
			this.txtCity.Name = "txtCity";
			this.txtCity.Size = new System.Drawing.Size(276, 40);
			this.txtCity.TabIndex = 5;
			// 
			// txtAddress1
			// 
			this.txtAddress1.AutoSize = false;
			this.txtAddress1.BackColor = System.Drawing.SystemColors.Window;
			this.txtAddress1.Location = new System.Drawing.Point(146, 90);
			this.txtAddress1.Name = "txtAddress1";
			this.txtAddress1.Size = new System.Drawing.Size(276, 40);
			this.txtAddress1.TabIndex = 3;
			// 
			// txtName
			// 
			this.txtName.AutoSize = false;
			this.txtName.BackColor = System.Drawing.SystemColors.Window;
			this.txtName.Location = new System.Drawing.Point(146, 30);
			this.txtName.Name = "txtName";
			this.txtName.Size = new System.Drawing.Size(276, 40);
			this.txtName.TabIndex = 1;
			// 
			// Label3
			// 
			this.Label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(143)))), ((int)(((byte)(164)))));
			this.Label3.Location = new System.Drawing.Point(20, 284);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(64, 16);
			this.Label3.TabIndex = 12;
			this.Label3.Text = "COUNTRY";
			this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label8
			// 
			this.Label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(143)))), ((int)(((byte)(164)))));
			this.Label8.Location = new System.Drawing.Point(261, 224);
			this.Label8.Name = "Label8";
			this.Label8.Size = new System.Drawing.Size(27, 16);
			this.Label8.TabIndex = 8;
			this.Label8.Text = "ZIP";
			this.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label7
			// 
			this.Label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(143)))), ((int)(((byte)(164)))));
			this.Label7.Location = new System.Drawing.Point(20, 224);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(49, 16);
			this.Label7.TabIndex = 6;
			this.Label7.Text = "STATE";
			this.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label6
			// 
			this.Label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(143)))), ((int)(((byte)(164)))));
			this.Label6.Location = new System.Drawing.Point(370, 224);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(11, 16);
			this.Label6.TabIndex = 10;
			this.Label6.Text = "-";
			this.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label5
			// 
			this.Label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(143)))), ((int)(((byte)(164)))));
			this.Label5.Location = new System.Drawing.Point(20, 164);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(26, 16);
			this.Label5.TabIndex = 4;
			this.Label5.Text = "CITY";
			this.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label2
			// 
			this.Label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(143)))), ((int)(((byte)(164)))));
			this.Label2.Location = new System.Drawing.Point(20, 104);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(64, 16);
			this.Label2.TabIndex = 2;
			this.Label2.Text = "ADDRESS";
			this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label1
			// 
			this.Label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(143)))), ((int)(((byte)(164)))));
			this.Label1.Location = new System.Drawing.Point(20, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(41, 19);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "NAME";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcess
			});
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = 0;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuSaveContinue,
				this.Seperator,
				this.mnuExit
			});
			this.mnuProcess.Text = "File";
			// 
			// mnuSaveContinue
			// 
			this.mnuSaveContinue.Index = 0;
			this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveContinue.Text = "Save & Continue";
			this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// frmEPaymentAddress
			// 
			this.ClientSize = new System.Drawing.Size(507, 606);
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Menu = this.MainMenu1;
			this.Name = "frmEPaymentAddress";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Address";
			this.Load += new System.EventHandler(this.frmEPaymentAddress_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmEPaymentAddress_KeyDown);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
	}
}
