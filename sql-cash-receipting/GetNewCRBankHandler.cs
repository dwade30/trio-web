﻿using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Commands;
using SharedApplication.CashReceipts.Models;
using SharedApplication.Messaging;

namespace TWCR0000
{
    public class GetNewCRBankHandler : CommandHandler<GetNewCRBank,CRBank>
    {
        private IModalView<INewCRBankViewModel> newBankView;
        public GetNewCRBankHandler(IModalView<INewCRBankViewModel> newBankView)
        {
            this.newBankView = newBankView;
        }
        protected override CRBank Handle(GetNewCRBank command)
        {
            newBankView.ShowModal();
            var bank = new CRBank(){Name = newBankView.ViewModel.Name, RoutingTransit = newBankView.ViewModel.RoutingTransitnumber};
            return bank;
        }
    }
}