﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using SharedApplication.CashReceipts.Enums;
using TWSharedLibrary;
using Wisej.Core;
using Wisej.Web;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmTypeSetup.
	/// </summary>
	public partial class frmTypeSetup : BaseForm
	{
		public frmTypeSetup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
            //FC:FINAL:BSE:#3514 allow only numeric values 
            txtCode.AllowOnlyNumericInput(true);
		}

		private void InitializeComponentEx()
		{
			this.chkMandatory = new System.Collections.Generic.List<fecherFoundation.FCCheckBox>();
			this.chkMandatory.AddControlArrayElement(chkMandatory_3, FCConvert.ToInt16(3));
			this.chkMandatory.AddControlArrayElement(chkMandatory_2, FCConvert.ToInt16(2));
			this.chkMandatory.AddControlArrayElement(chkMandatory_1, FCConvert.ToInt16(1));
			this.chkMandatory.AddControlArrayElement(chkMandatory_0, FCConvert.ToInt16(0));
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmTypeSetup InstancePtr
		{
			get
			{
				return (frmTypeSetup)Sys.GetInstance(typeof(frmTypeSetup));
			}
		}

		protected frmTypeSetup _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/19/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               04/03/2007              *
		// ********************************************************
		bool boolDirty;
		// this will keep track of new codes being added and not saved
		bool boolLockType;
		bool boolLoaded;
		int intCurCode;
		bool boolLeftArrow;
		bool boolAccountBox;
		string strComboList = "";
		bool boolChangingRows;
		bool boolSetBMV;
		// this will get set then the click event should not fire
		bool boolTRIOOverride;
		bool boolDoNotChangeType;
		clsGridAccount clsAcct = new clsGridAccount();
		clsGridAccount clsAltCashAccount = new clsGridAccount();
		bool boolSaving;
		int CategoryCol;
		int TitleCol;
		int AbbrevCol;
		int AccountCol;
		int YearCol;
		int DefaultCol;
		int KeyCol;
		int ProjCol;
		int PercentCol;
		bool blnIsDeleted;
		// MAL@20071010
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsGridAccount vsReceivableGrid = new clsGridAccount();
		private clsGridAccount vsReceivableGrid_AutoInitialized;

		private clsGridAccount vsReceivableGrid
		{
			get
			{
				if (vsReceivableGrid_AutoInitialized == null)
				{
					vsReceivableGrid_AutoInitialized = new clsGridAccount();
				}
				return vsReceivableGrid_AutoInitialized;
			}
			set
			{
				vsReceivableGrid_AutoInitialized = value;
			}
		}

		private void FormatGrid()
		{
			clsDRWrapper rsProjects = new clsDRWrapper();
			string strComboList = "";
			// sets all of the widths, datatypes and titles for the grid
			int Width = 0;
			vsReceipt.Cols = 9;
			vsReceipt.EditMaxLength = 20;
			Width = vsReceipt.WidthOriginal;
			vsReceipt.ColWidth(CategoryCol, FCConvert.ToInt32(Width * 0.08));
			// Category Number
			vsReceipt.ColWidth(TitleCol, FCConvert.ToInt32(Width * 0.22));
			// Title
			vsReceipt.ColWidth(AbbrevCol, FCConvert.ToInt32(Width * 0.15));
			// Abbrev Title
			vsReceipt.ColWidth(AccountCol, FCConvert.ToInt32(Width * 0.19));
			// Account
			vsReceipt.ColWidth(YearCol, FCConvert.ToInt32(Width * 0.08));
			// Year
			if (modGlobal.Statics.ProjectFlag)
			{
				vsReceipt.ColWidth(DefaultCol, FCConvert.ToInt32(Width * 0.11));
				// Default $
				vsReceipt.ColHidden(ProjCol, false);
				vsReceipt.ColWidth(ProjCol, FCConvert.ToInt32(Width * 0.08));
			}
			else
			{
				vsReceipt.ColWidth(DefaultCol, FCConvert.ToInt32(Width * 0.19));
				// Default $
				vsReceipt.ColHidden(ProjCol, true);
				vsReceipt.ColWidth(ProjCol, 0);
			}
			// hidden column
			vsReceipt.ColWidth(KeyCol, 0);
			// Primary ID in table
			vsReceipt.ColHidden(KeyCol, true);
			vsReceipt.ColWidth(PercentCol, FCConvert.ToInt32(Width * 0.08));
			// Percent
			// check box for the Year column
			vsReceipt.ColDataType(YearCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsReceipt.ColDataType(PercentCol, FCGrid.DataTypeSettings.flexDTBoolean);
			// alignment
			vsReceipt.ColAlignment(CategoryCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			//FC:FINAL:AM: set the alignment
			vsReceipt.ColAlignment(DefaultCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			// default format for the Default Dollar column
			// .ColFormat(5) = "#,##0.00"
			// bold the header
			vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, vsReceipt.Cols - 1, true);
			//vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsReceipt.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			// sets the titles for the flexgrid
			vsReceipt.TextMatrix(0, CategoryCol, "Cat");
			vsReceipt.TextMatrix(0, TitleCol, "Screen Title");
			vsReceipt.TextMatrix(0, AbbrevCol, "Report Title");
			vsReceipt.TextMatrix(0, AccountCol, "Account");
			vsReceipt.TextMatrix(0, YearCol, "Year");
			vsReceipt.TextMatrix(0, DefaultCol, "Default $");
			vsReceipt.TextMatrix(0, ProjCol, "Proj");
			vsReceipt.TextMatrix(0, PercentCol, "%");
			if (modGlobal.Statics.ProjectFlag)
			{
				strComboList = "#0000; " + "\t" + " |";
				rsProjects.OpenRecordset("SELECT * FROM ProjectMaster", "TWBD0000.vb1");
				if (rsProjects.EndOfFile() != true && rsProjects.BeginningOfFile() != true)
				{
					do
					{
						strComboList += "#" + rsProjects.Get_Fields_String("ProjectCode") + ";" + rsProjects.Get_Fields_String("ProjectCode") + "\t" + rsProjects.Get_Fields_String("LongDescription") + "|";
						rsProjects.MoveNext();
					}
					while (rsProjects.EndOfFile() != true);
					strComboList = Strings.Left(strComboList, strComboList.Length - 1);
				}
				vsReceipt.ColComboList(ProjCol, strComboList);
			}
			// this will make two cols and the second col has a length of 0
			cmbCode.Cols = 2;
			cmbCode.ColWidth(1, 0);
		}

		private void cboConvenienceFeeMethod_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (Strings.Left(cboConvenienceFeeMethod.Text, 1) == "P")
			{
				fraPerDiem.Visible = true;
				Label16.Enabled = true;
				Label14.Enabled = true;
				txtConvenienceFeeAccount.Enabled = true;
			}
			else
			{
				fraPerDiem.Visible = false;
				txtPerDiem.Text = "0.00";
				Label14.Enabled = false;
				txtConvenienceFeeAccount.TextMatrix(0, 0, "");
				Label16.Enabled = false;
				txtConvenienceFeeAccount.Enabled = false;
			}
		}

		private void chkBMV_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolSetBMV)
			{
				if (chkBMV.CheckState == Wisej.Web.CheckState.Checked)
				{
					SetBMV_8("Y", true);
				}
				else
				{
					SetBMV_8("N", true);
				}
			}
		}

		private void chkConvenienceFee_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkConvenienceFee.CheckState == Wisej.Web.CheckState.Checked)
			{
				fraConvenienceFee.Enabled = true;
				fraPerDiem.Enabled = true;
				cboConvenienceFeeMethod.Enabled = true;
				cboConvenienceFeeMethod.SelectedIndex = 0;
				txtPerDiem.Enabled = true;
				Label14.Enabled = true;
				Label16.Enabled = true;
				txtConvenienceFeeAccount.Enabled = true;
			}
			else
			{
				fraConvenienceFee.Enabled = false;
				fraPerDiem.Enabled = false;
				cboConvenienceFeeMethod.Enabled = false;
				cboConvenienceFeeMethod.SelectedIndex = -1;
				txtPerDiem.Text = "0.00";
				txtPerDiem.Enabled = false;
				Label14.Enabled = false;
				txtConvenienceFeeAccount.TextMatrix(0, 0, "");
				Label16.Enabled = false;
				txtConvenienceFeeAccount.Enabled = false;
			}
		}

		private void chkAltEPmtAccount_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkAltEPmtAccount.CheckState == Wisej.Web.CheckState.Checked)
			{
				fraAltEPmtAccount.Enabled = true;
				cboAltEPmtAccount.Enabled = true;
			}
			else
			{
				fraAltEPmtAccount.Enabled = false;
				cboAltEPmtAccount.Enabled = false;
				cboAltEPmtAccount.SelectedIndex = -1;
			}
		}

		private void chkMandatory_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void chkMandatory_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = chkMandatory.GetIndex((FCCheckBox)sender);
			chkMandatory_CheckedChanged(index, sender, e);
		}

		private void chkMandatory_Enter(short Index, object sender, System.EventArgs e)
		{
			// set the backcolor so that it can be seen to have focus
			chkMandatory[Index].Enabled = !RestrictedCode();
			//chkMandatory[Index].BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT);
			boolAccountBox = false;
		}

		private void chkMandatory_Enter(object sender, System.EventArgs e)
		{
			short index = chkMandatory.GetIndex((FCCheckBox)sender);
			chkMandatory_Enter(index, sender, e);
		}

		private void chkMandatory_Leave(short Index, object sender, System.EventArgs e)
		{
			// set the backcolor so that it cannot be seen to have focus
			//chkMandatory[Index].BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
		}

		private void chkMandatory_Leave(object sender, System.EventArgs e)
		{
			short index = chkMandatory.GetIndex((FCCheckBox)sender);
			chkMandatory_Leave(index, sender, e);
		}

		private void chkPrint_CheckedChanged(object sender, System.EventArgs e)
		{
			// If chkPrint.Value = vbChecked Then took this out because the user might want to have a default IF they change the setting at the time of the receipt
			// lblCopies.Enabled = True
			// cmbCopies.Enabled = True
			// Else
			// lblCopies.Enabled = False
			// cmbCopies.Enabled = False
			// End If
		}

		private void cmbCode_ChangeEdit(object sender, System.EventArgs e)
		{
			if (cmbCode.IsCurrentCellInEditMode)
			{
				ProcessType();
				// If RestrictedCode Then
				// cmbCopies.SetFocus
				// Else
				// txtTitle.SetFocus
				// End If
				cmbCode.Focus();
				cmbCode.EditCell();
			}
		}

		private void cmbCode_ClickEvent(object sender, System.EventArgs e)
		{
			ProcessType();
		}

		private void cmbCode_ComboDropDown(object sender, System.EventArgs e)
		{
			cmbCode.ComboList = strComboList;
		}

		private void cmbCode_DblClick(object sender, System.EventArgs e)
		{
			ProcessType();
		}

		private void cmbCode_Enter(object sender, System.EventArgs e)
		{
			boolAccountBox = false;
		}

		private void cmbCode_KeyDownEvent(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Return:
					{
						ProcessType();
						if (RestrictedCode())
						{
						}
						else
						{
							if (txtTitle.Enabled && txtTitle.Visible)
							{
								txtTitle.Focus();
							}
							else if (cmbType.Visible && cmbType.Enabled)
							{
								cmbType.Focus();
							}
						}
						break;
					}
			}
			//end switch
		}

		private void cmbCode_KeyDownEdit(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Return:
					{
						ProcessType();
						if (RestrictedCode())
						{
						}
						else
						{
							if (txtTitle.Enabled && txtTitle.Visible)
							{
								txtTitle.Focus();
							}
							else if (cmbType.Visible && cmbType.Enabled)
							{
								cmbType.Focus();
							}
						}
						break;
					}
			}
			//end switch
		}

		private void cmbCode_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			ProcessType();
		}

		private void cmbCode_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			ProcessType();
		}

		private void cmbCopies_Enter(object sender, System.EventArgs e)
		{
			boolAccountBox = false;
		}

		private void cmbCopies_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						//FC:FINAL:AM
						// this will force the combobox to open with a spacebar
						//if (modAPIsConst.SendMessageByNumWrp(cmbCopies.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, string.Empty) == 0)
						//{
						//	modAPIsConst.SendMessageByNumWrp(cmbCopies.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, string.Empty);
						//	KeyCode = (Keys)0;
						//}
						if (!cmbCopies.DroppedDown)
						{
							cmbCopies.DroppedDown = true;
						}
						break;
					}
			}
			//end switch
		}

		private void cmbNewResCode_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbNewResCode.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 300, 0);
		}

		private void cmbNewResCode_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						// this will force the combobox to open with a spacebar
						if (modAPIsConst.SendMessageByNum(cmbNewResCode.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbNewResCode.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void cmbResCode_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolDoNotChangeType)
			{
				ProcessType();
			}
		}

		private void cmbResCode_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbResCode.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 300, 0);
		}

		private void cmbResCode_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						// this will force the combobox to open with a spacebar
						if (modAPIsConst.SendMessageByNum(cmbResCode.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbResCode.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void cmbType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolDoNotChangeType)
			{
				ProcessType();
			}
		}

		private void cmbType_DoubleClick(object sender, System.EventArgs e)
		{
			if (!boolDoNotChangeType)
			{
				ProcessType();
			}
		}

		private void cmbType_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbType.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 300, 0);
		}

		private void cmbType_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						// this will force the combobox to open with a spacebar
						if (modAPIsConst.SendMessageByNum(cmbType.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbType.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
							KeyCode = (Keys)0;
						}
						break;
					}
				case Keys.J:
					{
						if (Shift == 3)
						{
							KeyCode = (Keys)0;
							//if (MDIParent.StatusBar1.Panels[2 - 1].Bevel == sbrInset)
							//{
							//	App.MainForm.StatusBarText1 = "";
							//	MDIParent.InstancePtr.StatusBar1.Panels[2 - 1].Bevel = sbrNoBevel;
							//}
							//else
							//{
							//	App.MainForm.StatusBarText1 = "Software Designer : " + modEncrypt.ToggleEncryptCode("????????????");
							//	MDIParent.InstancePtr.StatusBar1.Panels[2 - 1].Bevel = sbrInset;
							//}
						}
						break;
					}
			}
			//end switch
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			// this goes back to the original state where the user has to choose the type
			cmdPrevious.Enabled = true;
			cmdNext.Enabled = true;
			txtCode.Text = "";
			ShowFrame(fraReceiptList);
			fraNewType.Visible = false;
			if (cmbType.Visible && cmbType.Enabled)
			{
				cmbType.Focus();
			}
			// cmbCode.SetFocus
		}

		public void cmdCancel_Click()
		{
			cmdCancel_Click(cmdCancel, new System.EventArgs());
		}

		private void cmdCancel_Enter(object sender, System.EventArgs e)
		{
			boolAccountBox = false;
		}

		private void cmdCancelResCode_Click(object sender, System.EventArgs e)
		{
			// this goes back to the original state where the user has to choose the type
			cmdPrevious.Enabled = true;
			cmdNext.Enabled = true;
			txtCode.Text = "";
			ShowFrame(fraReceiptList);
			fraResCode.Visible = false;
			if (cmbType.Visible && cmbType.Enabled)
			{
				cmbType.Focus();
			}
		}

		private void cmdCancelResCode_Enter(object sender, System.EventArgs e)
		{
			boolAccountBox = false;
		}

		private void cmdNewCode_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: intTemp As short, int --> As DialogResult
			DialogResult intTemp;
			string strCode;
			clsDRWrapper rsCh = new clsDRWrapper();
			strCode = txtCode.Text;
			cmdPrevious.Enabled = true;
			cmdNext.Enabled = true;
			if (Conversion.Val(strCode) > 0 && Conversion.Val(strCode) < 1000)
			{
				if (RestrictedCode_2(FCConvert.ToInt32(Conversion.Val(strCode))))
				{
					MessageBox.Show("Please enter a valid Type Code value that does not end in 90 to 99 or greater than 799.", "Restricted Code Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtCode.SelectionStart = 0;
					txtCode.SelectionLength = strCode.Length;
					txtCode.Focus();
				}
				else
				{
					rsCh.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(Conversion.Val(strCode)), modExtraModules.strCRDatabase);
					if (rsCh.EndOfFile() != true && rsCh.BeginningOfFile() != true)
					{
						// if there is already a match
						MessageBox.Show("The code " + strCode + " is already in use.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					else
					{
						// this number is available
						intTemp = MessageBox.Show("Are you sure that you want to create the Code: " + Strings.Left(strCode, 3) + "?", "New Code", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
						if (intTemp == DialogResult.Yes)
						{
							// yes
							CreateNewType();
						}
					}
				}
			}
			else
			{
				MessageBox.Show("Please enter a valid Type Code value. (001 - 799)", "Type Code Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtCode.SelectionStart = 0;
				txtCode.SelectionLength = strCode.Length;
				txtCode.Focus();
			}
			if (txtTitle.Enabled && txtTitle.Visible)
			{
				txtTitle.Focus();
			}
			else if (cmbCode.Enabled && cmbCode.Visible)
			{
				cmbCode.Focus();
			}
		}

		public void cmdNewCode_Click()
		{
			cmdNewCode_Click(cmdNewCode, new System.EventArgs());
		}

		private void CreateNewType()
		{
			// this will create a new code
			int I;
			int intCode;
			clsDRWrapper rsTown = new clsDRWrapper();
			// boolDirty = True
			clsDRWrapper rsSave = new clsDRWrapper();
			intCode = FCConvert.ToInt32(Math.Round(Conversion.Val(txtCode.Text)));
			//Application.DoEvents();
			if (!modGlobal.Statics.gboolMultipleTowns)
			{
				// create a new record with the correct code
				rsSave.OpenRecordset("SELECT * FROM Type", modExtraModules.strCRDatabase);
				rsSave.AddNew();
				rsSave.Set_Fields("TypeCode", FCConvert.ToInt16(intCode));
				rsSave.Set_Fields("ShowFeeDetailOnReceipt", true);
				// kk 04042013 TROCRS-12  Creating 2 receipt types with the same number, one is blank
				rsSave.Set_Fields("TownKey", 0);
				rsSave.Set_Fields("MOSES", false);
				rsSave.Set_Fields("ChangeType", false);
				rsSave.Set_Fields("ConvenienceFlatAmount", 0);
				rsSave.Update();
				// clear the code combobox and refresh its list
				// cmbCode.ComboList = ""
				//Application.DoEvents();
				FillCodeCombo();
				// cmbCode.ComboIndex = 0
				ShowFrame(fraReceiptList);
				// cmbCode.Select 0, 1
				// cmbCode.Select 0, 0
				cmbCode.Refresh();
				// clear the new code textbox and any info in the acct textbox
				txtCode.Text = "";
				// txtAcct(6).Text = ""
				// txtAcct(1).Text = ""
				// txtAcct(2).Text = ""
				// txtAcct(3).Text = ""
				// txtAcct(4).Text = ""
				// txtAcct(5).Text = ""
				//Application.DoEvents();
				// choose the new code
				for (I = 0; I <= cmbType.Items.Count - 1; I++)
				{
					if (intCode == Conversion.Val(Strings.Left(cmbType.Items[I].ToString(), 3)))
					{
						cmbType.SelectedIndex = I;
						break;
					}
				}
			}
			else
			{
				rsTown.OpenRecordset("SELECT * FROM tblRegions WHERE TownNumber > 0 ORDER BY TownNumber", "CentralData");
				while (!rsTown.EndOfFile())
				{
					// create a new record with the correct code
					rsSave.OpenRecordset("SELECT * FROM Type", modExtraModules.strCRDatabase);
					rsSave.AddNew();
					rsSave.Set_Fields("TypeCode", FCConvert.ToInt16(intCode));
					// TODO Get_Fields: Check the table for the column [TownNumber] and replace with corresponding Get_Field method
					rsSave.Set_Fields("TownKey", rsTown.Get_Fields("TownNumber"));
					rsSave.Set_Fields("ShowFeeDetailOnReceipt", true);
					rsSave.Set_Fields("MOSES", false);
					rsSave.Set_Fields("ChangeType", false);
					rsSave.Set_Fields("ConvenienceFlatAmount", 0);
					rsSave.Update();
					//Application.DoEvents();
					rsTown.MoveNext();
				}
				// clear the code combobox and refresh its list
				//Application.DoEvents();
				FillCodeCombo();
				ShowFrame(fraReceiptList);
				cmbCode.Refresh();
				// clear the new code textbox and any info in the acct textbox
				txtCode.Text = "";
				// choose the new code
				for (I = 0; I <= cmbType.Items.Count - 1; I++)
				{
					if (intCode == Conversion.Val(Strings.Left(cmbType.Items[I].ToString(), 3)))
					{
						cmbType.SelectedIndex = I;
						break;
					}
				}
				cmbResCode.SelectedIndex = 0;
			}
			// cmbCode.ComboList = strComboList
			// cmbCode.EditCell
			// cmbCode.TextMatrix(0, 0) = intCode
			// 
			// cmbCode.ComboIndex = FindNextComboIndex(intCode)
			ShowType(intCode);
			fraNewType.Visible = false;
		}

		private void CreateNewResCode()
		{
			// this will create a new code
			int I;
			int intCode;
			int lngResCode;
			// boolDirty = True
			clsDRWrapper rsSave = new clsDRWrapper();
			intCode = intCurCode;
			lngResCode = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbNewResCode.Items[cmbNewResCode.SelectedIndex].ToString())));
			//Application.DoEvents();
			// create a new record with the correct code
			SaveCode(lngResCode);
			// clear the code combobox and refresh its list
			//Application.DoEvents();
			FillCodeCombo();
			ShowFrame(fraReceiptList);
			cmbCode.Refresh();
			// clear the new code textbox and any info in the acct textbox
			txtCode.Text = "";
			//Application.DoEvents();
			// choose the code
			for (I = 0; I <= cmbType.Items.Count - 1; I++)
			{
				if (intCode == Conversion.Val(Strings.Left(cmbType.Items[I].ToString(), 3)))
				{
					cmbType.SelectedIndex = I;
					break;
				}
			}
			// choose the new res code
			for (I = 0; I <= cmbType.Items.Count - 1; I++)
			{
				if (lngResCode == Conversion.Val(Strings.Left(cmbResCode.Items[I].ToString(), 3)))
				{
					cmbResCode.SelectedIndex = I;
					break;
				}
			}
			ShowType(intCode, lngResCode);
			fraResCode.Visible = false;
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		private short FindNextComboIndex(ref short intCD)
		{
			short FindNextComboIndex = 0;
			// this will return the combo index on the next type
			int intCT;
			FindNextComboIndex = 0;
			for (intCT = 0; intCT <= cmbCode.ComboCount - 1; intCT++)
			{
				if (Conversion.Val(cmbCode.ComboItem(intCT)) == intCD)
				{
					FindNextComboIndex = FCConvert.ToInt16(intCT);
					return FindNextComboIndex;
				}
			}
			return FindNextComboIndex;
		}

		private void ChangeTypeCode()
		{
			// this sub will save the current information under a new code and delete the old one
			int I;
			clsDRWrapper rsSave = new clsDRWrapper();
			clsDRWrapper rsRb = new clsDRWrapper();
			rsSave.OpenRecordset("SELECT * FROM Type", modExtraModules.strCRDatabase);
			rsSave.AddNew();
			rsSave.Set_Fields("TypeCode", FCConvert.ToString(Conversion.Val(Strings.Left(txtCode.Text, 3))));
			rsSave.Set_Fields("TypeTitle", Strings.Trim(txtTitle.Text));
			rsSave.Set_Fields("BMV", FCConvert.CBool(chkBMV.CheckState == Wisej.Web.CheckState.Checked));
			rsSave.Set_Fields("Reference", Strings.Trim(txtRefTitle.Text));
			rsSave.Set_Fields("Control1", Strings.Trim(txtCTRL1Title.Text));
			rsSave.Set_Fields("Control2", Strings.Trim(txtCTRL2Title.Text));
			rsSave.Set_Fields("Control3", Strings.Trim(txtCTRL3Title.Text));
			rsSave.Set_Fields("Copies", FCConvert.ToString(Conversion.Val(Strings.Left(cmbCopies.Items[cmbCopies.SelectedIndex].ToString(), 1))));
			rsSave.Set_Fields("Print", FCConvert.CBool(chkPrint.CheckState == Wisej.Web.CheckState.Checked));
			if (FCConvert.CBool(chkRB.CheckState == Wisej.Web.CheckState.Checked))
			{
				rsRb.OpenRecordset("SELECT * FROM Type WHERE RBType = 1 AND Type <> " + FCConvert.ToString(Conversion.Val(Strings.Left(txtCode.Text, 3))));
				if (rsRb.EndOfFile())
				{
					// there is no other type with this field set
					rsSave.Set_Fields("RBType", true);
				}
				else
				{
					// there is already a type with the fields set to true
					if (MessageBox.Show("There is already a type that is associated with Blue Book.  Would you like to change the association to Type " + FCConvert.ToString(Conversion.Val(Strings.Left(txtCode.Text, 3))) + "?", "Change Association", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						rsSave.Set_Fields("RBType", true);
						// change the old type
						rsRb.Edit();
						rsRb.Set_Fields("RBType", false);
						rsRb.Update();
					}
					else
					{
						rsSave.Set_Fields("RBType", false);
					}
				}
			}
			else
			{
				rsSave.Set_Fields("RBType", false);
			}
			rsSave.Set_Fields("Title1", vsReceipt.TextMatrix(1, TitleCol));
			rsSave.Set_Fields("Title1Abbrev", vsReceipt.TextMatrix(1, AbbrevCol));
			rsSave.Set_Fields("Account1", vsReceipt.TextMatrix(1, AccountCol));
			rsSave.Set_Fields("DefaultAmount1", vsReceipt.TextMatrix(1, DefaultCol));
			rsSave.Set_Fields("Year1", vsReceipt.TextMatrix(1, YearCol));
			if (Strings.Left(vsReceipt.TextMatrix(1, AccountCol), 1) != "G" && Strings.Left(vsReceipt.TextMatrix(1, AccountCol), 1) != "M")
			{
				vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, YearCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
			}
			else
			{
				vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, YearCol, Information.RGB(255, 255, 255));
			}
			rsSave.Set_Fields("Title2", vsReceipt.TextMatrix(2, TitleCol));
			rsSave.Set_Fields("Title2Abbrev", vsReceipt.TextMatrix(2, AbbrevCol));
			rsSave.Set_Fields("Account2", vsReceipt.TextMatrix(2, AccountCol));
			rsSave.Set_Fields("DefaultAmount2", vsReceipt.TextMatrix(2, DefaultCol));
			rsSave.Set_Fields("Year2", vsReceipt.TextMatrix(2, YearCol));
			if (Strings.Left(vsReceipt.TextMatrix(2, AccountCol), 1) != "G" && Strings.Left(vsReceipt.TextMatrix(2, AccountCol), 1) != "M")
			{
				vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, YearCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
			}
			else
			{
				vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, YearCol, Information.RGB(255, 255, 255));
			}
			rsSave.Set_Fields("Title3", vsReceipt.TextMatrix(3, TitleCol));
			rsSave.Set_Fields("Title3Abbrev", vsReceipt.TextMatrix(3, AbbrevCol));
			rsSave.Set_Fields("Account3", vsReceipt.TextMatrix(3, AccountCol));
			rsSave.Set_Fields("DefaultAmount3", vsReceipt.TextMatrix(3, DefaultCol));
			rsSave.Set_Fields("Year3", vsReceipt.TextMatrix(3, YearCol));
			if (Strings.Left(vsReceipt.TextMatrix(3, AccountCol), 1) != "G" && Strings.Left(vsReceipt.TextMatrix(3, AccountCol), 1) != "M")
			{
				vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, YearCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
			}
			else
			{
				vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, YearCol, Information.RGB(255, 255, 255));
			}
			rsSave.Set_Fields("Title4", vsReceipt.TextMatrix(4, TitleCol));
			rsSave.Set_Fields("Title4Abbrev", vsReceipt.TextMatrix(4, AbbrevCol));
			rsSave.Set_Fields("Account4", vsReceipt.TextMatrix(4, AccountCol));
			rsSave.Set_Fields("DefaultAmount4", vsReceipt.TextMatrix(4, DefaultCol));
			rsSave.Set_Fields("Year4", vsReceipt.TextMatrix(4, YearCol));
			if (Strings.Left(vsReceipt.TextMatrix(4, AccountCol), 1) != "G" && Strings.Left(vsReceipt.TextMatrix(4, AccountCol), 1) != "M")
			{
				vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 4, YearCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
			}
			else
			{
				vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 4, YearCol, Information.RGB(255, 255, 255));
			}
			rsSave.Set_Fields("Title5", vsReceipt.TextMatrix(5, TitleCol));
			rsSave.Set_Fields("Title5Abbrev", vsReceipt.TextMatrix(5, AbbrevCol));
			rsSave.Set_Fields("Account5", vsReceipt.TextMatrix(5, AccountCol));
			rsSave.Set_Fields("DefaultAmount5", vsReceipt.TextMatrix(5, DefaultCol));
			rsSave.Set_Fields("Year5", vsReceipt.TextMatrix(5, YearCol));
			if (Strings.Left(vsReceipt.TextMatrix(5, AccountCol), 1) != "G" && Strings.Left(vsReceipt.TextMatrix(5, AccountCol), 1) != "M")
			{
				vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 5, YearCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
			}
			else
			{
				vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 5, YearCol, Information.RGB(255, 255, 255));
			}
			rsSave.Set_Fields("Title6", vsReceipt.TextMatrix(6, TitleCol));
			rsSave.Set_Fields("Title6Abbrev", vsReceipt.TextMatrix(6, AbbrevCol));
			rsSave.Set_Fields("Account6", vsReceipt.TextMatrix(6, AccountCol));
			rsSave.Set_Fields("DefaultAmount6", vsReceipt.TextMatrix(6, DefaultCol));
			rsSave.Set_Fields("Year6", vsReceipt.TextMatrix(6, YearCol));
			if (Strings.Left(vsReceipt.TextMatrix(6, AccountCol), 1) != "G" && Strings.Left(vsReceipt.TextMatrix(6, AccountCol), 1) != "M")
			{
				vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 6, YearCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
			}
			else
			{
				vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 6, YearCol, Information.RGB(255, 255, 255));
			}
			rsSave.Update();
			// delete the old code
			if (!RestrictedCode())
			{
				// rsSave.Execute "DELETE FROM Type WHERE TypeCode = " & Val(Left$(cmbCode.ComboItem(cmbCode.ComboIndex), 3)), DatabaseName
				rsSave.Execute("DELETE FROM Type WHERE TypeCode = " + FCConvert.ToString(Conversion.Val(Strings.Left(cmbType.Items[cmbType.SelectedIndex].ToString(), 3))), modGlobal.Statics.DatabaseName);
			}
			// clear the code combobox and refresh its list
			cmbCode.Clear();
			FillCodeCombo();
			// choose the new code
			for (I = 0; I <= cmbType.Items.Count - 1; I++)
			{
				if (Conversion.Val(txtCode.Text) == Conversion.Val(Strings.Left(cmbType.Items[I].ToString(), 3)))
				{
					cmbType.SelectedIndex = I;
					break;
				}
			}
			// For i = 0 To cmbCode.ComboCount - 1
			// If Val(txtCode) = Val(Left$(cmbCode.ComboItem(i), 3)) Then
			// cmbCode.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 0) = i
			// Exit For
			// End If
			// Next
			// clear the new code textbox
			txtCode.Text = "";
			ShowFrame(fraReceiptList);
			fraNewType.Visible = false;
		}

		private void cmdNewCode_Enter(object sender, System.EventArgs e)
		{
			boolAccountBox = false;
		}

		private void cmdNewResCode_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// vbPorter upgrade warning: intTemp As short, int --> As DialogResult
				DialogResult intTemp;
				string strCode;
				clsDRWrapper rsCh = new clsDRWrapper();
				strCode = FCConvert.ToString(Conversion.Val(cmbNewResCode.Items[cmbNewResCode.SelectedIndex].ToString()));
				cmdPrevious.Enabled = true;
				cmdNext.Enabled = true;
				if (Conversion.Val(strCode) > 0)
				{
					rsCh.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(intCurCode) + " AND Townkey = " + FCConvert.ToString(Conversion.Val(strCode)), modExtraModules.strCRDatabase);
					if (rsCh.EndOfFile() != true && rsCh.BeginningOfFile() != true)
					{
						// if there is already a match
						MessageBox.Show("The Town ID " + strCode + " is already in use for this receipt.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					else
					{
						// this number is available
						intTemp = MessageBox.Show("Are you sure that you want to create the Town ID: " + Strings.Left(strCode, 3) + " for receipt type " + FCConvert.ToString(intCurCode) + "?", "New Code", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
						if (intTemp == DialogResult.Yes)
						{
							// yes
							CreateNewResCode();
						}
					}
				}
				else
				{
					MessageBox.Show("Please enter a valid Town ID value.", "Town ID Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					cmbNewResCode.Focus();
				}
				if (txtTitle.Enabled && txtTitle.Visible)
				{
					txtTitle.Focus();
				}
				else if (cmbCode.Enabled && cmbCode.Visible)
				{
					cmbCode.Focus();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Town ID", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void cmdNewResCode_Click()
		{
			cmdNewResCode_Click(cmdNewResCode, new System.EventArgs());
		}

		private void cmdNewResCode_Enter(object sender, System.EventArgs e)
		{
			boolAccountBox = false;
		}

		private void frmTypeSetup_Activated(object sender, System.EventArgs e)
		{
			int intErr = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				intErr = 1;
				if (modGlobal.FormExist(this))
					return;
				if (!boolLoaded)
				{
					intErr = 2;
					boolDirty = false;
					intErr = 4;
					//fraReceiptList.Left = FCConvert.ToInt32((this.Width - fraReceiptList.Width) / 2.0);
					//fraReceiptList.Top = 30;
					// (Me.Height - fraReceiptList.Height) / 3
					fraReceiptList.Visible = true;
					intErr = 5;
					lblDefaultAccount.Text = "Alternate cash account:";
					intErr = 6;
					FormatGrid();
					if (modGlobalConstants.Statics.gboolMS)
					{
						cmdMoses.Enabled = true;
					}
					else
					{
						cmdMoses.Enabled = false;
					}
					intErr = 7;
					if (modGlobal.Statics.gboolNarrowReceipt)
					{
						txtTitle.MaxLength = 19;
					}
					else
					{
						txtTitle.MaxLength = 30;
					}
					intErr = 8;
					// fill combo boxes
					cmbCopies.AddItem("1");
					cmbCopies.AddItem("2");
					cmbCopies.AddItem("3");
					cmbCopies.AddItem("4");
					intErr = 11;
					// kgk 03072012 trocr-325  Type specific service code for InforME
					if (StaticSettings.gGlobalCashReceiptSettings.ElectronicPaymentPortalType == ElectronicPaymentPortalOption.PayPort)
					{
						FillEPmtAccountCombo();
					}
					intErr = 9;
					FillCodeCombo();
					intErr = 10;
					// this should show the first type
					if (cmbType.Items.Count > 0)
					{
						cmbType.SelectedIndex = 0;
					}
					if (cmbType.Visible && cmbType.Enabled)
					{
						cmbType.Focus();
					}
					if (modGlobal.Statics.gboolMultipleTowns)
					{
						FillResCodeCombo();
					}
					else
					{
						cmbResCode.Items.Insert(0, "0");
					}
					boolLoaded = true;
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Activate - " + FCConvert.ToString(intErr), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmTypeSetup_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// vbPorter upgrade warning: lngTemp As int	OnWrite(string)
			int lngTemp = 0;
			int intIndex;
			switch (KeyCode)
			{
				case Keys.F10:
					{
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Escape:
					{
						// exit
						KeyCode = (Keys)0;
						if (fraNewType.Visible)
						{
							cmdCancel_Click();
						}
						else
						{
							Close();
							return;
						}
						break;
					}
				case Keys.F9:
					{
						if (Shift == 3)
						{
							// this will be CTRL-SHIFT-F1
							KeyCode = (Keys)0;
							lngTemp = FCConvert.ToInt32(Interaction.InputBox("Please enter the code supplied to you by a TRIO staff member", "Save Override Code", null));
							if (modModDayCode.CheckWinModCode(lngTemp, "CR"))
							{
								boolTRIOOverride = true;
								MessageBox.Show("TRIO Save override code has been activated.", "Override Activated", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
							else
							{
								boolTRIOOverride = false;
								MessageBox.Show("TRIO Save override code has not been activated.", "Override Not Activated", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
						}
						break;
					}
			}
			//end switch
			//FC:FINAL:VGE - #518 KeyDown event is possible even if ActiveControl isn't set/is null.
			if (this.ActiveControl == null)
			{
				return;
			}
			if (Strings.UCase(this.ActiveControl.GetName()) == Strings.UCase("vsReceipt"))
			{
				if (vsReceipt.Col == 3 && (KeyCode < Keys.F1 || KeyCode > Keys.F12))
				{
					modNewAccountBox.CheckFormKeyDown(vsReceipt, vsReceipt.Row, vsReceipt.Col, KeyCode, Shift, vsReceipt.EditSelStart, vsReceipt.EditText, vsReceipt.EditSelLength);
				}
			}
			if (this.Visible)
			{
				if (Strings.UCase(this.ActiveControl.GetName()) == "TXTCONVENIENCEFEEACCOUNT")
				{
					modNewAccountBox.CheckFormKeyDown(txtConvenienceFeeAccount, txtConvenienceFeeAccount.Row, txtConvenienceFeeAccount.Col, KeyCode, Shift, txtConvenienceFeeAccount.EditSelStart, txtConvenienceFeeAccount.EditText, txtConvenienceFeeAccount.EditSelLength);
				}
			}
		}

		private void frmTypeSetup_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTypeSetup.FillStyle	= 0;
			//frmTypeSetup.ScaleWidth	= 9045;
			//frmTypeSetup.ScaleHeight	= 7530;
			//frmTypeSetup.LinkTopic	= "Form1";
			//End Unmaped Properties
			// this will size the form to the MDIParent
			clsDRWrapper rsCustomize = new clsDRWrapper();
			CategoryCol = 0;
			TitleCol = 1;
			AbbrevCol = 2;
			AccountCol = 3;
			YearCol = 4;
			DefaultCol = 5;
			KeyCol = 6;
			ProjCol = 7;
			PercentCol = 8;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			vsReceipt.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			clsAcct.GRID7Light = vsReceipt;
			clsAltCashAccount.GRID7Light = txtDefaultAccount;
			clsAcct.AccountCol = FCConvert.ToInt16(AccountCol);
			clsAcct.DefaultAccountType = "R";
			clsAltCashAccount.DefaultAccountType = "G";
			if (modGlobal.Statics.gboolMultipleTowns)
			{
				lblResCode.Visible = true;
				cmbResCode.Visible = true;
			}
			else
			{
				lblResCode.Visible = false;
				cmbResCode.Visible = false;
			}
			if (modGlobalConstants.Statics.gboolBD)
			{
				modValidateAccount.SetBDFormats();
				modNewAccountBox.GetFormats();
			}
			vsReceipt.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			// kgk 03072012 trocr-325  Type specific service code for InforME
			if ( StaticSettings.gGlobalCashReceiptSettings.ElectronicPaymentPortalType == ElectronicPaymentPortalOption.PayPort)
			{
				chkAltEPmtAccount.Visible = true;
				chkAltEPmtAccount.Text = "Override InforME Service Code";
				fraAltEPmtAccount.Visible = true;
			}

			chkConvenienceFee.Visible = false;
			fraConvenienceFee.Visible = false;
			// End If
			vsReceivableGrid.GRID7Light = txtConvenienceFeeAccount;
			if (modGlobalConstants.Statics.gboolBD)
			{
				vsReceivableGrid.DefaultAccountType = "G";
			}
			else
			{
				vsReceivableGrid.DefaultAccountType = "M";
			}
			vsReceivableGrid.AccountCol = -1;
		}
		// vbPorter upgrade warning: intTypeCode As short	OnWrite(int, double)
		private void ShowType_2(short intTypeCode, int lngResCode = 0, bool boolTryOnce = false)
		{
			ShowType(intTypeCode, lngResCode, boolTryOnce);
		}

		private void ShowType_8(short intTypeCode, int lngResCode = 0, bool boolTryOnce = false)
		{
			ShowType(intTypeCode, lngResCode, boolTryOnce);
		}

		private void ShowType_18(int intTypeCode, int lngResCode = 0, bool boolTryOnce = false)
		{
			ShowType(intTypeCode, lngResCode, boolTryOnce);
		}

		private void ShowType(int intTypeCode = 0, int lngResCode = 0, bool boolTryOnce = false)
		{
			int intErr = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsType = new clsDRWrapper();
				clsDRWrapper rsEPmtAcct = new clsDRWrapper();
				int intCT;
				string strTemp = "";
				intErr = 0;
				rsType.DefaultDB = modGlobal.Statics.DatabaseName;
				intErr = 1;
				if (intTypeCode == 0 || (lngResCode == 0 && modGlobal.Statics.gboolMultipleTowns))
				{
					// new type or blank to start
					intErr = 2;
					vsReceipt.Rows = 1;
					// clears grid
					vsReceipt.Rows = 7;
					txtRefTitle.Text = "";
					// clears the default titles
					txtCTRL1Title.Text = "";
					txtCTRL2Title.Text = "";
					txtCTRL3Title.Text = "";
					chkMandatory[0].CheckState = Wisej.Web.CheckState.Unchecked;
					chkMandatory[1].CheckState = Wisej.Web.CheckState.Unchecked;
					chkMandatory[2].CheckState = Wisej.Web.CheckState.Unchecked;
					chkMandatory[3].CheckState = Wisej.Web.CheckState.Unchecked;
					chkRB.CheckState = Wisej.Web.CheckState.Unchecked;
					cmbCopies.SelectedIndex = 0;
					chkBMV.CheckState = Wisej.Web.CheckState.Unchecked;
					txtTitle.Text = "";
					boolLockType = false;
					lblAccountTitle.Text = "";
					ColorGrid_2(false);
					chkEFT.CheckState = Wisej.Web.CheckState.Unchecked;
					chkConvenienceFee.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				else
				{
					// edit or examine a code that is already created
					intErr = 3;
					lblAccountTitle.Text = "";
					rsType.OpenRecordset("SELECT * FROM Type WHERE isnull(TypeCode,0) = " + FCConvert.ToString(intTypeCode) + " AND isnull(TownKey,0) = " + FCConvert.ToString(lngResCode));
					if (rsType.EndOfFile() != true && rsType.BeginningOfFile() != true)
					{
						intErr = 4;
						strTemp = FCConvert.ToString(rsType.Get_Fields_String("TypeTitle"));
						txtTitle.Text = strTemp;
						// kgk 03072012 trocr-325  Type specific service code for InforME
						if (FCConvert.ToString(rsType.Get_Fields_String("EPmtAcctID")) != "")
						{
							chkAltEPmtAccount.CheckState = Wisej.Web.CheckState.Checked;
							// Set the combo box to the correct index
							for (intCT = 0; intCT <= cboAltEPmtAccount.Items.Count - 1; intCT++)
							{
								if (cboAltEPmtAccount.Items[intCT].ToString() == FCConvert.ToString(rsType.Get_Fields_String("EPmtAcctID")))
								{
									cboAltEPmtAccount.SelectedIndex = intCT;
								}
							}
						}
						else
						{
							chkAltEPmtAccount.CheckState = Wisej.Web.CheckState.Unchecked;
							cboAltEPmtAccount.SelectedIndex = -1;
						}
						if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("ConvenienceFeeOverride")))
						{
							if (FCConvert.ToString(rsType.Get_Fields_String("ConvenienceFeeMethod")) == "P")
							{
								chkConvenienceFee.CheckState = Wisej.Web.CheckState.Checked;
								cboConvenienceFeeMethod.SelectedIndex = 0;
								txtPerDiem.Text = Strings.Format(FCConvert.ToInt16(rsType.Get_Fields_Double("ConveniencePercentage")) * 100, "0.00");
								txtConvenienceFeeAccount.TextMatrix(0, 0, FCConvert.ToString(rsType.Get_Fields_String("ConvenienceFeeGLAccount")));
							}
							else
							{
								chkConvenienceFee.CheckState = Wisej.Web.CheckState.Checked;
								cboConvenienceFeeMethod.SelectedIndex = 1;
							}
						}
						else
						{
							chkConvenienceFee.CheckState = Wisej.Web.CheckState.Unchecked;
						}
						intErr = 5;
						if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("RBType")))
						{
							chkRB.CheckState = Wisej.Web.CheckState.Checked;
						}
						else
						{
							chkRB.CheckState = Wisej.Web.CheckState.Unchecked;
						}
						intErr = 55;
						if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("EFT")))
						{
							chkEFT.CheckState = Wisej.Web.CheckState.Checked;
						}
						else
						{
							chkEFT.CheckState = Wisej.Web.CheckState.Unchecked;
						}
						intErr = 6;
						if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("BMV")))
						{
							intErr = 7;
							SetBMV_2("Y");
						}
						else
						{
							intErr = 8;
							SetBMV_2("N");
							txtRefTitle.Text = FCConvert.ToString(rsType.Get_Fields_String("Reference"));
							txtCTRL1Title.Text = FCConvert.ToString(rsType.Get_Fields_String("Control1"));
							txtCTRL2Title.Text = FCConvert.ToString(rsType.Get_Fields_String("Control2"));
							txtCTRL3Title.Text = FCConvert.ToString(rsType.Get_Fields_String("Control3"));
							intErr = 9;
							if (rsType.Get_Fields_Boolean("ReferenceMandatory") == true)
							{
								chkMandatory[0].CheckState = Wisej.Web.CheckState.Checked;
							}
							else
							{
								chkMandatory[0].CheckState = Wisej.Web.CheckState.Unchecked;
							}
							intErr = 10;
							if (rsType.Get_Fields_Boolean("Control1Mandatory") == true)
							{
								chkMandatory[1].CheckState = Wisej.Web.CheckState.Checked;
							}
							else
							{
								chkMandatory[1].CheckState = Wisej.Web.CheckState.Unchecked;
							}
							intErr = 11;
							if (rsType.Get_Fields_Boolean("Control2Mandatory") == true)
							{
								chkMandatory[2].CheckState = Wisej.Web.CheckState.Checked;
							}
							else
							{
								chkMandatory[2].CheckState = Wisej.Web.CheckState.Unchecked;
							}
							intErr = 12;
							if (rsType.Get_Fields_Boolean("Control3Mandatory") == true)
							{
								chkMandatory[3].CheckState = Wisej.Web.CheckState.Checked;
							}
							else
							{
								chkMandatory[3].CheckState = Wisej.Web.CheckState.Unchecked;
							}
						}
						intErr = 13;
						if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("Print")))
						{
							chkPrint.CheckState = Wisej.Web.CheckState.Checked;
						}
						else
						{
							chkPrint.CheckState = Wisej.Web.CheckState.Unchecked;
						}
						intErr = 14;
						if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("ShowFeeDetailOnReceipt")))
						{
							chkDetail.CheckState = Wisej.Web.CheckState.Checked;
						}
						else
						{
							chkDetail.CheckState = Wisej.Web.CheckState.Unchecked;
						}
						// MAL@20071010
						if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("Deleted")))
						{
							chkDeleted.CheckState = Wisej.Web.CheckState.Checked;
							lblDeleted.Visible = true;
							lblDeleted.ForeColor = Color.Red;
							cmdReactivate.Enabled = true;
							cmdDelete.Enabled = false;
						}
						else
						{
							chkDeleted.CheckState = Wisej.Web.CheckState.Unchecked;
							lblDeleted.Visible = false;
							lblDeleted.ForeColor = Color.Red;
							cmdReactivate.Enabled = false;
							cmdDelete.Enabled = true;
						}
						intErr = 15;
						// TODO Get_Fields: Check the table for the column [Copies] and replace with corresponding Get_Field method
					    cmbCopies.SelectedIndex = Conversion.Val(rsType.Get_Fields("Copies")) == 0 ? 0 : FCConvert.ToInt32((Conversion.Val(rsType.Get_Fields("Copies")) - 1));
						intErr = 16;
						vsReceipt.TextMatrix(1, CategoryCol, FCConvert.ToString(1));
						vsReceipt.TextMatrix(1, TitleCol, FCConvert.ToString(rsType.Get_Fields_String("Title1")));
						vsReceipt.TextMatrix(1, AbbrevCol, FCConvert.ToString(rsType.Get_Fields_String("Title1Abbrev")));
						// MAL@20081113: Add check for Type 97 (AR)
						// Tracker Reference: 14057
						if (intTypeCode == 97 && modGlobalConstants.Statics.gboolAR)
						{
							vsReceipt.TextMatrix(1, AccountCol, "FROM A/R");
							vsReceipt.TextMatrix(1, ProjCol, false);
							vsReceipt.TextMatrix(1, YearCol, false);
							vsReceipt.TextMatrix(1, DefaultCol, 0);
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 1, 1, PercentCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 1, 1, 1, PercentCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT);
							// FC:FINAL:VGE - #873 COloring cell to match disabled columns
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 1, 1, AccountCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 1, 1, 1, AccountCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT);
						}
						else
						{
							vsReceipt.TextMatrix(1, AccountCol, rsType.Get_Fields_String("Account1"));
							vsReceipt.TextMatrix(1, ProjCol, rsType.Get_Fields_String("Project1"));
							vsReceipt.TextMatrix(1, YearCol, rsType.Get_Fields_Boolean("Year1"));
							vsReceipt.TextMatrix(1, DefaultCol, rsType.Get_Fields_Double("DefaultAmount1"));
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, AccountCol, 1, PercentCol, Color.White);
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 1, AccountCol, 1, PercentCol, modGlobalConstants.Statics.TRIOCOLORBLACK);
						}
						intErr = 17;
						if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("PercentageFee1")))
						{
							vsReceipt.TextMatrix(1, PercentCol, FCConvert.ToString(-1));
						}
						else
						{
							vsReceipt.TextMatrix(1, PercentCol, FCConvert.ToString(0));
						}
						intErr = 18;
						if (Strings.Left(vsReceipt.TextMatrix(1, AccountCol), 1) != "G" && Strings.Left(vsReceipt.TextMatrix(1, AccountCol), 1) != "M")
						{
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, YearCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
						}
						else
						{
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, YearCol, Information.RGB(255, 255, 255));
						}
						intErr = 19;
						vsReceipt.TextMatrix(2, CategoryCol, 2);
						vsReceipt.TextMatrix(2, TitleCol, rsType.Get_Fields_String("Title2"));
						vsReceipt.TextMatrix(2, AbbrevCol, rsType.Get_Fields_String("Title2Abbrev"));
						vsReceipt.TextMatrix(2, AccountCol, rsType.Get_Fields_String("Account2"));
						vsReceipt.TextMatrix(2, ProjCol, rsType.Get_Fields_String("Project2"));
						vsReceipt.TextMatrix(2, YearCol, rsType.Get_Fields_Boolean("Year2"));
						vsReceipt.TextMatrix(2, DefaultCol, rsType.Get_Fields_Double("DefaultAmount2"));
						intErr = 20;
						if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("PercentageFee2")))
						{
							vsReceipt.TextMatrix(2, PercentCol, FCConvert.ToString(-1));
						}
						else
						{
							vsReceipt.TextMatrix(2, PercentCol, FCConvert.ToString(0));
						}
						intErr = 21;
						if (Strings.Left(vsReceipt.TextMatrix(2, AccountCol), 1) != "G" && Strings.Left(vsReceipt.TextMatrix(2, AccountCol), 1) != "M")
						{
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, YearCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
						}
						else
						{
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, YearCol, Information.RGB(255, 255, 255));
						}
						intErr = 22;
						vsReceipt.TextMatrix(3, CategoryCol, 3);
						vsReceipt.TextMatrix(3, TitleCol, rsType.Get_Fields_String("Title3"));
						vsReceipt.TextMatrix(3, AbbrevCol, rsType.Get_Fields_String("Title3Abbrev"));
						vsReceipt.TextMatrix(3, AccountCol, rsType.Get_Fields_String("Account3"));
						vsReceipt.TextMatrix(3, ProjCol, rsType.Get_Fields_String("Project3"));
						vsReceipt.TextMatrix(3, YearCol, rsType.Get_Fields_Boolean("Year3"));
						vsReceipt.TextMatrix(3, DefaultCol, rsType.Get_Fields_Double("DefaultAmount3"));
						intErr = 23;
						if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("PercentageFee3")))
						{
							vsReceipt.TextMatrix(3, PercentCol, FCConvert.ToString(-1));
						}
						else
						{
							vsReceipt.TextMatrix(3, PercentCol, FCConvert.ToString(0));
						}
						intErr = 24;
						if (Strings.Left(vsReceipt.TextMatrix(3, AccountCol), 1) != "G" && Strings.Left(vsReceipt.TextMatrix(3, AccountCol), 1) != "M")
						{
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, YearCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
						}
						else
						{
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, YearCol, Information.RGB(255, 255, 255));
						}
						intErr = 25;
						vsReceipt.TextMatrix(4, CategoryCol, 4);
						vsReceipt.TextMatrix(4, TitleCol, rsType.Get_Fields_String("Title4"));
						vsReceipt.TextMatrix(4, AbbrevCol, rsType.Get_Fields_String("Title4Abbrev"));
						vsReceipt.TextMatrix(4, AccountCol, rsType.Get_Fields_String("Account4"));
						vsReceipt.TextMatrix(4, ProjCol, rsType.Get_Fields_String("Project4"));
						vsReceipt.TextMatrix(4, YearCol, rsType.Get_Fields_Boolean("Year4"));
						vsReceipt.TextMatrix(4, DefaultCol, rsType.Get_Fields_Double("DefaultAmount4"));
						intErr = 26;
						if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("PercentageFee4")))
						{
							vsReceipt.TextMatrix(4, PercentCol, FCConvert.ToString(-1));
						}
						else
						{
							vsReceipt.TextMatrix(4, PercentCol, FCConvert.ToString(0));
						}
						intErr = 27;
						if (Strings.Left(vsReceipt.TextMatrix(4, AccountCol), 1) != "G" && Strings.Left(vsReceipt.TextMatrix(4, AccountCol), 1) != "M")
						{
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 4, YearCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
						}
						else
						{
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 4, YearCol, Information.RGB(255, 255, 255));
						}
						intErr = 28;
						vsReceipt.TextMatrix(5, CategoryCol, 5);
						vsReceipt.TextMatrix(5, TitleCol, rsType.Get_Fields_String("Title5"));
						vsReceipt.TextMatrix(5, AbbrevCol, rsType.Get_Fields_String("Title5Abbrev"));
						vsReceipt.TextMatrix(5, AccountCol, rsType.Get_Fields_String("Account5"));
						vsReceipt.TextMatrix(5, ProjCol, rsType.Get_Fields_String("Project5"));
						vsReceipt.TextMatrix(5, YearCol, rsType.Get_Fields_Boolean("Year5"));
						vsReceipt.TextMatrix(5, DefaultCol, rsType.Get_Fields_Double("DefaultAmount5"));
						intErr = 29;
						if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("PercentageFee5")))
						{
							vsReceipt.TextMatrix(5, PercentCol, FCConvert.ToString(-1));
						}
						else
						{
							vsReceipt.TextMatrix(5, PercentCol, FCConvert.ToString(0));
						}
						intErr = 30;
						if (Strings.Left(vsReceipt.TextMatrix(5, AccountCol), 1) != "G" && Strings.Left(vsReceipt.TextMatrix(5, AccountCol), 1) != "M")
						{
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 5, YearCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
						}
						else
						{
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 5, YearCol, Information.RGB(255, 255, 255));
						}
						intErr = 31;
						vsReceipt.TextMatrix(6, CategoryCol, 6);
						vsReceipt.TextMatrix(6, TitleCol, rsType.Get_Fields_String("Title6"));
						vsReceipt.TextMatrix(6, AbbrevCol, rsType.Get_Fields_String("Title6Abbrev"));
						vsReceipt.TextMatrix(6, AccountCol, rsType.Get_Fields_String("Account6"));
						vsReceipt.TextMatrix(6, ProjCol, rsType.Get_Fields_String("Project6"));
						vsReceipt.TextMatrix(6, YearCol, rsType.Get_Fields_Boolean("Year6"));
						vsReceipt.TextMatrix(6, DefaultCol, rsType.Get_Fields_Double("DefaultAmount6"));
						intErr = 32;
						if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("PercentageFee6")))
						{
							vsReceipt.TextMatrix(6, PercentCol, FCConvert.ToString(-1));
						}
						else
						{
							vsReceipt.TextMatrix(6, PercentCol, FCConvert.ToString(0));
						}
						intErr = 33;
						if (Strings.Left(vsReceipt.TextMatrix(6, AccountCol), 1) != "G" && Strings.Left(vsReceipt.TextMatrix(6, AccountCol), 1) != "M")
						{
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 6, YearCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
						}
						else
						{
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 6, YearCol, Information.RGB(255, 255, 255));
						}
						intErr = 34;
						txtDefaultAccount.TextMatrix(0, 0, FCConvert.ToString(rsType.Get_Fields_String("DefaultAccount")));
						if ((intTypeCode >= 90 && intTypeCode <= 92) || (intTypeCode == 890) || (intTypeCode == 891))
						{
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, YearCol, Color.White);
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, YearCol, Color.White);
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, YearCol, Color.White);
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 4, YearCol, Color.White);
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 5, YearCol, Color.White);
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 6, YearCol, Color.White);
						}
						else
						{
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, YearCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, YearCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, YearCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 4, YearCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 5, YearCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 6, YearCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
						}
						intCurCode = intTypeCode;
						cmbCode.TextMatrix(0, 0, FCConvert.ToString(intTypeCode));
						// For intCT = 0 To cmbType.ListCount - 1
						// If Val(Left(cmbType.List(cmbType.ListIndex), 3)) = intTypeCode Then
						// boolDoNotChangeType = True
						// cmbType.ListIndex = intCT
						// boolDoNotChangeType = False
						// End If
						// Next
						intErr = 35;
						if (RestrictedCode_2(intCurCode))
						{
							// disable the boxes that the user should not be touching
							LockBox_8(true, FCConvert.ToBoolean(rsType.Get_Fields_Boolean("BMV")));
							ColorGrid_2(true);
							if (modGlobal.Statics.gboolMultipleTowns && FCConvert.ToBoolean(lngResCode))
							{
								chkBMV.Enabled = false;
								chkDetail.Enabled = false;
								chkRB.Enabled = false;
								chkPrint.Enabled = false;
								cmbCopies.Enabled = false;
								chkEFT.Enabled = false;
							}
						}
						else
						{
							LockBox_8(false, FCConvert.ToBoolean(rsType.Get_Fields_Boolean("BMV")));
							ColorGrid_2(false);
							// If gboolMultipleTowns And lngResCode Then
							// txtTitle.Enabled = False
							// txtRefTitle.Enabled = False
							// txtCTRL1Title.Enabled = False
							// txtCTRL2Title.Enabled = False
							// txtCTRL3Title.Enabled = False
							// chkMandatory(0).Enabled = False
							// chkMandatory(1).Enabled = False
							// chkMandatory(2).Enabled = False
							// chkMandatory(3).Enabled = False
							// chkBMV.Enabled = False
							// chkDetail.Enabled = False
							// chkRB.Enabled = False
							// chkPrint.Enabled = False
							// cmbCopies.Enabled = False
							// chkEFT.Enabled = False
							// Else
							txtTitle.Enabled = true;
							txtRefTitle.Enabled = true;
							txtCTRL1Title.Enabled = true;
							txtCTRL2Title.Enabled = true;
							txtCTRL3Title.Enabled = true;
							chkMandatory[0].Enabled = true;
							chkMandatory[1].Enabled = true;
							chkMandatory[2].Enabled = true;
							chkMandatory[3].Enabled = true;
							chkBMV.Enabled = true;
							chkDetail.Enabled = true;
							chkRB.Enabled = true;
							chkPrint.Enabled = true;
							cmbCopies.Enabled = true;
							chkEFT.Enabled = true;
							// End If
						}
					}
					else
					{
						if (modGlobal.Statics.gboolMultipleTowns && !boolTryOnce)
						{
							cmbResCode.SelectedIndex = 0;
							ShowType_18(intTypeCode, lngResCode, true);
						}
					}
					boolDirty = false;
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Showing Type - " + FCConvert.ToString(intErr), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmTypeSetup_Resize(object sender, System.EventArgs e)
		{
			if (fraNewType.Visible == true)
			{
				ShowFrame(fraNewType);
			}
			else
			{
				// ShowFrame fraDefaultAccount
				ShowFrame(fraReceiptList);
			}
			//vsReceipt.HeightOriginal = (vsReceipt.RowHeight(0) * vsReceipt.Rows) + 70;
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			// vbPorter upgrade warning: intTemp As short, int --> As DialogResult
			DialogResult intTemp;
			clsDRWrapper rsTemp = new clsDRWrapper();
			if (boolDirty)
			{
				if (Conversion.Val(Strings.Left(cmbType.Items[cmbType.SelectedIndex].ToString(), 3)) != 0)
				{
					intTemp = MessageBox.Show("Do you want to save Code " + Strings.Left(cmbType.Items[cmbType.SelectedIndex].ToString(), 3) + "?", "Save Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
					// If Val(cmbCode.ComboItem(cmbCode.ComboIndex)) <> 0 Then
					// intTemp = MsgBox("Do you want to save Code " & cmbCode.ComboItem(cmbCode.ComboIndex) & "?", vbYesNoCancel + vbQuestion, "Save Changes")
					if (intTemp == DialogResult.Yes)
					{
						// yes
						// save it and exit
						SaveCode();
					}
					else if (intTemp == DialogResult.No)
					{
						// no
						// exit w/o saving
					}
					else if (intTemp == DialogResult.Cancel)
					{
						// cancel
						// go back to the screen and
						e.Cancel = true;
						return;
					}
				}
			}
			// this will reload any types that have been changed
			modGlobal.Statics.grsType.OpenRecordset("SELECT * FROM Type", modGlobal.DEFAULTDATABASE);
			boolLoaded = false;
			//MDIParent.InstancePtr.Show();
			//MDIParent.InstancePtr.GRID.Focus();
		}

		private void mnuFileBack_Click(object sender, System.EventArgs e)
		{
			MoveBack();
		}

		private void mnuFileForward_Click(object sender, System.EventArgs e)
		{
			MoveForward();
		}

		private void mnuFileMOSESSetup_Click(object sender, System.EventArgs e)
		{
			frmMOSESSetup.InstancePtr.Show(App.MainForm);
			Close();
		}

		private void mnuFileNewResCode_Click(object sender, System.EventArgs e)
		{
			int intCT;
			clsDRWrapper rsNew = new clsDRWrapper();
			// do not allow the user to use these menu options
			cmdPrevious.Enabled = false;
			cmdNext.Enabled = false;
			// hide this frame
			fraReceiptList.Visible = false;
			// show the frame
			ShowFrame(fraResCode);
			if (txtCode.Visible && txtCode.Enabled)
			{
				txtCode.Focus();
				txtCode.SelectionStart = 0;
				txtCode.SelectionLength = txtCode.Text.Length;
			}
		}

		private void mnuProcess_Click(object sender, System.EventArgs e)
		{
			mnuFileNewResCode.Enabled = modGlobal.Statics.gboolMultipleTowns && fraReceiptList.Visible;
		}

		private void mnuProcessDelete_Click(object sender, System.EventArgs e)
		{
			// this will delete the current Type Code
			// vbPorter upgrade warning: intAnswer As short, int --> As DialogResult
			DialogResult intAnswer;
			if (!RestrictedCode())
			{
				// MAL@20071010: Changed the way deletions are handled
				// Call Reference: 116594
				if (!FCConvert.CBool(chkDeleted.CheckState == Wisej.Web.CheckState.Checked))
				{
					intAnswer = MessageBox.Show("Are you sure you wish to inactivate this receipt type?", "Delete Type", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (intAnswer == DialogResult.Yes)
					{
						chkDeleted.CheckState = Wisej.Web.CheckState.Checked;
						boolDirty = true;
						// MAL@20071012: Force a Save
						mnuProcessSave_Click();
					}
				}
				else
				{
					MessageBox.Show("This type has already been deleted", "Already Done", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				// vsReceipt.rows = 1
				// vsReceipt.rows = 7
				// txtTitle.Text = ""
				// chkBMV.Value = vbUnchecked
				// chkPrint.Value = vbChecked
				// txtRefTitle.Text = "Reference"
				// txtCTRL1Title.Text = "Control1"
				// txtCTRL2Title.Text = "Control2"
				// txtCTRL3Title.Text = "Control3"
			}
			else
			{
				MessageBox.Show("You cannot delete a restricted code.", "Restricted Code", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void mnuProcessExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessNew_Click(object sender, System.EventArgs e)
		{
			int intCT;
			clsDRWrapper rsNew = new clsDRWrapper();
			// find the next availible type number and insert it into the box as default
			rsNew.OpenRecordset("SELECT * FROM Type ORDER BY TypeCode", modExtraModules.strCRDatabase);
			intCT = 1;
			if (rsNew.EndOfFile() != true && rsNew.BeginningOfFile() != true)
			{
				rsNew.MoveFirst();
				for (intCT = 1; intCT <= 990; intCT++)
				{
					if (!rsNew.EndOfFile())
					{
						if (FCConvert.ToInt32(rsNew.Get_Fields_Int32("TypeCode")) > intCT)
						{
							// if the database is greater than the counter
							if (!RestrictedCode(intCT))
							{
								// and not a restricted codee
								// this is in the realm of valid numbers
								// the counter and database do not match
								break;
							}
							else
							{
								// this is a restricted code
								if (RestrictedCode_2(FCConvert.ToInt32(rsNew.Get_Fields_Int32("TypeCode"))))
								{
									// if the database has a restricted code, then move it until it is out
									rsNew.MoveNext();
								}
								else
								{
									// the database will wait until the counter catches up to it
								}
							}
						}
						else
						{
							rsNew.MoveNext();
							// they match, so try the next one
						}
					}
					else
					{
						if (!RestrictedCode(intCT))
						{
							// and not a restricted codee
							// this is in the realm of valid numbers
							// the counter and database do not match
							break;
						}
						else
						{
							// this is a restricted code
							if (RestrictedCode_2(FCConvert.ToInt32(rsNew.Get_Fields_Int32("TypeCode"))))
							{
								// if the database has a restricted code, then move it until it is out
								rsNew.MoveNext();
							}
							else
							{
								// this is an open code
								break;
							}
						}
					}
				}
			}
			if (intCT >= 1000)
			{
				MessageBox.Show("There are no more receipt types.", "Type Table Full", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return;
			}
			else
			{
				txtCode.Text = modGlobal.PadToString_6(intCT, 3);
			}
			// do not allow the user to use these menu options
			cmdPrevious.Enabled = false;
			cmdNext.Enabled = false;
			// hide this frame
			fraReceiptList.Visible = false;
			// show the frame
			ShowFrame(fraNewType);
			if (txtCode.Visible && txtCode.Enabled)
			{
				txtCode.Focus();
				txtCode.SelectionStart = 0;
				txtCode.SelectionLength = txtCode.Text.Length;
			}
		}

		private void mnuProcessReactivate_Click(object sender, System.EventArgs e)
		{
			// MAL@20071012: Add option to reactivate a Deleted type
			// vbPorter upgrade warning: intAnswer As short, int --> As DialogResult
			DialogResult intAnswer;
			if (!RestrictedCode())
			{
				if (!FCConvert.CBool(chkDeleted.CheckState == Wisej.Web.CheckState.Unchecked))
				{
					intAnswer = MessageBox.Show("Are you sure you wish to reactivate this receipt type?", "Reactivate Type", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (intAnswer == DialogResult.Yes)
					{
						chkDeleted.CheckState = Wisej.Web.CheckState.Unchecked;
						boolDirty = true;
						mnuProcessSave_Click();
					}
				}
				else
				{
					MessageBox.Show("This type is already active", "Already Done", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			else
			{
				MessageBox.Show("You cannot change a restricted code's status.", "Restricted Code", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			int I;
			int intCode = 0;
			bool boolNoSave = false;
			// this will make sure the validation of the alt cash account is done
			if (Strings.Trim(txtDefaultAccount.TextMatrix(0, 0)) != "" && Strings.Left(txtDefaultAccount.TextMatrix(0, 0), 1) != "M" && Strings.InStr(1, txtDefaultAccount.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) == 0)
			{
				txtDefaultAccount_Validating(boolNoSave);
			}
			if (!boolNoSave)
			{
				if (fraNewType.Visible)
				{
					// save the new code
					cmdNewCode_Click();
				}
				else
				{
					intCode = intCurCode;
					// saves the current Type Code
					// MAL@20071107: Add successful message
					// SaveCode
					if (SaveCode())
					{
						MessageBox.Show("Save Successful.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						// set the combo box to the last code
						for (I = 0; I <= cmbType.Items.Count - 1; I++)
						{
							if (intCode == Conversion.Val(Strings.Left(cmbType.Items[I].ToString(), 3)))
							{
								intCurCode = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbType.Items[I].ToString(), 3))));
								cmbType.SelectedIndex = I;
								break;
							}
						}
					}
					else
					{
						MessageBox.Show("This type was not saved.", "Unsuccessful", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
			}
			else
			{
				MessageBox.Show("This type was not saved.", "Fund Mismatch", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				if (txtDefaultAccount.Enabled && txtDefaultAccount.Visible)
				{
					txtDefaultAccount.Focus();
				}
			}
		}

		public void mnuProcessSave_Click()
		{
			mnuProcessSave_Click(mnuProcessSave, new System.EventArgs());
		}

		private void mnuProcessSaveExit_Click(object sender, System.EventArgs e)
		{
			int intTempCode = 0;
			int intCT;
			bool boolNoSave = false;
			// this will make sure the validation of the alt cash account is done
			if (Strings.Trim(txtDefaultAccount.TextMatrix(0, 0)) != "" && Strings.Left(txtDefaultAccount.TextMatrix(0, 0), 1) != "M" && Strings.InStr(1, txtDefaultAccount.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) == 0)
			{
				txtDefaultAccount_Validating(boolNoSave);
			}
			if (!boolNoSave)
			{
				if (fraNewType.Visible)
				{
					// save the new code
					cmdNewCode_Click();
				}
				else if (fraResCode.Visible)
				{
					// save the new res code
					cmdNewResCode_Click();
				}
				else
				{
					intTempCode = intCurCode;
					if (SaveCode())
					{
						// this will check to see if the code is still in the combobox, if it is then it will set
						for (intCT = 0; intCT <= cmbType.Items.Count - 1; intCT++)
						{
							if (intTempCode == Conversion.Val(Strings.Left(cmbType.Items[intCT].ToString(), 3)))
							{
								intCurCode = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbType.Items[intCT].ToString(), 3))));
								break;
							}
							else
							{
								intCurCode = -1;
							}
						}
						vsReceipt.EditText = "";
						// this will stop the edittext from bleeding into the next type
						//Application.DoEvents();
						MoveForward();
					}
					else
					{
						MessageBox.Show("This type was not saved correctly.", "Unsuccessful Save", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
				}
			}
			else
			{
				MessageBox.Show("This type was not saved.", "Fund Mismatch", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				if (txtDefaultAccount.Enabled && txtDefaultAccount.Visible)
				{
					txtDefaultAccount.Focus();
				}
			}
			// cmbCode.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 0) = cmbCode.ComboItem((cmbCode.ComboIndex + 1) Mod cmbCode.ComboCount)
		}
		// Private Sub txtAcct_Change(Index As Integer)
		// On Error GoTo ERROR_HANDLER
		// If gboolBD Then
		// DetermineAccountTitle txtAcct(Index).Text, lblAccountTitle
		// End If
		// Exit Sub
		// ERROR_HANDLER:
		// MsgBox "Error #" & Err.Number & " - " & Err.Description & ".", vbCritical, "Determine Account Title - " & Index
		// End Sub
		//
		// Private Sub txtAcct_GotFocus(Index As Integer)
		// boolAccountBox = True
		// End Sub
		//
		// Private Sub txtAcct_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
		// On Error GoTo ERROR_HANDLER
		// Dim boolTest        As Boolean
		// Dim lngRW           As Long
		// boolTest = False
		// Select Case KeyCode
		//
		// Case vbKeyReturn, vbKeyRight
		//
		// If txtAcct(Index).SelStart = Len(txtAcct(Index).Text) - 1 Then
		// txtAcct_Validate Index, boolTest
		// txtAcct(Index).Visible = False
		// If Not boolTest Then
		// vsReceipt.SetFocus
		// vsReceipt.Select Index, 4
		// End If
		// End If
		//
		// Case vbKeyLeft
		//
		// If txtAcct(Index).SelStart = 0 Then
		// txtAcct_Validate Index, boolTest
		// txtAcct(Index).Visible = False
		// If Not boolTest Then
		// vsReceipt.SetFocus
		// vsReceipt.Select Index, 2
		// End If
		// End If
		//
		// Case vbKeyUp
		//
		// KeyCode = 0
		// txtAcct_Validate Index, boolTest
		// If Not boolTest Then
		// vsReceipt.Editable = False
		// lngRW = Index
		// If lngRW > 1 Then
		// lngRW = lngRW - 1
		// txtAcct(Index).Visible = False
		// vsReceipt.Select lngRW, 3
		// DoEvents
		// txtAcct(lngRW).Text = vsReceipt.TextMatrix(lngRW, 3)
		// txtAcct(lngRW).Left = vsReceipt.Left + vsReceipt.ColWidth(0) + vsReceipt.ColWidth(1) + vsReceipt.ColWidth(2)
		// txtAcct(lngRW).Top = vsReceipt.Top + (lngRW * vsReceipt.RowHeight(0))
		// txtAcct(Index).Tag = lngRW
		// txtAcct(lngRW).Visible = True
		// DoEvents
		// If txtAcct(lngRW).Visible Then
		// txtAcct(lngRW).SetFocus
		// End If
		// End If
		// End If
		//
		// Case vbKeyDown
		//
		// KeyCode = 0
		// txtAcct_Validate Index, boolTest
		// If Not boolTest Then
		// vsReceipt.Editable = False
		// lngRW = Index
		// If lngRW < 6 Then
		// lngRW = lngRW + 1
		// txtAcct(Index).Visible = False
		// vsReceipt.Select lngRW, 3
		// DoEvents
		// txtAcct(lngRW).Text = vsReceipt.TextMatrix(lngRW, vsReceipt.Col)
		// txtAcct(lngRW).Left = vsReceipt.Left + vsReceipt.ColWidth(0) + vsReceipt.ColWidth(1) + vsReceipt.ColWidth(2)
		// txtAcct(lngRW).Top = vsReceipt.Top + (lngRW * vsReceipt.RowHeight(0))
		// txtAcct(lngRW).Tag = vsReceipt.Row
		// txtAcct(lngRW).Visible = True
		// DoEvents
		// If txtAcct(lngRW).Visible Then
		// txtAcct(lngRW).SetFocus
		// End If
		// End If
		// End If
		//
		// Case vbKeyDelete
		//
		// If Shift = 1 Then
		// KeyCode = 0
		// txtAcct(Index).Text = ""
		// txtAcct(Index).SelStart = 0
		// End If
		//
		// Case vbKeyHome
		//
		//
		// End Select
		// Exit Sub
		// ERROR_HANDLER:
		// MsgBox "Error #" & Err.Number & " - " & Err.Description & ".", vbCritical, "Account Validation Error On Keydown"
		// End Sub
		//
		// Private Sub txtAcct_LostFocus(Index As Integer)
		// On Error GoTo ERROR_HANDLER
		// txtAcct(Index).Visible = False
		//
		// If vsReceipt.Enabled And vsReceipt.Visible And Not txtAcct(Index).Visible Then
		// boolAccountBox = True
		// vsReceipt.SetFocus
		// boolAccountBox = False
		// End If
		// Exit Sub
		// ERROR_HANDLER:
		// MsgBox "Error #" & Err.Number & " - " & Err.Description & ".", vbCritical, "Account Validation Error On Keydown"
		// End Sub
		private void txtBMV_Change()
		{
			boolDirty = true;
		}

		private void txtBMV_DblClick()
		{
			if (!RestrictedCode())
			{
				if (chkBMV.CheckState == Wisej.Web.CheckState.Checked)
				{
					SetBMV_2("N");
					boolDirty = true;
				}
				else
				{
					SetBMV_2("Y");
					boolDirty = true;
				}
			}
		}

		private void txtBMV_GotFocus()
		{
			boolAccountBox = false;
			chkBMV.Enabled = !RestrictedCode();
			// If Not RestrictedCode Then
			// txtBMV.SelStart = 0
			// txtBMV.SelLength = 1
			// End If
		}

		private void txtBMV_KeyDown(short KeyCode, ref short Shift)
		{
			switch ((Keys)KeyCode)
			{
				case Keys.Space:
					{
						// space
						KeyCode = 0;
						if (chkBMV.CheckState == Wisej.Web.CheckState.Checked)
						{
							chkBMV.CheckState = Wisej.Web.CheckState.Unchecked;
							SetBMV_2("N");
						}
						else
						{
							chkBMV.CheckState = Wisej.Web.CheckState.Checked;
							SetBMV_2("Y");
						}
						break;
					}
				case Keys.Y:
					{
						// y
						KeyCode = 0;
						chkBMV.CheckState = Wisej.Web.CheckState.Checked;
						SetBMV_2("Y");
						break;
					}
				case Keys.N:
					{
						// n
						KeyCode = 0;
						chkBMV.CheckState = Wisej.Web.CheckState.Unchecked;
						SetBMV_2("N");
						break;
					}
				default:
					{
						KeyCode = 0;
						break;
					}
			}
			//end switch
		}
		// Private Sub txtAcct_Validate(Index As Integer, Cancel As Boolean)
		// On Error GoTo ERROR_HANDLER
		// Dim strAcct As String
		// strAcct = txtAcct(Index).Text
		// If Trim$(vsReceipt.TextMatrix(Index, 1)) <> "" Then 'And vsReceipt.TextMatrix(Index, 5) <> "0.00" Then
		// If gboolBD Then
		// If AccountValidate(strAcct) Then
		// If txtDefaultAccount.Text & " " <> "" And Len(txtDefaultAccount.Text) > 2 And InStr(1, "_", txtDefaultAccount.Text) <> 0 Then  'if there is an account number in the accountbox
		// if there is a default Cash Account then check the fund of this account number
		// If GetFundFromAccount(txtDefaultAccount.Text, True) <> GetFundFromAccount(txtAcct(Index).Text, True) Then
		// Cancel = True
		// MsgBox "The fund for this account must match the Default Cash Account fund.", vbCritical, "Fund Mismatch"
		// Exit Sub
		// End If
		// End If
		//
		// vsReceipt.TextMatrix(Index, 3) = txtAcct(Index).Text
		// If Left$(txtAcct(Index).Text, 1) = "G" Then
		// vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Index, 4) = RGB(255, 255, 255)
		// Else
		// vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Index, 4) = TRIOCOLORGRAYEDOUTTEXTBOX
		// End If
		// txtAcct.Visible = False
		//
		// Else
		// If MsgBox("Please enter a valid account.", vbOKCancel + vbInformation, "Input Error") = 1 Then
		// Cancel = True
		// Else
		// vsReceipt.Select vsReceipt.Row, 1
		// txtAcct.Visible = False
		// End If
		// End If
		// Else
		// If Left$(txtAcct(Index).Text, 1) <> "M" Then
		// MsgBox "You can only use the Account Code 'M' when you do not have the TRIO Budgetary Module.", vbInformation, "Account Validation Error"
		// Cancel = True
		// Else        'this is an 'M' Account
		// vsReceipt.TextMatrix(Index, 3) = txtAcct(Index).Text
		// If Left$(txtAcct.Text, 1) = "G" Then
		// vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsReceipt.Row, 4) = RGB(255, 255, 255)
		// Else
		// vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsReceipt.Row, 4) = TRIOCOLORGRAYEDOUTTEXTBOX
		// End If
		// txtAcct.Visible = False
		// End If
		// End If
		// Else
		// txtAcct.Visible = False
		// End If
		//
		// If txtAcct.Visible = False Then
		// vsReceipt.SetFocus
		// End If
		// Exit Sub
		// ERROR_HANDLER:
		// MsgBox "Error #" & Err.Number & " - " & Err.Description & ".", vbCritical, "Validation Error"
		// End Sub
		private void txtCode_Enter(object sender, System.EventArgs e)
		{
			boolAccountBox = false;
		}

		private void txtCode_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Return:
					{
						cmdNewCode_Click();
						break;
					}
			}
			//end switch
		}

		private void txtCode_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// this will not allow characters into the textbox '8,13
			if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back) || (KeyAscii == Keys.Return))
			{
				// the numbers, backspace and return
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void FillCodeCombo()
		{
			int intErr = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill all of the types into cmbType
				int Number = 0;
				string Name = "";
				string strTemp = "";
				clsDRWrapper rsTemp = new clsDRWrapper();
				int intCT = 0;
				rsTemp.DefaultDB = modGlobal.Statics.DatabaseName;
				cmbType.Clear();
				intErr = 1;
                
				//FCUtils.EraseSafe(modUseCR.Statics.strTypeDescriptions);
				intErr = 2;
				if (modGlobal.Statics.gboolMultipleTowns)
				{
					rsTemp.OpenRecordset("SELECT * FROM Type WHERE isnull(TownKey,0) = 1 ORDER BY TypeCode");
				}
				else
				{
					rsTemp.OpenRecordset("SELECT * FROM Type WHERE isnull(TownKey,0) = 0 ORDER BY TypeCode");
				}
				if (rsTemp.BeginningOfFile() != true && rsTemp.EndOfFile() != true)
				{
					intErr = 3;
					rsTemp.MoveFirst();
					intErr = 4;
					intCurCode = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("TypeCode"));
					do
					{
						intErr = 5;
						// this fills all of the types into the combobox
						Number = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("TypeCode"));
						intErr = 6;
						Name = FCConvert.ToString(rsTemp.Get_Fields_String("TypeTitle"));
						// kgk 01-04-2012 trocr-146   Add logic to hide types for unauth modules
						switch (Number)
						{
							case 90:
							case 91:
							case 92:
							case 890:
							case 891:
								{
									// Tax Collections
									if (!modGlobalConstants.Statics.gboolCL)
									{
										Number = 0;
										Name = "";
									}
									break;
								}
							case 93:
							case 94:
							case 95:
							case 96:
							case 893:
							case 894:
							case 895:
							case 896:
								{
									// Utility Billing
									if (!modGlobalConstants.Statics.gboolUT)
									{
										Number = 0;
										Name = "";
									}
									break;
								}
							case 97:
								{
									// Accounts Receivable
									if (!modGlobalConstants.Statics.gboolAR)
									{
										Number = 0;
										Name = "";
									}
									break;
								}
							case 98:
							case 800:
							case 801:
							case 802:
							case 803:
							case 804:
								{
									// Clerk
									if (!modGlobalConstants.Statics.gboolCK)
									{
										Number = 0;
										Name = "";
									}
									break;
								}
							case 99:
								{
									// Motor Vehicles
									if (!modGlobalConstants.Statics.gboolMV && !modGlobalConstants.Statics.gboolRB)
									{
										Number = 0;
										Name = "";
									}
									break;
								}
							case 190:
								{
									// MOSES
									if (!modGlobalConstants.Statics.gboolMS)
									{
										Number = 0;
										Name = "";
									}
									break;
								}
						}
						//end switch
						if (Number != 0)
						{
							intErr = 7;
							//modUseCR.Statics.strTypeDescriptions[Number] = Name;
							intErr = 8;
							strTemp = modGlobal.PadToString_6(Number, 3) + " - " + Name;
							intErr = 9;
							cmbType.AddItem(strTemp);
							intCT += 1;
						}
						rsTemp.MoveNext();
					}
					while (!rsTemp.EndOfFile());
				}
				else
				{
					MessageBox.Show("No types were loaded.", "No Receipt Types", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Fill Combo Error - " + FCConvert.ToString(intErr), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtCTRL1Title_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtCTRL1Title_Enter(object sender, System.EventArgs e)
		{
			boolAccountBox = false;
			txtCTRL1Title.ReadOnly = (RestrictedCode() || RestrictForBMV()) || (modGlobal.Statics.gboolMultipleTowns && Conversion.Val(cmbResCode.Items[cmbResCode.SelectedIndex].ToString()) != 1);
		}

		private void txtCTRL2Title_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtCTRL2Title_Enter(object sender, System.EventArgs e)
		{
			boolAccountBox = false;
			txtCTRL2Title.ReadOnly = (RestrictedCode() || RestrictForBMV()) || (modGlobal.Statics.gboolMultipleTowns && Conversion.Val(cmbResCode.Items[cmbResCode.SelectedIndex].ToString()) != 1);
		}

		private void txtCTRL3Title_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtCTRL3Title_Enter(object sender, System.EventArgs e)
		{
			boolAccountBox = false;
			txtCTRL3Title.ReadOnly = (RestrictedCode() || RestrictForBMV()) || (modGlobal.Statics.gboolMultipleTowns && Conversion.Val(cmbResCode.Items[cmbResCode.SelectedIndex].ToString()) != 1);
		}

		private void txtDefaultAccount_Change()
		{
			boolDirty = true;
		}

		private void txtDefaultAccount_Enter(object sender, System.EventArgs e)
		{
			boolAccountBox = false;
		}

		private void txtDefaultAccount_KeyDownEvent(object sender, KeyEventArgs e)
		{
			string strAcct = "";
			if (e.KeyCode == Keys.F2)
			{
				strAcct = frmLoadValidAccounts.InstancePtr.Init(txtDefaultAccount.TextMatrix(0, 0));
				if (strAcct != "")
				{
					txtDefaultAccount.TextMatrix(0, 0, strAcct);
				}
			}
		}

		private void txtDefaultAccount_Validating(bool cancel)
		{
			txtDefaultAccount_Validating(txtDefaultAccount, new System.ComponentModel.CancelEventArgs(cancel));
		}

		private void txtDefaultAccount_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// this will check to see if all of the accounts for this type are from the same fund as this account
			string strAcct;
			int lngFund = 0;
			int intCT;
			strAcct = txtDefaultAccount.TextMatrix(0, 0);
			if (Strings.Trim(strAcct + " ") != "" && Strings.InStr(1, strAcct, "_", CompareConstants.vbBinaryCompare) == 0)
			{
				lngFund = modBudgetaryAccounting.GetFundFromAccount(strAcct, true);
				if (lngFund > 0)
				{
					for (intCT = 1; intCT <= 6; intCT++)
					{
						if (vsReceipt.TextMatrix(intCT, AccountCol) != "")
						{
							if (lngFund != modBudgetaryAccounting.GetFundFromAccount(vsReceipt.TextMatrix(intCT, AccountCol), true))
							{
								e.Cancel = true;
								MessageBox.Show("All of the accounts in category 1 - 6 must be from the same fund as the Alternate Cash Account.", "Incorrect Fund", MessageBoxButtons.OK, MessageBoxIcon.Hand);
								break;
							}
						}
					}
				}
				else if (lngFund == -1)
				{
					// this is a bad account so blank the acct box out
					MessageBox.Show("Invalid account or the system could not find the fund for this account.", "Bad Account / Missing Fund", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
					txtDefaultAccount.TextMatrix(0, 0, "");
				}
			}
			else
			{
				// empty let it slide
				txtDefaultAccount.TextMatrix(0, 0, "");
			}
		}

		private void txtRefTitle_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtRefTitle_Enter(object sender, System.EventArgs e)
		{
			boolAccountBox = false;
			txtRefTitle.ReadOnly = (RestrictedCode() || RestrictForBMV()) || (modGlobal.Statics.gboolMultipleTowns && Conversion.Val(cmbResCode.Items[cmbResCode.SelectedIndex].ToString()) != 1);
		}

		private void txtRefTitle_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// if txtRefTitle.Tag
		}

		private void txtTitle_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtTitle_Enter(object sender, System.EventArgs e)
		{
			boolAccountBox = false;
			txtTitle.ReadOnly = RestrictedCode() || (modGlobal.Statics.gboolMultipleTowns && Conversion.Val(cmbResCode.Items[cmbResCode.SelectedIndex].ToString()) != 1);
		}

		private void vsReceipt_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			int intCode = 0;
			if (cmbType.SelectedIndex != -1)
			{
				intCode = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbType.Items[cmbType.SelectedIndex].ToString(), 3))));
			}
			// MAL@20081113: Add check for AR
			// Tracker Reference: 14057
			if (intCode == 97 && modGlobalConstants.Statics.gboolAR && vsReceipt.Row == 1)
			{
				e.Cancel = true;
				return;
			}
			else
			{
				if (vsReceipt.Col == AccountCol)
				{
					vsReceipt.EditMaxLength = 23;
				}
			}
		}

		private void vsReceipt_ChangeEdit(object sender, System.EventArgs e)
		{
			if (vsReceipt.IsCurrentCellInEditMode)
			{
				boolDirty = true;
				if (vsReceipt.Col == AccountCol)
				{
					// if this is the account col then
					if (modGlobalConstants.Statics.gboolBD)
					{
						lblAccountTitle.Text = modAccountTitle.ReturnAccountDescription(vsReceipt.EditText);
					}
				}
			}
		}

		private void vsReceipt_ClickEvent(object sender, System.EventArgs e)
		{
			int lngRow = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intCode = 0;
				// If cmbCode.ComboIndex <> -1 Then
				// intCode = Val(Left$(cmbCode.ComboItem(cmbCode.ComboIndex), 3))
				// End If
				if (cmbType.SelectedIndex != -1)
				{
					intCode = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbType.Items[cmbType.SelectedIndex].ToString(), 3))));
				}
				lngRow = vsReceipt.Row;
				if (!RestrictedCode())
				{
					if (vsReceipt.Col == TitleCol)
					{
						// MAL@20071107: Correct to allow editing of first cell when clicked into
						if (cmbResCode.SelectedIndex == -1 || (cmbResCode.SelectedIndex != -1 && Conversion.Val(cmbResCode.Items[cmbResCode.SelectedIndex].ToString()) != -1))
						{
							// If Val(cmbResCode.List(cmbResCode.ListIndex)) = 1 Then
							vsReceipt.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							vsReceipt.EditMaxLength = 15;
							vsReceipt.EditCell();
						}
						else
						{
							vsReceipt.Editable = FCGrid.EditableSettings.flexEDNone;
						}
					}
					else if (vsReceipt.Col == AbbrevCol)
					{
						if (cmbResCode.SelectedIndex == -1 || (cmbResCode.SelectedIndex != -1 && Conversion.Val(cmbResCode.Items[cmbResCode.SelectedIndex].ToString()) == 1))
						{
							vsReceipt.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							vsReceipt.EditMaxLength = 8;
							vsReceipt.EditCell();
						}
						else
						{
							vsReceipt.Editable = FCGrid.EditableSettings.flexEDNone;
						}
					}
					else if (vsReceipt.Col == AccountCol)
					{
						// account box row
						// vsReceipt.Editable = False
						// If Trim(vsReceipt.TextMatrix(lngRow, vsReceipt.Col) & " ") <> "" Then
						// txtAcct(lngRow).Text = vsReceipt.TextMatrix(lngRow, vsReceipt.Col)
						// Else
						// txtAcct(lngRow).Text = txtAcct(lngRow).Default
						// End If
						// txtAcct(lngRow).Left = vsReceipt.Left + vsReceipt.ColWidth(0) + vsReceipt.ColWidth(1) + vsReceipt.ColWidth(2)
						// txtAcct(lngRow).Top = vsReceipt.Top + (lngRow * vsReceipt.RowHeight(0))
						// txtAcct(lngRow).Tag = lngRow
						// txtAcct(lngRow).Visible = True
						// txtAcct(lngRow).SetFocus
					}
					else if (vsReceipt.Col == YearCol)
					{
						// If Left$(vsReceipt.TextMatrix(lngRow, 3), 1) = "G" Then
						// If Val(vsReceipt.TextMatrix(lngRow, vsReceipt.Col)) = -1 Then
						// vsReceipt.TextMatrix(lngRow, vsReceipt.Col) = 0
						// Else
						// vsReceipt.TextMatrix(lngRow, vsReceipt.Col) = -1
						// End If
						// Else
						vsReceipt.TextMatrix(lngRow, vsReceipt.Col, FCConvert.ToString(false));
						// k 10032012    0
						// End If
						// 
						// If Left$(vsReceipt.TextMatrix(lngRow, 3), 1) = "G" Then
						// vsReceipt.Editable = True
						// Else
						// vsReceipt.Editable = False
						// If vsReceipt.TextMatrix(lngRow, 4) = True Then vsReceipt.TextMatrix(lngRow, 4) = False
						// End If
					}
					else if (vsReceipt.Col == DefaultCol)
					{
						vsReceipt.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						vsReceipt.EditMaxLength = 15;
						vsReceipt.EditCell();
					}
					else if (vsReceipt.Col == ProjCol)
					{
						if (modGlobal.Statics.ProjectFlag)
						{
							vsReceipt.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							vsReceipt.EditCell();
						}
					}
					else if (vsReceipt.Col == PercentCol)
					{
						if (FCConvert.ToDouble(vsReceipt.TextMatrix(lngRow, PercentCol)) == -1)
						{
							vsReceipt.TextMatrix(lngRow, PercentCol, FCConvert.ToString(0));
						}
						else
						{
							vsReceipt.TextMatrix(lngRow, PercentCol, FCConvert.ToString(-1));
						}
						// vsReceipt.Editable = True
					}
					else
					{
						vsReceipt.Editable = FCGrid.EditableSettings.flexEDNone;
					}
				}
				else
				{
					if (vsReceipt.Col == YearCol)
					{
						if (intCode == 90 || intCode == 91 || intCode == 92 || intCode == 890 || intCode == 891)
						{
							if (Strings.Left(vsReceipt.TextMatrix(lngRow, AccountCol), 1) == "G")
							{
								if (lngRow == 1)
								{
									// always make the pricinpal of a RE or PP receipt selected
									vsReceipt.TextMatrix(lngRow, vsReceipt.Col, FCConvert.ToString(true));
									// k 10032102 -1
								}
								else
								{
									if (FCConvert.ToBoolean(Conversion.Val(vsReceipt.TextMatrix(lngRow, vsReceipt.Col))))
									{
										// k    = -1 Then
										vsReceipt.TextMatrix(lngRow, vsReceipt.Col, FCConvert.ToString(false));
										// k    0
									}
									else
									{
										vsReceipt.TextMatrix(lngRow, vsReceipt.Col, FCConvert.ToString(true));
										// k    -1
									}
								}
							}
							else if (Strings.Left(vsReceipt.TextMatrix(lngRow, AccountCol), 1) == "M" && lngRow == 1 && (intCode == 90 || intCode == 91 || intCode == 92 || intCode == 890 || intCode == 891))
							{
								if (FCConvert.CBool(vsReceipt.TextMatrix(lngRow, vsReceipt.Col)))
								{
									// k    = -1 Then
									vsReceipt.TextMatrix(lngRow, vsReceipt.Col, FCConvert.ToString(false));
									// k    0
								}
								else
								{
									vsReceipt.TextMatrix(lngRow, vsReceipt.Col, FCConvert.ToString(true));
									// k    -1
								}
							}
							else
							{
								vsReceipt.TextMatrix(lngRow, vsReceipt.Col, FCConvert.ToString(false));
								// k    0
							}
						}
						else
						{
							vsReceipt.TextMatrix(lngRow, vsReceipt.Col, FCConvert.ToString(false));
							// k    0
						}
						// If Left$(vsReceipt.TextMatrix(lngRow, 3), 1) = "G" Then
						// vsReceipt.Editable = True
						// Else
						// vsReceipt.Editable = False
						// If vsReceipt.TextMatrix(lngRow, 4) = True Then vsReceipt.TextMatrix(lngRow, 4) = False
						// End If
					}
					else if (vsReceipt.Col == AccountCol)
					{
						vsReceipt.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						// vsReceipt.Editable = False
						// If Trim(vsReceipt.TextMatrix(lngRow, vsReceipt.Col) & " ") <> "" Then
						// txtAcct(lngRow).Text = vsReceipt.TextMatrix(lngRow, vsReceipt.Col)
						// Else
						// txtAcct(lngRow).Text = txtAcct(lngRow).Default
						// End If
						// txtAcct(lngRow).Left = vsReceipt.Left + vsReceipt.ColWidth(0) + vsReceipt.ColWidth(1) + vsReceipt.ColWidth(2)
						// txtAcct(lngRow).Top = vsReceipt.Top + (lngRow * vsReceipt.RowHeight(0))
						// txtAcct(lngRow).Tag = lngRow
						// txtAcct(lngRow).Visible = True
						// txtAcct(lngRow).SetFocus
					}
					else
					{
						vsReceipt.Editable = FCGrid.EditableSettings.flexEDNone;
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Grid Click Error - " + FCConvert.ToString(lngRow), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void vsReceipt_Enter(object sender, System.EventArgs e)
		{
			int lngRow;
			// this is the row that was clicked on
			lngRow = vsReceipt.Row;
			// txtAcct(6).Visible = False
			// txtAcct(1).Visible = False
			// txtAcct(2).Visible = False
			// txtAcct(3).Visible = False
			// txtAcct(4).Visible = False
			// txtAcct(5).Visible = False
			if (!boolAccountBox)
			{
				// vsReceipt.Select 1, 1
				vsReceipt.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
			if (RestrictedCode())
			{
				if (vsReceipt.Col == DefaultCol)
				{
					vsReceipt.EditCell();
				}
				// If vsReceipt.Col = 3 Then
				// SetGridFormat vsReceipt, vsReceipt.Row, vsReceipt.Col, False
				// End If
			}
			else
			{
				// vsReceipt.EditCell
			}
			boolAccountBox = false;
		}

		private void vsReceipt_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (vsReceipt.Col == DefaultCol && KeyCode == Keys.Left)
			{
				boolLeftArrow = true;
			}
			else
			{
				boolLeftArrow = false;
			}
			if (vsReceipt.Col == YearCol && FCConvert.ToInt32(KeyCode) == 32)
			{
				vsReceipt_ClickEvent(sender, EventArgs.Empty);
			}
		}

		private void vsReceipt_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int keyAscii = Strings.Asc(e.KeyChar);
			if (vsReceipt.Col == DefaultCol)
			{
				if (RestrictedCode())
				{
					// no default values set for restriced types
					keyAscii = 0;
				}
				else
				{
					if ((keyAscii >= 48 && keyAscii <= 57) || (keyAscii == 8) || (keyAscii == 13) || (keyAscii == 46) || (keyAscii == 45))
					{
					}
					else
					{
						keyAscii = 0;
					}
				}
			}
		}

		private void vsReceipt_RowColChange(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intCode = 0;
				int lngRow;
				int lngCol;
				lngRow = vsReceipt.Row;
				lngCol = vsReceipt.Col;
				if (!boolChangingRows)
				{
					boolChangingRows = true;
					if (cmbType.SelectedIndex != -1)
					{
						intCode = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbType.Items[cmbType.SelectedIndex].ToString(), 3))));
					}
					if (RestrictedCode())
					{
						// show the account description in the label at the bottom of the page
						if (Strings.Trim(vsReceipt.TextMatrix(lngRow, AccountCol)) != "")
						{
							if (Strings.InStr(1, vsReceipt.TextMatrix(lngRow, AccountCol), "_", CompareConstants.vbBinaryCompare) == 0 && Strings.InStr(1, vsReceipt.TextMatrix(lngRow, AccountCol), "--", CompareConstants.vbBinaryCompare) == 0 && Strings.Right(vsReceipt.TextMatrix(lngRow, AccountCol), 1) != "-")
							{
								if (modGlobalConstants.Statics.gboolBD)
								{
									lblAccountTitle.Text = modAccountTitle.ReturnAccountDescription(vsReceipt.TextMatrix(lngRow, AccountCol));
								}
							}
							else
							{
								lblAccountTitle.Text = "";
							}
						}
						else
						{
							lblAccountTitle.Text = "";
						}
						if (vsReceipt.Col == CategoryCol || vsReceipt.Col == TitleCol || vsReceipt.Col == AbbrevCol)
						{
							vsReceipt.Col = 3;
						}
						else if (vsReceipt.Col == AccountCol)
						{
							if (lngRow > 0)
							{
								vsReceipt.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							}
							else
							{
								vsReceipt.Editable = FCGrid.EditableSettings.flexEDNone;
							}
							// SetGridFormat vsReceipt, vsReceipt.Row, vsReceipt.Col, True
							// .Editable = False
							// txtAcct(lngRow).Text = .TextMatrix(lngRow, .Col)
							// txtAcct(lngRow).Left = .Left + .ColWidth(0) + .ColWidth(1) + .ColWidth(2)
							// txtAcct(lngRow).Top = .Top + (lngRow * .RowHeight(0))
							// txtAcct(lngRow).Tag = lngRow
							// txtAcct(lngRow).Visible = True
							// txtAcct(lngRow).Enabled = True
							// txtAcct(lngRow).SetFocus
						}
						else if (vsReceipt.Col == YearCol)
						{
							// .Editable = True
							vsReceipt.Editable = FCGrid.EditableSettings.flexEDNone;
							if (lngRow == 1 && (intCode == 90 || intCode == 91 || intCode == 92 || intCode == 890 || intCode == 891))
							{
								// always make the pricinpal of a RE or PP receipt selected
								vsReceipt.TextMatrix(lngRow, vsReceipt.Col, FCConvert.ToString(true));
								// k 10032012   -1
							}
							// If Left$(.TextMatrix(lngRow, 3), 1) = "G" Then
							// .Editable = flexEDKbdMouse
							// .EditCell
							// Else
							// .Editable = False
							// If .TextMatrix(lngRow, 4) <> "" Then
							// If .TextMatrix(lngRow, 4) = True Then .TextMatrix(lngRow, 4) = False
							// End If
							// If boolLeftArrow Then
							// .Col = 3
							// Else
							// .Col = 5
							// .Editable = flexEDKbdMouse
							// .EditCell
							// End If
							// End If
						}
						else if (vsReceipt.Col == DefaultCol)
						{
							vsReceipt.Editable = FCGrid.EditableSettings.flexEDNone;
							// .EditMaxLength = 15
							// .EditCell
						}
						else if (vsReceipt.Col == ProjCol)
						{
							if (vsReceipt.ColHidden(ProjCol) == false)
							{
								vsReceipt.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							}
							else
							{
								vsReceipt.Editable = FCGrid.EditableSettings.flexEDNone;
							}
						}
						else if (vsReceipt.Col == PercentCol)
						{
							vsReceipt.Editable = FCGrid.EditableSettings.flexEDNone;
						}
						if (vsReceipt.Col == vsReceipt.RightCol - 1 && lngRow == vsReceipt.BottomRow)
						{
							vsReceipt.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
						}
						else
						{
							vsReceipt.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
						}
						if (vsReceipt.Col == AccountCol)
						{
							// If txtAcct(lngRow).Visible Then
							// txtAcct(lngRow).SetFocus
							// End If
						}
					}
					else
					{
						// show the account description in the label at the bottom of the page
						if (Strings.Trim(vsReceipt.TextMatrix(lngRow, AccountCol)) != "")
						{
							if (Strings.InStr(1, vsReceipt.TextMatrix(lngRow, AccountCol), "_", CompareConstants.vbBinaryCompare) == 0 && Strings.InStr(1, vsReceipt.TextMatrix(lngRow, AccountCol), "--", CompareConstants.vbBinaryCompare) == 0 && Strings.Right(vsReceipt.TextMatrix(lngRow, AccountCol), 1) != "-")
							{
								if (modGlobalConstants.Statics.gboolBD)
								{
									lblAccountTitle.Text = modAccountTitle.ReturnAccountDescription(vsReceipt.TextMatrix(lngRow, AccountCol));
								}
							}
							else
							{
								lblAccountTitle.Text = "";
							}
						}
						else
						{
							lblAccountTitle.Text = "";
						}
						if (vsReceipt.Col == TitleCol)
						{
							if (!modGlobal.Statics.gboolMultipleTowns || (cmbResCode.SelectedIndex != -1 && Conversion.Val(cmbResCode.Items[cmbResCode.SelectedIndex].ToString()) == 1) && lngRow > 0)
							{
								vsReceipt.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
								vsReceipt.EditMaxLength = 15;
								vsReceipt.EditCell();
							}
							else
							{
								vsReceipt.Editable = FCGrid.EditableSettings.flexEDNone;
							}
						}
						else if (vsReceipt.Col == AbbrevCol)
						{
							if (!modGlobal.Statics.gboolMultipleTowns || (cmbResCode.SelectedIndex != -1 && Conversion.Val(cmbResCode.Items[cmbResCode.SelectedIndex].ToString()) == 1) && lngRow > 0)
							{
								vsReceipt.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
								vsReceipt.EditMaxLength = 8;
								vsReceipt.EditCell();
							}
							else
							{
								vsReceipt.Editable = FCGrid.EditableSettings.flexEDNone;
							}
						}
						else if (vsReceipt.Col == AccountCol)
						{
							if (lngRow > 0)
							{
								vsReceipt.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
								modNewAccountBox.SetGridFormat(vsReceipt, vsReceipt.Row, vsReceipt.Col, true);
							}
							else
							{
								vsReceipt.Editable = FCGrid.EditableSettings.flexEDNone;
							}
						}
						else if (vsReceipt.Col == YearCol)
						{
							vsReceipt.Editable = FCGrid.EditableSettings.flexEDNone;
						}
						else if (vsReceipt.Col == DefaultCol)
						{
							if (lngRow > 0)
							{
								vsReceipt.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
								vsReceipt.EditMaxLength = 15;
								vsReceipt.EditCell();
							}
							else
							{
								vsReceipt.Editable = FCGrid.EditableSettings.flexEDNone;
							}
						}
						else if (vsReceipt.Col == ProjCol)
						{
							if (modGlobal.Statics.ProjectFlag)
							{
								if (lngRow > 0)
								{
									vsReceipt.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
								}
								else
								{
									vsReceipt.Editable = FCGrid.EditableSettings.flexEDNone;
								}
							}
						}
						else if (vsReceipt.Col == PercentCol)
						{
							if (lngRow > 0)
							{
								vsReceipt.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							}
							else
							{
								vsReceipt.Editable = FCGrid.EditableSettings.flexEDNone;
							}
						}
						else
						{
							vsReceipt.Editable = FCGrid.EditableSettings.flexEDNone;
						}
						// If .Col = 3 Then
						// If txtAcct(lngRow).Visible Then
						// txtAcct(lngRow).SetFocus
						// End If
						// End If
					}
					if (vsReceipt.Col >= vsReceipt.RightCol - 1 && lngRow == vsReceipt.BottomRow)
					{
						vsReceipt.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
					}
					else
					{
						vsReceipt.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
					}
					boolChangingRows = false;
				}
				else
				{
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Validation Error (Row Column Change)", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ShowFrame(Control fraFrame)
		{
			// this will place the frame in the middle of the form
			//fraFrame.Top = FCConvert.ToInt32((this.Height - fraFrame.Height) / 3.0);
			//fraFrame.Left = FCConvert.ToInt32((this.Width - fraFrame.Width) / 2.0);
			fraFrame.Visible = true;
		}

		private void SetBMV_2(string strVal, bool boolClicked = false)
		{
			SetBMV(ref strVal, boolClicked);
		}

		private void SetBMV_8(string strVal, bool boolClicked = false)
		{
			SetBMV(ref strVal, boolClicked);
		}

		private void SetBMV(ref string strVal, bool boolClicked = false)
		{
			if (strVal == "Y")
			{
				txtRefTitle.Text = "Plate";
				txtCTRL1Title.Text = "Year Sticker";
				txtCTRL2Title.Text = "Month Sticker";
				txtCTRL3Title.Text = "MVR3#";
				txtRefTitle.ReadOnly = true;
				txtCTRL1Title.ReadOnly = true;
				txtCTRL2Title.ReadOnly = true;
				txtCTRL3Title.ReadOnly = true;
				txtRefTitle.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
				txtCTRL1Title.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
				txtCTRL2Title.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
				txtCTRL3Title.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
				txtRefTitle.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT);
				txtCTRL1Title.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT);
				txtCTRL2Title.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT);
				txtCTRL3Title.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT);
				chkMandatory[0].CheckState = Wisej.Web.CheckState.Checked;
				chkMandatory[1].CheckState = Wisej.Web.CheckState.Checked;
				chkMandatory[2].CheckState = Wisej.Web.CheckState.Unchecked;
				chkMandatory[3].CheckState = Wisej.Web.CheckState.Checked;
				chkMandatory[0].Enabled = false;
				chkMandatory[1].Enabled = false;
				chkMandatory[2].Enabled = false;
				chkMandatory[3].Enabled = false;
				if (!boolClicked)
				{
					boolSetBMV = true;
					chkBMV.CheckState = Wisej.Web.CheckState.Checked;
					boolSetBMV = false;
				}
			}
			else
			{
				txtRefTitle.Text = "Reference";
				txtCTRL1Title.Text = "Control1";
				txtCTRL2Title.Text = "Control2";
				txtCTRL3Title.Text = "Control3";
				txtRefTitle.ReadOnly = false;
				txtCTRL1Title.ReadOnly = false;
				txtCTRL2Title.ReadOnly = false;
				txtCTRL3Title.ReadOnly = false;
				txtRefTitle.BackColor = Color.White;
				txtCTRL1Title.BackColor = Color.White;
				txtCTRL2Title.BackColor = Color.White;
				txtCTRL3Title.BackColor = Color.White;
				txtRefTitle.ForeColor = Color.Black;
				txtCTRL1Title.ForeColor = Color.Black;
				txtCTRL2Title.ForeColor = Color.Black;
				txtCTRL3Title.ForeColor = Color.Black;
				chkMandatory[0].CheckState = Wisej.Web.CheckState.Unchecked;
				chkMandatory[1].CheckState = Wisej.Web.CheckState.Unchecked;
				chkMandatory[2].CheckState = Wisej.Web.CheckState.Unchecked;
				chkMandatory[3].CheckState = Wisej.Web.CheckState.Unchecked;
				chkMandatory[0].Enabled = true;
				chkMandatory[1].Enabled = true;
				chkMandatory[2].Enabled = true;
				chkMandatory[3].Enabled = true;
				if (!boolClicked)
				{
					boolSetBMV = true;
					chkBMV.CheckState = Wisej.Web.CheckState.Unchecked;
					boolSetBMV = false;
				}
			}
		}

		private bool SaveCode(int lngForcedResCode = 0)
		{
			bool SaveCode = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this sub will save all of the information, as long as it has a title
				clsDRWrapper rsSave = new clsDRWrapper();
				clsDRWrapper rsMultiSave = new clsDRWrapper();
				clsDRWrapper rsRb = new clsDRWrapper();
				int intCT;
				int lngResCode = 0;
				bool[] boolYear = new bool[7 + 1];
				SaveCode = true;
				for (intCT = 1; intCT <= 6; intCT++)
				{
					// If txtAcct(intCT).Visible Then
					// SendKeys "{tab}"
					// txtAcct_Validate intCT, False
					// End If
				}
				boolSaving = true;
				if (cmbType.Visible)
				{
					cmbType.Focus();
				}
				boolSaving = false;
				if (Strings.Trim(txtTitle.Text) == "")
				{
					MessageBox.Show("Please enter a valid Receipt Title.", "Missing Title", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					SaveCode = false;
					if (txtTitle.Enabled && txtTitle.Visible)
					{
						txtTitle.Focus();
					}
					return SaveCode;
				}
				// kgk 03072012 trocr-325  Type specific service code for InforME
				if (chkAltEPmtAccount.CheckState == Wisej.Web.CheckState.Checked && cboAltEPmtAccount.SelectedIndex == -1)
				{
					if (MessageBox.Show("No InforME Service Code selected.  Continue with the default Service Code?", "Missing Service Code", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
					{
						SaveCode = false;
						cboAltEPmtAccount.Focus();
						return SaveCode;
					}
				}
				if (chkConvenienceFee.CheckState == Wisej.Web.CheckState.Checked && cboConvenienceFeeMethod.SelectedIndex != 1)
				{
					if (txtConvenienceFeeAccount.TextMatrix(0, 0) != "" && Strings.InStr(1, txtConvenienceFeeAccount.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) == 0)
					{
						if (!modValidateAccount.AccountValidate(txtConvenienceFeeAccount.TextMatrix(0, 0)))
						{
							MessageBox.Show("Invalid account number for convenience fee account.", "Invalid Convenience Fee Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							SaveCode = false;
							return SaveCode;
						}
					}
					else if (modGlobalConstants.Statics.gboolBD)
					{
						MessageBox.Show("You must enter a convenience fee account before you may save the type.", "Invalid Convenience Fee Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
						if (txtConvenienceFeeAccount.Visible == true)
						{
							txtConvenienceFeeAccount.Focus();
						}
						SaveCode = false;
						return SaveCode;
					}
				}
				if (!CheckPercentages())
				{
					MessageBox.Show("The percentages entered do not add up to 100%.", "Incorrect Percentage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					SaveCode = false;
					return SaveCode;
				}
				if (modGlobal.Statics.gboolMultipleTowns)
				{
					if (lngForcedResCode != 0)
					{
						lngResCode = lngForcedResCode;
					}
					else
					{
						lngResCode = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbResCode.Items[cmbResCode.SelectedIndex].ToString(), 2))));
					}
				}
				// check to see if there are any receipts of this type that have not been closed out.
				rsSave.OpenRecordset("SELECT * FROM Archive WHERE ISNULL(DailyCloseOut,0) = 0 AND ReceiptType = " + FCConvert.ToString(intCurCode) + " AND TownKey = " + FCConvert.ToString(lngResCode));
				if (rsSave.EndOfFile())
				{
					// this means that there are no receipts with this type so it is ok to let the user save
				}
				else
				{
					if (MessageBox.Show("There is already a receipt, that has not been closed out, of this receipt type.  Would you like to save this type anyway?", "Type In Use", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						modGlobalFunctions.AddCYAEntry_26("CR", "Overriding type setup in order to save.", "Type = " + FCConvert.ToString(intCurCode));
					}
					else
					{
						SaveCode = false;
						return SaveCode;
					}
				}
				if (Strings.Trim(txtTitle.Text) != "")
				{
					rsSave.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(intCurCode) + " AND TownKey = " + FCConvert.ToString(lngResCode));
					if (rsSave.BeginningOfFile() != true && rsSave.EndOfFile() != true)
					{
						rsSave.Edit();
					}
					else
					{
						if (lngForcedResCode != 0)
						{
							rsSave.AddNew();
							rsSave.Set_Fields("TypeCode", intCurCode);
							rsSave.Set_Fields("TownKey", lngResCode);
						}
						else
						{
							switch (MessageBox.Show("Type Code not found would you like to add it?", "New Receipt Type", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
							{
								case DialogResult.No:
									{
										SaveCode = false;
										return SaveCode;
									}
								case DialogResult.Yes:
									{
										rsSave.AddNew();
										rsSave.Set_Fields("TypeCode", intCurCode);
										rsSave.Set_Fields("TownKey", lngResCode);
										break;
									}
							}
							//end switch
						}
					}
					if (modGlobal.Statics.gboolMultipleTowns)
					{
						// this should save the same info for all types
						if (lngResCode == 1)
						{
							rsMultiSave.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(intCurCode) + " AND TownKey <> " + FCConvert.ToString(lngResCode), modExtraModules.strCRDatabase);
							while (!rsMultiSave.EndOfFile())
							{
								rsMultiSave.Edit();
								rsMultiSave.Set_Fields("TypeTitle", Strings.Trim(txtTitle.Text));
								rsMultiSave.Set_Fields("BMV", FCConvert.CBool(chkBMV.CheckState == Wisej.Web.CheckState.Checked));
								rsMultiSave.Set_Fields("Reference", Strings.Trim(txtRefTitle.Text));
								rsMultiSave.Set_Fields("ReferenceMandatory", chkMandatory[0].CheckState);
								rsMultiSave.Set_Fields("Control1", Strings.Trim(txtCTRL1Title.Text));
								rsMultiSave.Set_Fields("Control1Mandatory", chkMandatory[1].CheckState);
								rsMultiSave.Set_Fields("Control2", Strings.Trim(txtCTRL2Title.Text));
								rsMultiSave.Set_Fields("Control2Mandatory", chkMandatory[2].CheckState);
								rsMultiSave.Set_Fields("Control3", Strings.Trim(txtCTRL3Title.Text));
								rsMultiSave.Set_Fields("Control3Mandatory", chkMandatory[3].CheckState);
								rsMultiSave.Set_Fields("Copies", FCConvert.ToString(Conversion.Val(Strings.Left(cmbCopies.Items[cmbCopies.SelectedIndex].ToString(), 1))));
								rsMultiSave.Set_Fields("Print", FCConvert.CBool(chkPrint.CheckState == Wisej.Web.CheckState.Checked));
								rsMultiSave.Set_Fields("ShowFeeDetailOnReceipt", FCConvert.CBool(chkDetail.CheckState == Wisej.Web.CheckState.Checked));
								rsMultiSave.Set_Fields("EFT", FCConvert.CBool(chkEFT.CheckState == Wisej.Web.CheckState.Checked));
								rsMultiSave.Set_Fields("Title1", vsReceipt.TextMatrix(1, TitleCol));
								rsMultiSave.Set_Fields("Title1Abbrev", vsReceipt.TextMatrix(1, AbbrevCol));
								rsMultiSave.Set_Fields("Title2", vsReceipt.TextMatrix(2, TitleCol));
								rsMultiSave.Set_Fields("Title2Abbrev", vsReceipt.TextMatrix(2, AbbrevCol));
								rsMultiSave.Set_Fields("Title3", vsReceipt.TextMatrix(3, TitleCol));
								rsMultiSave.Set_Fields("Title3Abbrev", vsReceipt.TextMatrix(3, AbbrevCol));
								rsMultiSave.Set_Fields("Title4", vsReceipt.TextMatrix(4, TitleCol));
								rsMultiSave.Set_Fields("Title4Abbrev", vsReceipt.TextMatrix(4, AbbrevCol));
								rsMultiSave.Set_Fields("Title5", vsReceipt.TextMatrix(5, TitleCol));
								rsMultiSave.Set_Fields("Title5Abbrev", vsReceipt.TextMatrix(5, AbbrevCol));
								rsMultiSave.Set_Fields("Title6", vsReceipt.TextMatrix(6, TitleCol));
								rsMultiSave.Set_Fields("Title6Abbrev", vsReceipt.TextMatrix(6, AbbrevCol));
								// MAL@20071010
								rsMultiSave.Set_Fields("Deleted", FCConvert.CBool(chkDeleted.CheckState == Wisej.Web.CheckState.Checked));
								rsMultiSave.Update();
								rsMultiSave.MoveNext();
							}
						}
					}
					rsSave.Set_Fields("TypeTitle", Strings.Trim(txtTitle.Text));
					rsSave.Set_Fields("BMV", FCConvert.CBool(chkBMV.CheckState == Wisej.Web.CheckState.Checked));
					rsSave.Set_Fields("Reference", Strings.Trim(txtRefTitle.Text));
					rsSave.Set_Fields("ReferenceMandatory", chkMandatory[0].CheckState);
					rsSave.Set_Fields("Control1", Strings.Trim(txtCTRL1Title.Text));
					rsSave.Set_Fields("Control1Mandatory", chkMandatory[1].CheckState);
					rsSave.Set_Fields("Control2", Strings.Trim(txtCTRL2Title.Text));
					rsSave.Set_Fields("Control2Mandatory", chkMandatory[2].CheckState);
					rsSave.Set_Fields("Control3", Strings.Trim(txtCTRL3Title.Text));
					rsSave.Set_Fields("Control3Mandatory", chkMandatory[3].CheckState);
					rsSave.Set_Fields("Copies", FCConvert.ToString(Conversion.Val(Strings.Left(cmbCopies.Items[cmbCopies.SelectedIndex].ToString(), 1))));
					rsSave.Set_Fields("Print", FCConvert.CBool(chkPrint.CheckState == Wisej.Web.CheckState.Checked));
					rsSave.Set_Fields("ShowFeeDetailOnReceipt", FCConvert.CBool(chkDetail.CheckState == Wisej.Web.CheckState.Checked));
					rsSave.Set_Fields("EFT", FCConvert.CBool(chkEFT.CheckState == Wisej.Web.CheckState.Checked));
					// MAL@20071010
					rsSave.Set_Fields("Deleted", FCConvert.CBool(chkDeleted.CheckState == Wisej.Web.CheckState.Checked));
					// kgk 03072012 trocr-325  Type specific service code for InforME
					rsSave.Set_Fields("EPmtAcctID", cboAltEPmtAccount.Text);
					if (chkConvenienceFee.CheckState == Wisej.Web.CheckState.Checked)
					{
						rsSave.Set_Fields("ConvenienceFeeOverride", true);
						if (cboConvenienceFeeMethod.SelectedIndex == 0)
						{
							rsSave.Set_Fields("ConvenienceFeeMethod", "P");
							rsSave.Set_Fields("ConveniencePercentage", FCConvert.ToDouble(txtPerDiem.Text) / 100);
							rsSave.Set_Fields("ConvenienceFeeGLAccount", txtConvenienceFeeAccount.TextMatrix(0, 0));
						}
						else
						{
							rsSave.Set_Fields("ConvenienceFeeMethod", "N");
							rsSave.Set_Fields("ConveniencePercentage", 0);
							rsSave.Set_Fields("ConvenienceFeeGLAccount", "");
						}
					}
					else
					{
						rsSave.Set_Fields("ConvenienceFeeOverride", false);
						rsSave.Set_Fields("ConvenienceFeeMethod", "N");
						rsSave.Set_Fields("ConveniencePercentage", 0);
						rsSave.Set_Fields("ConvenienceFeeGLAccount", "");
					}
					if (FCConvert.CBool(chkRB.CheckState == Wisej.Web.CheckState.Checked))
					{
						rsRb.OpenRecordset("SELECT * FROM Type WHERE RBType = True AND TypeCode <> " + FCConvert.ToString(intCurCode));
						if (rsRb.EndOfFile())
						{
							// there is no other type with this field set
							rsSave.Set_Fields("RBType", true);
						}
						else
						{
							// there is already a type with the fields set to true
							if (MessageBox.Show("There is already a type that is associated with Blue Book.  Would you like to change the association to Type " + FCConvert.ToString(intCurCode) + "?", "Change Association", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
							{
								rsSave.Set_Fields("RBType", true);
								// change the old type
								rsRb.Edit();
								rsRb.Set_Fields("RBType", false);
								rsRb.Update();
							}
							else
							{
								rsSave.Set_Fields("RBType", false);
							}
						}
					}
					else
					{
						rsSave.Set_Fields("RBType", false);
					}
					if (Conversion.Val(vsReceipt.TextMatrix(1, PercentCol)) == -1)
					{
						rsSave.Set_Fields("PercentageFee1", true);
					}
					else
					{
						rsSave.Set_Fields("PercentageFee1", false);
					}
					if (Conversion.Val(vsReceipt.TextMatrix(2, PercentCol)) == -1)
					{
						rsSave.Set_Fields("PercentageFee2", true);
					}
					else
					{
						rsSave.Set_Fields("PercentageFee2", false);
					}
					if (Conversion.Val(vsReceipt.TextMatrix(3, PercentCol)) == -1)
					{
						rsSave.Set_Fields("PercentageFee3", true);
					}
					else
					{
						rsSave.Set_Fields("PercentageFee3", false);
					}
					if (Conversion.Val(vsReceipt.TextMatrix(4, PercentCol)) == -1)
					{
						rsSave.Set_Fields("PercentageFee4", true);
					}
					else
					{
						rsSave.Set_Fields("PercentageFee4", false);
					}
					if (Conversion.Val(vsReceipt.TextMatrix(5, PercentCol)) == -1)
					{
						rsSave.Set_Fields("PercentageFee5", true);
					}
					else
					{
						rsSave.Set_Fields("PercentageFee5", false);
					}
					if (Conversion.Val(vsReceipt.TextMatrix(6, PercentCol)) == -1)
					{
						rsSave.Set_Fields("PercentageFee6", true);
					}
					else
					{
						rsSave.Set_Fields("PercentageFee6", false);
					}
					if (Strings.Trim(vsReceipt.TextMatrix(1, TitleCol)) != "")
					{
						// And .TextMatrix(1, 3) <> "" Then
						if (!CheckStandardAccountType_2(Strings.Trim(vsReceipt.TextMatrix(1, AccountCol))))
						{
							MessageBox.Show("Invalid standard account number for fee 1.  You will not be able to use this fee until this is corrected.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							SaveCode = false;
							return SaveCode;
						}
						rsSave.Set_Fields("Title1", vsReceipt.TextMatrix(1, TitleCol));
						if (Strings.Trim(vsReceipt.TextMatrix(1, AbbrevCol)) == "")
						{
							rsSave.Set_Fields("Title1Abbrev", Strings.Trim(Strings.Left(vsReceipt.TextMatrix(1, TitleCol) + "        ", 8)));
						}
						else
						{
							rsSave.Set_Fields("Title1Abbrev", vsReceipt.TextMatrix(1, AbbrevCol));
						}
						rsSave.Set_Fields("Account1", Strings.Trim(RemoveUnderscore(vsReceipt.TextMatrix(1, AccountCol))));
						if (Conversion.Val(vsReceipt.TextMatrix(1, DefaultCol)) != 0)
						{
							rsSave.Set_Fields("DefaultAmount1", modGlobal.Round(FCConvert.ToDouble(vsReceipt.TextMatrix(1, DefaultCol)), 2));
						}
						else
						{
							rsSave.Set_Fields("DefaultAmount1", 0);
						}
						if (modGlobal.Statics.ProjectFlag)
						{
							rsSave.Set_Fields("Project1", Strings.Trim(vsReceipt.TextMatrix(1, ProjCol)));
						}
						boolYear[1] = !FCConvert.CBool(FCConvert.CBool(rsSave.Get_Fields_Boolean("Year1")) == FCConvert.CBool(vsReceipt.TextMatrix(1, YearCol)));
						rsSave.Set_Fields("Year1", FCConvert.CBool(vsReceipt.TextMatrix(1, YearCol)));
						if (!modValidateAccount.AccountValidate(rsSave.Get_Fields_String("Account1")))
						{
							if (modGlobalConstants.Statics.gboolAR && intCurCode == 97)
							{
								// Do Nothing - Validation Not Necessary on Principal Account
							}
							else
							{
								if (FCConvert.ToInt32(rsSave.Get_Fields_Int32("TownKey")) != 0)
								{
									rsSave.Set_Fields("Account1", "");
								}
								else
								{
									MessageBox.Show("Invalid account number for fee 1.  You will not be able to use this fee until this is corrected.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
									SaveCode = false;
									return SaveCode;
								}
							}
						}
					}
					else
					{
						if (!RestrictedCode())
						{
							rsSave.Set_Fields("Title1", "");
							rsSave.Set_Fields("Title1Abbrev", "");
						}
						rsSave.Set_Fields("Account1", "");
						rsSave.Set_Fields("Project1", "");
						rsSave.Set_Fields("DefaultAmount1", 0);
						rsSave.Set_Fields("Year1", false);
					}
					if (Strings.Trim(vsReceipt.TextMatrix(2, TitleCol)) != "")
					{
						// And .TextMatrix(2, 3) <> "" Then
						rsSave.Set_Fields("Title2", vsReceipt.TextMatrix(2, TitleCol));
						if (Strings.Trim(vsReceipt.TextMatrix(2, AbbrevCol)) == "")
						{
							rsSave.Set_Fields("Title2Abbrev", Strings.Trim(Strings.Left(vsReceipt.TextMatrix(2, TitleCol) + "        ", 8)));
						}
						else
						{
							rsSave.Set_Fields("Title2Abbrev", vsReceipt.TextMatrix(2, AbbrevCol));
						}
						rsSave.Set_Fields("Account2", Strings.Trim(RemoveUnderscore(vsReceipt.TextMatrix(2, AccountCol))));
						if (Conversion.Val(vsReceipt.TextMatrix(2, DefaultCol)) != 0)
						{
							rsSave.Set_Fields("DefaultAmount2", FCUtils.Round(FCConvert.ToDouble(vsReceipt.TextMatrix(2, DefaultCol)), 2));
						}
						else
						{
							rsSave.Set_Fields("DefaultAmount2", 0);
						}
						if (modGlobal.Statics.ProjectFlag)
						{
							rsSave.Set_Fields("Project2", Strings.Trim(vsReceipt.TextMatrix(2, ProjCol)));
						}
						boolYear[2] = !FCConvert.CBool(FCConvert.CBool(rsSave.Get_Fields_Boolean("Year2")) == (FCConvert.CBool(vsReceipt.TextMatrix(2, YearCol)) == true));
						rsSave.Set_Fields("Year2", FCConvert.CBool(vsReceipt.TextMatrix(2, YearCol)) == true);
						if (!modValidateAccount.AccountValidate(rsSave.Get_Fields_String("Account2")))
						{
							if (FCConvert.ToInt32(rsSave.Get_Fields_Int32("TownKey")) != 0)
							{
								rsSave.Set_Fields("Account2", "");
							}
							else
							{
								MessageBox.Show("Invalid account number for fee 2.  You will not be able to use this fee until this is corrected.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
								SaveCode = false;
								return SaveCode;
							}
						}
					}
					else
					{
						if (!RestrictedCode())
						{
							rsSave.Set_Fields("Title2", "");
							rsSave.Set_Fields("Title2Abbrev", "");
						}
						rsSave.Set_Fields("Account2", "");
						rsSave.Set_Fields("Project2", "");
						rsSave.Set_Fields("DefaultAmount2", 0);
						rsSave.Set_Fields("Year2", false);
					}
					if (Strings.Trim(vsReceipt.TextMatrix(3, TitleCol)) != "")
					{
						// And .TextMatrix(3, 3) <> "" Then
						rsSave.Set_Fields("Title3", vsReceipt.TextMatrix(3, TitleCol));
						if (Strings.Trim(vsReceipt.TextMatrix(3, AbbrevCol)) == "")
						{
							rsSave.Set_Fields("Title3Abbrev", Strings.Trim(Strings.Left(vsReceipt.TextMatrix(3, TitleCol) + "        ", 8)));
						}
						else
						{
							rsSave.Set_Fields("Title3Abbrev", vsReceipt.TextMatrix(3, AbbrevCol));
						}
						boolYear[3] = !FCConvert.CBool(FCConvert.CBool(rsSave.Get_Fields_Boolean("Year3")) == (FCConvert.CBool(vsReceipt.TextMatrix(3, YearCol)) == true));
						rsSave.Set_Fields("Account3", Strings.Trim(RemoveUnderscore(vsReceipt.TextMatrix(3, AccountCol))));
						if (Conversion.Val(vsReceipt.TextMatrix(3, DefaultCol)) != 0)
						{
							rsSave.Set_Fields("DefaultAmount3", FCUtils.Round(FCConvert.ToDouble(vsReceipt.TextMatrix(3, DefaultCol)), 2));
						}
						else
						{
							rsSave.Set_Fields("DefaultAmount3", 0);
						}
						if (modGlobal.Statics.ProjectFlag)
						{
							rsSave.Set_Fields("Project3", Strings.Trim(vsReceipt.TextMatrix(3, ProjCol)));
						}
						rsSave.Set_Fields("Year3", FCConvert.CBool(FCConvert.CBool(vsReceipt.TextMatrix(3, YearCol)) == true));
						if (!modValidateAccount.AccountValidate(rsSave.Get_Fields_String("Account3")))
						{
							if (FCConvert.ToInt32(rsSave.Get_Fields_Int32("TownKey")) != 0)
							{
								rsSave.Set_Fields("Account3", "");
							}
							else
							{
								MessageBox.Show("Invalid account number for fee 3.  You will not be able to use this fee until this is corrected.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
								SaveCode = false;
								return SaveCode;
							}
						}
					}
					else
					{
						if (!RestrictedCode())
						{
							rsSave.Set_Fields("Title3", "");
							rsSave.Set_Fields("Title3Abbrev", "");
						}
						rsSave.Set_Fields("Account3", "");
						rsSave.Set_Fields("Project3", "");
						rsSave.Set_Fields("DefaultAmount3", 0);
						rsSave.Set_Fields("Year3", false);
					}
					if (Strings.Trim(vsReceipt.TextMatrix(4, TitleCol)) != "")
					{
						// And .TextMatrix(4, 3) <> "" Then
						rsSave.Set_Fields("Title4", vsReceipt.TextMatrix(4, TitleCol));
						if (Strings.Trim(vsReceipt.TextMatrix(4, AbbrevCol)) == "")
						{
							rsSave.Set_Fields("Title4Abbrev", Strings.Trim(Strings.Left(vsReceipt.TextMatrix(4, TitleCol) + "        ", 8)));
						}
						else
						{
							rsSave.Set_Fields("Title4Abbrev", vsReceipt.TextMatrix(4, AbbrevCol));
						}
						if (modGlobal.Statics.ProjectFlag)
						{
							rsSave.Set_Fields("Project4", Strings.Trim(vsReceipt.TextMatrix(4, ProjCol)));
						}
						rsSave.Set_Fields("Account4", Strings.Trim(RemoveUnderscore(vsReceipt.TextMatrix(4, AccountCol))));
						if (Conversion.Val(vsReceipt.TextMatrix(4, DefaultCol)) != 0)
						{
							rsSave.Set_Fields("DefaultAmount4", FCUtils.Round(FCConvert.ToDouble(vsReceipt.TextMatrix(4, DefaultCol)), 2));
						}
						else
						{
							rsSave.Set_Fields("DefaultAmount4", 0);
						}
						boolYear[4] = !FCConvert.CBool(FCConvert.CBool(rsSave.Get_Fields_Boolean("Year4")) == (FCConvert.CBool(vsReceipt.TextMatrix(4, YearCol)) == true));
						rsSave.Set_Fields("Year4", FCConvert.CBool(vsReceipt.TextMatrix(4, YearCol)) == true);
						if (!modValidateAccount.AccountValidate(rsSave.Get_Fields_String("Account4")))
						{
							if (FCConvert.ToInt32(rsSave.Get_Fields_Int32("TownKey")) != 0)
							{
								rsSave.Set_Fields("Account4", "");
							}
							else
							{
								MessageBox.Show("Invalid account number for fee 4.  You will not be able to use this fee until this is corrected.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
								SaveCode = false;
								return SaveCode;
							}
						}
					}
					else
					{
						if (!RestrictedCode())
						{
							rsSave.Set_Fields("Title4", "");
							rsSave.Set_Fields("Title4Abbrev", "");
						}
						rsSave.Set_Fields("Account4", "");
						rsSave.Set_Fields("Project4", "");
						rsSave.Set_Fields("DefaultAmount4", 0);
						rsSave.Set_Fields("Year4", false);
					}
					if (Strings.Trim(vsReceipt.TextMatrix(5, TitleCol)) != "")
					{
						// And .TextMatrix(5, 3) <> "" Then
						rsSave.Set_Fields("Title5", vsReceipt.TextMatrix(5, TitleCol));
						if (Strings.Trim(vsReceipt.TextMatrix(5, AbbrevCol)) == "")
						{
							rsSave.Set_Fields("Title5Abbrev", Strings.Trim(Strings.Left(vsReceipt.TextMatrix(5, TitleCol) + "        ", 8)));
						}
						else
						{
							rsSave.Set_Fields("Title5Abbrev", vsReceipt.TextMatrix(5, AbbrevCol));
						}
						if (modGlobal.Statics.ProjectFlag)
						{
							rsSave.Set_Fields("Project5", Strings.Trim(vsReceipt.TextMatrix(5, ProjCol)));
						}
						rsSave.Set_Fields("Account5", Strings.Trim(RemoveUnderscore(vsReceipt.TextMatrix(5, AccountCol))));
						if (Conversion.Val(vsReceipt.TextMatrix(5, DefaultCol)) != 0)
						{
							rsSave.Set_Fields("DefaultAmount5", modGlobal.Round(FCConvert.ToDouble(vsReceipt.TextMatrix(5, DefaultCol)), 2));
						}
						else
						{
							rsSave.Set_Fields("DefaultAmount5", 0);
						}
						boolYear[5] = !FCConvert.CBool(FCConvert.CBool(rsSave.Get_Fields_Boolean("Year5")) == (FCConvert.CBool(vsReceipt.TextMatrix(5, YearCol)) == true));
						rsSave.Set_Fields("Year5", FCConvert.CBool(vsReceipt.TextMatrix(5, YearCol)) == true);
						if (!modValidateAccount.AccountValidate(rsSave.Get_Fields_String("Account5")))
						{
							if (FCConvert.ToInt32(rsSave.Get_Fields_Int32("TownKey")) != 0)
							{
								rsSave.Set_Fields("Account5", "");
							}
							else
							{
								MessageBox.Show("Invalid account number for fee 5.  You will not be able to use this fee until this is corrected.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
								SaveCode = false;
								return SaveCode;
							}
						}
					}
					else
					{
						if (!RestrictedCode())
						{
							rsSave.Set_Fields("Title5", "");
							rsSave.Set_Fields("Title5Abbrev", "");
						}
						rsSave.Set_Fields("Account5", "");
						rsSave.Set_Fields("Project5", "");
						rsSave.Set_Fields("DefaultAmount5", 0);
						rsSave.Set_Fields("Year5", false);
					}
					if (Strings.Trim(vsReceipt.TextMatrix(6, TitleCol)) != "")
					{
						// And .TextMatrix(6, 3) <> "" Then
						rsSave.Set_Fields("Title6", vsReceipt.TextMatrix(6, TitleCol));
						if (Strings.Trim(vsReceipt.TextMatrix(6, AbbrevCol)) == "")
						{
							rsSave.Set_Fields("Title6Abbrev", Strings.Trim(Strings.Left(vsReceipt.TextMatrix(6, TitleCol) + "        ", 8)));
						}
						else
						{
							rsSave.Set_Fields("Title6Abbrev", vsReceipt.TextMatrix(6, AbbrevCol));
						}
						if (modGlobal.Statics.ProjectFlag)
						{
							rsSave.Set_Fields("Project6", Strings.Trim(vsReceipt.TextMatrix(6, ProjCol)));
						}
						rsSave.Set_Fields("Account6", Strings.Trim(RemoveUnderscore(vsReceipt.TextMatrix(6, AccountCol))));
						if (Conversion.Val(vsReceipt.TextMatrix(6, DefaultCol)) != 0)
						{
							rsSave.Set_Fields("DefaultAmount6", FCUtils.Round(FCConvert.ToDouble(vsReceipt.TextMatrix(6, DefaultCol)), 2));
						}
						else
						{
							rsSave.Set_Fields("DefaultAmount6", 0);
						}
						boolYear[6] = !FCConvert.CBool(FCConvert.CBool(rsSave.Get_Fields_Boolean("Year6")) == (FCConvert.CBool(vsReceipt.TextMatrix(6, YearCol)) == true));
						rsSave.Set_Fields("Year6", FCConvert.CBool(vsReceipt.TextMatrix(6, YearCol)) == true);
						if (!modValidateAccount.AccountValidate(rsSave.Get_Fields_String("Account6")))
						{
							if (FCConvert.ToInt32(rsSave.Get_Fields_Int32("TownKey")) != 0)
							{
								rsSave.Set_Fields("Account6", "");
							}
							else
							{
								MessageBox.Show("Invalid account number for fee 6.  You will not be able to use this fee until this is corrected.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
								SaveCode = false;
								return SaveCode;
							}
						}
					}
					else
					{
						if (!RestrictedCode())
						{
							rsSave.Set_Fields("Title6", "");
							rsSave.Set_Fields("Title6Abbrev", "");
						}
						rsSave.Set_Fields("Account6", "");
						rsSave.Set_Fields("Project6", "");
						rsSave.Set_Fields("DefaultAmount6", 0);
						rsSave.Set_Fields("Year6", false);
					}
					if (boolYear[1] || boolYear[2] || boolYear[3] || boolYear[4] || boolYear[5] || boolYear[6])
					{
						modGlobalFunctions.AddCYAEntry_728("CR", "Changing the year flag in Type:" + FCConvert.ToString(intCurCode), " 1 : " + FCConvert.ToString(boolYear[1]) + " - " + " 2 : " + FCConvert.ToString(boolYear[2]), "3 : " + FCConvert.ToString(boolYear[3]) + " 4 : " + FCConvert.ToString(boolYear[4]), "5 : " + FCConvert.ToString(boolYear[5]) + " 6 : " + FCConvert.ToString(boolYear[6]), "Values : " + FCConvert.ToString(FCConvert.CBool(rsSave.Get_Fields_Boolean("Year1"))) + " " + FCConvert.ToString(FCConvert.CBool(rsSave.Get_Fields_Boolean("Year2"))) + " " + FCConvert.ToString(FCConvert.CBool(rsSave.Get_Fields_Boolean("Year3"))) + " " + FCConvert.ToString(FCConvert.CBool(rsSave.Get_Fields_Boolean("Year4"))) + " " + FCConvert.ToString(FCConvert.CBool(rsSave.Get_Fields_Boolean("Year5"))) + " " + FCConvert.ToString(FCConvert.CBool(rsSave.Get_Fields_Boolean("Year6"))));
					}
					if (txtDefaultAccount.TextMatrix(0, 0) != "" && Strings.InStr(1, txtDefaultAccount.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) == 0 && Strings.Trim(txtDefaultAccount.TextMatrix(0, 0)) != "M")
					{
						rsSave.Set_Fields("DefaultAccount", txtDefaultAccount.TextMatrix(0, 0));
					}
					else
					{
						rsSave.Set_Fields("DefaultAccount", "");
					}
					if (rsSave.Update())
					{
						boolDirty = false;
					}
				}
				else
				{
					// this will clear the type code
					if (boolDirty)
					{
						if (rsSave.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(intCurCode)))
						{
							if (rsSave.BeginningOfFile() != true && rsSave.EndOfFile() != true)
							{
								if (FCConvert.ToString(rsSave.Get_Fields_String("TypeTitle")) != "")
								{
									if (MessageBox.Show("Are you sure that you would like this Code cleared?", "Clear Code", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
									{
										rsSave.Edit();
										rsSave.Set_Fields("TypeTitle", "");
										rsSave.Set_Fields("BMV", false);
										rsSave.Set_Fields("Reference", "Reference");
										rsSave.Set_Fields("Control1", "Control1");
										rsSave.Set_Fields("Control2", "Control2");
										rsSave.Set_Fields("Control3", "Control3");
										rsSave.Set_Fields("Copies", 1);
										rsSave.Set_Fields("Print", true);
										rsSave.Set_Fields("DefaultAccount", "");
										rsSave.Set_Fields("EFT", false);
										rsSave.Set_Fields("Title1", "");
										rsSave.Set_Fields("Title1Abbrev", "");
										rsSave.Set_Fields("Account1", 0);
										rsSave.Set_Fields("Project1", "");
										rsSave.Set_Fields("DefaultAmount1", 0);
										rsSave.Set_Fields("Year1", false);
										rsSave.Set_Fields("Title2", "");
										rsSave.Set_Fields("Title2Abbrev", "");
										rsSave.Set_Fields("Account2", 0);
										rsSave.Set_Fields("Project2", "");
										rsSave.Set_Fields("DefaultAmount2", 0);
										rsSave.Set_Fields("Year2", false);
										rsSave.Set_Fields("Title3", "");
										rsSave.Set_Fields("Title3Abbrev", "");
										rsSave.Set_Fields("Account3", 0);
										rsSave.Set_Fields("Project3", "");
										rsSave.Set_Fields("DefaultAmount3", 0);
										rsSave.Set_Fields("Year3", false);
										rsSave.Set_Fields("Title4", "");
										rsSave.Set_Fields("Title4Abbrev", "");
										rsSave.Set_Fields("Account4", 0);
										rsSave.Set_Fields("Project4", "");
										rsSave.Set_Fields("DefaultAmount4", 0);
										rsSave.Set_Fields("Year4", false);
										rsSave.Set_Fields("Title5", "");
										rsSave.Set_Fields("Title5Abbrev", "");
										rsSave.Set_Fields("Account5", 0);
										rsSave.Set_Fields("Project5", "");
										rsSave.Set_Fields("DefaultAmount5", 0);
										rsSave.Set_Fields("Year5", false);
										rsSave.Set_Fields("Title6", "");
										rsSave.Set_Fields("Title6Abbrev", "");
										rsSave.Set_Fields("Account6", 0);
										rsSave.Set_Fields("Project6", "");
										rsSave.Set_Fields("DefaultAmount6", 0);
										rsSave.Set_Fields("Year6", false);
										if (rsSave.Update())
										{
											boolDirty = false;
										}
									}
									else
									{
										// nothing
									}
								}
								else
								{
								}
							}
						}
						else
						{
							MessageBox.Show("Error #" + FCConvert.ToString(rsSave.ErrorNumber) + " - " + rsSave.ErrorDescription + ".", "Clear Type Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							SaveCode = false;
							return SaveCode;
						}
					}
					else
					{
					}
				}
				//Application.DoEvents();
				FillCodeCombo();
				// For intCT = 1 To 6
				// If txtAcct(intCT).Visible Then
				// txtAcct(intCT).Visible = False
				// End If
				// Next
				if (txtCode.Visible && txtCode.Enabled)
				{
					txtCode.Focus();
				}
				return SaveCode;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving Code", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveCode;
		}

		private void MoveBack()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				CheckForSave();
				if (cmbType.SelectedIndex <= 0)
				{
					// it is at the beginning
					cmbType.SelectedIndex = cmbType.Items.Count - 1;
				}
				else
				{
					cmbType.SelectedIndex = cmbType.SelectedIndex - 1;
				}
				if (modGlobal.Statics.gboolMultipleTowns)
				{
					ShowType_8(FCConvert.ToInt16(Conversion.Val(cmbType.Items[cmbType.SelectedIndex].ToString())), 1);
				}
				else
				{
					ShowType_2(FCConvert.ToInt16(Conversion.Val(cmbType.Items[cmbType.SelectedIndex].ToString())));
				}
				intCurCode = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbType.Items[cmbType.SelectedIndex].ToString())));
				// txtAcct(6).Visible = False
				// txtAcct(1).Visible = False
				// txtAcct(2).Visible = False
				// txtAcct(3).Visible = False
				// txtAcct(4).Visible = False
				// txtAcct(5).Visible = False
				if (txtTitle.Enabled && txtTitle.Visible)
				{
					txtTitle.Focus();
				}
				else if (cmbType.Visible && cmbType.Enabled)
				{
					cmbType.Focus();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Move Back", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void MoveForward()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intIndex;
				CheckForSave();
				// this will find the code index
				for (intIndex = 0; intIndex <= cmbType.Items.Count - 1; intIndex++)
				{
					if (intCurCode == Conversion.Val(Strings.Left(cmbType.Items[intIndex].ToString(), 3)))
					{
						break;
					}
				}
				// ShowType Val(Left(cmbType.List(intIndex), 3))
				// intCurCode = Val(Left(cmbType.List(intIndex), 3))
				// txtAcct.Visible = False
				if (intIndex < cmbType.Items.Count - 1)
				{
					// this will get the next index
					cmbType.SelectedIndex = intIndex + 1 % cmbType.Items.Count;
				}
				else if (cmbType.Items.Count > 0)
				{
					cmbType.SelectedIndex = 0;
				}
				else
				{
					cmbType.SelectedIndex = -1;
				}
				// cmbCode.SetFocus
				// cmbCode.ComboIndex = ((cmbCode.ComboIndex + 1) Mod cmbCode.ComboCount)
				// If Trim$(cmbCode.ComboItem(cmbCode.ComboIndex) & " ") <> "" Then
				// ShowType Val(cmbCode.ComboItem(cmbCode.ComboIndex))
				// txtAcct.Visible = False
				// cmbCode.Text = cmbCode.ComboItem(cmbCode.ComboIndex)
				// End If
				if (txtTitle.Enabled && txtTitle.Visible)
				{
					txtTitle.Focus();
				}
				else if (cmbType.Visible && cmbType.Enabled)
				{
					cmbType.Focus();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Move Forward", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool CheckForSave()
		{
			bool CheckForSave = false;
			// vbPorter upgrade warning: intTemp As short, int --> As DialogResult
			DialogResult intTemp;
			CheckForSave = true;
			if (boolDirty)
			{
				if (Conversion.Val(Strings.Left(cmbType.Items[cmbType.SelectedIndex].ToString(), 3)) != 0)
				{
					if (intCurCode > 0)
					{
						intTemp = MessageBox.Show("Do you want to save the changes in Code " + FCConvert.ToString(intCurCode) + "?", "Save Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
						if (intTemp == DialogResult.Yes)
						{
							// yes
							// save it and exit
							SaveCode();
						}
						else if (intTemp == DialogResult.No)
						{
							// no
							// do nothing
						}
					}
					else
					{
						// CheckForSave = False
					}
				}
				else
				{
				}
				// If Val(cmbCode.ComboItem(cmbCode.ComboIndex)) <> 0 Then
				// intTemp = MsgBox("Do you want to save the changes in Code " & intCurCode & "?", vbYesNo + vbQuestion, "Save Changes")
				// If intTemp = 6 Then         'yes
				// save it and exit
				// SaveCode
				// ElseIf intTemp = 7 Then     'no
				// do nothing
				// End If
				// End If
			}
			boolDirty = false;
			return CheckForSave;
		}
		// vbPorter upgrade warning: lngCode As int	OnWrite(double, int)
		private bool RestrictedCode_2(int lngCode)
		{
			return RestrictedCode(lngCode);
		}

		private bool RestrictedCode(int lngCode = 0)
		{
			bool RestrictedCode = false;
			// this will return true if this is a restricted code
			int intTypeCode;
			if (cmbType.SelectedIndex != -1 || lngCode != 0)
			{
				// If cmbCode.ComboIndex <> -1 Then
				if (lngCode != 0)
				{
					intTypeCode = lngCode;
				}
				else
				{
					intTypeCode = FCConvert.ToInt16(intCurCode);
				}
				if ((intTypeCode >= 90 && intTypeCode <= 99) || (intTypeCode >= 190 && intTypeCode <= 199) || (intTypeCode >= 290 && intTypeCode <= 299) || (intTypeCode >= 390 && intTypeCode <= 399) || (intTypeCode >= 490 && intTypeCode <= 499) || (intTypeCode >= 590 && intTypeCode <= 599) || (intTypeCode >= 690 && intTypeCode <= 699) || (intTypeCode >= 790 && intTypeCode <= 799) || (intTypeCode >= 800 && intTypeCode <= 899) || (intTypeCode >= 900 && intTypeCode <= 999))
				{
					RestrictedCode = true;
				}
				else
				{
					RestrictedCode = false;
				}
			}
			else
			{
				RestrictedCode = true;
			}
			return RestrictedCode;
		}

		private bool RestrictForBMV()
		{
			bool RestrictForBMV = false;
			if (chkBMV.CheckState == Wisej.Web.CheckState.Checked)
			{
				RestrictForBMV = true;
			}
			else
			{
				RestrictForBMV = false;
			}
			return RestrictForBMV;
		}

		private void ProcessType()
		{
			int intTypeCode = 0;
			bool boolCancel;
			int intTemp;
			clsDRWrapper rsTemp;
			int lngResCode = 0;
			boolCancel = false;
			if (cmbType.SelectedIndex != -1)
			{
				intTypeCode = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbType.Items[cmbType.SelectedIndex].ToString(), 3))));
				if (modGlobal.Statics.gboolMultipleTowns)
				{
					lngResCode = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbResCode.Items[cmbResCode.SelectedIndex].ToString(), 2))));
				}
				if (CheckForSave())
				{
					intCurCode = intTypeCode;
					ShowType(intCurCode, lngResCode);
				}
			}
			// If cmbCode.ComboIndex <> -1 Then
			// If CheckForSave Then
			// If Not boolCancel Then
			// If Val(cmbCode.ComboItem(cmbCode.ComboIndex)) = Val(cmbCode.Text) Then  'is the text showing in the combo box the same as the current type number
			// ShowType Val(cmbCode.ComboItem(cmbCode.ComboIndex))
			// boolDirty = False
			// intTypeCode = Val(cmbCode.ComboItem(cmbCode.ComboIndex))
			// intCurCode = intTypeCode
			// Else
			// boolDirty = False
			// intTypeCode = Val(cmbCode.Text)
			// intCurCode = intTypeCode
			// End If
			// 
			// If RestrictedCode Then
			// boolLockType = True
			// Else
			// boolLockType = False
			// End If
			// End If
			// End If
			// End If
		}

		private void LockBox_8(bool boolLock, bool boolBMV = false)
		{
			LockBox(ref boolLock, boolBMV);
		}

		private void LockBox(ref bool boolLock, bool boolBMV = false)
		{
			// this will disable the boxes that need to be when a restricted code is displayed
			chkBMV.Enabled = !boolLock;
			// chkPrint.Enabled = Not boolLock
			chkRB.Enabled = !boolLock;
			txtTitle.Enabled = !boolLock;
			txtRefTitle.Enabled = !boolLock;
			txtCTRL1Title.Enabled = !boolLock;
			txtCTRL2Title.Enabled = !boolLock;
			txtCTRL3Title.Enabled = !boolLock;
			chkEFT.Enabled = !boolLock;
			if (boolBMV)
			{
				// this will set the check boxes to disabled if the lock is on and boolBMV is false
				chkMandatory[0].Enabled = false;
				chkMandatory[1].Enabled = false;
				chkMandatory[2].Enabled = false;
				chkMandatory[3].Enabled = false;
			}
			else
			{
				chkMandatory[0].Enabled = !boolLock;
				chkMandatory[1].Enabled = !boolLock;
				chkMandatory[2].Enabled = !boolLock;
				chkMandatory[3].Enabled = !boolLock;
			}
		}

		private bool CheckPercentages()
		{
			bool CheckPercentages = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will return true if the percentage amounts add up to 100%
				// and return false otherwise
				// Make sure that all the values are in the grid rather than in the .edittext
				double dblPercent;
				int intRow;
				dblPercent = 0;
				for (intRow = 1; intRow <= 6; intRow++)
				{
					if (Conversion.Val(vsReceipt.TextMatrix(intRow, PercentCol)) == -1)
					{
						// if it is checked
						dblPercent += FCConvert.ToDouble(vsReceipt.TextMatrix(intRow, DefaultCol));
					}
					else
					{
						// next
					}
				}
				if (FCUtils.Round(dblPercent, 4) == 100 || CheckPercentages == false)
				{
					CheckPercentages = true;
				}
				else
				{
					CheckPercentages = false;
				}
				return CheckPercentages;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Checking Percentages", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CheckPercentages;
		}

		private void vsReceipt_StartEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			// If (intCurCode = 90 Or intCurCode = 91 Or intCurCode = 890 Or intCurCode = 891) And Col = 4 And Row = 1 Then
			if (vsReceipt.Col == YearCol || vsReceipt.Col == PercentCol)
			{
				e.Cancel = true;
			}
			// FC:FINAL:VGE - #873 Disabling cell validation for A/R case
			if (intCurCode == 97 && modGlobalConstants.Statics.gboolAR && e.RowIndex == 0)
			{
				e.Cancel = true;
			}
		}

		private void vsReceipt_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vsReceipt.GetFlexRowIndex(e.RowIndex);
			int col = vsReceipt.GetFlexColIndex(e.ColumnIndex);
			// this will make sure the format is correct
			if (col == AccountCol)
			{
				if (row == 1)
				{
					if (modGlobalConstants.Statics.gboolBD)
					{
						e.Cancel = !CheckStandardAccountType(vsReceipt.EditText, boolSaving);
						if (intCurCode == 90 || intCurCode == 91 || intCurCode == 890 || intCurCode == 891)
						{
							vsReceipt.TextMatrix(row, YearCol, FCConvert.ToString(true));
							// set the year flag
						}
					}
					else
					{
						if (FCConvert.CBool(Strings.Left(vsReceipt.EditText, 1) != "M"))
						{
							vsReceipt.EditText = "";
						}
					}
				}
			}
			else if (col == DefaultCol)
			{
				// This will check to see if the percentage col is checked
				// if so then format this to a long so the 100% is easier to
				// match instead of with decimals and rounding will be less of an issue
				if (Conversion.Val(vsReceipt.TextMatrix(row, PercentCol)) == -1)
				{
					vsReceipt.EditText = Strings.Format(vsReceipt.EditText, "#0.00");
				}
				else
				{
					vsReceipt.EditText = Strings.Format(vsReceipt.EditText, "#,##0.00");
				}
			}
		}

		private void ColorGrid_2(bool boolRestricted)
		{
			ColorGrid(ref boolRestricted);
		}

		private void ColorGrid(ref bool boolRestricted)
		{
			if (boolRestricted)
			{
				// color the grid with grayed out columns
				vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 1, 6, AbbrevCol, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 1, 1, 6, AbbrevCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT);
			}
			else
			{
				// set the grid back to white
				vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 1, 6, AbbrevCol, Color.White);
				vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 1, 1, 6, AbbrevCol, modGlobalConstants.Statics.TRIOCOLORBLACK);
			}
		}

		private string RemoveUnderscore(string strAcct)
		{
			string RemoveUnderscore = "";
			// this function will remove the underscores from any M account and will
			// return "" if there is any underscores for all other accounts
			if (Strings.Trim(strAcct) != "")
			{
				if (Strings.Left(strAcct, 1) != "M")
				{
					if (Strings.InStr(1, strAcct, "_", CompareConstants.vbBinaryCompare) != 0)
					{
						RemoveUnderscore = "";
					}
					else
					{
						RemoveUnderscore = strAcct;
					}
				}
				else
				{
					RemoveUnderscore = strAcct.Replace("_", "");
				}
			}
			else
			{
				RemoveUnderscore = "";
			}
			return RemoveUnderscore;
		}

		private void FillResCodeCombo()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsRC = new clsDRWrapper();
				// cmbResCode.AddItem "00 - " & MuniName
				cmbResCode.Clear();
				cmbNewResCode.Clear();
				rsRC.OpenRecordset("SELECT * FROM tblRegions WHERE TownNumber > 0 ORDER BY TownNumber", "CentralData");
				while (!rsRC.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [TownNumber] and replace with corresponding Get_Field method
					cmbResCode.AddItem(Strings.Format(rsRC.Get_Fields("TownNumber"), "00") + " - " + rsRC.Get_Fields_String("TownName"));
					// TODO Get_Fields: Check the table for the column [TownNumber] and replace with corresponding Get_Field method
					cmbResCode.ItemData(cmbResCode.NewIndex, FCConvert.ToInt32(rsRC.Get_Fields("TownNumber")));
					// TODO Get_Fields: Check the table for the column [TownNumber] and replace with corresponding Get_Field method
					if (FCConvert.ToInt32(rsRC.Get_Fields("TownNumber")) != 0)
					{
						// do not show the main town when asking for a new rescode
						// TODO Get_Fields: Check the table for the column [TownNumber] and replace with corresponding Get_Field method
						cmbNewResCode.AddItem(Strings.Format(rsRC.Get_Fields("TownNumber"), "00") + " - " + rsRC.Get_Fields_String("TownName"));
						// TODO Get_Fields: Check the table for the column [TownNumber] and replace with corresponding Get_Field method
						cmbNewResCode.ItemData(cmbNewResCode.NewIndex, FCConvert.ToInt32(rsRC.Get_Fields("TownNumber")));
					}
					rsRC.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Towns", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillEPmtAccountCombo()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsStore = new clsDRWrapper();
				cboAltEPmtAccount.Clear();
				rsStore.OpenRecordset("SELECT AcctID,AcctDescription FROM EPmtAccounts", modExtraModules.strCRDatabase);
				while (!rsStore.EndOfFile())
				{
					cboAltEPmtAccount.AddItem(rsStore.Get_Fields_String("AcctID"));
					rsStore.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Service Codes", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool CheckStandardAccountType_2(string strAccount, bool boolSave = false)
		{
			return CheckStandardAccountType(strAccount, boolSave);
		}

		private bool CheckStandardAccountType(string strAccount, bool boolSave = false)
		{
			bool CheckStandardAccountType = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intTypeCode = 0;
				int lngResCode;
				string strLedgerPiece = "";
				string strSALedgerPiece = "";
				clsDRWrapper rsBD = new clsDRWrapper();
				if (modGlobalConstants.Statics.gboolBD && !boolSave)
				{
					if (Strings.Left(strAccount, 1) == "G")
					{
						if (cmbType.SelectedIndex != -1)
						{
							intTypeCode = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbType.Items[cmbType.SelectedIndex].ToString(), 3))));
							if (modGlobal.Statics.gboolMultipleTowns)
							{
								lngResCode = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbResCode.Items[cmbResCode.SelectedIndex].ToString(), 2))));
							}
						}
						rsBD.OpenRecordset("SELECT * FROM StandardAccounts", modExtraModules.strBDDatabase);
						if (rsBD.EndOfFile() != true && rsBD.BeginningOfFile() != true)
						{
							switch (intTypeCode)
							{
								case 90:
									{
										// RE Payment
										rsBD.FindFirstRecord("Code", "RR");
										break;
									}
								case 91:
									{
										// Lien Payment
										rsBD.FindFirstRecord("Code", "LR");
										break;
									}
								case 92:
									{
										// PP Payment
										rsBD.FindFirstRecord("Code", "PR");
										break;
									}
								case 93:
									{
										// Water
										rsBD.FindFirstRecord("Code", "WR");
										break;
									}
								case 94:
									{
										// Sewer
										rsBD.FindFirstRecord("Code", "SR");
										break;
									}
								case 95:
									{
										rsBD.FindFirstRecord("Code", "SL");
										break;
									}
								case 96:
									{
										rsBD.FindFirstRecord("Code", "WL");
										// MAL@20081113: Do not validate the Principal Account for A/R (if AR is active)
										// Tracker Reference: 14057
										break;
									}
								case 97:
									{
										if (modGlobalConstants.Statics.gboolAR)
										{
											CheckStandardAccountType = true;
											return CheckStandardAccountType;
										}
										else
										{
											rsBD.FindFirstRecord("Code", "AR");
										}
										break;
									}
								case 890:
									{
										rsBD.FindFirstRecord("Code", "TR");
										break;
									}
								case 891:
									{
										rsBD.FindFirstRecord("Code", "LV");
										break;
									}
								default:
									{
										CheckStandardAccountType = true;
										return CheckStandardAccountType;
									}
							}
							//end switch
							if (!rsBD.NoMatch)
							{
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								if (FCConvert.ToString(rsBD.Get_Fields("Account")) != "")
								{
									// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
									strSALedgerPiece = FCConvert.ToString(rsBD.Get_Fields("Account"));
								}
								else
								{
									strSALedgerPiece = "";
									MessageBox.Show("Error in Standard Accounts.  Please make sure that the Standard Account that you were looking for is setup in Budgetary.", "Standard Accounts Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								}
							}
							else
							{
								strSALedgerPiece = "";
								MessageBox.Show("Error in Standard Accounts.  Please make sure that the Standard Account that you were looking for is setup in Budgetary.", "Standard Accounts Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
							strLedgerPiece = GetLedgerAcctPiece(ref strAccount);
							if (strSALedgerPiece == strLedgerPiece)
							{
								CheckStandardAccountType = true;
							}
							else
							{
								MessageBox.Show("Please use the General Ledger Standard Account (" + strSALedgerPiece + ") for this field.", "Standard Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								CheckStandardAccountType = false;
							}
						}
						else
						{
							MessageBox.Show("Error in Standard Accounts.  Please make sure that the Standard Account that you were looking for is setup in Budgetary.", "Standard Accounts Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							CheckStandardAccountType = false;
						}
					}
					else if (Strings.Left(strAccount, 1) != "M")
					{
						if (intCurCode >= 90 && intCurCode <= 96)
						{
							MessageBox.Show("Please use a General Ledger account for this field.", "Standard Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							CheckStandardAccountType = false;
						}
						else
						{
							CheckStandardAccountType = true;
						}
					}
					else
					{
						CheckStandardAccountType = true;
					}
				}
				else
				{
					CheckStandardAccountType = true;
				}
				return CheckStandardAccountType;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Checking Standard Accounts", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CheckStandardAccountType;
		}

		public string GetLedgerAcctPiece(ref string Acct)
		{
			string GetLedgerAcctPiece = "";
			if (Strings.Left(Acct, 1) == "G")
			{
				GetLedgerAcctPiece = Strings.Mid(Acct, 4 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))));
			}
			else
			{
				GetLedgerAcctPiece = "";
			}
			return GetLedgerAcctPiece;
		}

		private void cmdSaveAdvance_Click(object sender, EventArgs e)
		{
			mnuProcessSaveExit_Click(sender, e);
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuProcessSave_Click();
		}

		private void cmdPrevious_Click(object sender, EventArgs e)
		{
			mnuFileBack_Click(sender, e);
		}

		private void cmdNext_Click(object sender, EventArgs e)
		{
			mnuFileForward_Click(sender, e);
		}

		private void cmdDelete_Click(object sender, EventArgs e)
		{
			mnuProcessDelete_Click(sender, e);
		}

		private void cmdNew_Click(object sender, EventArgs e)
		{
			mnuProcessNew_Click(sender, e);
		}

		private void cmdReactivate_Click(object sender, EventArgs e)
		{
			mnuProcessReactivate_Click(sender, e);
		}

		private void cmdMoses_Click(object sender, EventArgs e)
		{
			mnuFileMOSESSetup_Click(sender, e);
		}
	}
}
