﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using SharedApplication.CashReceipts.Models;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for sarRSReceiptDetail.
	/// </summary>
	public partial class sarRSReceiptDetail : FCSectionReport
	{
		private ReceiptSearchReportResult reportData;
		private int checksCount = 0;

		public sarRSReceiptDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			reportData = (ReceiptSearchReportResult)UserData;
			
			lblChange.Text = (reportData.MultipleLineItemsOnReceipt ? "*" : "") + "Change:";
			lblCredit.Text = (reportData.MultipleLineItemsOnReceipt ? "*" : "") + "Credit/Debit:";
			lblCash.Text = (reportData.MultipleLineItemsOnReceipt ? "*" : "") + "Cash:";
			lblCheck.Text = (reportData.MultipleLineItemsOnReceipt ? "*" : "") + "Check:";

			if (reportData.CheckAmount != 0)
			{
				lblCheckHeader.Visible = true;
				CreateCheckTable();
			}
			else
			{
				lblCheckHeader.Visible = false;
			}

		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
			SetupFields();
		}

		private void BindFields()
		{
			fldChange.Text = reportData.Change.ToString("#,##0.00");
			fldCash.Text = reportData.CashAmount.ToString("#,##0.00");
			fldCheck.Text = reportData.CheckAmount.ToString("#,##0.00");
			fldCredit.Text = reportData.CardAmount.ToString("#,##0.00");
		}

		private void SetupFields()
		{
			// this sub will not show the space of a blank line
			int intCT;
			intCT = 1;
			if (Conversion.Val(fldCredit.Text) == 0)
			{
				// if this is empty then move the one below it up one
				fldCredit.Text = fldChange.Text;
				lblCredit.Text = lblChange.Text;
				fldChange.Text = "0.00";
			}
			if (Conversion.Val(fldCheck.Text) == 0)
			{
				// if this is empty then move it up two below it up one
				fldCheck.Text = fldCredit.Text;
				lblCheck.Text = lblCredit.Text;
				fldCredit.Text = fldChange.Text;
				lblCredit.Text = lblChange.Text;
			}
			if (Conversion.Val(fldCash.Text) == 0)
			{
				// if this is empty then move it up three below it up one
				fldCash.Text = fldCheck.Text;
				lblCash.Text = lblCheck.Text;
				fldCheck.Text = fldCredit.Text;
				lblCheck.Text = lblCredit.Text;
				fldCredit.Text = fldChange.Text;
				lblCredit.Text = lblChange.Text;
			}
			if (Conversion.Val(fldCash.Text) == 0)
			{
				fldCash.Visible = false;
				lblCash.Visible = false;
			}
			else
			{
				intCT = 1;
				fldCash.Visible = true;
				lblCash.Visible = true;
			}
			if (Conversion.Val(fldCheck.Text) == 0)
			{
				fldCheck.Visible = false;
				lblCheck.Visible = false;
			}
			else
			{
				intCT = 2;
				fldCheck.Visible = true;
				lblCheck.Visible = true;
			}
			if (Conversion.Val(fldCredit.Text) == 0)
			{
				fldCredit.Visible = false;
				lblCredit.Visible = false;
			}
			else
			{
				intCT = 3;
				fldCredit.Visible = true;
				lblCredit.Visible = true;
			}
			if (Conversion.Val(fldChange.Text) == 0)
			{
				fldChange.Visible = false;
				lblChange.Visible = false;
			}
			else
			{
				intCT = 4;
				fldChange.Visible = true;
				lblChange.Visible = true;
			}

			if ((checksCount * lblCHK1.Height) + (300 / 1440F) < (fldChange.Height * intCT) + (270 / 1440F))
			{
				Detail.Height = fldChange.Height * intCT + 270 / 1440F;
			}
			else
			{
				Detail.Height = (checksCount * lblCHK1.Height) + (300 / 1440F);
			}
		}

		private void CreateCheckTable()
		{
			try
			{
				checksCount = 0;
				foreach (var check in reportData.Checks)
				{
					if (check.CheckNumber != "")
					{
						checksCount++;
						AddCheckLine(check.CheckNumber, check.Amount, checksCount);
					}
				}
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Check Table", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void AddCheckLine(string strCheckNumber, decimal dblAmount, int lngRNum)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				GrapeCity.ActiveReports.SectionReportModel.Label obNew1;
				GrapeCity.ActiveReports.SectionReportModel.Label obNew2;
				string strTemp = "";
				// this will add another per diem line in the report footer
				if (lngRNum == 1)
				{
					lblCHKAmt1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
					lblCHKAmt1.Text = Strings.Format(dblAmount, "#,##0.00");
					lblCHK1.Text = strCheckNumber;
					lblCHK1.Top = 200 / 1440F;
					lblCHKAmt1.Top = 200 / 1440F;
					lblCHK1.Visible = true;
					lblCHKAmt1.Visible = true;
				}
				else
				{
					// increment the number of fields
					// add a field
					obNew1 = this.Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>("lblCHKAmt" + FCConvert.ToString(lngRNum));
					//obNew1.Name = "lblCHKAmt" + FCConvert.ToString(lngRNum);
					obNew1.Top = lblCHKAmt1.Top + ((lngRNum - 1) * lblCHKAmt1.Height);
					obNew1.Left = lblCHKAmt1.Left;
					obNew1.Width = lblCHKAmt1.Width;
					obNew1.Height = lblCHKAmt1.Height;
					obNew1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
					//strTemp = obNew1.Font;
					obNew1.Font = lblCHKAmt1.Font;
					// this sets the font to the same as the field that is already created
					obNew1.Visible = true;
					obNew1.Text = Strings.Format(dblAmount, "#,##0.00");
					// add a label
					obNew2 = this.Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>("lblCHK" + FCConvert.ToString(lngRNum));
					//obNew2.Name = "lblCHK" + FCConvert.ToString(lngRNum);
					obNew2.Top = lblCHK1.Top + ((lngRNum - 1) * lblCHK1.Height);
					obNew2.Left = lblCHK1.Left;
					obNew2.Width = lblCHK1.Width;
					obNew2.Height = lblCHK1.Height;
					//strTemp = obNew2.Font;
					obNew2.Font = lblCHKAmt1.Font;
					// this sets the font to the same as the field that is already created
					obNew2.Visible = true;
					obNew2.Text = strCheckNumber;
				}
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Check Entry", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
	}
}
