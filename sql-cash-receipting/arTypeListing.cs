﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for arTypeListing.
	/// </summary>
	public partial class arTypeListing : BaseSectionReport
	{
		public static arTypeListing InstancePtr
		{
			get
			{
				return (arTypeListing)Sys.GetInstance(typeof(arTypeListing));
			}
		}

		protected arTypeListing _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            rsData.DisposeOf();
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	arTypeListing	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               10/01/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/22/2005              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		bool bool2Across;

		public arTypeListing()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Cash Receipt Type Listing";
            this.ReportEnd += ArTypeListing_ReportEnd;
		}

        private void ArTypeListing_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_Initialize()
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			bool2Across = (frmReceiptTypeListing.InstancePtr.chk2Across.CheckState == Wisej.Web.CheckState.Checked);
		}
		//private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		//{
		//	switch (KeyCode)
		//	{
		//		case Keys.Escape:
		//		{
		//			Close();
		//			break;
		//		}
		//	} //end switch
		//}
		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQL = "";
			string strOrderBy = "";
			//FC:FINAL:AM:#i508 - call here Initialize
			ActiveReport_Initialize();
			// MAL@20080123
			if (frmReceiptTypeListing.InstancePtr.blnSortAlpha)
			{
				strOrderBy = "ORDER BY TypeTitle";
			}
			else
			{
				strOrderBy = "ORDER BY TypeCode";
			}
			//this.Zoom = -1;
			if (bool2Across)
			{
				Detail.ColumnCount = 2;
				Detail.ColumnDirection = GrapeCity.ActiveReports.SectionReportModel.ColumnDirection.DownAcross;
				Detail.Height = 300 / 1440f;
			}
			MoveLabels();
			// this will move the labels depending on the options chosen
			Detail.CanShrink = true;
			// this lets the detail section shrink when I move/hide some of the fields
			if (modGlobal.Statics.gboolMultipleTowns)
			{
				strSQL = "SELECT * FROM Type WHERE TypeCode >= " + FCConvert.ToString(frmReceiptTypeListing.InstancePtr.StartNumber) + " AND TypeCode <= " + FCConvert.ToString(frmReceiptTypeListing.InstancePtr.EndNumber) + " AND isnull(TypeTitle, 'NULL') <> 'NULL' AND TypeTitle <> '' AND TownKey <> 0 " + strOrderBy;
			}
			else
			{
				strSQL = "SELECT * FROM Type WHERE TypeCode >= " + FCConvert.ToString(frmReceiptTypeListing.InstancePtr.StartNumber) + " AND TypeCode <= " + FCConvert.ToString(frmReceiptTypeListing.InstancePtr.EndNumber) + " AND isnull(TypeTitle, 'NULL') <> 'NULL' AND TypeTitle <> '' " + strOrderBy;
			}
			// set the data control
            if (rsData == null) rsData = new clsDRWrapper();
			rsData.OpenRecordset(strSQL, modExtraModules.strCRDatabase);
			// set the date
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
		}

		private void BindFields()
		{
			// this will bring the fields on the form to the fields in the recordset
			// MAL@20071010: Added indicator for Deleted
			if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("Deleted")))
			{
				fldCode.Text = FCConvert.ToString(rsData.Get_Fields_Int32("TypeCode")) + "*";
			}
			else
			{
				fldCode.Text = FCConvert.ToString(rsData.Get_Fields_Int32("TypeCode"));
			}
			fldType.Text = rsData.Get_Fields_String("TypeTitle");
			fldAccount1.Text = rsData.Get_Fields_String("Account1");
			fldAccount2.Text = rsData.Get_Fields_String("Account2");
			fldAccount3.Text = rsData.Get_Fields_String("Account3");
			fldAccount4.Text = rsData.Get_Fields_String("Account4");
			fldAccount5.Text = rsData.Get_Fields_String("Account5");
			fldAccount6.Text = rsData.Get_Fields_String("Account6");
			fldTitle1.Text = rsData.Get_Fields_String("Title1");
			fldTitle2.Text = rsData.Get_Fields_String("Title2");
			fldTitle3.Text = rsData.Get_Fields_String("Title3");
			fldTitle4.Text = rsData.Get_Fields_String("Title4");
			fldTitle5.Text = rsData.Get_Fields_String("Title5");
			fldTitle6.Text = rsData.Get_Fields_String("Title6");
			if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("DefaultAccount"))) != "")
			{
				if (Strings.InStr(1, "_", FCConvert.ToString(rsData.Get_Fields_String("DefaultAccount")), CompareConstants.vbBinaryCompare) <= 0)
				{
					fldAltCash.Text = rsData.Get_Fields_String("DefaultAccount");
					lblAltCash.Text = "Alt Cash Acct:";
				}
				else
				{
					lblAltCash.Text = "";
					fldAltCash.Text = "";
				}
			}
			else
			{
				lblAltCash.Text = "";
				fldAltCash.Text = "";
			}
			lblScreenTitle1.Text = rsData.Get_Fields_String("Title1Abbrev");
			lblScreenTitle2.Text = rsData.Get_Fields_String("Title2Abbrev");
			lblScreenTitle3.Text = rsData.Get_Fields_String("Title3Abbrev");
			lblScreenTitle4.Text = rsData.Get_Fields_String("Title4Abbrev");
			lblScreenTitle5.Text = rsData.Get_Fields_String("Title5Abbrev");
			lblScreenTitle6.Text = rsData.Get_Fields_String("Title6Abbrev");
			fldDefault1.Text = Strings.Format(rsData.Get_Fields_Double("DefaultAmount1"), "#,##0.00");
			fldDefault2.Text = Strings.Format(rsData.Get_Fields_Double("DefaultAmount2"), "#,##0.00");
			fldDefault3.Text = Strings.Format(rsData.Get_Fields_Double("DefaultAmount3"), "#,##0.00");
			fldDefault4.Text = Strings.Format(rsData.Get_Fields_Double("DefaultAmount4"), "#,##0.00");
			fldDefault5.Text = Strings.Format(rsData.Get_Fields_Double("DefaultAmount5"), "#,##0.00");
			fldDefault6.Text = Strings.Format(rsData.Get_Fields_Double("DefaultAmount6"), "#,##0.00");
			if (rsData.Get_Fields_Boolean("Year1") == true)
			{
				fldYear1.Text = "Y";
			}
			else
			{
				fldYear1.Text = "N";
			}
			if (rsData.Get_Fields_Boolean("Year2") == true)
			{
				fldYear2.Text = "Y";
			}
			else
			{
				fldYear2.Text = "N";
			}
			if (rsData.Get_Fields_Boolean("Year3") == true)
			{
				fldYear3.Text = "Y";
			}
			else
			{
				fldYear3.Text = "N";
			}
			if (rsData.Get_Fields_Boolean("Year4") == true)
			{
				fldYear4.Text = "Y";
			}
			else
			{
				fldYear4.Text = "N";
			}
			if (rsData.Get_Fields_Boolean("Year5") == true)
			{
				fldYear5.Text = "Y";
			}
			else
			{
				fldYear5.Text = "N";
			}
			if (rsData.Get_Fields_Boolean("Year6") == true)
			{
				fldYear6.Text = "Y";
			}
			else
			{
				fldYear6.Text = "N";
			}
			if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("BMV")))
			{
				fldBMV.Text = "Yes";
			}
			else
			{
				fldBMV.Text = "No";
			}
			fldRefDesc.Text = rsData.Get_Fields_String("Reference");
			if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("ReferenceMandatory")))
			{
				fldRefDesc.Text = fldRefDesc.Text;
				fldReq1.Text = "(R)";
			}
			else
			{
				fldReq1.Text = "";
			}
			fldControl1Desc.Text = rsData.Get_Fields_String("Control1");
			if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("Control1Mandatory")))
			{
				fldControl1Desc.Text = fldControl1Desc.Text;
				fldReq2.Text = "(R)";
			}
			else
			{
				fldReq2.Text = "";
			}
			fldControl2Desc.Text = rsData.Get_Fields_String("Control2");
			if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("Control2Mandatory")))
			{
				fldControl2Desc.Text = fldControl2Desc.Text;
				fldReq3.Text = "(R)";
			}
			else
			{
				fldReq3.Text = "";
			}
			fldControl3Desc.Text = rsData.Get_Fields_String("Control3");
			if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("Control3Mandatory")))
			{
				fldControl3Desc.Text = fldControl3Desc.Text;
				fldReq4.Text = "(R)";
			}
			else
			{
				fldReq4.Text = "";
			}
			// TODO Get_Fields: Check the table for the column [Copies] and replace with corresponding Get_Field method
			fldCopies.Text = FCConvert.ToString(rsData.Get_Fields("Copies"));
			if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("Print")))
			{
				fldPrint.Text = "Yes";
			}
			else
			{
				fldPrint.Text = "No";
			}
		}

		private void MoveLabels()
		{
			if (bool2Across)
			{
				// titles
				lblAccount.Visible = false;
				lblDefault.Visible = false;
				lblTitle.Visible = false;
				lblYear.Visible = false;
				lblCode2.Visible = true;
				lblType2.Visible = true;
				lblAccount.Top = 0;
				lblDefault.Top = 0;
				lblTitle.Top = 0;
				lblYear.Top = 0;
				lblAccount.Left = 0;
				lblDefault.Left = 0;
				lblTitle.Left = 0;
				lblYear.Left = 0;
				// fields
				fldAccount1.Visible = false;
				fldAccount2.Visible = false;
				fldAccount3.Visible = false;
				fldAccount4.Visible = false;
				fldAccount5.Visible = false;
				fldAccount6.Visible = false;
				fldYear1.Visible = false;
				fldYear2.Visible = false;
				fldYear3.Visible = false;
				fldYear4.Visible = false;
				fldYear5.Visible = false;
				fldYear6.Visible = false;
				fldTitle1.Visible = false;
				fldTitle2.Visible = false;
				fldTitle3.Visible = false;
				fldTitle4.Visible = false;
				fldTitle5.Visible = false;
				fldTitle6.Visible = false;
				fldReq1.Visible = false;
				fldReq2.Visible = false;
				fldReq3.Visible = false;
				fldReq4.Visible = false;
				lblScreenTitle1.Visible = false;
				lblScreenTitle2.Visible = false;
				lblScreenTitle3.Visible = false;
				lblScreenTitle4.Visible = false;
				lblScreenTitle5.Visible = false;
				lblScreenTitle6.Visible = false;
				fldDefault1.Visible = false;
				fldDefault2.Visible = false;
				fldDefault3.Visible = false;
				fldDefault4.Visible = false;
				fldDefault5.Visible = false;
				fldDefault6.Visible = false;
				fldBMV.Visible = false;
				lblBMV.Visible = false;
				fldAltCash.Visible = false;
				lblAltCash.Visible = false;
				fldRefDesc.Visible = false;
				fldControl1Desc.Visible = false;
				fldControl2Desc.Visible = false;
				fldControl3Desc.Visible = false;
				fldPrint.Visible = false;
				fldCopies.Visible = false;
				fldAccount1.Left = 0;
				fldAccount2.Left = 0;
				fldAccount3.Left = 0;
				fldAccount4.Left = 0;
				fldAccount5.Left = 0;
				fldAccount6.Left = 0;
				fldYear1.Left = 0;
				fldYear2.Left = 0;
				fldYear3.Left = 0;
				fldYear4.Left = 0;
				fldYear5.Left = 0;
				fldYear6.Left = 0;
				fldTitle1.Left = 0;
				fldTitle2.Left = 0;
				fldTitle3.Left = 0;
				fldTitle4.Left = 0;
				fldTitle5.Left = 0;
				fldTitle6.Left = 0;
				fldDefault1.Left = 0;
				fldDefault2.Left = 0;
				fldDefault3.Left = 0;
				fldDefault4.Left = 0;
				fldDefault5.Left = 0;
				fldDefault6.Left = 0;
				fldBMV.Left = 0;
				lblBMV.Left = 0;
				fldAltCash.Left = 0;
				lblAltCash.Left = 0;
				fldRefDesc.Left = 0;
				fldControl1Desc.Left = 0;
				fldControl2Desc.Left = 0;
				fldControl3Desc.Left = 0;
				fldReq1.Left = 0;
				fldReq2.Left = 0;
				fldReq3.Left = 0;
				fldReq4.Left = 0;
				fldPrint.Left = 0;
				fldCopies.Left = 0;
				fldAccount1.Top = 0;
				fldAccount2.Top = 0;
				fldAccount3.Top = 0;
				fldAccount4.Top = 0;
				fldAccount5.Top = 0;
				fldAccount6.Top = 0;
				fldYear1.Top = 0;
				fldYear2.Top = 0;
				fldYear3.Top = 0;
				fldYear4.Top = 0;
				fldYear5.Top = 0;
				fldYear6.Top = 0;
				fldTitle1.Top = 0;
				fldTitle2.Top = 0;
				fldTitle3.Top = 0;
				fldTitle4.Top = 0;
				fldTitle5.Top = 0;
				fldTitle6.Top = 0;
				fldDefault1.Top = 0;
				fldDefault2.Top = 0;
				fldDefault3.Top = 0;
				fldDefault4.Top = 0;
				fldDefault5.Top = 0;
				fldDefault6.Top = 0;
				fldBMV.Top = 0;
				lblBMV.Top = 0;
				fldRefDesc.Top = 0;
				fldControl1Desc.Top = 0;
				fldControl2Desc.Top = 0;
				fldControl3Desc.Top = 0;
				fldReq1.Top = 0;
				fldReq2.Top = 0;
				fldReq3.Top = 0;
				fldReq4.Top = 0;
				fldPrint.Top = 0;
				fldCopies.Top = 0;
				// hide lines and labels
				lnBottom.Visible = false;
				lnBottom.Y1 = 0;
				lnBottom.Y2 = 0;
				lnDefaultTitles.Visible = false;
				lnDefaultTitles.Y1 = 0;
				lnDefaultTitles.Y2 = 0;
				lblPrint.Visible = false;
				lblPrint.Top = 0;
				lblCopies.Visible = false;
				lblCopies.Top = 0;
				lblHeaderTitles.Visible = false;
				lblHeaderTitles.Top = 0;
			}
			else
			{
				lblAccount.Visible = true;
				lblDefault.Visible = true;
				lblTitle.Visible = true;
				lblYear.Visible = true;
				lblCode2.Visible = true;
				lblType2.Visible = false;
				lblTitle.Left = fldTitle1.Left;
				lblCode2.Left = lblScreenTitle1.Left;
				lblCode2.Width = lblDefault.Left - lblCode2.Left + 200 / 1440f;
				lblCode2.Text = "Screen Title";
				fldAccount1.Visible = true;
				fldAccount2.Visible = true;
				fldAccount3.Visible = true;
				fldAccount4.Visible = true;
				fldAccount5.Visible = true;
				fldAccount6.Visible = true;
				fldYear1.Visible = true;
				fldYear2.Visible = true;
				fldYear3.Visible = true;
				fldYear4.Visible = true;
				fldYear5.Visible = true;
				fldYear6.Visible = true;
				fldTitle1.Visible = true;
				fldTitle2.Visible = true;
				fldTitle3.Visible = true;
				fldTitle4.Visible = true;
				fldTitle5.Visible = true;
				fldTitle6.Visible = true;
				lblScreenTitle1.Visible = true;
				lblScreenTitle2.Visible = true;
				lblScreenTitle3.Visible = true;
				lblScreenTitle4.Visible = true;
				lblScreenTitle5.Visible = true;
				lblScreenTitle6.Visible = true;
				fldDefault1.Visible = true;
				fldDefault2.Visible = true;
				fldDefault3.Visible = true;
				fldDefault4.Visible = true;
				fldDefault5.Visible = true;
				fldDefault6.Visible = true;
				fldBMV.Visible = true;
				lblBMV.Visible = true;
				fldAltCash.Visible = true;
				lblAltCash.Visible = true;
				fldRefDesc.Visible = true;
				fldControl1Desc.Visible = true;
				fldControl2Desc.Visible = true;
				fldControl3Desc.Visible = true;
				fldReq1.Visible = true;
				fldReq2.Visible = true;
				fldReq3.Visible = true;
				fldReq4.Visible = true;
				fldPrint.Visible = true;
				fldCopies.Visible = true;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (rsData.EndOfFile() != true)
			{
				BindFields();
				if (!bool2Across)
				{
					if (Strings.Trim(rsData.Get_Fields_String("Title1") + " ") != "")
					{
						fldAccount1.Visible = true;
						fldTitle1.Visible = true;
						fldDefault1.Visible = true;
						fldYear1.Visible = true;
						lblScreenTitle1.Visible = true;
					}
					else
					{
						fldAccount1.Visible = false;
						fldTitle1.Visible = false;
						fldDefault1.Visible = false;
						fldYear1.Visible = false;
						lblScreenTitle1.Visible = false;
					}
					if (Strings.Trim(rsData.Get_Fields_String("Title2") + " ") != "")
					{
						fldAccount2.Visible = true;
						fldTitle2.Visible = true;
						fldDefault2.Visible = true;
						fldYear2.Visible = true;
						lblScreenTitle2.Visible = true;
					}
					else
					{
						fldAccount2.Visible = false;
						fldTitle2.Visible = false;
						fldDefault2.Visible = false;
						fldYear2.Visible = false;
						lblScreenTitle2.Visible = false;
					}
					if (Strings.Trim(rsData.Get_Fields_String("Title3") + " ") != "")
					{
						fldAccount3.Visible = true;
						fldTitle3.Visible = true;
						fldDefault3.Visible = true;
						fldYear3.Visible = true;
						lblScreenTitle3.Visible = true;
					}
					else
					{
						fldAccount3.Visible = false;
						fldTitle3.Visible = false;
						fldDefault3.Visible = false;
						fldYear3.Visible = false;
						lblScreenTitle3.Visible = false;
					}
					if (Strings.Trim(rsData.Get_Fields_String("Title4") + " ") != "")
					{
						fldAccount4.Visible = true;
						fldTitle4.Visible = true;
						fldDefault4.Visible = true;
						fldYear4.Visible = true;
						lblScreenTitle4.Visible = true;
					}
					else
					{
						fldAccount4.Visible = false;
						fldTitle4.Visible = false;
						fldDefault4.Visible = false;
						fldYear4.Visible = false;
						lblScreenTitle4.Visible = false;
					}
					if (Strings.Trim(rsData.Get_Fields_String("Title5") + " ") != "")
					{
						fldAccount5.Visible = true;
						fldTitle5.Visible = true;
						fldDefault5.Visible = true;
						fldYear5.Visible = true;
						lblScreenTitle5.Visible = true;
					}
					else
					{
						fldAccount5.Visible = false;
						fldTitle5.Visible = false;
						fldDefault5.Visible = false;
						fldYear5.Visible = false;
						lblScreenTitle5.Visible = false;
					}
					if (Strings.Trim(rsData.Get_Fields_String("Title6") + " ") != "")
					{
						fldAccount6.Visible = true;
						fldTitle6.Visible = true;
						fldDefault6.Visible = true;
						fldYear6.Visible = true;
						lblScreenTitle6.Visible = true;
					}
					else
					{
						fldAccount6.Visible = false;
						fldTitle6.Visible = false;
						fldDefault6.Visible = false;
						fldYear6.Visible = false;
						lblScreenTitle6.Visible = false;
					}
					if (Strings.Left(fldAccount1.Text, 1) != "G")
					{
						fldYear1.Visible = false;
					}
					else
					{
						fldYear1.Visible = true;
					}
					if (Strings.Left(fldAccount2.Text, 1) != "G")
					{
						fldYear2.Visible = false;
					}
					else
					{
						fldYear2.Visible = true;
					}
					if (Strings.Left(fldAccount3.Text, 1) != "G")
					{
						fldYear3.Visible = false;
					}
					else
					{
						fldYear3.Visible = true;
					}
					if (Strings.Left(fldAccount4.Text, 1) != "G")
					{
						fldYear4.Visible = false;
					}
					else
					{
						fldYear4.Visible = true;
					}
					if (Strings.Left(fldAccount5.Text, 1) != "G")
					{
						fldYear5.Visible = false;
					}
					else
					{
						fldYear5.Visible = true;
					}
					if (Strings.Left(fldAccount6.Text, 1) != "G")
					{
						fldYear6.Visible = false;
					}
					else
					{
						fldYear6.Visible = true;
					}
					if (Conversion.Val(fldBMV.Text) == 0)
					{
						fldBMV.Text = "No";
					}
					else
					{
						fldBMV.Text = "Yes";
					}
					if (Conversion.Val(fldPrint.Text) == 0)
					{
						fldPrint.Text = "No";
					}
					else
					{
						fldPrint.Text = "Yes";
					}
				}
				rsData.MoveNext();
			}
		}

		private void arTypeListing_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//arTypeListing.Caption	= "Cash Receipt Type Listing";
			//arTypeListing.Icon	= "arTypeListing.dsx":0000";
			//arTypeListing.Left	= 0;
			//arTypeListing.Top	= 0;
			//arTypeListing.Width	= 11805;
			//arTypeListing.Height	= 8595;
			//arTypeListing.StartUpPosition	= 3;
			//arTypeListing.SectionData	= "arTypeListing.dsx":058A;
			//End Unmaped Properties
		}
	}
}
