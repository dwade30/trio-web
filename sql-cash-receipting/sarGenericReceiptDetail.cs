﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Drawing;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for sarGenericReceiptDetail.
	/// </summary>
	public partial class sarGenericReceiptDetail : FCSectionReport
	{
		public static sarGenericReceiptDetail InstancePtr
		{
			get
			{
				return (sarGenericReceiptDetail)Sys.GetInstance(typeof(sarGenericReceiptDetail));
			}
		}

		protected sarGenericReceiptDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	sarGenericReceiptDetail	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               10/01/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               09/30/2004              *
		// ********************************************************
		float[] lngLeftCol = new float[5 + 1];
		float lngTotalWidth;
		int lngCurrentPayment;
		float[] lngTop = new float[6 + 1];
		int intRowsUsed;
		float lngFieldWid;
		string strNarrowInfo = "";
		clsDRWrapper rsBatch = new clsDRWrapper();
		bool boolOpened;
		int lngReceiptArrayIndex;
		float lngLineHeight;

		public sarGenericReceiptDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (Conversion.Val(this.UserData) == 0)
			{
				eArgs.EOF = true;
				lngReceiptArrayIndex = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData)));
			}
			else
			{
				lngReceiptArrayIndex = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData)));
				this.UserData = 0;
				eArgs.EOF = false;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lngLineHeight = 250 / 1440F;
			lngTop[0] = 0;
			lngTop[1] = lngLineHeight * 1;
			lngTop[2] = lngLineHeight * 2;
			lngTop[3] = lngLineHeight * 3;
			lngTop[4] = lngLineHeight * 4;
			lngTop[5] = lngLineHeight * 5;
			lngTop[6] = lngLineHeight * 6;
			SetArraySizes();
			SetupReceiptFormat();
			lngCurrentPayment = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData)));
			if (!modGlobal.Statics.gboolNarrowReceipt)
			{
				SetReceiptFontSize();
			}

            //FC:FINAl:SBE - #3235 - add report fields in report start
            for (int i = 1; i <= 7; i++)
            {
                GrapeCity.ActiveReports.SectionReportModel.Label obNew;
                string strTemp;
                // add a label for a description
                obNew = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>("lblFeeD" + FCConvert.ToString(i));
                //obNew.Name = "lblFeeD" + FCConvert.ToString(i);
                obNew.Visible = false;
            }
        }

		private void SetArraySizes()
		{
			// this sub will set the array elements to the left of each column and it will set
			// the total size of the report depending on the size of the receipt
			if (!modGlobal.Statics.gboolNarrowReceipt)
			{
				// wide format
				lngLeftCol[0] = 0;
				lngLeftCol[1] = 1170 / 1440F;
				lngLeftCol[2] = 2880 / 1440F;
				lngLeftCol[3] = 3060 / 1440F;
				lngLeftCol[4] = 4950 / 1440F;
				lngLeftCol[5] = 5130 / 1440F;
				lngTotalWidth = 7215 / 1440F;
				lngFieldWid = 2880 / 1440F;
				PageSettings.Margins.Left = 1440 / 1440F;
				PageSettings.Margins.Right = 1440 / 1440F;
			}
			else
			{
				// narrow format
				//Document.Printer.RenderMode = 1;
				lngLeftCol[0] = 0;
				lngLeftCol[1] = 100 / 1440F;
				lngLeftCol[2] = 0;
				lngLeftCol[3] = 200 / 1440F;
				lngLeftCol[4] = 500 / 1440F;
				lngLeftCol[5] = 300 / 1440F;
				lngTotalWidth = 5400 / 1440F;
				lngFieldWid = 1440 / 1440F;
				PageSettings.Margins.Left = 0;
				PageSettings.Margins.Right = 0;
			}
		}

		private void SetupReceiptFormat()
		{
			// set the lefts
			// 0 column
			fldName.Left = lngLeftCol[0];
			lblComment.Left = lngLeftCol[0];
			// set the widths
			this.PrintWidth = lngTotalWidth;
			fldNarrow.Width = lngTotalWidth;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			bool boolBatchDone;
			bool boolDetail = false;
			int intCT;
			int intCommentLine = 0;
			if (lngReceiptArrayIndex != 0)
			{
				// MAL@20070108
				// intRowsUsed = 3
				intRowsUsed = 6;
				// find the type in the global recordset
				modGlobal.Statics.grsType.FindFirstRecord("TypeCode", modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Type);
				if (modGlobal.Statics.grsType.NoMatch)
				{
					boolDetail = false;
				}
				else if (modGlobal.Statics.grsType.Get_Fields_Boolean("ShowFeeDetailOnReceipt"))
				{
					boolDetail = true;
				}
				else
				{
					boolDetail = false;
				}
				if (Strings.Trim(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Comment) == "")
				{
					// check to see if there is a comment
					lblComment.Top = lngTop[0];
					// if not then move the comment label to the top of the detail section
					lblComment.Visible = false;
					// and make it visible = false
					intRowsUsed -= 1;
					// row count is decreased by one line
				}
				else
				{
					// else
					lblComment.Visible = true;
					// show comment line
					lblComment.Top = lngTop[2];
					// set the top to the third line
					intCommentLine = 2;
					lblComment.Text = "   " + modGlobalFunctions.PadStringWithSpaces(Strings.Trim(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Comment), 39, false);
					// fill comment line
				}
				if (Strings.Trim(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Name) == Strings.Trim(Strings.Right(arGenericReceipt.InstancePtr.lblPaidBy.Text, arGenericReceipt.InstancePtr.lblPaidBy.Text.Length - 8)))
				{
					// check to see if the name matches the paid by name
					intRowsUsed -= 1;
					// if it is the same then decrease the number of rows needed
					fldNarrow.Top = lngTop[0];
					fldName.Text = "";
					fldName.Visible = false;
					if (Strings.Trim(lblComment.Text) != "")
					{
						// check to see if there is a comment
						lblComment.Top = lngTop[1];
						// if not then move the comment label to the 2nd line of the detail section
						intCommentLine = 2;
					}
				}
				else
				{
					fldName.Visible = true;
					fldName.Top = 0;
					fldName.Text = "   **" + modGlobalFunctions.PadStringWithSpaces(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Name, 37, false);
					fldNarrow.Top = lngTop[1];
				}
				// use the narrow receipt format or use the multiple field format
				fldNarrow.Visible = true;
				if (boolDetail)
				{
					// if I am showing the detail, then do not put the total amount at the end
					strNarrowInfo = modGlobalFunctions.PadStringWithSpaces(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].TypeDescription, 16, false) + " " + modGlobalFunctions.PadStringWithSpaces(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Reference, 10, false);
				}
				else
				{
					strNarrowInfo = modGlobalFunctions.PadStringWithSpaces(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].TypeDescription, 16, false) + " " + modGlobalFunctions.PadStringWithSpaces(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Reference, 9, false) + modGlobalFunctions.PadStringWithSpaces(Strings.Format(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Total, "#,##0.00"), 11);
				}
				fldNarrow.Text = strNarrowInfo;
				// MAL@20080107; Tracker Reference: 11158
				if (!modGlobalConstants.Statics.gblnShowControlFields)
				{
					fldControlDetail1.Top = lngTop[0];
					// Set the subreport to the top of the detail
					fldControlDetail1.Visible = false;
					// Set visible = false
					fldControlDetail2.Top = lngTop[0];
					// Set the subreport to the top of the detail
					fldControlDetail2.Visible = false;
					// Set visible = false
					fldControlDetail3.Top = lngTop[0];
					// Set the subreport to the top of the detail
					fldControlDetail3.Visible = false;
					// Set visible = false
					intRowsUsed -= 3;
				}
				else
				{
					FillControlFields(intCommentLine);
				}
				if (boolDetail)
				{
					// show the breakdown of the fees
					// If .Fee1 <> 0 And Trim(.FeeD1) <> "" Then
					// AddFeeRow .FeeD1, .Fee1, intRowsUsed * lngLineHeight, 1
					// intRowsUsed = intRowsUsed + 1
					// End If
					if (modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Type >= 90 && modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Type <= 96 && Strings.UCase(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control2) == "D" || Strings.UCase(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control2) == "A")
					{
						if (modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control2 == "D")
						{
							AddFeeRow_74("DISCOUNT", modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Fee1, intRowsUsed * lngLineHeight, 1);
							intRowsUsed += 1;
						}
						else if (modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control2 == "A")
						{
							AddFeeRow_74("ABATEMENT", modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Fee1, intRowsUsed * lngLineHeight, 1);
							intRowsUsed += 1;
						}
					}
					else
					{
						if (modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Fee1 != 0 && Strings.Trim(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].FeeD1) != "")
						{
							AddFeeRow_74(Strings.Left(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].FeeD1, 10), modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Fee1, intRowsUsed * lngLineHeight, 1);
							intRowsUsed += 1;
						}
					}
					if (modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Fee2 != 0 && Strings.Trim(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].FeeD2) != "")
					{
						AddFeeRow_72(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].FeeD2, modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Fee2, intRowsUsed * lngLineHeight, 2);
						intRowsUsed += 1;
					}
					if (modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Fee3 != 0 && Strings.Trim(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].FeeD3) != "")
					{
						AddFeeRow_72(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].FeeD3, modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Fee3, intRowsUsed * lngLineHeight, 3);
						intRowsUsed += 1;
					}
					if (modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Fee4 != 0 && Strings.Trim(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].FeeD4) != "")
					{
						AddFeeRow_72(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].FeeD4, modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Fee4, intRowsUsed * lngLineHeight, 4);
						intRowsUsed += 1;
					}
					if (modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Fee5 != 0 && Strings.Trim(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].FeeD5) != "")
					{
						AddFeeRow_72(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].FeeD5, modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Fee5, intRowsUsed * lngLineHeight, 5);
						intRowsUsed += 1;
					}
					if (modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Fee6 != 0 && Strings.Trim(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].FeeD6) != "")
					{
						AddFeeRow_72(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].FeeD6, modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Fee6, intRowsUsed * lngLineHeight, 6);
						intRowsUsed += 1;
					}
					if (modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].ConvenienceFee != 0)
					{
						AddFeeRow_80("CNV FEE", FCConvert.ToDouble(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].ConvenienceFee), intRowsUsed * lngLineHeight, 7);
						intRowsUsed += 1;
					}
				}
				else
				{
					if (modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].ConvenienceFee != 0)
					{
						AddFeeRow_242("CNV FEE", FCConvert.ToDouble(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].ConvenienceFee), intRowsUsed * lngLineHeight, 1, true);
						intRowsUsed += 1;
					}
				}
				Detail.Height = intRowsUsed * lngLineHeight;
			}
		}

		private void AddFeeRow_72(string strFeeDesc, double dblFee, float lngTop, short intNum, bool blnNoIndent = false)
		{
			AddFeeRow(ref strFeeDesc, ref dblFee, ref lngTop, ref intNum, blnNoIndent);
		}

		private void AddFeeRow_74(string strFeeDesc, double dblFee, float lngTop, short intNum, bool blnNoIndent = false)
		{
			AddFeeRow(ref strFeeDesc, ref dblFee, ref lngTop, ref intNum, blnNoIndent);
		}

		private void AddFeeRow_80(string strFeeDesc, double dblFee, float lngTop, short intNum, bool blnNoIndent = false)
		{
			AddFeeRow(ref strFeeDesc, ref dblFee, ref lngTop, ref intNum, blnNoIndent);
		}

		private void AddFeeRow_242(string strFeeDesc, double dblFee, float lngTop, short intNum, bool blnNoIndent = false)
		{
			AddFeeRow(ref strFeeDesc, ref dblFee, ref lngTop, ref intNum, blnNoIndent);
		}

		private void AddFeeRow(ref string strFeeDesc, ref double dblFee, ref float lngTop, ref short intNum, bool blnNoIndent = false)
		{
			GrapeCity.ActiveReports.SectionReportModel.Label obNew;
			string strTemp;
            // add a label for a description
            //FC:FINAL:SBE - #3235 - add report fields in ReportStart
            obNew = Detail.Controls["lblFeeD" + FCConvert.ToString(intNum)] as GrapeCity.ActiveReports.SectionReportModel.Label;
            obNew.Visible = true;
            //Detail.Controls.Add(obNew = new GrapeCity.ActiveReports.SectionReportModel.Label());
			//obNew.Name = "lblFeeD" + FCConvert.ToString(intNum);
			obNew.Top = lngTop;
			if (!blnNoIndent)
			{
				obNew.Left = fldNarrow.Left + 200 / 1440F;
			}
			else
			{
				obNew.Left = fldNarrow.Left;
			}
			obNew.Width = this.PrintWidth - 200 / 1440F;
			obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
			//strTemp = obNew.Font;
			obNew.Font = fldNarrow.Font;
			// this sets the font to the same as the field that is already created
			if (!blnNoIndent)
			{
				obNew.Text = modGlobalFunctions.PadStringWithSpaces(strFeeDesc, 20, false) + modGlobalFunctions.PadStringWithSpaces(Strings.Format(dblFee, "#,##0.00"), 15);
			}
			else
			{
				obNew.Text = modGlobalFunctions.PadStringWithSpaces(strFeeDesc, 20, false) + modGlobalFunctions.PadStringWithSpaces(Strings.Format(dblFee, "#,##0.00"), 17);
			}
		}

		private void SetReceiptFontSize()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngFSize;
				string strTemp = "";
				// this function will get the font size set for wide receipts and adjust the fields accordingly
				modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRWideFontSize", ref strTemp);
				lngFSize = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
				if (lngFSize <= 0)
				{
					return;
				}
				fldName.Font = new Font(fldName.Font.Name, lngFSize);
				fldNarrow.Font = new Font(fldNarrow.Font.Name, lngFSize);
				lblComment.Font = new Font(lblComment.Font.Name, lngFSize);
				fldControlDetail1.Font = new Font(fldControlDetail1.Font.Name, lngFSize);
				fldControlDetail2.Font = new Font(fldControlDetail2.Font.Name, lngFSize);
				fldControlDetail3.Font = new Font(fldControlDetail3.Font.Name, lngFSize);
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				// do nothing
			}
		}

		private void FillControlFields(int intCommentLine)
		{
			clsDRWrapper rsType = new clsDRWrapper();
			bool blnCont1 = false;
			bool blnCont2 = false;
			bool blnCont3 = false;
			int intLine;
			intLine = intCommentLine + 1;
			rsType.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Type, modExtraModules.strCRDatabase);
			if (rsType.RecordCount() > 0)
			{
				// Set boolean values for control values
				if (FCConvert.ToString(rsType.Get_Fields_String("Control1")) != "" && modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control1 != "")
				{
					blnCont1 = true;
				}
				else
				{
					blnCont1 = false;
				}
				if (FCConvert.ToString(rsType.Get_Fields_String("Control2")) != "" && modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control2 != "")
				{
					blnCont2 = true;
				}
				else
				{
					blnCont2 = false;
				}
				if (FCConvert.ToString(rsType.Get_Fields_String("Control3")) != "" && modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control3 != "")
				{
					blnCont3 = true;
				}
				else
				{
					blnCont3 = false;
				}
				if (blnCont1)
				{
					if (blnCont2)
					{
						if (blnCont3)
						{
							// All Three Have Data
							// Show Fields
							fldControlDetail1.Visible = true;
							fldControlDetail2.Visible = true;
							fldControlDetail3.Visible = true;
							fldControlDetail1.Text = rsType.Get_Fields_String("Control1") + ":" + modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control1;
							fldControlDetail1.Top = lngTop[intLine];
							fldControlDetail2.Text = rsType.Get_Fields_String("Control2") + ":" + modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control2;
							fldControlDetail2.Top = lngTop[intLine + 1];
							fldControlDetail3.Text = rsType.Get_Fields_String("Control3") + ":" + modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control3;
							fldControlDetail3.Top = lngTop[intLine + 2];
						}
						else
						{
							fldControlDetail1.Visible = true;
							fldControlDetail2.Visible = true;
							fldControlDetail3.Visible = false;
							fldControlDetail1.Text = rsType.Get_Fields_String("Control1") + ":" + modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control1;
							fldControlDetail1.Top = lngTop[intLine];
							fldControlDetail2.Text = rsType.Get_Fields_String("Control2") + ":" + modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control2;
							fldControlDetail2.Top = lngTop[intLine + 1];
							fldControlDetail3.Text = "";
							fldControlDetail3.Top = lngTop[0];
						}
					}
					else
					{
						if (blnCont3)
						{
							fldControlDetail1.Visible = true;
							fldControlDetail2.Visible = true;
							fldControlDetail3.Visible = false;
							fldControlDetail1.Text = rsType.Get_Fields_String("Control1") + ":" + modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control1;
							fldControlDetail1.Top = lngTop[intLine];
							fldControlDetail2.Text = rsType.Get_Fields_String("Control3") + ":" + modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control3;
							fldControlDetail2.Top = lngTop[intLine + 1];
							fldControlDetail3.Text = "";
							fldControlDetail3.Top = lngTop[0];
						}
						else
						{
							fldControlDetail1.Visible = true;
							fldControlDetail2.Visible = false;
							fldControlDetail3.Visible = false;
							fldControlDetail1.Text = rsType.Get_Fields_String("Control1") + ":" + modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control1;
							fldControlDetail1.Top = lngTop[intLine];
							fldControlDetail2.Text = "";
							fldControlDetail2.Top = lngTop[0];
							fldControlDetail3.Text = "";
							fldControlDetail3.Top = lngTop[0];
						}
					}
				}
				else
				{
					if (blnCont2)
					{
						if (blnCont3)
						{
							fldControlDetail1.Visible = true;
							fldControlDetail2.Visible = true;
							fldControlDetail3.Visible = false;
							fldControlDetail1.Text = rsType.Get_Fields_String("Control2") + ":" + modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control2;
							fldControlDetail1.Top = lngTop[intLine];
							fldControlDetail2.Text = rsType.Get_Fields_String("Control3") + ":" + modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control3;
							fldControlDetail2.Top = lngTop[intLine + 1];
							fldControlDetail3.Text = "";
							fldControlDetail3.Top = lngTop[0];
						}
						else
						{
							fldControlDetail1.Visible = true;
							fldControlDetail2.Visible = false;
							fldControlDetail3.Visible = false;
							fldControlDetail1.Text = rsType.Get_Fields_String("Control2") + ":" + modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control2;
							fldControlDetail1.Top = lngTop[intLine];
							fldControlDetail2.Text = "";
							fldControlDetail2.Top = lngTop[0];
							fldControlDetail3.Text = "";
							fldControlDetail3.Top = lngTop[0];
						}
					}
					else
					{
						if (blnCont3)
						{
							fldControlDetail1.Visible = true;
							fldControlDetail2.Visible = false;
							fldControlDetail3.Visible = false;
							fldControlDetail1.Text = rsType.Get_Fields_String("Control3") + ":" + modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control3;
							fldControlDetail1.Top = lngTop[intLine];
							fldControlDetail2.Text = "";
							fldControlDetail2.Top = lngTop[0];
							fldControlDetail3.Text = "";
							fldControlDetail3.Top = lngTop[0];
						}
						else
						{
							fldControlDetail1.Visible = false;
							fldControlDetail2.Visible = false;
							fldControlDetail3.Visible = false;
							fldControlDetail1.Text = "";
							fldControlDetail1.Top = lngTop[0];
							fldControlDetail2.Text = "";
							fldControlDetail2.Top = lngTop[0];
							fldControlDetail3.Text = "";
							fldControlDetail3.Top = lngTop[0];
						}
					}
				}
			}
			else
			{
				// Do Nothing - No Data
			}
		}

		private void sarGenericReceiptDetail_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//sarGenericReceiptDetail.Caption	= "Receipt Detail";
			//sarGenericReceiptDetail.Icon	= "sarGenericReceiptDetail.dsx":0000";
			//sarGenericReceiptDetail.Left	= 0;
			//sarGenericReceiptDetail.Top	= 0;
			//sarGenericReceiptDetail.Width	= 11880;
			//sarGenericReceiptDetail.Height	= 8445;
			//sarGenericReceiptDetail.StartUpPosition	= 3;
			//sarGenericReceiptDetail.SectionData	= "sarGenericReceiptDetail.dsx":058A;
			//End Unmaped Properties
		}
	}
}
