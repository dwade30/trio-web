﻿namespace TWCR0000
{
	/// <summary>
	/// Summary description for sarAccountingSummary.
	/// </summary>
	partial class sarAccountingSummary
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(sarAccountingSummary));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDescription = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldCD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCurCash = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCash = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCheck = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCreditCard = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblRowTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblNonEntry = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotalActivity = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCurPeriod = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPrevPeriod = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblNonCash = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblColTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalCash = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCurCheck = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCurCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCurOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCurTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCurNonEntry = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPrevious = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPrevTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPrevNonEntry = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblOther = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldCurTotalActivity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPrevTotalActivity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNonCash = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNonCashTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNonCashNonEntry = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNonCashTotalActivity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalCheck = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalNonEntry = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTotalActivity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblJournal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldError = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalPrevious = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPrevious = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurCash)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCash)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCreditCard)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRowTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNonEntry)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalActivity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCurPeriod)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrevPeriod)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNonCash)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblColTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCash)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurNonEntry)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrevious)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrevTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrevNonEntry)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurTotalActivity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrevTotalActivity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNonCash)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNonCashTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNonCashNonEntry)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNonCashTotalActivity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalNonEntry)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotalActivity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblJournal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldError)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPrevious)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrevious)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAccount,
				this.fldAmount,
				this.fldDescription,
				this.Line,
				this.fldCD
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.AfterPrint += new System.EventHandler(this.ReportFooter_AfterPrint);
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblAccount,
				this.lblAmount,
				this.lblDescription
			});
			this.GroupHeader1.Height = 0.2083333F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldCurCash,
				this.lblCash,
				this.lblCheck,
				this.lblCreditCard,
				this.lblRowTotal,
				this.lblNonEntry,
				this.lblTotalActivity,
				this.lblCurPeriod,
				this.lblPrevPeriod,
				this.lblNonCash,
				this.lblColTotal,
				this.fldTotalCash,
				this.fldCurCheck,
				this.fldCurCredit,
				this.fldCurOther,
				this.fldCurTotal,
				this.fldCurNonEntry,
				this.fldPrevious,
				this.fldPrevTotal,
				this.fldPrevNonEntry,
				this.lblOther,
				this.fldCurTotalActivity,
				this.fldPrevTotalActivity,
				this.fldNonCash,
				this.fldNonCashTotal,
				this.fldNonCashNonEntry,
				this.fldNonCashTotalActivity,
				this.fldTotalCheck,
				this.fldTotalCredit,
				this.fldTotalOther,
				this.fldTotalTotal,
				this.fldTotalNonEntry,
				this.fldTotalTotalActivity,
				this.Line1,
				this.Line2,
				this.lblJournal,
				this.fldError,
				this.fldTotalPrevious,
				this.lblPrevious
			});
			this.GroupFooter1.Height = 2.3125F;
			this.GroupFooter1.KeepTogether = true;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 0.5F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.lblAccount.Text = "Account";
			this.lblAccount.Top = 0F;
			this.lblAccount.Width = 1.0625F;
			// 
			// lblAmount
			// 
			this.lblAmount.Height = 0.1875F;
			this.lblAmount.HyperLink = null;
			this.lblAmount.Left = 2.5625F;
			this.lblAmount.Name = "lblAmount";
			this.lblAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblAmount.Text = "Amount";
			this.lblAmount.Top = 0F;
			this.lblAmount.Width = 1.125F;
			// 
			// lblDescription
			// 
			this.lblDescription.Height = 0.1875F;
			this.lblDescription.HyperLink = null;
			this.lblDescription.Left = 4.5F;
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.lblDescription.Text = "Description";
			this.lblDescription.Top = 0F;
			this.lblDescription.Width = 2.8125F;
			// 
			// fldAccount
			// 
			this.fldAccount.Height = 0.1875F;
			this.fldAccount.Left = 0.5F;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldAccount.Text = null;
			this.fldAccount.Top = 0F;
			this.fldAccount.Width = 1.75F;
			// 
			// fldAmount
			// 
			this.fldAmount.Height = 0.1875F;
			this.fldAmount.Left = 2.5625F;
			this.fldAmount.Name = "fldAmount";
			this.fldAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldAmount.Text = null;
			this.fldAmount.Top = 0F;
			this.fldAmount.Width = 0.875F;
			// 
			// fldDescription
			// 
			this.fldDescription.Height = 0.1875F;
			this.fldDescription.Left = 4.5F;
			this.fldDescription.Name = "fldDescription";
			this.fldDescription.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldDescription.Text = null;
			this.fldDescription.Top = 0F;
			this.fldDescription.Width = 2.8125F;
			// 
			// Line
			// 
			this.Line.Height = 0F;
			this.Line.Left = 0F;
			this.Line.LineWeight = 1F;
			this.Line.Name = "Line";
			this.Line.Top = 0F;
			this.Line.Visible = false;
			this.Line.Width = 7F;
			this.Line.X1 = 0F;
			this.Line.X2 = 7F;
			this.Line.Y1 = 0F;
			this.Line.Y2 = 0F;
			// 
			// fldCD
			// 
			this.fldCD.Height = 0.1875F;
			this.fldCD.Left = 3.4375F;
			this.fldCD.Name = "fldCD";
			this.fldCD.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldCD.Text = null;
			this.fldCD.Top = 0F;
			this.fldCD.Width = 0.25F;
			// 
			// fldCurCash
			// 
			this.fldCurCash.Height = 0.1875F;
			this.fldCurCash.Left = 1.8125F;
			this.fldCurCash.Name = "fldCurCash";
			this.fldCurCash.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldCurCash.Text = "0.00";
			this.fldCurCash.Top = 0.4375F;
			this.fldCurCash.Width = 1.125F;
			// 
			// lblCash
			// 
			this.lblCash.Height = 0.1875F;
			this.lblCash.HyperLink = null;
			this.lblCash.Left = 0.625F;
			this.lblCash.Name = "lblCash";
			this.lblCash.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.lblCash.Text = "Cash";
			this.lblCash.Top = 0.4375F;
			this.lblCash.Width = 1.0625F;
			// 
			// lblCheck
			// 
			this.lblCheck.Height = 0.1875F;
			this.lblCheck.HyperLink = null;
			this.lblCheck.Left = 0.625F;
			this.lblCheck.Name = "lblCheck";
			this.lblCheck.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.lblCheck.Text = "Check";
			this.lblCheck.Top = 0.625F;
			this.lblCheck.Width = 1.0625F;
			// 
			// lblCreditCard
			// 
			this.lblCreditCard.Height = 0.1875F;
			this.lblCreditCard.HyperLink = null;
			this.lblCreditCard.Left = 0.625F;
			this.lblCreditCard.Name = "lblCreditCard";
			this.lblCreditCard.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.lblCreditCard.Text = "Credit/Debit Card";
			this.lblCreditCard.Top = 0.8125F;
			this.lblCreditCard.Width = 1.125F;
			// 
			// lblRowTotal
			// 
			this.lblRowTotal.Height = 0.1875F;
			this.lblRowTotal.HyperLink = null;
			this.lblRowTotal.Left = 0.625F;
			this.lblRowTotal.Name = "lblRowTotal";
			this.lblRowTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.lblRowTotal.Text = "Total";
			this.lblRowTotal.Top = 1.375F;
			this.lblRowTotal.Width = 1.0625F;
			// 
			// lblNonEntry
			// 
			this.lblNonEntry.Height = 0.1875F;
			this.lblNonEntry.HyperLink = null;
			this.lblNonEntry.Left = 0.625F;
			this.lblNonEntry.Name = "lblNonEntry";
			this.lblNonEntry.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.lblNonEntry.Text = "Non Entry";
			this.lblNonEntry.Top = 1.5625F;
			this.lblNonEntry.Width = 1.0625F;
			// 
			// lblTotalActivity
			// 
			this.lblTotalActivity.Height = 0.1875F;
			this.lblTotalActivity.HyperLink = null;
			this.lblTotalActivity.Left = 0.625F;
			this.lblTotalActivity.Name = "lblTotalActivity";
			this.lblTotalActivity.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.lblTotalActivity.Text = "Total Activity";
			this.lblTotalActivity.Top = 1.75F;
			this.lblTotalActivity.Width = 1.0625F;
			// 
			// lblCurPeriod
			// 
			this.lblCurPeriod.Height = 0.1875F;
			this.lblCurPeriod.HyperLink = null;
			this.lblCurPeriod.Left = 1.75F;
			this.lblCurPeriod.Name = "lblCurPeriod";
			this.lblCurPeriod.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblCurPeriod.Text = "Current Period";
			this.lblCurPeriod.Top = 0.25F;
			this.lblCurPeriod.Width = 1.1875F;
			// 
			// lblPrevPeriod
			// 
			this.lblPrevPeriod.Height = 0.1875F;
			this.lblPrevPeriod.HyperLink = null;
			this.lblPrevPeriod.Left = 2.9375F;
			this.lblPrevPeriod.Name = "lblPrevPeriod";
			this.lblPrevPeriod.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblPrevPeriod.Text = "Previous Period";
			this.lblPrevPeriod.Top = 0.25F;
			this.lblPrevPeriod.Width = 1.1875F;
			// 
			// lblNonCash
			// 
			this.lblNonCash.Height = 0.1875F;
			this.lblNonCash.HyperLink = null;
			this.lblNonCash.Left = 4.125F;
			this.lblNonCash.Name = "lblNonCash";
			this.lblNonCash.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblNonCash.Text = "Non Cash";
			this.lblNonCash.Top = 0.25F;
			this.lblNonCash.Width = 1.1875F;
			// 
			// lblColTotal
			// 
			this.lblColTotal.Height = 0.1875F;
			this.lblColTotal.HyperLink = null;
			this.lblColTotal.Left = 5.625F;
			this.lblColTotal.Name = "lblColTotal";
			this.lblColTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblColTotal.Text = "Total";
			this.lblColTotal.Top = 0.25F;
			this.lblColTotal.Width = 1.125F;
			// 
			// fldTotalCash
			// 
			this.fldTotalCash.Height = 0.1875F;
			this.fldTotalCash.Left = 5.625F;
			this.fldTotalCash.Name = "fldTotalCash";
			this.fldTotalCash.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldTotalCash.Text = "0.00";
			this.fldTotalCash.Top = 0.4375F;
			this.fldTotalCash.Width = 1.125F;
			// 
			// fldCurCheck
			// 
			this.fldCurCheck.Height = 0.1875F;
			this.fldCurCheck.Left = 1.8125F;
			this.fldCurCheck.Name = "fldCurCheck";
			this.fldCurCheck.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldCurCheck.Text = "0.00";
			this.fldCurCheck.Top = 0.625F;
			this.fldCurCheck.Width = 1.125F;
			// 
			// fldCurCredit
			// 
			this.fldCurCredit.Height = 0.1875F;
			this.fldCurCredit.Left = 1.8125F;
			this.fldCurCredit.Name = "fldCurCredit";
			this.fldCurCredit.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldCurCredit.Text = "0.00";
			this.fldCurCredit.Top = 0.8125F;
			this.fldCurCredit.Width = 1.125F;
			// 
			// fldCurOther
			// 
			this.fldCurOther.Height = 0.1875F;
			this.fldCurOther.Left = 1.8125F;
			this.fldCurOther.Name = "fldCurOther";
			this.fldCurOther.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldCurOther.Text = "0.00";
			this.fldCurOther.Top = 1.1875F;
			this.fldCurOther.Width = 1.125F;
			// 
			// fldCurTotal
			// 
			this.fldCurTotal.Height = 0.1875F;
			this.fldCurTotal.Left = 1.8125F;
			this.fldCurTotal.Name = "fldCurTotal";
			this.fldCurTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.fldCurTotal.Text = "0.00";
			this.fldCurTotal.Top = 1.375F;
			this.fldCurTotal.Width = 1.125F;
			// 
			// fldCurNonEntry
			// 
			this.fldCurNonEntry.Height = 0.1875F;
			this.fldCurNonEntry.Left = 1.8125F;
			this.fldCurNonEntry.Name = "fldCurNonEntry";
			this.fldCurNonEntry.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldCurNonEntry.Text = "0.00";
			this.fldCurNonEntry.Top = 1.5625F;
			this.fldCurNonEntry.Width = 1.125F;
			// 
			// fldPrevious
			// 
			this.fldPrevious.Height = 0.1875F;
			this.fldPrevious.Left = 3F;
			this.fldPrevious.Name = "fldPrevious";
			this.fldPrevious.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldPrevious.Text = "0.00";
			this.fldPrevious.Top = 1F;
			this.fldPrevious.Width = 1.125F;
			// 
			// fldPrevTotal
			// 
			this.fldPrevTotal.Height = 0.1875F;
			this.fldPrevTotal.Left = 3F;
			this.fldPrevTotal.Name = "fldPrevTotal";
			this.fldPrevTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.fldPrevTotal.Text = "0.00";
			this.fldPrevTotal.Top = 1.375F;
			this.fldPrevTotal.Width = 1.125F;
			// 
			// fldPrevNonEntry
			// 
			this.fldPrevNonEntry.Height = 0.1875F;
			this.fldPrevNonEntry.Left = 3F;
			this.fldPrevNonEntry.Name = "fldPrevNonEntry";
			this.fldPrevNonEntry.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldPrevNonEntry.Text = "0.00";
			this.fldPrevNonEntry.Top = 1.5625F;
			this.fldPrevNonEntry.Width = 1.125F;
			// 
			// lblOther
			// 
			this.lblOther.Height = 0.1875F;
			this.lblOther.HyperLink = null;
			this.lblOther.Left = 0.625F;
			this.lblOther.Name = "lblOther";
			this.lblOther.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.lblOther.Text = "Other";
			this.lblOther.Top = 1.1875F;
			this.lblOther.Width = 1.0625F;
			// 
			// fldCurTotalActivity
			// 
			this.fldCurTotalActivity.Height = 0.1875F;
			this.fldCurTotalActivity.Left = 1.8125F;
			this.fldCurTotalActivity.Name = "fldCurTotalActivity";
			this.fldCurTotalActivity.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.fldCurTotalActivity.Text = "0.00";
			this.fldCurTotalActivity.Top = 1.75F;
			this.fldCurTotalActivity.Width = 1.125F;
			// 
			// fldPrevTotalActivity
			// 
			this.fldPrevTotalActivity.Height = 0.1875F;
			this.fldPrevTotalActivity.Left = 3F;
			this.fldPrevTotalActivity.Name = "fldPrevTotalActivity";
			this.fldPrevTotalActivity.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.fldPrevTotalActivity.Text = "0.00";
			this.fldPrevTotalActivity.Top = 1.75F;
			this.fldPrevTotalActivity.Width = 1.125F;
			// 
			// fldNonCash
			// 
			this.fldNonCash.Height = 0.1875F;
			this.fldNonCash.Left = 4.1875F;
			this.fldNonCash.Name = "fldNonCash";
			this.fldNonCash.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldNonCash.Text = "0.00";
			this.fldNonCash.Top = 1.1875F;
			this.fldNonCash.Width = 1.125F;
			// 
			// fldNonCashTotal
			// 
			this.fldNonCashTotal.Height = 0.1875F;
			this.fldNonCashTotal.Left = 4.1875F;
			this.fldNonCashTotal.Name = "fldNonCashTotal";
			this.fldNonCashTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.fldNonCashTotal.Text = "0.00";
			this.fldNonCashTotal.Top = 1.375F;
			this.fldNonCashTotal.Width = 1.125F;
			// 
			// fldNonCashNonEntry
			// 
			this.fldNonCashNonEntry.Height = 0.1875F;
			this.fldNonCashNonEntry.Left = 4.1875F;
			this.fldNonCashNonEntry.Name = "fldNonCashNonEntry";
			this.fldNonCashNonEntry.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldNonCashNonEntry.Text = "0.00";
			this.fldNonCashNonEntry.Top = 1.5625F;
			this.fldNonCashNonEntry.Width = 1.125F;
			// 
			// fldNonCashTotalActivity
			// 
			this.fldNonCashTotalActivity.Height = 0.1875F;
			this.fldNonCashTotalActivity.Left = 4.1875F;
			this.fldNonCashTotalActivity.Name = "fldNonCashTotalActivity";
			this.fldNonCashTotalActivity.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.fldNonCashTotalActivity.Text = "0.00";
			this.fldNonCashTotalActivity.Top = 1.75F;
			this.fldNonCashTotalActivity.Width = 1.125F;
			// 
			// fldTotalCheck
			// 
			this.fldTotalCheck.Height = 0.1875F;
			this.fldTotalCheck.Left = 5.625F;
			this.fldTotalCheck.Name = "fldTotalCheck";
			this.fldTotalCheck.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldTotalCheck.Text = "0.00";
			this.fldTotalCheck.Top = 0.625F;
			this.fldTotalCheck.Width = 1.125F;
			// 
			// fldTotalCredit
			// 
			this.fldTotalCredit.Height = 0.1875F;
			this.fldTotalCredit.Left = 5.625F;
			this.fldTotalCredit.Name = "fldTotalCredit";
			this.fldTotalCredit.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldTotalCredit.Text = "0.00";
			this.fldTotalCredit.Top = 0.8125F;
			this.fldTotalCredit.Width = 1.125F;
			// 
			// fldTotalOther
			// 
			this.fldTotalOther.Height = 0.1875F;
			this.fldTotalOther.Left = 5.625F;
			this.fldTotalOther.Name = "fldTotalOther";
			this.fldTotalOther.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldTotalOther.Text = "0.00";
			this.fldTotalOther.Top = 1.1875F;
			this.fldTotalOther.Width = 1.125F;
			// 
			// fldTotalTotal
			// 
			this.fldTotalTotal.Height = 0.1875F;
			this.fldTotalTotal.Left = 5.625F;
			this.fldTotalTotal.Name = "fldTotalTotal";
			this.fldTotalTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.fldTotalTotal.Text = "0.00";
			this.fldTotalTotal.Top = 1.375F;
			this.fldTotalTotal.Width = 1.125F;
			// 
			// fldTotalNonEntry
			// 
			this.fldTotalNonEntry.Height = 0.1875F;
			this.fldTotalNonEntry.Left = 5.625F;
			this.fldTotalNonEntry.Name = "fldTotalNonEntry";
			this.fldTotalNonEntry.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldTotalNonEntry.Text = "0.00";
			this.fldTotalNonEntry.Top = 1.5625F;
			this.fldTotalNonEntry.Width = 1.125F;
			// 
			// fldTotalTotalActivity
			// 
			this.fldTotalTotalActivity.Height = 0.1875F;
			this.fldTotalTotalActivity.Left = 5.625F;
			this.fldTotalTotalActivity.Name = "fldTotalTotalActivity";
			this.fldTotalTotalActivity.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.fldTotalTotalActivity.Text = "0.00";
			this.fldTotalTotalActivity.Top = 1.75F;
			this.fldTotalTotalActivity.Width = 1.125F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.625F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.375F;
			this.Line1.Width = 6.125F;
			this.Line1.X1 = 0.625F;
			this.Line1.X2 = 6.75F;
			this.Line1.Y1 = 1.375F;
			this.Line1.Y2 = 1.375F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0.625F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 1.75F;
			this.Line2.Width = 6.125F;
			this.Line2.X1 = 0.625F;
			this.Line2.X2 = 6.75F;
			this.Line2.Y1 = 1.75F;
			this.Line2.Y2 = 1.75F;
			// 
			// lblJournal
			// 
			this.lblJournal.Height = 0.3125F;
			this.lblJournal.HyperLink = null;
			this.lblJournal.Left = 1.375F;
			this.lblJournal.Name = "lblJournal";
			this.lblJournal.Style = "font-family: \'Tahoma\'";
			this.lblJournal.Text = null;
			this.lblJournal.Top = 2F;
			this.lblJournal.Width = 4.625F;
			// 
			// fldError
			// 
			this.fldError.Height = 0.1875F;
			this.fldError.Left = 0.3125F;
			this.fldError.Name = "fldError";
			this.fldError.Style = "font-family: \'Tahoma\'; text-align: left";
			this.fldError.Text = null;
			this.fldError.Top = 0.0625F;
			this.fldError.Visible = false;
			this.fldError.Width = 6.8125F;
			// 
			// fldTotalPrevious
			// 
			this.fldTotalPrevious.Height = 0.1875F;
			this.fldTotalPrevious.Left = 5.625F;
			this.fldTotalPrevious.Name = "fldTotalPrevious";
			this.fldTotalPrevious.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldTotalPrevious.Text = "0.00";
			this.fldTotalPrevious.Top = 1F;
			this.fldTotalPrevious.Width = 1.125F;
			// 
			// lblPrevious
			// 
			this.lblPrevious.Height = 0.1875F;
			this.lblPrevious.HyperLink = null;
			this.lblPrevious.Left = 0.625F;
			this.lblPrevious.Name = "lblPrevious";
			this.lblPrevious.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.lblPrevious.Text = "Previous";
			this.lblPrevious.Top = 1F;
			this.lblPrevious.Width = 1.0625F;
			// 
			// sarAccountingSummary
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.416667F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurCash)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCash)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCreditCard)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRowTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNonEntry)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalActivity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCurPeriod)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrevPeriod)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNonCash)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblColTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCash)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurNonEntry)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrevious)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrevTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrevNonEntry)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurTotalActivity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrevTotalActivity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNonCash)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNonCashTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNonCashNonEntry)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNonCashTotalActivity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalNonEntry)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotalActivity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblJournal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldError)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPrevious)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrevious)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCD;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDescription;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCurCash;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCash;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCheck;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCreditCard;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRowTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblNonEntry;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalActivity;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCurPeriod;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPrevPeriod;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblNonCash;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblColTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCash;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCurCheck;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCurCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCurOther;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCurTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCurNonEntry;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrevious;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrevTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrevNonEntry;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOther;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCurTotalActivity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrevTotalActivity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNonCash;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNonCashTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNonCashNonEntry;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNonCashTotalActivity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCheck;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalOther;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalNonEntry;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTotalActivity;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblJournal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldError;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPrevious;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPrevious;
	}
}
