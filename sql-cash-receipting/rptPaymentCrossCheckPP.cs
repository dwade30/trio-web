﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for rptPaymentCrossCheckPP.
	/// </summary>
	public partial class rptPaymentCrossCheckPP : BaseSectionReport
	{
		public static rptPaymentCrossCheckPP InstancePtr
		{
			get
			{
				return (rptPaymentCrossCheckPP)Sys.GetInstance(typeof(rptPaymentCrossCheckPP));
			}
		}

		protected rptPaymentCrossCheckPP _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPaymentCrossCheckPP	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               05/18/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/11/2006              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		int lngCount;
		double dblTotalAmt;
		int[] arrMissingPayments = null;
		bool boolDone;

		public rptPaymentCrossCheckPP()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "PP Payment Cross Check";
            this.ReportEnd += RptPaymentCrossCheckPP_ReportEnd;
		}

        private void RptPaymentCrossCheckPP_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = boolDone;
		}

		private void ActiveReport_Initialize()
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
		}
		//private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		//{
		//	switch (KeyCode)
		//	{
		//		case Keys.Escape:
		//			{
		//				Close();
		//				break;
		//			}
		//	} //end switch
		//}
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			CreateArrayElements();
			if (lngCount == 0)
			{
				MessageBox.Show("No PP records to report.", "No PP Missing Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Close();
			}
			else
			{
				lngCount = 0;
			}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			// MAL@20071012
			rptPaymentCrossCheck.InstancePtr.lblHeader2.Text = "Receipts appearing on this report do not have corresponding entries in the Tax Collections databases";
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void BindFields()
		{
			fldReceiptNumber.Text = "";
			fldDate.Text = "";
			fldName.Text = "";
			fldTeller.Text = "";
			fldPrin.Text = "";
			fldCurInt.Text = "";
			fldCost.Text = "";
			fldAccount.Text = "";
			if (lngCount <= Information.UBound(arrMissingPayments, 1))
			{
				rsData.FindFirstRecord("ID", arrMissingPayments[lngCount]);
				if (!rsData.NoMatch)
				{
					// TODO Get_Fields: Check the table for the column [Amount1] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Amount3] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Amount6] and replace with corresponding Get_Field method
					fldPrin.Text = Strings.Format(rsData.Get_Fields("Amount1") + rsData.Get_Fields("Amount3") + rsData.Get_Fields("Amount6"), "#,##0.00");
					// TODO Get_Fields: Check the table for the column [Amount2] and replace with corresponding Get_Field method
					fldCurInt.Text = Strings.Format(rsData.Get_Fields("Amount2"), "#,##0.00");
					// TODO Get_Fields: Check the table for the column [Amount4] and replace with corresponding Get_Field method
					fldCost.Text = Strings.Format(rsData.Get_Fields("Amount4"), "#,##0.00");
					fldReceiptNumber.Text = GetActualReceiptNumber_2(FCConvert.ToInt32(rsData.Get_Fields_Int32("ReceiptNumber")));
					fldDate.Text = Strings.Format(rsData.Get_Fields_DateTime("ActualSystemDate"), "MM/dd/yyyy");
					fldName.Text = rsData.Get_Fields_String("Name");
					fldTeller.Text = rsData.Get_Fields_String("TellerID");
					fldAccount.Text = Strings.Left(FCConvert.ToString(rsData.Get_Fields_String("Ref")), Strings.InStr(1, FCConvert.ToString(rsData.Get_Fields_String("Ref")), "-", CompareConstants.vbBinaryCompare));
				}
				else
				{
					ClearBoxes();
				}
				lngCount += 1;
			}
			else
			{
				boolDone = true;
			}
		}

		private void CreateArrayElements()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsCL = new clsDRWrapper();
				string strSQL;
				// vbPorter upgrade warning: strCCDate As DateTime	OnWrite(string)
				DateTime strCCDate = DateTime.FromOADate(0);
				int lngFM = 0;
				int lngYR = 0;
				int lngType = 0;
				// vbPorter upgrade warning: strLastEOY As string	OnWrite(DateTime)	OnRead(DateTime)
				string strLastEOY;
				if (modGlobalConstants.Statics.gboolBD)
				{
					rsCL.OpenRecordset("SELECT FiscalStart FROM Budgetary", modExtraModules.strBDDatabase);
					if (!rsCL.EndOfFile())
					{
						lngFM = FCConvert.ToInt32(rsCL.Get_Fields_String("FiscalStart"));
					}
					else
					{
						lngFM = 1;
					}
				}
				else
				{
					lngFM = 1;
				}
				// frmWait.Init "Please Wait..." & vbCrLf & "Loading Receipts"
				if (DateTime.Today.ToOADate() > DateAndTime.DateValue(FCConvert.ToString(lngFM) + "/01/" + FCConvert.ToString(DateTime.Today.Year)).ToOADate() && lngFM <= DateTime.Today.Month)
				{
					lngYR = DateTime.Today.Year;
				}
				else
				{
					lngYR = DateTime.Today.Year - 1;
				}
				strLastEOY = FCConvert.ToString(DateAndTime.DateValue(FCConvert.ToString(lngFM) + "/01/" + FCConvert.ToString(lngYR)));
				rsCL.OpenRecordset("SELECT * FROM CashRec");
				if (!rsCL.EndOfFile())
				{
					lngType = FCConvert.ToInt32(Math.Round(Conversion.Val(rsCL.Get_Fields_Int32("CrossCheckDefault"))));
				}
				switch (lngType)
				{
					case 0:
						{
							// last EOY
							strCCDate = FCConvert.ToDateTime(strLastEOY);
							break;
						}
					case 1:
						{
							// last cross check report
							if (DateAndTime.DateDiff("D", (DateTime)rsCL.Get_Fields_DateTime("LastCrossCheck"), DateAndTime.DateValue(strLastEOY)) > 0)
							{
								strCCDate = FCConvert.ToDateTime(strLastEOY);
							}
							else
							{
								strCCDate = FCConvert.ToDateTime(Strings.Format(rsCL.Get_Fields_DateTime("LastCrossCheck"), "MM/dd/yyyy"));
							}
							break;
						}
					case 2:
						{
							// specific date
							if (DateAndTime.DateDiff("D", (DateTime)rsCL.Get_Fields_DateTime("CrossCheckDefaultDate"), DateAndTime.DateValue(strLastEOY)) > 0)
							{
								strCCDate = FCConvert.ToDateTime(strLastEOY);
							}
							else
							{
								strCCDate = FCConvert.ToDateTime(Strings.Format(rsCL.Get_Fields_DateTime("CrossCheckDefaultDate"), "MM/dd/yyyy"));
							}
							break;
						}
				}
				//end switch
				// frmWait.Init "Please Wait..." & vbCrLf & "Loading Receipts"
				// If Date > CDate("07/01/" & Year(Date)) Then
				strSQL = "SELECT * FROM Archive WHERE ReceiptType = 92 AND ArchiveDate > '" + FCConvert.ToString(strCCDate) + "' ORDER BY ReceiptNumber";
				// Else
				// strSQL = "SELECT * FROM Archive WHERE ReceiptType = 92 AND Date > #" & CDate("07/01/" & Year(Date) - 1) & "# ORDER BY ReceiptNumber"
				// End If
				lngCount = 0;
				rsData.OpenRecordset(strSQL, modExtraModules.strCRDatabase);
				// frmWait.Init "Please Wait..." & vbCrLf & "Checking PP Receipts", True, rsData.RecordCount, True
				while (!rsData.EndOfFile())
				{
					// frmWait.IncrementProgress
					if (Conversion.Val(Strings.Right(Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Ref"))), 4)) > 0)
					{
						// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Amount1] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Amount3] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Amount6] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Amount2] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Amount4] and replace with corresponding Get_Field method
						rsCL.OpenRecordset("SELECT * FROM PaymentRec WHERE Account = " + rsData.Get_Fields("AccountNumber") + " AND BillCode = 'P' AND ([Year] / 10) = " + FCConvert.ToString(Conversion.Val(Strings.Right(Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Ref"))), 4))) + " AND ReceiptNumber = " + rsData.Get_Fields_Int32("ReceiptNumber") + " AND Code = '" + rsData.Get_Fields_String("Control2") + "' AND Month(ActualSystemDate) = " + Strings.Format(rsData.Get_Fields_DateTime("ActualSystemDate"), "MM") + " AND Day(ActualSystemDate) = " + Strings.Format(rsData.Get_Fields_DateTime("ActualSystemDate"), "dd") + " AND Principal = " + FCConvert.ToString(rsData.Get_Fields("Amount1") + rsData.Get_Fields("Amount3") + rsData.Get_Fields("Amount6")) + " AND CurrentInterest = " + rsData.Get_Fields("Amount2") + " AND LienCost = " + rsData.Get_Fields("Amount4"), modExtraModules.strCLDatabase);
						if (rsCL.EndOfFile())
						{
							Array.Resize(ref arrMissingPayments, lngCount + 1);
							arrMissingPayments[lngCount] = FCConvert.ToInt32(rsData.Get_Fields_Int32("ID"));
							lngCount += 1;
						}
					}
					rsData.MoveNext();
				}
				frmWait.InstancePtr.Unload();
				rsCL.DisposeOf();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Array Elements", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ClearBoxes()
		{
			fldCost.Text = "";
			fldDate.Text = "";
			fldCurInt.Text = "";
			fldName.Text = "";
			fldPrin.Text = "";
			fldReceiptNumber.Text = "";
			fldTeller.Text = "";
		}
		// vbPorter upgrade warning: 'Return' As string	OnWrite(string, int)
		private string GetActualReceiptNumber_2(int lngRN, DateTime? dtDate = null)
		{
			return GetActualReceiptNumber(ref lngRN, dtDate);
		}

		private string GetActualReceiptNumber(ref int lngRN, DateTime? dtDateTemp = null)
		{
			DateTime dtDate = dtDateTemp ?? DateTime.Now;
			string GetActualReceiptNumber = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will accept a long of a receipt number ID in the CR table
				// if the receipt does not exist or any other error occurs, the function will
				// return 0, otherwise it returns the receipt number shown on the receipt
				clsDRWrapper rsRN = new clsDRWrapper();
				if (lngRN > 0)
				{
					if (modGlobalConstants.Statics.gboolCR)
					{
						rsRN.OpenRecordset("SELECT * FROM Receipt WHERE ReceiptKey = " + FCConvert.ToString(lngRN), modExtraModules.strCRDatabase);
						if (rsRN.EndOfFile() != true && rsRN.BeginningOfFile() != true)
						{
							GetActualReceiptNumber = FCConvert.ToString(rsRN.Get_Fields_Int32("ReceiptNumber"));
						}
						else
						{
							rsRN.OpenRecordset("SELECT * FROM LastYearReceipt WHERE ReceiptKey = " + FCConvert.ToString(lngRN) + " AND LastYearReceiptDate > '" + Strings.Format(dtDate, "MM/dd/yyyy") + " 12:00:00 AM' AND LastYearReceiptDate < '" + Strings.Format(dtDate, "MM/dd/yyyy") + " 11:59:59 PM'", modExtraModules.strCRDatabase);
							if (rsRN.EndOfFile() != true && rsRN.BeginningOfFile() != true)
							{
								GetActualReceiptNumber = FCConvert.ToString(rsRN.Get_Fields_Int32("ReceiptNumber"));
							}
							else
							{
								GetActualReceiptNumber = FCConvert.ToString(lngRN) + "*";
							}
						}
					}
					else
					{
						GetActualReceiptNumber = FCConvert.ToString(lngRN);
					}
				}
				else
				{
					GetActualReceiptNumber = FCConvert.ToString(0);
				}
				rsRN.Reset();
				return GetActualReceiptNumber;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Returning Receipt Number", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetActualReceiptNumber;
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			if (lngCount != 1)
			{
				lblFooter.Text = "Missing " + FCConvert.ToString(lngCount) + " PP transactions.";
			}
			else
			{
				lblFooter.Text = "Missing " + FCConvert.ToString(lngCount) + " PP transaction.";
			}
		}

		private void rptPaymentCrossCheckPP_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptPaymentCrossCheckPP.Caption	= "PP Payment Cross Check";
			//rptPaymentCrossCheckPP.Icon	= "rptPaymentCrossCheckPP.dsx":0000";
			//rptPaymentCrossCheckPP.Left	= 0;
			//rptPaymentCrossCheckPP.Top	= 0;
			//rptPaymentCrossCheckPP.Width	= 11880;
			//rptPaymentCrossCheckPP.Height	= 8595;
			//rptPaymentCrossCheckPP.StartUpPosition	= 3;
			//rptPaymentCrossCheckPP.SectionData	= "rptPaymentCrossCheckPP.dsx":058A;
			//End Unmaped Properties
		}
	}
}
