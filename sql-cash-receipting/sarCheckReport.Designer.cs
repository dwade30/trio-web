﻿namespace TWCR0000
{
	/// <summary>
	/// Summary description for sarCheckReport.
	/// </summary>
	partial class sarCheckReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(sarCheckReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblBankNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCheckNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReceiptNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBankNumberTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBankTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.sarCheckReportDetailOB = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.lblTotalCash = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotalCheck = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lbltotalCredit = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotalOther = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalCash = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalCheck = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.lblBankNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheckNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReceiptNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBankNumberTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBankTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalCash)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbltotalCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCash)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.sarCheckReportDetailOB
			});
			this.Detail.Height = 0.1145833F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.CanShrink = true;
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblBankNumber,
				this.lblCheckNumber,
				this.lblAmount,
				this.lblReceiptNumber,
				this.lblBankNumberTotal,
				this.fldBankTotal,
				this.Line2
			});
			this.GroupHeader1.Height = 0.6875F;
			this.GroupHeader1.KeepTogether = true;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.AfterPrint += new System.EventHandler(this.GroupFooter1_AfterPrint);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblTotalCash,
				this.lblTotalCheck,
				this.lbltotalCredit,
				this.lblTotalOther,
				this.lblTotal,
				this.fldTotalCash,
				this.fldTotalCheck,
				this.fldTotalCredit,
				this.fldTotalOther,
				this.fldTotal,
				this.Line1
			});
			this.GroupFooter1.Height = 1.135417F;
			this.GroupFooter1.KeepTogether = true;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblBankNumber
			// 
			this.lblBankNumber.Height = 0.1875F;
			this.lblBankNumber.HyperLink = null;
			this.lblBankNumber.Left = 0.0625F;
			this.lblBankNumber.Name = "lblBankNumber";
			this.lblBankNumber.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left";
			this.lblBankNumber.Text = "Bank Number";
			this.lblBankNumber.Top = 0.0625F;
			this.lblBankNumber.Visible = false;
			this.lblBankNumber.Width = 2.3125F;
			// 
			// lblCheckNumber
			// 
			this.lblCheckNumber.Height = 0.1875F;
			this.lblCheckNumber.HyperLink = null;
			this.lblCheckNumber.Left = 2.375F;
			this.lblCheckNumber.Name = "lblCheckNumber";
			this.lblCheckNumber.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblCheckNumber.Text = "Check Number";
			this.lblCheckNumber.Top = 0.0625F;
			this.lblCheckNumber.Visible = false;
			this.lblCheckNumber.Width = 1.25F;
			// 
			// lblAmount
			// 
			this.lblAmount.Height = 0.1875F;
			this.lblAmount.HyperLink = null;
			this.lblAmount.Left = 3.625F;
			this.lblAmount.Name = "lblAmount";
			this.lblAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblAmount.Text = "Amount";
			this.lblAmount.Top = 0.0625F;
			this.lblAmount.Visible = false;
			this.lblAmount.Width = 1.4375F;
			// 
			// lblReceiptNumber
			// 
			this.lblReceiptNumber.Height = 0.1875F;
			this.lblReceiptNumber.HyperLink = null;
			this.lblReceiptNumber.Left = 5.0625F;
			this.lblReceiptNumber.Name = "lblReceiptNumber";
			this.lblReceiptNumber.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblReceiptNumber.Text = "Receipt Number";
			this.lblReceiptNumber.Top = 0.0625F;
			this.lblReceiptNumber.Visible = false;
			this.lblReceiptNumber.Width = 1.4375F;
			// 
			// lblBankNumberTotal
			// 
			this.lblBankNumberTotal.Height = 0.1875F;
			this.lblBankNumberTotal.HyperLink = null;
			this.lblBankNumberTotal.Left = 0.75F;
			this.lblBankNumberTotal.MultiLine = false;
			this.lblBankNumberTotal.Name = "lblBankNumberTotal";
			this.lblBankNumberTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; white-space: nowrap";
			this.lblBankNumberTotal.Text = "Bank Total:";
			this.lblBankNumberTotal.Top = 0.5F;
			this.lblBankNumberTotal.Visible = false;
			this.lblBankNumberTotal.Width = 2.875F;
			// 
			// fldBankTotal
			// 
			this.fldBankTotal.Height = 0.1875F;
			this.fldBankTotal.Left = 3.625F;
			this.fldBankTotal.MultiLine = false;
			this.fldBankTotal.Name = "fldBankTotal";
			this.fldBankTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; whi" + "te-space: nowrap";
			this.fldBankTotal.Text = "0.00";
			this.fldBankTotal.Top = 0.5F;
			this.fldBankTotal.Visible = false;
			this.fldBankTotal.Width = 1.4375F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 3.625F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.5F;
			this.Line2.Visible = false;
			this.Line2.Width = 1.5F;
			this.Line2.X1 = 3.625F;
			this.Line2.X2 = 5.125F;
			this.Line2.Y1 = 0.5F;
			this.Line2.Y2 = 0.5F;
			// 
			// sarCheckReportDetailOB
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.sarCheckReportDetailOB.CloseBorder = false;
			this.sarCheckReportDetailOB.Height = 0.125F;
			this.sarCheckReportDetailOB.Left = 0F;
			this.sarCheckReportDetailOB.Name = "sarCheckReportDetailOB";
			this.sarCheckReportDetailOB.Report = null;
			this.sarCheckReportDetailOB.Top = 0F;
			this.sarCheckReportDetailOB.Width = 7F;
			// 
			// lblTotalCash
			// 
			this.lblTotalCash.Height = 0.1875F;
			this.lblTotalCash.HyperLink = null;
			this.lblTotalCash.Left = 2.5F;
			this.lblTotalCash.Name = "lblTotalCash";
			this.lblTotalCash.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.lblTotalCash.Text = "Cash:";
			this.lblTotalCash.Top = 0.125F;
			this.lblTotalCash.Width = 1.0625F;
			// 
			// lblTotalCheck
			// 
			this.lblTotalCheck.Height = 0.1875F;
			this.lblTotalCheck.HyperLink = null;
			this.lblTotalCheck.Left = 2.5F;
			this.lblTotalCheck.Name = "lblTotalCheck";
			this.lblTotalCheck.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.lblTotalCheck.Text = "Check:";
			this.lblTotalCheck.Top = 0.3125F;
			this.lblTotalCheck.Width = 1.0625F;
			// 
			// lbltotalCredit
			// 
			this.lbltotalCredit.Height = 0.1875F;
			this.lbltotalCredit.HyperLink = null;
			this.lbltotalCredit.Left = 2.5F;
			this.lbltotalCredit.Name = "lbltotalCredit";
			this.lbltotalCredit.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.lbltotalCredit.Text = "Credit:";
			this.lbltotalCredit.Top = 0.5F;
			this.lbltotalCredit.Width = 1.0625F;
			// 
			// lblTotalOther
			// 
			this.lblTotalOther.Height = 0.1875F;
			this.lblTotalOther.HyperLink = null;
			this.lblTotalOther.Left = 2.5F;
			this.lblTotalOther.Name = "lblTotalOther";
			this.lblTotalOther.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.lblTotalOther.Text = "Other:";
			this.lblTotalOther.Top = 0.6875F;
			this.lblTotalOther.Width = 1.0625F;
			// 
			// lblTotal
			// 
			this.lblTotal.Height = 0.1875F;
			this.lblTotal.HyperLink = null;
			this.lblTotal.Left = 2.5F;
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.lblTotal.Text = "Total:";
			this.lblTotal.Top = 0.9375F;
			this.lblTotal.Width = 1.0625F;
			// 
			// fldTotalCash
			// 
			this.fldTotalCash.Height = 0.1875F;
			this.fldTotalCash.Left = 3.875F;
			this.fldTotalCash.Name = "fldTotalCash";
			this.fldTotalCash.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldTotalCash.Text = "0.00";
			this.fldTotalCash.Top = 0.125F;
			this.fldTotalCash.Width = 0.9375F;
			// 
			// fldTotalCheck
			// 
			this.fldTotalCheck.Height = 0.1875F;
			this.fldTotalCheck.Left = 3.875F;
			this.fldTotalCheck.Name = "fldTotalCheck";
			this.fldTotalCheck.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldTotalCheck.Text = "0.00";
			this.fldTotalCheck.Top = 0.3125F;
			this.fldTotalCheck.Width = 0.9375F;
			// 
			// fldTotalCredit
			// 
			this.fldTotalCredit.Height = 0.1875F;
			this.fldTotalCredit.Left = 3.875F;
			this.fldTotalCredit.Name = "fldTotalCredit";
			this.fldTotalCredit.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldTotalCredit.Text = "0.00";
			this.fldTotalCredit.Top = 0.5F;
			this.fldTotalCredit.Width = 0.9375F;
			// 
			// fldTotalOther
			// 
			this.fldTotalOther.Height = 0.1875F;
			this.fldTotalOther.Left = 3.875F;
			this.fldTotalOther.Name = "fldTotalOther";
			this.fldTotalOther.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldTotalOther.Text = "0.00";
			this.fldTotalOther.Top = 0.6875F;
			this.fldTotalOther.Width = 0.9375F;
			// 
			// fldTotal
			// 
			this.fldTotal.Height = 0.1875F;
			this.fldTotal.Left = 3.875F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.fldTotal.Text = "0.00";
			this.fldTotal.Top = 0.9375F;
			this.fldTotal.Width = 0.9375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 3.875F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.875F;
			this.Line1.Width = 0.9375F;
			this.Line1.X1 = 3.875F;
			this.Line1.X2 = 4.8125F;
			this.Line1.Y1 = 0.875F;
			this.Line1.Y2 = 0.875F;
			// 
			// sarCheckReport
			// 
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.020833F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblBankNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheckNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReceiptNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBankNumberTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBankTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalCash)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbltotalCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCash)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarCheckReportDetailOB;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBankNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCheckNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReceiptNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBankNumberTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBankTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalCash;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalCheck;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbltotalCredit;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalOther;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCash;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCheck;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalOther;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
	}
}
