﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for sarTellerReport.
	/// </summary>
	public partial class sarTellerReport : FCSectionReport
	{
		public static sarTellerReport InstancePtr
		{
			get
			{
				return (sarTellerReport)Sys.GetInstance(typeof(sarTellerReport));
			}
		}

		protected sarTellerReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	sarTellerReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               10/01/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               08/28/2003              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		string strLastID = "";
        private bool firstRecord = true;

		public sarTellerReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Teller Report";
            this.ReportEnd += SarTellerReport_ReportEnd;
		}

        private void SarTellerReport_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
        }

        private void ActiveReport_DataInitialize(object sender, EventArgs e)
        {
            this.Fields.Add("TellerBinder");
        }

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
            if (firstRecord)
            {
                firstRecord = false;
            }
            else
            {
                rsData.MoveNext();
            }

			if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
			{
                sarTellerDetailOB.Report.UserData = rsData.Get_Fields_String("TellerID");
				eArgs.EOF = false;
			}
			else
			{
				eArgs.EOF = true;
			}

            if (!eArgs.EOF)
            {
                this.Fields["TellerBinder"].Value = rsData.Get_Fields_String("TellerID");
            }
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will find out how many teller reports to print and will set the tag of the teller audit report
				// to the teller ID before printing it
				frmDailyReceiptAudit.InstancePtr.boolTellerOnly = true;
				if (arDailyAuditReport.InstancePtr.PageSettings.Orientation == GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape)
				{
					sarTellerDetailOB.Width = 15120 / 1440F;
					sarTellerReport.InstancePtr.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
					sarTellerReport.InstancePtr.PrintWidth = 15120 / 1440F;
					lblTellerID.Width = 15120 / 1440F;
				}
				else
				{
					sarTellerDetailOB.Width = 10800 / 1440F;
					sarTellerReport.InstancePtr.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
					sarTellerReport.InstancePtr.PrintWidth = 10800 / 1440F;
					lblTellerID.Width = 10800 / 1440F;
				}
				//Application.DoEvents();
				sarTellerDetailOB.Report = new arDailyTellerAuditReport(true);
				GetTellerID();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Teller Report Start", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void GetTellerID()
		{
			// this sub will fill rsdata with all of the TellerIDs that need to be reported apon
			string strSQL;

			if (frmDailyReceiptAudit.InstancePtr.boolUsePrevYears)
			{
				strSQL = "SELECT TellerID FROM LastYearArchive WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " GROUP BY TellerID";
			}
			else
			{
				strSQL = "SELECT TellerID FROM Archive WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " GROUP BY TellerID";
			}
			rsData.OpenRecordset(strSQL);
        }

		//private void Detail_Format(object sender, EventArgs e)
		//{
		//	if (rsData.EndOfFile() != true)
		//	{
		//		if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("TellerID"))) != "")
		//		{
		//			strLastID = "Teller ID: " + rsData.Get_Fields_String("TellerID");
		//		}
		//		else
		//		{
		//			strLastID = "Teller ID: NONE";
		//		}
		//		rsData.MoveNext();
		//	}
		//}

		//private void GroupHeader1_BeforePrint(object sender, EventArgs e)
		//{
		//	lblTellerID.Text = strLastID;
		//}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			arDailyAuditReport.InstancePtr.intReportHeader = 4;
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
            if (Strings.Trim(FCConvert.ToString(this.Fields["TellerBinder"].Value)) != "")
            {
                lblTellerID.Text = "Teller ID: " + this.Fields["TellerBinder"].Value;
            }
            else
            {
                lblTellerID.Text = "Teller ID: NONE";
            }
		}
	}
}
