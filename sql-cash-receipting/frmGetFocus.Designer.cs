﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Runtime.InteropServices;
using Global;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmGetFocus.
	/// </summary>
	partial class frmGetFocus : BaseForm
	{
		public fecherFoundation.FCButton cmdGo;
		public fecherFoundation.FCLabel lblOut;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmdGo = new fecherFoundation.FCButton();
			this.lblOut = new fecherFoundation.FCLabel();
			this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdGo)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 264);
			this.BottomPanel.Size = new System.Drawing.Size(349, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdGo);
			this.ClientArea.Controls.Add(this.lblOut);
			this.ClientArea.Size = new System.Drawing.Size(349, 204);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(349, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(253, 30);
			this.HeaderText.Text = "Motor Vehicle Receipt";
			// 
			// cmdGo
			// 
			this.cmdGo.AppearanceKey = "toolbarButton";
			this.cmdGo.Location = new System.Drawing.Point(97, 100);
			this.cmdGo.Name = "cmdGo";
			this.cmdGo.Size = new System.Drawing.Size(82, 30);
			this.cmdGo.TabIndex = 1;
			this.cmdGo.Text = "Continue";
			this.cmdGo.Click += new System.EventHandler(this.cmdGo_Click);
			// 
			// lblOut
			// 
			this.lblOut.Location = new System.Drawing.Point(30, 30);
			this.lblOut.Name = "lblOut";
			this.lblOut.Size = new System.Drawing.Size(228, 49);
			this.lblOut.TabIndex = 0;
			this.lblOut.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcess
			});
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = 0;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileSave,
				this.mnuProcessSaveExit,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Text = "File";
			// 
			// mnuFileSave
			// 
			this.mnuFileSave.Index = 0;
			this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuFileSave.Text = "Save";
			// 
			// mnuProcessSaveExit
			// 
			this.mnuProcessSaveExit.Index = 1;
			this.mnuProcessSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSaveExit.Text = "Save & Exit";
			this.mnuProcessSaveExit.Click += new System.EventHandler(this.mnuProcessSaveExit_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 2;
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 3;
			this.mnuProcessQuit.Text = "Exit";
			// 
			// frmGetFocus
			// 
			this.ClientSize = new System.Drawing.Size(349, 372);
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.Menu = this.MainMenu1;
			this.MinimizeBox = false;
			this.Name = "frmGetFocus";
			this.ShowInTaskbar = false;
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Motor Vehicle Receipt";
			this.Load += new System.EventHandler(this.frmGetFocus_Load);
			this.Activated += new System.EventHandler(this.frmGetFocus_Activated);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdGo)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
