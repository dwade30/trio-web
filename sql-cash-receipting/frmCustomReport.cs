﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using System.Linq;
using Global;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmCustomReport.
	/// </summary>
	public partial class frmCustomReport : BaseForm
	{
		public frmCustomReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			for (int i = 0; i < 20; i++)
			{
				this.arrQuestions[i] = new Questions(0);
			}
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCustomReport InstancePtr
		{
			get
			{
				return (frmCustomReport)Sys.GetInstance(typeof(frmCustomReport));
			}
		}

		protected frmCustomReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/19/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               02/19/2007              *
		// ********************************************************
		//
		// THIS FORM IS FOR THE CREATION OF CUSTOM REPORTS. IT ALLOWS THE USER        '
		// TO SELECT THE FIELDS TO BE DISPLAYED, THE ORDER TO DISPLAY THEM IN         '
		// THE SORT ORDER TO SHOW THEM IN AND THE ABILITY TO FILTER THE RECORDS       '
		// BY ANY FIELD EVEN IF IT IS NOT BEING SHOWN.                                '
		//
		// THIS FORM ***MUST*** WORK WITH modCustomReport.bas and rptCustomReport.rpt '
		//
		// THE DEVELOPER NEEDS ONLY ADD A CASE STATEMENT IN THE ROUTINE               '
		// SetFormFieldCaptions IN modCustomReport.bas                                '
		// NO CHANGES TO THIS FORM OR THE REPORT NEEDS TO BE DONE.                    '
		//
		// A CALL TO THIS FORM WOULD BE:                                              '
		// frmCustomReport.Show , MDIParent                                        '
		// Call SetFormFieldCaptions(frmCustomReport, "Births")                    '
		//
		//
		bool boolNoClick;
		int intCounter;
		int intStart;
		int intEnd;
		int intID;
		bool boolSummary;
		string strTemp = "";
		public bool boolSavingAnswers;
		bool boolSaveReport;
		bool boolAccountBox;
		clsGridAccount clsAcct = new clsGridAccount();
		bool boolEditingAcct;
		public clsDRWrapper rsType_AutoInitialized;

		public clsDRWrapper rsType
		{
			get
			{
				if (rsType_AutoInitialized == null)
				{
					rsType_AutoInitialized = new clsDRWrapper();
				}
				return rsType_AutoInitialized;
			}
			set
			{
				rsType_AutoInitialized = value;
			}
		}

		public clsDRWrapper rsCHK_AutoInitialized;

		public clsDRWrapper rsCHK
		{
			get
			{
				if (rsCHK_AutoInitialized == null)
				{
					rsCHK_AutoInitialized = new clsDRWrapper();
				}
				return rsCHK_AutoInitialized;
			}
			set
			{
				rsCHK_AutoInitialized = value;
			}
		}

		public clsDRWrapper rsSubReport_AutoInitialized;

		public clsDRWrapper rsSubReport
		{
			get
			{
				if (rsSubReport_AutoInitialized == null)
				{
					rsSubReport_AutoInitialized = new clsDRWrapper();
				}
				return rsSubReport_AutoInitialized;
			}
			set
			{
				rsSubReport_AutoInitialized = value;
			}
		}
		// these will be to pass the string on to the reports when needed
		public string strRSWhere = "";
		public string strRSFrom = "";
		public string strRSOrder = "";
		public bool boolShowActualSysDate;
		public int MAX_VARIABLES;
		// this is the maximum number of array elements in the arrCustomReport array
		public int lngSpecificBank;
		public bool boolCloseOutType;
		bool boolLoaded;
		public int lngRowReceiptDate;
		public int lngRowCloseOut;
		public int lngRowTellerID;
		public int lngRowReceiptType;
		public int lngRowReceiptNumber;
		public int lngRowReference;
		public int lngRowName;
		public int lngRowControl1;
		public int lngRowControl2;
		public int lngRowControl3;
		public int lngRowAccountNumber1;
		public int lngRowAccountNumber2;
		public int lngRowAccountNumber3;
		public int lngRowAccountNumber4;
		public int lngRowAccountNumber5;
		public int lngRowAccountNumber6;
		public int lngRowMVType;
		public int lngRowReceiptTotal;
		public int lngRowTenderedTotal;
		public int lngRowPaidBy;
		public int lngRowCheckNumber;
		public int lngRowDateToShow;
		public int lngRowBank;
		public int lngRowComment;
		public int lngRowPaymentType;
		public int lngRowReceiptTime;

		private struct Questions
		{
			public string Question;
			public int Type;
			public string AnswerString;
			public int answer;
			public string Instructions;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public Questions(int unusedParam)
			{
				this.Question = string.Empty;
				this.Type = 0;
				this.AnswerString = string.Empty;
				this.answer = 0;
				this.Instructions = string.Empty;
			}
		};

		private Questions[] arrQuestions = new Questions[20 + 1];

		private void cboSavedReport_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// THIS CODE WILL ALLOW THE USER TO DELETE A CUSTOM REPORT THAT
			// WAS PREVIOUSELY SAVED
			string strTemp = "";
			clsDRWrapper rs = new clsDRWrapper();
			int intCT;
			int intStart = 0;
			if (cmbReport.SelectedIndex == 2)
			{
				if (cboSavedReport.SelectedIndex < 0)
					return;
				if (MessageBox.Show("This will delete the custom report " + cboSavedReport.Items[cboSavedReport.SelectedIndex].ToString() + ". Continue?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					clsDRWrapper rsDelete = new clsDRWrapper();
					rsDelete.Execute("DELETE FROM SavedStatusReports WHERE ID = " + FCConvert.ToString(cboSavedReport.ItemData(cboSavedReport.SelectedIndex)), modGlobal.DEFAULTDATABASE);
					LoadCombo();
					MessageBox.Show("Custom report deleted successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			else if (cmbReport.SelectedIndex == 1)
			{
				// chekc to make sure that a report was selected
				if (cboSavedReport.SelectedIndex < 0)
					return;
				// show the report
				rs.OpenRecordset("SELECT * FROM SavedStatusReports WHERE ID = " + FCConvert.ToString(cboSavedReport.ItemData(cboSavedReport.SelectedIndex)), modGlobal.DEFAULTDATABASE);
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					lblReportName.Text = cboSavedReport.Items[cboSavedReport.SelectedIndex].ToString();
					// this will make the format of the report correct
					// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
					modCustomReport.Statics.strReportType = FCConvert.ToString(rs.Get_Fields("Type"));
					modCustomReport.SetFormFieldCaptions(this, modCustomReport.Statics.strReportType);
					// select all of the fields selected in the where list
					strTemp = "," + rs.Get_Fields_String("WhereSelection");
					for (intCT = 0; intCT <= lstTypes.Items.Count - 1; intCT++)
					{
						if (FCConvert.ToBoolean(Strings.InStr(1, strTemp, "," + FCConvert.ToString(Conversion.Val(Strings.Left(lstTypes.List(intCT), 3))) + ",", CompareConstants.vbBinaryCompare)))
						{
							lstTypes.SetSelected(intCT, true);
						}
						else
						{
							lstTypes.SetSelected(intCT, false);
						}
					}
					// clear the sort list
					lstSort.Clear();
					// load all of the selected fields in order
					strTemp = FCConvert.ToString(rs.Get_Fields_String("SortSelection"));
					if (strTemp.Length > 0)
					{
						intCT = 0;
						do
						{
							intStart = intCT + 1;
							intCT = Strings.InStr(intStart, strTemp, ",", CompareConstants.vbBinaryCompare);
							if (intCT > intStart)
							{
								// intStart will be the index of the next sort
								intStart = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(strTemp, intStart, intCT - 1))));
							}
							else
							{
								intStart = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Right(strTemp, strTemp.Length - (intStart - 1)))));
							}
							// add the item to the list
							lstSort.AddItem(modCustomReport.Statics.arrCustomReport[intStart].NameToShowUser);
							// enter the index in the item data
							lstSort.ItemData(lstSort.NewIndex, intStart);
							lstSort.SetSelected(lstSort.NewIndex, true);
							intStart = intCT + 1;
						}
						while (!(intCT == 0));
					}
					// load the rest of the fields that are not selected
					for (intCT = 0; intCT <= lstFields.Items.Count - 1; intCT++)
					{
						if (ItemInSortList(ref intCT))
						{
							// do nothing
						}
						else
						{
							lstSort.AddItem(lstFields.List(intCT));
							lstSort.ItemData(lstSort.NewIndex, lstFields.ItemData(intCT));
						}
					}
					// load the contraints
					for (intCT = 0; intCT <= vsWhere.Rows - 1; intCT++)
					{
						if (intCT < 15)
						{
							// TODO Get_Fields: Field [FieldConstraint] not found!! (maybe it is an alias?)
							if (Strings.InStr(1, FCConvert.ToString(rs.Get_Fields("FieldConstraint" + FCConvert.ToString(intCT))), "|||", CompareConstants.vbBinaryCompare) != 0)
							{
								// TODO Get_Fields: Field [FieldConstraint] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [FieldConstraint] not found!! (maybe it is an alias?)
								vsWhere.TextMatrix(intCT, 1, Strings.Left(FCConvert.ToString(rs.Get_Fields("FieldConstraint" + FCConvert.ToString(intCT))), Strings.InStr(1, FCConvert.ToString(rs.Get_Fields("FieldConstraint" + FCConvert.ToString(intCT))), "|||", CompareConstants.vbBinaryCompare) - 1));
								// TODO Get_Fields: Field [FieldConstraint] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [FieldConstraint] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [FieldConstraint] not found!! (maybe it is an alias?)
								strTemp = Strings.Right(FCConvert.ToString(rs.Get_Fields("FieldConstraint" + FCConvert.ToString(intCT))), FCConvert.ToString(rs.Get_Fields("FieldConstraint" + FCConvert.ToString(intCT))).Length - Strings.InStr(1, FCConvert.ToString(rs.Get_Fields("FieldConstraint" + FCConvert.ToString(intCT))), "|||", CompareConstants.vbBinaryCompare) - 2);
								strTemp = strTemp.Replace("|", "");
								vsWhere.TextMatrix(intCT, 2, strTemp);
							}
							else
							{
								vsWhere.TextMatrix(intCT, 1, "");
								vsWhere.TextMatrix(intCT, 2, "");
							}
						}
					}
					// load the answers to the questions
					for (intCT = 0; intCT <= vsSetup.Rows - 1; intCT++)
					{
						if (intCT < 10)
						{
							// TODO Get_Fields: Field [Ans] not found!! (maybe it is an alias?)
							vsSetup.TextMatrix(intCT, 1, FCConvert.ToString(rs.Get_Fields("Ans" + FCConvert.ToString(intCT))));
						}
					}
					// load the answers to the questions
					for (intCT = 1; intCT <= vsSetup.Rows - 1; intCT++)
					{
						if (intCT < 10)
						{
							arrQuestions[intCT].answer = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("SQL")), intCT, 1))));
						}
					}
				}
				else
				{
					MessageBox.Show("There was an error while opening this file.", "Load Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				cmbReport.SelectedIndex = 0;
				// set it back to create
			}
		}

		private void cmdAdd_Click(object sender, System.EventArgs e)
		{
			// THIS ALLOWS THE USER TO SAVE THE REPORT THAT WAS JUST GENERATED
			// THIS ONLY SAVES THE SQL STATEMENT THAT IS GENERATED. THE USER
			// WILL NOT SEE THE LIST BOXES AND WHERE GRID FILLED IN FOR THEM IF
			// THEY DISPLAY A SAVED REPORT SO NAMING EACH REPORT IS ***VERY***
			// IMPORTANT
			string strReturn = "";
			clsDRWrapper rsSave = new clsDRWrapper();
			int intRow;
			int intCol;
			clsDRWrapper RSLayout = new clsDRWrapper();
			int intCT;
			if (SaveAnswers_2(false))
			{
				strReturn = Interaction.InputBox("Enter name for new report", "New Custom Report", null);
				strReturn = modGlobalFunctions.RemoveApostrophe(strReturn);
				if (strReturn == string.Empty)
				{
					// DO NOT SAVE REPORT
					MessageBox.Show("Report not saved.", "No Report Name", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					// THIS ALLOWS FOR THE BUILDING OF THE SQL STATEMENT BUT DOES
					// NOT SHOW IT
					boolSaveReport = true;
					cmdPrint_Click();
					boolSaveReport = false;
					// SAVE THE REPORT
					rsSave.OpenRecordset("SELECT * FROM SavedStatusReports WHERE ReportName = '" + strReturn + "' and Type = '" + Strings.UCase(modCustomReport.Statics.strReportType) + "'", modGlobal.DEFAULTDATABASE);
					if (!rsSave.EndOfFile())
					{
						MessageBox.Show("A report by that name already exists. A different name must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}
					else
					{
						rsSave.AddNew();
						rsSave.Set_Fields("ReportName", strReturn);
						rsSave.Set_Fields("Type", "SEARCH");
						lblReportName.Text = strReturn;
						lblReportName.Visible = true;
						// build where string then save it into the Where Field
						// this will have a 0 when a field is not added and a 1 when it is added
						strTemp = "";
						// *****CR does not need this part, these are hard coded*****
						// For intCT = 0 To lstFields.ListCount - 1
						// If lstFields.Selected(intCT) Then
						// strTemp = strTemp & "1"
						// Else
						// strTemp = strTemp & "0"
						// End If
						// Next
						// Going to put the Type Selection here
						for (intCT = 0; intCT <= lstTypes.Items.Count - 1; intCT++)
						{
							if (lstTypes.Selected(intCT))
							{
								strTemp += FCConvert.ToString(Conversion.Val(Strings.Left(lstTypes.Items[intCT].Text, 3))) + ",";
							}
						}
						rsSave.Set_Fields("WhereSelection", strTemp);
						// create a sort selection string by creating a comma delilited string
						// that holds the order and the index number of each field
						strTemp = "";
						for (intCT = 0; intCT <= lstSort.Items.Count - 1; intCT++)
						{
							if (lstSort.Selected(intCT))
							{
								strTemp += FCConvert.ToString(lstSort.ItemData(intCT)) + ",";
							}
							else
							{
							}
						}
						if (strTemp.Length != 0)
						{
							strTemp = Strings.Left(strTemp, strTemp.Length - 1);
							// this removes the last comma
						}
						rsSave.Set_Fields("SortSelection", strTemp);
						// this will fill all of the where constraints in from the grid
						for (intCT = 0; intCT <= vsWhere.Rows - 1; intCT++)
						{
							if (intCT < 15)
							{
								rsSave.Set_Fields("FieldConstraint" + intCT, GetWhereConstraint(ref intCT));
							}
						}
						// this will put all of the answers from the top questions section
						strTemp = "";
						for (intCT = 1; intCT <= vsSetup.Rows - 1; intCT++)
						{
							if (intCT < 10)
							{
								if (vsSetup.TextMatrix(intCT, 1) != "")
								{
									rsSave.Set_Fields("Ans" + intCT, vsSetup.TextMatrix(intCT, 1));
								}
								else
								{
									rsSave.Set_Fields("Ans" + intCT, "");
								}
							}
						}
						for (intCT = 1; intCT <= vsSetup.Rows - 1; intCT++)
						{
							if (intCT < 10)
							{
								if (arrQuestions[intCT].answer != -1)
								{
									rsSave.Set_Fields("SQL", rsSave.Get_Fields_String("SQL") + FCConvert.ToString(arrQuestions[intCT].answer));
								}
								else
								{
									rsSave.Set_Fields("SQL", rsSave.Get_Fields_String("SQL") + "0");
								}
							}
						}
						if (rsSave.Update())
						{
							MessageBox.Show("Custom Report saved as " + strReturn, "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
						LoadCombo();
					}
				}
			}
			else
			{
				MessageBox.Show("Please answer the questions before saving this report.", "Not Enough Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			// CLEAR THE WHERE GRID SO THE USER CAN START FROM SCRATCH
			int intCounter;
			for (intCounter = 0; intCounter <= vsWhere.Rows - 1; intCounter++)
			{
				vsWhere.TextMatrix(intCounter, 1, string.Empty);
				vsWhere.TextMatrix(intCounter, 2, string.Empty);
				vsWhere.TextMatrix(intCounter, 3, string.Empty);
			}
			for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
			{
				lstSort.SetSelected(intCounter, false);
			}
			for (intCounter = 0; intCounter <= lstTypes.Items.Count - 1; intCounter++)
			{
				lstTypes.SetSelected(intCounter, false);
			}
			lblReportName.Text = "";
		}

		public void cmdClear_Click()
		{
			cmdClear_Click(cmdClear, new System.EventArgs());
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			// THIS ROUTINE WORKS TO PRINT OUT THE REPORT
			clsDRWrapper rsSQL = new clsDRWrapper();
			string strTemp = "";
			frmCustomReport.InstancePtr.rsType.OpenRecordset("SELECT * FROM Type");
			// make sure that the where grid is not in edit mode
			if (txtAcct.Visible)
			{
				// save account
				//Application.DoEvents();
				strTemp = txtAcct.EditText;
				//Application.DoEvents();
				if (modValidateAccount.AccountValidate(strTemp))
				{
					vsWhere.TextMatrix(lngRowAccountNumber1, 1, strTemp);
				}
				else
				{
					if (strTemp != "")
					{
						MessageBox.Show("Account was not validated correctly.  This criteria was not added to the search.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
				//Application.DoEvents();
				txtAcct.Visible = false;
			}
			vsWhere.Select(0, 0);
			// PREPARE THE SQL TO SHOW THE REPORT
			if (cmbReport.SelectedIndex == 0)
			{
				// BUILD THE SQL FOR THE NEW CUSTOM REPORT
				BuildSQL();
				// IF NO FIELDS WERE CHOSEN TO PRINT THEN DO NOT SHOW THE REPORT
				// If intNumberOfSQLFields < 0 Then GoTo NoFields
			}
			else
			{
				// IF THE USER DOES NOT CHOSE A REPORT THEN DO NOT SHOW ONE
				if (cboSavedReport.SelectedIndex < 0)
					return;
				// GET THE SAVED SQL STATEMENT FOR THE CUSTOM REPORT
				rsSQL.OpenRecordset("Select SQL from tblCustomReports where ID = " + FCConvert.ToString(cboSavedReport.ItemData(cboSavedReport.SelectedIndex)), "Twck0000.vb1");
				if (!rsSQL.EndOfFile())
				{
					modCustomReport.Statics.strCustomSQL = FCConvert.ToString(rsSQL.Get_Fields_String("SQL"));
				}
			}
			// GET THE NUMBER OF FIELDS TO DISPLAY AND FILL THE FIELDS CAPTION
			// ARRAY WITH THE DATABASE FIELD NAMES FOR THOSE THAT WERE CHOSEN
			// Call GetNumberOfFields(strCustomSQL)
			// If intNumberOfSQLFields < 0 Then
			// NoFields:
			// MsgBox "No fields were selected to display.", vbInformation + vbOKOnly, "TRIO Software"
			// Exit Sub
			// End If
			// GET THE CAPTIONS TO DISPLAY ON THE REPORT. THE DEVELOPER MAY
			// WANT THE DISPLAYED 'HEADINGS' TO BE DIFFERENT THEN THE DATABASE
			// FIELD NAMES OR THE CUSTOM REPORT FORM'S DISPLAY NAME
			// Call SetColumnCaptions(Me)
			if (!boolSaveReport)
			{
				// SHOW THE REPORT
				//frmReportViewer.InstancePtr.Init(arReceiptSearch.InstancePtr);
				//Application.DoEvents();
				frmReportViewer.InstancePtr.Focus();
				// use the hidden form to size the report
				// arReceiptSearch.Height = frmCustomReport.Height
				// arReceiptSearch.Top = frmCustomReport.Top
				// arReceiptSearch.Left = frmCustomReport.Left
				// arReceiptSearch.Width = frmCustomReport.Width
			}
		}

		public void cmdPrint_Click()
		{
			cmdPrint_Click(cmdPrint, new System.EventArgs());
		}

		public void BuildSQL()
		{
			// BUILD THE SQL STATEMENT FROM THE CRITERIA THAT THE USER HAS CHOSEN
			int intCounter;
			int intRow;
			int intCol;
			// kk01092013    Dim strSelectedFields(500) As String
			// CLEAR OUT VARIABLES
			modCustomReport.Statics.intNumberOfSQLFields = 0;
			modCustomReport.Statics.strCustomSQL = string.Empty;
			strRSFrom = "";
			strRSWhere = "";
			strRSOrder = "";
			strRSOrder = BuildSortParameter();
			strRSWhere = BuildWhereParameter();
            // strRSFrom
            //FC:FINAL:CHN: Fix difference in working Switch-Case with original app.
            // switch (FCConvert.ToInt32(Strings.Mid(modCustomReport.Statics.strBinaryReportType, 1, 1)))
            switch (Strings.Mid(modCustomReport.Statics.strBinaryReportType, 1, 1))
            {
                // case 0:
                case "0":
                    {
                        strRSFrom = "(" + modGlobal.Statics.strNewArchiveSQL + ") as NewArchive";
						break;
					}
				// case 1:
				case "1":
					{
						strRSFrom = "(" + modGlobal.Statics.strOldArchiveSQL + ") as OldArchive";
						break;
					}
				// case 2:
				case "2":
					{
						// kk01092014 Looking for some speed here
						// strRSFrom = "(SELECT * FROM (" & strOldArchiveSQL & ") as OldArchive UNION ALL SELECT * FROM (" & strNewArchiveSQL & ") as NewArchive) AS AllArchive"
						strRSFrom = "(" + modGlobal.Statics.strOldArchiveSQL + " UNION ALL " + modGlobal.Statics.strNewArchiveSQL + ") AS AllArchive";
						break;
					}
			}
			//end switch
		}

		private void frmCustomReport_Activated(object sender, System.EventArgs e)
		{
			string strTemp = "";
			if (boolLoaded == false)
			{
				lblInstructions.Text = "Answer these questions to customize your Receipt Report";
				ToolTip1.SetToolTip(fraWhere, "Double Click to clear search criteria.");
				// setup the Questions grid
				FormatQGrid();
				AddQuestionsToGrid();
				SetupGrid();
				FillQGrid();
				FillTypeList();
				// format the Where Grid
				ResizeGrid();
				LoadCombo();
				// set the notes text
				//strTemp = "1. Answer the questions at the top or load a saved report." + "\r\n" + "\r\n";
				//strTemp += "2. Select criteria by checking the box to the left in the Fields to Sort By list.  Then drag and drop captions to arrange the order of the sort.  " + "\r\n" + "\r\n";
				//strTemp += "3. Entering data into the Select Search Criteria control will filter the results shown on the report." + "\r\n" + "\r\n";
				//strTemp += "4. Double click on the caption Select Search Criteria to clear the filter and sort fields." + "\r\n" + "\r\n";
				//strTemp += "5. Print the report by choosing Print Report from the File Menu.";
				//txtInstructions.Text = strTemp;
				SaveAnswers();
				// set the last question's answer to the default
				// vsWhere.TextMatrix(arrCustomReport(lngRowDateToShow).RowNumber, 1) = "Effective Interest Date"
				vsWhere.TextMatrix(modCustomReport.Statics.arrCustomReport[lngRowDateToShow].RowNumber, 1, "Actual System Date");
				boolLoaded = true;
			}
		}

		private void frmCustomReport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						// If MsgBox("Are you sure that you would like to Exit from this screen?", vbYesNoCancel + vbQuestion, "Exit Receipt Search") = vbYes Then
						Close();
						// End If
						break;
					}
				case Keys.F10:
					{
						KeyCode = (Keys)0;
						// mnuPrint_Click
						break;
					}
			}
			//end switch
		}

		private void frmCustomReport_Load(object sender, System.EventArgs e)
		{
			MAX_VARIABLES = 25;
			lngRowReceiptDate = 0;
			lngRowCloseOut = 1;
			lngRowTellerID = 2;
			lngRowReceiptType = 3;
			lngRowReceiptNumber = 4;
			lngRowReference = 5;
			lngRowName = 6;
			lngRowControl1 = 7;
			lngRowControl2 = 8;
			lngRowControl3 = 9;
			lngRowAccountNumber1 = 10;
			lngRowAccountNumber2 = 11;
			lngRowAccountNumber3 = 12;
			lngRowAccountNumber4 = 13;
			lngRowAccountNumber5 = 14;
			lngRowAccountNumber6 = 15;
			lngRowMVType = 16;
			lngRowReceiptTotal = 17;
			lngRowTenderedTotal = 18;
			lngRowPaidBy = 19;
			lngRowCheckNumber = 20;
			lngRowDateToShow = 21;
			lngRowBank = 22;
			lngRowComment = 23;
			lngRowPaymentType = 24;
			lngRowReceiptTime = 25;
			clsAcct.GRID7Light = txtAcct;
			clsAcct.DefaultAccountType = "G";
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			cmbReport.SelectedIndex = 0;
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		private void frmCustomReport_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		public void LoadCombo()
		{
			// LOAD THE COMBO WITH ALL PREVIOUSLY SAVED REPORTS
			clsDRWrapper rsReports = new clsDRWrapper();
			// CLEAR OUT THE CONTROL
			cboSavedReport.Clear();
			// OPEN THE RECORDSET
			rsReports.OpenRecordset("SELECT * FROM SavedStatusReports", modExtraModules.strCRDatabase);
			// WHERE Type = '" & UCase(strReportType) & "'"
			while (!rsReports.EndOfFile())
			{
				// ADD THE ITEM TO THE COMBO
				cboSavedReport.AddItem(rsReports.Get_Fields_String("ReportName"));
				// ADD THE AUTONUMBER AS THE ID TO EACH ITEM
				cboSavedReport.ItemData(cboSavedReport.NewIndex, FCConvert.ToInt32(rsReports.Get_Fields_Int32("ID")));
				// GET THE NEXT RECORD
				rsReports.MoveNext();
			}
			// force the Where Grid to resize...
			ResizeGrid();
		}

		private void fraFields_DoubleClick(object sender, System.EventArgs e)
		{
			int intCounter;

			for (intCounter = 0; intCounter <= lstTypes.Items.Count - 1; intCounter++)
			{
				lstTypes.SetSelected(intCounter, true);
			}
		}

		private void fraSort_DoubleClick(object sender, System.EventArgs e)
		{
			int intCounter;
			for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
			{
				lstSort.SetSelected(intCounter, true);
			}
		}

		private void fraWhere_DoubleClick(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}

		private void lstSort_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolNoClick && lstSort.SelectedIndex != -1)
			{
				if (Strings.InStr(1, modCustomReport.Statics.strFields[lstSort.ItemData(lstSort.SelectedIndex)], "SUM(", CompareConstants.vbBinaryCompare) != 0)
				{
					MessageBox.Show("The Receipt Amount field is part of an aggregate function and cannot be used to sort.", "Invalid Sort parameter", MessageBoxButtons.OK, MessageBoxIcon.Information);
					boolNoClick = true;
					lstSort.SetSelected(lstSort.ListIndex, false);
				}
			}
			else
			{
				boolNoClick = false;
			}
		}

		private void mnuClear_Click(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}

		public void mnuClear_Click()
		{
			mnuClear_Click(mnuClear, new System.EventArgs());
		}

		private void mnuFileNew_Click(object sender, System.EventArgs e)
		{
			mnuClear_Click();
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			boolSavingAnswers = true;
			SaveAnswers();
			if (ValidateWhereGrid())
			{
				cmdPrint_Click();
			}
			boolSavingAnswers = false;
		}

		private void optReport_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			// THIS WILL ENABLE/DISABLE THE FRAMES ACCORDING TO THE RADIO BUTTONS
			// ON THE TOP OF THE FORM.
			// 
			// IF THE USER IS SHOWING OR DELETING A PREVIOUSLY SAVED REPORT THEN
			// WE DO NOT WANT THE USER TO SELECT ANY PARAMETERS
			//FC:FINAL:DDU:#2657 - always show delete prompt when selecting a report to delete
			if ((cmbReport.Text == "Show Saved Report") || (cmbReport.Text == "Delete Saved Report"))
			{
				cboSavedReport.Visible = true;
				cboSavedReport.Text = "";
			}
			cmdAdd.Visible = Index == 0;
			fraSort.Enabled = Index == 0;
			// fraFields.Enabled = Index = 0
			fraWhere.Enabled = Index == 0;
			lblReportName.Visible = Index == 0;
		}

		private void optReport_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbReport.SelectedIndex;
			optReport_CheckedChanged(index, sender, e);
		}

		private void txtAcct_Enter(object sender, System.EventArgs e)
		{
			boolAccountBox = true;
		}

		private void txtAcct_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			try
			{
				// On Error GoTo ERROR_HANDLER
				bool boolTest;
				int lngRW = 0;
				boolTest = false;
				switch (e.KeyCode)
				{
					case Keys.Up:
					case Keys.Left:
						{
							KeyCode = 0;
							txtAcct_Validating(boolTest);
							if (!boolTest)
							{
								vsWhere.Editable = FCGrid.EditableSettings.flexEDNone;
								lngRW = FCConvert.ToInt32(Math.Round(Conversion.Val(txtAcct.Tag)));
								if (lngRW > 0)
								{
									vsWhere.Focus();
									vsWhere.Select(lngRowAccountNumber1 - 1, 1);
								}
							}
							break;
						}
					case Keys.Down:
					case Keys.Return:
						{
							KeyCode = 0;
							txtAcct_Validating(boolTest);
							if (!boolTest)
							{
								vsWhere.Editable = FCGrid.EditableSettings.flexEDNone;
								if (lngRowAccountNumber1 + 1 < vsWhere.Rows - 2)
								{
									vsWhere.Focus();
									vsWhere.Select(lngRowAccountNumber1 + 1, 1);
								}
							}
							break;
						}
					case Keys.Right:
						{
							txtAcct_Validating(boolTest);
							if (!boolTest)
							{
								vsWhere.Editable = FCGrid.EditableSettings.flexEDNone;
								if (txtAcct.EditSelStart == txtAcct.TextMatrix(0, 0).Length - 1)
								{
									// this will check to see if this is at the end of the textbox
									if (lngRowAccountNumber1 + 1 < vsWhere.Rows - 2)
									{
										KeyCode = 0;
										vsWhere.Focus();
										vsWhere.Select(lngRowAccountNumber1 + 1, 1);
									}
								}
							}
							break;
						}
					case Keys.Delete:
						{
							if (e.Shift == true)
							{
								KeyCode = 0;
								txtAcct.TextMatrix(0, 0, "");
								// txtAcct.SelStart = 0
							}
							break;
						}
					case Keys.Home:
						{
							// If Shift = 3 Then
							// KeyCode = 0
							// If MDIParent.StatusBar1.Panels(2).Bevel = sbrInset Then
							// MDIParent.StatusBar1.Panels(2).Text = ""
							// MDIParent.StatusBar1.Panels(2).Bevel = sbrNoBevel
							// Else
							// MDIParent.StatusBar1.Panels(2).Text = "Software Engineer: " & ToggleEncryptCode("���������н��")
							// MDIParent.StatusBar1.Panels(2).Bevel = sbrInset
							// End If
							// End If
							break;
						}
				}
				//end switch
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Account Validation Error On Keydown", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtAcct_Leave(object sender, System.EventArgs e)
		{
			if (vsWhere.Visible && vsWhere.Enabled)
			{
				vsWhere.Focus();
			}
		}

		private void txtAcct_Validating(bool cancel)
		{
			txtAcct_Validating(txtAcct, new System.ComponentModel.CancelEventArgs(cancel));
		}

		private void txtAcct_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
            //FC:FINAL:AM:#4365 - use Text instead of EditText
            //if (txtAcct.EditText != "" && Strings.InStr(1, txtAcct.EditText, "_", CompareConstants.vbBinaryCompare) == 0)
            if (txtAcct.Text != "" && Strings.InStr(1, txtAcct.Text, "_", CompareConstants.vbBinaryCompare) == 0)
            {
				vsWhere.TextMatrix(lngRowAccountNumber1, 1, txtAcct.Text);
				// vsWhere.TextMatrix(Val(txtAcct.Tag), 1) = txtAcct.TextMatrix(0, 0)
			}
			else
			{
				vsWhere.TextMatrix(lngRowAccountNumber1, 1, "");
				// vsWhere.TextMatrix(Val(txtAcct.Tag), 1) = ""
			}
		}

		private void vsSetup_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			// this will set the type of the input column
			vsSetup.EditMask = string.Empty;
			vsSetup.ComboList = string.Empty;
			switch (arrQuestions[vsSetup.Row].Type)
			{
				case modCustomReport.GRIDTEXT:
					{
						break;
					}
				case modCustomReport.GRIDDATE:
					{
						vsSetup.EditMask = "##/##/####";
						break;
					}
				case modCustomReport.GRIDCOMBOIDTEXT:
				case modCustomReport.GRIDCOMBOIDNUM:
				case modCustomReport.GRIDCOMBOTEXT:
					{
						if (vsSetup.Col == 1)
						{
							vsSetup.ComboList = arrQuestions[vsSetup.Row].AnswerString;
						}
						break;
					}
				case modCustomReport.GRIDCOMBOANDNUM:
				case modCustomReport.GRIDCOMBOANDTEXT:
					{
						if (vsSetup.Col == 1)
						{
							vsSetup.ComboList = arrQuestions[vsSetup.Row].AnswerString;
						}
						break;
					}
				case modCustomReport.GRIDBDACCOUNT:
					{
						// FIX THIS!!!
						break;
					}
				case modCustomReport.GRIDLISTTEXT:
					{
						// FIX THIS!!!
						break;
					}
			}
			//end switch
		}

		private void vsSetup_CellChanged(object sender, DataGridViewCellMouseEventArgs e)
		{
			if (vsSetup.Col > 0)
			{
				arrQuestions[vsSetup.Row].answer = vsSetup.ComboIndex;
			}
		}

		private void vsSetup_ChangeEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (vsSetup.Col > 0)
			{
				arrQuestions[vsSetup.Row].answer = vsSetup.ComboIndex;
			}
		}

		private void vsSetup_EnterCell(object sender, System.EventArgs e)
        {
            //FC:FINAL:CHN: Change Row on Mouse Row to get correct new selected row.
            // switch (vsSetup.Row)
            switch (vsSetup.MouseRow)
            {
				case 1:
					{
						break;
					}
				case 2:
					{
						break;
					}
				case 3:
					{
						if (Strings.Left(vsSetup.TextMatrix(2, 1), 1) == "S")
						{
							ShowDetail(false);
						}
						else
						{
							ShowDetail(true);
						}
						if (Strings.UCase(Strings.Left(Strings.Trim(vsSetup.TextMatrix(5, 1)), 4)) == "TYPE")
						{
							ShowTypeSummary_2(true);
						}
						else
						{
							ShowTypeSummary_2(false);
						}
						break;
					}
				case 4:
					{
						if (Strings.Left(vsSetup.TextMatrix(2, 1), 1) == "S")
						{
							ShowDetail(false);
						}
						else
						{
							ShowDetail(true);
						}
						break;
					}
				case 5:
					{
						if (Strings.UCase(Strings.Left(Strings.Trim(vsSetup.TextMatrix(5, 1)), 4)) == "TYPE")
						{
							ShowTypeSummary_2(true);
						}
						else
						{
							ShowTypeSummary_2(false);
						}
						break;
					}
				case 6:
					{
						if (Strings.UCase(Strings.Left(Strings.Trim(vsSetup.TextMatrix(5, 1)), 4)) == "TYPE")
						{
							ShowTypeSummary_2(true);
						}
						else
						{
							ShowTypeSummary_2(false);
						}
						break;
					}
			}
			//end switch
		}

		private void vsSetup_Enter(object sender, System.EventArgs e)
		{
			boolAccountBox = false;
		}

		private void vsSetup_MouseMoveEvent(object sender, MouseEventArgs e)
		{
			int lngMouseRow;
			lngMouseRow = vsSetup.MouseRow;
			if (lngMouseRow >= 1 && lngMouseRow < Information.UBound(arrQuestions, 1))
			{
				lblInstructions.Text = arrQuestions[Math.Abs(lngMouseRow)].Instructions;
				if (Strings.Trim(lblInstructions.Text) == "")
				{
					lblInstructions.Text = "Answer these questions to customize your Receipt Report.";
				}
			}
		}

		private void vsSetup_RowColChange(object sender, System.EventArgs e)
		{
			// if this is set to summary, then shut off the next two options
			if (Strings.Left(vsSetup.TextMatrix(2, 1), 1) == "S")
			{
				ShowDetail(false);
			}
			else
			{
				ShowDetail(true);
			}
			//FC:FINAL:AM
			//if (vsSetup.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsSetup.Row, vsSetup.Col) == ColorTranslator.ToOle(vsSetup.BackColorFixed) || vsSetup.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsSetup.Row, vsSetup.Col) == ColorTranslator.ToOle(SystemColors.GrayText))
			if (vsSetup.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsSetup.Row, vsSetup.Col) == ColorTranslator.ToOle(SystemColors.GrayText))
			{
				vsSetup.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			else
			{
				switch (vsSetup.Col)
				{
					case 0:
						{
							vsSetup.Editable = FCGrid.EditableSettings.flexEDNone;
							break;
						}
					case 1:
					case 2:
						{
							vsSetup.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							break;
						}
				}
				//end switch
			}
		}

		private void vsSetup_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			bool boolReset = false;
			// if this is set to summary, then shut off the next two options
			if (Strings.Left(vsSetup.TextMatrix(2, 1), 1) == "S")
			{
				if (!boolSummary)
				{
					ShowDetail(false);
					boolReset = true;
				}
			}
			else
			{
				if (boolSummary)
				{
					ShowDetail(true);
					boolReset = true;
				}
			}
			SaveAnswers(boolReset);
		}

		private void vsSetup_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vsSetup.GetFlexRowIndex(e.RowIndex);
			int col = vsSetup.GetFlexColIndex(e.ColumnIndex);
			if (row == 2)
			{
				// if this is set to summary, then shut off the next two options
				if (Strings.Left(vsSetup.TextMatrix(2, 1), 1) == "S")
				{
					ShowDetail(false);
				}
				else
				{
					ShowDetail(true);
				}
			}
			if (col > 0)
			{
				arrQuestions[row].answer = vsSetup.ComboIndex;
			}
		}

		private void vsWhere_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			// AFTER THE USER SELECTS AN ITEM IN COMBO FOR A FIELD IN THE WHERE
			// GRID THEN WE NEED TO SAVE THE ***ID*** IN A FIELD FOR THAT SELECTION
			// 
			// THIS NEEDS TO BE SAVED AS THE COMBOITEMLIST IS PER FIELD AND BUILT
			// AND USED DYNAMICALLY INT HE BEFOREEDIT ROUTINE
			// 
			// THIS ID WILL BE USED TO PULL OUT THE CORRECT DATA IN THE WHERE CLAUSE
			vsWhere.TextMatrix(vsWhere.Row, 3, vsWhere.ComboData());
			if (Strings.Trim(vsWhere.TextMatrix(vsWhere.Row, 1)) == "__/__/____")
			{
				vsWhere.TextMatrix(vsWhere.Row, 1, string.Empty);
			}
			if (Strings.Trim(vsWhere.TextMatrix(vsWhere.Row, 2)) == "__/__/____")
			{
				vsWhere.TextMatrix(vsWhere.Row, 2, string.Empty);
			}
		}

		private void vsWhere_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			// SET THE FORMAT FOR THE TYPE OF FIELD THAT THIS IS
			// IF TEXT...ALLOW ANYTHING
			// IF DATE...ADD A MASK TO FORCE THE USE TO ENTER CORRECT DATA
			// IF COMBO...ADD THE LIST OF OPTIONS
			int lngIndex;
			vsWhere.EditMask = string.Empty;
			vsWhere.ComboList = string.Empty;
			lngIndex = FCConvert.ToInt32(Math.Round(Conversion.Val(vsWhere.TextMatrix(vsWhere.Row, 4))));
			switch (modCustomReport.Statics.arrCustomReport[lngIndex].WhereType)
			{
				case modCustomReport.GRIDTEXT:
					{
						break;
					}
				case modCustomReport.GRIDDATE:
                    {
                        //FC:FINAL:CHN: Fix Date mask format to get same behavior with original app.
                        // vsWhere.EditMask = "##/##/####";
                        vsWhere.EditMask = "00/00/0000";
                        break;
					}
				case modCustomReport.GRIDCOMBOIDTEXT:
				case modCustomReport.GRIDCOMBOIDNUM:
				case modCustomReport.GRIDCOMBOTEXT:
				case modCustomReport.GRIDCOMBONUMRANGE:
				case modCustomReport.GRIDCOMBORANGE:
                    {
                        //FC:FINAL:CHN: Remove empty select from ComboBox.
                        // vsWhere.ComboList = modCustomReport.Statics.arrCustomReport[lngIndex].ComboList;
                        vsWhere.ComboList = modCustomReport.Statics.arrCustomReport[lngIndex].ComboList.Length > 0 ?
                            modCustomReport.Statics.arrCustomReport[lngIndex].ComboList.Substring(0, modCustomReport.Statics.arrCustomReport[lngIndex].ComboList.Length - 1) : "";
                        break;
					}
				case modCustomReport.GRIDCOMBOANDNUM:
				case modCustomReport.GRIDCOMBOANDTEXT:
					{
						if (vsWhere.Col == 1)
						{
							vsWhere.ComboList = modCustomReport.Statics.arrCustomReport[lngIndex].ComboList;
						}
						break;
					}
				case modCustomReport.GRIDBDACCOUNT:
					{
						// FIX THIS!!!
						break;
					}
				case modCustomReport.GRIDLISTTEXT:
					{
						// FIX THIS!!!
						break;
					}
				case modCustomReport.GRIDTEXTRANGE:
					{
						// MAL@20081229: Added check for time range to format the edit mask
						// Tracker Reference: 13124
						if (modCustomReport.Statics.arrCustomReport[lngIndex].DatabaseFieldName == "ActualSystemTime")
						{
							vsWhere.EditMask = "00:00:00";
						}
						break;
					}
			}
			//end switch
		}

		private void vsWhere_BeforeScroll(object sender, ScrollEventArgs e)
		{
			if (txtAcct.Visible)
			{
				// txtAcct_Validate True
				txtAcct.Visible = false;
			}
		}

		private void vsWhere_Enter(object sender, System.EventArgs e)
		{
			// If Not (vsWhere.Row = vsWhere.rows - 1 And vsWhere.Col = 2) Then
			// vsWhere.TabBehavior = flexTabCells
			// End If
			if (boolAccountBox)
			{
				txtAcct.Visible = false;
				vsWhere.Select(lngRowAccountNumber1 + 1, 1);
			}
			else
			{
				vsWhere.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				//FC:FINAL:AM:#4365 - don't select the first row
                //vsWhere.Select(0, 1);
			}
			boolAccountBox = false;
		}

		private void vsWhere_KeyDownEvent(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Delete:
					{
						if (vsWhere.Col > 0)
						{
							vsWhere.TextMatrix(vsWhere.Row, vsWhere.Col, "");
						}
						break;
					}
			}
			//end switch
		}

		private void vsWhere_KeyDownEdit(object sender, KeyEventArgs e)
		{
			if (vsWhere.Col == 1)
			{
				switch (FCConvert.ToInt32(modCustomReport.Statics.strWhereType[vsWhere.Row]))
				{
					case modCustomReport.GRIDCOMBOTEXT:
					case modCustomReport.GRIDCOMBOIDNUM:
					case modCustomReport.GRIDCOMBOIDTEXT:
					case modCustomReport.GRIDCOMBOANDNUM:
					case modCustomReport.GRIDCOMBOANDTEXT:
					case modCustomReport.GRIDLISTTEXT:
						{
							if (e.KeyCode == Keys.Delete)
							{
								// can't set the .comboindex property to -1
								vsWhere.TextMatrix(vsWhere.Row, vsWhere.Col, "");
							}
							break;
						}
					case modCustomReport.GRIDBDACCOUNT:
						{
							break;
						}
				}
				//end switch
			}
		}

		private void vsWhere_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int keyAscii = Strings.Asc(e.KeyChar);
			FCGrid grid = sender as FCGrid;
			if (grid.Col != 0)
			{
				//switch (grid.Row) {
				if (grid.Row == modCustomReport.Statics.arrCustomReport[lngRowReceiptDate].RowNumber)
				{
					// Receipt Date
					if (grid.Col == 2)
					{
						if ((keyAscii >= 48 && keyAscii <= 57) || (keyAscii == 8))
						{
							// numbers
						}
						else
						{
							keyAscii = 0;
						}
					}
					else if (keyAscii == 27)
					{
						vsWhere.TextMatrix(grid.Row, grid.Col, "");
					}
					// numbers only
				}
				else if (grid.Row == modCustomReport.Statics.arrCustomReport[lngRowTellerID].RowNumber)
				{
					// Teller ID
					// only have letters and the backspace ID (8)
					if ((keyAscii >= 65 && keyAscii <= 90) || (keyAscii >= 96 && keyAscii <= 122) || (keyAscii == 8))
					{
						// change the letters to uppercase
						if (keyAscii >= 97 && keyAscii <= 122)
						{
							keyAscii -= 32;
						}
					}
					else
					{
						keyAscii = 0;
					}
				}
				else if (grid.Row == modCustomReport.Statics.arrCustomReport[lngRowAccountNumber1].RowNumber || grid.Row == modCustomReport.Statics.arrCustomReport[lngRowReceiptType].RowNumber)
				{
					// Collection Account & 'Receipt Type
					if ((keyAscii >= 48 && keyAscii <= 57) || (keyAscii == 8))
					{
						// do nothing
					}
					else
					{
						keyAscii = 0;
					}
				}
				else if (grid.Row == modCustomReport.Statics.arrCustomReport[lngRowReceiptNumber].RowNumber || grid.Row == modCustomReport.Statics.arrCustomReport[lngRowCheckNumber].RowNumber)
				{
					// Receipt Number + 'check number
					// If Col = 2 Then
					if ((keyAscii >= 48 && keyAscii <= 57) || (keyAscii == 8))
					{
						// numbers
					}
					else
					{
						keyAscii = 0;
					}
					// ElseIf KeyAscii = 27 Then
					// vsWhere.TextMatrix(Row, Col) = ""
					// End If
				}
				else if (grid.Row == modCustomReport.Statics.arrCustomReport[lngRowReceiptTotal].RowNumber)
				{
					// Receipt Amount
					if (grid.Col == 2)
					{
						if ((keyAscii >= 48 && keyAscii <= 57) || (keyAscii == 46) || (keyAscii == 8))
						{
							// numbers and "."
						}
						else
						{
							keyAscii = 0;
						}
					}
					else if (keyAscii == 27)
					{
						vsWhere.TextMatrix(grid.Row, grid.Col, "");
					}
				}
				if (grid.Row == modCustomReport.Statics.arrCustomReport[lngRowTenderedTotal].RowNumber)
				{
					// Tendered
					if (grid.Col == 2)
					{
						if ((keyAscii >= 48 && keyAscii <= 57) || (keyAscii == 46) || (keyAscii == 8))
						{
							// numbers and "."
						}
						else
						{
							keyAscii = 0;
						}
					}
					else if (keyAscii == 27)
					{
						vsWhere.TextMatrix(grid.Row, grid.Col, "");
					}
				}
				else if (grid.Row == modCustomReport.Statics.arrCustomReport[lngRowReference].RowNumber || grid.Row == modCustomReport.Statics.arrCustomReport[lngRowControl1].RowNumber || grid.Row == modCustomReport.Statics.arrCustomReport[lngRowControl2].RowNumber || grid.Row == modCustomReport.Statics.arrCustomReport[lngRowControl3].RowNumber || grid.Row == modCustomReport.Statics.arrCustomReport[lngRowName].RowNumber || grid.Row == modCustomReport.Statics.arrCustomReport[lngRowPaidBy].RowNumber)
				{
					// this is for the reference and the control fields
					switch (keyAscii)
					{
						case 39:
							{
								keyAscii = 0;
								break;
							}
					}
					//end switch
				}
				//} //end switch
			}
		}

		private void vsWhere_Leave(object sender, System.EventArgs e)
		{
			vsWhere.Row = 0;
			vsWhere.Col = 1;
		}

		private void vsWhere_RowColChange(object sender, System.EventArgs e)
		{
			// SET THE FORMAT FOR THE TYPE OF FIELD THAT THIS IS
			// IF COMBO...ADD THE LIST OF OPTIONS
			int lngCT = 0;
			// this is the index in arrCustomReport
			if (vsWhere.Row > 0)
			{
				lngCT = FCConvert.ToInt32(Math.Round(Conversion.Val(vsWhere.TextMatrix(vsWhere.Row, 4))));
				// this will hold the array index
				if (modCustomReport.Statics.arrCustomReport[lngCT].WhereType == modCustomReport.GRIDBDACCOUNT)
				{
                    // if it is an account row, then display an account box for the user to type into
                    txtAcct.LeftOriginal = vsWhere.LeftOriginal + vsWhere.ColWidth(0) + 50;
                    // this should put the account box in the second column
                    // this should put the account box on the correct row even if the grid is scrolled
                    txtAcct.TopOriginal = vsWhere.TopOriginal + ((vsWhere.Row - vsWhere.TopRow) * vsWhere.RowHeight(0)) + 40;
                    //FC:FINAL:BSE:#3574 textbox height should not be changed
                    //txtAcct.HeightOriginal = vsWhere.RowHeight(vsWhere.Row) - 50;
                    txtAcct.WidthOriginal = vsWhere.ColWidth(vsWhere.Col);
                    txtAcct.Tag = (System.Object)(vsWhere.Row);
                    // hold the current row
                    txtAcct.Visible = true;
                    txtAcct.BringToFront();
                    txtAcct.Focus();
                }
				else
				{
					// txtAcct.Visible = False
					if (Strings.Trim(modCustomReport.Statics.arrCustomReport[lngCT].ComboList) != "")
					{
						vsWhere.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						vsWhere.ComboList = modCustomReport.Statics.arrCustomReport[lngCT].ComboList;
					}
					// If strComboList(vsWhere.Row, 0) <> vbNullString Then
					// vsWhere.Editable = True
					// vsWhere.ComboList = strComboList(vsWhere.Row, 0)
					// End If
					if (vsWhere.Col == 2)
					{
						if (vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsWhere.Row, vsWhere.Col) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
                        {
                            //FC:FINAL:CHN: Move selected cell to next.
                            // Support.SendKeys("{tab}", false);
                            vsWhere.Col = 1;
                            vsWhere.Row = (vsWhere.Row + 1) % vsWhere.Rows;
						}
					}
				}
				if (vsWhere.Row == vsWhere.Rows - 1 && vsWhere.Col == 2)
				{
					vsWhere.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
				}
			}
		}

		private void vsWhere_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			// THIS WILL VALIDATE THE DATA THAT THE USER PUTS INTO THE WHERE
			// GIRD THAT WILL FILTER OUT RECORDS
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vsWhere.GetFlexRowIndex(e.RowIndex);
			int col = vsWhere.GetFlexColIndex(e.ColumnIndex);
			switch (FCConvert.ToInt32(modCustomReport.Statics.strWhereType[row]))
			{
				case modCustomReport.GRIDTEXT:
					{
						// ANYTHING GOES IF IT IS A TEXT FIELD
						break;
					}
				case modCustomReport.GRIDDATE:
					{
						// MAKE SURE THAT IT IS A VALID DATE
						if (Strings.Trim(vsWhere.EditText) == "/  /")
						{
							vsWhere.EditMask = string.Empty;
							vsWhere.EditText = string.Empty;
							vsWhere.TextMatrix(row, col, string.Empty);
							vsWhere.Refresh();
							return;
						}
						if (Strings.Trim(vsWhere.EditText).Length == 0)
						{
						}
						else if (Strings.Trim(vsWhere.EditText).Length != 10)
						{
							MessageBox.Show("Invalid date.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							e.Cancel = true;
							return;
						}
						if (!modCustomReport.IsValidDate(vsWhere.EditText))
						{
							e.Cancel = true;
							return;
						}
						break;
					}
				case modCustomReport.GRIDCOMBOIDTEXT:
					{
						// ASSIGN THE LIST TO THE COMBO IN THE GRID
						vsWhere.ComboList = modCustomReport.Statics.strComboList[row, 0];
						break;
					}
			}
			//end switch
		}

		public string BuildSortParameter()
		{
			string BuildSortParameter = "";
			// BUILD THE SORT CRITERIA FOR THE SQL STATEMENT
			string strSort;
			int intCounter;
			bool boolCheckedDate = false;
			// CLEAR OUT THE VARIABLES
			strSort = " ";
			boolCloseOutType = false;
			// GET THE FIELDS TO SORT BY
			for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
			{
				if (lstSort.Selected(intCounter))
				{
					if (strSort != " ")
						strSort += ", ";
					// strSort = strSort & CheckForAS(strFields(lstSort.ItemData(intCounter)), 1)
					if (lstSort.ItemData(intCounter) == lngRowCloseOut && !boolCheckedDate)
					{
						boolCloseOutType = true;
						boolCheckedDate = true;
					}
					if (lstSort.ItemData(intCounter) == lngRowReceiptDate && !boolCheckedDate)
					{
						boolCloseOutType = false;
						boolCheckedDate = true;
					}
					if (lstSort.ItemData(intCounter) == lngRowName)
					{
						// this is only for the name
						switch (FCConvert.ToInt32(Strings.Mid(modCustomReport.Statics.strBinaryReportType, 1, 1)))
						{
							case 0:
								{
									// let it ride
									strSort += "NewArchive.Name";
									break;
								}
							case 1:
								{
									strSort += "OldArchive.Name";
									break;
								}
							case 2:
								{
									strSort += "AllArchive.Name";
									// kk02022017 trocrs-45   "OldArchive.Name"
									break;
								}
						}
						//end switch
					}
					else if (lstSort.ItemData(intCounter) == lngRowReceiptDate)
					{
						// this is only for the name
						// strSort = strSort & "Format(RecordedTransactionDate,'MM/dd/yy')"
						// MAL@20080331: Correct sort field name
						// Tracker Reference: 12966
						// strSort = strSort & "Format('Date','MM/dd/yy')"
						strSort += "ActualSystemDate";
					}
					else
					{
						strSort += modCustomReport.Statics.arrCustomReport[lstSort.ItemData(intCounter)].DatabaseFieldName;
					}
				}
			}
			if (Strings.Trim(strSort) == "")
				strSort = " ReceiptNumber";
			// set the default if none are choosen
			// IF THERE WERE SOME FIELDS TO SORT BY THEN APPEND THEM TO THE
			// PREVIOUSLY CREATED SQL STATEMENT
			BuildSortParameter = strSort;
			return BuildSortParameter;
		}

		public string BuildWhereParameter()
		{
			string BuildWhereParameter = "";
			// BUILD THE WHERE CLAUSE TO ADD TO THE SQL STATEMENT
			string strWhere;
			int intCounter;
			clsDRWrapper rsCheck = new clsDRWrapper();
			string strCheck = "";
			string strTemp = "";
			// CLEAR THE VARIABLES
			strWhere = " ";
			vsWhere.Select(0, 0);
			modCustomReport.Statics.boolRSMVSummary = false;
			for (intCounter = 0; intCounter <= MAX_VARIABLES; intCounter++)
			{
				if (modCustomReport.Statics.arrCustomReport[intCounter].RowNumber >= 0)
				{
					// this will only check the variables that have a row associated with it
					// NEED TO KNOW WHAT TYPE OF WHERE CLAUSE TO APPEND BY
					switch (modCustomReport.Statics.arrCustomReport[intCounter].WhereType)
					{
						case modCustomReport.GRIDTEXT:
							{
								// THIS IS A TEXT FIELD IN THE DATABASE SO WE NEED TO
								// MAKE SURE THAT THIS PIECE OF THE WHERE CLAUSE HAS SINGLE
								// QUOTES AROUND IT
								if (intCounter == lngRowCheckNumber)
								{
									// skip this and check it at runtime
									if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1)) != string.Empty)
									{
										// this will only work if something is entered into at least the first column
										// kk01272017 trocls-56  Correct JOIN ON clause - change Receipt.ID to Receipt.ReceiptKey
										rsCheck.OpenRecordset("SELECT Receipt.ReceiptKey FROM Receipt INNER JOIN CheckMaster ON Receipt.ReceiptKey = CheckMaster.ReceiptNumber WHERE CheckNumber >= '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1) + "    ' AND CheckNumber <= '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1) + "ZZZZ'");
										if (!rsCheck.EndOfFile())
										{
											while (!rsCheck.EndOfFile())
											{
												strCheck += rsCheck.Get_Fields_Int32("ReceiptKey") + ",";
												rsCheck.MoveNext();
											}
											strCheck = Strings.Left(strCheck, strCheck.Length - 1);
											strCheck = "ReceiptKey IN (" + strCheck + ")";
										}
										else
										{
											strCheck = "ReceiptKey = 0";
										}
										if (strWhere != " ")
										{
											strWhere += " AND " + strCheck;
										}
										else
										{
											strWhere += strCheck;
										}
									}
								}
								else
								{
									if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1)) != string.Empty)
									{
										if (strWhere != " ")
											strWhere += " AND ";
										// kk09212017 trocrs-59  Changed Name and PaidBy to combo and text to allow choice of Begins With or Contains text
										// If intCounter = lngRowName Then
										// Select Case Mid$(strBinaryReportType, 1, 1)
										// Case 0
										// let it ride
										// strWhere = strWhere & "(NewArchive.Name >= '" & vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, arrCustomReport(intCounter).RowNumber, 1) & "    ' AND NewArchive.Name <= '" & vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, arrCustomReport(intCounter).RowNumber, 1) & "zzzz')"
										// Case 1
										// strWhere = strWhere & "(OldArchive.Name >= '" & vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, arrCustomReport(intCounter).RowNumber, 1) & "    ' AND OldArchive.Name <= '" & vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, arrCustomReport(intCounter).RowNumber, 1) & "zzzz')"
										// Case 2       'kk02022017 trocrs-45  Changed OldArchive.Name to AllArchive.Name
										// strWhere = strWhere & "(AllArchive.Name >= '" & vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, arrCustomReport(intCounter).RowNumber, 1) & "    ' AND AllArchive.Name <= '" & vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, arrCustomReport(intCounter).RowNumber, 1) & "zzzz')"
										// End Select
										// Else
										strWhere += "(" + modCustomReport.Statics.arrCustomReport[intCounter].DatabaseFieldName + " >= '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1) + "    ' AND " + modCustomReport.Statics.arrCustomReport[intCounter].DatabaseFieldName + " <= '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1) + "zzzz')";
										// End If
									}
								}
								break;
							}
						case modCustomReport.GRIDDATE:
							{
								// THIS IS A DATE FIELD IN THE DATABASE SO WE NEED TO
								// MAKE SURE THAT THIS PIECE OF THE WHERE CLAUSE HAS
								// THE POUND SYMBOL WRAPPED AROUND IT
								if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1)) != string.Empty)
								{
									if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 2)) != string.Empty)
									{
										// THE USER HAS FILLED IN BOTH FIELDS FOR THIS DATE
										// FIELD SO WE NOW HAVE A DATE RANGE
										if (strWhere != " ")
											strWhere += " AND ";
										strWhere += modCustomReport.Statics.arrCustomReport[intCounter].DatabaseFieldName + " BETWEEN '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1) + " 12:00 AM' AND '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 2) + " 11:59 PM'";
									}
									else
									{
										// ONE ONE FIELD WAS FILLED IN FOR THIS FIELD
										// SO WE HAVE THIS BEING AN EQUAL TO CLAUSE
										if (strWhere != " ")
											strWhere += " AND ";
										strWhere += modCustomReport.Statics.arrCustomReport[intCounter].DatabaseFieldName + " BETWEEN '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1) + " 12:00 AM' AND '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1) + " 11:59 PM'";
									}
								}
								break;
							}
						case modCustomReport.GRIDCOMBOIDTEXT:
							{
								// THIS IS OF A TYPE COMBO SO THE DATA IN THE DATABASE
								// IS THE ***ID*** NUMBER FROM ANOTHER TABLE
								if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1)) != string.Empty)
								{
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += modCustomReport.Statics.arrCustomReport[intCounter].DatabaseFieldName + " = '" + vsWhere.TextMatrix(modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 3) + "'";
								}
								break;
							}
						case modCustomReport.GRIDCOMBOIDNUM:
							{
								if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1)) != string.Empty)
								{
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += modCustomReport.Statics.arrCustomReport[intCounter].DatabaseFieldName + " = " + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1);
								}
								break;
							}
						case modCustomReport.GRIDCOMBOTEXT:
							{
								if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1)) != string.Empty)
								{
									if (intCounter == lngRowMVType)
									{
										// this is the MVR3 line
										if (strWhere != " ")
											strWhere += " AND ";
										if (Strings.UCase(Strings.Left(vsWhere.TextMatrix(modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1), 3)) == "ALL")
										{
											strWhere += "(ReceiptType = 99)";
											modCustomReport.Statics.boolRSMVSummary = true;
										}
										else
										{
											strWhere += "(ReceiptType = 99 AND Comment = '" + vsWhere.TextMatrix(modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1) + "')";
										}
									}
									else if (intCounter == lngRowDateToShow)
									{
										// check to see which answer the user gave
										boolShowActualSysDate = FCConvert.CBool(Strings.Left(vsWhere.TextMatrix(modCustomReport.Statics.arrCustomReport[lngRowDateToShow].RowNumber, 1), 6) == "Actual");
									}
									else if (intCounter == lngRowBank)
									{
										if (Strings.UCase(Strings.Trim(vsWhere.TextMatrix(modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1))) != "")
										{
											lngSpecificBank = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Right(Strings.Trim(vsWhere.TextMatrix(modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1)), 5))));
										}
										else
										{
											lngSpecificBank = 0;
										}
									}
									else if (intCounter == lngRowPaymentType)
									{
										if (strWhere != " ")
											strWhere += " AND ";
										if (Strings.UCase(vsWhere.TextMatrix(modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1)) == "CASH")
										{
											strWhere += "CashAmount <> 0";
										}
										else if (Strings.UCase(vsWhere.TextMatrix(modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1)) == "CHECK")
										{
											strWhere += "CheckAmount <> 0";
										}
										else if (Strings.UCase(vsWhere.TextMatrix(modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1)) == "CREDIT/DEBIT CARD")
										{
											strWhere += "CardAmount <> 0";
										}
										else
										{
											if (Strings.Right(strWhere, 4) == "AND ")
											{
												strWhere = Strings.Left(strWhere, strWhere.Length - 4);
											}
										}
									}
									else
									{
										if (strWhere != " ")
											strWhere += " AND ";
										if (intCounter != lngRowBank)
										{
											// 14 ?
											strWhere += modCustomReport.Statics.arrCustomReport[intCounter].DatabaseFieldName + " = '" + vsWhere.TextMatrix(modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1) + "'";
										}
										else
										{
											strWhere += modCustomReport.Statics.arrCustomReport[intCounter].DatabaseFieldName + " = '" + Strings.Left(vsWhere.TextMatrix(modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1), 1) + "'";
										}
									}
								}
								break;
							}
						case modCustomReport.GRIDNUMRANGE:
							{
								if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1)) != string.Empty)
								{
									// this will only work if something is entered into at least the first column
									if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 2)) != string.Empty)
									{
										if (strWhere != " ")
											strWhere += " AND ";
										strWhere += modCustomReport.Statics.arrCustomReport[intCounter].DatabaseFieldName + " >= " + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1) + " AND " + modCustomReport.Statics.arrCustomReport[intCounter].DatabaseFieldName + " <= " + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 2);
									}
									else
									{
										if (strWhere != " ")
											strWhere += " AND ";
										strWhere += modCustomReport.Statics.arrCustomReport[intCounter].DatabaseFieldName + " = " + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1);
									}
								}
								break;
							}
						case modCustomReport.GRIDTEXTRANGE:
							{
								// THIS IS A TEXT FIELD IN THE DATABASE SO WE NEED TO
								// MAKE SURE THAT THIS PIECE OF THE WHERE CLAUSE HAS SINGLE
								// QUOTES AROUND IT
								if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1)) != string.Empty)
								{
									if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 2)) != string.Empty)
									{
										if (strWhere != " ")
											strWhere += " AND ";
										strWhere += modCustomReport.Statics.arrCustomReport[intCounter].DatabaseFieldName + " >= '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1) + "' AND " + modCustomReport.Statics.arrCustomReport[intCounter].DatabaseFieldName + " <= '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 2) + "'";
									}
									else
									{
										if (strWhere != " ")
											strWhere += " AND ";
										strWhere += modCustomReport.Statics.arrCustomReport[intCounter].DatabaseFieldName + " = '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1) + "'";
									}
								}
								break;
							}
						case modCustomReport.GRIDCOMBOANDNUM:
							{
								// THIS HAS A COMBO BOX AND NUMERIC FIELD
								if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1)) != string.Empty && FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 2)) != string.Empty)
								{
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += modCustomReport.Statics.arrCustomReport[intCounter].DatabaseFieldName + " " + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1) + " " + vsWhere.TextMatrix(modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 2);
								}
								break;
							}
						case modCustomReport.GRIDCOMBOANDTEXT:
							{
								// THIS HAS A COMBO BOX AND TEXT FIELD
								// kk09202017 trocrs-59  Change Name and PaidBy queries to allow search names that begin with or contain the search criteria, instead of just beginning with
								if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1)) != string.Empty && FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 2)) != string.Empty)
								{
									if (strWhere != " ")
										strWhere += " AND ";
									if (intCounter == lngRowName)
									{
										switch (FCConvert.ToInt32(Strings.Mid(modCustomReport.Statics.strBinaryReportType, 1, 1)))
										{
											case 0:
												{
													// Current Year Only
													if (Strings.UCase(FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1))) == "BEGINS WITH")
													{
														strWhere += "NewArchive.Name LIKE '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 2) + "%'";
													}
													else
													{
														strWhere += "NewArchive.Name LIKE '%" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 2) + "%'";
													}
													break;
												}
											case 1:
												{
													// Archive Year Only
													if (Strings.UCase(FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1))) == "BEGINS WITH")
													{
														strWhere += "OldArchive.Name LIKE '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 2) + "%'";
													}
													else
													{
														strWhere += "OldArchive.Name LIKE '%" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 2) + "%'";
													}
													break;
												}
											case 2:
												{
													// kk02022017 trocrs-45  Changed OldArchive.Name to AllArchive.Name
													if (Strings.UCase(FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1))) == "BEGINS WITH")
													{
														strWhere += "AllArchive.Name LIKE '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 2) + "%'";
													}
													else
													{
														strWhere += "AllArchive.Name LIKE '%" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 2) + "%'";
													}
													break;
												}
										}
										//end switch
									}
									else if (intCounter == lngRowPaidBy)
									{
										if (Strings.UCase(FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1))) == "BEGINS WITH")
										{
											strWhere += "PaidBy LIKE '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 2) + "%'";
										}
										else
										{
											strWhere += "PaidBy LIKE '%" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 2) + "%'";
										}
									}
								}
								break;
							}
						case modCustomReport.GRIDBDACCOUNT:
							{
								// THIS HAS A COMBO BOX AND TEXT FIELD, THE COMBO HAVING E,R,G,M
								if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, lngRowAccountNumber1, 1)) != string.Empty)
								{
									// arrCustomReport(intCounter).RowNumber
									if (strWhere != " ")
										strWhere += " AND ";
									// strWhere = strWhere & arrCustomReport(intCounter).DatabaseFieldName & " LIKE '" & vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, arrCustomReport(intCounter).RowNumber, 1) & " " & vsWhere.TextMatrix(intCounter, 2) & "*'"
									strWhere += "(Account1 = '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, lngRowAccountNumber1, 1) + "'";
									strWhere += " OR ConvenienceFeeGLAccount = '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, lngRowAccountNumber1, 1) + "'";
									strWhere += " OR Account2 = '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, lngRowAccountNumber1, 1) + "'";
									strWhere += " OR Account3 = '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, lngRowAccountNumber1, 1) + "'";
									strWhere += " OR Account4 = '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, lngRowAccountNumber1, 1) + "'";
									strWhere += " OR Account5 = '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, lngRowAccountNumber1, 1) + "'";
									strWhere += " OR Account6 = '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, lngRowAccountNumber1, 1) + "')";
								}
								break;
							}
						case modCustomReport.GRIDLISTTEXT:
							{
								// ALLOWS A COMMA DELIMITED STRING OF CONSTRAINTS
								break;
							}
						case modCustomReport.GRIDCOMBORANGE:
							{
								// ALLOWS TWO COMBO BOXES OF TYPE STRINGS
								if (intCounter == lngRowCloseOut)
								{
									if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1)) != string.Empty)
									{
										if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 2)) != string.Empty)
										{
											if (strWhere != " ")
												strWhere += " AND ";
											strWhere += modCustomReport.Statics.arrCustomReport[intCounter].DatabaseFieldName + " >= " + Strings.Right(FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1)), 7) + " AND " + modCustomReport.Statics.arrCustomReport[intCounter].DatabaseFieldName + " <= " + Strings.Right(FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 2)), 7);
										}
										else
										{
											if (strWhere != " ")
												strWhere += " AND ";
											strWhere += modCustomReport.Statics.arrCustomReport[intCounter].DatabaseFieldName + " = " + Strings.Right(FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1)), 7);
										}
									}
								}
								else
								{
									if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1)) != string.Empty)
									{
										if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 2)) != string.Empty)
										{
											if (strWhere != " ")
												strWhere += " AND ";
											strWhere += modCustomReport.Statics.arrCustomReport[intCounter].DatabaseFieldName + " >= '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1) + "' AND " + modCustomReport.Statics.arrCustomReport[intCounter].DatabaseFieldName + " <= '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 2) + "'";
										}
										else
										{
											if (strWhere != " ")
												strWhere += " AND ";
											strWhere += modCustomReport.Statics.arrCustomReport[intCounter].DatabaseFieldName + " = '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1) + "'";
										}
									}
								}
								break;
							}
						case modCustomReport.GRIDCOMBONUMRANGE:
							{
								// ALLOWS TWO COMBO BOXES OF A NUMERIC TYPE
								if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1)) != string.Empty)
								{
									// this will only work if something is entered into at least the first column
									if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 2)) != string.Empty)
									{
										if (strWhere != " ")
											strWhere += " AND ";
										strWhere += modCustomReport.Statics.arrCustomReport[intCounter].DatabaseFieldName + " >= " + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1) + " AND " + modCustomReport.Statics.arrCustomReport[intCounter].DatabaseFieldName + " <= " + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 2);
									}
									else
									{
										if (strWhere != " ")
											strWhere += " AND ";
										strWhere += modCustomReport.Statics.arrCustomReport[intCounter].DatabaseFieldName + " = " + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, modCustomReport.Statics.arrCustomReport[intCounter].RowNumber, 1);
									}
								}
								break;
							}
					}
					//end switch
				}
			}
			if (lstTypes.SelectedItems.Any())
			{
				for (intCounter = 0; intCounter <= lstTypes.Items.Count - 1; intCounter++)
				{
					if (lstTypes.Selected(intCounter))
					{
						strTemp += FCConvert.ToString(lstTypes.ItemData(intCounter)) + ",";
					}
				}
				if (strTemp.Length > 1)
				{
					if (Strings.Trim(strWhere) != "")
					{
						strWhere += " AND ReceiptType IN (" + Strings.Left(strTemp, strTemp.Length - 1) + ")";
					}
					else
					{
						strWhere = " ReceiptType IN (" + Strings.Left(strTemp, strTemp.Length - 1) + ")";
					}
				}
			}
			// GET THE FIELDS TO FILTER BY
			// For intCounter = 0 To vsWhere.rows - 1
			// If intCounter > MAX_VARIABLES Then Exit For
			// NEED TO KNOW WHAT TYPE OF WHERE CLAUSE TO APPEND BY
			// Select Case strWhereType(intCounter)
			// Case GRIDTEXT
			// THIS IS A TEXT FIELD IN THE DATABASE SO WE NEED TO
			// MAKE SURE THAT THIS PIECE OF THE WHERE CLAUSE HAS SINGLE
			// QUOTES AROUND IT
			// If vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) <> vbNullString Then
			// If strWhere <> " " Then strWhere = strWhere & " AND "
			// strWhere = strWhere & strFieldNames(intCounter) & " = '" & vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) & "'"
			// End If
			// 
			// Case GRIDDATE
			// THIS IS A DATE FIELD IN THE DATABASE SO WE NEED TO
			// MAKE SURE THAT THIS PIECE OF THE WHERE CLAUSE HAS
			// THE POUND SYMBOL WRAPPED AROUND IT
			// If vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) <> vbNullString Then
			// If vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2) <> vbNullString Then
			// THE USER HAS FILLED IN BOTH FIELDS FOR THIS DATE
			// FIELD SO WE NOW HAVE A DATE RANGE
			// If strWhere <> " " Then strWhere = strWhere & " AND "
			// strWhere = strWhere & Trim(strFieldNames(intCounter)) & " BETWEEN #" & vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) & "# AND #" & vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2) & " 11:59 PM#"
			// Else
			// ONE ONE FIELD WAS FILLED IN FOR THIS FIELD
			// SO WE HAVE THIS BEING AN EQUAL TO CLAUSE
			// If strWhere <> " " Then strWhere = strWhere & " AND "
			// strWhere = strWhere & Trim(strFieldNames(intCounter)) & " = #" & vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) & "#"
			// End If
			// End If
			// 
			// Case GRIDCOMBOIDTEXT
			// THIS IS OF A TYPE COMBO SO THE DATA IN THE DATABASE
			// IS THE ***ID*** NUMBER FROM ANOTHER TABLE
			// If vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) <> vbNullString Then
			// If strWhere <> " " Then strWhere = strWhere & " AND "
			// strWhere = strWhere & Trim(strFieldNames(intCounter)) & " = '" & vsWhere.TextMatrix(intCounter, 3) & "'"
			// End If
			// 
			// Case GRIDCOMBOIDNUM
			// If vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) <> vbNullString Then
			// If strWhere <> " " Then strWhere = strWhere & " AND "
			// strWhere = strWhere & Trim(strFieldNames(intCounter)) & " = " & vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)
			// End If
			// 
			// Case GRIDCOMBOTEXT
			// If vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) <> vbNullString Then
			// If strWhere <> " " Then strWhere = strWhere & " AND "
			// If intCounter <> 14 Then
			// strWhere = strWhere & Trim(strFieldNames(intCounter)) & " = '" & vsWhere.TextMatrix(intCounter, 1) & "'"
			// Else
			// strWhere = strWhere & Trim(strFieldNames(intCounter)) & " = '" & Left$(vsWhere.TextMatrix(intCounter, 1), 1) & "'"
			// End If
			// End If
			// 
			// Case GRIDNUMRANGE
			// If vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) <> vbNullString Then
			// If vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2) <> vbNullString Then
			// If strWhere <> " " Then strWhere = strWhere & " AND "
			// strWhere = strWhere & strFieldNames(intCounter) & " >= " & vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) & " AND " & strFieldNames(intCounter) & " <= " & vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)
			// Else
			// If strWhere <> " " Then strWhere = strWhere & " AND "
			// strWhere = strWhere & strFieldNames(intCounter) & " = " & vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)
			// End If
			// End If
			// 
			// Case GRIDTEXTRANGE
			// THIS IS A TEXT FIELD IN THE DATABASE SO WE NEED TO
			// MAKE SURE THAT THIS PIECE OF THE WHERE CLAUSE HAS SINGLE
			// QUOTES AROUND IT
			// If vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) <> vbNullString Then
			// If vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2) <> vbNullString Then
			// If strWhere <> " " Then strWhere = strWhere & " AND "
			// strWhere = strWhere & strFieldNames(intCounter) & " >= '" & vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) & "' AND " & strFieldNames(intCounter) & " <= '" & vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2) & "'"
			// Else
			// If strWhere <> " " Then strWhere = strWhere & " AND "
			// strWhere = strWhere & strFieldNames(intCounter) & " = '" & vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) & "'"
			// End If
			// End If
			// Case GRIDCOMBOANDNUM    'THIS HAS A COMBO BOX AND NUMERIC FIELD
			// If vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) <> vbNullString And vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2) <> vbNullString Then
			// If strWhere <> " " Then strWhere = strWhere & " AND "
			// strWhere = strWhere & Trim(strFieldNames(intCounter)) & " " & vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) & " " & vsWhere.TextMatrix(intCounter, 2)
			// End If
			// Case GRIDCOMBOANDTEXT   'THIS HAS A COMBO BOX AND TEXT FIELD
			// 
			// Case GRIDBDACCOUNT      'THIS HAS A COMBO BOX AND TEXT FIELD, THE COMBO HAVING E,R,G
			// If vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) <> vbNullString And vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2) <> vbNullString Then
			// If strWhere <> " " Then strWhere = strWhere & " AND "
			// strWhere = strWhere & Trim(strFieldNames(intCounter)) & " LIKE '" & vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) & " " & vsWhere.TextMatrix(intCounter, 2) & "*'"
			// End If
			// Case GRIDLISTTEXT       'ALLOWS A COMMA DELIMITED STRING OF CONSTRAINTS
			// End Select
			// Next
			// IF SOME WHERE PARAMETERS WHERE CHOSEN THEN APPEND THIS TO THE SQL STATEMENT
			BuildWhereParameter = strWhere;
			return BuildWhereParameter;
		}

		private void CheckForInvalidSortParameters()
		{
			// this will deselect any invalid sort parameter
			int intCT;
			for (intCT = 0; intCT <= lstSort.Items.Count - 1; intCT++)
			{
				if (Strings.InStr(1, modCustomReport.Statics.strFields[lstSort.ItemData(intCT)], "SUM(", CompareConstants.vbBinaryCompare) != 0)
				{
					boolNoClick = true;
					lstSort.SetSelected(intCT, false);
				}
			}
		}

		private void ResizeGrid()
		{
			int wid = 0;
			// this will resize the grid at the bottom left of the screen
			wid = vsWhere.WidthOriginal;
			vsWhere.ColWidth(0, FCConvert.ToInt32(wid * 0.39));
			vsWhere.ColWidth(1, FCConvert.ToInt32(wid * 0.28));
			vsWhere.ColWidth(2, FCConvert.ToInt32(wid * 0.28));
			vsWhere.ExtendLastCol = true;
			vsWhere.TopOriginal = 500;
			if (vsWhere.Rows > 0)
			{
				vsWhere.HeightOriginal = (vsWhere.Rows * vsWhere.RowHeight(0)) + 70;
				if (vsWhere.HeightOriginal > fraWhere.HeightOriginal - 600)
				{
					vsWhere.HeightOriginal = fraWhere.HeightOriginal - 600;
					vsWhere.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
					vsWhere.ColWidth(0, FCConvert.ToInt32(wid * 0.36));
				}
				else
				{
					vsWhere.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				}
			}
			FormatQGrid();
		}

		private void AddQuestionsToGrid()
		{
			// this is where you will fill the array of questions and thier answer type
			arrQuestions[1].Question = "Select the time period to search.";
			// this is the question that you want to ask
			arrQuestions[1].Type = modCustomReport.GRIDCOMBOTEXT;
			// this is how you want the grid to show it
			arrQuestions[1].AnswerString = "#0;Current Year|#1;Archive Year|#2;Both Years";
			arrQuestions[1].Instructions = "";
			arrQuestions[2].Question = "Choose the level of detail for the report.";
			arrQuestions[2].Type = modCustomReport.GRIDCOMBOTEXT;
			arrQuestions[2].AnswerString = "#0;Summary|#1;Detail|#2;Both";
			arrQuestions[2].Instructions = "Select Detail to show each line item of the receipt, Summary to only show the summary.";
			arrQuestions[3].Question = "Choose the level of detail for the receipts.";
			arrQuestions[3].Type = modCustomReport.GRIDCOMBOTEXT;
			arrQuestions[3].AnswerString = "#0;Summary|#1;Detail";
			arrQuestions[3].Instructions = "To show line items for each receipt, select Detail. Select Summary Breakdown to show each receipt in the summary with the breakdown of fees.";
			// arrQuestions(3).Instructions = "To show line items for each receipt, select detail. Select Summary to show each receipt in totals."
			// arrQuestions(3).Instructions = "To display each line item of the receipt, select Detail. To just show receipt totals, select Summary."
			// arrQuestions(3).Instructions = "Only available if the report detail is set to detail.  Select 'Detail' to show each line item of the receipt, 'Summary' to show the receipt totals."
			arrQuestions[4].Question = "Show tendered amounts?";
			arrQuestions[4].Type = modCustomReport.GRIDCOMBOTEXT;
			arrQuestions[4].AnswerString = "#0;Yes|#1;No";
			arrQuestions[4].Instructions = "";
			arrQuestions[5].Question = "Summarize by:";
			arrQuestions[5].Type = modCustomReport.GRIDCOMBOTEXT;
			arrQuestions[5].AnswerString = "#0;Type|#1;Date|#2;Teller|#3;Type and Date|#4;Type and Teller|#5;Date and Teller";
			arrQuestions[5].Instructions = "";
			arrQuestions[6].Question = "Choose the level of detail for the summary.";
			arrQuestions[6].Type = modCustomReport.GRIDCOMBOTEXT;
			arrQuestions[6].AnswerString = "#0;Summary|#1;Detail";
			arrQuestions[6].Instructions = "";
		}

		private void FormatQGrid()
		{
			// this will setup the grid's col widths colors and other settings
			int wid = 0;
			wid = vsSetup.WidthOriginal;
			vsSetup.ColWidth(0, FCConvert.ToInt32(wid * 0.75));
			vsSetup.ColWidth(1, FCConvert.ToInt32(wid * 0.2));
			vsSetup.ColWidth(2, 0);
			vsSetup.ColHidden(2, true);
			vsSetup.RowHeight(0, 0);
			vsSetup.RowHidden(0, true);
			vsSetup.ExtendLastCol = true;
			//if (vsSetup.Rows > 1)
			//{
			//    vsSetup.HeightOriginal = ((vsSetup.Rows - 1) * vsSetup.RowHeight(1)) + 70;
			//}
		}

		private void FillQGrid()
		{
			// this sub will fill the questions into the grid and adjust it properly
			int lngNum;
			lngNum = 1;
			while (!(arrQuestions[lngNum].Type == 0))
			{
				// add row
				vsSetup.AddItem("");
				// fill the row with the question
				vsSetup.TextMatrix(lngNum, 0, arrQuestions[lngNum].Question);
				lngNum += 1;
			}
			//if (vsSetup.Rows > 1)
			//{
			//    vsSetup.HeightOriginal = (vsSetup.RowHeight(1) * (vsSetup.Rows - 1)) + 75;
			//}
			// setup default answers
			// this will set the textmatrix to this text is shown to the user
			vsSetup.TextMatrix(1, 1, "Current Year");
			vsSetup.TextMatrix(2, 1, "Detail");
			vsSetup.TextMatrix(3, 1, "Summary");
			vsSetup.TextMatrix(4, 1, "No");
			vsSetup.TextMatrix(5, 1, "Type");
			vsSetup.TextMatrix(6, 1, "Summary");
			// set the answers as well, just like the user had clicked on each one
			arrQuestions[1].answer = 0;
			arrQuestions[2].answer = 1;
			arrQuestions[3].answer = 0;
			arrQuestions[4].answer = 1;
			arrQuestions[5].answer = 0;
			arrQuestions[6].answer = 0;
			vsSetup.Focus();
		}

		private bool SaveAnswers_2(bool boolReset)
		{
			return SaveAnswers(boolReset);
		}

		private bool SaveAnswers(bool boolReset = true)
		{
			bool SaveAnswers = false;
			// this will take the answers given, validate them, find out what report
			// the user wants and show the frmCustomReport screen
			string strTypeOfReport;
			int intQuestions;
			vsSetup.Select(0, 1);
			SaveAnswers = true;
			if (arrQuestions[1].answer >= 0)
			{
				// arrQuestions(1).Question = "Enter the search time period criteria."     'this is the question that you want to ask
				// arrQuestions(1).Type = GRIDCOMBOTEXT                                    'this is how you want the grid to show it
				// arrQuestions(1).AnswerString = "#0;Current Year|#1;Last Year|#2;Both Years"
			}
			else
			{
				MessageBox.Show("Please choose an answer for all of the questions.", "Missing Answers", MessageBoxButtons.OK, MessageBoxIcon.Information);
				SaveAnswers = false;
				return SaveAnswers;
			}
			if (arrQuestions[2].answer >= 0)
			{
				// arrQuestions(2).Question = "Choose the level of detail for the payments."
				// arrQuestions(2).Type = GRIDCOMBOTEXT
				// arrQuestions(2).AnswerString = "#0;Summary|#1;Detail|#2;Both"
			}
			else
			{
				MessageBox.Show("Please choose an answer for all of the questions.", "Missing Answers", MessageBoxButtons.OK, MessageBoxIcon.Information);
				SaveAnswers = false;
				return SaveAnswers;
			}
			if (arrQuestions[3].answer >= 0)
			{
				// arrQuestions(3).Question = "Choose the level of detail for the receipts."
				// arrQuestions(3).Type = GRIDCOMBOTEXT
				// arrQuestions(3).AnswerString = "#0;Summary|#1;Detail"
			}
			else if (arrQuestions[2].answer != 0)
			{
				MessageBox.Show("Please choose an answer for all of the questions.", "Missing Answers", MessageBoxButtons.OK, MessageBoxIcon.Information);
				SaveAnswers = false;
				return SaveAnswers;
			}
			if (arrQuestions[4].answer >= 0)
			{
				// arrQuestions(4).Question = "Show Tendered Amounts?"
				// arrQuestions(4).Type = GRIDCOMBOTEXT
				// arrQuestions(4).AnswerString = "#0;Yes|#1;No"
			}
			else if (arrQuestions[2].answer != 0)
			{
				MessageBox.Show("Please choose an answer for all of the questions.", "Missing Answers", MessageBoxButtons.OK, MessageBoxIcon.Information);
				SaveAnswers = false;
				return SaveAnswers;
			}
			if (arrQuestions[5].answer >= 0)
			{
				// arrQuestions(5).Question = "Summarize by:"
				// arrQuestions(5).Type = GRIDCOMBOTEXT
				// arrQuestions(5).AnswerString = "#0;Type|#1;Date|#2;Teller"
			}
			else
			{
				MessageBox.Show("Please choose an answer for all of the questions.", "Missing Answers", MessageBoxButtons.OK, MessageBoxIcon.Information);
				SaveAnswers = false;
				return SaveAnswers;
			}
			if (arrQuestions[6].answer >= 0)
			{
				// arrQuestions(6).Question = "Choose level of "
				// arrQuestions(6).Type = GRIDCOMBOTEXT
				// arrQuestions(6).AnswerString = "#0;Summary|#1;Detail"
			}
			else
			{
				MessageBox.Show("Please choose an answer for all of the questions.", "Missing Answers", MessageBoxButtons.OK, MessageBoxIcon.Information);
				SaveAnswers = false;
				return SaveAnswers;
			}
			// this will send a code to the custom report to show all of the answers
			// THIS WILL NEED TO BE CHANGED IF YOU ARE NOT ONLY USING COMBO BOXES
			strTypeOfReport = "";
			for (intQuestions = 1; intQuestions <= vsSetup.Rows - 1; intQuestions++)
			{
				if (arrQuestions[intQuestions].answer == -1)
				{
					strTypeOfReport += "0";
				}
				else
				{
					strTypeOfReport += FCConvert.ToString(arrQuestions[intQuestions].answer);
				}
			}
			// this will be different depending on the answers to the questions
			if (boolReset)
				modCustomReport.SetFormFieldCaptions(frmCustomReport.InstancePtr, strTypeOfReport);
			return SaveAnswers;
		}

		private void SetupGrid()
		{
			// this sub will color the correct cells and then set all of the array answers to -1
			int intCT;
			// check the type of each question and color the grid accordingly
			for (intCT = 1; intCT <= vsSetup.Rows - 1; intCT++)
			{
				switch (arrQuestions[intCT].Type)
				{
					case modCustomReport.GRIDTEXT:
						{
							vsSetup.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCT, 3, vsSetup.BackColorFixed);
							break;
						}
					case modCustomReport.GRIDDATE:
						{
							break;
						}
					case modCustomReport.GRIDCOMBOIDTEXT:
					case modCustomReport.GRIDCOMBOIDNUM:
					case modCustomReport.GRIDCOMBOTEXT:
						{
							vsSetup.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCT, 2, vsSetup.BackColorFixed);
							break;
						}
					case modCustomReport.GRIDCOMBOANDNUM:
					case modCustomReport.GRIDCOMBOANDTEXT:
					case modCustomReport.GRIDCOMBORANGE:
						{
							break;
						}
					case modCustomReport.GRIDBDACCOUNT:
						{
							// FIX THIS!!!
							break;
						}
					case modCustomReport.GRIDLISTTEXT:
						{
							// FIX THIS!!!
							break;
						}
				}
				//end switch
			}
			// this will set all of the answers to -1 so that the user will have to choose which answer they want
			// For intCT = 1 To UBound(arrQuestions)
			// arrQuestions(intCT).Answer = -1
			// Next
		}

		private void ShowDetail(bool boolDetail)
		{
			if (boolDetail)
			{
				if (boolSummary)
				{
					// yes...let these questions go
					vsSetup.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 1, 4, 1, Color.White);
					boolSummary = false;
				}
			}
			//FC:FINAL:MSH - can't apply '==' operator to int and Color (same with i.issue #1442)
			else if (vsSetup.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 1) != ColorTranslator.ToOle(SystemColors.GrayText))
			{
				if (!boolSummary)
				{
					// no...grey these questions out
					vsSetup.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 1, 4, 1, SystemColors.GrayText);
					vsSetup.Select(3, 1);
					vsSetup.ComboIndex = 0;
					vsSetup.Select(4, 1);
					vsSetup.ComboIndex = 0;
					vsSetup.Select(3, 1);
					boolSummary = true;
				}
			}
		}

		private void ShowTypeSummary_2(bool boolType)
		{
			ShowTypeSummary(ref boolType);
		}

		private void ShowTypeSummary(ref bool boolType)
		{
			if (boolType && Strings.Left(vsSetup.TextMatrix(2, 1), 1) != "D")
			{
				// yes...let these questions go
				vsSetup.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 6, 1, 6, 1, Color.White);
			}
			else
			{
				// no...grey these questions out
				vsSetup.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 6, 1, 6, 1, SystemColors.GrayText);
			}
		}
		// vbPorter upgrade warning: intRow As short	OnWriteFCConvert.ToInt32(
		private string GetWhereConstraint(ref int intRow)
		{
			string GetWhereConstraint = "";
			// this will go to the grid and get the constraint for that row and return it
			// if there is none, then it will return a nullstring
			string strTemp;
			strTemp = vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, 1) + "|||" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, 2);
			GetWhereConstraint = strTemp;
			return GetWhereConstraint;
		}
		// vbPorter upgrade warning: intCounter As short	OnWriteFCConvert.ToInt32(
		private bool ItemInSortList(ref int intCounter)
		{
			bool ItemInSortList = false;
			// this will return true if the item has already been added to the Sort List
			int intTemp;
			ItemInSortList = false;
			for (intTemp = 0; intTemp <= lstSort.Items.Count - 1; intTemp++)
			{
				if (lstSort.ItemData(intTemp) == intCounter)
				{
					ItemInSortList = true;
					break;
				}
			}
			return ItemInSortList;
		}

		private bool ValidateWhereGrid()
		{
			bool ValidateWhereGrid = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will check the where grid for incorrect data entered i.e. text in a number field
				int lngFields;
				int intCol;
				ValidateWhereGrid = true;
				for (lngFields = 0; lngFields <= vsWhere.Rows - 1; lngFields++)
				{
					if (vsWhere.TextMatrix(lngFields, 4) != "")
					{
						// this is the index of the variable in the array
						switch (modCustomReport.Statics.arrCustomReport[FCConvert.ToInt32(vsWhere.TextMatrix(lngFields, 4))].WhereType)
						{
							case modCustomReport.GRIDBDACCOUNT:
								{
									// account field
									if (Strings.Trim(vsWhere.TextMatrix(lngFields, 1)) != "")
									{
										if (modValidateAccount.AccountValidate(vsWhere.TextMatrix(lngFields, 1)))
										{
											// valid account
										}
										else
										{
											ValidateWhereGrid = false;
											MessageBox.Show("Please enter a valid account.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
											break;
										}
									}
									break;
								}
							case modCustomReport.GRIDDATE:
								{
									// date fields
									if (vsWhere.TextMatrix(lngFields, 1) != "")
									{
										if (!Information.IsDate(vsWhere.TextMatrix(lngFields, 1)))
										{
											ValidateWhereGrid = false;
											MessageBox.Show("Please enter a valid date in the " + modCustomReport.Statics.arrCustomReport[FCConvert.ToInt32(vsWhere.TextMatrix(lngFields, 4))].NameToShowUser + " field.", "Data Entry Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
											vsWhere.Select(lngFields, 1);
											break;
										}
									}
									break;
								}
							case modCustomReport.GRIDNUMRANGE:
								{
									// number fields
									if (vsWhere.TextMatrix(lngFields, 1) != "")
									{
										// the first column is used
										if (Information.IsNumeric(vsWhere.TextMatrix(lngFields, 1)))
										{
											// if this is numeric, then check the second field in the range
											if (vsWhere.TextMatrix(lngFields, 2) != "")
											{
												// check to see if the second column is used as well
												if (!Information.IsNumeric(vsWhere.TextMatrix(lngFields, 2)))
												{
													ValidateWhereGrid = false;
													MessageBox.Show("Please enter a valid number in the " + modCustomReport.Statics.arrCustomReport[FCConvert.ToInt32(vsWhere.TextMatrix(lngFields, 4))].NameToShowUser + " field.", "Data Entry Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
													vsWhere.Select(lngFields, 2);
													break;
												}
											}
										}
										else
										{
											ValidateWhereGrid = false;
											MessageBox.Show("Please enter a valid number in the " + modCustomReport.Statics.arrCustomReport[FCConvert.ToInt32(vsWhere.TextMatrix(lngFields, 4))].NameToShowUser + " field.", "Data Entry Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
											vsWhere.Select(lngFields, 1);
											break;
										}
									}
									else
									{
										// only the second column is used
										if (vsWhere.TextMatrix(lngFields, 2) != "")
										{
											// If Not IsNumeric(vsWhere.TextMatrix(lngFields, 2)) Then
											// ValidateWhereGrid = False
											// MsgBox "Please enter a valid number in the " & arrCustomReport(Val(vsWhere.TextMatrix(lngFields, 4))).NameToShowUser & " field.", vbExclamation, "Data Entry Error"
											// vsWhere.Select lngFields, 2
											// Exit For
											// End If
											// Else
											ValidateWhereGrid = false;
											MessageBox.Show("Please enter a starting number for the range in the " + modCustomReport.Statics.arrCustomReport[FCConvert.ToInt32(vsWhere.TextMatrix(lngFields, 4))].NameToShowUser + " field.", "Data Entry Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
											vsWhere.Select(lngFields, 1);
											break;
										}
									}
									break;
								}
							case modCustomReport.GRIDCOMBORANGE:
								{
									break;
								}
						}
						//end switch
					}
				}
				return ValidateWhereGrid;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				ValidateWhereGrid = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Validating Data", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ValidateWhereGrid;
		}

		private void FillTypeList()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strSQL = "";
				clsDRWrapper rsT = new clsDRWrapper();
				if (!modGlobal.Statics.gboolMultipleTowns)
				{
					// MAL@20071010
					// strSQL = "SELECT TypeCode, TypeTitle FROM TYPE WHERE TypeTitle <> '' ORDER BY TypeCode"
					strSQL = "SELECT TypeCode, TypeTitle FROM TYPE WHERE TypeTitle <> '' AND Deleted = 0 ORDER BY TypeCode";
				}
				else
				{
					// MAL@20071010
					// strSQL = "SELECT TypeCode, TypeTitle FROM TYPE WHERE TypeTitle <> '' AND TownKey = 1 ORDER BY TypeCode"
					strSQL = "SELECT TypeCode, TypeTitle FROM TYPE WHERE TypeTitle <> '' AND TownKey = 1 AND Deleted = 0 ORDER BY TypeCode";
				}
				rsT.OpenRecordset(strSQL, modExtraModules.strCRDatabase);
				lstTypes.Clear();
				//FC:FINAL:MSH - issue #1322: restore column after Clear() call
				lstTypes.Columns.Add("");
				lstTypes.Columns[0].Width = 80;
				lstTypes.Columns[1].Width = lstTypes.Width - 100;
				lstTypes.GridLineStyle = GridLineStyle.None;
				while (!rsT.EndOfFile())
				{
					lstTypes.AddItem(Strings.Format(rsT.Get_Fields_Int32("TypeCode"), "000") + "\t" + rsT.Get_Fields_String("TypeTitle"));
					lstTypes.ItemData(lstTypes.NewIndex, FCConvert.ToInt32(rsT.Get_Fields_Int32("TypeCode")));
					rsT.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Type List", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void fcButton1_Click(object sender, EventArgs e)
		{
			cmdClear_Click();
		}
	}
}
