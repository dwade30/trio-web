﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using fecherFoundation.VisualBasicLayer;
using System.IO;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmEOY.
	/// </summary>
	partial class frmEOY : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCPictureBox> imgDone;
		public System.Collections.Generic.List<fecherFoundation.FCPictureBox> imgNotDone;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblEOY;
		public fecherFoundation.FCFrame fraUpdate;
		public fecherFoundation.FCPictureBox imgDone_3;
		public fecherFoundation.FCPictureBox imgDone_2;
		public fecherFoundation.FCPictureBox imgDone_1;
		public fecherFoundation.FCPictureBox imgDone_0;
		public fecherFoundation.FCPictureBox imgNotDone_3;
		public fecherFoundation.FCPictureBox imgNotDone_2;
		public fecherFoundation.FCPictureBox imgNotDone_1;
		public fecherFoundation.FCPictureBox imgNotDone_0;
		public fecherFoundation.FCLabel lblEOY_3;
		public fecherFoundation.FCLabel lblEOY_2;
		public fecherFoundation.FCLabel lblEOY_1;
		public fecherFoundation.FCLabel lblEOY_0;
		public fecherFoundation.FCFrame fraChoice;
		public fecherFoundation.FCButton cmdNo;
		public fecherFoundation.FCButton cmdGo;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCLabel lblInstructions;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEOY));
            this.fraUpdate = new fecherFoundation.FCFrame();
            this.imgDone_3 = new fecherFoundation.FCPictureBox();
            this.imgDone_2 = new fecherFoundation.FCPictureBox();
            this.imgDone_1 = new fecherFoundation.FCPictureBox();
            this.imgDone_0 = new fecherFoundation.FCPictureBox();
            this.imgNotDone_3 = new fecherFoundation.FCPictureBox();
            this.imgNotDone_2 = new fecherFoundation.FCPictureBox();
            this.imgNotDone_1 = new fecherFoundation.FCPictureBox();
            this.imgNotDone_0 = new fecherFoundation.FCPictureBox();
            this.lblEOY_3 = new fecherFoundation.FCLabel();
            this.lblEOY_2 = new fecherFoundation.FCLabel();
            this.lblEOY_1 = new fecherFoundation.FCLabel();
            this.lblEOY_0 = new fecherFoundation.FCLabel();
            this.fraChoice = new fecherFoundation.FCFrame();
            this.cmdNo = new fecherFoundation.FCButton();
            this.cmdGo = new fecherFoundation.FCButton();
            this.cmdCancel = new fecherFoundation.FCButton();
            this.lblInstructions = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileContinue = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileQuit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraUpdate)).BeginInit();
            this.fraUpdate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgDone_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDone_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDone_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDone_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgNotDone_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgNotDone_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgNotDone_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgNotDone_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraChoice)).BeginInit();
            this.fraChoice.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 539);
            this.BottomPanel.Size = new System.Drawing.Size(649, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraUpdate);
            this.ClientArea.Controls.Add(this.fraChoice);
            this.ClientArea.Size = new System.Drawing.Size(649, 479);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(649, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(147, 30);
            this.HeaderText.Text = "End Of Year";
            // 
            // fraUpdate
            // 
            this.fraUpdate.Controls.Add(this.imgDone_3);
            this.fraUpdate.Controls.Add(this.imgDone_2);
            this.fraUpdate.Controls.Add(this.imgDone_1);
            this.fraUpdate.Controls.Add(this.imgDone_0);
            this.fraUpdate.Controls.Add(this.imgNotDone_3);
            this.fraUpdate.Controls.Add(this.imgNotDone_2);
            this.fraUpdate.Controls.Add(this.imgNotDone_1);
            this.fraUpdate.Controls.Add(this.imgNotDone_0);
            this.fraUpdate.Controls.Add(this.lblEOY_3);
            this.fraUpdate.Controls.Add(this.lblEOY_2);
            this.fraUpdate.Controls.Add(this.lblEOY_1);
            this.fraUpdate.Controls.Add(this.lblEOY_0);
            this.fraUpdate.Location = new System.Drawing.Point(30, 242);
            this.fraUpdate.Name = "fraUpdate";
            this.fraUpdate.Size = new System.Drawing.Size(424, 211);
            this.fraUpdate.TabIndex = 1;
            this.fraUpdate.Text = "End Of Year";
            this.fraUpdate.Visible = false;
            // 
            // imgDone_3
            // 
            this.imgDone_3.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgDone_3.Image = ((System.Drawing.Image)(resources.GetObject("imgDone_3.Image")));
            this.imgDone_3.Location = new System.Drawing.Point(20, 159);
            this.imgDone_3.Name = "imgDone_3";
            this.imgDone_3.Size = new System.Drawing.Size(29, 32);
            this.imgDone_3.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgDone_3.Visible = false;
            // 
            // imgDone_2
            // 
            this.imgDone_2.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgDone_2.Image = ((System.Drawing.Image)(resources.GetObject("imgDone_2.Image")));
            this.imgDone_2.Location = new System.Drawing.Point(20, 116);
            this.imgDone_2.Name = "imgDone_2";
            this.imgDone_2.Size = new System.Drawing.Size(29, 32);
            this.imgDone_2.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgDone_2.Visible = false;
            // 
            // imgDone_1
            // 
            this.imgDone_1.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgDone_1.Image = ((System.Drawing.Image)(resources.GetObject("imgDone_1.Image")));
            this.imgDone_1.Location = new System.Drawing.Point(20, 73);
            this.imgDone_1.Name = "imgDone_1";
            this.imgDone_1.Size = new System.Drawing.Size(29, 32);
            this.imgDone_1.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgDone_1.Visible = false;
            // 
            // imgDone_0
            // 
            this.imgDone_0.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgDone_0.Image = ((System.Drawing.Image)(resources.GetObject("imgDone_0.Image")));
            this.imgDone_0.Location = new System.Drawing.Point(20, 30);
            this.imgDone_0.Name = "imgDone_0";
            this.imgDone_0.Size = new System.Drawing.Size(29, 32);
            this.imgDone_0.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgDone_0.Visible = false;
            // 
            // imgNotDone_3
            // 
            this.imgNotDone_3.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgNotDone_3.Image = ((System.Drawing.Image)(resources.GetObject("imgNotDone_3.Image")));
            this.imgNotDone_3.Location = new System.Drawing.Point(20, 159);
            this.imgNotDone_3.Name = "imgNotDone_3";
            this.imgNotDone_3.Size = new System.Drawing.Size(29, 32);
            this.imgNotDone_3.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            // 
            // imgNotDone_2
            // 
            this.imgNotDone_2.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgNotDone_2.Image = ((System.Drawing.Image)(resources.GetObject("imgNotDone_2.Image")));
            this.imgNotDone_2.Location = new System.Drawing.Point(20, 116);
            this.imgNotDone_2.Name = "imgNotDone_2";
            this.imgNotDone_2.Size = new System.Drawing.Size(29, 32);
            this.imgNotDone_2.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            // 
            // imgNotDone_1
            // 
            this.imgNotDone_1.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgNotDone_1.Image = ((System.Drawing.Image)(resources.GetObject("imgNotDone_1.Image")));
            this.imgNotDone_1.Location = new System.Drawing.Point(20, 73);
            this.imgNotDone_1.Name = "imgNotDone_1";
            this.imgNotDone_1.Size = new System.Drawing.Size(29, 32);
            this.imgNotDone_1.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            // 
            // imgNotDone_0
            // 
            this.imgNotDone_0.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgNotDone_0.Image = ((System.Drawing.Image)(resources.GetObject("imgNotDone_0.Image")));
            this.imgNotDone_0.Location = new System.Drawing.Point(20, 30);
            this.imgNotDone_0.Name = "imgNotDone_0";
            this.imgNotDone_0.Size = new System.Drawing.Size(29, 32);
            this.imgNotDone_0.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            // 
            // lblEOY_3
            // 
            this.lblEOY_3.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.lblEOY_3.Location = new System.Drawing.Point(94, 159);
            this.lblEOY_3.Name = "lblEOY_3";
            this.lblEOY_3.Size = new System.Drawing.Size(295, 23);
            this.lblEOY_3.TabIndex = 3;
            this.lblEOY_3.Text = "DELETE PAYMENT RECORDS FROM CURRENT TABLE";
            this.lblEOY_3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblEOY_2
            // 
            this.lblEOY_2.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.lblEOY_2.Location = new System.Drawing.Point(94, 116);
            this.lblEOY_2.Name = "lblEOY_2";
            this.lblEOY_2.Size = new System.Drawing.Size(323, 23);
            this.lblEOY_2.TabIndex = 2;
            this.lblEOY_2.Text = "DELETE RECEIPTS RECORDS FROM CURRENT TABLE";
            this.lblEOY_2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblEOY_1
            // 
            this.lblEOY_1.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.lblEOY_1.Location = new System.Drawing.Point(94, 73);
            this.lblEOY_1.Name = "lblEOY_1";
            this.lblEOY_1.Size = new System.Drawing.Size(246, 23);
            this.lblEOY_1.TabIndex = 1;
            this.lblEOY_1.Text = "ARCHIVE CURRENT PAYMENT RECORDS";
            this.lblEOY_1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblEOY_0
            // 
            this.lblEOY_0.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.lblEOY_0.Location = new System.Drawing.Point(94, 30);
            this.lblEOY_0.Name = "lblEOY_0";
            this.lblEOY_0.Size = new System.Drawing.Size(240, 23);
            this.lblEOY_0.Text = "ARCHIVE CURRENT RECEIPT RECORDS";
            this.lblEOY_0.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fraChoice
            // 
            this.fraChoice.AppearanceKey = "groupBoxNoBorders";
            this.fraChoice.Controls.Add(this.cmdNo);
            this.fraChoice.Controls.Add(this.cmdGo);
            this.fraChoice.Controls.Add(this.cmdCancel);
            this.fraChoice.Controls.Add(this.lblInstructions);
            this.fraChoice.Name = "fraChoice";
            this.fraChoice.Size = new System.Drawing.Size(631, 163);
            // 
            // cmdNo
            // 
            this.cmdNo.AppearanceKey = "actionButton";
            this.cmdNo.Location = new System.Drawing.Point(112, 92);
            this.cmdNo.Name = "cmdNo";
            this.cmdNo.Size = new System.Drawing.Size(47, 40);
            this.cmdNo.TabIndex = 2;
            this.cmdNo.Text = "No";
            this.cmdNo.Click += new System.EventHandler(this.cmdNo_Click);
            // 
            // cmdGo
            // 
            this.cmdGo.AppearanceKey = "actionButton";
            this.cmdGo.Location = new System.Drawing.Point(30, 92);
            this.cmdGo.Name = "cmdGo";
            this.cmdGo.Size = new System.Drawing.Size(63, 40);
            this.cmdGo.TabIndex = 1;
            this.cmdGo.Text = "Yes";
            this.cmdGo.Click += new System.EventHandler(this.cmdGo_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.AppearanceKey = "actionButton";
            this.cmdCancel.Location = new System.Drawing.Point(178, 92);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(78, 40);
            this.cmdCancel.TabIndex = 3;
            this.cmdCancel.Text = "Cancel";
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // lblInstructions
            // 
            this.lblInstructions.Location = new System.Drawing.Point(30, 30);
            this.lblInstructions.Name = "lblInstructions";
            this.lblInstructions.Size = new System.Drawing.Size(579, 52);
            this.lblInstructions.Text = resources.GetString("lblInstructions.Text");
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileContinue,
            this.Seperator,
            this.mnuFileQuit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuFileContinue
            // 
            this.mnuFileContinue.Index = 0;
            this.mnuFileContinue.Name = "mnuFileContinue";
            this.mnuFileContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuFileContinue.Text = "Save & Continue";
            this.mnuFileContinue.Click += new System.EventHandler(this.mnuFileContinue_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuFileQuit
            // 
            this.mnuFileQuit.Index = 2;
            this.mnuFileQuit.Name = "mnuFileQuit";
            this.mnuFileQuit.Text = "Exit";
            this.mnuFileQuit.Click += new System.EventHandler(this.mnuFileQuit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(202, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(90, 48);
            this.cmdSave.TabIndex = 3;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuFileContinue_Click);
            // 
            // frmEOY
            // 
            this.ClientSize = new System.Drawing.Size(649, 647);
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmEOY";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "End of Year";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmEOY_Load);
            this.Resize += new System.EventHandler(this.frmEOY_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmEOY_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmEOY_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraUpdate)).EndInit();
            this.fraUpdate.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgDone_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDone_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDone_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDone_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgNotDone_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgNotDone_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgNotDone_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgNotDone_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraChoice)).EndInit();
            this.fraChoice.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		public FCButton cmdSave;
	}
}
