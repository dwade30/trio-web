﻿namespace TWCR0000
{
	/// <summary>
	/// Summary description for arTypeListing.
	/// </summary>
	partial class arTypeListing
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(arTypeListing));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTitle1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDefault1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccount1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTitle2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDefault2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccount2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTitle3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDefault3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccount3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTitle4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDefault4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccount4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTitle5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDefault5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccount5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTitle6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDefault6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccount6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblBMV = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBMV = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lnBottom = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldRefDesc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldControl1Desc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldControl2Desc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldControl3Desc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblHeaderTitles = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lnDefaultTitles = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblCopies = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPrint = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldCopies = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPrint = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblScreenTitle1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblScreenTitle2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblScreenTitle3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblScreenTitle4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblScreenTitle5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblScreenTitle6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldYear1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYear2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYear3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYear4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYear5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYear6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReq1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReq2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReq3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReq4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblAltCash = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldAltCash = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPageHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCode = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDefault = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblCode2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblType2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lblDeleted = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.fldCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDefault1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDefault2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDefault3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDefault4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDefault5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDefault6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBMV)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBMV)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRefDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControl1Desc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControl2Desc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControl3Desc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeaderTitles)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCopies)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCopies)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblScreenTitle1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblScreenTitle2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblScreenTitle3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblScreenTitle4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblScreenTitle5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblScreenTitle6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReq1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReq2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReq3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReq4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAltCash)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAltCash)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDefault)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblType2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeleted)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldCode,
				this.fldTitle1,
				this.fldDefault1,
				this.fldAccount1,
				this.fldTitle2,
				this.fldDefault2,
				this.fldAccount2,
				this.fldTitle3,
				this.fldDefault3,
				this.fldAccount3,
				this.fldTitle4,
				this.fldDefault4,
				this.fldAccount4,
				this.fldTitle5,
				this.fldDefault5,
				this.fldAccount5,
				this.fldTitle6,
				this.fldDefault6,
				this.fldAccount6,
				this.fldType,
				this.lblBMV,
				this.fldBMV,
				this.lnBottom,
				this.fldRefDesc,
				this.fldControl1Desc,
				this.fldControl2Desc,
				this.fldControl3Desc,
				this.lblHeaderTitles,
				this.lnDefaultTitles,
				this.lblCopies,
				this.lblPrint,
				this.fldCopies,
				this.fldPrint,
				this.lblScreenTitle1,
				this.lblScreenTitle2,
				this.lblScreenTitle3,
				this.lblScreenTitle4,
				this.lblScreenTitle5,
				this.lblScreenTitle6,
				this.fldYear1,
				this.fldYear2,
				this.fldYear3,
				this.fldYear4,
				this.fldYear5,
				this.fldYear6,
				this.fldReq1,
				this.fldReq2,
				this.fldReq3,
				this.fldReq4,
				this.lblAltCash,
				this.fldAltCash
			});
			this.Detail.Height = 1.291667F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldCode
			// 
			this.fldCode.Height = 0.1875F;
			this.fldCode.Left = 0F;
			this.fldCode.Name = "fldCode";
			this.fldCode.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldCode.Text = null;
			this.fldCode.Top = 0F;
			this.fldCode.Width = 0.5625F;
			// 
			// fldTitle1
			// 
			this.fldTitle1.CanGrow = false;
			this.fldTitle1.Height = 0.1875F;
			this.fldTitle1.Left = 2.375F;
			this.fldTitle1.MultiLine = false;
			this.fldTitle1.Name = "fldTitle1";
			this.fldTitle1.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldTitle1.Text = " ";
			this.fldTitle1.Top = 0F;
			this.fldTitle1.Visible = false;
			this.fldTitle1.Width = 1.0625F;
			// 
			// fldDefault1
			// 
			this.fldDefault1.Height = 0.1875F;
			this.fldDefault1.Left = 4F;
			this.fldDefault1.Name = "fldDefault1";
			this.fldDefault1.OutputFormat = resources.GetString("fldDefault1.OutputFormat");
			this.fldDefault1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldDefault1.Text = " ";
			this.fldDefault1.Top = 0F;
			this.fldDefault1.Visible = false;
			this.fldDefault1.Width = 1F;
			// 
			// fldAccount1
			// 
			this.fldAccount1.Height = 0.1875F;
			this.fldAccount1.Left = 5.0625F;
			this.fldAccount1.Name = "fldAccount1";
			this.fldAccount1.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldAccount1.Text = " ";
			this.fldAccount1.Top = 0F;
			this.fldAccount1.Visible = false;
			this.fldAccount1.Width = 1.125F;
			// 
			// fldTitle2
			// 
			this.fldTitle2.CanGrow = false;
			this.fldTitle2.Height = 0.1875F;
			this.fldTitle2.Left = 2.375F;
			this.fldTitle2.MultiLine = false;
			this.fldTitle2.Name = "fldTitle2";
			this.fldTitle2.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldTitle2.Text = " ";
			this.fldTitle2.Top = 0.1875F;
			this.fldTitle2.Visible = false;
			this.fldTitle2.Width = 1.0625F;
			// 
			// fldDefault2
			// 
			this.fldDefault2.Height = 0.1875F;
			this.fldDefault2.Left = 4F;
			this.fldDefault2.Name = "fldDefault2";
			this.fldDefault2.OutputFormat = resources.GetString("fldDefault2.OutputFormat");
			this.fldDefault2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldDefault2.Text = " ";
			this.fldDefault2.Top = 0.1875F;
			this.fldDefault2.Visible = false;
			this.fldDefault2.Width = 1F;
			// 
			// fldAccount2
			// 
			this.fldAccount2.Height = 0.1875F;
			this.fldAccount2.Left = 5.0625F;
			this.fldAccount2.Name = "fldAccount2";
			this.fldAccount2.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldAccount2.Text = " ";
			this.fldAccount2.Top = 0.1875F;
			this.fldAccount2.Visible = false;
			this.fldAccount2.Width = 1.125F;
			// 
			// fldTitle3
			// 
			this.fldTitle3.CanGrow = false;
			this.fldTitle3.Height = 0.1875F;
			this.fldTitle3.Left = 2.375F;
			this.fldTitle3.MultiLine = false;
			this.fldTitle3.Name = "fldTitle3";
			this.fldTitle3.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldTitle3.Text = " ";
			this.fldTitle3.Top = 0.375F;
			this.fldTitle3.Visible = false;
			this.fldTitle3.Width = 1.0625F;
			// 
			// fldDefault3
			// 
			this.fldDefault3.Height = 0.1875F;
			this.fldDefault3.Left = 4F;
			this.fldDefault3.Name = "fldDefault3";
			this.fldDefault3.OutputFormat = resources.GetString("fldDefault3.OutputFormat");
			this.fldDefault3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldDefault3.Text = " ";
			this.fldDefault3.Top = 0.375F;
			this.fldDefault3.Visible = false;
			this.fldDefault3.Width = 1F;
			// 
			// fldAccount3
			// 
			this.fldAccount3.Height = 0.1875F;
			this.fldAccount3.Left = 5.0625F;
			this.fldAccount3.Name = "fldAccount3";
			this.fldAccount3.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldAccount3.Text = " ";
			this.fldAccount3.Top = 0.375F;
			this.fldAccount3.Visible = false;
			this.fldAccount3.Width = 1.125F;
			// 
			// fldTitle4
			// 
			this.fldTitle4.CanGrow = false;
			this.fldTitle4.Height = 0.1875F;
			this.fldTitle4.Left = 2.375F;
			this.fldTitle4.MultiLine = false;
			this.fldTitle4.Name = "fldTitle4";
			this.fldTitle4.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldTitle4.Text = " ";
			this.fldTitle4.Top = 0.5625F;
			this.fldTitle4.Visible = false;
			this.fldTitle4.Width = 1.0625F;
			// 
			// fldDefault4
			// 
			this.fldDefault4.Height = 0.1875F;
			this.fldDefault4.Left = 4F;
			this.fldDefault4.Name = "fldDefault4";
			this.fldDefault4.OutputFormat = resources.GetString("fldDefault4.OutputFormat");
			this.fldDefault4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldDefault4.Text = " ";
			this.fldDefault4.Top = 0.5625F;
			this.fldDefault4.Visible = false;
			this.fldDefault4.Width = 1F;
			// 
			// fldAccount4
			// 
			this.fldAccount4.Height = 0.1875F;
			this.fldAccount4.Left = 5.0625F;
			this.fldAccount4.Name = "fldAccount4";
			this.fldAccount4.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldAccount4.Text = " ";
			this.fldAccount4.Top = 0.5625F;
			this.fldAccount4.Visible = false;
			this.fldAccount4.Width = 1.125F;
			// 
			// fldTitle5
			// 
			this.fldTitle5.CanGrow = false;
			this.fldTitle5.Height = 0.1875F;
			this.fldTitle5.Left = 2.375F;
			this.fldTitle5.MultiLine = false;
			this.fldTitle5.Name = "fldTitle5";
			this.fldTitle5.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldTitle5.Text = " ";
			this.fldTitle5.Top = 0.75F;
			this.fldTitle5.Visible = false;
			this.fldTitle5.Width = 1.0625F;
			// 
			// fldDefault5
			// 
			this.fldDefault5.Height = 0.1875F;
			this.fldDefault5.Left = 4F;
			this.fldDefault5.Name = "fldDefault5";
			this.fldDefault5.OutputFormat = resources.GetString("fldDefault5.OutputFormat");
			this.fldDefault5.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldDefault5.Text = " ";
			this.fldDefault5.Top = 0.75F;
			this.fldDefault5.Visible = false;
			this.fldDefault5.Width = 1F;
			// 
			// fldAccount5
			// 
			this.fldAccount5.Height = 0.1875F;
			this.fldAccount5.Left = 5.0625F;
			this.fldAccount5.Name = "fldAccount5";
			this.fldAccount5.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldAccount5.Text = " ";
			this.fldAccount5.Top = 0.75F;
			this.fldAccount5.Visible = false;
			this.fldAccount5.Width = 1.125F;
			// 
			// fldTitle6
			// 
			this.fldTitle6.CanGrow = false;
			this.fldTitle6.Height = 0.1875F;
			this.fldTitle6.Left = 2.375F;
			this.fldTitle6.MultiLine = false;
			this.fldTitle6.Name = "fldTitle6";
			this.fldTitle6.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldTitle6.Text = " ";
			this.fldTitle6.Top = 0.9375F;
			this.fldTitle6.Visible = false;
			this.fldTitle6.Width = 1.0625F;
			// 
			// fldDefault6
			// 
			this.fldDefault6.Height = 0.1875F;
			this.fldDefault6.Left = 4F;
			this.fldDefault6.Name = "fldDefault6";
			this.fldDefault6.OutputFormat = resources.GetString("fldDefault6.OutputFormat");
			this.fldDefault6.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldDefault6.Text = " ";
			this.fldDefault6.Top = 0.9375F;
			this.fldDefault6.Visible = false;
			this.fldDefault6.Width = 1F;
			// 
			// fldAccount6
			// 
			this.fldAccount6.Height = 0.1875F;
			this.fldAccount6.Left = 5.0625F;
			this.fldAccount6.Name = "fldAccount6";
			this.fldAccount6.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldAccount6.Text = " ";
			this.fldAccount6.Top = 0.9375F;
			this.fldAccount6.Visible = false;
			this.fldAccount6.Width = 1.125F;
			// 
			// fldType
			// 
			this.fldType.Height = 0.1875F;
			this.fldType.Left = 0.5625F;
			this.fldType.Name = "fldType";
			this.fldType.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldType.Text = null;
			this.fldType.Top = 0F;
			this.fldType.Width = 1.6875F;
			// 
			// lblBMV
			// 
			this.lblBMV.Height = 0.1875F;
			this.lblBMV.HyperLink = null;
			this.lblBMV.Left = 0.3125F;
			this.lblBMV.Name = "lblBMV";
			this.lblBMV.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.lblBMV.Text = "BMV:";
			this.lblBMV.Top = 0.1875F;
			this.lblBMV.Visible = false;
			this.lblBMV.Width = 0.5F;
			// 
			// fldBMV
			// 
			this.fldBMV.Height = 0.1875F;
			this.fldBMV.Left = 0.75F;
			this.fldBMV.Name = "fldBMV";
			this.fldBMV.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldBMV.Text = null;
			this.fldBMV.Top = 0.1875F;
			this.fldBMV.Visible = false;
			this.fldBMV.Width = 0.375F;
			// 
			// lnBottom
			// 
			this.lnBottom.Height = 0F;
			this.lnBottom.Left = 0F;
			this.lnBottom.LineWeight = 1F;
			this.lnBottom.Name = "lnBottom";
			this.lnBottom.Top = 1.1875F;
			this.lnBottom.Width = 6.5F;
			this.lnBottom.X1 = 0F;
			this.lnBottom.X2 = 6.5F;
			this.lnBottom.Y1 = 1.1875F;
			this.lnBottom.Y2 = 1.1875F;
			// 
			// fldRefDesc
			// 
			this.fldRefDesc.Height = 0.1875F;
			this.fldRefDesc.Left = 1.375F;
			this.fldRefDesc.Name = "fldRefDesc";
			this.fldRefDesc.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldRefDesc.Text = null;
			this.fldRefDesc.Top = 0.375F;
			this.fldRefDesc.Width = 0.875F;
			// 
			// fldControl1Desc
			// 
			this.fldControl1Desc.Height = 0.1875F;
			this.fldControl1Desc.Left = 1.375F;
			this.fldControl1Desc.Name = "fldControl1Desc";
			this.fldControl1Desc.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldControl1Desc.Text = null;
			this.fldControl1Desc.Top = 0.5625F;
			this.fldControl1Desc.Width = 0.875F;
			// 
			// fldControl2Desc
			// 
			this.fldControl2Desc.Height = 0.1875F;
			this.fldControl2Desc.Left = 1.375F;
			this.fldControl2Desc.Name = "fldControl2Desc";
			this.fldControl2Desc.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldControl2Desc.Text = null;
			this.fldControl2Desc.Top = 0.75F;
			this.fldControl2Desc.Width = 0.875F;
			// 
			// fldControl3Desc
			// 
			this.fldControl3Desc.Height = 0.1875F;
			this.fldControl3Desc.Left = 1.375F;
			this.fldControl3Desc.Name = "fldControl3Desc";
			this.fldControl3Desc.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldControl3Desc.Text = null;
			this.fldControl3Desc.Top = 0.9375F;
			this.fldControl3Desc.Width = 0.875F;
			// 
			// lblHeaderTitles
			// 
			this.lblHeaderTitles.Height = 0.1875F;
			this.lblHeaderTitles.HyperLink = null;
			this.lblHeaderTitles.Left = 1.375F;
			this.lblHeaderTitles.Name = "lblHeaderTitles";
			this.lblHeaderTitles.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
			this.lblHeaderTitles.Text = "Default Titles";
			this.lblHeaderTitles.Top = 0.1875F;
			this.lblHeaderTitles.Width = 0.875F;
			// 
			// lnDefaultTitles
			// 
			this.lnDefaultTitles.Height = 0F;
			this.lnDefaultTitles.Left = 1.4375F;
			this.lnDefaultTitles.LineWeight = 1F;
			this.lnDefaultTitles.Name = "lnDefaultTitles";
			this.lnDefaultTitles.Top = 0.375F;
			this.lnDefaultTitles.Width = 0.8125F;
			this.lnDefaultTitles.X1 = 1.4375F;
			this.lnDefaultTitles.X2 = 2.25F;
			this.lnDefaultTitles.Y1 = 0.375F;
			this.lnDefaultTitles.Y2 = 0.375F;
			// 
			// lblCopies
			// 
			this.lblCopies.Height = 0.1875F;
			this.lblCopies.HyperLink = null;
			this.lblCopies.Left = 0.3125F;
			this.lblCopies.Name = "lblCopies";
			this.lblCopies.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.lblCopies.Text = "Copies:";
			this.lblCopies.Top = 0.375F;
			this.lblCopies.Width = 0.5F;
			// 
			// lblPrint
			// 
			this.lblPrint.Height = 0.1875F;
			this.lblPrint.HyperLink = null;
			this.lblPrint.Left = 0.3125F;
			this.lblPrint.Name = "lblPrint";
			this.lblPrint.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.lblPrint.Text = "Print:";
			this.lblPrint.Top = 0.5625F;
			this.lblPrint.Width = 0.5F;
			// 
			// fldCopies
			// 
			this.fldCopies.Height = 0.1875F;
			this.fldCopies.Left = 0.75F;
			this.fldCopies.Name = "fldCopies";
			this.fldCopies.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldCopies.Text = null;
			this.fldCopies.Top = 0.375F;
			this.fldCopies.Visible = false;
			this.fldCopies.Width = 0.375F;
			// 
			// fldPrint
			// 
			this.fldPrint.Height = 0.1875F;
			this.fldPrint.Left = 0.75F;
			this.fldPrint.Name = "fldPrint";
			this.fldPrint.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldPrint.Text = null;
			this.fldPrint.Top = 0.5625F;
			this.fldPrint.Visible = false;
			this.fldPrint.Width = 0.375F;
			// 
			// lblScreenTitle1
			// 
			this.lblScreenTitle1.Height = 0.1875F;
			this.lblScreenTitle1.HyperLink = null;
			this.lblScreenTitle1.Left = 3.4375F;
			this.lblScreenTitle1.Name = "lblScreenTitle1";
			this.lblScreenTitle1.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.lblScreenTitle1.Text = null;
			this.lblScreenTitle1.Top = 0F;
			this.lblScreenTitle1.Width = 0.5625F;
			// 
			// lblScreenTitle2
			// 
			this.lblScreenTitle2.Height = 0.1875F;
			this.lblScreenTitle2.HyperLink = null;
			this.lblScreenTitle2.Left = 3.4375F;
			this.lblScreenTitle2.Name = "lblScreenTitle2";
			this.lblScreenTitle2.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.lblScreenTitle2.Text = null;
			this.lblScreenTitle2.Top = 0.1875F;
			this.lblScreenTitle2.Width = 0.5625F;
			// 
			// lblScreenTitle3
			// 
			this.lblScreenTitle3.Height = 0.1875F;
			this.lblScreenTitle3.HyperLink = null;
			this.lblScreenTitle3.Left = 3.4375F;
			this.lblScreenTitle3.Name = "lblScreenTitle3";
			this.lblScreenTitle3.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.lblScreenTitle3.Text = null;
			this.lblScreenTitle3.Top = 0.375F;
			this.lblScreenTitle3.Width = 0.5625F;
			// 
			// lblScreenTitle4
			// 
			this.lblScreenTitle4.Height = 0.1875F;
			this.lblScreenTitle4.HyperLink = null;
			this.lblScreenTitle4.Left = 3.4375F;
			this.lblScreenTitle4.Name = "lblScreenTitle4";
			this.lblScreenTitle4.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.lblScreenTitle4.Text = null;
			this.lblScreenTitle4.Top = 0.5625F;
			this.lblScreenTitle4.Width = 0.5625F;
			// 
			// lblScreenTitle5
			// 
			this.lblScreenTitle5.Height = 0.1875F;
			this.lblScreenTitle5.HyperLink = null;
			this.lblScreenTitle5.Left = 3.4375F;
			this.lblScreenTitle5.Name = "lblScreenTitle5";
			this.lblScreenTitle5.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.lblScreenTitle5.Text = null;
			this.lblScreenTitle5.Top = 0.75F;
			this.lblScreenTitle5.Width = 0.5625F;
			// 
			// lblScreenTitle6
			// 
			this.lblScreenTitle6.Height = 0.1875F;
			this.lblScreenTitle6.HyperLink = null;
			this.lblScreenTitle6.Left = 3.4375F;
			this.lblScreenTitle6.Name = "lblScreenTitle6";
			this.lblScreenTitle6.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.lblScreenTitle6.Text = null;
			this.lblScreenTitle6.Top = 0.9375F;
			this.lblScreenTitle6.Width = 0.5625F;
			// 
			// fldYear1
			// 
			this.fldYear1.Height = 0.1875F;
			this.fldYear1.Left = 6.25F;
			this.fldYear1.Name = "fldYear1";
			this.fldYear1.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldYear1.Text = null;
			this.fldYear1.Top = 0F;
			this.fldYear1.Width = 0.125F;
			// 
			// fldYear2
			// 
			this.fldYear2.Height = 0.1875F;
			this.fldYear2.Left = 6.25F;
			this.fldYear2.Name = "fldYear2";
			this.fldYear2.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldYear2.Text = null;
			this.fldYear2.Top = 0.1875F;
			this.fldYear2.Width = 0.125F;
			// 
			// fldYear3
			// 
			this.fldYear3.Height = 0.1875F;
			this.fldYear3.Left = 6.25F;
			this.fldYear3.Name = "fldYear3";
			this.fldYear3.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldYear3.Text = null;
			this.fldYear3.Top = 0.375F;
			this.fldYear3.Width = 0.125F;
			// 
			// fldYear4
			// 
			this.fldYear4.Height = 0.1875F;
			this.fldYear4.Left = 6.25F;
			this.fldYear4.Name = "fldYear4";
			this.fldYear4.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldYear4.Text = null;
			this.fldYear4.Top = 0.5625F;
			this.fldYear4.Width = 0.125F;
			// 
			// fldYear5
			// 
			this.fldYear5.Height = 0.1875F;
			this.fldYear5.Left = 6.25F;
			this.fldYear5.Name = "fldYear5";
			this.fldYear5.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldYear5.Text = null;
			this.fldYear5.Top = 0.75F;
			this.fldYear5.Width = 0.125F;
			// 
			// fldYear6
			// 
			this.fldYear6.Height = 0.1875F;
			this.fldYear6.Left = 6.25F;
			this.fldYear6.Name = "fldYear6";
			this.fldYear6.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldYear6.Text = null;
			this.fldYear6.Top = 0.9375F;
			this.fldYear6.Width = 0.125F;
			// 
			// fldReq1
			// 
			this.fldReq1.Height = 0.1875F;
			this.fldReq1.Left = 1.125F;
			this.fldReq1.Name = "fldReq1";
			this.fldReq1.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldReq1.Text = " ";
			this.fldReq1.Top = 0.375F;
			this.fldReq1.Width = 0.25F;
			// 
			// fldReq2
			// 
			this.fldReq2.Height = 0.1875F;
			this.fldReq2.Left = 1.125F;
			this.fldReq2.Name = "fldReq2";
			this.fldReq2.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldReq2.Text = " ";
			this.fldReq2.Top = 0.5625F;
			this.fldReq2.Width = 0.25F;
			// 
			// fldReq3
			// 
			this.fldReq3.Height = 0.1875F;
			this.fldReq3.Left = 1.125F;
			this.fldReq3.Name = "fldReq3";
			this.fldReq3.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldReq3.Text = " ";
			this.fldReq3.Top = 0.75F;
			this.fldReq3.Width = 0.25F;
			// 
			// fldReq4
			// 
			this.fldReq4.Height = 0.1875F;
			this.fldReq4.Left = 1.125F;
			this.fldReq4.Name = "fldReq4";
			this.fldReq4.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldReq4.Text = " ";
			this.fldReq4.Top = 0.9375F;
			this.fldReq4.Width = 0.25F;
			// 
			// lblAltCash
			// 
			this.lblAltCash.Height = 0.1875F;
			this.lblAltCash.HyperLink = null;
			this.lblAltCash.Left = 0.0625F;
			this.lblAltCash.Name = "lblAltCash";
			this.lblAltCash.Style = "font-family: \'Tahoma\'";
			this.lblAltCash.Text = "Alt Cash:";
			this.lblAltCash.Top = 0.75F;
			this.lblAltCash.Visible = false;
			this.lblAltCash.Width = 1F;
			// 
			// fldAltCash
			// 
			this.fldAltCash.Height = 0.1875F;
			this.fldAltCash.Left = 0.0625F;
			this.fldAltCash.Name = "fldAltCash";
			this.fldAltCash.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldAltCash.Text = null;
			this.fldAltCash.Top = 0.9375F;
			this.fldAltCash.Visible = false;
			this.fldAltCash.Width = 1.125F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblDate,
				this.lblPageHeader,
				this.lblCode,
				this.lblType,
				this.lblYear,
				this.lblTitle,
				this.lblDefault,
				this.lblAccount,
				this.Line1,
				this.lblCode2,
				this.lblType2,
				this.lblPage,
				this.lblMuniName,
				this.lblTime
			});
			this.PageHeader.Height = 0.6875F;
			this.PageHeader.Name = "PageHeader";
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 5.5F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1F;
			// 
			// lblPageHeader
			// 
			this.lblPageHeader.Height = 0.25F;
			this.lblPageHeader.HyperLink = null;
			this.lblPageHeader.Left = 0F;
			this.lblPageHeader.Name = "lblPageHeader";
			this.lblPageHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblPageHeader.Text = "Cash Receipting Type Listing";
			this.lblPageHeader.Top = 0F;
			this.lblPageHeader.Width = 6.5F;
			// 
			// lblCode
			// 
			this.lblCode.Height = 0.1875F;
			this.lblCode.HyperLink = null;
			this.lblCode.Left = 0F;
			this.lblCode.Name = "lblCode";
			this.lblCode.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.lblCode.Text = "Code";
			this.lblCode.Top = 0.5F;
			this.lblCode.Width = 0.4375F;
			// 
			// lblType
			// 
			this.lblType.Height = 0.1875F;
			this.lblType.HyperLink = null;
			this.lblType.Left = 0.5625F;
			this.lblType.Name = "lblType";
			this.lblType.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left";
			this.lblType.Text = "Type";
			this.lblType.Top = 0.5F;
			this.lblType.Width = 1.375F;
			// 
			// lblYear
			// 
			this.lblYear.Height = 0.1875F;
			this.lblYear.HyperLink = null;
			this.lblYear.Left = 6.0625F;
			this.lblYear.MultiLine = false;
			this.lblYear.Name = "lblYear";
			this.lblYear.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblYear.Text = "Year";
			this.lblYear.Top = 0.5F;
			this.lblYear.Visible = false;
			this.lblYear.Width = 0.375F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.1875F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 2.4375F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left";
			this.lblTitle.Text = "Report Title";
			this.lblTitle.Top = 0.5F;
			this.lblTitle.Visible = false;
			this.lblTitle.Width = 0.875F;
			// 
			// lblDefault
			// 
			this.lblDefault.Height = 0.1875F;
			this.lblDefault.HyperLink = null;
			this.lblDefault.Left = 4F;
			this.lblDefault.Name = "lblDefault";
			this.lblDefault.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblDefault.Text = "Default $";
			this.lblDefault.Top = 0.5F;
			this.lblDefault.Visible = false;
			this.lblDefault.Width = 1F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 5.0625F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left";
			this.lblAccount.Text = "Account";
			this.lblAccount.Top = 0.5F;
			this.lblAccount.Visible = false;
			this.lblAccount.Width = 1F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.6875F;
			this.Line1.Width = 6.5F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 6.5F;
			this.Line1.Y1 = 0.6875F;
			this.Line1.Y2 = 0.6875F;
			// 
			// lblCode2
			// 
			this.lblCode2.Height = 0.1875F;
			this.lblCode2.HyperLink = null;
			this.lblCode2.Left = 3.625F;
			this.lblCode2.Name = "lblCode2";
			this.lblCode2.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.lblCode2.Text = "Code";
			this.lblCode2.Top = 0.5F;
			this.lblCode2.Visible = false;
			this.lblCode2.Width = 0.5625F;
			// 
			// lblType2
			// 
			this.lblType2.Height = 0.1875F;
			this.lblType2.HyperLink = null;
			this.lblType2.Left = 4.1875F;
			this.lblType2.Name = "lblType2";
			this.lblType2.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.lblType2.Text = "Type";
			this.lblType2.Top = 0.5F;
			this.lblType2.Visible = false;
			this.lblType2.Width = 1.6875F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 5.5F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblPage.Text = null;
			this.lblPage.Top = 0.1875F;
			this.lblPage.Width = 1F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 2.3125F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1875F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.1875F;
			this.lblTime.Width = 1F;
			// 
			// PageFooter
			// 
			this.PageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblDeleted
			});
			this.PageFooter.Height = 0.21875F;
			this.PageFooter.Name = "PageFooter";
			// 
			// lblDeleted
			// 
			this.lblDeleted.Height = 0.1875F;
			this.lblDeleted.HyperLink = null;
			this.lblDeleted.Left = 0.0625F;
			this.lblDeleted.Name = "lblDeleted";
			this.lblDeleted.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.lblDeleted.Text = "* = Deleted Type Code";
			this.lblDeleted.Top = 0F;
			this.lblDeleted.Width = 2F;
			// 
			// arTypeListing
			// 
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
			((System.ComponentModel.ISupportInitialize)(this.fldCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDefault1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDefault2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDefault3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDefault4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDefault5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDefault6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBMV)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBMV)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRefDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControl1Desc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControl2Desc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControl3Desc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeaderTitles)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCopies)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCopies)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblScreenTitle1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblScreenTitle2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblScreenTitle3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblScreenTitle4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblScreenTitle5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblScreenTitle6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReq1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReq2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReq3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReq4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAltCash)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAltCash)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDefault)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblType2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeleted)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTitle1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDefault1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTitle2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDefault2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTitle3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDefault3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTitle4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDefault4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTitle5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDefault5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTitle6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDefault6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBMV;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBMV;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnBottom;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRefDesc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldControl1Desc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldControl2Desc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldControl3Desc;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeaderTitles;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnDefaultTitles;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCopies;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPrint;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCopies;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrint;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblScreenTitle1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblScreenTitle2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblScreenTitle3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblScreenTitle4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblScreenTitle5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblScreenTitle6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReq1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReq2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReq3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReq4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAltCash;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAltCash;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDefault;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblType2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDeleted;
	}
}
