﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Wisej.Web;
using System.Diagnostics;
using Global;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using SharedApplication.Extensions;
using TWSharedLibrary;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for sarAccountingSummary.
	/// </summary>
	public partial class sarAccountingSummary : FCSectionReport
	{
		public static sarAccountingSummary InstancePtr
		{
			get
			{
				return (sarAccountingSummary)Sys.GetInstance(typeof(sarAccountingSummary));
			}
		}

		protected sarAccountingSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	sarAccountingSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		clsDRWrapper rsData = new clsDRWrapper();
		int intExtraLines;
		int intLvl;
		int intArrIndex;
		bool boolArrDone;
		modBudgetaryAccounting.FundType[] ftOverrideArray = null;
		modBudgetaryAccounting.FundType[] ftAccountArray = null;
		modBudgetaryAccounting.FundType[] ftEFTArray = null;
		modBudgetaryAccounting.FundType[] ftFundArray = null;
		modBudgetaryAccounting.FundType[] ftFundArray2 = null;
		modBudgetaryAccounting.FundType[] ftAltCashFundArray = null;
		modBudgetaryAccounting.FundType[] ftCheckRecEntries = null;
		int lngAcctCT;
		int lngFundCT;
		string strDefaultCashAccount = "";
		double dblM_DTotals;
		// this will keep the total of any M D Account to have its own entry at the end of the Accounting Summary
		bool boolM_DLine;
		double dblMFundTotal;
		public bool boolUseBDJournal;
		public bool boolFundArrayEmpty;
		modBudgetaryAccounting.FundType[] ftFunds = new modBudgetaryAccounting.FundType[999 + 1];
		// this will be the second array used for Due To / Due Froms
		double dblFTAccountTotals;
		double dblFTAccountTotalsEFT;
        private Dictionary<int,cFundTotal> FTAccountFunds = new Dictionary<int, cFundTotal>();
        private cBDAccountController acctCont;// = new cBDAccountController();
        private cBDSettings bdSets;
        private bool boolUsePrevYears = false;

		public sarAccountingSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += SarAccountingSummary_ReportEnd;
		}

        private void SarAccountingSummary_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
        }

        private struct DueToFrom
		{
			public string Account;
			public double Amount;
			public string Type;
			// CashEntry, Due To, Due From...ect
			public string DbCr;
			// DB' or 'CR'
		};

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// this will show the line when a new section is started
			if (intArrIndex == intLvl + 1)
			{
				Line.Visible = true;
			}
			else
			{
				Line.Visible = false;
			}
			eArgs.EOF = ShowFieldValues();
			if (eArgs.EOF)
			{
				GenerateAllTotals();
				// this will create the totals on the bottom of the report
			}
			else
			{
				intArrIndex += 1;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strSQL;
				clsDRWrapper rsTemp = new clsDRWrapper();
				int lngJN = 0;
				// this is the journal number that is returned
				bool boolAccountAdded = false;
				int lngCT;
				int lngNegCT;
				bool boolAltCashAccts;
				DateTime dtDate;
				// this is the latest date on any receipt that I am processing
				string strJDesc = "";
				// this is to pass the jounral desc back from a form
				string strTempAccount = "";
				string strTempDate = "";
				bool boolAddJournal = false;
				bool boolJournalEntryDesc = false;
				int lngInitialCashIndex = 0;
				bool boolContinue = false;
				string strBO = "";
				int lngEFT = 0;
				double dblCashFundCCAltCashTotal;
                var bdSetCont = new cBDSettingsController();
                cStandardAccounts stdAccts;

                if (StaticSettings.gGlobalActiveModuleSettings.BudgetaryIsActive)
                {
                    acctCont = new cBDAccountController();
                }

				boolUsePrevYears = frmDailyReceiptAudit.InstancePtr.boolUsePrevYears;

				// kk09092014 trocr-430
				// initialize the arrays
				FCUtils.EraseSafe(modGlobal.Statics.garrFundInfo);

				//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
				for (int i = 0; i < 99; i++)
				{
					modGlobal.Statics.garrFundInfo[i] = new modGlobal.FundMonies(0);
				}
				Array.Resize(ref ftEFTArray, 0 + 1);
                FTAccountFunds.Clear();
				frmWait.InstancePtr.IncrementProgress();
                if (modGlobalConstants.Statics.gboolBD)
                {
                    stdAccts = acctCont.GetStandardAccounts();
                    bdSets = bdSetCont.GetBDSettings();
                }
                else
                {
                    stdAccts = new cStandardAccounts();
                }
				dtDate = GetLastDate_2(DateTime.FromOADate(0));

				// This is True True with no alt cash accounts
				// this will get the SQL Statement for the T T Accounts
				// this will fill the ftAccountArray
				strSQL = GetSQL_2(0);
				boolUseBDJournal = false;
				boolFundArrayEmpty = true;
				rsData.OpenRecordset(strSQL, modExtraModules.strCRDatabase, 0, false);
				if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
				{
					boolFundArrayEmpty = false;
					ftAccountArray = new modBudgetaryAccounting.FundType[rsData.RecordCount() - 1 + 1];
					FillFundInfo_6(rsData, 0);
					// this function will actually add the accounts
					rsData.MoveFirst();
					if (lngAcctCT != Information.UBound(ftAccountArray, 1) + 1)
					{
						// check to make sure that there are no blank elements in the array
						if (lngAcctCT != 0)
						{
							Array.Resize(ref ftAccountArray, lngAcctCT - 1 + 1);
						}
					}
					else
					{
					}
					boolAccountAdded = false;
					boolFundArrayEmpty = false;
				}
				else
				{
					boolAccountAdded = true;
				}
				frmWait.InstancePtr.IncrementProgress();
				// **********************************************************************
				// This is False True with no alt cash accounts non EFT
				// this will get the SQL Statement for the F T Accounts
				// this will fill the ftAccountArray
				strSQL = GetSQL_2(1);
				rsData.OpenRecordset(strSQL);
				if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
				{
					boolFundArrayEmpty = false;
					if (!boolAccountAdded)
					{
						// keep rockin
					}
					else
					{
						ftAccountArray = new modBudgetaryAccounting.FundType[0 + 1];
						ftAccountArray[0].Init();
					}
					FillFundInfo_6(rsData, 1);
					rsData.MoveFirst();
					boolAccountAdded = false;
					boolFundArrayEmpty = false;
				}
				
				strSQL = GetSQL_2(7);
				rsData.OpenRecordset(strSQL);
				if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
				{
					boolFundArrayEmpty = false;
					if (!boolAccountAdded)
					{
						// keep rockin
					}
					else
					{
						ftAccountArray = new modBudgetaryAccounting.FundType[0 + 1];
						ftAccountArray[0].Init();
					}
					FillFundInfo_204(rsData, 1, true);
					rsData.MoveFirst();
					boolAccountAdded = false;
					boolFundArrayEmpty = false;
				}
				frmWait.InstancePtr.IncrementProgress();
				// **********************************************************************
				// This will find the cash entry
				if (!boolFundArrayEmpty)
				{
					if (modGlobalConstants.Statics.gboolBD)
					{
						// this sets up the cash accounts for the funds if the user has BD
						ftFundArray = modBudgetaryAccounting.CalcFundCash(ref ftAccountArray);
						
					}
					else
					{
						// create a cash entry into the account M CASH for these accounts
						ftFundArray = CashAccountForM(ref ftAccountArray);
					}
				}
				else
				{
					ftFundArray = new modBudgetaryAccounting.FundType[999 + 1];
				}
				// **********************************************************************
				// This is True True with alt cash accounts
				// this will get the SQL Statement for the T T Accounts
				// this will fill the ftAccountArray
				strSQL = GetSQL_68(0, true);
				rsData.OpenRecordset(strSQL, modExtraModules.strCRDatabase, 0, false);
				if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
				{
					boolAltCashAccts = true;
					if (!boolAccountAdded)
					{
						// keep rockin
					}
					else
					{
						ftAccountArray = new modBudgetaryAccounting.FundType[0 + 1];
						ftAccountArray[0].Init();
					}
					FillFundInfo_69(rsData, 0, true);
					// this function will actually add the accounts
					rsData.MoveFirst();
					if (lngAcctCT != Information.UBound(ftAccountArray, 1) + 1)
					{
						// check to make sure that there are no blank elements in the array
						if (lngAcctCT != 0)
						{
							Array.Resize(ref ftAccountArray, lngAcctCT - 1 + 1);
						}
					}
					else
					{
					}
					boolAccountAdded = false;
					boolFundArrayEmpty = false;
				}
				else
				{
					// boolAccountAdded = True
				}
				frmWait.InstancePtr.IncrementProgress();
				frmWait.InstancePtr.Unload();
				// **********************************************************************
				// This is False True with alt cash accounts
				// this will get the SQL Statement for the F T Accounts
				// this will fill the ftAccountArray
				strSQL = GetSQL_68(1, true);
				rsData.OpenRecordset(strSQL);
				if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
				{
					boolAltCashAccts = true;
					if (!boolAccountAdded)
					{
						// keep rockin
					}
					else
					{
						ftAccountArray = new modBudgetaryAccounting.FundType[0 + 1];
						ftAccountArray[0].Init();
					}
					FillFundInfo_69(rsData, 1, true);
					rsData.MoveFirst();
					boolAccountAdded = false;
					boolFundArrayEmpty = false;
				}
				// **********************************************************************
				// This is False False with no alt cash accounts
				// these are not included in the fund cash accounts...they will have thier own entry
				// this will get the accounts for the F F Accounts to be placed above the divider
				strSQL = GetSQL_2(2);
				rsData.OpenRecordset(strSQL);
				if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
				{
					if (!boolAccountAdded)
					{
						// keep rockin
					}
					else
					{
						ftAccountArray = new modBudgetaryAccounting.FundType[0 + 1];
						ftAccountArray[0].Init();
					}
					FillFundInfo_6(rsData, 2);
					rsData.MoveFirst();
					boolFundArrayEmpty = false;
				}
				if (boolFundArrayEmpty)
				{
					intLvl = 0;
					Array.Resize(ref ftAccountArray, 0 + 1);
					ftAccountArray[0].Init();
				}
				else
				{
					intLvl = Information.UBound(ftAccountArray, 1);
				}
				
				double dblTemp;
				if (modGlobalConstants.Statics.gboolBD)
				{
					
					strSQL = GetSQL_2(20);
					rsData.OpenRecordset(strSQL);
					if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
					{
						FillFundInfoWithAltCash_6(rsData, true);
					}
					if (FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("Due")) == "Y")
					{
						strSQL = GetSQL_68(5, true);
						rsData.OpenRecordset(strSQL);
						if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
						{
							// this will find the default cash account and save it for
							// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
							if (Conversion.Val(rsData.Get_Fields("SumTotal")) != 0)
							{
								rsTemp.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'CM'", "TWBD0000.vb1");
								if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
								{
									// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
									strDefaultCashAccount = "G " + modGlobal.PadToString_8(FCConvert.ToInt16(modGlobal.Statics.glngCashFund), FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + rsTemp.Get_Fields("Account") + "-" + "00";
								}
								else
								{
									MessageBox.Show("You must set up an Misc Cash Account before you may continue.", "Missing Cash Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
								}
							}
						}

						rsData.MoveFirst();
					}
					
					strSQL = GetSQL_2(3);
					rsData.OpenRecordset(strSQL);
					if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
					{
						FillFundInfo_6(rsData, 3);
						rsData.MoveFirst();
					}
					
					strSQL = GetSQL_2(4);
					rsData.OpenRecordset(strSQL, modExtraModules.strCRDatabase);
					if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
					{
						AddDefaultCashAccounts();
					}
					
					boolContinue = modBudgetaryAccounting.CalcDueToFrom(ref ftAccountArray, ref ftFundArray, false, Strings.Format(dtDate, "MM/dd/yyyy") + " C/R", boolFundArrayEmpty, true, "CR", "", false, lngInitialCashIndex);
					//if (modValidateAccount.Statics.gboolSchoolAccounts)
					//{
					//	boolContinue = boolContinue && modBudgetaryAccounting.CalcDueToFrom(ref ftAccountArray, ref ftFundArray2, false, Strings.Format(dtDate, "MM/dd/yyyy") + " C/R", boolFundArrayEmpty, true, "CR", "", modValidateAccount.Statics.gboolSchoolAccounts, lngInitialCashIndex);
					//}
					// kk10252016 trocr-467  Move this later in the process so we can adjust the cash accounts
					// **********************************************************************
					// This is the Seperate Credit Card Cash Account amounts
					// this will get the SQL Statement for the Seperate Credit Card Cash Account amounts
					// this will fill the ftAccountArray
					lngAcctCT = Information.UBound(ftAccountArray, 1) + 1;
					// kk10262016 trocr-467  CalcDueToFrom adds elements to the array - refresh the count
					strSQL = GetSQL_2(40);
					rsData.OpenRecordset(strSQL);
					if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
					{
						boolFundArrayEmpty = false;
						if (!boolAccountAdded)
						{
							// keep rockin
						}
						else
						{
							ftAccountArray = new modBudgetaryAccounting.FundType[0 + 1];
							ftAccountArray[0].Init();
						}
						AddSeperateCreditCardGLAccounts();
						// kk10262016 trocr-467 Make this a procedure call    'kk09092014 trocr-430
						rsData.MoveFirst();
						boolAccountAdded = false;
						boolFundArrayEmpty = false;
					}            
                    if (boolContinue)
                    {
                        if (!(lngInitialCashIndex > fecherFoundation.Information.UBound(ftAccountArray, 1)))
                        {
                            // vbPorter upgrade warning: ary As object	OnRead(cFundTotal)
                            cFundTotal[] ary = null;
                            // vbPorter upgrade warning: X As short	OnWrite(int)
                            short X;
                            int lngBound = 0;
                            cFundTotal ftItem;
                            string strAcct = "";
                            string strCshAccount = "";
                            int lngTempIndex = 0;
                            if (dblFTAccountTotals != 0 || dblFTAccountTotalsEFT != 0)
                            {
                                lngBound = FTAccountFunds.Count;
                                if (lngBound > 0)
                                {
                                    ary = FTAccountFunds.Values.ToArray();
                                    for (X = 0; X <= (short)(lngBound - 1); X++)
                                    {
                                        ftItem = ary[X];
                                        if (ftItem.Amount != 0)
                                        {
                                            strAcct = ftFundArray[ftItem.Fund].Account;
                                            strCshAccount = MakeCashAccount(ftItem.Fund, Convert.ToInt32(stdAccts.MiscCash), bdSets.Ledger);
                                            lngTempIndex = FindFundAccount(ref strCshAccount);
                                            ftAccountArray[lngTempIndex].Amount += Convert.ToDecimal(ftItem.Amount);
                                            Array.Resize(ref ftAccountArray, fecherFoundation.Information.UBound(ftAccountArray, 1) + 1 + 1);
                                            ftAccountArray[ftAccountArray.UBound()].Amount = Convert.ToDecimal(ftItem.Amount * -1);
                                            ftAccountArray[ftAccountArray.UBound()].Description = fecherFoundation.Strings.Format(dtDate, "MM/DD/YYYY") + " C/R - Prev Csh";
                                            ftAccountArray[ftAccountArray.UBound()].Account = strCshAccount;
                                            ftAccountArray[ftAccountArray.UBound()].AcctType = "C";
                                        }
                                    }
                                }
                                if (fecherFoundation.Information.UBound(ftEFTArray, 1) >= 0)
                                {
                                    int lngTempFund = 0;
                                    while (!(lngEFT >= fecherFoundation.Information.UBound(ftEFTArray, 1)))
                                    {
                                        if (ftEFTArray[lngEFT].Amount != 0)
                                        {
                                            // lngTempIndex = FindFundAccount(strAcct)
                                            if (strCshAccount == "")
                                            {
                                                lngTempFund = modBudgetaryAccounting.GetFundFromAccount( ftEFTArray[lngEFT].Account);
                                                strAcct = ftFundArray[lngTempFund].Account;
                                                if (ftFundArray[lngTempFund].UseDueToFrom)
                                                {
                                                    strCshAccount = MakeCashAccount(short.Parse(ftFundArray[lngTempFund].CashFund), Convert.ToInt32(stdAccts.MiscCash), bdSets.Ledger);
                                                }
                                                else
                                                {
                                                    strCshAccount = MakeCashAccount((short)lngTempFund, Convert.ToInt32(stdAccts.MiscCash), bdSets.Ledger);
                                                }
                                                lngTempIndex = FindFundAccount(ref strCshAccount);
                                            }
                                            else
                                            {
                                                lngTempIndex = FindFundAccount(ref strCshAccount);
                                            }
                                            ftAccountArray[lngTempIndex].Amount += ftEFTArray[lngEFT].Amount;
                                            Array.Resize(ref ftAccountArray, fecherFoundation.Information.UBound(ftAccountArray, 1) + 1 + 1);
                                            ftAccountArray[ftAccountArray.UBound()].Amount = ftEFTArray[lngEFT].Amount * -1;
                                            ftAccountArray[ftAccountArray.UBound()].Description = fecherFoundation.Strings.Format(dtDate, "MM/DD/YYYY") + " C/R - Cash EFT";
                                            ftAccountArray[ftAccountArray.UBound()].Account = ftAccountArray[lngTempIndex].Account;
                                            ftAccountArray[ftAccountArray.UBound()].AcctType = "C";
                                            lngEFT += 1;
                                        }
                                    }
                                }
                            }
                        }

                        // check to make sure the count of how many entries in the array are correct
                        if (lngAcctCT - 1 != fecherFoundation.Information.UBound(ftAccountArray, 1))
                        {
                            lngAcctCT += 1;
                        }

                        if (dblMFundTotal != 0)
                        { // only run this if the M total is not zero
                            if (AddCashAccountforM(ref ftAccountArray, ref ftFundArray,false, dblMFundTotal))
                            {

                            }
                        }

                        //if (frmDailyReceiptAudit.InstancePtr.optAudit[1].Checked == true)
                        if (frmDailyReceiptAudit.InstancePtr.cmbAudit.Text == "Print Audit Report and Close Out" || frmDailyReceiptAudit.InstancePtr.cmbAudit.Text == "Finalize Partial Close Out")
                        { // if this is a real audit then close it out
                            lngJN = frmDailyReceiptAudit.InstancePtr.lngJournalNumber;

                            // set all of the values in the array to negative
                            for (lngNegCT = 0; lngNegCT <= fecherFoundation.Information.UBound(ftAccountArray, 1); lngNegCT++)
                            {
                                ftAccountArray[lngNegCT].Amount *= -1;
                            }

                            strJDesc = fecherFoundation.Strings.Format(dtDate, "MM/DD/YYYY") + " C/R";

                            // show the date and description form
                            frmGetJournalDesc.InstancePtr.Init(ref dtDate, ref strJDesc, ref boolJournalEntryDesc); // this will check with the user to make sure that the info is correct
                                                                                                                    // set it back to the hourglass
                           

                            if (lngJN != 0)
                            {
                                // this is when the user does not want to add a new journal
                                modGlobalFunctions.AddCYAEntry("CR", "Daily Audit added to Journal " + FCConvert.ToString(lngJN) + ".","","","","");
                            }

                            for (lngCT = 0; lngCT <= fecherFoundation.Information.UBound(ftAccountArray, 1); lngCT++)
                            {
                                if (boolJournalEntryDesc)
                                {
                                    if (fecherFoundation.Strings.Left(ftAccountArray[lngCT].Account, 1) != "M" && ftAccountArray[lngCT].Account != "")
                                    {
                                        // Add this journal
                                        if (ftAccountArray[lngCT].Amount != 0)
                                        {
                                            boolAddJournal = true;
                                        }
                                    }
                                    if (ftAccountArray[lngCT].Description == "")
                                    {
                                        ftAccountArray[lngCT].Description = strJDesc;
                                    }
                                    else if (ftAccountArray[lngCT].AcctType == "C")
                                    {

                                    }
                                }
                                else
                                {
                                    if (fecherFoundation.Strings.Left(ftAccountArray[lngCT].Account, 1) != "M" && ftAccountArray[lngCT].Account != "")
                                    {
                                        // Add this journal
                                        if (ftAccountArray[lngCT].Amount != 0)
                                        {
                                            boolAddJournal = true;
                                        }
                                    }
                                    if (ftAccountArray[lngCT].Description == "")
                                    {
                                        ftAccountArray[lngCT].Description = fecherFoundation.Strings.Format(dtDate, "MM/DD/YYYY") + " C/R";
                                    }
                                    else if (ftAccountArray[lngCT].AcctType == "C")
                                    {
                                        if (fecherFoundation.Strings.Left(ftAccountArray[lngCT].Account, 1) != "M" && ftAccountArray[lngCT].Account != "")
                                        {
                                            // Add this journal
                                            boolAddJournal = true;
                                            if (ftAccountArray[lngCT].Description.Length > 11)
                                            {
                                                ftAccountArray[lngCT].Description = fecherFoundation.Strings.Format(dtDate, "MM/DD/YYYY") + fecherFoundation.Strings.Right(ftAccountArray[lngCT].Description, 11);
                                            }
                                        }
                                    }
                                }
                            }

                            if (boolAddJournal)
                            {
                                // journal needs to be posted, this routine will do it
                                lngJN = modBudgetaryAccounting.AddToJournal(ref ftAccountArray, "CW",  lngJN,  dtDate,  strJDesc, Convert.ToInt32(fecherFoundation.Strings.Left(frmDailyReceiptAudit.InstancePtr.cmbMonth.Items[frmDailyReceiptAudit.InstancePtr.cmbMonth.SelectedIndex].ToString(), 2)), !boolUseBDJournal,  frmDailyReceiptAudit.InstancePtr.lngCloseoutKey);
                            }

                            // set all of the values in the array to negative
                            for (lngNegCT = 0; lngNegCT <= fecherFoundation.Information.UBound(ftAccountArray, 1); lngNegCT++)
                            {
                                ftAccountArray[lngNegCT].Amount *= -1;
                            }

                            lblJournal.Visible = true;

                            if (lngJN == 0)
                            {
                                lblJournal.Text = "No BD journal was created.";
                            }
                            else
                            {
                                lblJournal.Text = "Journal Number: " + FCConvert.ToString(lngJN) + " - " + fecherFoundation.Strings.Format(dtDate, "MM/DD/YYYY") + " C/R" + "\r\n";
                                lblJournal.Text = lblJournal.Text + "Accounting Period: " + fecherFoundation.Strings.Right(frmDailyReceiptAudit.InstancePtr.cmbMonth.Items[frmDailyReceiptAudit.InstancePtr.cmbMonth.SelectedIndex].ToString(), frmDailyReceiptAudit.InstancePtr.cmbMonth.Items[frmDailyReceiptAudit.InstancePtr.cmbMonth.SelectedIndex].ToString().Length - 4);

                                // kk01282015 trocrs-32  Add option to automatically post system generated journals
                                if (modSecurity.ValidPermissions(null, modGlobal.CRSECURITYAUTOPOSTJOURNAL, false) && modBudgetaryAccounting.AutoPostAllowed( modBudgetaryAccounting.AutoPostType.aptCRDailyAudit))
                                {
                                    clsPostingJournalInfo tJournalInfo = new/*AsNew*/ clsPostingJournalInfo();

                                    tJournalInfo.JournalNumber = lngJN;
                                    tJournalInfo.CheckDate = "";
                                    tJournalInfo.JournalType = "CW";
                                    tJournalInfo.Period = FCConvert.ToString(Convert.ToInt32(double.Parse(fecherFoundation.Strings.Left(frmDailyReceiptAudit.InstancePtr.cmbMonth.Items[frmDailyReceiptAudit.InstancePtr.cmbMonth.SelectedIndex].ToString(), 2))));
                                    
                                    modGlobal.Statics.tPostInfo.AddJournal( tJournalInfo);
                                    tJournalInfo = null;
                                }
                            }                          
                        }
                    }
                    else
                    {
                        frmWait.InstancePtr.Unload();
                        MessageBox.Show("Error during the calculation of Due To/From entries.  No Journal has been created and will have to be manually entered.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    }
                }
				else
				{
					// create the cash account for all M accounts
					if (AddCashAccountforM(ref ftAccountArray, ref ftFundArray))
					{
					}
					// check for F T Payments that are for previous periods and add a default cash account entry for that amount
					strSQL = GetSQL_2(5);
					rsData.OpenRecordset(strSQL);
					if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
					{
						// this will find the default cash account and save it for
						if (modGlobalConstants.Statics.gboolBD)
						{
							rsTemp.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'CM'", "TWBD0000.vb1");
							if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
							{
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								strDefaultCashAccount = "G " + FCConvert.ToString(modGlobal.Statics.glngCashFund) + "-" + rsTemp.Get_Fields("Account") + "-" + "00";
							}
							else
							{
								frmWait.InstancePtr.Unload();
								MessageBox.Show("You must set up an Misc Cash Account before you may continue.", "Missing Cash Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
						}
						else
						{
							strDefaultCashAccount = "CASH";
						}
						// this could be a problem of overwriting M Cash account
						FillFundInfo_6(rsData, 4);
						rsData.MoveFirst();
					}
					// this will get the accounts for the F F Accounts grouped by Default Account to be put at the bottom of the account list
					strSQL = GetSQL_2(3);
					rsData.OpenRecordset(strSQL);
					if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
					{
						FillFundInfo_6(rsData, 3);
						rsData.MoveFirst();
					}
					// check for any accounts that have a default cash account and append them at the end of the array...
					// also subtract the amounts from the fund that they are associated with
					strSQL = GetSQL_2(4);
					rsData.OpenRecordset(strSQL, modExtraModules.strCRDatabase);
					if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
					{
						AddDefaultCashAccounts();
					}
					// USL Export
					if (modGlobal.Statics.gboolUseUSLExport)
					{
						// strTempDate = frmDateChange.Init("Please enter the date for the USL Export", 0, Date)
						CreateUSLExport_6( ftAccountArray, DateTime.Today);
						// This date will probably need to be entered manually
					}
				}
                    if ((modGlobalConstants.Statics.MuniName.ToLower() == "mt. desert" || modGlobalConstants.Statics.MuniName.ToLower() == "westbrook") &&
                        frmDailyReceiptAudit.InstancePtr.cmbAudit.Text == "Print Audit Report and Close Out")
                {
                    CreateMunisExport(ref ftAccountArray,
                        Convert.ToInt32(frmDailyReceiptAudit.InstancePtr.cmbMonth
                            .List[frmDailyReceiptAudit.InstancePtr.cmbMonth.ListIndex].ToString().Substring(0, 2)), dtDate);
                }
				SetupFields();
				// Line1.X2 = sarCashReceiptsSummary.PrintWidth
				fldCurCash.Text = "0.00";
				fldTotalCash.Text = "0.00";
				fldNonCash.Text = "0.00";
				// GenerateAllTotals
				rsTemp.DisposeOf();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				if (Information.Err(ex).Number != 9)
				{
					MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + " has occured.", "Accounting Summary", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				else
				{
					// this is the subscript out of range from the UBound on an empty array
				}
			}
		}

		private bool AutomaticCheckRecEntries(ref modBudgetaryAccounting.FundType[] Funds, ref DateTime dtDate)
		{
			bool AutomaticCheckRecEntries = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsBank = new clsDRWrapper();
				string strSQL;
				// vbPorter upgrade warning: dblFundAmount As double	OnWriteFCConvert.ToDecimal(
				double dblFundAmount = 0;
				// vbPorter upgrade warning: dblEFTAmount As double	OnWrite(Decimal, short)
				double[] dblEFTAmount = new double[100 + 1];
				int lngBankNumber = 0;
				int lngDefaultBank;
				int lngFundNumber = 0;
				int lngCT;
				bool boolBankNumberFound;
				// vbPorter upgrade warning: boolEFT As bool	OnWriteFCConvert.ToInt16(
				bool boolEFT = false;
				bool boolDueToDueFrom = false;
				// vbPorter upgrade warning: dblTotalEFT As double	OnWriteFCConvert.ToDecimal(
				double dblTotalEFT = 0;
				double dblTotalAmount;
				if (FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("DUE")) == "Y")
				{
					boolDueToDueFrom = true;
				}
				else
				{
					boolDueToDueFrom = false;
				}
				AutomaticCheckRecEntries = true;
				strSQL = GetSQL_2(10);
				// EFT for the CheckRec
				rsBank.OpenRecordset(strSQL, modExtraModules.strCRDatabase, 0, false);
				// fill the array with all the
				if (rsBank.EndOfFile() != true && rsBank.BeginningOfFile() != true)
				{
					ftCheckRecEntries = new modBudgetaryAccounting.FundType[rsBank.RecordCount() - 1 + 1];
					FillCheckRecEntries_6( rsBank, 0);
					boolEFT = FCConvert.ToBoolean(1);
				}
				rsBank.OpenRecordset("SELECT * FROM CheckRecMaster WHERE ID = 0", modExtraModules.strBDDatabase);
				if (boolEFT)
				{
					// add the check rec entries for the EFTs
					for (lngCT = 0; lngCT <= Information.UBound(ftCheckRecEntries, 1); lngCT++)
					{
						if (ftCheckRecEntries[lngCT].Amount != 0)
						{
							// this returns the fund
							lngBankNumber = modBudgetaryAccounting.GetFundFromAccount(ftCheckRecEntries[lngCT].Account);
							// this will set the actual bank number and combine the entries for banks
							if (boolDueToDueFrom)
							{
								lngBankNumber = GetBankNumber_2(1);
								dblTotalEFT += FCConvert.ToDouble(ftCheckRecEntries[lngCT].Amount);
							}
							else
							{
								lngBankNumber = GetBankNumber(ref lngBankNumber);
							}
							// add up the amount put into the EFT in order to take it out of the fund amount
							dblEFTAmount[lngBankNumber] += FCConvert.ToDouble(ftCheckRecEntries[lngCT].Amount);
							rsBank.AddNew();
							rsBank.Set_Fields("CheckNumber", 0);
							rsBank.Set_Fields("Type", 7);
							rsBank.Set_Fields("CheckDate", dtDate);
							rsBank.Set_Fields("Name", "FROM C/R - " + Strings.Format(dtDate, "MM/dd/yyyy"));
							rsBank.Set_Fields("Amount", ftCheckRecEntries[lngCT].Amount);
							rsBank.Set_Fields("Status", 1);
							rsBank.Set_Fields("StatusDate", dtDate);
							rsBank.Set_Fields("BankNumber", lngBankNumber);
							rsBank.Update();
							// strSQL = "INSERT INTO CheckRecMaster (CheckNumber, Type, CheckDate, Name, Amount, Status, StatusDate, BankNumber) VALUES (0,7,#" & dtDate & "#, 'FROM C/R - " & Format(dtDate, "MM/dd/yyyy") & "'," & ftCheckRecEntries(lngCT).Amount & ",1,#" & dtDate & "#," & lngBankNumber & ")"
							// rsBank.Execute strSQL, strBDDatabase
						}
					}
				}
				// If boolDueToDueFrom Then
				for (lngCT = 1; lngCT <= Information.UBound(Funds, 1); lngCT++)
				{
					// check each of the fund cash totals
					// get the fund number and the fund amount
					if (Funds[lngCT].Amount != 0 && Funds[lngCT].AcctType == "FUND")
					{
						dblFundAmount += FCConvert.ToDouble(Funds[lngCT].Amount);
						lngFundNumber = lngCT;
					}
				}
				// find the bank number for this fund
				// check the Banks table in BD to find the correct bank for this fund
				lngBankNumber = GetBankNumber_2(1);
				if (boolDueToDueFrom)
				{
					lngBankNumber = GetBankNumber_2(1);
				}
				else
				{
					lngBankNumber = GetBankNumber(ref lngFundNumber);
				}
				rsBank.AddNew();
				rsBank.Set_Fields("CheckNumber", 0);
				rsBank.Set_Fields("Type", 3);
				rsBank.Set_Fields("CheckDate", dtDate);
				rsBank.Set_Fields("Name", "Deposit (Auto)");
				rsBank.Set_Fields("Amount", dblFundAmount - dblTotalEFT - dblFTAccountTotals);
				dblEFTAmount[lngBankNumber] = 0;
				rsBank.Set_Fields("Status", 1);
				rsBank.Set_Fields("StatusDate", dtDate);
				rsBank.Set_Fields("BankNumber", lngBankNumber);
				rsBank.Update();
				if (dblFTAccountTotals != 0)
				{
					rsBank.AddNew();
					rsBank.Set_Fields("CheckNumber", 0);
					rsBank.Set_Fields("Type", 3);
					rsBank.Set_Fields("CheckDate", dtDate);
					rsBank.Set_Fields("Name", "Deposit (Auto) Previous");
					rsBank.Set_Fields("Amount", dblFTAccountTotals);
					dblEFTAmount[lngBankNumber] = 0;
					rsBank.Set_Fields("Status", 1);
					rsBank.Set_Fields("StatusDate", dtDate);
					rsBank.Set_Fields("BankNumber", lngBankNumber);
					rsBank.Update();
				}
				return AutomaticCheckRecEntries;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				AutomaticCheckRecEntries = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Automatic Check Reconciliation Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return AutomaticCheckRecEntries;
		}

		private int GetBankNumber_2(int lngFundNum)
		{
			return GetBankNumber(ref lngFundNum);
		}

		private int GetBankNumber(ref int lngFundNum)
		{
			int GetBankNumber = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will go to the banks table and return
				// the correct bank number for the fund passed in
				// Default to bank #1 if anything goes wrong
				clsDRWrapper rsBank = new clsDRWrapper();
				if (FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("DUE")) == "Y")
				{
					rsBank.OpenRecordset("SELECT * FROM BankIndex", "CentralData");
					if (rsBank.EndOfFile())
					{
						GetBankNumber = 1;
					}
					else
					{
						if (Conversion.Val("0" + rsBank.Get_Fields_Int16("CRBank")) != 0)
						{
							GetBankNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(rsBank.Get_Fields_Int16("CRBank"))));
						}
						else
						{
							GetBankNumber = 1;
						}
					}
				}
				else
				{
					rsBank.OpenRecordset("SELECT * FROM FundBanks", modExtraModules.strCRDatabase);
					if (rsBank.EndOfFile())
					{
						GetBankNumber = 1;
					}
					else
					{
						rsBank.FindFirstRecord("Fund", lngFundNum);
						if (rsBank.NoMatch)
						{
							GetBankNumber = 1;
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
							GetBankNumber = FCConvert.ToInt32(rsBank.Get_Fields("BankNumber"));
							if (GetBankNumber == 0)
							{
								GetBankNumber = 1;
							}
						}
					}
				}
				return GetBankNumber;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				GetBankNumber = 1;
				// default to bank number 1
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Get Bank Number Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetBankNumber;
		}

		private void SetupFields()
		{
			float PrtWid;
			// sarCashReceiptsSummary.PrintWidth = arDailyAuditReport.sarCRSummaryOB.Width
			PrtWid = sarAccountingSummary.InstancePtr.PrintWidth;
			fldAmount.Left = FCConvert.ToSingle(PrtWid * 0.33333);
			lblAmount.Left = FCConvert.ToSingle(PrtWid * 0.33333);
			fldCD.Left = fldAmount.Left + fldAmount.Width;
			fldDescription.Left = PrtWid - fldDescription.Width;
			lblDescription.Left = PrtWid - lblDescription.Width;
		}

		private void FillFundInfo_6(clsDRWrapper rsF, int lngTemp, bool boolOverWrite = false, bool boolAltCashAcct = false, bool boolEFT = false)
		{
			FillFundInfo(ref rsF, ref lngTemp, boolOverWrite, boolAltCashAcct, boolEFT);
		}

		private void FillFundInfo_69(clsDRWrapper rsF, int lngTemp, bool boolAltCashAcct = false, bool boolEFT = false)
		{
			FillFundInfo(ref rsF, ref lngTemp, false, boolAltCashAcct, boolEFT);
		}

		private void FillFundInfo_204(clsDRWrapper rsF, int lngTemp, bool boolEFT = false)
		{
			FillFundInfo(ref rsF, ref lngTemp, false, false, boolEFT);
		}

		private void FillFundInfo(ref clsDRWrapper rsF, ref int lngTemp, bool boolOverWrite = false, bool boolAltCashAcct = false, bool boolEFT = false)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsWOInt = new clsDRWrapper();
				// this will fill the recordset's Department field with the fund to be grouped on
				while (!(rsF.EndOfFile() || rsF.BeginningOfFile()))
				{
					if (lngTemp != 4)
					{
						if (lngTemp == 1)
						{
							// if it is a F T
							if (boolEFT)
							{
								Array.Resize(ref ftEFTArray, Information.UBound(ftEFTArray, 1) + 1 + 1);
								// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
								ftEFTArray[Information.UBound(ftEFTArray) - 1].Amount = FCConvert.ToDecimal(rsF.Get_Fields("SumTotal"));
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								ftEFTArray[Information.UBound(ftEFTArray) - 1].Account = rsF.Get_Fields_String("Account");
								// TODO Get_Fields: Field [Proj] not found!! (maybe it is an alias?)
								ftEFTArray[Information.UBound(ftEFTArray) - 1].Project = FCConvert.ToString(rsF.Get_Fields("Proj"));
								// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
								dblFTAccountTotalsEFT += FCConvert.ToDouble(rsF.Get_Fields("SumTotal"));
								// this will save the total of the F T that should be subracted from the cash account and added to the second entry of the cash account
							}
							else
							{
								// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
								dblFTAccountTotals += FCConvert.ToDouble(rsF.Get_Fields("SumTotal"));
								// this will save the total of the F T that should be subracted from the cash account and added to the second entry of the cash account
							}
						}
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (Strings.InStr(1, rsF.Get_Fields("Account"), "_", CompareConstants.vbBinaryCompare) == 0 && rsF.Get_Fields("Account").Length > 2)
						{
							// check to make sure that this is not an "M D" account
							// if it is...do not allow it to add its totals to the accounts...
							// it needs to have its own cash account and no entries in the BD system
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							if (Strings.Trim(rsF.Get_Fields("Account")) == "M D")
							{
								// add the M D total to be added at the end of the accounting summary
								// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
								dblM_DTotals = FCConvert.ToDouble(rsF.Get_Fields("SumTotal"));
								// and skip the rest of this function for this type of account only
							}
							else
							{
								// this will add the account summary to ftAccountArray
								if (lngTemp == 3)
								{
									// if it is a default cash account, it has to have a negative balance so that it is set to DB
									// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
									// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
									// TODO Get_Fields: Field [Proj] not found!! (maybe it is an alias?)
									AddAccount_24(FCConvert.ToString(rsF.Get_Fields("Account")), (Conversion.Val(rsF.Get_Fields("SumTotal")) + Conversion.Val(rsF.Get_Fields("Interest"))) * -1, Strings.Trim(rsF.Get_Fields("Proj")), lngTemp, boolOverWrite);
								}
								else if (lngTemp == 30)
								{
									// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
									if (Conversion.Val(rsF.Get_Fields("SumTotal")) != 0)
									{
										// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
										// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
										AddAccount_24(FCConvert.ToString(rsF.Get_Fields("Account")), Conversion.Val(rsF.Get_Fields("SumTotal")), "", 0, boolOverWrite);
										// this should collect just the M accounts so that I can calculate the end fund balance
										// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
										if (Strings.Left(FCConvert.ToString(rsF.Get_Fields("Account")), 1) == "M" && lngTemp != 2 && !boolAltCashAcct)
										{
											// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
											dblMFundTotal += FCConvert.ToDouble(rsF.Get_Fields("SumTotal"));
										}
									}
								}
								else
								{
									// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
									if (Conversion.Val(rsF.Get_Fields("SumTotal")) != 0)
									{
										// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
										// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
										// TODO Get_Fields: Field [Proj] not found!! (maybe it is an alias?)
										AddAccount_24(FCConvert.ToString(rsF.Get_Fields("Account")), Conversion.Val(rsF.Get_Fields("SumTotal")), Strings.Trim(rsF.Get_Fields("Proj")), lngTemp, boolOverWrite);
										// this should collect just the M accounts so that I can calculate the end fund balance
										// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
										if (Strings.Left(FCConvert.ToString(rsF.Get_Fields("Account")), 1) == "M" && lngTemp != 2 && !boolAltCashAcct)
										{
											// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
											dblMFundTotal += FCConvert.ToDouble(rsF.Get_Fields("SumTotal"));
										}
									}
								}
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								if (Strings.Left(FCConvert.ToString(rsF.Get_Fields("Account")), 1) != "M")
								{
									boolUseBDJournal = true;
								}
								boolOverWrite = false;
							}
						}
						else
						{
							// Stop
						}
					}
					else
					{
						// this should always have only one entry...F T Payments
						// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
						if (FCConvert.ToString(rsF.Get_Fields("SumTotal")) != "" && modGlobalConstants.Statics.gboolBD)
						{
							// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [Proj] not found!! (maybe it is an alias?)
							AddAccount_24(strDefaultCashAccount, Conversion.Val(rsF.Get_Fields("SumTotal")) * -1, Strings.Trim(rsF.Get_Fields("Proj")), lngTemp, boolOverWrite);
						}
					}
					rsF.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Fill Fund Info Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillFundInfoWithAltCash_6(clsDRWrapper rsF, bool boolAltCashAcct = false)
		{
			FillFundInfoWithAltCash(ref rsF, boolAltCashAcct);
		}

		private void FillFundInfoWithAltCash(ref clsDRWrapper rsF, bool boolAltCashAcct = false)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngFund;
				bool boolAddDTDFEntries;
				clsDRWrapper rsFundInfo = new clsDRWrapper();
				int lngMainFund;
				// vbPorter upgrade warning: curTotal As Decimal	OnWrite(short, Decimal)
				Decimal curTotal;
				// new fund array for alt cash accounts
				ftAltCashFundArray = new modBudgetaryAccounting.FundType[999 + 1];
				curTotal = 0;
				while (!(rsF.EndOfFile() || rsF.BeginningOfFile()))
				{
					boolAddDTDFEntries = false;
					// AddAccount rsF.Fields("Account"), Val(rsF.Fields("SumTotal")) * -1, 4
					// TODO Get_Fields: Field [Amt] not found!! (maybe it is an alias?)
					if (Conversion.Val(rsF.Get_Fields("Amt")) != 0)
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Field [Amt] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [Proj] not found!! (maybe it is an alias?)
						AddAccount_24(FCConvert.ToString(rsF.Get_Fields("Account")), Conversion.Val(rsF.Get_Fields("Amt")), Strings.Trim(rsF.Get_Fields("Proj")), 0);
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (Strings.Left(FCConvert.ToString(rsF.Get_Fields("Account")), 1) != "M")
						{
							boolUseBDJournal = true;
						}
						// TODO Get_Fields: Field [Amt] not found!! (maybe it is an alias?)
						curTotal += FCConvert.ToDecimal(rsF.Get_Fields("Amt"));
					}
					rsF.MoveNext();
				}
				if (curTotal != 0)
				{
					intLvl = Information.UBound(ftAccountArray, 1);
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Fill Fund Info Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private string GetSQL_2(short i, bool boolAffectCashDrawer = true, bool boolAffectCash = true, bool boolUseDefaultCashAccount = false, bool boolShowETF = false)
		{
			return GetSQL(ref i, boolAffectCashDrawer, boolAffectCash, boolUseDefaultCashAccount, boolShowETF);
		}

		private string GetSQL_68(short i, bool boolUseDefaultCashAccount = false, bool boolShowETF = false)
		{
			return GetSQL(ref i, true, true, boolUseDefaultCashAccount, boolShowETF);
		}

		private string GetSQL(ref short i, bool boolAffectCashDrawer = true, bool boolAffectCash = true, bool boolUseDefaultCashAccount = false, bool boolShowETF = false)
		{
			string GetSQL = "";
			// this will create a query def if needed, but will return the correct sql statement either way
			int j;
			string strTemp = "";
			string strAltCashAcct = "";
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strEFT = "";
			string strArchiveTable = "";
			if (boolShowETF)
			{
				strEFT = " AND EFT = 1";
			}
			else
			{
				strEFT = " AND EFT <> 1";
			}
			if (boolUseDefaultCashAccount)
			{
				strAltCashAcct = " AND rTRIM(isnull(DefaultCashAccount, '')) <> ''";
			}
			else
			{
				strAltCashAcct = " AND rTRIM(isnull(DefaultCashAccount, '')) = ''";
			}

			if (boolUsePrevYears)
			{
				strArchiveTable = "LastYearArchive";
			}
			else
			{
				strArchiveTable = "Archive";
			}
			// this will get ALL accounts that are not closed out and group them by the account number
			switch (i)
			{
				case 0:
					{
						// this will get all accounts where AffectCashDrawer = True
						for (j = 1; j <= 6; j++)
						{
							switch (j)
							{
								case 1:
									{
										strTemp = "(SELECT Account1 as Account, Amount1 as Amt, rtrim(Project1) as Proj FROM " + strArchiveTable + " WHERE AffectCashDrawer = 1 AND" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strAltCashAcct + ") ";
										break;
									}
								default:
									{
										strTemp += "UNION ALL (SELECT Account" + FCConvert.ToString(j) + " as Account, Amount" + FCConvert.ToString(j) + " as Amt, Project" + FCConvert.ToString(j) + " as Proj FROM " + strArchiveTable + " WHERE AffectCashDrawer = 1 AND" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strAltCashAcct + ") ";
										break;
									}
							}
							//end switch
						}
						GetSQL = "SELECT Account, Proj, sum(Amt) as SumTotal FROM (" + strTemp + ") as CashDrawerAccounts WHERE Amt <> 0 GROUP BY Account, Proj";
						break;
					}
				case 1:
					{
						// this will get all accounts where AffectCashDrawer = False and AffectCash = True
						for (j = 1; j <= 6; j++)
						{
							switch (j)
							{
								case 1:
									{
										strTemp = "(SELECT Account1 as Account, Amount1 as Amt, Project1 as Proj FROM " + strArchiveTable + " WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strAltCashAcct + " AND AffectCashDrawer = 0 AND AffectCash = 1" + strEFT + ") ";
										// XXXX 'ReceiptType BETWEEN 90 AND 95 AND
										break;
									}
								default:
									{
										strTemp += "UNION ALL (SELECT Account" + FCConvert.ToString(j) + " as Account, Amount" + FCConvert.ToString(j) + " as Amt, Project" + FCConvert.ToString(j) + " as Proj FROM " + strArchiveTable + " WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strAltCashAcct + " AND AffectCashDrawer = 0 AND AffectCash = 1" + strEFT + ") ";
										// XXXX   'ReceiptType BETWEEN 90 AND 95 AND
										break;
									}
							}
							//end switch
						}
						GetSQL = "SELECT Account, Proj, sum(Amt) as SumTotal FROM (" + strTemp + ") as NonCashDrawerAccounts WHERE Amt <> 0 GROUP BY Account, Proj";
						break;
					}
				case 2:
					{
						// this will get all accounts where AffectCashDrawer = False and AffectCash = False
						// that are between 90-96 or 890 or 891 and
						// kgk trocr-277 04-25-11  include 893-896
						// kgk trocrs-21 09-20-13  include 97
						for (j = 1; j <= 6; j++)
						{
							switch (j)
							{
								case 1:
									{
										strTemp = "(SELECT Account1 as Account, Amount1 as Amt, Project1 as Proj FROM " + strArchiveTable + " WHERE (ReceiptType BETWEEN 90 AND 97 OR ReceiptType = 890 OR ReceiptType = 891 OR ReceiptType BETWEEN 893 AND 896) AND" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strAltCashAcct + " AND AffectCashDrawer = 0 AND AffectCash = 0) ";
										break;
									}
								case 2:
									{
										strTemp += "UNION ALL (SELECT Account" + FCConvert.ToString(j) + " as Account, Amount" + FCConvert.ToString(j) + " as Amt, Project" + FCConvert.ToString(j) + " as Proj FROM " + strArchiveTable + " WHERE (ReceiptType BETWEEN 90 AND 97 OR ReceiptType = 890 OR ReceiptType = 891 OR ReceiptType BETWEEN 893 AND 896) AND" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strAltCashAcct + " AND AffectCashDrawer = 0 AND AffectCash = 0 ) ";
										// AND Amount2 < 0
										break;
									}
								default:
									{
										strTemp += "UNION ALL (SELECT Account" + FCConvert.ToString(j) + " as Account, Amount" + FCConvert.ToString(j) + " as Amt, Project" + FCConvert.ToString(j) + " as Proj FROM " + strArchiveTable + " WHERE (ReceiptType BETWEEN 90 AND 97 OR ReceiptType = 890 OR ReceiptType = 891 OR ReceiptType BETWEEN 893 AND 896) AND" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strAltCashAcct + " AND AffectCashDrawer = 0 AND AffectCash = 0) ";
										break;
									}
							}
							//end switch
						}
						GetSQL = "SELECT Account, Proj, sum(Amt) as SumTotal FROM (" + strTemp + ") as NonCashDrawerAccounts WHERE Amt <> 0 GROUP BY Account, Proj";
						break;
					}
				case 3:
					{
						// this will get all accounts that are between 90-96 or 890 or 891 AND Affect Cash = False AND Cash Drawer = False and have a DefaultAccount to use to pay with
						// kgk trocr-277 04-25-11  include 893-896
						// kgk trocrs-21 09-20-13  include 97
						// this needs to know which amount is the interest/principal amount to see if there is any other type of money coming in/out
						strTemp = "SELECT Sum(Amount1 + Amount3 + Amount4 + Amount5 + Amount6) as SumTotal , SUM(Amount2) AS Interest, DefaultAccount as Account, '' as Proj FROM " + strArchiveTable + " WHERE ((ReceiptType BETWEEN 90 AND 97 OR ReceiptType = 890 OR ReceiptType = 891 OR ReceiptType BETWEEN 893 AND 896) AND " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND AffectCashDrawer = 0 AND AffectCash = 0) AND (Amount1 <> 0 OR Amount2 <> 0 OR Amount3 <> 0 OR Amount4 <> 0 OR Amount5 <> 0 OR Amount6 <> 0) AND DefaultAccount <> '' GROUP BY DefaultAccount";
						GetSQL = strTemp;
						break;
					}
				case 4:
					{
						// get any accounts that have a default cash account
						for (j = 1; j <= 6; j++)
						{
							switch (j)
							{
								case 1:
									{
										strTemp = "(SELECT Account1 as Account, Amount1 as Amt, Project1 as Proj, DefaultCashAccount FROM " + strArchiveTable + " WHERE DefaultCashAccount <> '' AND " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL;
										break;
									}
								default:
									{
										strTemp += " UNION ALL (SELECT Account" + FCConvert.ToString(j) + " as Account, Amount" + FCConvert.ToString(j) + " as Amt, Project" + FCConvert.ToString(j) + " as Proj, DefaultCashAccount FROM " + strArchiveTable + " WHERE DefaultCashAccount <> '' AND" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL;
										break;
									}
							}
							//end switch
						}
						strTemp += "))))))";
						GetSQL = "SELECT sum(Amt) as SumTotal, DefaultCashAccount FROM (" + strTemp + ") as AlternativeCashAccounts WHERE Amt <> 0 GROUP BY DefaultCashAccount";
						break;
					}
				case 5:
					{
						// , Account1, Account2, Account3, Account4, Account5, Account6
						GetSQL = "SELECT SUM(Amount1 + Amount2 + Amount3 + Amount4 + Amount5 + Amount6) AS SumTotal FROM (SELECT * FROM " + strArchiveTable + " WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strAltCashAcct + " AND AffectCashDrawer = 0 AND AffectCash = 1 AND EFT = 0) as temp";
						break;
					}
				case 6:
					{
						for (j = 1; j <= 6; j++)
						{
							switch (j)
							{
								case 1:
									{
										strTemp = "(SELECT DefaultCashAccount AS Account, Amount1 AS Amt FROM " + strArchiveTable + " WHERE ((ReceiptType NOT BETWEEN 90 AND 97) OR (AffectCashDrawer = 1 OR AffectCashDrawer = 0 AND AffectCash = 1)) AND" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strAltCashAcct + ") ";
										break;
									}
								default:
									{
										strTemp += "UNION ALL (SELECT DefaultCashAccount AS Account, Amount" + FCConvert.ToString(j) + " AS Amt FROM " + strArchiveTable + " WHERE ((ReceiptType NOT BETWEEN 90 AND 97) OR (AffectCashDrawer = 1 OR AffectCashDrawer = 0 AND AffectCash = 1)) AND" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strAltCashAcct + ") ";
										break;
									}
							}
							//end switch
						}
						GetSQL = "SELECT Account, sum(Amt) as SumTotal FROM (" + strTemp + ") as AltCashAccounts WHERE Amt <> 0 GROUP BY Account";
						break;
					}
				case 7:
					{
						// Get all accounts with Affect Cash = False and Cash Drawer = True EFT
						for (j = 1; j <= 6; j++)
						{
							switch (j)
							{
								case 1:
									{
										strTemp = "(SELECT Account1 as Account, Amount1 as Amt, Project1 as Proj FROM " + strArchiveTable + " WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strAltCashAcct + " AND AffectCashDrawer = 0 AND AffectCash = 1 AND EFT = 1) ";
										break;
									}
								default:
									{
										strTemp += "UNION ALL (SELECT Account" + FCConvert.ToString(j) + " as Account, Amount" + FCConvert.ToString(j) + " as Amt, Project" + FCConvert.ToString(j) + " as Proj FROM " + strArchiveTable + " WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strAltCashAcct + " AND AffectCashDrawer = 0 AND AffectCash = 1 AND EFT = 1) ";
										break;
									}
							}
							//end switch
						}
						// GetSQL = "SELECT Account, sum(Amt) as SumTotal FROM NonCashDrawerAccounts WHERE Amt <> 0 GROUP BY Account"
						GetSQL = "SELECT Account, Proj, Amt as SumTotal FROM (" + strTemp + ") as NonCashDrawerAccounts WHERE Amt <> 0";
						break;
					}
				case 10:
					{
						// gets the SQL for EFTs and CheckRecEntries
						for (j = 1; j <= 6; j++)
						{
							switch (j)
							{
								case 1:
									{
										strTemp = "(SELECT Account" + FCConvert.ToString(j) + " AS Account, Amount1 AS Amt, Project1 as Proj, EFT, Name FROM " + strArchiveTable + " WHERE (ReceiptType NOT BETWEEN 90 AND 99) AND EFT = 1 AND" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + ") ";
										break;
									}
								default:
									{
										strTemp += "UNION ALL (SELECT Account" + FCConvert.ToString(j) + " as Account, Amount" + FCConvert.ToString(j) + " as Amt, Project" + FCConvert.ToString(j) + " as Proj, EFT, Name FROM " + strArchiveTable + " WHERE (ReceiptType NOT BETWEEN 90 AND 99) AND EFT = 1 AND" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + ") ";
										break;
									}
							}
							//end switch
						}
						GetSQL = "SELECT * FROM (" + strTemp + ") as CheckRecEntries WHERE Amt <> 0";
						break;
					}
				case 20:
					{
						// gets the SQL for EFTs that have alt cash accounts
						for (j = 1; j <= 6; j++)
						{
							switch (j)
							{
								case 1:
									{
										strTemp = "(SELECT Account" + FCConvert.ToString(j) + " AS Account, Amount1 AS Amt, Project1 as Proj, EFT, Name FROM " + strArchiveTable + " WHERE DefaultCashAccount <> '' AND EFT = 1 AND" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + ") ";
										break;
									}
								default:
									{
										strTemp += "UNION ALL (SELECT Account" + FCConvert.ToString(j) + " as Account, Amount" + FCConvert.ToString(j) + " as Amt, Project" + FCConvert.ToString(j) + " as Proj, EFT, Name FROM " + strArchiveTable + " WHERE DefaultCashAccount <> '' AND EFT = 1 AND" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + ") ";
										break;
									}
							}
							//end switch
						}
						GetSQL = "SELECT * FROM (" + strTemp + ") as AltCashEFTEntries WHERE Amt <> 0";
						// Case 30     'gets the SQL for convenience fees with no seperate credit card cash account
						// GetSQL = "SELECT SUM(ConvenienceFee) AS SumTotal, ConvenienceFeeGLAccount as Account FROM (SELECT * FROM Archive WHERE " & frmDailyReceiptAudit.strCloseOutSQL & ") GROUP BY ConvenienceFeeGLAccount"
						break;
					}
				case 40:
					{
						// gets the SQL for conveneince fees and other credit card amounts that go to a seperate credit card cash account
						// GetSQL = "SELECT SUM(ConvenienceFee) AS ConvenienceFeeTotal, SUM(CardPaidAmount) as SumTotal, SeperateCreditCardGLAccount, ConvenienceFeeGLAccount FROM (SELECT * FROM Archive WHERE " & frmDailyReceiptAudit.strCloseOutSQL & " AND SeperateCreditCardGLAccount <> '') GROUP BY SeperateCreditCardGLAccount, ConvenienceFeeGLAccount HAVING SUM(CardPaidAmount) <> 0"
						GetSQL = "SELECT SUM(CardPaidAmount) as SumTotal, SeperateCreditCardGLAccount FROM (SELECT * FROM " + strArchiveTable + " WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND SeperateCreditCardGLAccount <> '') as temp GROUP BY SeperateCreditCardGLAccount HAVING SUM(CardPaidAmount) <> 0";
						break;
					}
			}
			//end switch
			return GetSQL;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
		}

		private void ReportFooter_AfterPrint(object sender, EventArgs e)
		{
			if (boolUsePrevYears)
			{
				arDailyAuditReport.InstancePtr.intReportHeader = 3;
			}
			else
			{
				arDailyAuditReport.InstancePtr.intReportHeader = 2;
			}
		}

		private void GenerateAllTotals()
		{
			// this sub will find each total amount and fill the fields
			string strSQL;
			clsDRWrapper rsCur = new clsDRWrapper();
			clsDRWrapper rsPrev = new clsDRWrapper();
			clsDRWrapper rsNC = new clsDRWrapper();
			clsDRWrapper rsMisc = new clsDRWrapper();
			clsDRWrapper rsOther = new clsDRWrapper();
			clsDRWrapper rsTotals = new clsDRWrapper();
			int intCT;
			double dblPrevious;
			string strArchiveTable = "";

			if (boolUsePrevYears)
			{
				strArchiveTable = "LastYearArchive";
			}
			else
			{
				strArchiveTable = "Archive";
			}
			// this SQL statement will join the archive table and the receipt table entries by receipt number, using the DISTINCT keyword to make sure that
			// only one copy of the receipt with multiple payments in the archive table will be summed in the outer SQL
			// Current Period
			if (boolUsePrevYears)
			{
				strSQL = "SELECT SUM(CheckAmount) as CHK, SUM(CashAmount) as CSH, SUM(CardAmount) as CC FROM " + 
					"(SELECT DISTINCT a.ReceiptNumber, r.CardAmount, r.CheckAmount, r.CashAmount " +
					"FROM LastYearArchive a INNER JOIN LastYearReceipt r ON a.ReceiptNumber = r.ReceiptKey "+
					"WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL.Replace("Archive.","a.") + " AND AffectCash = 1) as temp";
			}
			else
			{
				strSQL = "SELECT SUM(CheckAmount) as CHK, SUM(CashAmount) as CSH, SUM(CardAmount) as CC FROM " +
					"(SELECT DISTINCT a.ReceiptNumber, r.CardAmount, r.CheckAmount, r.CashAmount " +
					"FROM Archive a INNER JOIN Receipt r ON a.ReceiptNumber = r.ReceiptKey " +
					"WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL.Replace("Archive.","a.") + " AND AffectCash = 1) as temp";
			}
			rsTotals.OpenRecordset(strSQL, modExtraModules.strCRDatabase);
			// Cash
			if (FCConvert.ToString(rsTotals.Get_Fields("CSH")) != "")
				fldCurCash.Text = Strings.Format(FCConvert.ToString(rsTotals.Get_Fields("CSH")), "#,##0.00");
			// Check
			if (FCConvert.ToString(rsTotals.Get_Fields("CHK")) != "")
				fldCurCheck.Text = Strings.Format(FCConvert.ToString(rsTotals.Get_Fields("CHK")), "#,##0.00");
			// Credit Card
			if (FCConvert.ToString(rsTotals.Get_Fields_String("CC")) != "")
				fldCurCredit.Text = Strings.Format(rsTotals.Get_Fields_String("CC"), "#,##0.00");
			// Previous
			dblPrevious = FCConvert.ToDouble(arDailyAuditReport.InstancePtr.fldTotalPrevious.Text);
			// Other
			// if CDbl(arDailyAuditReport.fldTotalOther.text ) <> 0 then
			// fldCurOther.Text = Format(CDbl(arDailyAuditReport.fldTotalOther.Text), "#,##0.00")
			if (FCConvert.ToDouble(arDailyAuditReport.InstancePtr.fldTotalTotal.Text) - (FCConvert.ToDouble(fldCurCash.Text) + FCConvert.ToDouble(fldCurCheck.Text) + FCConvert.ToDouble(fldCurCredit.Text) + dblPrevious) != 0)
			{
				fldCurOther.Text = Strings.Format(modGlobal.Round(FCConvert.ToDouble(arDailyAuditReport.InstancePtr.fldTotalTotal.Text) - (FCConvert.ToDouble(fldCurCash.Text) + FCConvert.ToDouble(fldCurCheck.Text) + FCConvert.ToDouble(fldCurCredit.Text) + dblPrevious), 2), "#,##0.00");
			}
			else
			{
				fldCurOther.Text = "0.00";
			}
			// Previous Period

			// This query isn't used????
			//if (boolUsePrevYears)
			//{
			//	strSQL = "SELECT SUM(CheckAmount) as CHK, SUM(CashAmount) as CSH, SUM(CardAmount) as CC FROM (SELECT DISTINCT a.ReceiptNumber, r.CardAmount, r.CheckAmount, r.CashAmount FROM LastYearArchive a INNER JOIN LastYearReceipt r ON a.ReceiptNumber = r.ReceiptKey WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND AffectCashDrawer = 0 AND AffectCash = 1)";
			//}
			//else
			//{
			//	strSQL = "SELECT SUM(CheckAmount) as CHK, SUM(CashAmount) as CSH, SUM(CardAmount) as CC FROM (SELECT DISTINCT a.ReceiptNumber, r.CardAmount, r.CheckAmount, r.CashAmount FROM Archive a INNER JOIN Receipt r ON a.ReceiptNumber = r.ReceiptKey WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND AffectCashDrawer = 0 AND AffectCash = 1)";
			//}
			
			strSQL = "(SELECT Account1 AS Acct, Amount1 AS Amt FROM " + strArchiveTable + " WHERE DefaultAccount <> '' AND " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + ")";
			strSQL += "UNION ALL (SELECT Account2 AS Acct, Amount2 AS Amt FROM " + strArchiveTable + " WHERE Amount2 < 0 AND DefaultAccount <> '' AND " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + ")";
			// do not add positive interest into this amount it should be written off completely
			strSQL += "UNION ALL (SELECT Account3 AS Acct, Amount3 AS Amt FROM " + strArchiveTable + " WHERE DefaultAccount <> '' AND " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + ")";
			strSQL += "UNION ALL (SELECT Account4 AS Acct, Amount4 AS Amt FROM " + strArchiveTable + " WHERE DefaultAccount <> '' AND " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + ")";
			strSQL += "UNION ALL (SELECT Account5 AS Acct, Amount5 AS Amt FROM " + strArchiveTable + " WHERE DefaultAccount <> '' AND " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + ")";
			strSQL += "UNION ALL (SELECT Account6 AS Acct, Amount6 AS Amt FROM " + strArchiveTable + " WHERE DefaultAccount <> '' AND " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + ")";
			strSQL = "SELECT sum(Amt) as Total FROM (" + strSQL + ") as TotalFFOther";
			rsTotals.OpenRecordset(strSQL, modExtraModules.strCRDatabase);
			// Cash
			// Check
			// Credit Card
			// Other
			if (!rsTotals.IsFieldNull("Total"))
				fldNonCash.Text = Strings.Format(rsTotals.Get_Fields_Decimal("Total"), "#,##0.00");

			fldCurNonEntry.Text = "0.00";
			// Previous Period
			fldPrevious.Text = Strings.Format(dblPrevious, "#,##0.00");
			// Non Cash
			strSQL = "SELECT Account1, Amount1, Account2, Amount2 FROM " + strArchiveTable + " WHERE DefaultAccount <> '' AND " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND Amount2 > 0";
			strSQL = "SELECT sum(Amount2) as Total FROM (" + strSQL + ") as NonCashNonEntry";
			rsTotals.OpenRecordset(strSQL, modExtraModules.strCRDatabase);
			if (!rsTotals.IsFieldNull("Total"))
			{
				fldNonCashNonEntry.Text = Strings.Format(rsTotals.Get_Fields_Decimal("Total"), "#,##0.00");
			}
			else
			{
				fldNonCashNonEntry.Text = "0.00";
			}
			// Misc
			// fldMiscNonEntry.Text = "0.00"
			// Total Row
			// strSQL = "SELECT SUM(Total) as Totals FROM TotalPositiveReceipts"
			strSQL = "SELECT SUM(Total) as Totals FROM (" + modGlobal.Statics.strTotalPositiveReceiptsSQL + ") as TotalPositiveReceipts";
			rsTotals.OpenRecordset(strSQL, modExtraModules.strCRDatabase);
			// Current Period
			fldCurTotal.Text = Strings.Format(FCConvert.ToDouble(fldCurCash.Text) + FCConvert.ToDouble(fldCurCheck.Text) + FCConvert.ToDouble(fldCurCredit.Text) + FCConvert.ToDouble(fldCurOther.Text), "#,##0.00");
			// fldCurTotal.Text = Format(rsTotals.Fields("Totals"), "#,##0.00")
			// Previous Period
			fldPrevTotal.Text = Strings.Format(FCConvert.ToDouble(fldPrevious.Text), "#,##0.00");
			// Non Cash
			fldNonCashTotal.Text = Strings.Format(FCConvert.ToDouble(fldNonCash.Text), "#,##0.00");
			// Misc
			// fldMiscTotal.Text = Format(CDbl(fldMiscCash.Text) + CDbl(fldMiscCheck.Text) + CDbl(fldMiscCredit.Text) + CDbl(fldMiscOther.Text), "#,##0.00")
			// Row Totals
			// Cash Total
			fldTotalCash.Text = Strings.Format(FCConvert.ToDouble(fldCurCash.Text), "#,##0.00");
			// + CDbl(fldMiscCash.Text)
			// Check Total
			fldTotalCheck.Text = Strings.Format(FCConvert.ToDouble(fldCurCheck.Text), "#,##0.00");
			// + CDbl(fldMiscCheck.Text)
			// Credit Total
			fldTotalCredit.Text = Strings.Format(FCConvert.ToDouble(fldCurCredit.Text), "#,##0.00");
			// + CDbl(fldMiscCredit.Text)
			// Previous Total
			fldTotalPrevious.Text = Strings.Format(FCConvert.ToDouble(fldPrevious.Text), "#,##0.00");
			// Other Total
			fldTotalOther.Text = Strings.Format(FCConvert.ToDouble(fldCurOther.Text) + FCConvert.ToDouble(fldNonCash.Text), "#,##0.00");
			// + CDbl(fldMiscOther.Text)
			// Total
			fldTotalTotal.Text = Strings.Format(FCConvert.ToDouble(fldCurTotal.Text) + FCConvert.ToDouble(fldPrevTotal.Text) + FCConvert.ToDouble(fldNonCashTotal.Text), "#,##0.00");
			// + CDbl(fldMiscTotal.Text)
			// Total
			fldTotalNonEntry.Text = Strings.Format(FCConvert.ToDouble(fldCurNonEntry.Text) + FCConvert.ToDouble(fldPrevNonEntry.Text) + FCConvert.ToDouble(fldNonCashNonEntry.Text), "#,##0.00");
			// + CDbl(fldMiscNonEntry.Text)
			// Total Activity
			// Current Period
			fldCurTotalActivity.Text = Strings.Format(FCConvert.ToDouble(fldCurNonEntry.Text) + FCConvert.ToDouble(fldCurTotal.Text), "#,##0.00");
			// Previous Period
			fldPrevTotalActivity.Text = Strings.Format(FCConvert.ToDouble(fldPrevNonEntry.Text) + FCConvert.ToDouble(fldPrevTotal.Text), "#,##0.00");
			// Non Cash
			fldNonCashTotalActivity.Text = Strings.Format(FCConvert.ToDouble(fldNonCashNonEntry.Text) + FCConvert.ToDouble(fldNonCashTotal.Text), "#,##0.00");
			// Misc
			// fldMiscTotalActivity.Text = Format(CDbl(fldMiscNonEntry.Text) + CDbl(fldMiscNonEntry.Text), "#,##0.00")
			// Total
			fldTotalTotalActivity.Text = Strings.Format(FCConvert.ToDouble(fldCurTotalActivity.Text) + FCConvert.ToDouble(fldPrevTotalActivity.Text) + FCConvert.ToDouble(fldNonCashTotalActivity.Text), "#,##0.00");
			// + CDbl(fldMiscTotalActivity.Text)
		}
		// vbPorter upgrade warning: dblAmt As double	OnReadFCConvert.ToDecimal(
		private void AddAccount_15(string strAcct, double dblAmt, int intType = 0, bool boolOverWrite = false)
		{
			AddAccount(ref strAcct, ref dblAmt, "", intType, boolOverWrite);
		}

		private void AddAccount_24(string strAcct, double dblAmt, string strProj = "", int intType = 0, bool boolOverWrite = false)
		{
			AddAccount(ref strAcct, ref dblAmt, strProj, intType, boolOverWrite);
		}

		private void AddAccount(ref string strAcct, ref double dblAmt, string strProj = "", int intType = 0, bool boolOverWrite = false)
		{
			// this will redim the array if needed and add the account to the next available index
			int lngTemp = 0;
			lngAcctCT += 1;
			if (!boolOverWrite)
			{
				if (intType == 4 || intType == 3)
				{
					if (lngAcctCT >= Information.UBound(ftAccountArray, 1))
					{
						// if the number elements is greater than the upper bound of the array then
						Array.Resize(ref ftAccountArray, lngAcctCT - 1 + 1);
						// this will reset the size of the array
					}
					lngTemp = lngAcctCT - 1;
				}
				else
				{
					if (lngAcctCT > Information.UBound(ftAccountArray, 1))
					{
						// if the number elements is greater than the upper bound of the array then
						Array.Resize(ref ftAccountArray, lngAcctCT - 1 + 1);
						// this will reset the size of the array
					}
					lngTemp = lngAcctCT - 1;
				}
			}
			else
			{
				lngTemp = 0;
			}
			// use the -1 to account for the zero based array
			ftAccountArray[lngTemp].Account = strAcct;
			ftAccountArray[lngTemp].Amount = FCConvert.ToDecimal(dblAmt);
			ftAccountArray[lngTemp].Project = strProj;
			switch (intType)
			{
				case 0:
				case 1:
				case 2:
					{
						ftAccountArray[lngTemp].AcctType = "A";
						break;
					}
				case 3:
					{
						ftAccountArray[lngTemp].AcctType = "F";
						break;
					}
				case 4:
					{
						ftAccountArray[lngTemp].AcctType = "C";
						break;
					}
			}
			//end switch
		}

		private bool ShowFieldValues()
		{
			bool ShowFieldValues = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will fill in the fields and return true when there are no more lines to print
				if (intArrIndex <= Information.UBound(ftAccountArray, 1))
				{
					fldAmount.Text = Strings.Format(Math.Abs(ftAccountArray[intArrIndex].Amount), "#,##0.00");
					// amount of transaction
					if (ftAccountArray[intArrIndex].Amount > 0)
					{
						fldCD.Text = "CR";
					}
					else
					{
						fldCD.Text = "DB";
					}
					fldAccount.Text = ftAccountArray[intArrIndex].Account;
					// account number
					if (Strings.Left(fldAccount.Text, 1) == "M")
					{
						if (ftAccountArray[intArrIndex].AcctType == "F")
						{
							fldDescription.Text = ftAccountArray[intArrIndex].Description;
						}
						else
						{
							fldDescription.Text = "Manual Account";
						}
					}
					else
					{
						if (modGlobalConstants.Statics.gboolBD)
						{
							fldDescription.Text = modAccountTitle.ReturnAccountDescription(ftAccountArray[intArrIndex].Account);
						}
						else
						{
							fldDescription.Text = "Manual Account";
						}
					}
					// fldDescription.Text = GetAccountDescription(ftAccountArray(intArrIndex).Account)
					if (Strings.Trim(fldDescription.Text) == "" && Strings.Left(fldAccount.Text, 1) != "M" && Strings.Trim(fldAccount.Text) != "CASH" && Conversion.Val(fldAmount.Text) != 0)
					{
						fldDescription.Text = "* * *   ERROR   * * *";
						fldDescription.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORRED);
						fldError.Text = "* * * ERROR * * *  There is an error in this report!  * * * ERROR * * * ";
						fldError.Visible = true;
					}
					else
					{
						fldDescription.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK);
					}
				}
				else
				{
					if (dblM_DTotals > 0 && !boolM_DLine)
					{
						// this will show one last line if the direct deposit account is here
						if (dblM_DTotals < 0)
						{
							fldCD.Text = "CR";
						}
						else
						{
							fldCD.Text = "DB";
						}
						fldAccount.Text = "M D";
						fldDescription.Text = "Direct Deposit";
						fldAmount.Text = Strings.Format(dblM_DTotals, "#,##0.00");
						boolM_DLine = true;
						ShowFieldValues = false;
					}
					else
					{
						ShowFieldValues = true;
						// this will end the subreport
					}
				}
				return ShowFieldValues;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				ShowFieldValues = true;
				if (Information.Err(ex).Number != 9)
				{
					MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + " has occured.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				else
				{
					// this is the subscript out of range from the UBound on an empty array
				}
			}
			return ShowFieldValues;
		}

		private double AddDefaultCashAccounts()
		{
			double AddDefaultCashAccounts = 0;
			// add all the accounts to the end of the array, assuming that the accounts are summed in this
			// this function will return a value equal to the amount changed in all of the funds
			int intFund = 0;
			int intIndex;
			double dblSum = 0;
			string strGLAcct = "";
			string strGLAltAcct = "";
			rsData.MoveFirst();
			while (!rsData.EndOfFile())
			{
				// subtract the amount from each associated fund total
				if (Strings.Left(FCConvert.ToString(rsData.Get_Fields_String("DefaultCashAccount")), 1) == "M")
				{
					intFund = modGlobal.Statics.glngCashFund;
				}
				else
				{
					intFund = modBudgetaryAccounting.GetFundFromAccount(rsData.Get_Fields_String("DefaultCashAccount"), false);
				}
				if (intFund > 0)
				{
					strGLAcct = "G " + modGlobal.PadToString_6(intFund, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modGlobal.PadToString_6(modGlobal.Statics.glngCashAccount, FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))) + "-" + modGlobal.PadToString_8(0, FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 5, 2))));
					strGLAltAcct = FCConvert.ToString(rsData.Get_Fields_String("DefaultCashAccount"));
					// add the account and amount to the array
					// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
					AddAccount_15(strGLAltAcct, Conversion.Val(rsData.Get_Fields("SumTotal")) * -1, 4);
					if (modGlobal.Statics.glngCashAccount == Conversion.Val(Strings.Mid(FCConvert.ToString(rsData.Get_Fields_String("DefaultCashAccount")), 3, FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))))))
					{

					}
				}
				rsData.MoveNext();
			}
			AddDefaultCashAccounts = dblSum;
			return AddDefaultCashAccounts;
		}
		// kk10262016 trocr-467  Change this to a procedure and handle all redistribution here
		private void AddSeperateCreditCardGLAccounts()
		{
			// kk09092014 trocr-430  Rewriting this function to:
			// 1 - If the fund uses DTDF then transfer the amount to the credit card alt cash account in the cash fund (per Brenda)
			// We don't reduce the fund amount because we want the DTDF code to transfer the entire amount
			// We will reduce the cash account by the amount after the DTDF entries are added
			// Otherwise add an entry for the credit card alt cash account and reduce the fund cash by the amount
			// 2 - Sum all the amounts to transfer and create one entry
			// 3 - Return the amount to reduce the cash account of the cash fund
			// 
			// add all the accounts to the end of the array, assuming that the accounts are summed in the array
			// this function will return a value equal to the amount changed in all of the funds
			int intFund = 0;
			string strGLAltAcct = "";
			string strGLAltAcctCash = "";
			double dblCashFundAmt = 0;
			string strGLCashAcct = "";
			int intAcctIndx = 0;
			clsDRWrapper rsTemp = new clsDRWrapper();
			rsData.MoveFirst();
			while (!rsData.EndOfFile())
			{
				intFund = modBudgetaryAccounting.GetFundFromAccount(rsData.Get_Fields_String("SeperateCreditCardGLAccount"), false);
				if (intFund > 0)
				{
					if (ftFundArray[intFund].UseDueToFrom || intFund == modGlobal.Statics.glngCashFund)
					{
						// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
						dblCashFundAmt += Conversion.Val(rsData.Get_Fields("SumTotal"));
						if (strGLAltAcctCash == "")
						{
							strGLAltAcctCash = "G " + modGlobal.PadToString_6(modGlobal.Statics.glngCashFund, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))));
							strGLAltAcctCash += Strings.Mid(FCConvert.ToString(rsData.Get_Fields_String("SeperateCreditCardGLAccount")), strGLAltAcctCash.Length + 1);
						}
					}
					else
					{
						strGLAltAcct = FCConvert.ToString(rsData.Get_Fields_String("SeperateCreditCardGLAccount"));
						// kk10252016 trocr-467  Adjust the Fund cash account amount
						if (modGlobalConstants.Statics.gboolBD)
						{
							rsTemp.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'CM'", "TWBD0000.vb1");
							if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
							{
								// NEED THE FIND THE CASH ACCOUNT HERE
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								strGLCashAcct = "G " + modGlobal.PadToString_6(intFund, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + rsTemp.Get_Fields("Account") + "-" + "00";
							}
						}
						intAcctIndx = FindFundAccount(ref strGLCashAcct);
						// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
						AdjustFundCashAccount_6(intAcctIndx, FCConvert.ToDouble(rsData.Get_Fields("SumTotal")));
						// add the account and amount to the array
						// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
						AddAccount_15(strGLAltAcct, Conversion.Val(rsData.Get_Fields("SumTotal")) * -1, 4);
						// subtract the amount from the associated fund total
						// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
						ftFundArray[intFund].Amount -= FCConvert.ToDecimal(rsData.Get_Fields("SumTotal"));
					}
				}
				rsData.MoveNext();
			}
			if (dblCashFundAmt != 0)
			{
				// kk10252016 trocr-467  Adjust the Fund cash account amount
				if (modGlobalConstants.Statics.gboolBD)
				{
					rsTemp.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'CM'", "TWBD0000.vb1");
					if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
					{
						// NEED THE FIND THE CASH ACCOUNT HERE
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						strGLCashAcct = "G " + modGlobal.PadToString_6(modGlobal.Statics.glngCashFund, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + rsTemp.Get_Fields("Account") + "-" + "00";
					}
				}
				intAcctIndx = FindFundAccount(ref strGLCashAcct);
				AdjustFundCashAccount(ref intAcctIndx, ref dblCashFundAmt);
				// Should we still see if it already exists?
				AddAccount_15(strGLAltAcctCash, dblCashFundAmt * -1, 4);
			}
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		private short FindFundAccount(ref string strAcct)
		{
			short FindFundAccount = 0;
			// this will search through the array and find the account that matches the one passed in
			// if found it will return the index of the account, if not then it returns a -1
			int intCT;
			for (intCT = 0; intCT <= Information.UBound(ftAccountArray, 1); intCT++)
			{
				if (ftAccountArray[intCT].Account == strAcct)
				{
					FindFundAccount = FCConvert.ToInt16(intCT);
					return FindFundAccount;
				}
				Debug.WriteLine(FCConvert.ToString(intCT) + " " + ftAccountArray[intCT].Account);
			}
			FindFundAccount = -1;
			return FindFundAccount;
		}
		// vbPorter upgrade warning: intX As short	OnWriteFCConvert.ToInt32(
		private bool AdjustFundCashAccount_6(int intX, double dblAmt)
		{
			return AdjustFundCashAccount(ref intX, ref dblAmt);
		}

		private bool AdjustFundCashAccount(ref int intX, ref double dblAmt)
		{
			bool AdjustFundCashAccount = false;
			// this function will change the amount of the fund cash entry
			try
			{
				// On Error GoTo ERROR_HANDLER
				ftAccountArray[intX].Amount += FCConvert.ToDecimal(dblAmt);
				AdjustFundCashAccount = true;
				return AdjustFundCashAccount;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				AdjustFundCashAccount = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Adjust Fund Cash Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return AdjustFundCashAccount;
		}

		private modBudgetaryAccounting.FundType[] CashAccountForM(ref modBudgetaryAccounting.FundType[] Accounts)
		{
			modBudgetaryAccounting.FundType[] CashAccountForM = null;
			// this function will setup the cash account for any user that does not have the BD module
			// it will return an array of FundTypes with only one fund which is the default cash account
			try
			{
				// On Error GoTo ERROR_HANDLER
				modBudgetaryAccounting.FundType[] ftFunds = new modBudgetaryAccounting.FundType[99 + 1];
				// this will be the array that is returned
				int lngCT;
				// total number of accounts in the array
				int lngUB;
				// this is the upper bound of the array passed in
				lngUB = Information.UBound(Accounts, 1);
				// reset all of the funds
				FCUtils.EraseSafe(ftFunds);
				ftFunds[1].Account = "M CASH";
				ftFunds[1].Description = "CASH ACCOUNT";
				for (lngCT = 0; lngCT <= lngUB; lngCT++)
				{
					// check all of the accounts
					// check to see if there is an invalid account number
					if (Strings.InStr(1, Accounts[lngCT].Account, "_", CompareConstants.vbBinaryCompare) == 0 && Accounts[lngCT].Account.Length > 2)
					{
						// extract the fund number and add the amount
						ftFunds[1].Amount -= Accounts[lngCT].Amount;
					}
					else
					{
						// invalid account format
					}
				}
				CashAccountForM = ftFunds;
				return CashAccountForM;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + " has occured.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CashAccountForM;
		}

		private bool AddCashAccountforM_9(ref modBudgetaryAccounting.FundType[] Accounts, modBudgetaryAccounting.FundType[] Cash, double dblMAmount = 0)
		{
			return AddCashAccountforM(ref Accounts, ref Cash, false, dblMAmount);
		}

		private bool AddCashAccountforM(ref modBudgetaryAccounting.FundType[] Accounts, ref modBudgetaryAccounting.FundType[] Cash, bool bolNoAccounts = false, double dblMAmount = 0)
		{
			bool AddCashAccountforM = false;
			// PRE:    'Cash() is an array of 99 funds with amounts and fund numbers in account field and
			// Accounts() is a dynamic array of any accounts you want posted to the budgetary system
			// POST:   'Accounts will have all of the original accounts and thier totals with one cash entry
			try
			{
				// On Error GoTo ErrorTag
				string strCashAccount = "";
				int lngUB;
				AddCashAccountforM = true;
				lngUB = Information.UBound(Accounts, 1) + 1;
				lngAcctCT += 1;
				Array.Resize(ref Accounts, lngUB + 1);
				lngUB = lngUB;
				// End If
				if (dblMAmount != 0)
				{
					Accounts[lngUB].Account = "M CASH";
					Accounts[lngUB].AcctType = "F";
					Accounts[lngUB].Amount = FCConvert.ToDecimal(dblMAmount * -1);
					// this has to be multiplied by -1 so that it will be a DB not a CR
					Accounts[lngUB].Description = "CASH ACCOUNT";
				}
				else
				{
					Accounts[lngUB].Account = Cash[1].Account;
					Accounts[lngUB].AcctType = "F";
					Accounts[lngUB].Amount = Cash[1].Amount;
					Accounts[lngUB].Description = Cash[1].Description;
				}
				return AddCashAccountforM;
			}
			catch (Exception ex)
			{
				// ErrorTag:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Add Cash Account (M)", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				AddCashAccountforM = false;
			}
			return AddCashAccountforM;
		}
		// vbPorter upgrade warning: dtMaxDate As DateTime	OnWrite(short, DateTime)
		private DateTime GetLastDate_2(DateTime dtMaxDate)
		{
			return GetLastDate(ref dtMaxDate);
		}

		private DateTime GetLastDate(ref DateTime dtMaxDate)
		{
			DateTime GetLastDate = System.DateTime.Now;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngCT;
				string strSQL;
				clsDRWrapper rsTemp = new clsDRWrapper();

				if (boolUsePrevYears)
				{
					strSQL =
						"SELECT LastYearArchiveDate FROM LastYearArchive WHERE ISNULL(DailyCloseOut,0) = 0 GROUP BY LastYearArchiveDate ORDER BY LastYearArchiveDate desc";
				}
				else
				{
					strSQL = "SELECT ArchiveDate FROM Archive WHERE ISNULL(DailyCloseOut,0) = 0 GROUP BY ArchiveDate ORDER BY ArchiveDate desc";
				}

				rsTemp.OpenRecordset(strSQL);
				if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
				{
					rsTemp.MoveFirst();
					while (!rsTemp.EndOfFile())
					{
						if (rsTemp.Get_Fields_DateTime("ArchiveDate").ToOADate() > dtMaxDate.ToOADate())
							dtMaxDate = (DateTime)rsTemp.Get_Fields_DateTime("ArchiveDate");
						rsTemp.MoveNext();
					}
				}
				if (dtMaxDate.ToOADate() == 0)
					dtMaxDate = DateTime.Today;
				GetLastDate = dtMaxDate;
				return GetLastDate;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				GetLastDate = dtMaxDate;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Get Last Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			return GetLastDate;
		}

		private void FillCheckRecEntries_6(clsDRWrapper rsF, int lngTemp, bool boolOverWrite = false)
		{
			FillCheckRecEntries(ref rsF, ref lngTemp, boolOverWrite);
		}

		private void FillCheckRecEntries(ref clsDRWrapper rsF, ref int lngTemp, bool boolOverWrite = false)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsWOInt = new clsDRWrapper();
				// this will fill the recordset's Department field with the fund to be grouped on
				while (!(rsF.EndOfFile() || rsF.BeginningOfFile()))
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (Strings.InStr(1, rsF.Get_Fields("Account"), "_", CompareConstants.vbBinaryCompare) == 0 && rsF.Get_Fields("Account").Length > 2)
					{
						// check to make sure that this is not an "M D" account
						// if it is...do not allow it to add its totals to the accounts...
						// it needs to have its own cash account and no entries in the BD system
						// If Left(rsF.Fields("Account"), 1) <> "M" Then
						// this will add the account summary to ftAccountArray
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						ftCheckRecEntries[lngTemp].Account = rsF.Get_Fields_String("Account");
						// TODO Get_Fields: Field [Amt] not found!! (maybe it is an alias?)
						ftCheckRecEntries[lngTemp].Amount = FCConvert.ToDecimal(rsF.Get_Fields("Amt"));
						ftCheckRecEntries[lngTemp].AcctType = "EFT";
						ftCheckRecEntries[lngTemp].Description = "Name";
						// End If
					}
					rsF.MoveNext();
					lngTemp += 1;
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Fill Check Rec Info Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void CreateUSLExport_6(modBudgetaryAccounting.FundType[] Accounts, DateTime dtDate)
		{
			CreateUSLExport(ref Accounts, ref dtDate);
		}

		private void CreateUSLExport(ref modBudgetaryAccounting.FundType[] Accounts, ref DateTime dtDate)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// This routine will create a file named ? to be
				// used as an import into the USL G/L module...
				int lngCT;
				FCFileSystem.FileOpen(11, "USLExport.DAT", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
				for (lngCT = 0; lngCT <= Information.UBound(Accounts, 1); lngCT++)
				{
					if (Accounts[lngCT].Amount != 0)
					{
						// Account Number(32)                         '1
						FCFileSystem.Print(11, Strings.StrDup(32, " "));
						// Date(19)                                   '33
						FCFileSystem.Print(11, modGlobalFunctions.PadStringWithSpaces(Strings.Format(dtDate, "MM/dd/yyyy"), 19));
						// Amount(21)                                 '54
						// If Accounts(lngCT).AcctType = "C" Then
						// Print #11, PadStringWithSpaces(Format(Accounts(lngCT).Amount, "###0.00"), 21);
						// Else
						FCFileSystem.Print(11, modGlobalFunctions.PadStringWithSpaces(Strings.Format((Accounts[lngCT].Amount) * -1, "###0.00"), 21));
						// End If
						// Description(50)                            '104
						FCFileSystem.Print(11, modGlobalFunctions.PadStringWithSpaces(Accounts[lngCT].Description, 50));
						// add blanks(455)
						FCFileSystem.Print(11, Strings.StrDup(455, " "));
						// add account number(32)
						FCFileSystem.Print(11, GetUSLAccountNumber(ref Accounts[lngCT]._account));
						// add blanks(119)
						FCFileSystem.PrintLine(11, Strings.StrDup(119, " "));						
					}
				}
				FCFileSystem.FileClose(11);
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating USL Export", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private string GetUSLAccountNumber(ref string strAccountNumber)
		{
			string GetUSLAccountNumber = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will return a USL Account number from the table that is set up
				// If it does not find a matching account, then it will return a string of 32 spaces
				GetUSLAccountNumber = modGlobalFunctions.PadStringWithSpaces(Strings.Right(strAccountNumber, strAccountNumber.Length - 1), 32, false);
				return GetUSLAccountNumber;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				GetUSLAccountNumber = Strings.StrDup(32, " ");
			}
			return GetUSLAccountNumber;
		}

        private string MakeCashAccount(int intFund, int intAcct, string strLedg)
        {
            var fundLen = Convert.ToInt32(strLedg.Substring(0, 2));
            var acctLen = Convert.ToInt32(strLedg.Substring(2, 2));
            var suffixLen = Convert.ToInt32(strLedg.Substring(4, 2));
            return "G " + intFund.ToString().PadLeft(fundLen, '0') + "-" + intAcct.ToString().PadLeft(acctLen, '0') +
                   "-" + String.Empty.PadLeft(suffixLen, '0');
        }

        //TODO: Use new way of creating excel documents to write this method
        private void CreateMunisExport(ref modBudgetaryAccounting.FundType[] Accounts, int lngPeriod, DateTime dtDate)
        {
            try
            {
                int lngCT;
                int lngRow;
                var strFName = GetFileName();
                var excelWrapper = new SharedDataAccess.ADOExcelWrapper();
                var columns = new List<string>()
                {
                    "F1","F2","F3","F4","F5","F6","F7","F8","F9","F10","F11","F12","F13","F14"
                };

                excelWrapper.CreateExcelFile(columns,strFName,false,"Sheet1");
                foreach (var fundType in Accounts)
                {
                    if (fundType.Amount != 0 && fundType.Description != @"Due To\From" &&
                        fundType.Description != "Revenue CTL" && fundType.Description != "Expense CTL")
                    {
                        var columnNames = new List<string>();
                        var columnValues = new List<string>();
                        columnNames.Add("F1");
                        columnValues.Add(lngPeriod.ToString());
                        columnNames.Add("F2");
                        columnValues.Add("'" + dtDate.FormatAndPadShortDate() + "'");
                        columnNames.Add("F7");
                        columnValues.Add("'" + fundType.Description + "'");
                        columnNames.Add("F9");
                        if (fundType.AcctType == "A")
                        {
                            if (!String.IsNullOrWhiteSpace(fundType.RCB.Trim()))
                            {
                                columnValues.Add("'" + fundType.RCB + "'");
                            }
                            else
                            {
                                columnValues.Add("'R'");
                            }
                        }
                        else
                        {
                            columnValues.Add("'L'");
                        }
                        columnNames.Add("F10");
                        if (fundType.Amount > 0)
                        {
                            columnValues.Add("'C'");
                        }
                        else
                        {
                            columnValues.Add("'D'");
                        }
                        columnNames.Add("F12");
                        columnValues.Add(fecherFoundation.Strings.Format( Math.Abs(fundType.Amount),"###0.00"));
                        columnNames.Add("F13");
                        columnValues.Add("'" + fundType.Account.Substring(0,1) + "'");
                        columnNames.Add("F14");
                        columnValues.Add("'" + fundType.Account.Substring(2) + "'");
                        excelWrapper.AddRow("Sheet1$",columnNames,columnValues);
                    }
                }
                excelWrapper.CloseFile();
                Application.Download(strFName);
            }
            catch (Exception ex)
            {

            }
        }

        private string GetFileName()
        {
            var fileName = new StringBuilder("CRExport");
            
            fileName.Append(DateTime.Now.Year.ToString().Substring(2,2));
            fileName.Append(DateTime.Now.ToString().PadLeft(2, '0'));
            fileName.Append(DateTime.Now.ToString().PadLeft(2, '0'));
            fileName.Append(DateTime.Now.ToString().PadLeft(2, '0'));
            fileName.Append(DateTime.Now.ToString().PadLeft(2, '0'));
            fileName.Append(".xlsx");
            return Path.Combine(GetTempPath(), fileName.ToString());
        }

        private string GetTempPath()
        {
            string applicationName = Path.GetFileName(Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory));
            string tempFolder = Path.Combine(FCFileSystem.Statics.UserDataFolder, "Temp");
            if (!Directory.Exists(tempFolder))
            {
                Directory.CreateDirectory(tempFolder);
            }

            return tempFolder;
        }
    }
}
