﻿//Fecher vbPorter - Version 1.0.0.32
using fecherFoundation;
using Global;
using System;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPP0000
{
    public class MDIParent
    //: BaseForm
    {
        public FCCommonDialog CommonDialog1;
        public fecherFoundation.FCTextBox txtCommDest;
        public MDIParent()
        {
            //
            // Required for Windows Form Designer support
            //
            //InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
            CommonDialog1 = new FCCommonDialog();
            txtCommDest = new fecherFoundation.FCTextBox();
        }

        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static MDIParent InstancePtr
        {
            get
            {
                return (MDIParent)Sys.GetInstance(typeof(MDIParent));
            }
        }

        protected MDIParent _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        //=========================================================
        private int[] LabelNumber = new int[200 + 1];
        private string Answer = "";
        private int intExit;
        const string strTrio = "TRIO Software - Personal Property";
        public string strBeginRange = "";
        public string strEndRange = "";
        public int intWhichRangeOrder;

        private void mnuFileCentralParties_Click(object sender, System.EventArgs e)
        {
            frmCentralPartySearch.InstancePtr.Show(App.MainForm);
        }

        public void Menu1()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
            if (vbPorterVar == "MAIN")
            {
                MainActions(1);
            }
            else if (vbPorterVar == "IMPORTEXPORT")
            {
                ImportExportActions_2(1);
            }
            else if (vbPorterVar == "COST")
            {
                CostActions_2(1);
            }
            else if (vbPorterVar == "PRINT")
            {
                PrintActions_2(1);
            }
            else if (vbPorterVar == "FILE")
            {
                FileActions_2(1);
            }
            else if (vbPorterVar == "CALCULATE")
            {
                CalculationActions_2(1);
            }
            else if (vbPorterVar == "CUSTOM")
            {
                CustomActions(1);
            }
            else
            {
            }
        }

        public void Menu11()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
            if (vbPorterVar == "MAIN")
            {
            }
            else if (vbPorterVar == "CALCULATE")
            {
                CalculationActions_2(11);
            }
        }

        public void Menu10()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
            if (vbPorterVar == "MAIN")
            {
            }
            else if (vbPorterVar == "SYSTEM")
            {
            }
            else if (vbPorterVar == "CALCULATE")
            {
                CalculationActions_2(10);
            }
        }

        public void Menu13()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
            if (vbPorterVar == "MAIN")
            {
            }
            else if (vbPorterVar == "SYSTEM")
            {
            }
            else
            {
            }
        }

        public void Menu14()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
            if (vbPorterVar == "MAIN")
            {
            }
            else if (vbPorterVar == "SYSTEM")
            {
            }
            else
            {
            }
        }

        public void Menu16()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
            if (vbPorterVar == "MAIN")
            {
            }
            else if (vbPorterVar == "SYSTEM")
            {
            }
            else
            {
            }
        }

        public void Menu18()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
            if (vbPorterVar == "MAIN")
            {
            }
            else if (vbPorterVar == "SYSTEM")
            {
            }
            else
            {
            }
        }

        public void Menu19()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
            if (vbPorterVar == "MAIN")
            {
            }
            else if (vbPorterVar == "SYSTEM")
            {
            }
            else
            {
            }
        }

        public void Menu2()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
            if (vbPorterVar == "MAIN")
            {
                MainActions(2);
            }
            else if (vbPorterVar == "COST")
            {
                CostActions_2(2);
            }
            else if (vbPorterVar == "CALCULATE")
            {
                CalculationActions_2(2);
            }
            else if (vbPorterVar == "IMPORTEXPORT")
            {
                ImportExportActions_2(2);
            }
            else if (vbPorterVar == "PRINT")
            {
                PrintActions_2(2);
            }
            else if (vbPorterVar == "FILE")
            {
                FileActions_2(2);
            }
            else
            {
            }
        }

        public void Menu3()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
            if (vbPorterVar == "MAIN")
            {
                MainActions(3);
            }
            else if (vbPorterVar == "IMPORTEXPORT")
            {
                ImportExportActions_2(3);
            }
            else if (vbPorterVar == "FILE")
            {
                FileActions_2(3);
            }
            else if (vbPorterVar == "COST")
            {
                CostActions_2(3);
            }
            else if (vbPorterVar == "PRINT")
            {
                PrintActions_2(3);
            }
            else if (vbPorterVar == "CALCULATE")
            {
                CalculationActions_2(3);
            }
            else
            {
            }
        }

        public void Menu4()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
            if (vbPorterVar == "MAIN")
            {
                MainActions(4);
            }
            else if (vbPorterVar == "IMPORTEXPORT")
            {
                ImportExportActions_2(4);
            }
            else if (vbPorterVar == "FILE")
            {
                FileActions_2(4);
            }
            else if (vbPorterVar == "PRINT")
            {
                PrintActions_2(4);
            }
            else if (vbPorterVar == "COST")
            {
                CostActions_2(4);
            }
            else if (vbPorterVar == "CALCULATE")
            {
                CalculationActions_2(4);
            }
            else
            {
            }
        }

        public void Menu5()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
            if (vbPorterVar == "MAIN")
            {
                MainActions(5);
            }
            else if (vbPorterVar == "IMPORTEXPORT")
            {
                ImportExportActions_2(5);
            }
            else if (vbPorterVar == "PRINT")
            {
                PrintActions_2(5);
            }
            else if (vbPorterVar == "CALCULATE")
            {
                CalculationActions_2(5);
            }
            else if (vbPorterVar == "COST")
            {
                CostActions_2(5);
            }
            else if (vbPorterVar == "FILE")
            {
                FileActions_2(5);
            }
            else
            {
            }
        }

        public void Menu6()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
            if (vbPorterVar == "MAIN")
            {
                MainActions(6);
            }
            else if (vbPorterVar == "IMPORTEXPORT")
            {
                ImportExportActions_2(6);
            }
            else if (vbPorterVar == "CALCULATE")
            {
                CalculationActions_2(6);
            }
            else if (vbPorterVar == "PRINT")
            {
                PrintActions_2(6);
            }
            else if (vbPorterVar == "COST")
            {
                CostActions_2(6);
            }
            else if (vbPorterVar == "FILE")
            {
                FileActions_2(6);
            }
            else
            {
            }
        }

        public void Menu7()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
            if (vbPorterVar == "MAIN")
            {
                MainActions(7);
            }
            else if (vbPorterVar == "PRINT")
            {
                PrintActions_2(7);
            }
            else if (vbPorterVar == "FILE")
            {
                FileActions_2(7);
            }
            else if (vbPorterVar == "CALCULATE")
            {
                CalculationActions_2(7);
            }
            else if (vbPorterVar == "COST")
            {
                CostActions_2(7);
            }
            else
            {
            }
        }

        public void Menu8()
        {
            try
            {
                // On Error GoTo ErrorHandler
                var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
                if (vbPorterVar == "MAIN")
                {
                    MainActions(8);
                }
                else if (vbPorterVar == "CALCULATE")
                {
                    CalculationActions_2(8);
                }
                else if (vbPorterVar == "PRINT")
                {
                    PrintActions_2(8);
                }
                else if (vbPorterVar == "FILE")
                {
                    FileActions_2(8);
                }
                else
                {
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("error number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description, null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        public void Menu9()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
            if (vbPorterVar == "MAIN")
            {
                MainActions(9);
            }
            else if (vbPorterVar == "PRINT")
            {
                PrintActions_2(9);
            }
            else if (vbPorterVar == "COST")
            {
                CostActions_2(9);
            }
            else if (vbPorterVar == "CALCULATE")
            {
                CalculationActions_2(9);
            }
            else
            {
            }
        }

        public void Init()
        {
            string strReturn = "";
            modGlobalFunctions.LoadTRIOColors();
            App.MainForm.StatusBarText1 = StaticSettings.EnvironmentSettings.ClientName;
            App.MainForm.NavigationMenu.Owner = this;
            App.MainForm.menuTree.ImageList = CreateImageList();
            modGlobalConstants.Statics.TRIOCOLORBLUE = Information.RGB(0, 0, 200);
            modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, "SOFTWARE\\TrioVB\\", "Maximize Forms", ref strReturn);
            if (strReturn == string.Empty)
                strReturn = "False";
            modGlobalConstants.Statics.boolMaxForms = FCConvert.CBool(strReturn);
            if (modPPGN.Statics.boolShortScreenOnly)
            {
                MainCaptions();
            }
            else
            {
                MainCaptions();
            }
            MDIParent.InstancePtr.CommonDialog1 = new FCCommonDialog();
        }

        public ImageList CreateImageList()
        {
            ImageList imageList = new ImageList();
            imageList.ImageSize = new System.Drawing.Size(18, 18);
            imageList.Images.AddRange(new ImageListEntry[] {
                new ImageListEntry("menutree-short-maintenance", "short-maintenance"),
                new ImageListEntry("menutree-cost-files", "cost-files"),
                new ImageListEntry("menutree-printing", "printing"),
                new ImageListEntry("menutree-audit", "audit"),
                new ImageListEntry("menutree-file-maintenance", "file-maintenance"),
                new ImageListEntry("menutree-import-export", "import-export"),
                new ImageListEntry("menutree-account-maintenance", "account-maintenance"),
                new ImageListEntry("menutree-compute", "compute"),
                new ImageListEntry("menutree-short-maintenance-active", "short-maintenance-active"),
                new ImageListEntry("menutree-cost-files-active", "cost-files-active"),
                new ImageListEntry("menutree-printing-active", "printing-active"),
                new ImageListEntry("menutree-audit-active", "audit-active"),
                new ImageListEntry("menutree-file-maintenance-active", "file-maintenance-active"),
                new ImageListEntry("menutree-import-export-active", "import-export-active"),
                new ImageListEntry("menutree-account-maintenance-active", "account-maintenance-active"),
                new ImageListEntry("menutree-compute-active", "compute-active"),
                new ImageListEntry("menutree-street-maintenance", "street-maintenance")
            });
            return imageList;
        }

        private void ShortMainActions(ref short intMenu)
        {
            switch (intMenu)
            {
                case 1:
                    {
                        if (modPPGN.Statics.strShortScreen != "N")
                        {
                            frmPPMasterShort.InstancePtr.Close();
                            frmPPGetAccount.InstancePtr.Show(App.MainForm);
                            frmPPGetAccount.InstancePtr.Text = "Short Maintenance";
                            frmPPGetAccount.InstancePtr.txtHold.Text = "S";
                        }
                        break;
                    }
                case 4:
                    {
                        modPPGN.ComputeExemptions();
                        break;
                    }
                case 5:
                    {
                        frmPPAuditBilling.InstancePtr.Show(App.MainForm);
                        break;
                    }
            }
        }

        private void MainActions(short intMenu)
        {
            if (modPPGN.Statics.boolShortScreenOnly)
            {
                ShortMainActions(ref intMenu);
                return;
            }
            switch (intMenu)
            {
                case 1:
                    {
                        frmPPSearch.InstancePtr.Unload();
                        if (modPPGN.Statics.strLongScreen != "N")
                        {
                            frmPPMaster.InstancePtr.Close();
                            if (modPPGN.Statics.PPBillingAssessing == true)
                            {
                                frmPPGetAccount.InstancePtr.txtHold.Text = "L";
                            }
                            else
                            {
                                frmPPGetAccount.InstancePtr.txtHold.Text = "S";
                            }
                            frmPPGetAccount.InstancePtr.Text = "Account Maintenance";
                            frmPPGetAccount.InstancePtr.Show(App.MainForm);
                        }
                        break;
                    }
                case 2:
                    {
                        frmPPSearch.InstancePtr.Unload();
                        if (modPPGN.Statics.strShortScreen != "N")
                        {
                            frmPPGetAccount.InstancePtr.Show(App.MainForm);
                            frmPPGetAccount.InstancePtr.Text = "Short Maintenance";
                            frmPPGetAccount.InstancePtr.txtHold.Text = "S";
                        }
                        break;
                    }
            }
        }

        public void ShortMainCaptions()
        {
            int intCount;
            int lngFCode = 0;
            string strTemp = "";
            bool boolDisable = false;
            string imageKey = "";
            for (intCount = 1; intCount <= 8; intCount++)
            {
                boolDisable = false;
                lngFCode = 0;
                switch (intCount)
                {
                    case 1:
                        {
                            imageKey = "short-maintenance";
                            strTemp = "Short Maintenance";
                            LabelNumber[Strings.Asc("1")] = 1;
                            lngFCode = modSecurity.SHORTMAINTENANCE;
                            break;
                        }
                    case 2:
                        {
                            imageKey = "cost-files";
                            strTemp = "Cost Files";
                            LabelNumber[Strings.Asc("2")] = 2;
                            lngFCode = modSecurity.COSTFILEMENU;
                            break;
                        }
                    case 3:
                        {
                            imageKey = "printing";
                            strTemp = "Printing";
                            LabelNumber[Strings.Asc("3")] = 3;
                            lngFCode = modSecurity.PRINTMENUSECURITY;
                            break;
                        }
                    case 4:
                        {
                            imageKey = "compute";
                            strTemp = "Calculate Exemptions";
                            LabelNumber[Strings.Asc("4")] = 4;
                            lngFCode = modSecurity.CALCULATEEXEMPTIONS;
                            break;
                        }
                    case 5:
                        {
                            imageKey = "audit";
                            strTemp = "Audit Summary";
                            LabelNumber[Strings.Asc("5")] = 5;
                            lngFCode = modSecurity.AUDITSUMMARYMENU;
                            break;
                        }
                    case 6:
                        {
                            imageKey = "import-export";
                            strTemp = "Import / Export";
                            LabelNumber[Strings.Asc("6")] = 6;
                            lngFCode = modSecurity.IMPORTEXPORTMENU;
                            break;
                        }
                    case 7:
                        {
                            imageKey = "file-maintenance";
                            strTemp = "File Maintenance";
                            LabelNumber[Strings.Asc("M")] = 7;
                            lngFCode = modSecurity.FILEMAINTENANCEMENU;
                            break;
                        }
                    case 8:
                        {
                            if (modGlobalConstants.Statics.MuniName.ToUpper() == "BATH")
                            {
                                strTemp = "Custom Programs";
                                imageKey = "street-maintenance";
                                LabelNumber[Strings.Asc("8")] = 8;
                                lngFCode = 0;
                            }
                            else
                            {
                                strTemp = "";
                            }
                            break;
                        }
                }
                if (lngFCode != 0)
                {
                    boolDisable |= !modSecurity.ValidPermissions(this, lngFCode, false);
                }
                if (!String.IsNullOrEmpty(strTemp))
                {
                    FCMenuItem newItem = App.MainForm.NavigationMenu.Add(strTemp, "Menu" + intCount, "MAIN", !boolDisable, 1, imageKey);
                    switch (intCount)
                    {
                        case 2:
                            {
                                ShortCostCaptions(newItem);
                                break;
                            }
                        case 3:
                            {
                                ShortPrintCaptions(newItem);
                                break;
                            }
                        case 6:
                            {
                                ShortImportExportCaptions(newItem);
                                break;
                            }
                        case 7:
                            {
                                ShortFileCaptions(newItem);
                                break;
                            }
                        case 8:
                            {
                                CustomCaptions(newItem);
                                break;
                            }
                    }
                }
            }
        }

        public object MainCaptions()
        {
            object MainCaptions = null;
            int intCount;
            int lngFCode = 0;
            string strTemp = "";
            bool boolDisable = false;
            string imageKey = "";
            App.MainForm.Caption = "PERSONAL PROPERTY";
            App.MainForm.NavigationMenu.OriginName = "Personal Property";
            App.MainForm.ApplicationIcon = "icon-personal-property";
            string assemblyName = GetType().Assembly.GetName().Name;
            if (!App.MainForm.ApplicationIcons.ContainsKey(assemblyName))
            {
                App.MainForm.ApplicationIcons.Add(assemblyName, "icon-personal-property");
            }
            if (modPPGN.Statics.boolShortScreenOnly)
            {
                ShortMainCaptions();
                return MainCaptions;
            }
            for (intCount = 1; intCount <= 8; intCount++)
            {
                boolDisable = false;
                lngFCode = 0;
                switch (intCount)
                {
                    case 1:
                        {
                            imageKey = "account-maintenance";
                            strTemp = "Account Maintenance";
                            LabelNumber[Strings.Asc("1")] = 1;
                            lngFCode = modSecurity.ACCOUNTMAINTENANCE;
                            break;
                        }
                    case 2:
                        {
                            imageKey = "short-maintenance";
                            strTemp = "Short Maintenance";
                            LabelNumber[Strings.Asc("2")] = 2;
                            lngFCode = modSecurity.SHORTMAINTENANCE;
                            break;
                        }
                    case 3:
                        {
                            imageKey = "cost-files";
                            strTemp = "Cost Files";
                            LabelNumber[Strings.Asc("3")] = 3;
                            lngFCode = modSecurity.COSTFILEMENU;
                            break;
                        }
                    case 4:
                        {
                            imageKey = "printing";
                            strTemp = "Printing";
                            LabelNumber[Strings.Asc("4")] = 4;
                            lngFCode = modSecurity.PRINTMENUSECURITY;
                            break;
                        }
                    case 5:
                        {
                            imageKey = "compute";
                            strTemp = "Compute";
                            LabelNumber[Strings.Asc("5")] = 5;
                            lngFCode = modSecurity.COMPUTEMENU;
                            break;
                        }
                    case 6:
                        {
                            imageKey = "import-export";
                            strTemp = "Import / Export";
                            LabelNumber[Strings.Asc("6")] = 6;
                            lngFCode = modSecurity.IMPORTEXPORTMENU;
                            break;
                        }
                    case 7:
                        {
                            imageKey = "file-maintenance";
                            strTemp = "File Maintenance";
                            LabelNumber[Strings.Asc("M")] = 7;
                            lngFCode = modSecurity.FILEMAINTENANCEMENU;
                            break;
                        }
                    case 8:
                        {
                            if (modGlobalConstants.Statics.MuniName.ToUpper() == "BATH")
                            {
                                strTemp = "Custom Programs";
                                imageKey = "street-maintenance";
                                LabelNumber[Strings.Asc("8")] = 8;
                                lngFCode = 0;
                            }
                            else
                            {
                                strTemp = "";
                            }
                            break;
                        }

                }
                if (lngFCode != 0)
                {
                    boolDisable |= !modSecurity.ValidPermissions(this, lngFCode, false);
                }
                if (!String.IsNullOrEmpty(strTemp))
                {
                    FCMenuItem newItem = App.MainForm.NavigationMenu.Add(strTemp, "Menu" + intCount, "MAIN", !boolDisable, 1, imageKey);
                    switch (intCount)
                    {
                        case 3:
                            {
                                CostCaptions(newItem);
                                break;
                            }
                        case 4:
                            {
                                PrintCaptions(newItem);
                                break;
                            }
                        case 5:
                            {
                                CalcuationCaptions(newItem);
                                break;
                            }
                        case 6:
                            {
                                ImportExportCaptions(newItem);
                                break;
                            }
                        case 7:
                            {
                                FileCaptions(newItem);
                                break;
                            }

                        case 8:
                            {
                                CustomCaptions(newItem);
                                break;
                            }

                    }
                }
            }
            return MainCaptions;
        }

        private void CustomCaptions(FCMenuItem parent)
        {
            int lngFCode = 0;
            int intCount;
            string strTemp = "";
            bool boolDisable = false;
            string imageKey = "";
            bool add = false;

            for (intCount = 1; intCount <= 1; intCount++)
            {
                boolDisable = false;
                lngFCode = 0;
                switch (intCount)
                {
                    case 1:
                        {
                            if (modGlobalConstants.Statics.MuniName.ToUpper() == "BATH")
                            {
                                strTemp = "Import Bath File";
                                add = true;
                            }
                            break;
                        }
                }

                if (lngFCode != 0)
                {
                    boolDisable |= !modSecurity.ValidPermissions(this, lngFCode, false);
                }

                if (add)
                {
                    FCMenuItem item = parent.SubItems.Add(strTemp, "Menu" + intCount, "CUSTOM", !boolDisable, 2);
                }
            }
        }

        private void CustomActions(short intMenu)
        {
            switch (intMenu)
            {
                case 1:
                    {
                        frmBathPPImport.InstancePtr.Show(App.MainForm);
                        break;
                    }
            }
        }

        private void ShortImportExportActions(short intMenu)
        {
            switch (intMenu)
            {
                case 1:
                    {
                        frmDBExtract.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 2:
                    {
                        modPPGN.Create_User_Extract();
                        break;
                    }
            }
        }

        private void ImportExportActions_2(short intMenu)
        {
            ImportExportActions(intMenu);
        }

        private void ImportExportActions(short intMenu)
        {
            if (modPPGN.Statics.boolShortScreenOnly)
            {
                ShortImportExportActions(intMenu);
                return;
            }
            try
            {
                switch (intMenu)
                {
                    case 1:
                        {

                            frmDBExtract.InstancePtr.Show(App.MainForm);
                            break;
                        }
                    case 2:
                        {
                            modPPGN.Create_User_Extract();
                            break;
                        }
                    case 3:
                        {
                            string strPath = "";
                            strPath = "";
                            MDIParent.InstancePtr.CommonDialog1.DialogTitle = "Choose file to import";
                            MDIParent.InstancePtr.CommonDialog1.CancelError = true;
                            try
                            {
                                MDIParent.InstancePtr.CommonDialog1.ShowOpen();
                            }
                            catch
                            {
                            }
                            if (Information.Err().Number != 0)
                            {
                                Information.Err().Clear();
                                return;
                            }
                            if (!string.IsNullOrWhiteSpace(MDIParent.InstancePtr.CommonDialog1.FileName))
                            {
                                strPath = MDIParent.InstancePtr.CommonDialog1.FileName;
                                frmPickItemFormat.InstancePtr.Init(strPath);
                            }
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                if (Information.Err().Number != 32755)
                {
                    MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ImportExportActions", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
            }
        }


        private void ShortImportExportCaptions(FCMenuItem parent)
        {
            int lngFCode = 0;
            int intCount;
            string strTemp = "";
            bool boolDisable = false;
            string imageKey = "";

            for (intCount = 1; intCount <= 2; intCount++)
            {
                boolDisable = false;
                lngFCode = 0;
                switch (intCount)
                {
                    case 1:
                        {
                            strTemp = "Create DB Extract";
                            LabelNumber[Strings.Asc("1")] = 1;
                            lngFCode = modSecurity.USEREXTRACTMENU;
                            break;
                        }
                    case 2:
                        {
                            strTemp = "Create User Extract";
                            LabelNumber[Strings.Asc("2")] = 2;
                            lngFCode = modSecurity.USEREXTRACTMENU;
                            break;
                        }
                }

                if (lngFCode != 0)
                {
                    boolDisable |= !modSecurity.ValidPermissions(this, lngFCode, false);
                }
                FCMenuItem item = parent.SubItems.Add(strTemp, "Menu" + intCount, "IMPORTEXPORT", !boolDisable, 2);
            }
        }

        private void ImportExportCaptions(FCMenuItem parent)
        {
            int intCount;
            int lngFCode = 0;
            string strTemp = "";
            bool boolDisable = false;
            string imageKey = "";
            if (modPPGN.Statics.boolShortScreenOnly)
            {
                ShortImportExportCaptions(parent);
                return;
            }

            for (intCount = 1; intCount <= 3; intCount++)
            {
                boolDisable = false;
                lngFCode = 0;
                switch (intCount)
                {
                    case 1:
                        {
                            strTemp = "Create DB Extract";
                            LabelNumber[Strings.Asc("1")] = 1;
                            lngFCode = modSecurity.CNSTCREATEEXTRACT;
                            break;
                        }
                    case 2:
                        {
                            strTemp = "Create User Extract";
                            LabelNumber[Strings.Asc("2")] = 2;
                            lngFCode = modSecurity.USEREXTRACTMENU;
                            break;
                        }
                    case 3:
                        {
                            strTemp = "Itemized from File";
                            LabelNumber[Strings.Asc("3")] = 3;
                            lngFCode = modSecurity.CNSTITEMIZEDFROMFILE;
                            break;
                        }
                }

                if (lngFCode != 0)
                {
                    boolDisable |= !modSecurity.ValidPermissions(this, lngFCode, false);
                }
                FCMenuItem item = parent.SubItems.Add(strTemp, "Menu" + intCount, "IMPORTEXPORT", !boolDisable, 2);
            }
        }

        private void ShortCalculationActions(ref short intMenu)
        {
            switch (intMenu)
            {
                case 1:
                    {
                        frmPPAuditBilling.InstancePtr.Show(App.MainForm);
                        break;
                    }
            }
        }

        private void CalculationActions_2(short intMenu)
        {
            CalculationActions(ref intMenu);
        }

        private void CalculationActions(ref short intMenu)
        {
            clsDRWrapper clsLoad = new clsDRWrapper();
            int lngAcct;
            if (modPPGN.Statics.boolShortScreenOnly)
            {
                ShortCalculationActions(ref intMenu);
                return;
            }
            switch (intMenu)
            {
                case 1:
                    {
                        frmPrintSummary.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 2:
                    {
                        modPPGN.Transfer_Values_To_Billing();
                        break;
                    }
                case 3:
                    {
                        modPPGN.ComputeExemptions();
                        break;
                    }
                case 4:
                    {
                        frmPPAuditBilling.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 5:
                    {
                        frmExemptAudit.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 6:
                    {
                        frmStatus.InstancePtr.Show(App.MainForm);
                        break;
                    }
            }
        }

        private void ShortCalculationCaptions(FCMenuItem parent)
        {
            int intCount;
            int lngFCode = 0;
            string strTemp = "";
            bool boolDisable = false;
            string imageKey = "";

            for (intCount = 1; intCount <= 1; intCount++)
            {
                boolDisable = false;
                lngFCode = 0;
                switch (intCount)
                {
                    case 1:
                        {
                            strTemp = "Audit Summary";
                            LabelNumber[Strings.Asc("1")] = 1;
                            lngFCode = modSecurity.AUDITSUMMARYMENU;
                            break;
                        }
                }

                if (lngFCode != 0)
                {
                    boolDisable |= !modSecurity.ValidPermissions(this, lngFCode, false);
                }

                FCMenuItem item = parent.SubItems.Add(strTemp, "Menu" + intCount, "CALCULATE", !boolDisable, 2);
            }
        }

        public object CalcuationCaptions(FCMenuItem parent)
        {
            object CalcuationCaptions = null;
            int intCount;
            int lngFCode = 0;
            string strTemp = "";
            bool boolDisable = false;
            string imageKey = "";
            if (modPPGN.Statics.boolShortScreenOnly)
            {
                ShortCalculationCaptions(parent);
                return CalcuationCaptions;
            }

            for (intCount = 1; intCount <= 6; intCount++)
            {
                boolDisable = false;
                lngFCode = 0;
                switch (intCount)
                {
                    case 1:
                        {
                            strTemp = "Batch Calculate";
                            LabelNumber[Strings.Asc("1")] = 1;
                            lngFCode = modSecurity.PRINTSUMMARYMENU;
                            break;
                        }
                    case 2:
                        {
                            strTemp = "Commit to Billing Values";
                            LabelNumber[Strings.Asc("2")] = 2;
                            lngFCode = modSecurity.TRANSFERTOBILLINGMENU;
                            break;
                        }
                    case 3:
                        {
                            strTemp = "Calculate Exemptions";
                            LabelNumber[Strings.Asc("3")] = 3;
                            lngFCode = modSecurity.CALCULATEEXEMPTIONS;
                            break;
                        }
                    case 4:
                        {
                            strTemp = "Audit Summary";
                            LabelNumber[Strings.Asc("4")] = 4;
                            lngFCode = modSecurity.AUDITSUMMARYMENU;
                            break;
                        }
                    case 5:
                        {
                            strTemp = "Exempt Audit";
                            LabelNumber[Strings.Asc("5")] = 5;
                            lngFCode = modSecurity.AUDITSUMMARYMENU;
                            break;
                        }
                    case 6:
                        {
                            strTemp = "Status";
                            LabelNumber[Strings.Asc("6")] = 6;
                            break;
                        }
                }

                if (lngFCode != 0)
                {
                    boolDisable |= !modSecurity.ValidPermissions(this, lngFCode, false);
                }

                FCMenuItem item = parent.SubItems.Add(strTemp, "Menu" + intCount, "CALCULATE", !boolDisable, 2);
            }

            return CalcuationCaptions;
        }

        private void PrintActions_2(short intMenu)
        {
            PrintActions(ref intMenu);
        }

        private void PrintActions(ref short intMenu)
        {
            if (modPPGN.Statics.boolShortScreenOnly)
            {
                ShortPrintActions(ref intMenu);
                return;
            }
            switch (intMenu)
            {
                case 1:
                    {
                        frmPPPrinting.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 2:
                    {
                        dlgTaxNotice.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 3:
                    {
                        frmPPReimbursementNotices.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 4:
                    {
                        frmPPTop40Taxpayers.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 5:
                    {
                        rptDeletedAccounts.InstancePtr.Init();
                        break;
                    }
                case 6:
                    {
                        frmLabelPrinting.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 7:
                    {
                        frmReportViewer.InstancePtr.Init(rptAssessByCode.InstancePtr, boolAllowEmail: true, strAttachmentName: "AssessByCode");
                        break;
                    }
            }
        }

        private void ShortPrintActions(ref short intMenu)
        {
            switch (intMenu)
            {
                case 1:
                    {
                        frmPPPrinting.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 2:
                    {
                        dlgTaxNotice.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 3:
                    {
                        frmPPTop40Taxpayers.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 4:
                    {
                        rptDeletedAccounts.InstancePtr.Init();
                        break;
                    }
                case 5:
                    {
                        frmLabelPrinting.InstancePtr.Show(App.MainForm);
                        break;
                    }
            }

        }

        private void ShortPrintCaptions(FCMenuItem parent)
        {
            int intCount;
            int lngFCode = 0;
            string strTemp = "";
            bool boolDisable = false;
            string imageKey = "";

            for (intCount = 1; intCount <= 5; intCount++)
            {
                boolDisable = false;
                lngFCode = 0;
                switch (intCount)
                {
                    case 1:
                        {
                            strTemp = "Print Account List";
                            LabelNumber[Strings.Asc("1")] = 1;
                            lngFCode = modSecurity.ACCOUNTLISTPRINTMENU;
                            break;
                        }
                    case 2:
                        {
                            strTemp = "Taxpayer Notices";
                            LabelNumber[Strings.Asc("2")] = 2;
                            lngFCode = modSecurity.TAXPAYERNOTICESMENU;
                            break;
                        }
                    case 3:
                        {
                            strTemp = "Highest Assessment Listing";
                            LabelNumber[Strings.Asc("3")] = 3;
                            lngFCode = modSecurity.HIGHESTASSESSMENTMENU;
                            break;
                        }
                    case 4:
                        {
                            strTemp = "Deleted Accounts";
                            LabelNumber[Strings.Asc("4")] = 4;
                            lngFCode = modSecurity.DELETEDACCOUNTPRINTMENU;
                            break;
                        }
                    case 5:
                        {
                            strTemp = "Labels";
                            LabelNumber[Strings.Asc("5")] = 5;
                            lngFCode = modSecurity.LABELSPRINTMENU;
                            break;
                        }
                }

                if (lngFCode != 0)
                {
                    boolDisable |= !modSecurity.ValidPermissions(this, lngFCode, false);
                }
                FCMenuItem item = parent.SubItems.Add(strTemp, "Menu" + intCount, "PRINT", !boolDisable, 2);
            }
        }

        public object PrintCaptions(FCMenuItem parent)
        {
            object PrintCaptions = null;
            int intCount;
            int lngFCode = 0;
            string strTemp = "";
            bool boolDisable = false;
            string imageKey = "";
            if (modPPGN.Statics.boolShortScreenOnly)
            {
                ShortPrintCaptions(parent);
                return PrintCaptions;
            }

            for (intCount = 1; intCount <= 7; intCount++)
            {
                lngFCode = 0;
                switch (intCount)
                {
                    case 1:
                        {
                            strTemp = "Print Account List";
                            LabelNumber[Strings.Asc("1")] = 1;
                            lngFCode = modSecurity.ACCOUNTLISTPRINTMENU;
                            break;
                        }
                    case 2:
                        {
                            strTemp = "Taxpayer Notices";
                            LabelNumber[Strings.Asc("2")] = 2;
                            lngFCode = modSecurity.TAXPAYERNOTICESMENU;
                            break;
                        }
                    case 3:
                        {
                            strTemp = "Reimbursement Notices";
                            LabelNumber[Strings.Asc("3")] = 3;
                            lngFCode = modSecurity.REIMBURSEMENTNOTICESMENU;
                            break;
                        }
                    case 4:
                        {
                            strTemp = "Highest Assessment Listing";
                            LabelNumber[Strings.Asc("4")] = 4;
                            lngFCode = modSecurity.HIGHESTASSESSMENTMENU;
                            break;
                        }
                    case 5:
                        {
                            strTemp = "Deleted Accounts";
                            LabelNumber[Strings.Asc("5")] = 5;
                            lngFCode = modSecurity.DELETEDACCOUNTPRINTMENU;
                            break;
                        }
                    case 6:
                        {
                            strTemp = "Labels";
                            LabelNumber[Strings.Asc("6")] = 6;
                            lngFCode = modSecurity.LABELSPRINTMENU;
                            break;
                        }
                    case 7:
                        {
                            strTemp = "Assessment By Code";
                            LabelNumber[Strings.Asc("7")] = 7;
                            break;
                        }
                }

                if (lngFCode != 0)
                {
                    boolDisable |= !modSecurity.ValidPermissions(this, lngFCode, false);
                }
                FCMenuItem item = parent.SubItems.Add(strTemp, "Menu" + intCount, "PRINT", !boolDisable, 2);
            }
            return PrintCaptions;
        }

        private void CostActions_2(short intMenu)
        {
            CostActions(ref intMenu);
        }

        private void CostActions(ref short intMenu)
        {
            if (modPPGN.Statics.boolShortScreenOnly)
            {
                ShortCostActions(ref intMenu);
                return;
            }
            switch (intMenu)
            {
                case 1:
                    {
                        frmPPCostRatios.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 2:
                    {
                        frmTrendExempt.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 3:
                    {
                        frmPPBusinessCodes.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 4:
                    {
                        frmPPStreetTranCodes.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 5:
                    {
                        frmEditDefaultItems.InstancePtr.Init();
                        break;
                    }
            }
        }

        public void ShortCostActions(ref short intMenu)
        {
            switch (intMenu)
            {
                case 1:
                    {
                        frmPPCostRatios.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 2:
                    {
                        frmTrendExempt.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 3:
                    {
                        frmPPBusinessCodes.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 4:
                    {
                        frmPPStreetTranCodes.InstancePtr.Show(App.MainForm);
                        break;
                    }
            }
        }

        public void ShortCostCaptions(FCMenuItem parent)
        {
            int intCount;
            int lngFCode = 0;
            string strTemp = "";
            bool boolDisable = false;
            string imageKey = "";

            for (intCount = 1; intCount <= 4; intCount++)
            {
                lngFCode = 0;
                switch (intCount)
                {
                    case 1:
                        {
                            strTemp = "Ratio / Opens Table";
                            LabelNumber[Strings.Asc("1")] = 1;
                            lngFCode = modSecurity.EDITRATIOOPENSMENU;
                            break;
                        }
                    case 2:
                        {
                            strTemp = "Exemptions / Trending";
                            LabelNumber[Strings.Asc("2")] = 2;
                            lngFCode = modSecurity.EDITEXEMPTIONSTRENDINGMENU;
                            break;
                        }
                    case 3:
                        {
                            strTemp = "Business Codes";
                            LabelNumber[Strings.Asc("3")] = 3;
                            lngFCode = modSecurity.EDITBUSINESSCODESMENU;
                            break;
                        }
                    case 4:
                        {
                            strTemp = "Street & Tran Codes";
                            LabelNumber[Strings.Asc("4")] = 4;
                            lngFCode = modSecurity.EDITSTREETTRANCODESMENU;
                            break;
                        }
                }

                if (lngFCode != 0)
                {
                    boolDisable |= !modSecurity.ValidPermissions(this, lngFCode, false);
                }
                FCMenuItem item = parent.SubItems.Add(strTemp, "Menu" + intCount, "COST", !boolDisable, 2);
            }
        }

        public object CostCaptions(FCMenuItem parent)
        {
            object CostCaptions = null;
            int intCount;
            int lngFCode = 0;
            string strTemp = "";
            bool boolDisable = false;
            string imageKey = "";
            if (modPPGN.Statics.boolShortScreenOnly)
            {
                ShortCostCaptions(parent);
                return CostCaptions;
            }

            for (intCount = 1; intCount <= 5; intCount++)
            {
                lngFCode = 0;
                switch (intCount)
                {
                    case 1:
                        {
                            strTemp = "Certified Ratio / Opens Table";
                            LabelNumber[Strings.Asc("1")] = 1;
                            lngFCode = modSecurity.EDITRATIOOPENSMENU;
                            break;
                        }
                    case 2:
                        {
                            strTemp = "Exemptions / Trending";
                            LabelNumber[Strings.Asc("2")] = 2;
                            lngFCode = modSecurity.EDITEXEMPTIONSTRENDINGMENU;
                            break;
                        }
                    case 3:
                        {
                            strTemp = "Business Codes";
                            LabelNumber[Strings.Asc("3")] = 3;
                            lngFCode = modSecurity.EDITBUSINESSCODESMENU;
                            break;
                        }
                    case 4:
                        {
                            strTemp = "Street & Tran Codes";
                            LabelNumber[Strings.Asc("4")] = 4;
                            lngFCode = modSecurity.EDITSTREETTRANCODESMENU;
                            break;
                        }
                    case 5:
                        {
                            strTemp = "Default Items";
                            LabelNumber[Strings.Asc("5")] = 5;
                            break;
                        }
                }

                if (lngFCode != 0)
                {
                    boolDisable |= !modSecurity.ValidPermissions(this, lngFCode, false);
                }
                FCMenuItem item = parent.SubItems.Add(strTemp, "Menu" + intCount, "COST", !boolDisable, 2);
            }
            return CostCaptions;
        }

        private void CDBS()
        {
            cPPDatabase pDB = new cPPDatabase();
            cVersionInfo tVer;
            if (pDB.CheckVersion())
            {
                MessageBox.Show("Database Structure Check completed successfully.", "Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Error Number " + FCConvert.ToString(pDB.LastErrorNumber) + "  " + pDB.LastErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            tVer = pDB.GetVersion();
            MessageBox.Show("Current DB Version is " + tVer.VersionString);
        }

        private void FileActions_2(short intMenu)
        {
            FileActions(ref intMenu);
        }

        private void FileActions(ref short intMenu)
        {
            if (modPPGN.Statics.boolShortScreenOnly)
            {
                ShortFileActions(ref intMenu);
                return;
            }
            switch (intMenu)
            {
                case 1:
                    {
                        frmPPFileMaintenance.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 2:
                    {
                        CDBS();
                        break;
                    }
                case 3:
                    {
                        frmGetGroup.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 4:
                    {
                        frmTransferData.InstancePtr.Show(App.MainForm);
                        break;
                    }

                case 5:
                    {
                        if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modSecurity.CNSTCREATEARCHIVES)) == "F")
                        {
                            frmArchive.InstancePtr.Init("twpp0000.vb1", true);
                        }
                        else
                        {
                            frmArchive.InstancePtr.Init("twpp0000.vb1", false);
                        }
                        break;
                    }
                case 6:
                    {
                        FixAccount();
                        break;
                    }
            }
        }

        private void ShortFileActions(ref short intMenu)
        {
            switch (intMenu)
            {
                case 1:
                    {
                        frmPPFileMaintenance.InstancePtr.Show(App.MainForm);
                        break;
                    }

                case 2:
                    {
                        if (modPPGN.CheckDatabaseStructure())
                        {
                            MessageBox.Show("Successfully checked database structure.", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show("There was an error checking/updating the database structure.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        }
                        FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                        break;
                    }
                case 3:
                    {
                        frmGetGroup.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 4:
                    {
                        FixAccount();
                        break;
                    }
            }
        }

        private void ShortFileCaptions(FCMenuItem parent)
        {
            int intCount;
            int lngFCode = 0;
            string strTemp = "";
            bool boolDisable = false;
            string imageKey = "";

            for (intCount = 1; intCount <= 4; intCount++)
            {
                lngFCode = 0;
                switch (intCount)
                {
                    case 1:
                        {
                            strTemp = "Customize";
                            LabelNumber[Strings.Asc("1")] = 1;
                            lngFCode = modSecurity.SETTINGSMENU;
                            break;
                        }
                    case 2:
                        {
                            strTemp = "Check Database Structure";
                            LabelNumber[Strings.Asc("2")] = 2;
                            break;
                        }
                    case 3:
                        {
                            strTemp = "Group Maintenance";
                            LabelNumber[Strings.Asc("3")] = 3;
                            break;
                        }

                    case 4:
                        {
                            strTemp = "Fix Account";
                            LabelNumber[Strings.Asc("4")] = 4;
                            break;
                        }
                }

                if (lngFCode != 0)
                {
                    boolDisable |= !modSecurity.ValidPermissions(this, lngFCode, false);
                }
                FCMenuItem item = parent.SubItems.Add(strTemp, "Menu" + intCount, "FILE", !boolDisable, 2);
            }
        }

        public object FileCaptions(FCMenuItem parent)
        {
            object FileCaptions = null;
            int intCount;
            int lngFCode = 0;
            string strTemp = "";
            bool boolDisable = false;
            string imageKey = "";
            if (modPPGN.Statics.boolShortScreenOnly)
            {
                ShortFileCaptions(parent);
                return FileCaptions;
            }

            for (intCount = 1; intCount < 7; intCount++)
            {
                boolDisable = false;
                lngFCode = 0;
                switch (intCount)
                {
                    case 1:
                        {
                            strTemp = "Customize";
                            LabelNumber[Strings.Asc("1")] = 1;
                            lngFCode = modSecurity.SETTINGSMENU;
                            break;
                        }
                    case 2:
                        {
                            strTemp = "Check Database Structure";
                            LabelNumber[Strings.Asc("2")] = 2;
                            break;
                        }
                    case 3:
                        {
                            strTemp = "Group Maintenance";
                            LabelNumber[Strings.Asc("3")] = 3;
                            break;
                        }
                    case 4:
                        {
                            strTemp = "Transfer Item/Leased Info";
                            LabelNumber[Strings.Asc("4")] = 4;
                            lngFCode = modSecurity.TRANSFERMENU;
                            break;
                        }

                    case 5:
                        {
                            strTemp = "Archives";
                            LabelNumber[Strings.Asc("5")] = 5;
                            lngFCode = modSecurity.CNSTRUNARCHIVES;
                            break;
                        }
                    case 6:
                        {
                            strTemp = "Fix Account";
                            LabelNumber[Strings.Asc("6")] = 6;
                            break;
                        }
                }

                if (lngFCode != 0)
                {
                    boolDisable |= !modSecurity.ValidPermissions(this, lngFCode, false);
                }
                FCMenuItem item = parent.SubItems.Add(strTemp, "Menu" + intCount, "FILE", !boolDisable, 2);
            }

            return FileCaptions;
        }

        private void FixAccount()
        {
            object lngAccount = 0;
            clsDRWrapper clsLoad = new clsDRWrapper();
            try
            {
                if (frmInput.InstancePtr.Init(ref lngAccount, "Account to Fix", "Enter the Account to Fix", 1440, false, modGlobalConstants.InputDTypes.idtWholeNumber, "", true))
                {
                    clsLoad.OpenRecordset("select * from ppmaster where account = " + FCConvert.ToString(lngAccount), modPPGN.strPPDatabase);
                    if (!clsLoad.EndOfFile())
                    {
                        clsLoad.MoveNext();
                        while (!clsLoad.EndOfFile())
                        {
                            clsLoad.Delete();
                            clsLoad.MoveNext();
                        }
                        clsLoad.OpenRecordset("select * from ppvaluations where valuekey = " + FCConvert.ToString(lngAccount), modPPGN.strPPDatabase);
                        if (!clsLoad.EndOfFile())
                        {
                            clsLoad.MoveNext();
                            while (!clsLoad.EndOfFile())
                            {
                                clsLoad.Delete();
                                clsLoad.MoveNext();
                            }
                        }
                    }
                    MessageBox.Show("Fix Complete", "Done", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In FixAccount", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        public class StaticVariables
        {
            public int R = 0, c;
        }

        public static StaticVariables Statics
        {
            get
            {
                return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
            }
        }
    }
}
