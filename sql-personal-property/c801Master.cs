﻿//Fecher vbPorter - Version 1.0.0.32
using Global;

namespace TWPP0000
{
	public class c801Master
	{
		//=========================================================
		private string strMunicipality = string.Empty;
		private string strMunicipalCode = string.Empty;
		private string strApplicantName = string.Empty;
		private string strLocatedAt = string.Empty;
		private int intTaxYear;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cGenericCollection colDetailsB = new cGenericCollection();
		private cGenericCollection colDetailsB_AutoInitialized;

		private cGenericCollection colDetailsB
		{
			get
			{
				if (colDetailsB_AutoInitialized == null)
				{
					colDetailsB_AutoInitialized = new cGenericCollection();
				}
				return colDetailsB_AutoInitialized;
			}
			set
			{
				colDetailsB_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cGenericCollection colDetailsA = new cGenericCollection();
		private cGenericCollection colDetailsA_AutoInitialized;

		private cGenericCollection colDetailsA
		{
			get
			{
				if (colDetailsA_AutoInitialized == null)
				{
					colDetailsA_AutoInitialized = new cGenericCollection();
				}
				return colDetailsA_AutoInitialized;
			}
			set
			{
				colDetailsA_AutoInitialized = value;
			}
		}

		private int lngAccount;
		private double dblAssessedTax;
		private double dblTaxRate;
		private string strReportDate = string.Empty;
		private string strTaxRate = string.Empty;

		public string DisplayedRate
		{
			set
			{
				strTaxRate = value;
			}
			get
			{
				string DisplayedRate = "";
				DisplayedRate = strTaxRate;
				return DisplayedRate;
			}
		}

		public double TaxRate
		{
			set
			{
				dblTaxRate = value;
			}
			get
			{
				double TaxRate = 0;
				TaxRate = dblTaxRate;
				return TaxRate;
			}
		}

		public string ReportDate
		{
			set
			{
				strReportDate = value;
			}
			get
			{
				string ReportDate = "";
				ReportDate = strReportDate;
				return ReportDate;
			}
		}

		public double AssessedTax
		{
			set
			{
				dblAssessedTax = value;
			}
			get
			{
				double AssessedTax = 0;
				AssessedTax = dblAssessedTax;
				return AssessedTax;
			}
		}

		public int Account
		{
			set
			{
				lngAccount = value;
			}
			get
			{
				int Account = 0;
				Account = lngAccount;
				return Account;
			}
		}

		public cGenericCollection DetailsA
		{
			set
			{
				colDetailsA = value;
			}
			get
			{
				cGenericCollection DetailsA = null;
				DetailsA = colDetailsA;
				return DetailsA;
			}
		}

		public cGenericCollection DetailsB
		{
			set
			{
				colDetailsB = value;
			}
			get
			{
				cGenericCollection DetailsB = null;
				DetailsB = colDetailsB;
				return DetailsB;
			}
		}

		public string Municipality
		{
			set
			{
				strMunicipality = value;
			}
			get
			{
				string Municipality = "";
				Municipality = strMunicipality;
				return Municipality;
			}
		}

		public string MunicipalCode
		{
			set
			{
				strMunicipalCode = value;
			}
			get
			{
				string MunicipalCode = "";
				MunicipalCode = strMunicipalCode;
				return MunicipalCode;
			}
		}

		public string ApplicantName
		{
			set
			{
				strApplicantName = value;
			}
			get
			{
				string ApplicantName = "";
				ApplicantName = strApplicantName;
				return ApplicantName;
			}
		}

		public string LocatedAt
		{
			set
			{
				strLocatedAt = value;
			}
			get
			{
				string LocatedAt = "";
				LocatedAt = strLocatedAt;
				return LocatedAt;
			}
		}

		public int TaxYear
		{
			set
			{
				intTaxYear = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				int TaxYear = 0;
				TaxYear = intTaxYear;
				return TaxYear;
			}
		}
	}
}
