﻿//Fecher vbPorter - Version 1.0.0.32
using fecherFoundation;
using Global;
using System;
using Wisej.Core;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for srptItemizedCalc.
	/// </summary>
	public partial class srptItemizedCalc : FCSectionReport
	{
		public srptItemizedCalc()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ActiveReport1";
		}

		public static srptItemizedCalc InstancePtr
		{
			get
			{
				return (srptItemizedCalc)Sys.GetInstance(typeof(srptItemizedCalc));
			}
		}

		protected srptItemizedCalc _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptItemizedCalc	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngLine;
		const int CNSTITEMIZEDCOLBETEEXEMPT = 15;
		const int CNSTITEMIZEDCOLBETEYEAR = 16;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = (lngLine >= frmPPCalculations.InstancePtr.vsItemized.Rows);
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lngLine = 1;
			if (frmPPCalculations.InstancePtr.vsItemized.Rows < 2)
			{
				this.Close();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			txtLine.Text = FCConvert.ToString(lngLine);
			txtCD.Text = frmPPCalculations.InstancePtr.vsItemized.TextMatrix(lngLine, 3);
			txtDescription.Text = frmPPCalculations.InstancePtr.vsItemized.TextMatrix(lngLine, 5);
			if (Strings.Trim(frmPPCalculations.InstancePtr.vsItemized.TextMatrix(lngLine, 2)) == "*")
			{
				txtR.Text = "*";
			}
			else
			{
				txtR.Text = "";
			}
			if (Strings.Trim(frmPPCalculations.InstancePtr.vsItemized.TextMatrix(lngLine, 3)) == "-")
			{
				txtQTY.Text = "";
				txtYear.Text = "";
				txtDepYrs.Text = "";
				txtSRO.Text = "";
				txtCost.Text = "";
				txtGD.Text = "";
				txtFCT.Text = "";
				txtValue.Text = "";
				txtYearExempt.Text = "";
				txtBETEExempt.Text = "";
			}
			else
			{
				txtQTY.Text = frmPPCalculations.InstancePtr.vsItemized.TextMatrix(lngLine, 4);
				if ((Conversion.Val(frmPPCalculations.InstancePtr.vsItemized.TextMatrix(lngLine, 6))) > 0 || (frmPPCalculations.InstancePtr.vsItemized.TextMatrix(lngLine, 6) == "00"))
				{
					txtYear.Text = frmPPCalculations.InstancePtr.vsItemized.TextMatrix(lngLine, 6);
				}
				else
				{
					txtYear.Text = "";
				}
				if (Conversion.Val(frmPPCalculations.InstancePtr.vsItemized.TextMatrix(lngLine, CNSTITEMIZEDCOLBETEYEAR)) > 0)
				{
					txtYearExempt.Text = frmPPCalculations.InstancePtr.vsItemized.TextMatrix(lngLine, CNSTITEMIZEDCOLBETEYEAR);
				}
				else
				{
					txtYearExempt.Text = "";
				}
				txtDepYrs.Text = frmPPCalculations.InstancePtr.vsItemized.TextMatrix(lngLine, 7);
				txtSRO.Text = frmPPCalculations.InstancePtr.vsItemized.TextMatrix(lngLine, 8);
				txtCost.Text = Strings.Format(frmPPCalculations.InstancePtr.vsItemized.TextMatrix(lngLine, 10), "###,###,###,###");
				txtGD.Text = frmPPCalculations.InstancePtr.vsItemized.TextMatrix(lngLine, 11);
				txtFCT.Text = frmPPCalculations.InstancePtr.vsItemized.TextMatrix(lngLine, 12);
				txtValue.Text = Strings.Format(frmPPCalculations.InstancePtr.vsItemized.TextMatrix(lngLine, 13), "###,###,###,###");
				txtBETEExempt.Text = Strings.Format(frmPPCalculations.InstancePtr.vsItemized.TextMatrix(lngLine, CNSTITEMIZEDCOLBETEEXEMPT), "###,###,###,###");
			}
			lngLine += 1;
		}

		
	}
}
