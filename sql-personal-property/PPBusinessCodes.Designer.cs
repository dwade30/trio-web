//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmPPBusinessCodes.
	/// </summary>
	partial class frmPPBusinessCodes : BaseForm
	{
		public fecherFoundation.FCGrid vs1;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuAddCode;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteCode;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnusepar3;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnusepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPPBusinessCodes));
			this.vs1 = new fecherFoundation.FCGrid();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddCode = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDeleteCode = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnusepar3 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnusepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSave = new fecherFoundation.FCButton();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.cmdAddCode = new fecherFoundation.FCButton();
			this.cmdDeleteCode = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteCode)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.vs1);
			this.ClientArea.Size = new System.Drawing.Size(1078, 498);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdPrint);
			this.TopPanel.Controls.Add(this.cmdDeleteCode);
			this.TopPanel.Controls.Add(this.cmdAddCode);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdAddCode, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDeleteCode, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(190, 30);
			this.HeaderText.Text = "Business Codes";
			// 
			// vs1
			// 
			this.vs1.AllowSelection = false;
			this.vs1.AllowUserToResizeColumns = false;
			this.vs1.AllowUserToResizeRows = false;
			this.vs1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vs1.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vs1.BackColorAlternate = System.Drawing.SystemColors.Window;
			this.vs1.BackColorBkg = System.Drawing.SystemColors.AppWorkspace;
			this.vs1.BackColorFixed = System.Drawing.SystemColors.Control;
			this.vs1.BackColorSel = System.Drawing.SystemColors.Highlight;
			this.vs1.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vs1.Cols = 2;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vs1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vs1.ColumnHeadersHeight = 30;
			this.vs1.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vs1.DefaultCellStyle = dataGridViewCellStyle2;
			this.vs1.DragIcon = null;
			this.vs1.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vs1.ExtendLastCol = true;
			this.vs1.FixedCols = 0;
			this.vs1.ForeColorFixed = System.Drawing.SystemColors.ControlText;
			this.vs1.FrozenCols = 0;
			this.vs1.GridColor = System.Drawing.SystemColors.Control;
			this.vs1.GridColorFixed = System.Drawing.SystemColors.ControlDark;
			this.vs1.Location = new System.Drawing.Point(30, 30);
			this.vs1.Name = "vs1";
			this.vs1.ReadOnly = true;
			this.vs1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.vs1.RowHeadersVisible = false;
			this.vs1.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vs1.RowHeightMin = 0;
			this.vs1.Rows = 1;
			this.vs1.ScrollTipText = null;
			this.vs1.ShowColumnVisibilityMenu = false;
			this.vs1.ShowFocusCell = false;
			this.vs1.Size = new System.Drawing.Size(1017, 439);
			this.vs1.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTAbove;
			this.vs1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vs1.TabIndex = 0;
			this.vs1.Visible = false;
			this.vs1.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vs1_ChangeEdit);
			this.vs1.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vs1_BeforeEdit);
			this.vs1.KeyDown += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEvent);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuAddCode,
				this.mnuDeleteCode,
				this.mnuSepar1,
				this.mnuPrint,
				this.mnusepar3,
				this.mnuSave,
				this.mnuSaveExit,
				this.mnusepar,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuAddCode
			// 
			this.mnuAddCode.Index = 0;
			this.mnuAddCode.Name = "mnuAddCode";
			this.mnuAddCode.Text = "Add Code";
			this.mnuAddCode.Click += new System.EventHandler(this.mnuAddCode_Click);
			// 
			// mnuDeleteCode
			// 
			this.mnuDeleteCode.Index = 1;
			this.mnuDeleteCode.Name = "mnuDeleteCode";
			this.mnuDeleteCode.Text = "Delete Code";
			this.mnuDeleteCode.Click += new System.EventHandler(this.mnuDeleteCode_Click);
			// 
			// mnuSepar1
			// 
			this.mnuSepar1.Index = 2;
			this.mnuSepar1.Name = "mnuSepar1";
			this.mnuSepar1.Text = "-";
			// 
			// mnuPrint
			// 
			this.mnuPrint.Index = 3;
			this.mnuPrint.Name = "mnuPrint";
			this.mnuPrint.Text = "Print";
			this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
			// 
			// mnusepar3
			// 
			this.mnusepar3.Index = 4;
			this.mnusepar3.Name = "mnusepar3";
			this.mnusepar3.Text = "-";
			// 
			// mnuSave
			// 
			this.mnuSave.Index = 5;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuSave.Text = "Save";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Index = 6;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Text = "Save & Exit";
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// mnusepar
			// 
			this.mnusepar.Index = 7;
			this.mnusepar.Name = "mnusepar";
			this.mnusepar.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 8;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(30, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(76, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// cmdPrint
			// 
			this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrint.AppearanceKey = "toolbarButton";
			this.cmdPrint.Location = new System.Drawing.Point(822, 29);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Size = new System.Drawing.Size(43, 24);
			this.cmdPrint.TabIndex = 1;
			this.cmdPrint.Text = "Print";
			this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
			// 
			// cmdAddCode
			// 
			this.cmdAddCode.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdAddCode.AppearanceKey = "toolbarButton";
			this.cmdAddCode.Location = new System.Drawing.Point(871, 29);
			this.cmdAddCode.Name = "cmdAddCode";
			this.cmdAddCode.Size = new System.Drawing.Size(78, 24);
			this.cmdAddCode.TabIndex = 1;
			this.cmdAddCode.Text = "Add Code";
			this.cmdAddCode.Click += new System.EventHandler(this.cmdAddCode_Click);
			// 
			// cmdDeleteCode
			// 
			this.cmdDeleteCode.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDeleteCode.AppearanceKey = "toolbarButton";
			this.cmdDeleteCode.Location = new System.Drawing.Point(955, 29);
			this.cmdDeleteCode.Name = "cmdDeleteCode";
			this.cmdDeleteCode.Size = new System.Drawing.Size(95, 24);
			this.cmdDeleteCode.TabIndex = 2;
			this.cmdDeleteCode.Text = "Delete Code";
			this.cmdDeleteCode.Click += new System.EventHandler(this.cmdDeleteCode_Click);
			// 
			// frmPPBusinessCodes
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmPPBusinessCodes";
			this.ShowInTaskbar = false;
			this.Text = "Business Codes";
			this.Load += new System.EventHandler(this.frmPPBusinessCodes_Load);
			this.Activated += new System.EventHandler(this.frmPPBusinessCodes_Activated);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPPBusinessCodes_KeyDown);
			this.Resize += new System.EventHandler(this.frmPPBusinessCodes_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteCode)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		private FCButton cmdPrint;
		private FCButton cmdAddCode;
		private FCButton cmdDeleteCode;
	}
}