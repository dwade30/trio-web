﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmPPStreetTranCodes.
	/// </summary>
	partial class frmPPStreetTranCodes : BaseForm
	{
		public fecherFoundation.FCGrid vsStreet;
		public fecherFoundation.FCGrid vsTran;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuAddTran;
		public fecherFoundation.FCToolStripMenuItem mnuAddStreet;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteTran;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteStreet;
		public fecherFoundation.FCToolStripMenuItem mnusepar4;
		public fecherFoundation.FCToolStripMenuItem mnuPrintTran;
		public fecherFoundation.FCToolStripMenuItem mnuPrintStreet;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnusepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPPStreetTranCodes));
            this.vsStreet = new fecherFoundation.FCGrid();
            this.vsTran = new fecherFoundation.FCGrid();
            this.MainMenu1 = new Wisej.Web.MainMenu(this.components);
            this.mnuPrintTran = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintStreet = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddTran = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddStreet = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteTran = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteStreet = new fecherFoundation.FCToolStripMenuItem();
            this.mnusepar4 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnusepar = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdAddTran = new fecherFoundation.FCButton();
            this.cmdAddStreet = new fecherFoundation.FCButton();
            this.cmdDeleteTran = new fecherFoundation.FCButton();
            this.cmdDeleteStreet = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsStreet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsTran)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddTran)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddStreet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteTran)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteStreet)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 564);
            this.BottomPanel.Size = new System.Drawing.Size(836, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.vsStreet);
            this.ClientArea.Controls.Add(this.vsTran);
            this.ClientArea.Size = new System.Drawing.Size(836, 504);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdDeleteStreet);
            this.TopPanel.Controls.Add(this.cmdDeleteTran);
            this.TopPanel.Controls.Add(this.cmdAddStreet);
            this.TopPanel.Controls.Add(this.cmdAddTran);
            this.TopPanel.Size = new System.Drawing.Size(836, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddTran, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddStreet, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteTran, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteStreet, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(236, 30);
            this.HeaderText.Text = "Street & Tran Codes";
            // 
            // vsStreet
            // 
            this.vsStreet.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsStreet.ExtendLastCol = true;
            this.vsStreet.FixedCols = 0;
            this.vsStreet.Location = new System.Drawing.Point(30, 365);
            this.vsStreet.MinimumSize = new System.Drawing.Size(200, 125);
            this.vsStreet.Name = "vsStreet";
            this.vsStreet.RowHeadersVisible = false;
            this.vsStreet.Rows = 50;
            this.vsStreet.Size = new System.Drawing.Size(776, 125);
            this.vsStreet.TabIndex = 1;
            this.vsStreet.Visible = false;
            this.vsStreet.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsStreet_ChangeEdit);
            this.vsStreet.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsStreet_BeforeEdit);
            this.vsStreet.KeyDown += new Wisej.Web.KeyEventHandler(this.vsStreet_KeyDownEvent);
            // 
            // vsTran
            // 
            this.vsTran.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsTran.ExtendLastCol = true;
            this.vsTran.FixedCols = 0;
            this.vsTran.Location = new System.Drawing.Point(30, 30);
            this.vsTran.MinimumSize = new System.Drawing.Size(200, 125);
            this.vsTran.Name = "vsTran";
            this.vsTran.RowHeadersVisible = false;
            this.vsTran.Rows = 50;
            this.vsTran.Size = new System.Drawing.Size(776, 320);
            this.vsTran.TabIndex = 2;
            this.vsTran.Visible = false;
            this.vsTran.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsTran_ChangeEdit);
            this.vsTran.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsTran_BeforeEdit);
            this.vsTran.KeyDown += new Wisej.Web.KeyEventHandler(this.vsTran_KeyDownEvent);
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPrintTran,
            this.mnuPrintStreet});
            this.MainMenu1.Name = "MainMenu1";
            // 
            // mnuPrintTran
            // 
            this.mnuPrintTran.Index = 0;
            this.mnuPrintTran.Name = "mnuPrintTran";
            this.mnuPrintTran.Text = "Print Tran Codes";
            this.mnuPrintTran.Click += new System.EventHandler(this.mnuPrintTran_Click);
            // 
            // mnuPrintStreet
            // 
            this.mnuPrintStreet.Index = 1;
            this.mnuPrintStreet.Name = "mnuPrintStreet";
            this.mnuPrintStreet.Text = "Print Street Codes";
            this.mnuPrintStreet.Click += new System.EventHandler(this.mnuPrintStreet_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 0;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // mnuAddTran
            // 
            this.mnuAddTran.Index = -1;
            this.mnuAddTran.Name = "mnuAddTran";
            this.mnuAddTran.Text = "Add Tran Code";
            this.mnuAddTran.Click += new System.EventHandler(this.mnuAddTran_Click);
            // 
            // mnuAddStreet
            // 
            this.mnuAddStreet.Index = -1;
            this.mnuAddStreet.Name = "mnuAddStreet";
            this.mnuAddStreet.Text = "Add Street Code";
            this.mnuAddStreet.Click += new System.EventHandler(this.mnuAddStreet_Click);
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = -1;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuDeleteTran
            // 
            this.mnuDeleteTran.Index = -1;
            this.mnuDeleteTran.Name = "mnuDeleteTran";
            this.mnuDeleteTran.Text = "Delete Tran Code";
            this.mnuDeleteTran.Click += new System.EventHandler(this.mnuDeleteTran_Click);
            // 
            // mnuDeleteStreet
            // 
            this.mnuDeleteStreet.Index = -1;
            this.mnuDeleteStreet.Name = "mnuDeleteStreet";
            this.mnuDeleteStreet.Text = "Delete Street Code";
            this.mnuDeleteStreet.Click += new System.EventHandler(this.mnuDeleteStreet_Click);
            // 
            // mnusepar4
            // 
            this.mnusepar4.Index = -1;
            this.mnusepar4.Name = "mnusepar4";
            this.mnusepar4.Text = "-";
            // 
            // mnuSepar1
            // 
            this.mnuSepar1.Index = -1;
            this.mnuSepar1.Name = "mnuSepar1";
            this.mnuSepar1.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = -1;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = -1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnusepar
            // 
            this.mnusepar.Index = -1;
            this.mnusepar.Name = "mnusepar";
            this.mnusepar.Text = "-";
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(349, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(100, 48);
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // cmdAddTran
            // 
            this.cmdAddTran.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddTran.Location = new System.Drawing.Point(700, 29);
            this.cmdAddTran.Name = "cmdAddTran";
            this.cmdAddTran.Size = new System.Drawing.Size(108, 24);
            this.cmdAddTran.TabIndex = 1;
            this.cmdAddTran.Text = "Add Tran Code";
            this.cmdAddTran.Click += new System.EventHandler(this.mnuAddTran_Click);
            // 
            // cmdAddStreet
            // 
            this.cmdAddStreet.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddStreet.Location = new System.Drawing.Point(576, 29);
            this.cmdAddStreet.Name = "cmdAddStreet";
            this.cmdAddStreet.Size = new System.Drawing.Size(118, 24);
            this.cmdAddStreet.TabIndex = 1;
            this.cmdAddStreet.Text = "Add Street Code";
            this.cmdAddStreet.Click += new System.EventHandler(this.mnuAddStreet_Click);
            // 
            // cmdDeleteTran
            // 
            this.cmdDeleteTran.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteTran.Location = new System.Drawing.Point(446, 29);
            this.cmdDeleteTran.Name = "cmdDeleteTran";
            this.cmdDeleteTran.Size = new System.Drawing.Size(124, 24);
            this.cmdDeleteTran.TabIndex = 1;
            this.cmdDeleteTran.Text = "Delete Tran Code";
            this.cmdDeleteTran.Click += new System.EventHandler(this.mnuDeleteTran_Click);
            // 
            // cmdDeleteStreet
            // 
            this.cmdDeleteStreet.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteStreet.Location = new System.Drawing.Point(308, 29);
            this.cmdDeleteStreet.Name = "cmdDeleteStreet";
            this.cmdDeleteStreet.Size = new System.Drawing.Size(132, 24);
            this.cmdDeleteStreet.TabIndex = 1;
            this.cmdDeleteStreet.Text = "Delete Street Code";
            this.cmdDeleteStreet.Click += new System.EventHandler(this.mnuDeleteStreet_Click);
            // 
            // frmPPStreetTranCodes
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(836, 672);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmPPStreetTranCodes";
            this.Text = "Street & Tran Codes";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmPPStreetTranCodes_Load);
            this.Activated += new System.EventHandler(this.frmPPStreetTranCodes_Activated);
            this.Resize += new System.EventHandler(this.frmPPStreetTranCodes_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPPStreetTranCodes_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsStreet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsTran)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddTran)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddStreet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteTran)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteStreet)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		private FCButton cmdAddTran;
		private FCButton cmdAddStreet;
		private FCButton cmdDeleteTran;
		private FCButton cmdDeleteStreet;
	}
}
