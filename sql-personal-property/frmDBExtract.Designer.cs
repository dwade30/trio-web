﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmDBExtract.
	/// </summary>
	partial class frmDBExtract : BaseForm
	{
		public fecherFoundation.FCComboBox cmbFileType;
		public fecherFoundation.FCLabel lblFileType;
		public fecherFoundation.FCComboBox cmbOrder;
		public fecherFoundation.FCLabel lblOrder;
		public fecherFoundation.FCComboBox cmbRange;
		public fecherFoundation.FCLabel lblRange;
		public fecherFoundation.FCCheckBox chkColumnHeaders;
		public fecherFoundation.FCDraggableListBox lstLeased;
		public fecherFoundation.FCDraggableListBox lstItemized;
		public fecherFoundation.FCDraggableListBox lstAccountInfo;
		public fecherFoundation.FCTextBox txtEnd;
		public fecherFoundation.FCTextBox txtStart;
		public fecherFoundation.FCLabel lblTo;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDBExtract));
			this.cmbFileType = new fecherFoundation.FCComboBox();
			this.lblFileType = new fecherFoundation.FCLabel();
			this.cmbOrder = new fecherFoundation.FCComboBox();
			this.lblOrder = new fecherFoundation.FCLabel();
			this.cmbRange = new fecherFoundation.FCComboBox();
			this.lblRange = new fecherFoundation.FCLabel();
			this.chkColumnHeaders = new fecherFoundation.FCCheckBox();
			this.lstLeased = new fecherFoundation.FCDraggableListBox();
			this.lstItemized = new fecherFoundation.FCDraggableListBox();
			this.lstAccountInfo = new fecherFoundation.FCDraggableListBox();
			this.txtEnd = new fecherFoundation.FCTextBox();
			this.txtStart = new fecherFoundation.FCTextBox();
			this.lblTo = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkColumnHeaders)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 610);
			this.BottomPanel.Size = new System.Drawing.Size(952, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtEnd);
			this.ClientArea.Controls.Add(this.chkColumnHeaders);
			this.ClientArea.Controls.Add(this.txtStart);
			this.ClientArea.Controls.Add(this.lblTo);
			this.ClientArea.Controls.Add(this.lblOrder);
			this.ClientArea.Controls.Add(this.cmbOrder);
			this.ClientArea.Controls.Add(this.lstLeased);
			this.ClientArea.Controls.Add(this.lstItemized);
			this.ClientArea.Controls.Add(this.lstAccountInfo);
			this.ClientArea.Controls.Add(this.cmbFileType);
			this.ClientArea.Controls.Add(this.lblFileType);
			this.ClientArea.Controls.Add(this.cmbRange);
			this.ClientArea.Controls.Add(this.lblRange);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Size = new System.Drawing.Size(952, 550);
			this.ClientArea.TabIndex = 0;
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(952, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(201, 30);
			this.HeaderText.Text = "Database Extract";
			// 
			// cmbFileType
			// 
			this.cmbFileType.AutoSize = false;
			this.cmbFileType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbFileType.FormattingEnabled = true;
			this.cmbFileType.Items.AddRange(new object[] {
				"Account Information",
				"Itemized",
				"Leased"
			});
			this.cmbFileType.Location = new System.Drawing.Point(163, 150);
			this.cmbFileType.Name = "cmbFileType";
			this.cmbFileType.Size = new System.Drawing.Size(213, 40);
			this.cmbFileType.TabIndex = 8;
			this.cmbFileType.Text = "Account Information";
			this.cmbFileType.SelectedIndexChanged += new System.EventHandler(this.cmbFileType_SelectedIndexChanged);
			// 
			// lblFileType
			// 
			this.lblFileType.AutoSize = true;
			this.lblFileType.Location = new System.Drawing.Point(30, 164);
			this.lblFileType.Name = "lblFileType";
			this.lblFileType.Size = new System.Drawing.Size(69, 15);
			this.lblFileType.TabIndex = 7;
			this.lblFileType.Text = "FILE TYPE";
			// 
			// cmbOrder
			// 
			this.cmbOrder.AutoSize = false;
			this.cmbOrder.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbOrder.FormattingEnabled = true;
			this.cmbOrder.Items.AddRange(new object[] {
				"Account",
				"Name",
				"Open 1",
				"Open 2",
				"Business Code",
				"Tran Code",
				"Street Code"
			});
			this.cmbOrder.Location = new System.Drawing.Point(163, 90);
			this.cmbOrder.Name = "cmbOrder";
			this.cmbOrder.Size = new System.Drawing.Size(213, 40);
			this.cmbOrder.TabIndex = 6;
			this.cmbOrder.Text = "Account";
			// 
			// lblOrder
			// 
			this.lblOrder.AutoSize = true;
			this.lblOrder.Location = new System.Drawing.Point(30, 104);
			this.lblOrder.Name = "lblOrder";
			this.lblOrder.Size = new System.Drawing.Size(72, 15);
			this.lblOrder.TabIndex = 5;
			this.lblOrder.Text = "ORDER BY";
			// 
			// cmbRange
			// 
			this.cmbRange.AutoSize = false;
			this.cmbRange.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbRange.FormattingEnabled = true;
			this.cmbRange.Items.AddRange(new object[] {
				"All",
				"Range",
				"Individual"
			});
			this.cmbRange.Location = new System.Drawing.Point(163, 30);
			this.cmbRange.Name = "cmbRange";
			this.cmbRange.Size = new System.Drawing.Size(213, 40);
			this.cmbRange.TabIndex = 1;
			this.cmbRange.Text = "All";
			this.cmbRange.SelectedIndexChanged += new System.EventHandler(this.optRange_CheckedChanged);
			// 
			// lblRange
			// 
			this.lblRange.AutoSize = true;
			this.lblRange.Location = new System.Drawing.Point(30, 45);
			this.lblRange.Name = "lblRange";
			this.lblRange.Size = new System.Drawing.Size(78, 15);
			this.lblRange.TabIndex = 0;
			this.lblRange.Text = "SELECTION";
			// 
			// chkColumnHeaders
			// 
			this.chkColumnHeaders.Location = new System.Drawing.Point(30, 210);
			this.chkColumnHeaders.Name = "chkColumnHeaders";
			this.chkColumnHeaders.Size = new System.Drawing.Size(251, 27);
			this.chkColumnHeaders.TabIndex = 9;
			this.chkColumnHeaders.Text = "Make first row column headers";
			// 
			// lstLeased
			// 
			this.lstLeased.Appearance = 0;
			this.lstLeased.BackColor = System.Drawing.SystemColors.Window;
			this.lstLeased.CheckBoxes = true;
			this.lstLeased.Location = new System.Drawing.Point(30, 257);
			this.lstLeased.MultiSelect = 0;
			this.lstLeased.Name = "lstLeased";
			this.lstLeased.Size = new System.Drawing.Size(395, 263);
			this.lstLeased.Sorted = false;
			this.lstLeased.Style = 1;
			this.lstLeased.TabIndex = 10;
			this.lstLeased.Visible = false;
			// 
			// lstItemized
			// 
			this.lstItemized.Appearance = 0;
			this.lstItemized.BackColor = System.Drawing.SystemColors.Window;
			this.lstItemized.CheckBoxes = true;
			this.lstItemized.Location = new System.Drawing.Point(30, 257);
			this.lstItemized.MultiSelect = 0;
			this.lstItemized.Name = "lstItemized";
			this.lstItemized.Size = new System.Drawing.Size(395, 263);
			this.lstItemized.Sorted = false;
			this.lstItemized.Style = 1;
			this.lstItemized.TabIndex = 20;
			this.lstItemized.Visible = false;
			// 
			// lstAccountInfo
			// 
			this.lstAccountInfo.AllowDrag = true;
			this.lstAccountInfo.AllowDrop = true;
			this.lstAccountInfo.Appearance = 0;
			this.lstAccountInfo.BackColor = System.Drawing.SystemColors.Window;
			this.lstAccountInfo.CheckBoxes = true;
			this.lstAccountInfo.Location = new System.Drawing.Point(30, 257);
			this.lstAccountInfo.MultiSelect = 0;
			this.lstAccountInfo.Name = "lstAccountInfo";
			this.lstAccountInfo.Size = new System.Drawing.Size(395, 263);
			this.lstAccountInfo.Sorted = false;
			this.lstAccountInfo.Style = 1;
			this.lstAccountInfo.TabIndex = 19;
			// 
			// txtEnd
			// 
			this.txtEnd.AutoSize = false;
			this.txtEnd.BackColor = System.Drawing.SystemColors.Window;
			this.txtEnd.LinkItem = null;
			this.txtEnd.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtEnd.LinkTopic = null;
			this.txtEnd.Location = new System.Drawing.Point(698, 90);
			this.txtEnd.Name = "txtEnd";
			this.txtEnd.Size = new System.Drawing.Size(155, 40);
			this.txtEnd.TabIndex = 4;
			this.txtEnd.Visible = false;
			// 
			// txtStart
			// 
			this.txtStart.AutoSize = false;
			this.txtStart.BackColor = System.Drawing.SystemColors.Window;
			this.txtStart.LinkItem = null;
			this.txtStart.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtStart.LinkTopic = null;
			this.txtStart.Location = new System.Drawing.Point(450, 90);
			this.txtStart.Name = "txtStart";
			this.txtStart.Size = new System.Drawing.Size(155, 40);
			this.txtStart.TabIndex = 2;
			this.txtStart.Visible = false;
			// 
			// lblTo
			// 
			this.lblTo.Location = new System.Drawing.Point(635, 104);
			this.lblTo.Name = "lblTo";
			this.lblTo.Size = new System.Drawing.Size(33, 15);
			this.lblTo.TabIndex = 3;
			this.lblTo.Text = "TO";
			this.lblTo.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.lblTo.Visible = false;
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(453, 257);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(223, 111);
			this.Label2.TabIndex = 11;
			this.Label2.Text = "DRAG ITEMS TO CHANGE THE ORDER THE ITEMS ARE PLACED IN THE FILE";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuSaveContinue,
				this.Seperator,
				this.mnuExit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuSaveContinue
			// 
			this.mnuSaveContinue.Index = 0;
			this.mnuSaveContinue.Name = "mnuSaveContinue";
			this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveContinue.Text = "Save & Continue";
			this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(297, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(100, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// frmDBExtract
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(881, 718);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmDBExtract";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Database Extract";
			this.Load += new System.EventHandler(this.frmDBExtract_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmDBExtract_KeyDown);
            this.Resize += FrmDBExtract_Resize;
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkColumnHeaders)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);
		}
        #endregion

        private FCButton cmdSave;
		private System.ComponentModel.IContainer components;
	}
}
