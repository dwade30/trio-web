﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmPPCostRatios.
	/// </summary>
	public partial class frmPPCostRatios : BaseForm
	{
		public frmPPCostRatios()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPPCostRatios InstancePtr
		{
			get
			{
				return (frmPPCostRatios)Sys.GetInstance(typeof(frmPPCostRatios));
			}
		}

		protected frmPPCostRatios _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		clsDRWrapper rsRatio = new clsDRWrapper();
		clsDRWrapper rsTrends = new clsDRWrapper();
		string strSQL;
		bool boolLoading;
		int lngLastTownCode;

		private void ResizeGridTownCode()
		{
			//gridTownCode.HeightOriginal = gridTownCode.RowHeight(0) + 60;
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			//FC:FINAL:MSH - i.issue #1281: restore closing functionality
			//- frmPPCostRatios.Close();
			this.Unload();
			this.Dispose();
		}

		public void cmdQuit_Click()
		{
			cmdQuit_Click(cmdQuit, new System.EventArgs());
		}

		private void gridTownCode_ComboCloseUp(object sender, EventArgs e)
		{
			// CustomizeStuff.TaxRate = Val(txtTaxRate.Text)
			if (!boolLoading)
			{
				if (modGlobal.Statics.CustomizeStuff.boolRegionalTown)
				{
					modGlobal.Statics.CustomizeStuff.CurrentTown = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.ComboData())));
				}
				// CustomizeStuff.TaxRate = RoundDbl(CDbl(txtTaxRate.Text) / 1000, 6)
				txtRatio.Text = FCConvert.ToString(100 * modGlobal.Statics.CustomizeStuff.dblRatio);
				//Application.DoEvents();
				txtRatio.Focus();
				//Application.DoEvents();
			}
		}

		private void gridTownCode_Leave(object sender, System.EventArgs e)
		{
			//Application.DoEvents();
			if (!boolLoading)
			{
				if (lngLastTownCode == Conversion.Val(gridTownCode.TextMatrix(0, 0)))
					return;
				LoadTownSpecificStuff();
				lngLastTownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
			}
		}

		private void LoadTownSpecificStuff()
		{
			int lngTownCode;
			lngTownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
			modGlobal.Statics.CustomizeStuff.CurrentTown = lngTownCode;
			txtRatio.Text = FCConvert.ToString(100 * modGlobal.Statics.CustomizeStuff.dblRatio);
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: Answer As string	OnWrite(DialogResult)
			string Answer = "";
			clsDRWrapper clsSave = new clsDRWrapper();
			vs.Col = 0;
			if (modPPGN.Statics.strCostFiles != "N")
			{
				if (Conversion.Val(txtRatio.Text) < 1)
				{
					//FC:FINAL:MSH - converting to int is incorrect, because Answer will has converted to int DialogResult 
					// and will be compared with string results (same with I.Issue #820)
					//Answer = FCConvert.ToString(FCConvert.ToInt32(MessageBox.Show("The value in the Ratio box is less than 1. A default value of 100 will be used.", "Ratio Value", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning));
					Answer = FCConvert.ToString(MessageBox.Show("The value in the Ratio box is less than 1. A default value of 100 will be used.", "Ratio Value", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning));
					if (Answer == FCConvert.ToString(DialogResult.Cancel))
					{
						txtRatio.Text = "";
						txtRatio.Focus();
					}
					else
					{
						txtRatio.Text = "100";
						// Unload frmPPCostRatios
					}
				}
				else
				{
					// Unload frmPPCostRatios
				}
				clsSave.OpenRecordset("select * from PPRATIOOPENS", "twpp0000.vb1");
				clsSave.Edit();
				clsSave.Set_Fields("ratio", FCConvert.ToString(Conversion.Val(txtRatio.Text)));
				clsSave.Set_Fields("openfield1", txtOpen1.Text);
				clsSave.Set_Fields("openfield2", txtOpen2.Text);
				clsSave.Update();
				int intRow;
				clsSave.OpenRecordset("select * from ratiotrends", modPPGN.strPPDatabase);
				for (intRow = 1; intRow <= vs.Rows - 1; intRow++)
				{
					if (clsSave.FindFirstRecord("ID", vs.TextMatrix(intRow, 0)))
					{
						clsSave.Edit();
						clsSave.Set_Fields("high", FCConvert.ToString(Conversion.Val(vs.TextMatrix(intRow, 2))));
						clsSave.Set_Fields("low", FCConvert.ToString(Conversion.Val(vs.TextMatrix(intRow, 3))));
						clsSave.Set_Fields("exponent", FCConvert.ToString(Conversion.Val(vs.TextMatrix(intRow, 4))));
						clsSave.Set_Fields("Life", FCConvert.ToString(Conversion.Val(vs.TextMatrix(intRow, 5))));
						clsSave.Set_Fields("SD", vs.TextMatrix(intRow, 6));
						clsSave.Set_Fields("trend", vs.TextMatrix(intRow, 7));
						clsSave.Set_Fields("description", vs.TextMatrix(intRow, 8));
						clsSave.Set_Fields("CategoryType", FCConvert.ToString(Conversion.Val(vs.TextMatrix(intRow, 9))));
						clsSave.Update();
					}
				}
				// intRow
				modGlobal.Statics.CustomizeStuff.dblRatio = Conversion.Val(txtRatio.Text) / 100;
				modGlobal.Statics.CustomizeStuff.SaveCustomInfo();
				// if it is a regional town then must save all ratios.
				MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		public void cmdSave_Click()
		{
			cmdSave_Click(cmdSave, new System.EventArgs());
		}

		private void ResizeVs()
		{
			// vbPorter upgrade warning: GridWidth As Variant --> As int
			int GridWidth = 0;
			GridWidth = vs.WidthOriginal;
			vs.ColWidth(0, 0);
			vs.ColWidth(1, FCConvert.ToInt32(0.06 * GridWidth));
			vs.ColWidth(2, FCConvert.ToInt32(0.06 * GridWidth));
			vs.ColWidth(3, FCConvert.ToInt32(0.06 * GridWidth));
			vs.ColWidth(4, FCConvert.ToInt32(0.08 * GridWidth));
			vs.ColWidth(5, FCConvert.ToInt32(0.06 * GridWidth));
			vs.ColWidth(6, FCConvert.ToInt32(0.08 * GridWidth));
			vs.ColWidth(7, FCConvert.ToInt32(0.08 * GridWidth));
			vs.ColWidth(8, FCConvert.ToInt32(0.25 * GridWidth));
		}

		private void FillTheScreen()
		{
			strSQL = "SELECT * FROM PPRatioOpens";
			// 
			rsRatio.OpenRecordset(strSQL, modPPGN.strPPDatabase);
			strSQL = "SELECT * FROM RatioTrends order by type";
			rsTrends.OpenRecordset(strSQL, modPPGN.strPPDatabase);
			txtOpen1.Text = FCConvert.ToString(rsRatio.Get_Fields_String("openfield1"));
			txtOpen2.Text = FCConvert.ToString(rsRatio.Get_Fields_String("openfield2"));
			if (!modGlobal.Statics.CustomizeStuff.boolRegionalTown)
			{
				// TODO Get_Fields: Check the table for the column [ratio] and replace with corresponding Get_Field method
				txtRatio.Text = FCConvert.ToString(Conversion.Val(rsRatio.Get_Fields("ratio") + ""));
			}
			else
			{
				gridTownCode.Visible = true;
				LoadTownSpecificStuff();
			}
			vs.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 1, 9, 1, SystemColors.ControlDark);
			int intRow;
			intRow = 0;
			vs.Rows = 1;
			vs.TextMatrix(0, 1, "Type");
			vs.TextMatrix(0, 2, "High");
			vs.TextMatrix(0, 3, "Low");
			vs.TextMatrix(0, 4, "Exponent");
			vs.TextMatrix(0, 5, "Life");
			vs.TextMatrix(0, 6, "S/D");
			vs.TextMatrix(0, 7, "Trend");
			vs.TextMatrix(0, 8, "Description");
			vs.TextMatrix(0, 9, "Category");
			vs.ColHidden(0, true);
			string strTemp = "";
			strTemp += "#0;Other|";
			strTemp += "#1;Furniture & Fixtures|";
			strTemp += "#2;Machinery & Equipment";
			vs.ColComboList(9, strTemp);
			while (!rsTrends.EndOfFile())
			{
				vs.Rows += 1;
				intRow = vs.Rows - 1;
				// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
				vs.TextMatrix(intRow, 1, FCConvert.ToString(rsTrends.Get_Fields("type")));
				// TODO Get_Fields: Check the table for the column [high] and replace with corresponding Get_Field method
				vs.TextMatrix(intRow, 2, FCConvert.ToString(Conversion.Val(rsTrends.Get_Fields("high"))));
				// TODO Get_Fields: Check the table for the column [low] and replace with corresponding Get_Field method
				vs.TextMatrix(intRow, 3, FCConvert.ToString(Conversion.Val(rsTrends.Get_Fields("low"))));
				vs.TextMatrix(intRow, 4, FCConvert.ToString(Conversion.Val(rsTrends.Get_Fields_Double("exponent"))));
				// TODO Get_Fields: Check the table for the column [life] and replace with corresponding Get_Field method
				vs.TextMatrix(intRow, 5, FCConvert.ToString(Conversion.Val(rsTrends.Get_Fields("life"))));
				vs.TextMatrix(intRow, 6, FCConvert.ToString(rsTrends.Get_Fields_String("SD")));
				// TODO Get_Fields: Check the table for the column [Trend] and replace with corresponding Get_Field method
				vs.TextMatrix(intRow, 7, FCConvert.ToString(rsTrends.Get_Fields("Trend")));
				vs.TextMatrix(intRow, 8, FCConvert.ToString(rsTrends.Get_Fields_String("Description")));
				vs.TextMatrix(intRow, 9, FCConvert.ToString(Conversion.Val(rsTrends.Get_Fields_Int32("CategoryType"))));
				vs.TextMatrix(intRow, 0, FCConvert.ToString(rsTrends.Get_Fields_Int32("ID")));
				rsTrends.MoveNext();
			}
			vs.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs.ColAlignment(7, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			// txtRatio.SetFocus
			//FC:FINAL:MSH - i.issue #1281: incorrect mask format
			//vs.ColEditMask(6, ">L");
			//vs.ColEditMask(7, ">L");
			vs.ColEditMask(6, "L>");
			vs.ColEditMask(7, "L>");
		}

		private void SetupGridTownCode()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			if (modGlobal.Statics.CustomizeStuff.boolRegionalTown)
			{
				strTemp = "";
				clsLoad.OpenRecordset("select * from TranCodes where trancode > 0 order by trancode", modPPGN.strPPDatabase);
				while (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [trancode] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [trancode] and replace with corresponding Get_Field method
					strTemp += "#" + clsLoad.Get_Fields("trancode") + ";" + clsLoad.Get_Fields_String("trantype") + "\t" + clsLoad.Get_Fields("trancode") + "|";
					clsLoad.MoveNext();
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				gridTownCode.ColComboList(0, strTemp);
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
				gridTownCode.Visible = true;
			}
			else
			{
				gridTownCode.Rows = 1;
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
				gridTownCode.Visible = false;
			}
		}

		private void frmPPCostRatios_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				mnuQuit_Click();
			}
		}

		private void frmPPCostRatios_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				cmdQuit_Click();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmPPCostRatios_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPPCostRatios properties;
			//frmPPCostRatios.ScaleWidth	= 8880;
			//frmPPCostRatios.ScaleHeight	= 7170;
			//frmPPCostRatios.LinkTopic	= "Form2";
			//frmPPCostRatios.LockControls	= true;
			//End Unmaped Properties
			boolLoading = true;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGridTownCode();
			if (modGlobal.Statics.CustomizeStuff.boolRegionalTown)
			{
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(1));
				modGlobal.Statics.CustomizeStuff.CurrentTown = 1;
			}
			else
			{
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
			}
			// Set pp = OpenDatabase(DatabaseName, False, False, ";PWD=" & DATABASEPASSWORD)
			// Set Data1.Recordset = pp.OpenRecordset("SELECT * FROM RatioTrends")
			// Set Data2.Recordset = pp.OpenRecordset("SELECT * FROM PPRatioOpens")
			//FC:FINAL:CHN - issue #1451: Fix new line symbol.
			// Label2.Text = "  S  =  Standard      " + "\r\n" + "   D  =  Declining Balance   ";
			Label2.Text = "  S  =  Standard      " + Environment.NewLine + "   D  =  Declining Balance   ";
			vs.TextMatrix(0, 4, "Exponent");
			FillTheScreen();
			boolLoading = false;
		}

		private void frmPPCostRatios_Resize(object sender, System.EventArgs e)
		{
			ResizeVs();
			ResizeGridTownCode();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			modGlobal.SaveWindowSize(this);
			modPPGN.GetOpenFields();
			// menuPPCost.Show
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			frmReportViewer.InstancePtr.Init(rptRatioOpen.InstancePtr, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "RatioOpens", false, false);
		}

		private void mnuQuit_Click(object sender, System.EventArgs e)
		{
			cmdQuit_Click();
		}

		public void mnuQuit_Click()
		{
			mnuQuit_Click(mnuQuit, new System.EventArgs());
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
		}

		public void mnuSave_Click()
		{
			mnuSave_Click(mnuSave, new System.EventArgs());
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			mnuSave_Click();
            //FC:FINAL: AKV form closing
            //mnuQuit_Click();
        }

        private void txtRatio_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolLoading)
			{
				if (Conversion.Val(txtRatio.Text) > 0)
				{
					modGlobal.Statics.CustomizeStuff.dblRatio = modGlobal.RoundDbl(FCConvert.ToDouble(txtRatio.Text) / 100, 6);
				}
				else
				{
					modGlobal.Statics.CustomizeStuff.dblRatio = 0;
				}
			}
		}

		private void vs_EnterCell(object sender, System.EventArgs e)
		{
			if (vs.Col == 8)
			{
				vs.EditMaxLength = 22;
			}
			else
			{
				vs.EditMaxLength = 0;
			}
		}

		private void vs_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - i.issue #1281: save and use correct indexes of the cell
			int row = vs.GetFlexRowIndex(e.RowIndex);
			int col = vs.GetFlexColIndex(e.ColumnIndex);
			switch (col)
			{
				case 1:
					{
						// Do Nothing.  Field is not editable
						break;
					}
				case 2:
					{
						if (Conversion.Val(vs.EditText) < 1 || Conversion.Val(vs.EditText) > 99)
						{
							MessageBox.Show("Please enter a value between 1 and 99.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							vs.EditText = FCConvert.ToString(1);
							e.Cancel = true;
						}
						else if (Conversion.Val(vs.EditText) < Conversion.Val(vs.TextMatrix(row, col + 1)))
						{
							MessageBox.Show("The High column cannot be lower than the Low column.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
						break;
					}
				case 3:
					{
						if (Conversion.Val(vs.EditText) < 1 || Conversion.Val(vs.EditText) > 99)
						{
							MessageBox.Show("Please enter a value between 1 and 99.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							vs.EditText = FCConvert.ToString(1);
							e.Cancel = true;
						}
						else if (Conversion.Val(vs.EditText) > Conversion.Val(vs.TextMatrix(row, col - 1)))
						{
							MessageBox.Show("The Low column cannot be higher than the High column.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
						break;
					}
				case 4:
					{
						if (Conversion.Val(vs.EditText) < 1 || Conversion.Val(vs.EditText) > 9.99)
						{
							MessageBox.Show("Please enter a value between 1 and 9.99.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							vs.EditText = FCConvert.ToString(1);
							e.Cancel = true;
						}
						break;
					}
				case 5:
					{
						if (Conversion.Val(vs.EditText) < 1 || Conversion.Val(vs.EditText) > 100)
						{
							MessageBox.Show("Please enter a value between 1 and 100.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							vs.EditText = FCConvert.ToString(1);
							e.Cancel = true;
						}
						break;
					}
				case 6:
					{
						if (vs.EditText != "S" && vs.EditText != "D")
						{
							MessageBox.Show("Please enter a value of S or D for this field.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							// vs.EditText = "";
							//FC:FINAL:CHN - issue #1323: Incorrect validation. 
							vs.EditText = vs.Text;
							e.Cancel = true;
						}
						break;
					}
				case 7:
					{
						if (vs.EditText != "Y" && vs.EditText != "N")
						{
							MessageBox.Show("Please enter a value of Y or N for this field.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							// vs.EditText = " ";
							//FC:FINAL:CHN - issue #1323: Incorrect validation. 
							vs.EditText = vs.Text;
							e.Cancel = true;
						}
						break;
					}
				case 8:
					{
						// Do Nothing.  Any Entry is OK
						break;
					}
			}
			//end switch
		}
	}
}
