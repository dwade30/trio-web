﻿//Fecher vbPorter - Version 1.0.0.32
using fecherFoundation;
using Global;
using System;
using TWSharedLibrary;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rpt801.
	/// </summary>
	public partial class rpt801 : BaseSectionReport
	{
		public rpt801()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "801";
		}

		public static rpt801 InstancePtr
		{
			get
			{
				return (rpt801)Sys.GetInstance(typeof(rpt801));
			}
		}

		protected rpt801 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rpt801	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private c801Report theReport;

		public void Init(c801Report rep801)
		{
			theReport = rep801;
			frmReportViewer.InstancePtr.Init(this);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = !theReport.Records.IsCurrent();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			theReport.Records.MoveFirst();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (theReport.Records.IsCurrent())
			{
				c801Master m801;
				m801 = (c801Master)theReport.Records.GetCurrentItem();
				if (m801.DetailsA.ItemCount() > 0)
				{
					SubReport1.Report = rpt801A.InstancePtr;
					rpt801A.InstancePtr.SetReportObject(m801);
				}
				else
				{
					SubReport1.Report = null;
				}
				if (m801.DetailsB.ItemCount() > 0)
				{
					SubReport2.Report = rpt801b.InstancePtr;
					rpt801b.InstancePtr.SetReportObject(m801);
				}
				else
				{
					SubReport2.Report = null;
				}
				theReport.Records.MoveNext();
			}
		}

		
	}
}
