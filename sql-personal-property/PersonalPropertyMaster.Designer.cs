﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmPPMaster.
	/// </summary>
	partial class frmPPMaster : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label2;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblstatistics;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblStatics;
		public fecherFoundation.FCButton cmdSearchOwner;
		public fecherFoundation.FCButton cmdEditOwner;
		public fecherFoundation.FCTextBox txtOwnerID;
		public fecherFoundation.FCButton cmdRemoveOwner;
		public fecherFoundation.FCTextBox txtTotalExempt;
		public fecherFoundation.FCTextBox txtTotWithBETE;
		public fecherFoundation.FCTextBox txtApt;
		public fecherFoundation.FCTextBox txtSquareFootage;
		public fecherFoundation.FCButton cmdMaster;
		public fecherFoundation.FCButton cmdItemized;
		public fecherFoundation.FCButton cmdLeased;
		public fecherFoundation.FCTextBox txtOpen2;
		public fecherFoundation.FCTextBox txtOpen1;
		public fecherFoundation.FCTextBox txtStreetCode;
		public fecherFoundation.FCTextBox txtBusinessCode;
		public fecherFoundation.FCTextBox txtExemptCode1;
		public fecherFoundation.FCTextBox txtExemption;
		public fecherFoundation.FCTextBox txtValue;
		public fecherFoundation.FCTextBox txtStreetNumber;
		public fecherFoundation.FCTextBox txtStreetName;
		public fecherFoundation.FCTextBox txtExemptCode2;
		public FCGrid gridTranCode;
		public fecherFoundation.FCLabel Label2_9;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel lblOwner1;
		public fecherFoundation.FCLabel lblAddress;
		public fecherFoundation.FCLabel Label2_0;
		public fecherFoundation.FCLabel Label2_1;
		public fecherFoundation.FCLabel lblComment;
		public fecherFoundation.FCLabel lblstatistics_2;
		public fecherFoundation.FCLabel txtREAssociate;
		public fecherFoundation.FCLabel lblAccount;
		public fecherFoundation.FCLabel lblStatics_14;
		public fecherFoundation.FCLabel lblStatics_15;
		public fecherFoundation.FCLabel lblStatics_16;
		public fecherFoundation.FCLabel lblStatics_17;
		public fecherFoundation.FCLabel lblStatics_18;
		public fecherFoundation.FCLabel lblStatics_19;
		public fecherFoundation.FCLabel lblStatics_0;
		public fecherFoundation.FCLabel lblOpen2;
		public fecherFoundation.FCLabel lblOpen1;
		public fecherFoundation.FCLabel lblStatics_20;
		public fecherFoundation.FCLabel lblStatics_29;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuAddAccount;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteAccount;
		public fecherFoundation.FCToolStripMenuItem Separator3;
		public fecherFoundation.FCToolStripMenuItem mnuCalculateDisplay;
		public fecherFoundation.FCToolStripMenuItem mnuCalculatePrint;
		public fecherFoundation.FCToolStripMenuItem mnuGroup;
		public fecherFoundation.FCToolStripMenuItem mnuInterestedParties;
		public fecherFoundation.FCToolStripMenuItem mnuChangeAssociation;
		public fecherFoundation.FCToolStripMenuItem mnuComment;
		public fecherFoundation.FCToolStripMenuItem mnuSearch;
		public fecherFoundation.FCToolStripMenuItem mnuPrevious;
		public fecherFoundation.FCToolStripMenuItem mnuNext;
		public fecherFoundation.FCToolStripMenuItem Separator6;
		public fecherFoundation.FCToolStripMenuItem mnuPrintBETEApp;
		public fecherFoundation.FCToolStripMenuItem mnuPrintInput;
		public fecherFoundation.FCToolStripMenuItem mnuPrintOriginal;
		public fecherFoundation.FCToolStripMenuItem mnuPrintMasterInfo;
        public fecherFoundation.FCToolStripMenuItem mnuPrintInventoryListing;
		public fecherFoundation.FCToolStripMenuItem mnuPrintLabel;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPPMaster));
            this.cmdSearchOwner = new fecherFoundation.FCButton();
            this.cmdEditOwner = new fecherFoundation.FCButton();
            this.txtOwnerID = new fecherFoundation.FCTextBox();
            this.cmdRemoveOwner = new fecherFoundation.FCButton();
            this.txtTotalExempt = new fecherFoundation.FCTextBox();
            this.txtTotWithBETE = new fecherFoundation.FCTextBox();
            this.txtApt = new fecherFoundation.FCTextBox();
            this.txtSquareFootage = new fecherFoundation.FCTextBox();
            this.cmdMaster = new fecherFoundation.FCButton();
            this.cmdItemized = new fecherFoundation.FCButton();
            this.cmdLeased = new fecherFoundation.FCButton();
            this.txtOpen2 = new fecherFoundation.FCTextBox();
            this.txtOpen1 = new fecherFoundation.FCTextBox();
            this.txtStreetCode = new fecherFoundation.FCTextBox();
            this.txtBusinessCode = new fecherFoundation.FCTextBox();
            this.txtExemptCode1 = new fecherFoundation.FCTextBox();
            this.txtExemption = new fecherFoundation.FCTextBox();
            this.txtValue = new fecherFoundation.FCTextBox();
            this.txtStreetNumber = new fecherFoundation.FCTextBox();
            this.txtStreetName = new fecherFoundation.FCTextBox();
            this.txtExemptCode2 = new fecherFoundation.FCTextBox();
            this.gridTranCode = new fecherFoundation.FCGrid();
            this.Label2_9 = new fecherFoundation.FCLabel();
            this.Label14 = new fecherFoundation.FCLabel();
            this.lblOwner1 = new fecherFoundation.FCLabel();
            this.lblAddress = new fecherFoundation.FCLabel();
            this.Label2_0 = new fecherFoundation.FCLabel();
            this.Label2_1 = new fecherFoundation.FCLabel();
            this.lblComment = new fecherFoundation.FCLabel();
            this.lblstatistics_2 = new fecherFoundation.FCLabel();
            this.txtREAssociate = new fecherFoundation.FCLabel();
            this.lblAccount = new fecherFoundation.FCLabel();
            this.lblStatics_14 = new fecherFoundation.FCLabel();
            this.lblStatics_15 = new fecherFoundation.FCLabel();
            this.lblStatics_16 = new fecherFoundation.FCLabel();
            this.lblStatics_17 = new fecherFoundation.FCLabel();
            this.lblStatics_18 = new fecherFoundation.FCLabel();
            this.lblStatics_19 = new fecherFoundation.FCLabel();
            this.lblStatics_0 = new fecherFoundation.FCLabel();
            this.lblOpen2 = new fecherFoundation.FCLabel();
            this.lblOpen1 = new fecherFoundation.FCLabel();
            this.lblStatics_20 = new fecherFoundation.FCLabel();
            this.lblStatics_29 = new fecherFoundation.FCLabel();
            this.MainMenu1 = new Wisej.Web.MainMenu(this.components);
            this.mnuAddAccount = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteAccount = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCalculatePrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuGroup = new fecherFoundation.FCToolStripMenuItem();
            this.mnuInterestedParties = new fecherFoundation.FCToolStripMenuItem();
            this.mnuChangeAssociation = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintBETEApp = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintInput = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintOriginal = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintMasterInfo = new fecherFoundation.FCToolStripMenuItem();
            this.mnuInventoryListing = new fecherFoundation.FCToolStripMenuItem();
            this.Separator3 = new fecherFoundation.FCToolStripMenuItem();
            this.Separator6 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintLabel = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSearch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrevious = new fecherFoundation.FCToolStripMenuItem();
            this.mnuNext = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCalculateDisplay = new fecherFoundation.FCToolStripMenuItem();
            this.mnuComment = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSaveExit = new fecherFoundation.FCButton();
            this.cmdNextAccount = new fecherFoundation.FCButton();
            this.cmdPreviousAccount = new fecherFoundation.FCButton();
            this.cmdComment = new fecherFoundation.FCButton();
            this.cmdPrintLabel = new fecherFoundation.FCButton();
            this.cmdCalculateDisplay = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearchOwner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEditOwner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRemoveOwner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMaster)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdItemized)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdLeased)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTranCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNextAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPreviousAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCalculateDisplay)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSaveExit);
            this.BottomPanel.Location = new System.Drawing.Point(0, 581);
            this.BottomPanel.Size = new System.Drawing.Size(1058, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.lblStatics_29);
            this.ClientArea.Controls.Add(this.cmdSearchOwner);
            this.ClientArea.Controls.Add(this.cmdEditOwner);
            this.ClientArea.Controls.Add(this.txtOwnerID);
            this.ClientArea.Controls.Add(this.cmdRemoveOwner);
            this.ClientArea.Controls.Add(this.txtTotalExempt);
            this.ClientArea.Controls.Add(this.txtTotWithBETE);
            this.ClientArea.Controls.Add(this.txtApt);
            this.ClientArea.Controls.Add(this.txtSquareFootage);
            this.ClientArea.Controls.Add(this.txtOpen2);
            this.ClientArea.Controls.Add(this.txtOpen1);
            this.ClientArea.Controls.Add(this.txtStreetCode);
            this.ClientArea.Controls.Add(this.txtBusinessCode);
            this.ClientArea.Controls.Add(this.txtExemptCode1);
            this.ClientArea.Controls.Add(this.txtExemption);
            this.ClientArea.Controls.Add(this.txtValue);
            this.ClientArea.Controls.Add(this.txtStreetNumber);
            this.ClientArea.Controls.Add(this.txtStreetName);
            this.ClientArea.Controls.Add(this.txtExemptCode2);
            this.ClientArea.Controls.Add(this.gridTranCode);
            this.ClientArea.Controls.Add(this.Label2_9);
            this.ClientArea.Controls.Add(this.Label14);
            this.ClientArea.Controls.Add(this.lblOwner1);
            this.ClientArea.Controls.Add(this.lblAddress);
            this.ClientArea.Controls.Add(this.Label2_0);
            this.ClientArea.Controls.Add(this.Label2_1);
            this.ClientArea.Controls.Add(this.lblComment);
            this.ClientArea.Controls.Add(this.lblstatistics_2);
            this.ClientArea.Controls.Add(this.lblStatics_14);
            this.ClientArea.Controls.Add(this.lblStatics_15);
            this.ClientArea.Controls.Add(this.lblStatics_16);
            this.ClientArea.Controls.Add(this.lblStatics_17);
            this.ClientArea.Controls.Add(this.lblStatics_18);
            this.ClientArea.Controls.Add(this.lblStatics_19);
            this.ClientArea.Controls.Add(this.lblStatics_0);
            this.ClientArea.Controls.Add(this.lblOpen2);
            this.ClientArea.Controls.Add(this.lblOpen1);
            this.ClientArea.Controls.Add(this.lblStatics_20);
            this.ClientArea.Controls.Add(this.txtREAssociate);
            this.ClientArea.Size = new System.Drawing.Size(1078, 606);
            this.ClientArea.TabIndex = 0;
            this.ClientArea.Controls.SetChildIndex(this.txtREAssociate, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblStatics_20, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblOpen1, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblOpen2, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblStatics_0, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblStatics_19, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblStatics_18, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblStatics_17, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblStatics_16, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblStatics_15, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblStatics_14, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblstatistics_2, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblComment, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label2_1, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label2_0, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblAddress, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblOwner1, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label14, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label2_9, 0);
            this.ClientArea.Controls.SetChildIndex(this.gridTranCode, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtExemptCode2, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtStreetName, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtStreetNumber, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtValue, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtExemption, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtExemptCode1, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtBusinessCode, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtStreetCode, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtOpen1, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtOpen2, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtSquareFootage, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtApt, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtTotWithBETE, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtTotalExempt, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdRemoveOwner, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtOwnerID, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdEditOwner, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdSearchOwner, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblStatics_29, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdItemized);
            this.TopPanel.Controls.Add(this.cmdLeased);
            this.TopPanel.Controls.Add(this.cmdPrintLabel);
            this.TopPanel.Controls.Add(this.cmdComment);
            this.TopPanel.Controls.Add(this.cmdPreviousAccount);
            this.TopPanel.Controls.Add(this.cmdNextAccount);
            this.TopPanel.Controls.Add(this.cmdMaster);
            this.TopPanel.Controls.Add(this.lblAccount);
            this.TopPanel.Controls.Add(this.cmdCalculateDisplay);
            this.TopPanel.Size = new System.Drawing.Size(1078, 60);
            this.TopPanel.Controls.SetChildIndex(this.cmdCalculateDisplay, 0);
            this.TopPanel.Controls.SetChildIndex(this.lblAccount, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdMaster, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdNextAccount, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPreviousAccount, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdComment, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrintLabel, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdLeased, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdItemized, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(91, 28);
            this.HeaderText.Text = "Account";
            // 
            // cmdSearchOwner
            // 
            this.cmdSearchOwner.AppearanceKey = "actionButton";
            this.cmdSearchOwner.ForeColor = System.Drawing.Color.White;
            this.cmdSearchOwner.ImageSource = "icon - search";
            this.cmdSearchOwner.Location = new System.Drawing.Point(320, 59);
            this.cmdSearchOwner.Name = "cmdSearchOwner";
            this.cmdSearchOwner.Size = new System.Drawing.Size(40, 40);
            this.cmdSearchOwner.TabIndex = 6;
            this.cmdSearchOwner.Click += new System.EventHandler(this.cmdSearchOwner_Click);
            // 
            // cmdEditOwner
            // 
            this.cmdEditOwner.AppearanceKey = "actionButton";
            this.cmdEditOwner.ForeColor = System.Drawing.Color.White;
            this.cmdEditOwner.ImageSource = "icon - edit";
            this.cmdEditOwner.Location = new System.Drawing.Point(364, 59);
            this.cmdEditOwner.Name = "cmdEditOwner";
            this.cmdEditOwner.Size = new System.Drawing.Size(40, 40);
            this.cmdEditOwner.TabIndex = 7;
            this.cmdEditOwner.Click += new System.EventHandler(this.cmdEditOwner_Click);
            // 
            // txtOwnerID
            // 
            this.txtOwnerID.BackColor = System.Drawing.Color.FromArgb(255, 255, 192);
            this.txtOwnerID.Location = new System.Drawing.Point(176, 59);
            this.txtOwnerID.Name = "txtOwnerID";
            this.txtOwnerID.Size = new System.Drawing.Size(137, 40);
            this.txtOwnerID.TabIndex = 5;
            this.txtOwnerID.Tag = "address";
            this.txtOwnerID.Text = "0";
            this.txtOwnerID.Validating += new System.ComponentModel.CancelEventHandler(this.txtOwnerID_Validating);
            // 
            // cmdRemoveOwner
            // 
            this.cmdRemoveOwner.AppearanceKey = "actionButton";
            this.cmdRemoveOwner.ForeColor = System.Drawing.Color.White;
            this.cmdRemoveOwner.ImageSource = "icon-close-menu";
            this.cmdRemoveOwner.Location = new System.Drawing.Point(408, 59);
            this.cmdRemoveOwner.Name = "cmdRemoveOwner";
            this.cmdRemoveOwner.Size = new System.Drawing.Size(40, 40);
            this.cmdRemoveOwner.TabIndex = 8;
            this.cmdRemoveOwner.Click += new System.EventHandler(this.cmdRemoveOwner_Click);
            // 
            // txtTotalExempt
            // 
            this.txtTotalExempt.BackColor = System.Drawing.SystemColors.Window;
            this.txtTotalExempt.Location = new System.Drawing.Point(696, 239);
            this.txtTotalExempt.LockedOriginal = true;
            this.txtTotalExempt.MaxLength = 11;
            this.txtTotalExempt.Name = "txtTotalExempt";
            this.txtTotalExempt.ReadOnly = true;
            this.txtTotalExempt.Size = new System.Drawing.Size(254, 40);
            this.txtTotalExempt.TabIndex = 27;
            this.txtTotalExempt.TabStop = false;
            this.txtTotalExempt.Tag = "values";
            this.txtTotalExempt.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtTotWithBETE
            // 
            this.txtTotWithBETE.BackColor = System.Drawing.SystemColors.Window;
            this.txtTotWithBETE.Location = new System.Drawing.Point(696, 119);
            this.txtTotWithBETE.LockedOriginal = true;
            this.txtTotWithBETE.MaxLength = 11;
            this.txtTotWithBETE.Name = "txtTotWithBETE";
            this.txtTotWithBETE.ReadOnly = true;
            this.txtTotWithBETE.Size = new System.Drawing.Size(254, 40);
            this.txtTotWithBETE.TabIndex = 23;
            this.txtTotWithBETE.TabStop = false;
            this.txtTotWithBETE.Tag = "values";
            this.txtTotWithBETE.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtApt
            // 
            this.txtApt.BackColor = System.Drawing.SystemColors.Window;
            this.txtApt.Location = new System.Drawing.Point(239, 299);
            this.txtApt.MaxLength = 1;
            this.txtApt.Name = "txtApt";
            this.txtApt.Size = new System.Drawing.Size(40, 40);
            this.txtApt.TabIndex = 14;
            this.txtApt.Tag = "address";
            this.txtApt.TextChanged += new System.EventHandler(this.txtApt_TextChanged);
            // 
            // txtSquareFootage
            // 
            this.txtSquareFootage.BackColor = System.Drawing.SystemColors.Window;
            this.txtSquareFootage.Location = new System.Drawing.Point(696, 541);
            this.txtSquareFootage.Name = "txtSquareFootage";
            this.txtSquareFootage.Size = new System.Drawing.Size(254, 40);
            this.txtSquareFootage.TabIndex = 38;
            this.txtSquareFootage.Tag = "values";
            this.txtSquareFootage.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtSquareFootage.TextChanged += new System.EventHandler(this.txtSquareFootage_TextChanged);
            this.txtSquareFootage.Validating += new System.ComponentModel.CancelEventHandler(this.txtSquareFootage_Validating);
            // 
            // cmdMaster
            // 
            this.cmdMaster.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdMaster.Enabled = false;
            this.cmdMaster.Location = new System.Drawing.Point(754, 29);
            this.cmdMaster.Name = "cmdMaster";
            this.cmdMaster.Size = new System.Drawing.Size(104, 23);
            this.cmdMaster.TabIndex = 8;
            this.cmdMaster.Text = "Master Screen";
            // 
            // cmdItemized
            // 
            this.cmdItemized.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdItemized.Location = new System.Drawing.Point(864, 29);
            this.cmdItemized.Name = "cmdItemized";
            this.cmdItemized.Size = new System.Drawing.Size(116, 23);
            this.cmdItemized.TabIndex = 1;
            this.cmdItemized.Text = "Itemized Screen";
            this.cmdItemized.Click += new System.EventHandler(this.cmdItemized_Click);
            // 
            // cmdLeased
            // 
            this.cmdLeased.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdLeased.Location = new System.Drawing.Point(986, 29);
            this.cmdLeased.Name = "cmdLeased";
            this.cmdLeased.Size = new System.Drawing.Size(108, 23);
            this.cmdLeased.TabIndex = 2;
            this.cmdLeased.Text = "Leased Screen";
            this.cmdLeased.Click += new System.EventHandler(this.cmdLeased_Click);
            // 
            // txtOpen2
            // 
            this.txtOpen2.BackColor = System.Drawing.SystemColors.Window;
            this.txtOpen2.Location = new System.Drawing.Point(176, 419);
            this.txtOpen2.MaxLength = 34;
            this.txtOpen2.Name = "txtOpen2";
            this.txtOpen2.Size = new System.Drawing.Size(318, 40);
            this.txtOpen2.TabIndex = 19;
            this.txtOpen2.Tag = "address";
            this.txtOpen2.Enter += new System.EventHandler(this.txtOpen2_Enter);
            this.txtOpen2.TextChanged += new System.EventHandler(this.txtOpen2_TextChanged);
            // 
            // txtOpen1
            // 
            this.txtOpen1.BackColor = System.Drawing.SystemColors.Window;
            this.txtOpen1.Location = new System.Drawing.Point(176, 359);
            this.txtOpen1.MaxLength = 34;
            this.txtOpen1.Name = "txtOpen1";
            this.txtOpen1.Size = new System.Drawing.Size(318, 40);
            this.txtOpen1.TabIndex = 17;
            this.txtOpen1.Tag = "address";
            this.txtOpen1.Enter += new System.EventHandler(this.txtOpen1_Enter);
            this.txtOpen1.TextChanged += new System.EventHandler(this.txtOpen1_TextChanged);
            // 
            // txtStreetCode
            // 
            this.txtStreetCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtStreetCode.Location = new System.Drawing.Point(696, 481);
            this.txtStreetCode.Name = "txtStreetCode";
            this.txtStreetCode.Size = new System.Drawing.Size(254, 40);
            this.txtStreetCode.TabIndex = 36;
            this.txtStreetCode.Tag = "values";
            this.txtStreetCode.Enter += new System.EventHandler(this.txtStreetCode_Enter);
            this.txtStreetCode.TextChanged += new System.EventHandler(this.txtStreetCode_TextChanged);
            this.txtStreetCode.KeyDown += new Wisej.Web.KeyEventHandler(this.txtStreetCode_KeyDown);
            // 
            // txtBusinessCode
            // 
            this.txtBusinessCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtBusinessCode.Location = new System.Drawing.Point(696, 421);
            this.txtBusinessCode.Name = "txtBusinessCode";
            this.txtBusinessCode.Size = new System.Drawing.Size(254, 40);
            this.txtBusinessCode.TabIndex = 34;
            this.txtBusinessCode.Tag = "values";
            this.txtBusinessCode.Enter += new System.EventHandler(this.txtBusinessCode_Enter);
            this.txtBusinessCode.TextChanged += new System.EventHandler(this.txtBusinessCode_TextChanged);
            this.txtBusinessCode.KeyDown += new Wisej.Web.KeyEventHandler(this.txtBusinessCode_KeyDown);
            // 
            // txtExemptCode1
            // 
            this.txtExemptCode1.BackColor = System.Drawing.SystemColors.Window;
            this.txtExemptCode1.Location = new System.Drawing.Point(696, 299);
            this.txtExemptCode1.MaxLength = 2;
            this.txtExemptCode1.Name = "txtExemptCode1";
            this.txtExemptCode1.Size = new System.Drawing.Size(122, 40);
            this.txtExemptCode1.TabIndex = 29;
            this.txtExemptCode1.Tag = "values";
            this.txtExemptCode1.Enter += new System.EventHandler(this.txtExemptCode1_Enter);
            this.txtExemptCode1.TextChanged += new System.EventHandler(this.txtExemptCode1_TextChanged);
            this.txtExemptCode1.KeyDown += new Wisej.Web.KeyEventHandler(this.txtExemptCode1_KeyDown);
            // 
            // txtExemption
            // 
            this.txtExemption.BackColor = System.Drawing.SystemColors.Window;
            this.txtExemption.Location = new System.Drawing.Point(696, 179);
            this.txtExemption.LockedOriginal = true;
            this.txtExemption.MaxLength = 11;
            this.txtExemption.Name = "txtExemption";
            this.txtExemption.ReadOnly = true;
            this.txtExemption.Size = new System.Drawing.Size(254, 40);
            this.txtExemption.TabIndex = 25;
            this.txtExemption.TabStop = false;
            this.txtExemption.Tag = "values";
            this.txtExemption.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtExemption.TextChanged += new System.EventHandler(this.txtExemption_TextChanged);
            // 
            // txtValue
            // 
            this.txtValue.BackColor = System.Drawing.SystemColors.Window;
            this.txtValue.Location = new System.Drawing.Point(696, 59);
            this.txtValue.LockedOriginal = true;
            this.txtValue.MaxLength = 11;
            this.txtValue.Name = "txtValue";
            this.txtValue.ReadOnly = true;
            this.txtValue.Size = new System.Drawing.Size(254, 40);
            this.txtValue.TabIndex = 21;
            this.txtValue.TabStop = false;
            this.txtValue.Tag = "values";
            this.txtValue.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtValue.TextChanged += new System.EventHandler(this.txtValue_TextChanged);
            // 
            // txtStreetNumber
            // 
            this.txtStreetNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtStreetNumber.Location = new System.Drawing.Point(176, 299);
            this.txtStreetNumber.MaxLength = 5;
            this.txtStreetNumber.Name = "txtStreetNumber";
            this.txtStreetNumber.Size = new System.Drawing.Size(55, 40);
            this.txtStreetNumber.TabIndex = 13;
            this.txtStreetNumber.Tag = "address";
            this.txtStreetNumber.Enter += new System.EventHandler(this.txtStreetNumber_Enter);
            this.txtStreetNumber.TextChanged += new System.EventHandler(this.txtStreetNumber_TextChanged);
            // 
            // txtStreetName
            // 
            this.txtStreetName.BackColor = System.Drawing.SystemColors.Window;
            this.txtStreetName.Location = new System.Drawing.Point(287, 299);
            this.txtStreetName.MaxLength = 26;
            this.txtStreetName.Name = "txtStreetName";
            this.txtStreetName.Size = new System.Drawing.Size(208, 40);
            this.txtStreetName.TabIndex = 15;
            this.txtStreetName.Tag = "address";
            this.txtStreetName.Enter += new System.EventHandler(this.txtStreetName_Enter);
            this.txtStreetName.TextChanged += new System.EventHandler(this.txtStreetName_TextChanged);
            // 
            // txtExemptCode2
            // 
            this.txtExemptCode2.BackColor = System.Drawing.SystemColors.Window;
            this.txtExemptCode2.Location = new System.Drawing.Point(828, 299);
            this.txtExemptCode2.MaxLength = 2;
            this.txtExemptCode2.Name = "txtExemptCode2";
            this.txtExemptCode2.Size = new System.Drawing.Size(122, 40);
            this.txtExemptCode2.TabIndex = 30;
            this.txtExemptCode2.Tag = "values";
            this.txtExemptCode2.Enter += new System.EventHandler(this.txtExemptCode2_Enter);
            this.txtExemptCode2.TextChanged += new System.EventHandler(this.txtExemptCode2_TextChanged);
            this.txtExemptCode2.KeyDown += new Wisej.Web.KeyEventHandler(this.txtExemptCode2_KeyDown);
            // 
            // gridTranCode
            // 
            this.gridTranCode.Cols = 1;
            this.gridTranCode.ColumnHeadersVisible = false;
            this.gridTranCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.gridTranCode.ExtendLastCol = true;
            this.gridTranCode.FixedCols = 0;
            this.gridTranCode.FixedRows = 0;
            this.gridTranCode.Location = new System.Drawing.Point(696, 359);
            this.gridTranCode.Name = "gridTranCode";
            this.gridTranCode.ReadOnly = false;
            this.gridTranCode.RowHeadersVisible = false;
            this.gridTranCode.Rows = 1;
            this.gridTranCode.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollNone;
            this.gridTranCode.ShowFocusCell = false;
            this.gridTranCode.Size = new System.Drawing.Size(254, 43);
            this.gridTranCode.TabIndex = 32;
            this.gridTranCode.Tag = "values";
            this.gridTranCode.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.gridTranCode_CellChanged);
            // 
            // Label2_9
            // 
            this.Label2_9.Location = new System.Drawing.Point(30, 193);
            this.Label2_9.Name = "Label2_9";
            this.Label2_9.Size = new System.Drawing.Size(80, 17);
            this.Label2_9.TabIndex = 9;
            this.Label2_9.Text = "ADDRESS";
            // 
            // Label14
            // 
            this.Label14.Location = new System.Drawing.Point(30, 73);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(80, 15);
            this.Label14.TabIndex = 4;
            this.Label14.Text = "OWNER";
            // 
            // lblOwner1
            // 
            this.lblOwner1.Location = new System.Drawing.Point(176, 133);
            this.lblOwner1.Name = "lblOwner1";
            this.lblOwner1.Size = new System.Drawing.Size(318, 17);
            this.lblOwner1.TabIndex = 10;
            // 
            // lblAddress
            // 
            this.lblAddress.Location = new System.Drawing.Point(176, 192);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(275, 87);
            this.lblAddress.TabIndex = 11;
            // 
            // Label2_0
            // 
            this.Label2_0.Location = new System.Drawing.Point(520, 253);
            this.Label2_0.Name = "Label2_0";
            this.Label2_0.Size = new System.Drawing.Size(95, 17);
            this.Label2_0.TabIndex = 26;
            this.Label2_0.Text = "TOTAL EXEMPT";
            // 
            // Label2_1
            // 
            this.Label2_1.Location = new System.Drawing.Point(520, 133);
            this.Label2_1.Name = "Label2_1";
            this.Label2_1.Size = new System.Drawing.Size(66, 17);
            this.Label2_1.TabIndex = 22;
            this.Label2_1.Text = "WITH BETE";
            // 
            // lblComment
            // 
            this.lblComment.ForeColor = System.Drawing.Color.FromArgb(255, 0, 0);
            this.lblComment.Location = new System.Drawing.Point(766, 16);
            this.lblComment.Name = "lblComment";
            this.lblComment.Size = new System.Drawing.Size(17, 18);
            this.lblComment.TabIndex = 30;
            this.lblComment.Text = "C";
            this.lblComment.Visible = false;
            // 
            // lblstatistics_2
            // 
            this.lblstatistics_2.AutoSize = true;
            this.lblstatistics_2.Location = new System.Drawing.Point(520, 555);
            this.lblstatistics_2.Name = "lblstatistics_2";
            this.lblstatistics_2.Size = new System.Drawing.Size(126, 15);
            this.lblstatistics_2.TabIndex = 37;
            this.lblstatistics_2.Text = "SQUARE FOOTAGE";
            // 
            // txtREAssociate
            // 
            this.txtREAssociate.Location = new System.Drawing.Point(179, 30);
            this.txtREAssociate.Name = "txtREAssociate";
            this.txtREAssociate.Size = new System.Drawing.Size(66, 19);
            this.txtREAssociate.TabIndex = 3;
            // 
            // lblAccount
            // 
            this.lblAccount.Font = new System.Drawing.Font("@header", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblAccount.ForeColor = System.Drawing.Color.FromName("@windowText");
            this.lblAccount.Location = new System.Drawing.Point(135, 26);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(132, 35);
            this.lblAccount.TabIndex = 1;
            // 
            // lblStatics_14
            // 
            this.lblStatics_14.AutoSize = true;
            this.lblStatics_14.Location = new System.Drawing.Point(520, 495);
            this.lblStatics_14.Name = "lblStatics_14";
            this.lblStatics_14.Size = new System.Drawing.Size(96, 15);
            this.lblStatics_14.TabIndex = 35;
            this.lblStatics_14.Text = "STREET CODE";
            // 
            // lblStatics_15
            // 
            this.lblStatics_15.AutoSize = true;
            this.lblStatics_15.Location = new System.Drawing.Point(520, 435);
            this.lblStatics_15.Name = "lblStatics_15";
            this.lblStatics_15.Size = new System.Drawing.Size(111, 15);
            this.lblStatics_15.TabIndex = 33;
            this.lblStatics_15.Text = "BUSINESS CODE";
            // 
            // lblStatics_16
            // 
            this.lblStatics_16.AutoSize = true;
            this.lblStatics_16.Location = new System.Drawing.Point(520, 373);
            this.lblStatics_16.Name = "lblStatics_16";
            this.lblStatics_16.Size = new System.Drawing.Size(81, 15);
            this.lblStatics_16.TabIndex = 31;
            this.lblStatics_16.Text = "TRAN CODE";
            // 
            // lblStatics_17
            // 
            this.lblStatics_17.AutoSize = true;
            this.lblStatics_17.Location = new System.Drawing.Point(520, 313);
            this.lblStatics_17.Name = "lblStatics_17";
            this.lblStatics_17.Size = new System.Drawing.Size(107, 15);
            this.lblStatics_17.TabIndex = 28;
            this.lblStatics_17.Text = "EXEMPT CODES";
            // 
            // lblStatics_18
            // 
            this.lblStatics_18.AutoSize = true;
            this.lblStatics_18.Location = new System.Drawing.Point(520, 193);
            this.lblStatics_18.Name = "lblStatics_18";
            this.lblStatics_18.Size = new System.Drawing.Size(81, 15);
            this.lblStatics_18.TabIndex = 24;
            this.lblStatics_18.Text = "EXEMPTION";
            // 
            // lblStatics_19
            // 
            this.lblStatics_19.AutoSize = true;
            this.lblStatics_19.Location = new System.Drawing.Point(520, 73);
            this.lblStatics_19.Name = "lblStatics_19";
            this.lblStatics_19.Size = new System.Drawing.Size(47, 15);
            this.lblStatics_19.TabIndex = 20;
            this.lblStatics_19.Text = "VALUE";
            // 
            // lblStatics_0
            // 
            this.lblStatics_0.Location = new System.Drawing.Point(30, 30);
            this.lblStatics_0.Name = "lblStatics_0";
            this.lblStatics_0.Size = new System.Drawing.Size(103, 19);
            this.lblStatics_0.TabIndex = 2;
            this.lblStatics_0.Text = "RE ASSOCIATE";
            // 
            // lblOpen2
            // 
            this.lblOpen2.Location = new System.Drawing.Point(30, 433);
            this.lblOpen2.Name = "lblOpen2";
            this.lblOpen2.Size = new System.Drawing.Size(104, 19);
            this.lblOpen2.TabIndex = 18;
            this.lblOpen2.Text = "OPEN 2";
            // 
            // lblOpen1
            // 
            this.lblOpen1.Location = new System.Drawing.Point(30, 373);
            this.lblOpen1.Name = "lblOpen1";
            this.lblOpen1.Size = new System.Drawing.Size(100, 19);
            this.lblOpen1.TabIndex = 16;
            this.lblOpen1.Text = "OPEN 1";
            // 
            // lblStatics_20
            // 
            this.lblStatics_20.Location = new System.Drawing.Point(30, 313);
            this.lblStatics_20.Name = "lblStatics_20";
            this.lblStatics_20.Size = new System.Drawing.Size(80, 19);
            this.lblStatics_20.TabIndex = 12;
            this.lblStatics_20.Text = "LOCATION";
            // 
            // lblStatics_29
            // 
            this.lblStatics_29.Location = new System.Drawing.Point(937, 15);
            this.lblStatics_29.Name = "lblStatics_29";
            this.lblStatics_29.Size = new System.Drawing.Size(103, 19);
            this.lblStatics_29.TabIndex = 1001;
            this.lblStatics_29.Text = "ACCOUNT";
            this.lblStatics_29.Visible = false;
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddAccount,
            this.mnuDeleteAccount,
            this.mnuCalculatePrint,
            this.mnuGroup,
            this.mnuInterestedParties,
            this.mnuChangeAssociation,
            this.mnuPrintBETEApp,
            this.mnuPrintInput,
            this.mnuPrintOriginal,
            this.mnuPrintMasterInfo,
            this.mnuInventoryListing});
            this.MainMenu1.Name = "MainMenu1";
            // 
            // mnuAddAccount
            // 
            this.mnuAddAccount.Index = 0;
            this.mnuAddAccount.Name = "mnuAddAccount";
            this.mnuAddAccount.Text = "Add Account";
            this.mnuAddAccount.Visible = false;
            this.mnuAddAccount.Click += new System.EventHandler(this.mnuAddAccount_Click);
            // 
            // mnuDeleteAccount
            // 
            this.mnuDeleteAccount.Index = 1;
            this.mnuDeleteAccount.Name = "mnuDeleteAccount";
            this.mnuDeleteAccount.Text = "Delete Account";
            this.mnuDeleteAccount.Click += new System.EventHandler(this.mnuDeleteAccount_Click);
            // 
            // mnuCalculatePrint
            // 
            this.mnuCalculatePrint.Index = 2;
            this.mnuCalculatePrint.Name = "mnuCalculatePrint";
            this.mnuCalculatePrint.Text = "Calculate Account (Print)";
            this.mnuCalculatePrint.Click += new System.EventHandler(this.mnuCalculatePrint_Click);
            // 
            // mnuGroup
            // 
            this.mnuGroup.Index = 3;
            this.mnuGroup.Name = "mnuGroup";
            this.mnuGroup.Text = "View Group Information";
            this.mnuGroup.Click += new System.EventHandler(this.mnuGroup_Click);
            // 
            // mnuInterestedParties
            // 
            this.mnuInterestedParties.Index = 4;
            this.mnuInterestedParties.Name = "mnuInterestedParties";
            this.mnuInterestedParties.Text = "Interested Parties";
            this.mnuInterestedParties.Click += new System.EventHandler(this.mnuInterestedParties_Click);
            // 
            // mnuChangeAssociation
            // 
            this.mnuChangeAssociation.Index = 5;
            this.mnuChangeAssociation.Name = "mnuChangeAssociation";
            this.mnuChangeAssociation.Text = "Change Real Estate Association";
            this.mnuChangeAssociation.Click += new System.EventHandler(this.mnuChangeAssociation_Click);
            // 
            // mnuPrintBETEApp
            // 
            this.mnuPrintBETEApp.Index = 6;
            this.mnuPrintBETEApp.Name = "mnuPrintBETEApp";
            this.mnuPrintBETEApp.Text = "Print BETE Application";
            this.mnuPrintBETEApp.Click += new System.EventHandler(this.mnuPrintBETEApp_Click);
            // 
            // mnuPrintInput
            // 
            this.mnuPrintInput.Index = 7;
            this.mnuPrintInput.Name = "mnuPrintInput";
            this.mnuPrintInput.Text = "Print Input Report - Input Format";
            this.mnuPrintInput.Click += new System.EventHandler(this.mnuPrintInput_Click);
            // 
            // mnuPrintOriginal
            // 
            this.mnuPrintOriginal.Index = 8;
            this.mnuPrintOriginal.Name = "mnuPrintOriginal";
            this.mnuPrintOriginal.Text = "Print Input Report - Original Cost Format";
            this.mnuPrintOriginal.Click += new System.EventHandler(this.mnuPrintOriginal_Click);
            // 
            // mnuPrintMasterInfo
            // 
            this.mnuPrintMasterInfo.Index = 9;
            this.mnuPrintMasterInfo.Name = "mnuPrintMasterInfo";
            this.mnuPrintMasterInfo.Text = "Print Master Info";
            this.mnuPrintMasterInfo.Click += new System.EventHandler(this.mnuPrintMasterInfo_Click);
            // 
            // mnuInventoryListing
            // 
            this.mnuInventoryListing.Index = 10;
            this.mnuInventoryListing.Name = "mnuInventoryListing";
            this.mnuInventoryListing.Text = "Print Inventory Listing";
            // 
            // Separator3
            // 
            this.Separator3.Index = -1;
            this.Separator3.Name = "Separator3";
            this.Separator3.Text = "-";
            // 
            // Separator6
            // 
            this.Separator6.Index = -1;
            this.Separator6.Name = "Separator6";
            this.Separator6.Text = "-";
            // 
            // mnuPrintLabel
            // 
            this.mnuPrintLabel.Index = -1;
            this.mnuPrintLabel.Name = "mnuPrintLabel";
            this.mnuPrintLabel.Shortcut = Wisej.Web.Shortcut.F2;
            this.mnuPrintLabel.Text = "Print Label";
            this.mnuPrintLabel.Click += new System.EventHandler(this.mnuPrintLabel_Click);
            // 
            // mnuSearch
            // 
            this.mnuSearch.Index = -1;
            this.mnuSearch.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPrevious,
            this.mnuNext});
            this.mnuSearch.Name = "mnuSearch";
            this.mnuSearch.Text = "Search";
            // 
            // mnuPrevious
            // 
            this.mnuPrevious.Index = 0;
            this.mnuPrevious.Name = "mnuPrevious";
            this.mnuPrevious.Shortcut = Wisej.Web.Shortcut.F7;
            this.mnuPrevious.Text = "Previous Account from Search";
            this.mnuPrevious.Click += new System.EventHandler(this.mnuPrevious_Click);
            // 
            // mnuNext
            // 
            this.mnuNext.Index = 1;
            this.mnuNext.Name = "mnuNext";
            this.mnuNext.Shortcut = Wisej.Web.Shortcut.F8;
            this.mnuNext.Text = "Next Account from Search";
            this.mnuNext.Click += new System.EventHandler(this.mnuNext_Click);
            // 
            // mnuCalculateDisplay
            // 
            this.mnuCalculateDisplay.Index = -1;
            this.mnuCalculateDisplay.Name = "mnuCalculateDisplay";
            this.mnuCalculateDisplay.Shortcut = Wisej.Web.Shortcut.F6;
            this.mnuCalculateDisplay.Text = "Calculate Account (Display)";
            this.mnuCalculateDisplay.Click += new System.EventHandler(this.mnuCalculateDisplay_Click);
            // 
            // mnuComment
            // 
            this.mnuComment.Index = -1;
            this.mnuComment.Name = "mnuComment";
            this.mnuComment.Shortcut = Wisej.Web.Shortcut.F5;
            this.mnuComment.Text = "Comment";
            this.mnuComment.Click += new System.EventHandler(this.mnuComment_Click);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = -1;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = -1;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = -1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = -1;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = -1;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSaveExit
            // 
            this.cmdSaveExit.AppearanceKey = "acceptButton";
            this.cmdSaveExit.Location = new System.Drawing.Point(499, 30);
            this.cmdSaveExit.Name = "cmdSaveExit";
            this.cmdSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSaveExit.Size = new System.Drawing.Size(80, 48);
            this.cmdSaveExit.TabIndex = 3;
            this.cmdSaveExit.Text = "Save";
            this.cmdSaveExit.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // cmdNextAccount
            // 
            this.cmdNextAccount.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdNextAccount.Location = new System.Drawing.Point(648, 29);
            this.cmdNextAccount.Name = "cmdNextAccount";
            this.cmdNextAccount.Shortcut = Wisej.Web.Shortcut.F8;
            this.cmdNextAccount.Size = new System.Drawing.Size(100, 23);
            this.cmdNextAccount.TabIndex = 4;
            this.cmdNextAccount.Text = "Next Account";
            this.cmdNextAccount.Click += new System.EventHandler(this.mnuNext_Click);
            // 
            // cmdPreviousAccount
            // 
            this.cmdPreviousAccount.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPreviousAccount.Location = new System.Drawing.Point(518, 29);
            this.cmdPreviousAccount.Name = "cmdPreviousAccount";
            this.cmdPreviousAccount.Shortcut = Wisej.Web.Shortcut.F7;
            this.cmdPreviousAccount.Size = new System.Drawing.Size(124, 23);
            this.cmdPreviousAccount.TabIndex = 5;
            this.cmdPreviousAccount.Text = "Previous Account";
            this.cmdPreviousAccount.Click += new System.EventHandler(this.mnuPrevious_Click);
            // 
            // cmdComment
            // 
            this.cmdComment.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdComment.Location = new System.Drawing.Point(434, 29);
            this.cmdComment.Name = "cmdComment";
            this.cmdComment.Shortcut = Wisej.Web.Shortcut.F5;
            this.cmdComment.Size = new System.Drawing.Size(78, 23);
            this.cmdComment.TabIndex = 6;
            this.cmdComment.Text = "Comment";
            this.cmdComment.Click += new System.EventHandler(this.mnuComment_Click);
            // 
            // cmdPrintLabel
            // 
            this.cmdPrintLabel.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintLabel.Location = new System.Drawing.Point(346, 29);
            this.cmdPrintLabel.Name = "cmdPrintLabel";
            this.cmdPrintLabel.Shortcut = Wisej.Web.Shortcut.F2;
            this.cmdPrintLabel.Size = new System.Drawing.Size(82, 23);
            this.cmdPrintLabel.TabIndex = 7;
            this.cmdPrintLabel.Text = "Print Label";
            this.cmdPrintLabel.Click += new System.EventHandler(this.mnuPrintLabel_Click);
            // 
            // cmdCalculateDisplay
            // 
            this.cmdCalculateDisplay.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdCalculateDisplay.Location = new System.Drawing.Point(156, 29);
            this.cmdCalculateDisplay.Name = "cmdCalculateDisplay";
            this.cmdCalculateDisplay.Shortcut = Wisej.Web.Shortcut.F6;
            this.cmdCalculateDisplay.Size = new System.Drawing.Size(184, 23);
            this.cmdCalculateDisplay.TabIndex = 8;
            this.cmdCalculateDisplay.Text = "Calculate Account (Display)";
            this.cmdCalculateDisplay.Click += new System.EventHandler(this.mnuCalculateDisplay_Click);
            // 
            // frmPPMaster
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1078, 666);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmPPMaster";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Account Maintenance";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmPPMaster_Load);
            this.Activated += new System.EventHandler(this.frmPPMaster_Activated);
            this.Resize += new System.EventHandler(this.frmPPMaster_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPPMaster_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPPMaster_KeyPress);
            this.KeyUp += new Wisej.Web.KeyEventHandler(this.frmPPMaster_KeyUp);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearchOwner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEditOwner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRemoveOwner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMaster)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdItemized)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdLeased)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTranCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNextAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPreviousAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCalculateDisplay)).EndInit();
            this.ResumeLayout(false);

		}


        #endregion

        private System.ComponentModel.IContainer components;
		public FCButton cmdSaveExit;
		public FCButton cmdComment;
		public FCButton cmdPreviousAccount;
		public FCButton cmdNextAccount;
		public FCButton cmdPrintLabel;
		public FCButton cmdCalculateDisplay;
        public FCToolStripMenuItem mnuInventoryListing;
    }
}
