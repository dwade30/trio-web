﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmPPTop40Taxpayers.
	/// </summary>
	partial class frmPPTop40Taxpayers : BaseForm
	{
		public fecherFoundation.FCComboBox cmbAccount;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label2;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCLabel Label2_1;
		public fecherFoundation.FCLabel Label2_0;
		public fecherFoundation.FCButton cmdQuit;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCTextBox txtQuantity;
		public fecherFoundation.FCLabel Label1;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPPTop40Taxpayers));
			this.cmbAccount = new fecherFoundation.FCComboBox();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.Label2_1 = new fecherFoundation.FCLabel();
			this.Label2_0 = new fecherFoundation.FCLabel();
			this.cmdQuit = new fecherFoundation.FCButton();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.txtQuantity = new fecherFoundation.FCTextBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 385);
			this.BottomPanel.Size = new System.Drawing.Size(826, 17);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.cmdQuit);
			this.ClientArea.Controls.Add(this.cmdPrint);
			this.ClientArea.Controls.Add(this.txtQuantity);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(826, 325);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(826, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(315, 30);
			this.HeaderText.Text = "Highest Assessment Listing";
			// 
			// cmbAccount
			// 
			this.cmbAccount.AutoSize = false;
			this.cmbAccount.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAccount.FormattingEnabled = true;
			this.cmbAccount.Items.AddRange(new object[] {
				"Account",
				"Name"
			});
			this.cmbAccount.Location = new System.Drawing.Point(20, 30);
			this.cmbAccount.Name = "cmbAccount";
			this.cmbAccount.Size = new System.Drawing.Size(155, 40);
			this.cmbAccount.TabIndex = 1;
			this.cmbAccount.Text = "Account";
			this.cmbAccount.SelectedIndexChanged += new System.EventHandler(this.cmbAccount_SelectedIndexChanged);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.cmbAccount);
			this.Frame1.Controls.Add(this.Label2_1);
			this.Frame1.Controls.Add(this.Label2_0);
			this.Frame1.Location = new System.Drawing.Point(30, 90);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(736, 145);
			this.Frame1.TabIndex = 4;
			this.Frame1.Text = "Select The Print Order";
			// 
			// Label2_1
			// 
			this.Label2_1.Location = new System.Drawing.Point(20, 94);
			this.Label2_1.Name = "Label2_1";
			this.Label2_1.Size = new System.Drawing.Size(708, 37);
			this.Label2_1.TabIndex = 8;
			this.Label2_1.Text = "EACH TAXPAYER LISTED WILL HAVE AN AGGREGATE TOTAL VALUATION OF ALL ACCOUNTS WITH " + "THEIR EXACT NAME";
			this.Label2_1.Visible = false;
			// 
			// Label2_0
			// 
			this.Label2_0.Location = new System.Drawing.Point(20, 94);
			this.Label2_0.Name = "Label2_0";
			this.Label2_0.Size = new System.Drawing.Size(708, 37);
			this.Label2_0.TabIndex = 7;
			this.Label2_0.Text = "EACH TAXPAYER LISTED WILL HAVE ONLY THE VALUATION OF THAT PARTICULAR ACCOUNT";
			// 
			// cmdQuit
			// 
			this.cmdQuit.AppearanceKey = "actionButton";
			this.cmdQuit.Location = new System.Drawing.Point(150, 267);
			this.cmdQuit.Name = "cmdQuit";
			this.cmdQuit.Size = new System.Drawing.Size(90, 40);
			this.cmdQuit.TabIndex = 3;
			this.cmdQuit.Text = "Quit";
			this.cmdQuit.Visible = false;
			this.cmdQuit.Click += new System.EventHandler(this.cmdQuit_Click);
			// 
			// cmdPrint
			// 
			this.cmdPrint.AppearanceKey = "acceptButton";
			this.cmdPrint.Location = new System.Drawing.Point(30, 259);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPrint.Size = new System.Drawing.Size(90, 48);
			this.cmdPrint.TabIndex = 2;
			this.cmdPrint.Text = "Print";
			this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
			// 
			// txtQuantity
			// 
			this.txtQuantity.AutoSize = false;
			this.txtQuantity.BackColor = System.Drawing.SystemColors.Window;
			this.txtQuantity.LinkItem = null;
			this.txtQuantity.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtQuantity.LinkTopic = null;
			this.txtQuantity.Location = new System.Drawing.Point(477, 30);
			this.txtQuantity.Name = "txtQuantity";
			this.txtQuantity.Size = new System.Drawing.Size(76, 40);
			this.txtQuantity.TabIndex = 0;
			// 
			// Label1
			// 
			this.Label1.AutoSize = true;
			this.Label1.Location = new System.Drawing.Point(30, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(413, 15);
			this.Label1.TabIndex = 1;
			this.Label1.Text = "HOW MANY OF THE TOP VALUATIONS WOULD YOU LIKE TO PRINT?";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuPrint,
				this.mnuSepar,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuPrint
			// 
			this.mnuPrint.Index = 0;
			this.mnuPrint.Name = "mnuPrint";
			this.mnuPrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuPrint.Text = "Print";
			this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 1;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// frmPPTop40Taxpayers
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(826, 402);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmPPTop40Taxpayers";
			this.Text = "Highest Assessment Listing";
			this.Load += new System.EventHandler(this.frmPPTop40Taxpayers_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPPTop40Taxpayers_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPPTop40Taxpayers_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
