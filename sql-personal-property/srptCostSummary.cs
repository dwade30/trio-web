﻿//Fecher vbPorter - Version 1.0.0.32
using Wisej.Web;
using fecherFoundation;
using System;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for srptCostSummary.
	/// </summary>
	public partial class srptCostSummary : FCSectionReport
	{
		public srptCostSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ActiveReport1";
		}

		public static srptCostSummary InstancePtr
		{
			get
			{
				return (srptCostSummary)Sys.GetInstance(typeof(srptCostSummary));
			}
		}

		protected srptCostSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptCostSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngLine;
		const int CNSTCATEGORYCOLBETEEXEMPT = 7;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (lngLine > frmPPCalculations.InstancePtr.vsDisplay.Rows)
			{
				eArgs.EOF = true;
			}
			else if (lngLine == frmPPCalculations.InstancePtr.vsDisplay.Rows)
			{
				if (frmPPCalculations.InstancePtr.Check1.CheckState == CheckState.Checked)
				{
					eArgs.EOF = false;
				}
				else
				{
					eArgs.EOF = true;
				}
			}
			else
			{
				eArgs.EOF = false;
				while (lngLine < frmPPCalculations.InstancePtr.vsDisplay.Rows)
				{
					if (Conversion.Val(frmPPCalculations.InstancePtr.vsDisplay.TextMatrix(lngLine, 6)) == 0 && Conversion.Val(frmPPCalculations.InstancePtr.vsDisplay.TextMatrix(lngLine, CNSTCATEGORYCOLBETEEXEMPT)) == 0)
					{
						lngLine += 1;
					}
					else
					{
						break;
					}
				}
				if (lngLine == frmPPCalculations.InstancePtr.vsDisplay.Rows)
				{
					if (frmPPCalculations.InstancePtr.Check1.CheckState == CheckState.Checked)
					{
						eArgs.EOF = false;
					}
					else
					{
						eArgs.EOF = true;
					}
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lngLine = 1;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (lngLine <= frmPPCalculations.InstancePtr.vsDisplay.Rows - 1)
			{
				txtCategory.Text = Strings.Trim(frmPPCalculations.InstancePtr.vsDisplay.TextMatrix(lngLine, 0));
				txtLeased.Text = Strings.Format(frmPPCalculations.InstancePtr.vsDisplay.TextMatrix(lngLine, 4), "###,###,###,###");
				txtItemized.Text = Strings.Format(frmPPCalculations.InstancePtr.vsDisplay.TextMatrix(lngLine, 2), "###,###,###,###");
				txtRatio.Text = frmPPCalculations.InstancePtr.vsDisplay.TextMatrix(lngLine, 5);
				txtAssessment.Text = Strings.Format(frmPPCalculations.InstancePtr.vsDisplay.TextMatrix(lngLine, 6), "###,###,###,###,###");
				txtBETEExempt.Text = Strings.Format(frmPPCalculations.InstancePtr.vsDisplay.TextMatrix(lngLine, CNSTCATEGORYCOLBETEEXEMPT), "###,###,###,###,###");
			}
			else
			{
				txtCategory.Text = "Current Override Amount";
				txtLeased.Text = "";
				txtItemized.Text = "";
				txtRatio.Text = "";
				txtAssessment.Text = frmPPCalculations.InstancePtr.txtOverride;
				txtBETEExempt.Text = "";
			}
			lngLine += 1;
		}

		
	}
}
