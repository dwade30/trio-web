﻿namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptTrends.
	/// </summary>
	partial class rptTrends
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptTrends));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniname = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCode = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDescription = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDesc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.ColumnCount = 3;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtCode,
				this.txtAmount,
				this.txtDesc
			});
			this.Detail.Height = 0.2395833F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblTitle,
				this.lblMuniname,
				this.lblTime,
				this.lblDate,
				this.lblPage,
				this.lblCode,
				this.lblAmount,
				this.lblDescription,
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6
			});
			this.PageHeader.Height = 0.8020833F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.25F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 2.333333F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.lblTitle.Text = "Exemptions";
			this.lblTitle.Top = 0F;
			this.lblTitle.Width = 3F;
			// 
			// lblMuniname
			// 
			this.lblMuniname.Height = 0.1666667F;
			this.lblMuniname.HyperLink = null;
			this.lblMuniname.Left = 0F;
			this.lblMuniname.Name = "lblMuniname";
			this.lblMuniname.Style = "";
			this.lblMuniname.Text = null;
			this.lblMuniname.Top = 0F;
			this.lblMuniname.Width = 2.25F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1666667F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.1666667F;
			this.lblTime.Width = 2.25F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1666667F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 5.75F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "text-align: right";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1.666667F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1666667F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 5.75F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "text-align: right";
			this.lblPage.Text = null;
			this.lblPage.Top = 0.1666667F;
			this.lblPage.Width = 1.666667F;
			// 
			// lblCode
			// 
			this.lblCode.Height = 0.1770833F;
			this.lblCode.HyperLink = null;
			this.lblCode.Left = 0.05208333F;
			this.lblCode.Name = "lblCode";
			this.lblCode.Style = "font-weight: bold; text-align: right";
			this.lblCode.Text = "Code";
			this.lblCode.Top = 0.6145833F;
			this.lblCode.Width = 0.53125F;
			// 
			// lblAmount
			// 
			this.lblAmount.Height = 0.1770833F;
			this.lblAmount.HyperLink = null;
			this.lblAmount.Left = 0.6666667F;
			this.lblAmount.Name = "lblAmount";
			this.lblAmount.Style = "font-weight: bold; text-align: right";
			this.lblAmount.Text = "Amount";
			this.lblAmount.Top = 0.6145833F;
			this.lblAmount.Width = 0.6979167F;
			// 
			// lblDescription
			// 
			this.lblDescription.Height = 0.1770833F;
			this.lblDescription.HyperLink = null;
			this.lblDescription.Left = 1.416667F;
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Style = "font-weight: bold; text-align: left";
			this.lblDescription.Text = "Description";
			this.lblDescription.Top = 0.6145833F;
			this.lblDescription.Width = 0.8645833F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1770833F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 2.552083F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-weight: bold; text-align: right";
			this.Label1.Text = "Year";
			this.Label1.Top = 0.6145833F;
			this.Label1.Width = 0.53125F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1770833F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 3.166667F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold; text-align: right";
			this.Label2.Text = "Trend";
			this.Label2.Top = 0.6145833F;
			this.Label2.Width = 0.6979167F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1770833F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 3.916667F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold; text-align: left";
			this.Label3.Text = "Cap Rate";
			this.Label3.Top = 0.6145833F;
			this.Label3.Width = 0.8645833F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1770833F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 4.96875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-weight: bold; text-align: right";
			this.Label4.Text = "Year";
			this.Label4.Top = 0.6145833F;
			this.Label4.Width = 0.53125F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1770833F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 5.583333F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-weight: bold; text-align: right";
			this.Label5.Text = "Trend";
			this.Label5.Top = 0.6145833F;
			this.Label5.Width = 0.6979167F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1770833F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 6.333333F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-weight: bold; text-align: left";
			this.Label6.Text = "Cap Rate";
			this.Label6.Top = 0.6145833F;
			this.Label6.Width = 0.8645833F;
			// 
			// txtCode
			// 
			this.txtCode.Height = 0.1979167F;
			this.txtCode.Left = 0.02083333F;
			this.txtCode.Name = "txtCode";
			this.txtCode.Style = "text-align: right";
			this.txtCode.Text = "Field1";
			this.txtCode.Top = 0.03125F;
			this.txtCode.Width = 0.53125F;
			// 
			// txtAmount
			// 
			this.txtAmount.Height = 0.1979167F;
			this.txtAmount.Left = 0.6666667F;
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.Style = "text-align: right";
			this.txtAmount.Text = "Field2";
			this.txtAmount.Top = 0.03125F;
			this.txtAmount.Width = 0.6979167F;
			// 
			// txtDesc
			// 
			this.txtDesc.Height = 0.1979167F;
			this.txtDesc.Left = 1.416667F;
			this.txtDesc.Name = "txtDesc";
			this.txtDesc.Style = "text-align: left";
			this.txtDesc.Text = "Field2";
			this.txtDesc.Top = 0.03125F;
			this.txtDesc.Width = 0.6979167F;
			// 
			// rptTrends
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.447917F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDesc;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniname;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDescription;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
