﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmStatus.
	/// </summary>
	public partial class frmStatus : BaseForm
	{
		public frmStatus()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmStatus InstancePtr
		{
			get
			{
				return (frmStatus)Sys.GetInstance(typeof(frmStatus));
			}
		}

		protected frmStatus _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private void frmStatus_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						Close();
						break;
					}
			}
			//end switch
		}

		private void frmStatus_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmStatus properties;
			//frmStatus.ScaleWidth	= 5880;
			//frmStatus.ScaleHeight	= 3960;
			//frmStatus.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			FillGrid();
		}

		private void frmStatus_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void FillGrid()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			// vbPorter upgrade warning: dtDate As DateTime	OnWrite(short, DateTime)
			DateTime dtDate;
			DateTime dtTempDate;
			DateTime dtTempDate2;
			// vbPorter upgrade warning: dtValDate As DateTime	OnWrite(short, DateTime)
			DateTime dtValDate;
			// vbPorter upgrade warning: dtCalcDate As DateTime	OnWriteFCConvert.ToInt16(
			DateTime dtCalcDate;
			//FileSystemObject fso = new FileSystemObject();
			Grid.ColDataType(1, FCGrid.DataTypeSettings.flexDTBoolean);
			clsLoad.OpenRecordset("select * from status", "twpp0000.vb1");
			if (clsLoad.EndOfFile())
			{
				clsLoad.AddNew();
				clsLoad.Update();
			}
			Grid.Rows = 1;
			Grid.TextMatrix(0, 0, "Description");
			Grid.TextMatrix(0, 1, "Done");
			Grid.TextMatrix(0, 2, "Last Performed");
			if (modPPGN.Statics.boolShortScreenOnly)
			{
				// If IsDate(clsLoad.Fields("CalcExemptions")) Then
				// If clsLoad.Fields("Caclexemptions") = 0 Then
				// .AddItem "Calculated Exemptions" & vbTab & False & vbTab & "Unknown"
				// Else
				// dtTempDate = clsLoad.Fields("calcexemptions")
				// If DateDiff("n", dtDate, dtTempDate) >= 0 And (dtDate > 0) Then
				// if second date is >=
				// .AddItem "Calculated Exemptions" & vbTab & True & vbTab & Format(dtTempDate, "MM/dd/yyyy h:mm tt")
				// dtDate = dtTempDate
				// Else
				// first date is > or is 0
				// dtDate = 0
				// .AddItem "Calculated Exemptions" & vbTab & False & vbTab & Format(dtTempDate, "MM/dd/yyyy h:mm tt")
				// End If
				// End If
				// Else
				// .AddItem "Calculated Exemptions" & vbTab & False & vbTab & "Unknown"
				// End If
			}
			else
			{
				// full assessing
				dtDate = DateTime.FromOADate(0);
				if (Information.IsDate(clsLoad.Get_Fields("changeddata")))
				{
					//FC:FINAL:MSH - i.issue #1280: incorrect comparing between DateTime and int
					//if (FCConvert.ToInt32(clsLoad.Get_Fields("changeddata")) == 0)
					if (clsLoad.Get_Fields_DateTime("changeddata").ToOADate() == 0)
					{
						Grid.AddItem("Account(s) Data Changed" + "\t" + false + "\t" + "Unknown");
					}
					else
					{
						dtDate = (DateTime)clsLoad.Get_Fields_DateTime("changeddata");
						Grid.AddItem("Account(s) Data Changed" + "\t" + true + "\t" + Strings.Format(dtDate, "MM/dd/yyyy h:mm tt"));
					}
				}
				else
				{
					Grid.AddItem("Account(s) Data Changed" + "\t" + false + "\t" + "Unknown");
				}
				// changed values doesn't depend on changing data.
				dtValDate = DateTime.FromOADate(0);
				if (Information.IsDate(clsLoad.Get_Fields("changedvalues")))
				{
					//FC:FINAL:MSH - i.issue #1280: incorrect comparing between DateTime and int
					//if (FCConvert.ToInt32(clsLoad.Get_Fields("Changedvalues")) == 0)
					if (clsLoad.Get_Fields_DateTime("changedvalues").ToOADate() == 0)
					{
						Grid.AddItem("Account(s) Values Changed" + "\t" + false + "\t" + "Unknown");
					}
					else
					{
						dtTempDate = (DateTime)clsLoad.Get_Fields_DateTime("changedvalues");
						dtValDate = dtTempDate;
						// If IsDate(clsLoad.Fields("xferbilling")) Then
						// dtTempDate2 = clsLoad.Fields("xferbilling")
						// If dtTempDate2 = 0 Then
						// .AddItem "Account(s) Values Changed" & vbTab & True & vbTab & Format(dtTempDate, "MM/dd/yyyy h:mm tt")
						// Else
						// must check
						// If DateDiff("n", dtTempDate2, dtTempDate) >= 0 Then
						// .AddItem "Account(s) Values Changed" & vbTab & True & vbTab & Format(dtTempDate, "MM/dd/yyyy h:mm tt")
						// Else
						// .AddItem "Account(s) Values Changed" & vbTab & False & vbTab & Format(dtTempDate, "MM/dd/yyyy h:mm tt")
						// End If
						// End If
						// Else
						Grid.AddItem("Account(s) Values Changed" + "\t" + true + "\t" + Strings.Format(dtTempDate, "MM/dd/yyyy h:mm tt"));
						// End If
					}
				}
				else
				{
					Grid.AddItem("Account(s) Values Changed" + "\t" + false + "\t" + "Unknown");
				}
				dtCalcDate = DateTime.FromOADate(0);
				if (Information.IsDate(clsLoad.Get_Fields("Calculated")))
				{
					//FC:FINAL:MSH - i.issue #1280: incorrect comparing between DateTime and int
					//if (FCConvert.ToInt32(clsLoad.Get_Fields("Calculated")) == 0)
					if (clsLoad.Get_Fields_DateTime("Calculated").ToOADate() == 0)
					{
						Grid.AddItem("Account(s) Calculated" + "\t" + false + "\t" + "Unknown");
					}
					else
					{
						dtCalcDate = (DateTime)clsLoad.Get_Fields_DateTime("Calculated");
						if (DateAndTime.DateDiff("n", dtDate, dtCalcDate) >= 0 && (dtDate.ToOADate() > 0))
						{
							Grid.AddItem("Account(s) Calculated" + "\t" + true + "\t" + Strings.Format(dtCalcDate, "MM/dd/yyyy h:mm tt"));
						}
						else
						{
							Grid.AddItem("Account(s) Calculated" + "\t" + false + "\t" + Strings.Format(dtCalcDate, "MM/dd/yyyy h:mm tt"));
						}
					}
				}
				else
				{
					Grid.AddItem("Account(s) Calculated" + "\t" + false + "\t" + "Unknown");
				}
				if (Information.IsDate(clsLoad.Get_Fields("batchcalculated")))
				{
					//FC:FINAL:MSH - i.issue #1280: incorrect comparing between DateTime and int
					//if (FCConvert.ToInt32(clsLoad.Get_Fields("batchcalculated")) == 0)
					if (clsLoad.Get_Fields_DateTime("batchcalculated").ToOADate() == 0)
					{
						dtDate = DateTime.FromOADate(0);
						Grid.AddItem("Batch Calculation" + "\t" + false + "\t" + "Unknown");
					}
					else
					{
						dtTempDate = (DateTime)clsLoad.Get_Fields_DateTime("batchcalculated");
						if (DateAndTime.DateDiff("n", dtDate, dtTempDate) >= 0 && (dtDate.ToOADate() > 0))
						{
							dtDate = dtTempDate;
							Grid.AddItem("Batch Calculation" + "\t" + true + "\t" + Strings.Format(dtTempDate, "MM/dd/yyyy h:mm tt"));
						}
						else
						{
							dtDate = DateTime.FromOADate(0);
							Grid.AddItem("Batch Calculation" + "\t" + false + "\t" + Strings.Format(dtTempDate, "MM/dd/yyyy h:mm tt"));
						}
					}
				}
				else
				{
					dtDate = DateTime.FromOADate(0);
					Grid.AddItem("Batch Calculation" + "\t" + false + "\t" + "Unknown");
				}
				if (Information.IsDate(clsLoad.Get_Fields("xferbilling")))
				{
					//FC:FINAL:MSH - i.issue #1280: incorrect comparing between DateTime and int
					//if (FCConvert.ToInt32(clsLoad.Get_Fields("xferbilling")) == 0)
					if (clsLoad.Get_Fields_DateTime("xferbilling").ToOADate() == 0)
					{
						dtDate = DateTime.FromOADate(0);
						Grid.AddItem("Transfer to Billing Values" + "\t" + false + "\t" + "Unknown");
					}
					else
					{
						dtTempDate = (DateTime)clsLoad.Get_Fields_DateTime("xferbilling");
						if (DateAndTime.DateDiff("n", dtDate, dtTempDate) >= 0 && (dtDate.ToOADate() > 0) && (DateAndTime.DateDiff("n", dtCalcDate, dtTempDate) >= 0))
						{
							dtDate = dtTempDate;
							Grid.AddItem("Transfer to Billing Values" + "\t" + true + "\t" + Strings.Format(dtTempDate, "MM/dd/yyyy h:mm tt"));
						}
						else
						{
							dtDate = DateTime.FromOADate(0);
							Grid.AddItem("Transfer to Billing Values" + "\t" + false + "\t" + Strings.Format(dtTempDate, "MM/dd/yyyy h:mm tt"));
						}
					}
				}
				else
				{
					dtDate = DateTime.FromOADate(0);
					Grid.AddItem("Transfer to Billing Values" + "\t" + false + "\t" + "Unknown");
				}
				if (Information.IsDate(clsLoad.Get_Fields("calcexemptions")))
				{
					//FC:FINAL:MSH - i.issue #1280: incorrect comparing between DateTime and int
					//if (FCConvert.ToInt32(clsLoad.Get_Fields("calcexemptions")) == 0)
					if (clsLoad.Get_Fields_DateTime("calcexemptions").ToOADate() == 0)
					{
						// dtDate = 0
						Grid.AddItem("Calculated Exemptions" + "\t" + false + "\t" + "Unknown");
					}
					else
					{
						dtTempDate = (DateTime)clsLoad.Get_Fields_DateTime("CalcExemptions");
						if (DateAndTime.DateDiff("n", dtDate, dtTempDate) >= 0 && (dtDate.ToOADate() > 0))
						{
							dtDate = dtTempDate;
							Grid.AddItem("Calculated Exemptions" + "\t" + true + "\t" + Strings.Format(dtTempDate, "MM/dd/yyyy h:mm tt"));
						}
						else
						{
							// dtDate = 0
							Grid.AddItem("Calculated Exemptions" + "\t" + false + "\t" + Strings.Format(dtTempDate, "MM/dd/yyyy h:mm tt"));
						}
					}
				}
				else
				{
					// dtDate = 0
					Grid.AddItem("Calculated Exemptions" + "\t" + false + "\t" + "Unknown");
				}
				// since most don't use exemptions, we won't require it to consider everything complete
				if (modGlobalConstants.Statics.gboolBL)
				{
					if (File.Exists("twcl0000.vb1"))
					{
						clsLoad.OpenRecordset("select transferfrombillingdateFIRST from billingmaster where BILLINGTYPE = 'PP' order by TRANSFERFROMbillingdatefirst desc", "twcl0000.vb1");
						if (!clsLoad.EndOfFile())
						{
							if (Information.IsDate(clsLoad.Get_Fields("transferfrombillingdatefirst")))
							{
								//FC:FINAL:MSH - i.issue #1280: incorrect comparing between DateTime and int
								//if (FCConvert.ToInt32(clsLoad.Get_Fields("transferfrombillingdatefirst")) == 0)
								if (clsLoad.Get_Fields_DateTime("transferfrombillingdatefirst").ToOADate() == 0)
								{
									Grid.AddItem("Transferred to Billing Records" + "\t" + false + "\t" + "Unknown");
								}
								else
								{
									dtTempDate = (DateTime)clsLoad.Get_Fields_DateTime("transferfrombillingdatefirst");
									clsLoad.OpenRecordset("select transferfrombillingdatelast from billingmaster where billingtype = 'PP' order by transferfrombillingdatelast desc", "twcl0000.vb1");
									if (!clsLoad.EndOfFile())
									{
										if (!fecherFoundation.FCUtils.IsEmptyDateTime(clsLoad.Get_Fields_DateTime("transferfrombillingdatelast")))
										{
											//FC:FINAL:MSH - i.issue #1280: incorrect comparing between DateTime and int
											//if (Information.IsDate(clsLoad.Get_Fields("transferfrombillingdatelast")) && FCConvert.ToInt32(clsLoad.Get_Fields("transferfrombillingdatelast")) != 0)
											if (Information.IsDate(clsLoad.Get_Fields("transferfrombillingdatelast")) && clsLoad.Get_Fields_DateTime("transferfrombillingdatelast").ToOADate() != 0)
											{
												// see which is greater
												dtTempDate2 = (DateTime)clsLoad.Get_Fields_DateTime("transferfrombillingdatelast");
												if (DateAndTime.DateDiff("n", dtTempDate, dtTempDate2) > 0)
													dtTempDate = dtTempDate2;
											}
										}
									}
									// if values were changed or calculations were done since then false
									if (DateAndTime.DateDiff("n", dtDate, dtTempDate) >= 0 && (dtDate.ToOADate() > 0) && (DateAndTime.DateDiff("n", dtValDate, dtTempDate) >= 0))
									{
										Grid.AddItem("Transferred to Billing Records" + "\t" + true + "\t" + Strings.Format(dtTempDate, "MM/dd/yyyy h:mm tt"));
									}
									else
									{
										Grid.AddItem("Transferred to Billing Records" + "\t" + false + "\t" + Strings.Format(dtTempDate, "MM/dd/yyyy h:mm tt"));
									}
								}
							}
							else
							{
								Grid.AddItem("Transferred to Billing Records" + "\t" + false + "\t" + "Unknown");
							}
						}
						else
						{
							Grid.AddItem("Transferred to Billing Records" + "\t" + false + "\t" + "Unknown");
						}
					}
				}
			}
			ResizeGrid();
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(0, FCConvert.ToInt32(0.56 * GridWidth));
			Grid.ColWidth(1, FCConvert.ToInt32(0.1 * GridWidth));
			//FC:FINAL:RPU:#1376 - The height of Grid was set in designer 
			//Grid.HeightOriginal = Grid.RowHeight(0) * Grid.Rows + 50;
		}
	}
}
