﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Core;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptNameLoc.
	/// </summary>
	public partial class rptNameLoc : BaseSectionReport
	{
		private clsDRWrapper rsLoad = new clsDRWrapper();
		public rptNameLoc()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Name Location Report";
		}

        public void Init(string strsql)
        {
            rsLoad.OpenRecordset(strsql, "PersonalProperty");
            if (rsLoad.EndOfFile())
            {
                MessageBox.Show("No records found");
                this.Unload();
                return;
            }
            frmReportViewer.InstancePtr.Init(this,boolAllowEmail: true,strAttachmentName: "NameLocation");
        }

		public static rptNameLoc InstancePtr
		{
			get
			{
				return (rptNameLoc)Sys.GetInstance(typeof(rptNameLoc));
			}
		}

		protected rptNameLoc _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}

            if (disposing)
            {
				rsLoad?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptNameLoc	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private int intPage;
		private int intAccount;
		private bool boolPrint;
		private string strName = "";
		private string strLocation = "";

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			Fields.Add("AccountNumber");
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			ShowRecord:
			;
			eArgs.EOF = rsLoad.EndOfFile();
			if (eArgs.EOF)
				return;
			if (FCConvert.ToInt32(rsLoad.GetData("masterAccount")) == intAccount)
			{
				if (!rsLoad.EndOfFile())
				{
					rsLoad.MoveNext();
				}
				goto ShowRecord;
			}
			txtName.Text = rsLoad.GetData("Name").ToString();
			txtLocation.Text = rsLoad.GetData("StreetNumber") + rsLoad.Get_Fields_String("streetapt") + " " + rsLoad.GetData("Street");
			txtAccount.Text = rsLoad.GetData("masterAccount").ToString();
			intAccount = FCConvert.ToInt32(rsLoad.GetData("masterAccount"));
			if (!rsLoad.EndOfFile())
				rsLoad.MoveNext();
			eArgs.EOF = false;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//this.Printer.RenderMode = 1;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTitle.Text = "Name / Location";
			intPage += 1;
			txtPage.Text = "Page: " + FCConvert.ToString(intPage);
		}

		
	}
}
