﻿//Fecher vbPorter - Version 1.0.0.32
using Global;
using Wisej.Web;
using Wisej.Core;
using System;
using fecherFoundation;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmGetRangeForm.
	/// </summary>
	public partial class frmGetRangeForm : BaseForm
	{
		public frmGetRangeForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			modGlobal.Statics.Resp1 = "cancel";
			this.Close();
		}

		public void cmdCancel_Click()
		{
			//cmdCancel_Click(cmdCancel, new System.EventArgs());
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			modGlobal.Statics.Resp1 = txtStart.Text;
			modGlobal.Statics.Resp2 = txtStop.Text;
			this.Close();
		}

		public void cmdOK_Click()
		{
			cmdOK_Click(cmdOK, new System.EventArgs());
		}

		private void frmGetRangeForm_Activated(object sender, System.EventArgs e)
		{
			if (modGNWork.FormExist(this))
				return;
		}

		private void frmGetRangeForm_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmGetRangeForm_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGetRangeForm properties;
			//frmGetRangeForm.ScaleWidth	= 5310;
			//frmGetRangeForm.ScaleHeight	= 4635;
			//frmGetRangeForm.LinkTopic	= "Form1";
			//frmGetRangeForm.LockControls	= true;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void mnuContinue_Click(object sender, System.EventArgs e)
		{
			cmdOK_Click();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdCancel_Click();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}
	}
}
