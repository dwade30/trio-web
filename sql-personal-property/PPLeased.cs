﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;
using System.IO;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmPPLeased.
	/// </summary>
	public partial class frmPPLeased : BaseForm
	{
		public frmPPLeased()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.lblStatics = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.lblStatics.AddControlArrayElement(lblStatics_27, 27);
			this.lblStatics.AddControlArrayElement(lblStatics_29, 29);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPPLeased InstancePtr
		{
			get
			{
				return (frmPPLeased)Sys.GetInstance(typeof(frmPPLeased));
			}
		}

		protected frmPPLeased _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		/// <summary>
		/// Used when user Deletes row
		/// </summary>
		bool boolKickedOut;
		/// <summary>
		/// Dim DeletedRows(1000)        As Long
		/// </summary>
		int lngDeletes;
		/// <summary>
		/// Used when user updates row
		/// </summary>
		int UpdatedRows;
		/// <summary>
		/// Dim UpdateLeased(1000)       As Long
		/// </summary>
		/// <summary>
		/// Used when user adds new row
		/// </summary>
		int AddedRows;
		/// <summary>
		/// Dim NewLeased(1000)          As Long
		/// </summary>
		/// <summary>
		///
		/// </summary>
		// vbPorter upgrade warning: LeasedAccountTotal As int	OnWrite(short, double)
		int LeasedAccountTotal;
		bool Formactivating;
		bool Calculating;
		clsDRWrapper ppl = new clsDRWrapper();
		clsDRWrapper ppm = new clsDRWrapper();
		string strSQL;
		int CurrentRow;
		int CurrentCol;
		bool boolDataChanged;
		int colLN;
		int colKY;
		int colRB;
		int colCD;
		int colQT;
		int colDS;
		int colMO;
		int colCS;
		int colLD;
		int colMS;
		int colLP;
		int colMR;
		int colDY;
		int colFC;
		int colVL;
		int colLF;
		int colYFA;
		int colreplacingvalue;
		int colBETE;
		int colYearsClaimed;
		private bool boolDontUnloadOthers;

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			framTransfer.Visible = false;
		}

		private void cmdItemized_Click(object sender, System.EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			// Call SaveWindowSize(Me)
			modPPGN.Statics.Exiting = true;
			//FC:FINAL:MSH - i.issue #1276: hide another forms and dispose the form before openning for correct focusing
			//FC:FINAL:DDU:#i1270 - close forms instead of comment their hide method
			frmPPLeased.InstancePtr.Close();
			frmPPMaster.InstancePtr.Close();
			frmPPItemized.InstancePtr.Unload();
			frmPPItemized.InstancePtr.Dispose();
			frmPPItemized.InstancePtr.Show(App.MainForm);
			// Load frmPPItemized
		}

		private void cmdMaster_Click(object sender, System.EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			// Call SaveWindowSize(Me)
			//FC:FINAL:MSH - i.issue #1276: hide another forms and dispose the form before openning for correct focusing
			//FC:FINAL:DDU:#i1270 - close forms instead of comment their hide method
			frmPPLeased.InstancePtr.Close();
			frmPPItemized.InstancePtr.Close();
			frmPPMaster.InstancePtr.Unload();
			frmPPMaster.InstancePtr.Dispose();
			frmPPMaster.InstancePtr.Show(App.MainForm);
			// Unload frmPPLeased
		}

		public void cmdMaster_Click()
		{
			cmdMaster_Click(cmdMaster, new System.EventArgs());
		}

		private void cmdOk_Click(object sender, System.EventArgs e)
		{
			int lngStart = 0;
			int lngEnd = 0;
			int x;
			clsDRWrapper clsSave = new clsDRWrapper();
			int lngMaxLine;
			int lngCurLine;
			if (vs1.Row < 1)
			{
				MessageBox.Show("You must choose at least one row of data to transfer", "No Data Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (cmbTransferTo.SelectedIndex < 0)
			{
				MessageBox.Show("You must choose an account to transfer data to", "No Account Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (cmbTransferTo.ItemData(cmbTransferTo.SelectedIndex) <= 0)
			{
				MessageBox.Show("You must choose an account to transfer data to", "No Account Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (vs1.Row > vs1.RowSel)
			{
				lngStart = vs1.RowSel;
				lngEnd = vs1.Row;
			}
			else
			{
				lngStart = vs1.Row;
				lngEnd = vs1.RowSel;
			}
			lngMaxLine = 0;
			clsSave.Execute("update status set changeddata = '" + FCConvert.ToString(DateTime.Now) + "'", "twpp0000.vb1");
			clsSave.OpenRecordset("select * from ppleased where account = " + FCConvert.ToString(cmbTransferTo.ItemData(cmbTransferTo.SelectedIndex)) + " order by line", modPPGN.strPPDatabase);
			if (!clsSave.EndOfFile())
			{
				clsSave.MoveLast();
				// TODO Get_Fields: Check the table for the column [line] and replace with corresponding Get_Field method
				lngMaxLine = FCConvert.ToInt32(clsSave.Get_Fields("line"));
			}
			lngCurLine = lngMaxLine + 1;
			for (x = lngStart; x <= lngEnd; x++)
			{
				clsSave.AddNew();
				clsSave.Set_Fields("line", lngCurLine);
				clsSave.Set_Fields("account", cmbTransferTo.ItemData(cmbTransferTo.SelectedIndex));
				clsSave.Set_Fields("rb", vs1.TextMatrix(x, colRB));
				clsSave.Set_Fields("cd", vs1.TextMatrix(x, colCD));
				clsSave.Set_Fields("quantity", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colQT))));
				clsSave.Set_Fields("description", Strings.Trim(vs1.TextMatrix(x, colDS)));
				clsSave.Set_Fields("month", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colMO))));
				if (Strings.Trim(vs1.TextMatrix(x, colCS)) == string.Empty)
				{
					clsSave.Set_Fields("cost", 0);
				}
				else
				{
					clsSave.Set_Fields("cost", FCConvert.ToInt32(FCConvert.ToDouble(vs1.TextMatrix(x, colCS) + "")));
				}
				clsSave.Set_Fields("amountreplacing", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colreplacingvalue))));
				clsSave.Set_Fields("yearfirstassessed", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colYFA))));
				clsSave.Set_Fields("exemptyear", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colBETE))));
				clsSave.Set_Fields("leasedate", vs1.TextMatrix(x, colLD));
				clsSave.Set_Fields("mos", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colMS))));
				clsSave.Set_Fields("l1p2", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colLP))));
				clsSave.Set_Fields("monthlyrent", FCConvert.ToSingle(FCConvert.ToDouble(vs1.TextMatrix(x, colMR))));
				clsSave.Set_Fields("depyrs", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colDY))));
				clsSave.Set_Fields("fctr", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colFC))));
				if (Strings.Trim(vs1.TextMatrix(x, colVL) + "") == string.Empty)
				{
					clsSave.Set_Fields("value", 0);
				}
				else
				{
					clsSave.Set_Fields("value", FCConvert.ToInt32(FCConvert.ToDouble(vs1.TextMatrix(x, colVL))));
				}
				clsSave.Set_Fields("leasedfrom", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colLF))));
				clsSave.Update();
				lngCurLine += 1;
			}
			// x
			if (cmbTransfer_1.SelectedIndex == 1)
			{
				// delete these rows
				for (x = lngEnd; x >= lngStart; x--)
				{
					if (Conversion.Val(vs1.TextMatrix(x, colKY)) > 0)
					{
						GridDeleted.AddItem(FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colKY))));
					}
					vs1.RemoveItem(x);
				}
				// x
				vs1.Select(vs1.Row, vs1.Col);
				//Application.DoEvents();
				RefreshNumbers();
			}
			framTransfer.Visible = false;
		}

		private void frmPPLeased_Activated(object sender, System.EventArgs e)
		{
			object hold;
			object Layout;
			// If FormExist(Me) Then Exit Sub
			// 
			if (!modSecurity.ValidPermissions_6(this, modSecurity.ACCOUNTMAINTENANCE))
			{
				MessageBox.Show("Your permission setting for this function is set to none or is missing.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				boolKickedOut = true;
				cmdMaster_Click();
			}
			Formactivating = false;
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
		}

		private void frmPPLeased_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// Save & Quit
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				mnuQuitNoSave_Click();
			}
		}

		private void frmPPLeased_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// Escape And No Save
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				mnuQuitNoSave_Click();
				// Make Enter Act Like Tab
			}
			else if (KeyAscii == Keys.Return)
			{
				if (frmPPLeased.InstancePtr.ActiveControl.GetName() == vs1.Name)
				{
					if (vs1.Col < vs1.Cols - 1)
					{
						vs1.Col += 1;
					}
				}
				else
				{
					KeyAscii = (Keys)0;
					Support.SendKeys("{TAB}", false);
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmPPLeased_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.End:
					{
						// Add Rows to End
						// See vs1_KeyDown
						break;
					}
				case Keys.Insert:
					{
						// Insert Row in Middle
						if (Shift == 2)
						{
							// ADD TO MIDDLE OF GRID
							vs1.AddItem("\t" + FCConvert.ToString(FCConvert.ToInt32(vs1.TextMatrix(vs1.Rows - 1, colLN)) + 1), vs1.Row);
							vs1.TextMatrix(vs1.Row, colLN, "NEW");
							vs1.TextMatrix(vs1.Row, colRB, "");
							CurrentRow = vs1.Row;
							boolDataChanged = true;
						}
						// DELETE ROW
						break;
					}
				case Keys.Delete:
					{
						if (vs1.Rows > 1)
						{
							if (vs1.Col == colLN)
							{
								boolDataChanged = true;
								modGlobal.Statics.MessageResp1 = MessageBox.Show("If you delete this Row, it will be permanently deleted from your database.  Are you sure you want to delete this record?", "Confirm Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
								if (modGlobal.Statics.MessageResp1 == DialogResult.No)
									return;
								vs1.Select(vs1.Row, colLN, vs1.Row, colVL);
								string newSQL = "";
								newSQL = "Account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount) + " And Line = " + vs1.TextMatrix(CurrentRow, colLN);
								ppl.FindFirstRecord2("Account,Line", FCConvert.ToString(modPPGN.Statics.CurrentAccount) + "," + vs1.TextMatrix(CurrentRow, colLN), ",");
								ppl.Delete();
								vs1.RemoveItem(vs1.Row);
								vs2.Refresh();
								vs1.Select(vs1.Row, colRB);
							}
						}
						break;
					}
				case Keys.F5:
					{
						// Refresh Grid
						// REFRESH Grid
						vs1.Refresh();
						vs2.Refresh();
						break;
					}
				case Keys.F9:
					{
						// Calculate
						Support.SendKeys("{HOME}", false);
						Calculate();
						break;
					}
			}
			//end switch
		}

		public void ReLoad()
		{
			// vbPorter upgrade warning: hold As Variant --> As string
			string hold = "";
			object Layout;
			colLN = 0;
			colRB = 1;
			colBETE = 2;
			colCD = 3;
			colQT = 4;
			colDS = 5;
			colYFA = 6;
			colYearsClaimed = 7;
			colreplacingvalue = 8;
			colMO = 9;
			colCS = 10;
			colLD = 11;
			colMS = 12;
			colLP = 13;
			colMR = 14;
			colDY = 15;
			colFC = 16;
			colKY = 17;
			colVL = 18;
			colLF = 19;
			CurrentRow = 0;
			vs1.Cols = colLF + 1;
			// Erase DeletedRows()
			lngDeletes = 0;
			UpdatedRows = 0;
			// Erase UpdateLeased()
			AddedRows = 0;
			// Erase NewLeased()
			// Select CurrentAccount if none.
			if (modPPGN.Statics.CurrentAccount < 1)
			{
				DoOverTag:
				;
				hold = Interaction.InputBox("Enter Account Number", "");
				if (hold == "")
				{
					//MDIParent.InstancePtr.Show();
					frmPPLeased.InstancePtr.Close();
					return;
				}
				modPPGN.Statics.CurrentAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(hold)));
				if (modPPGN.Statics.CurrentAccount > modPPCalculate.Statics.NextPPAccount)
				{
					MessageBox.Show("You have selected an account number greater than your highest available number. Please reselect an account.", "Account Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					goto DoOverTag;
				}
			}
			// 
			strSQL = "SELECT * FROM PPLeased WHERE Account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount) + " ORDER BY [Line]";
			ppl.OpenRecordset(strSQL, modPPGN.strPPDatabase);
			// 
			vs1.Rows = 1;
			if (ppl.EndOfFile() != true && ppl.BeginningOfFile() != true)
			{
				FillGrid();
			}
			else
			{
				// Dim xx As Integer
				// If vs1.Rows <= 1 Then
				// For xx = 1 To 16
				AddNewLeased();
				// Next xx
				// End If
			}
			// 
			// 
			FillMasterLabels();
			SetColumnWidths();
            //FC:FINAL:AM:#3643 - don't set cell alignments
			//SetCellAlignments();
			// 
			// 
			// Erase DeletedRows
			lngDeletes = 0;
			// 
			modPPGN.Statics.LeasedTable = true;
			modPPGN.Statics.Exiting = false;
			// Formactivating = True
			// Fill Master Update Fields
			string strLOC = "";
			ppm.OpenRecordset("SELECT * FROM PPMaster Where Account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount), modPPGN.strPPDatabase);
			if (ppm.EndOfFile())
				goto SkipStuff;
			// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
			if (fecherFoundation.FCUtils.IsNull(ppm.Get_Fields("StreetNumber")) == false)
			{
				// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
				strLOC = FCConvert.ToString(ppm.Get_Fields("StreetNumber"));
				strLOC += " ";
			}
			if (fecherFoundation.FCUtils.IsNull(ppm.Get_Fields_String("Street")) == false)
			{
				strLOC += ppm.Get_Fields_String("Street");
			}
			// 
			// 
			SkipStuff:
			;
			// Set Column Masks for Cells
			// vs1.ColEditMask(colRB, "&;); "
			vs1.ColComboList(colRB, " " + "\t" + "No Reimbursement|*" + "\t" + "BETR Reimbursable|P" + "\t" + "BETE Pending");
			//FC:FINAL:MSH - i.issue #1269: remove incorrect mask elements
			//vs1.ColEditMask(colCD, "&;; ");
			//// vs1.ColEditMask(colQT, "999;); "
			//vs1.ColEditMask(colQT, "99999;; ");
			//vs1.ColEditMask(colBETE, "9999;; ");
			//// vs1.ColEditMask(colDS, ">&&&&&&&&&&&&&&&&&&&&&;q); "
			//// vs1.ColEditMask(colCS, "99999999;); "
			//vs1.ColEditMask(colLD, "99/99;; ");
			//vs1.ColEditMask(colMS, "999;; ");
			//vs1.ColEditMask(colLP, "9;; ");
			//vs1.ColEditMask(colMR, "&&&&&&&&;; ");
			//vs1.ColEditMask(colDY, "999;; ");
			//vs1.ColEditMask(colFC, "999;; ");
			vs1.ColEditMask(colCD, "#");
			// vs1.ColEditMask(colQT, "999;); "
			vs1.ColEditMask(colQT, "99999");
			vs1.ColEditMask(colBETE, "9999");
			// vs1.ColEditMask(colDS, ">&&&&&&&&&&&&&&&&&&&&&;q); "
			// vs1.ColEditMask(colCS, "99999999;); "
			vs1.ColEditMask(colLD, "99/99");
			vs1.ColEditMask(colMS, "999");
			vs1.ColEditMask(colLP, "9");
			vs1.ColEditMask(colMR, "########");
			vs1.ColEditMask(colDY, "999");
			vs1.ColEditMask(colFC, "999");
			// vs1.Cell(flexcpText, 1, colVL, vs1.Rows - 1, colVL) = 0
			// 
			SetColumnWidths();
			// 
			// Setting header height and titles
			// vs1.RowHeight(0) = 750
			// 
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, colLN, 0, colLN, "Line");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, colRB, 0, colRB, "RB");
			vs1.TextMatrix(0, colBETE, "Year Exempt");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, colCD, 0, colCD, "CD");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, colQT, 0, colQT, "Qty");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, colDS, 0, colDS, "Description");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, colYFA, "Year First Assessed");
			vs1.TextMatrix(0, colYearsClaimed, "Years Claimed");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, colreplacingvalue, "Amount Replacing");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, colMO, 0, colMO, "MO");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, colCS, 0, colCS, "Cost");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, colLD, 0, colLD, "Lease Date");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, colMS, 0, colMS, "MOS");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, colLP, 0, colLP, "1=L 2=P");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, colMR, 0, colMR, "Monthly Rent");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, colDY, 0, colDY, "% GD");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, colFC, 0, colFC, "Fctr");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, colVL, 0, colVL, "Value");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, colLF, 0, colLF, "Leased From");
			// 
			// vs1.Cell(flexcpAlignment, 0, colLN, 0, colVL) = 4
			// vs1.Cell(flexcpFontBold, 0, colLN, 0, colLF) = True
			// 
			// Set Column Formats
			//vs1.ColFormat(colCS, "#,###");
			//vs1.ColFormat(colreplacingvalue, "#,###,###");
			//vs1.ColFormat(colLD, "##/##");
			//vs1.ColFormat(colMR, "##,###.##");
			//FC:FINAL:MSH - i.issue #1269: set correct string format
			vs1.ColFormat(colCS, "#,##0");
			vs1.ColFormat(colreplacingvalue, "#,###,##0");
			vs1.ColFormat(colLD, "MM/dd");
			vs1.ColFormat(colMR, "##,##0.00");
			vs1.ColFormat(colVL, "#,##0");
			// 
			vs1.Visible = true;
			vs1.Refresh();
			boolDataChanged = false;
		}

		public void frmPPLeased_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPPLeased properties;
			//frmPPLeased.ScaleWidth	= 9300;
			//frmPPLeased.ScaleHeight	= 7830;
			//frmPPLeased.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this, false);
			Formactivating = true;
			ReLoad();
			//FC:FINAL:MSH - i.issue #1269: select first cell after loading the form for correct work of row adding(as in original app)
			//FC:FINAL:MSH - i.issue #1325: remove focus from grid to avoid staring of editing CurrentCell, which won't be handled correctly
			//vs1.Focus();
			vs1.Row = vs1.GetFlexRowIndex(0);
			vs1.Col = vs1.GetFlexColIndex(0);
			vs1.Appear -= new EventHandler(this.Vs1_Appear);
			vs1.Appear += new EventHandler(this.Vs1_Appear);
		}
		//FC:FINAL:MSH - i.issue #1274: assign handlers only after displaying grid, because in another case 'boolDataChanged' will be changed upon initialization of the form
		private void Vs1_Appear(object sender, EventArgs e)
		{
			this.vs1.CellValueChanged -= new Wisej.Web.DataGridViewCellEventHandler(this.vs1_ChangeEdit);
			this.vs1.CellEndEdit -= new Wisej.Web.DataGridViewCellEventHandler(this.vs1_AfterEdit);
			this.vs1.CellValidating -= new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vs1_ValidateEdit);
			this.vs1.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vs1_ChangeEdit);
			this.vs1.CellEndEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vs1_AfterEdit);
			this.vs1.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vs1_ValidateEdit);
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (boolDataChanged)
			{
				vs1.Row = 0;
				//Application.DoEvents();
				// vbPorter upgrade warning: intret As short, int --> As DialogResult
				DialogResult intret;
				intret = MessageBox.Show("Data has been changed" + "\r\n" + "Save changes made?", "Save Changes?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
				if (intret == DialogResult.Yes)
				{
					if (!NewSaveLeased())
					{
						e.Cancel = true;
						return;
					}
				}
				else if (intret == DialogResult.Cancel)
				{
					e.Cancel = true;
					return;
				}
			}
			modPPGN.Statics.LeasedTable = false;
		}

		private void frmPPLeased_Resize(object sender, System.EventArgs e)
		{
			SetColumnWidths();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			modGlobal.SaveWindowSize(this);
			// Erase DeletedRows
			lngDeletes = 0;
			// 
			// If Not boolKickedOut Then Call CheckForRB
			if (!boolKickedOut)
				modPPGN.Check_RB_Fields();
			modPPGN.Statics.LeasedTable = false;
			if (!boolDontUnloadOthers)
			{
				//FC:FINAL:DDU:#i1270 - close forms instead of comment their hide method
				frmPPMaster.InstancePtr.Close();
				if (modPPGN.Statics.ItemizedTable)
				{
					frmPPItemized.InstancePtr.Close();
				}
				//MDIParent.InstancePtr.BringToFront();
				//MDIParent.InstancePtr.Show();
			}
		}

		private void mnuBackToMaster_Click()
		{
			frmPPMaster.InstancePtr.Show(App.MainForm);
			// frmPPLeased.Hide
			frmPPLeased.InstancePtr.Close();
		}

		public void UnloadOnlySelf()
		{
			boolDontUnloadOthers = true;
			Close();
		}

		private void mnuAdd_Click(object sender, System.EventArgs e)
		{
			int intCounter;
			if (vs1.Rows > 1)
			{
				if (vs1.Row < 1)
					return;
			}
			int intTemp;
			intTemp = vs1.Row;
			// vs1.AddItem Format(vs1.Row, "0000"), vs1.Row
			if (vs1.Rows > 1)
			{
				if (vs1.Row == 0)
				{
					vs1.AddItem("NEW");
					CurrentRow = vs1.Rows - 1;
				}
				else
				{
					vs1.AddItem("NEW", intTemp);
					CurrentRow = intTemp;
				}
			}
			else
			{
				vs1.AddItem("NEW");
				CurrentRow = 1;
			}
			// vs1.TextMatrix(vs1.Row, colLN) = "NEW"
			// CurrentRow = vs1.Row
			// vs1.Cell(flexcpText, CurrentRow, colLN) = Format(vs1.Row, "0000")
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colCD, "1");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colQT, "1");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colCS, "0");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colreplacingvalue, "0");
			if (modGlobal.Statics.CustomizeStuff.DefaultYearFirstAssessed == 0)
			{
				vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colYFA, DateTime.Today.Year);
			}
			else
			{
				vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colYFA, modGlobal.Statics.CustomizeStuff.DefaultYearFirstAssessed);
			}
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colLD, "00/00");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colMS, "000");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colLP, "2");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colMR, "0");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colDY, "00");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colFC, "100");
			vs1.RowData(CurrentRow, true);
			for (intCounter = vs1.Row + 1; intCounter <= vs1.Rows - 1; intCounter++)
			{
				if (Strings.UCase(vs1.TextMatrix(intCounter, 0)) != "NEW")
				{
					vs1.TextMatrix(intCounter, 0, Strings.Format(FCConvert.ToInt32(vs1.TextMatrix(intCounter, 0)) + 1, "0000"));
					vs1.RowData(intCounter, true);
				}
			}
			//Application.DoEvents();
			RefreshNumbers();
		}

		public void mnuAdd_Click()
		{
			mnuAdd_Click(mnuAdd, new System.EventArgs());
		}

		private void mnuAddMultipleRows_Click(object sender, System.EventArgs e)
		{
			int intRows;
			int intCounter;
			intRows = FCConvert.ToInt32(Math.Round(Conversion.Val(Interaction.InputBox("Enter number of rows to add", "TRIO Software"))));
			for (intCounter = 1; intCounter <= intRows; intCounter++)
			{
				mnuAdd_Click();
			}
			//Application.DoEvents();
			RefreshNumbers();
		}

		private void mnuBack_Click(object sender, System.EventArgs e)
		{
			// frmPPMaster.Show , MDIParent
		}

		private void mnuCalc_Click(object sender, System.EventArgs e)
		{
			if (boolDataChanged)
			{
				if (MessageBox.Show("In order to calculate, current changes must be saved" + "\r\n" + "Save changes?", "Save Changes?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
				{
					if (!NewSaveLeased())
					{
						return;
					}
				}
				else
				{
					return;
				}
			}
			this.Hide();
			//FC:FINAL:MSH - i.issue #1274: only after disposing the form will be showed correct
			frmPPCalculations.InstancePtr.Unload();
			frmPPCalculations.InstancePtr.Dispose();
			modPPCalculate.Statics.CalculateFromMaster = true;
			frmPPCalculations.InstancePtr.Show(App.MainForm);
            //FC:FINAL:SBE - #3589 - update client side in order to activate the correct form
            FCUtils.ApplicationUpdate(App.MainForm);
            //FC:FINAL:MSH - i.issue #1274: restore missing form closing
            this.Close();
            frmPPLeased.InstancePtr.Close();
        }

		private void mnuPendingToBETE_Click(object sender, System.EventArgs e)
		{
			int lngRow;
			int intYear;
			intYear = modGlobal.Statics.CustomizeStuff.CurrentBETEYear;
			if (intYear == 0)
				intYear = DateTime.Today.Year;
			if (MessageBox.Show("This will change all items marked as pending to be BETE exempt for the year " + FCConvert.ToString(intYear) + "\r\n" + "Continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
			{
				for (lngRow = 1; lngRow <= vs1.Rows - 1; lngRow++)
				{
					if (Strings.UCase(vs1.TextMatrix(lngRow, colRB)) == "P")
					{
						if (modGlobal.Statics.CustomizeStuff.CurrentBETEYear > 0)
						{
							vs1.TextMatrix(lngRow, colBETE, FCConvert.ToString(modGlobal.Statics.CustomizeStuff.CurrentBETEYear));
						}
						else
						{
							vs1.TextMatrix(lngRow, colBETE, FCConvert.ToString(DateTime.Today.Year));
						}
						vs1.TextMatrix(lngRow, colRB, "");
						vs1.RowData(lngRow, true);
						boolDataChanged = true;
					}
				}
				// lngRow
			}
		}

		private void mnuQuitNoSave_Click(object sender, System.EventArgs e)
		{
			modPPGN.Statics.LeasedTable = false;
			frmPPLeased.InstancePtr.Close();
		}

		public void mnuQuitNoSave_Click()
		{
			mnuQuitNoSave_Click(mnuQuitNoSave, new System.EventArgs());
		}

		private void mnuRemove_Click(object sender, System.EventArgs e)
		{
			//FC:FINAL:MSH - i.issue #1269: replace SendKeys, because it is not implemented
			//Support.SendKeys("{DELETE}", false);
			vs1_KeyDownEvent(vs1, new KeyEventArgs(Keys.Delete));
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			// Call SaveLeased
			if (NewSaveLeased())
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
			else
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
		}

		private void mnuSaveQuit_Click(object sender, System.EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			if (NewSaveLeased())
			{
				modPPGN.Statics.LeasedTable = false;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				frmPPLeased.InstancePtr.Close();
			}
			else
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
		}

		private void mnuTransfer_Click(object sender, System.EventArgs e)
		{
			// will copy or move highlighted rows to another account
			FillcmbTransfer();
			framTransfer.Visible = true;
		}

		private void FillcmbTransfer()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			cmbTransferTo.Clear();
			clsLoad.OpenRecordset("select account,name from ppmaster where not deleted = 1 order by account", modPPGN.strPPDatabase);
			while (!clsLoad.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				cmbTransferTo.AddItem(clsLoad.Get_Fields("account") + "   " + clsLoad.Get_Fields_String("name"));
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				cmbTransferTo.ItemData(cmbTransferTo.NewIndex, FCConvert.ToInt32(clsLoad.Get_Fields("Account")));
				clsLoad.MoveNext();
			}
		}

		private void vs1_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			int fn;
			// For fn = 1 To 200
			// If NewLeased(fn) = Row Then Exit Sub
			// If UpdateLeased(fn) = Row Then Exit Sub
			// If UpdateLeased(fn) = 0 Then Exit For
			// Next fn
			// UpdatedRows = UpdatedRows + 1
			// UpdateLeased(UpdatedRows) = Val(vs1.TextMatrix(Row, colKY))
			vs1.RowData(vs1.Row, true);
			boolDataChanged = true;
		}
		//FC:TODO:AM
		//private void vs1_BeforeScrollTip(object sender, AxVSFlex6DAO._IvsFlexGridEvents_BeforeScrollTipEvent e)
		//{
		//	vs1.ScrollTipText = FCConvert.ToString(vs1.MouseRow);
		//}
		private void vs1_ChangeEdit(object sender, DataGridViewCellEventArgs e)
		{
			boolDataChanged = true;
		}

		private void vs1_EnterCell(object sender, DataGridViewCellEventArgs e)
		{
			//int Col;
			//Col = vs1.Col;
			//FC:FINAL:MSH - i.issue #1269: save correct indexes of the cell
			int row = vs1.GetFlexRowIndex(e.RowIndex);
			int col = vs1.GetFlexColIndex(e.ColumnIndex);
			// If calculating then no edit in last row.
			if (Calculating == true)
			{
				if (row == vs1.Rows - 1)
				{
					vs1.Editable = FCGrid.EditableSettings.flexEDNone;
					return;
				}
			}
			else
			{
				// Line & Value
				if (col == colLN || col == colVL)
				{
					// No Input Allowed
					vs1.Editable = FCGrid.EditableSettings.flexEDNone;
				}
				else if (col == colKY)
				{
					CurrentCol = 1;
					CurrentRow = row + 1;
					vs1.Col = 1;
					if ((row + 1) < vs1.Rows)
					{
						vs1.Row += 1;
					}
				}
				else
				{
					vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					// Input Allowed
					// vs1.EditCell
					// vs1.EditSelStart = 0
					// vs1.EditSelLength = 0
				}
			}
		}

		private void vs1_KeyDownEvent(object sender, KeyEventArgs e)
		{
			// VB6 Bad Scope Dim:
			// vbPorter upgrade warning: strTemp As string	OnWriteFCConvert.ToInt32(
			string strTemp = "";
			int lngTemp1 = 0;
			int lngTemp2 = 0;
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.F9)
			{
				KeyCode = 0;
				if (vs1.Col == colCD)
				{
					modPPHelp.HelpCatNumbers();
					frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				}
				else if (vs1.Col == colDS)
				{
					modPPHelp.HelpDescription();
					frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				}
				else if (vs1.Col == colLD)
				{
					modPPHelp.HelpLeaseDate();
					frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				}
				else if (vs1.Col == colMS)
				{
					modPPHelp.HelpLeaseMonths();
					frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				}
				else if (vs1.Col == colLP)
				{
					modPPHelp.HelpLP();
					frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				}
				else if (vs1.Col == colMR)
				{
					modPPHelp.HelpMonthlyRent();
					frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				}
				else if (vs1.Col == colCS)
				{
					modPPHelp.HelpLeasedReplaceCost();
					frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				}
				else if (vs1.Col == colDY)
				{
					modPPHelp.HelpPercGood();
					frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				}
				else if (vs1.Col == colFC)
				{
					modPPHelp.HelpFactor();
					frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				}
				return;
			}
			if (KeyCode == Keys.Delete)
			{
				int JJ;
				if (vs1.Row > vs1.RowSel)
				{
					lngTemp1 = vs1.Row;
					lngTemp2 = vs1.RowSel;
					vs1.Row = lngTemp2;
					vs1.RowSel = lngTemp1;
					lngTemp1 = vs1.Row;
					lngTemp2 = vs1.RowSel;
				}
				else
				{
					lngTemp1 = vs1.Row;
					lngTemp2 = vs1.RowSel;
				}
				if (lngTemp1 < 1)
					return;
				for (JJ = lngTemp2; JJ >= lngTemp1; JJ--)
				{
					// If vs1.Col = 1 Then
					// If Val(vs1.TextMatrix(JJ, colLN)) <> 0 Then
					// lngDeletes = lngDeletes + 1
					// If lngDeletes >= 1000 Then
					// MsgBox "You must save your work before deleting anymore lines.", vbOKOnly + vbExclamation, "DELETE"
					// Else
					// DeletedRows(lngDeletes) = Val(vs1.TextMatrix(vs1.RowSel, colKY))
					if (Conversion.Val(vs1.TextMatrix(JJ, colKY)) > 0)
					{
						GridDeleted.AddItem(FCConvert.ToString(Conversion.Val(vs1.TextMatrix(JJ, colKY))));
					}
					vs1.RemoveItem(JJ);
					// End If
					// ElseIf vs1.TextMatrix(JJ, colLN) = "NEW" Then
					// vs1.RemoveItem (JJ)
					// End If
					// End If
				}
				// JJ
				// vs1.Select vs1.Row, vs1.Col
				//Application.DoEvents();
				RefreshNumbers();
			}
			// 
			if ((vs1.Row == (vs1.Rows - 1)) && KeyCode == Keys.Down)
			{
				KeyCode = 0;
				AddNewLeased();
				vs1.Select(CurrentRow, colRB);
				return;
			}
			if (CurrentCol == colRB)
			{
				// If KeyCode = 106 Then
				// vs1.RowData(vs1.Row) = True
				// SendKeys "{DOWN}"
				// ElseIf KeyCode = 56 And Shift = 1 Then
				// vs1.RowData(vs1.Row) = True
				// DoEvents
				// If (vs1.Rows - 1) - vs1.Row < 5 Then
				// Call AddNewLeased
				// vs1.Row = vs1.Row - 1
				// End If
				// vs1.Select vs1.Row, vs1.Col, vs1.Row, vs1.Col
				// vs1.Select vs1.Row + 1, vs1.Col, vs1.Row + 1, vs1.Col
				// DoEvents
				// End If
				//FC:FINAL:BBE - use enum Keys for KeyCode == 106
				if (KeyCode == Keys.Multiply)
				{
					if (Strings.Trim(vs1.TextMatrix(vs1.Row, colRB) + "") == "*")
					{
						vs1.TextMatrix(vs1.Row, colRB, " ");
					}
					else
					{
						vs1.TextMatrix(vs1.Row, colRB, "*");
						vs1.TextMatrix(vs1.Row, colBETE, "");
					}
					vs1.RowData(vs1.Row, true);
					if (vs1.Row < vs1.Rows - 1)
					{
						Support.SendKeys("{DOWN}", false);
					}
					// KeyCode = 0
				}
				//FC:FINAL:BBE - use enum Keys for KeyCode == 56
				else if (KeyCode == Keys.D8 && e.Shift)
				{
					// MsgBox "D"
					if (Strings.Trim(vs1.TextMatrix(vs1.Row, colRB) + "") == "*")
					{
						vs1.TextMatrix(vs1.Row, colRB, " ");
					}
					else
					{
						vs1.TextMatrix(vs1.Row, colRB, "*");
						vs1.TextMatrix(vs1.Row, colBETE, "");
					}
					vs1.RowData(vs1.Row, true);
					// SendKeys "{DOWN}"
					if (vs1.Row < (vs1.Rows - 1))
					{
						vs1.Row += 1;
					}
				}
				else
				{
					switch (KeyCode)
					{
						case Keys.D1:
						case Keys.NumPad1:
							{
								lngTemp1 = 1;
								break;
							}
						case Keys.D2:
						case Keys.NumPad2:
							{
								lngTemp1 = 2;
								break;
							}
						case Keys.D3:
						case Keys.NumPad3:
							{
								lngTemp1 = 3;
								break;
							}
						case Keys.D4:
						case Keys.NumPad4:
							{
								lngTemp1 = 4;
								break;
							}
						case Keys.D5:
						case Keys.NumPad5:
							{
								lngTemp1 = 5;
								break;
							}
						case Keys.D6:
						case Keys.NumPad6:
							{
								lngTemp1 = 6;
								break;
							}
						case Keys.D7:
						case Keys.NumPad7:
							{
								lngTemp1 = 7;
								break;
							}
						case Keys.D8:
						case Keys.NumPad8:
							{
								lngTemp1 = 8;
								break;
							}
						case Keys.D9:
						case Keys.NumPad9:
							{
								lngTemp1 = 9;
								break;
							}
						case Keys.D0:
						case Keys.NumPad0:
							{
								lngTemp1 = 0;
								break;
							}
						case Keys.P:
							{
								if (Strings.UCase(Strings.Trim(vs1.TextMatrix(vs1.Row, colRB) + "")) == "P")
								{
									vs1.TextMatrix(vs1.Row, colRB, " ");
								}
								else
								{
									vs1.TextMatrix(vs1.Row, colRB, "P");
									vs1.TextMatrix(vs1.Row, colBETE, "");
								}
								vs1.RowData(vs1.Row, true);
								if (e.Shift)
								{
									if (vs1.Row < vs1.Rows - 1)
									{
										Support.SendKeys("{DOWN}", false);
									}
								}
								else
								{
									if (vs1.Row < (vs1.Rows - 1))
									{
										vs1.Row += 1;
									}
								}
								return;
							}
						case Keys.Space:
							{
								vs1.TextMatrix(vs1.Row, colRB, " ");
								vs1.TextMatrix(vs1.Row, colBETE, "");
								vs1.RowData(vs1.Row, true);
								if (e.Shift)
								{
									if (vs1.Row < vs1.Rows - 1)
									{
										Support.SendKeys("{DOWN}", false);
									}
								}
								else
								{
									if (vs1.Row < (vs1.Rows - 1))
									{
										vs1.Row += 1;
									}
								}
								return;
							}
						default:
							{
								return;
							}
					}
					//end switch
					lngTemp2 = DateTime.Today.Year;
					strTemp = FCConvert.ToString(DateTime.Today.Year);
					fecherFoundation.Strings.MidSet(ref strTemp, 4, 1, FCConvert.ToString(lngTemp1));
					if (Conversion.Val(strTemp) > lngTemp2 + 1)
					{
						strTemp = FCConvert.ToString(DateTime.Today.Year);
					}
					else if (Conversion.Val(strTemp) < lngTemp2 - 8)
					{
						strTemp = FCConvert.ToString(DateTime.Today.Year + 1);
					}
					KeyCode = 0;
					vs1.TextMatrix(vs1.Row, colRB, " ");
					vs1.TextMatrix(vs1.Row, colBETE, strTemp);
					// vs1.Col = colCD
					vs1.RowData(vs1.Row, true);
					if (e.Shift)
					{
						if (vs1.Row < vs1.Rows - 1)
						{
							Support.SendKeys("{DOWN}", false);
						}
					}
					else
					{
						if (vs1.Row < (vs1.Rows - 1))
						{
							vs1.Row += 1;
						}
					}
				}
				KeyCode = 0;
			}
			else if (CurrentCol == colCD)
			{
				if (KeyCode == Keys.P)
				{
					if (CurrentRow != 1)
					{
						if (CurrentRow == vs1.Rows - 1)
						{
							// If CurrentRow = vs1.Rows - 2 Or CurrentRow = vs1.Rows - 1 Then
							vs1.Rows += 1;
							vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow + 1, colLN, Strings.Format(vs1.Row, "0000"));
							vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow + 1, colCD, "1");
							vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow + 1, colQT, "1");
							vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow + 1, colCS, "0");
							if (modGlobal.Statics.CustomizeStuff.DefaultYearFirstAssessed == 0)
							{
								vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow + 1, colYFA, DateTime.Today.Year);
							}
							else
							{
								vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow + 1, colYFA, modGlobal.Statics.CustomizeStuff.DefaultYearFirstAssessed);
							}
							vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow + 1, colreplacingvalue, "0");
							vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow + 1, colLD, "00/00");
							vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow + 1, colLP, "2");
							vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow + 1, colMR, "0");
							vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow + 1, colDY, "000");
							vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow + 1, colFC, "100");
						}
						int iii;
						for (iii = colRB; iii <= colFC; iii++)
						{
							vs1.TextMatrix(CurrentRow, iii, vs1.TextMatrix(CurrentRow - 1, iii));
						}
						// iii
						vs1.Rows += 1;
						KeyCode = 0;
						// SendKeys "{DOWN}"
						Support.SendKeys("{DOWN}", false);
					}
				}
				else if (KeyCode == Keys.D)
				{
					vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colLN, Strings.Format(vs1.Row, "0000"));
					vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colCD, "1");
					vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colQT, "1");
					vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colDS, "");
					vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colCS, "0");
					if (modGlobal.Statics.CustomizeStuff.DefaultYearFirstAssessed == 0)
					{
						vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colYFA, DateTime.Today.Year);
					}
					else
					{
						vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colYFA, modGlobal.Statics.CustomizeStuff.DefaultYearFirstAssessed);
					}
					vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow + 1, colreplacingvalue, "0");
					vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colMS, "0");
					vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colLD, "00/00");
					vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colLP, "2");
					vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colMR, "0");
					vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colDY, "000");
					vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colFC, "100");
					KeyCode = 0;
					// SendKeys "{DOWN}"
					Support.SendKeys("{DOWN}", false);
				}
			}
			// make the arrow keys work
			//FC:FINAL:BBE - use enum Keys for KeyCode == 37
			if (vs1.Col == 1 && KeyCode == Keys.Left)
			{
				if (vs1.Row > 1)
				{
					vs1.Row -= 1;
					vs1.Col = colLF;
					KeyCode = 0;
				}
			}
			//FC:FINAL:BBE - use enum Keys for KeyCode == 39
			else if (vs1.Col == colLF && KeyCode == Keys.Right)
			{
				if (vs1.Row != vs1.Rows - 1)
				{
					vs1.Row += 1;
					vs1.Col = 1;
					KeyCode = 0;
				}
			}
		}

		private void vs1_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.F9)
			{
				KeyCode = 0;
				if (vs1.Col == colCD)
				{
					modPPHelp.HelpCatNumbers();
					frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				}
				else if (vs1.Col == colDS)
				{
					modPPHelp.HelpDescription();
					frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				}
				else if (vs1.Col == colLD)
				{
					modPPHelp.HelpLeaseDate();
					frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				}
				else if (vs1.Col == colMS)
				{
					modPPHelp.HelpLeaseMonths();
					frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				}
				else if (vs1.Col == colLP)
				{
					modPPHelp.HelpLP();
					frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				}
				else if (vs1.Col == colMR)
				{
					modPPHelp.HelpMonthlyRent();
					frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				}
				else if (vs1.Col == colCS)
				{
					modPPHelp.HelpLeasedReplaceCost();
					frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				}
				else if (vs1.Col == colDY)
				{
					modPPHelp.HelpPercGood();
					frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				}
				else if (vs1.Col == colFC)
				{
					modPPHelp.HelpFactor();
					frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				}
			}
			if (KeyCode == Keys.Return)
			{
				KeyCode = 0;
				if (CurrentRow == vs1.Rows - 1)
				{
					if (vs1.Col == colLF)
					{
						AddNewLeased();
						vs1.Select(CurrentRow, colRB);
					}
				}
				if (vs1.Col != vs1.Cols - 1)
				{
					vs1.Col += 1;
				}
			}
		}

		private void vs1_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii == 13)
			{
				KeyAscii = 0;
				if (CurrentRow == vs1.Rows - 1)
				{
					if (vs1.Col == colLF)
					{
						AddNewLeased();
						vs1.Select(CurrentRow, colRB);
					}
				}
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void vs1_LeaveCell(object sender, DataGridViewCellEventArgs e)
		{
			if (Formactivating == true)
				CurrentRow = vs1.Row;
			// If CurrentCol = 6 Then
			// vs1.TextMatrix(CurrentRow, colCS) = Trim(Format(Format(vs1.TextMatrix(CurrentRow, colCS), "#,##0"), "@@@@@@@@"))
			// ElseIf CurrentCol = 10 Then
			// vs1.TextMatrix(CurrentRow, colMR) = Trim(Format(Format(vs1.TextMatrix(CurrentRow, colMR), "#,##0.00"), "@@@@@@@@"))
			// End If
			if (CurrentRow < 1 || CurrentRow > vs1.Rows - 1)
				return;
			if (CurrentCol == colreplacingvalue)
			{
				int lngTemp = 0;
				if (Strings.Mid(vs1.TextMatrix(CurrentRow, colreplacingvalue) + " ", 1, 1) == ".")
				{
					lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(vs1.TextMatrix(CurrentRow, colreplacingvalue) + " ", 2))));
					if (lngTemp > 0 && lngTemp < vs1.Rows)
					{
						vs1.TextMatrix(CurrentRow, colreplacingvalue, FCConvert.ToString(Conversion.Val(vs1.TextMatrix(lngTemp, colVL))));
					}
					else
					{
						vs1.TextMatrix(CurrentRow, colreplacingvalue, FCConvert.ToString(0));
					}
				}
			}
			else if (CurrentCol == colDS)
			{
				int hold = 0;
				string hold1 = "";
				if (Strings.Mid(vs1.TextMatrix(CurrentRow, colDS), 1, 1) == ".")
				{
					hold1 = Strings.Mid(vs1.TextMatrix(CurrentRow, colDS), 2, 5);
					hold = FCConvert.ToInt32(Math.Round(Conversion.Val(hold1)));
					if (hold > 0 && hold < vs1.Rows)
					{
						vs1.TextMatrix(CurrentRow, colCD, vs1.TextMatrix(hold, colCD));
						vs1.TextMatrix(CurrentRow, colQT, vs1.TextMatrix(hold, colQT));
						vs1.TextMatrix(CurrentRow, colDS, vs1.TextMatrix(hold, colDS));
						vs1.TextMatrix(CurrentRow, colMO, vs1.TextMatrix(hold, colMO));
						vs1.TextMatrix(CurrentRow, colDY, vs1.TextMatrix(hold, colDY));
						vs1.TextMatrix(CurrentRow, colCS, vs1.TextMatrix(hold, colCS));
						vs1.TextMatrix(CurrentRow, colFC, vs1.TextMatrix(hold, colFC));
						vs1.TextMatrix(CurrentRow, colYFA, vs1.TextMatrix(hold, colYFA));
						vs1.TextMatrix(CurrentRow, colLP, vs1.TextMatrix(hold, colLP));
						vs1.TextMatrix(CurrentRow, colMS, vs1.TextMatrix(hold, colMS));
						vs1.TextMatrix(CurrentRow, colMR, vs1.TextMatrix(hold, colMR));
						vs1.TextMatrix(CurrentRow, colLD, vs1.TextMatrix(hold, colLD));
						vs1.TextMatrix(CurrentRow, colLF, vs1.TextMatrix(hold, colLF));
						vs1.TextMatrix(CurrentRow, colreplacingvalue, vs1.TextMatrix(hold, colreplacingvalue));
					}
				}
			}
		}

		private void vs1_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
			string strToolTip;
			int lngMouseCol;
			strToolTip = "";
			lngMouseCol = e.ColumnIndex;
			//FC:FINAL:MSH - added an extra comparing to avoid exceptions if index less than 0
			if (e.ColumnIndex > -1 && e.RowIndex > -1)
			{
				DataGridViewCell cell = vs1[e.ColumnIndex, e.RowIndex];
				if (lngMouseCol == colCD)
				{
					strToolTip = "Code used to indicate the property category this item belongs to.";
				}
				else if (lngMouseCol == colCS)
				{
					strToolTip = "Original Cost of the leased equipment if known";
				}
				else if (lngMouseCol == colBETE)
				{
					strToolTip = "Year last exemption was claimed";
				}
				else if (lngMouseCol == colDS)
				{
					strToolTip = "Description of this item";
				}
				else if (lngMouseCol == colFC)
				{
					strToolTip = "Percent multiplier to adjust value of the item";
				}
				else if (lngMouseCol == colDY)
				{
					strToolTip = "Physical percent good. Leave this 0 to have the percent good calculated";
				}
				else if (lngMouseCol == colLF)
				{
					strToolTip = "Account this item is leased from.";
				}
				else if (lngMouseCol == colMS)
				{
					strToolTip = "Number of months over which the equipment lease in is effect";
				}
				else if (lngMouseCol == colLD)
				{
					strToolTip = "Month and year the lease began";
				}
				else if (lngMouseCol == colLP)
				{
					strToolTip = "Indicate whether this is a straight lease or a purchase lease";
				}
				else if (lngMouseCol == colMR)
				{
					strToolTip = "Amount of the monthly lease payment";
				}
				else if (lngMouseCol == colQT)
				{
					strToolTip = "The quantity of this item.  Multiplied by cost to determine total cost";
				}
				else if (lngMouseCol == colRB)
				{
					strToolTip = "Enter an asterisk if this item is reimbursable";
				}
				else if (lngMouseCol == colYFA)
				{
					strToolTip = "The year this item was first assessed for.  Used for determining new valuation for a particular year";
				}
				else if (lngMouseCol == colreplacingvalue)
				{
					strToolTip = "Value of equipment this equipment replaces.  Used in determining new valuation for a particular year.";
				}
				cell.ToolTipText = strToolTip;
			}
		}

		private void vs1_RowColChange(object sender, System.EventArgs e)
		{
			CurrentRow = vs1.Row;
			CurrentCol = vs1.Col;
			// If vs1.Col = colRB Then
			// vs1.Editable = False
			// Exit Sub
			// End If
		}

		private void SetColumnWidths()
		{
			// Dim intScreenFactor As Double
			int GridWidth;
			if (vs1.Rows > 1)
			{
				vs1.RowHeight(0, 2 * vs1.RowHeight(1));
			}
			GridWidth = vs1.WidthOriginal;
			vs1.ColWidth(colLN, FCConvert.ToInt32(0.05 * GridWidth));
			//FC:FINAL:MSH - i.issue #1325: change width of column for displaying selected value in combobox
			//vs1.ColWidth(colRB, FCConvert.ToInt32(0.03 * GridWidth));
			vs1.ColWidth(colRB, FCConvert.ToInt32(0.035 * GridWidth));
			vs1.ColWidth(colCD, FCConvert.ToInt32(0.03 * GridWidth));
			vs1.ColWidth(colQT, FCConvert.ToInt32(0.039 * GridWidth));
			vs1.ColWidth(colDS, FCConvert.ToInt32(0.2 * GridWidth));
			vs1.ColWidth(colMO, 0);
			vs1.ColWidth(colBETE, FCConvert.ToInt32(0.045 * GridWidth));
			vs1.ColWidth(colYearsClaimed, FCConvert.ToInt32(0.045 * GridWidth));
			vs1.ColWidth(colYFA, FCConvert.ToInt32(0.055 * GridWidth));
			vs1.ColWidth(colreplacingvalue, FCConvert.ToInt32(0.1 * GridWidth));
			vs1.ColWidth(colCS, FCConvert.ToInt32(0.09 * GridWidth));
			vs1.ColWidth(colLD, FCConvert.ToInt32(0.052 * GridWidth));
			vs1.ColWidth(colMS, FCConvert.ToInt32(0.046 * GridWidth));
			vs1.ColWidth(colLP, FCConvert.ToInt32(0.041 * GridWidth));
			vs1.ColWidth(colMR, FCConvert.ToInt32(0.09 * GridWidth));
			vs1.ColWidth(colDY, FCConvert.ToInt32(0.041 * GridWidth));
			vs1.ColWidth(colFC, FCConvert.ToInt32(0.041 * GridWidth));
			vs1.ColWidth(colKY, 0);
			// 0  hidden key value
			vs1.ColWidth(colLF, FCConvert.ToInt32(0.06 * GridWidth));
			vs1.ColHidden(colVL, true);
			//FC:FINAL:MSH - i.issue #1269: set correct string format
			//vs1.ColFormat(colCS, "#,###,###,###");
			//vs1.ColFormat(10, "#,###,###,###.##");
			vs1.ColFormat(colCS, "#,###,###,##0.00");
			vs1.ColFormat(10, "#,###,###,##0.00");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, 0, 0, vs1.Cols - 1, 0.66 * vs1.Font.Size);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vs1.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
		}

		private void SetCellAlignments()
		{
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, colLN, vs1.Rows - 1, colLN, 1);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, colRB, vs1.Rows - 1, colRB, 4);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, colCD, vs1.Rows - 1, colCD, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, colQT, vs1.Rows - 1, colQT, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, colDS, vs1.Rows - 1, colDS, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, colCS, vs1.Rows - 1, colCS, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, colLD, vs1.Rows - 1, colLD, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, colMS, vs1.Rows - 1, colMS, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, colLP, vs1.Rows - 1, colLP, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, colMR, vs1.Rows - 1, colMR, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, colDY, vs1.Rows - 1, colDY, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, colFC, vs1.Rows - 1, colFC, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, colVL, vs1.Rows - 1, colVL, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, colYearsClaimed, 0, colYearsClaimed, FCGrid.AlignmentSettings.flexAlignRightCenter);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, colLF, 0, colLF, FCGrid.AlignmentSettings.flexAlignCenterCenter);
		}

		private void Calculate()
		{
			modPPCalculate.Statics.xRow = 0;
			LeasedAccountTotal = 0;
			if (vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, colVL, vs1.Rows - 1, colVL) != modGlobalConstants.Statics.TRIOCOLORBLUE)
			{
				Calculating = true;
				vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, colVL, vs1.Rows - 1, colVL, Color.Blue);
				vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 1, colVL, vs1.Rows - 1, colVL, Color.White);
				// PERFORM CALCULATIONS HERE
				modPPCalculate.Statics.SoundCost = 0;
				modPPCalculate.Statics.SoundValue = 0;
				modPPCalculate.Statics.ReplacementCost = 0;
				modPPCalculate.Statics.ReplacementValue = 0;
				modPPCalculate.Statics.OriginalCost = 0;
				modPPCalculate.Statics.OriginalValue = 0;
				modPPCalculate.Statics.ItemAccount = 0;
				//FCUtils.EraseSafe(modPPCalculate.Statics.Item);
				//FCUtils.EraseSafe(modPPCalculate.Statics.TotalItem);
				modPPCalculate.GetTrendValues();
				for (modPPCalculate.Statics.xRow = 1; modPPCalculate.Statics.xRow <= vs1.Rows - 1; modPPCalculate.Statics.xRow++)
				{
					if (vs1.TextMatrix(modPPCalculate.Statics.xRow, colCD) != "-")
					{
						// Set Variables equal to CellText from Current Line
						modPPCalculate.Statics.clCode = "0";
						modPPCalculate.Statics.clQuantity = 0;
						modPPCalculate.Statics.clCost = 0;
						modPPCalculate.Statics.clLeaseDate = FCConvert.ToString(0);
						modPPCalculate.Statics.clMonths = 0;
						modPPCalculate.Statics.clLP = 0;
						modPPCalculate.Statics.clMonthlyRent = 0;
						modPPCalculate.Statics.clPercentGood = 0;
						modPPCalculate.Statics.LeasedDepreciation = 0;
						modPPCalculate.Statics.Work1 = 0;
						modPPCalculate.Statics.clCode = vs1.TextMatrix(modPPCalculate.Statics.xRow, colCD);
						modPPCalculate.Statics.clQuantity = FCConvert.ToInt32(vs1.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, colQT));
						modPPCalculate.Statics.clCost = FCConvert.ToInt32(vs1.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, colCS));
						modPPCalculate.Statics.clLeaseDate = vs1.TextMatrix(modPPCalculate.Statics.xRow, colLD);
						modPPCalculate.Statics.clMonths = FCConvert.ToInt32(vs1.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, colMS));
						modPPCalculate.Statics.clLP = FCConvert.ToInt32(vs1.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, colLP));
						modPPCalculate.Statics.clMonthlyRent = Conversion.Val(vs1.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, colMR));
						modPPCalculate.Statics.clPercentGood = Conversion.Val(vs1.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, colDY));
						if (modPPCalculate.Statics.clPercentGood != 0)
							modPPCalculate.Statics.clPercentGood *= 100;
						modPPCalculate.Statics.clFactor = Conversion.Val(vs1.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, colFC)) / 100;
						// 
						if (modPPCalculate.Statics.clCost > 0 || modPPCalculate.Statics.clMonthlyRent > 0)
							modPPCalculate.LeasedValue();
						if (modPPCalculate.Statics.Work1 > 1)
						{
							//FC:FINAL:MSH - i.issue #1269: set correct string format
							//vs1.TextMatrix(modPPCalculate.Statics.xRow, colVL, Strings.Format(modPPCalculate.Statics.Work1, "#,###"));
							vs1.TextMatrix(modPPCalculate.Statics.xRow, colVL, Strings.Format(modPPCalculate.Statics.Work1, "#,##0"));
						}
						else
						{
							vs1.TextMatrix(modPPCalculate.Statics.xRow, colVL, "");
						}
						LeasedAccountTotal += FCConvert.ToInt32(modPPCalculate.Statics.Work1);
						if (modPPCalculate.Statics.xRow == vs1.Rows - 1)
						{
							vs1.SubtotalPosition = FCGrid.SubtotalPositionSettings.flexSTBelow;
							//FC:FINAL:MSH - i.issue #1269: set correct string format
							//vs1.Subtotal(FCGrid.SubtotalSettings.flexSTSum, -1, colVL, "$#,###", Color.Red, Color.White, true, " ");
							vs1.Subtotal(FCGrid.SubtotalSettings.flexSTSum, -1, colVL, "$#,##0", Color.Red, Color.White, true, " ");
							vs1.Cell(FCGrid.CellPropertySettings.flexcpText, vs1.Rows - 1, colDS, "Total Value");
						}
					}
					else
					{
						vs1.TextMatrix(modPPCalculate.Statics.xRow, colVL, "");
					}
				}
				// xRow
				//FC:FINAL:MSH - i.issue #1269: set correct string format
				//vs1.Cell(FCGrid.CellPropertySettings.flexcpText, vs1.Rows - 1, colVL, Strings.Format(LeasedAccountTotal, "#,###"));
				vs1.Cell(FCGrid.CellPropertySettings.flexcpText, vs1.Rows - 1, colVL, Strings.Format(LeasedAccountTotal, "#,##0"));
			}
			else
			{
				Calculating = false;
				vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, colVL, vs1.Rows - 1, colVL, Color.White);
				vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 1, colVL, vs1.Rows - 1, colVL, Color.Black);
				vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 1, colVL, vs1.Rows - 1, colVL, "");
				vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 1, colDY, vs1.Rows - 1, colDY, "");
				int xx;
				for (xx = 1; xx <= vs1.Cols - 1; xx++)
				{
					if (FCConvert.ToString(vs1.Cell(FCGrid.CellPropertySettings.flexcpText, vs1.Rows - 1, xx)) == "Total Value")
					{
						vs1.RemoveItem(vs1.Rows - 1);
						break;
					}
				}
				// xx
				LeasedAccountTotal = 0;
			}
		}

		private void GetCostData()
		{
			// vbPorter upgrade warning: xx As object	OnWrite(short, object)
			int xx;
			clsDRWrapper datCostFiles = new clsDRWrapper();
			datCostFiles.OpenRecordset("SELECT * FROM RatioTrends", modPPGN.strPPDatabase);
			datCostFiles.MoveLast();
			datCostFiles.MoveFirst();
			for (xx = 1; xx <= 9; xx++)
			{
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				xx = datCostFiles.Get_Fields("Type");
				// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
				modPPCalculate.Statics.HighPct[xx] = FCConvert.ToInt32(datCostFiles.Get_Fields("High"));
				// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
				modPPCalculate.Statics.LowPct[xx] = FCConvert.ToInt32(datCostFiles.Get_Fields("Low"));
				modPPCalculate.Statics.Exponent[xx] = Conversion.Val(datCostFiles.Get_Fields_Double("Exponent"));
				// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
				modPPCalculate.Statics.DefLife[xx] = FCConvert.ToInt32(datCostFiles.Get_Fields("Life"));
				modPPCalculate.Statics.DepMeth[xx] = FCConvert.ToString(datCostFiles.Get_Fields_String("SD"));
				// TODO Get_Fields: Check the table for the column [Trend] and replace with corresponding Get_Field method
				modPPCalculate.Statics.TrendCode[xx] = FCConvert.ToString(datCostFiles.Get_Fields("Trend"));
				modPPCalculate.Statics.DefDesc[xx] = FCConvert.ToString(datCostFiles.Get_Fields_String("Description"));
				datCostFiles.MoveNext();
			}
			// xx
			datCostFiles.OpenRecordset("SELECT * FROM TrendCapRates");
			datCostFiles.MoveLast();
			datCostFiles.MoveFirst();
			xx = 0;
			while (!datCostFiles.EndOfFile())
			{
				xx += 1;
				modPPCalculate.Statics.CapRate[xx] = datCostFiles.Get_Fields_Double("CapRate");
				datCostFiles.MoveNext();
			}
		}

		private void vs1_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
            const int curOnErrorGoToLabel_Default = 0;
			const int curOnErrorGoToLabel_CancelTag = 1;
			int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
			//FC:FINAL:MSH - i.issue #1269: save correct indexes of the cell
			int row = vs1.GetFlexRowIndex(e.RowIndex);
			int col = vs1.GetFlexColIndex(e.ColumnIndex);
			try
			{
				// Validate Entries Made in Cell
				//Application.DoEvents();
				boolDataChanged = true;
				if (col == colRB)
				{
					// RB
					//FC:FINAL:MSH - i.issue #1325: removed an extra comparing and changing data
					if (Strings.Trim(vs1.EditText) == "*")
					{
						if (vs1.TextMatrix(row, colRB) == "*")
						{
							// MsgBox "A"
							//FC:FINAL:MSH - in original app running this row doesn't give any results (same with i.issue #1268)
							//vs1.EditText = " ";
						}
						else
						{
							// MsgBox "B"
							vs1.EditText = "*";
						}
					}
					else if (Strings.Trim(vs1.EditText) == "P" || Strings.Trim(vs1.EditText) == "p")
					{
						if (Strings.UCase(vs1.TextMatrix(row, colRB)) == "P")
						{
							//FC:FINAL:MSH - in original app running this row doesn't give any results (same with i.issue #1268)
							//vs1.EditText = " ";
						}
						else
						{
							vs1.EditText = "P";
						}
					}
					else if (Strings.Trim(vs1.EditText) != "")
					{
						e.Cancel = true;
					}
				}
				else if (col == colCD)
				{
					// CD
					//FC:FINAL:MSH - issue #1325: remove masked part from entered text for preventing errors and correct comparing
					//string vbPorterVar = vs1.EditText;
					string vbPorterVar = Strings.Trim(vs1.EditText);
					if ((vbPorterVar == "1") || (vbPorterVar == "2") || (vbPorterVar == "3") || (vbPorterVar == "4") || (vbPorterVar == "5") || (vbPorterVar == "6") || (vbPorterVar == "7") || (vbPorterVar == "8") || (vbPorterVar == "9") || (vbPorterVar == "-") || (vbPorterVar == " "))
					{
						// Input OK
					}
					else if ((vbPorterVar == "p") || (vbPorterVar == "P") || (vbPorterVar == "d") || (vbPorterVar == "D"))
					{
						vs1.EditText = vs1.TextMatrix(row - 1, colCD);
					}
					else
					{
						e.Cancel = true;
					}
				}
				else if (col == colLD)
				{
					// Lease Date
					int MO;
					MO = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(vs1.EditText, 1, 2))));
					// If MO < 1 Or MO > 12 Then
					// If MO = 0 Then
					// vs1.EditText = "00/00"
					// End If
					// Cancel = True
					// End If
				}
				else if (col == colMS)
				{
					if (Conversion.Val(Strings.Trim(vs1.EditText)) == 0)
						vs1.EditText = FCConvert.ToString(0);
				}
				else if (col == colFC)
				{
					if (Conversion.Val(Strings.Trim(vs1.EditText)) == 0)
						vs1.EditText = "0";
					if (Conversion.Val(vs1.EditText) == 100)
					{
						vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, row, colFC, vs1.ForeColor);
					}
					else
					{
						vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, row, colFC, modGlobalConstants.Statics.TRIOCOLORRED);
					}
				}
				else if (col == colLP)
				{
					// L=1  P=2
					if (Conversion.Val(Strings.Trim(vs1.EditText)) == 1 || Conversion.Val(Strings.Trim(vs1.EditText)) == 2)
					{
						// Input OK
					}
					else
					{
						vs1.EditText = FCConvert.ToString(2);
					}
				}
				else if (col == colMR)
				{
					// Monthly Rent
					if (Information.IsNumeric(vs1.EditText) == true)
					{
						vOnErrorGoToLabel = curOnErrorGoToLabel_CancelTag;
						/* On Error GoTo CancelTag */
						if (FCConvert.ToSingle(FCConvert.ToDouble(vs1.EditText)) > 99999.99)
						{
							CancelTag:
							;
							MessageBox.Show("Value must be lower than $99,999.99", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							e.Cancel = true;
						}
						else
						{
							// Input OK
						}
					}
					else
					{
						MessageBox.Show("Enter Numeric Values Only", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						vs1.EditText = FCConvert.ToString(0);
					}
				}
				else if (col == colQT)
				{
					if (Conversion.Val(Strings.Trim(vs1.EditText)) == 0)
					{
						vs1.EditText = FCConvert.ToString(1);
					}
				}
				else
				{
					// Input is either OK or Not Allowed
				}
			}
			catch (Exception ex)
			{
				switch (vOnErrorGoToLabel)
				{
					default:
					case curOnErrorGoToLabel_Default:
						// ...
						break;
					case curOnErrorGoToLabel_CancelTag:
						//? goto CancelTag;
						break;
				}
			}
		}

		private void AddNewLeased()
		{
			int lngCRow;
			int intVisibleRows;
			boolDataChanged = true;
			vs1.Rows += 1;
			if (vs1.Rows == 2)
			{
				vs1.Select(vs1.Row, colRB);
			}
			else
			{
				vs1.Select(vs1.Row + 1, colRB);
			}
			if (CurrentRow == 0)
				CurrentRow = 1;
			// 
			// Dim xx As Integer
			// For xx = 1 To 200
			// If NewLeased(xx) = CurrentRow Then GoTo AlreadyAdded
			// If NewLeased(xx) = 0 Then Exit For
			// Next xx
			// AddedRows = AddedRows + 1
			// NewLeased(AddedRows) = CurrentRow
			AlreadyAdded:
			;
			// 
			vs1.RowData(CurrentRow, true);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colLN, Strings.Format(vs1.Row, "0000"));
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colCD, "1");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colQT, "1");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colCS, "0");
			if (modGlobal.Statics.CustomizeStuff.DefaultYearFirstAssessed == 0)
			{
				vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colYFA, DateTime.Today.Year);
			}
			else
			{
				vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colYFA, modGlobal.Statics.CustomizeStuff.DefaultYearFirstAssessed);
			}
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colreplacingvalue, "0");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colLD, "00/00");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colMS, "000");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colLP, "2");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colMR, "0");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colDY, "000");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colFC, "100");
			vs1.TextMatrix(CurrentRow, colYearsClaimed, FCConvert.ToString(0));
			//Application.DoEvents();
			RefreshNumbers();
			lngCRow = vs1.Rows - 1;
			intVisibleRows = FCConvert.ToInt32(FCConvert.ToDouble(vs1.HeightOriginal - vs1.RowHeight(0)) / vs1.RowHeight(1));
			if (lngCRow > intVisibleRows)
			{
				if (vs1.TopRow <= lngCRow - intVisibleRows)
				{
					vs1.TopRow = lngCRow - intVisibleRows + 1;
				}
			}
		}

		private void vs1_DragDrop(object sender, Wisej.Web.DragEventArgs e)
		{
			if (FCConvert.ToString(vs1.Tag) == "")
				return;
			if (vs1.MouseRow == 0)
				return;
			vs1.Redraw = false;
			int xxx;
			int rr;
			rr = vs1.RowSel - vs1.Row;
			if (rr < 0)
				rr *= -1;
			// rr = rr + 1
			if (vs1.MouseRow < vs1.Row)
			{
				for (xxx = rr; xxx >= 1; xxx--)
				{
					vs1.RowPosition(FCConvert.ToInt32(Conversion.Val(vs1.Tag) - 1 + rr), vs1.MouseRow);
				}
				// xxx
			}
			else
			{
				for (xxx = 1; xxx <= rr; xxx++)
				{
					vs1.RowPosition(FCConvert.ToInt32(Conversion.Val(vs1.Tag)), vs1.MouseRow);
				}
				// xxx
			}
			vs1.RowPosition(FCConvert.ToInt32(Conversion.Val(vs1.Tag)), vs1.MouseRow);
			vs1.Redraw = true;
			vs1.Select(1, colLN);
		}

		private void vs1_DragOver(object sender, Wisej.Web.DragEventArgs e)
		{
			if (vs1.MouseRow == 0)
			{
				vs1.DragIcon = FCUtils.LoadPicture("NO_M.CUR");
			}
			else
			{
				// vs1.DragIcon = LoadPicture("H_MOVE.CUR")
			}
		}

		private void vs1_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			vs1.Tag = (System.Object)("");
			if (vs1.MouseRow < 1)
				return;
			if (vs1.MouseCol != colLN)
			{
				if ((vs1.MouseCol != colVL) && (vs1.MouseCol != colRB))
				{
					vs1.Row = vs1.MouseRow;
					vs1.Col = vs1.MouseCol;
					vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vs1.EditCell();
				}
				return;
			}
			if (vs1.MouseRow == 0)
				return;
			if (Calculating == true)
				return;
			if (e.Button != MouseButtons.Right)
			{
				return;
			}
			// If vs1.ColSel < vs1.Cols - 1 Then Exit Sub
			vs1.Row = vs1.MouseRow;
			if (vs1.RowSel == vs1.Rows - 1)
				return;
			vs1.Tag = (System.Object)(Conversion.Str(vs1.Row));
			vs1.Drag(1);
		}

		private void frmPPLeased_DragDrop(object sender, Wisej.Web.DragEventArgs e)
		{
			FileInfo ff = new FileInfo("misc23.ico");
			if (ff.Exists)
				vs1.DragIcon = FCUtils.LoadPicture("misc23.ico");
		}

		private void CheckForRB()
		{
			clsDRWrapper mst = new clsDRWrapper();
			int ii;
			// 
			// Set Recordset
			strSQL = "SELECT * FROM PPMaster WHERE Account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount);
			mst.OpenRecordset(strSQL, modPPGN.strPPDatabase);
			mst.Edit();
			// Start Loop to determine if any rows have Reimbursement Items
			for (ii = 1; ii <= vs1.Rows - 1; ii++)
			{
				if (vs1.TextMatrix(ii, colRB) == "*")
				{
					mst.Set_Fields("RBCode", "Y");
					break;
				}
				else
				{
					mst.Set_Fields("RBCode", "N");
				}
			}
			// ii
			mst.Update();
		}

		private void FillGrid()
		{
			int intCounter = 0;
			ppl.MoveLast();
			ppl.MoveFirst();
			vs1.Rows = ppl.RecordCount() + 1;
			do
			{
				intCounter += 1;
				vs1.TextMatrix(intCounter, colLN, Strings.Format(intCounter, "0000"));
				// TODO Get_Fields: Check the table for the column [rb] and replace with corresponding Get_Field method
				if (fecherFoundation.FCUtils.IsNull(ppl.Get_Fields("rb")) == false)
					// TODO Get_Fields: Check the table for the column [rb] and replace with corresponding Get_Field method
					vs1.TextMatrix(intCounter, colRB, FCConvert.ToString(ppl.Get_Fields("rb")));
				if (fecherFoundation.FCUtils.IsNull(ppl.Get_Fields_String("Cd")) == false)
					vs1.TextMatrix(intCounter, colCD, FCConvert.ToString(ppl.Get_Fields_String("Cd")));
				if (Conversion.Val(ppl.Get_Fields_Int32("exemptyear") + "") > 0)
					vs1.TextMatrix(intCounter, colBETE, FCConvert.ToString(ppl.Get_Fields_Int32("exemptyear")));
				// TODO Get_Fields: Check the table for the column [Quantity] and replace with corresponding Get_Field method
				if (fecherFoundation.FCUtils.IsNull(ppl.Get_Fields("Quantity")) == false)
					// TODO Get_Fields: Check the table for the column [Quantity] and replace with corresponding Get_Field method
					vs1.TextMatrix(intCounter, colQT, Strings.Format(ppl.Get_Fields("Quantity"), "0"));
				if (fecherFoundation.FCUtils.IsNull(ppl.Get_Fields_String("Description")) == false)
					vs1.TextMatrix(intCounter, colDS, FCConvert.ToString(ppl.Get_Fields_String("Description")));
				if (fecherFoundation.FCUtils.IsNull(ppl.Get_Fields_Int16("Month")) == false)
					vs1.TextMatrix(intCounter, colMO, Strings.Format(ppl.Get_Fields_Int16("Month"), "00"));
				// TODO Get_Fields: Check the table for the column [Cost] and replace with corresponding Get_Field method
				if (fecherFoundation.FCUtils.IsNull(ppl.Get_Fields("Cost")) == false)
					//FC:FINAL:MSH - i.issue #1269: set correct string format
					//vs1.TextMatrix(intCounter, colCS, Strings.Format(ppl.Get_Fields("Cost"), "#,###"));
					// TODO Get_Fields: Check the table for the column [Cost] and replace with corresponding Get_Field method
					vs1.TextMatrix(intCounter, colCS, Strings.Format(ppl.Get_Fields("Cost"), "#,##0"));
				//vs1.TextMatrix(intCounter, colreplacingvalue, Strings.Format(FCConvert.ToString(Conversion.Val(ppl.Get_Fields("amountreplacing") + "")), "#,###"));
				vs1.TextMatrix(intCounter, colreplacingvalue, Strings.Format(FCConvert.ToString(Conversion.Val(ppl.Get_Fields_Int32("amountreplacing") + "")), "#,##0"));
				if (Conversion.Val(ppl.Get_Fields_Int32("yearfirstassessed") + "") > 0)
				{
					vs1.TextMatrix(intCounter, colYFA, FCConvert.ToString(ppl.Get_Fields_Int32("yearfirstassessed")));
				}
				vs1.TextMatrix(intCounter, colYearsClaimed, FCConvert.ToString(Conversion.Val(ppl.Get_Fields_Int32("yearsclaimed"))));
				if (fecherFoundation.FCUtils.IsNull(ppl.Get_Fields_String("LeaseDate")) == false)
				{
					if (Strings.Mid(FCConvert.ToString(ppl.Get_Fields_String("LeaseDate")), 3, 1) == "/")
					{
						vs1.TextMatrix(intCounter, colLD, FCConvert.ToString(ppl.Get_Fields_String("LeaseDate")));
					}
					else
					{
						vs1.TextMatrix(intCounter, colLD, Strings.Format(ppl.Get_Fields_String("LeaseDate"), "00/00"));
					}
				}
				if (fecherFoundation.FCUtils.IsNull(ppl.Get_Fields_Int16("MOS")) == false)
					vs1.TextMatrix(intCounter, colMS, FCConvert.ToString(ppl.Get_Fields_Int16("MOS")));
				if (fecherFoundation.FCUtils.IsNull(ppl.Get_Fields_Int16("L1P2")) == false)
					vs1.TextMatrix(intCounter, colLP, Strings.Format(ppl.Get_Fields_Int16("L1P2"), "0"));
				if (fecherFoundation.FCUtils.IsNull(ppl.Get_Fields_Decimal("MonthlyRent")) == false)
					//FC:FINAL:MSH - i.issue #1269: set correct string format
					//vs1.TextMatrix(intCounter, colMR, Strings.Format(ppl.Get_Fields("MonthlyRent"), "#,###.00"));
					vs1.TextMatrix(intCounter, colMR, Strings.Format(ppl.Get_Fields_Decimal("MonthlyRent"), "#,##0.00"));
				if (fecherFoundation.FCUtils.IsNull(ppl.Get_Fields_Int16("DEPYRS")) == false)
					vs1.TextMatrix(intCounter, colDY, Strings.Format(ppl.Get_Fields_Int16("DEPYRS"), "000"));
				if (fecherFoundation.FCUtils.IsNull(ppl.Get_Fields_Int16("Fctr")) == false)
					vs1.TextMatrix(intCounter, colFC, Strings.Format(ppl.Get_Fields_Int16("Fctr"), "0"));
				if (Conversion.Val(vs1.TextMatrix(intCounter, colFC)) != 100)
				{
					vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, intCounter, colFC, modGlobalConstants.Statics.TRIOCOLORRED);
				}
				if (Conversion.Val(ppl.Get_Fields_Int32("LeasedFrom") + "") > 0)
				{
					vs1.TextMatrix(intCounter, colLF, FCConvert.ToString(ppl.Get_Fields_Int32("leasedfrom")));
				}
				vs1.TextMatrix(intCounter, colKY, FCConvert.ToString(ppl.Get_Fields_Int32("id")));
				// TODO Get_Fields: Check the table for the column [Value] and replace with corresponding Get_Field method
				vs1.TextMatrix(intCounter, colVL, FCConvert.ToString(Conversion.Val(ppl.Get_Fields("Value"))));
				ppl.MoveNext();
			}
			while (!ppl.EndOfFile());
			for (intCounter = 1; intCounter <= vs1.Rows - 1; intCounter++)
			{
				if (Conversion.Val(vs1.TextMatrix(intCounter, colLN)) == 0)
					vs1.TextMatrix(intCounter, colLN, Strings.Format(intCounter, "0000"));
			}
			// intCounter
		}

		private void FillMasterLabels()
		{
			clsDRWrapper rsMaster = new clsDRWrapper();
			string strMasterJoin;
			strMasterJoin = modPPGN.GetMasterJoin();
			lblAccount.Text = FCConvert.ToString(modPPGN.Statics.CurrentAccount);
			// Call rsMaster.OpenRecordset("SELECT * FROM PPMaster WHERE Account = " & CurrentAccount, modPPGN.strPPDatabase)
			rsMaster.OpenRecordset(strMasterJoin + " WHERE Account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount), modPPGN.strPPDatabase);
			string strLOC = "";
			if (rsMaster.EndOfFile())
				return;
			// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
			if (fecherFoundation.FCUtils.IsNull(rsMaster.Get_Fields("StreetNumber")) == false)
			{
				// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
				strLOC = FCConvert.ToString(rsMaster.Get_Fields("StreetNumber"));
				strLOC += " ";
			}
			if (fecherFoundation.FCUtils.IsNull(rsMaster.Get_Fields_String("Street")) == false)
			{
				strLOC += rsMaster.Get_Fields_String("Street");
			}
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			lblAccount.Text = Strings.Format(rsMaster.Get_Fields("Account"), "00000");
			lblName.Text = FCConvert.ToString(rsMaster.Get_Fields_String("Name"));
		}

		public void HandlePartialPermission(ref int lngFuncID)
		{
			// takes a function number as a parameter
			// Then gets all the child functions that belong to it and
			// disables the appropriate controls
			clsDRWrapper clsChildList = new clsDRWrapper();
			// Dim dcTemp As New clsdrwrapper
			string strPerm = "";
			int lngChild = 0;
			Control ctl = new Control();
			// Set clsChildList = New clsdrwrapper
			modGlobalConstants.Statics.clsSecurityClass.Get_Children(ref clsChildList, ref lngFuncID);
			while (!clsChildList.EndOfFile())
			{
				lngChild = FCConvert.ToInt32(clsChildList.GetData("childid"));
				strPerm = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(lngChild));
				switch (lngChild)
				{
					case modSecurity.ITEMIZEDSCREENMENU:
						{
							if (strPerm == "F")
							{
								cmdItemized.Enabled = true;
							}
							else
							{
								cmdItemized.Enabled = false;
							}
							break;
						}
					case modSecurity.EDITLEASEDSCREENMENU:
						{
							if (strPerm == "F")
							{
								mnuAdd.Enabled = true;
								mnuAddMultipleRows.Enabled = true;
								mnuRemove.Enabled = true;
							}
							else
							{
								mnuAdd.Enabled = false;
								mnuAddMultipleRows.Enabled = false;
								mnuRemove.Enabled = false;
								vs1.Enabled = false;
							}
							break;
						}
				}
				//end switch
				clsChildList.MoveNext();
			}
		}

		private void RefreshNumbers()
		{
			int x;
			for (x = 1; x <= vs1.Rows - 1; x++)
			{
				if (Conversion.Val(vs1.TextMatrix(x, 0)) != x)
				{
					vs1.RowData(x, true);
				}
				vs1.TextMatrix(x, 0, Strings.Format(x, "0000"));
			}
			// x
			vs1.Refresh();
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		private bool NewSaveLeased()
		{
			bool NewSaveLeased = false;
			clsDRWrapper clsSave = new clsDRWrapper();
			string strSQL = "";
			string strCols;
			int x;
			string strTemp = "";
			int lngLastRowChecked;
			// Dim boolReimbursable As Boolean
			NewSaveLeased = false;
			try
			{
				// On Error GoTo ErrorHandler
				lblScreenTitle.Visible = true;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				// colLN = 0
				// colRB = 1
				// colCD = 2
				// colQT = 3
				// colDS = 4
				// colMO = 5
				// colCS = 6
				// colLD = 7
				// colMS = 8
				// colLP = 9
				// colMR = 10
				// colDY = 11
				// colFC = 12
				// colKY = 13
				// colVL = 14
				clsSave.Execute("update status set changeddata = '" + FCConvert.ToString(DateTime.Now) + "'", "twpp0000.vb1");
				if ((vs1.Row > 0) && (vs1.Col >= 0))
				{
					vs1.Row = 0;
				}
				strCols = "(Account,Line,RB,CD,quantity,description,month,cost,leasedate,mos,l1p2,monthlyrent,depyrs,fctr,[value],leasedfrom)";
				if (GridDeleted.Rows > 0)
				{
					modGlobalFunctions.AddCYAEntry_26("PP", "Deleted Leased Line(s)", "Account " + FCConvert.ToString(modPPGN.Statics.CurrentAccount));
					for (x = 0; x <= GridDeleted.Rows - 1; x++)
					{
						clsSave.Execute("delete from ppleased where id = " + FCConvert.ToString(Conversion.Val(GridDeleted.TextMatrix(x, 0))), "twpp0000.vb1");
					}
					// x
					GridDeleted.Rows = 0;
				}
				// Call clsSave.Execute("delete from ppleased where account = " & CurrentAccount, "twpp0000.vb1")
				// boolReimbursable = False
				lngLastRowChecked = 1;
				// only save changed rows
				x = vs1.FindRow(true, lngLastRowChecked);
				while (x > 0)
				{
					// For x = 1 To vs1.Rows - 1
					// DoEvents
					this.Refresh();
					// If vs1.TextMatrix(x, colRB) = "*" Then
					// boolReimbursable = True
					// End If
					lngLastRowChecked = x;
					lblScreenTitle.Text = "Saving Line: " + FCConvert.ToString(x);
					// check if this is a new on or an update
					if (Conversion.Val(vs1.TextMatrix(x, colKY)) > 0)
					{
						// update
						strSQL = "update ppleased set ";
						strSQL += "line = " + FCConvert.ToString(x);
						strSQL += ",rb = '" + vs1.TextMatrix(x, colRB) + "'";
						strSQL += ",exemptyear = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colBETE)));
						strSQL += ",yearsclaimed = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colYearsClaimed)));
						strSQL += ",cd = '" + vs1.TextMatrix(x, colCD) + "'";
						strSQL += ",quantity = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colQT)));
						strSQL += ",description = '" + Strings.Trim(vs1.TextMatrix(x, colDS)) + "'";
						strSQL += ",month = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colMO)));
						if (Strings.Trim(vs1.TextMatrix(x, colCS)) == string.Empty)
						{
							strSQL += ",cost = 0";
						}
						else
						{
							strSQL += ",cost = " + FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(vs1.TextMatrix(x, colCS) + "")));
						}
						if (Strings.Trim(vs1.TextMatrix(x, colreplacingvalue)) == string.Empty)
						{
							strSQL += ",amountreplacing = 0";
						}
						else
						{
							strSQL += ",amountreplacing = " + FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(vs1.TextMatrix(x, colreplacingvalue))));
						}
						strSQL += ",yearfirstassessed = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colYFA)));
						strSQL += ",leasedate = '" + vs1.TextMatrix(x, colLD) + "'";
						strSQL += ",mos = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colMS)));
						strSQL += ",l1p2 = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colLP)));
						strSQL += ",monthlyrent = " + FCConvert.ToString(FCConvert.ToSingle(FCConvert.ToDouble(vs1.TextMatrix(x, colMR))));
						strSQL += ",depyrs = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colDY)));
						strSQL += ",fctr = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colFC)));
						if (Strings.Trim(vs1.TextMatrix(x, colVL) + "") == string.Empty)
						{
							strSQL += ",[value] = 0";
						}
						else
						{
							strSQL += ",[value] = " + FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(vs1.TextMatrix(x, colVL))));
						}
						strSQL += ",leasedfrom = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colLF)));
						strSQL += " where id = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colKY)));
						if (!clsSave.Execute(strSQL, "twpp0000.vb1"))
						{
							strTemp = Strings.Trim(vs1.TextMatrix(x, colDS) + "");
							modGlobal.escapequote(ref strTemp);
							strSQL = "update ppleased set ";
							strSQL += "line = " + FCConvert.ToString(x);
							strSQL += ",rb = '" + vs1.TextMatrix(x, colRB) + "'";
							strSQL += ",exemptyear = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colBETE)));
							strSQL += ",yearsclaimed = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colYearsClaimed)));
							strSQL += ",cd = '" + vs1.TextMatrix(x, colCD) + "'";
							strSQL += ",quantity = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colQT)));
							strSQL += ",description = '" + strTemp + "'";
							strSQL += ",month = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colMO)));
							if (Strings.Trim(vs1.TextMatrix(x, colreplacingvalue)) == string.Empty)
							{
								strSQL += ",amountreplacing = 0";
							}
							else
							{
								strSQL += ",amountreplacing = " + FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(vs1.TextMatrix(x, colreplacingvalue))));
							}
							strSQL += ",yearfirstassessed = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colYFA)));
							if (Strings.Trim(vs1.TextMatrix(x, colCS)) == string.Empty)
							{
								strSQL += ",cost = 0";
							}
							else
							{
								strSQL += ",cost = " + FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(vs1.TextMatrix(x, colCS) + "")));
							}
							strSQL += ",leasedate = '" + vs1.TextMatrix(x, colLD) + "'";
							strSQL += ",mos = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colMS)));
							strSQL += ",l1p2 = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colLP)));
							strSQL += ",monthlyrent = " + FCConvert.ToString(FCConvert.ToSingle(FCConvert.ToDouble(vs1.TextMatrix(x, colMR))));
							strSQL += ",depyrs = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colDY)));
							strSQL += ",fctr = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colFC)));
							if (Strings.Trim(vs1.TextMatrix(x, colVL) + "") == string.Empty)
							{
								strSQL += ",[value] = 0";
							}
							else
							{
								strSQL += ",[value] = " + FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(vs1.TextMatrix(x, colVL))));
							}
							strSQL += ",leasedfrom = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colLF)));
							strSQL += " where id = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colKY)));
							if (!clsSave.Execute(strSQL, "twpp0000.vb1"))
							{
								MessageBox.Show("Error saving line " + FCConvert.ToString(x) + ".  Please try again before quitting, or information may be lost.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
								NewSaveLeased = false;
								return NewSaveLeased;
							}
						}
					}
					else
					{
						// new item
						clsSave.OpenRecordset("select * from ppleased where id = -1", "twpp0000.vb1");
						// can't ever match anything
						clsSave.AddNew();
						clsSave.Set_Fields("account", modPPGN.Statics.CurrentAccount);
						clsSave.Set_Fields("line", x);
						clsSave.Set_Fields("rb", vs1.TextMatrix(x, colRB));
						clsSave.Set_Fields("exemptyear", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colBETE))));
						clsSave.Set_Fields("cd", vs1.TextMatrix(x, colCD));
						clsSave.Set_Fields("quantity", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colQT))));
						clsSave.Set_Fields("description", Strings.Trim(vs1.TextMatrix(x, colDS)));
						clsSave.Set_Fields("month", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colMO))));
						if (Strings.Trim(vs1.TextMatrix(x, colCS)) == string.Empty)
						{
							clsSave.Set_Fields("cost", 0);
						}
						else
						{
							clsSave.Set_Fields("cost", FCConvert.ToInt32(FCConvert.ToDouble(vs1.TextMatrix(x, colCS))));
						}
						clsSave.Set_Fields("leasedate", vs1.TextMatrix(x, colLD));
						clsSave.Set_Fields("mos", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colMS))));
						clsSave.Set_Fields("l1p2", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colLP))));
						if (Strings.Trim(vs1.TextMatrix(x, colreplacingvalue)) == string.Empty)
						{
							clsSave.Set_Fields("amountreplacing", 0);
						}
						else
						{
							clsSave.Set_Fields("amountreplacing", FCConvert.ToInt32(FCConvert.ToDouble(vs1.TextMatrix(x, colreplacingvalue))));
						}
						clsSave.Set_Fields("yearsclaimed", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colYearsClaimed))));
						clsSave.Set_Fields("yearfirstassessed", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colYFA))));
						clsSave.Set_Fields("monthlyrent", FCConvert.ToSingle(FCConvert.ToDouble(vs1.TextMatrix(x, colMR))));
						clsSave.Set_Fields("depyrs", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colDY))));
						clsSave.Set_Fields("fctr", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colFC))));
						if (Strings.Trim(vs1.TextMatrix(x, colVL) + "") == string.Empty)
						{
							clsSave.Set_Fields("value", 0);
						}
						else
						{
							clsSave.Set_Fields("value", FCConvert.ToInt32(FCConvert.ToDouble(vs1.TextMatrix(x, colVL))));
						}
						clsSave.Set_Fields("leasedfrom", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colLF))));
						clsSave.Update();
						vs1.TextMatrix(x, colKY, FCConvert.ToString(clsSave.Get_Fields_Int32("id")));
					}
					vs1.RowData(x, false);
					// Next x
					x = vs1.FindRow(true, lngLastRowChecked);
				}
				NewSaveLeased = true;
				boolDataChanged = false;
				MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				ppl.OpenRecordset("select * from ppleased where account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount) + " order by line", modPPGN.strPPDatabase);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				lblScreenTitle.Visible = false;
				return NewSaveLeased;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lblScreenTitle.Visible = false;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In save itemized" + "\r\n" + "There was an error during the save process" + "\r\n" + "You should try saving again before exiting, otherwise the itemized data may be lost.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return NewSaveLeased;
		}
		//FC:FINAL:CHN: Delete underline from mask.
		private void vs1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
            e.Control.KeyDown -= new KeyEventHandler(this.vs1_KeyDownEdit);
            e.Control.KeyDown += new KeyEventHandler(this.vs1_KeyDownEdit);

            var editingControl = e.Control as FCMaskedTextBox;
			if (editingControl == null)
				return;
			editingControl.PromptChar = ' ';
		}

        private void mnuViewGroupInfo_Click(object sender, EventArgs e)
        {
            clsDRWrapper clsLoad = new clsDRWrapper();
            // vbPorter upgrade warning: intRes As short, int --> As DialogResult
            DialogResult intRes;
            int lngGroupNumber = 0;
            clsLoad.OpenRecordset("select * from moduleassociation where module = 'PP' and account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount), "CentralData");
            if (!clsLoad.EndOfFile())
            {
                lngGroupNumber = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("groupnumber"));
                clsLoad.OpenRecordset("select * from groupmaster where id = " + FCConvert.ToString(lngGroupNumber), "CentralData");
                if (!clsLoad.EndOfFile())
                {
                    //! Load frmGroup;
                    frmGroup.InstancePtr.Init(lngGroupNumber);
                }
                else
                {
                    MessageBox.Show("This account is associated with a group but the group does not exist.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
            }
            else
            {
                intRes = MessageBox.Show("This account is not in a group." + "\r\n" + "Would you like to create a new group?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (intRes == DialogResult.Yes)
                {
                    //! Load frmGroup;
                    frmGroup.InstancePtr.Init(0);
                }
            }
		}
    }
}
