﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for MDIParent.
	/// </summary>
	partial class MDIParent : BaseForm
	{
		public fecherFoundation.FCPictureBox PicArchive;
		public fecherFoundation.FCTextBox txtCommDest;
		public fecherFoundation.FCPictureBox imgArchive;
		public FCCommonDialog CommonDialog1_Open;
		public FCCommonDialog CommonDialog1;
		public Wisej.Web.ColorDialog CommonDialog1_Color;
		public FCCommonDialog CommonDialog1_Printer;
		public Wisej.Web.StatusBar StatusBar1;
		private Wisej.Web.StatusBarPanel StatusBar1_Panel1;
		private Wisej.Web.StatusBarPanel StatusBar1_Panel2;
		private Wisej.Web.StatusBarPanel StatusBar1_Panel3;
		private Wisej.Web.StatusBarPanel StatusBar1_Panel4;
		public fecherFoundation.FCGrid GRID;
		public Wisej.Web.ImageList ImageList1;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuMenucolor;
		public fecherFoundation.FCToolStripMenuItem mnuFileCentralParties;
		public fecherFoundation.FCToolStripMenuItem mnuFExit;
		public fecherFoundation.FCToolStripMenuItem mnuForms;
		public fecherFoundation.FCToolStripMenuItem mnuPrintForm;
		public fecherFoundation.FCToolStripMenuItem mnuOMaxForms;
		public fecherFoundation.FCToolStripMenuItem mnuHelp;
		public fecherFoundation.FCToolStripMenuItem mnuRedbookHelp;
		public fecherFoundation.FCToolStripMenuItem mnuBudgetaryHelp;
		public fecherFoundation.FCToolStripMenuItem mnuCashReceiptsHelp;
		public fecherFoundation.FCToolStripMenuItem mnuClerkHelp;
		public fecherFoundation.FCToolStripMenuItem mnuCodeEnforcementHelp;
		public fecherFoundation.FCToolStripMenuItem mnuEnhanced911Help;
		public fecherFoundation.FCToolStripMenuItem mnuFixedAssetsHelp;
		public fecherFoundation.FCToolStripMenuItem mnuGeneralEntryHelp;
		public fecherFoundation.FCToolStripMenuItem mnuMotorVehicleRegistrationHelp;
		public fecherFoundation.FCToolStripMenuItem mnuPayrollHelp;
		public fecherFoundation.FCToolStripMenuItem mnuPersonalPropertyHelp;
		public fecherFoundation.FCToolStripMenuItem mnuRealEstateHelp;
		public fecherFoundation.FCToolStripMenuItem mnuTaxBillingHelp;
		public fecherFoundation.FCToolStripMenuItem mnuTaxCollectionsHelp;
		public fecherFoundation.FCToolStripMenuItem mnuUtilityBillingHelp;
		public fecherFoundation.FCToolStripMenuItem mnuVoterRegistrationHelp;
		public fecherFoundation.FCToolStripMenuItem mnuHelpSeparator;
		public fecherFoundation.FCToolStripMenuItem mnuHAbout;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(MDIParent));
			this.PicArchive = new fecherFoundation.FCPictureBox();
			this.txtCommDest = new fecherFoundation.FCTextBox();
			this.imgArchive = new fecherFoundation.FCPictureBox();
			this.CommonDialog1_Open = new FCCommonDialog();
			this.CommonDialog1 = new FCCommonDialog();
			this.CommonDialog1_Color = new Wisej.Web.ColorDialog();
			this.CommonDialog1_Printer = new FCCommonDialog();
			this.StatusBar1 = new Wisej.Web.StatusBar();
			this.StatusBar1_Panel1 = new Wisej.Web.StatusBarPanel();
			this.StatusBar1_Panel2 = new Wisej.Web.StatusBarPanel();
			this.StatusBar1_Panel3 = new Wisej.Web.StatusBarPanel();
			this.StatusBar1_Panel4 = new Wisej.Web.StatusBarPanel();
			this.GRID = new fecherFoundation.FCGrid();
			this.components = new System.ComponentModel.Container();
			this.ImageList1 = new Wisej.Web.ImageList();
			this.MainMenu1 = new Wisej.Web.MainMenu();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuMenucolor = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileCentralParties = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuForms = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintForm = new fecherFoundation.FCToolStripMenuItem();
			this.mnuOMaxForms = new fecherFoundation.FCToolStripMenuItem();
			this.mnuHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuRedbookHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuBudgetaryHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCashReceiptsHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuClerkHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCodeEnforcementHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuEnhanced911Help = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFixedAssetsHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuGeneralEntryHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuMotorVehicleRegistrationHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPayrollHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPersonalPropertyHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuRealEstateHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuTaxBillingHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuTaxCollectionsHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuUtilityBillingHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuVoterRegistrationHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuHelpSeparator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuHAbout = new fecherFoundation.FCToolStripMenuItem();
			this.SuspendLayout();
			//
			// PicArchive
			//
			this.PicArchive.Controls.Add(this.txtCommDest);
			this.PicArchive.Controls.Add(this.imgArchive);
			this.PicArchive.Name = "PicArchive";
			this.PicArchive.TabIndex = 2;
			this.PicArchive.Location = new System.Drawing.Point(168, 24);
			this.PicArchive.Size = new System.Drawing.Size(712, 423);
			this.PicArchive.BorderStyle = Wisej.Web.BorderStyle.Solid;
			this.PicArchive.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(255)), ((System.Byte)(255)));
			this.PicArchive.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left));
			//
			// txtCommDest
			//
			this.txtCommDest.Name = "txtCommDest";
			this.txtCommDest.Visible = false;
			this.txtCommDest.TabIndex = 3;
			this.txtCommDest.Location = new System.Drawing.Point(0, 324);
			this.txtCommDest.Size = new System.Drawing.Size(27, 40);
			this.txtCommDest.Text = "Text1";
			this.txtCommDest.BackColor = System.Drawing.SystemColors.Window;
			//
			// imgArchive
			//
			this.imgArchive.Name = "imgArchive";
			this.imgArchive.Visible = false;
			this.imgArchive.Location = new System.Drawing.Point(14, 40);
			this.imgArchive.Size = new System.Drawing.Size(291, 73);
			this.imgArchive.BorderStyle = Wisej.Web.BorderStyle.None;
			//this.imgArchive.BackColor = System.Drawing.SystemColors.Control;
			this.imgArchive.Image = ((System.Drawing.Image)(resources.GetObject("imgArchive.Image")));
			this.imgArchive.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			//
			// CommonDialog1
			//
			//
			// StatusBar1
			//
			this.StatusBar1.Panels.AddRange(new Wisej.Web.StatusBarPanel[] {
				this.StatusBar1_Panel1,
				this.StatusBar1_Panel2,
				this.StatusBar1_Panel3,
				this.StatusBar1_Panel4
			});
			this.StatusBar1.Name = "StatusBar1";
			this.StatusBar1.TabIndex = 0;
			this.StatusBar1.Location = new System.Drawing.Point(0, 447);
			this.StatusBar1.Size = new System.Drawing.Size(599, 19);
			this.StatusBar1.ShowPanels = true;
			this.StatusBar1.SizingGrip = false;
			//this.StatusBar1.Font = new System.Drawing.Font("MS Sans Serif", 8.25F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Bold), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// Panel1
			//
			this.StatusBar1_Panel1.Text = "TRIO Software Corporation";
			this.StatusBar1_Panel1.AutoSize = Wisej.Web.StatusBarPanelAutoSize.Contents;
			this.StatusBar1_Panel1.Width = 166;
			this.StatusBar1_Panel1.MinWidth = 142;
			//
			// Panel2
			//
			this.StatusBar1_Panel2.Text = "";
			this.StatusBar1_Panel2.Alignment = Wisej.Web.HorizontalAlignment.Right;
			this.StatusBar1_Panel2.BorderStyle = Wisej.Web.StatusBarPanelBorderStyle.None;
			this.StatusBar1_Panel2.AutoSize = Wisej.Web.StatusBarPanelAutoSize.Spring;
			this.StatusBar1_Panel2.Width = 265;
			//
			// Panel3
			//
			this.StatusBar1_Panel3.Text = "";
			this.StatusBar1_Panel3.Alignment = Wisej.Web.HorizontalAlignment.Right;
			this.StatusBar1_Panel3.AutoSize = Wisej.Web.StatusBarPanelAutoSize.Contents;
			this.StatusBar1_Panel3.Width = 77;
			this.StatusBar1_Panel3.MinWidth = 67;
			//
			// Panel4
			//
			this.StatusBar1_Panel4.Text = "";
			this.StatusBar1_Panel4.Alignment = Wisej.Web.HorizontalAlignment.Right;
			this.StatusBar1_Panel4.Width = 67;
			this.StatusBar1_Panel4.MinWidth = 67;
			//
			// GRID
			//
			this.GRID.Name = "GRID";
			this.GRID.Enabled = true;
			this.GRID.TabIndex = 1;
			this.GRID.Location = new System.Drawing.Point(0, 24);
			this.GRID.Size = new System.Drawing.Size(168, 423);
			//this.GRID._ConvInfo = 1;
			//this.GRID.MousePointer = 0;
			//this.GRID.BackColorFixed = 12632256;
			//this.GRID.ForeColorFixed = -2147483630;
			//this.GRID.BackColorSel = -2147483645;
			//this.GRID.ForeColorSel = -2147483634;
			//this.GRID.BackColorBkg = 12632256;
			//this.GRID.BackColorAlternate = 12632256;
			//this.GRID.GridColor = -2147483633;
			//this.GRID.GridColorFixed = -2147483632;
			//this.GRID.TreeColor = -2147483632;
			//this.GRID.FloodColor = 192;
			//this.GRID.SheetBorder = -2147483642;
			//this.GRID.FocusRect = 0;
			//this.GRID.HighLight = 1;
			this.GRID.AllowSelection = false;
			this.GRID.AllowBigSelection = true;
			this.GRID.AllowUserResizing = 0;
			this.GRID.SelectionMode = 0;
			//this.GRID.GridLinesFixed = 0;
			this.GRID.GridLineWidth = 0;
			this.GRID.Rows = 21;
			this.GRID.Cols = 3;
			this.GRID.FixedRows = 1;
			this.GRID.FixedCols = 0;
			this.GRID.RowHeightMin = 0;
			this.GRID.RowHeightMax = 0;
			//this.GRID.ColWidthMin = 0;
			//this.GRID.ColWidthMax = 0;
			this.GRID.ExtendLastCol = false;
			//this.GRID.FormatString = "";
			this.GRID.ScrollTrack = false;
			//this.GRID.ScrollTips = false;
			this.GRID.MergeCells = 0;
			//this.GRID.MergeCompare = 0;
			//this.GRID.AutoResize = true;
			this.GRID.AutoSizeMode = 0;
			//this.GRID.AutoSearch = 0;
			//this.GRID.MultiTotals = true;
			//this.GRID.SubtotalPosition = 1;
			//this.GRID.OutlineBar = 0;
			//this.GRID.OutlineCol = 0;
			//this.GRID.Ellipsis = 0;
			//this.GRID.ExplorerBar = 0;
			//this.GRID.PicturesOver = false;
			//this.GRID.PictureType = 0;
			this.GRID.TabBehavior = 0;
			//this.GRID.OwnerDraw = 0;
			this.GRID.Editable = FCGrid.EditableSettings.flexEDNone;
			//this.GRID.ShowComboButton = true;
			//this.GRID.TextStyle = 0;
			//this.GRID.TextStyleFixed = 0;
			//this.GRID.OleDragMode = 0;
			//this.GRID.OleDropMode = 0;
			//this.GRID.DataMode = 0;
			//this.GRID.VirtualData = true;
			this.GRID.KeyDownEdit += new KeyEventHandler(this.GRID_KeyDownEdit);
			this.GRID.KeyPressEdit += new KeyPressEventHandler(this.GRID_KeyPressEdit);
			this.GRID.Enter += new System.EventHandler(this.GRID_Enter);
			this.GRID.KeyDown += new KeyEventHandler(this.GRID_KeyDownEvent);
			this.GRID.KeyPress += new KeyPressEventHandler(this.GRID_KeyPressEvent);
			this.GRID.CellMouseDown += new DataGridViewCellMouseEventHandler(this.GRID_MouseDownEvent);
			this.GRID.CellMouseMove += new DataGridViewCellMouseEventHandler(this.GRID_MouseMoveEvent);
			//
			// ImageList1
			//
			this.ImageList1.ImageSize = new System.Drawing.Size(17, 19);
			//this.ImageList1.ColorDepth = Wisej.Web.ColorDepth.Depth8Bit;
			this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(192)), ((System.Byte)(192)));
			this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
			this.ImageList1.Images.SetKeyName(0, "Clear");
			this.ImageList1.Images.SetKeyName(1, "Left");
			this.ImageList1.Images.SetKeyName(2, "Right");
			//
			// mnuFile
			//
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuMenucolor,
				this.mnuFileCentralParties,
				this.mnuFExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			//
			// mnuMenucolor
			//
			this.mnuMenucolor.Name = "mnuMenucolor";
			this.mnuMenucolor.Visible = false;
			this.mnuMenucolor.Text = "Set Menu Color";
			this.mnuMenucolor.Click += new System.EventHandler(this.mnuMenucolor_Click);
			//
			// mnuFileCentralParties
			//
			this.mnuFileCentralParties.Name = "mnuFileCentralParties";
			this.mnuFileCentralParties.Text = "Central Parties";
			this.mnuFileCentralParties.Click += new System.EventHandler(this.mnuFileCentralParties_Click);
			//
			// mnuFExit
			//
			this.mnuFExit.Name = "mnuFExit";
			this.mnuFExit.Text = "Exit";
			this.mnuFExit.Click += new System.EventHandler(this.mnuFExit_Click);
			//
			// mnuForms
			//
			this.mnuForms.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuPrintForm,
				this.mnuOMaxForms
			});
			this.mnuForms.Name = "mnuForms";
			this.mnuForms.Text = "Forms";
			//
			// mnuPrintForm
			//
			this.mnuPrintForm.Name = "mnuPrintForm";
			this.mnuPrintForm.Text = "Print Form(s)";
			this.mnuPrintForm.Click += new System.EventHandler(this.mnuPrintForm_Click);
			//
			// mnuOMaxForms
			//
			this.mnuOMaxForms.Name = "mnuOMaxForms";
			this.mnuOMaxForms.Text = "Maximize Forms";
			this.mnuOMaxForms.Click += new System.EventHandler(this.mnuOMaxForms_Click);
			//
			// mnuHelp
			//
			this.mnuHelp.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuRedbookHelp,
				this.mnuBudgetaryHelp,
				this.mnuCashReceiptsHelp,
				this.mnuClerkHelp,
				this.mnuCodeEnforcementHelp,
				this.mnuEnhanced911Help,
				this.mnuFixedAssetsHelp,
				this.mnuGeneralEntryHelp,
				this.mnuMotorVehicleRegistrationHelp,
				this.mnuPayrollHelp,
				this.mnuPersonalPropertyHelp,
				this.mnuRealEstateHelp,
				this.mnuTaxBillingHelp,
				this.mnuTaxCollectionsHelp,
				this.mnuUtilityBillingHelp,
				this.mnuVoterRegistrationHelp,
				this.mnuHelpSeparator,
				this.mnuHAbout
			});
			this.mnuHelp.Name = "mnuHelp";
			this.mnuHelp.Text = "Help";
			//
			// mnuRedbookHelp
			//
			this.mnuRedbookHelp.Name = "mnuRedbookHelp";
			this.mnuRedbookHelp.Text = "Blue Book";
			this.mnuRedbookHelp.Click += new System.EventHandler(this.mnuRedbookHelp_Click);
			//
			// mnuBudgetaryHelp
			//
			this.mnuBudgetaryHelp.Name = "mnuBudgetaryHelp";
			this.mnuBudgetaryHelp.Text = "Budgetary";
			this.mnuBudgetaryHelp.Click += new System.EventHandler(this.mnuBudgetaryHelp_Click);
			//
			// mnuCashReceiptsHelp
			//
			this.mnuCashReceiptsHelp.Name = "mnuCashReceiptsHelp";
			this.mnuCashReceiptsHelp.Text = "Cash Receipts";
			this.mnuCashReceiptsHelp.Click += new System.EventHandler(this.mnuCashReceiptsHelp_Click);
			//
			// mnuClerkHelp
			//
			this.mnuClerkHelp.Name = "mnuClerkHelp";
			this.mnuClerkHelp.Text = "Clerk";
			this.mnuClerkHelp.Click += new System.EventHandler(this.mnuClerkHelp_Click);
			//
			// mnuCodeEnforcementHelp
			//
			this.mnuCodeEnforcementHelp.Name = "mnuCodeEnforcementHelp";
			this.mnuCodeEnforcementHelp.Text = "Code Enforcement";
			this.mnuCodeEnforcementHelp.Click += new System.EventHandler(this.mnuCodeEnforcementHelp_Click);
			//
			// mnuEnhanced911Help
			//
			this.mnuEnhanced911Help.Name = "mnuEnhanced911Help";
			this.mnuEnhanced911Help.Text = "Enhanced 911";
			this.mnuEnhanced911Help.Click += new System.EventHandler(this.mnuEnhanced911Help_Click);
			//
			// mnuFixedAssetsHelp
			//
			this.mnuFixedAssetsHelp.Name = "mnuFixedAssetsHelp";
			this.mnuFixedAssetsHelp.Text = "Fixed Assets";
			this.mnuFixedAssetsHelp.Click += new System.EventHandler(this.mnuFixedAssetsHelp_Click);
			//
			// mnuGeneralEntryHelp
			//
			this.mnuGeneralEntryHelp.Name = "mnuGeneralEntryHelp";
			this.mnuGeneralEntryHelp.Text = "General Entry";
			this.mnuGeneralEntryHelp.Click += new System.EventHandler(this.mnuGeneralEntryHelp_Click);
			//
			// mnuMotorVehicleRegistrationHelp
			//
			this.mnuMotorVehicleRegistrationHelp.Name = "mnuMotorVehicleRegistrationHelp";
			this.mnuMotorVehicleRegistrationHelp.Text = "Motor Vehicle Registration";
			this.mnuMotorVehicleRegistrationHelp.Click += new System.EventHandler(this.mnuMotorVehicleRegistrationHelp_Click);
			//
			// mnuPayrollHelp
			//
			this.mnuPayrollHelp.Name = "mnuPayrollHelp";
			this.mnuPayrollHelp.Text = "Payroll";
			this.mnuPayrollHelp.Click += new System.EventHandler(this.mnuPayrollHelp_Click);
			//
			// mnuPersonalPropertyHelp
			//
			this.mnuPersonalPropertyHelp.Name = "mnuPersonalPropertyHelp";
			this.mnuPersonalPropertyHelp.Text = "Personal Property";
			this.mnuPersonalPropertyHelp.Click += new System.EventHandler(this.mnuPersonalPropertyHelp_Click);
			//
			// mnuRealEstateHelp
			//
			this.mnuRealEstateHelp.Name = "mnuRealEstateHelp";
			this.mnuRealEstateHelp.Text = "Real Estate";
			this.mnuRealEstateHelp.Click += new System.EventHandler(this.mnuRealEstateHelp_Click);
			//
			// mnuTaxBillingHelp
			//
			this.mnuTaxBillingHelp.Name = "mnuTaxBillingHelp";
			this.mnuTaxBillingHelp.Text = "Tax Billing";
			this.mnuTaxBillingHelp.Click += new System.EventHandler(this.mnuTaxBillingHelp_Click);
			//
			// mnuTaxCollectionsHelp
			//
			this.mnuTaxCollectionsHelp.Name = "mnuTaxCollectionsHelp";
			this.mnuTaxCollectionsHelp.Text = "Tax Collections";
			this.mnuTaxCollectionsHelp.Click += new System.EventHandler(this.mnuTaxCollectionsHelp_Click);
			//
			// mnuUtilityBillingHelp
			//
			this.mnuUtilityBillingHelp.Name = "mnuUtilityBillingHelp";
			this.mnuUtilityBillingHelp.Text = "Utility Billing";
			this.mnuUtilityBillingHelp.Click += new System.EventHandler(this.mnuUtilityBillingHelp_Click);
			//
			// mnuVoterRegistrationHelp
			//
			this.mnuVoterRegistrationHelp.Name = "mnuVoterRegistrationHelp";
			this.mnuVoterRegistrationHelp.Text = "Voter Registration";
			this.mnuVoterRegistrationHelp.Click += new System.EventHandler(this.mnuVoterRegistrationHelp_Click);
			//
			// mnuHelpSeparator
			//
			this.mnuHelpSeparator.Name = "mnuHelpSeparator";
			this.mnuHelpSeparator.Text = "-";
			//
			// mnuHAbout
			//
			this.mnuHAbout.Name = "mnuHAbout";
			this.mnuHAbout.Text = "About";
			this.mnuHAbout.Click += new System.EventHandler(this.mnuHAbout_Click);
			//
			// MainMenu1
			//
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFile,
				this.mnuForms,
				this.mnuHelp
			});
			//
			// MDIParent
			//
			this.ClientSize = new System.Drawing.Size(599, 442);
			this.ClientArea.Controls.Add(this.PicArchive);
			this.ClientArea.Controls.Add(this.StatusBar1);
			this.ClientArea.Controls.Add(this.GRID);
			this.Menu = this.MainMenu1;
			this.IsMdiContainer = true;
			this.Name = "MDIParent";
			this.BackColor = System.Drawing.SystemColors.AppWorkspace;
			this.MinimizeBox = true;
			this.MaximizeBox = true;
			this.BackgroundImageLayout = Wisej.Web.ImageLayout.None;
			//this.Icon = ((System.Drawing.Icon)(resources.GetObject("MDIParent.Icon")));
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.WindowState = Wisej.Web.FormWindowState.Maximized;
			this.Activated += new System.EventHandler(this.MDIParent_Activated);
			//this.Load += new System.EventHandler(this.MDIParent_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.MDIForm_Unload);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.MDIForm_QueryUnload);
			this.LinkExecute += MDIParent_LinkExecute;
            this.Text = "TRIO Software Corporation    (General Entry)";
			this.PicArchive.ResumeLayout(false);
			this.GRID.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GRID)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
