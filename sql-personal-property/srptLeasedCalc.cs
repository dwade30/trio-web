﻿//Fecher vbPorter - Version 1.0.0.32
using fecherFoundation;
using Global;
using System;
using Wisej.Core;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for srptLeasedCalc.
	/// </summary>
	public partial class srptLeasedCalc : FCSectionReport
	{
		public srptLeasedCalc()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ActiveReport1";
		}

		public static srptLeasedCalc InstancePtr
		{
			get
			{
				return (srptLeasedCalc)Sys.GetInstance(typeof(srptLeasedCalc));
			}
		}

		protected srptLeasedCalc _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptLeasedCalc	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngLine;
		const int CNSTITEMIZEDCOLBETEEXEMPT = 15;
		const int CNSTITEMIZEDCOLBETEYEAR = 16;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			lngLine = 1;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = (lngLine >= frmPPCalculations.InstancePtr.vsLeased.Rows);
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lngLine = 1;
			if (frmPPCalculations.InstancePtr.vsLeased.Rows < 2)
			{
				this.Close();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			txtLine.Text = lngLine.ToString();
			txtCD.Text = Strings.Trim(frmPPCalculations.InstancePtr.vsLeased.TextMatrix(lngLine, 3));
			txtDesc.Text = Strings.Trim(frmPPCalculations.InstancePtr.vsLeased.TextMatrix(lngLine, 5));
			if (Strings.Trim(frmPPCalculations.InstancePtr.vsLeased.TextMatrix(lngLine, 2)) == "*")
			{
				txtR.Text = "*";
			}
			else
			{
				txtR.Text = "";
			}
			if (Strings.Trim(frmPPCalculations.InstancePtr.vsLeased.TextMatrix(lngLine, 3)) == "-")
			{
				txtQTY.Text = "";
				txtCost.Text = "";
				txtDate.Text = "";
				txtMOS.Text = "";
				txtLP.Text = "";
				txtGD.Text = "";
				txtFCTR.Text = "";
				txtValue.Text = "";
				txtRent.Text = "";
				txtBETEExempt.Text = "";
				txtYearExempt.Text = "";
			}
			else
			{
				txtQTY.Text = frmPPCalculations.InstancePtr.vsLeased.TextMatrix(lngLine, 4);
				txtCost.Text = Strings.Format(frmPPCalculations.InstancePtr.vsLeased.TextMatrix(lngLine, 6), "#,###,###,###");
				txtDate.Text = frmPPCalculations.InstancePtr.vsLeased.TextMatrix(lngLine, 7);
				txtMOS.Text = frmPPCalculations.InstancePtr.vsLeased.TextMatrix(lngLine, 8);
				txtLP.Text = "";
				switch (FCConvert.ToInt32(Conversion.Val(Strings.Trim(frmPPCalculations.InstancePtr.vsLeased.TextMatrix(lngLine, 9)))))
				{
					case 1:
						{
							txtLP.Text = "L";
							break;
						}
					case 2:
						{
							txtLP.Text = "P";
							break;
						}
				}
				//end switch
				txtRent.Text = frmPPCalculations.InstancePtr.vsLeased.TextMatrix(lngLine, 10);
				txtGD.Text = frmPPCalculations.InstancePtr.vsLeased.TextMatrix(lngLine, 11);
				txtFCTR.Text = frmPPCalculations.InstancePtr.vsLeased.TextMatrix(lngLine, 12);
				txtValue.Text = Strings.Format(frmPPCalculations.InstancePtr.vsLeased.TextMatrix(lngLine, 13), "#,###,###,###");
				txtBETEExempt.Text = Strings.Format(frmPPCalculations.InstancePtr.vsLeased.TextMatrix(lngLine, CNSTITEMIZEDCOLBETEEXEMPT), "###,###,###,###");
				if (Conversion.Val(frmPPCalculations.InstancePtr.vsLeased.TextMatrix(lngLine, CNSTITEMIZEDCOLBETEYEAR)) > 0)
				{
					txtYearExempt.Text = frmPPCalculations.InstancePtr.vsLeased.TextMatrix(lngLine, CNSTITEMIZEDCOLBETEYEAR);
				}
				else
				{
					txtYearExempt.Text = "";
				}
			}
			lngLine += 1;
		}

		
	}
}
