//Fecher vbPorter - Version 1.0.0.93

using System.Collections.Generic;
using Wisej.Web;

using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmBathPPImport.
	/// </summary>
	public partial class frmBathPPImport : fecherFoundation.FCForm
	{

		//=========================================================

		cBathImportPPDBController dbImport;
		cBathImportPPFileController flImport;


		private void frmBathPPImport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = ((int)e.KeyData) / 0x10000;

			switch (KeyCode) {
				
				case Keys.Escape:
				{
					KeyCode = (Keys)0;
					mnuExit_Click();
					break;
				}
			} 
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Unload();
		}
		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}


		private void cmdBrowse_Click(object sender, System.EventArgs e)
		{

			// MDIParent.InstancePtr.CommonDialog1.Flags = vbPorterConverter.cdlOFNExplorer+vbPorterConverter.cdlOFNFileMustExist+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
			MDIParent.InstancePtr.CommonDialog1.Text = "Choose the file to import from";
			MDIParent.InstancePtr.CommonDialog1.Filter = "dat|*.dat";
			MDIParent.InstancePtr.CommonDialog1.ShowOpen();
			txtFile.Text = MDIParent.InstancePtr.CommonDialog1.FileName;

		}

		private void Command1_Click(object sender, System.EventArgs e)
		{
			Import();

		}

		private void Import()
		{
			dbImport = new cBathImportPPDBController();
			flImport = new cBathImportPPFileController();
			
			flImport.FileName = txtFile.Text;
			var tColl = flImport.LoadFile();
			cBathImportPreferences tPrefs = new/*AsNew*/ cBathImportPreferences();

			if (chkMarkDeleted.CheckState==Wisej.Web.CheckState.Checked) {
				tPrefs.MarkAsDeleted = true;
			} else {
				tPrefs.MarkAsDeleted = false;
			}
			if (!dbImport.SaveCollection(tColl, tPrefs)) {
				MessageBox.Show("Error Importing");
			} else {
				MessageBox.Show("Done");
			}
		}


	}
}
