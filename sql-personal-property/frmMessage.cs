﻿//Fecher vbPorter - Version 1.0.0.32
using fecherFoundation;
using Global;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmMessage.
	/// </summary>
	public partial class frmMessage : BaseForm
	{
		public frmMessage()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmMessage InstancePtr
		{
			get
			{
				return (frmMessage)Sys.GetInstance(typeof(frmMessage));
			}
		}

		protected frmMessage _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private void frmMessage_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmMessage properties;
			//frmMessage.ScaleWidth	= 2925;
			//frmMessage.ScaleHeight	= 1380;
			//frmMessage.LinkTopic	= "Form1";
			//End Unmaped Properties
			this.Left = (FCGlobal.Screen.Width - frmMessage.InstancePtr.Width) / 2;
			this.Top = (FCGlobal.Screen.Height - frmMessage.InstancePtr.Height) / 2;
		}
	}
}
