﻿//Fecher vbPorter - Version 1.0.0.32
using fecherFoundation;

namespace TWPP0000
{
	public class cBETEApplicationList
	{
		//=========================================================
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private FCCollection lstDetails = new FCCollection();
		private FCCollection lstDetails_AutoInitialized;

		private FCCollection lstDetails
		{
			get
			{
				if (lstDetails_AutoInitialized == null)
				{
					lstDetails_AutoInitialized = new FCCollection();
				}
				return lstDetails_AutoInitialized;
			}
			set
			{
				lstDetails_AutoInitialized = value;
			}
		}

		private int intCurrentIndex;

		public cBETEApplicationList() : base()
		{
			intCurrentIndex = -1;
		}

		public void ClearList()
		{
			//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
			if (!(lstDetails_AutoInitialized == null))
			{
				foreach (cBETEApplication tRec in lstDetails)
				{
					lstDetails.Remove(1);
				}
				// tRec
			}
		}

		public void AddItem(cBETEApplication tItem)
		{
			if (!(tItem == null))
			{
				lstDetails.Add(tItem);
			}
		}

		public void MoveFirst()
		{
			//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
			if (!(lstDetails_AutoInitialized == null))
			{
				if (!fecherFoundation.FCUtils.IsEmpty(lstDetails))
				{
					if (lstDetails.Count >= 0)
					{
						intCurrentIndex = -1;
						MoveNext();
					}
					else
					{
						intCurrentIndex = -1;
					}
				}
				else
				{
					intCurrentIndex = -1;
				}
			}
			else
			{
				intCurrentIndex = -1;
			}
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short MoveNext()
		{
			short MoveNext = 0;
			int intReturn;
			intReturn = -1;
			MoveNext = -1;
			if (intCurrentIndex == -1)
				intCurrentIndex = 0;
			if (!fecherFoundation.FCUtils.IsEmpty(lstDetails))
			{
				if (intCurrentIndex > lstDetails.Count)
				{
					intReturn = -1;
				}
				else
				{
					while (intCurrentIndex <= lstDetails.Count)
					{
						intCurrentIndex += 1;
						if (intCurrentIndex > lstDetails.Count)
						{
							intReturn = -1;
							break;
						}
						else if (lstDetails[intCurrentIndex] == null)
						{
						}
						else
						{
							intReturn = intCurrentIndex;
							break;
						}
					}
				}
				if (intReturn == 0)
					intReturn = -1;
				intCurrentIndex = intReturn;
				MoveNext = FCConvert.ToInt16(intReturn);
			}
			else
			{
				intCurrentIndex = -1;
				MoveNext = -1;
			}
			return MoveNext;
		}

		public bool IsCurrent()
		{
			bool IsCurrent = false;
			if (intCurrentIndex > 0)
			{
				IsCurrent = true;
			}
			else
			{
				IsCurrent = false;
			}
			return IsCurrent;
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short ItemCount()
		{
			short ItemCount = 0;
			if (!fecherFoundation.FCUtils.IsEmpty(lstDetails))
			{
				ItemCount = FCConvert.ToInt16(lstDetails.Count);
			}
			else
			{
				ItemCount = 0;
			}
			return ItemCount;
		}

		public cBETEApplication GetCurrentApplication()
		{
			cBETEApplication GetCurrentApplication = null;
			cBETEApplication tRec;
			tRec = null;
			if (!fecherFoundation.FCUtils.IsEmpty(lstDetails))
			{
				if (intCurrentIndex > 0)
				{
					if (!(lstDetails[intCurrentIndex] == null))
					{
						tRec = lstDetails[intCurrentIndex];
					}
				}
			}
			GetCurrentApplication = tRec;
			return GetCurrentApplication;
		}

		public cBETEApplication GetApplicationByIndex(ref short intIndex)
		{
			cBETEApplication GetApplicationByIndex = null;
			cBETEApplication tRec;
			tRec = null;
			if (!fecherFoundation.FCUtils.IsEmpty(lstDetails) && intIndex > 0)
			{
				if (!(lstDetails[intIndex] == null))
				{
					intCurrentIndex = intIndex;
					tRec = lstDetails[intIndex];
				}
			}
			GetApplicationByIndex = tRec;
			return GetApplicationByIndex;
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short GetCurrentIndex()
		{
			short GetCurrentIndex = 0;
			GetCurrentIndex = FCConvert.ToInt16(intCurrentIndex);
			return GetCurrentIndex;
		}

		public void CreateApplication(int lngAccount, string strBusiness, string strBusinessAddress, string strOwner, string strTypeOfBusiness)
		{
			cBETEApplication tRec = new cBETEApplication();
			tRec.Account = lngAccount;
			tRec.Business = strBusiness;
			tRec.BusinessAddress = strBusinessAddress;
			tRec.Owner = strOwner;
			tRec.TypeOfBusiness = strTypeOfBusiness;
			AddItem(tRec);
		}
	}
}
