﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;
using fecherFoundation.VisualBasicLayer;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmPPPrinting.
	/// </summary>
	public partial class frmPPPrinting : BaseForm
	{
		public frmPPPrinting()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPPPrinting InstancePtr
		{
			get
			{
				return (frmPPPrinting)Sys.GetInstance(typeof(frmPPPrinting));
			}
		}

		protected frmPPPrinting _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		bool boolLeased;
		bool boolList;
		bool boolFormat;
		int cntr;
		int lngCat1;
		int lngCat2;
		int lngCat3;
		int lngCat4;
		int lngCat5;
		int lngCat6;
		int lngCat7;
		int lngCat8;
		int lngCat9;
		int lngCat49;
		int lngCatTTL;
		int lngExemption;
		int lngValue;
		int lngNetValue;
		int DepreciationYear;
		string FirstTimeSwitch = "";
		int NextDefaultReportNumber;
		string strMasterSQL = "";
		string strValueSQL = "";
		float CustomPrintWidth;
		string SQLWhere = "";
		bool BadRangeTag;
		bool boolInputReport;
		clsDRWrapper mr = new clsDRWrapper();
		/// <summary>
		/// Master Recordset
		/// </summary>
		string strSQL = "";
		// vbPorter upgrade warning: Start As object	OnWrite(double, string)
		object Start;
		// vbPorter upgrade warning: Ending As object	OnWrite(double, string)
		object Ending;
		string sqlTable = "";
		string sqlTable2 = "";
		string ListingType = "";
		string MasterFormat = "";
		string ValueFormat = "";
		string strOrder = "";
		string strOrderField = "";
		bool AssessmentFlag;
		char[] PrintFormat = new char[1];
		int LineCount;
		int PageCount;
		int LinesOnPage;
		int intLastAccount;
		string strLastName = "";
		const int CNSTRPTNAMELOC = 0;
		const int CNSTRPTNAMELOCOPENS = 1;
		const int CNSTRPTNAMELOCBUSSTREET = 10;
		const int CNSTRPTNAMEADDRESSLOC = 2;
		const int CNSTRPTNAMELOCASSESS = 3;
		const int CNSTRPTNAMEADDRESSLOCASSESS = 4;
		const int CNSTRPTNAMEADDLOCOPENSASSESS = 5;
		const int CNSTRPTINPUTFORMAT = 6;
		const int CNSTRPTORIGINALCOSTFORMAT = 7;
		const int CNSTRPTNEWACCOUNTS = 9;
		const int CNSTRPTCUSTOMFORMAT = 8;
		const int CNSTRPTNEWVALUATIONFORYEAR = 11;
		const int CNSTRPTCATEGORYBREAKDOWN = 12;
		const int CNSTRPTBETE = 13;
		const int CNSTRPTBETEACCOUNTS = 14;
		const int CNSTRPTBETEAPPLICATION = 15;
		const int CNSTRPTBETEPENDING = 16;
		const int CNSTRPTNAMEASSESSBETE = 17;
        private const int CNSTRPTINVENTORYLISTING = 18;
		private void FillGrid()
		{
			int lngRow = 0;
			int intCounter;
			bool boolShow = false;
			Grid.ColHidden(0, true);
			Grid.Rows = 0;
			for (intCounter = 0; intCounter <= 18; intCounter++)
			{
				Grid.Rows += 1;
				boolShow = true;
				lngRow = Grid.Rows - 1;
				switch (intCounter)
				{
					case 0:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(CNSTRPTNAMELOC));
							Grid.TextMatrix(lngRow, 1, "Name / Location");
							break;
						}
					case 1:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(CNSTRPTNAMELOCOPENS));
							Grid.TextMatrix(lngRow, 1, "Name / Location / Opens");
							break;
						}
					case 2:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(CNSTRPTNAMELOCBUSSTREET));
							Grid.TextMatrix(lngRow, 1, "Name / Location / Business / Street");
							break;
						}
					case 3:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(CNSTRPTNAMEADDRESSLOC));
							Grid.TextMatrix(lngRow, 1, "Name / Address / Location");
							break;
						}
					case 4:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(CNSTRPTNAMELOCASSESS));
							Grid.TextMatrix(lngRow, 1, "Name / Location / Assessment");
							break;
						}
					case 5:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(CNSTRPTNAMEADDRESSLOCASSESS));
							Grid.TextMatrix(lngRow, 1, "Name / Address / Location / Assessment");
							break;
						}
					case 6:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(CNSTRPTNAMEADDLOCOPENSASSESS));
							Grid.TextMatrix(lngRow, 1, "Name / Address / Location / Opens / Assessment");
							break;
						}
					case 7:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(CNSTRPTNAMEASSESSBETE));
							Grid.TextMatrix(lngRow, 1, "Name / Assessment / BETE");
							break;
						}
					case 8:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(CNSTRPTINPUTFORMAT));
							Grid.TextMatrix(lngRow, 1, "Input Format");
							if (modPPGN.Statics.boolShortScreenOnly)
								boolShow = false;
							break;
						}
					case 9:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(CNSTRPTORIGINALCOSTFORMAT));
							Grid.TextMatrix(lngRow, 1, "Original Cost Format");
							if (modPPGN.Statics.boolShortScreenOnly)
								boolShow = false;
							break;
						}
					case 10:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(CNSTRPTNEWACCOUNTS));
							Grid.TextMatrix(lngRow, 1, "New Accounts (Never Calculated)");
							if (modPPGN.Statics.boolShortScreenOnly)
								boolShow = false;
							break;
						}
					case 11:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(CNSTRPTCUSTOMFORMAT));
							Grid.TextMatrix(lngRow, 1, "Custom Format");
							break;
						}
					case 12:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(CNSTRPTNEWVALUATIONFORYEAR));
							Grid.TextMatrix(lngRow, 1, "New Valuation For Year");
							// If boolShortScreenOnly Then boolShow = False
							break;
						}
					case 13:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(CNSTRPTCATEGORYBREAKDOWN));
							Grid.TextMatrix(lngRow, 1, "Category Breakdown");
							break;
						}
					case 16:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(CNSTRPTBETE));
							Grid.TextMatrix(lngRow, 1, "BETE Exempt Property");
							break;
						}
					case 14:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(CNSTRPTBETEACCOUNTS));
							Grid.TextMatrix(lngRow, 1, "Accounts with BETE Exemptions");
							break;
						}
					case 15:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(CNSTRPTBETEPENDING));
							Grid.TextMatrix(lngRow, 1, "Accounts with Pending BETE");
							break;
						}
					case 17:
						{
							Grid.TextMatrix(lngRow, 0, FCConvert.ToString(CNSTRPTBETEAPPLICATION));
							Grid.TextMatrix(lngRow, 1, "BETE Application");
							if (modPPGN.Statics.boolShortScreenOnly)
								boolShow = false;
							break;
						}
                    case 18:
                    {
                        Grid.TextMatrix(lngRow, 0, CNSTRPTINVENTORYLISTING.ToString());
                        Grid.TextMatrix(lngRow, 1, "Inventory Listing");
                        break;
                    }
				}
				//end switch
				if (!boolShow)
				{
					Grid.RemoveItem(lngRow);
				}
			}
			// intCounter
			Grid.Row = 0;
			Grid.Col = 1;
		}

		private void GetOpenTitles()
		{
			try
			{
				// On Error GoTo ErrorTag
				modPPGN.Statics.Open1Title = "OPEN 1";
				modPPGN.Statics.Open2Title = "OPEN 2";
				clsDRWrapper rsTemp = new clsDRWrapper();
				rsTemp.OpenRecordset("Select * from ppratioopens", modPPGN.strPPDatabase);
				if (!rsTemp.EndOfFile())
				{
					modPPGN.Statics.Open1Title = FCConvert.ToString(rsTemp.Get_Fields_String("OpenField1"));
					modPPGN.Statics.Open2Title = FCConvert.ToString(rsTemp.Get_Fields_String("OpenField2"));
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorTag:
				MessageBox.Show(Information.Err(ex).Description, null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void chkAccount_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkAccount.CheckState == CheckState.Checked)
			{
				CustomPrintWidth += 6;
			}
			else
			{
				CustomPrintWidth -= 6;
			}
			lblPrintWidth.Text = "Current Line Width = " + FCConvert.ToString(CustomPrintWidth);
		}
		
		private void chkLocation_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkLocation.CheckState == CheckState.Checked)
			{
				CustomPrintWidth += 33 / 1440F;
			}
			else
			{
				CustomPrintWidth -= 33 / 1440F;
			}
			lblPrintWidth.Text = "Current Line Width = " + FCConvert.ToString(CustomPrintWidth);
		}

		private void chkMailingAddress_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkMailingAddress.CheckState == CheckState.Checked)
			{
				CustomPrintWidth += 0;
			}
			else
			{
				CustomPrintWidth -= 0;
			}
			lblPrintWidth.Text = "Current Line Width = " + FCConvert.ToString(CustomPrintWidth);
		}

		private void chkName_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkName.CheckState == CheckState.Checked)
			{
				CustomPrintWidth += 35 / 1440F;
			}
			else
			{
				CustomPrintWidth -= 35 / 1440F;
			}
			lblPrintWidth.Text = "Current Line Width = " + FCConvert.ToString(CustomPrintWidth);
		}

		private void chkOpen1_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkOpen1.CheckState == CheckState.Checked)
			{
				CustomPrintWidth += 35 / 1440F;
			}
			else
			{
				CustomPrintWidth -= 35 / 1440F;
			}
			lblPrintWidth.Text = "Current Line Width = " + FCConvert.ToString(CustomPrintWidth);
		}

		private void chkOpen2_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkOpen2.CheckState == CheckState.Checked)
			{
				CustomPrintWidth += 35 / 1440F;
			}
			else
			{
				CustomPrintWidth -= 35 / 1440F;
			}
			lblPrintWidth.Text = "Current Line Width = " + FCConvert.ToString(CustomPrintWidth);
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			// menuPPPrint.Show
			frmPPPrinting.InstancePtr.Close();
		}

		private void frmPPPrinting_Activated(object sender, System.EventArgs e)
		{
			GetOpenTitles();
		}

		private void frmPPPrinting_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmPPPrinting_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				FCFileSystem.FileClose();
				frmPPPrinting.InstancePtr.Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmPPPrinting_Load(object sender, System.EventArgs e)
		{
			CustomPrintWidth = 0;
			boolList = false;
			boolFormat = false;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			FillGrid();
			if (Strings.Trim(modPPGN.Statics.Open1Title) != string.Empty)
			{
				chkOpen1.Text = modPPGN.Statics.Open1Title;
				if (cmbSequence.Items.Contains("Open 1"))
				{
					cmbSequence.Items.Remove("Open 1");
					cmbSequence.Items.Insert(3, modPPGN.Statics.Open1Title);
				}
			}
			if (Strings.Trim(modPPGN.Statics.Open2Title) != string.Empty)
			{
				chkOpen2.Text = modPPGN.Statics.Open2Title;
				if (cmbSequence.Items.Contains("Open 2"))
				{
					cmbSequence.Items.Remove("Open 2");
					cmbSequence.Items.Insert(4, modPPGN.Statics.Open2Title);
				}
			}
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			modGlobal.SaveWindowSize(this);
		}

		private void Grid_DblClick(object sender, System.EventArgs e)
		{
			if (Grid.Row >= 0)
			{
				mnuPrintPreview_Click();
			}
		}

		private void Grid_RowColChange(object sender, System.EventArgs e)
		{
			int lngRow;
			int Index;
			lngRow = Grid.Row;
			if (lngRow < 0)
				return;
			int x;
			Index = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(lngRow, 0))));
			boolFormat = true;
			//FC:FINAL:AM: print menu is always enabled
			//cmdPrint.Enabled = boolList;
			fraLeased.Visible = Index == 7 || Index == 6;
			if (Index == 7 || Index == 6)
			{
				if (!cmbLeased.Items.Contains("Leased Report"))
				{
					cmbLeased.Items.Insert(0, "Leased Report");
				}
			}
			else
			{
				if (cmbLeased.Items.Contains("Leased Report"))
				{
					cmbLeased.Items.Remove("Leased Report");
				}
			}
			fraCustomFormatOnly.Enabled = Index == 8;
			chkName.Enabled = Index == 8;
			chkAccount.Enabled = Index == 8;
			chkMailingAddress.Enabled = Index == 8;
			chkLocation.Enabled = Index == 8;
			chkOpen1.Enabled = Index == 8;
			chkOpen2.Enabled = Index == 8;
			if (Index == CNSTRPTNEWVALUATIONFORYEAR)
			{
				chkYearNew.Visible = true;
			}
			else
			{
				chkYearNew.Visible = false;
			}
			if (Index == CNSTRPTCATEGORYBREAKDOWN)
			{
				chkCategory.Visible = true;
			}
			else
			{
				chkCategory.Visible = false;
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuPrintPreview_Click(object sender, System.EventArgs e)
		{
			cmdPrint_Click();
		}

		public void mnuPrintPreview_Click()
		{
			mnuPrintPreview_Click(mnuPrintPreview, new System.EventArgs());
		}
	
		private void optListing_Click(ref short Index)
		{
			boolList = true;
			cmdPrint.Enabled = boolFormat;
			fraRange.Enabled = Index == 1 || Index == 3 || Index == 5 || Index == 6 || Index == 7;
		}

		private void RangeEnabled()
		{
			fraRange.Enabled = true;
			// lblInformation.Enabled = True
			lblStart.Enabled = true;
			lblStop.Enabled = true;
			txtStart.Enabled = true;
			txtStop.Enabled = true;
		}

		private void RangeDisabled()
		{
			fraRange.Enabled = false;
			// lblInformation.Enabled = False
			lblStart.Enabled = false;
			lblStop.Enabled = false;
			txtStart.Text = "";
			txtStart.Enabled = false;
			txtStop.Text = "";
			txtStop.Enabled = false;
		}
		
		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			string strOrderBy = "";
			string strWhere = "";
			bool boolRange = false;
			bool boolTemp = false;
			int intRange = 0;
			modPrinting.Statics.rsReports = new clsDRWrapper();
			int intChoice;
			intChoice = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(Grid.Row, 0))));
			if (intChoice < 0)
				return;
			if (cmbSequence.SelectedIndex == 0)
			{
				strOrderBy = "mj.account";
				strWhere = " and account between " + FCConvert.ToString(Conversion.Val(txtStart.Text)) + " and " + FCConvert.ToString(Conversion.Val(txtStop.Text)) + " ";
				intRange = 0;
			}
			else if (cmbSequence.SelectedIndex == 1)
			{
				strOrderBy = "Name";
				strWhere = " and name between '" + Strings.Trim(txtStart.Text) + "' and '" + Strings.Trim(txtStop.Text) + "zzz' ";
				intRange = 1;
			}
			else if (cmbSequence.SelectedIndex == 2)
			{
				strOrderBy = "Street,streetnumber";
				strWhere = " and street between '" + Strings.Trim(txtStart.Text) + "' and '" + Strings.Trim(txtStop.Text) + "zzz' ";
				intRange = 2;
			}
			else if (cmbSequence.SelectedIndex == 3)
			{
				strOrderBy = "Open1";
				strWhere = " and open1 between '" + Strings.Trim(txtStart.Text) + "' and '" + Strings.Trim(txtStop.Text) + "zzz' ";
				intRange = 3;
			}
			else if (cmbSequence.SelectedIndex == 4)
			{
				strOrderBy = "Open2";
				strWhere = " and open2 between '" + Strings.Trim(txtStart.Text) + "' and '" + Strings.Trim(txtStop.Text) + "zzz' ";
				intRange = 4;
			}
			else if (cmbSequence.SelectedIndex == 5)
			{
				strOrderBy = "Businesscode,name";
				strWhere = " and businesscode between " + FCConvert.ToString(Conversion.Val(txtStart.Text)) + " and " + FCConvert.ToString(Conversion.Val(txtStop.Text)) + " ";
				intRange = 5;
			}
			else if (cmbSequence.SelectedIndex == 6)
			{
				strOrderBy = "StreetCode,name";
				strWhere = " and streetcode between " + FCConvert.ToString(Conversion.Val(txtStart.Text)) + " and " + FCConvert.ToString(Conversion.Val(txtStop.Text)) + " ";
				intRange = 6;
			}
			else if (cmbSequence.SelectedIndex == 7)
			{
				strOrderBy = "trancode,name";
				strWhere = " and trancode between " + FCConvert.ToString(Conversion.Val(txtStart.Text)) + " and " + FCConvert.ToString(Conversion.Val(txtStop.Text)) + " ";
				intRange = 7;
			}
			if (intChoice == CNSTRPTCATEGORYBREAKDOWN)
			{
				if (chkCategory.CheckState == CheckState.Checked)
				{
					boolTemp = false;
				}
				else
				{
					boolTemp = true;
				}
				if (cmbRange.SelectedIndex == 0)
				{
					boolRange = false;
				}
				else
				{
					boolRange = true;
				}
				rptCategoryBreakdown.InstancePtr.Init(boolTemp, boolRange, intRange, Strings.Trim(txtStart.Text), Strings.Trim(txtStop.Text));
				return;
			}
			if (intChoice == CNSTRPTNEWVALUATIONFORYEAR)
			{
				rptNewValuation.InstancePtr.Init(chkYearNew.CheckState == CheckState.Checked);
				return;
			}
			if (intChoice == CNSTRPTNEWACCOUNTS)
			{
				frmReportViewer.InstancePtr.Init(rptNewAccounts.InstancePtr, boolAllowEmail: true, strAttachmentName: "NewAccounts");
				return;
			}
			if ((Strings.Trim(txtStart.Text) == string.Empty) && (Strings.Trim(txtStop.Text) == string.Empty))
			{
				if (cmbRange.SelectedIndex == 1)
				{
					MessageBox.Show("You did not provide a range", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (cmbRange.SelectedIndex == 0)
			{
				strWhere = "";
			}
			if (intChoice == CNSTRPTBETEAPPLICATION)
			{
				strOrderBy = strOrderBy.Replace("mj.account", "account");
				frmBETEApplication.InstancePtr.Init(ref strOrderBy, ref strWhere);
				return;
			}
			string strMasterJoin;
			string strMasterJoinJoin;
			strMasterJoin = modPPGN.GetMasterJoin();
			strMasterJoinJoin = modPPGN.GetMasterJoinForJoin();
			if (intChoice == CNSTRPTBETE)
			{
				// strSQL = "select *,ppmaster.account as account  from ppmaster INNER join ppvaluations on (ppmaster.account = ppvaluations.valuekey) where not deleted = 1 and (cat1exempt > 0 or cat2exempt > 0 or cat3exempt > 0 or cat4exempt > 0 or cat5exempt > 0 or cat6exempt > 0 or cat7exempt > 0 or cat8Exempt > 0 or cat9exempt > 0) " & strWhere & " order by " & strOrderBy
				strSQL = "select *,mj.account as account  from " + strMasterJoinJoin + " INNER join ppvaluations on (mj.account = ppvaluations.valuekey) where not deleted = 1 and (cat1exempt > 0 or cat2exempt > 0 or cat3exempt > 0 or cat4exempt > 0 or cat5exempt > 0 or cat6exempt > 0 or cat7exempt > 0 or cat8Exempt > 0 or cat9exempt > 0) " + strWhere + " order by " + strOrderBy;
				rptBETE.InstancePtr.Init(ref strSQL);
				return;
			}
			else if (intChoice == CNSTRPTBETEACCOUNTS)
			{
				// strSQL = "select *,ppmaster.account as account  from ppmaster INNER join ppvaluations on (ppmaster.account = ppvaluations.valuekey) where not deleted = 1 and (cat1exempt > 0 or cat2exempt > 0 or cat3exempt > 0 or cat4exempt > 0 or cat5exempt > 0 or cat6exempt > 0 or cat7exempt > 0 or cat8Exempt > 0 or cat9exempt > 0) " & strWhere & " order by " & strOrderBy
				strSQL = "select *,mj.account as account  from " + strMasterJoinJoin + " INNER join ppvaluations on (mj.account = ppvaluations.valuekey) where not deleted = 1 and (cat1exempt > 0 or cat2exempt > 0 or cat3exempt > 0 or cat4exempt > 0 or cat5exempt > 0 or cat6exempt > 0 or cat7exempt > 0 or cat8Exempt > 0 or cat9exempt > 0) " + strWhere + " order by " + strOrderBy;
				rptBETEAccounts.InstancePtr.Init(ref strSQL);
				return;
			}
			else if (intChoice == CNSTRPTBETEPENDING)
			{
				// strSQL = "select *,ppmaster.account as account  from ppmaster INNER join ppvaluations on (ppmaster.account = ppvaluations.valuekey) where not deleted  " & strWhere & " and ppmaster.account in (select account from ppitemized where rb = 'P' group by account) order by " & strOrderBy
				// strSQL = "select *,ppmaster.account as account from ppmaster inner join ( select account as tbl1account, sum(value) as PendingAmount from ppitemized where rb = 'P' group by account) as tbl1 on (tbl1.tbl1account = ppmaster.account) where not deleted = 1 " & strWhere & " order by " & strOrderBy
				strSQL = "select *,mj.account as account from " + strMasterJoinJoin + " inner join ( select account as tbl1account, sum(value) as PendingAmount from ppitemized where rb = 'P' group by account)  tbl1 on (tbl1.tbl1account = mj.account) where not deleted = 1 " + strWhere + " order by " + strOrderBy;
				rptBETEAccounts.InstancePtr.Init(ref strSQL, true);
				return;
			}
			else if (intChoice == CNSTRPTNAMEASSESSBETE)
			{
				// strSQL = "select *,ppmaster.account as account from ppmaster inner join ppvaluations on (ppmaster.account = ppvaluations.valuekey) where not deleted = 1 " & strWhere & " order by " & strOrderBy
				strSQL = "select *,mj.account as account from " + strMasterJoinJoin + " inner join ppvaluations on (mj.account = ppvaluations.valuekey) where not deleted = 1 " + strWhere + " order by " + strOrderBy;
				rptNameAssessBETE.InstancePtr.Init(ref strSQL);
			}

            if (intChoice == CNSTRPTINVENTORYLISTING)
            {
                strSQL = "select *,mj.account as account from " + strMasterJoinJoin +
                         " inner join ppvaluations on (mj.account = ppvaluations.valuekey) where not deleted = 1 " +
                         strWhere + " order by " + strOrderBy;
                rptInventoryListing.InstancePtr.Init(strSQL);
                return;
            }			
			strSQL = "select *,account as masteraccount from " + strMasterJoinJoin + " where not deleted = 1 " + strWhere + " order by " + strOrderBy;
			//modPrinting.Statics.rsReports.OpenRecordset(strSQL, "PersonalProperty");
			if (intChoice == CNSTRPTNAMELOC)
			{
				rptNameLoc.InstancePtr.Init(strSQL);
				//frmReportViewer.InstancePtr.Init(rptObj: rptNameLoc.InstancePtr, boolAllowEmail: true, strAttachmentName: "NameLocation");
			}
			else if (intChoice == CNSTRPTNAMELOCOPENS)
			{
				rptNameLocOpen.InstancePtr.Init(strSQL);
				//frmReportViewer.InstancePtr.Init(rptObj: rptNameLocOpen.InstancePtr, boolAllowEmail: true, strAttachmentName: "NameLocOpen");
			}
			else if (intChoice == CNSTRPTNAMEADDRESSLOC)
            {
                rptNameAddressLoc.InstancePtr.Init(strSQL);
				//frmReportViewer.InstancePtr.Init(rptObj: rptNameAddressLoc.InstancePtr, boolAllowEmail: true, strAttachmentName: "NameAddressLoc");
			}
			else if (intChoice == CNSTRPTNAMELOCASSESS)
			{
				rptNameLocAsses.InstancePtr.Init(strSQL);
				//frmReportViewer.InstancePtr.Init(rptObj: rptNameLocAsses.InstancePtr, boolAllowEmail: true, strAttachmentName: "NameLocAssess");
			}
			else if (intChoice == CNSTRPTNAMEADDRESSLOCASSESS)
            {
                rptNameAddressLocAsses.InstancePtr.Init(strSQL);
				//frmReportViewer.InstancePtr.Init(rptObj: rptNameAddressLocAsses.InstancePtr, boolAllowEmail: true, strAttachmentName: "NameAddrLocAssess");
			}
			else if (intChoice == CNSTRPTNAMEADDLOCOPENSASSESS)
            {
                rptNameAddressLocAssesOpen.InstancePtr.Init(strSQL);
				//frmReportViewer.InstancePtr.Init(rptObj: rptNameAddressLocAssesOpen.InstancePtr, boolAllowEmail: true, strAttachmentName: "NameAddrLocAssessOpen");
			}
			else if (intChoice == CNSTRPTINPUTFORMAT)
			{
				if (cmbLeased.SelectedIndex == 1)
				{
					// rptItemizedInput.Show
					rptLeasedandItemizedInput.InstancePtr.Start(1, (chkQTY.CheckState != CheckState.Checked),strSQL);
				}
				else if (cmbLeased.SelectedIndex == 0)
				{
					// rptItemizedInput.Show
					// rptLeasedInput.Show
					rptLeasedandItemizedInput.InstancePtr.Start(0, (chkQTY.CheckState != CheckState.Checked),strSQL);
				}
				else
				{
					rptLeasedandItemizedInput.InstancePtr.Start(2, (chkQTY.CheckState != CheckState.Checked),strSQL);
				}
			}
			else if (intChoice == CNSTRPTORIGINALCOSTFORMAT)
			{
				if (cmbLeased.SelectedIndex == 0)
				{
					// rptLeasedOriginalCost.Show
					rptLeasedandItemized.InstancePtr.Start(0, (chkQTY.CheckState != CheckState.Checked),strSQL);
				}
				else if (cmbLeased.SelectedIndex == 1)
				{
					// rptItemizedOriginalCost.Show
					rptLeasedandItemized.InstancePtr.Start(1, (chkQTY.CheckState != CheckState.Checked),strSQL);
				}
				else
				{
					rptLeasedandItemized.InstancePtr.Start(2, (chkQTY.CheckState != CheckState.Checked),strSQL);
				}
			}
			else if (intChoice == CNSTRPTCUSTOMFORMAT)
            {
                rptCustom.InstancePtr.Init(strSQL);
				//frmReportViewer.InstancePtr.Init(rptCustom.InstancePtr, boolAllowEmail: true, strAttachmentName: "Custom");
			}
			else if (intChoice == CNSTRPTNAMELOCBUSSTREET)
            {
                rptNameLocStreetBus.InstancePtr.Init(strSQL);
				//frmReportViewer.InstancePtr.Init(rptNameLocStreetBus.InstancePtr, boolAllowEmail: true, strAttachmentName: "NameLocStreetBus");
			}
            modPrinting.Statics.rsReports_AutoInitialized?.Dispose();
		}

		public void cmdPrint_Click()
		{
			cmdPrint_Click(cmdPrint, new System.EventArgs());
		}

		private void CheckListingType()
		{
			if (ListingType == "Account")
			{
				Start = Conversion.Val(txtStart.Text);
				Ending = Conversion.Val(txtStop.Text);
				if (FCConvert.ToInt32(Start) == 0 || FCConvert.ToInt32(Ending) == 0)
				{
					strSQL += " WHERE " + ListingType;
					if (FCConvert.ToInt32(Start) != 0)
					{
						strSQL += " >= " + Start;
					}
					else
					{
						strSQL += " >= 1";
					}
					if (FCConvert.ToInt32(Ending) != 0)
					{
						strSQL += " AND " + ListingType + " <= " + Ending;
					}
					else
					{
						strSQL += " AND " + ListingType + " <= " + FCConvert.ToString(modPPCalculate.Statics.NextPPAccount - 1);
					}
				}
				else if (FCConvert.ToInt32(Start) != 0 && FCConvert.ToInt32(Ending) != 0)
				{
					if (FCConvert.ToInt32(Ending) < FCConvert.ToInt32(Start))
					{
						MessageBox.Show("You entered an Ending Account Number that is lower than your Starting Account Number.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
						BadRangeTag = true;
						return;
					}
					else
					{
						BadRangeTag = false;
					}
					strSQL += " WHERE " + sqlTable + ".Account >=" + Start + " AND " + sqlTable + ".Account <=" + Ending;
				}
			}
			else
			{
				Start = txtStart.Text;
				Ending = txtStop.Text;
				if (FCConvert.ToString(Start) == "" || FCConvert.ToString(Ending) == "")
				{
					strSQL += " WHERE " + ListingType;
					if (FCConvert.ToString(Start) != "")
						strSQL += " >= '" + Start + "'";
					if (FCConvert.ToString(Ending) != "")
						strSQL += " <= '" + Ending + "'";
				}
				else if (FCConvert.ToString(Start) != "" && FCConvert.ToString(Ending) != "")
				{
					if (Convert.ToByte(FCConvert.ToString(Ending)[0]) < Convert.ToByte(FCConvert.ToString(Start)[0]))
					{
						BadRangeTag = true;
						return;
					}
					else
					{
						BadRangeTag = false;
					}
					strSQL += " WHERE " + ListingType + " BETWEEN '" + Start + "' AND '" + Ending + "*'";
				}
			}
			if (FCConvert.ToString(PrintFormat) != "8")
				strSQL += " And Deleted <> 1";
			strSQL += " AND Deleted <> 1";
		}

		private bool CheckData()
		{
			bool CheckData = false;
			// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Cost] and replace with corresponding Get_Field method
			if (FCConvert.ToString(mr.Get_Fields_String("CD")) == "1" && Strings.Trim(FCConvert.ToString(mr.Get_Fields_String("Description"))) == string.Empty && Conversion.Val(mr.Get_Fields("Year")) == 0 && Conversion.Val(mr.Get_Fields("Cost")) == 0)
			{
				CheckData = false;
				return CheckData;
			}
			CheckData = true;
			return CheckData;
		}

		private void optRange_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			if (Index == 1)
			{
				fraRange.Enabled = true;
			}
			else
			{
				fraRange.Enabled = false;
			}
		}

		private void optRange_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = FCConvert.ToInt16(cmbRange.SelectedIndex);
			optRange_CheckedChanged(index, sender, e);
		}

		private void cmbRange_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			optRange_CheckedChanged(sender, e);
		}
	}
}
