﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPP0000
{
	public class clsCategories
	{
		//=========================================================
		private string[] strCategories = new string[9 + 1];
		private double[] dblCategories = new double[9 + 1];
		private double[] dblBETE = new double[9 + 1];
		private int[] intYears = new int[9 + 1];
		private int[] intType = new int[9 + 1];

		public void Clear()
		{
			int x;
			for (x = 1; x <= 9; x++)
			{
				dblCategories[x] = 0;
				dblBETE[x] = 0;
			}
			// x
		}

		public void Set_CategoryValue(ref short intIndex, ref double dblValue)
		{
			if (intIndex > 0 && intIndex < 10)
			{
				dblCategories[intIndex] = dblValue;
			}
		}

		public double Get_CategoryValue(ref short intIndex)
		{
			double CategoryValue = 0;
			if (intIndex > 0 && intIndex < 10)
			{
				CategoryValue = dblCategories[intIndex];
			}
			else
			{
				CategoryValue = 0;
			}
			return CategoryValue;
		}

		public void Set_BETEValue(int intIndex, double dblValue)
		{
			if (intIndex > 0 && intIndex < 10)
			{
				dblBETE[intIndex] = dblValue;
			}
		}

		public double Get_BETEValue(int intIndex)
		{
			double BETEValue = 0;
			if (intIndex > 0 && intIndex < 10)
			{
				BETEValue = dblBETE[intIndex];
			}
			else
			{
				BETEValue = 0;
			}
			return BETEValue;
		}

		public void Set_CategoryDescription(ref short intIndex, ref string strDesc)
		{
			if (intIndex > 0 && intIndex < 10)
			{
				strCategories[intIndex] = strDesc;
			}
		}

		public string Get_CategoryDescription(int intIndex)
		{
			string CategoryDescription = "";
			if (intIndex > 0 && intIndex < 10)
			{
				CategoryDescription = strCategories[intIndex];
			}
			return CategoryDescription;
		}

		public void Set_Life(ref short intIndex, ref short intLife)
		{
			if (intIndex > 0 && intIndex < 10)
			{
				intYears[intIndex] = intLife;
			}
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short Get_Life(short intIndex)
		{
			short Life = 0;
			if (intIndex > 0 && intIndex < 10)
			{
				Life = FCConvert.ToInt16(intYears[intIndex]);
			}
			else
			{
				Life = 0;
			}
			return Life;
		}

		public void Set_CategoryType(int intIndex, ref short intCatType)
		{
			if (intIndex > 0 && intIndex < 10)
			{
				intType[intIndex] = intCatType;
			}
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short Get_CategoryType(int intIndex)
		{
			short CategoryType = 0;
			if (intIndex > 0 && intIndex < 10)
			{
				CategoryType = FCConvert.ToInt16(intType[intIndex]);
			}
			else
			{
				CategoryType = 0;
			}
			return CategoryType;
		}

		public void LoadCategories()
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			rsTemp.OpenRecordset("select * from ratiotrends order by type", modPPGN.strPPDatabase);
			while (!rsTemp.EndOfFile())
			{
				if (Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Description"))) != "")
				{
					// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
					strCategories[rsTemp.Get_Fields("type")] = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Description")));
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
					strCategories[rsTemp.Get_Fields("type")] = "Category " + rsTemp.Get_Fields("type");
				}
				// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [life] and replace with corresponding Get_Field method
				intYears[rsTemp.Get_Fields("type")] = FCConvert.ToInt32(Math.Round(Conversion.Val(rsTemp.Get_Fields("life"))));
				// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
				intType[rsTemp.Get_Fields("type")] = FCConvert.ToInt32(Math.Round(Conversion.Val(rsTemp.Get_Fields_Int32("categorytype"))));
				rsTemp.MoveNext();
			}
		}

		public clsCategories() : base()
		{
			LoadCategories();
		}
	}
}
