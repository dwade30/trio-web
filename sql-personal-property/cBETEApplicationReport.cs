﻿//Fecher vbPorter - Version 1.0.0.32
namespace TWPP0000
{
	public class cBETEApplicationReport
	{
		//=========================================================
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cBETEApplicationList applications = new cBETEApplicationList();
		private cBETEApplicationList applications_AutoInitialized;

		private cBETEApplicationList applications
		{
			get
			{
				if (applications_AutoInitialized == null)
				{
					applications_AutoInitialized = new cBETEApplicationList();
				}
				return applications_AutoInitialized;
			}
			set
			{
				applications_AutoInitialized = value;
			}
		}

		private int intYear;
		private int intEffYear;
		private int intOwnerSource;
		private int intBusinessSource;
		private bool boolCurrentBETEOnly;
		private string strOrderBy = string.Empty;
		private string strWhere = string.Empty;
		private bool boolPending;

		public int TaxYear
		{
			set
			{
				intYear = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				int TaxYear = 0;
				TaxYear = intYear;
				return TaxYear;
			}
		}

		public int EffectiveYear
		{
			set
			{
				intEffYear = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				int EffectiveYear = 0;
				EffectiveYear = intEffYear;
				return EffectiveYear;
			}
		}

		public int OwnerSource
		{
			set
			{
				intOwnerSource = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				int OwnerSource = 0;
				OwnerSource = intOwnerSource;
				return OwnerSource;
			}
		}

		public int BusinessSource
		{
			set
			{
				intBusinessSource = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				int BusinessSource = 0;
				BusinessSource = intBusinessSource;
				return BusinessSource;
			}
		}

		public bool CurrentBETEOnly
		{
			set
			{
				boolCurrentBETEOnly = value;
			}
			get
			{
				bool CurrentBETEOnly = false;
				CurrentBETEOnly = boolCurrentBETEOnly;
				return CurrentBETEOnly;
			}
		}

		public string OrderByClause
		{
			set
			{
				strOrderBy = value;
			}
			get
			{
				string OrderByClause = "";
				OrderByClause = strOrderBy;
				return OrderByClause;
			}
		}

		public string WhereClause
		{
			set
			{
				strWhere = value;
			}
			get
			{
				string WhereClause = "";
				WhereClause = strWhere;
				return WhereClause;
			}
		}

		public bool Pending
		{
			set
			{
				boolPending = value;
			}
			get
			{
				bool Pending = false;
				Pending = boolPending;
				return Pending;
			}
		}

		public cBETEApplicationList BETEApplications
		{
			set
			{
				applications = value;
			}
			get
			{
				cBETEApplicationList BETEApplications = null;
				BETEApplications = applications;
				return BETEApplications;
			}
		}
	}
}
