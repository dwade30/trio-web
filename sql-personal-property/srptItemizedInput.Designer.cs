﻿namespace TWPP0000
{
	/// <summary>
	/// Summary description for srptItemizedInput.
	/// </summary>
	partial class srptItemizedInput
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptItemizedInput));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtLine = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTY = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDepYrs = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSRO = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFCTR = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRB = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLeasedTo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemptYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			((System.ComponentModel.ISupportInitialize)(this.txtLine)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTY)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDepYrs)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSRO)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFCTR)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLeasedTo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtLine,
				this.txtCD,
				this.txtQTY,
				this.txtDescription,
				this.txtYear,
				this.txtDepYrs,
				this.txtSRO,
				this.txtCost,
				this.txtGD,
				this.txtFCTR,
				this.txtRB,
				this.txtLeasedTo,
				this.txtExemptYear
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			// 
			// txtLine
			// 
			this.txtLine.Height = 0.1875F;
			this.txtLine.Left = 0F;
			this.txtLine.Name = "txtLine";
			this.txtLine.Style = "font-family: \'Tahoma\'";
			this.txtLine.Text = "Field1";
			this.txtLine.Top = 0F;
			this.txtLine.Width = 0.5F;
			// 
			// txtCD
			// 
			this.txtCD.Height = 0.1875F;
			this.txtCD.Left = 1.520833F;
			this.txtCD.Name = "txtCD";
			this.txtCD.Style = "font-family: \'Tahoma\'";
			this.txtCD.Text = "Field2";
			this.txtCD.Top = 0F;
			this.txtCD.Width = 0.3125F;
			// 
			// txtQTY
			// 
			this.txtQTY.Height = 0.1875F;
			this.txtQTY.Left = 1.895833F;
			this.txtQTY.Name = "txtQTY";
			this.txtQTY.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtQTY.Text = "Field3";
			this.txtQTY.Top = 0F;
			this.txtQTY.Width = 0.375F;
			// 
			// txtDescription
			// 
			this.txtDescription.CanShrink = true;
			this.txtDescription.Height = 0.1875F;
			this.txtDescription.Left = 2.333333F;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Style = "font-family: \'Tahoma\'";
			this.txtDescription.Text = "Field4";
			this.txtDescription.Top = 0F;
			this.txtDescription.Width = 3.5F;
			// 
			// txtYear
			// 
			this.txtYear.Height = 0.1875F;
			this.txtYear.Left = 5.895833F;
			this.txtYear.Name = "txtYear";
			this.txtYear.Style = "font-family: \'Tahoma\'; text-align: center";
			this.txtYear.Text = "Field5";
			this.txtYear.Top = 0F;
			this.txtYear.Width = 0.4375F;
			// 
			// txtDepYrs
			// 
			this.txtDepYrs.Height = 0.1875F;
			this.txtDepYrs.Left = 6.395833F;
			this.txtDepYrs.Name = "txtDepYrs";
			this.txtDepYrs.Style = "font-family: \'Tahoma\'; text-align: left";
			this.txtDepYrs.Text = "Field6";
			this.txtDepYrs.Top = 0F;
			this.txtDepYrs.Width = 0.4375F;
			// 
			// txtSRO
			// 
			this.txtSRO.Height = 0.1875F;
			this.txtSRO.Left = 6.895833F;
			this.txtSRO.Name = "txtSRO";
			this.txtSRO.Style = "font-family: \'Tahoma\'; text-align: left";
			this.txtSRO.Text = "Field7";
			this.txtSRO.Top = 0F;
			this.txtSRO.Width = 0.3125F;
			// 
			// txtCost
			// 
			this.txtCost.Height = 0.1875F;
			this.txtCost.Left = 7.270833F;
			this.txtCost.Name = "txtCost";
			this.txtCost.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtCost.Text = null;
			this.txtCost.Top = 0F;
			this.txtCost.Width = 1.0625F;
			// 
			// txtGD
			// 
			this.txtGD.Height = 0.1875F;
			this.txtGD.Left = 8.385417F;
			this.txtGD.Name = "txtGD";
			this.txtGD.Style = "font-family: \'Tahoma\'; text-align: center";
			this.txtGD.Text = null;
			this.txtGD.Top = 0F;
			this.txtGD.Width = 0.25F;
			// 
			// txtFCTR
			// 
			this.txtFCTR.Height = 0.1875F;
			this.txtFCTR.Left = 8.75F;
			this.txtFCTR.Name = "txtFCTR";
			this.txtFCTR.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtFCTR.Text = null;
			this.txtFCTR.Top = 0F;
			this.txtFCTR.Width = 0.375F;
			// 
			// txtRB
			// 
			this.txtRB.Height = 0.1875F;
			this.txtRB.Left = 0.5625F;
			this.txtRB.Name = "txtRB";
			this.txtRB.Style = "font-family: \'Tahoma\'";
			this.txtRB.Text = null;
			this.txtRB.Top = 0F;
			this.txtRB.Width = 0.25F;
			// 
			// txtLeasedTo
			// 
			this.txtLeasedTo.Height = 0.1875F;
			this.txtLeasedTo.Left = 9.1875F;
			this.txtLeasedTo.Name = "txtLeasedTo";
			this.txtLeasedTo.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtLeasedTo.Text = null;
			this.txtLeasedTo.Top = 0F;
			this.txtLeasedTo.Width = 0.625F;
			// 
			// txtExemptYear
			// 
			this.txtExemptYear.Height = 0.1875F;
			this.txtExemptYear.Left = 0.9583333F;
			this.txtExemptYear.Name = "txtExemptYear";
			this.txtExemptYear.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtExemptYear.Text = null;
			this.txtExemptYear.Top = 0F;
			this.txtExemptYear.Width = 0.4375F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.CanShrink = true;
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.SubReport1
			});
			this.ReportFooter.Name = "ReportFooter";
			// 
			// SubReport1
			// 
			this.SubReport1.CloseBorder = false;
			this.SubReport1.Height = 0.0625F;
			this.SubReport1.Left = 0F;
			this.SubReport1.Name = "SubReport1";
			this.SubReport1.Report = null;
			this.SubReport1.Top = 0.15625F;
			this.SubReport1.Width = 9.8125F;
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Label8,
				this.Label9,
				this.Label10,
				this.Label11,
				this.Label12,
				this.Label13,
				this.Label14
			});
			this.GroupHeader1.Height = 0.65625F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 2.177083F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.Label1.Text = "-------------- Itemized Description --------------";
			this.Label1.Top = 0F;
			this.Label1.Width = 3.5625F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.19F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label2.Text = "Line #";
			this.Label2.Top = 0.28125F;
			this.Label2.Width = 0.5F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.19F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 1.520833F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label3.Text = "CD";
			this.Label3.Top = 0.28125F;
			this.Label3.Width = 0.25F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.19F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 1.895833F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Label4.Text = "QTY";
			this.Label4.Top = 0.28125F;
			this.Label4.Width = 0.375F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 2.333333F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label5.Text = "Description";
			this.Label5.Top = 0.28125F;
			this.Label5.Width = 1.75F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 5.895833F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label6.Text = "Year";
			this.Label6.Top = 0.28125F;
			this.Label6.Width = 0.4375F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.34375F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 6.395833F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label7.Text = "DEP YRS";
			this.Label7.Top = 0.28125F;
			this.Label7.Width = 0.375F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 6.895833F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label8.Text = "SRO";
			this.Label8.Top = 0.28125F;
			this.Label8.Width = 0.4375F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 7.645833F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Label9.Text = "Cost";
			this.Label9.Top = 0.28125F;
			this.Label9.Width = 0.6875F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.34375F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 8.364583F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label10.Text = "% GD";
			this.Label10.Top = 0.28125F;
			this.Label10.Width = 0.2708333F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 8.6875F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Label11.Text = "FCTR";
			this.Label11.Top = 0.28125F;
			this.Label11.Width = 0.4375F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.19F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0.5625F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label12.Text = "RB";
			this.Label12.Top = 0.28125F;
			this.Label12.Width = 0.3125F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.34375F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 9.1875F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label13.Text = "Leased To";
			this.Label13.Top = 0.28125F;
			this.Label13.Width = 0.6354167F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.333F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 0.826F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label14.Text = "Exempt Year";
			this.Label14.Top = 0.281F;
			this.Label14.Width = 0.652F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// ActiveReport1
			// 
			this.MasterReport = false;
			this.Name = "ActiveReport1";
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 9.885417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
			((System.ComponentModel.ISupportInitialize)(this.txtLine)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTY)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDepYrs)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSRO)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFCTR)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLeasedTo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTY;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDepYrs;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSRO;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFCTR;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRB;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLeasedTo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemptYear;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
