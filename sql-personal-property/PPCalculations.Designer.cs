﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;
using fecherFoundation.VisualBasicLayer;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmPPCalculations.
	/// </summary>
	partial class frmPPCalculations : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblStatics;
		public fecherFoundation.FCTabControl sstPPCalculate;
		public fecherFoundation.FCTabPage sstPPCalculate_Page1;
		public fecherFoundation.FCGrid vsLeased;
		public fecherFoundation.FCTabPage sstPPCalculate_Page2;
		public fecherFoundation.FCGrid vsItemized;
		public fecherFoundation.FCTabPage sstPPCalculate_Page3;
		public fecherFoundation.FCTextBox txtOverride;
		public fecherFoundation.FCCheckBox Check1;
		public fecherFoundation.FCGrid vsValuations;
		public fecherFoundation.FCGrid vsDisplay;
		public fecherFoundation.FCLabel lblOverride;
		public fecherFoundation.FCLabel lblStatics_27;
		public fecherFoundation.FCLabel lblStatics_29;
		public fecherFoundation.FCLabel lblAccount;
		public fecherFoundation.FCLabel lblStatics_20;
		public fecherFoundation.FCLabel lblLocation;
		public fecherFoundation.FCLabel lblLocationName;
		public fecherFoundation.FCLabel lblName;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFPrint;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExitNoSave;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPPCalculations));
            this.sstPPCalculate = new fecherFoundation.FCTabControl();
            this.sstPPCalculate_Page1 = new fecherFoundation.FCTabPage();
            this.vsLeased = new fecherFoundation.FCGrid();
            this.sstPPCalculate_Page2 = new fecherFoundation.FCTabPage();
            this.vsItemized = new fecherFoundation.FCGrid();
            this.sstPPCalculate_Page3 = new fecherFoundation.FCTabPage();
            this.txtOverride = new fecherFoundation.FCTextBox();
            this.Check1 = new fecherFoundation.FCCheckBox();
            this.vsValuations = new fecherFoundation.FCGrid();
            this.vsDisplay = new fecherFoundation.FCGrid();
            this.lblOverride = new fecherFoundation.FCLabel();
            this.lblStatics_27 = new fecherFoundation.FCLabel();
            this.lblStatics_29 = new fecherFoundation.FCLabel();
            this.lblAccount = new fecherFoundation.FCLabel();
            this.lblStatics_20 = new fecherFoundation.FCLabel();
            this.lblLocation = new fecherFoundation.FCLabel();
            this.lblLocationName = new fecherFoundation.FCLabel();
            this.lblName = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExitNoSave = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdFPrint = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            this.sstPPCalculate.SuspendLayout();
            this.sstPPCalculate_Page1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsLeased)).BeginInit();
            this.sstPPCalculate_Page2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsItemized)).BeginInit();
            this.sstPPCalculate_Page3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Check1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsValuations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsDisplay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFPrint)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 737);
            this.BottomPanel.Size = new System.Drawing.Size(698, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.sstPPCalculate);
            this.ClientArea.Controls.Add(this.lblStatics_27);
            this.ClientArea.Controls.Add(this.lblStatics_29);
            this.ClientArea.Controls.Add(this.lblAccount);
            this.ClientArea.Controls.Add(this.lblStatics_20);
            this.ClientArea.Controls.Add(this.lblLocation);
            this.ClientArea.Controls.Add(this.lblLocationName);
            this.ClientArea.Controls.Add(this.lblName);
            this.ClientArea.Size = new System.Drawing.Size(698, 677);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdFPrint);
            this.TopPanel.Size = new System.Drawing.Size(698, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFPrint, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(134, 30);
            this.HeaderText.Text = "Calculation";
            // 
            // sstPPCalculate
            // 
            this.sstPPCalculate.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.sstPPCalculate.Controls.Add(this.sstPPCalculate_Page1);
            this.sstPPCalculate.Controls.Add(this.sstPPCalculate_Page2);
            this.sstPPCalculate.Controls.Add(this.sstPPCalculate_Page3);
            this.sstPPCalculate.Location = new System.Drawing.Point(30, 170);
            this.sstPPCalculate.Name = "sstPPCalculate";
            this.sstPPCalculate.PageInsets = new Wisej.Web.Padding(1, 47, 1, 1);
            this.sstPPCalculate.Size = new System.Drawing.Size(649, 488);
            this.sstPPCalculate.TabIndex = 7;
            this.sstPPCalculate.Text = "Correlation";
            // 
            // sstPPCalculate_Page1
            // 
            this.sstPPCalculate_Page1.Controls.Add(this.vsLeased);
            this.sstPPCalculate_Page1.Location = new System.Drawing.Point(1, 47);
            this.sstPPCalculate_Page1.Name = "sstPPCalculate_Page1";
            this.sstPPCalculate_Page1.Size = new System.Drawing.Size(647, 440);
            this.sstPPCalculate_Page1.Text = "Leased";
            // 
            // vsLeased
            // 
            this.vsLeased.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsLeased.Cols = 17;
            this.vsLeased.FocusRect = fecherFoundation.FCGrid.FocusRectSettings.flexFocusNone;
            this.vsLeased.HighLight = fecherFoundation.FCGrid.HighLightSettings.flexHighlightNever;
            this.vsLeased.Location = new System.Drawing.Point(13, 18);
            this.vsLeased.Name = "vsLeased";
            this.vsLeased.Rows = 50;
            this.vsLeased.ShowFocusCell = false;
            this.vsLeased.Size = new System.Drawing.Size(614, 408);
            // 
            // sstPPCalculate_Page2
            // 
            this.sstPPCalculate_Page2.Controls.Add(this.vsItemized);
            this.sstPPCalculate_Page2.Location = new System.Drawing.Point(1, 47);
            this.sstPPCalculate_Page2.Name = "sstPPCalculate_Page2";
            this.sstPPCalculate_Page2.Size = new System.Drawing.Size(647, 440);
            this.sstPPCalculate_Page2.Text = "Itemized";
            // 
            // vsItemized
            // 
            this.vsItemized.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsItemized.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeRowHeight;
            this.vsItemized.Cols = 17;
            this.vsItemized.Location = new System.Drawing.Point(20, 20);
            this.vsItemized.Name = "vsItemized";
            this.vsItemized.ShowFocusCell = false;
            this.vsItemized.Size = new System.Drawing.Size(1057, 742);
            // 
            // sstPPCalculate_Page3
            // 
            this.sstPPCalculate_Page3.Controls.Add(this.txtOverride);
            this.sstPPCalculate_Page3.Controls.Add(this.Check1);
            this.sstPPCalculate_Page3.Controls.Add(this.vsValuations);
            this.sstPPCalculate_Page3.Controls.Add(this.vsDisplay);
            this.sstPPCalculate_Page3.Controls.Add(this.lblOverride);
            this.sstPPCalculate_Page3.Location = new System.Drawing.Point(1, 47);
            this.sstPPCalculate_Page3.Name = "sstPPCalculate_Page3";
            this.sstPPCalculate_Page3.Size = new System.Drawing.Size(647, 440);
            this.sstPPCalculate_Page3.Text = "Correlation";
            // 
            // txtOverride
            // 
            this.txtOverride.BackColor = System.Drawing.SystemColors.Window;
            this.txtOverride.Enabled = false;
            this.txtOverride.Location = new System.Drawing.Point(246, 30);
            this.txtOverride.LockedOriginal = true;
            this.txtOverride.Name = "txtOverride";
            this.txtOverride.ReadOnly = true;
            this.txtOverride.Size = new System.Drawing.Size(150, 40);
            this.txtOverride.TabIndex = 1;
            this.txtOverride.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtOverride.TextChanged += new System.EventHandler(this.txtOverride_TextChanged);
            this.txtOverride.Validating += new System.ComponentModel.CancelEventHandler(this.txtOverride_Validating);
            // 
            // Check1
            // 
            this.Check1.Location = new System.Drawing.Point(20, 38);
            this.Check1.Name = "Check1";
            this.Check1.Size = new System.Drawing.Size(170, 27);
            this.Check1.TabIndex = 2;
            this.Check1.Text = "Use Override Value";
            this.Check1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Check1.CheckedChanged += new System.EventHandler(this.Check1_CheckedChanged);
            // 
            // vsValuations
            // 
            this.vsValuations.Cols = 10;
            this.vsValuations.Location = new System.Drawing.Point(20, 304);
            this.vsValuations.Name = "vsValuations";
            this.vsValuations.Rows = 50;
            this.vsValuations.ShowFocusCell = false;
            this.vsValuations.Size = new System.Drawing.Size(535, 19);
            this.vsValuations.TabIndex = 4;
            this.vsValuations.Visible = false;
            // 
            // vsDisplay
            // 
            this.vsDisplay.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsDisplay.AutoSizeColumnsMode = Wisej.Web.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.vsDisplay.Cols = 10;
            //this.vsDisplay.ExtendLastCol = true;
            this.vsDisplay.FocusRect = fecherFoundation.FCGrid.FocusRectSettings.flexFocusNone;
            this.vsDisplay.HighLight = fecherFoundation.FCGrid.HighLightSettings.flexHighlightNever;
            this.vsDisplay.Location = new System.Drawing.Point(20, 108);
            this.vsDisplay.Name = "vsDisplay";
            this.vsDisplay.Rows = 50;
            this.vsDisplay.ShowFocusCell = false;
            this.vsDisplay.Size = new System.Drawing.Size(608, 320);
            this.vsDisplay.TabIndex = 3;
            // 
            // lblOverride
            // 
            this.lblOverride.AutoSize = true;
            this.lblOverride.Location = new System.Drawing.Point(20, 90);
            this.lblOverride.Name = "lblOverride";
            this.lblOverride.Size = new System.Drawing.Size(261, 15);
            this.lblOverride.TabIndex = 2;
            this.lblOverride.Text = "THIS ACCOUNT HAS AN OVERRIDE VALUE";
            this.lblOverride.Visible = false;
            // 
            // lblStatics_27
            // 
            this.lblStatics_27.Location = new System.Drawing.Point(30, 80);
            this.lblStatics_27.Name = "lblStatics_27";
            this.lblStatics_27.Size = new System.Drawing.Size(60, 15);
            this.lblStatics_27.TabIndex = 2;
            this.lblStatics_27.Text = "NAME";
            // 
            // lblStatics_29
            // 
            this.lblStatics_29.Location = new System.Drawing.Point(30, 30);
            this.lblStatics_29.Name = "lblStatics_29";
            this.lblStatics_29.Size = new System.Drawing.Size(61, 15);
            this.lblStatics_29.TabIndex = 8;
            this.lblStatics_29.Text = "ACCOUNT";
            // 
            // lblAccount
            // 
            this.lblAccount.AutoSize = true;
            this.lblAccount.Location = new System.Drawing.Point(160, 30);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(4, 14);
            this.lblAccount.TabIndex = 1;
            // 
            // lblStatics_20
            // 
            this.lblStatics_20.Location = new System.Drawing.Point(30, 130);
            this.lblStatics_20.Name = "lblStatics_20";
            this.lblStatics_20.Size = new System.Drawing.Size(66, 15);
            this.lblStatics_20.TabIndex = 5;
            this.lblStatics_20.Text = "LOCATION";
            // 
            // lblLocation
            // 
            this.lblLocation.AutoSize = true;
            this.lblLocation.Location = new System.Drawing.Point(160, 130);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(4, 14);
            this.lblLocation.TabIndex = 6;
            // 
            // lblLocationName
            // 
            this.lblLocationName.AutoSize = true;
            this.lblLocationName.Location = new System.Drawing.Point(247, 130);
            this.lblLocationName.Name = "lblLocationName";
            this.lblLocationName.Size = new System.Drawing.Size(4, 14);
            this.lblLocationName.TabIndex = 3;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(160, 80);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(4, 14);
            this.lblName.TabIndex = 4;
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFPrint,
            this.mnuSaveExit,
            this.mnuSepar,
            this.mnuExitNoSave});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuFPrint
            // 
            this.mnuFPrint.Index = 0;
            this.mnuFPrint.Name = "mnuFPrint";
            this.mnuFPrint.Text = "Print Calculation";
            this.mnuFPrint.Click += new System.EventHandler(this.mnuFPrint_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 2;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuExitNoSave
            // 
            this.mnuExitNoSave.Index = 3;
            this.mnuExitNoSave.Name = "mnuExitNoSave";
            this.mnuExitNoSave.Text = "Exit";
            this.mnuExitNoSave.Click += new System.EventHandler(this.mnuExitNoSave_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(302, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(100, 48);
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // cmdFPrint
            // 
            this.cmdFPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFPrint.Location = new System.Drawing.Point(556, 29);
            this.cmdFPrint.Name = "cmdFPrint";
            this.cmdFPrint.Size = new System.Drawing.Size(114, 24);
            this.cmdFPrint.TabIndex = 1;
            this.cmdFPrint.Text = "Print Calculation";
            this.cmdFPrint.Click += new System.EventHandler(this.mnuFPrint_Click);
            // 
            // frmPPCalculations
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(698, 845);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmPPCalculations";
            this.ShowInTaskbar = false;
            this.Text = "Calculation";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmPPCalculations_Load);
            this.Activated += new System.EventHandler(this.frmPPCalculations_Activated);
            this.FormClosed += new Wisej.Web.FormClosedEventHandler(this.frmPPCalculations_FormClosed);
            this.Resize += new System.EventHandler(this.frmPPCalculations_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPPCalculations_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPPCalculations_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            this.sstPPCalculate.ResumeLayout(false);
            this.sstPPCalculate_Page1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsLeased)).EndInit();
            this.sstPPCalculate_Page2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsItemized)).EndInit();
            this.sstPPCalculate_Page3.ResumeLayout(false);
            this.sstPPCalculate_Page3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Check1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsValuations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsDisplay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFPrint)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		private FCButton cmdFPrint;
	}
}
