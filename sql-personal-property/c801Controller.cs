﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPP0000
{
	public class c801Controller
	{
		//=========================================================
		public void Fill801Report(c801Report rep801)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (!(rep801 == null))
				{
					string strSQL = "";
					string strFieldName = "";
					string strStart = "";
					string strEnd = "";
					strStart = rep801.RangeStart;
					strEnd = rep801.RangeEnd;
					switch (rep801.RangeType)
					{
						case 0:
							{
								// name
								strStart = "'" + strStart + "'";
								strEnd = "'" + strEnd + "zzz'";
								strFieldName = "Name ";
								break;
							}
						case 1:
							{
								// account
								strFieldName = "Account ";
								break;
							}
						case 2:
							{
								// location
								strStart = "'" + strStart + "'";
								strEnd = "'" + strEnd + "zzz'";
								strFieldName = "StreetName ";
								break;
							}
						case 3:
							{
								// business code
								strFieldName = "Businesscode ";
								break;
							}
						case 4:
							{
								// tran code
								strFieldName = "TranCode ";
								break;
							}
						case 5:
							{
								// street code
								strFieldName = "StreetCode ";
								break;
							}
						case 6:
							{
								// open 1
								strStart = "'" + strStart + "'";
								strEnd = "'" + strEnd + "zzz'";
								strFieldName = "Open1 ";
								break;
							}
						case 7:
							{
								// open 2
								strStart = "'" + strStart + "'";
								strEnd = "'" + strEnd + "zzz'";
								strFieldName = "Open2 ";
								break;
							}
					}
					//end switch
					if (rep801.RangeType > 0)
					{
						if (rep801.ByRange)
						{
							strSQL = "Select * from ppmaster where not deleted = 1 and rbcode = 'Y' and " + strFieldName + " between " + strStart + " and " + strEnd + " order by " + strFieldName;
						}
						else
						{
							strSQL = "select * from ppmaster where not deleted = 1 and rbcode = 'Y' order by " + strFieldName;
						}
					}
					else
					{
						strSQL = modPPGN.GetMasterJoin();
						if (rep801.ByRange)
						{
							strSQL += " where not deleted = 1 and rbcode = 'Y' and " + strFieldName + " between " + strStart + " and " + strEnd + " order by " + strFieldName;
						}
						else
						{
							strSQL += " where not deleted = 1 and rbcode = 'Y' order by " + strFieldName;
						}
					}
					clsDRWrapper rsLoad = new clsDRWrapper();
					cGenericCollection colReport;
					c801Master m801;
					double dblSum = 0;
					colReport = rep801.Records;
					colReport.ClearList();
					rsLoad.OpenRecordset(strSQL, modPPGN.strPPDatabase);
					while (!rsLoad.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						m801 = Get801ForAccount(rsLoad.Get_Fields("account"), rep801.TaxYear);
						if (!(m801 == null))
						{
							m801.TaxRate = rep801.TaxRate;
							m801.DisplayedRate = rep801.DisplayedRate;
							// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
							if (m801.Account == FCConvert.ToInt32(rsLoad.Get_Fields("account")))
							{
								if (!(m801.DetailsB == null) && !(m801.DetailsA == null))
								{
									if (m801.DetailsA.ItemCount() > 0)
									{
										colReport.AddItem(m801);
									}
									else if (m801.DetailsB.ItemCount() > 0)
									{
										m801.DetailsB.MoveFirst();
										while (m801.DetailsB.IsCurrent())
										{
											dblSum += ((c801BDetail)m801.DetailsB.GetCurrentItem()).AllowableAssessedValue;
											if (dblSum > 0)
												break;
											m801.DetailsB.MoveNext();
										}
										if (dblSum > 0)
										{
											colReport.AddItem(m801);
										}
									}
								}
							}
						}
						rsLoad.MoveNext();
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public c801Master Get801ForAccount(int lngAccount, int intCurrentYear)
		{
			c801Master Get801ForAccount = null;
			c801Master retMaster = new c801Master();
			cGenericCollection colDetails;
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strLocation;
			retMaster.TaxYear = intCurrentYear;
			rsLoad.OpenRecordset("select * from ppmaster where account = " + FCConvert.ToString(lngAccount), modPPGN.strPPDatabase);
			strLocation = "";
			if (!rsLoad.EndOfFile())
			{
				retMaster.Account = lngAccount;
				cPartyController contParty = new cPartyController();
				cParty pOwner;
				pOwner = contParty.GetParty(rsLoad.Get_Fields_Int32("partyid"));
				if (!(pOwner == null))
				{
					retMaster.ApplicantName = pOwner.FullName;
				}
				if (modGlobal.Statics.CustomizeStuff.boolRegionalTown)
				{
					// TODO Get_Fields: Check the table for the column [trancode] and replace with corresponding Get_Field method
					modGlobal.Statics.CustomizeStuff.CurrentTown = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("trancode"))));
				}
				retMaster.MunicipalCode = FCConvert.ToString(modGlobal.Statics.CustomizeStuff.MunicipalCode);
				retMaster.Municipality = modGlobal.Statics.CustomizeStuff.MunicipalName;
				retMaster.ReportDate = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
				if (Conversion.Val(rsLoad.Get_Fields("streetnumber")) > 0)
					strLocation = FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("streetnumber")));
				strLocation = Strings.Trim(strLocation + " " + Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("street"))));
				retMaster.LocatedAt = strLocation;
				colDetails = Get801BDetails(lngAccount, intCurrentYear);
				if (!(colDetails == null))
				{
					retMaster.DetailsB = colDetails;
				}
				colDetails = Get801ADetails(lngAccount, intCurrentYear);
				if (!(colDetails == null))
				{
					retMaster.DetailsA = colDetails;
				}
			}
			Get801ForAccount = retMaster;
			return Get801ForAccount;
		}

		public cGenericCollection Get801BDetails(int intAccount, int intCurrentYear)
		{
			cGenericCollection Get801BDetails = null;
			cGenericCollection retCollection = new cGenericCollection();
			clsCategories cats = new clsCategories();
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strSQL;
			clsDRWrapper rsDet = new clsDRWrapper();
			int intMinYears;
			int intMaxYears;
			int x;
			int intCatType;
			int intYearsClaimed = 0;
			c801BDetail det801;
			double dblValue = 0;
			double dblCost = 0;
			intMinYears = 13;
			intMaxYears = 18;
			det801 = new c801BDetail();
			det801.YearsClaimed = 13;
			det801.ValueLimitation = 0.75;
			retCollection.AddItem(det801);
			det801 = new c801BDetail();
			det801.YearsClaimed = 14;
			det801.ValueLimitation = 0.7;
			retCollection.AddItem(det801);
			det801 = new c801BDetail();
			det801.YearsClaimed = 15;
			det801.ValueLimitation = 0.65;
			retCollection.AddItem(det801);
			det801 = new c801BDetail();
			det801.YearsClaimed = 16;
			det801.ValueLimitation = 0.6;
			retCollection.AddItem(det801);
			det801 = new c801BDetail();
			det801.YearsClaimed = 17;
			det801.ValueLimitation = 0.55;
			retCollection.AddItem(det801);
			det801 = new c801BDetail();
			det801.YearsClaimed = 18;
			det801.ValueLimitation = 0.5;
			retCollection.AddItem(det801);
			cats.LoadCategories();
			strSQL = "select * from ppmaster where not deleted = 1 and account = " + FCConvert.ToString(intAccount);
			rsLoad.OpenRecordset(strSQL, modPPGN.strPPDatabase);
			if (!rsLoad.EndOfFile())
			{
				// strSQL = "select * from ppitemized where account = " & rsLoad.Fields("account") & " and rb = '*' and yearfirstassessed > 1994 and yearfirstassessed <= " & intCurrentYear - intMinYears + 1
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				strSQL = "select * from ppitemized where account = " + rsLoad.Get_Fields("account") + " and rb = '*' and yearfirstassessed > 1994 and yearsclaimed >= " + FCConvert.ToString(intMinYears);
				rsDet.OpenRecordset(strSQL, modPPGN.strPPDatabase);
				while (!rsDet.EndOfFile())
				{
					if (Conversion.Val(rsDet.Get_Fields_String("CD")) > 0)
					{
						// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
						dblValue = Conversion.Val(rsDet.Get_Fields("value"));
						if (dblValue > 0)
						{
							intCatType = cats.Get_CategoryType(FCConvert.ToInt32(Math.Round(Conversion.Val(rsDet.Get_Fields_String("CD")))));
							// intYearsClaimed = intCurrentYear - rsDet.Fields("yearfirstassessed") + 1
							intYearsClaimed = FCConvert.ToInt32(Math.Round(Conversion.Val(rsDet.Get_Fields_Int32("yearsclaimed"))));
							if (intYearsClaimed > intMaxYears)
								intYearsClaimed = intMaxYears;
							// TODO Get_Fields: Check the table for the column [sro] and replace with corresponding Get_Field method
							if (Strings.LCase(FCConvert.ToString(rsDet.Get_Fields("sro"))) == "o")
							{
								// TODO Get_Fields: Check the table for the column [cost] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [quantity] and replace with corresponding Get_Field method
								dblCost = Conversion.Val(rsDet.Get_Fields("cost")) * Conversion.Val(rsDet.Get_Fields("quantity"));
							}
							else
							{
								dblCost = 0;
							}
							det801 = (c801BDetail)retCollection.GetItemByIndex(FCConvert.ToInt16(intYearsClaimed - intMinYears + 1));
							if (!(det801 == null))
							{
								switch (intCatType)
								{
									case 1:
										{
											// ff
											det801.Furniture += dblCost;
											det801.FurnitureAssessed += dblValue;
											break;
										}
									case 2:
										{
											// me
											det801.MachineryAndEquipment += dblCost;
											det801.MachineryAndEquimpmentAssessed += dblValue;
											break;
										}
									default:
										{
											// other
											det801.Other += dblCost;
											det801.OtherAssessed += dblValue;
											break;
										}
								}
								//end switch
							}
						}
					}
					rsDet.MoveNext();
				}
				// strSQL = "select * from ppleased where account = " & rsLoad.Fields("account") & " and rb = '*' and yearfirstassessed > 1994 and yearfirstassessed < " & intCurrentYear - intMinYears + 1
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				strSQL = "select * from ppleased where account = " + rsLoad.Get_Fields("account") + " and rb = '*' and yearfirstassessed > 1994 and yearsclaimed >= " + FCConvert.ToString(intMinYears);
				rsDet.OpenRecordset(strSQL, modPPGN.strPPDatabase);
				while (!rsDet.EndOfFile())
				{
					if (Conversion.Val(rsDet.Get_Fields_String("CD")) > 0)
					{
						// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
						dblValue = Conversion.Val(rsDet.Get_Fields("value"));
						if (dblValue > 0)
						{
							intCatType = cats.Get_CategoryType(FCConvert.ToInt32(Math.Round(Conversion.Val(rsDet.Get_Fields_String("CD")))));
							// intYearsClaimed = intCurrentYear - rsDet.Fields("yearfirstassessed") + 1
							intYearsClaimed = FCConvert.ToInt32(Math.Round(Conversion.Val(rsDet.Get_Fields_Int32("yearsclaimed"))));
							if (intYearsClaimed > intMaxYears)
								intYearsClaimed = intMaxYears;
							det801 = (c801BDetail)retCollection.GetItemByIndex(FCConvert.ToInt16(intYearsClaimed - intMinYears + 1));
							if (!(det801 == null))
							{
								switch (intCatType)
								{
									case 1:
										{
											// ff
											// TODO Get_Fields: Check the table for the column [quantity] and replace with corresponding Get_Field method
											// TODO Get_Fields: Check the table for the column [cost] and replace with corresponding Get_Field method
											det801.Furniture += Conversion.Val(rsDet.Get_Fields("quantity")) * Conversion.Val(rsDet.Get_Fields("cost"));
											det801.FurnitureAssessed += dblValue;
											break;
										}
									case 2:
										{
											// me
											// TODO Get_Fields: Check the table for the column [quantity] and replace with corresponding Get_Field method
											// TODO Get_Fields: Check the table for the column [cost] and replace with corresponding Get_Field method
											det801.MachineryAndEquipment += Conversion.Val(rsDet.Get_Fields("quantity")) * Conversion.Val(rsDet.Get_Fields("cost"));
											det801.MachineryAndEquimpmentAssessed += dblValue;
											break;
										}
									default:
										{
											// other
											// TODO Get_Fields: Check the table for the column [quantity] and replace with corresponding Get_Field method
											// TODO Get_Fields: Check the table for the column [cost] and replace with corresponding Get_Field method
											det801.Other += Conversion.Val(rsDet.Get_Fields("quantity")) * Conversion.Val(rsDet.Get_Fields("cost"));
											det801.OtherAssessed += dblValue;
											break;
										}
								}
								//end switch
							}
						}
					}
					rsDet.MoveNext();
				}
			}
			retCollection.MoveFirst();
			while (retCollection.IsCurrent())
			{
				det801 = (c801BDetail)retCollection.GetCurrentItem();
				det801.FurnitureAssessed *= modGlobal.Statics.CustomizeStuff.dblRatio;
				det801.MachineryAndEquimpmentAssessed *= modGlobal.Statics.CustomizeStuff.dblRatio;
				det801.OtherAssessed *= modGlobal.Statics.CustomizeStuff.dblRatio;
				retCollection.MoveNext();
			}
			Get801BDetails = retCollection;
			return Get801BDetails;
		}

		public cGenericCollection Get801ADetails(int intAccount, int intCurrentYear)
		{
			cGenericCollection Get801ADetails = null;
			cGenericCollection retCollection = new cGenericCollection();
			int intMaxYears;
			clsDRWrapper rsLoad = new clsDRWrapper();
			clsDRWrapper rsDet = new clsDRWrapper();
			string strSQL;
			c801ADetail det801A;
			double dblValue = 0;
			int intYearsClaimed = 0;
			intMaxYears = 12;
			strSQL = "select * from ppmaster where ";
			strSQL = "select * from ppmaster where not deleted = 1 and account = " + FCConvert.ToString(intAccount);
			rsLoad.OpenRecordset(strSQL, modPPGN.strPPDatabase);
			if (!rsLoad.EndOfFile())
			{
				// strSQL = "select * from ppitemized where account = " & intAccount & " and rb = '*' and yearfirstassessed > 1994 and yearfirstassessed <= " & intCurrentYear & " and yearfirstassessed > " & intCurrentYear - intMaxYears & " order by line"
				strSQL = "select * from ppitemized where account = " + FCConvert.ToString(intAccount) + " and rb = '*' and yearfirstassessed > 1994 and yearsclaimed <= " + FCConvert.ToString(intMaxYears) + " order by line";
				rsDet.OpenRecordset(strSQL, modPPGN.strPPDatabase);
				while (!rsDet.EndOfFile())
				{
					if (Conversion.Val(rsDet.Get_Fields_String("CD")) > 0)
					{
						// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
						dblValue = Conversion.Val(rsDet.Get_Fields("value"));
						dblValue *= modGlobal.Statics.CustomizeStuff.dblRatio;
						if (dblValue > 0)
						{
							// intYearsClaimed = intCurrentYear - rsDet.Fields("yearfirstassessed") + 1
							intYearsClaimed = FCConvert.ToInt32(Math.Round(Conversion.Val(rsDet.Get_Fields_Int32("yearsclaimed"))));
							det801A = new c801ADetail();
							det801A.AssessedValue = dblValue;
							// TODO Get_Fields: Check the table for the column [sro] and replace with corresponding Get_Field method
							if (Strings.LCase(FCConvert.ToString(rsDet.Get_Fields("sro"))) == "o")
							{
								// TODO Get_Fields: Check the table for the column [cost] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [quantity] and replace with corresponding Get_Field method
								det801A.OriginalCost = Conversion.Val(rsDet.Get_Fields("cost")) * Conversion.Val(rsDet.Get_Fields("quantity"));
							}
							det801A.Description = FCConvert.ToString(rsDet.Get_Fields_String("description"));
							det801A.StateOfOrigin = "";
							det801A.NumberOfYearsClaimed = intYearsClaimed;
							// TODO Get_Fields: Check the table for the column [year] and replace with corresponding Get_Field method
							det801A.YearPlacedInService = FCConvert.ToInt32(Math.Round(Conversion.Val(rsDet.Get_Fields("year"))));
							det801A.MonthPlacedInService = FCConvert.ToInt32(Math.Round(Conversion.Val(rsDet.Get_Fields_Int16("MOnth"))));
							retCollection.AddItem(det801A);
						}
					}
					rsDet.MoveNext();
				}
				// strSQL = "select * from ppleased where account = " & intAccount & " and rb = '*' and yearfirstassessed > 1994 and yearfirstassessed > " & intCurrentYear - intMaxYears & " order by line"
				strSQL = "select * from ppleased where account = " + FCConvert.ToString(intAccount) + " and rb = '*' and yearfirstassessed > 1994 and yearsclaimed <= " + FCConvert.ToString(intMaxYears) + " order by line";
				rsDet.OpenRecordset(strSQL, modPPGN.strPPDatabase);
				while (!rsDet.EndOfFile())
				{
					if (Conversion.Val(rsDet.Get_Fields_String("cd")) > 0)
					{
						// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
						dblValue = Conversion.Val(rsDet.Get_Fields("value"));
						dblValue *= modGlobal.Statics.CustomizeStuff.dblRatio;
						if (dblValue > 0)
						{
							// intYearsClaimed = intCurrentYear - rsDet.Fields("yearfirstassessed") + 1
							intYearsClaimed = FCConvert.ToInt32(Math.Round(Conversion.Val(rsDet.Get_Fields_Int32("yearsclaimed"))));
							det801A = new c801ADetail();
							det801A.AssessedValue = dblValue;
							// TODO Get_Fields: Check the table for the column [cost] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [quantity] and replace with corresponding Get_Field method
							det801A.OriginalCost = Conversion.Val(rsDet.Get_Fields("cost")) * Conversion.Val(rsDet.Get_Fields("quantity"));
							det801A.Description = FCConvert.ToString(rsDet.Get_Fields_String("description"));
							det801A.StateOfOrigin = "";
							det801A.NumberOfYearsClaimed = intYearsClaimed;
							det801A.YearPlacedInService = 0;
							det801A.MonthPlacedInService = 0;
							retCollection.AddItem(det801A);
						}
					}
					rsDet.MoveNext();
				}
			}
			Get801ADetails = retCollection;
			return Get801ADetails;
		}
	}
}
