﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;
using System.IO;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for dlgTaxMessage.
	/// </summary>
	partial class dlgTaxMessage : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtNotice;
		public fecherFoundation.FCTextBox txtEstimatedTotal;
		public fecherFoundation.FCTextBox txtEstimated;
		public fecherFoundation.FCTextBox txtTotalValue;
		public fecherFoundation.FCTextBox txtTotal;
		public fecherFoundation.FCTextBox txtCategory9;
		public fecherFoundation.FCTextBox Text1;
		public fecherFoundation.FCTextBox Text21;
		public fecherFoundation.FCTextBox Text20;
		public fecherFoundation.FCTextBox Text19;
		public fecherFoundation.FCTextBox Text18;
		public fecherFoundation.FCTextBox txtHeading;
		public fecherFoundation.FCTextBox Text16;
		public fecherFoundation.FCTextBox txtNotice_14;
		public fecherFoundation.FCTextBox txtNotice_13;
		public fecherFoundation.FCTextBox txtNotice_12;
		public fecherFoundation.FCTextBox txtNotice_11;
		public fecherFoundation.FCTextBox txtNotice_10;
		public fecherFoundation.FCTextBox txtNotice_9;
		public fecherFoundation.FCTextBox txtNotice_8;
		public fecherFoundation.FCTextBox txtNotice_7;
		public fecherFoundation.FCTextBox txtNotice_6;
		public fecherFoundation.FCTextBox txtNotice_5;
		public fecherFoundation.FCTextBox txtNotice_4;
		public fecherFoundation.FCTextBox txtNotice_3;
		public fecherFoundation.FCTextBox txtNotice_2;
		public fecherFoundation.FCTextBox txtNotice_1;
		public fecherFoundation.FCTextBox txtNotice_0;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuClear;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuDone;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSep;
		public fecherFoundation.FCToolStripMenuItem mnuCancel;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(dlgTaxMessage));
			this.txtEstimatedTotal = new fecherFoundation.FCTextBox();
			this.txtEstimated = new fecherFoundation.FCTextBox();
			this.txtTotalValue = new fecherFoundation.FCTextBox();
			this.txtTotal = new fecherFoundation.FCTextBox();
			this.txtCategory9 = new fecherFoundation.FCTextBox();
			this.Text1 = new fecherFoundation.FCTextBox();
			this.Text21 = new fecherFoundation.FCTextBox();
			this.Text20 = new fecherFoundation.FCTextBox();
			this.Text19 = new fecherFoundation.FCTextBox();
			this.Text18 = new fecherFoundation.FCTextBox();
			this.txtHeading = new fecherFoundation.FCTextBox();
			this.Text16 = new fecherFoundation.FCTextBox();
			this.txtNotice_14 = new fecherFoundation.FCTextBox();
			this.txtNotice_13 = new fecherFoundation.FCTextBox();
			this.txtNotice_12 = new fecherFoundation.FCTextBox();
			this.txtNotice_11 = new fecherFoundation.FCTextBox();
			this.txtNotice_10 = new fecherFoundation.FCTextBox();
			this.txtNotice_9 = new fecherFoundation.FCTextBox();
			this.txtNotice_8 = new fecherFoundation.FCTextBox();
			this.txtNotice_7 = new fecherFoundation.FCTextBox();
			this.txtNotice_6 = new fecherFoundation.FCTextBox();
			this.txtNotice_5 = new fecherFoundation.FCTextBox();
			this.txtNotice_4 = new fecherFoundation.FCTextBox();
			this.txtNotice_3 = new fecherFoundation.FCTextBox();
			this.txtNotice_2 = new fecherFoundation.FCTextBox();
			this.txtNotice_1 = new fecherFoundation.FCTextBox();
			this.txtNotice_0 = new fecherFoundation.FCTextBox();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuClear = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDone = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSep = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCancel = new fecherFoundation.FCToolStripMenuItem();
			this.btnSave = new fecherFoundation.FCButton();
			this.btnClear = new fecherFoundation.FCButton();
			this.lblHeader = new fecherFoundation.FCLabel();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnClear)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 467);
			this.BottomPanel.Size = new System.Drawing.Size(820, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.lblHeader);
			this.ClientArea.Controls.Add(this.txtEstimatedTotal);
			this.ClientArea.Controls.Add(this.txtEstimated);
			this.ClientArea.Controls.Add(this.txtTotalValue);
			this.ClientArea.Controls.Add(this.txtTotal);
			this.ClientArea.Controls.Add(this.txtCategory9);
			this.ClientArea.Controls.Add(this.Text1);
			this.ClientArea.Controls.Add(this.Text21);
			this.ClientArea.Controls.Add(this.Text20);
			this.ClientArea.Controls.Add(this.Text19);
			this.ClientArea.Controls.Add(this.Text18);
			this.ClientArea.Controls.Add(this.txtHeading);
			this.ClientArea.Controls.Add(this.Text16);
			this.ClientArea.Controls.Add(this.txtNotice_14);
			this.ClientArea.Controls.Add(this.txtNotice_13);
			this.ClientArea.Controls.Add(this.txtNotice_12);
			this.ClientArea.Controls.Add(this.txtNotice_11);
			this.ClientArea.Controls.Add(this.txtNotice_10);
			this.ClientArea.Controls.Add(this.txtNotice_9);
			this.ClientArea.Controls.Add(this.txtNotice_8);
			this.ClientArea.Controls.Add(this.txtNotice_7);
			this.ClientArea.Controls.Add(this.txtNotice_6);
			this.ClientArea.Controls.Add(this.txtNotice_5);
			this.ClientArea.Controls.Add(this.txtNotice_4);
			this.ClientArea.Controls.Add(this.txtNotice_3);
			this.ClientArea.Controls.Add(this.txtNotice_2);
			this.ClientArea.Controls.Add(this.txtNotice_1);
			this.ClientArea.Controls.Add(this.txtNotice_0);
			this.ClientArea.Size = new System.Drawing.Size(820, 407);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.btnClear);
			this.TopPanel.Size = new System.Drawing.Size(820, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.btnClear, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(186, 30);
			this.HeaderText.Text = "Personal Property Tax Notice";
			// 
			// txtEstimatedTotal
			// 
			this.txtEstimatedTotal.AutoSize = false;
			this.txtEstimatedTotal.BackColor = System.Drawing.SystemColors.Window;
			this.txtEstimatedTotal.Enabled = false;
			this.txtEstimatedTotal.LinkItem = null;
			this.txtEstimatedTotal.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtEstimatedTotal.LinkTopic = null;
			this.txtEstimatedTotal.Location = new System.Drawing.Point(566, 975);
			this.txtEstimatedTotal.Name = "txtEstimatedTotal";
			this.txtEstimatedTotal.Size = new System.Drawing.Size(145, 40);
			this.txtEstimatedTotal.TabIndex = 27;
			this.txtEstimatedTotal.Text = "Text2";
			this.txtEstimatedTotal.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtEstimatedTotal.Visible = false;
			// 
			// txtEstimated
			// 
			this.txtEstimated.AutoSize = false;
			this.txtEstimated.BackColor = System.Drawing.SystemColors.Window;
			this.txtEstimated.Enabled = false;
			this.txtEstimated.LinkItem = null;
			this.txtEstimated.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtEstimated.LinkTopic = null;
			this.txtEstimated.Location = new System.Drawing.Point(267, 975);
			this.txtEstimated.Name = "txtEstimated";
			this.txtEstimated.Size = new System.Drawing.Size(294, 40);
			this.txtEstimated.TabIndex = 26;
			this.txtEstimated.Text = "Text4";
			this.txtEstimated.Visible = false;
			// 
			// txtTotalValue
			// 
			this.txtTotalValue.AutoSize = false;
			this.txtTotalValue.BackColor = System.Drawing.SystemColors.Window;
			this.txtTotalValue.Enabled = false;
			this.txtTotalValue.LinkItem = null;
			this.txtTotalValue.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtTotalValue.LinkTopic = null;
			this.txtTotalValue.Location = new System.Drawing.Point(566, 925);
			this.txtTotalValue.Name = "txtTotalValue";
			this.txtTotalValue.Size = new System.Drawing.Size(145, 40);
			this.txtTotalValue.TabIndex = 25;
			this.txtTotalValue.Text = "00,000";
			this.txtTotalValue.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtTotalValue.Visible = false;
			// 
			// txtTotal
			// 
			this.txtTotal.AutoSize = false;
			this.txtTotal.BackColor = System.Drawing.SystemColors.Window;
			this.txtTotal.Enabled = false;
			this.txtTotal.LinkItem = null;
			this.txtTotal.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtTotal.LinkTopic = null;
			this.txtTotal.Location = new System.Drawing.Point(267, 925);
			this.txtTotal.Name = "txtTotal";
			this.txtTotal.Size = new System.Drawing.Size(294, 40);
			this.txtTotal.TabIndex = 24;
			this.txtTotal.Text = "Total:";
			this.txtTotal.Visible = false;
			// 
			// txtCategory9
			// 
			this.txtCategory9.AutoSize = false;
			this.txtCategory9.BackColor = System.Drawing.SystemColors.Window;
			this.txtCategory9.Enabled = false;
			this.txtCategory9.LinkItem = null;
			this.txtCategory9.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCategory9.LinkTopic = null;
			this.txtCategory9.Location = new System.Drawing.Point(267, 875);
			this.txtCategory9.Name = "txtCategory9";
			this.txtCategory9.Size = new System.Drawing.Size(443, 40);
			this.txtCategory9.TabIndex = 23;
			this.txtCategory9.Visible = false;
			// 
			// Text1
			// 
			this.Text1.AutoSize = false;
			this.Text1.BackColor = System.Drawing.SystemColors.Window;
			this.Text1.Enabled = false;
			this.Text1.LinkItem = null;
			this.Text1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.Text1.LinkTopic = null;
			this.Text1.Location = new System.Drawing.Point(30, 475);
			this.Text1.Name = "Text1";
			this.Text1.Size = new System.Drawing.Size(199, 40);
			this.Text1.TabIndex = 22;
			this.Text1.Text = "Acct 000000";
			// 
			// Text21
			// 
			this.Text21.AutoSize = false;
			this.Text21.BackColor = System.Drawing.SystemColors.Window;
			this.Text21.Enabled = false;
			this.Text21.LinkItem = null;
			this.Text21.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.Text21.LinkTopic = null;
			this.Text21.Location = new System.Drawing.Point(30, 725);
			this.Text21.Name = "Text21";
			this.Text21.Size = new System.Drawing.Size(199, 40);
			this.Text21.TabIndex = 21;
			this.Text21.Text = "City, State  Zip";
			// 
			// Text20
			// 
			this.Text20.AutoSize = false;
			this.Text20.BackColor = System.Drawing.SystemColors.Window;
			this.Text20.Enabled = false;
			this.Text20.LinkItem = null;
			this.Text20.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.Text20.LinkTopic = null;
			this.Text20.Location = new System.Drawing.Point(30, 675);
			this.Text20.Name = "Text20";
			this.Text20.Size = new System.Drawing.Size(199, 40);
			this.Text20.TabIndex = 20;
			this.Text20.Text = "Address 2";
			// 
			// Text19
			// 
			this.Text19.AutoSize = false;
			this.Text19.BackColor = System.Drawing.SystemColors.Window;
			this.Text19.Enabled = false;
			this.Text19.LinkItem = null;
			this.Text19.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.Text19.LinkTopic = null;
			this.Text19.Location = new System.Drawing.Point(30, 625);
			this.Text19.Name = "Text19";
			this.Text19.Size = new System.Drawing.Size(199, 40);
			this.Text19.TabIndex = 19;
			this.Text19.Text = "Address 1";
			// 
			// Text18
			// 
			this.Text18.AutoSize = false;
			this.Text18.BackColor = System.Drawing.SystemColors.Window;
			this.Text18.Enabled = false;
			this.Text18.LinkItem = null;
			this.Text18.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.Text18.LinkTopic = null;
			this.Text18.Location = new System.Drawing.Point(30, 575);
			this.Text18.Name = "Text18";
			this.Text18.Size = new System.Drawing.Size(199, 40);
			this.Text18.TabIndex = 18;
			this.Text18.Text = "Name";
			// 
			// txtHeading
			// 
			this.txtHeading.AutoSize = false;
			this.txtHeading.BackColor = System.Drawing.SystemColors.Window;
			this.txtHeading.LinkItem = null;
			this.txtHeading.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtHeading.LinkTopic = null;
			this.txtHeading.Location = new System.Drawing.Point(30, 59);
			this.txtHeading.MaxLength = 75;
			this.txtHeading.Name = "txtHeading";
			this.txtHeading.Size = new System.Drawing.Size(680, 40);
			this.txtHeading.TabIndex = 0;
			this.txtHeading.KeyDown += new Wisej.Web.KeyEventHandler(this.txtHeading_KeyDown);
			this.txtHeading.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtHeading_KeyPress);
			this.txtHeading.Enter += new System.EventHandler(this.txtHeading_Enter);
			// 
			// Text16
			// 
			this.Text16.AutoSize = false;
			this.Text16.BackColor = System.Drawing.SystemColors.Window;
			this.Text16.Enabled = false;
			this.Text16.LinkItem = null;
			this.Text16.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.Text16.LinkTopic = null;
			this.Text16.Location = new System.Drawing.Point(30, 525);
			this.Text16.Name = "Text16";
			this.Text16.Size = new System.Drawing.Size(199, 40);
			this.Text16.TabIndex = 16;
			this.Text16.Text = "Location";
			// 
			// txtNotice_14
			// 
			this.txtNotice_14.AutoSize = false;
			this.txtNotice_14.BackColor = System.Drawing.SystemColors.Window;
			this.txtNotice_14.LinkItem = null;
			this.txtNotice_14.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNotice_14.LinkTopic = null;
			this.txtNotice_14.Location = new System.Drawing.Point(267, 825);
			this.txtNotice_14.MaxLength = 75;
			this.txtNotice_14.Name = "txtNotice_14";
			this.txtNotice_14.Size = new System.Drawing.Size(443, 40);
			this.txtNotice_14.TabIndex = 15;
			this.txtNotice_14.KeyDown += new Wisej.Web.KeyEventHandler(this.txtNotice_KeyDown);
			this.txtNotice_14.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtNotice_KeyPress);
			this.txtNotice_14.Enter += new System.EventHandler(this.txtNotice_Enter);
			// 
			// txtNotice_13
			// 
			this.txtNotice_13.AutoSize = false;
			this.txtNotice_13.BackColor = System.Drawing.SystemColors.Window;
			this.txtNotice_13.LinkItem = null;
			this.txtNotice_13.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNotice_13.LinkTopic = null;
			this.txtNotice_13.Location = new System.Drawing.Point(267, 775);
			this.txtNotice_13.MaxLength = 75;
			this.txtNotice_13.Name = "txtNotice_13";
			this.txtNotice_13.Size = new System.Drawing.Size(443, 40);
			this.txtNotice_13.TabIndex = 14;
			this.txtNotice_13.KeyDown += new Wisej.Web.KeyEventHandler(this.txtNotice_KeyDown);
			this.txtNotice_13.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtNotice_KeyPress);
			this.txtNotice_13.Enter += new System.EventHandler(this.txtNotice_Enter);
			// 
			// txtNotice_12
			// 
			this.txtNotice_12.AutoSize = false;
			this.txtNotice_12.BackColor = System.Drawing.SystemColors.Window;
			this.txtNotice_12.LinkItem = null;
			this.txtNotice_12.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNotice_12.LinkTopic = null;
			this.txtNotice_12.Location = new System.Drawing.Point(267, 725);
			this.txtNotice_12.MaxLength = 75;
			this.txtNotice_12.Name = "txtNotice_12";
			this.txtNotice_12.Size = new System.Drawing.Size(443, 40);
			this.txtNotice_12.TabIndex = 13;
			this.txtNotice_12.KeyDown += new Wisej.Web.KeyEventHandler(this.txtNotice_KeyDown);
			this.txtNotice_12.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtNotice_KeyPress);
			this.txtNotice_12.Enter += new System.EventHandler(this.txtNotice_Enter);
			// 
			// txtNotice_11
			// 
			this.txtNotice_11.AutoSize = false;
			this.txtNotice_11.BackColor = System.Drawing.SystemColors.Window;
			this.txtNotice_11.LinkItem = null;
			this.txtNotice_11.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNotice_11.LinkTopic = null;
			this.txtNotice_11.Location = new System.Drawing.Point(267, 675);
			this.txtNotice_11.MaxLength = 75;
			this.txtNotice_11.Name = "txtNotice_11";
			this.txtNotice_11.Size = new System.Drawing.Size(443, 40);
			this.txtNotice_11.TabIndex = 12;
			this.txtNotice_11.KeyDown += new Wisej.Web.KeyEventHandler(this.txtNotice_KeyDown);
			this.txtNotice_11.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtNotice_KeyPress);
			this.txtNotice_11.Enter += new System.EventHandler(this.txtNotice_Enter);
			// 
			// txtNotice_10
			// 
			this.txtNotice_10.AutoSize = false;
			this.txtNotice_10.BackColor = System.Drawing.SystemColors.Window;
			this.txtNotice_10.LinkItem = null;
			this.txtNotice_10.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNotice_10.LinkTopic = null;
			this.txtNotice_10.Location = new System.Drawing.Point(267, 625);
			this.txtNotice_10.MaxLength = 75;
			this.txtNotice_10.Name = "txtNotice_10";
			this.txtNotice_10.Size = new System.Drawing.Size(443, 40);
			this.txtNotice_10.TabIndex = 11;
			this.txtNotice_10.KeyDown += new Wisej.Web.KeyEventHandler(this.txtNotice_KeyDown);
			this.txtNotice_10.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtNotice_KeyPress);
			this.txtNotice_10.Enter += new System.EventHandler(this.txtNotice_Enter);
			// 
			// txtNotice_9
			// 
			this.txtNotice_9.AutoSize = false;
			this.txtNotice_9.BackColor = System.Drawing.SystemColors.Window;
			this.txtNotice_9.LinkItem = null;
			this.txtNotice_9.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNotice_9.LinkTopic = null;
			this.txtNotice_9.Location = new System.Drawing.Point(267, 575);
			this.txtNotice_9.MaxLength = 75;
			this.txtNotice_9.Name = "txtNotice_9";
			this.txtNotice_9.Size = new System.Drawing.Size(443, 40);
			this.txtNotice_9.TabIndex = 10;
			this.txtNotice_9.KeyDown += new Wisej.Web.KeyEventHandler(this.txtNotice_KeyDown);
			this.txtNotice_9.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtNotice_KeyPress);
			this.txtNotice_9.Enter += new System.EventHandler(this.txtNotice_Enter);
			// 
			// txtNotice_8
			// 
			this.txtNotice_8.AutoSize = false;
			this.txtNotice_8.BackColor = System.Drawing.SystemColors.Window;
			this.txtNotice_8.LinkItem = null;
			this.txtNotice_8.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNotice_8.LinkTopic = null;
			this.txtNotice_8.Location = new System.Drawing.Point(267, 525);
			this.txtNotice_8.MaxLength = 75;
			this.txtNotice_8.Name = "txtNotice_8";
			this.txtNotice_8.Size = new System.Drawing.Size(443, 40);
			this.txtNotice_8.TabIndex = 9;
			this.txtNotice_8.KeyDown += new Wisej.Web.KeyEventHandler(this.txtNotice_KeyDown);
			this.txtNotice_8.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtNotice_KeyPress);
			this.txtNotice_8.Enter += new System.EventHandler(this.txtNotice_Enter);
			// 
			// txtNotice_7
			// 
			this.txtNotice_7.AutoSize = false;
			this.txtNotice_7.BackColor = System.Drawing.SystemColors.Window;
			this.txtNotice_7.LinkItem = null;
			this.txtNotice_7.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNotice_7.LinkTopic = null;
			this.txtNotice_7.Location = new System.Drawing.Point(267, 475);
			this.txtNotice_7.MaxLength = 40;
			this.txtNotice_7.Name = "txtNotice_7";
			this.txtNotice_7.Size = new System.Drawing.Size(443, 40);
			this.txtNotice_7.TabIndex = 8;
			this.txtNotice_7.KeyDown += new Wisej.Web.KeyEventHandler(this.txtNotice_KeyDown);
			this.txtNotice_7.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtNotice_KeyPress);
			this.txtNotice_7.Enter += new System.EventHandler(this.txtNotice_Enter);
			// 
			// txtNotice_6
			// 
			this.txtNotice_6.AutoSize = false;
			this.txtNotice_6.BackColor = System.Drawing.SystemColors.Window;
			this.txtNotice_6.LinkItem = null;
			this.txtNotice_6.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNotice_6.LinkTopic = null;
			this.txtNotice_6.Location = new System.Drawing.Point(31, 421);
			this.txtNotice_6.MaxLength = 40;
			this.txtNotice_6.Name = "txtNotice_6";
			this.txtNotice_6.Size = new System.Drawing.Size(678, 40);
			this.txtNotice_6.TabIndex = 7;
			this.txtNotice_6.KeyDown += new Wisej.Web.KeyEventHandler(this.txtNotice_KeyDown);
			this.txtNotice_6.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtNotice_KeyPress);
			this.txtNotice_6.Enter += new System.EventHandler(this.txtNotice_Enter);
			// 
			// txtNotice_5
			// 
			this.txtNotice_5.AutoSize = false;
			this.txtNotice_5.BackColor = System.Drawing.SystemColors.Window;
			this.txtNotice_5.LinkItem = null;
			this.txtNotice_5.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNotice_5.LinkTopic = null;
			this.txtNotice_5.Location = new System.Drawing.Point(31, 371);
			this.txtNotice_5.MaxLength = 40;
			this.txtNotice_5.Name = "txtNotice_5";
			this.txtNotice_5.Size = new System.Drawing.Size(678, 40);
			this.txtNotice_5.TabIndex = 6;
			this.txtNotice_5.KeyDown += new Wisej.Web.KeyEventHandler(this.txtNotice_KeyDown);
			this.txtNotice_5.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtNotice_KeyPress);
			this.txtNotice_5.Enter += new System.EventHandler(this.txtNotice_Enter);
			// 
			// txtNotice_4
			// 
			this.txtNotice_4.AutoSize = false;
			this.txtNotice_4.BackColor = System.Drawing.SystemColors.Window;
			this.txtNotice_4.LinkItem = null;
			this.txtNotice_4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNotice_4.LinkTopic = null;
			this.txtNotice_4.Location = new System.Drawing.Point(31, 321);
			this.txtNotice_4.MaxLength = 40;
			this.txtNotice_4.Name = "txtNotice_4";
			this.txtNotice_4.Size = new System.Drawing.Size(678, 40);
			this.txtNotice_4.TabIndex = 5;
			this.txtNotice_4.KeyDown += new Wisej.Web.KeyEventHandler(this.txtNotice_KeyDown);
			this.txtNotice_4.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtNotice_KeyPress);
			this.txtNotice_4.Enter += new System.EventHandler(this.txtNotice_Enter);
			// 
			// txtNotice_3
			// 
			this.txtNotice_3.AutoSize = false;
			this.txtNotice_3.BackColor = System.Drawing.SystemColors.Window;
			this.txtNotice_3.LinkItem = null;
			this.txtNotice_3.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNotice_3.LinkTopic = null;
			this.txtNotice_3.Location = new System.Drawing.Point(31, 271);
			this.txtNotice_3.MaxLength = 40;
			this.txtNotice_3.Name = "txtNotice_3";
			this.txtNotice_3.Size = new System.Drawing.Size(678, 40);
			this.txtNotice_3.TabIndex = 4;
			this.txtNotice_3.KeyDown += new Wisej.Web.KeyEventHandler(this.txtNotice_KeyDown);
			this.txtNotice_3.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtNotice_KeyPress);
			this.txtNotice_3.Enter += new System.EventHandler(this.txtNotice_Enter);
			// 
			// txtNotice_2
			// 
			this.txtNotice_2.AutoSize = false;
			this.txtNotice_2.BackColor = System.Drawing.SystemColors.Window;
			this.txtNotice_2.LinkItem = null;
			this.txtNotice_2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNotice_2.LinkTopic = null;
			this.txtNotice_2.Location = new System.Drawing.Point(31, 221);
			this.txtNotice_2.MaxLength = 40;
			this.txtNotice_2.Name = "txtNotice_2";
			this.txtNotice_2.Size = new System.Drawing.Size(678, 40);
			this.txtNotice_2.TabIndex = 3;
			this.txtNotice_2.KeyDown += new Wisej.Web.KeyEventHandler(this.txtNotice_KeyDown);
			this.txtNotice_2.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtNotice_KeyPress);
			this.txtNotice_2.Enter += new System.EventHandler(this.txtNotice_Enter);
			// 
			// txtNotice_1
			// 
			this.txtNotice_1.AutoSize = false;
			this.txtNotice_1.BackColor = System.Drawing.SystemColors.Window;
			this.txtNotice_1.LinkItem = null;
			this.txtNotice_1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNotice_1.LinkTopic = null;
			this.txtNotice_1.Location = new System.Drawing.Point(31, 171);
			this.txtNotice_1.MaxLength = 40;
			this.txtNotice_1.Name = "txtNotice_1";
			this.txtNotice_1.Size = new System.Drawing.Size(678, 40);
			this.txtNotice_1.TabIndex = 2;
			this.txtNotice_1.KeyDown += new Wisej.Web.KeyEventHandler(this.txtNotice_KeyDown);
			this.txtNotice_1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtNotice_KeyPress);
			this.txtNotice_1.Enter += new System.EventHandler(this.txtNotice_Enter);
			// 
			// txtNotice_0
			// 
			this.txtNotice_0.AutoSize = false;
			this.txtNotice_0.BackColor = System.Drawing.SystemColors.Window;
			this.txtNotice_0.LinkItem = null;
			this.txtNotice_0.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNotice_0.LinkTopic = null;
			this.txtNotice_0.Location = new System.Drawing.Point(30, 121);
			this.txtNotice_0.MaxLength = 40;
			this.txtNotice_0.Name = "txtNotice_0";
			this.txtNotice_0.Size = new System.Drawing.Size(678, 40);
			this.txtNotice_0.TabIndex = 1;
			this.txtNotice_0.KeyDown += new Wisej.Web.KeyEventHandler(this.txtNotice_KeyDown);
			this.txtNotice_0.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtNotice_KeyPress);
			this.txtNotice_0.Enter += new System.EventHandler(this.txtNotice_Enter);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuClear,
				this.mnuSepar,
				this.mnuDone,
				this.mnuSaveExit,
				this.mnuSep,
				this.mnuCancel
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuClear
			// 
			this.mnuClear.Index = 0;
			this.mnuClear.Name = "mnuClear";
			this.mnuClear.Text = "New";
			this.mnuClear.Click += new System.EventHandler(this.mnuClear_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 1;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuDone
			// 
			this.mnuDone.Index = 2;
			this.mnuDone.Name = "mnuDone";
			this.mnuDone.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuDone.Text = "Save";
			this.mnuDone.Click += new System.EventHandler(this.mnuDone_Click);
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Index = 3;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Text = "Save & Continue";
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// mnuSep
			// 
			this.mnuSep.Index = 4;
			this.mnuSep.Name = "mnuSep";
			this.mnuSep.Text = "-";
			// 
			// mnuCancel
			// 
			this.mnuCancel.Index = 5;
			this.mnuCancel.Name = "mnuCancel";
			this.mnuCancel.Text = "Exit";
			this.mnuCancel.Click += new System.EventHandler(this.mnuCancel_Click);
			// 
			// btnSave
			// 
			this.btnSave.AppearanceKey = "acceptButton";
			this.btnSave.Location = new System.Drawing.Point(303, 30);
			this.btnSave.Name = "btnSave";
			this.btnSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnSave.Size = new System.Drawing.Size(100, 48);
			this.btnSave.TabIndex = 0;
			this.btnSave.Text = "Save";
			this.btnSave.Click += new System.EventHandler(this.mnuDone_Click);
			// 
			// btnClear
			// 
			this.btnClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.btnClear.AppearanceKey = "toolbarButton";
			this.btnClear.Location = new System.Drawing.Point(743, 29);
			this.btnClear.Name = "btnClear";
			this.btnClear.Size = new System.Drawing.Size(47, 25);
			this.btnClear.TabIndex = 1;
			this.btnClear.Text = "New";
			this.btnClear.Click += new System.EventHandler(this.mnuClear_Click);
			// 
			// lblHeader
			// 
			this.lblHeader.Font = new System.Drawing.Font("default", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.lblHeader.Location = new System.Drawing.Point(30, 30);
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Size = new System.Drawing.Size(147, 18);
			this.lblHeader.TabIndex = 28;
			this.lblHeader.Text = "HEADING OR TITLE";
			// 
			// dlgTaxMessage
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(820, 575);
			this.FillColor = 0;
			this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "dlgTaxMessage";
			this.ShowInTaskbar = false;
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Personal Property Tax Notice";
			this.Load += new System.EventHandler(this.dlgTaxMessage_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.dlgTaxMessage_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnClear)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton btnSave;
		private FCButton btnClear;
		private FCLabel lblHeader;
	}
}
