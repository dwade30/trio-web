﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptExemptAudit.
	/// </summary>
	public partial class rptExemptAudit : BaseSectionReport
	{
		public rptExemptAudit()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Exempt Audit";
		}

		public static rptExemptAudit InstancePtr
		{
			get
			{
				return (rptExemptAudit)Sys.GetInstance(typeof(rptExemptAudit));
			}
		}

		protected rptExemptAudit _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsReport?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptExemptAudit	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private bool boolShowDetail;
		clsDRWrapper rsReport = new clsDRWrapper();
		double dblTotProperty;
		double dblSubProperty;
		double dblTotExempt;
		double dblSubExempt;
		double dblTotAssess;
		double dblSubAssess;
		// vbPorter upgrade warning: intOrder As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intTownNumber As short	OnWriteFCConvert.ToInt32(
		public void Init(bool boolDetail, int intOrder, string strList, int intTownNumber)
		{
			string strSQL;
			strSQL = "SElect * from (select * from ";
			strSQL += "(select account,partyid,[value],exemption,exemption1 as exemptamount,exemptcode1 as exemptcode from ppmaster where not deleted = 1 and exemption1 > 0 ";
			// strSQL = "select * from ppmaster where not deleted and (exemption1 > 0 or exemption2 > 0) "
			if (intTownNumber > 0)
			{
				strSQL += " and trancode = " + FCConvert.ToString(intTownNumber);
			}
			strSQL += ") tbl1";
			strSQL += " union all ";
			strSQL += "(select account,partyid,[value],exemption,exemption2 as exemptamount,exemptcode2 as exemptcode from ppmaster where not deleted = 1 and exemption2 > 0 ";
			if (intTownNumber > 0)
			{
				strSQL += " and trancode = " + FCConvert.ToString(intTownNumber);
			}
			strSQL += ")) tbl2 ";
			if (strList != "")
			{
				strSQL += " where exemptcode in (" + strList + ") ";
			}
			if (intOrder == 0)
			{
				strSQL += " order by exemptcode,account";
			}
			else
			{
				strSQL += " order by exemptcode,name,account";
			}
			rsReport.OpenRecordset(strSQL, modPPGN.strPPDatabase);
			if (rsReport.EndOfFile())
			{
				MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			if (!boolDetail)
			{
				Detail.Visible = false;
				Field2.Visible = false;
				Field3.Visible = false;
			}
			else
			{
				Detail.Visible = true;
				Field2.Visible = true;
				Field3.Visible = true;
			}
			frmReportViewer.InstancePtr.Init(this);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsReport.EndOfFile();
			if (!rsReport.EndOfFile())
			{
				// TODO Get_Fields: Field [exemptcode] not found!! (maybe it is an alias?)
				//GroupHeader1.DataField = FCConvert.ToString(rsReport.Get_Fields("exemptcode"));
				this.Fields["grpHeader"].Value = FCConvert.ToString(rsReport.Get_Fields("exemptcode"));
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			dblTotExempt = 0;
			dblTotProperty = 0;
			dblSubProperty = 0;
			dblSubExempt = 0;
			dblTotAssess = 0;
			dblSubAssess = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsReport.EndOfFile())
			{
				cPartyController tPC = new cPartyController();
				cParty tP;
				tP = tPC.GetParty(FCConvert.ToInt32(Conversion.Val(rsReport.Get_Fields_Int32("partyid"))), true);
				if (!(tP == null))
				{
					txtName.Text = tP.FullNameLastFirst;
				}
				else
				{
					txtName.Text = "";
				}
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				txtAccount.Text = FCConvert.ToString(rsReport.Get_Fields("account"));
				// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
				txtProperty.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("value")), "#,###,###,##0");
				// TODO Get_Fields: Field [exemptamount] not found!! (maybe it is an alias?)
				txtExemption.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("exemptamount")), "#,###,###,##0");
				// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
				txtAssessment.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("value")) - Conversion.Val(rsReport.Get_Fields_Int32("exemption")), "#,###,###,##0");
				// TODO Get_Fields: Field [exemptamount] not found!! (maybe it is an alias?)
				dblSubExempt += Conversion.Val(rsReport.Get_Fields("exemptamount"));
				// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
				dblSubProperty += Conversion.Val(rsReport.Get_Fields("value"));
				// TODO Get_Fields: Field [exemptamount] not found!! (maybe it is an alias?)
				dblTotExempt += Conversion.Val(rsReport.Get_Fields("exemptamount"));
				// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
				dblTotProperty += Conversion.Val(rsReport.Get_Fields("value"));
				// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
				dblSubAssess += Conversion.Val(rsReport.Get_Fields("value")) - Conversion.Val(rsReport.Get_Fields_Int32("exemption"));
				// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
				dblTotAssess += Conversion.Val(rsReport.Get_Fields("value")) - Conversion.Val(rsReport.Get_Fields_Int32("exemption"));
				rsReport.MoveNext();
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			txtSubExempt.Text = Strings.Format(dblSubExempt, "#,###,###,##0");
			txtSubProperty.Text = Strings.Format(dblSubProperty, "#,###,###,##0");
			txtSubAssess.Text = Strings.Format(dblSubAssess, "#,###,###,##0");
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			//rsLoad.OpenRecordset("select description from exemptcodes where code = " + FCConvert.ToString(Conversion.Val(GroupHeader1.DataField)), modPPGN.strPPDatabase);
			rsLoad.OpenRecordset("select description from exemptcodes where code = " + FCConvert.ToString(Conversion.Val(this.Fields["grpHeader"].Value)), modPPGN.strPPDatabase);
			if (!rsLoad.EndOfFile())
			{
				//txtGroup.Text = GroupHeader1.DataField + " " + rsLoad.Get_Fields_String("description");
				txtGroup.Text = this.Fields["grpHeader"].Value + " " + rsLoad.Get_Fields_String("description");
			}
			dblSubExempt = 0;
			dblSubProperty = 0;
			dblSubAssess = 0;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtTotAssessment.Text = Strings.Format(dblTotAssess, "#,###,###,##0");
			txtTotExempt.Text = Strings.Format(dblTotExempt, "#,###,###,##0");
			txtTotProperty.Text = Strings.Format(dblTotProperty, "#,###,###,##0");
		}

		

		private void rptExemptAudit_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("grpHeader");
		}
	}
}
