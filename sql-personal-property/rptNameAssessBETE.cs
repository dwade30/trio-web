﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptNameAssessBETE.
	/// </summary>
	public partial class rptNameAssessBETE : BaseSectionReport
	{
		public rptNameAssessBETE()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Name / Assess / BETE";
		}

		public static rptNameAssessBETE InstancePtr
		{
			get
			{
				return (rptNameAssessBETE)Sys.GetInstance(typeof(rptNameAssessBETE));
			}
		}

		protected rptNameAssessBETE _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsLoad_AutoInitialized?.Dispose();
                rsLoad_AutoInitialized = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptNameAssessBETE	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private int intPage;
		private int intAccount;
		private bool boolPrint;
		private string strName = "";
		private string strLocation = "";
		private double dblTotal;
		private int lngCount;
		private double dblAssess;
		private double dblBETE;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsDRWrapper rsLoad = new clsDRWrapper();
		private clsDRWrapper rsLoad_AutoInitialized;

		private clsDRWrapper rsLoad
		{
			get
			{
				if (rsLoad_AutoInitialized == null)
				{
					rsLoad_AutoInitialized = new clsDRWrapper();
				}
				return rsLoad_AutoInitialized;
			}
			set
			{
				rsLoad_AutoInitialized = value;
			}
		}

		private double dblTAssess;
		private double dblTBETE;
		private int x;

		public void Init(ref string strSQL)
		{
			rsLoad.OpenRecordset(strSQL, "twpp0000.vb1");
			if (rsLoad.EndOfFile())
			{
				MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Close();
				return;
			}
			frmReportViewer.InstancePtr.Init(rptObj: this, strFileTitle: "BETE and Assessments", strAttachmentName: "BETERPT");
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			txttime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsLoad.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lngCount = 0;
			dblTotal = 0;
			dblBETE = 0;
			dblAssess = 0;
			intPage = 1;
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtPage.Text = "Page 1";
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsLoad.EndOfFile())
			{
				txtName.Text = rsLoad.Get_Fields_String("name");
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				txtAccount.Text = FCConvert.ToString(rsLoad.Get_Fields("account"));
				lngCount += 1;
				// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
				dblTAssess = Conversion.Val(rsLoad.Get_Fields("value"));
				dblAssess += dblTAssess;
				txtAssessment.Text = Strings.Format(dblTAssess, "###,###,###,##0");
				dblTBETE = 0;
				for (x = 1; x <= 9; x++)
				{
					// TODO Get_Fields: Field [cat] not found!! (maybe it is an alias?)
					dblTBETE += Conversion.Val(rsLoad.Get_Fields("cat" + FCConvert.ToString(x) + "Exempt"));
				}
				// x
				dblBETE += dblTBETE;
				txtBETE.Text = Strings.Format(dblTBETE, "#,###,###,##0");
				txtTotal.Text = Strings.Format(dblTBETE + dblTAssess, "#,###,###,###,##0");
				rsLoad.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + FCConvert.ToString(intPage);
			intPage += 1;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtCount.Text = Strings.Format(lngCount, "#,###,###,##0");
			dblTotal = dblAssess + dblBETE;
			txtTotalAssess.Text = Strings.Format(dblAssess, "#,###,###,###,##0");
			txtTotalTotal.Text = Strings.Format(dblTotal, "#,###,###,###,##0");
			txtTotalBETE.Text = Strings.Format(dblBETE, "#,###,###,###,##0");
		}

		
	}
}
