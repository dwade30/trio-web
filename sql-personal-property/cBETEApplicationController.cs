﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPP0000
{
	public class cBETEApplicationController
	{
		//=========================================================
		cPartyController partCont = new cPartyController();

		public void LoadReport(ref cBETEApplication cReport)
		{
		}

		public void LoadRange(ref cBETEApplicationReport cReport)
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			clsDRWrapper rsAccount = new clsDRWrapper();
			string strSQL = "";
			string strDetail;
			int intYear;
			int intEffYear;
			string strWhere;
			string strOrderBy;
			string strOwnerField = "";
			string strBusinessField = "";
			clsDRWrapper rsBusCode = new clsDRWrapper();
			clsDRWrapper rsDetails = new clsDRWrapper();
			bool boolPending;
			bool boolCurrentOnly;
			string strLeased;
			cReport.BETEApplications.ClearList();
			intYear = cReport.TaxYear;
			intEffYear = cReport.EffectiveYear;
			strWhere = cReport.WhereClause;
			strOrderBy = cReport.OrderByClause;
			boolPending = cReport.Pending;
			boolCurrentOnly = cReport.CurrentBETEOnly;
			if (intYear == 0)
				intYear = DateTime.Today.Year;
			if (intEffYear == 0)
				intEffYear = DateTime.Today.Year;
			rsBusCode.OpenRecordset("select * from businesscodes order by businesscode", modPPGN.strPPDatabase);
			strDetail = "select [description],exemptyear,[month] as monthnew, [year] as yearnew,[value],beteexempt,cost,quantity, '' as LeaseDate from ppitemized ";
			strLeased = "Select [description],exemptyear,0 as monthnew,yearfirstassessed as yearnew,[value],beteexempt,cost,quantity,LeaseDate from ppleased ";
			if (boolPending)
			{
				strSQL = "Select account from (select account from ppitemized where rb = 'P' union all select account from ppleased where rb = 'P') tbl1 group by account";
				// strSQL = "select account from ppitemized  where (RB = 'P')  group by account"
				strDetail += " where (RB = 'P') ";
				strLeased += " where (RB = 'P') ";
			}
			else if (boolCurrentOnly)
			{
				// strSQL = "select account from ppitemized where exemptyear = " & intYear & " group by account"
				strSQL = "Select account from (select account from ppitemized where exemptyear = " + FCConvert.ToString(intYear) + "  union all select account from ppleased where exemptyear = " + FCConvert.ToString(intYear) + ") tbl1 group by account";
				strDetail += " where exemptyear = " + FCConvert.ToString(intYear) + " ";
				strLeased += " where exemptyear = " + FCConvert.ToString(intYear) + " ";
			}
			else
			{
				// strSQL = "select account from ppitemized where (RB = 'P' or exemptyear >= 2008)  group by account"
				strSQL = "Select account from (select account from ppitemized where (rb = 'P' or exemptyear >= 2008) union all select account from ppleased where (rb = 'P' or exemptyear >= 2008)) tbl1 group by account";
				strDetail += " where (RB = 'P' or exemptyear >= 2008) ";
				strLeased += " where (RB = 'P' or exemptyear >= 2008) ";
				// strSQL = " select ppmaster.account from ppmaster inner join (select account from ppitemized where (RB = 'P' veor exemptyear >= 2008)  group by account) as tbl1 on (tbl1.account = ppmaster.account)  where not deleted " & strWhere & " order by " & strOrderBy
			}
			rsAccount.OpenRecordset(strSQL, modPPGN.strPPDatabase);
			switch (cReport.OwnerSource)
			{
				case 0:
					{
						strOwnerField = "name";
						break;
					}
				case 1:
					{
						strOwnerField = "open1";
						break;
					}
				case 2:
					{
						strOwnerField = "open2";
						// Case 3
						// strOwnerField = "address1"
						// Case 4
						// strOwnerField = ""
						break;
					}
				default:
					{
						strOwnerField = "";
						break;
					}
			}
			//end switch
			switch (cReport.BusinessSource)
			{
				case 0:
					{
						strBusinessField = "name";
						break;
					}
				case 1:
					{
						strBusinessField = "open1";
						break;
					}
				case 2:
					{
						strBusinessField = "open2";
						// Case 3
						// strBusinessField = "address1"
						break;
					}
				default:
					{
						strBusinessField = "";
						break;
					}
			}
			//end switch
			cBETEApplication bApp;
			double dblAssessedValue = 0;
			// vbPorter upgrade warning: intAge As short --> As int	OnWrite(double, short)
			int intAge = 0;
			bool boolBETEEligible = false;
			string strDescription = "";
			string strEquipLocation = "";
			double dblEstimatedValue = 0;
			string strPlacedInService = "";
			bool boolTIF = false;
			double dblValueNew = 0;
			string[] strAry = null;
			cParty tParty;
			cPartyAddress tAddress;
			if (rsAccount.RecordCount() > 0)
			{
				strSQL = "select * from ppmaster where not deleted = 1" + strWhere + " order by " + strOrderBy;
				rsLoad.OpenRecordset(strSQL, modPPGN.strPPDatabase);
				while (!rsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					if (rsAccount.FindFirst("account = " + rsLoad.Get_Fields("account")))
					{
						tParty = partCont.GetParty(FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields_Int32("PartyID"))));
						bApp = new cBETEApplication();
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						bApp.Account = FCConvert.ToInt32(rsLoad.Get_Fields("account"));
						if (strOwnerField != "")
						{
							switch (cReport.OwnerSource)
							{
								case 1:
								case 2:
									{
										bApp.Owner = FCConvert.ToString(rsLoad.Get_Fields(strOwnerField));
										break;
									}
								case 0:
									{
										if (!(tParty == null))
										{
											bApp.Owner = tParty.FullName;
										}
										break;
									}
							}
							//end switch
						}
						if (strBusinessField != "")
						{
							switch (cReport.BusinessSource)
							{
								case 1:
								case 2:
									{
										bApp.Business = FCConvert.ToString(rsLoad.Get_Fields(strBusinessField));
										break;
									}
								case 0:
									{
										if (!(tParty == null))
										{
											bApp.Business = tParty.FullName;
										}
										break;
									}
							}
							//end switch
						}
						// If strBusinessField = "address1" Or strOwnerField = "address1" Then
						// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
						if (Conversion.Val(rsLoad.Get_Fields("streetnumber")) > 0)
						{
							// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
							bApp.BusinessAddress = FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("streetnumber"))) + " " + rsLoad.Get_Fields_String("street");
						}
						else
						{
							bApp.BusinessAddress = FCConvert.ToString(rsLoad.Get_Fields_String("street"));
						}
						// Else
						// bApp.BusinessAddress = rsLoad.Fields("address1")
						// If Not tParty Is Nothing Then
						// Set tAddress = tParty.GetPrimaryAddress
						// If Not tAddress Is Nothing Then
						// bApp.BusinessAddress = tAddress.Address1
						// End If
						// End If
						// End If
						if (rsBusCode.FindFirstRecord("businesscode", Conversion.Val(rsLoad.Get_Fields_Int32("businesscode"))))
						{
							bApp.TypeOfBusiness = FCConvert.ToString(rsBusCode.Get_Fields_String("businesstype"));
						}
						rsDetails.OpenRecordset(strDetail + " and account = " + FCConvert.ToString(bApp.Account) + " union all " + strLeased + " and account = " + FCConvert.ToString(bApp.Account), modPPGN.strPPDatabase);
						while (!rsDetails.EndOfFile())
						{
							if (Conversion.Val(rsDetails.Get_Fields_Int32("BETEExempt")) > 0)
							{
								dblAssessedValue = Conversion.Val(rsDetails.Get_Fields_Int32("beteexempt"));
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
								dblAssessedValue = Conversion.Val(rsDetails.Get_Fields("value"));
							}
							// TODO Get_Fields: Check the table for the column [cost] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [quantity] and replace with corresponding Get_Field method
							dblValueNew = Conversion.Val(rsDetails.Get_Fields("cost")) * Conversion.Val(rsDetails.Get_Fields("quantity"));
							// TODO Get_Fields: Field [yearnew] not found!! (maybe it is an alias?)
							if (Conversion.Val(rsDetails.Get_Fields("yearnew")) > 1900)
							{
								// TODO Get_Fields: Field [yearnew] not found!! (maybe it is an alias?)
								intAge = FCConvert.ToInt32(intYear - Conversion.Val(rsDetails.Get_Fields("yearnew")));
							}
							else
							{
								intAge = 0;
							}
							boolBETEEligible = true;
							strDescription = FCConvert.ToString(rsDetails.Get_Fields_String("description"));
							strEquipLocation = "";
							dblEstimatedValue = dblAssessedValue;
							boolTIF = false;
							strPlacedInService = "";
							// TODO Get_Fields: Field [yearnew] not found!! (maybe it is an alias?)
							if (Conversion.Val(rsDetails.Get_Fields("yearnew")) > 1900)
							{
								// TODO Get_Fields: Field [monthnew] not found!! (maybe it is an alias?)
								if (Conversion.Val(rsDetails.Get_Fields("monthnew")) > 0)
								{
									// TODO Get_Fields: Field [monthnew] not found!! (maybe it is an alias?)
									strPlacedInService = Strings.Format(rsDetails.Get_Fields("monthnew"), "00");
								}
								else if (Strings.Trim(FCConvert.ToString(rsDetails.Get_Fields_String("leasedate"))) != "/" && Strings.Trim(FCConvert.ToString(rsDetails.Get_Fields_String("leasedate"))) != "")
								{
									strAry = Strings.Split(Strings.Trim(FCConvert.ToString(rsDetails.Get_Fields_String("leasedate"))), "/", -1, CompareConstants.vbBinaryCompare);
									if (Strings.Trim(strAry[0]) != "")
									{
										if (Conversion.Val(strAry[0]) > 0)
										{
											strPlacedInService = Strings.Format(FCConvert.ToString(Conversion.Val(strAry[0])), "00");
										}
									}
								}
								// TODO Get_Fields: Field [yearnew] not found!! (maybe it is an alias?)
								strPlacedInService += "/" + rsDetails.Get_Fields("yearnew");
							}
							bApp.Details.CreateDetail(dblAssessedValue, intAge, boolBETEEligible, strDescription, strEquipLocation, dblEstimatedValue, strPlacedInService, boolTIF, dblValueNew);
							rsDetails.MoveNext();
						}
						cReport.BETEApplications.AddItem(bApp);
					}
					rsLoad.MoveNext();
				}
			}
		}
	}
}
