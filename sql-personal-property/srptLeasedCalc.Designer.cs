﻿namespace TWPP0000
{
	/// <summary>
	/// Summary description for srptLeasedCalc.
	/// </summary>
	partial class srptLeasedCalc
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptLeasedCalc));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtLine = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDesc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMOS = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLP = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFCTR = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtQTY = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtR = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBETEExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtYearExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            ((System.ComponentModel.ISupportInitialize)(this.txtLine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMOS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFCTR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQTY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEExempt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYearExempt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanShrink = true;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtLine,
            this.txtCD,
            this.txtDesc,
            this.txtValue,
            this.txtCost,
            this.txtDate,
            this.txtMOS,
            this.txtLP,
            this.txtRent,
            this.txtGD,
            this.txtFCTR,
            this.txtQTY,
            this.txtR,
            this.txtBETEExempt,
            this.txtYearExempt});
            this.Detail.Height = 0.19F;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // txtLine
            // 
            this.txtLine.CanGrow = false;
            this.txtLine.Height = 0.19F;
            this.txtLine.Left = 0F;
            this.txtLine.MultiLine = false;
            this.txtLine.Name = "txtLine";
            this.txtLine.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtLine.Text = null;
            this.txtLine.Top = 0F;
            this.txtLine.Width = 0.4166667F;
            // 
            // txtCD
            // 
            this.txtCD.CanGrow = false;
            this.txtCD.Height = 0.19F;
            this.txtCD.Left = 1.322917F;
            this.txtCD.MultiLine = false;
            this.txtCD.Name = "txtCD";
            this.txtCD.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtCD.Text = null;
            this.txtCD.Top = 0F;
            this.txtCD.Width = 0.2291667F;
            // 
            // txtDesc
            // 
            this.txtDesc.CanShrink = true;
            this.txtDesc.Height = 0.19F;
            this.txtDesc.Left = 2.072917F;
            this.txtDesc.Name = "txtDesc";
            this.txtDesc.Style = "font-family: \'Tahoma\'; text-align: left";
            this.txtDesc.Text = null;
            this.txtDesc.Top = 0F;
            this.txtDesc.Width = 2.3125F;
            // 
            // txtValue
            // 
            this.txtValue.CanGrow = false;
            this.txtValue.Height = 0.19F;
            this.txtValue.Left = 7.854167F;
            this.txtValue.MultiLine = false;
            this.txtValue.Name = "txtValue";
            this.txtValue.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtValue.Text = null;
            this.txtValue.Top = 0F;
            this.txtValue.Width = 1.020833F;
            // 
            // txtCost
            // 
            this.txtCost.CanGrow = false;
            this.txtCost.Height = 0.19F;
            this.txtCost.Left = 4.40625F;
            this.txtCost.MultiLine = false;
            this.txtCost.Name = "txtCost";
            this.txtCost.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtCost.Text = null;
            this.txtCost.Top = 0F;
            this.txtCost.Width = 0.9479167F;
            // 
            // txtDate
            // 
            this.txtDate.CanGrow = false;
            this.txtDate.Height = 0.19F;
            this.txtDate.Left = 5.40625F;
            this.txtDate.MultiLine = false;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtDate.Text = null;
            this.txtDate.Top = 0F;
            this.txtDate.Width = 0.5F;
            // 
            // txtMOS
            // 
            this.txtMOS.CanGrow = false;
            this.txtMOS.Height = 0.19F;
            this.txtMOS.Left = 5.979167F;
            this.txtMOS.MultiLine = false;
            this.txtMOS.Name = "txtMOS";
            this.txtMOS.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtMOS.Text = null;
            this.txtMOS.Top = 0F;
            this.txtMOS.Width = 0.3645833F;
            // 
            // txtLP
            // 
            this.txtLP.CanGrow = false;
            this.txtLP.Height = 0.19F;
            this.txtLP.Left = 6.25F;
            this.txtLP.MultiLine = false;
            this.txtLP.Name = "txtLP";
            this.txtLP.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtLP.Text = null;
            this.txtLP.Top = 0F;
            this.txtLP.Visible = false;
            this.txtLP.Width = 0.1875F;
            // 
            // txtRent
            // 
            this.txtRent.CanGrow = false;
            this.txtRent.Height = 0.19F;
            this.txtRent.Left = 6.40625F;
            this.txtRent.MultiLine = false;
            this.txtRent.Name = "txtRent";
            this.txtRent.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtRent.Text = null;
            this.txtRent.Top = 0F;
            this.txtRent.Width = 0.5F;
            // 
            // txtGD
            // 
            this.txtGD.CanGrow = false;
            this.txtGD.Height = 0.19F;
            this.txtGD.Left = 7.052083F;
            this.txtGD.MultiLine = false;
            this.txtGD.Name = "txtGD";
            this.txtGD.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtGD.Text = null;
            this.txtGD.Top = 0F;
            this.txtGD.Width = 0.3125F;
            // 
            // txtFCTR
            // 
            this.txtFCTR.CanGrow = false;
            this.txtFCTR.Height = 0.19F;
            this.txtFCTR.Left = 7.375F;
            this.txtFCTR.MultiLine = false;
            this.txtFCTR.Name = "txtFCTR";
            this.txtFCTR.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtFCTR.Text = null;
            this.txtFCTR.Top = 0F;
            this.txtFCTR.Width = 0.4375F;
            // 
            // txtQTY
            // 
            this.txtQTY.Height = 0.19F;
            this.txtQTY.Left = 1.635417F;
            this.txtQTY.MultiLine = false;
            this.txtQTY.Name = "txtQTY";
            this.txtQTY.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtQTY.Text = null;
            this.txtQTY.Top = 0F;
            this.txtQTY.Width = 0.375F;
            // 
            // txtR
            // 
            this.txtR.Height = 0.19F;
            this.txtR.Left = 0.4375F;
            this.txtR.Name = "txtR";
            this.txtR.Style = "font-family: \'Tahoma\'; text-align: center";
            this.txtR.Text = null;
            this.txtR.Top = 0F;
            this.txtR.Width = 0.25F;
            // 
            // txtBETEExempt
            // 
            this.txtBETEExempt.CanGrow = false;
            this.txtBETEExempt.Height = 0.19F;
            this.txtBETEExempt.Left = 8.9375F;
            this.txtBETEExempt.MultiLine = false;
            this.txtBETEExempt.Name = "txtBETEExempt";
            this.txtBETEExempt.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtBETEExempt.Text = null;
            this.txtBETEExempt.Top = 0F;
            this.txtBETEExempt.Width = 1F;
            // 
            // txtYearExempt
            // 
            this.txtYearExempt.CanGrow = false;
            this.txtYearExempt.Height = 0.19F;
            this.txtYearExempt.Left = 0.7430556F;
            this.txtYearExempt.MultiLine = false;
            this.txtYearExempt.Name = "txtYearExempt";
            this.txtYearExempt.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtYearExempt.Text = null;
            this.txtYearExempt.Top = 0F;
            this.txtYearExempt.Width = 0.5520833F;
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label1,
            this.Line1,
            this.Label2,
            this.Label3,
            this.Label4,
            this.Label5,
            this.Label6,
            this.Label7,
            this.Label8,
            this.Label9,
            this.Label10,
            this.Label11,
            this.Label12,
            this.Label13,
            this.Label14,
            this.Label15,
            this.Label16});
            this.GroupHeader1.GroupKeepTogether = GrapeCity.ActiveReports.SectionReportModel.GroupKeepTogether.FirstDetail;
            this.GroupHeader1.Height = 0.65625F;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
            // 
            // Label1
            // 
            this.Label1.Height = 0.1875F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 3.385417F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
            this.Label1.Text = "Leased Description";
            this.Label1.Top = 0.03125F;
            this.Label1.Width = 3.125F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0.0625F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.21875F;
            this.Line1.Width = 9.8125F;
            this.Line1.X1 = 0.0625F;
            this.Line1.X2 = 9.875F;
            this.Line1.Y1 = 0.21875F;
            this.Line1.Y2 = 0.21875F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.19F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 0F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.Label2.Text = "Line";
            this.Label2.Top = 0.4375F;
            this.Label2.Width = 0.4166667F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.19F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 1.322917F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.Label3.Text = "CD";
            this.Label3.Top = 0.4375F;
            this.Label3.Width = 0.25F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.19F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 2.072917F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.Label4.Text = "Description";
            this.Label4.Top = 0.4375F;
            this.Label4.Width = 1.3125F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.4F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 4.979167F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
            this.Label5.Text = "Org Cost";
            this.Label5.Top = 0.28125F;
            this.Label5.Width = 0.375F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.4F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 5.40625F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
            this.Label6.Text = "Lease Date";
            this.Label6.Top = 0.28125F;
            this.Label6.Width = 0.5F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.4F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 5.96875F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
            this.Label7.Text = "# MOS";
            this.Label7.Top = 0.28125F;
            this.Label7.Width = 0.375F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.3541667F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 6.25F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.Label8.Text = "L P";
            this.Label8.Top = 0.28125F;
            this.Label8.Visible = false;
            this.Label8.Width = 0.1875F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.4F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 6.40625F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.Label9.Text = "Rent/ Month";
            this.Label9.Top = 0.28125F;
            this.Label9.Width = 0.5F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.19F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 6.947917F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.Label10.Text = "%GD";
            this.Label10.Top = 0.4375F;
            this.Label10.Width = 0.4166667F;
            // 
            // Label11
            // 
            this.Label11.Height = 0.19F;
            this.Label11.HyperLink = null;
            this.Label11.Left = 7.375F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.Label11.Text = "FCTR";
            this.Label11.Top = 0.4375F;
            this.Label11.Width = 0.4375F;
            // 
            // Label12
            // 
            this.Label12.Height = 0.19F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 8.4375F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.Label12.Text = "Value";
            this.Label12.Top = 0.4375F;
            this.Label12.Width = 0.4375F;
            // 
            // Label13
            // 
            this.Label13.Height = 0.19F;
            this.Label13.HyperLink = null;
            this.Label13.Left = 1.635417F;
            this.Label13.Name = "Label13";
            this.Label13.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.Label13.Text = "QTY";
            this.Label13.Top = 0.4375F;
            this.Label13.Width = 0.33F;
            // 
            // Label14
            // 
            this.Label14.Height = 0.19F;
            this.Label14.HyperLink = null;
            this.Label14.Left = 0.4375F;
            this.Label14.Name = "Label14";
            this.Label14.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.Label14.Text = "RB";
            this.Label14.Top = 0.4375F;
            this.Label14.Width = 0.25F;
            // 
            // Label15
            // 
            this.Label15.Height = 0.19F;
            this.Label15.HyperLink = null;
            this.Label15.Left = 8.9375F;
            this.Label15.Name = "Label15";
            this.Label15.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.Label15.Text = "BETE Exempt";
            this.Label15.Top = 0.4375F;
            this.Label15.Width = 1F;
            // 
            // Label16
            // 
            this.Label16.Height = 0.3541667F;
            this.Label16.HyperLink = null;
            this.Label16.Left = 0.7395833F;
            this.Label16.Name = "Label16";
            this.Label16.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
            this.Label16.Text = "Year Exempt";
            this.Label16.Top = 0.2708333F;
            this.Label16.Width = 0.5729167F;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Height = 0F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // ActiveReport1
            // 
            this.MasterReport = false;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 8.5F;
            this.PageSettings.PaperWidth = 11F;
            this.PrintWidth = 9.958333F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
            ((System.ComponentModel.ISupportInitialize)(this.txtLine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMOS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFCTR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQTY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEExempt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYearExempt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDesc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMOS;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLP;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFCTR;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTY;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtR;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBETEExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYearExempt;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
