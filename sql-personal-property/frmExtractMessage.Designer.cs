//Fecher vbPorter - Version 1.0.0.32
using fecherFoundation;
using Wisej.Web;
using Wisej.Core;
using Global;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmExtractMessage.
	/// </summary>
	partial class frmExtractMessage : BaseForm
	{
		public fecherFoundation.FCProgressBar pbrExtract;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Shape1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmExtractMessage));
			this.pbrExtract = new fecherFoundation.FCProgressBar();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Shape1 = new fecherFoundation.FCLabel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 202);
			this.BottomPanel.Size = new System.Drawing.Size(241, 28);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.pbrExtract);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.Shape1);
			this.ClientArea.Size = new System.Drawing.Size(241, 142);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(241, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			// 
			// pbrExtract
			// 
			this.pbrExtract.Location = new System.Drawing.Point(30, 110);
			this.pbrExtract.Name = "pbrExtract";
			this.pbrExtract.Size = new System.Drawing.Size(178, 15);
			this.pbrExtract.TabIndex = 1;
			// 
			// Label2
			// 
			this.Label2.AutoSize = true;
			this.Label2.Location = new System.Drawing.Point(30, 65);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(208, 15);
			this.Label2.TabIndex = 2;
			this.Label2.Text = "PRESS ESC TO ABORT PROCESS";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(205, 15);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "CREATING USER EXTRACT.......";
			// 
			// Shape1
			// 
			this.Shape1.BorderStyle = 1;
			this.Shape1.Location = new System.Drawing.Point(4, 2);
			this.Shape1.Name = "Shape1";
			this.Shape1.Size = new System.Drawing.Size(204, 41);
			this.Shape1.TabIndex = 3;
			// 
			// frmExtractMessage
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(241, 230);
			this.FillColor = 0;
			this.FormBorderStyle = Wisej.Web.FormBorderStyle.None;
			this.Name = "frmExtractMessage";
			this.ShowInTaskbar = false;
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.frmExtractMessage_Load);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmExtractMessage_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			this.ResumeLayout(false);
		}
		#endregion
	}
}