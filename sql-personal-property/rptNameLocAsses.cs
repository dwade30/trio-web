﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Core;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptNameLocAsses.
	/// </summary>
	public partial class rptNameLocAsses : BaseSectionReport
	{

		private clsDRWrapper rsLoad = new clsDRWrapper();
		public rptNameLocAsses()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Name Location Assessment Report";
		}

        public void Init(string strsql)
        {
            rsLoad.OpenRecordset(strsql, "PersonalProperty");
            if (rsLoad.EndOfFile())
            {
                MessageBox.Show("No records found");
                this.Unload();
                return;
            }
            frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "NameLocAssess");
        }

		public static rptNameLocAsses InstancePtr
		{
			get
			{
				return (rptNameLocAsses)Sys.GetInstance(typeof(rptNameLocAsses));
			}
		}

		protected rptNameLocAsses _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}

            if (disposing)
            {
				rsLoad?.Dispose();
                rsLoad = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptNameLocAsses	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private int intPage;
		private int intAccount;
		private bool boolPrint;
		private string strName = "";
		private string strLocation = "";
		private double dblTotal;
		private int lngCount;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			Fields.Add("AccountNumber");
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			txttime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			ShowRecord:
			;
			eArgs.EOF = rsLoad.EndOfFile();
			if (eArgs.EOF)
				return;
			if (FCConvert.ToInt32(rsLoad.GetData("masterAccount")) == intAccount)
			{
				if (!rsLoad.EndOfFile())
				{
					rsLoad.MoveNext();
				}
				goto ShowRecord;
			}
			txtName.Text = rsLoad.GetData("Name").ToString();
			txtLocation.Text = rsLoad.GetData("StreetNumber") + rsLoad.Get_Fields_String("streetapt") + " " + rsLoad.GetData("Street");
			txtAccount.Text = rsLoad.GetData("masterAccount").ToString();
			lngCount += 1;
			// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
			dblTotal += Conversion.Val(rsLoad.Get_Fields("value"));
			// TODO Get_Fields: Check the table for the column [Value] and replace with corresponding Get_Field method
			txtAssessment.Text = Strings.Format(rsLoad.Get_Fields("Value"), "###,###,###,##0");
			intAccount = FCConvert.ToInt32(rsLoad.GetData("masterAccount"));
			if (!rsLoad.EndOfFile())
				rsLoad.MoveNext();
			eArgs.EOF = false;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//this.Printer.RenderMode = 1;
			lngCount = 0;
			dblTotal = 0;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTitle.Text = "Name / Assessment / Location";
			intPage += 1;
			txtPage.Text = "Page: " + FCConvert.ToString(intPage);
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtCount.Text = Strings.Format(lngCount, "#,###,###,##0");
			txtTotalAssess.Text = Strings.Format(dblTotal, "#,###,###,###,##0");
		}

		
	}
}
