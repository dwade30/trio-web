﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmPPSearch.
	/// </summary>
	public partial class frmPPSearch : BaseForm
	{
		public frmPPSearch()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPPSearch InstancePtr
		{
			get
			{
				return (frmPPSearch)Sys.GetInstance(typeof(frmPPSearch));
			}
		}

		protected frmPPSearch _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int acctcol;
		int NameCol;
		string strScreenType;
        clsDRWrapper clsLoad;

        // vbPorter upgrade warning: intSearchType As short	OnWriteFCConvert.ToInt32(
        public void Init(string strSQL, int intSearchType, string strScreen)
		{
			clsLoad = new clsDRWrapper();
			int GridWidth;
			GridWidth = SearchGrid.WidthOriginal;
			clsLoad.OpenRecordset(strSQL, modPPGN.strPPDatabase);
			strScreenType = strScreen;
			if (!clsLoad.EndOfFile())
			{
				SetupSearchGrid();
				if (intSearchType == 2 || intSearchType == 3)
				{
					SearchGrid.Cols = 5;
					SearchGrid.ColWidth(3, SearchGrid.ColWidth(2));
					SearchGrid.ColData(3, SearchGrid.ColData(2));
					SearchGrid.ColWidth(2, FCConvert.ToInt32(0.3 * GridWidth));
					// 2880
					SearchGrid.ColData(2, 0.3);
					if (intSearchType == 2)
					{
						SearchGrid.TextMatrix(0, 2, modPPGN.Statics.Open1Title);
					}
					else
					{
						SearchGrid.TextMatrix(0, 2, modPPGN.Statics.Open2Title);
					}
					SearchGrid.TextMatrix(0, 3, "St. No.");
					SearchGrid.TextMatrix(0, 4, "Street");
					SearchGrid.ColWidth(4, FCConvert.ToInt32(0.3 * GridWidth));
					// 2880
					SearchGrid.ColData(4, 0.3);
				}
				if (intSearchType == 4)
				{
					SearchGrid.Cols = 5;
					SearchGrid.TextMatrix(0, 2, "B. Code");
					SearchGrid.TextMatrix(0, 3, "St. No.");
					SearchGrid.TextMatrix(0, 4, "Street");
					SearchGrid.ColWidth(4, FCConvert.ToInt32(0.3 * GridWidth));
					// 2880
					SearchGrid.ColData(4, 0.3);
					SearchGrid.ColWidth(3, SearchGrid.ColWidth(0));
					SearchGrid.ColData(3, SearchGrid.ColData(0));
				}
				string strList = "";
				while (!clsLoad.EndOfFile())
				{
					// If Val(!Deleted & " ") <> 1 Then
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
					strList = modGlobal.PadToString(clsLoad.Get_Fields("Account"), 5) + "  " + modPPGN.StringToSpaces_8(clsLoad.Get_Fields_String("Name"), 34) + "  " + modGlobal.PadToString(clsLoad.Get_Fields("StreetNumber"), 5) + "  " + modPPGN.StringToSpaces_8(clsLoad.Get_Fields_String("Street"), 24);
					lstPPSearch.AddItem(strList);
					if (intSearchType == 2 || intSearchType == 3)
					{
						if (intSearchType == 2)
						{
							// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
							SearchGrid.AddItem(clsLoad.Get_Fields("account") + "\t" + clsLoad.Get_Fields_String("name") + "\t" + clsLoad.Get_Fields_String("open1") + "\t" + clsLoad.Get_Fields("streetnumber") + "\t" + clsLoad.Get_Fields_String("street"));
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
							SearchGrid.AddItem(clsLoad.Get_Fields("account") + "\t" + clsLoad.Get_Fields_String("name") + "\t" + clsLoad.Get_Fields_String("open2") + "\t" + clsLoad.Get_Fields("streetnumber") + "\t" + clsLoad.Get_Fields_String("street"));
						}
					}
					else if (intSearchType == 4)
					{
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
						SearchGrid.AddItem(clsLoad.Get_Fields("account") + "\t" + clsLoad.Get_Fields_String("name") + "\t" + clsLoad.Get_Fields_Int32("businesscode") + "\t" + clsLoad.Get_Fields("streetnumber") + "\t" + clsLoad.Get_Fields_String("street"));
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
						SearchGrid.AddItem(clsLoad.Get_Fields("Account") + "\t" + clsLoad.Get_Fields_String("Name") + "\t" + clsLoad.Get_Fields("StreetNumber") + "\t" + clsLoad.Get_Fields_String("Street"));
					}
					// End If
					if (FCConvert.ToBoolean((clsLoad.Get_Fields_Boolean("DELETED") + "")) == true)
					{
						SearchGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, SearchGrid.Rows - 1, 0, SearchGrid.Rows - 1, SearchGrid.Cols - 1, modGlobalConstants.Statics.TRIOCOLORRED);
						SearchGrid.RowData(SearchGrid.Rows - 1, true);
					}
					else
					{
						SearchGrid.RowData(SearchGrid.Rows - 1, false);
					}
					clsLoad.MoveNext();
				}
				if (intSearchType == 1)
				{
					SearchGrid.ColPosition(2, 1);
					SearchGrid.ColPosition(3, 2);
				}
				else if (intSearchType == 2)
				{
					SearchGrid.ColPosition(2, 1);
				}
				else if (intSearchType == 3)
				{
					SearchGrid.ColPosition(2, 1);
				}
				else if (intSearchType == 4)
				{
					SearchGrid.ColPosition(2, 1);
				}
				this.Show(App.MainForm);
				// If CurrentAccount = -1 Then CurrentAccount = 1
				// Call SaveKeyValue(HKEY_CURRENT_USER, REGISTRYKEY, "PPLastAccountNumber", Val(CurrentAccount))
			}
			else
			{
				MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Close();
				return;
			}
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			try
			{
                clsLoad.MoveFirst();
                modPrinting.Statics.rsReports = clsLoad;
				
                frmReportViewer.InstancePtr.Init(rptObj: rptPPSearch.InstancePtr);
                return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In cmdPrint_click", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmPPSearch_Activated(object sender, System.EventArgs e)
		{
			if (modGNWork.FormExist(this))
				return;
		}

		private void frmPPSearch_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				frmPPSearch.InstancePtr.Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmPPSearch_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPPSearch properties;
			//frmPPSearch.ScaleWidth	= 9075;
			//frmPPSearch.ScaleHeight	= 7530;
			//frmPPSearch.LinkTopic	= "Form1";
			//frmPPSearch.LockControls	= true;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			Shape1.FillColor(ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORRED));
			acctcol = 0;
			//FC:FINAL:RPU: #1448 - Set Column ValueType correctly
			SearchGrid.Columns[0].ValueType = typeof(long);
		}

		private void frmPPSearch_Resize(object sender, System.EventArgs e)
		{
			ResizeSearchGrid();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			modPPGN.Statics.Searching = false;
			//FC:FINAL:CHN - issue #1260: Search results should appear on search criteria screen. 
			frmPPGetAccount.InstancePtr.Show(App.MainForm);
		}

		private void lstPPSearch_DoubleClick(object sender, System.EventArgs e)
		{
			if (Conversion.Val(Strings.Mid(lstPPSearch.Text, 1, 5)) != 0)
			{
				modPPGN.Statics.CurrentAccount = FCConvert.ToInt32(FCConvert.ToDouble(Strings.Mid(lstPPSearch.Text, 1, 5)));
				modPPGN.Statics.Searching = true;
			}
			frmPPSearch.InstancePtr.Close();
		}

		private void lstPPSearch_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Return)
			{
				KeyCode = (Keys)0;
				if (Conversion.Val(Strings.Mid(lstPPSearch.Text, 1, 5)) != 0)
				{
					modPPGN.Statics.CurrentAccount = FCConvert.ToInt32(FCConvert.ToDouble(Strings.Mid(lstPPSearch.Text, 1, 5)));
					modPPGN.Statics.Searching = true;
				}
				frmPPSearch.InstancePtr.Close();
			}
		}

		private void SearchGrid_DblClick(object sender, System.EventArgs e)
		{
			if (SearchGrid.Row == 0)
				return;
			if (Conversion.Val(SearchGrid.TextMatrix(SearchGrid.Row, acctcol)) != 0)
			{
				modPPGN.Statics.CurrentAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(SearchGrid.TextMatrix(SearchGrid.Row, acctcol))));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "PPLastAccountNumber", FCConvert.ToString(modPPGN.Statics.CurrentAccount));
				modPPGN.Statics.Searching = true;
				LoadAcct();
			}
			// Unload frmPPSearch
		}

		private void LoadAcct()
		{
			if (strScreenType == "L")
			{
				// Long Screen
				// frmPPMaster.Show , MDIParent
				frmPPGetAccount.InstancePtr.txtGetAccountNumber.Text = FCConvert.ToString(modPPGN.Statics.CurrentAccount);
				frmPPGetAccount.InstancePtr.GetAcctNum();
				// Call UnlockAllRecordsByUser
				// Call PPLockRecord(User, "Master", CStr(CurrentAccount))
			}
			else if (strScreenType == "S")
			{
				// Short Screen
				// frmPPMasterShort.Show , MDIParent
				frmPPGetAccount.InstancePtr.txtGetAccountNumber.Text = FCConvert.ToString(modPPGN.Statics.CurrentAccount);
				frmPPGetAccount.InstancePtr.GetAcctNum();
				// Call UnlockAllRecordsByUser
				// Call PPLockRecord(User, "Short", CStr(CurrentAccount))
			}
			else if (strScreenType == "C")
			{
				// Calculate Screen
				//MDIParent.InstancePtr.Show();
			}
		}

		private void SearchGrid_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Return)
			{
				KeyCode = 0;
				if (Conversion.Val(SearchGrid.TextMatrix(SearchGrid.Row, acctcol)) != 0)
				{
					modPPGN.Statics.CurrentAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(SearchGrid.TextMatrix(SearchGrid.Row, acctcol))));
					modPPGN.Statics.Searching = true;
					LoadAcct();
				}
				// Unload frmPPSearch
			}
		}

		public void SetupSearchGrid()
		{
			int GridWidth;
			GridWidth = SearchGrid.WidthOriginal;
			SearchGrid.Rows = 1;
			SearchGrid.TextMatrix(0, 0, "Account");
			SearchGrid.TextMatrix(0, 1, "Name");
			SearchGrid.TextMatrix(0, 2, "St. No.");
			SearchGrid.TextMatrix(0, 3, "Street");
			SearchGrid.ColWidth(0, FCConvert.ToInt32(0.11 * GridWidth));
			SearchGrid.ColData(0, 0.11);
			SearchGrid.ColData(2, 0.11);
			SearchGrid.ColWidth(2, FCConvert.ToInt32(0.11 * GridWidth));
			SearchGrid.ColWidth(1, FCConvert.ToInt32(0.3 * GridWidth));
			// 2880
			SearchGrid.ColData(1, 0.3);
			SearchGrid.ColWidth(3, FCConvert.ToInt32(0.3 * GridWidth));
			// 2880
			SearchGrid.ColData(3, 0.3);
            ResizeSearchGrid();
		}

		public void ResizeSearchGrid()
		{
			int GridWidth;
			int x;
			GridWidth = SearchGrid.WidthOriginal;
			for (x = 0; x <= SearchGrid.Cols - 1; x++)
			{
				SearchGrid.ColWidth(x, FCConvert.ToInt32(Conversion.Val(SearchGrid.ColData(x)) * GridWidth));
			}
			// x
		}
	}
}
