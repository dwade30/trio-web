﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptCategoryBreakdown.
	/// </summary>
	public partial class rptCategoryBreakdown : BaseSectionReport
	{
		public rptCategoryBreakdown()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Category Breakdown";
		}

		public static rptCategoryBreakdown InstancePtr
		{
			get
			{
				return (rptCategoryBreakdown)Sys.GetInstance(typeof(rptCategoryBreakdown));
			}
		}

		protected rptCategoryBreakdown _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsReport?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCategoryBreakdown	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsReport = new clsDRWrapper();
		bool boolByCat;
		string[] CatDesc = new string[9 + 1];
		double dblSubTot;
		double dblTotTot;
		// vbPorter upgrade warning: intOrder As short	OnWriteFCConvert.ToInt32(
		public void Init(bool boolByCategory, bool boolRange, int intOrder = 0, string strStart = "", string strEnd = "")
		{
			string strSQL;
			string strWhere = "";
			string strOrderBy;
			string strMasterJoinJoin;
			strMasterJoinJoin = modPPGN.GetMasterJoinForJoin();
			boolByCat = boolByCategory;
			strOrderBy = "";
			if (boolRange)
			{
				strWhere += " and ";
				switch (intOrder)
				{
					case 0:
						{
							strWhere += " account between " + FCConvert.ToString(Conversion.Val(strStart)) + " and " + FCConvert.ToString(Conversion.Val(strEnd)) + " ";
							strOrderBy = ",account";
							break;
						}
					case 1:
						{
							strWhere += " name between '" + modGlobalFunctions.EscapeQuotes(strStart) + "' and '" + modGlobalFunctions.EscapeQuotes(strEnd) + "' ";
							strOrderBy = "";
							break;
						}
					case 2:
						{
							strWhere += " street between '" + modGlobalFunctions.EscapeQuotes(strStart) + "' and '" + modGlobalFunctions.EscapeQuotes(strEnd) + "' ";
							strOrderBy = ",street,streetnumber";
							break;
						}
					case 3:
						{
							strWhere += " open1 between '" + modGlobalFunctions.EscapeQuotes(strStart) + "' and '" + modGlobalFunctions.EscapeQuotes(strEnd) + "' ";
							strOrderBy = ",open1";
							break;
						}
					case 4:
						{
							strWhere += " open2 between '" + modGlobalFunctions.EscapeQuotes(strStart) + "' and '" + modGlobalFunctions.EscapeQuotes(strEnd) + "' ";
							strOrderBy = ",open2";
							break;
						}
					case 5:
						{
							strWhere += " businesscode between " + FCConvert.ToString(Conversion.Val(strStart)) + " and " + FCConvert.ToString(Conversion.Val(strEnd)) + " ";
							strOrderBy = ",businesscode";
							break;
						}
					case 6:
						{
							strWhere += " streetcode between " + FCConvert.ToString(Conversion.Val(strStart)) + " and " + FCConvert.ToString(Conversion.Val(strEnd)) + " ";
							strOrderBy = ",streetcode";
							break;
						}
					case 7:
						{
							strWhere += " trancode between " + FCConvert.ToString(Conversion.Val(strStart)) + " and " + FCConvert.ToString(Conversion.Val(strEnd)) + " ";
							strOrderBy = ",trancode";
							break;
						}
				}
				//end switch
				strWhere += " and not deleted = 1 ";
			}
			else
			{
				strWhere = " where not deleted = 1 ";
			}
			strSQL = " (select valuekey,category1 as CatValue,1 as CatNum from ppvaluations where category1 > 0) as cat1 ";
			strSQL += " union all ";
			strSQL += " (select valuekey,category2 as CatValue,2 as CatNum from ppvaluations where category2  > 0)  ";
			strSQL += " union all ";
			strSQL += " (select valuekey,category3 as CatValue,3 as CatNum from ppvaluations where category3 > 0) ";
			strSQL += " union all ";
			strSQL += " (select valuekey,category4 as CatValue,4 as CatNum from ppvaluations where category4 > 0) ";
			strSQL += " union all ";
			strSQL += " (select valuekey,category5 as CatValue,5 as CatNum from ppvaluations where category5 > 0) ";
			strSQL += " union all ";
			strSQL += " (select valuekey,category6 as CatValue,6 as CatNum from ppvaluations where category6 > 0) ";
			strSQL += " union all ";
			strSQL += " (select valuekey,category7 as CatValue,7 as CatNum from ppvaluations where category7 > 0) ";
			strSQL += " union all ";
			strSQL += " (select valuekey,category8 as CatValue,8 as CatNum from ppvaluations where category8 > 0) ";
			strSQL += " union all ";
			strSQL += " (select valuekey,category9 as CatValue,9 as CatNum from ppvaluations where category9 > 0) ";
			strSQL = "  (select * from " + strSQL + ") as PPCatBreakdown";
			// strSQL = " select * from ppmaster inner join " & strSQL & " on (ppcatbreakdown.valuekey = ppmaster.account) "
			strSQL = " select * from " + strMasterJoinJoin + " inner join " + strSQL + " on (ppcatbreakdown.valuekey = mj.account) ";
			if (boolByCategory)
			{
				strOrderBy = " order by CatNum,name " + strOrderBy;
				if (boolRange)
				{
					// add where and get rid of and
					strWhere = " where " + Strings.Mid(strWhere, 5);
				}
				strSQL += strWhere + strOrderBy;
			}
			else
			{
				// strSQL = "Select * from ppmaster inner join ppvaluations on (ppmaster.account = ppvaluations.valuekey) where not deleted "
				strOrderBy = " order by name,account,catnum " + strOrderBy;
				if (boolRange)
				{
					strWhere = " where " + Strings.Mid(strWhere, 5);
				}
				strSQL += strWhere + strOrderBy;
			}
			rsReport.OpenRecordset(strSQL, modPPGN.strPPDatabase);
			if (rsReport.EndOfFile())
			{
				MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Close();
				return;
			}
			frmReportViewer.InstancePtr.Init(rptObj: this, boolAllowEmail: true, strAttachmentName: "CategoryBreakdown");
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("fldgroupid");
			this.Fields.Add("fldgroupdesc");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsReport.EndOfFile();
			if (!eArgs.EOF)
			{
				if (boolByCat)
				{
					// TODO Get_Fields: Field [catnum] not found!! (maybe it is an alias?)
					this.Fields["fldgroupid"].Value = Conversion.Val(rsReport.Get_Fields("catnum"));
					// TODO Get_Fields: Field [catnum] not found!! (maybe it is an alias?)
					if (Conversion.Val(rsReport.Get_Fields("catnum")) > 1)
					{
						eArgs.EOF = eArgs.EOF;
					}
					// TODO Get_Fields: Field [catnum] not found!! (maybe it is an alias?)
					this.Fields["fldgroupdesc"].Value = CatDesc[FCConvert.ToInt32(rsReport.Get_Fields("catnum"))];
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					this.Fields["fldgroupid"].Value = rsReport.Get_Fields("account");
					this.Fields["fldgroupdesc"].Value = rsReport.Get_Fields_String("name");
				}
				//this.GroupHeader1.DataField = this.Fields["fldgroupid"].Value.ToString();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
            using (clsDRWrapper rsLoad = new clsDRWrapper())
            {
                int x;
                dblSubTot = 0;
                dblTotTot = 0;
                for (x = 1; x <= 9; x++)
                {
                    CatDesc[x] = "Category " + FCConvert.ToString(x);
                }

                // x
                rsLoad.OpenRecordset("select * from ratiotrends order by type", modPPGN.strPPDatabase);
                while (!rsLoad.EndOfFile())
                {
                    // TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
                    CatDesc[rsLoad.Get_Fields("type")] = FCConvert.ToString(rsLoad.Get_Fields_String("description"));
                    rsLoad.MoveNext();
                }

                if (boolByCat)
                {
                    // TODO Get_Fields: Field [catnum] not found!! (maybe it is an alias?)
                    this.Fields["fldgroupid"].Value = Conversion.Val(rsReport.Get_Fields("catnum"));
                }
                else
                {
                    // TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
                    this.Fields["fldgroupid"].Value = rsReport.Get_Fields("account");
                }

                lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
                lblTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
                lblMuniname.Text = modGlobalConstants.Statics.MuniName;
            }
        }

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsReport.EndOfFile())
			{
				int lngVal = 0;
				// TODO Get_Fields: Field [CatValue] not found!! (maybe it is an alias?)
				lngVal = FCConvert.ToInt32(Math.Round(Conversion.Val(rsReport.Get_Fields("CatValue"))));
				dblSubTot += lngVal;
				dblTotTot += lngVal;
				txtValue.Text = Strings.Format(lngVal, "#,###,###,##0");
				if (boolByCat)
				{
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					txtID2.Text = FCConvert.ToString(rsReport.Get_Fields("account"));
					txtDescription2.Text = rsReport.Get_Fields_String("name");
				}
				else
				{
					// TODO Get_Fields: Field [catnum] not found!! (maybe it is an alias?)
					txtID2.Text = FCConvert.ToString(Conversion.Val(rsReport.Get_Fields("catnum")));
					// TODO Get_Fields: Field [catnum] not found!! (maybe it is an alias?)
					txtDescription2.Text = CatDesc[rsReport.Get_Fields("catnum")];
				}
				rsReport.MoveNext();
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			txtSubTot.Text = Strings.Format(dblSubTot, "#,###,###,##0");
			dblSubTot = 0;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + PageNumber;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtGrandTotal.Text = Strings.Format(dblTotTot, "#,###,###,##0");
		}

		
	}
}
