﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Core;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptNewAccounts.
	/// </summary>
	public partial class rptNewAccounts : BaseSectionReport
	{
		public rptNewAccounts()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "New Accounts (Never Calculated)";
		}

		public static rptNewAccounts InstancePtr
		{
			get
			{
				return (rptNewAccounts)Sys.GetInstance(typeof(rptNewAccounts));
			}
		}

		protected rptNewAccounts _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsNewAccts?.Dispose();
                clsNewAccts = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptNewAccounts	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intPage;
		clsDRWrapper clsNewAccts = new clsDRWrapper();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsNewAccts.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strMasterJoin;
			strMasterJoin = modPPGN.GetMasterJoin();
			// Call clsNewAccts.OpenRecordset("select * from ppmaster where not deleted = 1 and account in (select VALUEKEY from ppvaluations where isnull(category1))", "twpp0000.vb1")
			clsNewAccts.OpenRecordset(strMasterJoin + " where not deleted = 1 and account in (select VALUEKEY from ppvaluations where isnull(category1,-1) = -1)", "twpp0000.vb1");
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent);
			//this.Printer.RenderMode = 1;
			txtdate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txttime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			intPage = 1;
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!clsNewAccts.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				txtAcct.Text = clsNewAccts.Get_Fields_String("account");
				txtName.Text = clsNewAccts.Get_Fields_String("name");
				// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
				txtStNo.Text = FCConvert.ToString(clsNewAccts.Get_Fields("streetnumber"));
				txtStreet.Text = clsNewAccts.Get_Fields_String("street");
			}
			clsNewAccts.MoveNext();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + FCConvert.ToString(intPage);
			intPage += 1;
		}

		
	}
}
