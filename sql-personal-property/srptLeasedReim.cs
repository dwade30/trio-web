﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for srptLeasedReim.
	/// </summary>
	public partial class srptLeasedReim : FCSectionReport
	{
		public srptLeasedReim()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ActiveReport1";
		}

		public static srptLeasedReim InstancePtr
		{
			get
			{
				return (srptLeasedReim)Sys.GetInstance(typeof(srptLeasedReim));
			}
		}

		protected srptLeasedReim _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsLS?.Dispose();
                clsLS = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptLeasedReim	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsLS = new clsDRWrapper();
		float PercentGood;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsLS.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			clsLS.OpenRecordset("Select * from PPleased where Account = " + this.UserData + " and RB = '*' order by Line", "twpp0000.vb1");
			if (clsLS.EndOfFile())
			{
				//this.Stop();
				this.Close();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				string strmonth = "";
				string strYear = "";
				if (clsLS.EndOfFile())
					return;
				txtLine.Text = clsLS.GetData("line").ToString();
				txtCD.Text = clsLS.GetData("CD").ToString();
				if (Strings.Trim(clsLS.GetData("cd") + "") != "-")
				{
					if (rptReimbursement2.InstancePtr.boolPrintQTY)
					{
						txtQTY.Text = clsLS.GetData("quantity").ToString();
					}
					else
					{
						txtQTY.Text = "__";
					}
					txtDesc.Text = clsLS.GetData("description").ToString();
					if ((clsLS.GetData("leasedate") + "").Length > 3)
					{
						strmonth = Strings.Left(FCConvert.ToString(clsLS.GetData("leasedate")), 2);
						strYear = Strings.Right(FCConvert.ToString(clsLS.GetData("leasedate")), 2);
						if (Conversion.Val(strYear) < 26)
						{
							strYear = "20" + strYear;
						}
						else
						{
							strYear = "19" + strYear;
						}
					}
					else
					{
						strmonth = "";
						strYear = "";
					}
					txtMO.Text = strmonth;
					// txtMO.Text = clsLS.GetData("month")
					// txtYr.Text = clsLS.GetData("year")
					// txtYr.Text = strYear
					txtYr.Text = strYear;
					txtYrs.Text = "";
					if (Conversion.Val(clsLS.GetData("cost")) > 0)
					{
						// TODO Get_Fields: Check the table for the column [cost] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [quantity] and replace with corresponding Get_Field method
						txtOriginal.Text = Strings.Format(Conversion.Val(clsLS.Get_Fields("cost")) * Conversion.Val(clsLS.Get_Fields("quantity")), "###,###,###,##0");
					}
					else
					{
						txtOriginal.Text = "unknown";
					}
					txtAssess.Text = Strings.Format(Conversion.Val(clsLS.GetData("value")), "###,###,###,##0");
				}
				else
				{
					txtQTY.Text = "";
					txtMO.Text = "";
					txtYr.Text = "";
					txtYrs.Text = "";
					txtOriginal.Text = "";
					txtAssess.Text = "";
					txtDesc.Text = Strings.Trim(FCConvert.ToString(clsLS.Get_Fields_String("description")));
				}
				clsLS.MoveNext();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Detail_Format of sub report Leased Reimbursement", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			int x;
            using (clsDRWrapper clsLeased = new clsDRWrapper())
            {
                double dblRatio;
                int lngLSOrigCat;
                // vbPorter upgrade warning: lngLSCat As int	OnWrite(int, double)
                int lngLSCat;
                int lngLsOriginal;
                int lngLsAssessment;
                int intTemp = 0;
                clsLeased.OpenRecordset("SELECT * FROM PPRatioOpens", "twpp0000.vb1");
                // TODO Get_Fields: Check the table for the column [Ratio] and replace with corresponding Get_Field method
                dblRatio = Conversion.Val(clsLeased.Get_Fields("Ratio")) / 100;
                // TODO Get_Fields: Check the table for the column [ratio] and replace with corresponding Get_Field method
                Label14.Text = "Leased Total (With " +
                               FCConvert.ToString(Conversion.Val(clsLeased.Get_Fields("ratio"))) +
                               "% ratio applied to assessment and rounded by Category):";
                lngLsOriginal = 0;
                lngLsAssessment = 0;
                lngLSOrigCat = 0;
                lngLSCat = 0;
                clsLeased.OpenRecordset(
                    "Select cd ,sum(cost  * quantity ) as theCost,sum(value) as theValue  from PPLeased where Account = " +
                    this.UserData + " and RB = '*' group by cd", "twpp0000.vb1");
                while (!clsLeased.EndOfFile())
                {
                    intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLeased.Get_Fields_String("cd"))));
                    if (intTemp < 10 && intTemp > 0)
                    {
                        // TODO Get_Fields: Field [thecost] not found!! (maybe it is an alias?)
                        lngLSOrigCat = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLeased.Get_Fields("thecost"))));
                        // TODO Get_Fields: Field [thevalue] not found!! (maybe it is an alias?)
                        lngLSCat = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLeased.Get_Fields("thevalue"))));
                        // lngLSOrigCat = Round(CDbl(lngLSOrigCat), PPRounding)
                        lngLSCat = FCConvert.ToInt32(modGlobal.Round(FCConvert.ToDouble(lngLSCat) * dblRatio,
                            modPPCalculate.Statics.PPRounding));
                        lngLsOriginal += lngLSOrigCat;
                        lngLsAssessment += lngLSCat;
                    }

                    clsLeased.MoveNext();
                }


                txtLsOriginal.Text = Strings.Format(lngLsOriginal, "###,###,###,##0");
                txtLsAssessment.Text = Strings.Format(lngLsAssessment, "###,###,###,##0");
            }
        }

		
	}
}
