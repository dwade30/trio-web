﻿namespace TWPP0000
{
	/// <summary>
	/// Summary description for srptItemizedCalc.
	/// </summary>
	partial class srptItemizedCalc
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptItemizedCalc));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLine = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDepYrs = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSRO = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFCT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTY = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtR = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBETEExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYearExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDepYrs)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSRO)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFCT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTY)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtR)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBETEExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYearExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtLine,
				this.txtCD,
				this.txtYear,
				this.txtDepYrs,
				this.txtDescription,
				this.txtSRO,
				this.txtCost,
				this.txtValue,
				this.txtGD,
				this.txtFCT,
				this.txtQTY,
				this.txtR,
				this.txtBETEExempt,
				this.txtYearExempt
			});
			this.Detail.Height = 0.1666667F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Line1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Label8,
				this.Label9,
				this.Label10,
				this.Label11,
				this.Label12,
				this.Label13,
				this.Label14,
				this.Label15
			});
			this.GroupHeader1.GroupKeepTogether = GrapeCity.ActiveReports.SectionReportModel.GroupKeepTogether.FirstDetail;
			this.GroupHeader1.Height = 0.65625F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Label1
			// 
			this.Label1.Height = 0.19F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 3.416667F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label1.Text = "Itemized Description";
			this.Label1.Top = 0F;
			this.Label1.Width = 3F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.0625F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.1875F;
			this.Line1.Width = 9.6875F;
			this.Line1.X1 = 0.0625F;
			this.Line1.X2 = 9.75F;
			this.Line1.Y1 = 0.1875F;
			this.Line1.Y2 = 0.1875F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Label2.Text = "Line";
			this.Label2.Top = 0.4375F;
			this.Label2.Width = 0.4166667F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 1.375F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label3.Text = "CD";
			this.Label3.Top = 0.4375F;
			this.Label3.Width = 0.25F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 4.5625F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label4.Text = "Year";
			this.Label4.Top = 0.4375F;
			this.Label4.Width = 0.375F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.375F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 5F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label5.Text = "Dep Yrs";
			this.Label5.Top = 0.2604167F;
			this.Label5.Width = 0.4375F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 5.4375F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label6.Text = "SRO";
			this.Label6.Top = 0.4375F;
			this.Label6.Width = 0.375F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.19F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 6.4375F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label7.Text = "Cost";
			this.Label7.Top = 0.4375F;
			this.Label7.Width = 0.375F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.34375F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 6.927083F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label8.Text = "%GD";
			this.Label8.Top = 0.2708333F;
			this.Label8.Width = 0.25F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.19F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 7.25F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label9.Text = "FCT";
			this.Label9.Top = 0.4375F;
			this.Label9.Width = 0.375F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.19F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 8.177083F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label10.Text = "Value";
			this.Label10.Top = 0.4375F;
			this.Label10.Width = 0.5F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 2.072917F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label11.Text = "Description";
			this.Label11.Top = 0.4375F;
			this.Label11.Width = 2F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 1.6875F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label12.Text = "QTY";
			this.Label12.Top = 0.4375F;
			this.Label12.Width = 0.33F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 0.5F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label13.Text = "RB";
			this.Label13.Top = 0.4375F;
			this.Label13.Width = 0.2395833F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.19F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 8.75F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Label14.Text = "BETE Exempt";
			this.Label14.Top = 0.4375F;
			this.Label14.Width = 1F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.3541667F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 0.75F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label15.Text = "Year Exempt";
			this.Label15.Top = 0.2708333F;
			this.Label15.Width = 0.5729167F;
			// 
			// txtLine
			// 
			this.txtLine.CanGrow = false;
			this.txtLine.Height = 0.19F;
			this.txtLine.Left = 0F;
			this.txtLine.MultiLine = false;
			this.txtLine.Name = "txtLine";
			this.txtLine.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtLine.Text = null;
			this.txtLine.Top = 0F;
			this.txtLine.Width = 0.4166667F;
			// 
			// txtCD
			// 
			this.txtCD.CanGrow = false;
			this.txtCD.Height = 0.19F;
			this.txtCD.Left = 1.385417F;
			this.txtCD.MultiLine = false;
			this.txtCD.Name = "txtCD";
			this.txtCD.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtCD.Text = null;
			this.txtCD.Top = 0F;
			this.txtCD.Width = 0.25F;
			// 
			// txtYear
			// 
			this.txtYear.CanGrow = false;
			this.txtYear.Height = 0.19F;
			this.txtYear.Left = 4.5F;
			this.txtYear.MultiLine = false;
			this.txtYear.Name = "txtYear";
			this.txtYear.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtYear.Text = null;
			this.txtYear.Top = 0F;
			this.txtYear.Width = 0.4375F;
			// 
			// txtDepYrs
			// 
			this.txtDepYrs.CanGrow = false;
			this.txtDepYrs.Height = 0.19F;
			this.txtDepYrs.Left = 5F;
			this.txtDepYrs.MultiLine = false;
			this.txtDepYrs.Name = "txtDepYrs";
			this.txtDepYrs.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtDepYrs.Text = null;
			this.txtDepYrs.Top = 0F;
			this.txtDepYrs.Width = 0.375F;
			// 
			// txtDescription
			// 
			this.txtDescription.CanShrink = true;
			this.txtDescription.Height = 0.19F;
			this.txtDescription.Left = 2.072917F;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Style = "font-family: \'Tahoma\'";
			this.txtDescription.Text = null;
			this.txtDescription.Top = 0F;
			this.txtDescription.Width = 2.34375F;
			// 
			// txtSRO
			// 
			this.txtSRO.CanGrow = false;
			this.txtSRO.Height = 0.19F;
			this.txtSRO.Left = 5.4375F;
			this.txtSRO.MultiLine = false;
			this.txtSRO.Name = "txtSRO";
			this.txtSRO.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtSRO.Text = null;
			this.txtSRO.Top = 0F;
			this.txtSRO.Width = 0.375F;
			// 
			// txtCost
			// 
			this.txtCost.CanGrow = false;
			this.txtCost.Height = 0.19F;
			this.txtCost.Left = 5.8125F;
			this.txtCost.MultiLine = false;
			this.txtCost.Name = "txtCost";
			this.txtCost.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtCost.Text = null;
			this.txtCost.Top = 0F;
			this.txtCost.Width = 1F;
			// 
			// txtValue
			// 
			this.txtValue.CanGrow = false;
			this.txtValue.Height = 0.19F;
			this.txtValue.Left = 7.635417F;
			this.txtValue.MultiLine = false;
			this.txtValue.Name = "txtValue";
			this.txtValue.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtValue.Text = null;
			this.txtValue.Top = 0F;
			this.txtValue.Width = 1.041667F;
			// 
			// txtGD
			// 
			this.txtGD.CanGrow = false;
			this.txtGD.Height = 0.19F;
			this.txtGD.Left = 6.864583F;
			this.txtGD.MultiLine = false;
			this.txtGD.Name = "txtGD";
			this.txtGD.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtGD.Text = null;
			this.txtGD.Top = 0F;
			this.txtGD.Width = 0.3229167F;
			// 
			// txtFCT
			// 
			this.txtFCT.CanGrow = false;
			this.txtFCT.Height = 0.19F;
			this.txtFCT.Left = 7.25F;
			this.txtFCT.MultiLine = false;
			this.txtFCT.Name = "txtFCT";
			this.txtFCT.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtFCT.Text = null;
			this.txtFCT.Top = 0F;
			this.txtFCT.Width = 0.3125F;
			// 
			// txtQTY
			// 
			this.txtQTY.CanGrow = false;
			this.txtQTY.Height = 0.19F;
			this.txtQTY.Left = 1.697917F;
			this.txtQTY.MultiLine = false;
			this.txtQTY.Name = "txtQTY";
			this.txtQTY.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtQTY.Text = null;
			this.txtQTY.Top = 0F;
			this.txtQTY.Width = 0.3125F;
			// 
			// txtR
			// 
			this.txtR.Height = 0.19F;
			this.txtR.Left = 0.53125F;
			this.txtR.Name = "txtR";
			this.txtR.Style = "font-family: \'Tahoma\'; text-align: center";
			this.txtR.Text = null;
			this.txtR.Top = 0F;
			this.txtR.Width = 0.1875F;
			// 
			// txtBETEExempt
			// 
			this.txtBETEExempt.CanGrow = false;
			this.txtBETEExempt.Height = 0.19F;
			this.txtBETEExempt.Left = 8.75F;
			this.txtBETEExempt.MultiLine = false;
			this.txtBETEExempt.Name = "txtBETEExempt";
			this.txtBETEExempt.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtBETEExempt.Text = null;
			this.txtBETEExempt.Top = 0F;
			this.txtBETEExempt.Width = 1F;
			// 
			// txtYearExempt
			// 
			this.txtYearExempt.CanGrow = false;
			this.txtYearExempt.Height = 0.19F;
			this.txtYearExempt.Left = 0.7708333F;
			this.txtYearExempt.MultiLine = false;
			this.txtYearExempt.Name = "txtYearExempt";
			this.txtYearExempt.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtYearExempt.Text = null;
			this.txtYearExempt.Top = 0F;
			this.txtYearExempt.Width = 0.5520833F;
			// 
			// srptItemizedCalc
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.Name = "ActiveReport1";
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 9.833333F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDepYrs)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSRO)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFCT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTY)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtR)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBETEExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYearExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDepYrs;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSRO;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFCT;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTY;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtR;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBETEExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYearExempt;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
