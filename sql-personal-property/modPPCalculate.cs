﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Runtime.InteropServices;
using Wisej.Core;

namespace TWPP0000
{
	public class modPPCalculate
	{
		public static void CalculateSoundValue()
		{
			Statics.Work1 = Statics.clCost / 1;
			if (Statics.clQuantity > 1)
				Statics.Work1 = Statics.clCost * Statics.clQuantity;
			Statics.Work1 *= Statics.clFactor;
		}

		public static bool OriginalCostValue()
		{
			bool OriginalCostValue = false;
			try
			{
				// On Error GoTo ErrorTag
				// 
				// Check For Sound Value
				OriginalCostValue = false;
				if (Statics.clSRO == "S")
				{
					Statics.TrendedCost = Statics.clCost * Statics.clQuantity / 1;
					CalculateSoundValue();
					if (modGlobal.Statics.CustomizeStuff.RoundIndividualItems)
					{
						Statics.Work1 = modGlobal.Round(Statics.Work1, Statics.PPRounding);
					}
					goto Sound_Tag;
				}
				// 
				Statics.CurrentYear = DateTime.Now.Year;
				if (Statics.DepYear != 0)
					Statics.CurrentYear = Statics.DepYear;
				Statics.HoldYear = Statics.CLYEAR;
				Statics.Code = FCConvert.ToInt32(Math.Round(Conversion.Val(Statics.clCode)));
				if (FCConvert.ToInt16(Statics.clDPYR) != 0)
				{
					Statics.WLife = FCConvert.ToInt16(Statics.clDPYR);
				}
				else
				{
					Statics.WLife = Statics.DefLife[Statics.Code];
					Statics.clDPYR = FCConvert.ToInt32(Strings.LTrim(Conversion.Str(Statics.WLife)));
					if (Marshal.SizeOf(Statics.clDPYR) == 1)
						//Statics.clDPYR = " " + Statics.clDPYR;
						Statics.clDPYR = Statics.clDPYR;
				}
				Statics.WholeDep = Statics.clPercentGood;
				if (Statics.clSRO == "O" && Statics.CLYEAR < 1926)
				{
					Statics.CLYEAR = 1926;
					Statics.HoldYear = 1926;
				}
				else if (Statics.clSRO == "R" && Statics.CLYEAR == 0)
				{
					Statics.CLYEAR = 0;
					Statics.HoldYear = 1926;
				}
				if (Statics.HoldYear > 1925)
				{
					if (Statics.clPercentGood == 0 && Statics.clSRO == "O")
					{
						PhysicalPercent();
						// clPercentGood = PadToString(Work1, 4)
						Statics.clPercentGood = Statics.Work1;
					}
					else if (Statics.clPercentGood == 0 && Statics.clSRO == "R")
					{
						PhysicalPercent();
						// clPercentGood = PadToString(Work1, 4)
						Statics.clPercentGood = Statics.Work1;
					}
				}
				if (Statics.clSRO == "O")
				{
					Statics.HoldYear = Statics.CLYEAR;
				}
				else if (Statics.clSRO == "R")
				{
					Statics.HoldYear = Statics.clRCYR;
					if (Statics.HoldYear < 25)
					{
						Statics.HoldYear += 2000;
					}
					else
					{
						Statics.HoldYear += 1900;
					}
				}
				else
				{
					Statics.HoldYear = 0;
				}
				Statics.CurrentYear = DateTime.Now.Year;
				if (Statics.DepYear != 0)
					Statics.CurrentYear = Statics.DepYear;
				if (Statics.HoldYear != 0)
				{
					Statics.TrendAmount = 1;
					if (Statics.TrendCode[Statics.Code] == "Y")
					{
						Statics.TrendAmount = Statics.TrendMultipliers[Statics.CurrentYear - 1900] / Statics.TrendMultipliers[Statics.HoldYear - 1900];
						Statics.TrendAmount = FCConvert.ToDouble(Strings.Format(Statics.TrendAmount, "0.000"));
					}
				}
				else
				{
					Statics.TrendAmount = 0;
				}
				if (Statics.DepMeth[Statics.Code] == "S")
				{
					if (Statics.CurrentYear - Statics.HoldYear > Statics.WLife)
						Statics.HoldYear = Statics.CurrentYear - Statics.WLife;
				}
				if (Statics.clSRO == "R")
				{
					Statics.clCost = FCConvert.ToInt32(modGlobal.PadToString((fecherFoundation.FCUtils.iDiv(Statics.clCost * Statics.TrendAmount, 1)), 8));
				}
				else if (Statics.clSRO == "O")
				{
					if (modGlobal.Statics.CustomizeStuff.TrendTimesCost)
					{
						Statics.clCost = fecherFoundation.FCUtils.iDiv(Statics.clCost * Statics.TrendAmount, 1);
						Statics.clPercentGood = fecherFoundation.FCUtils.iDiv(Statics.WholeDep, 1);
					}
					else
					{
						Statics.clPercentGood = fecherFoundation.FCUtils.iDiv(Statics.WholeDep * Statics.TrendAmount, 1);
					}
				}
				Statics.TrendedCost = Statics.clCost * Statics.clQuantity / 1;
				Statics.clPercentGood = FCConvert.ToInt16(Statics.clPercentGood / 100);
				// Jim Philipps 07/20/2005
				if (modGlobal.Statics.CustomizeStuff.LimitPercentGood)
				{
					if (Statics.clPercentGood > 100)
						Statics.clPercentGood = 100;
				}
				// Work1 = clCost * clPercentGood / 100 * clFactor / 100
				Statics.Work1 = Statics.clCost * Statics.clPercentGood * Statics.clFactor / 100;
				// removed quantity per Jim P request on 6/18/2001
				Statics.Work1 *= Statics.clQuantity / 1;
				Statics.Work1 = fecherFoundation.FCUtils.iDiv(Statics.Work1, 1);
				Statics.Work2 = Statics.clCost / 1;
				Statics.Work1 = fecherFoundation.FCUtils.iDiv(Statics.Work1, 1);
				if (modGlobal.Statics.CustomizeStuff.RoundIndividualItems)
				{
					Statics.Work1 = modGlobal.Round(Statics.Work1, Statics.PPRounding);
				}
				Statics.IValue = Statics.Work1.ToString();
				Statics.IValue = modGlobal.PadToString(Statics.Work1, Statics.IValue.Length);
				Statics.clCost = FCConvert.ToInt32(modGlobal.PadToString(Statics.Work2, Marshal.SizeOf(Statics.Work1)));
				Sound_Tag:
				;
				if (Statics.clSRO == "S")
				{
					Statics.SoundCost += Statics.clCost;
					Statics.SoundValue += FCConvert.ToInt32(Conversion.Val(Statics.IValue));
				}
				else if (Statics.clSRO == "R")
				{
					Statics.ReplacementCost += Statics.clCost;
					Statics.ReplacementValue += FCConvert.ToInt32(Conversion.Val(Statics.IValue));
				}
				else if (Statics.clSRO == "O")
				{
					Statics.OriginalCost += Statics.clCost;
					Statics.OriginalValue += FCConvert.ToInt32(Conversion.Val(Statics.IValue));
				}
				Statics.ItemAccount += FCConvert.ToInt32(Conversion.Val(Statics.IValue));
				//if (Statics.clCode != "#")
				//{
				//	Statics.Item[Statics.Code] += FCConvert.ToInt32(Conversion.Val(Statics.IValue));
				//	Statics.TotalItem[Statics.Code] += FCConvert.ToInt32(Conversion.Val(Statics.IValue));
				//}
				OriginalCostValue = true;
				return OriginalCostValue;
			}
			catch (Exception ex)
			{
				// ErrorTag:
			}
			return OriginalCostValue;
		}

		public static void PhysicalPercent()
		{
			Statics.WHigh = Statics.HighPct[Statics.Code];
			Statics.WLow = Statics.LowPct[Statics.Code];
			Statics.WExp = Statics.Exponent[Statics.Code];
			if (Statics.WLife == 0)
			{
				Statics.Work1 = 0;
				MessageBox.Show("Account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount) + ".  The Life of Line " + FCConvert.ToString(Statics.xRow) + " on this screen is 0.  Correct and ReRun - You may " + " need to update the default life in the P/P Cost File.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (Statics.CurrentYear < Statics.HoldYear)
				Statics.CurrentYear = Statics.HoldYear;
			if (Statics.DepMeth[Statics.Code] == "S")
			{
				if (Statics.CurrentYear - Statics.HoldYear > Statics.WLife)
					Statics.CurrentYear = Statics.HoldYear + Statics.WLife;
				Statics.Work1 = Statics.WHigh - ((Statics.WHigh - Statics.WLow) * (Math.Pow((FCConvert.ToDouble(Statics.CurrentYear - Statics.HoldYear) / Statics.WLife), Statics.WExp)));
			}
			else
			{
				if (Statics.CurrentYear == Statics.HoldYear)
				{
					Statics.Work1 = Statics.WHigh;
				}
				else
				{
					Statics.Work1 = Math.Pow((1 - (1 / (Statics.WLife / Statics.WExp))), (Statics.CurrentYear - Statics.HoldYear));
					if (Statics.Work1 < Statics.WLow)
						Statics.Work1 = Statics.WLow;
				}
			}
			// The next few lines check for rounding of the percent good
			// and make sure that no piece of equipment has a percent good of 100
			// unless it is typed in by the user.
			// matthew
			// Work1 = Format(Work1, "##.0000")
			// If (Work1 * 10000) Mod 10000 <> 0 Then
			// WholeDep# = (CInt(Work1 + 0.5) * 100)
			// Work1 = (CInt(Work1 + 0.5) * 100) \ 1
			// Else
			// WholeDep# = (CInt(Work1) * 100)
			// Work1 = (CInt(Work1) * 100) \ 1
			// End If
			Statics.Work1 = FCConvert.ToInt16(Statics.Work1 * 100);
			Statics.WholeDep = Statics.Work1;
		}

		public static void LeasedValue()
		{
			// perform leased calculations here
			int clMO;
			int clYR = 0;
			double clPercentGoodToUse;
			if (Strings.Mid(Statics.clLeaseDate, 3, 1) == "/")
			{
				clMO = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Statics.clLeaseDate, 1, 2))));
				clYR = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Statics.clLeaseDate, 4, 2))));
			}
			else
			{
				clMO = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Statics.clLeaseDate, 1, 2))));
				clYR = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Statics.clLeaseDate, 3, 2))));
			}
			Statics.CurrentYear = DateTime.Now.Year;
			if (Statics.DepYear != 0)
				Statics.CurrentYear = Statics.DepYear;
			Statics.Code = FCConvert.ToInt32(Math.Round(Conversion.Val(Statics.clCode)));
			if (Statics.Code == 0)
				return;
			if (Statics.clPercentGood != 0)
			{
				Statics.WLife = FCConvert.ToInt32(Statics.clPercentGood);
			}
			else
			{
				Statics.WLife = Statics.DefLife[Statics.Code];
				// clPercentGood = LTrim$(Str$(WLife))
			}
			Statics.HoldYear = 1900 + clYR;
			if (Statics.HoldYear < 1926)
				Statics.HoldYear += 100;
			if (Statics.HoldYear < 2000)
			{
				if (Statics.HoldYear < (Statics.CurrentYear - 73))
				{
					Statics.HoldYear += 100;
				}
			}
			Statics.TrendAmount = 1;
			if (Statics.TrendCode[Statics.Code] == "Y")
			{
				Statics.TrendAmount = Statics.TrendMultipliers[Statics.CurrentYear - 1900] / Statics.TrendMultipliers[Statics.HoldYear - 1900];
				Statics.TrendAmount = FCConvert.ToDouble(Strings.Format(Statics.TrendAmount, "0.000"));
			}
			if (Statics.clPercentGood == 0)
			{
				PhysicalPercent();
				if (modGlobal.Statics.CustomizeStuff.TrendTimesCost)
				{
					Statics.LeasedDepreciation = FCConvert.ToInt16(Statics.WholeDep / 100);
				}
				else
				{
					Statics.LeasedDepreciation = FCConvert.ToInt16(Statics.WholeDep / 100 * Statics.TrendAmount);
				}
			}
			else
			{
				if (modGlobal.Statics.CustomizeStuff.TrendTimesCost)
				{
					Statics.LeasedDepreciation = FCConvert.ToInt16(Statics.clPercentGood / 100);
				}
				else
				{
					Statics.LeasedDepreciation = FCConvert.ToInt16(Statics.clPercentGood / 100 * Statics.TrendAmount);
				}
			}
			if (Statics.clCost == 0)
				goto ZeroCostTag;
			Statics.OriginalCost = Statics.clCost;
			goto NonZeroCostTag;
			ZeroCostTag:
			;
			Statics.Cap = Statics.CapRate[Statics.HoldYear - 1900] / 12;
			if (Statics.clLP == 1)
			{
				// NumberofMonths = clPercentGood * 12
				// 03/21/2005
				Statics.NumberofMonths = FCConvert.ToInt32((Statics.LeasedDepreciation / 100.0) * 12);
			}
			else
			{
				Statics.NumberofMonths = Statics.clMonths;
			}
			Statics.Formula1 = (Math.Pow((1 + Statics.Cap), Statics.NumberofMonths));
			Statics.Formula2 = (1 - (1 / Statics.Formula1));
			if (Statics.Formula2 == 0 && Statics.Cap == 0)
			{
				Statics.OriginalCost = 0;
				goto NonZeroCostTag;
			}
			Statics.Work1 = Statics.Formula2 / Statics.Cap;
			Statics.Work1 = Statics.clMonthlyRent * Statics.Work1;
			Statics.OriginalCost = FCConvert.ToInt32(Statics.Work1);
			NonZeroCostTag:
			;
			if (modGlobal.Statics.CustomizeStuff.TrendTimesCost)
			{
				Statics.TrendedCost = fecherFoundation.FCUtils.iDiv(Statics.clQuantity * Statics.OriginalCost * Statics.TrendAmount, 1);
				Statics.Work1 = fecherFoundation.FCUtils.iDiv(Statics.clQuantity * Statics.OriginalCost * Statics.LeasedDepreciation / 100.0 * Statics.clFactor * Statics.TrendAmount, 1);
			}
			else
			{
				Statics.TrendedCost = Statics.clQuantity * Statics.OriginalCost / 1;
				Statics.Work1 = fecherFoundation.FCUtils.iDiv(Statics.clQuantity * Statics.OriginalCost * Statics.LeasedDepreciation / 100.0 * Statics.clFactor, 1);
			}
			if (modGlobal.Statics.CustomizeStuff.RoundIndividualItems)
			{
				Statics.Work1 = modGlobal.Round(Statics.Work1, Statics.PPRounding);
			}
			Statics.LsdValue = FCConvert.ToInt32(Statics.Work1);
		}

		public static void GetCapRates()
		{
			int xx;
			clsDRWrapper clsTemp = new clsDRWrapper();
			clsTemp.OpenRecordset("select * from trendcaprates", "twpp0000.vb1");
			xx = 0;
			while (!clsTemp.EndOfFile())
			{
				xx += 1;
				Statics.CapRate[xx] = clsTemp.Get_Fields_Double("CapRate");
				clsTemp.MoveNext();
			}
		}

		public static void GetTrendValues()
		{
			int cc;
			int yy;
			clsDRWrapper rs = new clsDRWrapper();
			cc = 0;
			rs.OpenRecordset("SELECT * FROM TrendCapRates ORDER BY Year", modPPGN.strPPDatabase);
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				while (!rs.EndOfFile())
				{
					if (cc <= 175)
					{
						// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
						yy = rs.Get_Fields("Year") - 1900;
						// TODO Get_Fields: Check the table for the column [Trend] and replace with corresponding Get_Field method
						Statics.TrendMultipliers[yy] = rs.Get_Fields("Trend");
					}
					cc += 1;
					rs.MoveNext();
				}
			}
		}

		public class StaticVariables
		{
			//=========================================================
			public bool CalculateFromMaster;
			public int xRow;
			/// </summary>
			/// Variables From Cost Table
			/// <summary>
			public double[] TrendMultipliers = new double[260 + 1];
			public double[] CapRate = new double[175 + 1];
			public int[] HighPct = new int[9 + 1];
			public int[] LowPct = new int[9 + 1];
			public double[] Exponent = new double[9 + 1];
			public int[] DefLife = new int[9 + 1];
			public string[] DepMeth = new string[9 + 1];
			public string[] TrendCode = new string[9 + 1];
			public string[] DefDesc = new string[9 + 1];
			// vbPorter upgrade warning: Work1 As double	OnRead(double, int)
			public double Work1;
			public double Work2;
			public int NextPPAccount;
			public int PPRounding;
			public int RoundContingency;
			public int DepYear;
			public int WHigh;
			public int WLow;
			public double WExp;
			// vbPorter upgrade warning: WLife As short --> As int	OnWrite(int, double)
			public int WLife;
			public double WholeDep;
			// vbPorter upgrade warning: clCode As FixedString	OnWrite(string, short)
			/// </summary>
			/// Variables Used For Calculation on Both Leased & Itemized Forms
			/// <summary>
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1)]
			public string clCode = string.Empty;
			public int clQuantity;
			public int CLYEAR;
			// vbPorter upgrade warning: clDPYR As short --> As int	OnWrite(int, string)
			public int clDPYR;
			// vbPorter upgrade warning: clSRO As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1)]
			public string clSRO = string.Empty;
			public int clRCYR;
			// vbPorter upgrade warning: clCost As int	OnWrite(short, string)
			public int clCost;
			// vbPorter upgrade warning: clPercentGood As double	OnRead(double, int)
			public double clPercentGood;
			// vbPorter upgrade warning: clFactor As double	OnWrite(string, double)
			public double clFactor;
			// vbPorter upgrade warning: clLeaseDate As string	OnWrite(short, string)
			public string clLeaseDate = "";
			public int clMonths;
			public int clLP;
			public double clMonthlyRent;
			// vbPorter upgrade warning: LeasedDepreciation As Variant, short --> As int	OnWriteFCConvert.ToInt16(
			public int LeasedDepreciation;
			public double Cap;
			public double Formula1;
			public double Formula2;
			// vbPorter upgrade warning: NumberofMonths As short --> As int	OnWrite(double, int)
			public int NumberofMonths;
			public int Code;
			public int HoldYear;
			public int CurrentYear;
			//public double[] Item = new double[9 + 1];
			// vbPorter upgrade warning: ItemAccount As int	OnWrite(short, double)
			public int ItemAccount;
			public string IValue = string.Empty;
			// vbPorter upgrade warning: LsdValue As int	OnWriteFCConvert.ToDouble(
			public int LsdValue;
			// vbPorter upgrade warning: OriginalCost As int	OnWrite(int, double)
			public int OriginalCost;
			// vbPorter upgrade warning: OriginalValue As int	OnWrite(short, double)
			public int OriginalValue;
			public int ReplacementCost;
			// vbPorter upgrade warning: ReplacementValue As int	OnWrite(short, double)
			public int ReplacementValue;
			public int SoundCost;
			// vbPorter upgrade warning: SoundValue As int	OnWrite(short, double)
			public int SoundValue;
			// vbPorter upgrade warning: TotalItem As int	OnWriteFCConvert.ToDouble(
			//public int[] TotalItem = new int[9 + 1];
			// vbPorter upgrade warning: TrendAmount As double	OnWrite(short, double, string)
			public double TrendAmount;
			public int TrendedCost;
			public bool ShowfrmPPMaster;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
