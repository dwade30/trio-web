﻿namespace TWPP0000
{
	/// <summary>
	/// Summary description for rpt801b.
	/// </summary>
	partial class rpt801b
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rpt801b));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtApplicant = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAssessedDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblAssessmentYearME1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAssessmentYearFurniture1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAssessmentYearOther1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOriginalME1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOriginalFurniture1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOriginalOther1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalOriginalCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessedME1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessedFurniture1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessedOther1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessedTotal1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label42 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAllowedValue1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label43 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label44 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label47 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label48 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label49 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label50 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label51 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblAssessmentYearME2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAssessmentYearFurniture2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAssessmentYearOther2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOriginalME2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOriginalFurniture2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOriginalOther2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalOriginalCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessedME2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessedFurniture2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessedOther2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessedTotal2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label55 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAllowedValue2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label56 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label57 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label58 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label59 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label60 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label61 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label62 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label63 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label64 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblAssessmentYearME3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAssessmentYearFurniture3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAssessmentYearOther3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOriginalME3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOriginalFurniture3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOriginalOther3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalOriginalCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessedME3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessedFurniture3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessedOther3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessedTotal3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label68 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAllowedValue3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMunicipalCode = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTaxYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuniCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageBreak1 = new GrapeCity.ActiveReports.SectionReportModel.PageBreak();
			this.Label70 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label71 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label72 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label73 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label74 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label75 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label76 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label77 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label78 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label79 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label80 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label81 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label82 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label83 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label84 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label85 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label86 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label87 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label88 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label89 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label90 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label91 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label92 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label93 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label94 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label95 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label96 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label97 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label98 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblAssessmentYearME4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAssessmentYearFurniture4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAssessmentYearOther4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOriginalME4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOriginalFurniture4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOriginalOther4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalOriginalCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessedME4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessedFurniture4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessedOther4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessedTotal4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label102 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAllowedValue4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label103 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label104 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label105 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label106 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label107 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label108 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label109 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label110 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label111 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblAssessmentYearME5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAssessmentYearFurniture5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAssessmentYearOther5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOriginalME5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOriginalFurniture5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOriginalOther5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalOriginalCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessedME5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessedFurniture5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessedOther5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessedTotal5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label115 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAllowedValue5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label116 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label117 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label118 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label119 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label120 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label121 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label122 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label123 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label124 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblAssessmentYearME6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAssessmentYearFurniture6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAssessmentYearOther6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOriginalME6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOriginalFurniture6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOriginalOther6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalOriginalCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessedME6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessedFurniture6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessedOther6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessedTotal6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label128 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAllowedValue6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label129 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalAllowedValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label66 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label67 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label130 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAsOfYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label69 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label131 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label132 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtMunicipalityName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label133 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label134 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label135 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtTaxRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessedTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtApplicant)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessedDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearME1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearFurniture1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearOther1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalME1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalFurniture1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalOther1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalOriginalCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedME1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedFurniture1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedOther1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedTotal1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAllowedValue1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label48)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label49)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label50)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label51)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearME2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearFurniture2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearOther2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalME2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalFurniture2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalOther2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalOriginalCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedME2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedFurniture2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedOther2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedTotal2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label55)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAllowedValue2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label56)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label57)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label58)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label59)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label60)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label61)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label62)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label63)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label64)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearME3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearFurniture3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearOther3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalME3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalFurniture3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalOther3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalOriginalCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedME3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedFurniture3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedOther3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedTotal3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label68)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAllowedValue3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMunicipalCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTaxYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label70)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label71)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label72)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label73)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label74)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label75)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label76)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label77)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label78)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label79)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label80)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label81)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label82)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label83)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label84)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label85)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label86)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label87)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label88)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label89)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label90)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label91)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label92)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label93)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label94)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label95)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label96)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label97)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label98)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearME4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearFurniture4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearOther4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalME4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalFurniture4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalOther4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalOriginalCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedME4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedFurniture4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedOther4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedTotal4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label102)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAllowedValue4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label103)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label104)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label105)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label106)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label107)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label108)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label109)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label110)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label111)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearME5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearFurniture5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearOther5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalME5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalFurniture5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalOther5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalOriginalCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedME5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedFurniture5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedOther5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedTotal5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label115)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAllowedValue5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label116)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label117)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label118)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label119)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label120)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label121)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label122)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label123)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label124)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearME6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearFurniture6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearOther6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalME6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalFurniture6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalOther6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalOriginalCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedME6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedFurniture6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedOther6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedTotal6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label128)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAllowedValue6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label129)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAllowedValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label66)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label67)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label130)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAsOfYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label69)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label131)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label132)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMunicipalityName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label133)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label134)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label135)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMuniName,
				this.Label6,
				this.Label7,
				this.txtApplicant,
				this.Label8,
				this.lblAssessedDate,
				this.Label15,
				this.Label16,
				this.txtLocation,
				this.Label17,
				this.Label18,
				this.Label19,
				this.Label20,
				this.Label21,
				this.Label22,
				this.Label23,
				this.Label24,
				this.Label25,
				this.Label26,
				this.Label27,
				this.Label28,
				this.Label29,
				this.Label30,
				this.Label31,
				this.Label32,
				this.Label33,
				this.Label34,
				this.Label35,
				this.Label36,
				this.Label37,
				this.Label38,
				this.Line2,
				this.Line3,
				this.Line1,
				this.lblAssessmentYearME1,
				this.lblAssessmentYearFurniture1,
				this.lblAssessmentYearOther1,
				this.txtOriginalME1,
				this.txtOriginalFurniture1,
				this.txtOriginalOther1,
				this.txtTotalOriginalCost1,
				this.txtAssessedME1,
				this.txtAssessedFurniture1,
				this.txtAssessedOther1,
				this.txtAssessedTotal1,
				this.Label42,
				this.txtAllowedValue1,
				this.Label3,
				this.Line8,
				this.Label43,
				this.Label44,
				this.Label45,
				this.Label46,
				this.Label47,
				this.Label48,
				this.Label49,
				this.Label50,
				this.Label51,
				this.Line9,
				this.Line10,
				this.Line11,
				this.lblAssessmentYearME2,
				this.lblAssessmentYearFurniture2,
				this.lblAssessmentYearOther2,
				this.txtOriginalME2,
				this.txtOriginalFurniture2,
				this.txtOriginalOther2,
				this.txtTotalOriginalCost2,
				this.txtAssessedME2,
				this.txtAssessedFurniture2,
				this.txtAssessedOther2,
				this.txtAssessedTotal2,
				this.Label55,
				this.txtAllowedValue2,
				this.Line12,
				this.Label56,
				this.Label57,
				this.Label58,
				this.Label59,
				this.Label60,
				this.Label61,
				this.Label62,
				this.Label63,
				this.Label64,
				this.Line13,
				this.Line14,
				this.Line15,
				this.lblAssessmentYearME3,
				this.lblAssessmentYearFurniture3,
				this.lblAssessmentYearOther3,
				this.txtOriginalME3,
				this.txtOriginalFurniture3,
				this.txtOriginalOther3,
				this.txtTotalOriginalCost3,
				this.txtAssessedME3,
				this.txtAssessedFurniture3,
				this.txtAssessedOther3,
				this.txtAssessedTotal3,
				this.Label68,
				this.txtAllowedValue3,
				this.Label1,
				this.Label2,
				this.Label4,
				this.Label5,
				this.lblMunicipalCode,
				this.lblTaxYear,
				this.txtMuniCode,
				this.PageBreak1,
				this.Label70,
				this.Label71,
				this.Label72,
				this.Label73,
				this.Label74,
				this.Label75,
				this.Line17,
				this.Label76,
				this.Label77,
				this.Label78,
				this.Label79,
				this.Label80,
				this.Label81,
				this.Label82,
				this.Label83,
				this.Label84,
				this.Label85,
				this.Label86,
				this.Label87,
				this.Label88,
				this.Label89,
				this.Label90,
				this.Label91,
				this.Label92,
				this.Label93,
				this.Label94,
				this.Label95,
				this.Label96,
				this.Label97,
				this.Label98,
				this.Line18,
				this.Line19,
				this.Line20,
				this.lblAssessmentYearME4,
				this.lblAssessmentYearFurniture4,
				this.lblAssessmentYearOther4,
				this.txtOriginalME4,
				this.txtOriginalFurniture4,
				this.txtOriginalOther4,
				this.txtTotalOriginalCost4,
				this.txtAssessedME4,
				this.txtAssessedFurniture4,
				this.txtAssessedOther4,
				this.txtAssessedTotal4,
				this.Label102,
				this.txtAllowedValue4,
				this.Line21,
				this.Label103,
				this.Label104,
				this.Label105,
				this.Label106,
				this.Label107,
				this.Label108,
				this.Label109,
				this.Label110,
				this.Label111,
				this.Line22,
				this.Line23,
				this.Line24,
				this.lblAssessmentYearME5,
				this.lblAssessmentYearFurniture5,
				this.lblAssessmentYearOther5,
				this.txtOriginalME5,
				this.txtOriginalFurniture5,
				this.txtOriginalOther5,
				this.txtTotalOriginalCost5,
				this.txtAssessedME5,
				this.txtAssessedFurniture5,
				this.txtAssessedOther5,
				this.txtAssessedTotal5,
				this.Label115,
				this.txtAllowedValue5,
				this.Line25,
				this.Label116,
				this.Label117,
				this.Label118,
				this.Label119,
				this.Label120,
				this.Label121,
				this.Label122,
				this.Label123,
				this.Label124,
				this.Line26,
				this.Line27,
				this.Line28,
				this.lblAssessmentYearME6,
				this.lblAssessmentYearFurniture6,
				this.lblAssessmentYearOther6,
				this.txtOriginalME6,
				this.txtOriginalFurniture6,
				this.txtOriginalOther6,
				this.txtTotalOriginalCost6,
				this.txtAssessedME6,
				this.txtAssessedFurniture6,
				this.txtAssessedOther6,
				this.txtAssessedTotal6,
				this.Label128,
				this.txtAllowedValue6,
				this.Label129,
				this.txtTotalAllowedValue,
				this.lblPage
			});
			this.Detail.Height = 16.59375F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label39,
				this.Label66,
				this.Label67,
				this.Label130,
				this.txtAsOfYear,
				this.Label69,
				this.Label131,
				this.Label132,
				this.Line29,
				this.txtMunicipalityName,
				this.Label133,
				this.Label134,
				this.Line30,
				this.Line4,
				this.txtDate,
				this.Label135,
				this.Line5,
				this.txtTaxRate,
				this.txtAssessedTax
			});
			this.ReportFooter.Height = 1.447917F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1666667F;
			this.txtMuniName.Left = 1.84375F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'";
			this.txtMuniName.Text = null;
			this.txtMuniName.Top = 0.9791667F;
			this.txtMuniName.Width = 5.15625F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1666667F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0.01041667F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.Label6.Text = "1. MUNICIPALITY OF";
			this.Label6.Top = 0.9791667F;
			this.Label6.Width = 1.791667F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.21875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0.2291667F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "";
			//FC:FINAL:CHN - issue #1357: Fix report. Change incorrect symbol. 
			// this.Label7.Text = "Dear Assessor: Pursuant to 36 MRSA Ã¯Â¿Â½ 6653, you are hereby notified that:";
			this.Label7.Text = "Dear Assessor: Pursuant to 36 MRSA § 6653, you are hereby notified that:";
			this.Label7.Top = 1.3125F;
			this.Label7.Width = 5.65625F;
			// 
			// txtApplicant
			// 
			this.txtApplicant.Height = 0.1666667F;
			this.txtApplicant.Left = 0.2291667F;
			this.txtApplicant.Name = "txtApplicant";
			this.txtApplicant.Text = null;
			this.txtApplicant.Top = 1.520833F;
			this.txtApplicant.Width = 7.052083F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.3645833F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0.2291667F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "";
			this.Label8.Text = resources.GetString("Label8.Text");
			this.Label8.Top = 1.6875F;
			this.Label8.Width = 7.041667F;
			// 
			// lblAssessedDate
			// 
			this.lblAssessedDate.Height = 0.21875F;
			this.lblAssessedDate.HyperLink = null;
			this.lblAssessedDate.Left = 6.09375F;
			this.lblAssessedDate.Name = "lblAssessedDate";
			this.lblAssessedDate.Style = "";
			this.lblAssessedDate.Text = "April 1,";
			this.lblAssessedDate.Top = 1.854167F;
			this.lblAssessedDate.Width = 0.9375F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1666667F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 0.01041667F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.Label15.Text = "SECTION 2. Schedule for Business Equipment Tax Reimbursement";
			this.Label15.Top = 2.09375F;
			this.Label15.Width = 6.833333F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 0.01041667F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "";
			this.Label16.Text = "The eligible business property is situated at (street address, map & lot, account" + " #, etc.):";
			this.Label16.Top = 2.322917F;
			this.Label16.Width = 5.895833F;
			// 
			// txtLocation
			// 
			this.txtLocation.Height = 0.1666667F;
			this.txtLocation.Left = 0F;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Text = "Field1";
			this.txtLocation.Top = 2.5F;
			this.txtLocation.Width = 7.427083F;
			// 
			// Label17
			// 
			this.Label17.Height = 1.083333F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 0F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "";
			this.Label17.Text = resources.GetString("Label17.Text");
			this.Label17.Top = 2.729167F;
			this.Label17.Width = 7.239583F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1666667F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 0.5625F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.Label18.Text = "A.";
			this.Label18.Top = 3.875F;
			this.Label18.Width = 0.2916667F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1666667F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 2.125F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.Label19.Text = "B.";
			this.Label19.Top = 3.875F;
			this.Label19.Width = 0.2916667F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1666667F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 3.125F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.Label20.Text = "C.*";
			this.Label20.Top = 3.875F;
			this.Label20.Width = 0.2916667F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1666667F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 4.0625F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.Label21.Text = "D.";
			this.Label21.Top = 3.875F;
			this.Label21.Width = 0.2916667F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.1666667F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 4.9375F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.Label22.Text = "E.";
			this.Label22.Top = 3.875F;
			this.Label22.Width = 0.2916667F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.1666667F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 6.0625F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.Label23.Text = "F.";
			this.Label23.Top = 3.875F;
			this.Label23.Width = 0.2916667F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.1875F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 0F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "";
			this.Label24.Text = "1. Machinery & Equipment";
			this.Label24.Top = 4.604167F;
			this.Label24.Width = 1.8125F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.1875F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 0F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "";
			this.Label25.Text = "2. Furniture";
			this.Label25.Top = 4.854167F;
			this.Label25.Width = 1.8125F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.1875F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 0F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "";
			this.Label26.Text = "3. Other";
			this.Label26.Top = 5.104167F;
			this.Label26.Width = 1.8125F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.1875F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 0F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "";
			this.Label27.Text = "4. TOTALS  (for columns E & F, add lines 1,2 and 3)";
			this.Label27.Top = 5.354167F;
			this.Label27.Width = 3.3125F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.1875F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 0F;
			this.Label28.Name = "Label28";
			this.Label28.Style = "";
			this.Label28.Text = "5. Value Limitation";
			this.Label28.Top = 5.604167F;
			this.Label28.Width = 1.8125F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.1875F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 0F;
			this.Label29.Name = "Label29";
			this.Label29.Style = "";
			this.Label29.Text = "6. Allowable Assessed Value (in column F, multiply line 4 by line 5)";
			this.Label29.Top = 5.854167F;
			this.Label29.Width = 4.208333F;
			// 
			// Label30
			// 
			this.Label30.Height = 0.1875F;
			this.Label30.HyperLink = null;
			this.Label30.Left = 3.0625F;
			this.Label30.Name = "Label30";
			this.Label30.Style = "";
			this.Label30.Text = "13";
			this.Label30.Top = 4.604167F;
			this.Label30.Width = 0.28125F;
			// 
			// Label31
			// 
			this.Label31.Height = 0.1875F;
			this.Label31.HyperLink = null;
			this.Label31.Left = 3.0625F;
			this.Label31.Name = "Label31";
			this.Label31.Style = "";
			this.Label31.Text = "13";
			this.Label31.Top = 4.854167F;
			this.Label31.Width = 0.28125F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.1875F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 3.0625F;
			this.Label32.Name = "Label32";
			this.Label32.Style = "";
			this.Label32.Text = "13";
			this.Label32.Top = 5.104167F;
			this.Label32.Width = 0.28125F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.4375F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 1.916667F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-size: 8.5pt; text-align: center";
			this.Label33.Text = "State of Origin (if acquired used)";
			this.Label33.Top = 4.104167F;
			this.Label33.Width = 0.8125F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.4375F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 0F;
			this.Label34.Name = "Label34";
			this.Label34.Style = "font-size: 8.5pt; text-align: center";
			this.Label34.Text = "Property Description Category";
			this.Label34.Top = 4.104167F;
			this.Label34.Width = 1.239583F;
			// 
			// Label35
			// 
			this.Label35.Height = 0.4375F;
			this.Label35.HyperLink = null;
			this.Label35.Left = 2.8125F;
			this.Label35.Name = "Label35";
			this.Label35.Style = "font-size: 8.5pt; text-align: center";
			this.Label35.Text = "Number of Years Claimed";
			this.Label35.Top = 4.104167F;
			this.Label35.Width = 0.8125F;
			// 
			// Label36
			// 
			this.Label36.Height = 0.4375F;
			this.Label36.HyperLink = null;
			this.Label36.Left = 3.75F;
			this.Label36.Name = "Label36";
			this.Label36.Style = "font-size: 8.5pt; text-align: center";
			this.Label36.Text = "Original Assessment Year";
			this.Label36.Top = 4.104167F;
			this.Label36.Width = 0.8125F;
			// 
			// Label37
			// 
			this.Label37.Height = 0.4375F;
			this.Label37.HyperLink = null;
			this.Label37.Left = 4.625F;
			this.Label37.Name = "Label37";
			this.Label37.Style = "font-size: 8.5pt; text-align: center";
			this.Label37.Text = "Original Cost";
			this.Label37.Top = 4.104167F;
			this.Label37.Width = 0.8125F;
			// 
			// Label38
			// 
			this.Label38.Height = 0.5104167F;
			this.Label38.HyperLink = null;
			this.Label38.Left = 5.5625F;
			this.Label38.Name = "Label38";
			this.Label38.Style = "font-size: 8.5pt; text-align: center";
			this.Label38.Text = "Assessed Value   (To be completed by local tax assessor)";
			this.Label38.Top = 4.104167F;
			//FC:FINAL:CHN - issue #1357: Fix report. Fix incorrect wordwrap. 
			// this.Label38.Width = 1.083333F;
			this.Label38.Width = 1.108f;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 1.944444F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 5.006945F;
			this.Line2.Width = 0.7326388F;
			this.Line2.X1 = 1.944444F;
			this.Line2.X2 = 2.677083F;
			this.Line2.Y1 = 5.006945F;
			this.Line2.Y2 = 5.006945F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 1.944444F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 5.243055F;
			this.Line3.Width = 0.7326388F;
			this.Line3.X1 = 1.944444F;
			this.Line3.X2 = 2.677083F;
			this.Line3.Y1 = 5.243055F;
			this.Line3.Y2 = 5.243055F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 1.944444F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 4.777778F;
			this.Line1.Width = 0.7326388F;
			this.Line1.X1 = 1.944444F;
			this.Line1.X2 = 2.677083F;
			this.Line1.Y1 = 4.777778F;
			this.Line1.Y2 = 4.777778F;
			// 
			// lblAssessmentYearME1
			// 
			this.lblAssessmentYearME1.Height = 0.1875F;
			this.lblAssessmentYearME1.HyperLink = null;
			this.lblAssessmentYearME1.Left = 4.020833F;
			this.lblAssessmentYearME1.Name = "lblAssessmentYearME1";
			this.lblAssessmentYearME1.Style = "";
			this.lblAssessmentYearME1.Text = null;
			this.lblAssessmentYearME1.Top = 4.604167F;
			this.lblAssessmentYearME1.Width = 0.3958333F;
			// 
			// lblAssessmentYearFurniture1
			// 
			this.lblAssessmentYearFurniture1.Height = 0.1875F;
			this.lblAssessmentYearFurniture1.HyperLink = null;
			this.lblAssessmentYearFurniture1.Left = 4.020833F;
			this.lblAssessmentYearFurniture1.Name = "lblAssessmentYearFurniture1";
			this.lblAssessmentYearFurniture1.Style = "";
			this.lblAssessmentYearFurniture1.Text = null;
			this.lblAssessmentYearFurniture1.Top = 4.854167F;
			this.lblAssessmentYearFurniture1.Width = 0.3958333F;
			// 
			// lblAssessmentYearOther1
			// 
			this.lblAssessmentYearOther1.Height = 0.1875F;
			this.lblAssessmentYearOther1.HyperLink = null;
			this.lblAssessmentYearOther1.Left = 4.020833F;
			this.lblAssessmentYearOther1.Name = "lblAssessmentYearOther1";
			this.lblAssessmentYearOther1.Style = "";
			this.lblAssessmentYearOther1.Text = null;
			this.lblAssessmentYearOther1.Top = 5.104167F;
			this.lblAssessmentYearOther1.Width = 0.3958333F;
			// 
			// txtOriginalME1
			// 
			this.txtOriginalME1.Height = 0.1875F;
			this.txtOriginalME1.Left = 4.625F;
			this.txtOriginalME1.Name = "txtOriginalME1";
			this.txtOriginalME1.Style = "text-align: right";
			this.txtOriginalME1.Text = null;
			this.txtOriginalME1.Top = 4.604167F;
			this.txtOriginalME1.Width = 0.7916667F;
			// 
			// txtOriginalFurniture1
			// 
			this.txtOriginalFurniture1.Height = 0.1875F;
			this.txtOriginalFurniture1.Left = 4.625F;
			this.txtOriginalFurniture1.Name = "txtOriginalFurniture1";
			this.txtOriginalFurniture1.Style = "text-align: right";
			this.txtOriginalFurniture1.Text = null;
			this.txtOriginalFurniture1.Top = 4.854167F;
			this.txtOriginalFurniture1.Width = 0.7916667F;
			// 
			// txtOriginalOther1
			// 
			this.txtOriginalOther1.Height = 0.1875F;
			this.txtOriginalOther1.Left = 4.625F;
			this.txtOriginalOther1.Name = "txtOriginalOther1";
			this.txtOriginalOther1.Style = "text-align: right";
			this.txtOriginalOther1.Text = null;
			this.txtOriginalOther1.Top = 5.104167F;
			this.txtOriginalOther1.Width = 0.7916667F;
			// 
			// txtTotalOriginalCost1
			// 
			this.txtTotalOriginalCost1.Height = 0.1875F;
			this.txtTotalOriginalCost1.Left = 4.625F;
			this.txtTotalOriginalCost1.Name = "txtTotalOriginalCost1";
			this.txtTotalOriginalCost1.Style = "text-align: right";
			this.txtTotalOriginalCost1.Text = null;
			this.txtTotalOriginalCost1.Top = 5.354167F;
			this.txtTotalOriginalCost1.Width = 0.7916667F;
			// 
			// txtAssessedME1
			// 
			this.txtAssessedME1.Height = 0.1875F;
			this.txtAssessedME1.Left = 5.760417F;
			this.txtAssessedME1.Name = "txtAssessedME1";
			this.txtAssessedME1.Style = "text-align: right";
			this.txtAssessedME1.Text = null;
			this.txtAssessedME1.Top = 4.604167F;
			this.txtAssessedME1.Width = 0.90625F;
			// 
			// txtAssessedFurniture1
			// 
			this.txtAssessedFurniture1.Height = 0.1875F;
			this.txtAssessedFurniture1.Left = 5.760417F;
			this.txtAssessedFurniture1.Name = "txtAssessedFurniture1";
			this.txtAssessedFurniture1.Style = "text-align: right";
			this.txtAssessedFurniture1.Text = null;
			this.txtAssessedFurniture1.Top = 4.854167F;
			this.txtAssessedFurniture1.Width = 0.90625F;
			// 
			// txtAssessedOther1
			// 
			this.txtAssessedOther1.Height = 0.1875F;
			this.txtAssessedOther1.Left = 5.75F;
			this.txtAssessedOther1.Name = "txtAssessedOther1";
			this.txtAssessedOther1.Style = "text-align: right";
			this.txtAssessedOther1.Text = null;
			this.txtAssessedOther1.Top = 5.104167F;
			this.txtAssessedOther1.Width = 0.9166667F;
			// 
			// txtAssessedTotal1
			// 
			this.txtAssessedTotal1.Height = 0.1875F;
			this.txtAssessedTotal1.Left = 5.75F;
			this.txtAssessedTotal1.Name = "txtAssessedTotal1";
			this.txtAssessedTotal1.Style = "text-align: right";
			this.txtAssessedTotal1.Text = null;
			this.txtAssessedTotal1.Top = 5.354167F;
			this.txtAssessedTotal1.Width = 0.9166667F;
			// 
			// Label42
			// 
			this.Label42.Height = 0.1875F;
			this.Label42.HyperLink = null;
			this.Label42.Left = 6.0625F;
			this.Label42.Name = "Label42";
			this.Label42.Style = "";
			this.Label42.Text = ".75";
			this.Label42.Top = 5.604167F;
			this.Label42.Width = 0.28125F;
			// 
			// txtAllowedValue1
			// 
			this.txtAllowedValue1.Height = 0.1875F;
			this.txtAllowedValue1.Left = 5.75F;
			this.txtAllowedValue1.Name = "txtAllowedValue1";
			this.txtAllowedValue1.Style = "text-align: right";
			this.txtAllowedValue1.Text = null;
			this.txtAllowedValue1.Top = 5.854167F;
			this.txtAllowedValue1.Width = 0.9166667F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1666667F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.Label3.Text = "SECTION 1. Notice of Intent";
			this.Label3.Top = 0.75F;
			this.Label3.Width = 2.114583F;
			// 
			// Line8
			// 
			this.Line8.Height = 0F;
			this.Line8.Left = 0.0625F;
			this.Line8.LineWeight = 1F;
			this.Line8.Name = "Line8";
			this.Line8.Top = 6.072917F;
			this.Line8.Width = 7.375F;
			this.Line8.X1 = 0.0625F;
			this.Line8.X2 = 7.4375F;
			this.Line8.Y1 = 6.072917F;
			this.Line8.Y2 = 6.072917F;
			// 
			// Label43
			// 
			this.Label43.Height = 0.1875F;
			this.Label43.HyperLink = null;
			this.Label43.Left = 0F;
			this.Label43.Name = "Label43";
			this.Label43.Style = "";
			this.Label43.Text = "7. Machinery & Equipment";
			this.Label43.Top = 6.166667F;
			this.Label43.Width = 1.8125F;
			// 
			// Label44
			// 
			this.Label44.Height = 0.1875F;
			this.Label44.HyperLink = null;
			this.Label44.Left = 0F;
			this.Label44.Name = "Label44";
			this.Label44.Style = "";
			this.Label44.Text = "8. Furniture";
			this.Label44.Top = 6.416667F;
			this.Label44.Width = 1.8125F;
			// 
			// Label45
			// 
			this.Label45.Height = 0.1875F;
			this.Label45.HyperLink = null;
			this.Label45.Left = 0F;
			this.Label45.Name = "Label45";
			this.Label45.Style = "";
			this.Label45.Text = "9. Other";
			this.Label45.Top = 6.666667F;
			this.Label45.Width = 1.8125F;
			// 
			// Label46
			// 
			this.Label46.Height = 0.1875F;
			this.Label46.HyperLink = null;
			this.Label46.Left = 0F;
			this.Label46.Name = "Label46";
			this.Label46.Style = "";
			this.Label46.Text = "10. TOTALS (for columns E & F, add lines 7, 8 and 9)";
			this.Label46.Top = 6.916667F;
			this.Label46.Width = 4.0625F;
			// 
			// Label47
			// 
			this.Label47.Height = 0.1875F;
			this.Label47.HyperLink = null;
			this.Label47.Left = 0F;
			this.Label47.Name = "Label47";
			this.Label47.Style = "";
			this.Label47.Text = "11. Value Limitation";
			this.Label47.Top = 7.166667F;
			this.Label47.Width = 1.8125F;
			// 
			// Label48
			// 
			this.Label48.Height = 0.1875F;
			this.Label48.HyperLink = null;
			this.Label48.Left = 0F;
			this.Label48.Name = "Label48";
			this.Label48.Style = "";
			this.Label48.Text = "12. Allowable Assessed Value (in column F, multiply line 10 by line 11)";
			this.Label48.Top = 7.416667F;
			this.Label48.Width = 4.208333F;
			// 
			// Label49
			// 
			this.Label49.Height = 0.1875F;
			this.Label49.HyperLink = null;
			this.Label49.Left = 3.0625F;
			this.Label49.Name = "Label49";
			this.Label49.Style = "";
			this.Label49.Text = "14";
			this.Label49.Top = 6.166667F;
			this.Label49.Width = 0.28125F;
			// 
			// Label50
			// 
			this.Label50.Height = 0.1875F;
			this.Label50.HyperLink = null;
			this.Label50.Left = 3.0625F;
			this.Label50.Name = "Label50";
			this.Label50.Style = "";
			this.Label50.Text = "14";
			this.Label50.Top = 6.416667F;
			this.Label50.Width = 0.28125F;
			// 
			// Label51
			// 
			this.Label51.Height = 0.1875F;
			this.Label51.HyperLink = null;
			this.Label51.Left = 3.0625F;
			this.Label51.Name = "Label51";
			this.Label51.Style = "";
			this.Label51.Text = "14";
			this.Label51.Top = 6.666667F;
			this.Label51.Width = 0.28125F;
			// 
			// Line9
			// 
			this.Line9.Height = 0F;
			this.Line9.Left = 1.944444F;
			this.Line9.LineWeight = 1F;
			this.Line9.Name = "Line9";
			this.Line9.Top = 6.569445F;
			this.Line9.Width = 0.7326388F;
			this.Line9.X1 = 1.944444F;
			this.Line9.X2 = 2.677083F;
			this.Line9.Y1 = 6.569445F;
			this.Line9.Y2 = 6.569445F;
			// 
			// Line10
			// 
			this.Line10.Height = 0F;
			this.Line10.Left = 1.944444F;
			this.Line10.LineWeight = 1F;
			this.Line10.Name = "Line10";
			this.Line10.Top = 6.805555F;
			this.Line10.Width = 0.7326388F;
			this.Line10.X1 = 1.944444F;
			this.Line10.X2 = 2.677083F;
			this.Line10.Y1 = 6.805555F;
			this.Line10.Y2 = 6.805555F;
			// 
			// Line11
			// 
			this.Line11.Height = 0F;
			this.Line11.Left = 1.944444F;
			this.Line11.LineWeight = 1F;
			this.Line11.Name = "Line11";
			this.Line11.Top = 6.340278F;
			this.Line11.Width = 0.7326388F;
			this.Line11.X1 = 1.944444F;
			this.Line11.X2 = 2.677083F;
			this.Line11.Y1 = 6.340278F;
			this.Line11.Y2 = 6.340278F;
			// 
			// lblAssessmentYearME2
			// 
			this.lblAssessmentYearME2.Height = 0.1875F;
			this.lblAssessmentYearME2.HyperLink = null;
			this.lblAssessmentYearME2.Left = 4.020833F;
			this.lblAssessmentYearME2.Name = "lblAssessmentYearME2";
			this.lblAssessmentYearME2.Style = "";
			this.lblAssessmentYearME2.Text = "2006";
			this.lblAssessmentYearME2.Top = 6.166667F;
			this.lblAssessmentYearME2.Width = 0.3958333F;
			// 
			// lblAssessmentYearFurniture2
			// 
			this.lblAssessmentYearFurniture2.Height = 0.1875F;
			this.lblAssessmentYearFurniture2.HyperLink = null;
			this.lblAssessmentYearFurniture2.Left = 4.020833F;
			this.lblAssessmentYearFurniture2.Name = "lblAssessmentYearFurniture2";
			this.lblAssessmentYearFurniture2.Style = "";
			this.lblAssessmentYearFurniture2.Text = "2006";
			this.lblAssessmentYearFurniture2.Top = 6.416667F;
			this.lblAssessmentYearFurniture2.Width = 0.3958333F;
			// 
			// lblAssessmentYearOther2
			// 
			this.lblAssessmentYearOther2.Height = 0.1875F;
			this.lblAssessmentYearOther2.HyperLink = null;
			this.lblAssessmentYearOther2.Left = 4.020833F;
			this.lblAssessmentYearOther2.Name = "lblAssessmentYearOther2";
			this.lblAssessmentYearOther2.Style = "";
			this.lblAssessmentYearOther2.Text = "2006";
			this.lblAssessmentYearOther2.Top = 6.666667F;
			this.lblAssessmentYearOther2.Width = 0.3958333F;
			// 
			// txtOriginalME2
			// 
			this.txtOriginalME2.Height = 0.1875F;
			this.txtOriginalME2.Left = 4.625F;
			this.txtOriginalME2.Name = "txtOriginalME2";
			this.txtOriginalME2.Style = "text-align: right";
			this.txtOriginalME2.Text = null;
			this.txtOriginalME2.Top = 6.166667F;
			this.txtOriginalME2.Width = 0.7916667F;
			// 
			// txtOriginalFurniture2
			// 
			this.txtOriginalFurniture2.Height = 0.1875F;
			this.txtOriginalFurniture2.Left = 4.625F;
			this.txtOriginalFurniture2.Name = "txtOriginalFurniture2";
			this.txtOriginalFurniture2.Style = "text-align: right";
			this.txtOriginalFurniture2.Text = null;
			this.txtOriginalFurniture2.Top = 6.416667F;
			this.txtOriginalFurniture2.Width = 0.7916667F;
			// 
			// txtOriginalOther2
			// 
			this.txtOriginalOther2.Height = 0.1875F;
			this.txtOriginalOther2.Left = 4.625F;
			this.txtOriginalOther2.Name = "txtOriginalOther2";
			this.txtOriginalOther2.Style = "text-align: right";
			this.txtOriginalOther2.Text = null;
			this.txtOriginalOther2.Top = 6.666667F;
			this.txtOriginalOther2.Width = 0.7916667F;
			// 
			// txtTotalOriginalCost2
			// 
			this.txtTotalOriginalCost2.Height = 0.1875F;
			this.txtTotalOriginalCost2.Left = 4.625F;
			this.txtTotalOriginalCost2.Name = "txtTotalOriginalCost2";
			this.txtTotalOriginalCost2.Style = "text-align: right";
			this.txtTotalOriginalCost2.Text = null;
			this.txtTotalOriginalCost2.Top = 6.916667F;
			this.txtTotalOriginalCost2.Width = 0.7916667F;
			// 
			// txtAssessedME2
			// 
			this.txtAssessedME2.Height = 0.1875F;
			this.txtAssessedME2.Left = 5.760417F;
			this.txtAssessedME2.Name = "txtAssessedME2";
			this.txtAssessedME2.Style = "text-align: right";
			this.txtAssessedME2.Text = null;
			this.txtAssessedME2.Top = 6.166667F;
			this.txtAssessedME2.Width = 0.90625F;
			// 
			// txtAssessedFurniture2
			// 
			this.txtAssessedFurniture2.Height = 0.1875F;
			this.txtAssessedFurniture2.Left = 5.760417F;
			this.txtAssessedFurniture2.Name = "txtAssessedFurniture2";
			this.txtAssessedFurniture2.Style = "text-align: right";
			this.txtAssessedFurniture2.Text = null;
			this.txtAssessedFurniture2.Top = 6.416667F;
			this.txtAssessedFurniture2.Width = 0.90625F;
			// 
			// txtAssessedOther2
			// 
			this.txtAssessedOther2.Height = 0.1875F;
			this.txtAssessedOther2.Left = 5.75F;
			this.txtAssessedOther2.Name = "txtAssessedOther2";
			this.txtAssessedOther2.Style = "text-align: right";
			this.txtAssessedOther2.Text = null;
			this.txtAssessedOther2.Top = 6.666667F;
			this.txtAssessedOther2.Width = 0.9166667F;
			// 
			// txtAssessedTotal2
			// 
			this.txtAssessedTotal2.Height = 0.1875F;
			this.txtAssessedTotal2.Left = 5.75F;
			this.txtAssessedTotal2.Name = "txtAssessedTotal2";
			this.txtAssessedTotal2.Style = "text-align: right";
			this.txtAssessedTotal2.Text = null;
			this.txtAssessedTotal2.Top = 6.916667F;
			this.txtAssessedTotal2.Width = 0.9166667F;
			// 
			// Label55
			// 
			this.Label55.Height = 0.1875F;
			this.Label55.HyperLink = null;
			this.Label55.Left = 6.0625F;
			this.Label55.Name = "Label55";
			this.Label55.Style = "";
			this.Label55.Text = ".70";
			this.Label55.Top = 7.166667F;
			this.Label55.Width = 0.28125F;
			// 
			// txtAllowedValue2
			// 
			this.txtAllowedValue2.Height = 0.1875F;
			this.txtAllowedValue2.Left = 5.75F;
			this.txtAllowedValue2.Name = "txtAllowedValue2";
			this.txtAllowedValue2.Style = "text-align: right";
			this.txtAllowedValue2.Text = null;
			this.txtAllowedValue2.Top = 7.416667F;
			this.txtAllowedValue2.Width = 0.9166667F;
			// 
			// Line12
			// 
			this.Line12.Height = 0F;
			this.Line12.Left = 0.0625F;
			this.Line12.LineWeight = 1F;
			this.Line12.Name = "Line12";
			this.Line12.Top = 7.635417F;
			this.Line12.Width = 7.375F;
			this.Line12.X1 = 0.0625F;
			this.Line12.X2 = 7.4375F;
			this.Line12.Y1 = 7.635417F;
			this.Line12.Y2 = 7.635417F;
			// 
			// Label56
			// 
			this.Label56.Height = 0.1875F;
			this.Label56.HyperLink = null;
			this.Label56.Left = 0F;
			this.Label56.Name = "Label56";
			this.Label56.Style = "";
			this.Label56.Text = "13. Machinery & Equipment";
			this.Label56.Top = 7.729167F;
			this.Label56.Width = 1.8125F;
			// 
			// Label57
			// 
			this.Label57.Height = 0.1875F;
			this.Label57.HyperLink = null;
			this.Label57.Left = 0F;
			this.Label57.Name = "Label57";
			this.Label57.Style = "";
			this.Label57.Text = "14. Furniture";
			this.Label57.Top = 7.979167F;
			this.Label57.Width = 1.8125F;
			// 
			// Label58
			// 
			this.Label58.Height = 0.1875F;
			this.Label58.HyperLink = null;
			this.Label58.Left = 0F;
			this.Label58.Name = "Label58";
			this.Label58.Style = "";
			this.Label58.Text = "15. Other";
			this.Label58.Top = 8.229167F;
			this.Label58.Width = 1.8125F;
			// 
			// Label59
			// 
			this.Label59.Height = 0.1875F;
			this.Label59.HyperLink = null;
			this.Label59.Left = 0F;
			this.Label59.Name = "Label59";
			this.Label59.Style = "";
			this.Label59.Text = "16. TOTALS (for columns E & F, add lines 13, 14 and 15)";
			this.Label59.Top = 8.479167F;
			this.Label59.Width = 3.8125F;
			// 
			// Label60
			// 
			this.Label60.Height = 0.1875F;
			this.Label60.HyperLink = null;
			this.Label60.Left = 0F;
			this.Label60.Name = "Label60";
			this.Label60.Style = "";
			this.Label60.Text = "17. Value Limitation";
			this.Label60.Top = 8.729167F;
			this.Label60.Width = 1.8125F;
			// 
			// Label61
			// 
			this.Label61.Height = 0.1875F;
			this.Label61.HyperLink = null;
			this.Label61.Left = 0F;
			this.Label61.Name = "Label61";
			this.Label61.Style = "";
			this.Label61.Text = "18. Allowable Assessed Value (in column F, multiply line 10 by line 11)";
			this.Label61.Top = 8.979167F;
			this.Label61.Width = 4.645833F;
			// 
			// Label62
			// 
			this.Label62.Height = 0.1875F;
			this.Label62.HyperLink = null;
			this.Label62.Left = 3.0625F;
			this.Label62.Name = "Label62";
			this.Label62.Style = "";
			this.Label62.Text = "15";
			this.Label62.Top = 7.729167F;
			this.Label62.Width = 0.28125F;
			// 
			// Label63
			// 
			this.Label63.Height = 0.1875F;
			this.Label63.HyperLink = null;
			this.Label63.Left = 3.0625F;
			this.Label63.Name = "Label63";
			this.Label63.Style = "";
			this.Label63.Text = "15";
			this.Label63.Top = 7.979167F;
			this.Label63.Width = 0.28125F;
			// 
			// Label64
			// 
			this.Label64.Height = 0.1875F;
			this.Label64.HyperLink = null;
			this.Label64.Left = 3.0625F;
			this.Label64.Name = "Label64";
			this.Label64.Style = "";
			this.Label64.Text = "15";
			this.Label64.Top = 8.229167F;
			this.Label64.Width = 0.28125F;
			// 
			// Line13
			// 
			this.Line13.Height = 0F;
			this.Line13.Left = 1.944444F;
			this.Line13.LineWeight = 1F;
			this.Line13.Name = "Line13";
			this.Line13.Top = 8.131945F;
			this.Line13.Width = 0.7326388F;
			this.Line13.X1 = 1.944444F;
			this.Line13.X2 = 2.677083F;
			this.Line13.Y1 = 8.131945F;
			this.Line13.Y2 = 8.131945F;
			// 
			// Line14
			// 
			this.Line14.Height = 0F;
			this.Line14.Left = 1.944444F;
			this.Line14.LineWeight = 1F;
			this.Line14.Name = "Line14";
			this.Line14.Top = 8.368055F;
			this.Line14.Width = 0.7326388F;
			this.Line14.X1 = 1.944444F;
			this.Line14.X2 = 2.677083F;
			this.Line14.Y1 = 8.368055F;
			this.Line14.Y2 = 8.368055F;
			// 
			// Line15
			// 
			this.Line15.Height = 0F;
			this.Line15.Left = 1.944444F;
			this.Line15.LineWeight = 1F;
			this.Line15.Name = "Line15";
			this.Line15.Top = 7.902778F;
			this.Line15.Width = 0.7326388F;
			this.Line15.X1 = 1.944444F;
			this.Line15.X2 = 2.677083F;
			this.Line15.Y1 = 7.902778F;
			this.Line15.Y2 = 7.902778F;
			// 
			// lblAssessmentYearME3
			// 
			this.lblAssessmentYearME3.Height = 0.1875F;
			this.lblAssessmentYearME3.HyperLink = null;
			this.lblAssessmentYearME3.Left = 4.020833F;
			this.lblAssessmentYearME3.Name = "lblAssessmentYearME3";
			this.lblAssessmentYearME3.Style = "";
			this.lblAssessmentYearME3.Text = "2006";
			this.lblAssessmentYearME3.Top = 7.729167F;
			this.lblAssessmentYearME3.Width = 0.3958333F;
			// 
			// lblAssessmentYearFurniture3
			// 
			this.lblAssessmentYearFurniture3.Height = 0.1875F;
			this.lblAssessmentYearFurniture3.HyperLink = null;
			this.lblAssessmentYearFurniture3.Left = 4.020833F;
			this.lblAssessmentYearFurniture3.Name = "lblAssessmentYearFurniture3";
			this.lblAssessmentYearFurniture3.Style = "";
			this.lblAssessmentYearFurniture3.Text = "2006";
			this.lblAssessmentYearFurniture3.Top = 7.979167F;
			this.lblAssessmentYearFurniture3.Width = 0.3958333F;
			// 
			// lblAssessmentYearOther3
			// 
			this.lblAssessmentYearOther3.Height = 0.1875F;
			this.lblAssessmentYearOther3.HyperLink = null;
			this.lblAssessmentYearOther3.Left = 4.020833F;
			this.lblAssessmentYearOther3.Name = "lblAssessmentYearOther3";
			this.lblAssessmentYearOther3.Style = "";
			this.lblAssessmentYearOther3.Text = "2006";
			this.lblAssessmentYearOther3.Top = 8.229167F;
			this.lblAssessmentYearOther3.Width = 0.3958333F;
			// 
			// txtOriginalME3
			// 
			this.txtOriginalME3.Height = 0.1875F;
			this.txtOriginalME3.Left = 4.625F;
			this.txtOriginalME3.Name = "txtOriginalME3";
			this.txtOriginalME3.Style = "text-align: right";
			this.txtOriginalME3.Text = null;
			this.txtOriginalME3.Top = 7.729167F;
			this.txtOriginalME3.Width = 0.7916667F;
			// 
			// txtOriginalFurniture3
			// 
			this.txtOriginalFurniture3.Height = 0.1875F;
			this.txtOriginalFurniture3.Left = 4.625F;
			this.txtOriginalFurniture3.Name = "txtOriginalFurniture3";
			this.txtOriginalFurniture3.Style = "text-align: right";
			this.txtOriginalFurniture3.Text = null;
			this.txtOriginalFurniture3.Top = 7.979167F;
			this.txtOriginalFurniture3.Width = 0.7916667F;
			// 
			// txtOriginalOther3
			// 
			this.txtOriginalOther3.Height = 0.1875F;
			this.txtOriginalOther3.Left = 4.625F;
			this.txtOriginalOther3.Name = "txtOriginalOther3";
			this.txtOriginalOther3.Style = "text-align: right";
			this.txtOriginalOther3.Text = null;
			this.txtOriginalOther3.Top = 8.229167F;
			this.txtOriginalOther3.Width = 0.7916667F;
			// 
			// txtTotalOriginalCost3
			// 
			this.txtTotalOriginalCost3.Height = 0.1875F;
			this.txtTotalOriginalCost3.Left = 4.625F;
			this.txtTotalOriginalCost3.Name = "txtTotalOriginalCost3";
			this.txtTotalOriginalCost3.Style = "text-align: right";
			this.txtTotalOriginalCost3.Text = null;
			this.txtTotalOriginalCost3.Top = 8.479167F;
			this.txtTotalOriginalCost3.Width = 0.7916667F;
			// 
			// txtAssessedME3
			// 
			this.txtAssessedME3.Height = 0.1875F;
			this.txtAssessedME3.Left = 5.760417F;
			this.txtAssessedME3.Name = "txtAssessedME3";
			this.txtAssessedME3.Style = "text-align: right";
			this.txtAssessedME3.Text = null;
			this.txtAssessedME3.Top = 7.729167F;
			this.txtAssessedME3.Width = 0.90625F;
			// 
			// txtAssessedFurniture3
			// 
			this.txtAssessedFurniture3.Height = 0.1875F;
			this.txtAssessedFurniture3.Left = 5.760417F;
			this.txtAssessedFurniture3.Name = "txtAssessedFurniture3";
			this.txtAssessedFurniture3.Style = "text-align: right";
			this.txtAssessedFurniture3.Text = null;
			this.txtAssessedFurniture3.Top = 7.979167F;
			this.txtAssessedFurniture3.Width = 0.90625F;
			// 
			// txtAssessedOther3
			// 
			this.txtAssessedOther3.Height = 0.1875F;
			this.txtAssessedOther3.Left = 5.75F;
			this.txtAssessedOther3.Name = "txtAssessedOther3";
			this.txtAssessedOther3.Style = "text-align: right";
			this.txtAssessedOther3.Text = null;
			this.txtAssessedOther3.Top = 8.229167F;
			this.txtAssessedOther3.Width = 0.9166667F;
			// 
			// txtAssessedTotal3
			// 
			this.txtAssessedTotal3.Height = 0.1875F;
			this.txtAssessedTotal3.Left = 5.75F;
			this.txtAssessedTotal3.Name = "txtAssessedTotal3";
			this.txtAssessedTotal3.Style = "text-align: right";
			this.txtAssessedTotal3.Text = null;
			this.txtAssessedTotal3.Top = 8.479167F;
			this.txtAssessedTotal3.Width = 0.9166667F;
			// 
			// Label68
			// 
			this.Label68.Height = 0.1875F;
			this.Label68.HyperLink = null;
			this.Label68.Left = 6.0625F;
			this.Label68.Name = "Label68";
			this.Label68.Style = "";
			this.Label68.Text = ".65";
			this.Label68.Top = 8.729167F;
			this.Label68.Width = 0.28125F;
			// 
			// txtAllowedValue3
			// 
			this.txtAllowedValue3.Height = 0.1875F;
			this.txtAllowedValue3.Left = 5.75F;
			this.txtAllowedValue3.Name = "txtAllowedValue3";
			this.txtAllowedValue3.Style = "text-align: right";
			this.txtAllowedValue3.Text = null;
			this.txtAllowedValue3.Top = 8.979167F;
			this.txtAllowedValue3.Width = 0.9166667F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 2.0625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.Label1.Text = "ASSESSOR NOTIFICATION";
			this.Label1.Top = 0.03125F;
			this.Label1.Width = 3.125F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 1.5625F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.Label2.Text = "PROPERTY CLAIMED FOR MORE THAN 12 YEARS";
			this.Label2.Top = 0.25F;
			this.Label2.Width = 4.427083F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 0.01041667F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: left";
			this.Label4.Text = "Form";
			this.Label4.Top = 0.4583333F;
			this.Label4.Width = 0.5625F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 0.6354167F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: left";
			this.Label5.Text = "801B";
			this.Label5.Top = 0.4583333F;
			this.Label5.Width = 0.4895833F;
			// 
			// lblMunicipalCode
			// 
			this.lblMunicipalCode.Height = 0.1666667F;
			this.lblMunicipalCode.HyperLink = null;
			this.lblMunicipalCode.Left = 5.6875F;
			this.lblMunicipalCode.Name = "lblMunicipalCode";
			this.lblMunicipalCode.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblMunicipalCode.Text = "Municipal code:";
			this.lblMunicipalCode.Top = 0.4895833F;
			this.lblMunicipalCode.Width = 1.135417F;
			// 
			// lblTaxYear
			// 
			this.lblTaxYear.Height = 0.3020833F;
			this.lblTaxYear.HyperLink = null;
			this.lblTaxYear.Left = 0.625F;
			this.lblTaxYear.Name = "lblTaxYear";
			this.lblTaxYear.Style = "font-family: \'Tahoma\'; font-size: 18pt; text-align: left";
			this.lblTaxYear.Text = "2015";
			this.lblTaxYear.Top = 0.05208333F;
			this.lblTaxYear.Width = 0.75F;
			// 
			// txtMuniCode
			// 
			this.txtMuniCode.Height = 0.1666667F;
			this.txtMuniCode.Left = 6.916667F;
			this.txtMuniCode.Name = "txtMuniCode";
			this.txtMuniCode.Text = null;
			this.txtMuniCode.Top = 0.4895833F;
			this.txtMuniCode.Width = 0.4895833F;
			// 
			// PageBreak1
			// 
			this.PageBreak1.Height = 0.05555556F;
			this.PageBreak1.Left = 0F;
			this.PageBreak1.Name = "PageBreak1";
			this.PageBreak1.Size = new System.Drawing.SizeF(7.479167F, 0.05555556F);
			this.PageBreak1.Top = 9.72F;
			this.PageBreak1.Width = 7.479167F;
			// 
			// Label70
			// 
			this.Label70.Height = 0.21875F;
			this.Label70.HyperLink = null;
			this.Label70.Left = 2.0625F;
			this.Label70.Name = "Label70";
			this.Label70.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.Label70.Text = "ASSESSOR NOTIFICATION";
			this.Label70.Top = 9.864583F;
			this.Label70.Width = 3.125F;
			// 
			// Label71
			// 
			this.Label71.Height = 0.1875F;
			this.Label71.HyperLink = null;
			this.Label71.Left = 1.5625F;
			this.Label71.Name = "Label71";
			this.Label71.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.Label71.Text = "PROPERTY CLAIMED FOR MORE THAN 12 YEARS";
			this.Label71.Top = 10.08333F;
			this.Label71.Width = 4.427083F;
			// 
			// Label72
			// 
			this.Label72.Height = 0.1875F;
			this.Label72.HyperLink = null;
			this.Label72.Left = 0.01041667F;
			this.Label72.Name = "Label72";
			this.Label72.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: left";
			this.Label72.Text = "Form";
			this.Label72.Top = 10.29167F;
			this.Label72.Width = 0.5625F;
			// 
			// Label73
			// 
			this.Label73.Height = 0.1875F;
			this.Label73.HyperLink = null;
			this.Label73.Left = 0.6354167F;
			this.Label73.Name = "Label73";
			this.Label73.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: left";
			this.Label73.Text = "801B";
			this.Label73.Top = 10.29167F;
			this.Label73.Width = 0.4895833F;
			// 
			// Label74
			// 
			this.Label74.Height = 0.3020833F;
			this.Label74.HyperLink = null;
			this.Label74.Left = 0.625F;
			this.Label74.Name = "Label74";
			this.Label74.Style = "font-family: \'Tahoma\'; font-size: 18pt; text-align: left";
			this.Label74.Text = "2015";
			this.Label74.Top = 9.885417F;
			this.Label74.Width = 0.75F;
			// 
			// Label75
			// 
			this.Label75.Height = 0.1875F;
			this.Label75.HyperLink = null;
			this.Label75.Left = 1.125F;
			this.Label75.Name = "Label75";
			this.Label75.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left";
			this.Label75.Text = "(page 2)";
			this.Label75.Top = 10.29167F;
			this.Label75.Width = 1.177083F;
			// 
			// Line17
			// 
			this.Line17.Height = 0F;
			this.Line17.Left = 0F;
			this.Line17.LineWeight = 1F;
			this.Line17.Name = "Line17";
			this.Line17.Top = 10.58333F;
			this.Line17.Width = 7.375F;
			this.Line17.X1 = 0F;
			this.Line17.X2 = 7.375F;
			this.Line17.Y1 = 10.58333F;
			this.Line17.Y2 = 10.58333F;
			// 
			// Label76
			// 
			this.Label76.Height = 0.21875F;
			this.Label76.HyperLink = null;
			this.Label76.Left = 0.07291666F;
			this.Label76.Name = "Label76";
			this.Label76.Style = "font-weight: bold";
			this.Label76.Text = "* Form 801B is for property claimed more than 12 years.  See instructions for thi" + "s form.";
			this.Label76.Top = 9.364583F;
			this.Label76.Width = 6.21875F;
			// 
			// Label77
			// 
			this.Label77.Height = 0.21875F;
			this.Label77.HyperLink = null;
			this.Label77.Left = 0.07291666F;
			this.Label77.Name = "Label77";
			this.Label77.Style = "font-weight: bold; text-align: center";
			this.Label77.Text = "Continued on next page";
			//FC:FINAL:CHN - issue #1357: Fix label location.
			// this.Label77.Top = 9.583333F;
			this.Label77.Top = 9.5F;
			this.Label77.Width = 7.291667F;
			// 
			// Label78
			// 
			this.Label78.Height = 0.1666667F;
			this.Label78.HyperLink = null;
			this.Label78.Left = 0.5625F;
			this.Label78.Name = "Label78";
			this.Label78.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.Label78.Text = "A.";
			this.Label78.Top = 10.64583F;
			this.Label78.Width = 0.2916667F;
			// 
			// Label79
			// 
			this.Label79.Height = 0.1666667F;
			this.Label79.HyperLink = null;
			this.Label79.Left = 2.125F;
			this.Label79.Name = "Label79";
			this.Label79.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.Label79.Text = "B.";
			this.Label79.Top = 10.64583F;
			this.Label79.Width = 0.2916667F;
			// 
			// Label80
			// 
			this.Label80.Height = 0.1666667F;
			this.Label80.HyperLink = null;
			this.Label80.Left = 3.125F;
			this.Label80.Name = "Label80";
			this.Label80.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.Label80.Text = "C.*";
			this.Label80.Top = 10.64583F;
			this.Label80.Width = 0.2916667F;
			// 
			// Label81
			// 
			this.Label81.Height = 0.1666667F;
			this.Label81.HyperLink = null;
			this.Label81.Left = 4.0625F;
			this.Label81.Name = "Label81";
			this.Label81.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.Label81.Text = "D.";
			this.Label81.Top = 10.64583F;
			this.Label81.Width = 0.2916667F;
			// 
			// Label82
			// 
			this.Label82.Height = 0.1666667F;
			this.Label82.HyperLink = null;
			this.Label82.Left = 4.9375F;
			this.Label82.Name = "Label82";
			this.Label82.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.Label82.Text = "E.";
			this.Label82.Top = 10.64583F;
			this.Label82.Width = 0.2916667F;
			// 
			// Label83
			// 
			this.Label83.Height = 0.1666667F;
			this.Label83.HyperLink = null;
			this.Label83.Left = 6.0625F;
			this.Label83.Name = "Label83";
			this.Label83.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.Label83.Text = "F.";
			this.Label83.Top = 10.64583F;
			this.Label83.Width = 0.2916667F;
			// 
			// Label84
			// 
			this.Label84.Height = 0.1875F;
			this.Label84.HyperLink = null;
			this.Label84.Left = 0F;
			this.Label84.Name = "Label84";
			this.Label84.Style = "";
			this.Label84.Text = "19. Machinery & Equipment";
			this.Label84.Top = 11.375F;
			this.Label84.Width = 1.8125F;
			// 
			// Label85
			// 
			this.Label85.Height = 0.1875F;
			this.Label85.HyperLink = null;
			this.Label85.Left = 0F;
			this.Label85.Name = "Label85";
			this.Label85.Style = "";
			this.Label85.Text = "20. Furniture";
			this.Label85.Top = 11.625F;
			this.Label85.Width = 1.8125F;
			// 
			// Label86
			// 
			this.Label86.Height = 0.1875F;
			this.Label86.HyperLink = null;
			this.Label86.Left = 0F;
			this.Label86.Name = "Label86";
			this.Label86.Style = "";
			this.Label86.Text = "21. Other";
			this.Label86.Top = 11.875F;
			this.Label86.Width = 1.8125F;
			// 
			// Label87
			// 
			this.Label87.Height = 0.1875F;
			this.Label87.HyperLink = null;
			this.Label87.Left = 0F;
			this.Label87.Name = "Label87";
			this.Label87.Style = "";
			this.Label87.Text = "22. TOTALS (for columns E & F, add lines 19, 20 and 21)";
			this.Label87.Top = 12.125F;
			this.Label87.Width = 3.875F;
			// 
			// Label88
			// 
			this.Label88.Height = 0.1875F;
			this.Label88.HyperLink = null;
			this.Label88.Left = 0F;
			this.Label88.Name = "Label88";
			this.Label88.Style = "";
			this.Label88.Text = "23. Value Limitation";
			this.Label88.Top = 12.375F;
			this.Label88.Width = 1.8125F;
			// 
			// Label89
			// 
			this.Label89.Height = 0.1875F;
			this.Label89.HyperLink = null;
			this.Label89.Left = 0F;
			this.Label89.Name = "Label89";
			this.Label89.Style = "";
			this.Label89.Text = "24. Allowable Assessed Value (in column F, multiply line 22 by line 23)";
			this.Label89.Top = 12.625F;
			this.Label89.Width = 4.645833F;
			// 
			// Label90
			// 
			this.Label90.Height = 0.1875F;
			this.Label90.HyperLink = null;
			this.Label90.Left = 3.0625F;
			this.Label90.Name = "Label90";
			this.Label90.Style = "";
			this.Label90.Text = "16";
			this.Label90.Top = 11.375F;
			this.Label90.Width = 0.28125F;
			// 
			// Label91
			// 
			this.Label91.Height = 0.1875F;
			this.Label91.HyperLink = null;
			this.Label91.Left = 3.0625F;
			this.Label91.Name = "Label91";
			this.Label91.Style = "";
			this.Label91.Text = "16";
			this.Label91.Top = 11.625F;
			this.Label91.Width = 0.28125F;
			// 
			// Label92
			// 
			this.Label92.Height = 0.1875F;
			this.Label92.HyperLink = null;
			this.Label92.Left = 3.0625F;
			this.Label92.Name = "Label92";
			this.Label92.Style = "";
			this.Label92.Text = "16";
			this.Label92.Top = 11.875F;
			this.Label92.Width = 0.28125F;
			// 
			// Label93
			// 
			this.Label93.Height = 0.4375F;
			this.Label93.HyperLink = null;
			this.Label93.Left = 1.916667F;
			this.Label93.Name = "Label93";
			this.Label93.Style = "font-size: 8.5pt; text-align: center";
			this.Label93.Text = "State of Origin (if acquired used)";
			this.Label93.Top = 10.875F;
			this.Label93.Width = 0.8125F;
			// 
			// Label94
			// 
			this.Label94.Height = 0.4375F;
			this.Label94.HyperLink = null;
			this.Label94.Left = 0F;
			this.Label94.Name = "Label94";
			this.Label94.Style = "font-size: 8.5pt; text-align: center";
			this.Label94.Text = "Property Description Category";
			this.Label94.Top = 10.875F;
			this.Label94.Width = 1.239583F;
			// 
			// Label95
			// 
			this.Label95.Height = 0.4375F;
			this.Label95.HyperLink = null;
			this.Label95.Left = 2.8125F;
			this.Label95.Name = "Label95";
			this.Label95.Style = "font-size: 8.5pt; text-align: center";
			this.Label95.Text = "Number of Years Claimed";
			this.Label95.Top = 10.875F;
			this.Label95.Width = 0.8125F;
			// 
			// Label96
			// 
			this.Label96.Height = 0.4375F;
			this.Label96.HyperLink = null;
			this.Label96.Left = 3.75F;
			this.Label96.Name = "Label96";
			this.Label96.Style = "font-size: 8.5pt; text-align: center";
			this.Label96.Text = "Original Assessment Year";
			this.Label96.Top = 10.875F;
			this.Label96.Width = 0.8125F;
			// 
			// Label97
			// 
			this.Label97.Height = 0.4375F;
			this.Label97.HyperLink = null;
			this.Label97.Left = 4.625F;
			this.Label97.Name = "Label97";
			this.Label97.Style = "font-size: 8.5pt; text-align: center";
			this.Label97.Text = "Original Cost";
			this.Label97.Top = 10.875F;
			this.Label97.Width = 0.8125F;
			// 
			// Label98
			// 
			this.Label98.Height = 0.5104167F;
			this.Label98.HyperLink = null;
			this.Label98.Left = 5.625F;
			this.Label98.Name = "Label98";
			this.Label98.Style = "font-size: 8.5pt; text-align: center";
			this.Label98.Text = "Assessed Value   (To be completed by local tax assessor)";
			this.Label98.Top = 10.875F;
			this.Label98.Width = 1.108f;
            // 
            // Line18
            // 
            this.Line18.Height = 0F;
			this.Line18.Left = 1.944444F;
			this.Line18.LineWeight = 1F;
			this.Line18.Name = "Line18";
			this.Line18.Top = 11.77778F;
			this.Line18.Width = 0.7326388F;
			this.Line18.X1 = 1.944444F;
			this.Line18.X2 = 2.677083F;
			this.Line18.Y1 = 11.77778F;
			this.Line18.Y2 = 11.77778F;
			// 
			// Line19
			// 
			this.Line19.Height = 0F;
			this.Line19.Left = 1.944444F;
			this.Line19.LineWeight = 1F;
			this.Line19.Name = "Line19";
			this.Line19.Top = 12.01389F;
			this.Line19.Width = 0.7326388F;
			this.Line19.X1 = 1.944444F;
			this.Line19.X2 = 2.677083F;
			this.Line19.Y1 = 12.01389F;
			this.Line19.Y2 = 12.01389F;
			// 
			// Line20
			// 
			this.Line20.Height = 0F;
			this.Line20.Left = 1.944444F;
			this.Line20.LineWeight = 1F;
			this.Line20.Name = "Line20";
			this.Line20.Top = 11.54861F;
			this.Line20.Width = 0.7326388F;
			this.Line20.X1 = 1.944444F;
			this.Line20.X2 = 2.677083F;
			this.Line20.Y1 = 11.54861F;
			this.Line20.Y2 = 11.54861F;
			// 
			// lblAssessmentYearME4
			// 
			this.lblAssessmentYearME4.Height = 0.1875F;
			this.lblAssessmentYearME4.HyperLink = null;
			this.lblAssessmentYearME4.Left = 4.020833F;
			this.lblAssessmentYearME4.Name = "lblAssessmentYearME4";
			this.lblAssessmentYearME4.Style = "";
			this.lblAssessmentYearME4.Text = null;
			this.lblAssessmentYearME4.Top = 11.375F;
			this.lblAssessmentYearME4.Width = 0.3958333F;
			// 
			// lblAssessmentYearFurniture4
			// 
			this.lblAssessmentYearFurniture4.Height = 0.1875F;
			this.lblAssessmentYearFurniture4.HyperLink = null;
			this.lblAssessmentYearFurniture4.Left = 4.020833F;
			this.lblAssessmentYearFurniture4.Name = "lblAssessmentYearFurniture4";
			this.lblAssessmentYearFurniture4.Style = "";
			this.lblAssessmentYearFurniture4.Text = null;
			this.lblAssessmentYearFurniture4.Top = 11.625F;
			this.lblAssessmentYearFurniture4.Width = 0.3958333F;
			// 
			// lblAssessmentYearOther4
			// 
			this.lblAssessmentYearOther4.Height = 0.1875F;
			this.lblAssessmentYearOther4.HyperLink = null;
			this.lblAssessmentYearOther4.Left = 4.020833F;
			this.lblAssessmentYearOther4.Name = "lblAssessmentYearOther4";
			this.lblAssessmentYearOther4.Style = "";
			this.lblAssessmentYearOther4.Text = null;
			this.lblAssessmentYearOther4.Top = 11.875F;
			this.lblAssessmentYearOther4.Width = 0.3958333F;
			// 
			// txtOriginalME4
			// 
			this.txtOriginalME4.Height = 0.1875F;
			this.txtOriginalME4.Left = 4.625F;
			this.txtOriginalME4.Name = "txtOriginalME4";
			this.txtOriginalME4.Style = "text-align: right";
			this.txtOriginalME4.Text = null;
			this.txtOriginalME4.Top = 11.375F;
			this.txtOriginalME4.Width = 0.7916667F;
			// 
			// txtOriginalFurniture4
			// 
			this.txtOriginalFurniture4.Height = 0.1875F;
			this.txtOriginalFurniture4.Left = 4.625F;
			this.txtOriginalFurniture4.Name = "txtOriginalFurniture4";
			this.txtOriginalFurniture4.Style = "text-align: right";
			this.txtOriginalFurniture4.Text = null;
			this.txtOriginalFurniture4.Top = 11.625F;
			this.txtOriginalFurniture4.Width = 0.7916667F;
			// 
			// txtOriginalOther4
			// 
			this.txtOriginalOther4.Height = 0.1875F;
			this.txtOriginalOther4.Left = 4.625F;
			this.txtOriginalOther4.Name = "txtOriginalOther4";
			this.txtOriginalOther4.Style = "text-align: right";
			this.txtOriginalOther4.Text = null;
			this.txtOriginalOther4.Top = 11.875F;
			this.txtOriginalOther4.Width = 0.7916667F;
			// 
			// txtTotalOriginalCost4
			// 
			this.txtTotalOriginalCost4.Height = 0.1875F;
			this.txtTotalOriginalCost4.Left = 4.625F;
			this.txtTotalOriginalCost4.Name = "txtTotalOriginalCost4";
			this.txtTotalOriginalCost4.Style = "text-align: right";
			this.txtTotalOriginalCost4.Text = null;
			this.txtTotalOriginalCost4.Top = 12.125F;
			this.txtTotalOriginalCost4.Width = 0.7916667F;
			// 
			// txtAssessedME4
			// 
			this.txtAssessedME4.Height = 0.1875F;
			this.txtAssessedME4.Left = 5.760417F;
			this.txtAssessedME4.Name = "txtAssessedME4";
			this.txtAssessedME4.Style = "text-align: right";
			this.txtAssessedME4.Text = null;
			this.txtAssessedME4.Top = 11.375F;
			this.txtAssessedME4.Width = 0.90625F;
			// 
			// txtAssessedFurniture4
			// 
			this.txtAssessedFurniture4.Height = 0.1875F;
			this.txtAssessedFurniture4.Left = 5.760417F;
			this.txtAssessedFurniture4.Name = "txtAssessedFurniture4";
			this.txtAssessedFurniture4.Style = "text-align: right";
			this.txtAssessedFurniture4.Text = null;
			this.txtAssessedFurniture4.Top = 11.625F;
			this.txtAssessedFurniture4.Width = 0.90625F;
			// 
			// txtAssessedOther4
			// 
			this.txtAssessedOther4.Height = 0.1875F;
			this.txtAssessedOther4.Left = 5.75F;
			this.txtAssessedOther4.Name = "txtAssessedOther4";
			this.txtAssessedOther4.Style = "text-align: right";
			this.txtAssessedOther4.Text = null;
			this.txtAssessedOther4.Top = 11.875F;
			this.txtAssessedOther4.Width = 0.9166667F;
			// 
			// txtAssessedTotal4
			// 
			this.txtAssessedTotal4.Height = 0.1875F;
			this.txtAssessedTotal4.Left = 5.75F;
			this.txtAssessedTotal4.Name = "txtAssessedTotal4";
			this.txtAssessedTotal4.Style = "text-align: right";
			this.txtAssessedTotal4.Text = null;
			this.txtAssessedTotal4.Top = 12.125F;
			this.txtAssessedTotal4.Width = 0.9166667F;
			// 
			// Label102
			// 
			this.Label102.Height = 0.1875F;
			this.Label102.HyperLink = null;
			this.Label102.Left = 6.0625F;
			this.Label102.Name = "Label102";
			this.Label102.Style = "";
			this.Label102.Text = ".60";
			this.Label102.Top = 12.375F;
			this.Label102.Width = 0.28125F;
			// 
			// txtAllowedValue4
			// 
			this.txtAllowedValue4.Height = 0.1875F;
			this.txtAllowedValue4.Left = 5.75F;
			this.txtAllowedValue4.Name = "txtAllowedValue4";
			this.txtAllowedValue4.Style = "text-align: right";
			this.txtAllowedValue4.Text = null;
			this.txtAllowedValue4.Top = 12.625F;
			this.txtAllowedValue4.Width = 0.9166667F;
			// 
			// Line21
			// 
			this.Line21.Height = 0F;
			this.Line21.Left = 0.0625F;
			this.Line21.LineWeight = 1F;
			this.Line21.Name = "Line21";
			this.Line21.Top = 12.84375F;
			this.Line21.Width = 7.375F;
			this.Line21.X1 = 0.0625F;
			this.Line21.X2 = 7.4375F;
			this.Line21.Y1 = 12.84375F;
			this.Line21.Y2 = 12.84375F;
			// 
			// Label103
			// 
			this.Label103.Height = 0.1875F;
			this.Label103.HyperLink = null;
			this.Label103.Left = 0F;
			this.Label103.Name = "Label103";
			this.Label103.Style = "";
			this.Label103.Text = "25. Machinery & Equipment";
			this.Label103.Top = 12.9375F;
			this.Label103.Width = 1.8125F;
			// 
			// Label104
			// 
			this.Label104.Height = 0.1875F;
			this.Label104.HyperLink = null;
			this.Label104.Left = 0F;
			this.Label104.Name = "Label104";
			this.Label104.Style = "";
			this.Label104.Text = "26. Furniture";
			this.Label104.Top = 13.1875F;
			this.Label104.Width = 1.8125F;
			// 
			// Label105
			// 
			this.Label105.Height = 0.1875F;
			this.Label105.HyperLink = null;
			this.Label105.Left = 0F;
			this.Label105.Name = "Label105";
			this.Label105.Style = "";
			this.Label105.Text = "27. Other";
			this.Label105.Top = 13.4375F;
			this.Label105.Width = 1.8125F;
			// 
			// Label106
			// 
			this.Label106.Height = 0.1875F;
			this.Label106.HyperLink = null;
			this.Label106.Left = 0F;
			this.Label106.Name = "Label106";
			this.Label106.Style = "";
			this.Label106.Text = "28. TOTALS (for columns E & F, add lines 25, 26 and 27)";
			this.Label106.Top = 13.6875F;
			this.Label106.Width = 3.8125F;
			// 
			// Label107
			// 
			this.Label107.Height = 0.1875F;
			this.Label107.HyperLink = null;
			this.Label107.Left = 0F;
			this.Label107.Name = "Label107";
			this.Label107.Style = "";
			this.Label107.Text = "29. Value Limitation";
			this.Label107.Top = 13.9375F;
			this.Label107.Width = 1.8125F;
			// 
			// Label108
			// 
			this.Label108.Height = 0.1875F;
			this.Label108.HyperLink = null;
			this.Label108.Left = 0F;
			this.Label108.Name = "Label108";
			this.Label108.Style = "";
			this.Label108.Text = "30. Allowable Assessed Value (in column F, multiply line 28 by line 29)";
			this.Label108.Top = 14.1875F;
			this.Label108.Width = 4.708333F;
			// 
			// Label109
			// 
			this.Label109.Height = 0.1875F;
			this.Label109.HyperLink = null;
			this.Label109.Left = 3.0625F;
			this.Label109.Name = "Label109";
			this.Label109.Style = "";
			this.Label109.Text = "17";
			this.Label109.Top = 12.9375F;
			this.Label109.Width = 0.28125F;
			// 
			// Label110
			// 
			this.Label110.Height = 0.1875F;
			this.Label110.HyperLink = null;
			this.Label110.Left = 3.0625F;
			this.Label110.Name = "Label110";
			this.Label110.Style = "";
			this.Label110.Text = "17";
			this.Label110.Top = 13.1875F;
			this.Label110.Width = 0.28125F;
			// 
			// Label111
			// 
			this.Label111.Height = 0.1875F;
			this.Label111.HyperLink = null;
			this.Label111.Left = 3.0625F;
			this.Label111.Name = "Label111";
			this.Label111.Style = "";
			this.Label111.Text = "17";
			this.Label111.Top = 13.4375F;
			this.Label111.Width = 0.28125F;
			// 
			// Line22
			// 
			this.Line22.Height = 0F;
			this.Line22.Left = 1.944444F;
			this.Line22.LineWeight = 1F;
			this.Line22.Name = "Line22";
			this.Line22.Top = 13.34028F;
			this.Line22.Width = 0.7326388F;
			this.Line22.X1 = 1.944444F;
			this.Line22.X2 = 2.677083F;
			this.Line22.Y1 = 13.34028F;
			this.Line22.Y2 = 13.34028F;
			// 
			// Line23
			// 
			this.Line23.Height = 0F;
			this.Line23.Left = 1.944444F;
			this.Line23.LineWeight = 1F;
			this.Line23.Name = "Line23";
			this.Line23.Top = 13.57639F;
			this.Line23.Width = 0.7326388F;
			this.Line23.X1 = 1.944444F;
			this.Line23.X2 = 2.677083F;
			this.Line23.Y1 = 13.57639F;
			this.Line23.Y2 = 13.57639F;
			// 
			// Line24
			// 
			this.Line24.Height = 0F;
			this.Line24.Left = 1.944444F;
			this.Line24.LineWeight = 1F;
			this.Line24.Name = "Line24";
			this.Line24.Top = 13.11111F;
			this.Line24.Width = 0.7326388F;
			this.Line24.X1 = 1.944444F;
			this.Line24.X2 = 2.677083F;
			this.Line24.Y1 = 13.11111F;
			this.Line24.Y2 = 13.11111F;
			// 
			// lblAssessmentYearME5
			// 
			this.lblAssessmentYearME5.Height = 0.1875F;
			this.lblAssessmentYearME5.HyperLink = null;
			this.lblAssessmentYearME5.Left = 4.020833F;
			this.lblAssessmentYearME5.Name = "lblAssessmentYearME5";
			this.lblAssessmentYearME5.Style = "";
			this.lblAssessmentYearME5.Text = "2006";
			this.lblAssessmentYearME5.Top = 12.9375F;
			this.lblAssessmentYearME5.Width = 0.3958333F;
			// 
			// lblAssessmentYearFurniture5
			// 
			this.lblAssessmentYearFurniture5.Height = 0.1875F;
			this.lblAssessmentYearFurniture5.HyperLink = null;
			this.lblAssessmentYearFurniture5.Left = 4.020833F;
			this.lblAssessmentYearFurniture5.Name = "lblAssessmentYearFurniture5";
			this.lblAssessmentYearFurniture5.Style = "";
			this.lblAssessmentYearFurniture5.Text = "2006";
			this.lblAssessmentYearFurniture5.Top = 13.1875F;
			this.lblAssessmentYearFurniture5.Width = 0.3958333F;
			// 
			// lblAssessmentYearOther5
			// 
			this.lblAssessmentYearOther5.Height = 0.1875F;
			this.lblAssessmentYearOther5.HyperLink = null;
			this.lblAssessmentYearOther5.Left = 4.020833F;
			this.lblAssessmentYearOther5.Name = "lblAssessmentYearOther5";
			this.lblAssessmentYearOther5.Style = "";
			this.lblAssessmentYearOther5.Text = "2006";
			this.lblAssessmentYearOther5.Top = 13.4375F;
			this.lblAssessmentYearOther5.Width = 0.3958333F;
			// 
			// txtOriginalME5
			// 
			this.txtOriginalME5.Height = 0.1875F;
			this.txtOriginalME5.Left = 4.625F;
			this.txtOriginalME5.Name = "txtOriginalME5";
			this.txtOriginalME5.Style = "text-align: right";
			this.txtOriginalME5.Text = null;
			this.txtOriginalME5.Top = 12.9375F;
			this.txtOriginalME5.Width = 0.7916667F;
			// 
			// txtOriginalFurniture5
			// 
			this.txtOriginalFurniture5.Height = 0.1875F;
			this.txtOriginalFurniture5.Left = 4.625F;
			this.txtOriginalFurniture5.Name = "txtOriginalFurniture5";
			this.txtOriginalFurniture5.Style = "text-align: right";
			this.txtOriginalFurniture5.Text = null;
			this.txtOriginalFurniture5.Top = 13.1875F;
			this.txtOriginalFurniture5.Width = 0.7916667F;
			// 
			// txtOriginalOther5
			// 
			this.txtOriginalOther5.Height = 0.1875F;
			this.txtOriginalOther5.Left = 4.625F;
			this.txtOriginalOther5.Name = "txtOriginalOther5";
			this.txtOriginalOther5.Style = "text-align: right";
			this.txtOriginalOther5.Text = null;
			this.txtOriginalOther5.Top = 13.4375F;
			this.txtOriginalOther5.Width = 0.7916667F;
			// 
			// txtTotalOriginalCost5
			// 
			this.txtTotalOriginalCost5.Height = 0.1875F;
			this.txtTotalOriginalCost5.Left = 4.625F;
			this.txtTotalOriginalCost5.Name = "txtTotalOriginalCost5";
			this.txtTotalOriginalCost5.Style = "text-align: right";
			this.txtTotalOriginalCost5.Text = null;
			this.txtTotalOriginalCost5.Top = 13.6875F;
			this.txtTotalOriginalCost5.Width = 0.7916667F;
			// 
			// txtAssessedME5
			// 
			this.txtAssessedME5.Height = 0.1875F;
			this.txtAssessedME5.Left = 5.760417F;
			this.txtAssessedME5.Name = "txtAssessedME5";
			this.txtAssessedME5.Style = "text-align: right";
			this.txtAssessedME5.Text = null;
			this.txtAssessedME5.Top = 12.9375F;
			this.txtAssessedME5.Width = 0.90625F;
			// 
			// txtAssessedFurniture5
			// 
			this.txtAssessedFurniture5.Height = 0.1875F;
			this.txtAssessedFurniture5.Left = 5.760417F;
			this.txtAssessedFurniture5.Name = "txtAssessedFurniture5";
			this.txtAssessedFurniture5.Style = "text-align: right";
			this.txtAssessedFurniture5.Text = null;
			this.txtAssessedFurniture5.Top = 13.1875F;
			this.txtAssessedFurniture5.Width = 0.90625F;
			// 
			// txtAssessedOther5
			// 
			this.txtAssessedOther5.Height = 0.1875F;
			this.txtAssessedOther5.Left = 5.75F;
			this.txtAssessedOther5.Name = "txtAssessedOther5";
			this.txtAssessedOther5.Style = "text-align: right";
			this.txtAssessedOther5.Text = null;
			this.txtAssessedOther5.Top = 13.4375F;
			this.txtAssessedOther5.Width = 0.9166667F;
			// 
			// txtAssessedTotal5
			// 
			this.txtAssessedTotal5.Height = 0.1875F;
			this.txtAssessedTotal5.Left = 5.75F;
			this.txtAssessedTotal5.Name = "txtAssessedTotal5";
			this.txtAssessedTotal5.Style = "text-align: right";
			this.txtAssessedTotal5.Text = null;
			this.txtAssessedTotal5.Top = 13.6875F;
			this.txtAssessedTotal5.Width = 0.9166667F;
			// 
			// Label115
			// 
			this.Label115.Height = 0.1875F;
			this.Label115.HyperLink = null;
			this.Label115.Left = 6.0625F;
			this.Label115.Name = "Label115";
			this.Label115.Style = "";
			this.Label115.Text = ".55";
			this.Label115.Top = 13.9375F;
			this.Label115.Width = 0.28125F;
			// 
			// txtAllowedValue5
			// 
			this.txtAllowedValue5.Height = 0.1875F;
			this.txtAllowedValue5.Left = 5.75F;
			this.txtAllowedValue5.Name = "txtAllowedValue5";
			this.txtAllowedValue5.Style = "text-align: right";
			this.txtAllowedValue5.Text = null;
			this.txtAllowedValue5.Top = 14.1875F;
			this.txtAllowedValue5.Width = 0.9166667F;
			// 
			// Line25
			// 
			this.Line25.Height = 0F;
			this.Line25.Left = 0.0625F;
			this.Line25.LineWeight = 1F;
			this.Line25.Name = "Line25";
			this.Line25.Top = 14.40625F;
			this.Line25.Width = 7.375F;
			this.Line25.X1 = 0.0625F;
			this.Line25.X2 = 7.4375F;
			this.Line25.Y1 = 14.40625F;
			this.Line25.Y2 = 14.40625F;
			// 
			// Label116
			// 
			this.Label116.Height = 0.1875F;
			this.Label116.HyperLink = null;
			this.Label116.Left = 0F;
			this.Label116.Name = "Label116";
			this.Label116.Style = "";
			this.Label116.Text = "31. Machinery & Equipment";
			this.Label116.Top = 14.5F;
			this.Label116.Width = 1.8125F;
			// 
			// Label117
			// 
			this.Label117.Height = 0.1875F;
			this.Label117.HyperLink = null;
			this.Label117.Left = 0F;
			this.Label117.Name = "Label117";
			this.Label117.Style = "";
			this.Label117.Text = "32. Furniture";
			this.Label117.Top = 14.75F;
			this.Label117.Width = 1.8125F;
			// 
			// Label118
			// 
			this.Label118.Height = 0.1875F;
			this.Label118.HyperLink = null;
			this.Label118.Left = 0F;
			this.Label118.Name = "Label118";
			this.Label118.Style = "";
			this.Label118.Text = "33. Other";
			this.Label118.Top = 15F;
			this.Label118.Width = 1.8125F;
			// 
			// Label119
			// 
			this.Label119.Height = 0.1875F;
			this.Label119.HyperLink = null;
			this.Label119.Left = 0F;
			this.Label119.Name = "Label119";
			this.Label119.Style = "";
			this.Label119.Text = "34. TOTALS (for columns E & F, add lines 31, 32 and 33)";
			this.Label119.Top = 15.25F;
			this.Label119.Width = 3.6875F;
			// 
			// Label120
			// 
			this.Label120.Height = 0.1875F;
			this.Label120.HyperLink = null;
			this.Label120.Left = 0F;
			this.Label120.Name = "Label120";
			this.Label120.Style = "";
			this.Label120.Text = "35. Value Limitation";
			this.Label120.Top = 15.5F;
			this.Label120.Width = 1.8125F;
			// 
			// Label121
			// 
			this.Label121.Height = 0.1875F;
			this.Label121.HyperLink = null;
			this.Label121.Left = 0F;
			this.Label121.Name = "Label121";
			this.Label121.Style = "";
			this.Label121.Text = "36. Allowable Assessed Value (in column F, multiply line 34 by line 35)";
			this.Label121.Top = 15.75F;
			this.Label121.Width = 4.895833F;
			// 
			// Label122
			// 
			this.Label122.Height = 0.1875F;
			this.Label122.HyperLink = null;
			this.Label122.Left = 3.0625F;
			this.Label122.Name = "Label122";
			this.Label122.Style = "";
			this.Label122.Text = "18+";
			this.Label122.Top = 14.5F;
			this.Label122.Width = 0.28125F;
			// 
			// Label123
			// 
			this.Label123.Height = 0.1875F;
			this.Label123.HyperLink = null;
			this.Label123.Left = 3.0625F;
			this.Label123.Name = "Label123";
			this.Label123.Style = "";
			this.Label123.Text = "18+";
			this.Label123.Top = 14.75F;
			this.Label123.Width = 0.28125F;
			// 
			// Label124
			// 
			this.Label124.Height = 0.1875F;
			this.Label124.HyperLink = null;
			this.Label124.Left = 3.0625F;
			this.Label124.Name = "Label124";
			this.Label124.Style = "";
			this.Label124.Text = "18+";
			this.Label124.Top = 15F;
			this.Label124.Width = 0.28125F;
			// 
			// Line26
			// 
			this.Line26.Height = 0F;
			this.Line26.Left = 1.944444F;
			this.Line26.LineWeight = 1F;
			this.Line26.Name = "Line26";
			this.Line26.Top = 14.90278F;
			this.Line26.Width = 0.7326388F;
			this.Line26.X1 = 1.944444F;
			this.Line26.X2 = 2.677083F;
			this.Line26.Y1 = 14.90278F;
			this.Line26.Y2 = 14.90278F;
			// 
			// Line27
			// 
			this.Line27.Height = 0F;
			this.Line27.Left = 1.944444F;
			this.Line27.LineWeight = 1F;
			this.Line27.Name = "Line27";
			this.Line27.Top = 15.13889F;
			this.Line27.Width = 0.7326388F;
			this.Line27.X1 = 1.944444F;
			this.Line27.X2 = 2.677083F;
			this.Line27.Y1 = 15.13889F;
			this.Line27.Y2 = 15.13889F;
			// 
			// Line28
			// 
			this.Line28.Height = 0F;
			this.Line28.Left = 1.944444F;
			this.Line28.LineWeight = 1F;
			this.Line28.Name = "Line28";
			this.Line28.Top = 14.67361F;
			this.Line28.Width = 0.7326388F;
			this.Line28.X1 = 1.944444F;
			this.Line28.X2 = 2.677083F;
			this.Line28.Y1 = 14.67361F;
			this.Line28.Y2 = 14.67361F;
			// 
			// lblAssessmentYearME6
			// 
			this.lblAssessmentYearME6.Height = 0.1875F;
			this.lblAssessmentYearME6.HyperLink = null;
			this.lblAssessmentYearME6.Left = 4.020833F;
			this.lblAssessmentYearME6.Name = "lblAssessmentYearME6";
			this.lblAssessmentYearME6.Style = "";
			this.lblAssessmentYearME6.Text = "2006";
			this.lblAssessmentYearME6.Top = 14.5F;
			this.lblAssessmentYearME6.Width = 0.3958333F;
			// 
			// lblAssessmentYearFurniture6
			// 
			this.lblAssessmentYearFurniture6.Height = 0.1875F;
			this.lblAssessmentYearFurniture6.HyperLink = null;
			this.lblAssessmentYearFurniture6.Left = 4.020833F;
			this.lblAssessmentYearFurniture6.Name = "lblAssessmentYearFurniture6";
			this.lblAssessmentYearFurniture6.Style = "";
			this.lblAssessmentYearFurniture6.Text = "2006";
			this.lblAssessmentYearFurniture6.Top = 14.75F;
			this.lblAssessmentYearFurniture6.Width = 0.3958333F;
			// 
			// lblAssessmentYearOther6
			// 
			this.lblAssessmentYearOther6.Height = 0.1875F;
			this.lblAssessmentYearOther6.HyperLink = null;
			this.lblAssessmentYearOther6.Left = 4.020833F;
			this.lblAssessmentYearOther6.Name = "lblAssessmentYearOther6";
			this.lblAssessmentYearOther6.Style = "";
			this.lblAssessmentYearOther6.Text = "2006";
			this.lblAssessmentYearOther6.Top = 15F;
			this.lblAssessmentYearOther6.Width = 0.3958333F;
			// 
			// txtOriginalME6
			// 
			this.txtOriginalME6.Height = 0.1875F;
			this.txtOriginalME6.Left = 4.625F;
			this.txtOriginalME6.Name = "txtOriginalME6";
			this.txtOriginalME6.Style = "text-align: right";
			this.txtOriginalME6.Text = null;
			this.txtOriginalME6.Top = 14.5F;
			this.txtOriginalME6.Width = 0.7916667F;
			// 
			// txtOriginalFurniture6
			// 
			this.txtOriginalFurniture6.Height = 0.1875F;
			this.txtOriginalFurniture6.Left = 4.625F;
			this.txtOriginalFurniture6.Name = "txtOriginalFurniture6";
			this.txtOriginalFurniture6.Style = "text-align: right";
			this.txtOriginalFurniture6.Text = null;
			this.txtOriginalFurniture6.Top = 14.75F;
			this.txtOriginalFurniture6.Width = 0.7916667F;
			// 
			// txtOriginalOther6
			// 
			this.txtOriginalOther6.Height = 0.1875F;
			this.txtOriginalOther6.Left = 4.625F;
			this.txtOriginalOther6.Name = "txtOriginalOther6";
			this.txtOriginalOther6.Style = "text-align: right";
			this.txtOriginalOther6.Text = null;
			this.txtOriginalOther6.Top = 15F;
			this.txtOriginalOther6.Width = 0.7916667F;
			// 
			// txtTotalOriginalCost6
			// 
			this.txtTotalOriginalCost6.Height = 0.1875F;
			this.txtTotalOriginalCost6.Left = 4.625F;
			this.txtTotalOriginalCost6.Name = "txtTotalOriginalCost6";
			this.txtTotalOriginalCost6.Style = "text-align: right";
			this.txtTotalOriginalCost6.Text = null;
			this.txtTotalOriginalCost6.Top = 15.25F;
			this.txtTotalOriginalCost6.Width = 0.7916667F;
			// 
			// txtAssessedME6
			// 
			this.txtAssessedME6.Height = 0.1875F;
			this.txtAssessedME6.Left = 5.760417F;
			this.txtAssessedME6.Name = "txtAssessedME6";
			this.txtAssessedME6.Style = "text-align: right";
			this.txtAssessedME6.Text = null;
			this.txtAssessedME6.Top = 14.5F;
			this.txtAssessedME6.Width = 0.90625F;
			// 
			// txtAssessedFurniture6
			// 
			this.txtAssessedFurniture6.Height = 0.1875F;
			this.txtAssessedFurniture6.Left = 5.760417F;
			this.txtAssessedFurniture6.Name = "txtAssessedFurniture6";
			this.txtAssessedFurniture6.Style = "text-align: right";
			this.txtAssessedFurniture6.Text = null;
			this.txtAssessedFurniture6.Top = 14.75F;
			this.txtAssessedFurniture6.Width = 0.90625F;
			// 
			// txtAssessedOther6
			// 
			this.txtAssessedOther6.Height = 0.1875F;
			this.txtAssessedOther6.Left = 5.75F;
			this.txtAssessedOther6.Name = "txtAssessedOther6";
			this.txtAssessedOther6.Style = "text-align: right";
			this.txtAssessedOther6.Text = null;
			this.txtAssessedOther6.Top = 15F;
			this.txtAssessedOther6.Width = 0.9166667F;
			// 
			// txtAssessedTotal6
			// 
			this.txtAssessedTotal6.Height = 0.1875F;
			this.txtAssessedTotal6.Left = 5.75F;
			this.txtAssessedTotal6.Name = "txtAssessedTotal6";
			this.txtAssessedTotal6.Style = "text-align: right";
			this.txtAssessedTotal6.Text = null;
			this.txtAssessedTotal6.Top = 15.25F;
			this.txtAssessedTotal6.Width = 0.9166667F;
			// 
			// Label128
			// 
			this.Label128.Height = 0.1875F;
			this.Label128.HyperLink = null;
			this.Label128.Left = 6.0625F;
			this.Label128.Name = "Label128";
			this.Label128.Style = "";
			this.Label128.Text = ".50";
			this.Label128.Top = 15.5F;
			this.Label128.Width = 0.28125F;
			// 
			// txtAllowedValue6
			// 
			this.txtAllowedValue6.Height = 0.1875F;
			this.txtAllowedValue6.Left = 5.75F;
			this.txtAllowedValue6.Name = "txtAllowedValue6";
			this.txtAllowedValue6.Style = "text-align: right";
			this.txtAllowedValue6.Text = null;
			this.txtAllowedValue6.Top = 15.75F;
			this.txtAllowedValue6.Width = 0.9166667F;
			// 
			// Label129
			// 
			this.Label129.Height = 0.1875F;
			this.Label129.HyperLink = null;
			this.Label129.Left = 0F;
			this.Label129.Name = "Label129";
			this.Label129.Style = "";
			this.Label129.Text = "37. Total Allowable Assessed Value (in column F, add lines 6, 12, 18, 24, 30, and" + " 36)";
			this.Label129.Top = 16F;
			this.Label129.Width = 5.395833F;
			// 
			// txtTotalAllowedValue
			// 
			this.txtTotalAllowedValue.Height = 0.1875F;
			this.txtTotalAllowedValue.Left = 5.75F;
			this.txtTotalAllowedValue.Name = "txtTotalAllowedValue";
			this.txtTotalAllowedValue.Style = "text-align: right";
			this.txtTotalAllowedValue.Text = null;
			this.txtTotalAllowedValue.Top = 16F;
			this.txtTotalAllowedValue.Width = 0.9166667F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 1.0625F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left";
			this.lblPage.Text = "(page 1)";
			this.lblPage.Top = 0.4583333F;
			this.lblPage.Width = 1.177083F;
			// 
			// Label39
			// 
			this.Label39.Height = 0.21875F;
			this.Label39.HyperLink = null;
			this.Label39.Left = 0.2291667F;
			this.Label39.Name = "Label39";
			this.Label39.Style = "";
			this.Label39.Text = "Property Tax Rate";
			this.Label39.Top = 0.28125F;
			this.Label39.Width = 1.3125F;
			// 
			// Label66
			// 
			this.Label66.Height = 0.1666667F;
			this.Label66.HyperLink = null;
			this.Label66.Left = 0.125F;
			this.Label66.Name = "Label66";
			this.Label66.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.Label66.Text = "Section 3. Property Tax Information (To be completed by local tax assessor.)";
			this.Label66.Top = 0.0625F;
			this.Label66.Width = 6.833333F;
			// 
			// Label67
			// 
			this.Label67.Height = 0.21875F;
			this.Label67.HyperLink = null;
			this.Label67.Left = 2.25F;
			this.Label67.Name = "Label67";
			this.Label67.Style = "";
			this.Label67.Text = "Assessed Tax";
			this.Label67.Top = 0.28125F;
			this.Label67.Width = 1.3125F;
			// 
			// Label130
			// 
			this.Label130.Height = 0.21875F;
			this.Label130.HyperLink = null;
			this.Label130.Left = 4.875F;
			this.Label130.Name = "Label130";
			this.Label130.Style = "";
			this.Label130.Text = "For Taxes Assessed April 1,";
			this.Label130.Top = 0.28125F;
			this.Label130.Width = 1.8125F;
			// 
			// txtAsOfYear
			// 
			this.txtAsOfYear.Height = 0.21875F;
			this.txtAsOfYear.Left = 6.625F;
			this.txtAsOfYear.Name = "txtAsOfYear";
			this.txtAsOfYear.Style = "text-align: left";
			this.txtAsOfYear.Text = null;
			this.txtAsOfYear.Top = 0.28125F;
			this.txtAsOfYear.Width = 0.5416667F;
			// 
			// Label69
			// 
			this.Label69.Height = 0.21875F;
			this.Label69.HyperLink = null;
			this.Label69.Left = 0.2291667F;
			this.Label69.Name = "Label69";
			this.Label69.Style = "";
			this.Label69.Text = "Assessor Signature";
			this.Label69.Top = 0.53125F;
			this.Label69.Width = 1.3125F;
			// 
			// Label131
			// 
			this.Label131.Height = 0.21875F;
			this.Label131.HyperLink = null;
			this.Label131.Left = 3.3125F;
			this.Label131.Name = "Label131";
			this.Label131.Style = "";
			this.Label131.Text = "Municipality Name";
			this.Label131.Top = 0.53125F;
			this.Label131.Width = 1.3125F;
			// 
			// Label132
			// 
			this.Label132.Height = 0.21875F;
			this.Label132.HyperLink = null;
			this.Label132.Left = 6.1875F;
			this.Label132.Name = "Label132";
			this.Label132.Style = "";
			this.Label132.Text = "Date";
			this.Label132.Top = 0.53125F;
			this.Label132.Width = 0.4375F;
			// 
			// Line29
			// 
			this.Line29.Height = 0F;
			this.Line29.Left = 1.447917F;
			this.Line29.LineWeight = 1F;
			this.Line29.Name = "Line29";
			this.Line29.Top = 0.78125F;
			this.Line29.Width = 1.833333F;
			this.Line29.X1 = 1.447917F;
			this.Line29.X2 = 3.28125F;
			this.Line29.Y1 = 0.78125F;
			this.Line29.Y2 = 0.78125F;
			// 
			// txtMunicipalityName
			// 
			this.txtMunicipalityName.Height = 0.21875F;
			this.txtMunicipalityName.Left = 4.5F;
			this.txtMunicipalityName.Name = "txtMunicipalityName";
			this.txtMunicipalityName.Style = "text-align: left";
			this.txtMunicipalityName.Text = null;
			this.txtMunicipalityName.Top = 0.53125F;
			this.txtMunicipalityName.Width = 1.666667F;
			// 
			// Label133
			// 
			this.Label133.Height = 0.21875F;
			this.Label133.HyperLink = null;
			this.Label133.Left = 0.25F;
			this.Label133.Name = "Label133";
			this.Label133.Style = "font-size: 8.5pt";
			this.Label133.Text = "Applicant (or agent) Signature";
			this.Label133.Top = 1.1875F;
			this.Label133.Width = 1.9375F;
			// 
			// Label134
			// 
			this.Label134.Height = 0.21875F;
			this.Label134.HyperLink = null;
			this.Label134.Left = 3.125F;
			this.Label134.Name = "Label134";
			this.Label134.Style = "font-size: 8.5pt";
			this.Label134.Text = "Social Security Number or Federal EIN (see note, page 5)";
			this.Label134.Top = 1.1875F;
			this.Label134.Width = 3.125F;
			// 
			// Line30
			// 
			this.Line30.Height = 0F;
			this.Line30.Left = 0.25F;
			this.Line30.LineWeight = 1F;
			this.Line30.Name = "Line30";
			this.Line30.Top = 1.125F;
			this.Line30.Width = 2.708333F;
			this.Line30.X1 = 0.25F;
			this.Line30.X2 = 2.958333F;
			this.Line30.Y1 = 1.125F;
			this.Line30.Y2 = 1.125F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 3.125F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 1.125F;
			this.Line4.Width = 2.583333F;
			this.Line4.X1 = 3.125F;
			this.Line4.X2 = 5.708333F;
			this.Line4.Y1 = 1.125F;
			this.Line4.Y2 = 1.125F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.21875F;
			this.txtDate.Left = 6.5625F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "text-align: left";
			this.txtDate.Text = null;
			this.txtDate.Top = 0.53125F;
			this.txtDate.Width = 0.8541667F;
			// 
			// Label135
			// 
			this.Label135.Height = 0.21875F;
			this.Label135.HyperLink = null;
			this.Label135.Left = 6.3125F;
			this.Label135.Name = "Label135";
			this.Label135.Style = "font-size: 8.5pt";
			this.Label135.Text = "Date";
			this.Label135.Top = 1.1875F;
			this.Label135.Width = 0.625F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 6.3125F;
			this.Line5.LineWeight = 1F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 1.125F;
			this.Line5.Width = 1.020833F;
			this.Line5.X1 = 6.3125F;
			this.Line5.X2 = 7.333333F;
			this.Line5.Y1 = 1.125F;
			this.Line5.Y2 = 1.125F;
			// 
			// txtTaxRate
			// 
			this.txtTaxRate.Height = 0.21875F;
			this.txtTaxRate.Left = 1.5F;
			this.txtTaxRate.Name = "txtTaxRate";
			this.txtTaxRate.Style = "text-align: left";
			this.txtTaxRate.Text = null;
			this.txtTaxRate.Top = 0.28125F;
			this.txtTaxRate.Width = 0.6666667F;
			// 
			// txtAssessedTax
			// 
			this.txtAssessedTax.Height = 0.21875F;
			this.txtAssessedTax.Left = 3.3125F;
			this.txtAssessedTax.Name = "txtAssessedTax";
			this.txtAssessedTax.Style = "text-align: left";
			this.txtAssessedTax.Text = null;
			this.txtAssessedTax.Top = 0.28125F;
			this.txtAssessedTax.Width = 1.166667F;
			// 
			// rpt801b
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.479167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtApplicant)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessedDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearME1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearFurniture1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearOther1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalME1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalFurniture1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalOther1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalOriginalCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedME1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedFurniture1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedOther1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedTotal1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAllowedValue1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label48)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label49)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label50)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label51)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearME2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearFurniture2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearOther2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalME2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalFurniture2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalOther2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalOriginalCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedME2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedFurniture2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedOther2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedTotal2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label55)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAllowedValue2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label56)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label57)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label58)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label59)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label60)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label61)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label62)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label63)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label64)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearME3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearFurniture3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearOther3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalME3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalFurniture3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalOther3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalOriginalCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedME3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedFurniture3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedOther3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedTotal3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label68)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAllowedValue3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMunicipalCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTaxYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label70)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label71)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label72)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label73)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label74)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label75)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label76)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label77)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label78)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label79)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label80)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label81)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label82)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label83)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label84)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label85)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label86)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label87)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label88)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label89)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label90)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label91)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label92)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label93)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label94)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label95)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label96)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label97)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label98)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearME4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearFurniture4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearOther4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalME4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalFurniture4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalOther4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalOriginalCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedME4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedFurniture4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedOther4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedTotal4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label102)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAllowedValue4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label103)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label104)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label105)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label106)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label107)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label108)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label109)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label110)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label111)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearME5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearFurniture5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearOther5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalME5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalFurniture5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalOther5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalOriginalCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedME5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedFurniture5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedOther5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedTotal5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label115)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAllowedValue5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label116)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label117)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label118)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label119)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label120)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label121)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label122)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label123)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label124)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearME6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearFurniture6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYearOther6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalME6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalFurniture6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalOther6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalOriginalCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedME6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedFurniture6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedOther6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedTotal6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label128)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAllowedValue6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label129)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAllowedValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label66)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label67)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label130)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAsOfYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label69)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label131)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label132)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMunicipalityName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label133)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label134)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label135)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtApplicant;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAssessedDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label36;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label37;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label38;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAssessmentYearME1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAssessmentYearFurniture1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAssessmentYearOther1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOriginalME1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOriginalFurniture1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOriginalOther1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalOriginalCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessedME1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessedFurniture1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessedOther1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessedTotal1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label42;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAllowedValue1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label43;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label44;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label45;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label46;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label47;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label48;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label49;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label50;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label51;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line9;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line10;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line11;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAssessmentYearME2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAssessmentYearFurniture2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAssessmentYearOther2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOriginalME2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOriginalFurniture2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOriginalOther2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalOriginalCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessedME2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessedFurniture2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessedOther2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessedTotal2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label55;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAllowedValue2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label56;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label57;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label58;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label59;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label60;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label61;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label62;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label63;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label64;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line13;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line14;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line15;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAssessmentYearME3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAssessmentYearFurniture3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAssessmentYearOther3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOriginalME3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOriginalFurniture3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOriginalOther3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalOriginalCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessedME3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessedFurniture3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessedOther3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessedTotal3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label68;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAllowedValue3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMunicipalCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTaxYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniCode;
		private GrapeCity.ActiveReports.SectionReportModel.PageBreak PageBreak1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label70;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label71;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label72;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label73;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label74;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label75;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label76;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label77;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label78;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label79;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label80;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label81;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label82;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label83;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label84;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label85;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label86;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label87;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label88;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label89;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label90;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label91;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label92;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label93;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label94;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label95;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label96;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label97;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label98;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line18;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line19;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line20;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAssessmentYearME4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAssessmentYearFurniture4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAssessmentYearOther4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOriginalME4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOriginalFurniture4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOriginalOther4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalOriginalCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessedME4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessedFurniture4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessedOther4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessedTotal4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label102;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAllowedValue4;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label103;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label104;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label105;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label106;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label107;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label108;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label109;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label110;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label111;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line22;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line23;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line24;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAssessmentYearME5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAssessmentYearFurniture5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAssessmentYearOther5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOriginalME5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOriginalFurniture5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOriginalOther5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalOriginalCost5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessedME5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessedFurniture5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessedOther5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessedTotal5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label115;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAllowedValue5;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label116;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label117;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label118;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label119;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label120;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label121;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label122;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label123;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label124;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line26;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line27;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line28;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAssessmentYearME6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAssessmentYearFurniture6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAssessmentYearOther6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOriginalME6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOriginalFurniture6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOriginalOther6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalOriginalCost6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessedME6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessedFurniture6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessedOther6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessedTotal6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label128;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAllowedValue6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label129;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalAllowedValue;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label39;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label66;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label67;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label130;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAsOfYear;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label69;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label131;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label132;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line29;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMunicipalityName;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label133;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label134;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line30;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label135;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxRate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessedTax;
	}
}
