﻿namespace TWPP0000
{
	/// <summary>
	/// Summary description for srptBETEApplication.
	/// </summary>
	partial class srptBETEApplication
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptBETEApplication));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDatePurchased = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInService = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLife = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePurchased)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInService)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLife)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtDatePurchased,
				this.txtInService,
				this.txtDescription,
				this.txtCost,
				this.txtValue,
				this.txtLife,
				this.txtLine
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label1,
				this.Label7
			});
			this.ReportHeader.Height = 0.4583333F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// Label2
			// 
			this.Label2.Height = 0.3541667F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 1.4375F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold; text-align: center";
			this.Label2.Text = "Date Placed in Service";
			this.Label2.Top = 0.05208333F;
			this.Label2.Width = 0.8229167F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.2083333F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 2.28125F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold";
			this.Label3.Text = "Item Description";
			this.Label3.Top = 0.125F;
			this.Label3.Width = 1.625F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.2083333F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 4.625F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-weight: bold; text-align: right";
			this.Label4.Text = "Purchase Price";
			this.Label4.Top = 0.125F;
			this.Label4.Width = 0.9791667F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.2083333F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 5.625F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-weight: bold; text-align: right";
			this.Label5.Text = "Current Value";
			this.Label5.Top = 0.125F;
			this.Label5.Width = 0.9791667F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.4583333F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 6.625F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-weight: bold; text-align: center";
			this.Label6.Text = "Life Expectancy (years)";
			this.Label6.Top = 0F;
			this.Label6.Width = 0.7916667F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.2083333F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.4479167F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-weight: bold";
			this.Label1.Text = "Date Acquired";
			this.Label1.Top = 0.125F;
			this.Label1.Width = 0.9791667F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.2083333F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-weight: bold";
			this.Label7.Text = "Line";
			this.Label7.Top = 0.125F;
			this.Label7.Width = 0.4166667F;
			// 
			// txtDatePurchased
			// 
			this.txtDatePurchased.Height = 0.1770833F;
			this.txtDatePurchased.Left = 0.4479167F;
			this.txtDatePurchased.Name = "txtDatePurchased";
			this.txtDatePurchased.Text = null;
			this.txtDatePurchased.Top = 0.01041667F;
			this.txtDatePurchased.Width = 0.9791667F;
			// 
			// txtInService
			// 
			this.txtInService.Height = 0.1770833F;
			this.txtInService.Left = 1.4375F;
			this.txtInService.Name = "txtInService";
			this.txtInService.Text = null;
			this.txtInService.Top = 0.01041667F;
			this.txtInService.Width = 0.8229167F;
			// 
			// txtDescription
			// 
			this.txtDescription.Height = 0.1770833F;
			this.txtDescription.Left = 2.28125F;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Text = null;
			this.txtDescription.Top = 0.01041667F;
			this.txtDescription.Width = 2.291667F;
			// 
			// txtCost
			// 
			this.txtCost.Height = 0.1770833F;
			this.txtCost.Left = 4.625F;
			this.txtCost.Name = "txtCost";
			this.txtCost.Style = "text-align: right";
			this.txtCost.Text = null;
			this.txtCost.Top = 0.01041667F;
			this.txtCost.Width = 0.9791667F;
			// 
			// txtValue
			// 
			this.txtValue.Height = 0.1770833F;
			this.txtValue.Left = 5.625F;
			this.txtValue.Name = "txtValue";
			this.txtValue.Style = "text-align: right";
			this.txtValue.Text = null;
			this.txtValue.Top = 0.01041667F;
			this.txtValue.Width = 0.9791667F;
			// 
			// txtLife
			// 
			this.txtLife.Height = 0.1770833F;
			this.txtLife.Left = 6.729167F;
			this.txtLife.Name = "txtLife";
			this.txtLife.Style = "text-align: right";
			this.txtLife.Text = null;
			this.txtLife.Top = 0.01041667F;
			this.txtLife.Width = 0.375F;
			// 
			// txtLine
			// 
			this.txtLine.Height = 0.1770833F;
			this.txtLine.Left = 0F;
			this.txtLine.Name = "txtLine";
			this.txtLine.Text = null;
			this.txtLine.Top = 0.01041667F;
			this.txtLine.Width = 0.4166667F;
			// 
			// srptBETEApplication
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.Name = "ActiveReport1";
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.458333F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePurchased)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInService)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLife)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePurchased;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInService;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLife;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
	}
}
