﻿//Fecher vbPorter - Version 1.0.0.32
using Global;

namespace TWPP0000
{
	public class c801Report
	{
		//=========================================================
		private string strRangeStart = string.Empty;
		private string strRangeEnd = string.Empty;
		private int intRangeType;
		private bool boolRange;
		private int intTaxYear;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cGenericCollection colReport = new cGenericCollection();
		private cGenericCollection colReport_AutoInitialized;

		private cGenericCollection colReport
		{
			get
			{
				if (colReport_AutoInitialized == null)
				{
					colReport_AutoInitialized = new cGenericCollection();
				}
				return colReport_AutoInitialized;
			}
			set
			{
				colReport_AutoInitialized = value;
			}
		}

		private double dblTaxRate;
		private string strTaxRate = string.Empty;
		private string strReportDate = string.Empty;

		public string DisplayedRate
		{
			set
			{
				strTaxRate = value;
			}
			get
			{
				string DisplayedRate = "";
				DisplayedRate = strTaxRate;
				return DisplayedRate;
			}
		}

		public double TaxRate
		{
			set
			{
				dblTaxRate = value;
			}
			get
			{
				double TaxRate = 0;
				TaxRate = dblTaxRate;
				return TaxRate;
			}
		}

		public string ReportDate
		{
			set
			{
				strReportDate = value;
			}
			get
			{
				string ReportDate = "";
				ReportDate = strReportDate;
				return ReportDate;
			}
		}

		public cGenericCollection Records
		{
			get
			{
				cGenericCollection Records = null;
				Records = colReport;
				return Records;
			}
		}

		public string RangeStart
		{
			set
			{
				strRangeStart = value;
			}
			get
			{
				string RangeStart = "";
				RangeStart = strRangeStart;
				return RangeStart;
			}
		}

		public string RangeEnd
		{
			set
			{
				strRangeEnd = value;
			}
			get
			{
				string RangeEnd = "";
				RangeEnd = strRangeEnd;
				return RangeEnd;
			}
		}

		public int RangeType
		{
			set
			{
				intRangeType = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				int RangeType = 0;
				RangeType = intRangeType;
				return RangeType;
			}
		}

		public bool ByRange
		{
			set
			{
				boolRange = value;
			}
			get
			{
				bool ByRange = false;
				ByRange = boolRange;
				return ByRange;
			}
		}

		public int TaxYear
		{
			set
			{
				intTaxYear = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				int TaxYear = 0;
				TaxYear = intTaxYear;
				return TaxYear;
			}
		}
	}
}
