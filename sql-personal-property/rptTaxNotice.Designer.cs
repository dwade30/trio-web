﻿namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptTaxNotice.
	/// </summary>
	partial class rptTaxNotice
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptTaxNotice));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSpace = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMail1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMail2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMail4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMail3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTax1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTax2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTax3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTax4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTax5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTax6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTax7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTax8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTax9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTax10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txttax11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTax12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTax13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTax15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtVal7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtVal8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtVal6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTax14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTaxLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtVal9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtVal5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtVal4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtVal3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtVal2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtVal1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSpace)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMail1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMail2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMail4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMail3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txttax11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVal7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVal8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVal6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVal9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVal5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVal4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVal3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVal2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVal1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTitle,
				this.Field1,
				this.Line1,
				this.txtLocation,
				this.txtSpace,
				this.txtMail1,
				this.txtMail2,
				this.txtMail4,
				this.txtMail3,
				this.txtTax1,
				this.txtTax2,
				this.txtTax3,
				this.txtTax4,
				this.txtTax5,
				this.txtTax6,
				this.txtTax7,
				this.txtTax8,
				this.txtTax9,
				this.txtTax10,
				this.txttax11,
				this.txtTax12,
				this.txtTax13,
				this.txtMapLot,
				this.txtAccount,
				this.txtTax15,
				this.txtVal7,
				this.txtVal8,
				this.txtVal6,
				this.txtTax14,
				this.txtTotal,
				this.txtTotalLabel,
				this.txtTax,
				this.txtTaxLabel,
				this.txtCat9,
				this.txtVal9,
				this.txtVal5,
				this.txtVal4,
				this.txtVal3,
				this.txtVal2,
				this.txtVal1,
				this.txtCat1,
				this.txtCat2,
				this.txtCat3,
				this.txtCat4,
				this.txtCat5,
				this.txtCat6,
				this.txtCat7,
				this.txtCat8
			});
			this.Detail.Height = 3.302083F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// txtTitle
			// 
			this.txtTitle.CanGrow = false;
			this.txtTitle.Height = 0.195F;
			this.txtTitle.Left = 0.0625F;
			this.txtTitle.MultiLine = false;
			this.txtTitle.Name = "txtTitle";
			this.txtTitle.Style = "text-align: center";
			this.txtTitle.Text = null;
			this.txtTitle.Top = 0.0625F;
			this.txtTitle.Width = 7.25F;
			// 
			// Field1
			// 
			this.Field1.CanGrow = false;
			this.Field1.Height = 0.195F;
			this.Field1.Left = 0.0625F;
			this.Field1.MultiLine = false;
			this.Field1.Name = "Field1";
			this.Field1.Style = "text-align: center";
			this.Field1.Text = "* * * * * * * *  T H I S   I S   N O T   A   T A X   B I L L * * * * * * * *";
			this.Field1.Top = 3.09375F;
			this.Field1.Width = 7.25F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 3.28125F;
			this.Line1.Width = 7.5F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.5F;
			this.Line1.Y1 = 3.28125F;
			this.Line1.Y2 = 3.28125F;
			// 
			// txtLocation
			// 
			this.txtLocation.Height = 0.19F;
			this.txtLocation.Left = 0.5625F;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Text = null;
			this.txtLocation.Top = 1.5F;
			this.txtLocation.Width = 3.5F;
			// 
			// txtSpace
			// 
			this.txtSpace.Height = 0.19F;
			this.txtSpace.Left = 0.5625F;
			this.txtSpace.Name = "txtSpace";
			this.txtSpace.Text = null;
			this.txtSpace.Top = 1.65625F;
			this.txtSpace.Width = 3.5F;
			// 
			// txtMail1
			// 
			this.txtMail1.Height = 0.19F;
			this.txtMail1.Left = 0.5625F;
			this.txtMail1.Name = "txtMail1";
			this.txtMail1.Text = "Field4";
			this.txtMail1.Top = 1.875F;
			this.txtMail1.Width = 3.5F;
			// 
			// txtMail2
			// 
			this.txtMail2.Height = 0.19F;
			this.txtMail2.Left = 0.5625F;
			this.txtMail2.Name = "txtMail2";
			this.txtMail2.Text = "Field5";
			this.txtMail2.Top = 2.03125F;
			this.txtMail2.Width = 3.5F;
			// 
			// txtMail4
			// 
			this.txtMail4.Height = 0.19F;
			this.txtMail4.Left = 0.5625F;
			this.txtMail4.Name = "txtMail4";
			this.txtMail4.Text = "Field6";
			this.txtMail4.Top = 2.34375F;
			this.txtMail4.Width = 3.5F;
			// 
			// txtMail3
			// 
			this.txtMail3.Height = 0.19F;
			this.txtMail3.Left = 0.5625F;
			this.txtMail3.Name = "txtMail3";
			this.txtMail3.Text = "Field7";
			this.txtMail3.Top = 2.1875F;
			this.txtMail3.Width = 3.5F;
			// 
			// txtTax1
			// 
			this.txtTax1.Height = 0.19F;
			this.txtTax1.Left = 0.1875F;
			this.txtTax1.Name = "txtTax1";
			this.txtTax1.Style = "font-family: \'Courier\'; ddo-char-set: 1";
			this.txtTax1.Text = "Field2";
			this.txtTax1.Top = 0.25F;
			this.txtTax1.Width = 7.0625F;
			// 
			// txtTax2
			// 
			this.txtTax2.Height = 0.19F;
			this.txtTax2.Left = 0.1875F;
			this.txtTax2.Name = "txtTax2";
			this.txtTax2.Style = "font-family: \'Courier\'; ddo-char-set: 1";
			this.txtTax2.Text = "Field3";
			this.txtTax2.Top = 0.40625F;
			this.txtTax2.Width = 7.0625F;
			// 
			// txtTax3
			// 
			this.txtTax3.Height = 0.19F;
			this.txtTax3.Left = 0.1875F;
			this.txtTax3.Name = "txtTax3";
			this.txtTax3.Style = "font-family: \'Courier\'; ddo-char-set: 1";
			this.txtTax3.Text = "Field4";
			this.txtTax3.Top = 0.5625F;
			this.txtTax3.Width = 7.0625F;
			// 
			// txtTax4
			// 
			this.txtTax4.Height = 0.19F;
			this.txtTax4.Left = 0.1875F;
			this.txtTax4.Name = "txtTax4";
			this.txtTax4.Style = "font-family: \'Courier\'; ddo-char-set: 1";
			this.txtTax4.Text = "Field5";
			this.txtTax4.Top = 0.71875F;
			this.txtTax4.Width = 7.0625F;
			// 
			// txtTax5
			// 
			this.txtTax5.Height = 0.19F;
			this.txtTax5.Left = 0.1875F;
			this.txtTax5.Name = "txtTax5";
			this.txtTax5.Style = "font-family: \'Courier\'; ddo-char-set: 1";
			this.txtTax5.Text = "Field6";
			this.txtTax5.Top = 0.875F;
			this.txtTax5.Width = 7.0625F;
			// 
			// txtTax6
			// 
			this.txtTax6.Height = 0.19F;
			this.txtTax6.Left = 0.1875F;
			this.txtTax6.Name = "txtTax6";
			this.txtTax6.Style = "font-family: \'Courier\'; ddo-char-set: 1";
			this.txtTax6.Text = null;
			this.txtTax6.Top = 1.03125F;
			this.txtTax6.Width = 7.0625F;
			// 
			// txtTax7
			// 
			this.txtTax7.Height = 0.19F;
			this.txtTax7.Left = 0.1875F;
			this.txtTax7.Name = "txtTax7";
			this.txtTax7.Style = "font-family: \'Courier\'; ddo-char-set: 1";
			this.txtTax7.Text = "Field8";
			this.txtTax7.Top = 1.1875F;
			this.txtTax7.Width = 7.0625F;
			// 
			// txtTax8
			// 
			this.txtTax8.Height = 0.19F;
			this.txtTax8.Left = 4.25F;
			this.txtTax8.MultiLine = false;
			this.txtTax8.Name = "txtTax8";
			this.txtTax8.Style = "font-family: \'Courier\'; ddo-char-set: 1";
			this.txtTax8.Text = "Field9";
			this.txtTax8.Top = 1.34375F;
			this.txtTax8.Width = 3F;
			// 
			// txtTax9
			// 
			this.txtTax9.Height = 0.19F;
			this.txtTax9.Left = 4.25F;
			this.txtTax9.MultiLine = false;
			this.txtTax9.Name = "txtTax9";
			this.txtTax9.Style = "font-family: \'Courier\'; ddo-char-set: 1";
			this.txtTax9.Text = "Field10";
			this.txtTax9.Top = 1.5F;
			this.txtTax9.Width = 3F;
			// 
			// txtTax10
			// 
			this.txtTax10.Height = 0.19F;
			this.txtTax10.Left = 4.25F;
			this.txtTax10.MultiLine = false;
			this.txtTax10.Name = "txtTax10";
			this.txtTax10.Style = "font-family: \'Courier\'; ddo-char-set: 1";
			this.txtTax10.Text = "Field11";
			this.txtTax10.Top = 1.65625F;
			this.txtTax10.Width = 3F;
			// 
			// txttax11
			// 
			this.txttax11.CanGrow = false;
			this.txttax11.Height = 0.19F;
			this.txttax11.Left = 4.25F;
			this.txttax11.MultiLine = false;
			this.txttax11.Name = "txttax11";
			this.txttax11.Style = "font-family: \'Courier\'; ddo-char-set: 1";
			this.txttax11.Text = "Field12";
			this.txttax11.Top = 1.8125F;
			this.txttax11.Width = 3F;
			// 
			// txtTax12
			// 
			this.txtTax12.Height = 0.19F;
			this.txtTax12.Left = 4.25F;
			this.txtTax12.MultiLine = false;
			this.txtTax12.Name = "txtTax12";
			this.txtTax12.Style = "font-family: \'Courier\'; ddo-char-set: 1";
			this.txtTax12.Text = "Field13";
			this.txtTax12.Top = 1.96875F;
			this.txtTax12.Width = 3F;
			// 
			// txtTax13
			// 
			this.txtTax13.Height = 0.19F;
			this.txtTax13.Left = 4.25F;
			this.txtTax13.MultiLine = false;
			this.txtTax13.Name = "txtTax13";
			this.txtTax13.Style = "font-family: \'Courier\'; ddo-char-set: 1";
			this.txtTax13.Text = "Field14";
			this.txtTax13.Top = 2.125F;
			this.txtTax13.Width = 3F;
			// 
			// txtMapLot
			// 
			this.txtMapLot.CanGrow = false;
			this.txtMapLot.Height = 0.19F;
			this.txtMapLot.Left = 1.8125F;
			this.txtMapLot.MultiLine = false;
			this.txtMapLot.Name = "txtMapLot";
			this.txtMapLot.Style = "font-family: \'Courier\'; ddo-char-set: 1";
			this.txtMapLot.Text = null;
			this.txtMapLot.Top = 1.34375F;
			this.txtMapLot.Visible = false;
			this.txtMapLot.Width = 2.25F;
			// 
			// txtAccount
			// 
			this.txtAccount.CanGrow = false;
			this.txtAccount.Height = 0.19F;
			this.txtAccount.Left = 0.5625F;
			this.txtAccount.MultiLine = false;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-family: \'Courier\'; ddo-char-set: 1";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 1.34375F;
			this.txtAccount.Width = 1.1875F;
			// 
			// txtTax15
			// 
			this.txtTax15.CanGrow = false;
			this.txtTax15.Height = 0.19F;
			this.txtTax15.Left = 4.25F;
			this.txtTax15.MultiLine = false;
			this.txtTax15.Name = "txtTax15";
			this.txtTax15.Style = "font-family: \'Courier\'; ddo-char-set: 1";
			this.txtTax15.Text = "Field2";
			this.txtTax15.Top = 2.4375F;
			this.txtTax15.Width = 3F;
			// 
			// txtVal7
			// 
			this.txtVal7.CanGrow = false;
			this.txtVal7.Height = 0.19F;
			this.txtVal7.Left = 6.125F;
			this.txtVal7.MultiLine = false;
			this.txtVal7.Name = "txtVal7";
			this.txtVal7.Style = "text-align: right";
			this.txtVal7.Text = null;
			this.txtVal7.Top = 2.28125F;
			this.txtVal7.Visible = false;
			this.txtVal7.Width = 1.125F;
			// 
			// txtVal8
			// 
			this.txtVal8.CanGrow = false;
			this.txtVal8.Height = 0.19F;
			this.txtVal8.Left = 6.125F;
			this.txtVal8.MultiLine = false;
			this.txtVal8.Name = "txtVal8";
			this.txtVal8.Style = "text-align: right";
			this.txtVal8.Text = null;
			this.txtVal8.Top = 2.4375F;
			this.txtVal8.Visible = false;
			this.txtVal8.Width = 1.125F;
			// 
			// txtVal6
			// 
			this.txtVal6.CanGrow = false;
			this.txtVal6.Height = 0.19F;
			this.txtVal6.Left = 6.125F;
			this.txtVal6.MultiLine = false;
			this.txtVal6.Name = "txtVal6";
			this.txtVal6.Style = "text-align: right";
			this.txtVal6.Text = null;
			this.txtVal6.Top = 2.125F;
			this.txtVal6.Visible = false;
			this.txtVal6.Width = 1.125F;
			// 
			// txtTax14
			// 
			this.txtTax14.Height = 0.19F;
			this.txtTax14.Left = 4.25F;
			this.txtTax14.Name = "txtTax14";
			this.txtTax14.Style = "font-family: \'Courier\'; ddo-char-set: 1";
			this.txtTax14.Text = "Field2";
			this.txtTax14.Top = 2.28125F;
			this.txtTax14.Width = 3F;
			// 
			// txtTotal
			// 
			this.txtTotal.Height = 0.19F;
			this.txtTotal.Left = 5.875F;
			this.txtTotal.Name = "txtTotal";
			this.txtTotal.Style = "text-align: right";
			this.txtTotal.Text = "Field2";
			this.txtTotal.Top = 2.75F;
			this.txtTotal.Visible = false;
			this.txtTotal.Width = 1.375F;
			// 
			// txtTotalLabel
			// 
			this.txtTotalLabel.Height = 0.19F;
			this.txtTotalLabel.Left = 4.4375F;
			this.txtTotalLabel.Name = "txtTotalLabel";
			this.txtTotalLabel.Style = "font-family: \'Courier\'; ddo-char-set: 1";
			this.txtTotalLabel.Text = "TOTAL:";
			this.txtTotalLabel.Top = 2.75F;
			this.txtTotalLabel.Visible = false;
			this.txtTotalLabel.Width = 1F;
			// 
			// txtTax
			// 
			this.txtTax.CanGrow = false;
			this.txtTax.Height = 0.19F;
			this.txtTax.Left = 6.25F;
			this.txtTax.MultiLine = false;
			this.txtTax.Name = "txtTax";
			this.txtTax.Style = "text-align: right";
			this.txtTax.Text = "Field2";
			this.txtTax.Top = 2.90625F;
			this.txtTax.Visible = false;
			this.txtTax.Width = 1F;
			// 
			// txtTaxLabel
			// 
			this.txtTaxLabel.CanGrow = false;
			this.txtTaxLabel.Height = 0.19F;
			this.txtTaxLabel.Left = 4.4375F;
			this.txtTaxLabel.MultiLine = false;
			this.txtTaxLabel.Name = "txtTaxLabel";
			this.txtTaxLabel.Style = "font-family: \'Courier\'; ddo-char-set: 1";
			this.txtTaxLabel.Text = "Estimated Tax";
			this.txtTaxLabel.Top = 2.90625F;
			this.txtTaxLabel.Visible = false;
			this.txtTaxLabel.Width = 1.8125F;
			// 
			// txtCat9
			// 
			this.txtCat9.Height = 0.19F;
			this.txtCat9.Left = 4.25F;
			this.txtCat9.Name = "txtCat9";
			this.txtCat9.Text = "Field2";
			this.txtCat9.Top = 2.59375F;
			this.txtCat9.Width = 1.8125F;
			// 
			// txtVal9
			// 
			this.txtVal9.CanGrow = false;
			this.txtVal9.Height = 0.19F;
			this.txtVal9.Left = 6.125F;
			this.txtVal9.MultiLine = false;
			this.txtVal9.Name = "txtVal9";
			this.txtVal9.Style = "text-align: right";
			this.txtVal9.Text = "Field2";
			this.txtVal9.Top = 2.59375F;
			this.txtVal9.Width = 1.125F;
			// 
			// txtVal5
			// 
			this.txtVal5.CanGrow = false;
			this.txtVal5.Height = 0.19F;
			this.txtVal5.Left = 6.125F;
			this.txtVal5.MultiLine = false;
			this.txtVal5.Name = "txtVal5";
			this.txtVal5.Style = "text-align: right";
			this.txtVal5.Text = "Field2";
			this.txtVal5.Top = 1.96875F;
			this.txtVal5.Width = 1.125F;
			// 
			// txtVal4
			// 
			this.txtVal4.CanGrow = false;
			this.txtVal4.Height = 0.19F;
			this.txtVal4.Left = 6.125F;
			this.txtVal4.MultiLine = false;
			this.txtVal4.Name = "txtVal4";
			this.txtVal4.Style = "text-align: right";
			this.txtVal4.Text = "Field2";
			this.txtVal4.Top = 1.8125F;
			this.txtVal4.Width = 1.125F;
			// 
			// txtVal3
			// 
			this.txtVal3.CanGrow = false;
			this.txtVal3.Height = 0.19F;
			this.txtVal3.Left = 6.125F;
			this.txtVal3.MultiLine = false;
			this.txtVal3.Name = "txtVal3";
			this.txtVal3.Style = "text-align: right";
			this.txtVal3.Text = "Field3";
			this.txtVal3.Top = 1.65625F;
			this.txtVal3.Width = 1.125F;
			// 
			// txtVal2
			// 
			this.txtVal2.CanGrow = false;
			this.txtVal2.Height = 0.19F;
			this.txtVal2.Left = 6.125F;
			this.txtVal2.MultiLine = false;
			this.txtVal2.Name = "txtVal2";
			this.txtVal2.Style = "text-align: right";
			this.txtVal2.Text = "Field4";
			this.txtVal2.Top = 1.5F;
			this.txtVal2.Width = 1.125F;
			// 
			// txtVal1
			// 
			this.txtVal1.CanGrow = false;
			this.txtVal1.Height = 0.19F;
			this.txtVal1.Left = 6.125F;
			this.txtVal1.MultiLine = false;
			this.txtVal1.Name = "txtVal1";
			this.txtVal1.Style = "text-align: right";
			this.txtVal1.Text = "Field5";
			this.txtVal1.Top = 1.34375F;
			this.txtVal1.Width = 1.125F;
			// 
			// txtCat1
			// 
			this.txtCat1.CanGrow = false;
			this.txtCat1.Height = 0.19F;
			this.txtCat1.Left = 4.25F;
			this.txtCat1.MultiLine = false;
			this.txtCat1.Name = "txtCat1";
			this.txtCat1.Text = "Field2";
			this.txtCat1.Top = 1.34375F;
			this.txtCat1.Width = 1.8125F;
			// 
			// txtCat2
			// 
			this.txtCat2.CanGrow = false;
			this.txtCat2.Height = 0.19F;
			this.txtCat2.Left = 4.25F;
			this.txtCat2.MultiLine = false;
			this.txtCat2.Name = "txtCat2";
			this.txtCat2.Text = "Field3";
			this.txtCat2.Top = 1.5F;
			this.txtCat2.Width = 1.8125F;
			// 
			// txtCat3
			// 
			this.txtCat3.CanGrow = false;
			this.txtCat3.Height = 0.19F;
			this.txtCat3.Left = 4.25F;
			this.txtCat3.MultiLine = false;
			this.txtCat3.Name = "txtCat3";
			this.txtCat3.Text = "Field2";
			this.txtCat3.Top = 1.65625F;
			this.txtCat3.Width = 1.8125F;
			// 
			// txtCat4
			// 
			this.txtCat4.CanGrow = false;
			this.txtCat4.Height = 0.19F;
			this.txtCat4.Left = 4.25F;
			this.txtCat4.MultiLine = false;
			this.txtCat4.Name = "txtCat4";
			this.txtCat4.Text = "Field3";
			this.txtCat4.Top = 1.8125F;
			this.txtCat4.Width = 1.8125F;
			// 
			// txtCat5
			// 
			this.txtCat5.CanGrow = false;
			this.txtCat5.Height = 0.19F;
			this.txtCat5.Left = 4.25F;
			this.txtCat5.MultiLine = false;
			this.txtCat5.Name = "txtCat5";
			this.txtCat5.Text = "Field2";
			this.txtCat5.Top = 1.96875F;
			this.txtCat5.Width = 1.8125F;
			// 
			// txtCat6
			// 
			this.txtCat6.CanGrow = false;
			this.txtCat6.Height = 0.19F;
			this.txtCat6.Left = 4.25F;
			this.txtCat6.MultiLine = false;
			this.txtCat6.Name = "txtCat6";
			this.txtCat6.Text = "Field3";
			this.txtCat6.Top = 2.125F;
			this.txtCat6.Width = 1.8125F;
			// 
			// txtCat7
			// 
			this.txtCat7.CanGrow = false;
			this.txtCat7.Height = 0.19F;
			this.txtCat7.Left = 4.25F;
			this.txtCat7.MultiLine = false;
			this.txtCat7.Name = "txtCat7";
			this.txtCat7.Text = "Field2";
			this.txtCat7.Top = 2.28125F;
			this.txtCat7.Width = 1.8125F;
			// 
			// txtCat8
			// 
			this.txtCat8.CanGrow = false;
			this.txtCat8.Height = 0.19F;
			this.txtCat8.Left = 4.25F;
			this.txtCat8.MultiLine = false;
			this.txtCat8.Name = "txtCat8";
			this.txtCat8.Text = "Field3";
			this.txtCat8.Top = 2.4375F;
			this.txtCat8.Width = 1.8125F;
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// rptTaxNotice
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.499306F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSpace)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMail1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMail2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMail4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMail3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txttax11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVal7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVal8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVal6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVal9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVal5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVal4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVal3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVal2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVal1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSpace;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMail1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMail2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMail4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMail3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTax1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTax2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTax3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTax4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTax5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTax6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTax7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTax8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTax9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTax10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txttax11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTax12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTax13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTax15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTax14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVal1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat8;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
