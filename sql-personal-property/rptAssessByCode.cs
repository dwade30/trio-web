﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptAssessByCode.
	/// </summary>
	public partial class rptAssessByCode : BaseSectionReport
	{
		public rptAssessByCode()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Assessment By Code";
		}

		public static rptAssessByCode InstancePtr
		{
			get
			{
				return (rptAssessByCode)Sys.GetInstance(typeof(rptAssessByCode));
			}
		}

		protected rptAssessByCode _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptAssessByCode	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			SubBillingBusCode.Report = null;
			SubCurrentBusCode.Report = null;
			SubBillingStreetCode.Report = null;
			SubCurrentStreetCode.Report = null;
			SubBillingTranCode.Report = null;
			SubCurrentTranCode.Report = null;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			SubBillingBusCode.Report = new srptAssessBusCode();
			SubBillingBusCode.Report.UserData = "BILLINGBUSCODE";
			SubCurrentBusCode.Report = new srptAssessBusCode();
			SubCurrentBusCode.Report.UserData = "CURRENTBUSCODE";
			SubBillingStreetCode.Report = new srptAssessBusCode();
			SubBillingStreetCode.Report.UserData = "BILLINGSTREETCODE";
			SubCurrentStreetCode.Report = new srptAssessBusCode();
			SubCurrentStreetCode.Report.UserData = "CURRENTSTREETCODE";
			SubBillingTranCode.Report = new srptAssessBusCode();
			SubBillingTranCode.Report.UserData = "BILLINGTRANCODE";
			SubCurrentTranCode.Report = new srptAssessBusCode();
			SubCurrentTranCode.Report.UserData = "CURRENTTRANCODE";
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		
	}
}
