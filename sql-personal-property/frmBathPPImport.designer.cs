//Fecher vbPorter - Version 1.0.0.93
using Wisej.Web;

using fecherFoundation;
using fecherFoundation.Extensions;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmBathPPImport.
	/// </summary>
	public partial class frmBathPPImport
	{
		public fecherFoundation.FCCheckBox chkMarkDeleted;
		public fecherFoundation.FCCheckBox chkClearValues;
		public fecherFoundation.FCTextBox txtFile;
		public fecherFoundation.FCButton Command1;
		public fecherFoundation.FCButton cmdBrowse;
		public fecherFoundation.FCLabel lblUpdate;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		public frmBathPPImport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmBathPPImport InstancePtr
		{
			get
			{
				return (frmBathPPImport)Sys.GetInstance(typeof(frmBathPPImport));
			}
		}
		protected static frmBathPPImport _InstancePtr = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.chkMarkDeleted = new fecherFoundation.FCCheckBox();
			this.chkClearValues = new fecherFoundation.FCCheckBox();
			this.txtFile = new fecherFoundation.FCTextBox();
			this.Command1 = new fecherFoundation.FCButton();
			this.cmdBrowse = new fecherFoundation.FCButton();
			this.lblUpdate = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			((System.ComponentModel.ISupportInitialize)(this.chkMarkDeleted)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClearValues)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Command1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).BeginInit();
			this.SuspendLayout();
			// 
			// chkMarkDeleted
			// 
			this.chkMarkDeleted.Checked = true;
			this.chkMarkDeleted.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkMarkDeleted.ForeColor = System.Drawing.SystemColors.ControlText;
			this.chkMarkDeleted.Location = new System.Drawing.Point(100, 38);
			this.chkMarkDeleted.Name = "chkMarkDeleted";
			this.chkMarkDeleted.Size = new System.Drawing.Size(188, 22);
			this.chkMarkDeleted.TabIndex = 4;
			this.chkMarkDeleted.Text = "Mark all Properties as Deleted";
			this.ToolTip1.SetToolTip(this.chkMarkDeleted, "Mark all properties as deleted so that only imported accounts are active");
			// 
			// chkClearValues
			// 
			this.chkClearValues.ForeColor = System.Drawing.SystemColors.ControlText;
			this.chkClearValues.Location = new System.Drawing.Point(100, 59);
			this.chkClearValues.Name = "chkClearValues";
			this.chkClearValues.Size = new System.Drawing.Size(93, 22);
			this.chkClearValues.TabIndex = 3;
			this.chkClearValues.Text = "Clear Values";
			this.ToolTip1.SetToolTip(this.chkClearValues, "Clear all billing values so that only imported properties have values");
			this.chkClearValues.Visible = false;
			// 
			// txtFile
			// 
			this.txtFile.BackColor = System.Drawing.SystemColors.Window;
			this.txtFile.ForeColor = System.Drawing.SystemColors.WindowText;
			this.txtFile.Location = new System.Drawing.Point(9, 104);
			this.txtFile.Name = "txtFile";
			this.txtFile.Size = new System.Drawing.Size(379, 24);
			this.txtFile.TabIndex = 2;
			this.ToolTip1.SetToolTip(this.txtFile, null);
			// 
			// Command1
			// 
			this.Command1.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Command1.Location = new System.Drawing.Point(153, 261);
			this.Command1.Name = "Command1";
			this.Command1.Size = new System.Drawing.Size(95, 25);
			this.Command1.TabIndex = 1;
			this.Command1.Text = "Import";
			this.ToolTip1.SetToolTip(this.Command1, null);
			this.Command1.Click += new System.EventHandler(this.Command1_Click);
			// 
			// cmdBrowse
			// 
			this.cmdBrowse.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cmdBrowse.Location = new System.Drawing.Point(319, 138);
			this.cmdBrowse.Name = "cmdBrowse";
			this.cmdBrowse.Size = new System.Drawing.Size(71, 24);
			this.cmdBrowse.Text = "Browse";
			this.ToolTip1.SetToolTip(this.cmdBrowse, null);
			this.cmdBrowse.Click += new System.EventHandler(this.cmdBrowse_Click);
			// 
			// lblUpdate
			// 
			this.lblUpdate.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lblUpdate.Location = new System.Drawing.Point(11, 204);
			this.lblUpdate.Name = "lblUpdate";
			this.lblUpdate.Size = new System.Drawing.Size(381, 35);
			this.lblUpdate.TabIndex = 6;
			this.ToolTip1.SetToolTip(this.lblUpdate, null);
			// 
			// Label1
			// 
			this.Label1.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label1.Location = new System.Drawing.Point(12, 87);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(173, 16);
			this.Label1.TabIndex = 5;
			this.Label1.Text = "FILENAME";
			this.ToolTip1.SetToolTip(this.Label1, null);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.Seperator,
            this.mnuExit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "&File";
			// 
			// mnuSaveContinue
			// 
			this.mnuSaveContinue.Index = 0;
			this.mnuSaveContinue.Name = "mnuSaveContinue";
			this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveContinue.Text = "S&ave && Continue";
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "E&xit    (Esc)";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// frmBathPPImport
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(396, 307);
			this.Controls.Add(this.chkMarkDeleted);
			this.Controls.Add(this.chkClearValues);
			this.Controls.Add(this.txtFile);
			this.Controls.Add(this.Command1);
			this.Controls.Add(this.cmdBrowse);
			this.Controls.Add(this.lblUpdate);
			this.Controls.Add(this.Label1);
			this.FillColor = 16777215;
			this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmBathPPImport";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Import Properties";
			this.ToolTip1.SetToolTip(this, null);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmBathPPImport_KeyDown);
			((System.ComponentModel.ISupportInitialize)(this.chkMarkDeleted)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClearValues)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Command1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
