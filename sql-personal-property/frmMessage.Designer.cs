﻿//Fecher vbPorter - Version 1.0.0.32
using fecherFoundation;
using Global;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmMessage.
	/// </summary>
	partial class frmMessage : BaseForm
	{
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Shape1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMessage));
			this.Label1 = new fecherFoundation.FCLabel();
			this.Shape1 = new fecherFoundation.FCLabel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 194);
			this.BottomPanel.Size = new System.Drawing.Size(259, 23);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.Shape1);
			this.ClientArea.Size = new System.Drawing.Size(259, 134);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(259, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(187, 82);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "PRINTING PLEASE WAIT";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Shape1
			// 
			this.Shape1.BorderStyle = 1;
			this.Shape1.Location = new System.Drawing.Point(35, 21);
			this.Shape1.Name = "Shape1";
			this.Shape1.Size = new System.Drawing.Size(195, 90);
			this.Shape1.TabIndex = 1;
			// 
			// frmMessage
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(259, 217);
			this.FillColor = 0;
			this.FormBorderStyle = Wisej.Web.FormBorderStyle.None;
			this.Name = "frmMessage";
			this.ShowInTaskbar = false;
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.frmMessage_Load);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
