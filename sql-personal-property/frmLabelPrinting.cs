﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmLabelPrinting.
	/// </summary>
	public partial class frmLabelPrinting : BaseForm
	{
		public frmLabelPrinting()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.txtRange = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.txtRange.AddControlArrayElement(txtRange_1, 1);
			this.txtRange.AddControlArrayElement(txtRange_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmLabelPrinting InstancePtr
		{
			get
			{
				return (frmLabelPrinting)Sys.GetInstance(typeof(frmLabelPrinting));
			}
		}

		protected frmLabelPrinting _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		const byte LOC_CODE = 64;
		const byte OPEN2_CODE = 32;
		const byte ACCT_CODE = 16;
		const byte OPEN1_CODE = 8;
		const byte ADDR_CODE = 4;
		const byte VALUE_CODE = 2;
		const byte ESTTAX_CODE = 1;
		clsPrintLabel labLabel = new clsPrintLabel();
		private int intAssessIndex;

		private void FillLabelTypeCombo()
		{
			int x;
			for (x = 0; x <= labLabel.TypeCount - 1; x++)
			{
				if (labLabel.Get_Visible(x))
				{
					// If labLabel.IsDymoLabel(x) Then
					// labLabel.Visible(x) = False
					// End If
				}
				else
				{
					if (labLabel.Get_ID(x) == modLabels.CNSTLBLTYPEASSESSMENT)
					{
						labLabel.Set_Visible(x, true);
					}
					else if (labLabel.Get_ID(x) == modLabels.CNSTLBLTYPE4033)
					{
						labLabel.Set_Visible(x, true);
					}
					else if (labLabel.Get_ID(x) == modLabels.CNSTLBLTYPE4031)
					{
						labLabel.Set_Visible(x, true);
					}
					else if (labLabel.Get_ID(x) == modLabels.CNSTLBLTYPE4065)
					{
						labLabel.Set_Visible(x, true);
					}
					else if (labLabel.Get_IsDymoLabel(x))
					{
						labLabel.Set_Visible(x, true);
					}
				}
				if (labLabel.Get_Visible(x))
				{
					cmbLabelType.AddItem(labLabel.Get_Caption(x));
					cmbLabelType.ItemData(cmbLabelType.NewIndex, labLabel.Get_ID(x));
					if (labLabel.Get_ID(x) == modLabels.CNSTLBLTYPEASSESSMENT)
					{
						intAssessIndex = cmbLabelType.NewIndex;
					}
				}
			}
			// x
			// .AddItem ("Avery 4013") '0
			// .ItemData(.NewIndex) = 0
			// .AddItem ("Avery 4014") '1
			// .ItemData(.NewIndex) = 1
			// 
			// .AddItem ("Avery 4030") '2
			// .ItemData(.NewIndex) = 2
			// 
			// .AddItem ("Avery 4031") '9
			// .ItemData(.NewIndex) = 9
			// .AddItem ("Avery 4033") '10
			// .ItemData(.NewIndex) = 10
			// .AddItem ("Avery 4065")
			// .ItemData(.NewIndex) = 11
			// 
			// .AddItem ("Avery 5160,5260,5970") '3
			// .ItemData(.NewIndex) = 3
			// .AddItem ("Avery 5161,5261,5661") '4
			// .ItemData(.NewIndex) = 4
			// .AddItem ("Avery 5162,5262,5662") '5
			// .ItemData(.NewIndex) = 5
			// .AddItem ("Avery 5163,5263,5663") '6
			// .ItemData(.NewIndex) = 6
			// .AddItem ("Assessment Labels") '8
			// .ItemData(.NewIndex) = 8
			// intAssessIndex = .NewIndex
		}

		private void cmbLabelType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			string strDesc = "";
			//OptLabelType[0].Enabled = true;
			//OptLabelType[1].Enabled = true;
			//OptLabelType[2].Enabled = true;
			//OptLabelType[4].Enabled = true;
			if (cmbLabelType.SelectedIndex < 0)
				return;
			// Select Case .ListIndex
			switch (cmbLabelType.ItemData(cmbLabelType.SelectedIndex))
			{
				case modLabels.CNSTLBLTYPE4013:
					{
						strDesc = "Style 4013.  15/16 in. x 3.5 in. This type is a continuous sheet of labels used with a dot matrix.";
						// vsLayout.ColWidth(0) = 3.5 * 1440
						framLaserOptions.Visible = false;
						break;
					}
				case modLabels.CNSTLBLTYPE4014:
					{
						strDesc = "Style 4014.  1 7/16 in. x 4 in. This type is a continuous sheet of labels used with a dot matrix.";
						// vsLayout.ColWidth(0) = 4 * 1440
						framLaserOptions.Visible = false;
						break;
					}
				case modLabels.CNSTLBLTYPE4030:
					{
						strDesc = "Style 4030.  15/16 in. x 3.5 in. 2 Across. This type is a continuous sheet of labels used with a dot matrix.";
						framLaserOptions.Visible = false;
						break;
					}
				case modLabels.CNSTLBLTYPE5160:
					{
						strDesc = "Style 5160, 5260, 5560, 5660, 5960, 5970, 5971, 5972, 5979, 5980, 6241, 6460. Sheet of 3 x 10. 1 in. X 2 5/8 in.";
						// vsLayout.ColWidth(0) = 2.625 * 1440
						framLaserOptions.Visible = true;
						break;
					}
				case modLabels.CNSTLBLTYPE5161:
					{
						strDesc = "Style 5161, 5261, 5661, 5961. Sheet of 2 X 10. 1 in. X 4 in.";
						framLaserOptions.Visible = true;
						// vsLayout.ColWidth(0) = 4 * 1440
						break;
					}
				case modLabels.CNSTLBLTYPE5162:
					{
						strDesc = "Style 5162, 5262, 5662, 5962. Sheet of 2 X 7. 1 1/3 in. X 4 in.";
						framLaserOptions.Visible = true;
						// vsLayout.ColWidth(0) = 4 * 1440
						break;
					}
				case modLabels.CNSTLBLTYPE5163:
					{
						strDesc = "Style 5163, 5263, 5663, 5963. Sheet of 2 X 5. 2 in. X 4 in.";
						framLaserOptions.Visible = true;
						// vsLayout.ColWidth(0) = 4 * 1440
						break;
					}
				case modLabels.CNSTLBLTYPE5026:
					{
						strDesc = "Style 5026, 5027. Sheet of 2 X 9.  1 in. X 3.5 in.";
						framLaserOptions.Visible = true;
						break;
					}
				case modLabels.CNSTLBLTYPE5066:
					{
						strDesc = "Style 5066, 5266, 5366, 5666, 5766, 5866, 5966, 8366. Sheet of 2 X 15.  .66 in. X 3.5 in.";
						framLaserOptions.Visible = true;
						break;
					}
				case modLabels.CNSTLBLTYPEASSESSMENT:
					{
						strDesc = "Assessment labels 7/16 in. X 5 in. This type is a continuous sheet of labels used with a dot matrix.";
						// OptLabelType(0).Enabled = False
						// OptLabelType(1).Enabled = False
						// OptLabelType(2).Enabled = False
						// OptLabelType(4).Enabled = False
						framLaserOptions.Visible = false;
						if (cmbtLabelType.SelectedIndex != 3)
						{
							cmbtLabelType.SelectedIndex = 3;
						}
						break;
					}
				case modLabels.CNSTLBLTYPE4031:
					{
						strDesc = "Style 4031.  15/16 in. x 3.5 in. 3 Across. This type is a continuous sheet of labels used with a wide dot matrix.";
						framLaserOptions.Visible = false;
						break;
					}
				case modLabels.CNSTLBLTYPE4033:
					{
						strDesc = "Style 4033.  1 7/16 in. x 4 in. 3 Across. This type is a continuous sheet of labels used with a wide dot matrix.";
						framLaserOptions.Visible = false;
						break;
					}
				case modLabels.CNSTLBLTYPE4065:
					{
						strDesc = "Style 4065.  15/16 in. x 4 in. This type is a continuous sheet of labels used with a dot matrix.";
						framLaserOptions.Visible = false;
						break;
					}
			}
			//end switch
			lblDescription.Text = strDesc;
		}

		private void frmLabelPrinting_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				mnuExit_Click();
			}
		}

		private void frmLabelPrinting_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmLabelPrinting properties;
			//frmLabelPrinting.ScaleWidth	= 9045;
			//frmLabelPrinting.ScaleHeight	= 7530;
			//frmLabelPrinting.LinkTopic	= "Form1";
			//frmLabelPrinting.LockControls	= true;
			//End Unmaped Properties
			int x;
			int intType;
			string strType;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			//lblDescription.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			FillLabelTypeCombo();
			strType = Strings.Trim(modRegistry.GetRegistryKey("PPLastLabelType"));
			intType = FCConvert.ToInt32(Math.Round(Conversion.Val(strType)));
			cmbLabelType.SelectedIndex = 1;
			// the 4014 labels
			if (strType != string.Empty)
			{
				if (intType >= 0)
				{
					for (x = 0; x <= cmbLabelType.Items.Count - 1; x++)
					{
						if (cmbLabelType.ItemData(x) == intType)
						{
							cmbLabelType.SelectedIndex = x;
						}
					}
					// x
				}
			}
			if (Strings.Trim(modPPGN.Statics.Open1Title) != string.Empty)
			{
				chkOpen1.Text = modPPGN.Statics.Open1Title;
			}
			if (Strings.Trim(modPPGN.Statics.Open2Title) != string.Empty)
			{
				chkOpen2.Text = modPPGN.Statics.Open2Title;
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuPrintPreview_Click(object sender, System.EventArgs e)
		{
			string strResponse = "";
			string minaccount;
			string maxaccount;
			int intHowToPrint;
			int intTypePrint = 0;
			int intOrdBy = 0;
			// vbPorter upgrade warning: bytOptions As byte	OnWrite(short, bool)
			byte bytOptions;
			string strOrder = "";
			string strYear;
			string strTax = "";
			float sngTax = 0;
			bool boolElim = false;
			string strSQL = "";
			// vbPorter upgrade warning: sngAdjust As Variant --> As double	OnWrite(double, short)
			double sngAdjust = 0;
			int intLabelStart = 0;
			// vbPorter upgrade warning: intRows As Variant --> As int
			int intRows = 0, intCols = 0;
			try
			{
				// On Error GoTo ErrorHandler
				intHowToPrint = 0;
				bytOptions = 0;
				minaccount = " ";
				maxaccount = " ";
				strYear = " ";
				if (chkEliminate.Checked & chkEliminate.Enabled)
				{
					boolElim = true;
				}
				else
				{
					boolElim = false;
				}
				//FC:FINAL:MSH - i.issue #1410: checking SelectedIndex of the wrong comboBox
				//if (cmbLabelType.SelectedIndex == 0)
				if (cmbtLabelType.SelectedIndex == 0)
				{
					intTypePrint = 1;
				}
				//FC:FINAL:MSH - i.issue #1410: checking SelectedIndex of the wrong comboBox
				//else if (cmbLabelType.SelectedIndex == 1)
				else if (cmbtLabelType.SelectedIndex == 1)
				{
					intTypePrint = 2;
				}
					//FC:FINAL:MSH - i.issue #1410: checking SelectedIndex of the wrong comboBox
					//else if (cmbLabelType.SelectedIndex == 2)
					else if (cmbtLabelType.SelectedIndex == 2)
				{
					intTypePrint = 3;
				}
						//FC:FINAL:MSH - i.issue #1410: checking SelectedIndex of the wrong comboBox
						//else if (cmbLabelType.SelectedIndex == 3)
						else if (cmbtLabelType.SelectedIndex == 3)
				{
					intTypePrint = 4;
				}
							//FC:FINAL:MSH - i.issue #1410: checking SelectedIndex of the wrong comboBox
							//else if (cmbLabelType.SelectedIndex == 4)
							else if (cmbtLabelType.SelectedIndex == 4)
				{
					intTypePrint = 5;
				}
								//FC:FINAL:MSH - i.issue #1410: checking SelectedIndex of the wrong comboBox
								//else if (cmbLabelType.SelectedIndex == 5)
								else if (cmbtLabelType.SelectedIndex == 5)
				{
					intTypePrint = 6;
				}
				//FC:FINAL:MSH - i.issue #1410: checking SelectedIndex of the wrong comboBox
				//if (cmbLabelType.SelectedIndex == 3 || (chkETax.CheckState == CheckState.Checked))
				if (cmbtLabelType.SelectedIndex == 3 || (chkETax.CheckState == CheckState.Checked))
				{
					//FC:FINAL:MSH - i.issue #1410: checking SelectedIndex of the wrong comboBox
					//if (cmbLabelType.SelectedIndex == 3)
					if (cmbtLabelType.SelectedIndex == 3)
					{
						strYear = Interaction.InputBox("Enter the tax year description" + "\r\n" + "This may be in the form 2003, 03/04 or FY03", "Enter description", null);
						strYear = Strings.Mid(strYear, 1, 5);
					}
					if (chkETax.CheckState == CheckState.Checked)
					{
						strTax = Interaction.InputBox("Enter the Tax Rate Per 1000", "Enter Tax Rate");
						sngTax = FCConvert.ToSingle(Conversion.Val(strTax));
						if (sngTax <= 0)
							return;
					}
				}
				if (cmbOrder.SelectedIndex == 0)
				{
					strOrder = "Account";
					intOrdBy = 1;
				}
				else if (cmbOrder.SelectedIndex == 1)
				{
					strOrder = "Name";
					intOrdBy = 2;
				}
				else if (cmbOrder.SelectedIndex == 2)
				{
					strOrder = "Street";
					intOrdBy = 3;
				}
				else if (cmbOrder.SelectedIndex == 3)
				{
					strOrder = "Zip";
					intOrdBy = 4;
				}
				//FC:FINAL:MSH - i.issue #819: checking SelectedIndex of the wrong comboBox
				//if (cmbOrder.SelectedIndex == 0)
				if (cmbtPrint.SelectedIndex == 0)
				{
					intHowToPrint = 1;
				}
				else if (cmbtPrint.SelectedIndex == 2)
				{
					if (cmbOrder.SelectedIndex == 0)
					{
						if (Conversion.Val(txtRange[0].Text) < 1)
						{
							MessageBox.Show("This is an invalid minimum account", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
						minaccount = FCConvert.ToString(Conversion.Val(txtRange[0].Text));
					}
					else
					{
						minaccount = Strings.Trim(txtRange[0].Text);
					}
					if (cmbOrder.SelectedIndex == 0)
					{
						if ((Conversion.Val(txtRange[1].Text) < 1) || (Conversion.Val(txtRange[1].Text) < Conversion.Val(minaccount)))
						{
							MessageBox.Show("This is an invalid range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
						maxaccount = FCConvert.ToString(Conversion.Val(txtRange[1].Text));
					}
					else
					{
						if (Strings.Trim(txtRange[1].Text) == string.Empty)
						{
							MessageBox.Show("This is an invalid range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
						maxaccount = Strings.Trim(txtRange[1].Text + "z");
					}
					intHowToPrint = 2;
				}
				else if (cmbtPrint.SelectedIndex == 1)
				{
					// must get individual accounts to save
					modPPGN.Statics.CancelledIt = false;
					frmGetAccountsToPrint.InstancePtr.Show(FormShowEnum.Modal);
					if (modPPGN.Statics.CancelledIt)
						return;
					intHowToPrint = 3;
				}
				// set options
				if ((chkAccount.CheckState == CheckState.Checked) && (chkAccount.Enabled))
				{
					bytOptions = Convert.ToByte(bytOptions | ACCT_CODE);
				}
				if ((chkOpen1.CheckState == CheckState.Checked) && (chkOpen1.Enabled))
				{
					bytOptions = Convert.ToByte(bytOptions | OPEN1_CODE);
				}
				if ((chkAddress.CheckState == CheckState.Checked) && (chkAddress.Enabled))
				{
					bytOptions = Convert.ToByte(bytOptions | ADDR_CODE);
				}
				if ((chkValue.CheckState == CheckState.Checked) && (chkValue.Enabled))
				{
					bytOptions = Convert.ToByte(bytOptions | VALUE_CODE);
				}
				if ((chkETax.CheckState == CheckState.Checked) && (chkETax.Enabled))
				{
					bytOptions = Convert.ToByte(bytOptions | ESTTAX_CODE);
				}
				if ((chkOpen2.CheckState == CheckState.Checked) && (chkOpen2.Enabled))
				{
					bytOptions = Convert.ToByte(bytOptions | OPEN2_CODE);
				}
				if ((ChkLocation.CheckState == CheckState.Checked) && (ChkLocation.Enabled))
				{
					bytOptions = Convert.ToByte(bytOptions | LOC_CODE);
				}
				// not sure what declaration labels will be
				if (chkAdjust.CheckState == CheckState.Checked)
				{
					sngAdjust = Conversion.Val(txtAlignment.Text);
				}
				else
				{
					sngAdjust = 0;
				}
				if (cmbLabelType.ItemData(cmbLabelType.SelectedIndex) != modLabels.CNSTLBLTYPEASSESSMENT)
				{
					// don't want to save if it is just assessment labels
					modRegistry.SaveRegistryKey("PPLastLabelType", FCConvert.ToString(cmbLabelType.ItemData(cmbLabelType.SelectedIndex)));
				}
				if (framLaserOptions.Visible == true)
				{
					if (chkChooseLabelStart.CheckState == CheckState.Checked)
					{
						int intLabIndex = 0;
						intLabIndex = labLabel.Get_IndexFromID(cmbLabelType.ItemData(cmbLabelType.SelectedIndex));
						// Select Case cmbLabelType.ItemData(cmbLabelType.ListIndex)
						if (intLabIndex >= 0)
						{
							intCols = labLabel.Get_LabelsWide(intLabIndex);
							intRows = FCConvert.ToInt32(labLabel.Get_LabelsHigh(intLabIndex));
						}
						else
						{
							intCols = 2;
							intRows = 10;
						}
						// Case 3
						// intRows = 10
						// intCols = 3
						// Case 4
						// intRows = 10
						// intCols = 2
						// Case 5
						// intRows = 7
						// intCols = 2
						// Case 6
						// intRows = 5
						// intCols = 2
						// Case Else
						// shouldn't get here
						// intRows = 10
						// intCols = 2
						// End Select
						intLabelStart = frmChooseLabelPosition.InstancePtr.Init(intRows, intCols);
						if (intLabelStart < 0)
							return;
					}
					// If cmbLabelType.ItemData(cmbLabelType.ListIndex) <> CNSTLBLTYPEDYMO30252 And cmbLabelType.ItemData(cmbLabelType.ListIndex) <> CNSTLBLTYPEDYMO30256 Then
					rptCustomLabels.InstancePtr.Init(boolElim, FCConvert.ToInt16(intHowToPrint), FCConvert.ToInt16(intTypePrint), FCConvert.ToInt16(intOrdBy), FCConvert.ToInt16(cmbLabelType.ItemData(cmbLabelType.SelectedIndex)), bytOptions, minaccount, maxaccount, strYear, sngTax, "", FCConvert.ToSingle(sngAdjust), FCConvert.ToInt32(Conversion.Val(txtNumDuplicates.Text)), false, FCConvert.ToInt16(intLabelStart));
					// Else
					// Call rptCustomLabels.Init(boolElim, intHowToPrint, intTypePrint, intOrdBy, cmbLabelType.ItemData(cmbLabelType.ListIndex), bytOptions, minaccount, maxaccount, strYear, sngTax, , sngAdjust, Val(txtNumDuplicates.Text), , intLabelStart)
					// End If
				}
				else
				{
					rptCustomLabels.InstancePtr.Init(boolElim, FCConvert.ToInt16(intHowToPrint), FCConvert.ToInt16(intTypePrint), FCConvert.ToInt16(intOrdBy), FCConvert.ToInt16(cmbLabelType.ItemData(cmbLabelType.SelectedIndex)), bytOptions, minaccount, maxaccount, strYear, sngTax, FCGlobal.Printer.DeviceName, FCConvert.ToSingle(sngAdjust), FCConvert.ToInt32(Conversion.Val(txtNumDuplicates.Text)), false, FCConvert.ToInt16(intLabelStart));
				}
				Close();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("error number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Label Printing form", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void OptLabelType_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (cmbtLabelType.SelectedIndex == 5)
			{
				chkAccount.Enabled = true;
				chkOpen1.Enabled = true;
				chkOpen2.Enabled = true;
				ChkLocation.Enabled = true;
				chkAddress.Enabled = true;
				chkValue.Enabled = true;
				chkETax.Enabled = true;
				cmbLabelType.Enabled = true;
			}
			else
			{
				chkAccount.Enabled = false;
				chkOpen1.Enabled = false;
				chkOpen2.Enabled = false;
				ChkLocation.Enabled = false;
				chkAddress.Enabled = false;
				chkValue.Enabled = false;
				chkETax.Enabled = false;
				chkEliminate.Enabled = true;
				cmbLabelType.Enabled = true;
			}
			if (cmbtLabelType.SelectedIndex == 3)
			{
				chkETax.Enabled = true;
				chkEliminate.Enabled = false;
				cmbLabelType.SelectedIndex = intAssessIndex;
				cmbLabelType.Enabled = false;
			}
			if (Index != 3)
			{
				if (cmbLabelType.SelectedIndex > 0)
				{
					if (cmbLabelType.ItemData(cmbLabelType.SelectedIndex) == 8)
					{
						// assessment labels but didn't choose the assessment format
						MessageBox.Show("The current label type is assessment labels.  This may not be compatible with the label format you have just selected.", "Incompatible format", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
			}
		}

		private void OptLabelType_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbLabelType.SelectedIndex;
			OptLabelType_CheckedChanged(index, sender, e);
		}

		private void OptPrint_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
				case 1:
					{
						txtRange[0].Visible = false;
						txtRange[1].Visible = false;
						lblTo.Visible = false;
						break;
					}
				case 2:
					{
						txtRange[0].Visible = true;
						txtRange[1].Visible = true;
						lblTo.Visible = true;
						break;
					}
			}
			//end switch
		}

		private void OptPrint_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbtPrint.SelectedIndex;
			OptPrint_CheckedChanged(index, sender, e);
		}
	}
}
