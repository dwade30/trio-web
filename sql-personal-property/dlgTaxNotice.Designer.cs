﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for dlgTaxNotice.
	/// </summary>
	partial class dlgTaxNotice : BaseForm
	{
		public fecherFoundation.FCComboBox cmbOrder;
		public fecherFoundation.FCLabel lblOrder;
		public fecherFoundation.FCComboBox cmbaccounts;
		public fecherFoundation.FCLabel lblaccounts;
		public fecherFoundation.FCComboBox cmbFormat;
		public fecherFoundation.FCLabel lblFormat;
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> chkInclude;
		public fecherFoundation.FCTextBox txtRate;
		public fecherFoundation.FCTextBox txtYear;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCCheckBox chkInclude_3;
		public fecherFoundation.FCCheckBox chkInclude_2;
		public fecherFoundation.FCCheckBox chkInclude_1;
		public fecherFoundation.FCCheckBox chkInclude_0;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(dlgTaxNotice));
            this.cmbOrder = new fecherFoundation.FCComboBox();
            this.lblOrder = new fecherFoundation.FCLabel();
            this.cmbaccounts = new fecherFoundation.FCComboBox();
            this.lblaccounts = new fecherFoundation.FCLabel();
            this.cmbFormat = new fecherFoundation.FCComboBox();
            this.lblFormat = new fecherFoundation.FCLabel();
            this.txtRate = new fecherFoundation.FCTextBox();
            this.txtYear = new fecherFoundation.FCTextBox();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.chkInclude_3 = new fecherFoundation.FCCheckBox();
            this.chkInclude_2 = new fecherFoundation.FCCheckBox();
            this.chkInclude_1 = new fecherFoundation.FCCheckBox();
            this.chkInclude_0 = new fecherFoundation.FCCheckBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuContinue = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.btnContinue = new fecherFoundation.FCButton();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnContinue)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Size = new System.Drawing.Size(651, 66);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.btnContinue);
            this.ClientArea.Controls.Add(this.cmbOrder);
            this.ClientArea.Controls.Add(this.lblOrder);
            this.ClientArea.Controls.Add(this.txtRate);
            this.ClientArea.Controls.Add(this.txtYear);
            this.ClientArea.Controls.Add(this.cmbaccounts);
            this.ClientArea.Controls.Add(this.lblaccounts);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.cmbFormat);
            this.ClientArea.Controls.Add(this.lblFormat);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(651, 410);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(651, 60);
            this.TopPanel.TabIndex = 0;
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(204, 30);
            this.HeaderText.Text = "Taxpayer Notices";
            // 
            // cmbOrder
            // 
            this.cmbOrder.Items.AddRange(new object[] {
            "Account",
            "Name",
            "Location"});
            this.cmbOrder.Location = new System.Drawing.Point(167, 90);
            this.cmbOrder.Name = "cmbOrder";
            this.cmbOrder.Size = new System.Drawing.Size(182, 40);
            this.cmbOrder.TabIndex = 3;
            this.cmbOrder.Text = "Account";
            // 
            // lblOrder
            // 
            this.lblOrder.AutoSize = true;
            this.lblOrder.Location = new System.Drawing.Point(30, 104);
            this.lblOrder.Name = "lblOrder";
            this.lblOrder.Size = new System.Drawing.Size(72, 15);
            this.lblOrder.TabIndex = 2;
            this.lblOrder.Text = "ORDER BY";
            // 
            // cmbaccounts
            // 
            this.cmbaccounts.Items.AddRange(new object[] {
            "All",
            "Range",
            "Specific Accounts"});
            this.cmbaccounts.Location = new System.Drawing.Point(167, 150);
            this.cmbaccounts.Name = "cmbaccounts";
            this.cmbaccounts.Size = new System.Drawing.Size(182, 40);
            this.cmbaccounts.TabIndex = 5;
            this.cmbaccounts.Text = "All";
            // 
            // lblaccounts
            // 
            this.lblaccounts.AutoSize = true;
            this.lblaccounts.Location = new System.Drawing.Point(30, 164);
            this.lblaccounts.Name = "lblaccounts";
            this.lblaccounts.Size = new System.Drawing.Size(77, 15);
            this.lblaccounts.TabIndex = 4;
            this.lblaccounts.Text = "ACCOUNTS";
            // 
            // cmbFormat
            // 
            this.cmbFormat.Items.AddRange(new object[] {
            "Free Form",
            "Assessment"});
            this.cmbFormat.Location = new System.Drawing.Point(167, 30);
            this.cmbFormat.Name = "cmbFormat";
            this.cmbFormat.Size = new System.Drawing.Size(182, 40);
            this.cmbFormat.TabIndex = 1;
            this.cmbFormat.Text = "Assessment";
            this.cmbFormat.SelectedIndexChanged += new System.EventHandler(this.cmbFormat_SelectedIndexChanged);
            // 
            // lblFormat
            // 
            this.lblFormat.AutoSize = true;
            this.lblFormat.Location = new System.Drawing.Point(30, 44);
            this.lblFormat.Name = "lblFormat";
            this.lblFormat.Size = new System.Drawing.Size(60, 15);
            this.lblFormat.Text = "FORMAT";
            // 
            // txtRate
            // 
            this.txtRate.BackColor = System.Drawing.SystemColors.Window;
            this.txtRate.Location = new System.Drawing.Point(167, 268);
            this.txtRate.Name = "txtRate";
            this.txtRate.Size = new System.Drawing.Size(114, 40);
            this.txtRate.TabIndex = 9;
            // 
            // txtYear
            // 
            this.txtYear.BackColor = System.Drawing.SystemColors.Window;
            this.txtYear.Location = new System.Drawing.Point(167, 208);
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(114, 40);
            this.txtYear.TabIndex = 7;
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.chkInclude_3);
            this.Frame2.Controls.Add(this.chkInclude_2);
            this.Frame2.Controls.Add(this.chkInclude_1);
            this.Frame2.Controls.Add(this.chkInclude_0);
            this.Frame2.Location = new System.Drawing.Point(383, 30);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(238, 218);
            this.Frame2.TabIndex = 10;
            this.Frame2.Text = "Include";
            // 
            // chkInclude_3
            // 
            this.chkInclude_3.Checked = true;
            this.chkInclude_3.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
            this.chkInclude_3.Location = new System.Drawing.Point(20, 171);
            this.chkInclude_3.Name = "chkInclude_3";
            this.chkInclude_3.Size = new System.Drawing.Size(136, 27);
            this.chkInclude_3.TabIndex = 3;
            this.chkInclude_3.Text = "Existing Notice";
            this.chkInclude_3.CheckedChanged += new System.EventHandler(this.chkInclude_CheckedChanged);
            // 
            // chkInclude_2
            // 
            this.chkInclude_2.Checked = true;
            this.chkInclude_2.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
            this.chkInclude_2.Location = new System.Drawing.Point(20, 124);
            this.chkInclude_2.Name = "chkInclude_2";
            this.chkInclude_2.Size = new System.Drawing.Size(174, 27);
            this.chkInclude_2.TabIndex = 2;
            this.chkInclude_2.Text = "Exemption Amounts";
            this.chkInclude_2.CheckedChanged += new System.EventHandler(this.chkInclude_CheckedChanged);
            // 
            // chkInclude_1
            // 
            this.chkInclude_1.Checked = true;
            this.chkInclude_1.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
            this.chkInclude_1.Location = new System.Drawing.Point(20, 77);
            this.chkInclude_1.Name = "chkInclude_1";
            this.chkInclude_1.Size = new System.Drawing.Size(132, 27);
            this.chkInclude_1.TabIndex = 1;
            this.chkInclude_1.Text = "Estimated Tax";
            this.chkInclude_1.CheckedChanged += new System.EventHandler(this.chkInclude_CheckedChanged);
            // 
            // chkInclude_0
            // 
            this.chkInclude_0.Checked = true;
            this.chkInclude_0.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
            this.chkInclude_0.Location = new System.Drawing.Point(20, 30);
            this.chkInclude_0.Name = "chkInclude_0";
            this.chkInclude_0.Size = new System.Drawing.Size(208, 27);
            this.chkInclude_0.Text = "Totally Exempt Accounts";
            this.chkInclude_0.CheckedChanged += new System.EventHandler(this.chkInclude_CheckedChanged);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 282);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(70, 15);
            this.Label2.TabIndex = 8;
            this.Label2.Text = "TAX RATE";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 222);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(64, 15);
            this.Label1.TabIndex = 6;
            this.Label1.Text = "TAX YEAR";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuContinue,
            this.mnuSepar,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuContinue
            // 
            this.mnuContinue.Index = 0;
            this.mnuContinue.Name = "mnuContinue";
            this.mnuContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuContinue.Text = "Continue";
            this.mnuContinue.Click += new System.EventHandler(this.mnuContinue_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 1;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // btnContinue
            // 
            this.btnContinue.AppearanceKey = "acceptButton";
            this.btnContinue.Location = new System.Drawing.Point(30, 338);
            this.btnContinue.Name = "btnContinue";
            this.btnContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnContinue.Size = new System.Drawing.Size(100, 48);
            this.btnContinue.Text = "Continue";
            this.btnContinue.Click += new System.EventHandler(this.mnuContinue_Click);
            // 
            // dlgTaxNotice
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(651, 536);
            this.FillColor = 0;
            this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "dlgTaxNotice";
            this.ShowInTaskbar = false;
            this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
            this.Text = "Taxpayer Notices";
            this.Load += new System.EventHandler(this.dlgTaxNotice_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.dlgTaxNotice_KeyDown);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnContinue)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton btnContinue;
	}
}
