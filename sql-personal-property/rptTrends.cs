﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptTrends.
	/// </summary>
	public partial class rptTrends : BaseSectionReport
	{
		public rptTrends()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ActiveReport1";
		}

		public static rptTrends InstancePtr
		{
			get
			{
				return (rptTrends)Sys.GetInstance(typeof(rptTrends));
			}
		}

		protected rptTrends _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsRpt?.Dispose();
                clsRpt = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptTrends	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		string strCode;
		string strDesc;
		string strAmount;
		string strTable;
		clsDRWrapper clsRpt = new clsDRWrapper();

		public void Init()
		{
			try
			{
				// On Error GoTo ErrorHandler
				// trends
				strTable = "TrendCapRates";
				strCode = "[Year]";
				strAmount = "Trend";
				strDesc = "CapRate";
				lblTitle.Text = "Trending / Cap Rates";
				lblCode.Text = "Year";
				lblAmount.Text = "Trend";
				lblDescription.Text = "Cap Rate";
				this.Name = lblTitle.Text;
				clsRpt.OpenRecordset("select * from " + strTable + " order by " + strCode, modPPGN.strPPDatabase);
				if (clsRpt.EndOfFile())
				{
					MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					this.Close();
					return;
				}
				frmReportViewer.InstancePtr.Init(rptObj: this, boolAllowEmail: true, strAttachmentName: "Trends");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Init", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsRpt.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			lblMuniname.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!clsRpt.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
				txtCode.Text = FCConvert.ToString(clsRpt.Get_Fields("Year"));
				txtAmount.Text = FCConvert.ToString(clsRpt.Get_Fields(strAmount));
				txtDesc.Text = FCConvert.ToString(clsRpt.Get_Fields(strDesc));
				clsRpt.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		
	}
}
