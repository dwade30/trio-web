﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;
using fecherFoundation.VisualBasicLayer;
using System.IO;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmPrintSummary.
	/// </summary>
	public partial class frmPrintSummary : BaseForm
	{
		public frmPrintSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPrintSummary InstancePtr
		{
			get
			{
				return (frmPrintSummary)Sys.GetInstance(typeof(frmPrintSummary));
			}
		}

		protected frmPrintSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int LineCount;
		int PageCount;
		bool bolQuitButton;
		int FirstAccount;
		int LastAccount;
		string OverrideCode = "";
		int OverrideValue;
		int[] LeasedOriginalCost = new int[9 + 1];
		// vbPorter upgrade warning: LeasedCalculatedCost As int	OnWriteFCConvert.ToDouble(
		int[] LeasedCalculatedCost = new int[9 + 1];
		int[] ItemizedOriginalCost = new int[9 + 1];
		// vbPorter upgrade warning: ItemizedCalculatedCost As int	OnWriteFCConvert.ToDouble(
		int[] ItemizedCalculatedCost = new int[9 + 1];
		// vbPorter upgrade warning: ItemizedBETE As int	OnWriteFCConvert.ToDouble(
		private int[] ItemizedBETE = new int[9 + 1];
		// vbPorter upgrade warning: LeasedBETE As int	OnWriteFCConvert.ToDouble(
		private int[] LeasedBETE = new int[9 + 1];
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsDRWrapper rsM = new clsDRWrapper();
		clsDRWrapper rsM_AutoInitialized;

		clsDRWrapper rsM
		{
			get
			{
				if (rsM_AutoInitialized == null)
				{
					rsM_AutoInitialized = new clsDRWrapper();
				}
				return rsM_AutoInitialized;
			}
			set
			{
				rsM_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsDRWrapper rsI = new clsDRWrapper();
		clsDRWrapper rsI_AutoInitialized;

		clsDRWrapper rsI
		{
			get
			{
				if (rsI_AutoInitialized == null)
				{
					rsI_AutoInitialized = new clsDRWrapper();
				}
				return rsI_AutoInitialized;
			}
			set
			{
				rsI_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsDRWrapper rsL = new clsDRWrapper();
		clsDRWrapper rsL_AutoInitialized;

		clsDRWrapper rsL
		{
			get
			{
				if (rsL_AutoInitialized == null)
				{
					rsL_AutoInitialized = new clsDRWrapper();
				}
				return rsL_AutoInitialized;
			}
			set
			{
				rsL_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsDRWrapper rsV = new clsDRWrapper();
		clsDRWrapper rsV_AutoInitialized;

		clsDRWrapper rsV
		{
			get
			{
				if (rsV_AutoInitialized == null)
				{
					rsV_AutoInitialized = new clsDRWrapper();
				}
				return rsV_AutoInitialized;
			}
			set
			{
				rsV_AutoInitialized = value;
			}
		}

		string strSQL = "";
		// vbPorter upgrade warning: lngTotalPrintL As int	OnWrite(short, double)
		int lngTotalPrintL;
		// vbPorter upgrade warning: lngTotalPrintI As int	OnWrite(short, double)
		int lngTotalPrintI;
		int lngTotalPrintBETE;
		int lngTotalPrintTot;
		int lngTotalPrintFinal;
		int lngORTotal;
		int lngORCalc;
		string strField;
		bool boolReim;
		double Ratio;
		private int lngCurBETEYear;

		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
            //this.ShowWait(true);
            this.Enabled = false;
            this.LockCloseDuringLongProcess();
            FCUtils.StartTask(this, () =>
            {
                try
                {
                    clsDRWrapper clsSave = new clsDRWrapper();
                    string strLowValue = "";
                    string strHighValue = "";
                    // vbPorter upgrade warning: intWhichOrder As string	OnWriteFCConvert.ToInt16(
                    string intWhichOrder = "";
                    string strMasterJoin;
                    strMasterJoin = modPPGN.GetMasterJoin();
                    string strMasterJoinJoin;
                    strMasterJoinJoin = modPPGN.GetMasterJoinForJoin();
                    FCFileSystem.FileClose();
                    clsSave.Execute("update status set batchcalculated = '" + FCConvert.ToString(DateTime.Now) + "'", "twpp0000.vb1");
                    lngTotalPrintL = 0;
                    lngTotalPrintI = 0;
                    lngTotalPrintBETE = 0;
                    lngTotalPrintTot = 0;
                    lngTotalPrintFinal = 0;
                    lngORTotal = 0;
                    lngORCalc = 0;
                    // 
                    bolQuitButton = false;
                    strField = "";
                    if (cmbSequence.SelectedIndex == 0)
                    {
                        strField = "Account";
                    }
                    else if (cmbSequence.SelectedIndex == 1)
                    {
                        strField = "Name";
                    }
                    else if (cmbSequence.SelectedIndex == 2)
                    {
                        strField = "Street";
                    }
                    else if (cmbSequence.SelectedIndex == 3)
                    {
                        // business code
                        strField = "businesscode";
                    }
                    else if (cmbSequence.SelectedIndex == 4)
                    {
                        // open1
                        strField = "open1";
                    }
                    else if (cmbSequence.SelectedIndex == 5)
                    {
                        // open2
                        strField = "open2";
                    }
                    if (strField == "")
                    {
                        MessageBox.Show("You must choose the order to process the listing.", "Order", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    //FC:FINAL:MSH - i.issue #1330: incorrect checking of the file existing
                    //FileInfo fs = new FileInfo("PrintSum.txt");
                    //if (fs.Exists)
                    //{
                    //    fs.Delete();
                    //}
                    if (cmbRange.SelectedIndex == 0)
                    {
                        strSQL = strMasterJoin + " where not deleted = 1 ORDER BY " + strField;
                        if (cmbSequence.SelectedIndex == 2)
                            strSQL += ", StreetNumber";
                    }
                    else
                    {
                        if (strField != "Account" && strField != "businesscode")
                        {
                            if (Strings.CompareString(txtLow.Text, "<=", txtHigh.Text))
                            {
                                if (txtLow.Text != "" && txtHigh.Text != "")
                                {
                                    strSQL = strMasterJoin + " WHERE not deleted = 1 and " + strField + " >= '" + txtLow.Text + "' AND " + strField + " <= '" + txtHigh.Text + "zzz' ORDER BY " + strField;
                                }
                                else if (txtLow.Text == "" && txtHigh.Text != "")
                                {
                                    strSQL = strMasterJoin + " WHERE not deleted = 1 and " + strField + " =< '" + txtHigh.Text + "zzz' ORDER BY " + strField;
                                }
                                else if (txtLow.Text != "" && txtHigh.Text == "")
                                {
                                    strSQL = strMasterJoin + " WHERE not deleted = 1  and " + strField + " => '" + txtLow.Text + "' ORDER BY " + strField;
                                }
                                else if (txtLow.Text == "" && txtHigh.Text == "")
                                {
                                    strSQL = strMasterJoin + " where not deleted = 1  ORDER BY " + strField;
                                }
                                if (cmbSequence.SelectedIndex == 2)
                                    strSQL += ", StreetNumber";
                            }
                            else
                            {
                                MessageBox.Show("From value is greater than To value", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            }
                        }
                        else
                        {
                            if (!Information.IsNumeric(txtLow.Text) && !Information.IsNumeric(txtHigh.Text))
                            {
                                MessageBox.Show("Invalid Range", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            }
                            if (Conversion.Val(txtLow.Text) <= Conversion.Val(txtHigh.Text))
                            {
                                if (txtLow.Text != "" && txtHigh.Text != "")
                                {
                                    strSQL = strMasterJoin + " WHERE not deleted = 1 and " + strField + " >= " + FCConvert.ToString(Conversion.Val(txtLow.Text)) + " AND " + strField + " <= " + FCConvert.ToString(Conversion.Val(txtHigh.Text)) + " ORDER BY " + strField;
                                }
                                else if (txtLow.Text == "" && txtHigh.Text != "")
                                {
                                    strSQL = strMasterJoin + " WHERE not deleted = 1 and " + strField + " =< " + FCConvert.ToString(Conversion.Val(txtHigh.Text)) + " ORDER BY " + strField;
                                }
                                else if (txtLow.Text != "" && txtHigh.Text == "")
                                {
                                    strSQL = strMasterJoin + " WHERE not deleted = 1 and " + strField + " => " + FCConvert.ToString(Conversion.Val(txtLow.Text)) + " ORDER BY " + strField;
                                }
                                else if (txtLow.Text == "" && txtHigh.Text == "")
                                {
                                    strSQL = strMasterJoin + " where not deleted = 1 ORDER BY " + strField;
                                }
                            }
                            else
                            {
                                MessageBox.Show("From value is greater than To value", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            }
                        }
                    }
                    if (cmbType.SelectedIndex == 1)
                    {
                        strLowValue = txtLow.Text;
                        strHighValue = txtHigh.Text;
                        if (cmbSequence.SelectedIndex == 0)
                        {
                            // account
                            intWhichOrder = FCConvert.ToString(0);
                        }
                        else if (cmbSequence.SelectedIndex == 1)
                        {
                            // name
                            intWhichOrder = FCConvert.ToString(1);
                            strHighValue += "zzz";
                        }
                        else if (cmbSequence.SelectedIndex == 2)
                        {
                            // location
                            intWhichOrder = FCConvert.ToString(2);
                        }
                        else if (cmbSequence.SelectedIndex == 3)
                        {
                            // open1
                            intWhichOrder = FCConvert.ToString(3);
                            strHighValue += "zzz";
                        }
                        else
                        {
                            // open2
                            intWhichOrder = FCConvert.ToString(4);
                            strHighValue += "zzz";
                        }
                        clsSave.Execute("update status set calculated = '" + FCConvert.ToString(DateTime.Now) + "'", "twpp0000.vb1");
                        if (cmbRange.SelectedIndex != 1)
                        {
                            modGlobalFunctions.AddCYAEntry_8("PP", "Batch Calculation");
                        }
                        else
                        {
                            modGlobalFunctions.AddCYAEntry_242("PP", "Batch Calculation", "Range " + strField, txtLow.Text, txtHigh.Text);
                        }
                        rptCalculation.InstancePtr.Init(0, 0, strLowValue, strHighValue, intWhichOrder);
                        return;
                    }
                    FCUtils.EraseSafe(ItemizedOriginalCost);
                    FCUtils.EraseSafe(ItemizedCalculatedCost);
                    FCUtils.EraseSafe(ItemizedBETE);
                    FCUtils.EraseSafe(LeasedBETE);
                    FCUtils.EraseSafe(LeasedOriginalCost);
                    FCUtils.EraseSafe(LeasedCalculatedCost);
                    if (cmbRange.SelectedIndex != 0)
                    {
                        modGlobalFunctions.AddCYAEntry_242("PP", "Batch Calculation", "Range " + strField, txtLow.Text, txtHigh.Text);
                    }
                    else
                    {
                        modGlobalFunctions.AddCYAEntry_8("PP", "Batch Calculation");
                    }
                    //FC:FINAL:BBE - FCGrid.Clear() will be clear everywhere and everything, optional parameters
                    //vsLeased.Clear(FCGrid.ClearWhereSettings.flexClearEverywhere, FCGrid.ClearWhatSettings.flexClearEverything);
                    //vsItemized.Clear(FCGrid.ClearWhereSettings.flexClearEverywhere, FCGrid.ClearWhatSettings.flexClearEverything);
                    //vsDisplay.Clear(FCGrid.ClearWhereSettings.flexClearEverywhere, FCGrid.ClearWhatSettings.flexClearEverything);
                    //vsValuations.Clear(FCGrid.ClearWhereSettings.flexClearEverywhere, FCGrid.ClearWhatSettings.flexClearEverything);
                    vsLeased.Clear();
                    vsItemized.Clear();
                    vsDisplay.Clear();
                    vsValuations.Clear();
                    // Get Data From Cost Files
                    string strP;
                    strP = FCFileSystem.CurDir();
                    if (Strings.Right(strP, 1) != "\\")
                        strP += "\\";
                    //FC:FINAL:MSH - i.issue #1330: remove file if exists
                    if (File.Exists(strP + "PrintSum.txt"))
                    {
                        File.Delete(strP + "PrintSum.txt");
                    }
                    rsM.OpenRecordset(strSQL, modPPGN.strPPDatabase);
                    GetCostData();
                    modPPCalculate.GetCapRates();
                    // 
                    if (rsM.EndOfFile() != true && rsM.BeginningOfFile() != true)
                    {
                        rsM.MoveLast();
                        rsM.MoveFirst();
                        // matthew
                        // Open RegularPrinter For Output As #40
                        FCFileSystem.FileOpen(40, "PrintSum.txt", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
                        while (!rsM.EndOfFile())
                        {
                            ////Application.DoEvents();
                            // TODO Get_Fields: Check the table for the column [ACCOUNT] and replace with corresponding Get_Field method
                            modPPGN.Statics.CurrentAccount = FCConvert.ToInt32(rsM.Get_Fields("ACCOUNT"));
                            ////Application.DoEvents();
                            if (bolQuitButton == true)
                                return;
                            if (Conversion.Val(rsM.Get_Fields_Boolean("Deleted")) != 0)
                                goto NextRecordTag;
                            // 
                            //FC:FINAL:BBE - FCGrid.Clear() will be clear everywhere and everything, optional parameters
                            //vsLeased.Clear(FCGrid.ClearWhereSettings.flexClearEverywhere, FCGrid.ClearWhatSettings.flexClearEverything);
                            //vsItemized.Clear(FCGrid.ClearWhereSettings.flexClearEverywhere, FCGrid.ClearWhatSettings.flexClearEverything);
                            //vsDisplay.Clear(FCGrid.ClearWhereSettings.flexClearEverywhere, FCGrid.ClearWhatSettings.flexClearEverything);
                            //vsValuations.Clear(FCGrid.ClearWhereSettings.flexClearEverywhere, FCGrid.ClearWhatSettings.flexClearEverything);
                            vsLeased.Clear();
                            vsItemized.Clear();
                            vsDisplay.Clear();
                            vsValuations.Clear();
                            // 
                            // Fill Master Update Fields
                            lblAccountTitle.Visible = true;
                            lblNameTitle.Visible = true;
                            lblLocationTitle.Visible = true;
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            lblAccount.Text = Strings.Format(rsM.Get_Fields("Account"), "00000");
                            if (fecherFoundation.FCUtils.IsNull(rsM.Get_Fields_String("Name")) == false)
                                lblName.Text = FCConvert.ToString(rsM.Get_Fields_String("Name"));
                            // TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
                            if (fecherFoundation.FCUtils.IsNull(rsM.Get_Fields("StreetNumber")) == false)
                                lblLocation.Text = FCConvert.ToString(rsM.Get_Fields("StreetNumber"));
                            if (fecherFoundation.FCUtils.IsNull(rsM.Get_Fields_String("Street")) == false)
                                lblLocationName.Text = FCConvert.ToString(rsM.Get_Fields_String("Street"));
                            //FC:FINAL:RPU:#i1502 - Update labels text to show progress
                            FCUtils.ApplicationUpdate(this);
                            if (fecherFoundation.FCUtils.IsNull(rsM.Get_Fields_String("ORCode")) == false)
                            {
                                OverrideCode = FCConvert.ToString(rsM.Get_Fields_String("ORCode"));
                            }
                            else
                            {
                                OverrideCode = "";
                            }
                            // 
                            // Setup Leased Table
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            strSQL = "SELECT * FROM PPLeased WHERE Account = " + rsM.Get_Fields("Account") + " ORDER BY [Line]";
                            rsL.OpenRecordset(strSQL, modPPGN.strPPDatabase);
                            if (vsLeased.Rows == 1)
                                vsLeased.Rows = 2;
                            // 
                            // Setup Itemized Table
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            strSQL = "SELECT * FROM PPItemized WHERE Account = " + rsM.Get_Fields("Account") + " ORDER BY [Line]";
                            rsI.OpenRecordset(strSQL, modPPGN.strPPDatabase);
                            if (vsItemized.Rows == 1)
                                vsItemized.Rows = 2;
                            // 
                            boolReim = false;
                            CalculateItemized();
                            CalculateLeased();
                            // .Edit
                            if (boolReim)
                            {
                                // .Fields("rbcode") = "Y"
                                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                clsSave.Execute("update ppmaster set rbcode = 'Y' where account = " + rsM.Get_Fields("Account"), modPPGN.strPPDatabase);
                            }
                            else
                            {
                                // .Fields("rbcode") = "N"
                                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                clsSave.Execute("update ppmaster set rbcode = 'N' where account = " + rsM.Get_Fields("Account"), modPPGN.strPPDatabase);
                            }
                            // .Update
                            // 
                            // Setup Valuation Table
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            strSQL = "SELECT * FROM PPValuations WHERE ValueKey = " + rsM.Get_Fields("Account");
                            rsV.OpenRecordset(strSQL, modPPGN.strPPDatabase);
                            OverrideValue = 0;
                            if (rsV.EndOfFile() != true)
                            {
                                rsV.MoveLast();
                                rsV.MoveFirst();
                                OverrideValue = FCConvert.ToInt32((fecherFoundation.FCUtils.IsNull(rsV.Get_Fields_Int32("OverrideAmount")) == false ? rsV.Get_Fields_Int32("OverrideAmount") : 0));
                                // TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
                                rsV.Execute("delete from ppvaluations where valuekey = " + rsM.Get_Fields("account"), modPPGN.strPPDatabase);
                            }
                            rsV.AddNew();
                            SetValuations();
                            // .Edit
                            // .Fields("compvalue") = Val(rsV.Fields("totalitemized") & "") + Val(rsV.Fields("totalleased") & "")
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            clsSave.Execute("update ppmaster set compvalue = " + FCConvert.ToString(Conversion.Val(rsV.Get_Fields_Int32("totalitemized") + "") + Conversion.Val(rsV.Get_Fields_Int32("totalleased") + "")) + " where account = " + rsM.Get_Fields("Account"), modPPGN.strPPDatabase);
                            // .Update
                            // 
                            // Build Display Flex Grid For Valuations
                            BuildDisplay();
                            CheckOverride();
                            Print_Line();
                            NextRecordTag:
                            ;
                            rsM.MoveNext();
                        }
                        // PRINT TOTALS HERE
                        FCFileSystem.PrintLine(40, "");
                        FCFileSystem.PrintLine(40, "TOTALS - ");
                        string resString = "";
                        //FC:FINAL:MSH - i.issue #1330: remove not implemented method Strings.StrP. In VB6 Tab() returns the spaces, number of which calculated from start of the string 
                        //FCFileSystem.Print(40, FCUtils.Tab(17) + Strings.Format(Strings.Format(lngTotalPrintL, "#,##0"), "@@@@@@@@@@"));
                        resString += FCUtils.Tab(17) + Strings.Format(Strings.Format(lngTotalPrintL, "#,##0"), "@@@@@@@@@@");
                        //FCFileSystem.Print(40, FCUtils.Tab(31) + Strings.Format(Strings.Format(lngTotalPrintI, "#,##0"), "@@@@@@@@@@"));
                        resString += FCUtils.Tab(31, resString) + Strings.Format(Strings.Format(lngTotalPrintI, "#,##0"), "@@@@@@@@@@");
                        // Print #40, Tab(44); Format(Format(lngTotalPrintL + lngTotalPrintI, "#,##0"), "@@@@@@@@@@@");
                        //FCFileSystem.Print(40, FCUtils.Tab(44) + Strings.Format(Strings.Format(lngTotalPrintTot, "#,##0"), "@@@@@@@@@@@"));
                        resString += FCUtils.Tab(44, resString) + Strings.Format(Strings.Format(lngTotalPrintTot, "#,##0"), "@@@@@@@@@@@");
                        FCFileSystem.Print(40, resString + FCUtils.Tab(56, resString) + Strings.Format(Strings.Format(lngTotalPrintBETE, "#,##0"), "@@@@@@@@@@@"));
                        FCFileSystem.PrintLine(40, " ");
                        FCFileSystem.PrintLine(40, " ");
                        FCFileSystem.PrintLine(40, "Total Override Amounts Entered         $" + Strings.Format(Strings.Format(lngORTotal, "#,##0"), "@@@@@@@@@@@@"));
                        FCFileSystem.PrintLine(40, "Total Calculated Amounts Overridden    $" + Strings.Format(Strings.Format(lngORCalc, "#,##0"), "@@@@@@@@@@@@"));
                        FCFileSystem.PrintLine(40, "Total After Overrides Applied          $" + Strings.Format(Strings.Format(lngTotalPrintFinal, "#,##0"), "@@@@@@@@@@@@"));
                        FCFileSystem.PrintLine(40, FCConvert.ToString(Convert.ToChar(12)));
                        // Close #40
                        ////Application.DoEvents();
                        FCFileSystem.FileClose();
                        rptPrintSummary.InstancePtr.Init();
                    }
                    else
                    {
                        MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    // DoEvents
                    //FC:FINAL:AM:#3667 - file already closed; this will close the file opened from the report
                    //FCFileSystem.FileClose();
                    // Call rptPrintSummary.Init
                    // MDIParent.Show
                    // Unload frmPrintSummary
                }
                catch (Exception ex)
                {
                    FCFileSystem.FileClose();
                    throw ex;
                }
                finally
                {
                    //this.EndWait();
                    this.Enabled = true;
                    this.UnlockCloseDuringLongProcess();
                }
            });

        }

		public void cmdProcess_Click()
		{
			cmdProcess_Click(cmdProcess, new System.EventArgs());
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			bolQuitButton = true;
			//MDIParent.InstancePtr.Show();
			frmPrintSummary.InstancePtr.Close();
		}

		public void cmdQuit_Click()
		{
			cmdQuit_Click(cmdQuit, new System.EventArgs());
		}

		private void frmPrintSummary_Activated(object sender, System.EventArgs e)
		{
			if (modGNWork.FormExist(this))
				return;
			// 
			modPPGN.Statics.Exiting = false;
			sstPPCalculate.Visible = false;
		}

		private void GetCostData()
		{
			// vbPorter upgrade warning: xx As Variant, short --> As int	OnWriteFCConvert.ToInt16(
			int xx;
			clsDRWrapper rr = new clsDRWrapper();
			rr.OpenRecordset("SELECT * FROM RatioTrends", modPPGN.strPPDatabase);
			rr.MoveLast();
			rr.MoveFirst();
			// 
			for (xx = 1; xx <= 9; xx++)
			{
				// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
				modPPCalculate.Statics.HighPct[xx] = FCConvert.ToInt32(rr.Get_Fields("High"));
				// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
				modPPCalculate.Statics.LowPct[xx] = FCConvert.ToInt32(rr.Get_Fields("Low"));
				modPPCalculate.Statics.Exponent[xx] = rr.Get_Fields_Double("Exponent");
				// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
				modPPCalculate.Statics.DefLife[xx] = FCConvert.ToInt32(rr.Get_Fields("Life"));
				modPPCalculate.Statics.DepMeth[xx] = FCConvert.ToString(rr.Get_Fields_String("SD"));
				// TODO Get_Fields: Check the table for the column [Trend] and replace with corresponding Get_Field method
				modPPCalculate.Statics.TrendCode[xx] = FCConvert.ToString(rr.Get_Fields("Trend"));
				modPPCalculate.Statics.DefDesc[xx] = FCConvert.ToString(rr.Get_Fields_String("Description"));
				rr.MoveNext();
			}
			// xx
		}

		private void CalculateItemized()
		{
			double dblValue = 0;
			modPPCalculate.GetTrendValues();
			if (rsI.EndOfFile() != true && rsI.BeginningOfFile() != true)
			{
				rsI.MoveLast();
				rsI.MoveFirst();
				while (!rsI.EndOfFile())
				{
					////Application.DoEvents();
					modPPCalculate.Statics.Work1 = 0;
					// TODO Get_Fields: Check the table for the column [rb] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsI.Get_Fields("rb")) == "*")
						boolReim = true;
					if (FCConvert.ToString(rsI.Get_Fields_String("Cd")) != "-")
					{
						modPPCalculate.Statics.clCode = rsI.Get_Fields_String("Cd");
						// TODO Get_Fields: Check the table for the column [Quantity] and replace with corresponding Get_Field method
						modPPCalculate.Statics.clQuantity = FCConvert.ToInt32(rsI.Get_Fields("Quantity"));
						// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
						modPPCalculate.Statics.CLYEAR = FCConvert.ToInt32(rsI.Get_Fields("Year"));
						modPPCalculate.Statics.clDPYR = FCConvert.ToInt32(Math.Round(Conversion.Val(rsI.Get_Fields_Int16("DPYR") + "")));
						// TODO Get_Fields: Check the table for the column [SRO] and replace with corresponding Get_Field method
						modPPCalculate.Statics.clSRO = rsI.Get_Fields("SRO");
						modPPCalculate.Statics.clRCYR = FCConvert.ToInt32(Math.Round(Conversion.Val(rsI.Get_Fields_Int16("RCYR") + "")));
						// TODO Get_Fields: Check the table for the column [Cost] and replace with corresponding Get_Field method
						modPPCalculate.Statics.clCost = FCConvert.ToInt32(rsI.Get_Fields("Cost"));
						modPPCalculate.Statics.clPercentGood = Conversion.Val(rsI.Get_Fields_Int16("GD"));
						if (modPPCalculate.Statics.clPercentGood != 0)
							modPPCalculate.Statics.clPercentGood *= 100;
						//FC:FINAL:MSH - i.issue #1330: can't inplicitly convert to double
						modPPCalculate.Statics.clFactor = FCConvert.ToDouble(rsI.Get_Fields_Int16("Fctr")) / 100;
						// 
						if (modPPCalculate.Statics.clCost != 0)
							modPPCalculate.OriginalCostValue();
						// If CustomizeStuff.RoundIndividualItems Then
						// Work1 = Round(Work1, PPRounding)
						// End If
						if (Conversion.Val(modPPCalculate.Statics.clCode) != 0)
						{
							rsI.Edit();
							dblValue = FCConvert.ToDouble(modPPCalculate.Statics.Work1);
							if (Conversion.Val(rsI.Get_Fields_Int32("EXEMPTYEAR") + "") == lngCurBETEYear)
							{
								// Or (Val(.Fields("EXEMPTYEAR") & "") = lngCurBETEYear - 1) Then
								rsI.Set_Fields("value", 0);
								rsI.Set_Fields("BETEEXEMPT", dblValue);
								ItemizedBETE[FCConvert.ToInt32(modPPCalculate.Statics.clCode)] += FCConvert.ToInt32(dblValue);
							}
							else
							{
								rsI.Set_Fields("value", dblValue);
								rsI.Set_Fields("beteexempt", 0);
								ItemizedCalculatedCost[FCConvert.ToInt32(modPPCalculate.Statics.clCode)] += FCConvert.ToInt32(dblValue);
							}
							rsI.Update();
							ItemizedOriginalCost[FCConvert.ToInt32(modPPCalculate.Statics.clCode)] += modPPCalculate.Statics.clCost;
						}
					}
					rsI.MoveNext();
				}
			}
		}

		private void CalculateLeased()
		{
			modPPCalculate.GetTrendValues();
			double dblValue = 0;
			if (rsL.EndOfFile() != true && rsL.BeginningOfFile() != true)
			{
				rsL.MoveLast();
				rsL.MoveFirst();
				while (!rsL.EndOfFile())
				{
					modPPCalculate.Statics.Work1 = 0;
					// TODO Get_Fields: Check the table for the column [rb] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsL.Get_Fields("rb")) == "*")
						boolReim = true;
					if (FCConvert.ToString(rsL.Get_Fields_String("Cd")) != "-")
					{
						modPPCalculate.Statics.clCode = rsL.Get_Fields_String("Cd");
						// TODO Get_Fields: Check the table for the column [Quantity] and replace with corresponding Get_Field method
						modPPCalculate.Statics.clQuantity = FCConvert.ToInt32(rsL.Get_Fields("Quantity"));
						// TODO Get_Fields: Check the table for the column [Cost] and replace with corresponding Get_Field method
						modPPCalculate.Statics.clCost = FCConvert.ToInt32(rsL.Get_Fields("Cost"));
						modPPCalculate.Statics.clLeaseDate = FCConvert.ToString(rsL.Get_Fields_String("LeaseDate"));
						modPPCalculate.Statics.clMonths = FCConvert.ToInt32(rsL.Get_Fields_Int16("MOS"));
						modPPCalculate.Statics.clLP = FCConvert.ToInt32(rsL.Get_Fields_Int16("L1P2"));
						//FC:FINAL:MSH - i.issue #1330: can't inplicitly convert decimal to double
						modPPCalculate.Statics.clMonthlyRent = FCConvert.ToDouble(rsL.Get_Fields_Decimal("MonthlyRent"));
						modPPCalculate.Statics.clPercentGood = FCConvert.ToDouble(rsL.Get_Fields_Int16("DEPYRS"));
						if (modPPCalculate.Statics.clPercentGood != 0)
							modPPCalculate.Statics.clPercentGood *= 100;
						//FC:FINAL:MSH - i.issue #1330: can't inplicitly convert to double
						modPPCalculate.Statics.clFactor = FCConvert.ToDouble(rsL.Get_Fields_Int16("Fctr")) / 100;
						// 
						modPPCalculate.LeasedValue();
						if (Conversion.Val(modPPCalculate.Statics.clCode) != 0)
						{
							dblValue = FCConvert.ToDouble(modPPCalculate.Statics.Work1);
							rsL.Edit();
							if (Conversion.Val(rsL.Get_Fields_Int32("EXEMPTYEAR") + "") == lngCurBETEYear || (Conversion.Val(rsL.Get_Fields_Int32("EXEMPTYEAR") + "") == lngCurBETEYear - 1))
							{
								rsL.Set_Fields("value", 0);
								rsL.Set_Fields("BETEEXEMPT", dblValue);
								LeasedBETE[FCConvert.ToInt32(modPPCalculate.Statics.clCode)] += FCConvert.ToInt32(dblValue);
							}
							else
							{
								rsL.Set_Fields("value", dblValue);
								rsL.Set_Fields("beteexempt", 0);
								LeasedCalculatedCost[FCConvert.ToInt32(modPPCalculate.Statics.clCode)] += FCConvert.ToInt32(dblValue);
							}
							// 
							rsL.Update();
							LeasedOriginalCost[FCConvert.ToInt32(modPPCalculate.Statics.clCode)] += modPPCalculate.Statics.clCost;
						}
					}
					rsL.MoveNext();
				}
			}
		}

		private void SetValuations()
		{
			int iii;
			clsDRWrapper clsTemp = new clsDRWrapper();
			modGNWork.GetLocalVariables2();
			clsTemp.OpenRecordset("SELECT * FROM PPRatioOpens", "twpp0000.vb1");
			// TODO Get_Fields: Check the table for the column [Ratio] and replace with corresponding Get_Field method
			Ratio = Conversion.Val(clsTemp.Get_Fields("Ratio")) / 100;
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			rsV.Set_Fields("ValueKey", rsM.Get_Fields("Account"));
			// 
			rsV.Set_Fields("TotalLeased", 0);
			rsV.Set_Fields("TotalItemized", 0);
			for (iii = 1; iii <= 9; iii++)
			{
				rsV.Set_Fields("TotalLeased", rsV.Get_Fields_Int32("TotalLeased") + modGlobal.Round(LeasedCalculatedCost[iii], modPPCalculate.Statics.PPRounding));
				rsV.Set_Fields("TotalItemized", rsV.Get_Fields_Int32("TotalItemized") + modGlobal.Round(ItemizedCalculatedCost[iii], modPPCalculate.Statics.PPRounding));
				// rsV.Fields("Cat" & iii & "Exempt") = Round(Round(ItemizedBETE(iii), PPRounding) * Ratio, PPRounding)
				rsV.Set_Fields("Cat" + iii + "Exempt", modGlobal.Round((modGlobal.Round(ItemizedBETE[iii], modPPCalculate.Statics.PPRounding) + modGlobal.Round(LeasedBETE[iii], modPPCalculate.Statics.PPRounding)) * Ratio, modPPCalculate.Statics.PPRounding));
			}
			// iii
			rsV.Set_Fields("TotalLeased", modGlobal.Round(rsV.Get_Fields_Int32("TotalLeased"), modPPCalculate.Statics.PPRounding));
			rsV.Set_Fields("TotalItemized", modGlobal.Round(rsV.Get_Fields_Int32("TotalItemized"), modPPCalculate.Statics.PPRounding));
			// 
			rsV.Set_Fields("Category1", modGlobal.Round((modGlobal.Round(ItemizedCalculatedCost[1], modPPCalculate.Statics.PPRounding) + modGlobal.Round(LeasedCalculatedCost[1], modPPCalculate.Statics.PPRounding)) * Ratio, modPPCalculate.Statics.PPRounding));
			rsV.Set_Fields("Category2", modGlobal.Round((modGlobal.Round(ItemizedCalculatedCost[2], modPPCalculate.Statics.PPRounding) + modGlobal.Round(LeasedCalculatedCost[2], modPPCalculate.Statics.PPRounding)) * Ratio, modPPCalculate.Statics.PPRounding));
			rsV.Set_Fields("Category3", modGlobal.Round((modGlobal.Round(ItemizedCalculatedCost[3], modPPCalculate.Statics.PPRounding) + modGlobal.Round(LeasedCalculatedCost[3], modPPCalculate.Statics.PPRounding)) * Ratio, modPPCalculate.Statics.PPRounding));
			rsV.Set_Fields("Category4", modGlobal.Round((modGlobal.Round(ItemizedCalculatedCost[4], modPPCalculate.Statics.PPRounding) + modGlobal.Round(LeasedCalculatedCost[4], modPPCalculate.Statics.PPRounding)) * Ratio, modPPCalculate.Statics.PPRounding));
			rsV.Set_Fields("Category5", modGlobal.Round((modGlobal.Round(ItemizedCalculatedCost[5], modPPCalculate.Statics.PPRounding) + modGlobal.Round(LeasedCalculatedCost[5], modPPCalculate.Statics.PPRounding)) * Ratio, modPPCalculate.Statics.PPRounding));
			rsV.Set_Fields("Category6", modGlobal.Round((modGlobal.Round(ItemizedCalculatedCost[6], modPPCalculate.Statics.PPRounding) + modGlobal.Round(LeasedCalculatedCost[6], modPPCalculate.Statics.PPRounding)) * Ratio, modPPCalculate.Statics.PPRounding));
			rsV.Set_Fields("Category7", modGlobal.Round((modGlobal.Round(ItemizedCalculatedCost[7], modPPCalculate.Statics.PPRounding) + modGlobal.Round(LeasedCalculatedCost[7], modPPCalculate.Statics.PPRounding)) * Ratio, modPPCalculate.Statics.PPRounding));
			rsV.Set_Fields("Category8", modGlobal.Round((modGlobal.Round(ItemizedCalculatedCost[8], modPPCalculate.Statics.PPRounding) + modGlobal.Round(LeasedCalculatedCost[8], modPPCalculate.Statics.PPRounding)) * Ratio, modPPCalculate.Statics.PPRounding));
			rsV.Set_Fields("Category9", modGlobal.Round((modGlobal.Round(ItemizedCalculatedCost[9], modPPCalculate.Statics.PPRounding) + modGlobal.Round(LeasedCalculatedCost[9], modPPCalculate.Statics.PPRounding)) * Ratio, modPPCalculate.Statics.PPRounding));
			rsV.Set_Fields("OverrideAmount", OverrideValue);
			FCUtils.EraseSafe(ItemizedCalculatedCost);
			FCUtils.EraseSafe(LeasedCalculatedCost);
			FCUtils.EraseSafe(ItemizedOriginalCost);
			FCUtils.EraseSafe(ItemizedBETE);
			FCUtils.EraseSafe(LeasedBETE);
			FCUtils.EraseSafe(LeasedOriginalCost);
			rsV.Update();
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			rsV.FindFirstRecord("ValueKey", rsM.Get_Fields("Account"));
		}

		private void BuildDisplay()
		{
			modGNWork.GetLocalVariables2();
			vsDisplay.Rows = 11;
			vsDisplay.RowHeight(0, 750);
			vsDisplay.Cols = 7;
			int fn;
			int tItemizedO = 0;
			int tItemizedC = 0;
			int tLeasedO = 0;
			int tLeasedC = 0;
			for (fn = 1; fn <= 9; fn++)
			{
				vsDisplay.TextMatrix(fn, 1, Strings.Format(ItemizedOriginalCost[fn], "#,##0"));
				vsDisplay.TextMatrix(fn, 2, Strings.Format(ItemizedCalculatedCost[fn], "#,##0"));
				vsDisplay.TextMatrix(fn, 3, Strings.Format(LeasedOriginalCost[fn], "#,##0"));
				vsDisplay.TextMatrix(fn, 4, Strings.Format(LeasedCalculatedCost[fn], "#,##0"));
				vsDisplay.TextMatrix(fn, 6, Strings.Format(modGlobal.Round(ItemizedCalculatedCost[fn] + LeasedCalculatedCost[fn], modPPCalculate.Statics.PPRounding), "#,##0"));
				tItemizedO += ItemizedOriginalCost[fn];
				tItemizedC += ItemizedCalculatedCost[fn];
				tLeasedO += LeasedOriginalCost[fn];
				tLeasedC += LeasedCalculatedCost[fn];
				vsDisplay.TextMatrix(10, 6, Strings.Format(vsDisplay.ValueMatrix(10, 6) + vsDisplay.ValueMatrix(fn, 6), "#,##0"));
			}
			// fn
			vsDisplay.TextMatrix(10, 1, Strings.Format(tItemizedO, "#,##0"));
			vsDisplay.TextMatrix(10, 2, Strings.Format(tItemizedC, "#,##0"));
			vsDisplay.TextMatrix(10, 3, Strings.Format(tLeasedO, "#,##0"));
			vsDisplay.TextMatrix(10, 4, Strings.Format(tLeasedC, "#,##0"));
			// vbPorter upgrade warning: xxx As Variant --> As int
			//int xxx = 0;
			//xxx += vsDisplay.RowHeight(0);
			//xxx += vsDisplay.RowHeight(1) * 10 + 75;
			//vsDisplay.Height = xxx;
			vsDisplay.Left = FCConvert.ToInt32((sstPPCalculate.Width - vsDisplay.Width) / 2.0);
		}

		private void frmPrintSummary_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmPrintSummary_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				modPPGN.Statics.CalcTag = true;
				frmPrintSummary.InstancePtr.Close();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmPrintSummary_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPrintSummary properties;
			//frmPrintSummary.ScaleWidth	= 9045;
			//frmPrintSummary.ScaleHeight	= 7245;
			//frmPrintSummary.LinkTopic	= "Form1";
			//frmPrintSummary.LockControls	= true;
			//End Unmaped Properties
			// Call GetWindowSize(Me)
			// UnloadFormPPGNMenu
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			if (modPPGN.Statics.Open1Title != "")
			{
				if (cmbSequence.Items.Contains("Open 1"))
				{
					cmbSequence.Items.Remove("Open 1");
					//FC:FINAL:MSH - i.issue #1280: after item removing number of items will be less than insert position
					//cmbSequence.Items.Insert(4, modPPGN.Statics.Open1Title);
					cmbSequence.Items.Add(modPPGN.Statics.Open1Title);
				}
			}
			if (modPPGN.Statics.Open2Title != "")
			{
				if (cmbSequence.Items.Contains("Open 2"))
				{
					cmbSequence.Items.Remove("Open 2");
					//FC:FINAL:MSH - i.issue #1280: after item removing number of items will be less than insert position
					//cmbSequence.Items.Insert(5, modPPGN.Statics.Open2Title);
					cmbSequence.Items.Add(modPPGN.Statics.Open2Title);
				}
			}
			modGlobalFunctions.SetTRIOColors(this);
			lngCurBETEYear = modGlobal.Statics.CustomizeStuff.CurrentBETEYear;
			if (lngCurBETEYear == 0)
				lngCurBETEYear = DateTime.Today.Year;
		}

		private void CheckOverride()
		{
			if (OverrideCode == "Y")
			{
				Check1.CheckState = CheckState.Checked;
				txtOverride.Enabled = true;
				txtOverride.ReadOnly = false;
				lblOverride.Visible = true;
				// If OverrideValue > 0 Then
				txtOverride.Text = Strings.Format(OverrideValue, "#,##0");
				// End If
			}
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			modGlobal.SaveWindowSize(this);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdQuit_Click();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuPrintPreview_Click(object sender, System.EventArgs e)
		{
			cmdProcess_Click();
		}

		private void optRange_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			if (Index == 1)
			{
				txtLow.Enabled = true;
				txtHigh.Enabled = true;
				txtLow.Visible = true;
				txtHigh.Visible = true;
				Label2.Visible = true;
			}
			else
			{
				txtLow.Enabled = false;
				txtHigh.Enabled = false;
				txtLow.Visible = false;
				txtHigh.Visible = false;
				Label2.Visible = false;
			}
		}

		private void optRange_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = FCConvert.ToInt16(cmbRange.SelectedIndex);
			optRange_CheckedChanged(index, sender, e);
		}

		private void Print_Line()
		{
			// vbPorter upgrade warning: lngCategory As int	OnWrite(short, double)
			int lngCategory;
			int lngTemp;
			// vbPorter upgrade warning: lngBETE As int	OnWrite(short, double)
			int lngBETE;
			lngCategory = 0;
			lngCategory += FCConvert.ToInt32(Conversion.Val(rsV.Get_Fields_Int32("category1")));
			lngCategory += FCConvert.ToInt32(Conversion.Val(rsV.Get_Fields_Int32("category2")));
			lngCategory += FCConvert.ToInt32(Conversion.Val(rsV.Get_Fields_Int32("category3")));
			lngCategory += FCConvert.ToInt32(Conversion.Val(rsV.Get_Fields_Int32("category4")));
			lngCategory += FCConvert.ToInt32(Conversion.Val(rsV.Get_Fields_Int32("category5")));
			lngCategory += FCConvert.ToInt32(Conversion.Val(rsV.Get_Fields_Int32("category6")));
			lngCategory += FCConvert.ToInt32(Conversion.Val(rsV.Get_Fields_Int32("category7")));
			lngCategory += FCConvert.ToInt32(Conversion.Val(rsV.Get_Fields_Int32("category8")));
			lngCategory += FCConvert.ToInt32(Conversion.Val(rsV.Get_Fields_Int32("Category9")));
			int x;
			lngBETE = 0;
			for (x = 1; x <= 9; x++)
			{
				// TODO Get_Fields: Field [cat] not found!! (maybe it is an alias?)
				lngBETE += FCConvert.ToInt32(Conversion.Val(rsV.Get_Fields("cat" + FCConvert.ToString(x) + "exempt")));
			}
			// x
			// If LineCount < 5 Then Call Print_Heading
			if (strField == "Name")
			{
				FCFileSystem.PrintLine(40, rsM.Get_Fields_String("Name"));
				LineCount += 1;
			}
			else if (strField == "Street")
			{
				// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
				FCFileSystem.PrintLine(40, rsM.Get_Fields("StreetNumber") + " " + rsM.Get_Fields_String("Street"));
				LineCount += 1;
			}
			// new 1/23/03
			lngCategory = FCConvert.ToInt32(modGlobal.Round(lngCategory, modPPCalculate.Statics.PPRounding));
			// corey 6/25/03
			lngTemp = lngCategory;
			string resString = "";
			//FC:FINAL:MSH - i.issue #1330: remove not implemented method Strings.StrP. In VB6 Tab() returns the spaces, number of which calculated from start of the string,
			//so change formatting and type of writing row to the file
			//FCFileSystem.Print(40, FCUtils.Tab(1) + Strings.Format(rsM.Get_Fields("Account"), "00000"));
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			resString += FCUtils.Tab(1) + Strings.Format(rsM.Get_Fields("Account"), "00000");
			//FCFileSystem.Print(40, FCUtils.Tab(7) + Strings.Format(Strings.Format(rsV.Get_Fields("TotalLeased"), "#,##0"), "@@@@@@@@@@"));
			resString += FCUtils.Tab(7, resString) + Strings.Format(Strings.Format(rsV.Get_Fields_Int32("TotalLeased"), "#,##0"), "@@@@@@@@@@");
			//FCFileSystem.Print(40, FCUtils.Tab(20) + Strings.Format(Strings.Format(rsV.Get_Fields("TotalItemized"), "#,##0"), "@@@@@@@@@@"));
			resString += FCUtils.Tab(20, resString) + Strings.Format(Strings.Format(rsV.Get_Fields_Int32("TotalItemized"), "#,##0"), "@@@@@@@@@@");
			//FCFileSystem.Print(40, FCUtils.Tab(33) + Strings.Format(Strings.Format(lngCategory, "#,##0"), "@@@@@@@@@@"));
			resString += FCUtils.Tab(33, resString) + Strings.Format(Strings.Format(lngCategory, "#,##0"), "@@@@@@@@@@");
			//FCFileSystem.Print(40, FCUtils.Tab(46) + Strings.Format(Strings.Format(lngBETE, "#,##0"), "@@@@@@@@@@"));
			resString += FCUtils.Tab(46, resString) + Strings.Format(Strings.Format(lngBETE, "#,##0"), "@@@@@@@@@@");
			// Print #40, Tab(59); Format(Format(lngBETE + lngCategory, "#,##0"), "@@@@@@@@@@");
			if (fecherFoundation.FCUtils.IsNull(rsM.Get_Fields_String("orcode")) == false)
			{
				if (FCConvert.ToString(rsM.Get_Fields_String("orcode")) == "Y")
				{
					if (fecherFoundation.FCUtils.IsNull(rsV.Get_Fields_Int32("OverrideAmount")) == false)
					{
						// If rsV.Fields("OverrideAmount") <> 0 Then
						//FCFileSystem.Print(40, FCUtils.Tab(59) + Strings.Format(Strings.Format(lngBETE + lngCategory, "#,##0"), "@@@@@@@@@@"));
						resString += FCUtils.Tab(59, resString) + Strings.Format(Strings.Format(lngBETE + lngCategory, "#,##0"), "@@@@@@@@@@");
						//FCFileSystem.Print(40, FCUtils.Tab(73) + Strings.Format(Strings.Format(rsV.Get_Fields("OverrideAmount"), "#,##0"), "@@@@@@@@@@@"));
						resString += FCUtils.Tab(73, resString) + Strings.Format(Strings.Format(rsV.Get_Fields_Int32("OverrideAmount"), "#,##0"), "@@@@@@@@@@@");
						FCFileSystem.PrintLine(40, resString + FCUtils.Tab(85, resString) + "OV");
						lngORTotal += rsV.Get_Fields_Int32("OverrideAmount");
						lngTemp = FCConvert.ToInt32(rsV.Get_Fields_Int32("overrideamount"));
						lngORCalc += lngCategory;
						// End If
					}
					else
					{
						FCFileSystem.PrintLine(40, resString + FCUtils.Tab(59, resString) + Strings.Format(Strings.Format(lngBETE + lngCategory, "#,##0"), "@@@@@@@@@@"));
					}
				}
				else
				{
					FCFileSystem.PrintLine(40, resString + FCUtils.Tab(59, resString) + Strings.Format(Strings.Format(lngBETE + lngCategory, "#,##0"), "@@@@@@@@@@"));
				}
			}
			else
			{
				FCFileSystem.PrintLine(40, resString + FCUtils.Tab(59, resString) + Strings.Format(Strings.Format(lngBETE + lngCategory, "#,##0"), "@@@@@@@@@@"));
			}
			// Print #40, vbCrLf
			lngTotalPrintI += FCConvert.ToInt32(Conversion.Val(rsV.Get_Fields_Int32("TotalItemized")));
			lngTotalPrintL += FCConvert.ToInt32(Conversion.Val(rsV.Get_Fields_Int32("TotalLeased")));
			lngTotalPrintBETE += lngBETE;
			lngTotalPrintTot += lngCategory;
			lngTotalPrintFinal += lngTemp;
		}

		private void Print_Heading()
		{
			PageCount += 1;
			//FC:FINAL:MSH - i.issue #1330: remove not implemented method Strings.StrP
			FCFileSystem.Print(40, FCUtils.Tab(1) + "- " + modGlobalConstants.Statics.MuniName + " -");
			FCFileSystem.Print(40, FCUtils.Tab(25) + "PERSONAL PROPERTY ASSESSMENT REPORT");
			FCFileSystem.PrintLine(40, FCUtils.Tab(63) + "AS OF " + Strings.Format(DateTime.Now, "MM/dd/yyyy"));
			FCFileSystem.Print(40, FCUtils.Tab(37) + "RATIO=" + FCConvert.ToString(modPPGN.Statics.PPRatio * 100) + "%");
			FCFileSystem.PrintLine(40, FCUtils.Tab(69) + "PAGE " + FCConvert.ToString(PageCount));
			FCFileSystem.PrintLine(40, " ");
			FCFileSystem.Print(40, FCUtils.Tab(14) + "- - - - - C O M P U T E R - - - - -");
			FCFileSystem.PrintLine(40, FCUtils.Tab(67) + "OVERRIDE");
			FCFileSystem.Print(40, FCUtils.Tab(2) + "ACCOUNT");
			FCFileSystem.Print(40, FCUtils.Tab(16) + "LEASED");
			FCFileSystem.Print(40, FCUtils.Tab(28) + "ITEMIZED");
			FCFileSystem.Print(40, FCUtils.Tab(55) + "BETE");
			FCFileSystem.Print(40, FCUtils.Tab(43) + "-TOTAL-");
			// Print #40, Tab(57); "AMOUNT";
			LineCount = 6;
		}

		private void cmbRange_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbRange.SelectedIndex == 0)
			{
				optRange_CheckedChanged(sender, e);
			}
			//FC:FINAL:MSH - i.issue #1329: add comparing to handle selection of the another option
			else if (cmbRange.SelectedIndex == 1)
			{
				optRange_CheckedChanged(sender, e);
			}
		}
	}
}
