//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmPPReimbursementNotices.
	/// </summary>
	partial class frmPPReimbursementNotices : BaseForm
	{
		public fecherFoundation.FCComboBox cmbType;
		public fecherFoundation.FCLabel lblType;
		public fecherFoundation.FCComboBox cmbRange;
		public fecherFoundation.FCLabel lblRange;
		public fecherFoundation.FCTextBox txtTaxYear;
		public fecherFoundation.FCTextBox txtSignerTitle;
		public fecherFoundation.FCTextBox txtMessage;
		public fecherFoundation.FCTextBox txtSigner;
		public fecherFoundation.FCTextBox txtTaxRate;
		public fecherFoundation.FCTextBox txtSecondPayment;
		public fecherFoundation.FCTextBox txtFirstPayment;
		public fecherFoundation.FCCheckBox chkQty;
		public fecherFoundation.FCFrame fraRange;
		public fecherFoundation.FCTextBox txtStart;
		public fecherFoundation.FCTextBox txtStop;
		public fecherFoundation.FCLabel lblStart;
		public fecherFoundation.FCLabel lblStop;
		public fecherFoundation.FCButton cmdOK;
		public fecherFoundation.FCData datRB;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label2;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPPReimbursementNotices));
            this.cmbType = new fecherFoundation.FCComboBox();
            this.lblType = new fecherFoundation.FCLabel();
            this.cmbRange = new fecherFoundation.FCComboBox();
            this.lblRange = new fecherFoundation.FCLabel();
            this.txtTaxYear = new fecherFoundation.FCTextBox();
            this.txtSignerTitle = new fecherFoundation.FCTextBox();
            this.txtMessage = new fecherFoundation.FCTextBox();
            this.txtSigner = new fecherFoundation.FCTextBox();
            this.txtTaxRate = new fecherFoundation.FCTextBox();
            this.txtSecondPayment = new fecherFoundation.FCTextBox();
            this.txtFirstPayment = new fecherFoundation.FCTextBox();
            this.chkQty = new fecherFoundation.FCCheckBox();
            this.fraRange = new fecherFoundation.FCFrame();
            this.txtStart = new fecherFoundation.FCTextBox();
            this.txtStop = new fecherFoundation.FCTextBox();
            this.lblStart = new fecherFoundation.FCLabel();
            this.lblStop = new fecherFoundation.FCLabel();
            this.cmdOK = new fecherFoundation.FCButton();
            this.datRB = new fecherFoundation.FCData();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuContinue = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraRange)).BeginInit();
            this.fraRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 642);
            this.BottomPanel.Size = new System.Drawing.Size(1078, 24);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdSave);
            this.ClientArea.Controls.Add(this.cmdOK);
            this.ClientArea.Controls.Add(this.txtTaxYear);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.txtSignerTitle);
            this.ClientArea.Controls.Add(this.txtMessage);
            this.ClientArea.Controls.Add(this.txtSigner);
            this.ClientArea.Controls.Add(this.txtTaxRate);
            this.ClientArea.Controls.Add(this.txtSecondPayment);
            this.ClientArea.Controls.Add(this.txtFirstPayment);
            this.ClientArea.Controls.Add(this.chkQty);
            this.ClientArea.Controls.Add(this.cmbType);
            this.ClientArea.Controls.Add(this.lblType);
            this.ClientArea.Controls.Add(this.fraRange);
            this.ClientArea.Controls.Add(this.cmbRange);
            this.ClientArea.Controls.Add(this.lblRange);
            this.ClientArea.Controls.Add(this.datRB);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.Label4);
            this.ClientArea.Controls.Add(this.Label5);
            this.ClientArea.Controls.Add(this.Label7);
            this.ClientArea.Controls.Add(this.Label6);
            this.ClientArea.Controls.Add(this.Label8);
            this.ClientArea.Controls.Add(this.Label9);
            this.ClientArea.Size = new System.Drawing.Size(1078, 582);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(1078, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(275, 30);
            this.HeaderText.Text = "Reimbursement Notices";
            // 
            // cmbType
            // 
            this.cmbType.Items.AddRange(new object[] {
            "Name",
            "Account",
            "Location",
            "Business Code",
            "Tran Code",
            "Street Code",
            "Open 1",
            "Open 2"});
            this.cmbType.Location = new System.Drawing.Point(142, 80);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(210, 40);
            this.cmbType.TabIndex = 3;
            this.cmbType.Text = "Name";
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(30, 94);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(97, 15);
            this.lblType.TabIndex = 2;
            this.lblType.Text = "SEQUENCE BY";
            // 
            // cmbRange
            // 
            this.cmbRange.Items.AddRange(new object[] {
            "All",
            "Range"});
            this.cmbRange.Location = new System.Drawing.Point(142, 30);
            this.cmbRange.Name = "cmbRange";
            this.cmbRange.Size = new System.Drawing.Size(210, 40);
            this.cmbRange.TabIndex = 1;
            this.cmbRange.Text = "All";
            this.cmbRange.SelectedIndexChanged += new System.EventHandler(this.optRange_CheckedChanged);
            // 
            // lblRange
            // 
            this.lblRange.AutoSize = true;
            this.lblRange.Location = new System.Drawing.Point(30, 44);
            this.lblRange.Name = "lblRange";
            this.lblRange.Size = new System.Drawing.Size(77, 15);
            this.lblRange.Text = "ACCOUNTS";
            // 
            // txtTaxYear
            // 
            this.txtTaxYear.BackColor = System.Drawing.SystemColors.Window;
            this.txtTaxYear.Location = new System.Drawing.Point(142, 130);
            this.txtTaxYear.Name = "txtTaxYear";
            this.txtTaxYear.Size = new System.Drawing.Size(85, 40);
            this.txtTaxYear.TabIndex = 5;
            this.txtTaxYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtTaxYear_Validating);
            // 
            // txtSignerTitle
            // 
            this.txtSignerTitle.BackColor = System.Drawing.SystemColors.Window;
            this.txtSignerTitle.Location = new System.Drawing.Point(219, 357);
            this.txtSignerTitle.Name = "txtSignerTitle";
            this.txtSignerTitle.Size = new System.Drawing.Size(157, 40);
            this.txtSignerTitle.TabIndex = 18;
            this.txtSignerTitle.Visible = false;
            this.txtSignerTitle.Validating += new System.ComponentModel.CancelEventHandler(this.txtSignerTitle_Validating);
            // 
            // txtMessage
            // 
            this.txtMessage.BackColor = System.Drawing.SystemColors.Window;
            this.txtMessage.Location = new System.Drawing.Point(219, 507);
            this.txtMessage.Multiline = true;
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.ScrollBars = Wisej.Web.ScrollBars.Vertical;
            this.txtMessage.Size = new System.Drawing.Size(414, 112);
            this.txtMessage.TabIndex = 19;
            this.txtMessage.Visible = false;
            this.txtMessage.Validating += new System.ComponentModel.CancelEventHandler(this.txtMessage_Validating);
            // 
            // txtSigner
            // 
            this.txtSigner.BackColor = System.Drawing.SystemColors.Window;
            this.txtSigner.Location = new System.Drawing.Point(219, 307);
            this.txtSigner.Name = "txtSigner";
            this.txtSigner.Size = new System.Drawing.Size(157, 40);
            this.txtSigner.TabIndex = 17;
            this.txtSigner.Visible = false;
            this.txtSigner.Validating += new System.ComponentModel.CancelEventHandler(this.txtSigner_Validating);
            // 
            // txtTaxRate
            // 
            this.txtTaxRate.BackColor = System.Drawing.SystemColors.Window;
            this.txtTaxRate.Location = new System.Drawing.Point(142, 180);
            this.txtTaxRate.Name = "txtTaxRate";
            this.txtTaxRate.Size = new System.Drawing.Size(111, 40);
            this.txtTaxRate.TabIndex = 7;
            this.txtTaxRate.Validating += new System.ComponentModel.CancelEventHandler(this.txtTaxRate_Validating);
            // 
            // txtSecondPayment
            // 
            this.txtSecondPayment.BackColor = System.Drawing.SystemColors.Window;
            this.txtSecondPayment.Location = new System.Drawing.Point(219, 457);
            this.txtSecondPayment.Name = "txtSecondPayment";
            this.txtSecondPayment.Size = new System.Drawing.Size(157, 40);
            this.txtSecondPayment.TabIndex = 15;
            this.txtSecondPayment.Visible = false;
            this.txtSecondPayment.Validating += new System.ComponentModel.CancelEventHandler(this.txtSecondPayment_Validating);
            // 
            // txtFirstPayment
            // 
            this.txtFirstPayment.BackColor = System.Drawing.SystemColors.Window;
            this.txtFirstPayment.Location = new System.Drawing.Point(219, 407);
            this.txtFirstPayment.Name = "txtFirstPayment";
            this.txtFirstPayment.Size = new System.Drawing.Size(157, 40);
            this.txtFirstPayment.TabIndex = 14;
            this.txtFirstPayment.Visible = false;
            this.txtFirstPayment.Validating += new System.ComponentModel.CancelEventHandler(this.txtFirstPayment_Validating);
            // 
            // chkQty
            // 
            this.chkQty.Location = new System.Drawing.Point(402, 314);
            this.chkQty.Name = "chkQty";
            this.chkQty.Size = new System.Drawing.Size(482, 27);
            this.chkQty.TabIndex = 12;
            this.chkQty.Text = "Don\'t print quantity values (Usually for recently converted files)";
            this.chkQty.Visible = false;
            // 
            // fraRange
            // 
            this.fraRange.Controls.Add(this.txtStart);
            this.fraRange.Controls.Add(this.txtStop);
            this.fraRange.Controls.Add(this.lblStart);
            this.fraRange.Controls.Add(this.lblStop);
            this.fraRange.Location = new System.Drawing.Point(382, 30);
            this.fraRange.Name = "fraRange";
            this.fraRange.Size = new System.Drawing.Size(290, 150);
            this.fraRange.TabIndex = 8;
            this.fraRange.Text = "Range";
            // 
            // txtStart
            // 
            this.txtStart.BackColor = System.Drawing.SystemColors.Window;
            this.txtStart.Location = new System.Drawing.Point(117, 30);
            this.txtStart.Name = "txtStart";
            this.txtStart.Size = new System.Drawing.Size(153, 40);
            this.txtStart.TabIndex = 1;
            // 
            // txtStop
            // 
            this.txtStop.BackColor = System.Drawing.SystemColors.Window;
            this.txtStop.Location = new System.Drawing.Point(117, 90);
            this.txtStop.Name = "txtStop";
            this.txtStop.Size = new System.Drawing.Size(153, 40);
            this.txtStop.TabIndex = 3;
            // 
            // lblStart
            // 
            this.lblStart.Location = new System.Drawing.Point(20, 44);
            this.lblStart.Name = "lblStart";
            this.lblStart.Size = new System.Drawing.Size(77, 19);
            this.lblStart.Text = "STARTING";
            // 
            // lblStop
            // 
            this.lblStop.Location = new System.Drawing.Point(20, 104);
            this.lblStop.Name = "lblStop";
            this.lblStop.Size = new System.Drawing.Size(60, 19);
            this.lblStop.TabIndex = 2;
            this.lblStop.Text = "ENDING";
            // 
            // cmdOK
            // 
            this.cmdOK.AppearanceKey = "actionButton";
            this.cmdOK.Location = new System.Drawing.Point(30, 668);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(82, 40);
            this.cmdOK.TabIndex = 20;
            this.cmdOK.Text = "OK";
            this.cmdOK.Visible = false;
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // datRB
            // 
            this.datRB.BackColor = System.Drawing.Color.FromName("@window");
            this.datRB.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("datRB.BackgroundImage")));
            this.datRB.Caption = "RB";
            this.datRB.ConnectAsString = "Access";
            this.datRB.ForeColor = System.Drawing.SystemColors.ControlText;
            this.datRB.Name = "datRB";
            this.datRB.RecordSource = "";
            this.datRB.Size = new System.Drawing.Size(164, 26);
            this.datRB.TabIndex = 26;
            this.datRB.UserControlBackColor = System.Drawing.Color.FromName("@window");
            this.datRB.UserControlForeColor = System.Drawing.SystemColors.ControlText;
            this.datRB.UserControlHeight = 312;
            this.datRB.UserControlWidth = 1968;
            this.datRB.Visible = false;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 144);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(73, 17);
            this.Label1.TabIndex = 4;
            this.Label1.Text = "TAX YEAR";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(30, 521);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(115, 17);
            this.Label4.TabIndex = 33;
            this.Label4.Text = "ENTER MESSAGE";
            this.Label4.Visible = false;
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(30, 371);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(130, 17);
            this.Label5.TabIndex = 32;
            this.Label5.Text = "SIGNER\'S TITLE";
            this.Label5.Visible = false;
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(30, 321);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(126, 17);
            this.Label6.TabIndex = 31;
            this.Label6.Text = "SIGNER";
            this.Label6.Visible = false;
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(30, 184);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(82, 17);
            this.Label7.TabIndex = 6;
            this.Label7.Text = "TAX RATE";
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(30, 471);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(150, 17);
            this.Label8.TabIndex = 29;
            this.Label8.Text = "2ND PAYMENT DUE DATE";
            this.Label8.Visible = false;
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(30, 421);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(150, 17);
            this.Label9.TabIndex = 28;
            this.Label9.Text = "1ST PAYMENT DUE DATE";
            this.Label9.Visible = false;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 629);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(335, 19);
            this.Label2.TabIndex = 27;
            this.Label2.Text = "PLEASE REVIEW AND MAKE ANY NECESSARY CHANGES";
            this.Label2.Visible = false;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuContinue,
            this.mnuSepar,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuContinue
            // 
            this.mnuContinue.Index = 0;
            this.mnuContinue.Name = "mnuContinue";
            this.mnuContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuContinue.Text = "Save & Continue";
            this.mnuContinue.Click += new System.EventHandler(this.mnuContinue_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 1;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(30, 256);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(150, 48);
            this.cmdSave.TabIndex = 21;
            this.cmdSave.Text = "Save & Continue";
            this.cmdSave.Click += new System.EventHandler(this.mnuContinue_Click);
            // 
            // frmPPReimbursementNotices
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1078, 468);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmPPReimbursementNotices";
            this.ShowInTaskbar = false;
            this.Text = "Reimbursement Notices";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmPPReimbursementNotices_Load);
            this.Activated += new System.EventHandler(this.frmPPReimbursementNotices_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPPReimbursementNotices_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPPReimbursementNotices_KeyPress);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraRange)).EndInit();
            this.fraRange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		public FCButton cmdSave;
	}
}