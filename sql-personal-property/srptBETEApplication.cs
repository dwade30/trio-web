﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for srptBETEApplication.
	/// </summary>
	public partial class srptBETEApplication : FCSectionReport
	{
		public srptBETEApplication()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ActiveReport1";
		}

		public static srptBETEApplication InstancePtr
		{
			get
			{
				return (srptBETEApplication)Sys.GetInstance(typeof(srptBETEApplication));
			}
		}

		protected srptBETEApplication _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsReport?.Dispose();
                rsReport = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptBETEApplication	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsReport = new clsDRWrapper();
		clsCategories clsCats = new clsCategories();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsReport.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strList;
			string[] strAry = null;
			string strSQL = "";
			int intAccount;
			int intYear;
			bool boolCurrentOnly;
			strList = FCConvert.ToString(this.UserData);
			strAry = Strings.Split(strList, ",", -1, CompareConstants.vbTextCompare);
			intAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[0])));
			boolCurrentOnly = FCConvert.CBool(strAry[1]);
			intYear = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[2])));
			if (boolCurrentOnly)
			{
				strSQL = "select * from ppitemized where account = " + FCConvert.ToString(intAccount) + " and exemptyear = " + FCConvert.ToString(intYear) + " order by line";
			}
			else
			{
				strSQL = "select * from ppitemized where account = " + FCConvert.ToString(intAccount) + " and (rb = 'P' or exemptyear >= 2008) order by line";
			}
			rsReport.OpenRecordset(strSQL, modPPGN.strPPDatabase);
			if (rsReport.EndOfFile())
			{
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsReport.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [line] and replace with corresponding Get_Field method
				txtLine.Text = rsReport.Get_Fields_String("line");
				txtDescription.Text = rsReport.Get_Fields_String("DESCRIPTION");
				if (Conversion.Val(rsReport.Get_Fields_String("cd")) != 0)
				{
					// TODO Get_Fields: Check the table for the column [sro] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [qUANTIty] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [cost] and replace with corresponding Get_Field method
					if (Strings.UCase(FCConvert.ToString(rsReport.Get_Fields("sro"))) == "O" && (Conversion.Val(rsReport.Get_Fields("qUANTIty")) * Conversion.Val(rsReport.Get_Fields("cost"))) > 0)
					{
						// TODO Get_Fields: Check the table for the column [qUANTIty] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [cost] and replace with corresponding Get_Field method
						txtCost.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("qUANTIty")) * Conversion.Val(rsReport.Get_Fields("cost")), "#,###,###,##0");
					}
					else
					{
						txtCost.Text = "________";
					}
					// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
					if (Conversion.Val(rsReport.Get_Fields("value")) > 0)
					{
						// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
						txtValue.Text = Strings.Format(Conversion.Val(rsReport.Get_Fields("value")), "#,###,###,##0");
					}
					else
					{
						txtValue.Text = "";
					}
					if (Conversion.Val(rsReport.Get_Fields_Int16("month")) > 0)
					{
						// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
						txtDatePurchased.Text = rsReport.Get_Fields_Int16("month") + "/" + rsReport.Get_Fields("Year");
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [year] and replace with corresponding Get_Field method
						if (Conversion.Val(rsReport.Get_Fields("year")) > 0)
						{
							// TODO Get_Fields: Check the table for the column [year] and replace with corresponding Get_Field method
							txtDatePurchased.Text = rsReport.Get_Fields_String("year");
						}
						else
						{
							txtDatePurchased.Text = "";
						}
					}
					if (Conversion.Val(rsReport.Get_Fields_Int16("dpyr")) > 0)
					{
						txtLife.Text = FCConvert.ToString(rsReport.Get_Fields_Int16("dpyr"));
					}
					else
					{
						txtLife.Text = FCConvert.ToString(clsCats.Get_Life(FCConvert.ToInt16(rsReport.Get_Fields_String("cd"))));
						if (txtLife.Text == "0")
							txtLife.Text = "";
					}
				}
				else
				{
					txtCost.Text = "";
					txtValue.Text = "";
					txtDatePurchased.Text = "";
					txtLife.Text = "";
				}
				rsReport.MoveNext();
			}
		}

		
	}
}
