﻿//Fecher vbPorter - Version 1.0.0.32
using Wisej.Web;
using Wisej.Core;
using Global;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmImportItem.
	/// </summary>
	partial class frmImportItem : BaseForm
	{
		public fecherFoundation.FCDriveListBox Drive1;
		public fecherFoundation.FCDirListBox Dir1;
		public fecherFoundation.FCFileListBox File1;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmImportItem));
			this.Drive1 = new fecherFoundation.FCDriveListBox();
			this.Dir1 = new fecherFoundation.FCDirListBox();
			this.File1 = new fecherFoundation.FCFileListBox();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuContinue = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSaveContinue = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSaveContinue);
			this.BottomPanel.Location = new System.Drawing.Point(0, 360);
			this.BottomPanel.Size = new System.Drawing.Size(450, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Drive1);
			this.ClientArea.Controls.Add(this.Dir1);
			this.ClientArea.Controls.Add(this.File1);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(450, 300);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(450, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(216, 30);
			this.HeaderText.Text = "Itemized From File";
			// 
			// Drive1
			// 
			this.Drive1.Location = new System.Drawing.Point(30, 100);
			this.Drive1.Name = "Drive1";
			this.Drive1.Size = new System.Drawing.Size(197, 21);
			this.Drive1.TabIndex = 2;
			this.Drive1.SelectedIndexChanged += new System.EventHandler(this.Drive1_SelectedIndexChanged);
			// 
			// Dir1
			// 
			this.Dir1.Location = new System.Drawing.Point(30, 186);
			this.Dir1.Name = "Dir1";
			this.Dir1.Size = new System.Drawing.Size(197, 97);
			this.Dir1.TabIndex = 1;
			this.Dir1.SelectedIndexChanged += new System.EventHandler(this.Dir1_SelectedIndexChanged);
			// 
			// File1
			// 
			this.File1.Location = new System.Drawing.Point(257, 65);
			this.File1.Name = "File1";
			this.File1.Size = new System.Drawing.Size(165, 218);
			this.File1.TabIndex = 0;
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(30, 30);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(389, 15);
			this.Label3.TabIndex = 5;
			this.Label3.Text = "CHOOSE THE FILE YOU WISH TO IMPORT FROM";
			this.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 151);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(197, 15);
			this.Label2.TabIndex = 4;
			this.Label2.Text = "FOLDER";
			this.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 65);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(197, 15);
			this.Label1.TabIndex = 3;
			this.Label1.Text = "DRIVE";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuContinue,
				this.mnuSepar1,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuContinue
			// 
			this.mnuContinue.Index = 0;
			this.mnuContinue.Name = "mnuContinue";
			this.mnuContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuContinue.Text = "Save & Continue";
			this.mnuContinue.Click += new System.EventHandler(this.mnuContinue_Click);
			// 
			// mnuSepar1
			// 
			this.mnuSepar1.Index = 1;
			this.mnuSepar1.Name = "mnuSepar1";
			this.mnuSepar1.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSaveContinue
			// 
			this.cmdSaveContinue.AppearanceKey = "acceptButton";
			this.cmdSaveContinue.Location = new System.Drawing.Point(146, 30);
			this.cmdSaveContinue.Name = "cmdSaveContinue";
			this.cmdSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSaveContinue.Size = new System.Drawing.Size(148, 48);
			this.cmdSaveContinue.TabIndex = 0;
			this.cmdSaveContinue.Text = "Save & Continue";
			this.cmdSaveContinue.Click += new System.EventHandler(this.mnuContinue_Click);
			// 
			// frmImportItem
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(450, 468);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmImportItem";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Itemized From File";
			this.Load += new System.EventHandler(this.frmImportItem_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmImportItem_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private fecherFoundation.FCButton cmdSaveContinue;
	}
}
