﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Core;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptShortScreen.
	/// </summary>
	public partial class rptShortScreen : BaseSectionReport
	{
		public rptShortScreen()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Short Screen Billing";
		}

		public static rptShortScreen InstancePtr
		{
			get
			{
				return (rptShortScreen)Sys.GetInstance(typeof(rptShortScreen));
			}
		}

		protected rptShortScreen _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptShortScreen	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtLocation.Text = Strings.Trim(frmPPMasterShort.InstancePtr.txtStreetNumber.Text + frmPPMasterShort.InstancePtr.txtApt.Text + " " + frmPPMasterShort.InstancePtr.txtStreetName.Text);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
            using (clsDRWrapper clsLoad = new clsDRWrapper())
            {
                txtAccount.Text = frmPPMasterShort.InstancePtr.lblAccount.Text;
                txtName.Text = frmPPMasterShort.InstancePtr.lblOwner1.Text;
                txtAddress1.Text = frmPPMasterShort.InstancePtr.lblAddress.Text;
                // txtAddress2.Text = .txtAddress2.Text
                // txtCity.Text = .txtCity.Text
                // txtState.Text = .txtState.Text
                // txtZip.Text = .txtZip.Text
                // txtZip4.Text = .txtZip4.Text
                // txtTelephone.Text = .t2kPhone.Text
                if (Conversion.Val(frmPPMasterShort.InstancePtr.txtREAssociate.Text) > 0)
                {
                    txtRE.Text = frmPPMasterShort.InstancePtr.txtREAssociate.Text;
                }
                else
                {
                    txtRE.Visible = false;
                    txtRELabel.Visible = false;
                }

                txtOpen1Desc.Text = frmPPMasterShort.InstancePtr.lblOpen1;
                txtOpen2Desc.Text = frmPPMasterShort.InstancePtr.lblOpen2;
                txtOpen1.Text = frmPPMasterShort.InstancePtr.txtOpen1;
                txtOpen2.Text = frmPPMasterShort.InstancePtr.txtOpen2;
                txtExemption.Text = frmPPMasterShort.InstancePtr.txtExemption;
                // txtTranCode.Text = Val(.txtTranCode)
                txtTranCode.Text = frmPPMasterShort.InstancePtr.gridTranCode.TextMatrix(0, 0);
                txtBusinessCode.Text =
                    FCConvert.ToDouble(frmPPMasterShort.InstancePtr.txtBusinessCode.Text).ToStringES();
                txtStreetCode.Text = FCConvert.ToDouble(frmPPMasterShort.InstancePtr.txtStreetCode.Text).ToStringES();
                txtTotal.Text = frmPPMasterShort.InstancePtr.txtTotal;
                txtTotalTaxable.Text = frmPPMasterShort.InstancePtr.lblTotalValue.Text;
                txtTaxRate.Text = frmPPMasterShort.InstancePtr.lblTaxRate.Text;
                txtTax.Text = frmPPMasterShort.InstancePtr.lblTotalTax.Text;
                txtCat1Desc.Text = frmPPMasterShort.InstancePtr.lblCategory[0].Text;
                txtCat2Desc.Text = frmPPMasterShort.InstancePtr.lblCategory[1].Text;
                txtCat3Desc.Text = frmPPMasterShort.InstancePtr.lblCategory[2].Text;
                txtCat4Desc.Text = frmPPMasterShort.InstancePtr.lblCategory[3].Text;
                txtCat5Desc.Text = frmPPMasterShort.InstancePtr.lblCategory[4].Text;
                txtCat6Desc.Text = frmPPMasterShort.InstancePtr.lblCategory[5].Text;
                txtCat7Desc.Text = frmPPMasterShort.InstancePtr.lblCategory[6].Text;
                txtCat8Desc.Text = frmPPMasterShort.InstancePtr.lblCategory[7].Text;
                txtCat9Desc.Text = frmPPMasterShort.InstancePtr.lblCategory[8].Text;
                txtCat1.Text = frmPPMasterShort.InstancePtr.txtCategory1.Text;
                txtCat2.Text = frmPPMasterShort.InstancePtr.txtCategory2.Text;
                txtCat3.Text = frmPPMasterShort.InstancePtr.txtCategory3.Text;
                txtCat4.Text = frmPPMasterShort.InstancePtr.txtCategory4.Text;
                txtCat5.Text = frmPPMasterShort.InstancePtr.txtCategory5.Text;
                txtCat6.Text = frmPPMasterShort.InstancePtr.txtCategory6.Text;
                txtCat7.Text = frmPPMasterShort.InstancePtr.txtCategory7.Text;
                txtCat8.Text = frmPPMasterShort.InstancePtr.txtCategory8.Text;
                txtCat9.Text = frmPPMasterShort.InstancePtr.txtCategory9.Text;
                txtExempt1.Text = frmPPMasterShort.InstancePtr.txtExemptCode1.Text;
                txtExempt2.Text = frmPPMasterShort.InstancePtr.txtExemptCode2.Text;
                txtExempt1Desc.Text = "";
                txtExempt2Desc.Text = "";
                if (FCConvert.ToBoolean(Conversion.Val(txtExempt1.Text)) | (Conversion.Val(txtExempt2.Text)) > 0)
                {
                    clsLoad.OpenRecordset("select * from EXEMPTCODES order by code", "twpp0000.vb1");
                    if (Conversion.Val(txtExempt1.Text) > 0)
                    {
                        if (!clsLoad.EndOfFile())
                        {
                            clsLoad.FindFirstRecord("code", Conversion.Val(txtExempt1.Text));
                            if (!clsLoad.NoMatch)
                            {
                                txtExempt1Desc.Text =
                                    Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description")));
                            }
                        }
                    }

                    if (Conversion.Val(txtExempt2.Text) > 0)
                    {
                        if (!clsLoad.EndOfFile())
                        {
                            clsLoad.FindFirstRecord("code", Conversion.Val(txtExempt2.Text));
                            if (!clsLoad.NoMatch)
                            {
                                txtExempt2Desc.Text =
                                    Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description")));
                            }
                        }
                    }
                }
            }
        }

		
	}
}
