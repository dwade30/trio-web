﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmPPFileMaintenance.
	/// </summary>
	public partial class frmPPFileMaintenance : BaseForm
	{
		public frmPPFileMaintenance()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			cmbTrend.SelectedIndex = 1;
			cmbLeasedTrend.SelectedIndex = 1;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPPFileMaintenance InstancePtr
		{
			get
			{
				return (frmPPFileMaintenance)Sys.GetInstance(typeof(frmPPFileMaintenance));
			}
		}

		protected frmPPFileMaintenance _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int RoundOption;
		string Round1000 = "";
		int SROOption;
		bool boolLoading;
		int lngLastTownCode;
		clsPrintLabel labLabel = new clsPrintLabel();

		private void cmbLabelType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			string strDesc;
			int intIndex;
			if (cmbLabelType.SelectedIndex < 0)
				return;
			intIndex = labLabel.Get_IndexFromID(cmbLabelType.ItemData(cmbLabelType.SelectedIndex));
			strDesc = labLabel.Get_Description(intIndex);
			lblDescription.Text = strDesc;
		}

		private void FillLabelTypeCombo()
		{
			int x;
			for (x = 0; x <= labLabel.TypeCount - 1; x++)
			{
				if (labLabel.Get_ID(x) == modLabels.CNSTLBLTYPE4013)
				{
					labLabel.Set_Visible(x, true);
				}
				else if (labLabel.Get_ID(x) == modLabels.CNSTLBLTYPE4014)
				{
					labLabel.Set_Visible(x, true);
				}
				else if (labLabel.Get_ID(x) == modLabels.CNSTLBLTYPEDYMO30252)
				{
					labLabel.Set_Visible(x, true);
				}
				else if (labLabel.Get_ID(x) == modLabels.CNSTLBLTYPEDYMO30256)
				{
					labLabel.Set_Visible(x, true);
				}
				else
				{
					labLabel.Set_Visible(x, false);
				}
				if (labLabel.Get_Visible(x))
				{
					cmbLabelType.AddItem(labLabel.Get_Caption(x));
					cmbLabelType.ItemData(cmbLabelType.NewIndex, labLabel.Get_ID(x));
				}
			}
			// x
		}

		private void frmPPFileMaintenance_Activated(object sender, System.EventArgs e)
		{
			if (modGNWork.FormExist(this))
				return;
			// Call FillTheScreen
		}

		private void frmPPFileMaintenance_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				Close();
			}
		}

		private void frmPPFileMaintenance_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				frmPPFileMaintenance.InstancePtr.Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmPPFileMaintenance_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPPFileMaintenance properties;
			//frmPPFileMaintenance.ScaleWidth	= 9045;
			//frmPPFileMaintenance.ScaleHeight	= 7440;
			//frmPPFileMaintenance.LinkTopic	= "Form1";
			//frmPPFileMaintenance.LockControls	= true;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGridTownCode();
			if (modGlobal.Statics.CustomizeStuff.boolRegionalTown)
			{
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(1));
				modGlobal.Statics.CustomizeStuff.CurrentTown = 1;
			}
			else
			{
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
			}
			FillLabelTypeCombo();
			FillTheScreen();
		}

		private void frmPPFileMaintenance_Resize(object sender, System.EventArgs e)
		{
			ResizeGridTownCode();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			modGlobal.SaveWindowSize(this);
			// menuFileMaintenance.Show
		}

		private void SaveRoutine()
		{
			int ii;
			int intContingency = 0;
			double dblTaxRate;
			clsDRWrapper clsSave = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				// Call clsSave.OpenRecordset("select * from customize", "twpp0000.vb1")
				// If Not clsSave.EndOfFile Then
				// clsSave.Edit
				// Else
				// clsSave.AddNew
				// End If
				iLabel2:
				for (ii = 0; ii <= 3; ii++)
				{
					iLabel3:
					if (cmbRounding.SelectedIndex == ii)
						RoundOption = ii + 1;
					iLabel4:
					;
				}
				// ii
				iLabel5:
				for (ii = 0; ii <= 3; ii++)
				{
					iLabel6:
					if (cmbSRO.SelectedIndex == ii)
						SROOption = ii + 1;
					iLabel7:
					;
				}
				// ii
				if (chkShowOpen1OnCalc.CheckState == CheckState.Checked)
				{
					modGlobal.Statics.CustomizeStuff.ShowOpen1OnCalc = true;
				}
				else
				{
					modGlobal.Statics.CustomizeStuff.ShowOpen1OnCalc = false;
				}
				if (chkShowOpen2OnCalc.CheckState == CheckState.Checked)
				{
					modGlobal.Statics.CustomizeStuff.ShowOpen2OnCalc = true;
				}
				else
				{
					modGlobal.Statics.CustomizeStuff.ShowOpen2OnCalc = false;
				}
				if (chkRoundIndividually.CheckState == CheckState.Checked)
				{
					modGlobal.Statics.CustomizeStuff.RoundIndividualItems = true;
				}
				else
				{
					modGlobal.Statics.CustomizeStuff.RoundIndividualItems = false;
				}
				if (cmbLabelType.SelectedIndex >= 0)
				{
					modGlobal.Statics.CustomizeStuff.AccountLabelType = FCConvert.ToInt16(cmbLabelType.ItemData(cmbLabelType.SelectedIndex));
				}
				if (chkPercentGood.CheckState == CheckState.Checked)
				{
					// clsSave.Fields("LimitPercentGood") = True
					modGlobal.Statics.CustomizeStuff.LimitPercentGood = true;
				}
				else
				{
					// clsSave.Fields("LimitPercentGood") = False
					modGlobal.Statics.CustomizeStuff.LimitPercentGood = false;
				}
				if (cmbWhattodo.SelectedIndex == 0)
				{
					intContingency = 0;
				}
				else if (cmbWhattodo.SelectedIndex == 1)
				{
                    intContingency = 1;
                }
				else
				{
					intContingency = 2;
				}
                // 15    If chkTaxes.Value = 1 Then Call WPP.SetData("WSHOWRATEONSHORT", "Y") Else Call WPP.SetData("WSHOWRATEONSHORT", "N")
                //end switch
                // CustomizeStuff.TaxRate = dblTaxRate
                // If chkPreviousBETEYear.Value = vbChecked Then
                // CustomizeStuff.AllowPreviousBETEYear = True
                // Else
                // CustomizeStuff.AllowPreviousBETEYear = False
                // End If
                if (chkTaxes.CheckState == CheckState.Checked)
					modGlobal.Statics.CustomizeStuff.ShowRateOnShort = true;
				else
					modGlobal.Statics.CustomizeStuff.ShowRateOnShort = false;
				iLabel16:
				switch (RoundOption)
				{
					case 1:
						{
							// 17            Call WPP.SetData("WPPRound", 3)
							modGlobal.Statics.CustomizeStuff.Round = 3;
							break;
						}
					case 2:
						{
							// 18            Call WPP.SetData("WPPRound", 2)
							modGlobal.Statics.CustomizeStuff.Round = 2;
							break;
						}
					case 3:
						{
							// 19            Call WPP.SetData("WPPRound", 1)
							modGlobal.Statics.CustomizeStuff.Round = 1;
							break;
						}
					default:
						{
							// 20            Call WPP.SetData("WPPRound", 0)
							modGlobal.Statics.CustomizeStuff.Round = 0;
							break;
						}
				}
				if (Conversion.Val(txtTaxRate.Text) > 0)
				{
					dblTaxRate = modGlobal.RoundDbl(FCConvert.ToDouble(txtTaxRate.Text) / 1000, 6);
				}
				else
				{
					dblTaxRate = 0;
				}
				modGlobal.Statics.CustomizeStuff.BETEReimburseRate = Conversion.Val(txtBETEReimburseRate.Text) / 100;
				modGlobal.Statics.CustomizeStuff.DefaultYearFirstAssessed = FCConvert.ToInt32(Math.Round(Conversion.Val(txtDefaultYFA.Text)));
				modGlobal.Statics.CustomizeStuff.RoundContingency = FCConvert.ToInt32(intContingency);
				modGlobal.Statics.CustomizeStuff.DepreciationYear = FCConvert.ToInt32(Math.Round(Conversion.Val(txtDepreciation.Text)));
				iLabel25:
				modPPCalculate.Statics.DepYear = FCConvert.ToInt32(Math.Round(Conversion.Val(txtDepreciation.Text + "")));
				modGlobal.Statics.CustomizeStuff.CurrentBETEYear = FCConvert.ToInt32(Math.Round(Conversion.Val(txtBETEYear.Text)));
				if (cmbTrend.SelectedIndex == 0)
				{
					// multiply by cost
					modGlobal.Statics.CustomizeStuff.TrendTimesCost = true;
				}
				else
				{
					// multiply by percent good
					modGlobal.Statics.CustomizeStuff.TrendTimesCost = false;
				}
				if (cmbLeasedTrend.SelectedIndex == 0)
				{
					modGlobal.Statics.CustomizeStuff.LeasedTrendTimesCost = true;
				}
				else
				{
					modGlobal.Statics.CustomizeStuff.LeasedTrendTimesCost = false;
				}
				if (chkShowOriginalTotals.CheckState == CheckState.Checked)
				{
					modGlobal.Statics.CustomizeStuff.ShowOriginalTotals = true;
				}
				else
				{
					modGlobal.Statics.CustomizeStuff.ShowOriginalTotals = false;
				}
				if (chkBETEinExtract.CheckState == CheckState.Checked)
				{
					modGlobal.Statics.CustomizeStuff.UserExtractIncBETE = true;
				}
				else
				{
					modGlobal.Statics.CustomizeStuff.UserExtractIncBETE = false;
				}
				modGlobal.Statics.CustomizeStuff.SaveCustomInfo();
				iLabel1:
				modGNWork.GetLocalVariables2();
				MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //FC:FINAL:SBE - #4617 - reload navigation menu, after settings was changed
                App.MainForm.ReloadNavigationMenu();
                return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + " In Save Rountine. Line number " + Information.Erl(), null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtMunicipalCode_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolLoading)
			{
				if (Conversion.Val(txtMunicipalCode.Text) > 0)
				{
					modGlobal.Statics.CustomizeStuff.MunicipalCode = FCConvert.ToInt16(txtMunicipalCode.Text);
				}
				else
				{
					modGlobal.Statics.CustomizeStuff.MunicipalCode = 0;
				}
			}
		}

		private void FillTheScreen()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngTemp;
			boolLoading = true;
			modGNWork.GetLocalVariables2();
			for (lngTemp = 0; lngTemp <= cmbLabelType.Items.Count - 1; lngTemp++)
			{
				if (cmbLabelType.ItemData(lngTemp) == modGlobal.Statics.CustomizeStuff.AccountLabelType)
				{
					cmbLabelType.SelectedIndex = lngTemp;
					break;
				}
			}
			// lngTemp
			clsLoad.OpenRecordset("select count(account) as thecount from ppmaster", "twpp0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
				if (!fecherFoundation.FCUtils.IsNull(clsLoad.Get_Fields("thecount")))
				{
					// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
					lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("thecount") + "")));
					lblTotalAccounts.Text = FCConvert.ToString(lngTemp);
					if (lngTemp > 0)
					{
						clsLoad.OpenRecordset("select count(account) as thecount from ppmaster where not deleted = 1", "twpp0000.vb1");
						// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
						lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("thecount") + "")));
						lblActiveAccounts.Text = FCConvert.ToString(lngTemp);
					}
					else
					{
						lblActiveAccounts.Text = FCConvert.ToString(0);
					}
				}
				else
				{
					lblTotalAccounts.Text = FCConvert.ToString(0);
					lblActiveAccounts.Text = FCConvert.ToString(0);
				}
			}
			else
			{
				lblTotalAccounts.Text = FCConvert.ToString(0);
				lblActiveAccounts.Text = FCConvert.ToString(0);
			}
			// If CustomizeStuff.AllowPreviousBETEYear Then
			// chkPreviousBETEYear.Value = vbChecked
			// Else
			// chkPreviousBETEYear.Value = vbUnchecked
			// End If
			if (modGlobal.Statics.CustomizeStuff.ShowOpen1OnCalc)
			{
				chkShowOpen1OnCalc.CheckState = CheckState.Checked;
			}
			else
			{
				chkShowOpen1OnCalc.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (modGlobal.Statics.CustomizeStuff.ShowOpen2OnCalc)
			{
				chkShowOpen2OnCalc.CheckState = CheckState.Checked;
			}
			else
			{
				chkShowOpen2OnCalc.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (modGlobal.Statics.CustomizeStuff.RoundIndividualItems)
			{
				chkRoundIndividually.CheckState = CheckState.Checked;
			}
			else
			{
				chkRoundIndividually.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			txtBETEYear.Text = FCConvert.ToString(modGlobal.Statics.CustomizeStuff.CurrentBETEYear);
			txtBETEReimburseRate.Text = FCConvert.ToString(modGlobal.Statics.CustomizeStuff.BETEReimburseRate * 100);
			if (modGlobal.Statics.CustomizeStuff.LimitPercentGood)
			{
				chkPercentGood.CheckState = CheckState.Checked;
			}
			else
			{
				chkPercentGood.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (modGlobal.Statics.CustomizeStuff.TrendTimesCost)
			{
				cmbTrend.SelectedIndex = 0;
			}
			else
			{
				cmbTrend.SelectedIndex = 1;
			}
			if (modGlobal.Statics.CustomizeStuff.LeasedTrendTimesCost)
			{
				cmbLeasedTrend.SelectedIndex = 0;
			}
			else
			{
				cmbLeasedTrend.SelectedIndex = 1;
			}
			if (modGlobal.Statics.CustomizeStuff.ShowRateOnShort)
			{
				chkTaxes.CheckState = CheckState.Checked;
			}
			else
			{
				chkTaxes.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (modGlobal.Statics.CustomizeStuff.UserExtractIncBETE)
			{
				chkBETEinExtract.CheckState = CheckState.Checked;
			}
			else
			{
				chkBETEinExtract.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			modGlobal.Statics.CustomizeStuff.CurrentTown = FCConvert.ToInt32(gridTownCode.TextMatrix(0, 0));
			if (modGlobal.Statics.CustomizeStuff.boolRegionalTown && modGlobal.Statics.CustomizeStuff.CurrentTown == 0)
			{
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(1));
			}
			if (Conversion.Val(modGlobal.Statics.CustomizeStuff.TaxRate) > 0)
			{
				txtTaxRate.Text = Strings.Format(modGlobal.Statics.CustomizeStuff.TaxRate * 1000, "##0.000");
			}
			else
			{
				txtTaxRate.Text = "0.000";
			}
			txtMunicipalCode.Text = FCConvert.ToString(modGlobal.Statics.CustomizeStuff.MunicipalCode);
			// If CustomizeStuff.RoundContingency = "" Then
			// optWhattodo(RoundContingency).Value = True
			// Else
			switch (FCConvert.ToInt32(Conversion.Val(FCConvert.ToString(modGlobal.Statics.CustomizeStuff.RoundContingency) + "")))
			{
				case 0:
					{
						cmbWhattodo.SelectedIndex = 0;
						Round1000 = "Y";
						break;
					}
				case 1:
					{
						cmbWhattodo.SelectedIndex = 1;
						Round1000 = "N";
						break;
					}
				case 2:
					{
						cmbWhattodo.SelectedIndex = 2;
						Round1000 = "N";
						break;
					}
			}
			//end switch
			// End If
			switch (modGlobal.Statics.CustomizeStuff.Round)
			{
				case 1:
					{
						cmbRounding.SelectedIndex = 2;
						break;
					}
				case 2:
					{
						cmbRounding.SelectedIndex = 1;
						break;
					}
				case 3:
					{
						cmbRounding.SelectedIndex = 0;
						break;
					}
				default:
					{
						cmbRounding.SelectedIndex = 3;
						break;
					}
			}
			//end switch
			if (modGlobal.Statics.CustomizeStuff.ShowOriginalTotals)
			{
				chkShowOriginalTotals.CheckState = CheckState.Checked;
			}
			else
			{
				chkShowOriginalTotals.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (modGlobal.Statics.CustomizeStuff.DepreciationYear == 0)
			{
				txtDepreciation.Text = "0000";
			}
			else
			{
				txtDepreciation.Text = FCConvert.ToString(modGlobal.Statics.CustomizeStuff.DepreciationYear);
			}
			txtDefaultYFA.Text = FCConvert.ToString(modGlobal.Statics.CustomizeStuff.DefaultYearFirstAssessed);
			boolLoading = false;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			//- frmPPFileMaintenance.Close();
			//FC:FINAL:MSH - restore unloading of the form after saving data by using F12 shortcut
			this.Unload();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveRoutine();
			// Unload frmPPFileMaintenance
		}

		public void mnuSave_Click()
		{
			mnuSave_Click(mnuSave, new System.EventArgs());
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			mnuSave_Click();
            //FC:FINAL: AKV form closing
           // mnuExit_Click();
		}

		private void optWhattodo_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			if (Index == 0)
			{
				Round1000 = "Y";
			}
			else
			{
				Round1000 = "N";
			}

            //FC:FINAL:CHN - issue #1554: Add changing Examples labels visible.
            if (Index == 0)
            {
                this.Label2.Visible = true;
                this.Label3.Visible = false;
            }
            else if (Index == 1)
            {
                this.Label2.Visible = false;
                this.Label3.Visible = true;
            }
            else
            {
                this.Label2.Visible = false;
                this.Label3.Visible = false;
            }
		}

		private void optWhattodo_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = FCConvert.ToInt16(cmbWhattodo.SelectedIndex);
			optWhattodo_CheckedChanged(index, sender, e);
		}

		private void txtDepreciation_Leave(object sender, System.EventArgs e)
		{
			if (Conversion.Val(txtDepreciation.Text) == 0)
			{
				txtDepreciation.Text = "0000";
			}
			else if (Conversion.Val(txtDepreciation.Text) < 1980)
			{
				MessageBox.Show("The depreciation year must be higher than 1979.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDepreciation.Text = "";
				txtDepreciation.Focus();
			}
			else if (Conversion.Val(txtDepreciation.Text) > DateTime.Now.Year + 1)
			{
				MessageBox.Show("The depreciation year cannot be more than a year from now.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDepreciation.Text = "";
				txtDepreciation.Focus();
			}
		}

		private void gridTownCode_ComboCloseUp(object sender, EventArgs e)
		{
			// CustomizeStuff.TaxRate = Val(txtTaxRate.Text)
			if (!boolLoading)
			{
				if (modGlobal.Statics.CustomizeStuff.boolRegionalTown)
				{
					modGlobal.Statics.CustomizeStuff.CurrentTown = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.ComboData())));
				}
				// CustomizeStuff.TaxRate = RoundDbl(CDbl(txtTaxRate.Text) / 1000, 6)
				txtTaxRate.Text = FCConvert.ToString(1000 * modGlobal.Statics.CustomizeStuff.TaxRate);
				////Application.DoEvents();
				txtTaxRate.Focus();
				////Application.DoEvents();
			}
		}

		private void gridTownCode_Leave(object sender, System.EventArgs e)
		{
			////Application.DoEvents();
			if (!boolLoading)
			{
				if (lngLastTownCode == Conversion.Val(gridTownCode.TextMatrix(0, 0)))
					return;
				LoadTownSpecificStuff();
				lngLastTownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
			}
		}

		private void LoadTownSpecificStuff()
		{
			int lngTownCode;
			lngTownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
			modGlobal.Statics.CustomizeStuff.CurrentTown = lngTownCode;
			txtTaxRate.Text = Strings.Format(modGlobal.Statics.CustomizeStuff.TaxRate * 1000, "##0.000");
		}

		private void SetupGridTownCode()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			if (modGlobal.Statics.CustomizeStuff.boolRegionalTown)
			{
				strTemp = "";
				clsLoad.OpenRecordset("select * from TranCodes where trancode > 0 order by trancode", modPPGN.strPPDatabase);
				while (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [trancode] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [trancode] and replace with corresponding Get_Field method
					strTemp += "#" + clsLoad.Get_Fields("trancode") + ";" + clsLoad.Get_Fields_String("trantype") + "\t" + clsLoad.Get_Fields("trancode") + "|";
					clsLoad.MoveNext();
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				gridTownCode.ColComboList(0, strTemp);
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
				gridTownCode.Visible = true;
			}
			else
			{
				gridTownCode.Rows = 1;
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
				gridTownCode.Visible = false;
			}
		}

		private void ResizeGridTownCode()
		{
			//gridTownCode.HeightOriginal = gridTownCode.RowHeight(0) + 60;
		}

		private void txtTaxRate_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolLoading)
			{
				if (Conversion.Val(txtTaxRate.Text) > 0)
				{
					modGlobal.Statics.CustomizeStuff.TaxRate = modGlobal.RoundDbl(FCConvert.ToDouble(txtTaxRate.Text) / 1000, 6);
				}
				else
				{
					modGlobal.Statics.CustomizeStuff.TaxRate = 0;
				}
			}
		}

		private void cmbWhattodo_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbWhattodo.SelectedIndex == 0)
			{
				optWhattodo_CheckedChanged(sender, e);
			}
			else if (cmbWhattodo.SelectedIndex == 1)
			{
				optWhattodo_CheckedChanged(sender, e);
			}
			else if (cmbWhattodo.SelectedIndex == 2)
			{
				optWhattodo_CheckedChanged(sender, e);
			}
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuSave_Click(mnuSave, EventArgs.Empty);
		}
	}
}
