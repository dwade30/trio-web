﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rpt801b.
	/// </summary>
	public partial class rpt801b : BaseSectionReport
	{
		public rpt801b()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "801B";
		}

		public static rpt801b InstancePtr
		{
			get
			{
				return (rpt801b)Sys.GetInstance(typeof(rpt801b));
			}
		}

		protected rpt801b _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rpt801b	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private c801Master m801;
		private double dblTotalAllowedValue;

		public object SetReportObject(c801Master rep801)
		{
			object SetReportObject = null;
			m801 = rep801;
			return SetReportObject;
		}

		public void Init(ref c801Master rep801)
		{
			m801 = rep801;
			frmReportViewer.InstancePtr.Init(this);
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			ClearReport();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!(m801 == null))
			{
				int intTaxYear = 0;
				txtMuniName.Text = m801.Municipality;
				txtApplicant.Text = m801.ApplicantName;
				txtLocation.Text = m801.LocatedAt;
				txtMuniCode.Text = m801.MunicipalCode;
				txtTaxRate.Text = m801.DisplayedRate;
				txtAsOfYear.Text = FCConvert.ToString(m801.TaxYear);
				txtDate.Text = m801.ReportDate;
				txtMunicipalityName.Text = m801.Municipality;
				intTaxYear = m801.TaxYear;
				lblTaxYear.Text = FCConvert.ToString(intTaxYear);
				lblAssessedDate.Text = "April 1, " + FCConvert.ToString(intTaxYear);
				FillOriginalYears(intTaxYear);
				c801BDetail det801;
				if (m801.DetailsB.ItemCount() > 0)
				{
					m801.DetailsB.MoveFirst();
					while (m801.DetailsB.IsCurrent())
					{
						det801 = (c801BDetail)m801.DetailsB.GetCurrentItem();
						FillYearClaimed(ref det801);
						m801.DetailsB.MoveNext();
					}
				}
				// theReport.Records.MoveNext
			}
		}

		private void FillOriginalYears(int intYear)
		{
			// vbPorter upgrade warning: strYear As string	OnWriteFCConvert.ToInt32(
			string strYear;
			// vbPorter upgrade warning: intBaseYear As short --> As int	OnRead(string)
			int intBaseYear;
			intBaseYear = intYear - 13 + 1;
			strYear = FCConvert.ToString(intBaseYear);
			lblAssessmentYearFurniture1.Text = strYear;
			lblAssessmentYearME1.Text = strYear;
			lblAssessmentYearOther1.Text = strYear;
			strYear = FCConvert.ToString(intBaseYear - 1);
			lblAssessmentYearFurniture2.Text = strYear;
			lblAssessmentYearME2.Text = strYear;
			lblAssessmentYearOther2.Text = strYear;
			strYear = FCConvert.ToString(intBaseYear - 2);
			lblAssessmentYearFurniture3.Text = strYear;
			lblAssessmentYearME3.Text = strYear;
			lblAssessmentYearOther3.Text = strYear;
			strYear = FCConvert.ToString(intBaseYear - 3);
			lblAssessmentYearFurniture4.Text = strYear;
			lblAssessmentYearME4.Text = strYear;
			lblAssessmentYearOther4.Text = strYear;
			strYear = FCConvert.ToString(intBaseYear - 4);
			lblAssessmentYearFurniture5.Text = strYear;
			lblAssessmentYearME5.Text = strYear;
			lblAssessmentYearOther5.Text = strYear;
			strYear = FCConvert.ToString(intBaseYear - 5);
			lblAssessmentYearFurniture6.Text = strYear;
			lblAssessmentYearME6.Text = strYear;
			lblAssessmentYearOther6.Text = strYear;
		}

		private void ClearReport()
		{
			dblTotalAllowedValue = 0;
			txtAllowedValue1.Text = "";
			txtAllowedValue2.Text = "";
			txtAllowedValue3.Text = "";
			txtAllowedValue4.Text = "";
			txtAllowedValue5.Text = "";
			txtAllowedValue6.Text = "";
			txtAssessedFurniture1.Text = "";
			txtAssessedFurniture2.Text = "";
			txtAssessedFurniture3.Text = "";
			txtAssessedFurniture4.Text = "";
			txtAssessedFurniture5.Text = "";
			txtAssessedFurniture6.Text = "";
			txtAssessedME1.Text = "";
			txtAssessedME2.Text = "";
			txtAssessedME3.Text = "";
			txtAssessedME4.Text = "";
			txtAssessedME5.Text = "";
			txtAssessedME6.Text = "";
			txtAssessedOther1.Text = "";
			txtAssessedOther2.Text = "";
			txtAssessedOther3.Text = "";
			txtAssessedOther4.Text = "";
			txtAssessedOther5.Text = "";
			txtAssessedOther6.Text = "";
			txtAssessedTotal1.Text = "";
			txtAssessedTotal2.Text = "";
			txtAssessedTotal3.Text = "";
			txtAssessedTotal4.Text = "";
			txtAssessedTotal5.Text = "";
			txtAssessedTotal6.Text = "";
			txtOriginalFurniture1.Text = "";
			txtOriginalFurniture2.Text = "";
			txtOriginalFurniture3.Text = "";
			txtOriginalFurniture4.Text = "";
			txtOriginalFurniture5.Text = "";
			txtOriginalFurniture6.Text = "";
			txtOriginalME1.Text = "";
			txtOriginalME2.Text = "";
			txtOriginalME3.Text = "";
			txtOriginalME4.Text = "";
			txtOriginalME5.Text = "";
			txtOriginalME6.Text = "";
			txtOriginalOther1.Text = "";
			txtOriginalOther2.Text = "";
			txtOriginalOther3.Text = "";
			txtOriginalOther4.Text = "";
			txtOriginalOther5.Text = "";
			txtOriginalOther6.Text = "";
			txtTotalOriginalCost1.Text = "";
			txtTotalOriginalCost2.Text = "";
			txtTotalOriginalCost3.Text = "";
			txtTotalOriginalCost4.Text = "";
			txtTotalOriginalCost5.Text = "";
			txtTotalOriginalCost6.Text = "";
		}

		private void FillYearClaimed(ref c801BDetail detRecord)
		{
			double dblTotal = 0;
			double dblAllowable = 0;
			double dblAssessedTotal = 0;
			switch (detRecord.YearsClaimed)
			{
				case 13:
					{
						txtOriginalME1.Text = Strings.Format(detRecord.MachineryAndEquipment, "#,###,###,##0");
						txtOriginalFurniture1.Text = Strings.Format(detRecord.Furniture, "#,###,###,##0");
						txtOriginalOther1.Text = Strings.Format(detRecord.Other, "#,###,###,##0");
						txtAssessedFurniture1.Text = Strings.Format(detRecord.FurnitureAssessed, "#,###,###,##0");
						txtAssessedME1.Text = Strings.Format(detRecord.MachineryAndEquimpmentAssessed, "#,###,###,##0");
						txtAssessedOther1.Text = Strings.Format(detRecord.OtherAssessed, "#,###,###,##0");
						dblTotal = detRecord.MachineryAndEquipment + detRecord.Furniture + detRecord.Other;
						dblAssessedTotal = detRecord.MachineryAndEquimpmentAssessed + detRecord.FurnitureAssessed + detRecord.OtherAssessed;
						txtTotalOriginalCost1.Text = Strings.Format(dblTotal, "#,###,###,##0");
						txtAssessedTotal1.Text = Strings.Format(dblAssessedTotal, "#,###,###,##0");
						dblAllowable = dblAssessedTotal * 0.75;
						txtAllowedValue1.Text = Strings.Format(dblAllowable, "#,###,###,##0");
						break;
					}
				case 14:
					{
						txtOriginalME2.Text = Strings.Format(detRecord.MachineryAndEquipment, "#,###,###,##0");
						txtOriginalFurniture2.Text = Strings.Format(detRecord.Furniture, "#,###,###,##0");
						txtOriginalOther2.Text = Strings.Format(detRecord.Other, "#,###,###,##0");
						txtAssessedFurniture2.Text = Strings.Format(detRecord.FurnitureAssessed, "#,###,###,##0");
						txtAssessedME2.Text = Strings.Format(detRecord.MachineryAndEquimpmentAssessed, "#,###,###,##0");
						txtAssessedOther2.Text = Strings.Format(detRecord.OtherAssessed, "#,###,###,##0");
						dblTotal = detRecord.MachineryAndEquipment + detRecord.Furniture + detRecord.Other;
						dblAssessedTotal = detRecord.MachineryAndEquimpmentAssessed + detRecord.FurnitureAssessed + detRecord.OtherAssessed;
						txtTotalOriginalCost2.Text = Strings.Format(dblTotal, "#,###,###,##0");
						txtAssessedTotal2.Text = Strings.Format(dblAssessedTotal, "#,###,###,##0");
						dblAllowable = dblAssessedTotal * 0.7;
						txtAllowedValue2.Text = Strings.Format(dblAllowable, "#,###,###,##0");
						break;
					}
				case 15:
					{
						txtOriginalME3.Text = Strings.Format(detRecord.MachineryAndEquipment, "#,###,###,##0");
						txtOriginalFurniture3.Text = Strings.Format(detRecord.Furniture, "#,###,###,##0");
						txtOriginalOther3.Text = Strings.Format(detRecord.Other, "#,###,###,##0");
						txtAssessedFurniture3.Text = Strings.Format(detRecord.FurnitureAssessed, "#,###,###,##0");
						txtAssessedME3.Text = Strings.Format(detRecord.MachineryAndEquimpmentAssessed, "#,###,###,##0");
						txtAssessedOther3.Text = Strings.Format(detRecord.OtherAssessed, "#,###,###,##0");
						dblTotal = detRecord.MachineryAndEquipment + detRecord.Furniture + detRecord.Other;
						dblAssessedTotal = detRecord.MachineryAndEquimpmentAssessed + detRecord.FurnitureAssessed + detRecord.OtherAssessed;
						txtTotalOriginalCost3.Text = Strings.Format(dblTotal, "#,###,###,##0");
						txtAssessedTotal3.Text = Strings.Format(dblAssessedTotal, "#,###,###,##0");
						dblAllowable = dblAssessedTotal * 0.65;
						txtAllowedValue3.Text = Strings.Format(dblAllowable, "#,###,###,##0");
						break;
					}
				case 16:
					{
						txtOriginalME4.Text = Strings.Format(detRecord.MachineryAndEquipment, "#,###,###,##0");
						txtOriginalFurniture4.Text = Strings.Format(detRecord.Furniture, "#,###,###,##0");
						txtOriginalOther4.Text = Strings.Format(detRecord.Other, "#,###,###,##0");
						txtAssessedFurniture4.Text = Strings.Format(detRecord.FurnitureAssessed, "#,###,###,##0");
						txtAssessedME4.Text = Strings.Format(detRecord.MachineryAndEquimpmentAssessed, "#,###,###,##0");
						txtAssessedOther4.Text = Strings.Format(detRecord.OtherAssessed, "#,###,###,##0");
						dblTotal = detRecord.MachineryAndEquipment + detRecord.Furniture + detRecord.Other;
						dblAssessedTotal = detRecord.MachineryAndEquimpmentAssessed + detRecord.FurnitureAssessed + detRecord.OtherAssessed;
						txtTotalOriginalCost4.Text = Strings.Format(dblTotal, "#,###,###,##0");
						txtAssessedTotal4.Text = Strings.Format(dblAssessedTotal, "#,###,###,##0");
						dblAllowable = dblAssessedTotal * 0.6;
						txtAllowedValue4.Text = Strings.Format(dblAllowable, "#,###,###,##0");
						break;
					}
				case 17:
					{
						txtOriginalME5.Text = Strings.Format(detRecord.MachineryAndEquipment, "#,###,###,##0");
						txtOriginalFurniture5.Text = Strings.Format(detRecord.Furniture, "#,###,###,##0");
						txtOriginalOther5.Text = Strings.Format(detRecord.Other, "#,###,###,##0");
						txtAssessedFurniture5.Text = Strings.Format(detRecord.FurnitureAssessed, "#,###,###,##0");
						txtAssessedME5.Text = Strings.Format(detRecord.MachineryAndEquimpmentAssessed, "#,###,###,##0");
						txtAssessedOther5.Text = Strings.Format(detRecord.OtherAssessed, "#,###,###,##0");
						dblTotal = detRecord.MachineryAndEquipment + detRecord.Furniture + detRecord.Other;
						dblAssessedTotal = detRecord.MachineryAndEquimpmentAssessed + detRecord.FurnitureAssessed + detRecord.OtherAssessed;
						txtTotalOriginalCost5.Text = Strings.Format(dblTotal, "#,###,###,##0");
						txtAssessedTotal5.Text = Strings.Format(dblAssessedTotal, "#,###,###,##0");
						dblAllowable = dblAssessedTotal * 0.55;
						txtAllowedValue5.Text = Strings.Format(dblAllowable, "#,###,###,##0");
						break;
					}
				case 18:
					{
						txtOriginalME6.Text = Strings.Format(detRecord.MachineryAndEquipment, "#,###,###,##0");
						txtOriginalFurniture6.Text = Strings.Format(detRecord.Furniture, "#,###,###,##0");
						txtOriginalOther6.Text = Strings.Format(detRecord.Other, "#,###,###,##0");
						txtAssessedFurniture6.Text = Strings.Format(detRecord.FurnitureAssessed, "#,###,###,##0");
						txtAssessedME6.Text = Strings.Format(detRecord.MachineryAndEquimpmentAssessed, "#,###,###,##0");
						txtAssessedOther6.Text = Strings.Format(detRecord.OtherAssessed, "#,###,###,##0");
						dblTotal = detRecord.MachineryAndEquipment + detRecord.Furniture + detRecord.Other;
						dblAssessedTotal = detRecord.MachineryAndEquimpmentAssessed + detRecord.FurnitureAssessed + detRecord.OtherAssessed;
						txtTotalOriginalCost6.Text = Strings.Format(dblTotal, "#,###,###,##0");
						txtAssessedTotal6.Text = Strings.Format(dblAssessedTotal, "#,###,###,##0");
						dblAllowable = dblAssessedTotal * 0.5;
						txtAllowedValue6.Text = Strings.Format(dblAllowable, "#,###,###,##0");
						break;
					}
			}
			//end switch
			dblTotalAllowedValue += dblAllowable;
			txtTotalAllowedValue.Text = Strings.Format(dblTotalAllowedValue, "#,###,###,##0");
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: dblAssessedTax As double	OnWrite(string)
			double dblAssessedTax = 0;
			if (m801.TaxRate > 0)
			{
				dblAssessedTax = FCConvert.ToDouble(Strings.Format(dblTotalAllowedValue * m801.TaxRate, "0.00"));
				txtAssessedTax.Text = Strings.Format(dblAssessedTax, "#,###,###,##0.00");
			}
			else
			{
				txtAssessedTax.Text = "";
			}
		}

		
	}
}
