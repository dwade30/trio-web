﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;
using fecherFoundation.VisualBasicLayer;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmPrintSummary.
	/// </summary>
	partial class frmPrintSummary : BaseForm
	{
		public fecherFoundation.FCComboBox cmbType;
		public fecherFoundation.FCLabel lblType;
		public fecherFoundation.FCComboBox cmbRange;
		public fecherFoundation.FCLabel lblRange;
		public fecherFoundation.FCComboBox cmbSequence;
		public fecherFoundation.FCLabel lblSequence;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtLow;
		public fecherFoundation.FCTextBox txtHigh;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCButton cmdQuit;
		public fecherFoundation.FCButton cmdProcess;
		public fecherFoundation.FCTabControl sstPPCalculate;
		public fecherFoundation.FCTabPage sstPPCalculate_Page1;
		public fecherFoundation.FCGrid vsLeased;
		public fecherFoundation.FCTabPage sstPPCalculate_Page2;
		public fecherFoundation.FCGrid vsItemized;
		public fecherFoundation.FCTabPage sstPPCalculate_Page3;
		public fecherFoundation.FCLabel lblOverride;
		public fecherFoundation.FCGrid vsValuations;
		public fecherFoundation.FCGrid vsDisplay;
		public fecherFoundation.FCCheckBox Check1;
		public fecherFoundation.FCTextBox txtOverride;
		public fecherFoundation.FCLabel lblName;
		public fecherFoundation.FCLabel lblLocationName;
		public fecherFoundation.FCLabel lblLocation;
		public fecherFoundation.FCLabel lblLocationTitle;
		public fecherFoundation.FCLabel lblAccount;
		public fecherFoundation.FCLabel lblAccountTitle;
		public fecherFoundation.FCLabel lblNameTitle;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle5 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle6 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle7 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle8 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrintSummary));
			this.cmbType = new fecherFoundation.FCComboBox();
			this.lblType = new fecherFoundation.FCLabel();
			this.cmbRange = new fecherFoundation.FCComboBox();
			this.lblRange = new fecherFoundation.FCLabel();
			this.cmbSequence = new fecherFoundation.FCComboBox();
			this.lblSequence = new fecherFoundation.FCLabel();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.txtLow = new fecherFoundation.FCTextBox();
			this.txtHigh = new fecherFoundation.FCTextBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.cmdQuit = new fecherFoundation.FCButton();
			this.cmdProcess = new fecherFoundation.FCButton();
			this.sstPPCalculate = new fecherFoundation.FCTabControl();
			this.sstPPCalculate_Page1 = new fecherFoundation.FCTabPage();
			this.vsLeased = new fecherFoundation.FCGrid();
			this.sstPPCalculate_Page2 = new fecherFoundation.FCTabPage();
			this.vsItemized = new fecherFoundation.FCGrid();
			this.sstPPCalculate_Page3 = new fecherFoundation.FCTabPage();
			this.lblOverride = new fecherFoundation.FCLabel();
			this.vsValuations = new fecherFoundation.FCGrid();
			this.vsDisplay = new fecherFoundation.FCGrid();
			this.Check1 = new fecherFoundation.FCCheckBox();
			this.txtOverride = new fecherFoundation.FCTextBox();
			this.lblName = new fecherFoundation.FCLabel();
			this.lblLocationName = new fecherFoundation.FCLabel();
			this.lblLocation = new fecherFoundation.FCLabel();
			this.lblLocationTitle = new fecherFoundation.FCLabel();
			this.lblAccount = new fecherFoundation.FCLabel();
			this.lblAccountTitle = new fecherFoundation.FCLabel();
			this.lblNameTitle = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintPreview = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdPrintPreview = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
			this.sstPPCalculate.SuspendLayout();
			this.sstPPCalculate_Page1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsLeased)).BeginInit();
			this.sstPPCalculate_Page2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsItemized)).BeginInit();
			this.sstPPCalculate_Page3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsValuations)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsDisplay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Check1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdPrintPreview);
			this.BottomPanel.Location = new System.Drawing.Point(0, 523);
			this.BottomPanel.Size = new System.Drawing.Size(1098, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.cmdQuit);
			this.ClientArea.Controls.Add(this.cmdProcess);
			this.ClientArea.Controls.Add(this.sstPPCalculate);
			this.ClientArea.Controls.Add(this.lblName);
			this.ClientArea.Controls.Add(this.lblLocationName);
			this.ClientArea.Controls.Add(this.lblLocation);
			this.ClientArea.Controls.Add(this.lblLocationTitle);
			this.ClientArea.Controls.Add(this.lblAccount);
			this.ClientArea.Controls.Add(this.lblAccountTitle);
			this.ClientArea.Controls.Add(this.lblNameTitle);
			this.ClientArea.Size = new System.Drawing.Size(1098, 463);
			this.ClientArea.TabIndex = 0;
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(1098, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(185, 30);
			this.HeaderText.Text = "Batch Calculate";
			// 
			// cmbType
			// 
			this.cmbType.AutoSize = false;
			this.cmbType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbType.FormattingEnabled = true;
			this.cmbType.Items.AddRange(new object[] {
				"Summary",
				"Item Detail"
			});
			this.cmbType.Location = new System.Drawing.Point(154, 30);
			this.cmbType.Name = "cmbType";
			this.cmbType.Size = new System.Drawing.Size(206, 40);
			this.cmbType.TabIndex = 1;
			this.cmbType.Text = "Summary";
			// 
			// lblType
			// 
			this.lblType.AutoSize = true;
			this.lblType.Location = new System.Drawing.Point(20, 44);
			this.lblType.Name = "lblType";
			this.lblType.Size = new System.Drawing.Size(40, 15);
			this.lblType.TabIndex = 0;
			this.lblType.Text = "TYPE";
			// 
			// cmbRange
			// 
			this.cmbRange.AutoSize = false;
			this.cmbRange.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbRange.FormattingEnabled = true;
			this.cmbRange.Items.AddRange(new object[] {
				"All",
				"Range"
			});
			this.cmbRange.Location = new System.Drawing.Point(154, 150);
			this.cmbRange.Name = "cmbRange";
			this.cmbRange.Size = new System.Drawing.Size(206, 40);
			this.cmbRange.TabIndex = 5;
			this.cmbRange.Text = "All";
			this.cmbRange.SelectedIndexChanged += new System.EventHandler(this.cmbRange_SelectedIndexChanged);
			// 
			// lblRange
			// 
			this.lblRange.AutoSize = true;
			this.lblRange.Location = new System.Drawing.Point(20, 164);
			this.lblRange.Name = "lblRange";
			this.lblRange.Size = new System.Drawing.Size(51, 15);
			this.lblRange.TabIndex = 4;
			this.lblRange.Text = "RANGE";
			// 
			// cmbSequence
			// 
			this.cmbSequence.AutoSize = false;
			this.cmbSequence.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbSequence.FormattingEnabled = true;
			this.cmbSequence.Items.AddRange(new object[] {
				"Account",
				"Name",
				"Location",
				"Business Code",
				"Open 1",
				"Open 2"
			});
			this.cmbSequence.Location = new System.Drawing.Point(154, 90);
			this.cmbSequence.Name = "cmbSequence";
			this.cmbSequence.Size = new System.Drawing.Size(206, 40);
			this.cmbSequence.TabIndex = 3;
			this.cmbSequence.Text = "Account";
			// 
			// lblSequence
			// 
			this.lblSequence.AutoSize = true;
			this.lblSequence.Location = new System.Drawing.Point(20, 104);
			this.lblSequence.Name = "lblSequence";
			this.lblSequence.Size = new System.Drawing.Size(77, 15);
			this.lblSequence.TabIndex = 2;
			this.lblSequence.Text = "SEQUENCE";
			// 
			// Frame1
			// 
			this.Frame1.AppearanceKey = "groupBoxNoBorders";
			this.Frame1.Controls.Add(this.cmbRange);
			this.Frame1.Controls.Add(this.lblRange);
			this.Frame1.Controls.Add(this.cmbType);
			this.Frame1.Controls.Add(this.lblType);
			this.Frame1.Controls.Add(this.cmbSequence);
			this.Frame1.Controls.Add(this.lblSequence);
			this.Frame1.Controls.Add(this.txtLow);
			this.Frame1.Controls.Add(this.txtHigh);
			this.Frame1.Controls.Add(this.Label2);
			this.Frame1.Location = new System.Drawing.Point(10, 0);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(473, 267);
			this.Frame1.TabIndex = 1;
			// 
			// txtLow
			// 
			this.txtLow.AutoSize = false;
			this.txtLow.BackColor = System.Drawing.SystemColors.Window;
			this.txtLow.Enabled = false;
			this.txtLow.LinkItem = null;
			this.txtLow.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtLow.LinkTopic = null;
			this.txtLow.Location = new System.Drawing.Point(20, 210);
			this.txtLow.Name = "txtLow";
			this.txtLow.Size = new System.Drawing.Size(156, 40);
			this.txtLow.TabIndex = 6;
			this.txtLow.Visible = false;
			// 
			// txtHigh
			// 
			this.txtHigh.AutoSize = false;
			this.txtHigh.BackColor = System.Drawing.SystemColors.Window;
			this.txtHigh.Enabled = false;
			this.txtHigh.LinkItem = null;
			this.txtHigh.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtHigh.LinkTopic = null;
			this.txtHigh.Location = new System.Drawing.Point(288, 210);
			this.txtHigh.Name = "txtHigh";
			this.txtHigh.Size = new System.Drawing.Size(156, 40);
			this.txtHigh.TabIndex = 8;
			this.txtHigh.Visible = false;
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(220, 224);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(31, 21);
			this.Label2.TabIndex = 7;
			this.Label2.Text = "TO";
			this.Label2.Visible = false;
			// 
			// cmdQuit
			// 
			this.cmdQuit.AppearanceKey = "actionButton";
			this.cmdQuit.Location = new System.Drawing.Point(30, 398);
			this.cmdQuit.Name = "cmdQuit";
			this.cmdQuit.Size = new System.Drawing.Size(76, 40);
			this.cmdQuit.TabIndex = 9;
			this.cmdQuit.Text = "Quit";
			this.cmdQuit.Visible = false;
			this.cmdQuit.Click += new System.EventHandler(this.cmdQuit_Click);
			// 
			// cmdProcess
			// 
			this.cmdProcess.AppearanceKey = "actionButton";
			this.cmdProcess.Location = new System.Drawing.Point(126, 398);
			this.cmdProcess.Name = "cmdProcess";
			this.cmdProcess.Size = new System.Drawing.Size(97, 40);
			this.cmdProcess.TabIndex = 10;
			this.cmdProcess.Text = "Process";
			this.cmdProcess.Visible = false;
			this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
			// 
			// sstPPCalculate
			// 
			this.sstPPCalculate.Controls.Add(this.sstPPCalculate_Page1);
			this.sstPPCalculate.Controls.Add(this.sstPPCalculate_Page2);
			this.sstPPCalculate.Controls.Add(this.sstPPCalculate_Page3);
			this.sstPPCalculate.Location = new System.Drawing.Point(521, 31);
			this.sstPPCalculate.Name = "sstPPCalculate";
			this.sstPPCalculate.PageInsets = new Wisej.Web.Padding(1, 47, 1, 1);
			this.sstPPCalculate.ShowFocusRect = false;
			this.sstPPCalculate.Size = new System.Drawing.Size(545, 421);
			this.sstPPCalculate.TabIndex = 0;
			this.sstPPCalculate.TabsPerRow = 0;
			this.sstPPCalculate.Text = "Itemized";
			this.sstPPCalculate.WordWrap = false;
			// 
			// sstPPCalculate_Page1
			// 
			this.sstPPCalculate_Page1.Controls.Add(this.vsLeased);
			this.sstPPCalculate_Page1.Location = new System.Drawing.Point(1, 47);
			this.sstPPCalculate_Page1.Name = "sstPPCalculate_Page1";
			this.sstPPCalculate_Page1.Text = "Correlation";
			// 
			// vsLeased
			// 
			this.vsLeased.AllowSelection = false;
			this.vsLeased.AllowUserToResizeColumns = false;
			this.vsLeased.AllowUserToResizeRows = false;
			this.vsLeased.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsLeased.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsLeased.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsLeased.BackColorBkg = System.Drawing.Color.Empty;
			this.vsLeased.BackColorFixed = System.Drawing.Color.Empty;
			this.vsLeased.BackColorSel = System.Drawing.Color.Empty;
			this.vsLeased.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsLeased.Cols = 10;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsLeased.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsLeased.ColumnHeadersHeight = 30;
			this.vsLeased.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsLeased.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsLeased.DragIcon = null;
			this.vsLeased.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsLeased.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsLeased.FrozenCols = 0;
			this.vsLeased.GridColor = System.Drawing.Color.Empty;
			this.vsLeased.GridColorFixed = System.Drawing.Color.Empty;
			this.vsLeased.Location = new System.Drawing.Point(30, 30);
			this.vsLeased.Name = "vsLeased";
			this.vsLeased.ReadOnly = true;
			this.vsLeased.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsLeased.RowHeightMin = 0;
			this.vsLeased.Rows = 50;
			this.vsLeased.ScrollTipText = null;
			this.vsLeased.ShowColumnVisibilityMenu = false;
			this.vsLeased.ShowFocusCell = false;
			this.vsLeased.Size = new System.Drawing.Size(499, 329);
			this.vsLeased.StandardTab = true;
			this.vsLeased.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsLeased.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsLeased.TabIndex = 0;
			// 
			// sstPPCalculate_Page2
			// 
			this.sstPPCalculate_Page2.Controls.Add(this.vsItemized);
			this.sstPPCalculate_Page2.Location = new System.Drawing.Point(1, 47);
			this.sstPPCalculate_Page2.Name = "sstPPCalculate_Page2";
			this.sstPPCalculate_Page2.Text = "Itemized";
			// 
			// vsItemized
			// 
			this.vsItemized.AllowSelection = false;
			this.vsItemized.AllowUserToResizeColumns = false;
			this.vsItemized.AllowUserToResizeRows = false;
			this.vsItemized.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsItemized.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsItemized.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsItemized.BackColorBkg = System.Drawing.Color.Empty;
			this.vsItemized.BackColorFixed = System.Drawing.Color.Empty;
			this.vsItemized.BackColorSel = System.Drawing.Color.Empty;
			this.vsItemized.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsItemized.Cols = 10;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsItemized.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.vsItemized.ColumnHeadersHeight = 30;
			this.vsItemized.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsItemized.DefaultCellStyle = dataGridViewCellStyle4;
			this.vsItemized.DragIcon = null;
			this.vsItemized.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsItemized.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsItemized.FrozenCols = 0;
			this.vsItemized.GridColor = System.Drawing.Color.Empty;
			this.vsItemized.GridColorFixed = System.Drawing.Color.Empty;
			this.vsItemized.Location = new System.Drawing.Point(30, 30);
			this.vsItemized.Name = "vsItemized";
			this.vsItemized.ReadOnly = true;
			this.vsItemized.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsItemized.RowHeightMin = 0;
			this.vsItemized.Rows = 50;
			this.vsItemized.ScrollTipText = null;
			this.vsItemized.ShowColumnVisibilityMenu = false;
			this.vsItemized.ShowFocusCell = false;
			this.vsItemized.Size = new System.Drawing.Size(498, 326);
			this.vsItemized.StandardTab = true;
			this.vsItemized.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsItemized.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsItemized.TabIndex = 0;
			// 
			// sstPPCalculate_Page3
			// 
			this.sstPPCalculate_Page3.Controls.Add(this.lblOverride);
			this.sstPPCalculate_Page3.Controls.Add(this.vsValuations);
			this.sstPPCalculate_Page3.Controls.Add(this.vsDisplay);
			this.sstPPCalculate_Page3.Controls.Add(this.Check1);
			this.sstPPCalculate_Page3.Controls.Add(this.txtOverride);
			this.sstPPCalculate_Page3.Location = new System.Drawing.Point(1, 47);
			this.sstPPCalculate_Page3.Name = "sstPPCalculate_Page3";
			this.sstPPCalculate_Page3.Text = "Correlation";
			// 
			// lblOverride
			// 
			this.lblOverride.AutoSize = true;
			this.lblOverride.Location = new System.Drawing.Point(30, 80);
			this.lblOverride.Name = "lblOverride";
			this.lblOverride.Size = new System.Drawing.Size(261, 15);
			this.lblOverride.TabIndex = 2;
			this.lblOverride.Text = "THIS ACCOUNT HAS AN OVERRIDE VALUE";
			this.lblOverride.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.lblOverride.Visible = false;
			// 
			// vsValuations
			// 
			this.vsValuations.AllowSelection = false;
			this.vsValuations.AllowUserToResizeColumns = false;
			this.vsValuations.AllowUserToResizeRows = false;
			this.vsValuations.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsValuations.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsValuations.BackColorBkg = System.Drawing.Color.Empty;
			this.vsValuations.BackColorFixed = System.Drawing.Color.Empty;
			this.vsValuations.BackColorSel = System.Drawing.Color.Empty;
			this.vsValuations.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsValuations.Cols = 10;
			dataGridViewCellStyle5.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsValuations.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
			this.vsValuations.ColumnHeadersHeight = 30;
			this.vsValuations.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle6.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsValuations.DefaultCellStyle = dataGridViewCellStyle6;
			this.vsValuations.DragIcon = null;
			this.vsValuations.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsValuations.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsValuations.FrozenCols = 0;
			this.vsValuations.GridColor = System.Drawing.Color.Empty;
			this.vsValuations.GridColorFixed = System.Drawing.Color.Empty;
			this.vsValuations.Location = new System.Drawing.Point(30, 267);
			this.vsValuations.Name = "vsValuations";
			this.vsValuations.ReadOnly = true;
			this.vsValuations.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsValuations.RowHeightMin = 0;
			this.vsValuations.Rows = 50;
			this.vsValuations.ScrollTipText = null;
			this.vsValuations.ShowColumnVisibilityMenu = false;
			this.vsValuations.ShowFocusCell = false;
			this.vsValuations.Size = new System.Drawing.Size(498, 93);
			this.vsValuations.StandardTab = true;
			this.vsValuations.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsValuations.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsValuations.TabIndex = 4;
			this.vsValuations.Visible = false;
			// 
			// vsDisplay
			// 
			this.vsDisplay.AllowSelection = false;
			this.vsDisplay.AllowUserToResizeColumns = false;
			this.vsDisplay.AllowUserToResizeRows = false;
			this.vsDisplay.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsDisplay.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsDisplay.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsDisplay.BackColorBkg = System.Drawing.Color.Empty;
			this.vsDisplay.BackColorFixed = System.Drawing.Color.Empty;
			this.vsDisplay.BackColorSel = System.Drawing.Color.Empty;
			this.vsDisplay.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsDisplay.Cols = 10;
			dataGridViewCellStyle7.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsDisplay.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
			this.vsDisplay.ColumnHeadersHeight = 30;
			this.vsDisplay.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle8.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsDisplay.DefaultCellStyle = dataGridViewCellStyle8;
			this.vsDisplay.DragIcon = null;
			this.vsDisplay.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsDisplay.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsDisplay.FrozenCols = 0;
			this.vsDisplay.GridColor = System.Drawing.Color.Empty;
			this.vsDisplay.GridColorFixed = System.Drawing.Color.Empty;
			this.vsDisplay.Location = new System.Drawing.Point(30, 107);
			this.vsDisplay.Name = "vsDisplay";
			this.vsDisplay.ReadOnly = true;
			this.vsDisplay.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsDisplay.RowHeightMin = 0;
			this.vsDisplay.Rows = 50;
			this.vsDisplay.ScrollTipText = null;
			this.vsDisplay.ShowColumnVisibilityMenu = false;
			this.vsDisplay.ShowFocusCell = false;
			this.vsDisplay.Size = new System.Drawing.Size(498, 246);
			this.vsDisplay.StandardTab = true;
			this.vsDisplay.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsDisplay.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsDisplay.TabIndex = 3;
			// 
			// Check1
			// 
			this.Check1.Location = new System.Drawing.Point(30, 34);
			this.Check1.Name = "Check1";
			this.Check1.Size = new System.Drawing.Size(170, 27);
			this.Check1.TabIndex = 0;
			this.Check1.Text = "Use Override Value";
			// 
			// txtOverride
			// 
			this.txtOverride.AutoSize = false;
			this.txtOverride.BackColor = System.Drawing.SystemColors.Window;
			this.txtOverride.Enabled = false;
			this.txtOverride.LinkItem = null;
			this.txtOverride.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtOverride.LinkTopic = null;
			this.txtOverride.Location = new System.Drawing.Point(218, 30);
			this.txtOverride.LockedOriginal = true;
			this.txtOverride.Name = "txtOverride";
			this.txtOverride.ReadOnly = true;
			this.txtOverride.Size = new System.Drawing.Size(82, 40);
			this.txtOverride.TabIndex = 1;
			this.txtOverride.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// lblName
			// 
			this.lblName.AutoSize = true;
			this.lblName.Location = new System.Drawing.Point(122, 320);
			this.lblName.Name = "lblName";
			this.lblName.Size = new System.Drawing.Size(4, 14);
			this.lblName.TabIndex = 5;
			// 
			// lblLocationName
			// 
			this.lblLocationName.AutoSize = true;
			this.lblLocationName.Location = new System.Drawing.Point(167, 360);
			this.lblLocationName.Name = "lblLocationName";
			this.lblLocationName.Size = new System.Drawing.Size(4, 14);
			this.lblLocationName.TabIndex = 8;
			// 
			// lblLocation
			// 
			this.lblLocation.AutoSize = true;
			this.lblLocation.Location = new System.Drawing.Point(122, 360);
			this.lblLocation.Name = "lblLocation";
			this.lblLocation.Size = new System.Drawing.Size(4, 14);
			this.lblLocation.TabIndex = 7;
			// 
			// lblLocationTitle
			// 
			this.lblLocationTitle.AutoSize = true;
			this.lblLocationTitle.Location = new System.Drawing.Point(30, 360);
			this.lblLocationTitle.Name = "lblLocationTitle";
			this.lblLocationTitle.Size = new System.Drawing.Size(71, 15);
			this.lblLocationTitle.TabIndex = 6;
			this.lblLocationTitle.Text = "LOCATION";
			this.lblLocationTitle.Visible = false;
			// 
			// lblAccount
			// 
			this.lblAccount.AutoSize = true;
			this.lblAccount.Location = new System.Drawing.Point(122, 280);
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Size = new System.Drawing.Size(4, 14);
			this.lblAccount.TabIndex = 3;
			// 
			// lblAccountTitle
			// 
			this.lblAccountTitle.AutoSize = true;
			this.lblAccountTitle.Location = new System.Drawing.Point(30, 280);
			this.lblAccountTitle.Name = "lblAccountTitle";
			this.lblAccountTitle.Size = new System.Drawing.Size(69, 15);
			this.lblAccountTitle.TabIndex = 2;
			this.lblAccountTitle.Text = "ACCOUNT";
			this.lblAccountTitle.Visible = false;
			// 
			// lblNameTitle
			// 
			this.lblNameTitle.AutoSize = true;
			this.lblNameTitle.Location = new System.Drawing.Point(30, 320);
			this.lblNameTitle.Name = "lblNameTitle";
			this.lblNameTitle.Size = new System.Drawing.Size(43, 15);
			this.lblNameTitle.TabIndex = 4;
			this.lblNameTitle.Text = "NAME";
			this.lblNameTitle.Visible = false;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuPrintPreview,
				this.mnuSepar,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuPrintPreview
			// 
			this.mnuPrintPreview.Index = 0;
			this.mnuPrintPreview.Name = "mnuPrintPreview";
			this.mnuPrintPreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuPrintPreview.Text = "Print Preview";
			this.mnuPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 1;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdPrintPreview
			// 
			this.cmdPrintPreview.AppearanceKey = "acceptButton";
			this.cmdPrintPreview.Location = new System.Drawing.Point(490, 30);
			this.cmdPrintPreview.Name = "cmdPrintPreview";
			this.cmdPrintPreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPrintPreview.Size = new System.Drawing.Size(143, 48);
			this.cmdPrintPreview.TabIndex = 0;
			this.cmdPrintPreview.Text = "Print Preview";
			this.cmdPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
			// 
			// frmPrintSummary
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1098, 631);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmPrintSummary";
			this.Text = "Batch Calculate";
			this.Load += new System.EventHandler(this.frmPrintSummary_Load);
			this.Activated += new System.EventHandler(this.frmPrintSummary_Activated);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPrintSummary_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPrintSummary_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.Frame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
			this.sstPPCalculate.ResumeLayout(false);
			this.sstPPCalculate_Page1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsLeased)).EndInit();
			this.sstPPCalculate_Page2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsItemized)).EndInit();
			this.sstPPCalculate_Page3.ResumeLayout(false);
			this.sstPPCalculate_Page3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsValuations)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsDisplay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Check1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdPrintPreview;
	}
}
