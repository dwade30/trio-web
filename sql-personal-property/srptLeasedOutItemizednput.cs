﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Core;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for srptLeasedOutItemizednput.
	/// </summary>
	public partial class srptLeasedOutItemizednput : FCSectionReport
	{
		public srptLeasedOutItemizednput()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ActiveReport1";
		}

		public static srptLeasedOutItemizednput InstancePtr
		{
			get
			{
				return (srptLeasedOutItemizednput)Sys.GetInstance(typeof(srptLeasedOutItemizednput));
			}
		}

		protected srptLeasedOutItemizednput _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsReports?.Dispose();
                clsReports = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptLeasedOutItemizednput	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool boolPrint;
		clsDRWrapper clsReports = new clsDRWrapper();
		
		int lngAcct;

		public void PrintLeased()
		{
			if (FCConvert.ToString(clsReports.Get_Fields_String("CD")) == "-")
			{
				// TODO Get_Fields: Check the table for the column [Line] and replace with corresponding Get_Field method
				txtLine.Text = Strings.Format(clsReports.Get_Fields("Line"), "0000");
				txtCD.Text = clsReports.Get_Fields_String("CD");
				// TODO Get_Fields: Check the table for the column [rb] and replace with corresponding Get_Field method
				txtRB.Text = clsReports.Get_Fields_String("rb");
				txtQTY.Text = string.Empty;
				txtDescription.Text = Strings.Trim(FCConvert.ToString(clsReports.Get_Fields_String("Description")));
				txtCost.Text = string.Empty;
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				txtLeasedTo.Text = clsReports.Get_Fields_String("account");
				txtExemptYear.Text = "";
				boolPrint = true;
			}
			// TODO Get_Fields: Check the table for the column [Cost] and replace with corresponding Get_Field method
			else if (Conversion.Val(clsReports.Get_Fields("Cost")) != 0)
			{
				if (Strings.Trim(FCConvert.ToString(clsReports.Get_Fields_String("Description"))) != string.Empty)
				{
					// TODO Get_Fields: Check the table for the column [Line] and replace with corresponding Get_Field method
					txtLine.Text = Strings.Format(clsReports.Get_Fields("Line"), "0000");
					txtCD.Text = clsReports.Get_Fields_String("CD");
					// TODO Get_Fields: Check the table for the column [rb] and replace with corresponding Get_Field method
					txtRB.Text = clsReports.Get_Fields_String("rb");
					if (rptLeasedandItemizedInput.InstancePtr.boolPQTY)
					{
						// TODO Get_Fields: Check the table for the column [Quantity] and replace with corresponding Get_Field method
						txtQTY.Text = FCConvert.ToString(clsReports.Get_Fields("Quantity"));
					}
					else
					{
						txtQTY.Text = "__";
					}
					txtDescription.Text = Strings.Trim(FCConvert.ToString(clsReports.Get_Fields_String("Description")));
					// TODO Get_Fields: Check the table for the column [Cost] and replace with corresponding Get_Field method
					txtCost.Text = Strings.Format(clsReports.Get_Fields("Cost"), "###,###,###");
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					txtLeasedTo.Text = FCConvert.ToString(Conversion.Val(clsReports.Get_Fields("account")));
					if (clsReports.Get_Fields_Int32("exemptyear") > 0)
					{
						txtExemptYear.Text = FCConvert.ToString(clsReports.Get_Fields_Int32("exemptyear"));
					}
					else
					{
						txtExemptYear.Text = "";
					}
					boolPrint = true;
				}
			}
				// TODO Get_Fields: Check the table for the column [Cost] and replace with corresponding Get_Field method
				else if (Conversion.Val(clsReports.Get_Fields("Cost")) == 0)
			{
				if (Strings.Trim(FCConvert.ToString(clsReports.Get_Fields_String("Description"))) != string.Empty)
				{
					// TODO Get_Fields: Check the table for the column [Line] and replace with corresponding Get_Field method
					txtLine.Text = Strings.Format(clsReports.Get_Fields("Line"), "0000");
					txtCD.Text = clsReports.Get_Fields_String("CD");
					if (rptLeasedandItemizedInput.InstancePtr.boolPQTY)
					{
						// TODO Get_Fields: Check the table for the column [Quantity] and replace with corresponding Get_Field method
						txtQTY.Text = FCConvert.ToString(clsReports.Get_Fields("Quantity"));
					}
					else
					{
						txtQTY.Text = "__";
					}
					txtDescription.Text = Strings.Trim(FCConvert.ToString(clsReports.Get_Fields_String("Description")));
					txtCost.Text = string.Empty;
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					txtLeasedTo.Text = FCConvert.ToString(Conversion.Val(clsReports.Get_Fields("account")));
					if (clsReports.Get_Fields_Int32("exemptyear") > 0)
					{
						txtExemptYear.Text = FCConvert.ToString(clsReports.Get_Fields_Int32("exemptyear"));
					}
					else
					{
						txtExemptYear.Text = "";
					}
					boolPrint = true;
				}
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			ShowRecord:
			;
			eArgs.EOF = clsReports.EndOfFile();
			if (eArgs.EOF)
				return;
			boolPrint = false;
			PrintLeased();
			if (!boolPrint)
			{
				clsReports.MoveNext();
				goto ShowRecord;
			}
			else
			{
				clsReports.MoveNext();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData)));
			clsReports.OpenRecordset("select * from ppleased where leasedfrom = " + FCConvert.ToString(lngAcct) + " order by account,line", "twpp0000.vb1");
			//FC:FINAL:MSH - issue #1332: in original report will be unloaded and data will be deleted, but in web we can't unload report, so hide 
			// data area if data doesn't exist
			//if (clsReports.EndOfFile())
			//    this.Close();
			if (clsReports.EndOfFile())
			{
				Detail.Visible = false;
				ReportHeader.Visible = false;
				this.Close();
			}
			else
			{
				Detail.Visible = true;
				ReportHeader.Visible = true;
			}
			// Call clsTemp.OpenRecordset("select name,street,streetnumber from ppmaster where account = " & lngAcct, "twpp0000.vb1")
		}

		
	}
}
