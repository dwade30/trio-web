//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;
using fecherFoundation.VisualBasicLayer;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmPPMasterShort.
	/// </summary>
	partial class frmPPMasterShort : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label2;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblStatics;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblCategory;
		public fecherFoundation.FCButton cmdRemoveOwner;
		public fecherFoundation.FCTextBox txtOwnerID;
		public fecherFoundation.FCButton cmdEditOwner;
		public fecherFoundation.FCButton cmdSearchOwner;
		public fecherFoundation.FCTextBox txtTotal;
		public fecherFoundation.FCTextBox txtBETE1;
		public fecherFoundation.FCTextBox txtBETE2;
		public fecherFoundation.FCTextBox txtBETE3;
		public fecherFoundation.FCTextBox txtBETE8;
		public fecherFoundation.FCTextBox txtBETE7;
		public fecherFoundation.FCTextBox txtBETE6;
		public fecherFoundation.FCTextBox txtBETE5;
		public fecherFoundation.FCTextBox txtBETE4;
		public fecherFoundation.FCTextBox txtBETE9;
		public fecherFoundation.FCTextBox txtApt;
		public fecherFoundation.FCTextBox txtSquareFootage;
		public fecherFoundation.FCFrame fraRate;
		public fecherFoundation.FCLabel Label2_7;
		public fecherFoundation.FCLabel lblTotalValue;
		public fecherFoundation.FCLabel Label2_5;
		public fecherFoundation.FCLabel lblTaxRate;
		public fecherFoundation.FCLabel Label2_3;
		public fecherFoundation.FCLabel lblTotalTax;
		public fecherFoundation.FCTextBox txtCategory9;
		public fecherFoundation.FCTextBox txtExemptCode2;
		public fecherFoundation.FCTextBox txtStreetName;
		public fecherFoundation.FCTextBox txtStreetNumber;
		public fecherFoundation.FCTextBox txtExemption;
		public fecherFoundation.FCTextBox txtExemptCode1;
		public fecherFoundation.FCTextBox txtBusinessCode;
		public fecherFoundation.FCTextBox txtStreetCode;
		public fecherFoundation.FCTextBox txtOpen1;
		public fecherFoundation.FCTextBox txtOpen2;
		public fecherFoundation.FCTextBox txtCategory4;
		public fecherFoundation.FCTextBox txtCategory5;
		public fecherFoundation.FCTextBox txtCategory6;
		public fecherFoundation.FCTextBox txtCategory7;
		public fecherFoundation.FCTextBox txtCategory8;
		public fecherFoundation.FCTextBox txtCategory3;
		public fecherFoundation.FCTextBox txtCategory2;
		public fecherFoundation.FCTextBox txtCategory1;
		public fecherFoundation.FCGrid gridTranCode;
		public fecherFoundation.FCLabel lblAddress;
		public fecherFoundation.FCLabel lblOwner1;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel Label2_9;
		public fecherFoundation.FCLabel Label2_4;
		public fecherFoundation.FCLabel lblDate;
		public fecherFoundation.FCLabel Label2_0;
		public fecherFoundation.FCLabel Label2_1;
		public fecherFoundation.FCLabel lblTotWithBETE;
		public fecherFoundation.FCLabel lblTotExempt;
		public fecherFoundation.FCLabel Label2_2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblComment;
		public fecherFoundation.FCLabel lblStatics_2;
		public fecherFoundation.FCLabel txtREAssociate;
		public fecherFoundation.FCLabel lblCategory_8;
		public fecherFoundation.FCLabel lblCategory_7;
		public fecherFoundation.FCLabel lblCategory_6;
		public fecherFoundation.FCLabel lblCategory_5;
		public fecherFoundation.FCLabel lblCategory_4;
		public fecherFoundation.FCLabel lblCategory_3;
		public fecherFoundation.FCLabel lblCategory_2;
		public fecherFoundation.FCLabel lblCategory_1;
		public fecherFoundation.FCLabel lblCategory_0;
		public fecherFoundation.FCLabel lblStatics_0;
		public fecherFoundation.FCLabel lblOpen2;
		public fecherFoundation.FCLabel lblOpen1;
		public fecherFoundation.FCLabel lblStatics_14;
		public fecherFoundation.FCLabel lblStatics_15;
		public fecherFoundation.FCLabel lblStatics_16;
		public fecherFoundation.FCLabel lblStatics_17;
		public fecherFoundation.FCLabel lblStatics_18;
		public fecherFoundation.FCLabel lblStatics_20;
		public fecherFoundation.FCLabel lblAccount;
		public fecherFoundation.FCLabel lblStatics_29;
		public fecherFoundation.FCLabel Label6;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuPrintLabel;
		public fecherFoundation.FCToolStripMenuItem mnuGroup;
		public fecherFoundation.FCToolStripMenuItem mnuInterestedParties;
		public fecherFoundation.FCToolStripMenuItem mnuChangeAssociation;
		public fecherFoundation.FCToolStripMenuItem mnuComment;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPPMasterShort));
            this.cmdRemoveOwner = new fecherFoundation.FCButton();
            this.txtOwnerID = new fecherFoundation.FCTextBox();
            this.cmdEditOwner = new fecherFoundation.FCButton();
            this.cmdSearchOwner = new fecherFoundation.FCButton();
            this.txtTotal = new fecherFoundation.FCTextBox();
            this.txtBETE1 = new fecherFoundation.FCTextBox();
            this.txtBETE2 = new fecherFoundation.FCTextBox();
            this.txtBETE3 = new fecherFoundation.FCTextBox();
            this.txtBETE8 = new fecherFoundation.FCTextBox();
            this.txtBETE7 = new fecherFoundation.FCTextBox();
            this.txtBETE6 = new fecherFoundation.FCTextBox();
            this.txtBETE5 = new fecherFoundation.FCTextBox();
            this.txtBETE4 = new fecherFoundation.FCTextBox();
            this.txtBETE9 = new fecherFoundation.FCTextBox();
            this.txtApt = new fecherFoundation.FCTextBox();
            this.txtSquareFootage = new fecherFoundation.FCTextBox();
            this.fraRate = new fecherFoundation.FCFrame();
            this.Label2_7 = new fecherFoundation.FCLabel();
            this.lblTotalValue = new fecherFoundation.FCLabel();
            this.Label2_5 = new fecherFoundation.FCLabel();
            this.lblTaxRate = new fecherFoundation.FCLabel();
            this.Label2_3 = new fecherFoundation.FCLabel();
            this.lblTotalTax = new fecherFoundation.FCLabel();
            this.txtCategory9 = new fecherFoundation.FCTextBox();
            this.txtExemptCode2 = new fecherFoundation.FCTextBox();
            this.txtStreetName = new fecherFoundation.FCTextBox();
            this.txtStreetNumber = new fecherFoundation.FCTextBox();
            this.txtExemption = new fecherFoundation.FCTextBox();
            this.txtExemptCode1 = new fecherFoundation.FCTextBox();
            this.txtBusinessCode = new fecherFoundation.FCTextBox();
            this.txtStreetCode = new fecherFoundation.FCTextBox();
            this.txtOpen1 = new fecherFoundation.FCTextBox();
            this.txtOpen2 = new fecherFoundation.FCTextBox();
            this.txtCategory4 = new fecherFoundation.FCTextBox();
            this.txtCategory5 = new fecherFoundation.FCTextBox();
            this.txtCategory6 = new fecherFoundation.FCTextBox();
            this.txtCategory7 = new fecherFoundation.FCTextBox();
            this.txtCategory8 = new fecherFoundation.FCTextBox();
            this.txtCategory3 = new fecherFoundation.FCTextBox();
            this.txtCategory2 = new fecherFoundation.FCTextBox();
            this.txtCategory1 = new fecherFoundation.FCTextBox();
            this.gridTranCode = new fecherFoundation.FCGrid();
            this.lblAddress = new fecherFoundation.FCLabel();
            this.lblOwner1 = new fecherFoundation.FCLabel();
            this.Label14 = new fecherFoundation.FCLabel();
            this.Label2_9 = new fecherFoundation.FCLabel();
            this.Label2_4 = new fecherFoundation.FCLabel();
            this.lblDate = new fecherFoundation.FCLabel();
            this.Label2_0 = new fecherFoundation.FCLabel();
            this.Label2_1 = new fecherFoundation.FCLabel();
            this.lblTotWithBETE = new fecherFoundation.FCLabel();
            this.lblTotExempt = new fecherFoundation.FCLabel();
            this.Label2_2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.lblComment = new fecherFoundation.FCLabel();
            this.lblStatics_2 = new fecherFoundation.FCLabel();
            this.txtREAssociate = new fecherFoundation.FCLabel();
            this.lblCategory_8 = new fecherFoundation.FCLabel();
            this.lblCategory_7 = new fecherFoundation.FCLabel();
            this.lblCategory_6 = new fecherFoundation.FCLabel();
            this.lblCategory_5 = new fecherFoundation.FCLabel();
            this.lblCategory_4 = new fecherFoundation.FCLabel();
            this.lblCategory_3 = new fecherFoundation.FCLabel();
            this.lblCategory_2 = new fecherFoundation.FCLabel();
            this.lblCategory_1 = new fecherFoundation.FCLabel();
            this.lblCategory_0 = new fecherFoundation.FCLabel();
            this.lblStatics_0 = new fecherFoundation.FCLabel();
            this.lblOpen2 = new fecherFoundation.FCLabel();
            this.lblOpen1 = new fecherFoundation.FCLabel();
            this.lblStatics_14 = new fecherFoundation.FCLabel();
            this.lblStatics_15 = new fecherFoundation.FCLabel();
            this.lblStatics_16 = new fecherFoundation.FCLabel();
            this.lblStatics_17 = new fecherFoundation.FCLabel();
            this.lblStatics_18 = new fecherFoundation.FCLabel();
            this.lblStatics_20 = new fecherFoundation.FCLabel();
            this.lblAccount = new fecherFoundation.FCLabel();
            this.lblStatics_29 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.MainMenu1 = new Wisej.Web.MainMenu(this.components);
            this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuInterestedParties = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintLabel = new fecherFoundation.FCToolStripMenuItem();
            this.mnuGroup = new fecherFoundation.FCToolStripMenuItem();
            this.mnuChangeAssociation = new fecherFoundation.FCToolStripMenuItem();
            this.mnuComment = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdPrintLabel = new fecherFoundation.FCButton();
            this.cmdGroupInfo = new fecherFoundation.FCButton();
            this.cmdChangeAssociation = new fecherFoundation.FCButton();
            this.cmdComment = new fecherFoundation.FCButton();
            this.fcToolStripMenuItem1 = new fecherFoundation.FCToolStripMenuItem();
            this.fcToolStripMenuItem2 = new fecherFoundation.FCToolStripMenuItem();
            this.fcToolStripMenuItem3 = new fecherFoundation.FCToolStripMenuItem();
            this.menuItem1 = new Wisej.Web.MenuItem();
            this.fcToolStripMenuItem4 = new fecherFoundation.FCToolStripMenuItem();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRemoveOwner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEditOwner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearchOwner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraRate)).BeginInit();
            this.fraRate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTranCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGroupInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdChangeAssociation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdComment)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(1001, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdRemoveOwner);
            this.ClientArea.Controls.Add(this.txtOwnerID);
            this.ClientArea.Controls.Add(this.cmdEditOwner);
            this.ClientArea.Controls.Add(this.cmdSearchOwner);
            this.ClientArea.Controls.Add(this.txtTotal);
            this.ClientArea.Controls.Add(this.txtBETE1);
            this.ClientArea.Controls.Add(this.txtBETE2);
            this.ClientArea.Controls.Add(this.txtBETE3);
            this.ClientArea.Controls.Add(this.txtBETE8);
            this.ClientArea.Controls.Add(this.txtBETE7);
            this.ClientArea.Controls.Add(this.txtBETE6);
            this.ClientArea.Controls.Add(this.txtBETE5);
            this.ClientArea.Controls.Add(this.txtBETE4);
            this.ClientArea.Controls.Add(this.txtBETE9);
            this.ClientArea.Controls.Add(this.txtApt);
            this.ClientArea.Controls.Add(this.txtSquareFootage);
            this.ClientArea.Controls.Add(this.fraRate);
            this.ClientArea.Controls.Add(this.txtCategory9);
            this.ClientArea.Controls.Add(this.txtExemptCode2);
            this.ClientArea.Controls.Add(this.txtStreetName);
            this.ClientArea.Controls.Add(this.txtStreetNumber);
            this.ClientArea.Controls.Add(this.txtExemption);
            this.ClientArea.Controls.Add(this.txtExemptCode1);
            this.ClientArea.Controls.Add(this.txtBusinessCode);
            this.ClientArea.Controls.Add(this.txtStreetCode);
            this.ClientArea.Controls.Add(this.txtOpen1);
            this.ClientArea.Controls.Add(this.txtOpen2);
            this.ClientArea.Controls.Add(this.txtCategory4);
            this.ClientArea.Controls.Add(this.txtCategory5);
            this.ClientArea.Controls.Add(this.txtCategory6);
            this.ClientArea.Controls.Add(this.txtCategory7);
            this.ClientArea.Controls.Add(this.txtCategory8);
            this.ClientArea.Controls.Add(this.txtCategory3);
            this.ClientArea.Controls.Add(this.txtCategory2);
            this.ClientArea.Controls.Add(this.txtCategory1);
            this.ClientArea.Controls.Add(this.gridTranCode);
            this.ClientArea.Controls.Add(this.lblAddress);
            this.ClientArea.Controls.Add(this.lblOwner1);
            this.ClientArea.Controls.Add(this.Label14);
            this.ClientArea.Controls.Add(this.Label2_9);
            this.ClientArea.Controls.Add(this.Label2_4);
            this.ClientArea.Controls.Add(this.lblDate);
            this.ClientArea.Controls.Add(this.Label2_0);
            this.ClientArea.Controls.Add(this.Label2_1);
            this.ClientArea.Controls.Add(this.lblTotWithBETE);
            this.ClientArea.Controls.Add(this.lblTotExempt);
            this.ClientArea.Controls.Add(this.Label2_2);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.lblComment);
            this.ClientArea.Controls.Add(this.lblStatics_2);
            this.ClientArea.Controls.Add(this.txtREAssociate);
            this.ClientArea.Controls.Add(this.lblCategory_8);
            this.ClientArea.Controls.Add(this.lblCategory_7);
            this.ClientArea.Controls.Add(this.lblCategory_6);
            this.ClientArea.Controls.Add(this.lblCategory_5);
            this.ClientArea.Controls.Add(this.lblCategory_4);
            this.ClientArea.Controls.Add(this.lblCategory_3);
            this.ClientArea.Controls.Add(this.lblCategory_2);
            this.ClientArea.Controls.Add(this.lblCategory_1);
            this.ClientArea.Controls.Add(this.lblCategory_0);
            this.ClientArea.Controls.Add(this.lblStatics_0);
            this.ClientArea.Controls.Add(this.lblOpen2);
            this.ClientArea.Controls.Add(this.lblOpen1);
            this.ClientArea.Controls.Add(this.lblStatics_14);
            this.ClientArea.Controls.Add(this.lblStatics_15);
            this.ClientArea.Controls.Add(this.lblStatics_16);
            this.ClientArea.Controls.Add(this.lblStatics_17);
            this.ClientArea.Controls.Add(this.lblStatics_18);
            this.ClientArea.Controls.Add(this.lblStatics_20);
            this.ClientArea.Controls.Add(this.lblAccount);
            this.ClientArea.Controls.Add(this.lblStatics_29);
            this.ClientArea.Controls.Add(this.Label6);
            this.ClientArea.Size = new System.Drawing.Size(1001, 498);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdChangeAssociation);
            this.TopPanel.Controls.Add(this.cmdGroupInfo);
            this.TopPanel.Controls.Add(this.cmdComment);
            this.TopPanel.Controls.Add(this.cmdPrintLabel);
            this.TopPanel.Size = new System.Drawing.Size(1001, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrintLabel, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdComment, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdGroupInfo, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdChangeAssociation, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(219, 30);
            this.HeaderText.Text = "Short Maintenance";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmdRemoveOwner
            // 
            this.cmdRemoveOwner.AppearanceKey = "actionButton";
            this.cmdRemoveOwner.ImageSource = "icon-close-menu";
            this.cmdRemoveOwner.Location = new System.Drawing.Point(421, 161);
            this.cmdRemoveOwner.Name = "cmdRemoveOwner";
            this.cmdRemoveOwner.Size = new System.Drawing.Size(40, 40);
            this.cmdRemoveOwner.TabIndex = 9;
            this.ToolTip1.SetToolTip(this.cmdRemoveOwner, null);
            this.cmdRemoveOwner.Click += new System.EventHandler(this.cmdRemoveOwner_Click);
            // 
            // txtOwnerID
            // 
            this.txtOwnerID.BackColor = System.Drawing.Color.FromArgb(255, 255, 192);
            this.txtOwnerID.Location = new System.Drawing.Point(177, 160);
            this.txtOwnerID.Name = "txtOwnerID";
            this.txtOwnerID.Size = new System.Drawing.Size(137, 40);
            this.txtOwnerID.TabIndex = 6;
            this.txtOwnerID.Tag = "address";
            this.txtOwnerID.Text = "0";
            this.ToolTip1.SetToolTip(this.txtOwnerID, null);
            this.txtOwnerID.Validating += new System.ComponentModel.CancelEventHandler(this.txtOwnerID_Validating);
            // 
            // cmdEditOwner
            // 
            this.cmdEditOwner.AppearanceKey = "actionButton";
            this.cmdEditOwner.ImageSource = "icon - edit";
            this.cmdEditOwner.Location = new System.Drawing.Point(375, 161);
            this.cmdEditOwner.Name = "cmdEditOwner";
            this.cmdEditOwner.Size = new System.Drawing.Size(40, 40);
            this.cmdEditOwner.TabIndex = 8;
            this.ToolTip1.SetToolTip(this.cmdEditOwner, null);
            this.cmdEditOwner.Click += new System.EventHandler(this.cmdEditOwner_Click);
            // 
            // cmdSearchOwner
            // 
            this.cmdSearchOwner.AppearanceKey = "actionButton";
            this.cmdSearchOwner.ImageSource = "icon - search";
            this.cmdSearchOwner.Location = new System.Drawing.Point(329, 160);
            this.cmdSearchOwner.Name = "cmdSearchOwner";
            this.cmdSearchOwner.Size = new System.Drawing.Size(40, 40);
            this.cmdSearchOwner.TabIndex = 7;
            this.ToolTip1.SetToolTip(this.cmdSearchOwner, null);
            this.cmdSearchOwner.Click += new System.EventHandler(this.cmdSearchOwner_Click);
            // 
            // txtTotal
            // 
            this.txtTotal.BackColor = System.Drawing.SystemColors.Window;
            this.txtTotal.Location = new System.Drawing.Point(785, 520);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(132, 40);
            this.txtTotal.TabIndex = 64;
            this.txtTotal.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtTotal, null);
            this.txtTotal.Validating += new System.ComponentModel.CancelEventHandler(this.txtTotal_Validating);
            // 
            // txtBETE1
            // 
            this.txtBETE1.BackColor = System.Drawing.SystemColors.Window;
            this.txtBETE1.Location = new System.Drawing.Point(379, 563);
            this.txtBETE1.MaxLength = 13;
            this.txtBETE1.Name = "txtBETE1";
            this.txtBETE1.Size = new System.Drawing.Size(118, 40);
            this.txtBETE1.TabIndex = 25;
            this.txtBETE1.Tag = "values";
            this.txtBETE1.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtBETE1, null);
            this.txtBETE1.TextChanged += new System.EventHandler(this.txtBETE1_TextChanged);
            this.txtBETE1.Validating += new System.ComponentModel.CancelEventHandler(this.txtBETE1_Validating);
            // 
            // txtBETE2
            // 
            this.txtBETE2.BackColor = System.Drawing.SystemColors.Window;
            this.txtBETE2.Location = new System.Drawing.Point(379, 623);
            this.txtBETE2.MaxLength = 13;
            this.txtBETE2.Name = "txtBETE2";
            this.txtBETE2.Size = new System.Drawing.Size(118, 40);
            this.txtBETE2.TabIndex = 28;
            this.txtBETE2.Tag = "values";
            this.txtBETE2.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtBETE2, null);
            this.txtBETE2.TextChanged += new System.EventHandler(this.txtBETE2_TextChanged);
            this.txtBETE2.Validating += new System.ComponentModel.CancelEventHandler(this.txtBETE2_Validating);
            // 
            // txtBETE3
            // 
            this.txtBETE3.BackColor = System.Drawing.SystemColors.Window;
            this.txtBETE3.Location = new System.Drawing.Point(379, 687);
            this.txtBETE3.MaxLength = 13;
            this.txtBETE3.Name = "txtBETE3";
            this.txtBETE3.Size = new System.Drawing.Size(118, 40);
            this.txtBETE3.TabIndex = 31;
            this.txtBETE3.Tag = "values";
            this.txtBETE3.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtBETE3, null);
            this.txtBETE3.TextChanged += new System.EventHandler(this.txtBETE3_TextChanged);
            this.txtBETE3.Validating += new System.ComponentModel.CancelEventHandler(this.txtBETE3_Validating);
            // 
            // txtBETE8
            // 
            this.txtBETE8.BackColor = System.Drawing.SystemColors.Window;
            this.txtBETE8.Location = new System.Drawing.Point(379, 987);
            this.txtBETE8.MaxLength = 13;
            this.txtBETE8.Name = "txtBETE8";
            this.txtBETE8.Size = new System.Drawing.Size(118, 40);
            this.txtBETE8.TabIndex = 46;
            this.txtBETE8.Tag = "values";
            this.txtBETE8.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtBETE8, null);
            this.txtBETE8.TextChanged += new System.EventHandler(this.txtBETE8_TextChanged);
            this.txtBETE8.Validating += new System.ComponentModel.CancelEventHandler(this.txtBETE8_Validating);
            // 
            // txtBETE7
            // 
            this.txtBETE7.BackColor = System.Drawing.SystemColors.Window;
            this.txtBETE7.Location = new System.Drawing.Point(379, 927);
            this.txtBETE7.MaxLength = 13;
            this.txtBETE7.Name = "txtBETE7";
            this.txtBETE7.Size = new System.Drawing.Size(118, 40);
            this.txtBETE7.TabIndex = 43;
            this.txtBETE7.Tag = "values";
            this.txtBETE7.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtBETE7, null);
            this.txtBETE7.TextChanged += new System.EventHandler(this.txtBETE7_TextChanged);
            this.txtBETE7.Validating += new System.ComponentModel.CancelEventHandler(this.txtBETE7_Validating);
            // 
            // txtBETE6
            // 
            this.txtBETE6.BackColor = System.Drawing.SystemColors.Window;
            this.txtBETE6.Location = new System.Drawing.Point(379, 867);
            this.txtBETE6.MaxLength = 13;
            this.txtBETE6.Name = "txtBETE6";
            this.txtBETE6.Size = new System.Drawing.Size(118, 40);
            this.txtBETE6.TabIndex = 40;
            this.txtBETE6.Tag = "values";
            this.txtBETE6.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtBETE6, null);
            this.txtBETE6.TextChanged += new System.EventHandler(this.txtBETE6_TextChanged);
            this.txtBETE6.Validating += new System.ComponentModel.CancelEventHandler(this.txtBETE6_Validating);
            // 
            // txtBETE5
            // 
            this.txtBETE5.BackColor = System.Drawing.SystemColors.Window;
            this.txtBETE5.Location = new System.Drawing.Point(379, 807);
            this.txtBETE5.MaxLength = 13;
            this.txtBETE5.Name = "txtBETE5";
            this.txtBETE5.Size = new System.Drawing.Size(118, 40);
            this.txtBETE5.TabIndex = 37;
            this.txtBETE5.Tag = "values";
            this.txtBETE5.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtBETE5, null);
            this.txtBETE5.TextChanged += new System.EventHandler(this.txtBETE5_TextChanged);
            this.txtBETE5.Validating += new System.ComponentModel.CancelEventHandler(this.txtBETE5_Validating);
            // 
            // txtBETE4
            // 
            this.txtBETE4.BackColor = System.Drawing.SystemColors.Window;
            this.txtBETE4.Location = new System.Drawing.Point(379, 747);
            this.txtBETE4.MaxLength = 13;
            this.txtBETE4.Name = "txtBETE4";
            this.txtBETE4.Size = new System.Drawing.Size(118, 40);
            this.txtBETE4.TabIndex = 34;
            this.txtBETE4.Tag = "values";
            this.txtBETE4.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtBETE4, null);
            this.txtBETE4.TextChanged += new System.EventHandler(this.txtBETE4_TextChanged);
            this.txtBETE4.Validating += new System.ComponentModel.CancelEventHandler(this.txtBETE4_Validating);
            // 
            // txtBETE9
            // 
            this.txtBETE9.BackColor = System.Drawing.SystemColors.Window;
            this.txtBETE9.Location = new System.Drawing.Point(379, 1047);
            this.txtBETE9.MaxLength = 13;
            this.txtBETE9.Name = "txtBETE9";
            this.txtBETE9.Size = new System.Drawing.Size(118, 40);
            this.txtBETE9.TabIndex = 49;
            this.txtBETE9.Tag = "values";
            this.txtBETE9.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtBETE9, null);
            this.txtBETE9.TextChanged += new System.EventHandler(this.txtBETE9_TextChanged);
            this.txtBETE9.Validating += new System.ComponentModel.CancelEventHandler(this.txtBETE9_Validating);
            // 
            // txtApt
            // 
            this.txtApt.BackColor = System.Drawing.SystemColors.Window;
            this.txtApt.Location = new System.Drawing.Point(274, 348);
            this.txtApt.MaxLength = 1;
            this.txtApt.Name = "txtApt";
            this.txtApt.Size = new System.Drawing.Size(40, 40);
            this.txtApt.TabIndex = 15;
            this.txtApt.Tag = "address";
            this.ToolTip1.SetToolTip(this.txtApt, null);
            this.txtApt.TextChanged += new System.EventHandler(this.txtApt_TextChanged);
            // 
            // txtSquareFootage
            // 
            this.txtSquareFootage.BackColor = System.Drawing.SystemColors.Window;
            this.txtSquareFootage.Location = new System.Drawing.Point(785, 340);
            this.txtSquareFootage.MaxLength = 11;
            this.txtSquareFootage.Name = "txtSquareFootage";
            this.txtSquareFootage.Size = new System.Drawing.Size(132, 40);
            this.txtSquareFootage.TabIndex = 57;
            this.txtSquareFootage.Tag = "values";
            this.txtSquareFootage.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtSquareFootage, null);
            this.txtSquareFootage.TextChanged += new System.EventHandler(this.txtSquareFootage_TextChanged);
            this.txtSquareFootage.Validating += new System.ComponentModel.CancelEventHandler(this.txtSquareFootage_Validating);
            // 
            // fraRate
            // 
            this.fraRate.Controls.Add(this.Label2_7);
            this.fraRate.Controls.Add(this.lblTotalValue);
            this.fraRate.Controls.Add(this.Label2_5);
            this.fraRate.Controls.Add(this.lblTaxRate);
            this.fraRate.Controls.Add(this.Label2_3);
            this.fraRate.Controls.Add(this.lblTotalTax);
            this.fraRate.ForeColor = System.Drawing.SystemColors.Highlight;
            this.fraRate.Location = new System.Drawing.Point(611, 650);
            this.fraRate.Name = "fraRate";
            this.fraRate.Size = new System.Drawing.Size(306, 138);
            this.fraRate.TabIndex = 69;
            this.fraRate.Text = "Tax Year ";
            this.ToolTip1.SetToolTip(this.fraRate, null);
            this.fraRate.Visible = false;
            // 
            // Label2_7
            // 
            this.Label2_7.AutoSize = true;
            this.Label2_7.Location = new System.Drawing.Point(20, 33);
            this.Label2_7.Name = "Label2_7";
            this.Label2_7.Size = new System.Drawing.Size(63, 15);
            this.Label2_7.Text = "TAXABLE";
            this.ToolTip1.SetToolTip(this.Label2_7, null);
            // 
            // lblTotalValue
            // 
            this.lblTotalValue.BackColor = System.Drawing.Color.FromName("@window");
            this.lblTotalValue.Location = new System.Drawing.Point(126, 30);
            this.lblTotalValue.Name = "lblTotalValue";
            this.lblTotalValue.Size = new System.Drawing.Size(160, 16);
            this.lblTotalValue.TabIndex = 1;
            this.lblTotalValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ToolTip1.SetToolTip(this.lblTotalValue, null);
            // 
            // Label2_5
            // 
            this.Label2_5.AutoSize = true;
            this.Label2_5.Location = new System.Drawing.Point(20, 69);
            this.Label2_5.Name = "Label2_5";
            this.Label2_5.Size = new System.Drawing.Size(68, 15);
            this.Label2_5.TabIndex = 2;
            this.Label2_5.Text = "TAX RATE";
            this.ToolTip1.SetToolTip(this.Label2_5, null);
            // 
            // lblTaxRate
            // 
            this.lblTaxRate.BackColor = System.Drawing.Color.FromName("@window");
            this.lblTaxRate.Location = new System.Drawing.Point(126, 66);
            this.lblTaxRate.Name = "lblTaxRate";
            this.lblTaxRate.Size = new System.Drawing.Size(160, 16);
            this.lblTaxRate.TabIndex = 3;
            this.lblTaxRate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ToolTip1.SetToolTip(this.lblTaxRate, null);
            // 
            // Label2_3
            // 
            this.Label2_3.AutoSize = true;
            this.Label2_3.Location = new System.Drawing.Point(20, 103);
            this.Label2_3.Name = "Label2_3";
            this.Label2_3.Size = new System.Drawing.Size(75, 15);
            this.Label2_3.TabIndex = 4;
            this.Label2_3.Text = "TOTAL TAX";
            this.ToolTip1.SetToolTip(this.Label2_3, null);
            // 
            // lblTotalTax
            // 
            this.lblTotalTax.BackColor = System.Drawing.Color.FromName("@window");
            this.lblTotalTax.Location = new System.Drawing.Point(126, 102);
            this.lblTotalTax.Name = "lblTotalTax";
            this.lblTotalTax.Size = new System.Drawing.Size(160, 16);
            this.lblTotalTax.TabIndex = 5;
            this.lblTotalTax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ToolTip1.SetToolTip(this.lblTotalTax, null);
            // 
            // txtCategory9
            // 
            this.txtCategory9.BackColor = System.Drawing.SystemColors.Window;
            this.txtCategory9.Location = new System.Drawing.Point(236, 1047);
            this.txtCategory9.MaxLength = 13;
            this.txtCategory9.Name = "txtCategory9";
            this.txtCategory9.Size = new System.Drawing.Size(118, 40);
            this.txtCategory9.TabIndex = 48;
            this.txtCategory9.Tag = "values";
            this.txtCategory9.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtCategory9, null);
            this.txtCategory9.Enter += new System.EventHandler(this.txtCategory9_Enter);
            this.txtCategory9.Leave += new System.EventHandler(this.txtCategory9_Leave);
            this.txtCategory9.TextChanged += new System.EventHandler(this.txtCategory9_TextChanged);
            this.txtCategory9.Validating += new System.ComponentModel.CancelEventHandler(this.txtCategory9_Validating);
            this.txtCategory9.KeyDown += new Wisej.Web.KeyEventHandler(this.txtCategory9_KeyDown);
            this.txtCategory9.KeyUp += new Wisej.Web.KeyEventHandler(this.txtCategory9_KeyUp);
            // 
            // txtExemptCode2
            // 
            this.txtExemptCode2.BackColor = System.Drawing.SystemColors.Window;
            this.txtExemptCode2.Location = new System.Drawing.Point(856, 400);
            this.txtExemptCode2.MaxLength = 2;
            this.txtExemptCode2.Name = "txtExemptCode2";
            this.txtExemptCode2.Size = new System.Drawing.Size(61, 40);
            this.txtExemptCode2.TabIndex = 60;
            this.txtExemptCode2.Tag = "values";
            this.ToolTip1.SetToolTip(this.txtExemptCode2, null);
            this.txtExemptCode2.Enter += new System.EventHandler(this.txtExemptCode2_Enter);
            this.txtExemptCode2.TextChanged += new System.EventHandler(this.txtExemptCode2_TextChanged);
            this.txtExemptCode2.Validating += new System.ComponentModel.CancelEventHandler(this.txtExemptCode2_Validating);
            this.txtExemptCode2.KeyDown += new Wisej.Web.KeyEventHandler(this.txtExemptCode2_KeyDown);
            // 
            // txtStreetName
            // 
            this.txtStreetName.BackColor = System.Drawing.SystemColors.Window;
            this.txtStreetName.Location = new System.Drawing.Point(324, 348);
            this.txtStreetName.MaxLength = 26;
            this.txtStreetName.Name = "txtStreetName";
            this.txtStreetName.Size = new System.Drawing.Size(247, 40);
            this.txtStreetName.TabIndex = 16;
            this.txtStreetName.Tag = "address";
            this.ToolTip1.SetToolTip(this.txtStreetName, null);
            this.txtStreetName.Enter += new System.EventHandler(this.txtStreetName_Enter);
            this.txtStreetName.TextChanged += new System.EventHandler(this.txtStreetName_TextChanged);
            // 
            // txtStreetNumber
            // 
            this.txtStreetNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtStreetNumber.Location = new System.Drawing.Point(177, 348);
            this.txtStreetNumber.MaxLength = 5;
            this.txtStreetNumber.Name = "txtStreetNumber";
            this.txtStreetNumber.Size = new System.Drawing.Size(87, 40);
            this.txtStreetNumber.TabIndex = 14;
            this.txtStreetNumber.Tag = "address";
            this.ToolTip1.SetToolTip(this.txtStreetNumber, null);
            this.txtStreetNumber.Enter += new System.EventHandler(this.txtStreetNumber_Enter);
            this.txtStreetNumber.TextChanged += new System.EventHandler(this.txtStreetNumber_TextChanged);
            // 
            // txtExemption
            // 
            this.txtExemption.BackColor = System.Drawing.SystemColors.Window;
            this.txtExemption.Location = new System.Drawing.Point(785, 460);
            this.txtExemption.MaxLength = 11;
            this.txtExemption.Name = "txtExemption";
            this.txtExemption.Size = new System.Drawing.Size(132, 40);
            this.txtExemption.TabIndex = 62;
            this.txtExemption.Tag = "values";
            this.txtExemption.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtExemption, null);
            this.txtExemption.Enter += new System.EventHandler(this.txtExemption_Enter);
            this.txtExemption.TextChanged += new System.EventHandler(this.txtExemption_TextChanged);
            this.txtExemption.Validating += new System.ComponentModel.CancelEventHandler(this.txtExemption_Validating);
            // 
            // txtExemptCode1
            // 
            this.txtExemptCode1.BackColor = System.Drawing.SystemColors.Window;
            this.txtExemptCode1.Location = new System.Drawing.Point(785, 400);
            this.txtExemptCode1.MaxLength = 2;
            this.txtExemptCode1.Name = "txtExemptCode1";
            this.txtExemptCode1.Size = new System.Drawing.Size(61, 40);
            this.txtExemptCode1.TabIndex = 59;
            this.txtExemptCode1.Tag = "values";
            this.ToolTip1.SetToolTip(this.txtExemptCode1, null);
            this.txtExemptCode1.Enter += new System.EventHandler(this.txtExemptCode1_Enter);
            this.txtExemptCode1.TextChanged += new System.EventHandler(this.txtExemptCode1_TextChanged);
            this.txtExemptCode1.Validating += new System.ComponentModel.CancelEventHandler(this.txtExemptCode1_Validating);
            this.txtExemptCode1.KeyDown += new Wisej.Web.KeyEventHandler(this.txtExemptCode1_KeyDown);
            // 
            // txtBusinessCode
            // 
            this.txtBusinessCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtBusinessCode.Location = new System.Drawing.Point(785, 220);
            this.txtBusinessCode.Name = "txtBusinessCode";
            this.txtBusinessCode.Size = new System.Drawing.Size(132, 40);
            this.txtBusinessCode.TabIndex = 53;
            this.txtBusinessCode.Tag = "values";
            this.ToolTip1.SetToolTip(this.txtBusinessCode, null);
            this.txtBusinessCode.Enter += new System.EventHandler(this.txtBusinessCode_Enter);
            this.txtBusinessCode.TextChanged += new System.EventHandler(this.txtBusinessCode_TextChanged);
            this.txtBusinessCode.KeyDown += new Wisej.Web.KeyEventHandler(this.txtBusinessCode_KeyDown);
            // 
            // txtStreetCode
            // 
            this.txtStreetCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtStreetCode.Location = new System.Drawing.Point(785, 280);
            this.txtStreetCode.Name = "txtStreetCode";
            this.txtStreetCode.Size = new System.Drawing.Size(132, 40);
            this.txtStreetCode.TabIndex = 55;
            this.txtStreetCode.Tag = "values";
            this.ToolTip1.SetToolTip(this.txtStreetCode, null);
            this.txtStreetCode.Enter += new System.EventHandler(this.txtStreetCode_Enter);
            this.txtStreetCode.TextChanged += new System.EventHandler(this.txtStreetCode_TextChanged);
            this.txtStreetCode.KeyDown += new Wisej.Web.KeyEventHandler(this.txtStreetCode_KeyDown);
            // 
            // txtOpen1
            // 
            this.txtOpen1.BackColor = System.Drawing.SystemColors.Window;
            this.txtOpen1.Location = new System.Drawing.Point(177, 408);
            this.txtOpen1.MaxLength = 34;
            this.txtOpen1.Name = "txtOpen1";
            this.txtOpen1.Size = new System.Drawing.Size(394, 40);
            this.txtOpen1.TabIndex = 18;
            this.txtOpen1.Tag = "address";
            this.ToolTip1.SetToolTip(this.txtOpen1, null);
            this.txtOpen1.Enter += new System.EventHandler(this.txtOpen1_Enter);
            this.txtOpen1.TextChanged += new System.EventHandler(this.txtOpen1_TextChanged);
            // 
            // txtOpen2
            // 
            this.txtOpen2.BackColor = System.Drawing.SystemColors.Window;
            this.txtOpen2.Location = new System.Drawing.Point(177, 468);
            this.txtOpen2.MaxLength = 34;
            this.txtOpen2.Name = "txtOpen2";
            this.txtOpen2.Size = new System.Drawing.Size(394, 40);
            this.txtOpen2.TabIndex = 20;
            this.txtOpen2.Tag = "address";
            this.ToolTip1.SetToolTip(this.txtOpen2, null);
            this.txtOpen2.Enter += new System.EventHandler(this.txtOpen2_Enter);
            this.txtOpen2.TextChanged += new System.EventHandler(this.txtOpen2_TextChanged);
            // 
            // txtCategory4
            // 
            this.txtCategory4.BackColor = System.Drawing.SystemColors.Window;
            this.txtCategory4.Location = new System.Drawing.Point(236, 747);
            this.txtCategory4.MaxLength = 13;
            this.txtCategory4.Name = "txtCategory4";
            this.txtCategory4.Size = new System.Drawing.Size(118, 40);
            this.txtCategory4.TabIndex = 33;
            this.txtCategory4.Tag = "values";
            this.txtCategory4.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtCategory4, null);
            this.txtCategory4.Enter += new System.EventHandler(this.txtCategory4_Enter);
            this.txtCategory4.Leave += new System.EventHandler(this.txtCategory4_Leave);
            this.txtCategory4.TextChanged += new System.EventHandler(this.txtCategory4_TextChanged);
            this.txtCategory4.Validating += new System.ComponentModel.CancelEventHandler(this.txtCategory4_Validating);
            this.txtCategory4.KeyDown += new Wisej.Web.KeyEventHandler(this.txtCategory4_KeyDown);
            this.txtCategory4.KeyUp += new Wisej.Web.KeyEventHandler(this.txtCategory4_KeyUp);
            // 
            // txtCategory5
            // 
            this.txtCategory5.BackColor = System.Drawing.SystemColors.Window;
            this.txtCategory5.Location = new System.Drawing.Point(236, 807);
            this.txtCategory5.MaxLength = 13;
            this.txtCategory5.Name = "txtCategory5";
            this.txtCategory5.Size = new System.Drawing.Size(118, 40);
            this.txtCategory5.TabIndex = 36;
            this.txtCategory5.Tag = "values";
            this.txtCategory5.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtCategory5, null);
            this.txtCategory5.Enter += new System.EventHandler(this.txtCategory5_Enter);
            this.txtCategory5.Leave += new System.EventHandler(this.txtCategory5_Leave);
            this.txtCategory5.TextChanged += new System.EventHandler(this.txtCategory5_TextChanged);
            this.txtCategory5.Validating += new System.ComponentModel.CancelEventHandler(this.txtCategory5_Validating);
            this.txtCategory5.KeyDown += new Wisej.Web.KeyEventHandler(this.txtCategory5_KeyDown);
            this.txtCategory5.KeyUp += new Wisej.Web.KeyEventHandler(this.txtCategory5_KeyUp);
            // 
            // txtCategory6
            // 
            this.txtCategory6.BackColor = System.Drawing.SystemColors.Window;
            this.txtCategory6.Location = new System.Drawing.Point(236, 867);
            this.txtCategory6.MaxLength = 13;
            this.txtCategory6.Name = "txtCategory6";
            this.txtCategory6.Size = new System.Drawing.Size(118, 40);
            this.txtCategory6.TabIndex = 39;
            this.txtCategory6.Tag = "values";
            this.txtCategory6.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtCategory6, null);
            this.txtCategory6.Enter += new System.EventHandler(this.txtCategory6_Enter);
            this.txtCategory6.Leave += new System.EventHandler(this.txtCategory6_Leave);
            this.txtCategory6.TextChanged += new System.EventHandler(this.txtCategory6_TextChanged);
            this.txtCategory6.Validating += new System.ComponentModel.CancelEventHandler(this.txtCategory6_Validating);
            this.txtCategory6.KeyDown += new Wisej.Web.KeyEventHandler(this.txtCategory6_KeyDown);
            this.txtCategory6.KeyUp += new Wisej.Web.KeyEventHandler(this.txtCategory6_KeyUp);
            // 
            // txtCategory7
            // 
            this.txtCategory7.BackColor = System.Drawing.SystemColors.Window;
            this.txtCategory7.Location = new System.Drawing.Point(236, 927);
            this.txtCategory7.MaxLength = 13;
            this.txtCategory7.Name = "txtCategory7";
            this.txtCategory7.Size = new System.Drawing.Size(118, 40);
            this.txtCategory7.TabIndex = 42;
            this.txtCategory7.Tag = "values";
            this.txtCategory7.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtCategory7, null);
            this.txtCategory7.Enter += new System.EventHandler(this.txtCategory7_Enter);
            this.txtCategory7.Leave += new System.EventHandler(this.txtCategory7_Leave);
            this.txtCategory7.TextChanged += new System.EventHandler(this.txtCategory7_TextChanged);
            this.txtCategory7.Validating += new System.ComponentModel.CancelEventHandler(this.txtCategory7_Validating);
            this.txtCategory7.KeyDown += new Wisej.Web.KeyEventHandler(this.txtCategory7_KeyDown);
            this.txtCategory7.KeyUp += new Wisej.Web.KeyEventHandler(this.txtCategory7_KeyUp);
            // 
            // txtCategory8
            // 
            this.txtCategory8.BackColor = System.Drawing.SystemColors.Window;
            this.txtCategory8.Location = new System.Drawing.Point(236, 987);
            this.txtCategory8.MaxLength = 13;
            this.txtCategory8.Name = "txtCategory8";
            this.txtCategory8.Size = new System.Drawing.Size(118, 40);
            this.txtCategory8.TabIndex = 45;
            this.txtCategory8.Tag = "values";
            this.txtCategory8.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtCategory8, null);
            this.txtCategory8.Enter += new System.EventHandler(this.txtCategory8_Enter);
            this.txtCategory8.Leave += new System.EventHandler(this.txtCategory8_Leave);
            this.txtCategory8.TextChanged += new System.EventHandler(this.txtCategory8_TextChanged);
            this.txtCategory8.Validating += new System.ComponentModel.CancelEventHandler(this.txtCategory8_Validating);
            this.txtCategory8.KeyDown += new Wisej.Web.KeyEventHandler(this.txtCategory8_KeyDown);
            this.txtCategory8.KeyUp += new Wisej.Web.KeyEventHandler(this.txtCategory8_KeyUp);
            // 
            // txtCategory3
            // 
            this.txtCategory3.BackColor = System.Drawing.SystemColors.Window;
            this.txtCategory3.Location = new System.Drawing.Point(236, 687);
            this.txtCategory3.MaxLength = 13;
            this.txtCategory3.Name = "txtCategory3";
            this.txtCategory3.Size = new System.Drawing.Size(118, 40);
            this.txtCategory3.TabIndex = 30;
            this.txtCategory3.Tag = "values";
            this.txtCategory3.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtCategory3, null);
            this.txtCategory3.Enter += new System.EventHandler(this.txtCategory3_Enter);
            this.txtCategory3.Leave += new System.EventHandler(this.txtCategory3_Leave);
            this.txtCategory3.TextChanged += new System.EventHandler(this.txtCategory3_TextChanged);
            this.txtCategory3.Validating += new System.ComponentModel.CancelEventHandler(this.txtCategory3_Validating);
            this.txtCategory3.KeyDown += new Wisej.Web.KeyEventHandler(this.txtCategory3_KeyDown);
            this.txtCategory3.KeyUp += new Wisej.Web.KeyEventHandler(this.txtCategory3_KeyUp);
            // 
            // txtCategory2
            // 
            this.txtCategory2.BackColor = System.Drawing.SystemColors.Window;
            this.txtCategory2.Location = new System.Drawing.Point(236, 623);
            this.txtCategory2.MaxLength = 13;
            this.txtCategory2.Name = "txtCategory2";
            this.txtCategory2.Size = new System.Drawing.Size(118, 40);
            this.txtCategory2.TabIndex = 27;
            this.txtCategory2.Tag = "values";
            this.txtCategory2.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtCategory2, null);
            this.txtCategory2.Enter += new System.EventHandler(this.txtCategory2_Enter);
            this.txtCategory2.Leave += new System.EventHandler(this.txtCategory2_Leave);
            this.txtCategory2.TextChanged += new System.EventHandler(this.txtCategory2_TextChanged);
            this.txtCategory2.Validating += new System.ComponentModel.CancelEventHandler(this.txtCategory2_Validating);
            this.txtCategory2.KeyDown += new Wisej.Web.KeyEventHandler(this.txtCategory2_KeyDown);
            this.txtCategory2.KeyUp += new Wisej.Web.KeyEventHandler(this.txtCategory2_KeyUp);
            // 
            // txtCategory1
            // 
            this.txtCategory1.BackColor = System.Drawing.SystemColors.Window;
            this.txtCategory1.Location = new System.Drawing.Point(236, 563);
            this.txtCategory1.MaxLength = 13;
            this.txtCategory1.Name = "txtCategory1";
            this.txtCategory1.Size = new System.Drawing.Size(118, 40);
            this.txtCategory1.TabIndex = 22;
            this.txtCategory1.Tag = "values";
            this.txtCategory1.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtCategory1, null);
            this.txtCategory1.Enter += new System.EventHandler(this.txtCategory1_Enter);
            this.txtCategory1.Leave += new System.EventHandler(this.txtCategory1_Leave);
            this.txtCategory1.TextChanged += new System.EventHandler(this.txtCategory1_TextChanged);
            this.txtCategory1.Validating += new System.ComponentModel.CancelEventHandler(this.txtCategory1_Validating);
            this.txtCategory1.KeyDown += new Wisej.Web.KeyEventHandler(this.txtCategory1_KeyDown);
            this.txtCategory1.KeyUp += new Wisej.Web.KeyEventHandler(this.txtCategory1_KeyUp);
            // 
            // gridTranCode
            // 
            this.gridTranCode.BackColorAlternate = System.Drawing.SystemColors.Window;
            this.gridTranCode.BackColorBkg = System.Drawing.SystemColors.AppWorkspace;
            this.gridTranCode.BackColorFixed = System.Drawing.SystemColors.Control;
            this.gridTranCode.BackColorSel = System.Drawing.SystemColors.Highlight;
            this.gridTranCode.Cols = 1;
            this.gridTranCode.ColumnHeadersVisible = false;
            this.gridTranCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.gridTranCode.ExtendLastCol = true;
            this.gridTranCode.FixedCols = 0;
            this.gridTranCode.FixedRows = 0;
            this.gridTranCode.ForeColorFixed = System.Drawing.SystemColors.ControlText;
            this.gridTranCode.GridColor = System.Drawing.SystemColors.Control;
            this.gridTranCode.GridColorFixed = System.Drawing.SystemColors.ControlDark;
            this.gridTranCode.HighLight = fecherFoundation.FCGrid.HighLightSettings.flexHighlightNever;
            this.gridTranCode.Location = new System.Drawing.Point(785, 160);
            this.gridTranCode.Name = "gridTranCode";
            this.gridTranCode.ReadOnly = false;
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gridTranCode.RowHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridTranCode.RowHeadersVisible = false;
            this.gridTranCode.Rows = 1;
            this.gridTranCode.ShowFocusCell = false;
            this.gridTranCode.Size = new System.Drawing.Size(132, 42);
            this.gridTranCode.TabIndex = 51;
            this.gridTranCode.Tag = "values";
            this.ToolTip1.SetToolTip(this.gridTranCode, null);
            this.gridTranCode.ComboCloseUp += new System.EventHandler(this.gridTranCode_ComboCloseUp);
            this.gridTranCode.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.gridTranCode_CellChanged);
            this.gridTranCode.Leave += new System.EventHandler(this.gridTranCode_Leave);
            // 
            // lblAddress
            // 
            this.lblAddress.Location = new System.Drawing.Point(177, 255);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(394, 73);
            this.lblAddress.TabIndex = 12;
            this.ToolTip1.SetToolTip(this.lblAddress, null);
            // 
            // lblOwner1
            // 
            this.lblOwner1.Location = new System.Drawing.Point(177, 220);
            this.lblOwner1.Name = "lblOwner1";
            this.lblOwner1.Size = new System.Drawing.Size(290, 15);
            this.lblOwner1.TabIndex = 10;
            this.ToolTip1.SetToolTip(this.lblOwner1, null);
            // 
            // Label14
            // 
            this.Label14.AutoSize = true;
            this.Label14.Location = new System.Drawing.Point(30, 174);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(55, 15);
            this.Label14.TabIndex = 5;
            this.Label14.Text = "OWNER";
            this.ToolTip1.SetToolTip(this.Label14, null);
            // 
            // Label2_9
            // 
            this.Label2_9.Location = new System.Drawing.Point(30, 255);
            this.Label2_9.Name = "Label2_9";
            this.Label2_9.Size = new System.Drawing.Size(80, 17);
            this.Label2_9.TabIndex = 11;
            this.Label2_9.Text = "ADDRESS";
            this.ToolTip1.SetToolTip(this.Label2_9, null);
            // 
            // Label2_4
            // 
            this.Label2_4.AutoSize = true;
            this.Label2_4.Location = new System.Drawing.Point(611, 615);
            this.Label2_4.Name = "Label2_4";
            this.Label2_4.Size = new System.Drawing.Size(40, 15);
            this.Label2_4.TabIndex = 67;
            this.Label2_4.Text = "DATE";
            this.ToolTip1.SetToolTip(this.Label2_4, null);
            this.Label2_4.Visible = false;
            // 
            // lblDate
            // 
            this.lblDate.Location = new System.Drawing.Point(785, 615);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(132, 15);
            this.lblDate.TabIndex = 68;
            this.lblDate.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.ToolTip1.SetToolTip(this.lblDate, null);
            this.lblDate.Visible = false;
            // 
            // Label2_0
            // 
            this.Label2_0.AutoSize = true;
            this.Label2_0.Location = new System.Drawing.Point(611, 534);
            this.Label2_0.Name = "Label2_0";
            this.Label2_0.Size = new System.Drawing.Size(47, 15);
            this.Label2_0.TabIndex = 63;
            this.Label2_0.Text = "TOTAL";
            this.ToolTip1.SetToolTip(this.Label2_0, null);
            // 
            // Label2_1
            // 
            this.Label2_1.AutoSize = true;
            this.Label2_1.Location = new System.Drawing.Point(611, 580);
            this.Label2_1.Name = "Label2_1";
            this.Label2_1.Size = new System.Drawing.Size(75, 15);
            this.Label2_1.TabIndex = 65;
            this.Label2_1.Text = "WITH BETE";
            this.ToolTip1.SetToolTip(this.Label2_1, null);
            // 
            // lblTotWithBETE
            // 
            this.lblTotWithBETE.Location = new System.Drawing.Point(785, 580);
            this.lblTotWithBETE.Name = "lblTotWithBETE";
            this.lblTotWithBETE.Size = new System.Drawing.Size(132, 15);
            this.lblTotWithBETE.TabIndex = 66;
            this.lblTotWithBETE.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.ToolTip1.SetToolTip(this.lblTotWithBETE, null);
            // 
            // lblTotExempt
            // 
            this.lblTotExempt.Location = new System.Drawing.Point(785, 615);
            this.lblTotExempt.Name = "lblTotExempt";
            this.lblTotExempt.Size = new System.Drawing.Size(132, 15);
            this.lblTotExempt.TabIndex = 70;
            this.lblTotExempt.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.ToolTip1.SetToolTip(this.lblTotExempt, null);
            // 
            // Label2_2
            // 
            this.Label2_2.AutoSize = true;
            this.Label2_2.Location = new System.Drawing.Point(611, 615);
            this.Label2_2.Name = "Label2_2";
            this.Label2_2.Size = new System.Drawing.Size(101, 15);
            this.Label2_2.TabIndex = 63;
            this.Label2_2.Text = "TOTAL EXEMPT";
            this.ToolTip1.SetToolTip(this.Label2_2, null);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(397, 528);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(86, 15);
            this.Label1.TabIndex = 24;
            this.Label1.Text = "BETE EXEMPT";
            this.ToolTip1.SetToolTip(this.Label1, null);
            // 
            // lblComment
            // 
            this.lblComment.ForeColor = System.Drawing.Color.FromArgb(255, 0, 0);
            this.lblComment.Location = new System.Drawing.Point(30, 24);
            this.lblComment.Name = "lblComment";
            this.lblComment.Size = new System.Drawing.Size(25, 15);
            this.lblComment.Text = "C";
            this.ToolTip1.SetToolTip(this.lblComment, null);
            this.lblComment.Visible = false;
            // 
            // lblStatics_2
            // 
            this.lblStatics_2.AutoSize = true;
            this.lblStatics_2.Location = new System.Drawing.Point(611, 354);
            this.lblStatics_2.Name = "lblStatics_2";
            this.lblStatics_2.Size = new System.Drawing.Size(123, 15);
            this.lblStatics_2.TabIndex = 56;
            this.lblStatics_2.Text = "SQUARE FOOTAGE";
            this.ToolTip1.SetToolTip(this.lblStatics_2, null);
            // 
            // txtREAssociate
            // 
            this.txtREAssociate.Location = new System.Drawing.Point(177, 110);
            this.txtREAssociate.Name = "txtREAssociate";
            this.txtREAssociate.Size = new System.Drawing.Size(47, 15);
            this.txtREAssociate.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.txtREAssociate, null);
            // 
            // lblCategory_8
            // 
            this.lblCategory_8.Location = new System.Drawing.Point(30, 1061);
            this.lblCategory_8.Name = "lblCategory_8";
            this.lblCategory_8.Size = new System.Drawing.Size(156, 15);
            this.lblCategory_8.TabIndex = 47;
            this.ToolTip1.SetToolTip(this.lblCategory_8, null);
            // 
            // lblCategory_7
            // 
            this.lblCategory_7.Location = new System.Drawing.Point(30, 1001);
            this.lblCategory_7.Name = "lblCategory_7";
            this.lblCategory_7.Size = new System.Drawing.Size(156, 15);
            this.lblCategory_7.TabIndex = 44;
            this.ToolTip1.SetToolTip(this.lblCategory_7, null);
            // 
            // lblCategory_6
            // 
            this.lblCategory_6.Location = new System.Drawing.Point(30, 941);
            this.lblCategory_6.Name = "lblCategory_6";
            this.lblCategory_6.Size = new System.Drawing.Size(156, 15);
            this.lblCategory_6.TabIndex = 41;
            this.ToolTip1.SetToolTip(this.lblCategory_6, null);
            // 
            // lblCategory_5
            // 
            this.lblCategory_5.Location = new System.Drawing.Point(30, 881);
            this.lblCategory_5.Name = "lblCategory_5";
            this.lblCategory_5.Size = new System.Drawing.Size(156, 15);
            this.lblCategory_5.TabIndex = 38;
            this.ToolTip1.SetToolTip(this.lblCategory_5, null);
            // 
            // lblCategory_4
            // 
            this.lblCategory_4.Location = new System.Drawing.Point(30, 821);
            this.lblCategory_4.Name = "lblCategory_4";
            this.lblCategory_4.Size = new System.Drawing.Size(156, 15);
            this.lblCategory_4.TabIndex = 35;
            this.ToolTip1.SetToolTip(this.lblCategory_4, null);
            // 
            // lblCategory_3
            // 
            this.lblCategory_3.Location = new System.Drawing.Point(30, 761);
            this.lblCategory_3.Name = "lblCategory_3";
            this.lblCategory_3.Size = new System.Drawing.Size(156, 15);
            this.lblCategory_3.TabIndex = 32;
            this.ToolTip1.SetToolTip(this.lblCategory_3, null);
            // 
            // lblCategory_2
            // 
            this.lblCategory_2.Location = new System.Drawing.Point(30, 701);
            this.lblCategory_2.Name = "lblCategory_2";
            this.lblCategory_2.Size = new System.Drawing.Size(156, 15);
            this.lblCategory_2.TabIndex = 29;
            this.ToolTip1.SetToolTip(this.lblCategory_2, null);
            // 
            // lblCategory_1
            // 
            this.lblCategory_1.Location = new System.Drawing.Point(30, 637);
            this.lblCategory_1.Name = "lblCategory_1";
            this.lblCategory_1.Size = new System.Drawing.Size(156, 15);
            this.lblCategory_1.TabIndex = 26;
            this.ToolTip1.SetToolTip(this.lblCategory_1, null);
            // 
            // lblCategory_0
            // 
            this.lblCategory_0.Location = new System.Drawing.Point(30, 577);
            this.lblCategory_0.Name = "lblCategory_0";
            this.lblCategory_0.Size = new System.Drawing.Size(156, 15);
            this.lblCategory_0.TabIndex = 21;
            this.ToolTip1.SetToolTip(this.lblCategory_0, null);
            // 
            // lblStatics_0
            // 
            this.lblStatics_0.Location = new System.Drawing.Point(30, 110);
            this.lblStatics_0.Name = "lblStatics_0";
            this.lblStatics_0.Size = new System.Drawing.Size(97, 15);
            this.lblStatics_0.TabIndex = 3;
            this.lblStatics_0.Text = "RE ASSOCIATE";
            this.ToolTip1.SetToolTip(this.lblStatics_0, null);
            // 
            // lblOpen2
            // 
            this.lblOpen2.AutoSize = true;
            this.lblOpen2.Location = new System.Drawing.Point(30, 482);
            this.lblOpen2.Name = "lblOpen2";
            this.lblOpen2.Size = new System.Drawing.Size(53, 15);
            this.lblOpen2.TabIndex = 19;
            this.lblOpen2.Text = "OPEN 2";
            this.ToolTip1.SetToolTip(this.lblOpen2, null);
            // 
            // lblOpen1
            // 
            this.lblOpen1.AutoSize = true;
            this.lblOpen1.Location = new System.Drawing.Point(30, 422);
            this.lblOpen1.Name = "lblOpen1";
            this.lblOpen1.Size = new System.Drawing.Size(53, 15);
            this.lblOpen1.TabIndex = 17;
            this.lblOpen1.Text = "OPEN 1";
            this.ToolTip1.SetToolTip(this.lblOpen1, null);
            // 
            // lblStatics_14
            // 
            this.lblStatics_14.AutoSize = true;
            this.lblStatics_14.Location = new System.Drawing.Point(611, 294);
            this.lblStatics_14.Name = "lblStatics_14";
            this.lblStatics_14.Size = new System.Drawing.Size(95, 15);
            this.lblStatics_14.TabIndex = 54;
            this.lblStatics_14.Text = "STREET CODE";
            this.ToolTip1.SetToolTip(this.lblStatics_14, null);
            // 
            // lblStatics_15
            // 
            this.lblStatics_15.AutoSize = true;
            this.lblStatics_15.Location = new System.Drawing.Point(611, 234);
            this.lblStatics_15.Name = "lblStatics_15";
            this.lblStatics_15.Size = new System.Drawing.Size(109, 15);
            this.lblStatics_15.TabIndex = 52;
            this.lblStatics_15.Text = "BUSINESS CODE";
            this.ToolTip1.SetToolTip(this.lblStatics_15, null);
            // 
            // lblStatics_16
            // 
            this.lblStatics_16.AutoSize = true;
            this.lblStatics_16.Location = new System.Drawing.Point(611, 174);
            this.lblStatics_16.Name = "lblStatics_16";
            this.lblStatics_16.Size = new System.Drawing.Size(80, 15);
            this.lblStatics_16.TabIndex = 50;
            this.lblStatics_16.Text = "TRAN CODE";
            this.ToolTip1.SetToolTip(this.lblStatics_16, null);
            // 
            // lblStatics_17
            // 
            this.lblStatics_17.AutoSize = true;
            this.lblStatics_17.Location = new System.Drawing.Point(611, 415);
            this.lblStatics_17.Name = "lblStatics_17";
            this.lblStatics_17.Size = new System.Drawing.Size(106, 15);
            this.lblStatics_17.TabIndex = 58;
            this.lblStatics_17.Text = "EXEMPT CODES";
            this.ToolTip1.SetToolTip(this.lblStatics_17, null);
            // 
            // lblStatics_18
            // 
            this.lblStatics_18.AutoSize = true;
            this.lblStatics_18.Location = new System.Drawing.Point(611, 474);
            this.lblStatics_18.Name = "lblStatics_18";
            this.lblStatics_18.Size = new System.Drawing.Size(80, 15);
            this.lblStatics_18.TabIndex = 61;
            this.lblStatics_18.Text = "EXEMPTION";
            this.ToolTip1.SetToolTip(this.lblStatics_18, null);
            // 
            // lblStatics_20
            // 
            this.lblStatics_20.Location = new System.Drawing.Point(30, 362);
            this.lblStatics_20.Name = "lblStatics_20";
            this.lblStatics_20.Size = new System.Drawing.Size(73, 15);
            this.lblStatics_20.TabIndex = 13;
            this.lblStatics_20.Text = "LOCATION";
            this.ToolTip1.SetToolTip(this.lblStatics_20, null);
            // 
            // lblAccount
            // 
            this.lblAccount.Location = new System.Drawing.Point(177, 65);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(49, 15);
            this.lblAccount.TabIndex = 2;
            this.lblAccount.Text = "000000";
            this.ToolTip1.SetToolTip(this.lblAccount, null);
            // 
            // lblStatics_29
            // 
            this.lblStatics_29.Location = new System.Drawing.Point(30, 65);
            this.lblStatics_29.Name = "lblStatics_29";
            this.lblStatics_29.Size = new System.Drawing.Size(105, 15);
            this.lblStatics_29.TabIndex = 1;
            this.lblStatics_29.Text = "ACCOUNT";
            this.ToolTip1.SetToolTip(this.lblStatics_29, null);
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(265, 528);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(56, 15);
            this.Label6.TabIndex = 23;
            this.Label6.Text = "VALUES";
            this.Label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.Label6, null);
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuDelete,
            this.mnuPrint,
            this.mnuInterestedParties});
            this.MainMenu1.Name = "MainMenu1";
            // 
            // mnuDelete
            // 
            this.mnuDelete.Index = 0;
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Text = "Delete Account";
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // mnuPrint
            // 
            this.mnuPrint.Index = 1;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Text = "Print";
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // mnuInterestedParties
            // 
            this.mnuInterestedParties.Index = 2;
            this.mnuInterestedParties.Name = "mnuInterestedParties";
            this.mnuInterestedParties.Text = "Interested Parties";
            this.mnuInterestedParties.Click += new System.EventHandler(this.mnuInterestedParties_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSepar1,
            this.mnuPrintLabel,
            this.mnuGroup,
            this.mnuChangeAssociation,
            this.mnuComment,
            this.mnuSepar2,
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSepar,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuSepar1
            // 
            this.mnuSepar1.Index = 0;
            this.mnuSepar1.Name = "mnuSepar1";
            this.mnuSepar1.Text = "-";
            // 
            // mnuPrintLabel
            // 
            this.mnuPrintLabel.Index = 1;
            this.mnuPrintLabel.Name = "mnuPrintLabel";
            this.mnuPrintLabel.Shortcut = Wisej.Web.Shortcut.F2;
            this.mnuPrintLabel.Text = "Print Label";
            this.mnuPrintLabel.Click += new System.EventHandler(this.mnuPrintLabel_Click);
            // 
            // mnuGroup
            // 
            this.mnuGroup.Index = 2;
            this.mnuGroup.Name = "mnuGroup";
            this.mnuGroup.Shortcut = Wisej.Web.Shortcut.F4;
            this.mnuGroup.Text = "View Group Information";
            this.mnuGroup.Click += new System.EventHandler(this.mnuGroup_Click);
            // 
            // mnuChangeAssociation
            // 
            this.mnuChangeAssociation.Index = 3;
            this.mnuChangeAssociation.Name = "mnuChangeAssociation";
            this.mnuChangeAssociation.Text = "Change Real Estate Association";
            this.mnuChangeAssociation.Click += new System.EventHandler(this.mnuChangeAssociation_Click);
            // 
            // mnuComment
            // 
            this.mnuComment.Index = 4;
            this.mnuComment.Name = "mnuComment";
            this.mnuComment.Shortcut = Wisej.Web.Shortcut.F5;
            this.mnuComment.Text = "Comment";
            this.mnuComment.Click += new System.EventHandler(this.mnuComment_Click);
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = 5;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 6;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 7;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 8;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 9;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(367, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(100, 48);
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // cmdPrintLabel
            // 
            this.cmdPrintLabel.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintLabel.Location = new System.Drawing.Point(889, 29);
            this.cmdPrintLabel.Name = "cmdPrintLabel";
            this.cmdPrintLabel.Shortcut = Wisej.Web.Shortcut.F2;
            this.cmdPrintLabel.Size = new System.Drawing.Size(83, 24);
            this.cmdPrintLabel.TabIndex = 1;
            this.cmdPrintLabel.Text = "Print Label";
            this.cmdPrintLabel.Click += new System.EventHandler(this.mnuPrintLabel_Click);
            // 
            // cmdGroupInfo
            // 
            this.cmdGroupInfo.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdGroupInfo.Location = new System.Drawing.Point(769, 29);
            this.cmdGroupInfo.Name = "cmdGroupInfo";
            this.cmdGroupInfo.Shortcut = Wisej.Web.Shortcut.F4;
            this.cmdGroupInfo.Size = new System.Drawing.Size(114, 24);
            this.cmdGroupInfo.TabIndex = 1;
            this.cmdGroupInfo.Text = "View Group Info";
            this.cmdGroupInfo.Click += new System.EventHandler(this.mnuGroup_Click);
            // 
            // cmdChangeAssociation
            // 
            this.cmdChangeAssociation.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdChangeAssociation.Location = new System.Drawing.Point(555, 29);
            this.cmdChangeAssociation.Name = "cmdChangeAssociation";
            this.cmdChangeAssociation.Shortcut = Wisej.Web.Shortcut.F9;
            this.cmdChangeAssociation.Size = new System.Drawing.Size(208, 24);
            this.cmdChangeAssociation.TabIndex = 1;
            this.cmdChangeAssociation.Text = "Change Real Estate Association";
            this.cmdChangeAssociation.Click += new System.EventHandler(this.mnuChangeAssociation_Click);
            // 
            // cmdComment
            // 
            this.cmdComment.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdComment.Location = new System.Drawing.Point(471, 29);
            this.cmdComment.Name = "cmdComment";
            this.cmdComment.Shortcut = Wisej.Web.Shortcut.F5;
            this.cmdComment.Size = new System.Drawing.Size(78, 24);
            this.cmdComment.TabIndex = 1;
            this.cmdComment.Text = "Comment";
            this.cmdComment.Click += new System.EventHandler(this.mnuComment_Click);
            // 
            // fcToolStripMenuItem1
            // 
            this.fcToolStripMenuItem1.Index = -1;
            this.fcToolStripMenuItem1.Name = "fcToolStripMenuItem1";
            this.fcToolStripMenuItem1.Text = "fcToolStripMenuItem1";
            // 
            // fcToolStripMenuItem2
            // 
            this.fcToolStripMenuItem2.Index = -1;
            this.fcToolStripMenuItem2.Name = "fcToolStripMenuItem2";
            this.fcToolStripMenuItem2.Text = "fcToolStripMenuItem2";
            // 
            // fcToolStripMenuItem3
            // 
            this.fcToolStripMenuItem3.Index = -1;
            this.fcToolStripMenuItem3.Name = "fcToolStripMenuItem3";
            this.fcToolStripMenuItem3.Text = "fcToolStripMenuItem3";
            // 
            // menuItem1
            // 
            this.menuItem1.Index = -1;
            this.menuItem1.Name = "menuItem1";
            this.menuItem1.Text = "menuItem1";
            // 
            // fcToolStripMenuItem4
            // 
            this.fcToolStripMenuItem4.Index = -1;
            this.fcToolStripMenuItem4.Name = "fcToolStripMenuItem4";
            this.fcToolStripMenuItem4.Text = "fcToolStripMenuItem4";
            // 
            // frmPPMasterShort
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1001, 666);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmPPMasterShort";
            this.ShowInTaskbar = false;
            this.Text = "Short Maintenance";
            this.ToolTip1.SetToolTip(this, null);
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmPPMasterShort_Load);
            this.Activated += new System.EventHandler(this.frmPPMasterShort_Activated);
            this.Resize += new System.EventHandler(this.frmPPMasterShort_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPPMasterShort_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPPMasterShort_KeyPress);
            this.KeyUp += new Wisej.Web.KeyEventHandler(this.frmPPMasterShort_KeyUp);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRemoveOwner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEditOwner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearchOwner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraRate)).EndInit();
            this.fraRate.ResumeLayout(false);
            this.fraRate.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTranCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGroupInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdChangeAssociation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdComment)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		private FCButton cmdPrintLabel;
		private FCButton cmdGroupInfo;
		private FCButton cmdChangeAssociation;
		private FCButton cmdComment;
		private FCToolStripMenuItem fcToolStripMenuItem1;
		private FCToolStripMenuItem fcToolStripMenuItem2;
		private FCToolStripMenuItem fcToolStripMenuItem3;
		private MenuItem menuItem1;
		private FCToolStripMenuItem fcToolStripMenuItem4;
	}
}