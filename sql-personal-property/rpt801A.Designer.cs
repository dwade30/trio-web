﻿namespace TWPP0000
{
	/// <summary>
	/// Summary description for rpt801A.
	/// </summary>
	partial class rpt801A
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rpt801A));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtApplicant = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAssessedDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMunicipalCode = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTaxYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuniCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPage1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.labelA = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.LabelB = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.LabelC = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.LabelD = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.LabelE = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.LabelF = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBDesc = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblADesc = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCDesc = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDDesc = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblEDesc = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFDesc = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDescription = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblServiceDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOriginal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtYearsClaimed = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label40 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalOriginal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalAssessed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label65 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label66 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label67 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label68 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAsOfYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label69 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label70 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label71 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtMunicipalityName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label72 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label73 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label74 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtTaxRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessedTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtApplicant)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessedDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMunicipalCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTaxYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.labelA)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.LabelB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.LabelC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.LabelD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.LabelE)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.LabelF)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblADesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblEDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblServiceDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYearsClaimed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label40)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalOriginal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAssessed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label65)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label66)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label67)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label68)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAsOfYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label69)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label70)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label71)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMunicipalityName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label72)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label73)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label74)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblDescription,
				this.lblServiceDate,
				this.txtOriginal,
				this.txtAssessed,
				this.Line1,
				this.txtYearsClaimed
			});
			//FC:FINAL:CHN - issue #1357: Fix pages count.
			// this.Detail.Height = 0.2916667F;
			this.Detail.Height = 0.31f;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Format += new System.EventHandler(this.ReportHeader_Format);
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMuniName,
				this.Label6,
				this.Label7,
				this.txtApplicant,
				this.Label8,
				this.lblAssessedDate,
				this.Label15,
				this.Label16,
				this.txtLocation,
				this.Label17,
				this.Label3,
				this.Label1,
				this.Label2,
				this.Label4,
				this.Label5,
				this.lblMunicipalCode,
				this.lblTaxYear,
				this.txtMuniCode,
				this.lblPage1
			});
			//FC:FINAL:CHN - issue #1357: Fix pages count.
			// this.ReportHeader.Height = 3.78125F;
			this.ReportHeader.Height = 4.1F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label39,
				this.Label40,
				this.txtTotalOriginal,
				this.txtTotalAssessed,
				this.Label65,
				this.Label66,
				this.Label67,
				this.Label68,
				this.txtAsOfYear,
				this.Label69,
				this.Label70,
				this.Label71,
				this.Line2,
				this.txtMunicipalityName,
				this.Label72,
				this.Label73,
				this.Line3,
				this.Line4,
				this.txtDate,
				this.Label74,
				this.Line5,
				this.txtTaxRate,
				this.txtAssessedTax
			});
			this.ReportFooter.Height = 1.84375F;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.DataField = "grpHeader";
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.labelA,
				this.LabelB,
				this.LabelC,
				this.LabelD,
				this.LabelE,
				this.LabelF,
				this.lblBDesc,
				this.lblADesc,
				this.lblCDesc,
				this.lblDDesc,
				this.lblEDesc,
				this.lblFDesc
			});
			this.GroupHeader1.Height = 0.7604167F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1666667F;
			this.txtMuniName.Left = 1.84375F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'";
			this.txtMuniName.Text = null;
			this.txtMuniName.Top = 0.9791667F;
			this.txtMuniName.Width = 5.15625F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1666667F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0.01041667F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.Label6.Text = "1. MUNICIPALITY OF";
			this.Label6.Top = 0.9791667F;
			this.Label6.Width = 1.791667F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.21875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0.2291667F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "";
			//FC:FINAL:CHN - issue #1357: Fix report. Change incorrect symbol. 
			// this.Label7.Text = "Dear Assessor: Pursuant to 36 MRSA Ã¯Â¿Â½ 6653, you are hereby notified that:";
			this.Label7.Text = "Dear Assessor: Pursuant to 36 MRSA § 6653, you are hereby notified that:";
			this.Label7.Top = 1.3125F;
			this.Label7.Width = 5.65625F;
			// 
			// txtApplicant
			// 
			this.txtApplicant.Height = 0.1666667F;
			this.txtApplicant.Left = 0.2291667F;
			this.txtApplicant.Name = "txtApplicant";
			this.txtApplicant.Text = null;
			this.txtApplicant.Top = 1.520833F;
			this.txtApplicant.Width = 7.052083F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.3645833F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0.2291667F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "";
			this.Label8.Text = resources.GetString("Label8.Text");
			this.Label8.Top = 1.6875F;
			this.Label8.Width = 7.041667F;
			// 
			// lblAssessedDate
			// 
			this.lblAssessedDate.Height = 0.21875F;
			this.lblAssessedDate.HyperLink = null;
			this.lblAssessedDate.Left = 6.09375F;
			this.lblAssessedDate.Name = "lblAssessedDate";
			this.lblAssessedDate.Style = "";
			this.lblAssessedDate.Text = "April 1,";
			this.lblAssessedDate.Top = 1.854167F;
			this.lblAssessedDate.Width = 0.9375F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1666667F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 0.01041667F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.Label15.Text = "SECTION 2. Schedule for Business Equipment Tax Reimbursement";
			this.Label15.Top = 2.09375F;
			this.Label15.Width = 6.833333F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 0.01041667F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "";
			this.Label16.Text = "The eligible business property is situated at (street address, map & lot, account" + " #, etc.):";
			this.Label16.Top = 2.322917F;
			this.Label16.Width = 5.645833F;
			// 
			// txtLocation
			// 
			this.txtLocation.Height = 0.1666667F;
			this.txtLocation.Left = 0F;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Text = "Field1";
			this.txtLocation.Top = 2.5F;
			this.txtLocation.Width = 7.427083F;
			// 
			// Label17
			// 
			this.Label17.Height = 1.083333F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 0F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "";
			this.Label17.Text = resources.GetString("Label17.Text");
			this.Label17.Top = 2.708333F;
			this.Label17.Width = 7.239583F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1666667F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.Label3.Text = "SECTION 1. Notice of Intent";
			this.Label3.Top = 0.75F;
			this.Label3.Width = 2.114583F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 2.0625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.Label1.Text = "ASSESSOR NOTIFICATION";
			this.Label1.Top = 0.03125F;
			this.Label1.Width = 3.125F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 1.5625F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.Label2.Text = "PROPERTY CLAIMED FOR 12 OR FEWER YEARS";
			this.Label2.Top = 0.25F;
			this.Label2.Width = 4.427083F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 0.01041667F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: left";
			this.Label4.Text = "Form";
			this.Label4.Top = 0.4583333F;
			this.Label4.Width = 0.5625F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 0.6354167F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: left";
			this.Label5.Text = "801A";
			this.Label5.Top = 0.4583333F;
			this.Label5.Width = 0.4895833F;
			// 
			// lblMunicipalCode
			// 
			this.lblMunicipalCode.Height = 0.1666667F;
			this.lblMunicipalCode.HyperLink = null;
			this.lblMunicipalCode.Left = 5.6875F;
			this.lblMunicipalCode.Name = "lblMunicipalCode";
			this.lblMunicipalCode.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblMunicipalCode.Text = "Municipal code:";
			this.lblMunicipalCode.Top = 0.4895833F;
			this.lblMunicipalCode.Width = 1.135417F;
			// 
			// lblTaxYear
			// 
			this.lblTaxYear.Height = 0.3020833F;
			this.lblTaxYear.HyperLink = null;
			this.lblTaxYear.Left = 0.625F;
			this.lblTaxYear.Name = "lblTaxYear";
			this.lblTaxYear.Style = "font-family: \'Tahoma\'; font-size: 18pt; text-align: left";
			this.lblTaxYear.Text = "2015";
			this.lblTaxYear.Top = 0.05208333F;
			this.lblTaxYear.Width = 0.75F;
			// 
			// txtMuniCode
			// 
			this.txtMuniCode.Height = 0.1666667F;
			this.txtMuniCode.Left = 6.916667F;
			this.txtMuniCode.Name = "txtMuniCode";
			this.txtMuniCode.Text = null;
			this.txtMuniCode.Top = 0.4895833F;
			this.txtMuniCode.Width = 0.4895833F;
			// 
			// lblPage1
			// 
			this.lblPage1.Height = 0.1875F;
			this.lblPage1.HyperLink = null;
			this.lblPage1.Left = 1.0625F;
			this.lblPage1.Name = "lblPage1";
			this.lblPage1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left";
			this.lblPage1.Text = "(page 1)";
			this.lblPage1.Top = 0.4583333F;
			this.lblPage1.Width = 1.177083F;
			// 
			// labelA
			// 
			this.labelA.Height = 0.1666667F;
			this.labelA.HyperLink = null;
			this.labelA.Left = 0.5625F;
			this.labelA.Name = "labelA";
			this.labelA.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.labelA.Text = "A.";
			this.labelA.Top = 0.0625F;
			this.labelA.Width = 0.2916667F;
			// 
			// LabelB
			// 
			this.LabelB.Height = 0.1666667F;
			this.LabelB.HyperLink = null;
			this.LabelB.Left = 2.75F;
			this.LabelB.Name = "LabelB";
			this.LabelB.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.LabelB.Text = "B.";
			this.LabelB.Top = 0.0625F;
			this.LabelB.Width = 0.2916667F;
			// 
			// LabelC
			// 
			this.LabelC.Height = 0.1666667F;
			this.LabelC.HyperLink = null;
			this.LabelC.Left = 3.75F;
			this.LabelC.Name = "LabelC";
			this.LabelC.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.LabelC.Text = "C.*";
			this.LabelC.Top = 0.0625F;
			this.LabelC.Width = 0.2916667F;
			// 
			// LabelD
			// 
			this.LabelD.Height = 0.1666667F;
			this.LabelD.HyperLink = null;
			this.LabelD.Left = 4.6875F;
			this.LabelD.Name = "LabelD";
			this.LabelD.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.LabelD.Text = "D.";
			this.LabelD.Top = 0.0625F;
			this.LabelD.Width = 0.2916667F;
			// 
			// LabelE
			// 
			this.LabelE.Height = 0.1666667F;
			this.LabelE.HyperLink = null;
			this.LabelE.Left = 5.5625F;
			this.LabelE.Name = "LabelE";
			this.LabelE.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.LabelE.Text = "E.";
			this.LabelE.Top = 0.0625F;
			this.LabelE.Width = 0.2916667F;
			// 
			// LabelF
			// 
			this.LabelF.Height = 0.1666667F;
			this.LabelF.HyperLink = null;
			this.LabelF.Left = 6.6875F;
			this.LabelF.Name = "LabelF";
			this.LabelF.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.LabelF.Text = "F.";
			this.LabelF.Top = 0.0625F;
			this.LabelF.Width = 0.2916667F;
			// 
			// lblBDesc
			// 
			this.lblBDesc.Height = 0.4375F;
			this.lblBDesc.HyperLink = null;
			this.lblBDesc.Left = 2.541667F;
			this.lblBDesc.Name = "lblBDesc";
			this.lblBDesc.Style = "font-size: 8.5pt; text-align: center";
			this.lblBDesc.Text = "State of Origin (if acquired used)";
			this.lblBDesc.Top = 0.2916667F;
			this.lblBDesc.Width = 0.8125F;
			// 
			// lblADesc
			// 
			this.lblADesc.Height = 0.4375F;
			this.lblADesc.HyperLink = null;
			this.lblADesc.Left = 0F;
			this.lblADesc.Name = "lblADesc";
			this.lblADesc.Style = "font-size: 8.5pt; text-align: center";
			this.lblADesc.Text = "Property Description Category";
			this.lblADesc.Top = 0.2916667F;
			this.lblADesc.Width = 1.239583F;
			// 
			// lblCDesc
			// 
			this.lblCDesc.Height = 0.4375F;
			this.lblCDesc.HyperLink = null;
			this.lblCDesc.Left = 3.4375F;
			this.lblCDesc.Name = "lblCDesc";
			this.lblCDesc.Style = "font-size: 8.5pt; text-align: center";
			this.lblCDesc.Text = "Number of Years Claimed";
			this.lblCDesc.Top = 0.2916667F;
			this.lblCDesc.Width = 0.8125F;
			// 
			// lblDDesc
			// 
			this.lblDDesc.Height = 0.4375F;
			this.lblDDesc.HyperLink = null;
			this.lblDDesc.Left = 4.375F;
			this.lblDDesc.Name = "lblDDesc";
			this.lblDDesc.Style = "font-size: 8.5pt; text-align: center";
			this.lblDDesc.Text = "Date Placed in Service (month/year)";
			this.lblDDesc.Top = 0.2916667F;
			this.lblDDesc.Width = 0.8125F;
			// 
			// lblEDesc
			// 
			this.lblEDesc.Height = 0.4375F;
			this.lblEDesc.HyperLink = null;
			this.lblEDesc.Left = 5.25F;
			this.lblEDesc.Name = "lblEDesc";
			this.lblEDesc.Style = "font-size: 8.5pt; text-align: center";
			this.lblEDesc.Text = "Original Cost";
			this.lblEDesc.Top = 0.2916667F;
			this.lblEDesc.Width = 0.8125F;
			// 
			// lblFDesc
			// 
			this.lblFDesc.Height = 0.5104167F;
			this.lblFDesc.HyperLink = null;
			this.lblFDesc.Left = 6.25F;
			this.lblFDesc.Name = "lblFDesc";
			this.lblFDesc.Style = "font-size: 8.5pt; text-align: center";
			this.lblFDesc.Text = "Assessed Value   (To be completed by local tax assessor)";
			this.lblFDesc.Top = 0.2916667F;
			//FC:FINAL:CHN - issue #1357: Fix report. Fix incorrect wordwrap. 
			// this.lblFDesc.Width = 1.083333F;
			this.lblFDesc.Width = 1.108f;
			// 
			// lblDescription
			// 
			this.lblDescription.Height = 0.1875F;
			this.lblDescription.HyperLink = null;
			this.lblDescription.Left = 0F;
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Style = "";
			this.lblDescription.Text = null;
			this.lblDescription.Top = 0.04166667F;
			this.lblDescription.Width = 2.4375F;
			// 
			// lblServiceDate
			// 
			this.lblServiceDate.Height = 0.1875F;
			this.lblServiceDate.HyperLink = null;
			this.lblServiceDate.Left = 4.395833F;
			this.lblServiceDate.Name = "lblServiceDate";
			this.lblServiceDate.Style = "text-align: center";
			this.lblServiceDate.Text = null;
			this.lblServiceDate.Top = 0.04166667F;
			this.lblServiceDate.Width = 0.7083333F;
			// 
			// txtOriginal
			// 
			this.txtOriginal.Height = 0.1875F;
			this.txtOriginal.Left = 5.25F;
			this.txtOriginal.Name = "txtOriginal";
			this.txtOriginal.Style = "text-align: right";
			this.txtOriginal.Text = null;
			this.txtOriginal.Top = 0.04166667F;
			this.txtOriginal.Width = 0.7916667F;
			// 
			// txtAssessed
			// 
			this.txtAssessed.Height = 0.1875F;
			this.txtAssessed.Left = 6.385417F;
			this.txtAssessed.Name = "txtAssessed";
			this.txtAssessed.Style = "text-align: right";
			this.txtAssessed.Text = null;
			this.txtAssessed.Top = 0.04166667F;
			this.txtAssessed.Width = 0.90625F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 2.614583F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.1944444F;
			this.Line1.Width = 0.6770835F;
			this.Line1.X1 = 2.614583F;
			this.Line1.X2 = 3.291667F;
			this.Line1.Y1 = 0.1944444F;
			this.Line1.Y2 = 0.1944444F;
			// 
			// txtYearsClaimed
			// 
			this.txtYearsClaimed.Height = 0.1875F;
			this.txtYearsClaimed.HyperLink = null;
			this.txtYearsClaimed.Left = 3.6875F;
			this.txtYearsClaimed.Name = "txtYearsClaimed";
			this.txtYearsClaimed.Style = "";
			this.txtYearsClaimed.Text = null;
			this.txtYearsClaimed.Top = 0.04166667F;
			this.txtYearsClaimed.Width = 0.3958333F;
			// 
			// Label39
			// 
			this.Label39.Height = 0.21875F;
			this.Label39.HyperLink = null;
			this.Label39.Left = 0.2291667F;
			this.Label39.Name = "Label39";
			this.Label39.Style = "";
			this.Label39.Text = "Property Tax Rate";
			this.Label39.Top = 0.71875F;
			this.Label39.Width = 1.3125F;
			// 
			// Label40
			// 
			this.Label40.Height = 0.1875F;
			this.Label40.HyperLink = null;
			this.Label40.Left = 4.489583F;
			this.Label40.Name = "Label40";
			this.Label40.Style = "";
			this.Label40.Text = "Totals";
			this.Label40.Top = 0.04166667F;
			this.Label40.Width = 0.46875F;
			// 
			// txtTotalOriginal
			// 
			this.txtTotalOriginal.Height = 0.1875F;
			this.txtTotalOriginal.Left = 5.125F;
			this.txtTotalOriginal.Name = "txtTotalOriginal";
			this.txtTotalOriginal.Style = "text-align: right";
			this.txtTotalOriginal.Text = null;
			this.txtTotalOriginal.Top = 0.04166667F;
			this.txtTotalOriginal.Width = 0.9166667F;
			// 
			// txtTotalAssessed
			// 
			this.txtTotalAssessed.Height = 0.1875F;
			this.txtTotalAssessed.Left = 6.260417F;
			this.txtTotalAssessed.Name = "txtTotalAssessed";
			this.txtTotalAssessed.Style = "text-align: right";
			this.txtTotalAssessed.Text = null;
			this.txtTotalAssessed.Top = 0.04166667F;
			this.txtTotalAssessed.Width = 1.03125F;
			// 
			// Label65
			// 
			this.Label65.Height = 0.1666667F;
			this.Label65.HyperLink = null;
			this.Label65.Left = 0F;
			this.Label65.Name = "Label65";
			this.Label65.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.Label65.Text = "* Form 801A is for property claimed 12 or fewer years. See instructions.";
			this.Label65.Top = 0.25F;
			this.Label65.Width = 6.833333F;
			// 
			// Label66
			// 
			this.Label66.Height = 0.1666667F;
			this.Label66.HyperLink = null;
			this.Label66.Left = 0.125F;
			this.Label66.Name = "Label66";
			this.Label66.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.Label66.Text = "Section 3. Property Tax Information (To be completed by local tax assessor.)";
			this.Label66.Top = 0.5F;
			this.Label66.Width = 6.833333F;
			// 
			// Label67
			// 
			this.Label67.Height = 0.21875F;
			this.Label67.HyperLink = null;
			this.Label67.Left = 2.25F;
			this.Label67.Name = "Label67";
			this.Label67.Style = "";
			this.Label67.Text = "Assessed Tax";
			this.Label67.Top = 0.71875F;
			this.Label67.Width = 1.3125F;
			// 
			// Label68
			// 
			this.Label68.Height = 0.21875F;
			this.Label68.HyperLink = null;
			this.Label68.Left = 4.875F;
			this.Label68.Name = "Label68";
			this.Label68.Style = "";
			this.Label68.Text = "For Taxes Assessed April 1,";
			this.Label68.Top = 0.75F;
			this.Label68.Width = 1.8125F;
			// 
			// txtAsOfYear
			// 
			this.txtAsOfYear.Height = 0.21875F;
			this.txtAsOfYear.Left = 6.625F;
			this.txtAsOfYear.Name = "txtAsOfYear";
			this.txtAsOfYear.Style = "text-align: left";
			this.txtAsOfYear.Text = null;
			this.txtAsOfYear.Top = 0.75F;
			this.txtAsOfYear.Width = 0.5416667F;
			// 
			// Label69
			// 
			this.Label69.Height = 0.21875F;
			this.Label69.HyperLink = null;
			this.Label69.Left = 0.2291667F;
			this.Label69.Name = "Label69";
			this.Label69.Style = "";
			this.Label69.Text = "Assessor Signature";
			this.Label69.Top = 0.96875F;
			this.Label69.Width = 1.3125F;
			// 
			// Label70
			// 
			this.Label70.Height = 0.21875F;
			this.Label70.HyperLink = null;
			this.Label70.Left = 3.3125F;
			this.Label70.Name = "Label70";
			this.Label70.Style = "";
			this.Label70.Text = "Municipality Name";
			this.Label70.Top = 0.96875F;
			this.Label70.Width = 1.3125F;
			// 
			// Label71
			// 
			this.Label71.Height = 0.21875F;
			this.Label71.HyperLink = null;
			this.Label71.Left = 6.1875F;
			this.Label71.Name = "Label71";
			this.Label71.Style = "";
			this.Label71.Text = "Date";
			this.Label71.Top = 0.96875F;
			this.Label71.Width = 0.4375F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 1.447917F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 1.21875F;
			this.Line2.Width = 1.833333F;
			this.Line2.X1 = 1.447917F;
			this.Line2.X2 = 3.28125F;
			this.Line2.Y1 = 1.21875F;
			this.Line2.Y2 = 1.21875F;
			// 
			// txtMunicipalityName
			// 
			this.txtMunicipalityName.Height = 0.21875F;
			this.txtMunicipalityName.Left = 4.5F;
			this.txtMunicipalityName.Name = "txtMunicipalityName";
			this.txtMunicipalityName.Style = "text-align: left";
			this.txtMunicipalityName.Text = null;
			this.txtMunicipalityName.Top = 0.96875F;
			this.txtMunicipalityName.Width = 1.666667F;
			// 
			// Label72
			// 
			this.Label72.Height = 0.21875F;
			this.Label72.HyperLink = null;
			this.Label72.Left = 0.25F;
			this.Label72.Name = "Label72";
			this.Label72.Style = "font-size: 8.5pt";
			this.Label72.Text = "Applicant (or agent) Signature";
			this.Label72.Top = 1.625F;
			this.Label72.Width = 1.9375F;
			// 
			// Label73
			// 
			this.Label73.Height = 0.21875F;
			this.Label73.HyperLink = null;
			this.Label73.Left = 3.125F;
			this.Label73.Name = "Label73";
			this.Label73.Style = "font-size: 8.5pt";
			this.Label73.Text = "Social Security Number or Federal EIN (see note, page 5)";
			this.Label73.Top = 1.625F;
			this.Label73.Width = 3.125F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 0.25F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 1.5625F;
			this.Line3.Width = 2.708333F;
			this.Line3.X1 = 0.25F;
			this.Line3.X2 = 2.958333F;
			this.Line3.Y1 = 1.5625F;
			this.Line3.Y2 = 1.5625F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 3.125F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 1.5625F;
			this.Line4.Width = 2.583333F;
			this.Line4.X1 = 3.125F;
			this.Line4.X2 = 5.708333F;
			this.Line4.Y1 = 1.5625F;
			this.Line4.Y2 = 1.5625F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.21875F;
			this.txtDate.Left = 6.5625F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "text-align: left";
			this.txtDate.Text = null;
			this.txtDate.Top = 0.96875F;
			this.txtDate.Width = 0.8541667F;
			// 
			// Label74
			// 
			this.Label74.Height = 0.21875F;
			this.Label74.HyperLink = null;
			this.Label74.Left = 6.3125F;
			this.Label74.Name = "Label74";
			this.Label74.Style = "font-size: 8.5pt";
			this.Label74.Text = "Date";
			this.Label74.Top = 1.625F;
			this.Label74.Width = 0.625F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 6.3125F;
			this.Line5.LineWeight = 1F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 1.5625F;
			this.Line5.Width = 1.020833F;
			this.Line5.X1 = 6.3125F;
			this.Line5.X2 = 7.333333F;
			this.Line5.Y1 = 1.5625F;
			this.Line5.Y2 = 1.5625F;
			// 
			// txtTaxRate
			// 
			this.txtTaxRate.Height = 0.1875F;
			this.txtTaxRate.Left = 1.5F;
			this.txtTaxRate.Name = "txtTaxRate";
			this.txtTaxRate.Style = "text-align: left";
			this.txtTaxRate.Text = null;
			this.txtTaxRate.Top = 0.71875F;
			this.txtTaxRate.Width = 0.6666667F;
			// 
			// txtAssessedTax
			// 
			this.txtAssessedTax.Height = 0.21875F;
			this.txtAssessedTax.Left = 3.1875F;
			this.txtAssessedTax.Name = "txtAssessedTax";
			this.txtAssessedTax.Style = "text-align: left";
			this.txtAssessedTax.Text = null;
			this.txtAssessedTax.Top = 0.71875F;
			this.txtAssessedTax.Width = 1.166667F;
			// 
			// rpt801A
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.4375F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtApplicant)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessedDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMunicipalCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTaxYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.labelA)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.LabelB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.LabelC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.LabelD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.LabelE)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.LabelF)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblADesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblEDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblServiceDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYearsClaimed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label40)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalOriginal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAssessed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label65)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label66)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label67)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label68)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAsOfYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label69)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label70)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label71)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMunicipalityName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label72)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label73)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label74)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessedTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDescription;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblServiceDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOriginal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessed;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtYearsClaimed;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtApplicant;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAssessedDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMunicipalCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTaxYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage1;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label39;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label40;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalOriginal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalAssessed;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label65;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label66;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label67;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label68;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAsOfYear;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label69;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label70;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label71;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMunicipalityName;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label72;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label73;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label74;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxRate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessedTax;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label labelA;
		private GrapeCity.ActiveReports.SectionReportModel.Label LabelB;
		private GrapeCity.ActiveReports.SectionReportModel.Label LabelC;
		private GrapeCity.ActiveReports.SectionReportModel.Label LabelD;
		private GrapeCity.ActiveReports.SectionReportModel.Label LabelE;
		private GrapeCity.ActiveReports.SectionReportModel.Label LabelF;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBDesc;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblADesc;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCDesc;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDDesc;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblEDesc;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFDesc;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
