﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Core;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for srptLeasedInput.
	/// </summary>
	public partial class srptLeasedInput : FCSectionReport
	{
		public srptLeasedInput()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ActiveReport1";
		}

		public static srptLeasedInput InstancePtr
		{
			get
			{
				return (srptLeasedInput)Sys.GetInstance(typeof(srptLeasedInput));
			}
		}

		protected srptLeasedInput _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsTemp?.Dispose();
				clsReports?.Dispose();
                clsTemp = null;
                clsReports = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptLeasedInput	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool boolPrint;
		int lngAcct;
		clsDRWrapper clsReports = new clsDRWrapper();
		clsDRWrapper clsTemp = new clsDRWrapper();

		public void PrintLeased()
		{
			if (FCConvert.ToString(clsReports.Get_Fields_String("CD")) == "-" || FCConvert.CBool(Strings.Trim(FCConvert.ToString(FCConvert.ToString(clsReports.Get_Fields_String("description")) == string.Empty))))
			{
				// TODO Get_Fields: Check the table for the column [Line] and replace with corresponding Get_Field method
				txtLine.Text = Strings.Format(clsReports.Get_Fields("Line"), "0000");
				txtCD.Text = clsReports.Get_Fields_String("CD");
				txtQTY.Text = string.Empty;
				txtDescription.Text = Strings.Trim(FCConvert.ToString(clsReports.Get_Fields_String("Description")));
				txtCost.Text = string.Empty;
				txtGD.Text = string.Empty;
				txtFCTR.Text = string.Empty;
				txtMOs.Text = string.Empty;
				txtRB.Text = string.Empty;
				txtRent.Text = string.Empty;
				txtLDate.Text = string.Empty;
				txtLorP.Text = string.Empty;
				txtLeasedFrom.Text = string.Empty;
				txtExemptYear.Text = "";
				boolPrint = true;
			}
			// TODO Get_Fields: Check the table for the column [Cost] and replace with corresponding Get_Field method
			else if (Conversion.Val(clsReports.Get_Fields("Cost")) != 0)
			{
				if (Strings.Trim(FCConvert.ToString(clsReports.Get_Fields_String("Description"))) != string.Empty)
				{
					// TODO Get_Fields: Check the table for the column [Line] and replace with corresponding Get_Field method
					txtLine.Text = Strings.Format(clsReports.Get_Fields("Line"), "0000");
					txtCD.Text = clsReports.Get_Fields_String("CD");
					if (rptLeasedandItemizedInput.InstancePtr.boolPQTY)
					{
						// TODO Get_Fields: Check the table for the column [Quantity] and replace with corresponding Get_Field method
						txtQTY.Text = clsReports.Get_Fields_String("Quantity");
					}
					else
					{
						txtQTY.Text = "__";
					}
					if (clsReports.Get_Fields_Int32("leasedfrom") > 0)
					{
						txtLeasedFrom.Text = FCConvert.ToString(clsReports.Get_Fields_Int32("leasedfrom"));
					}
					else
					{
						txtLeasedFrom.Text = string.Empty;
					}
					txtDescription.Text = Strings.Trim(FCConvert.ToString(clsReports.Get_Fields_String("Description")));
					// TODO Get_Fields: Check the table for the column [Cost] and replace with corresponding Get_Field method
					txtCost.Text = Strings.Format(clsReports.Get_Fields("Cost"), "###,###,###");
					txtGD.Text = FCConvert.ToString(clsReports.Get_Fields_Int16("depyrs"));
					txtFCTR.Text = FCConvert.ToString(clsReports.Get_Fields_Int16("FCTR"));
					txtLDate.Text = clsReports.Get_Fields_String("leasedate");
					txtRent.Text = FCConvert.ToString(clsReports.Get_Fields_Decimal("monthlyrent"));
					if (clsReports.Get_Fields_Int32("exemptyear") > 0)
					{
						txtExemptYear.Text = FCConvert.ToString(clsReports.Get_Fields_Int32("exemptyear"));
					}
					else
					{
						txtExemptYear.Text = "";
					}
					txtLorP.Text = FCConvert.ToString(clsReports.Get_Fields_Int16("l1p2"));
					txtMOs.Text = FCConvert.ToString(clsReports.Get_Fields_Int16("mos"));
					// TODO Get_Fields: Check the table for the column [rb] and replace with corresponding Get_Field method
					txtRB.Text = clsReports.Get_Fields("rb");
					boolPrint = true;
				}
			}
				// TODO Get_Fields: Check the table for the column [Cost] and replace with corresponding Get_Field method
				else if (Conversion.Val(clsReports.Get_Fields("Cost")) == 0)
			{
				if (Strings.Trim(FCConvert.ToString(clsReports.Get_Fields_String("Description"))) != string.Empty)
				{
					// TODO Get_Fields: Check the table for the column [Line] and replace with corresponding Get_Field method
					txtLine.Text = Strings.Format(clsReports.Get_Fields("Line"), "0000");
					txtCD.Text = clsReports.Get_Fields_String("CD");
					if (rptLeasedandItemizedInput.InstancePtr.boolPQTY)
					{
						// TODO Get_Fields: Check the table for the column [Quantity] and replace with corresponding Get_Field method
						txtQTY.Text = clsReports.Get_Fields_String("Quantity");
					}
					else
					{
						txtQTY.Text = "__";
					}
					if (clsReports.Get_Fields_Int32("leasedfrom") > 0)
					{
						txtLeasedFrom.Text = FCConvert.ToString(clsReports.Get_Fields_Int32("leasedfrom"));
					}
					else
					{
						txtLeasedFrom.Text = string.Empty;
					}
					txtDescription.Text = Strings.Trim(FCConvert.ToString(clsReports.Get_Fields_String("Description")));
					txtCost.Text = string.Empty;
					txtGD.Text = FCConvert.ToString(clsReports.Get_Fields_Int16("depyrs"));
					txtFCTR.Text = FCConvert.ToString(clsReports.Get_Fields_Int16("FCTR"));
					// TODO Get_Fields: Check the table for the column [rb] and replace with corresponding Get_Field method
					txtRB.Text = clsReports.Get_Fields("rb");
					txtMOs.Text = FCConvert.ToString(clsReports.Get_Fields_Int16("mos"));
					txtLorP.Text = FCConvert.ToString(clsReports.Get_Fields_Int16("l1p2"));
					txtLDate.Text = clsReports.Get_Fields_String("leasedate");
					txtRent.Text = FCConvert.ToString(clsReports.Get_Fields_Decimal("monthlyrent"));
					if (clsReports.Get_Fields_Int32("exemptyear") > 0)
					{
						txtExemptYear.Text = FCConvert.ToString(clsReports.Get_Fields_Int32("exemptyear"));
					}
					else
					{
						txtExemptYear.Text = "";
					}
					boolPrint = true;
				}
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			ShowRecord:
			;
			eArgs.EOF = clsReports.EndOfFile();
			if (eArgs.EOF)
				return;
			boolPrint = false;
			PrintLeased();
			if (!boolPrint)
			{
				clsReports.MoveNext();
				goto ShowRecord;
			}
			else
			{
				clsReports.MoveNext();
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			//SubReport1.Report = null;
			//SubReport1 = null;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData)));
			string strMasterJoinJoin;
			strMasterJoinJoin = modPPGN.GetMasterJoinForJoin(false);
			clsReports.OpenRecordset("select * from ppleased where account = " + FCConvert.ToString(lngAcct) + " order by line", "twpp0000.vb1");
			// Unload srptLeasedFromLeased
			SubReport1.Report = srptLeasedFromLeased.InstancePtr;
			SubReport1.Report.UserData = this.UserData;
			// Call clsTemp.OpenRecordset("select name,street,streetnumber from ppmaster where account = " & lngAcct, "twpp0000.vb1")
			clsTemp.OpenRecordset("select name,street,streetnumber from " + strMasterJoinJoin + " where account = " + FCConvert.ToString(lngAcct), "twpp0000.vb1");
			if (clsReports.EndOfFile())
			{
				Detail.Visible = false;
				GroupHeader1.Visible = false;
			}
			else
			{
				Detail.Visible = true;
				GroupHeader1.Visible = true;
			}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			rptLeasedandItemizedInput.InstancePtr.txtAcct.Text = FCConvert.ToString(this.UserData);
			rptLeasedandItemizedInput.InstancePtr.txtName.Text = clsTemp.Get_Fields_String("name");
			// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
			rptLeasedandItemizedInput.InstancePtr.txtLocation.Text = clsTemp.Get_Fields("streetnumber") + " " + clsTemp.Get_Fields_String("street");
		}

	
	}
}
