﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWPP0000
{
	public class modSecurity
	{
		//=========================================================
		public const int ACCOUNTMAINTENANCE = 2;
		public const int CALCULATEMENUSECURITY = 27;
		public const int EDITITEMIZEDSCREENMENU = 28;
		public const int EDITLEASEDSCREENMENU = 29;
		public const int EDITMASTERVALUESMENU = 31;
		public const int ITEMIZEDSCREENMENU = 26;
		public const int LEASEDSCREENMENU = 25;
		public const int ADDNEWACCOUNTMENU = 33;
		public const int COMPUTEMENU = 6;
		public const int AUDITSUMMARYMENU = 10;
		public const int DISPLAYCALCMENU = 11;
		public const int PRINTCALCMENU = 12;
		public const int PRINTSUMMARYMENU = 8;
		public const int TRANSFERTOBILLINGMENU = 9;
		public const int COSTFILEMENU = 4;
		public const int EDITBUSINESSCODESMENU = 23;
		public const int EDITEXEMPTIONSTRENDINGMENU = 22;
		public const int EDITRATIOOPENSMENU = 21;
		public const int EDITSTREETTRANCODESMENU = 24;
		public const int DELETEACCOUNTSMENU = 34;
		public const int EDITADDRESSESMENU = 30;
		public const int FILEMAINTENANCEMENU = 7;
		public const int USEREXTRACTMENU = 20;
		public const int SETTINGSMENU = 19;
		public const int PRINTMENUSECURITY = 5;
		public const int DELETEDACCOUNTPRINTMENU = 17;
		public const int HIGHESTASSESSMENTMENU = 16;
		public const int LABELSPRINTMENU = 18;
		public const int ACCOUNTLISTPRINTMENU = 13;
		public const int REIMBURSEMENTNOTICESMENU = 15;
		public const int TAXPAYERNOTICESMENU = 14;
		public const int SHORTMAINTENANCE = 3;
		public const int EDITSHORTVALUES = 32;
		public const int TRANSFERMENU = 35;
		public const int UNDELETEACCOUNTSMENU = 36;
		public const int CNSTEDITGROUPS = 39;
		public const int CNSTGROUPMAINT = 37;
		public const int CNSTCREATEARCHIVES = 41;
		public const int CNSTRUNARCHIVES = 40;
		public const int CALCULATEEXEMPTIONS = 43;
		public const int IMPORTEXPORTMENU = 44;
		public const int CNSTCREATEEXTRACT = 45;
		public const int CNSTITEMIZEDFROMFILE = 47;
		// vbPorter upgrade warning: obForm As object	OnWrite(frmPPItemized, frmPPMaster, frmPPLeased, frmPPGetAccount, frmPPMasterShort, MDIParent, frmGroup, frmGetGroup)
		public static bool ValidPermissions_6(object obForm, int lngFuncID, bool boolHandlePartials = true)
		{
			return ValidPermissions(obForm, lngFuncID, boolHandlePartials);
		}

		public static bool ValidPermissions(object obForm, int lngFuncID, bool boolHandlePartials = true)
		{
			bool ValidPermissions = false;
			try
			{
				// On Error GoTo ErrorHandler
				string strPerm;
				// checks the permissions and returns false if there are no permissions
				// if there are partial permissions then it calls the function for the form
				// if false is passed in then partial permissions will not be handled by the function
				// This function is for MDI menu options.  At the time this is called the child
				// menu options wont be present, so you cant disable them yet
				strPerm = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(lngFuncID));
				if (strPerm == "F")
				{
					ValidPermissions = true;
				}
				else if (strPerm == "N")
				{
					ValidPermissions = false;
				}
				else if (strPerm == "P")
				{
					ValidPermissions = true;
					if (boolHandlePartials)
					{
						FCUtils.CallByName(obForm, "HandlePartialPermission", CallType.Method, lngFuncID);
					}
				}
				// ValidPermissions = True
				return ValidPermissions;
				// 
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description, null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ValidPermissions;
		}
	}
}
