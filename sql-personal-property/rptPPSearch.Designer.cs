﻿namespace TWPP0000
{
    /// <summary>
    /// Summary description for rptPPSearch.
    /// </summary>
    partial class rptPPSearch
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptPPSearch));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txttime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtStNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtAccount,
            this.txtName,
            this.txtAddress,
            this.txtStNo});
            this.Detail.Height = 0.2916667F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            // 
            // txtAccount
            // 
            this.txtAccount.Height = 0.1875F;
            this.txtAccount.Left = 0.125F;
            this.txtAccount.Name = "txtAccount";
            this.txtAccount.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
            this.txtAccount.Text = null;
            this.txtAccount.Top = 0.0625F;
            this.txtAccount.Width = 0.7500001F;
            // 
            // txtName
            // 
            this.txtName.Height = 0.1875F;
            this.txtName.Left = 4.313F;
            this.txtName.Name = "txtName";
            this.txtName.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
            this.txtName.Text = null;
            this.txtName.Top = 0.063F;
            this.txtName.Width = 2.8745F;
            // 
            // txtAddress
            // 
            this.txtAddress.Height = 0.1875F;
            this.txtAddress.Left = 1.875F;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
            this.txtAddress.Text = null;
            this.txtAddress.Top = 0.06275004F;
            this.txtAddress.Width = 2.354F;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label1,
            this.Label4,
            this.txtTitle,
            this.txtMuniName,
            this.txtDate,
            this.txtPage,
            this.Line1,
            this.Label6,
            this.txttime,
            this.label2});
            this.PageHeader.Height = 0.8125F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            // 
            // Label1
            // 
            this.Label1.Height = 0.1875F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 0.125F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
            this.Label1.Text = "Account";
            this.Label1.Top = 0.59375F;
            this.Label1.Width = 0.75F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.1875F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 4.313F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
            this.Label4.Text = "Name";
            this.Label4.Top = 0.594F;
            this.Label4.Width = 0.8125F;
            // 
            // txtTitle
            // 
            this.txtTitle.CanGrow = false;
            this.txtTitle.Height = 0.25F;
            this.txtTitle.Left = 1.875F;
            this.txtTitle.MultiLine = false;
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
            this.txtTitle.Text = "Search Results";
            this.txtTitle.Top = 0.0625F;
            this.txtTitle.Width = 4.0625F;
            // 
            // txtMuniName
            // 
            this.txtMuniName.Height = 0.1875F;
            this.txtMuniName.Left = 0.0625F;
            this.txtMuniName.Name = "txtMuniName";
            this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
            this.txtMuniName.Text = "Field1";
            this.txtMuniName.Top = 0.0625F;
            this.txtMuniName.Width = 2.5F;
            // 
            // txtDate
            // 
            this.txtDate.Height = 0.1875F;
            this.txtDate.Left = 5.9375F;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
            this.txtDate.Text = "Field1";
            this.txtDate.Top = 0.0625F;
            this.txtDate.Width = 1.3125F;
            // 
            // txtPage
            // 
            this.txtPage.Height = 0.1875F;
            this.txtPage.Left = 5.9375F;
            this.txtPage.Name = "txtPage";
            this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
            this.txtPage.Text = "Field1";
            this.txtPage.Top = 0.25F;
            this.txtPage.Width = 1.3125F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0.0625F;
            this.Line1.LineWeight = 3F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.78125F;
            this.Line1.Width = 7.25F;
            this.Line1.X1 = 0.0625F;
            this.Line1.X2 = 7.3125F;
            this.Line1.Y1 = 0.78125F;
            this.Line1.Y2 = 0.78125F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.1875F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 1.875F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
            this.Label6.Text = "Street";
            this.Label6.Top = 0.594F;
            this.Label6.Width = 0.8125F;
            // 
            // txttime
            // 
            this.txttime.Height = 0.1875F;
            this.txttime.Left = 0.0625F;
            this.txttime.Name = "txttime";
            this.txttime.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
            this.txttime.Text = "Field1";
            this.txttime.Top = 0.25F;
            this.txttime.Width = 1.8125F;
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Visible = false;
            // 
            // label2
            // 
            this.label2.Height = 0.1875F;
            this.label2.HyperLink = null;
            this.label2.Left = 0.99F;
            this.label2.Name = "label2";
            this.label2.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
            this.label2.Text = "St. No.";
            this.label2.Top = 0.59375F;
            this.label2.Width = 0.8125F;
            // 
            // txtStNo
            // 
            this.txtStNo.Height = 0.1875F;
            this.txtStNo.Left = 0.99F;
            this.txtStNo.Name = "txtStNo";
            this.txtStNo.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
            this.txtStNo.Top = 0.0625F;
            this.txtStNo.Width = 0.812F;
            // 
            // rptPPSearch
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.375F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
            ((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txttime;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStNo;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
    }
}
