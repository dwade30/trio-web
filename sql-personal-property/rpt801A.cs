﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rpt801A.
	/// </summary>
	public partial class rpt801A : BaseSectionReport
	{
		public rpt801A()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "801 A";
		}

		public static rpt801A InstancePtr
		{
			get
			{
				return (rpt801A)Sys.GetInstance(typeof(rpt801A));
			}
		}

		protected rpt801A _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rpt801A	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private int intCurrentLine;
		private c801Master theMaster;
		private double dblTotalOriginal;
		private double dblTotalAssessed;
		int intPage;

		public object SetReportObject(c801Master rep801)
		{
			object SetReportObject = null;
			theMaster = rep801;
			return SetReportObject;
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("grpHeader");
			this.Fields["grpHeader"].Value = "1";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!(theMaster == null))
			{
				eArgs.EOF = !theMaster.DetailsA.IsCurrent();
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			intPage += 1;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			intCurrentLine = 0;
			dblTotalOriginal = 0;
			dblTotalAssessed = 0;
			theMaster.DetailsA.MoveFirst();
			intPage = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!(theMaster == null))
			{
				if (theMaster.DetailsA.IsCurrent())
				{
					intCurrentLine += 1;
					c801ADetail detA;
					detA = (c801ADetail)theMaster.DetailsA.GetCurrentItem();
					lblDescription.Text = FCConvert.ToString(intCurrentLine) + ".  " + detA.Description;
					if (detA.NumberOfYearsClaimed > 0)
					{
						txtYearsClaimed.Text = FCConvert.ToString(detA.NumberOfYearsClaimed);
					}
					else
					{
						txtYearsClaimed.Text = "";
					}
					txtOriginal.Text = Strings.Format(detA.OriginalCost, "#,###,###,##0");
					txtAssessed.Text = Strings.Format(detA.AssessedValue, "#,###,###,##0");
					dblTotalOriginal += detA.OriginalCost;
					dblTotalAssessed += detA.AssessedValue;
					if (detA.MonthPlacedInService > 0 && detA.YearPlacedInService > 0)
					{
						lblServiceDate.Text = Strings.Format(detA.MonthPlacedInService, "00") + "/" + Strings.Format(detA.YearPlacedInService, "00");
					}
					else
					{
						lblServiceDate.Text = "/";
					}
					theMaster.DetailsA.MoveNext();
				}
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: dblAssessedTax As double	OnWrite(string)
			double dblAssessedTax = 0;
			txtTotalAssessed.Text = Strings.Format(dblTotalAssessed, "#,###,###,###,##0");
			txtTotalOriginal.Text = Strings.Format(dblTotalOriginal, "#,###,###,###,##0");
			if (theMaster.TaxRate > 0)
			{
				dblAssessedTax = FCConvert.ToDouble(Strings.Format(dblTotalAssessed * theMaster.TaxRate, "0.00"));
				txtAssessedTax.Text = Strings.Format(dblAssessedTax, "#,###,###,##0.00");
			}
			else
			{
				txtAssessedTax.Text = "";
			}
		}

		private void ReportHeader_Format(object sender, EventArgs e)
		{
			lblTaxYear.Text = FCConvert.ToString(theMaster.TaxYear);
			txtMuniName.Text = theMaster.Municipality;
			txtMunicipalityName.Text = theMaster.Municipality;
			txtLocation.Text = theMaster.LocatedAt;
			txtApplicant.Text = theMaster.ApplicantName;
			txtMuniCode.Text = theMaster.MunicipalCode;
			txtAsOfYear.Text = FCConvert.ToString(theMaster.TaxYear);
			txtDate.Text = theMaster.ReportDate;
			txtTaxRate.Text = theMaster.DisplayedRate;
			lblAssessedDate.Text = "April 1, " + FCConvert.ToString(theMaster.TaxYear);
		}

		
	}
}
