﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;
using System.IO;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmTrendExempt.
	/// </summary>
	partial class frmTrendExempt : BaseForm
	{
		public fecherFoundation.FCGrid GridExempt;
		public fecherFoundation.FCGrid gridTrend;
		public fecherFoundation.FCFrame fraWait;
		public fecherFoundation.FCProgressBar ProgressBar1;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuPrintExemptions;
		public fecherFoundation.FCToolStripMenuItem mnuPrintTrending;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuImport;
		public fecherFoundation.FCToolStripMenuItem mnuExport;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		public FCButton cmdImport;
		public FCButton cmdExport;
		public FCButton cmdSave;
		public FCButton cmdPrintExemptions;
		public FCButton cmdPrintTrending;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTrendExempt));
			this.GridExempt = new fecherFoundation.FCGrid();
			this.gridTrend = new fecherFoundation.FCGrid();
			this.fraWait = new fecherFoundation.FCFrame();
			this.Label4 = new fecherFoundation.FCLabel();
			this.ProgressBar1 = new fecherFoundation.FCProgressBar();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintExemptions = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintTrending = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuImport = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExport = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSave = new fecherFoundation.FCButton();
			this.cmdExport = new fecherFoundation.FCButton();
			this.cmdImport = new fecherFoundation.FCButton();
			this.cmdPrintExemptions = new fecherFoundation.FCButton();
			this.cmdPrintTrending = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridTrend)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraWait)).BeginInit();
			this.fraWait.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdExport)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdImport)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintExemptions)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintTrending)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 488);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.GridExempt);
			this.ClientArea.Controls.Add(this.gridTrend);
			this.ClientArea.Controls.Add(this.fraWait);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Size = new System.Drawing.Size(1014, 518);
			this.ClientArea.Controls.SetChildIndex(this.Label3, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label2, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraWait, 0);
			this.ClientArea.Controls.SetChildIndex(this.gridTrend, 0);
			this.ClientArea.Controls.SetChildIndex(this.GridExempt, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdPrintExemptions);
			this.TopPanel.Controls.Add(this.cmdPrintTrending);
			this.TopPanel.Controls.Add(this.cmdImport);
			this.TopPanel.Controls.Add(this.cmdExport);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdExport, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdImport, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrintTrending, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrintExemptions, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(243, 28);
			this.HeaderText.Text = "Exemptions / Trending";
			// 
			// GridExempt
			// 
			this.GridExempt.AllowBigSelection = false;
			this.GridExempt.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.GridExempt.Cols = 4;
			this.GridExempt.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridExempt.ExtendLastCol = true;
			this.GridExempt.HighLight = fecherFoundation.FCGrid.HighLightSettings.flexHighlightNever;
			this.GridExempt.Location = new System.Drawing.Point(468, 75);
			this.GridExempt.Name = "GridExempt";
			this.GridExempt.ReadOnly = false;
			this.GridExempt.Rows = 1;
			this.GridExempt.Size = new System.Drawing.Size(509, 413);
			this.GridExempt.StandardTab = false;
			this.GridExempt.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.GridExempt.TabIndex = 6;
			this.GridExempt.CurrentCellChanged += new System.EventHandler(this.GridExempt_EnterCell);
			// 
			// gridTrend
			// 
			this.gridTrend.AllowBigSelection = false;
			this.gridTrend.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
			this.gridTrend.Cols = 4;
			this.gridTrend.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.gridTrend.ExtendLastCol = true;
			this.gridTrend.FixedCols = 2;
			this.gridTrend.HighLight = fecherFoundation.FCGrid.HighLightSettings.flexHighlightNever;
			this.gridTrend.Location = new System.Drawing.Point(30, 75);
			this.gridTrend.Name = "gridTrend";
			this.gridTrend.ReadOnly = false;
			this.gridTrend.Rows = 1;
			this.gridTrend.Size = new System.Drawing.Size(393, 413);
			this.gridTrend.StandardTab = false;
			this.gridTrend.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.gridTrend.TabIndex = 5;
			this.gridTrend.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.gridTrend_ValidateEdit);
			// 
			// fraWait
			// 
			this.fraWait.Controls.Add(this.Label4);
			this.fraWait.Location = new System.Drawing.Point(870, 6);
			this.fraWait.Name = "fraWait";
			this.fraWait.Size = new System.Drawing.Size(186, 75);
			this.fraWait.TabIndex = 0;
			this.fraWait.Visible = false;
			this.fraWait.Controls.SetChildIndex(this.Label4, 0);
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(23, 17);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(149, 23);
			this.Label4.TabIndex = 2;
			this.Label4.Text = "PLEASE WAIT... SAVING";
			// 
			// ProgressBar1
			// 
			this.ProgressBar1.Location = new System.Drawing.Point(13, 39);
			this.ProgressBar1.Name = "ProgressBar1";
			this.ProgressBar1.Size = new System.Drawing.Size(159, 23);
			this.ProgressBar1.TabIndex = 1;
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 30);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(175, 21);
			this.Label2.TabIndex = 4;
			this.Label2.Text = "TREND CAPITALIZATION RATE";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(468, 30);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(116, 21);
			this.Label3.TabIndex = 3;
			this.Label3.Text = "EXEMPTION CODES";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPrintExemptions,
            this.mnuPrintTrending,
            this.mnuSepar3,
            this.mnuImport,
            this.mnuExport,
            this.mnuSepar1,
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSepar,
            this.mnuExit});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuPrintExemptions
			// 
			this.mnuPrintExemptions.Index = 0;
			this.mnuPrintExemptions.Name = "mnuPrintExemptions";
			this.mnuPrintExemptions.Text = "Print Exemptions";
			this.mnuPrintExemptions.Click += new System.EventHandler(this.mnuPrintExemptions_Click);
			// 
			// mnuPrintTrending
			// 
			this.mnuPrintTrending.Index = 1;
			this.mnuPrintTrending.Name = "mnuPrintTrending";
			this.mnuPrintTrending.Text = "Print Trending / Cap Rates";
			this.mnuPrintTrending.Click += new System.EventHandler(this.mnuPrintTrending_Click);
			// 
			// mnuSepar3
			// 
			this.mnuSepar3.Index = 2;
			this.mnuSepar3.Name = "mnuSepar3";
			this.mnuSepar3.Text = "-";
			// 
			// mnuImport
			// 
			this.mnuImport.Index = 3;
			this.mnuImport.Name = "mnuImport";
			this.mnuImport.Text = "Import Trend Rates";
			this.mnuImport.Click += new System.EventHandler(this.mnuImport_Click);
			// 
			// mnuExport
			// 
			this.mnuExport.Index = 4;
			this.mnuExport.Name = "mnuExport";
			this.mnuExport.Text = "Export Trend Rates";
			this.mnuExport.Click += new System.EventHandler(this.mnuExport_Click);
			// 
			// mnuSepar1
			// 
			this.mnuSepar1.Index = 5;
			this.mnuSepar1.Name = "mnuSepar1";
			this.mnuSepar1.Text = "-";
			// 
			// mnuSave
			// 
			this.mnuSave.Index = 6;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuSave.Text = "Save";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Index = 7;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Text = "Save & Exit";
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 8;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 9;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(375, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(135, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.TabStop = false;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// cmdExport
			// 
			this.cmdExport.Anchor = Wisej.Web.AnchorStyles.Right;
			this.cmdExport.Location = new System.Drawing.Point(467, 26);
			this.cmdExport.Name = "cmdExport";
			this.cmdExport.Size = new System.Drawing.Size(134, 25);
			this.cmdExport.TabIndex = 10;
			this.cmdExport.Text = "Export Trend Rates";
			this.cmdExport.Click += new System.EventHandler(this.mnuExport_Click);
			// 
			// cmdImport
			// 
			this.cmdImport.Anchor = Wisej.Web.AnchorStyles.Right;
			this.cmdImport.Location = new System.Drawing.Point(607, 26);
			this.cmdImport.Name = "cmdImport";
			this.cmdImport.Size = new System.Drawing.Size(134, 25);
			this.cmdImport.TabIndex = 9;
			this.cmdImport.Text = "Import Trend Rates";
			this.cmdImport.Click += new System.EventHandler(this.mnuImport_Click);
			// 
			// cmdPrintExemptions
			// 
			this.cmdPrintExemptions.Anchor = Wisej.Web.AnchorStyles.Right;
			this.cmdPrintExemptions.Location = new System.Drawing.Point(937, 26);
			this.cmdPrintExemptions.Name = "cmdPrintExemptions";
			this.cmdPrintExemptions.Size = new System.Drawing.Size(119, 25);
			this.cmdPrintExemptions.TabIndex = 7;
			this.cmdPrintExemptions.Text = "Print Exemptions";
			this.cmdPrintExemptions.Click += new System.EventHandler(this.mnuPrintExemptions_Click);
			// 
			// cmdPrintTrending
			// 
			this.cmdPrintTrending.Anchor = Wisej.Web.AnchorStyles.Right;
			this.cmdPrintTrending.Location = new System.Drawing.Point(747, 26);
			this.cmdPrintTrending.Name = "cmdPrintTrending";
			this.cmdPrintTrending.Size = new System.Drawing.Size(184, 25);
			this.cmdPrintTrending.TabIndex = 8;
			this.cmdPrintTrending.Text = "Print Trending / Cap Rates";
			this.cmdPrintTrending.Click += new System.EventHandler(this.mnuPrintTrending_Click);
			// 
			// frmTrendExempt
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmTrendExempt";
			this.ShowInTaskbar = false;
			this.Text = "Exemptions / Trending";
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmTrendExempt_Load);
			this.Resize += new System.EventHandler(this.frmTrendExempt_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmTrendExempt_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmTrendExempt_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridTrend)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraWait)).EndInit();
			this.fraWait.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdExport)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdImport)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintExemptions)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintTrending)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
