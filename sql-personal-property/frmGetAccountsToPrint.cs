﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmGetAccountsToPrint.
	/// </summary>
	public partial class frmGetAccountsToPrint : BaseForm
	{
		public frmGetAccountsToPrint()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGetAccountsToPrint InstancePtr
		{
			get
			{
				return (frmGetAccountsToPrint)Sys.GetInstance(typeof(frmGetAccountsToPrint));
			}
		}

		protected frmGetAccountsToPrint _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		bool boolExited;

		public bool Init(string strCaption = "Accounts To Print")
		{
			bool Init = false;
			Init = false;
			boolExited = true;
			Label1.Text = strCaption;
			this.Show(FormShowEnum.Modal);
			Init = !boolExited;
			return Init;
		}

		private void destGrid_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (destGrid.Row < destGrid.Rows - 1)
			{
				destGrid.Row += 1;
			}
		}

		private void destGrid_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right)
			{
				if (destGrid.MouseRow > 0 && destGrid.MouseRow < destGrid.Rows)
				{
					destGrid.Row = destGrid.MouseRow;
				}
			}
		}

		private void destGrid_MouseUpEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right)
			{
				this.PopupMenu(mnuDest);
			}
		}

		private void destGrid_RowColChange(object sender, System.EventArgs e)
		{
			switch (destGrid.Col)
			{
				case 0:
					{
						destGrid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						break;
					}
				case 1:
					{
						destGrid.Editable = FCGrid.EditableSettings.flexEDNone;
						break;
					}
			}
			//end switch
		}

		private void destGrid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = destGrid.GetFlexRowIndex(e.RowIndex);
			int col = destGrid.GetFlexColIndex(e.ColumnIndex);
			clsDRWrapper clsLoad = new clsDRWrapper();
			if (col == 0)
			{
				if (Conversion.Val(destGrid.EditText) > 0)
				{
					clsLoad.OpenRecordset("select name,deleted from ppmaster where account = " + FCConvert.ToString(Conversion.Val(destGrid.EditText)), modPPGN.strPPDatabase);
					if (!clsLoad.EndOfFile())
					{
						if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("deleted")))
						{
							MessageBox.Show("That account is deleted", "Deleted Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							e.Cancel = true;
							return;
						}
						destGrid.TextMatrix(row, 1, FCConvert.ToString(clsLoad.Get_Fields_String("name")));
						if (row == destGrid.Rows - 1)
						{
							// add another row
							destGrid.Rows += 1;
							return;
						}
						if (Strings.Trim(destGrid.TextMatrix(destGrid.Rows - 1, 0)) != string.Empty)
						{
							destGrid.Rows += 1;
						}
					}
					else
					{
						MessageBox.Show("That account doesn't exist", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
						return;
					}
				}
				else
				{
					//FC:FINAL:AM:#i1517 - validate the edit if the text is empty
					//e.Cancel = true;
					return;
				}
			}
		}

		private void frmGetAccountsToPrint_Activated(object sender, System.EventArgs e)
		{
			destGrid.Focus();
		}

		private void frmGetAccountsToPrint_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.F10:
					{
						KeyCode = (Keys)0;
						mnuSave_Click();
						break;
					}
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuCancel_Click();
						break;
					}
			}
			//end switch
		}

		private void frmGetAccountsToPrint_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGetAccountsToPrint properties;
			//frmGetAccountsToPrint.ScaleWidth	= 9045;
			//frmGetAccountsToPrint.ScaleHeight	= 7395;
			//frmGetAccountsToPrint.LinkTopic	= "Form1";
			//frmGetAccountsToPrint.LockControls	= true;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			//Label1.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			SetupGrids();
			FillSrcGrid();
			//FC:FINAL:DDU:#1468 - fixed sorting by setting col type
			srcGrid.Columns[0].ValueType = typeof(int);
		}

		private void SetupGrids()
		{
			srcGrid.TextMatrix(0, 0, "Account");
			srcGrid.TextMatrix(0, 1, "Name");
			destGrid.TextMatrix(0, 0, "Account");
			destGrid.TextMatrix(0, 1, "Name");
		}

		private void ResizeGrids()
		{
			int GridWidth = 0;
			GridWidth = srcGrid.WidthOriginal;
			srcGrid.ColWidth(0, FCConvert.ToInt32(0.25 * GridWidth));
			srcGrid.ColWidth(1, FCConvert.ToInt32(0.7 * GridWidth));
			GridWidth = destGrid.WidthOriginal;
			destGrid.ColWidth(0, FCConvert.ToInt32(0.25 * GridWidth));
			destGrid.ColWidth(1, FCConvert.ToInt32(0.7 * GridWidth));
		}

		private void FillSrcGrid()
		{
			clsDRWrapper clsTemp = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				string strMasterJoin;
				strMasterJoin = modPPGN.GetMasterJoinForJoin();
				clsTemp.OpenRecordset("select account,name from " + strMasterJoin + " where account > 0 and not deleted = 1", "twpp0000.vb1");
				while (!clsTemp.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					srcGrid.AddItem(clsTemp.Get_Fields("account") + "\t" + clsTemp.Get_Fields_String("name"));
					clsTemp.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "Occured in Fill Source Grid", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmGetAccountsToPrint_Resize(object sender, System.EventArgs e)
		{
			ResizeGrids();
		}

		private void mnuAdd_Click(object sender, System.EventArgs e)
		{
			if (srcGrid.Row > 0 && srcGrid.Row < srcGrid.Rows - 1)
			{
				destGrid.AddItem(srcGrid.TextMatrix(srcGrid.Row, 0) + "\t" + srcGrid.TextMatrix(srcGrid.Row, 1));
			}
		}

		private void mnuCancel_Click(object sender, System.EventArgs e)
		{
			modPPGN.Statics.CancelledIt = true;
			boolExited = true;
			Close();
		}

		public void mnuCancel_Click()
		{
			mnuCancel_Click(mnuCancel, new System.EventArgs());
		}

		private void mnuCancel2_Click(object sender, System.EventArgs e)
		{
			return;
		}

		private void mnuCancel3_Click(object sender, System.EventArgs e)
		{
			return;
		}

		private void mnuClear_Click(object sender, System.EventArgs e)
		{
			destGrid.Rows = 1;
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			if (destGrid.Row > 0 && destGrid.Row < destGrid.Rows)
			{
				destGrid.RemoveItem(destGrid.Row);
				if (destGrid.Rows == 1)
				{
					destGrid.Rows += 1;
				}
				else
				{
					if (Strings.Trim(destGrid.TextMatrix(destGrid.Rows - 1, 0)) != string.Empty)
					{
						destGrid.Rows += 1;
					}
				}
			}
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			int x;
			clsDRWrapper clsTemp = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				if (destGrid.Rows < 2)
				{
					MessageBox.Show("You haven't picked any accounts yet.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				destGrid.Row = 0;
				clsTemp.Execute("delete from accountlist", "twpp0000.vb1");
				for (x = 1; x <= destGrid.Rows - 1; x++)
				{
					if (Conversion.Val(destGrid.TextMatrix(x, 0)) > 0)
					{
						// ignore blanks and zeros
						clsTemp.Execute("insert into accountlist  (account) values (" + destGrid.TextMatrix(x, 0) + ")", "twpp0000.vb1");
					}
				}
				// x
				boolExited = false;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				//FC:FINAL:CHN - issue #1354: Fix not handled form closing. 
				isClosedBySaveButton = true;
				Close();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Error number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "Occured in Save", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void mnuSave_Click()
		{
			mnuSave_Click(mnuSave, new System.EventArgs());
		}

		private void srcGrid_DblClick(object sender, System.EventArgs e)
		{
			if (srcGrid.Row > 0 && srcGrid.Row < srcGrid.Rows)
			{
				destGrid.AddItem(srcGrid.TextMatrix(srcGrid.Row, 0) + "\t" + srcGrid.TextMatrix(srcGrid.Row, 1));
			}
		}

		private void srcGrid_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right)
			{
				if (srcGrid.MouseRow > 0 && srcGrid.MouseRow < srcGrid.Rows)
				{
					srcGrid.Row = srcGrid.MouseRow;
				}
			}
		}

		private void srcGrid_MouseUpEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right)
			{
				this.PopupMenu(mnuSrc);
			}
		}

		private void cmdExit_Click(object sender, EventArgs e)
		{
			mnuCancel_Click();
		}
		//FC:FINAL:CHN - issue #1354: Fix not handled form closing.
		private bool isClosedBySaveButton = false;

		private void FrmGetAccountsToPrint_FormClosed(object sender, FormClosedEventArgs e)
		{
			if (!isClosedBySaveButton)
			{
				this.mnuCancel_Click(null, null);
			}
		}
	}
}
