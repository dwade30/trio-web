﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;
using fecherFoundation.VisualBasicLayer;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmPPCalculations.
	/// </summary>
	public partial class frmPPCalculations : BaseForm
	{
		public frmPPCalculations()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.lblStatics = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.lblStatics.AddControlArrayElement(lblStatics_27, 27);
			this.lblStatics.AddControlArrayElement(lblStatics_29, 29);
			this.lblStatics.AddControlArrayElement(lblStatics_20, 20);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPPCalculations InstancePtr
		{
			get
			{
				return (frmPPCalculations)Sys.GetInstance(typeof(frmPPCalculations));
			}
		}

		protected frmPPCalculations _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		double Ratio;
		string OverrideCode = "";
		int OverrideValue;
		int[] LeasedOriginalCost = new int[9 + 1];
		int[] LeasedCalculatedCost = new int[9 + 1];
		int[] ItemizedOriginalCost = new int[9 + 1];
		int[] ItemizedCalculatedCost = new int[9 + 1];
		int[] ItemizedBETEExempt = new int[9 + 1];
		int[] LeasedBETEExempt = new int[9 + 1];
		clsDRWrapper datItemized = new clsDRWrapper();
		clsDRWrapper datLeased = new clsDRWrapper();
		clsDRWrapper datRatio = new clsDRWrapper();
		clsDRWrapper datValuations = new clsDRWrapper();
		clsDRWrapper datMaster = new clsDRWrapper();
		cParty tParty;
		cPartyController tPCont = new cPartyController();
		string strSQL;
		int intPage;
		bool boolNotSaved;
		string strOpen1 = "";
		string strOpen2 = "";
		const int CNSTITEMIZEDCOLBETEEXEMPT = 15;
		const int CNSTITEMIZEDCOLBETEYEAR = 16;
		const int CNSTCATEGORYCOLBETEEXEMPT = 7;

		public string Open1
		{
			get
			{
				string Open1 = "";
				Open1 = strOpen1;
				return Open1;
			}
		}

		public string Open2
		{
			get
			{
				string Open2 = "";
				Open2 = strOpen2;
				return Open2;
			}
		}

		private void Check1_CheckedChanged(object sender, System.EventArgs e)
		{
			if (Check1.CheckState == CheckState.Checked)
			{
				CheckOverride();
				txtOverride.Enabled = true;
				txtOverride.ReadOnly = false;
				txtOverride.Visible = true;
				lblOverride.Visible = true;
				sstPPCalculate.Visible = true;
				// txtOverride.SetFocus
			}
			else
			{
				txtOverride.Enabled = false;
				txtOverride.ReadOnly = true;
				txtOverride.Text = "";
				lblOverride.Visible = false;
			}
		}

		public void frmPPCalculations_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo Error_Tag_1
				if (modGNWork.FormExist(this))
					return;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				// sstPPCalculate.Visible = False
				modPPGN.Statics.Exiting = false;
				// 
				FCUtils.EraseSafe(ItemizedOriginalCost);
				FCUtils.EraseSafe(ItemizedCalculatedCost);
				FCUtils.EraseSafe(LeasedOriginalCost);
				FCUtils.EraseSafe(LeasedCalculatedCost);
				FCUtils.EraseSafe(ItemizedBETEExempt);
				FCUtils.EraseSafe(LeasedBETEExempt);
				//FC:FINAL:BBE - FCGrid.Clear() will be clear everywhere and everything, optional parameters
				//vsLeased.Clear(FCGrid.ClearWhereSettings.flexClearEverywhere, FCGrid.ClearWhatSettings.flexClearEverything);
				//vsItemized.Clear(FCGrid.ClearWhereSettings.flexClearEverywhere, FCGrid.ClearWhatSettings.flexClearEverything);
				//vsDisplay.Clear(FCGrid.ClearWhereSettings.flexClearEverywhere, FCGrid.ClearWhatSettings.flexClearEverything);
				//vsValuations.Clear(FCGrid.ClearWhereSettings.flexClearEverywhere, FCGrid.ClearWhatSettings.flexClearEverything);
				vsLeased.Clear();
				vsItemized.Clear();
				vsDisplay.Clear();
				vsValuations.Clear();
				SetupGrid();
				// Select CurrentAccount if none.
				if (modPPGN.Statics.CurrentAccount < 1 || modPPGN.Statics.CurrentAccount == -1)
				{
					// vbPorter upgrade warning: hold As Variant --> As string
					string hold = "";
					DoOverTag:
					;
					hold = Interaction.InputBox("Enter Account Number", "");
					if (hold == "")
					{
						frmPPCalculations.InstancePtr.Close();
						return;
					}
					modPPGN.Statics.CurrentAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(hold)));
					if (modPPGN.Statics.CurrentAccount > modPPCalculate.Statics.NextPPAccount)
					{
						MessageBox.Show("You have selected an account number greater than your highest available number. Please reselect an account.", "Account Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						goto DoOverTag;
					}
				}
				// 
				// Get Data From Cost Files
				strSQL = "SELECT * FROM PPMaster WHERE Account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount);
				datMaster.OpenRecordset(strSQL, modPPGN.strPPDatabase);
				if (datMaster.EndOfFile())
				{
					Close();
					FCGlobal.Screen.MousePointer = 0;
					return;
				}
				if (FCConvert.ToBoolean(datMaster.Get_Fields_Boolean("deleted")))
				{
					MessageBox.Show("This account has been deleted.  Unable to display.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					Close();
					FCGlobal.Screen.MousePointer = 0;
					return;
				}
				lblAccount.Text = FCConvert.ToString(modPPGN.Statics.CurrentAccount);
				tParty = tPCont.GetParty(datMaster.Get_Fields_Int32("partyid"));
				if (!(tParty == null))
				{
					lblName.Text = tParty.FullNameLastFirst;
				}
				else
				{
					lblName.Text = "";
				}
				lblLocationName.Text = FCConvert.ToString(datMaster.Get_Fields_String("street"));
				// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
				if (Conversion.Val(datMaster.Get_Fields("streetnumber")) > 0)
				{
					// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
					lblLocation.Text = FCConvert.ToString(datMaster.Get_Fields("streetnumber"));
				}
				else
				{
					lblLocation.Text = "";
				}
				strOpen1 = FCConvert.ToString(datMaster.Get_Fields_String("Open1"));
				strOpen2 = FCConvert.ToString(datMaster.Get_Fields_String("Open2"));
				GetCostData();
				// 
				// Setup Leased Table
				strSQL = "SELECT Line, RB, CD, Quantity, Description, Cost, LeaseDate, MOS, L1P2, MonthlyRent, DEPYRS, Fctr, [Value],id,beteexempt,exemptyear FROM PPLeased WHERE Account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount) + " ORDER BY [Line]";
				// strSQL = "select * from ppleased where account = " & CurrentAccount & " order by [line]"
				datLeased.OpenRecordset(strSQL, modPPGN.strPPDatabase);
				if (vsLeased.Rows == 1)
					vsLeased.Rows = 2;
				// 
				// Setup Itemized Table
				strSQL = "SELECT Line, RB, Cd, Quantity, Description, Year, DPYR, SRO, RCYR, Cost, GD, Fctr, [Value],id,beteexempt,EXEMPTYEAR FROM PPItemized WHERE Account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount) + " ORDER BY [Line]";
				// strSQL = "select * from ppitemized where account = " & CurrentAccount & "order by [line]"
				datItemized.OpenRecordset(strSQL, modPPGN.strPPDatabase);
				if (vsItemized.Rows == 1)
					vsItemized.Rows = 2;
				// 
				// Setup Ratio Amount
				strSQL = "SELECT * FROM PPRatioOpens";
				datRatio.OpenRecordset(strSQL, modPPGN.strPPDatabase);
				// TODO Get_Fields: Check the table for the column [Ratio] and replace with corresponding Get_Field method
				Ratio = Conversion.Val(datRatio.Get_Fields("Ratio")) / 100;
				// 
				// Fill Master Update Fields
				// lblLocation.Caption = Format(lblLocation.Caption, "#####")
				if (fecherFoundation.FCUtils.IsNull(datMaster.Get_Fields_String("ORCode")) == false)
					OverrideCode = FCConvert.ToString(datMaster.Get_Fields_String("ORCode"));
				// 
				// Setting header height and titles
				// Leased
				FillGrid();
				SetColumnWidths();
				if (!CalculateItemized())
				{
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					Close();
					return;
				}
				CalculateLeased();
				AccumulateLeasedValues();
				AccumulateItemizedValues();
				// 
				// Setup Valuation Table
				strSQL = "SELECT * FROM PPValuations WHERE ValueKey = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount);
				// strSQL = "select * from ppvaluations where valuekey = " & CurrentAccount
				datValuations.OpenRecordset(strSQL, modPPGN.strPPDatabase);
				if (datValuations.RecordCount() > 0)
				{
					if (fecherFoundation.FCUtils.IsNull(datValuations.Get_Fields_Int32("OverRideAmount")) == false)
						OverrideValue = FCConvert.ToInt32(datValuations.Get_Fields_Int32("OverRideAmount"));
				}
				SetValuations();
				// 
				// Build Display Flex Grid For Valuations
				BuildDisplay();
				CheckOverride();
				// Call SetupGrid
				sstPPCalculate.Left = FCConvert.ToInt32((frmPPCalculations.InstancePtr.Width - sstPPCalculate.Width) / 2.0);
				sstPPCalculate.SelectedIndex = 2;
				vsItemized.ColHidden(14, true);
				//FC:FINAL:MSH - issue #1349: remove an extra method call to avoid missing data
				//SetupGrid();
				// vsItemized.ColHidden(15) = True
				frmPPCalculations.InstancePtr.Refresh();
				modPPGN.Statics.boolDoneCalc = true;
			}
			catch (Exception ex)
			{
				// Error_Tag_1:
				Information.Err(ex).Clear();
				FCGlobal.Screen.MousePointer = 0;
			}
		}

		private void GetCostData()
		{
			// vbPorter upgrade warning: xx As object	OnWrite(short, object)
			int xx;
			clsDRWrapper datCostFiles = new clsDRWrapper();
			datCostFiles.OpenRecordset("SELECT * FROM RatioTrends", modPPGN.strPPDatabase);
			datCostFiles.MoveLast();
			datCostFiles.MoveFirst();
			for (xx = 1; xx <= 9; xx++)
			{
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				xx = datCostFiles.Get_Fields("Type");
				// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
				modPPCalculate.Statics.HighPct[xx] = FCConvert.ToInt32(datCostFiles.Get_Fields("High"));
				// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
				modPPCalculate.Statics.LowPct[xx] = FCConvert.ToInt32(datCostFiles.Get_Fields("Low"));
				modPPCalculate.Statics.Exponent[xx] = datCostFiles.Get_Fields_Double("Exponent");
				// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
				modPPCalculate.Statics.DefLife[xx] = FCConvert.ToInt32(datCostFiles.Get_Fields("Life"));
				modPPCalculate.Statics.DepMeth[xx] = FCConvert.ToString(datCostFiles.Get_Fields_String("SD"));
				// TODO Get_Fields: Check the table for the column [Trend] and replace with corresponding Get_Field method
				modPPCalculate.Statics.TrendCode[xx] = FCConvert.ToString(datCostFiles.Get_Fields("Trend"));
				modPPCalculate.Statics.DefDesc[xx] = FCConvert.ToString(datCostFiles.Get_Fields_String("Description"));
				datCostFiles.MoveNext();
			}
			// xx
			datCostFiles.OpenRecordset("SELECT * FROM TrendCapRates", modPPGN.strPPDatabase);
			datCostFiles.MoveLast();
			datCostFiles.MoveFirst();
			xx = 0;
			while (!datCostFiles.EndOfFile())
			{
				xx += 1;
				modPPCalculate.Statics.CapRate[xx] = datCostFiles.Get_Fields_Double("CapRate");
				datCostFiles.MoveNext();
			}
		}

		private void SetColumnWidths()
		{
			double dblScreenAdjustment = 0;
			int GridWidth;
			// dblScreenAdjustment = Screen.Width / 9600
			// Itemized Table
			GridWidth = vsItemized.WidthOriginal;
			vsItemized.ColWidth(0, FCConvert.ToInt32(0 * dblScreenAdjustment));
			// vsItemized.ColWidth(1) = 450 * dblScreenAdjustment     ' 4  Line
			vsItemized.ColWidth(1, FCConvert.ToInt32(0.04 * GridWidth));
			// vsItemized.ColWidth(2) = 225 * dblScreenAdjustment     ' 2  RB
			vsItemized.ColWidth(2, FCConvert.ToInt32(0.029 * GridWidth));
			// vsItemized.ColWidth(3) = 215 * dblScreenAdjustment     ' 2  CD
			vsItemized.ColWidth(3, FCConvert.ToInt32(0.028 * GridWidth));
			// vsItemized.ColWidth(4) = 325 * dblScreenAdjustment     ' 3  Quantity
			vsItemized.ColWidth(4, FCConvert.ToInt32(0.035 * GridWidth));
			// vsItemized.ColWidth(5) = 2300 * dblScreenAdjustment    '22  Description
			vsItemized.ColWidth(5, FCConvert.ToInt32(0.24 * GridWidth));
			// vsItemized.ColWidth(6) = 425 * dblScreenAdjustment     ' 4  Year
			vsItemized.ColWidth(6, FCConvert.ToInt32(0.054 * GridWidth));
			// vsItemized.ColWidth(7) = 375 * dblScreenAdjustment     ' 4  DPYR
			vsItemized.ColWidth(7, FCConvert.ToInt32(0.036 * GridWidth));
			// vsItemized.ColWidth(8) = 320 * dblScreenAdjustment     ' 2  SRO
			vsItemized.ColWidth(8, FCConvert.ToInt32(0.031 * GridWidth));
			// vsItemized.ColWidth(9) = 325 * dblScreenAdjustment     ' 2  RCYR
			vsItemized.ColWidth(9, FCConvert.ToInt32(0.042 * GridWidth));
			// vsItemized.ColWidth(10) = 860 * dblScreenAdjustment   '10  Cost
			vsItemized.ColWidth(10, FCConvert.ToInt32(0.1 * GridWidth));
			// vsItemized.ColWidth(11) = 325 * dblScreenAdjustment    ' 3  %GD
			vsItemized.ColWidth(11, FCConvert.ToInt32(0.042 * GridWidth));
			// vsItemized.ColWidth(12) = 435 * dblScreenAdjustment    ' 4  FCTR
			vsItemized.ColWidth(12, FCConvert.ToInt32(0.047 * GridWidth));
			// vsItemized.ColWidth(13) = 1160 * dblScreenAdjustment   '10  Value
			vsItemized.ColWidth(13, FCConvert.ToInt32(0.14 * GridWidth));
			vsItemized.ExtendLastCol = true;
			// Leased Table
			GridWidth = vsLeased.WidthOriginal;
			vsLeased.ColWidth(0, FCConvert.ToInt32(0 * dblScreenAdjustment));
			vsLeased.ColWidth(1, FCConvert.ToInt32(0.058 * GridWidth));
			// vsLeased.ColWidth(2) = 0.027 * GridWidth
			vsLeased.ColHidden(2, true);
			vsLeased.ColWidth(3, FCConvert.ToInt32(0.027 * GridWidth));
			vsLeased.ColWidth(4, FCConvert.ToInt32(0.037 * GridWidth));
			vsLeased.ColWidth(5, FCConvert.ToInt32(0.244 * GridWidth));
			vsLeased.ColWidth(6, FCConvert.ToInt32(0.102 * GridWidth));
			vsLeased.ColWidth(7, FCConvert.ToInt32(0.079 * GridWidth));
			vsLeased.ColWidth(8, FCConvert.ToInt32(0.054 * GridWidth));
			vsLeased.ColHidden(9, true);
			// vsLeased.ColWidth(9) = 0.041 * GridWidth
			vsLeased.ColWidth(10, FCConvert.ToInt32(0.09 * GridWidth));
			vsLeased.ColWidth(11, FCConvert.ToInt32(0.036 * GridWidth));
			vsLeased.ColWidth(12, FCConvert.ToInt32(0.05 * GridWidth));
			vsLeased.ColWidth(13, FCConvert.ToInt32(0.12 * GridWidth));
			vsLeased.ExtendLastCol = true;
			GridWidth = vsDisplay.WidthOriginal;
			// .ColWidth(0) = 2500 * dblScreenAdjustment
			vsDisplay.ColWidth(0, FCConvert.ToInt32(0.21 * GridWidth));
			// .ColWidth(1) = 900 * dblScreenAdjustment
			vsDisplay.ColWidth(1, FCConvert.ToInt32(0.11 * GridWidth));
			// .ColWidth(2) = 900 * dblScreenAdjustment
			vsDisplay.ColWidth(2, FCConvert.ToInt32(0.12 * GridWidth));
			// .ColWidth(3) = 900 * dblScreenAdjustment
			vsDisplay.ColWidth(3, FCConvert.ToInt32(0.11 * GridWidth));
			// .ColWidth(4) = 900 * dblScreenAdjustment
			vsDisplay.ColWidth(4, FCConvert.ToInt32(0.12 * GridWidth));
			// .ColWidth(5) = 600 * dblScreenAdjustment
			vsDisplay.ColWidth(5, FCConvert.ToInt32(0.04 * GridWidth));
			vsDisplay.ColWidth(6, FCConvert.ToInt32(0.14 * GridWidth));
		}

		private void SetCellAlignments()
		{
			// Itemized Table
			//FC:FINAL:DDU:#1320 - corect align columns
			vsItemized.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsItemized.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsItemized.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsItemized.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsItemized.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsItemized.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsItemized.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsItemized.ColAlignment(7, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsItemized.ColAlignment(8, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsItemized.ColAlignment(9, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsItemized.ColAlignment(10, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsItemized.ColAlignment(11, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsItemized.ColAlignment(12, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsItemized.ColAlignment(13, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsItemized.ColAlignment(15, FCGrid.AlignmentSettings.flexAlignRightCenter);
			//vsItemized.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 1, vsItemized.Rows - 1, 1, 4);
			//         vsItemized.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 2, vsItemized.Rows - 1, 2, 4);
			//         vsItemized.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 3, vsItemized.Rows - 1, 3, 4);
			//         vsItemized.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 4, vsItemized.Rows - 1, 4, 4);
			//         vsItemized.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 5, vsItemized.Rows - 1, 5, 1);
			//         vsItemized.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 6, vsItemized.Rows - 1, 6, 4);
			//         vsItemized.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 7, vsItemized.Rows - 1, 7, 4);
			//         vsItemized.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 8, vsItemized.Rows - 1, 8, 4);
			//         vsItemized.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 9, vsItemized.Rows - 1, 9, 4);
			//         vsItemized.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 10, vsItemized.Rows - 1, 10, 7);
			//         vsItemized.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 11, vsItemized.Rows - 1, 11, 4);
			//         vsItemized.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 12, vsItemized.Rows - 1, 12, 4);
			//         vsItemized.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 13, vsItemized.Rows - 1, 13, 7);
			// Leased Table
			vsLeased.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsLeased.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsLeased.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsLeased.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsLeased.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsLeased.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsLeased.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsLeased.ColAlignment(7, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsLeased.ColAlignment(8, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsLeased.ColAlignment(9, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsLeased.ColAlignment(10, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsLeased.ColAlignment(11, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsLeased.ColAlignment(12, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsLeased.ColAlignment(13, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vsLeased.ColAlignment(15, FCGrid.AlignmentSettings.flexAlignRightCenter); 
			//vsLeased.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 1, vsLeased.Rows - 1, 1, 1);
			//         vsLeased.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 2, vsLeased.Rows - 1, 2, 4);
			//         vsLeased.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 3, vsLeased.Rows - 1, 3, 4);
			//         vsLeased.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 4, vsLeased.Rows - 1, 4, 4);
			//         vsLeased.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 5, vsLeased.Rows - 1, 5, 1);
			//         vsLeased.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 6, vsLeased.Rows - 1, 6, 7);
			//         vsLeased.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 7, vsLeased.Rows - 1, 7, 1);
			//         vsLeased.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 8, vsLeased.Rows - 1, 8, 4);
			//         vsLeased.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 9, vsLeased.Rows - 1, 9, 4);
			//         vsLeased.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 10, vsLeased.Rows - 1, 10, 7);
			//         vsLeased.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 11, vsLeased.Rows - 1, 11, 4);
			//         vsLeased.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 12, vsLeased.Rows - 1, 12, 4);
			//         vsLeased.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 13, vsLeased.Rows - 1, 13, 7);
		}

		private bool CalculateItemized()
		{
			bool CalculateItemized = false;
			clsDRWrapper rsTemp = new clsDRWrapper();
			int lngCurBETEYear;
			lngCurBETEYear = modGlobal.Statics.CustomizeStuff.CurrentBETEYear;
			if (lngCurBETEYear == 0)
			{
				lngCurBETEYear = DateTime.Today.Year;
			}
			CalculateItemized = false;
			if (datItemized.RecordCount() > 0)
			{
				modPPCalculate.Statics.xRow = 0;
				if (vsItemized.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 13, vsItemized.Rows - 1, 13) != modGlobalConstants.Statics.TRIOCOLORBLUE)
				{
					//FC:FINAL:DDU:#1319 - removed blue color
					//vsItemized.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 13, vsItemized.Rows - 1, 13, Color.Blue);
					//vsItemized.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 1, 13, vsItemized.Rows - 1, 13, Color.White);
					// PERFORM CALCULATIONS HERE
					modPPCalculate.GetTrendValues();
					for (modPPCalculate.Statics.xRow = 1; modPPCalculate.Statics.xRow <= vsItemized.Rows - 1; modPPCalculate.Statics.xRow++)
					{
						if (vsItemized.TextMatrix(modPPCalculate.Statics.xRow, 3) != "-")
						{
							// Set Variables equal to CellText from Current Line
							modPPCalculate.Statics.Work1 = 0;
							modPPCalculate.Statics.TrendedCost = 0;
							modPPCalculate.Statics.clCode = vsItemized.TextMatrix(modPPCalculate.Statics.xRow, 3);
							modPPCalculate.Statics.clQuantity = FCConvert.ToInt32(vsItemized.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 4));
							modPPCalculate.Statics.CLYEAR = FCConvert.ToInt32(vsItemized.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 6));
							modPPCalculate.Statics.clDPYR = FCConvert.ToInt32(vsItemized.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 7));
							modPPCalculate.Statics.clSRO = vsItemized.TextMatrix(modPPCalculate.Statics.xRow, 8);
							modPPCalculate.Statics.clRCYR = FCConvert.ToInt32(vsItemized.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 9));
							modPPCalculate.Statics.clCost = FCConvert.ToInt32(vsItemized.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 10));
							modPPCalculate.Statics.clPercentGood = Conversion.Val(vsItemized.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 11));
							if (modPPCalculate.Statics.clPercentGood != 0)
								modPPCalculate.Statics.clPercentGood *= 100;
							modPPCalculate.Statics.clFactor = Conversion.Val(vsItemized.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 12)) / 100;
							if (modPPCalculate.Statics.clFactor == 1)
							{
								vsItemized.Cell(FCGrid.CellPropertySettings.flexcpForeColor, modPPCalculate.Statics.xRow, 12, vsItemized.ForeColor);
							}
							else
							{
								vsItemized.Cell(FCGrid.CellPropertySettings.flexcpForeColor, modPPCalculate.Statics.xRow, 12, modGlobalConstants.Statics.TRIOCOLORRED);
							}
							// 
							if (modPPCalculate.Statics.clCost != 0)
							{
								if (!modPPCalculate.OriginalCostValue())
								{
									MessageBox.Show("Error calculating original cost for " + vsItemized.TextMatrix(modPPCalculate.Statics.xRow, 5), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
									return CalculateItemized;
								}
							}
							vsItemized.TextMatrix(modPPCalculate.Statics.xRow, 10, FCConvert.ToString(modPPCalculate.Statics.TrendedCost));
							// If CustomizeStuff.RoundIndividualItems Then
							// Work1 = Round(Work1, PPRounding)
							// End If
							if (Conversion.Val(vsItemized.TextMatrix(modPPCalculate.Statics.xRow, CNSTITEMIZEDCOLBETEYEAR)) > 0)
							{
								// If Val(vsItemized.TextMatrix(xRow, CNSTITEMIZEDCOLBETEYEAR)) = lngCurBETEYear Or (Val(vsItemized.TextMatrix(xRow, CNSTITEMIZEDCOLBETEYEAR)) = lngCurBETEYear - 1 And CustomizeStuff.AllowPreviousBETEYear) Then
								if (Conversion.Val(vsItemized.TextMatrix(modPPCalculate.Statics.xRow, CNSTITEMIZEDCOLBETEYEAR)) == lngCurBETEYear)
								{
									// Or (Val(vsItemized.TextMatrix(xRow, CNSTITEMIZEDCOLBETEYEAR)) = lngCurBETEYear - 1) Then
									vsItemized.TextMatrix(modPPCalculate.Statics.xRow, CNSTITEMIZEDCOLBETEEXEMPT, FCConvert.ToString(modPPCalculate.Statics.Work1));
									vsItemized.TextMatrix(modPPCalculate.Statics.xRow, 13, FCConvert.ToString(0));
								}
								else
								{
									vsItemized.TextMatrix(modPPCalculate.Statics.xRow, 13, FCConvert.ToString(modPPCalculate.Statics.Work1));
									vsItemized.TextMatrix(modPPCalculate.Statics.xRow, CNSTITEMIZEDCOLBETEEXEMPT, FCConvert.ToString(0));
								}
							}
							else
							{
								vsItemized.TextMatrix(modPPCalculate.Statics.xRow, 13, FCConvert.ToString(modPPCalculate.Statics.Work1));
								vsItemized.TextMatrix(modPPCalculate.Statics.xRow, CNSTITEMIZEDCOLBETEEXEMPT, FCConvert.ToString(0));
							}
							vsItemized.TextMatrix(modPPCalculate.Statics.xRow, 11, FCConvert.ToString(modPPCalculate.Statics.clPercentGood));
							vsItemized.TextMatrix(modPPCalculate.Statics.xRow, 7, FCConvert.ToString(modPPCalculate.Statics.clDPYR));
							rsTemp.Execute("Update PPItemized Set PPItemized.Value = " + FCConvert.ToString(modPPCalculate.Statics.Work1) + " where id = " + vsItemized.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 14), modPPGN.strPPDatabase);
							if (modPPCalculate.Statics.clSRO == "S")
							{
								vsItemized.TextMatrix(modPPCalculate.Statics.xRow, 11, "00");
								// Else
								// vsItemized.TextMatrix(xRow, 11) = Format(clPercentGood \ 100, "00")
							}
						}
					}
					// xRow
				}
				vsItemized.Refresh();
			}
			CalculateItemized = true;
			return CalculateItemized;
		}

		private void CalculateLeased()
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			int lngCurBETEYear;
			lngCurBETEYear = modGlobal.Statics.CustomizeStuff.CurrentBETEYear;
			if (lngCurBETEYear == 0)
			{
				lngCurBETEYear = DateTime.Today.Year;
			}
			if (datLeased.RecordCount() > 0)
			{
				modPPCalculate.Statics.xRow = 0;
				if (vsLeased.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 13, vsLeased.Rows - 1, 13) != modGlobalConstants.Statics.TRIOCOLORBLUE)
				{
					//FC:FINAL:DDU:#1319 - removed blue color
					//vsLeased.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 13, vsLeased.Rows - 1, 13, Color.Blue);
					//               vsLeased.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 1, 13, vsLeased.Rows - 1, 13, Color.White);
					// PERFORM CALCULATIONS HERE
					modPPCalculate.GetTrendValues();
					for (modPPCalculate.Statics.xRow = 1; modPPCalculate.Statics.xRow <= vsLeased.Rows - 1; modPPCalculate.Statics.xRow++)
					{
						if (vsLeased.TextMatrix(modPPCalculate.Statics.xRow, 3) != "-")
						{
							modPPCalculate.Statics.Work1 = 0;
							modPPCalculate.Statics.TrendedCost = 0;
							modPPCalculate.Statics.LeasedDepreciation = 0;
							// Set Variables equal to CellText from Current Line
							modPPCalculate.Statics.clCode = vsLeased.TextMatrix(modPPCalculate.Statics.xRow, 3);
							modPPCalculate.Statics.clQuantity = FCConvert.ToInt32(vsLeased.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 4));
							modPPCalculate.Statics.clCost = FCConvert.ToInt32(vsLeased.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 6));
							modPPCalculate.Statics.clLeaseDate = vsLeased.TextMatrix(modPPCalculate.Statics.xRow, 7);
							modPPCalculate.Statics.clMonths = FCConvert.ToInt32(vsLeased.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 8));
							modPPCalculate.Statics.clLP = FCConvert.ToInt32(vsLeased.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 9));
							modPPCalculate.Statics.clMonthlyRent = FCConvert.ToDouble(vsLeased.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 10));
							modPPCalculate.Statics.clPercentGood = FCConvert.ToDouble(vsLeased.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 11));
							if (modPPCalculate.Statics.clPercentGood != 0)
								modPPCalculate.Statics.clPercentGood *= 100;
							modPPCalculate.Statics.clFactor = FCConvert.ToDouble(vsLeased.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 12)) / 100;
							if (modPPCalculate.Statics.clFactor == 1)
							{
								vsLeased.Cell(FCGrid.CellPropertySettings.flexcpForeColor, modPPCalculate.Statics.xRow, 12, vsLeased.ForeColor);
							}
							else
							{
								vsLeased.Cell(FCGrid.CellPropertySettings.flexcpForeColor, modPPCalculate.Statics.xRow, 12, modGlobalConstants.Statics.TRIOCOLORRED);
							}
							// 
							if (modPPCalculate.Statics.clCost != 0 || modPPCalculate.Statics.clMonthlyRent != 0)
								modPPCalculate.LeasedValue();
							// If CustomizeStuff.RoundIndividualItems Then
							// Work1 = Round(Work1, PPRounding)
							// End If
							vsLeased.TextMatrix(modPPCalculate.Statics.xRow, 6, Strings.Format(modPPCalculate.Statics.TrendedCost, "#,###,##0"));
							if (Conversion.Val(vsLeased.TextMatrix(modPPCalculate.Statics.xRow, CNSTITEMIZEDCOLBETEYEAR)) > 0)
							{
								// If Val(vsItemized.TextMatrix(xRow, CNSTITEMIZEDCOLBETEYEAR)) = lngCurBETEYear Or (Val(vsItemized.TextMatrix(xRow, CNSTITEMIZEDCOLBETEYEAR)) = lngCurBETEYear - 1 And CustomizeStuff.AllowPreviousBETEYear) Then
								if (Conversion.Val(vsLeased.TextMatrix(modPPCalculate.Statics.xRow, CNSTITEMIZEDCOLBETEYEAR)) == lngCurBETEYear)
								{
									vsLeased.TextMatrix(modPPCalculate.Statics.xRow, CNSTITEMIZEDCOLBETEEXEMPT, FCConvert.ToString(modPPCalculate.Statics.Work1));
									vsLeased.TextMatrix(modPPCalculate.Statics.xRow, 13, FCConvert.ToString(0));
								}
								else
								{
									vsLeased.TextMatrix(modPPCalculate.Statics.xRow, 13, FCConvert.ToString(modPPCalculate.Statics.Work1));
									vsLeased.TextMatrix(modPPCalculate.Statics.xRow, CNSTITEMIZEDCOLBETEEXEMPT, FCConvert.ToString(0));
								}
							}
							else
							{
								vsLeased.TextMatrix(modPPCalculate.Statics.xRow, 13, FCConvert.ToString(modPPCalculate.Statics.Work1));
								vsLeased.TextMatrix(modPPCalculate.Statics.xRow, CNSTITEMIZEDCOLBETEEXEMPT, FCConvert.ToString(0));
							}
							// vsLeased.TextMatrix(xRow, 13) = Work1
							vsLeased.TextMatrix(modPPCalculate.Statics.xRow, 11, FCConvert.ToString(modPPCalculate.Statics.LeasedDepreciation));
							rsTemp.Execute("Update PPleased Set PPleased.Value = " + FCConvert.ToString(modPPCalculate.Statics.Work1) + " where id = " + vsLeased.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 14), modPPGN.strPPDatabase);
						}
					}
					// xRow
				}
			}
		}

		private void AccumulateLeasedValues()
		{
			modPPCalculate.Statics.xRow = 0;
			for (modPPCalculate.Statics.xRow = 1; modPPCalculate.Statics.xRow <= vsLeased.Rows - 1; modPPCalculate.Statics.xRow++)
			{
				if (vsLeased.TextMatrix(modPPCalculate.Statics.xRow, 3) != "-")
				{
					LeasedBETEExempt[FCConvert.ToInt32(Conversion.Val(vsLeased.TextMatrix(modPPCalculate.Statics.xRow, 3)))] = LeasedBETEExempt[FCConvert.ToInt32(Conversion.Val(vsLeased.TextMatrix(modPPCalculate.Statics.xRow, 3)))] + FCConvert.ToInt32(vsLeased.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, CNSTITEMIZEDCOLBETEEXEMPT));
					switch (FCConvert.ToInt32(Conversion.Val(vsLeased.TextMatrix(modPPCalculate.Statics.xRow, 3))))
					{
						case 1:
							{
								LeasedOriginalCost[1] += FCConvert.ToInt32(vsLeased.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 6));
								LeasedCalculatedCost[1] += FCConvert.ToInt32(vsLeased.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 13));
								break;
							}
						case 2:
							{
								LeasedOriginalCost[2] += FCConvert.ToInt32(vsLeased.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 6));
								LeasedCalculatedCost[2] += FCConvert.ToInt32(vsLeased.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 13));
								break;
							}
						case 3:
							{
								LeasedOriginalCost[3] += FCConvert.ToInt32(vsLeased.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 6));
								LeasedCalculatedCost[3] += FCConvert.ToInt32(vsLeased.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 13));
								break;
							}
						case 4:
							{
								LeasedOriginalCost[4] += FCConvert.ToInt32(vsLeased.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 6));
								LeasedCalculatedCost[4] += FCConvert.ToInt32(vsLeased.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 13));
								break;
							}
						case 5:
							{
								LeasedOriginalCost[5] += FCConvert.ToInt32(vsLeased.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 6));
								LeasedCalculatedCost[5] += FCConvert.ToInt32(vsLeased.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 13));
								break;
							}
						case 6:
							{
								LeasedOriginalCost[6] += FCConvert.ToInt32(vsLeased.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 6));
								LeasedCalculatedCost[6] += FCConvert.ToInt32(vsLeased.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 13));
								break;
							}
						case 7:
							{
								LeasedOriginalCost[7] += FCConvert.ToInt32(vsLeased.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 6));
								LeasedCalculatedCost[7] += FCConvert.ToInt32(vsLeased.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 13));
								break;
							}
						case 8:
							{
								LeasedOriginalCost[8] += FCConvert.ToInt32(vsLeased.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 6));
								LeasedCalculatedCost[8] += vsLeased.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 13);
								break;
							}
						case 9:
							{
								LeasedOriginalCost[9] += FCConvert.ToInt32(vsLeased.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 6));
								LeasedCalculatedCost[9] += FCConvert.ToInt32(vsLeased.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 13));
								break;
							}
					}
					//end switch
				}
			}
			// xRow
		}

		private void AccumulateItemizedValues()
		{
			modPPCalculate.Statics.xRow = 0;
			for (modPPCalculate.Statics.xRow = 1; modPPCalculate.Statics.xRow <= vsItemized.Rows - 1; modPPCalculate.Statics.xRow++)
			{
				if (vsItemized.TextMatrix(modPPCalculate.Statics.xRow, 3) != "-")
				{
					ItemizedBETEExempt[FCConvert.ToInt32(Conversion.Val(vsItemized.TextMatrix(modPPCalculate.Statics.xRow, 3)))] = FCConvert.ToInt32(ItemizedBETEExempt[FCConvert.ToInt32(Conversion.Val(vsItemized.TextMatrix(modPPCalculate.Statics.xRow, 3)))] + Conversion.Val(vsItemized.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, CNSTITEMIZEDCOLBETEEXEMPT)));
					switch (FCConvert.ToInt32(Conversion.Val(vsItemized.TextMatrix(modPPCalculate.Statics.xRow, 3))))
					{
						case 1:
							{
								ItemizedOriginalCost[1] += FCConvert.ToInt32(vsItemized.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 10));
								ItemizedCalculatedCost[1] += FCConvert.ToInt32(vsItemized.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 13));
								break;
							}
						case 2:
							{
								ItemizedOriginalCost[2] += FCConvert.ToInt32(vsItemized.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 10));
								ItemizedCalculatedCost[2] += FCConvert.ToInt32(vsItemized.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 13));
								break;
							}
						case 3:
							{
								ItemizedOriginalCost[3] += FCConvert.ToInt32(vsItemized.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 10));
								ItemizedCalculatedCost[3] += FCConvert.ToInt32(vsItemized.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 13));
								break;
							}
						case 4:
							{
								ItemizedOriginalCost[4] += FCConvert.ToInt32(vsItemized.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 10));
								ItemizedCalculatedCost[4] += FCConvert.ToInt32(vsItemized.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 13));
								break;
							}
						case 5:
							{
								ItemizedOriginalCost[5] += FCConvert.ToInt32(vsItemized.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 10));
								ItemizedCalculatedCost[5] += FCConvert.ToInt32(vsItemized.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 13));
								break;
							}
						case 6:
							{
								ItemizedOriginalCost[6] += FCConvert.ToInt32(vsItemized.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 10));
								ItemizedCalculatedCost[6] += FCConvert.ToInt32(vsItemized.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 13));
								break;
							}
						case 7:
							{
								ItemizedOriginalCost[7] += FCConvert.ToInt32(vsItemized.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 10));
								ItemizedCalculatedCost[7] += FCConvert.ToInt32(vsItemized.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 13));
								break;
							}
						case 8:
							{
								ItemizedOriginalCost[8] += FCConvert.ToInt32(vsItemized.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 10));
								ItemizedCalculatedCost[8] += FCConvert.ToInt32(vsItemized.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 13));
								break;
							}
						case 9:
							{
								ItemizedOriginalCost[9] += FCConvert.ToInt32(vsItemized.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 10));
								ItemizedCalculatedCost[9] += FCConvert.ToInt32(vsItemized.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, 13));
								break;
							}
					}
					//end switch
				}
			}
			// xRow
		}

		private void SetValuations()
		{
			if (datValuations.RecordCount() == 0)
			{
				datValuations.AddNew();
				datValuations.Set_Fields("ValueKey", modPPGN.Statics.CurrentAccount);
			}
			else
			{
				datValuations.Edit();
			}
			datValuations.Set_Fields("Category1", modGlobal.Round((modGlobal.Round(ItemizedCalculatedCost[1], modPPCalculate.Statics.PPRounding) + modGlobal.Round(LeasedCalculatedCost[1], modPPCalculate.Statics.PPRounding)) * Ratio, modPPCalculate.Statics.PPRounding));
			datValuations.Set_Fields("Category2", modGlobal.Round((modGlobal.Round(ItemizedCalculatedCost[2], modPPCalculate.Statics.PPRounding) + modGlobal.Round(LeasedCalculatedCost[2], modPPCalculate.Statics.PPRounding)) * Ratio, modPPCalculate.Statics.PPRounding));
			datValuations.Set_Fields("Category3", modGlobal.Round((modGlobal.Round(ItemizedCalculatedCost[3], modPPCalculate.Statics.PPRounding) + modGlobal.Round(LeasedCalculatedCost[3], modPPCalculate.Statics.PPRounding)) * Ratio, modPPCalculate.Statics.PPRounding));
			datValuations.Set_Fields("Category4", modGlobal.Round((modGlobal.Round(ItemizedCalculatedCost[4], modPPCalculate.Statics.PPRounding) + modGlobal.Round(LeasedCalculatedCost[4], modPPCalculate.Statics.PPRounding)) * Ratio, modPPCalculate.Statics.PPRounding));
			datValuations.Set_Fields("Category5", modGlobal.Round((modGlobal.Round(ItemizedCalculatedCost[5], modPPCalculate.Statics.PPRounding) + modGlobal.Round(LeasedCalculatedCost[5], modPPCalculate.Statics.PPRounding)) * Ratio, modPPCalculate.Statics.PPRounding));
			datValuations.Set_Fields("Category6", modGlobal.Round((modGlobal.Round(ItemizedCalculatedCost[6], modPPCalculate.Statics.PPRounding) + modGlobal.Round(LeasedCalculatedCost[6], modPPCalculate.Statics.PPRounding)) * Ratio, modPPCalculate.Statics.PPRounding));
			datValuations.Set_Fields("Category7", modGlobal.Round((modGlobal.Round(ItemizedCalculatedCost[7], modPPCalculate.Statics.PPRounding) + modGlobal.Round(LeasedCalculatedCost[7], modPPCalculate.Statics.PPRounding)) * Ratio, modPPCalculate.Statics.PPRounding));
			datValuations.Set_Fields("Category8", modGlobal.Round((modGlobal.Round(ItemizedCalculatedCost[8], modPPCalculate.Statics.PPRounding) + modGlobal.Round(LeasedCalculatedCost[8], modPPCalculate.Statics.PPRounding)) * Ratio, modPPCalculate.Statics.PPRounding));
			datValuations.Set_Fields("Category9", modGlobal.Round((modGlobal.Round(ItemizedCalculatedCost[9], modPPCalculate.Statics.PPRounding) + modGlobal.Round(LeasedCalculatedCost[9], modPPCalculate.Statics.PPRounding)) * Ratio, modPPCalculate.Statics.PPRounding));
			datValuations.Set_Fields("Cat1exempt", modGlobal.Round((modGlobal.Round(ItemizedBETEExempt[1], modPPCalculate.Statics.PPRounding) + modGlobal.Round(LeasedBETEExempt[1], modPPCalculate.Statics.PPRounding)) * Ratio, modPPCalculate.Statics.PPRounding));
			datValuations.Set_Fields("Cat2exempt", modGlobal.Round((modGlobal.Round(ItemizedBETEExempt[2], modPPCalculate.Statics.PPRounding) + modGlobal.Round(LeasedBETEExempt[2], modPPCalculate.Statics.PPRounding)) * Ratio, modPPCalculate.Statics.PPRounding));
			datValuations.Set_Fields("Cat3exempt", modGlobal.Round((modGlobal.Round(ItemizedBETEExempt[3], modPPCalculate.Statics.PPRounding) + modGlobal.Round(LeasedBETEExempt[3], modPPCalculate.Statics.PPRounding)) * Ratio, modPPCalculate.Statics.PPRounding));
			datValuations.Set_Fields("Cat4exempt", modGlobal.Round((modGlobal.Round(ItemizedBETEExempt[4], modPPCalculate.Statics.PPRounding) + modGlobal.Round(LeasedBETEExempt[4], modPPCalculate.Statics.PPRounding)) * Ratio, modPPCalculate.Statics.PPRounding));
			datValuations.Set_Fields("Cat5exempt", modGlobal.Round((modGlobal.Round(ItemizedBETEExempt[5], modPPCalculate.Statics.PPRounding) + modGlobal.Round(LeasedBETEExempt[5], modPPCalculate.Statics.PPRounding)) * Ratio, modPPCalculate.Statics.PPRounding));
			datValuations.Set_Fields("Cat6exempt", modGlobal.Round((modGlobal.Round(ItemizedBETEExempt[6], modPPCalculate.Statics.PPRounding) + modGlobal.Round(LeasedBETEExempt[6], modPPCalculate.Statics.PPRounding)) * Ratio, modPPCalculate.Statics.PPRounding));
			datValuations.Set_Fields("Cat7exempt", modGlobal.Round((modGlobal.Round(ItemizedBETEExempt[7], modPPCalculate.Statics.PPRounding) + modGlobal.Round(LeasedBETEExempt[7], modPPCalculate.Statics.PPRounding)) * Ratio, modPPCalculate.Statics.PPRounding));
			datValuations.Set_Fields("Cat8exempt", modGlobal.Round((modGlobal.Round(ItemizedBETEExempt[8], modPPCalculate.Statics.PPRounding) + modGlobal.Round(LeasedBETEExempt[8], modPPCalculate.Statics.PPRounding)) * Ratio, modPPCalculate.Statics.PPRounding));
			datValuations.Set_Fields("Cat9exempt", modGlobal.Round((modGlobal.Round(ItemizedBETEExempt[9], modPPCalculate.Statics.PPRounding) + modGlobal.Round(LeasedBETEExempt[9], modPPCalculate.Statics.PPRounding)) * Ratio, modPPCalculate.Statics.PPRounding));
			datValuations.Update();
		}

		private void BuildDisplay()
		{
			int fn;
			int tItemizedO = 0;
			int tItemizedC = 0;
			int tLeasedO = 0;
			int tLeasedC = 0;
			// vbPorter upgrade warning: tBETEExempt As int	OnWrite(short, double)
			int tBETEExempt = 0;
			double dblScreenAdjustment;
			// vbPorter upgrade warning: xxx As Variant --> As int
			int xxx = 0;
			modGNWork.GetLocalVariables2();
			dblScreenAdjustment = FCGlobal.Screen.Width / 9600;
			vsDisplay.Rows = 11;
			//FC:FINAL:DDU:#1320 - corect row header height
			//vsDisplay.RowHeight(0, FCConvert.ToInt32(2.8 * vsDisplay.RowHeight(1)));
			vsDisplay.Cols = 8;
			// .ColWidth(0) = 2500 * dblScreenAdjustment
			// .ColWidth(1) = 900 * dblScreenAdjustment
			// .ColWidth(2) = 900 * dblScreenAdjustment
			// .ColWidth(3) = 900 * dblScreenAdjustment
			// .ColWidth(4) = 900 * dblScreenAdjustment
			// .ColWidth(5) = 600 * dblScreenAdjustment
            //FC:FINAL:BSE:#3564 should not extend last col
			//vsDisplay.ExtendLastCol = true;
			//FC:FINAL:DDU:#1320 - corect row header height
			//FC:FINAL:DDU:#1317 - removed purple color and align cols
			vsDisplay.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDisplay.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsDisplay.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsDisplay.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsDisplay.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsDisplay.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsDisplay.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsDisplay.ColAlignment(7, FCGrid.AlignmentSettings.flexAlignRightCenter);
			//vsDisplay.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 7, 4);
			// .Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 5, 9, 5) = vbBlue
			//vsDisplay.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 5, 9, 5, Information.RGB(160, 160, 220));
			// .Cell(FCGrid.CellPropertySettings.flexcpBackColor, 10, 0, 10, 6) = vbRed
			//vsDisplay.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 10, 0, 10, 7, Information.RGB(220, 160, 160));
			vsDisplay.Cell(FCGrid.CellPropertySettings.flexcpText, 1, 5, 10, 5, Strings.Format(Ratio, "0.00"));
			vsDisplay.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, 7, true);
			vsDisplay.TextMatrix(0, 1, "Itemized" + "\r\n" + "Original" + "\r\n" + "Cost");
			vsDisplay.TextMatrix(0, 2, "Itemized" + "\r\n" + "Calculated" + "\r\n" + "Value");
			vsDisplay.TextMatrix(0, 3, "Leased" + "\r\n" + "Original" + "\r\n" + "Cost");
			vsDisplay.TextMatrix(0, 4, "Leased" + "\r\n" + "Calculated" + "\r\n" + "Value");
			vsDisplay.TextMatrix(0, 5, "Ratio");
			vsDisplay.TextMatrix(0, 6, "Total" + "\r\n" + "Calculated" + "\r\n" + "Assessment");
			vsDisplay.TextMatrix(0, CNSTCATEGORYCOLBETEEXEMPT, "BETE" + "\r\n" + "Exempt");
			vsDisplay.TextMatrix(1, 0, modPPCalculate.Statics.DefDesc[1]);
			vsDisplay.TextMatrix(2, 0, modPPCalculate.Statics.DefDesc[2]);
			vsDisplay.TextMatrix(3, 0, modPPCalculate.Statics.DefDesc[3]);
			vsDisplay.TextMatrix(4, 0, modPPCalculate.Statics.DefDesc[4]);
			vsDisplay.TextMatrix(5, 0, modPPCalculate.Statics.DefDesc[5]);
			vsDisplay.TextMatrix(6, 0, modPPCalculate.Statics.DefDesc[6]);
			vsDisplay.TextMatrix(7, 0, modPPCalculate.Statics.DefDesc[7]);
			vsDisplay.TextMatrix(8, 0, modPPCalculate.Statics.DefDesc[8]);
			vsDisplay.TextMatrix(9, 0, modPPCalculate.Statics.DefDesc[9]);
			vsDisplay.TextMatrix(10, 0, "Totals");
			tBETEExempt = 0;
			for (fn = 1; fn <= 9; fn++)
			{
				vsDisplay.TextMatrix(fn, 1, Strings.Format(ItemizedOriginalCost[fn], "#,###"));
				// .TextMatrix(fn, 2) = Format(Round(ItemizedCalculatedCost(fn), PPRounding), "#,###")
				vsDisplay.TextMatrix(fn, 2, Strings.Format(ItemizedCalculatedCost[fn], "#,###,###"));
				vsDisplay.TextMatrix(fn, 3, Strings.Format(LeasedOriginalCost[fn], "#,###"));
				// .TextMatrix(fn, 4) = Format(Round(LeasedCalculatedCost(fn), PPRounding), "#,###")
				vsDisplay.TextMatrix(fn, 4, Strings.Format(LeasedCalculatedCost[fn], "#,###,###"));
				// .TextMatrix(fn, 6) = Format(Round((Round(ItemizedCalculatedCost(fn), PPRounding) + Round(LeasedCalculatedCost(fn), PPRounding) * Ratio), PPRounding), "#,##0")
				vsDisplay.TextMatrix(fn, 6, Strings.Format(modGlobal.Round(((modGlobal.Round(ItemizedCalculatedCost[fn], modPPCalculate.Statics.PPRounding) + modGlobal.Round(LeasedCalculatedCost[fn], modPPCalculate.Statics.PPRounding)) * Ratio), modPPCalculate.Statics.PPRounding), "#,###,##0"));
				vsDisplay.TextMatrix(fn, CNSTCATEGORYCOLBETEEXEMPT, Strings.Format(modGlobal.Round(((modGlobal.Round(ItemizedBETEExempt[fn], modPPCalculate.Statics.PPRounding) + modGlobal.Round(LeasedBETEExempt[fn], modPPCalculate.Statics.PPRounding)) * Ratio), modPPCalculate.Statics.PPRounding), "#,###,##0"));
				tItemizedO += ItemizedOriginalCost[fn];
				// tItemizedC = tItemizedC + Round(ItemizedCalculatedCost(fn), PPRounding)
				tItemizedC += ItemizedCalculatedCost[fn];
				tLeasedO += LeasedOriginalCost[fn];
				// tLeasedC = tLeasedC + Round(LeasedCalculatedCost(fn), PPRounding)
				tLeasedC += LeasedCalculatedCost[fn];
				// tBETEExempt = tBETEExempt + Round(Round(ItemizedBETEExempt(fn), PPRounding) * Ratio, PPRounding)
				tBETEExempt += FCConvert.ToInt32(modGlobal.Round(((modGlobal.Round(ItemizedBETEExempt[fn], modPPCalculate.Statics.PPRounding) + modGlobal.Round(LeasedBETEExempt[fn], modPPCalculate.Statics.PPRounding)) * Ratio), modPPCalculate.Statics.PPRounding));
				vsDisplay.TextMatrix(10, 6, Strings.Format(vsDisplay.ValueMatrix(10, 6) + vsDisplay.ValueMatrix(fn, 6), "#,###"));
			}
			// fn
			vsDisplay.TextMatrix(10, CNSTCATEGORYCOLBETEEXEMPT, Strings.Format(tBETEExempt, "#,##0"));
			vsDisplay.TextMatrix(10, 1, Strings.Format(tItemizedO, "#,###"));
			// .TextMatrix(10, 2) = Format(Round(tItemizedC, PPRounding), "#,###")
			vsDisplay.TextMatrix(10, 2, Strings.Format(tItemizedC, "#,###,###"));
			vsDisplay.TextMatrix(10, 3, Strings.Format(tLeasedO, "#,###"));
			// .TextMatrix(10, 4) = Format(Round(tLeasedC, PPRounding), "#,###")
			vsDisplay.TextMatrix(10, 4, Strings.Format(tLeasedC, "#,###,###"));
			//FC:FINAL:DDU:#1317 - design part takes care about the height of grid
			//xxx += vsDisplay.RowHeight(0);
			//xxx += vsDisplay.RowHeight(1) * 10 + 75;
			//vsDisplay.HeightOriginal = xxx;
            //FC:FINAL:BSE:#3564 should not change location 
			//vsDisplay.Left = FCConvert.ToInt32((sstPPCalculate.Width - vsDisplay.Width) / 2.0);
		}

		private void frmPPCalculations_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				mnuExitNoSave_Click();
			}
		}

		private void frmPPCalculations_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			/*? On Error Resume Next  */
			try
			{
				if (KeyAscii == Keys.Escape)
				{
					KeyAscii = (Keys)0;
					modPPGN.Statics.CalcTag = true;
					frmPPCalculations.InstancePtr.Close();
				}
			}
			catch (Exception ex)
			{
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmPPCalculations_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPPCalculations properties;
			//frmPPCalculations.ScaleWidth	= 9300;
			//frmPPCalculations.ScaleHeight	= 7755;
			//frmPPCalculations.LinkTopic	= "Form1";
			//frmPPCalculations.LockControls	= true;
			//End Unmaped Properties
			clsDRWrapper clsTemp = new clsDRWrapper();
			SetupGrid();
			// Call GetWindowSize(Me)
			// Me.WindowState = vbMaximized
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			clsTemp.Execute("update ppmaster set deleted = 0 where isnull(deleted,2) = 2", "twpp0000.vb1");
			boolNotSaved = true;
		}

		private void frmPPCalculations_Resize(object sender, System.EventArgs e)
		{
			SetColumnWidths();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			// Call SaveWindowSize(Me)
			int intResponse;
			modPPGN.Statics.CalcTag = true;
			// If PrintDisplayRange Then
			datRatio = null;
			datMaster = null;
			datItemized = null;
			datLeased = null;
			datValuations = null;
			if (modPPCalculate.Statics.CalculateFromMaster == true)
			{
				// If boolNotSaved Then
				// intResponse = MsgBox("You have calculated but not saved the new amounts." & vbNewLine & "Do you wish to save them?", vbQuestion + vbYesNo, "Save Amounts?")
				// If intResponse = vbYes Then
				// Cancel = True
				// mnuSaveExit_Click
				// End If
				// End If
				// frmPPMaster.Show , MDIParent
				//FC:FINAL:MSH - i.issue #1274: reload form only if it doesn't exist
				//frmPPMaster newMaster = new frmPPMaster();
				//newMaster.Show(App.MainForm);
				//FC:FINAL:DDU:#i1270 - move show in formClosed and open using a flag
				//frmPPMaster.InstancePtr.Show(App.MainForm);
				modPPCalculate.Statics.ShowfrmPPMaster = true;
				modPPCalculate.Statics.CalculateFromMaster = false;
			}
			else
			{
				// MDIParent.ZOrder
				// MDIParent.Show
			}
			// End If
		}

		private void frmPPCalculations_FormClosed(object sender, FormClosedEventArgs e)
		{
			if (modPPCalculate.Statics.ShowfrmPPMaster)
			{
				frmPPMaster.InstancePtr.Show(App.MainForm);
				modPPCalculate.Statics.ShowfrmPPMaster = false;
			}
		}

		private void mnuExitNoSave_Click(object sender, System.EventArgs e)
		{
			modPPGN.Statics.CalcTag = true;
			frmPPCalculations.InstancePtr.Close();
		}

		public void mnuExitNoSave_Click()
		{
			mnuExitNoSave_Click(mnuExitNoSave, new System.EventArgs());
		}

		public void PrintHeader()
		{
			// Print #1, "  " & Me.lblWMuniName & String(30 - Len(Me.lblWMuniName), " ") & Me.lblName & String(33 - Len(Me.lblName), " ") & Format(Date, "MM/dd/yyyy")
			FCFileSystem.PrintLine(1, "  " + modGlobalConstants.Statics.MuniName + Strings.StrDup(25 - modGlobalConstants.Statics.MuniName.Length, " ") + "PERSONAL PROPERTY ASSESSMENTS" + Strings.StrDup(40 - "PERSONAL PROPERTY ASSESSMENTS".Length, " ") + Strings.Format(DateTime.Today, "MM/dd/yyyy"));
			FCFileSystem.PrintLine(1, "    Account " + this.lblAccount.Text + Strings.StrDup(25 - (10 + this.lblAccount.Text.Length), " ") + Strings.Trim(lblName.Text) + Strings.StrDup(40 - lblName.Text.Length, " "));
			// Print #1, "  " & String(30, " ") & datmaster.Fields("City") & ", " & datmaster.Fields("State") & " " & datmaster.Fields("zip")
			// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
			FCFileSystem.PrintLine(1, Strings.StrDup(27, " ") + FCConvert.ToString(Conversion.Val(datMaster.Get_Fields("streetnumber") + "")) + " " + Strings.Trim(datMaster.Get_Fields_String("street") + ""));
			FCFileSystem.PrintLine(1, "");
			FCFileSystem.PrintLine(1, "");
		}

		public void PrintLeased()
		{
			FCFileSystem.PrintLine(1, "  -------------------- L E A S E D  D E S C R I P T I O N --------------------");
			FCFileSystem.PrintLine(1, "");
			FCFileSystem.PrintLine(1, "                             ORG   LEASE      L1    RENT/       ");
			FCFileSystem.PrintLine(1, "LINE CD     DESCRIPTION      COST  DATE  #MOS P2    MONTH  %GD  FCTR    VALUE");
		}

		public void PrintItemized()
		{
			FCFileSystem.PrintLine(1, " ------------------- I T E M I Z E D  D E S C R I P T I O N -------------------");
			FCFileSystem.PrintLine(1, "");
			FCFileSystem.PrintLine(1, "                                       DEP    ");
			FCFileSystem.PrintLine(1, " LINE CD  DESCRIPTION             YEAR YRS SRO    COST   %GD FCT    VALUE");
		}

		public void mnuFPrint_Click(object sender, System.EventArgs e)
		{
			int intCounter;
			int intPageBreak;
			// Call rptCalculation.Show(vbModal)
			rptCalculation.InstancePtr.Start();
			return;
		}

		public void mnuFPrint_Click()
		{
			mnuFPrint_Click(mnuFPrint, new System.EventArgs());
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			// Put Override Code in Master
			clsDRWrapper clsSave = new clsDRWrapper();
			modGlobalFunctions.AddCYAEntry_26("PP", "Calculated and Saved", "Account " + FCConvert.ToString(modPPGN.Statics.CurrentAccount));
			clsSave.Execute("update status set calculated = '" + FCConvert.ToString(DateTime.Now) + "'", "twpp0000.vb1");
			modPPGN.Check_RB_Fields();
			strSQL = "SELECT * FROM PPMaster WHERE Account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount);
			datMaster.OpenRecordset(strSQL, modPPGN.strPPDatabase);
			datMaster.Edit();
			if (Check1.CheckState == CheckState.Checked)
			{
				datMaster.Set_Fields("ORCode", "Y");
			}
			else
			{
				datMaster.Set_Fields("ORCode", "N");
			}
			datMaster.Update();
			// 
			// Put Override Amount in Valuations
			strSQL = "SELECT * FROM PPValuations WHERE ValueKey = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount);
			datValuations.OpenRecordset(strSQL, modPPGN.strPPDatabase);
			datValuations.Edit();
			if (Conversion.Val(txtOverride.Text) > 0)
			{
				datValuations.Set_Fields("OverRideAmount", FCConvert.ToDouble(txtOverride.Text));
			}
			else
			{
				datValuations.Set_Fields("OverRideAmount", 0);
			}
			datValuations.Update();
			////Application.DoEvents();
			boolNotSaved = false;
			Close();
		}

		private void CheckOverride()
		{
			if (OverrideCode == "Y")
			{
				Check1.CheckState = CheckState.Checked;
				txtOverride.Enabled = true;
				txtOverride.ReadOnly = false;
				lblOverride.Visible = true;
			}
			if (OverrideValue > 0)
			{
				txtOverride.Text = Strings.Format(OverrideValue, "#,###");
			}
			else
			{
				if (Check1.CheckState == CheckState.Checked)
				{
					txtOverride.Text = "0";
				}
			}
		}

		private void txtOverride_TextChanged(object sender, System.EventArgs e)
		{
			string hold = "";
			if (txtOverride.Text.Length >= 13)
			{
				hold = Strings.Mid(txtOverride.Text, 1, 13);
				txtOverride.Text = Strings.Format(hold, "#,##0");
				txtOverride.SelectionStart = 15;
				return;
			}
			hold = txtOverride.Text;
			txtOverride.Text = Strings.Format(hold, "#,##0");
			txtOverride.SelectionStart = 15;
		}

		private void txtOverride_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// If Check1.Value = 1 Then
			// If Val(txtOverride.Text) = 0 Then
			// MsgBox "Do you really want an Override on this account with a Zero Value?", vbYesNo + vbQuestion, " "
			// End If
			// End If
		}

		private void SetupGrid()
		{
			vsItemized.Rows = 2;
			vsLeased.Rows = 2;
			//FC:FINAL:DDU:#1320 - corect row header height
			//vsItemized.RowHeight(0, FCConvert.ToInt32(2.8 * vsItemized.RowHeight(1)));
			//vsLeased.RowHeight(0, FCConvert.ToInt32(2.8 * vsLeased.RowHeight(1)));
			vsItemized.Rows = 1;
			vsLeased.Rows = 1;
			vsItemized.ColHidden(14, true);
			// vsItemized.Cols = 15
			// vsItemized.ColHidden(16) = True
			//FC:FINAL:CHN - issue #1320: Incorrect cells formatting. 
			// vsLeased.ColFormat(6, "0,000");
			// vsLeased.ColFormat(13, "0,000");
			vsLeased.ColHidden(14, true);
			if (vsLeased.Rows > 1)
			{
				vsLeased.Cell(FCGrid.CellPropertySettings.flexcpText, 1, 13, vsLeased.Rows - 1, 13, 0);
			}
			// 
			// vsLeased.RowHeight(0) = 750
			vsLeased.TextMatrix(0, 1, "Line");
			vsLeased.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 2, 0, 2, "R" + "\r\n" + "B");
			vsLeased.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 3, 0, 3, "C" + "\r\n" + "d");
			vsLeased.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 4, 0, 4, "Q" + "\r\n" + "t" + "\r\n" + "y");
			vsLeased.TextMatrix(0, 5, "Description");
			vsLeased.TextMatrix(0, 6, "Cost");
			vsLeased.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 7, 0, 7, "Lease" + "\r\n" + "Date");
			vsLeased.TextMatrix(0, 8, "MOS");
			vsLeased.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 9, 0, 9, "1=L" + "\r\n" + "2=P");
			vsLeased.TextMatrix(0, 10, "Monthly Rent");
			// vsLeased.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 10, 0, 10) = "Monthly" & vbNewLine & "Rent"
			vsLeased.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 11, 0, 11, "%" + "\r\n" + "Gd");
			vsLeased.TextMatrix(0, 12, "Fctr");
			vsLeased.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, 13, true);
			vsLeased.TextMatrix(0, 13, "Value");
			vsLeased.TextMatrix(0, CNSTITEMIZEDCOLBETEEXEMPT, "BETE" + "\r\n" + "Exempt");
			vsLeased.ColHidden(CNSTITEMIZEDCOLBETEYEAR, true);
			//FC:FINAL:CHN - issue #1320: Incorrect cells formatting. 
			// vsLeased.ColFormat(CNSTITEMIZEDCOLBETEYEAR, "0");
			// vsLeased.ColFormat(CNSTITEMIZEDCOLBETEEXEMPT, "0,000");
			//FC:FINAL:DDU:#1320 - corect column alignment
			//vsLeased.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, CNSTITEMIZEDCOLBETEEXEMPT, 4);
			// Itemized
			// vsItemized.RowHeight(0) = 750
			vsItemized.TextMatrix(0, 1, "Line");
			vsItemized.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 2, 0, 2, "R" + "\r\n" + "B");
			vsItemized.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 3, 0, 3, "C" + "\r\n" + "d");
			vsItemized.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 4, 0, 4, "Q" + "\r\n" + "t" + "\r\n" + "y");
			vsItemized.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 5, 0, 5, "Description");
			vsItemized.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 6, 0, 6, "Year");
			vsItemized.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 7, 0, 7, "DP" + "\r\n" + "YR");
			vsItemized.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 8, 0, 8, "S" + "\r\n" + "R" + "\r\n" + "O");
			vsItemized.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 9, 0, 9, "RC" + "\r\n" + "YR");
			vsItemized.TextMatrix(0, 10, "Cost");
			vsItemized.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 11, 0, 11, "% Gd");
			vsItemized.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 12, 0, 12, "Fctr");
			vsItemized.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, 13, true);
			vsItemized.TextMatrix(0, 13, "Value");
			vsItemized.TextMatrix(0, CNSTITEMIZEDCOLBETEEXEMPT, "BETE" + "\r\n" + "Exempt");
			//FC:FINAL:CHN - issue #1319: Incorrect cells formatting. 
			// vsItemized.ColFormat(10, "0,000");
			// vsItemized.ColFormat(13, "0,000");
			// vsItemized.ColFormat(CNSTITEMIZEDCOLBETEYEAR, "0");
			// vsItemized.ColFormat(CNSTITEMIZEDCOLBETEEXEMPT, "0,000");
			vsItemized.ColFormat(10, "n0");
			vsItemized.ColFormat(13, "n0");
			vsItemized.ColFormat(CNSTITEMIZEDCOLBETEEXEMPT, "n0");
			if (vsItemized.Rows > 1)
			{
				vsItemized.Cell(FCGrid.CellPropertySettings.flexcpText, 1, 13, vsItemized.Rows - 1, 13, 0);
			}
			vsItemized.ColHidden(CNSTITEMIZEDCOLBETEYEAR, true);
			// 
			SetCellAlignments();
			//FC:FINAL:DDU:#1320 - corect column alignment
			//vsLeased.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 13, 4);
			//vsItemized.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 13, 4);
			//vsItemized.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, CNSTITEMIZEDCOLBETEEXEMPT, 4);
		}

		private void FillGrid()
		{
			vsItemized.Rows = 1;
			datItemized.MoveFirst();
			int lngRow;
			lngRow = 0;
			while (!datItemized.EndOfFile())
			{
				vsItemized.Rows += 1;
				lngRow = vsItemized.Rows - 1;
				// TODO Get_Fields: Check the table for the column [Line] and replace with corresponding Get_Field method
				vsItemized.TextMatrix(lngRow, 1, FCConvert.ToString(datItemized.Get_Fields("Line")));
				// TODO Get_Fields: Check the table for the column [RB] and replace with corresponding Get_Field method
				vsItemized.TextMatrix(lngRow, 2, FCConvert.ToString(datItemized.Get_Fields("RB")));
				vsItemized.TextMatrix(lngRow, 3, FCConvert.ToString(datItemized.Get_Fields_String("cd")));
				// TODO Get_Fields: Check the table for the column [quantity] and replace with corresponding Get_Field method
				vsItemized.TextMatrix(lngRow, 4, FCConvert.ToString(datItemized.Get_Fields("quantity")));
				vsItemized.TextMatrix(lngRow, 5, FCConvert.ToString(datItemized.Get_Fields_String("description")));
				// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
				vsItemized.TextMatrix(lngRow, 6, FCConvert.ToString(datItemized.Get_Fields("Year")));
				vsItemized.TextMatrix(lngRow, 7, FCConvert.ToString(datItemized.Get_Fields_Int16("dpyr")));
				// TODO Get_Fields: Check the table for the column [sro] and replace with corresponding Get_Field method
				vsItemized.TextMatrix(lngRow, 8, FCConvert.ToString(datItemized.Get_Fields("sro")));
				vsItemized.TextMatrix(lngRow, 9, FCConvert.ToString(datItemized.Get_Fields_Int16("rcyr")));
				// TODO Get_Fields: Check the table for the column [cost] and replace with corresponding Get_Field method
				vsItemized.TextMatrix(lngRow, 10, FCConvert.ToString(datItemized.Get_Fields("cost")));
				vsItemized.TextMatrix(lngRow, 11, FCConvert.ToString(datItemized.Get_Fields_Int16("gd")));
				vsItemized.TextMatrix(lngRow, 12, FCConvert.ToString(datItemized.Get_Fields_Int16("Fctr")));
				// TODO Get_Fields: Check the table for the column [Value] and replace with corresponding Get_Field method
				vsItemized.TextMatrix(lngRow, 13, FCConvert.ToString(datItemized.Get_Fields("Value")));
				vsItemized.TextMatrix(lngRow, 14, FCConvert.ToString(datItemized.Get_Fields_Int32("id")));
				vsItemized.TextMatrix(lngRow, 15, FCConvert.ToString(datItemized.Get_Fields_Int32("beteexempt")));
				vsItemized.TextMatrix(lngRow, 16, FCConvert.ToString(datItemized.Get_Fields_Int32("exemptyear")));
				datItemized.MoveNext();
			}
			datLeased.MoveFirst();
			vsLeased.Rows = 1;
			while (!datLeased.EndOfFile())
			{
				vsLeased.Rows += 1;
				lngRow = vsLeased.Rows - 1;
				// TODO Get_Fields: Check the table for the column [Line] and replace with corresponding Get_Field method
				vsLeased.TextMatrix(lngRow, 1, FCConvert.ToString(datLeased.Get_Fields("Line")));
				// TODO Get_Fields: Check the table for the column [RB] and replace with corresponding Get_Field method
				vsLeased.TextMatrix(lngRow, 2, FCConvert.ToString(datLeased.Get_Fields("RB")));
				vsLeased.TextMatrix(lngRow, 3, FCConvert.ToString(datLeased.Get_Fields_String("CD")));
				// TODO Get_Fields: Check the table for the column [quantity] and replace with corresponding Get_Field method
				vsLeased.TextMatrix(lngRow, 4, FCConvert.ToString(datLeased.Get_Fields("quantity")));
				vsLeased.TextMatrix(lngRow, 5, FCConvert.ToString(datLeased.Get_Fields_String("Description")));
				// TODO Get_Fields: Check the table for the column [cost] and replace with corresponding Get_Field method
				vsLeased.TextMatrix(lngRow, 6, FCConvert.ToString(datLeased.Get_Fields("cost")));
				vsLeased.TextMatrix(lngRow, 7, FCConvert.ToString(datLeased.Get_Fields_String("leasedate")));
				vsLeased.TextMatrix(lngRow, 8, FCConvert.ToString(datLeased.Get_Fields_Int16("MOS")));
				vsLeased.TextMatrix(lngRow, 9, FCConvert.ToString(datLeased.Get_Fields_Int16("L1P2")));
				//FC:FINAL:CHN - issue #1320: Incorrect cells formatting. 
				// vsLeased.TextMatrix(lngRow, 10, FCConvert.ToString(datLeased.Get_Fields_Decimal("MONTHLYRENT")));
				vsLeased.TextMatrix(lngRow, 10, (datLeased.Get_Fields_Decimal("MONTHLYRENT").ToString("G29")));
				vsLeased.TextMatrix(lngRow, 11, FCConvert.ToString(datLeased.Get_Fields_Int16("DEPYRS")));
				vsLeased.TextMatrix(lngRow, 12, FCConvert.ToString(datLeased.Get_Fields_Int16("FCTR")));
				// TODO Get_Fields: Check the table for the column [VALUE] and replace with corresponding Get_Field method
				vsLeased.TextMatrix(lngRow, 13, FCConvert.ToString(datLeased.Get_Fields("VALUE")));
				vsLeased.TextMatrix(lngRow, 14, FCConvert.ToString(datLeased.Get_Fields_Int32("id")));
				vsLeased.TextMatrix(lngRow, 15, FCConvert.ToString(datLeased.Get_Fields_Int32("beteexempt")));
				vsLeased.TextMatrix(lngRow, 16, FCConvert.ToString(datLeased.Get_Fields_Int32("exemptyear")));
				datLeased.MoveNext();
			}
		}
	}
}
