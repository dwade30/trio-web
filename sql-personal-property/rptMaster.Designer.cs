﻿namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptMaster.
	/// </summary>
	partial class rptMaster
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptMaster));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddr1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddr2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPhone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtREAssoc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOpen1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblOpen1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOpen2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblOpen2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtExemption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCd1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCd2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTrancode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtBusinesscode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtStreetCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSQFT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtFullAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddr1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddr2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtREAssoc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOpen1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOpen2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCd1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCd2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTrancode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBusinesscode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStreetCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSQFT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFullAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAccount,
				this.txtName,
				this.txtAddr1,
				this.txtAddr2,
				this.txtCity,
				this.txtState,
				this.txtZip,
				this.Label2,
				this.Label3,
				this.txtPhone,
				this.txtLocation,
				this.Label4,
				this.Label5,
				this.txtREAssoc,
				this.txtOpen1,
				this.lblOpen1,
				this.txtOpen2,
				this.lblOpen2,
				this.txtValue,
				this.Label7,
				this.txtExemption,
				this.Label8,
				this.txtCd1,
				this.Label9,
				this.txtCd2,
				this.txtTrancode,
				this.Label10,
				this.txtBusinesscode,
				this.Label11,
				this.txtStreetCode,
				this.Label12,
				this.txtSQFT,
				this.Label13,
				this.txtFullAddress
			});
			this.Detail.Height = 3.25F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.txtPage,
				this.txtMuni,
				this.txtDate,
				this.txtTime
			});
			this.PageHeader.Height = 0.5520833F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.Label1.Text = "Personal Property Master Info Report";
			this.Label1.Top = 0.03125F;
			this.Label1.Width = 4.5F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 6.25F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtPage.Text = "Field1";
			this.txtPage.Top = 0.1875F;
			this.txtPage.Width = 1.1875F;
			// 
			// txtMuni
			// 
			this.txtMuni.Height = 0.1875F;
			this.txtMuni.Left = 0.0625F;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Style = "font-family: \'Tahoma\'";
			this.txtMuni.Text = "Field1";
			this.txtMuni.Top = 0.03125F;
			this.txtMuni.Width = 1.4375F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.HyperLink = null;
			this.txtDate.Left = 6.25F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtDate.Text = "Name";
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.1875F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0.0625F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'";
			this.txtTime.Text = "Field1";
			this.txtTime.Top = 0.21875F;
			this.txtTime.Width = 1.4375F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.1875F;
			this.txtAccount.Left = 0.9375F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "text-align: right";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0.09375F;
			this.txtAccount.Width = 0.8125F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1875F;
			this.txtName.Left = 0.0625F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "text-align: left";
			this.txtName.Text = null;
			this.txtName.Top = 0.75F;
			this.txtName.Width = 3.3125F;
			// 
			// txtAddr1
			// 
			this.txtAddr1.Height = 0.1875F;
			this.txtAddr1.Left = 0.0625F;
			this.txtAddr1.Name = "txtAddr1";
			this.txtAddr1.Style = "text-align: left";
			this.txtAddr1.Text = null;
			this.txtAddr1.Top = 0.9375F;
			this.txtAddr1.Width = 3.3125F;
			// 
			// txtAddr2
			// 
			this.txtAddr2.Height = 0.1875F;
			this.txtAddr2.Left = 0.0625F;
			this.txtAddr2.Name = "txtAddr2";
			this.txtAddr2.Style = "text-align: left";
			this.txtAddr2.Text = null;
			this.txtAddr2.Top = 1.125F;
			this.txtAddr2.Width = 3.3125F;
			// 
			// txtCity
			// 
			this.txtCity.Height = 0.1875F;
			this.txtCity.Left = 0.0625F;
			this.txtCity.Name = "txtCity";
			this.txtCity.Style = "text-align: left";
			this.txtCity.Text = null;
			this.txtCity.Top = 1.3125F;
			this.txtCity.Width = 1.5F;
			// 
			// txtState
			// 
			this.txtState.Height = 0.1875F;
			this.txtState.Left = 1.625F;
			this.txtState.Name = "txtState";
			this.txtState.Style = "text-align: left";
			this.txtState.Text = null;
			this.txtState.Top = 1.3125F;
			this.txtState.Width = 0.3125F;
			// 
			// txtZip
			// 
			this.txtZip.Height = 0.1875F;
			this.txtZip.Left = 2F;
			this.txtZip.Name = "txtZip";
			this.txtZip.Style = "text-align: left";
			this.txtZip.Text = null;
			this.txtZip.Top = 1.3125F;
			this.txtZip.Width = 1.3125F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.0625F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold; text-align: left";
			this.Label2.Text = "Account";
			this.Label2.Top = 0.09375F;
			this.Label2.Width = 0.75F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0.0625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold; text-align: left";
			this.Label3.Text = "Name & Address";
			this.Label3.Top = 0.5625F;
			this.Label3.Width = 3.3125F;
			// 
			// txtPhone
			// 
			this.txtPhone.Height = 0.1875F;
			this.txtPhone.Left = 0.0625F;
			this.txtPhone.Name = "txtPhone";
			this.txtPhone.Text = null;
			this.txtPhone.Top = 1.75F;
			this.txtPhone.Width = 1.5F;
			// 
			// txtLocation
			// 
			this.txtLocation.Height = 0.1875F;
			this.txtLocation.Left = 0.0625F;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Style = "text-align: left";
			this.txtLocation.Text = null;
			this.txtLocation.Top = 2.3125F;
			this.txtLocation.Width = 3.0625F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 0.0625F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-weight: bold; text-align: left";
			this.Label4.Text = "Location";
			this.Label4.Top = 2.125F;
			this.Label4.Width = 3.0625F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 0.0625F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-weight: bold; text-align: left";
			this.Label5.Text = "RE Assoc";
			this.Label5.Top = 0.28125F;
			this.Label5.Width = 0.75F;
			// 
			// txtREAssoc
			// 
			this.txtREAssoc.Height = 0.1875F;
			this.txtREAssoc.Left = 0.9375F;
			this.txtREAssoc.Name = "txtREAssoc";
			this.txtREAssoc.Style = "text-align: right";
			this.txtREAssoc.Text = null;
			this.txtREAssoc.Top = 0.28125F;
			this.txtREAssoc.Width = 0.8125F;
			// 
			// txtOpen1
			// 
			this.txtOpen1.Height = 0.1875F;
			this.txtOpen1.Left = 0.0625F;
			this.txtOpen1.Name = "txtOpen1";
			this.txtOpen1.Style = "text-align: left";
			this.txtOpen1.Text = null;
			this.txtOpen1.Top = 2.6875F;
			this.txtOpen1.Width = 2.8125F;
			// 
			// lblOpen1
			// 
			this.lblOpen1.Height = 0.1875F;
			this.lblOpen1.HyperLink = null;
			this.lblOpen1.Left = 0.0625F;
			this.lblOpen1.Name = "lblOpen1";
			this.lblOpen1.Style = "font-weight: bold; text-align: left";
			this.lblOpen1.Text = "Open 1";
			this.lblOpen1.Top = 2.5F;
			this.lblOpen1.Width = 2.8125F;
			// 
			// txtOpen2
			// 
			this.txtOpen2.Height = 0.1875F;
			this.txtOpen2.Left = 0.0625F;
			this.txtOpen2.Name = "txtOpen2";
			this.txtOpen2.Style = "text-align: left";
			this.txtOpen2.Text = null;
			this.txtOpen2.Top = 3.0625F;
			this.txtOpen2.Width = 2.8125F;
			// 
			// lblOpen2
			// 
			this.lblOpen2.Height = 0.1875F;
			this.lblOpen2.HyperLink = null;
			this.lblOpen2.Left = 0.0625F;
			this.lblOpen2.Name = "lblOpen2";
			this.lblOpen2.Style = "font-weight: bold; text-align: left";
			this.lblOpen2.Text = "Open 2";
			this.lblOpen2.Top = 2.875F;
			this.lblOpen2.Width = 2.8125F;
			// 
			// txtValue
			// 
			this.txtValue.Height = 0.1875F;
			this.txtValue.Left = 5.25F;
			this.txtValue.Name = "txtValue";
			this.txtValue.Style = "text-align: right";
			this.txtValue.Text = null;
			this.txtValue.Top = 0.5625F;
			this.txtValue.Width = 0.9375F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 4F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-weight: bold; text-align: left";
			this.Label7.Text = "Value";
			this.Label7.Top = 0.5625F;
			this.Label7.Width = 1F;
			// 
			// txtExemption
			// 
			this.txtExemption.Height = 0.1875F;
			this.txtExemption.Left = 5.25F;
			this.txtExemption.Name = "txtExemption";
			this.txtExemption.Style = "text-align: right";
			this.txtExemption.Text = null;
			this.txtExemption.Top = 0.75F;
			this.txtExemption.Width = 0.9375F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 4F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-weight: bold; text-align: left";
			this.Label8.Text = "Exemption";
			this.Label8.Top = 0.75F;
			this.Label8.Width = 1F;
			// 
			// txtCd1
			// 
			this.txtCd1.Height = 0.1875F;
			this.txtCd1.Left = 5.375F;
			this.txtCd1.Name = "txtCd1";
			this.txtCd1.Style = "text-align: right";
			this.txtCd1.Text = null;
			this.txtCd1.Top = 0.9375F;
			this.txtCd1.Width = 0.3125F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 4F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-weight: bold; text-align: left";
			this.Label9.Text = "Exempt Codes";
			this.Label9.Top = 0.9375F;
			this.Label9.Width = 1.1875F;
			// 
			// txtCd2
			// 
			this.txtCd2.Height = 0.1875F;
			this.txtCd2.Left = 5.875F;
			this.txtCd2.Name = "txtCd2";
			this.txtCd2.Style = "text-align: right";
			this.txtCd2.Text = null;
			this.txtCd2.Top = 0.9375F;
			this.txtCd2.Width = 0.3125F;
			// 
			// txtTrancode
			// 
			this.txtTrancode.Height = 0.1875F;
			this.txtTrancode.Left = 5.25F;
			this.txtTrancode.Name = "txtTrancode";
			this.txtTrancode.Style = "text-align: right";
			this.txtTrancode.Text = null;
			this.txtTrancode.Top = 1.125F;
			this.txtTrancode.Width = 0.9375F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 4F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-weight: bold; text-align: left";
			this.Label10.Text = "Tran Code";
			this.Label10.Top = 1.125F;
			this.Label10.Width = 1F;
			// 
			// txtBusinesscode
			// 
			this.txtBusinesscode.Height = 0.1875F;
			this.txtBusinesscode.Left = 5.25F;
			this.txtBusinesscode.Name = "txtBusinesscode";
			this.txtBusinesscode.Style = "text-align: right";
			this.txtBusinesscode.Text = null;
			this.txtBusinesscode.Top = 1.3125F;
			this.txtBusinesscode.Width = 0.9375F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 4F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-weight: bold; text-align: left";
			this.Label11.Text = "Business Code";
			this.Label11.Top = 1.3125F;
			this.Label11.Width = 1.1875F;
			// 
			// txtStreetCode
			// 
			this.txtStreetCode.Height = 0.1875F;
			this.txtStreetCode.Left = 5.25F;
			this.txtStreetCode.Name = "txtStreetCode";
			this.txtStreetCode.Style = "text-align: right";
			this.txtStreetCode.Text = null;
			this.txtStreetCode.Top = 1.5F;
			this.txtStreetCode.Width = 0.9375F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 4F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-weight: bold; text-align: left";
			this.Label12.Text = "Street Code";
			this.Label12.Top = 1.5F;
			this.Label12.Width = 1.1875F;
			// 
			// txtSQFT
			// 
			this.txtSQFT.Height = 0.1875F;
			this.txtSQFT.Left = 5.25F;
			this.txtSQFT.Name = "txtSQFT";
			this.txtSQFT.Style = "text-align: right";
			this.txtSQFT.Text = null;
			this.txtSQFT.Top = 1.6875F;
			this.txtSQFT.Width = 0.9375F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 4F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-weight: bold; text-align: left";
			this.Label13.Text = "Square Footage";
			this.Label13.Top = 1.6875F;
			this.Label13.Width = 1.1875F;
			// 
			// txtFullAddress
			// 
			this.txtFullAddress.Height = 0.75F;
			this.txtFullAddress.Left = 0.0625F;
			this.txtFullAddress.Name = "txtFullAddress";
			this.txtFullAddress.Style = "text-align: left";
			this.txtFullAddress.Text = null;
			this.txtFullAddress.Top = 0.9375F;
			this.txtFullAddress.Width = 3.3125F;
			// 
			// rptMaster
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.458333F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddr1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddr2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtREAssoc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOpen1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOpen2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCd1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCd2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTrancode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBusinesscode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStreetCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSQFT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFullAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddr1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddr2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhone;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtREAssoc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOpen1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOpen1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOpen2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOpen2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemption;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCd1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCd2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTrancode;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBusinesscode;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStreetCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSQFT;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFullAddress;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
