﻿namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptShortScreen.
	/// </summary>
	partial class rptShortScreen
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptShortScreen));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtZip4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTelephone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRELabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRE = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOpen1Desc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOpen2Desc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOpen1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOpen2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExempt1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExempt2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExempt1Desc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExempt2Desc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTranCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBusinessCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStreetCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat1Desc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat2Desc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat3Desc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat4Desc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat5Desc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat6Desc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat7Desc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat8Desc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat9Desc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalTaxable = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTaxRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZip4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTelephone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRELabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRE)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen1Desc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen2Desc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt1Desc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt2Desc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTranCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBusinessCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStreetCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat1Desc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat2Desc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat3Desc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat4Desc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat5Desc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat6Desc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat7Desc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat8Desc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat9Desc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalTaxable)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAddress1,
				this.txtAddress2,
				this.txtCity,
				this.txtState,
				this.txtZip,
				this.txtZip4,
				this.txtName,
				this.txtAccount,
				this.txtTelephone,
				this.txtRELabel,
				this.txtRE,
				this.txtOpen1Desc,
				this.txtOpen2Desc,
				this.txtOpen1,
				this.txtOpen2,
				this.Field3,
				this.txtExempt1,
				this.txtExempt2,
				this.txtExempt1Desc,
				this.txtExempt2Desc,
				this.Field4,
				this.Field5,
				this.Field6,
				this.txtTranCode,
				this.txtBusinessCode,
				this.txtStreetCode,
				this.txtCat1Desc,
				this.txtCat2Desc,
				this.txtCat3Desc,
				this.txtCat4Desc,
				this.txtCat5Desc,
				this.txtCat6Desc,
				this.txtCat7Desc,
				this.txtCat8Desc,
				this.txtCat9Desc,
				this.txtCat1,
				this.txtCat2,
				this.txtCat3,
				this.txtCat4,
				this.txtCat5,
				this.txtCat6,
				this.txtCat7,
				this.txtCat8,
				this.txtCat9,
				this.txtTotal,
				this.txtExemption,
				this.Field7,
				this.Field8,
				this.Field9,
				this.Field10,
				this.txtTotalTaxable,
				this.txtTaxRate,
				this.Field11,
				this.txtTax
			});
			this.Detail.Height = 4.229167F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMuniName,
				this.txtTime,
				this.txtDate,
				this.Field1,
				this.txtLocation,
				this.Field12,
				this.Field13
			});
			this.PageHeader.Height = 0.5104167F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.19F;
			this.txtMuniName.Left = 0F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'";
			this.txtMuniName.Text = "Field1";
			this.txtMuniName.Top = 0F;
			this.txtMuniName.Width = 1.625F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.19F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'";
			this.txtTime.Text = "Field1";
			this.txtTime.Top = 0.15625F;
			this.txtTime.Width = 1.625F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.19F;
			this.txtDate.Left = 5.8125F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtDate.Text = "Field1";
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.625F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.21875F;
			this.Field1.Left = 2F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.Field1.Text = "Billing Information";
			this.Field1.Top = 0F;
			this.Field1.Width = 3.5F;
			// 
			// txtLocation
			// 
			this.txtLocation.Height = 0.19F;
			this.txtLocation.Left = 2F;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Style = "font-family: \'Tahoma\'";
			this.txtLocation.Text = "Field3";
			this.txtLocation.Top = 0.21875F;
			this.txtLocation.Width = 3.5F;
			// 
			// Field12
			// 
			this.Field12.Height = 0.1875F;
			this.Field12.Left = 7.1875F;
			this.Field12.Name = "Field12";
			this.Field12.Style = "text-align: right";
			this.Field12.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
			this.Field12.Text = null;
			this.Field12.Top = 0.15625F;
			this.Field12.Width = 0.25F;
			// 
			// Field13
			// 
			this.Field13.Height = 0.1875F;
			this.Field13.Left = 6.5F;
			this.Field13.Name = "Field13";
			this.Field13.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field13.Text = "Page";
			this.Field13.Top = 0.15625F;
			this.Field13.Width = 0.6875F;
			// 
			// txtAddress1
			// 
			this.txtAddress1.Height = 0.53125F;
			this.txtAddress1.Left = 1F;
			this.txtAddress1.Name = "txtAddress1";
			this.txtAddress1.Style = "font-family: \'Tahoma\'";
			this.txtAddress1.Text = "Field2";
			this.txtAddress1.Top = 0.15625F;
			this.txtAddress1.Width = 3.5F;
			// 
			// txtAddress2
			// 
			this.txtAddress2.Height = 0.19F;
			this.txtAddress2.Left = 1F;
			this.txtAddress2.Name = "txtAddress2";
			this.txtAddress2.Style = "font-family: \'Tahoma\'";
			this.txtAddress2.Text = "Field3";
			this.txtAddress2.Top = 0.3125F;
			this.txtAddress2.Visible = false;
			this.txtAddress2.Width = 3.5F;
			// 
			// txtCity
			// 
			this.txtCity.Height = 0.19F;
			this.txtCity.Left = 1F;
			this.txtCity.Name = "txtCity";
			this.txtCity.Style = "font-family: \'Tahoma\'";
			this.txtCity.Text = "Field4";
			this.txtCity.Top = 0.46875F;
			this.txtCity.Visible = false;
			this.txtCity.Width = 1.875F;
			// 
			// txtState
			// 
			this.txtState.Height = 0.19F;
			this.txtState.Left = 2.9375F;
			this.txtState.Name = "txtState";
			this.txtState.Style = "font-family: \'Tahoma\'";
			this.txtState.Text = "Field5";
			this.txtState.Top = 0.46875F;
			this.txtState.Visible = false;
			this.txtState.Width = 0.4375F;
			// 
			// txtZip
			// 
			this.txtZip.Height = 0.19F;
			this.txtZip.Left = 3.4375F;
			this.txtZip.Name = "txtZip";
			this.txtZip.Style = "font-family: \'Tahoma\'";
			this.txtZip.Text = "Field6";
			this.txtZip.Top = 0.46875F;
			this.txtZip.Visible = false;
			this.txtZip.Width = 0.5625F;
			// 
			// txtZip4
			// 
			this.txtZip4.Height = 0.19F;
			this.txtZip4.Left = 4.0625F;
			this.txtZip4.Name = "txtZip4";
			this.txtZip4.Style = "font-family: \'Tahoma\'";
			this.txtZip4.Text = "Field7";
			this.txtZip4.Top = 0.46875F;
			this.txtZip4.Visible = false;
			this.txtZip4.Width = 0.4375F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.19F;
			this.txtName.Left = 1F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-family: \'Tahoma\'";
			this.txtName.Text = "Field2";
			this.txtName.Top = 0F;
			this.txtName.Width = 3.5F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.19F;
			this.txtAccount.Left = 0F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-family: \'Tahoma\'";
			this.txtAccount.Text = "Field7";
			this.txtAccount.Top = 0F;
			this.txtAccount.Width = 0.9375F;
			// 
			// txtTelephone
			// 
			this.txtTelephone.Height = 0.19F;
			this.txtTelephone.Left = 4.8125F;
			this.txtTelephone.Name = "txtTelephone";
			this.txtTelephone.Style = "font-family: \'Tahoma\'";
			this.txtTelephone.Text = "Field4";
			this.txtTelephone.Top = 0F;
			this.txtTelephone.Visible = false;
			this.txtTelephone.Width = 2F;
			// 
			// txtRELabel
			// 
			this.txtRELabel.Height = 0.19F;
			this.txtRELabel.Left = 4.8125F;
			this.txtRELabel.Name = "txtRELabel";
			this.txtRELabel.Style = "font-family: \'Tahoma\'";
			this.txtRELabel.Text = "Associated Real Estate";
			this.txtRELabel.Top = 0.1875F;
			this.txtRELabel.Width = 1.9375F;
			// 
			// txtRE
			// 
			this.txtRE.Height = 0.19F;
			this.txtRE.Left = 6.8125F;
			this.txtRE.Name = "txtRE";
			this.txtRE.Style = "font-family: \'Tahoma\'";
			this.txtRE.Text = "Field3";
			this.txtRE.Top = 0.1875F;
			this.txtRE.Width = 0.625F;
			// 
			// txtOpen1Desc
			// 
			this.txtOpen1Desc.CanGrow = false;
			this.txtOpen1Desc.Height = 0.19F;
			this.txtOpen1Desc.Left = 1F;
			this.txtOpen1Desc.MultiLine = false;
			this.txtOpen1Desc.Name = "txtOpen1Desc";
			this.txtOpen1Desc.Style = "font-family: \'Tahoma\'";
			this.txtOpen1Desc.Text = "Field3";
			this.txtOpen1Desc.Top = 0.75F;
			this.txtOpen1Desc.Width = 1.875F;
			// 
			// txtOpen2Desc
			// 
			this.txtOpen2Desc.CanGrow = false;
			this.txtOpen2Desc.Height = 0.19F;
			this.txtOpen2Desc.Left = 1F;
			this.txtOpen2Desc.MultiLine = false;
			this.txtOpen2Desc.Name = "txtOpen2Desc";
			this.txtOpen2Desc.Style = "font-family: \'Tahoma\'";
			this.txtOpen2Desc.Text = "Field4";
			this.txtOpen2Desc.Top = 0.90625F;
			this.txtOpen2Desc.Width = 1.875F;
			// 
			// txtOpen1
			// 
			this.txtOpen1.Height = 0.19F;
			this.txtOpen1.Left = 2.9375F;
			this.txtOpen1.Name = "txtOpen1";
			this.txtOpen1.Style = "font-family: \'Tahoma\'";
			this.txtOpen1.Text = "Field3";
			this.txtOpen1.Top = 0.75F;
			this.txtOpen1.Width = 1.5625F;
			// 
			// txtOpen2
			// 
			this.txtOpen2.Height = 0.19F;
			this.txtOpen2.Left = 2.9375F;
			this.txtOpen2.Name = "txtOpen2";
			this.txtOpen2.Style = "font-family: \'Tahoma\'";
			this.txtOpen2.Text = "Field4";
			this.txtOpen2.Top = 0.90625F;
			this.txtOpen2.Width = 1.5625F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.19F;
			this.Field3.Left = 0F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-family: \'Tahoma\'";
			this.Field3.Text = "Exempt Codes";
			this.Field3.Top = 1.1875F;
			this.Field3.Width = 1.1875F;
			// 
			// txtExempt1
			// 
			this.txtExempt1.Height = 0.19F;
			this.txtExempt1.Left = 1.3125F;
			this.txtExempt1.Name = "txtExempt1";
			this.txtExempt1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtExempt1.Text = "Field4";
			this.txtExempt1.Top = 1.1875F;
			this.txtExempt1.Width = 0.4375F;
			// 
			// txtExempt2
			// 
			this.txtExempt2.Height = 0.19F;
			this.txtExempt2.Left = 1.3125F;
			this.txtExempt2.Name = "txtExempt2";
			this.txtExempt2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtExempt2.Text = "Field5";
			this.txtExempt2.Top = 1.34375F;
			this.txtExempt2.Width = 0.4375F;
			// 
			// txtExempt1Desc
			// 
			this.txtExempt1Desc.CanGrow = false;
			this.txtExempt1Desc.Height = 0.19F;
			this.txtExempt1Desc.Left = 1.8125F;
			this.txtExempt1Desc.MultiLine = false;
			this.txtExempt1Desc.Name = "txtExempt1Desc";
			this.txtExempt1Desc.Style = "font-family: \'Tahoma\'";
			this.txtExempt1Desc.Text = "Field6";
			this.txtExempt1Desc.Top = 1.1875F;
			this.txtExempt1Desc.Width = 1.625F;
			// 
			// txtExempt2Desc
			// 
			this.txtExempt2Desc.CanGrow = false;
			this.txtExempt2Desc.Height = 0.19F;
			this.txtExempt2Desc.Left = 1.8125F;
			this.txtExempt2Desc.MultiLine = false;
			this.txtExempt2Desc.Name = "txtExempt2Desc";
			this.txtExempt2Desc.Style = "font-family: \'Tahoma\'";
			this.txtExempt2Desc.Text = "Field7";
			this.txtExempt2Desc.Top = 1.34375F;
			this.txtExempt2Desc.Width = 1.625F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.19F;
			this.Field4.Left = 3.75F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-family: \'Tahoma\'";
			this.Field4.Text = "Business Code";
			this.Field4.Top = 1.34375F;
			this.Field4.Width = 1.3125F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.19F;
			this.Field5.Left = 5.8125F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-family: \'Tahoma\'";
			this.Field5.Text = "Street Code";
			this.Field5.Top = 1.1875F;
			this.Field5.Width = 1.125F;
			// 
			// Field6
			// 
			this.Field6.Height = 0.19F;
			this.Field6.Left = 3.75F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-family: \'Tahoma\'";
			this.Field6.Text = "Tran Code";
			this.Field6.Top = 1.1875F;
			this.Field6.Width = 1.3125F;
			// 
			// txtTranCode
			// 
			this.txtTranCode.Height = 0.19F;
			this.txtTranCode.Left = 5.0625F;
			this.txtTranCode.Name = "txtTranCode";
			this.txtTranCode.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTranCode.Text = "Field7";
			this.txtTranCode.Top = 1.1875F;
			this.txtTranCode.Width = 0.5F;
			// 
			// txtBusinessCode
			// 
			this.txtBusinessCode.Height = 0.19F;
			this.txtBusinessCode.Left = 5.0625F;
			this.txtBusinessCode.Name = "txtBusinessCode";
			this.txtBusinessCode.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtBusinessCode.Text = "Field8";
			this.txtBusinessCode.Top = 1.34375F;
			this.txtBusinessCode.Width = 0.5F;
			// 
			// txtStreetCode
			// 
			this.txtStreetCode.Height = 0.19F;
			this.txtStreetCode.Left = 6.9375F;
			this.txtStreetCode.Name = "txtStreetCode";
			this.txtStreetCode.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtStreetCode.Text = "Field9";
			this.txtStreetCode.Top = 1.1875F;
			this.txtStreetCode.Width = 0.5F;
			// 
			// txtCat1Desc
			// 
			this.txtCat1Desc.Height = 0.19F;
			this.txtCat1Desc.Left = 2.1875F;
			this.txtCat1Desc.Name = "txtCat1Desc";
			this.txtCat1Desc.Style = "font-family: \'Tahoma\'";
			this.txtCat1Desc.Text = "Field7";
			this.txtCat1Desc.Top = 1.71875F;
			this.txtCat1Desc.Width = 2F;
			// 
			// txtCat2Desc
			// 
			this.txtCat2Desc.Height = 0.19F;
			this.txtCat2Desc.Left = 2.1875F;
			this.txtCat2Desc.Name = "txtCat2Desc";
			this.txtCat2Desc.Style = "font-family: \'Tahoma\'";
			this.txtCat2Desc.Text = "Field8";
			this.txtCat2Desc.Top = 1.875F;
			this.txtCat2Desc.Width = 2F;
			// 
			// txtCat3Desc
			// 
			this.txtCat3Desc.Height = 0.19F;
			this.txtCat3Desc.Left = 2.1875F;
			this.txtCat3Desc.Name = "txtCat3Desc";
			this.txtCat3Desc.Style = "font-family: \'Tahoma\'";
			this.txtCat3Desc.Text = "Field9";
			this.txtCat3Desc.Top = 2.03125F;
			this.txtCat3Desc.Width = 2F;
			// 
			// txtCat4Desc
			// 
			this.txtCat4Desc.Height = 0.19F;
			this.txtCat4Desc.Left = 2.1875F;
			this.txtCat4Desc.Name = "txtCat4Desc";
			this.txtCat4Desc.Style = "font-family: \'Tahoma\'";
			this.txtCat4Desc.Text = "Field10";
			this.txtCat4Desc.Top = 2.1875F;
			this.txtCat4Desc.Width = 2F;
			// 
			// txtCat5Desc
			// 
			this.txtCat5Desc.Height = 0.19F;
			this.txtCat5Desc.Left = 2.1875F;
			this.txtCat5Desc.Name = "txtCat5Desc";
			this.txtCat5Desc.Style = "font-family: \'Tahoma\'";
			this.txtCat5Desc.Text = "Field11";
			this.txtCat5Desc.Top = 2.34375F;
			this.txtCat5Desc.Width = 2F;
			// 
			// txtCat6Desc
			// 
			this.txtCat6Desc.Height = 0.19F;
			this.txtCat6Desc.Left = 2.1875F;
			this.txtCat6Desc.Name = "txtCat6Desc";
			this.txtCat6Desc.Style = "font-family: \'Tahoma\'";
			this.txtCat6Desc.Text = "Field12";
			this.txtCat6Desc.Top = 2.5F;
			this.txtCat6Desc.Width = 2F;
			// 
			// txtCat7Desc
			// 
			this.txtCat7Desc.Height = 0.19F;
			this.txtCat7Desc.Left = 2.1875F;
			this.txtCat7Desc.Name = "txtCat7Desc";
			this.txtCat7Desc.Style = "font-family: \'Tahoma\'";
			this.txtCat7Desc.Text = "Field13";
			this.txtCat7Desc.Top = 2.65625F;
			this.txtCat7Desc.Width = 2F;
			// 
			// txtCat8Desc
			// 
			this.txtCat8Desc.Height = 0.19F;
			this.txtCat8Desc.Left = 2.1875F;
			this.txtCat8Desc.Name = "txtCat8Desc";
			this.txtCat8Desc.Style = "font-family: \'Tahoma\'";
			this.txtCat8Desc.Text = "Field14";
			this.txtCat8Desc.Top = 2.8125F;
			this.txtCat8Desc.Width = 2F;
			// 
			// txtCat9Desc
			// 
			this.txtCat9Desc.Height = 0.19F;
			this.txtCat9Desc.Left = 2.1875F;
			this.txtCat9Desc.Name = "txtCat9Desc";
			this.txtCat9Desc.Style = "font-family: \'Tahoma\'";
			this.txtCat9Desc.Text = "Field15";
			this.txtCat9Desc.Top = 2.96875F;
			this.txtCat9Desc.Width = 2F;
			// 
			// txtCat1
			// 
			this.txtCat1.Height = 0.19F;
			this.txtCat1.Left = 4.25F;
			this.txtCat1.Name = "txtCat1";
			this.txtCat1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtCat1.Text = "Field7";
			this.txtCat1.Top = 1.71875F;
			this.txtCat1.Width = 1.0625F;
			// 
			// txtCat2
			// 
			this.txtCat2.Height = 0.19F;
			this.txtCat2.Left = 4.25F;
			this.txtCat2.Name = "txtCat2";
			this.txtCat2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtCat2.Text = "Field7";
			this.txtCat2.Top = 1.875F;
			this.txtCat2.Width = 1.0625F;
			// 
			// txtCat3
			// 
			this.txtCat3.Height = 0.19F;
			this.txtCat3.Left = 4.25F;
			this.txtCat3.Name = "txtCat3";
			this.txtCat3.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtCat3.Text = "Field9";
			this.txtCat3.Top = 2.03125F;
			this.txtCat3.Width = 1.0625F;
			// 
			// txtCat4
			// 
			this.txtCat4.Height = 0.19F;
			this.txtCat4.Left = 4.25F;
			this.txtCat4.Name = "txtCat4";
			this.txtCat4.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtCat4.Text = "Field7";
			this.txtCat4.Top = 2.1875F;
			this.txtCat4.Width = 1.0625F;
			// 
			// txtCat5
			// 
			this.txtCat5.Height = 0.19F;
			this.txtCat5.Left = 4.25F;
			this.txtCat5.Name = "txtCat5";
			this.txtCat5.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtCat5.Text = "Field11";
			this.txtCat5.Top = 2.34375F;
			this.txtCat5.Width = 1.0625F;
			// 
			// txtCat6
			// 
			this.txtCat6.Height = 0.19F;
			this.txtCat6.Left = 4.25F;
			this.txtCat6.Name = "txtCat6";
			this.txtCat6.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtCat6.Text = "Field7";
			this.txtCat6.Top = 2.5F;
			this.txtCat6.Width = 1.0625F;
			// 
			// txtCat7
			// 
			this.txtCat7.Height = 0.19F;
			this.txtCat7.Left = 4.25F;
			this.txtCat7.Name = "txtCat7";
			this.txtCat7.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtCat7.Text = "Field13";
			this.txtCat7.Top = 2.65625F;
			this.txtCat7.Width = 1.0625F;
			// 
			// txtCat8
			// 
			this.txtCat8.Height = 0.19F;
			this.txtCat8.Left = 4.25F;
			this.txtCat8.Name = "txtCat8";
			this.txtCat8.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtCat8.Text = "Field7";
			this.txtCat8.Top = 2.8125F;
			this.txtCat8.Width = 1.0625F;
			// 
			// txtCat9
			// 
			this.txtCat9.Height = 0.19F;
			this.txtCat9.Left = 4.25F;
			this.txtCat9.Name = "txtCat9";
			this.txtCat9.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtCat9.Text = "Field7";
			this.txtCat9.Top = 2.96875F;
			this.txtCat9.Width = 1.0625F;
			// 
			// txtTotal
			// 
			this.txtTotal.Height = 0.19F;
			this.txtTotal.Left = 4.25F;
			this.txtTotal.Name = "txtTotal";
			this.txtTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotal.Text = "Field7";
			this.txtTotal.Top = 3.3125F;
			this.txtTotal.Width = 1.0625F;
			// 
			// txtExemption
			// 
			this.txtExemption.Height = 0.19F;
			this.txtExemption.Left = 4.25F;
			this.txtExemption.Name = "txtExemption";
			this.txtExemption.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtExemption.Text = "Field7";
			this.txtExemption.Top = 3.46875F;
			this.txtExemption.Width = 1.0625F;
			// 
			// Field7
			// 
			this.Field7.Height = 0.19F;
			this.Field7.Left = 2.9375F;
			this.Field7.Name = "Field7";
			this.Field7.Style = "font-family: \'Tahoma\'";
			this.Field7.Text = "Total";
			this.Field7.Top = 3.3125F;
			this.Field7.Width = 1.25F;
			// 
			// Field8
			// 
			this.Field8.Height = 0.19F;
			this.Field8.Left = 2.9375F;
			this.Field8.Name = "Field8";
			this.Field8.Style = "font-family: \'Tahoma\'";
			this.Field8.Text = "Exemption";
			this.Field8.Top = 3.46875F;
			this.Field8.Width = 1.25F;
			// 
			// Field9
			// 
			this.Field9.Height = 0.19F;
			this.Field9.Left = 2.9375F;
			this.Field9.Name = "Field9";
			this.Field9.Style = "font-family: \'Tahoma\'";
			this.Field9.Text = "Total Taxable";
			this.Field9.Top = 3.625F;
			this.Field9.Width = 1.25F;
			// 
			// Field10
			// 
			this.Field10.Height = 0.19F;
			this.Field10.Left = 2.9375F;
			this.Field10.Name = "Field10";
			this.Field10.Style = "font-family: \'Tahoma\'";
			this.Field10.Text = "Tax Rate";
			this.Field10.Top = 3.78125F;
			this.Field10.Width = 1.25F;
			// 
			// txtTotalTaxable
			// 
			this.txtTotalTaxable.Height = 0.19F;
			this.txtTotalTaxable.Left = 4.25F;
			this.txtTotalTaxable.Name = "txtTotalTaxable";
			this.txtTotalTaxable.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotalTaxable.Text = "Field7";
			this.txtTotalTaxable.Top = 3.625F;
			this.txtTotalTaxable.Width = 1.0625F;
			// 
			// txtTaxRate
			// 
			this.txtTaxRate.Height = 0.19F;
			this.txtTaxRate.Left = 4.25F;
			this.txtTaxRate.Name = "txtTaxRate";
			this.txtTaxRate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTaxRate.Text = "Field7";
			this.txtTaxRate.Top = 3.78125F;
			this.txtTaxRate.Width = 1.0625F;
			// 
			// Field11
			// 
			this.Field11.Height = 0.19F;
			this.Field11.Left = 2.9375F;
			this.Field11.Name = "Field11";
			this.Field11.Style = "font-family: \'Tahoma\'";
			this.Field11.Text = "Tax";
			this.Field11.Top = 3.9375F;
			this.Field11.Width = 1.25F;
			// 
			// txtTax
			// 
			this.txtTax.Height = 0.19F;
			this.txtTax.Left = 4.25F;
			this.txtTax.Name = "txtTax";
			this.txtTax.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTax.Text = "Field7";
			this.txtTax.Top = 3.9375F;
			this.txtTax.Width = 1.0625F;
			// 
			// rptShortScreen
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.479167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZip4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTelephone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRELabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRE)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen1Desc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen2Desc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt1Desc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt2Desc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTranCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBusinessCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStreetCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat1Desc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat2Desc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat3Desc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat4Desc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat5Desc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat6Desc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat7Desc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat8Desc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat9Desc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalTaxable)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTelephone;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRELabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRE;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOpen1Desc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOpen2Desc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOpen1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOpen2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExempt1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExempt2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExempt1Desc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExempt2Desc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTranCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBusinessCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStreetCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat1Desc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat2Desc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat3Desc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat4Desc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat5Desc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat6Desc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat7Desc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat8Desc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat9Desc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalTaxable;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxRate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTax;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field13;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
