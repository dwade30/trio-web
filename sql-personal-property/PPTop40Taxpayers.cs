﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmPPTop40Taxpayers.
	/// </summary>
	public partial class frmPPTop40Taxpayers : BaseForm
	{
		public frmPPTop40Taxpayers()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Label2 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.Label2.AddControlArrayElement(Label2_1, 1);
			this.Label2.AddControlArrayElement(Label2_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPPTop40Taxpayers InstancePtr
		{
			get
			{
				return (frmPPTop40Taxpayers)Sys.GetInstance(typeof(frmPPTop40Taxpayers));
			}
		}

		protected frmPPTop40Taxpayers _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		/// <summary>
		/// Used for Account Sequence
		/// </summary>
		int aAccount;
		string aName = "";
		int aValuation;
		/// <summary>
		/// Used for Name Sequence
		/// </summary>
		int[] nAccount = null;
		string[] nName = null;
		int[] nValuation = null;
		int[] nCounter = null;
		/// <summary>
		///
		/// </summary>
		int t40;
		/// <summary>
		/// used as array counter
		/// </summary>
		string tSequence = "";
		string strSQL = "";
		int cntr;
		int LineCount;
		int PageCount;

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			if (Conversion.Val(txtQuantity.Text) != 0)
			{
				t40 = FCConvert.ToInt32(Math.Round(Conversion.Val(txtQuantity.Text)));
			}
			else
			{
				MessageBox.Show("You must enter a valid number in the Top Valuations box.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtQuantity.Focus();
				return;
			}
			if (cmbAccount.SelectedIndex == 1)
				tSequence = "N";
			if (cmbAccount.SelectedIndex == 0)
				tSequence = "A";
			if (tSequence == "")
			{
				MessageBox.Show("You must select a Print Order", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			rptHighestAssess.InstancePtr.Init(cmbAccount.SelectedIndex == 0, t40, true);
			frmPPTop40Taxpayers.InstancePtr.Close();
		}

		public void cmdPrint_Click()
		{
			cmdPrint_Click(cmdPrint, new System.EventArgs());
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			frmPPTop40Taxpayers.InstancePtr.Close();
		}

		public void cmdQuit_Click()
		{
			cmdQuit_Click(cmdQuit, new System.EventArgs());
		}

		private void frmPPTop40Taxpayers_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmPPTop40Taxpayers_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				frmPPTop40Taxpayers.InstancePtr.Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmPPTop40Taxpayers_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPPTop40Taxpayers properties;
			//frmPPTop40Taxpayers.ScaleWidth	= 8880;
			//frmPPTop40Taxpayers.ScaleHeight	= 7155;
			//frmPPTop40Taxpayers.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			modGlobal.SaveWindowSize(this);
			// menuPPPrint.Show
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdQuit_Click();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			cmdPrint_Click();
		}

		private void cmbAccount_SelectedIndexChanged(object sender, EventArgs e)
		{
			Label2_0.Visible = cmbAccount.SelectedIndex == 0;
			Label2_1.Visible = cmbAccount.SelectedIndex == 1;
		}
	}
}
