﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmBETEApplication.
	/// </summary>
	partial class frmBETEApplication : BaseForm
	{
		public fecherFoundation.FCComboBox cmbBusiness;
		public fecherFoundation.FCLabel lblBusiness;
		public fecherFoundation.FCComboBox cmbOwner;
		public fecherFoundation.FCLabel lblOwner;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtMessage;
		public fecherFoundation.FCTextBox txtMessage_2;
		public fecherFoundation.FCTextBox txtMessage_7;
		public fecherFoundation.FCTextBox txtMessage_6;
		public fecherFoundation.FCTextBox txtMessage_5;
		public fecherFoundation.FCTextBox txtMessage_4;
		public fecherFoundation.FCTextBox txtMessage_3;
		public fecherFoundation.FCTextBox txtMessage_0;
		public fecherFoundation.FCTextBox txtMessage_1;
		public fecherFoundation.FCCheckBox chkPendingOnly;
		public fecherFoundation.FCCheckBox chkIncludeOther;
		public fecherFoundation.FCTextBox txtEffectiveTaxYear;
		public fecherFoundation.FCTextBox txtTaxYear;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBETEApplication));
			this.cmbBusiness = new fecherFoundation.FCComboBox();
			this.lblBusiness = new fecherFoundation.FCLabel();
			this.cmbOwner = new fecherFoundation.FCComboBox();
			this.lblOwner = new fecherFoundation.FCLabel();
			this.txtMessage_2 = new fecherFoundation.FCTextBox();
			this.txtMessage_7 = new fecherFoundation.FCTextBox();
			this.txtMessage_6 = new fecherFoundation.FCTextBox();
			this.txtMessage_5 = new fecherFoundation.FCTextBox();
			this.txtMessage_4 = new fecherFoundation.FCTextBox();
			this.txtMessage_3 = new fecherFoundation.FCTextBox();
			this.txtMessage_0 = new fecherFoundation.FCTextBox();
			this.txtMessage_1 = new fecherFoundation.FCTextBox();
			this.chkPendingOnly = new fecherFoundation.FCCheckBox();
			this.chkIncludeOther = new fecherFoundation.FCCheckBox();
			this.txtEffectiveTaxYear = new fecherFoundation.FCTextBox();
			this.txtTaxYear = new fecherFoundation.FCTextBox();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.btnSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkPendingOnly)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIncludeOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 481);
			this.BottomPanel.Size = new System.Drawing.Size(679, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtMessage_2);
			this.ClientArea.Controls.Add(this.txtMessage_7);
			this.ClientArea.Controls.Add(this.txtMessage_6);
			this.ClientArea.Controls.Add(this.txtMessage_5);
			this.ClientArea.Controls.Add(this.txtMessage_4);
			this.ClientArea.Controls.Add(this.txtMessage_3);
			this.ClientArea.Controls.Add(this.txtMessage_0);
			this.ClientArea.Controls.Add(this.txtMessage_1);
			this.ClientArea.Controls.Add(this.chkPendingOnly);
			this.ClientArea.Controls.Add(this.chkIncludeOther);
			this.ClientArea.Controls.Add(this.txtEffectiveTaxYear);
			this.ClientArea.Controls.Add(this.cmbBusiness);
			this.ClientArea.Controls.Add(this.lblBusiness);
			this.ClientArea.Controls.Add(this.cmbOwner);
			this.ClientArea.Controls.Add(this.lblOwner);
			this.ClientArea.Controls.Add(this.txtTaxYear);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(679, 421);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(679, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(204, 30);
			this.HeaderText.Text = "BETE Application";
			// 
			// cmbBusiness
			// 
			this.cmbBusiness.Items.AddRange(new object[] {
            "Name",
            "Open1",
            "Open 2",
            "None"});
			this.cmbBusiness.Location = new System.Drawing.Point(228, 210);
			this.cmbBusiness.Name = "cmbBusiness";
			this.cmbBusiness.TabIndex = 3;
			this.cmbBusiness.Text = "None";
			// 
			// lblBusiness
			// 
			this.lblBusiness.AutoSize = true;
			this.lblBusiness.Location = new System.Drawing.Point(30, 224);
			this.lblBusiness.Name = "lblBusiness";
			this.lblBusiness.Size = new System.Drawing.Size(76, 16);
			this.lblBusiness.TabIndex = 28;
			this.lblBusiness.Text = "BUSINESS";
			// 
			// cmbOwner
			// 
			this.cmbOwner.Items.AddRange(new object[] {
            "Name",
            "Open1",
            "Open 2",
            "None"});
			this.cmbOwner.Location = new System.Drawing.Point(228, 150);
			this.cmbOwner.Name = "cmbOwner";
			this.cmbOwner.TabIndex = 2;
			this.cmbOwner.Text = "Name";
			// 
			// lblOwner
			// 
			this.lblOwner.AutoSize = true;
			this.lblOwner.Location = new System.Drawing.Point(30, 164);
			this.lblOwner.Name = "lblOwner";
			this.lblOwner.Size = new System.Drawing.Size(59, 16);
			this.lblOwner.TabIndex = 30;
			this.lblOwner.Text = "OWNER";
			// 
			// txtMessage_2
			// 
			this.txtMessage_2.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage_2.Location = new System.Drawing.Point(30, 519);
			this.txtMessage_2.MaxLength = 41;
			this.txtMessage_2.Name = "txtMessage_2";
			this.txtMessage_2.Size = new System.Drawing.Size(322, 40);
			this.txtMessage_2.TabIndex = 26;
			this.txtMessage_2.Visible = false;
			this.txtMessage_2.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMessage_KeyDown);
			// 
			// txtMessage_7
			// 
			this.txtMessage_7.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage_7.Location = new System.Drawing.Point(30, 819);
			this.txtMessage_7.MaxLength = 41;
			this.txtMessage_7.Name = "txtMessage_7";
			this.txtMessage_7.Size = new System.Drawing.Size(322, 40);
			this.txtMessage_7.TabIndex = 25;
			this.txtMessage_7.Visible = false;
			this.txtMessage_7.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMessage_KeyDown);
			// 
			// txtMessage_6
			// 
			this.txtMessage_6.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage_6.Location = new System.Drawing.Point(30, 759);
			this.txtMessage_6.MaxLength = 41;
			this.txtMessage_6.Name = "txtMessage_6";
			this.txtMessage_6.Size = new System.Drawing.Size(322, 40);
			this.txtMessage_6.TabIndex = 24;
			this.txtMessage_6.Visible = false;
			this.txtMessage_6.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMessage_KeyDown);
			// 
			// txtMessage_5
			// 
			this.txtMessage_5.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage_5.Location = new System.Drawing.Point(30, 699);
			this.txtMessage_5.MaxLength = 41;
			this.txtMessage_5.Name = "txtMessage_5";
			this.txtMessage_5.Size = new System.Drawing.Size(322, 40);
			this.txtMessage_5.TabIndex = 23;
			this.txtMessage_5.Visible = false;
			this.txtMessage_5.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMessage_KeyDown);
			// 
			// txtMessage_4
			// 
			this.txtMessage_4.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage_4.Location = new System.Drawing.Point(30, 639);
			this.txtMessage_4.MaxLength = 41;
			this.txtMessage_4.Name = "txtMessage_4";
			this.txtMessage_4.Size = new System.Drawing.Size(322, 40);
			this.txtMessage_4.TabIndex = 22;
			this.txtMessage_4.Visible = false;
			this.txtMessage_4.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMessage_KeyDown);
			// 
			// txtMessage_3
			// 
			this.txtMessage_3.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage_3.Location = new System.Drawing.Point(30, 579);
			this.txtMessage_3.MaxLength = 41;
			this.txtMessage_3.Name = "txtMessage_3";
			this.txtMessage_3.Size = new System.Drawing.Size(322, 40);
			this.txtMessage_3.TabIndex = 21;
			this.txtMessage_3.Visible = false;
			this.txtMessage_3.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMessage_KeyDown);
			// 
			// txtMessage_0
			// 
			this.txtMessage_0.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage_0.Location = new System.Drawing.Point(30, 399);
			this.txtMessage_0.MaxLength = 41;
			this.txtMessage_0.Name = "txtMessage_0";
			this.txtMessage_0.Size = new System.Drawing.Size(322, 40);
			this.txtMessage_0.TabIndex = 19;
			this.txtMessage_0.Visible = false;
			this.txtMessage_0.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMessage_KeyDown);
			// 
			// txtMessage_1
			// 
			this.txtMessage_1.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage_1.Location = new System.Drawing.Point(30, 459);
			this.txtMessage_1.MaxLength = 41;
			this.txtMessage_1.Name = "txtMessage_1";
			this.txtMessage_1.Size = new System.Drawing.Size(322, 40);
			this.txtMessage_1.TabIndex = 20;
			this.txtMessage_1.Visible = false;
			this.txtMessage_1.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMessage_KeyDown);
			// 
			// chkPendingOnly
			// 
			this.chkPendingOnly.Location = new System.Drawing.Point(30, 317);
			this.chkPendingOnly.Name = "chkPendingOnly";
			this.chkPendingOnly.Size = new System.Drawing.Size(272, 23);
			this.chkPendingOnly.TabIndex = 17;
			this.chkPendingOnly.Text = "Include only accounts with pending BETE";
			this.chkPendingOnly.CheckedChanged += new System.EventHandler(this.chkPendingOnly_CheckedChanged);
			// 
			// chkIncludeOther
			// 
			this.chkIncludeOther.Checked = true;
			this.chkIncludeOther.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkIncludeOther.Location = new System.Drawing.Point(30, 270);
			this.chkIncludeOther.Name = "chkIncludeOther";
			this.chkIncludeOther.Size = new System.Drawing.Size(330, 23);
			this.chkIncludeOther.TabIndex = 16;
			this.chkIncludeOther.Text = "Include pending items and any BETE exempt years";
			this.chkIncludeOther.CheckedChanged += new System.EventHandler(this.chkIncludeOther_CheckedChanged);
			// 
			// txtEffectiveTaxYear
			// 
			this.txtEffectiveTaxYear.BackColor = System.Drawing.SystemColors.Window;
			this.txtEffectiveTaxYear.Location = new System.Drawing.Point(228, 90);
			this.txtEffectiveTaxYear.Name = "txtEffectiveTaxYear";
			this.txtEffectiveTaxYear.Size = new System.Drawing.Size(94, 40);
			this.txtEffectiveTaxYear.TabIndex = 1;
			// 
			// txtTaxYear
			// 
			this.txtTaxYear.BackColor = System.Drawing.SystemColors.Window;
			this.txtTaxYear.Location = new System.Drawing.Point(228, 30);
			this.txtTaxYear.Name = "txtTaxYear";
			this.txtTaxYear.Size = new System.Drawing.Size(94, 40);
			this.txtTaxYear.TabIndex = 31;
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(30, 364);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(245, 15);
			this.Label3.TabIndex = 18;
			this.Label3.Text = "MESSAGE";
			this.Label3.Visible = false;
			// 
			// Label2
			// 
			this.Label2.AutoSize = true;
			this.Label2.Location = new System.Drawing.Point(30, 104);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(151, 16);
			this.Label2.TabIndex = 15;
			this.Label2.Text = "EFFECTIVE TAX YEAR";
			// 
			// Label1
			// 
			this.Label1.AutoSize = true;
			this.Label1.Location = new System.Drawing.Point(30, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(163, 16);
			this.Label1.TabIndex = 2;
			this.Label1.Text = "FOR TAX YEAR APRIL 1,";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.Seperator,
            this.mnuExit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuSaveContinue
			// 
			this.mnuSaveContinue.Index = 0;
			this.mnuSaveContinue.Name = "mnuSaveContinue";
			this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveContinue.Text = "Save & Continue";
			this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// btnSave
			// 
			this.btnSave.AppearanceKey = "acceptButton";
			this.btnSave.Location = new System.Drawing.Point(252, 29);
			this.btnSave.Name = "btnSave";
			this.btnSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnSave.Size = new System.Drawing.Size(100, 48);
			this.btnSave.Text = "Save";
			this.btnSave.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// frmBETEApplication
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(679, 589);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmBETEApplication";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "BETE Application";
			this.Load += new System.EventHandler(this.frmBETEApplication_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmBETEApplication_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkPendingOnly)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIncludeOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnSave)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton btnSave;
	}
}
