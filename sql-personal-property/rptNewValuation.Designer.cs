﻿namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptNewValuation.
	/// </summary>
	partial class rptNewValuation
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptNewValuation));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniname = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNewValuation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmountReplacing = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNetNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtTotalNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalReplacing = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalNet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCat1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotNewCat1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotReplCat1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotNetCat1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCat2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotNewCat2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotReplCat2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotNetCat2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCat3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotNewCat3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotReplCat3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotNetCat3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCat4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotNewCat4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotReplCat4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotNetCat4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCat5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotNewCat5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotReplCat5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotNetCat5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCat6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotNewCat6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotReplCat6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotNetCat6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCat7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotNewCat7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotReplCat7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotNetCat7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCat8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotNewCat8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotReplCat8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotNetCat8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCat9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotNewCat9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotReplCat9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotNetCat9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewValuation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmountReplacing)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNetNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalReplacing)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalNet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNewCat1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotReplCat1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNetCat1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNewCat2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotReplCat2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNetCat2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNewCat3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotReplCat3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNetCat3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNewCat4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotReplCat4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNetCat4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNewCat5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotReplCat5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNetCat5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNewCat6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotReplCat6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNetCat6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNewCat7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotReplCat7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNetCat7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNewCat8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotReplCat8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNetCat8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNewCat9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotReplCat9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNetCat9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAccount,
				this.txtName,
				this.txtNewValuation,
				this.txtAmountReplacing,
				this.txtNetNew
			});
			this.Detail.Height = 0.2083333F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label6,
				this.Line1,
				this.txtTotalNew,
				this.txtTotalReplacing,
				this.txtTotalNet,
				this.Label7,
				this.txtCount,
				this.lblCat1,
				this.txtTotNewCat1,
				this.txtTotReplCat1,
				this.txtTotNetCat1,
				this.lblCat2,
				this.txtTotNewCat2,
				this.txtTotReplCat2,
				this.txtTotNetCat2,
				this.lblCat3,
				this.txtTotNewCat3,
				this.txtTotReplCat3,
				this.txtTotNetCat3,
				this.lblCat4,
				this.txtTotNewCat4,
				this.txtTotReplCat4,
				this.txtTotNetCat4,
				this.lblCat5,
				this.txtTotNewCat5,
				this.txtTotReplCat5,
				this.txtTotNetCat5,
				this.lblCat6,
				this.txtTotNewCat6,
				this.txtTotReplCat6,
				this.txtTotNetCat6,
				this.lblCat7,
				this.txtTotNewCat7,
				this.txtTotReplCat7,
				this.txtTotNetCat7,
				this.lblCat8,
				this.txtTotNewCat8,
				this.txtTotReplCat8,
				this.txtTotNetCat8,
				this.lblCat9,
				this.txtTotNewCat9,
				this.txtTotReplCat9,
				this.txtTotNetCat9
			});
			this.ReportFooter.Height = 2.125F;
			this.ReportFooter.KeepTogether = true;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblTitle,
				this.lblMuniname,
				this.lblTime,
				this.lblDate,
				this.lblPage,
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5
			});
			this.PageHeader.Height = 0.9166667F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.25F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 2.25F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.lblTitle.Text = "New Valuation for Year ";
			this.lblTitle.Top = 0F;
			this.lblTitle.Width = 3F;
			// 
			// lblMuniname
			// 
			this.lblMuniname.Height = 0.1666667F;
			this.lblMuniname.HyperLink = null;
			this.lblMuniname.Left = 0F;
			this.lblMuniname.Name = "lblMuniname";
			this.lblMuniname.Style = "";
			this.lblMuniname.Text = null;
			this.lblMuniname.Top = 0F;
			this.lblMuniname.Width = 2.25F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1666667F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.1666667F;
			this.lblTime.Width = 2.25F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1666667F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 5.666667F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "text-align: right";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1.75F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1666667F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 5.666667F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "text-align: right";
			this.lblPage.Text = null;
			this.lblPage.Top = 0.1666667F;
			this.lblPage.Width = 1.75F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1666667F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-weight: bold; text-align: right";
			this.Label1.Text = "Account";
			this.Label1.Top = 0.6666667F;
			this.Label1.Width = 0.6666667F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1666667F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.75F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold; text-align: left";
			this.Label2.Text = "Name";
			this.Label2.Top = 0.6666667F;
			this.Label2.Width = 2.333333F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.3333333F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 3.166667F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold; text-align: center";
			this.Label3.Text = "New Valuation";
			this.Label3.Top = 0.5F;
			this.Label3.Width = 0.9166667F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.4166667F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 4.166667F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-weight: bold; text-align: center";
			this.Label4.Text = "Amount Replacing";
			this.Label4.Top = 0.5F;
			this.Label4.Width = 0.9166667F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1666667F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 5.166667F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-weight: bold; text-align: center";
			this.Label5.Text = "Net New";
			this.Label5.Top = 0.6666667F;
			this.Label5.Width = 0.9166667F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.1666667F;
			this.txtAccount.Left = 0F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "text-align: right";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0F;
			this.txtAccount.Width = 0.6666667F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1666667F;
			this.txtName.Left = 0.75F;
			this.txtName.Name = "txtName";
			this.txtName.Text = null;
			this.txtName.Top = 0F;
			this.txtName.Width = 2.333333F;
			// 
			// txtNewValuation
			// 
			this.txtNewValuation.Height = 0.1666667F;
			this.txtNewValuation.Left = 3.166667F;
			this.txtNewValuation.Name = "txtNewValuation";
			this.txtNewValuation.Style = "text-align: right";
			this.txtNewValuation.Text = null;
			this.txtNewValuation.Top = 0F;
			this.txtNewValuation.Width = 0.9166667F;
			// 
			// txtAmountReplacing
			// 
			this.txtAmountReplacing.Height = 0.1666667F;
			this.txtAmountReplacing.Left = 4.166667F;
			this.txtAmountReplacing.Name = "txtAmountReplacing";
			this.txtAmountReplacing.Style = "text-align: right";
			this.txtAmountReplacing.Text = null;
			this.txtAmountReplacing.Top = 0F;
			this.txtAmountReplacing.Width = 0.9166667F;
			// 
			// txtNetNew
			// 
			this.txtNetNew.Height = 0.1666667F;
			this.txtNetNew.Left = 5.166667F;
			this.txtNetNew.Name = "txtNetNew";
			this.txtNetNew.Style = "text-align: right";
			this.txtNetNew.Text = null;
			this.txtNetNew.Top = 0F;
			this.txtNetNew.Width = 0.9166667F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1666667F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 2.416667F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-weight: bold; text-align: right";
			this.Label6.Text = "Total";
			this.Label6.Top = 1.916667F;
			this.Label6.Width = 0.6666667F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.08333334F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.08333334F;
			this.Line1.Width = 7.333333F;
			this.Line1.X1 = 0.08333334F;
			this.Line1.X2 = 7.416667F;
			this.Line1.Y1 = 0.08333334F;
			this.Line1.Y2 = 0.08333334F;
			// 
			// txtTotalNew
			// 
			this.txtTotalNew.Height = 0.1666667F;
			this.txtTotalNew.Left = 3.166667F;
			this.txtTotalNew.Name = "txtTotalNew";
			this.txtTotalNew.Style = "text-align: right";
			this.txtTotalNew.Text = null;
			this.txtTotalNew.Top = 1.916667F;
			this.txtTotalNew.Width = 0.9166667F;
			// 
			// txtTotalReplacing
			// 
			this.txtTotalReplacing.Height = 0.1666667F;
			this.txtTotalReplacing.Left = 4.166667F;
			this.txtTotalReplacing.Name = "txtTotalReplacing";
			this.txtTotalReplacing.Style = "text-align: right";
			this.txtTotalReplacing.Text = null;
			this.txtTotalReplacing.Top = 1.916667F;
			this.txtTotalReplacing.Width = 0.9166667F;
			// 
			// txtTotalNet
			// 
			this.txtTotalNet.Height = 0.1666667F;
			this.txtTotalNet.Left = 5.166667F;
			this.txtTotalNet.Name = "txtTotalNet";
			this.txtTotalNet.Style = "text-align: right";
			this.txtTotalNet.Text = null;
			this.txtTotalNet.Top = 1.916667F;
			this.txtTotalNet.Width = 0.9166667F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1666667F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-weight: bold; text-align: right";
			this.Label7.Text = "Count";
			this.Label7.Top = 1.916667F;
			this.Label7.Width = 0.6666667F;
			// 
			// txtCount
			// 
			this.txtCount.Height = 0.1666667F;
			this.txtCount.Left = 0.75F;
			this.txtCount.Name = "txtCount";
			this.txtCount.Style = "text-align: left";
			this.txtCount.Text = null;
			this.txtCount.Top = 1.916667F;
			this.txtCount.Width = 0.8333333F;
			// 
			// lblCat1
			// 
			this.lblCat1.Height = 0.1666667F;
			this.lblCat1.HyperLink = null;
			this.lblCat1.Left = 0.6666667F;
			this.lblCat1.Name = "lblCat1";
			this.lblCat1.Style = "font-weight: bold; text-align: right";
			this.lblCat1.Text = "Category 1";
			this.lblCat1.Top = 0.1666667F;
			this.lblCat1.Width = 2.416667F;
			// 
			// txtTotNewCat1
			// 
			this.txtTotNewCat1.Height = 0.1666667F;
			this.txtTotNewCat1.Left = 3.166667F;
			this.txtTotNewCat1.Name = "txtTotNewCat1";
			this.txtTotNewCat1.Style = "text-align: right";
			this.txtTotNewCat1.Text = "0";
			this.txtTotNewCat1.Top = 0.1666667F;
			this.txtTotNewCat1.Width = 0.9166667F;
			// 
			// txtTotReplCat1
			// 
			this.txtTotReplCat1.Height = 0.1666667F;
			this.txtTotReplCat1.Left = 4.166667F;
			this.txtTotReplCat1.Name = "txtTotReplCat1";
			this.txtTotReplCat1.Style = "text-align: right";
			this.txtTotReplCat1.Text = "0";
			this.txtTotReplCat1.Top = 0.1666667F;
			this.txtTotReplCat1.Width = 0.9166667F;
			// 
			// txtTotNetCat1
			// 
			this.txtTotNetCat1.Height = 0.1666667F;
			this.txtTotNetCat1.Left = 5.166667F;
			this.txtTotNetCat1.Name = "txtTotNetCat1";
			this.txtTotNetCat1.Style = "text-align: right";
			this.txtTotNetCat1.Text = "0";
			this.txtTotNetCat1.Top = 0.1666667F;
			this.txtTotNetCat1.Width = 0.9166667F;
			// 
			// lblCat2
			// 
			this.lblCat2.Height = 0.1666667F;
			this.lblCat2.HyperLink = null;
			this.lblCat2.Left = 0.6666667F;
			this.lblCat2.Name = "lblCat2";
			this.lblCat2.Style = "font-weight: bold; text-align: right";
			this.lblCat2.Text = "Category 2";
			this.lblCat2.Top = 0.3333333F;
			this.lblCat2.Width = 2.416667F;
			// 
			// txtTotNewCat2
			// 
			this.txtTotNewCat2.Height = 0.1666667F;
			this.txtTotNewCat2.Left = 3.166667F;
			this.txtTotNewCat2.Name = "txtTotNewCat2";
			this.txtTotNewCat2.Style = "text-align: right";
			this.txtTotNewCat2.Text = "0";
			this.txtTotNewCat2.Top = 0.3333333F;
			this.txtTotNewCat2.Width = 0.9166667F;
			// 
			// txtTotReplCat2
			// 
			this.txtTotReplCat2.Height = 0.1666667F;
			this.txtTotReplCat2.Left = 4.166667F;
			this.txtTotReplCat2.Name = "txtTotReplCat2";
			this.txtTotReplCat2.Style = "text-align: right";
			this.txtTotReplCat2.Text = "0";
			this.txtTotReplCat2.Top = 0.3333333F;
			this.txtTotReplCat2.Width = 0.9166667F;
			// 
			// txtTotNetCat2
			// 
			this.txtTotNetCat2.Height = 0.1666667F;
			this.txtTotNetCat2.Left = 5.166667F;
			this.txtTotNetCat2.Name = "txtTotNetCat2";
			this.txtTotNetCat2.Style = "text-align: right";
			this.txtTotNetCat2.Text = "0";
			this.txtTotNetCat2.Top = 0.3333333F;
			this.txtTotNetCat2.Width = 0.9166667F;
			// 
			// lblCat3
			// 
			this.lblCat3.Height = 0.1666667F;
			this.lblCat3.HyperLink = null;
			this.lblCat3.Left = 0.6666667F;
			this.lblCat3.Name = "lblCat3";
			this.lblCat3.Style = "font-weight: bold; text-align: right";
			this.lblCat3.Text = "Category 3";
			this.lblCat3.Top = 0.5F;
			this.lblCat3.Width = 2.416667F;
			// 
			// txtTotNewCat3
			// 
			this.txtTotNewCat3.Height = 0.1666667F;
			this.txtTotNewCat3.Left = 3.166667F;
			this.txtTotNewCat3.Name = "txtTotNewCat3";
			this.txtTotNewCat3.Style = "text-align: right";
			this.txtTotNewCat3.Text = "0";
			this.txtTotNewCat3.Top = 0.5F;
			this.txtTotNewCat3.Width = 0.9166667F;
			// 
			// txtTotReplCat3
			// 
			this.txtTotReplCat3.Height = 0.1666667F;
			this.txtTotReplCat3.Left = 4.166667F;
			this.txtTotReplCat3.Name = "txtTotReplCat3";
			this.txtTotReplCat3.Style = "text-align: right";
			this.txtTotReplCat3.Text = "0";
			this.txtTotReplCat3.Top = 0.5F;
			this.txtTotReplCat3.Width = 0.9166667F;
			// 
			// txtTotNetCat3
			// 
			this.txtTotNetCat3.Height = 0.1666667F;
			this.txtTotNetCat3.Left = 5.166667F;
			this.txtTotNetCat3.Name = "txtTotNetCat3";
			this.txtTotNetCat3.Style = "text-align: right";
			this.txtTotNetCat3.Text = "0";
			this.txtTotNetCat3.Top = 0.5F;
			this.txtTotNetCat3.Width = 0.9166667F;
			// 
			// lblCat4
			// 
			this.lblCat4.Height = 0.1666667F;
			this.lblCat4.HyperLink = null;
			this.lblCat4.Left = 0.6666667F;
			this.lblCat4.Name = "lblCat4";
			this.lblCat4.Style = "font-weight: bold; text-align: right";
			this.lblCat4.Text = "Category 4";
			this.lblCat4.Top = 0.6666667F;
			this.lblCat4.Width = 2.416667F;
			// 
			// txtTotNewCat4
			// 
			this.txtTotNewCat4.Height = 0.1666667F;
			this.txtTotNewCat4.Left = 3.166667F;
			this.txtTotNewCat4.Name = "txtTotNewCat4";
			this.txtTotNewCat4.Style = "text-align: right";
			this.txtTotNewCat4.Text = "0";
			this.txtTotNewCat4.Top = 0.6666667F;
			this.txtTotNewCat4.Width = 0.9166667F;
			// 
			// txtTotReplCat4
			// 
			this.txtTotReplCat4.Height = 0.1666667F;
			this.txtTotReplCat4.Left = 4.166667F;
			this.txtTotReplCat4.Name = "txtTotReplCat4";
			this.txtTotReplCat4.Style = "text-align: right";
			this.txtTotReplCat4.Text = "0";
			this.txtTotReplCat4.Top = 0.6666667F;
			this.txtTotReplCat4.Width = 0.9166667F;
			// 
			// txtTotNetCat4
			// 
			this.txtTotNetCat4.Height = 0.1666667F;
			this.txtTotNetCat4.Left = 5.166667F;
			this.txtTotNetCat4.Name = "txtTotNetCat4";
			this.txtTotNetCat4.Style = "text-align: right";
			this.txtTotNetCat4.Text = "0";
			this.txtTotNetCat4.Top = 0.6666667F;
			this.txtTotNetCat4.Width = 0.9166667F;
			// 
			// lblCat5
			// 
			this.lblCat5.Height = 0.1666667F;
			this.lblCat5.HyperLink = null;
			this.lblCat5.Left = 0.6666667F;
			this.lblCat5.Name = "lblCat5";
			this.lblCat5.Style = "font-weight: bold; text-align: right";
			this.lblCat5.Text = "Category 5";
			this.lblCat5.Top = 0.8333333F;
			this.lblCat5.Width = 2.416667F;
			// 
			// txtTotNewCat5
			// 
			this.txtTotNewCat5.Height = 0.1666667F;
			this.txtTotNewCat5.Left = 3.166667F;
			this.txtTotNewCat5.Name = "txtTotNewCat5";
			this.txtTotNewCat5.Style = "text-align: right";
			this.txtTotNewCat5.Text = "0";
			this.txtTotNewCat5.Top = 0.8333333F;
			this.txtTotNewCat5.Width = 0.9166667F;
			// 
			// txtTotReplCat5
			// 
			this.txtTotReplCat5.Height = 0.1666667F;
			this.txtTotReplCat5.Left = 4.166667F;
			this.txtTotReplCat5.Name = "txtTotReplCat5";
			this.txtTotReplCat5.Style = "text-align: right";
			this.txtTotReplCat5.Text = "0";
			this.txtTotReplCat5.Top = 0.8333333F;
			this.txtTotReplCat5.Width = 0.9166667F;
			// 
			// txtTotNetCat5
			// 
			this.txtTotNetCat5.Height = 0.1666667F;
			this.txtTotNetCat5.Left = 5.166667F;
			this.txtTotNetCat5.Name = "txtTotNetCat5";
			this.txtTotNetCat5.Style = "text-align: right";
			this.txtTotNetCat5.Text = "0";
			this.txtTotNetCat5.Top = 0.8333333F;
			this.txtTotNetCat5.Width = 0.9166667F;
			// 
			// lblCat6
			// 
			this.lblCat6.Height = 0.1666667F;
			this.lblCat6.HyperLink = null;
			this.lblCat6.Left = 0.6666667F;
			this.lblCat6.Name = "lblCat6";
			this.lblCat6.Style = "font-weight: bold; text-align: right";
			this.lblCat6.Text = "Category 6";
			this.lblCat6.Top = 1F;
			this.lblCat6.Width = 2.416667F;
			// 
			// txtTotNewCat6
			// 
			this.txtTotNewCat6.Height = 0.1666667F;
			this.txtTotNewCat6.Left = 3.166667F;
			this.txtTotNewCat6.Name = "txtTotNewCat6";
			this.txtTotNewCat6.Style = "text-align: right";
			this.txtTotNewCat6.Text = "0";
			this.txtTotNewCat6.Top = 1F;
			this.txtTotNewCat6.Width = 0.9166667F;
			// 
			// txtTotReplCat6
			// 
			this.txtTotReplCat6.Height = 0.1666667F;
			this.txtTotReplCat6.Left = 4.166667F;
			this.txtTotReplCat6.Name = "txtTotReplCat6";
			this.txtTotReplCat6.Style = "text-align: right";
			this.txtTotReplCat6.Text = "0";
			this.txtTotReplCat6.Top = 1F;
			this.txtTotReplCat6.Width = 0.9166667F;
			// 
			// txtTotNetCat6
			// 
			this.txtTotNetCat6.Height = 0.1666667F;
			this.txtTotNetCat6.Left = 5.166667F;
			this.txtTotNetCat6.Name = "txtTotNetCat6";
			this.txtTotNetCat6.Style = "text-align: right";
			this.txtTotNetCat6.Text = "0";
			this.txtTotNetCat6.Top = 1F;
			this.txtTotNetCat6.Width = 0.9166667F;
			// 
			// lblCat7
			// 
			this.lblCat7.Height = 0.1666667F;
			this.lblCat7.HyperLink = null;
			this.lblCat7.Left = 0.6666667F;
			this.lblCat7.Name = "lblCat7";
			this.lblCat7.Style = "font-weight: bold; text-align: right";
			this.lblCat7.Text = "Category 7";
			this.lblCat7.Top = 1.166667F;
			this.lblCat7.Width = 2.416667F;
			// 
			// txtTotNewCat7
			// 
			this.txtTotNewCat7.Height = 0.1666667F;
			this.txtTotNewCat7.Left = 3.166667F;
			this.txtTotNewCat7.Name = "txtTotNewCat7";
			this.txtTotNewCat7.Style = "text-align: right";
			this.txtTotNewCat7.Text = "0";
			this.txtTotNewCat7.Top = 1.166667F;
			this.txtTotNewCat7.Width = 0.9166667F;
			// 
			// txtTotReplCat7
			// 
			this.txtTotReplCat7.Height = 0.1666667F;
			this.txtTotReplCat7.Left = 4.166667F;
			this.txtTotReplCat7.Name = "txtTotReplCat7";
			this.txtTotReplCat7.Style = "text-align: right";
			this.txtTotReplCat7.Text = "0";
			this.txtTotReplCat7.Top = 1.166667F;
			this.txtTotReplCat7.Width = 0.9166667F;
			// 
			// txtTotNetCat7
			// 
			this.txtTotNetCat7.Height = 0.1666667F;
			this.txtTotNetCat7.Left = 5.166667F;
			this.txtTotNetCat7.Name = "txtTotNetCat7";
			this.txtTotNetCat7.Style = "text-align: right";
			this.txtTotNetCat7.Text = "0";
			this.txtTotNetCat7.Top = 1.166667F;
			this.txtTotNetCat7.Width = 0.9166667F;
			// 
			// lblCat8
			// 
			this.lblCat8.Height = 0.1666667F;
			this.lblCat8.HyperLink = null;
			this.lblCat8.Left = 0.6666667F;
			this.lblCat8.Name = "lblCat8";
			this.lblCat8.Style = "font-weight: bold; text-align: right";
			this.lblCat8.Text = "Category 8";
			this.lblCat8.Top = 1.333333F;
			this.lblCat8.Width = 2.416667F;
			// 
			// txtTotNewCat8
			// 
			this.txtTotNewCat8.Height = 0.1666667F;
			this.txtTotNewCat8.Left = 3.166667F;
			this.txtTotNewCat8.Name = "txtTotNewCat8";
			this.txtTotNewCat8.Style = "text-align: right";
			this.txtTotNewCat8.Text = "0";
			this.txtTotNewCat8.Top = 1.333333F;
			this.txtTotNewCat8.Width = 0.9166667F;
			// 
			// txtTotReplCat8
			// 
			this.txtTotReplCat8.Height = 0.1666667F;
			this.txtTotReplCat8.Left = 4.166667F;
			this.txtTotReplCat8.Name = "txtTotReplCat8";
			this.txtTotReplCat8.Style = "text-align: right";
			this.txtTotReplCat8.Text = "0";
			this.txtTotReplCat8.Top = 1.333333F;
			this.txtTotReplCat8.Width = 0.9166667F;
			// 
			// txtTotNetCat8
			// 
			this.txtTotNetCat8.Height = 0.1666667F;
			this.txtTotNetCat8.Left = 5.166667F;
			this.txtTotNetCat8.Name = "txtTotNetCat8";
			this.txtTotNetCat8.Style = "text-align: right";
			this.txtTotNetCat8.Text = "0";
			this.txtTotNetCat8.Top = 1.333333F;
			this.txtTotNetCat8.Width = 0.9166667F;
			// 
			// lblCat9
			// 
			this.lblCat9.Height = 0.1666667F;
			this.lblCat9.HyperLink = null;
			this.lblCat9.Left = 0.6666667F;
			this.lblCat9.Name = "lblCat9";
			this.lblCat9.Style = "font-weight: bold; text-align: right";
			this.lblCat9.Text = "Category 9";
			this.lblCat9.Top = 1.5F;
			this.lblCat9.Width = 2.416667F;
			// 
			// txtTotNewCat9
			// 
			this.txtTotNewCat9.Height = 0.1666667F;
			this.txtTotNewCat9.Left = 3.166667F;
			this.txtTotNewCat9.Name = "txtTotNewCat9";
			this.txtTotNewCat9.Style = "text-align: right";
			this.txtTotNewCat9.Text = "0";
			this.txtTotNewCat9.Top = 1.5F;
			this.txtTotNewCat9.Width = 0.9166667F;
			// 
			// txtTotReplCat9
			// 
			this.txtTotReplCat9.Height = 0.1666667F;
			this.txtTotReplCat9.Left = 4.166667F;
			this.txtTotReplCat9.Name = "txtTotReplCat9";
			this.txtTotReplCat9.Style = "text-align: right";
			this.txtTotReplCat9.Text = "0";
			this.txtTotReplCat9.Top = 1.5F;
			this.txtTotReplCat9.Width = 0.9166667F;
			// 
			// txtTotNetCat9
			// 
			this.txtTotNetCat9.Height = 0.1666667F;
			this.txtTotNetCat9.Left = 5.166667F;
			this.txtTotNetCat9.Name = "txtTotNetCat9";
			this.txtTotNetCat9.Style = "text-align: right";
			this.txtTotNetCat9.Text = "0";
			this.txtTotNetCat9.Top = 1.5F;
			this.txtTotNetCat9.Width = 0.9166667F;
			// 
			// rptNewValuation
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewValuation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmountReplacing)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNetNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalReplacing)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalNet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNewCat1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotReplCat1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNetCat1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNewCat2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotReplCat2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNetCat2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNewCat3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotReplCat3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNetCat3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNewCat4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotReplCat4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNetCat4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNewCat5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotReplCat5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNetCat5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNewCat6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotReplCat6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNetCat6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNewCat7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotReplCat7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNetCat7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNewCat8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotReplCat8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNetCat8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNewCat9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotReplCat9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotNetCat9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNewValuation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmountReplacing;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNetNew;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalNew;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalReplacing;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalNet;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotNewCat1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotReplCat1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotNetCat1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotNewCat2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotReplCat2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotNetCat2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotNewCat3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotReplCat3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotNetCat3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotNewCat4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotReplCat4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotNetCat4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotNewCat5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotReplCat5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotNetCat5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotNewCat6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotReplCat6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotNetCat6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotNewCat7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotReplCat7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotNetCat7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotNewCat8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotReplCat8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotNetCat8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotNewCat9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotReplCat9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotNetCat9;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniname;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
