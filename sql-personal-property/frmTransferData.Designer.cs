﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmTransferData.
	/// </summary>
	partial class frmTransferData : BaseForm
	{
		public fecherFoundation.FCComboBox cmbLeasedItemized;
		public fecherFoundation.FCButton cmdProcess;
		public fecherFoundation.FCComboBox cboFromAccount;
		public fecherFoundation.FCComboBox cboToAccount;
		public fecherFoundation.FCLabel lblToData;
		public fecherFoundation.FCLabel lblFromData;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuTransfer;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTransferData));
            this.cmbLeasedItemized = new fecherFoundation.FCComboBox();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.cboFromAccount = new fecherFoundation.FCComboBox();
            this.cboToAccount = new fecherFoundation.FCComboBox();
            this.lblToData = new fecherFoundation.FCLabel();
            this.lblFromData = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuTransfer = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 592);
            this.BottomPanel.Size = new System.Drawing.Size(1088, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdProcess);
            this.ClientArea.Controls.Add(this.cmbLeasedItemized);
            this.ClientArea.Controls.Add(this.cboFromAccount);
            this.ClientArea.Controls.Add(this.cboToAccount);
            this.ClientArea.Controls.Add(this.lblToData);
            this.ClientArea.Controls.Add(this.lblFromData);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(1088, 532);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(1088, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(422, 30);
            this.HeaderText.Text = "Transfer Itemized/Leased Information";
            // 
            // cmbLeasedItemized
            // 
            this.cmbLeasedItemized.Items.AddRange(new object[] {
            "Itemized",
            "Leased",
            "Both"});
            this.cmbLeasedItemized.Location = new System.Drawing.Point(30, 222);
            this.cmbLeasedItemized.Name = "cmbLeasedItemized";
            this.cmbLeasedItemized.Size = new System.Drawing.Size(222, 40);
            this.cmbLeasedItemized.TabIndex = 3;
            this.cmbLeasedItemized.Text = "Both";
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(30, 292);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Size = new System.Drawing.Size(98, 48);
            this.cmdProcess.TabIndex = 6;
            this.cmdProcess.Text = "Transfer";
            this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
            // 
            // cboFromAccount
            // 
            this.cboFromAccount.BackColor = System.Drawing.SystemColors.Window;
            this.cboFromAccount.Location = new System.Drawing.Point(245, 126);
            this.cboFromAccount.Name = "cboFromAccount";
            this.cboFromAccount.Size = new System.Drawing.Size(476, 40);
            this.cboFromAccount.TabIndex = 2;
            this.cboFromAccount.SelectedIndexChanged += new System.EventHandler(this.cboFromAccount_SelectedIndexChanged);
            // 
            // cboToAccount
            // 
            this.cboToAccount.BackColor = System.Drawing.SystemColors.Window;
            this.cboToAccount.Location = new System.Drawing.Point(245, 30);
            this.cboToAccount.Name = "cboToAccount";
            this.cboToAccount.Size = new System.Drawing.Size(476, 40);
            this.cboToAccount.TabIndex = 1;
            this.cboToAccount.SelectedIndexChanged += new System.EventHandler(this.cboToAccount_SelectedIndexChanged);
            // 
            // lblToData
            // 
            this.lblToData.Location = new System.Drawing.Point(30, 90);
            this.lblToData.Name = "lblToData";
            this.lblToData.Size = new System.Drawing.Size(637, 16);
            this.lblToData.TabIndex = 5;
            // 
            // lblFromData
            // 
            this.lblFromData.Location = new System.Drawing.Point(30, 186);
            this.lblFromData.Name = "lblFromData";
            this.lblFromData.Size = new System.Drawing.Size(637, 16);
            this.lblFromData.TabIndex = 4;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 140);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(139, 16);
            this.Label2.TabIndex = 8;
            this.Label2.Text = "TRANSFER DATA FROM";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(145, 16);
            this.Label1.TabIndex = 7;
            this.Label1.Text = "TRANSFER DATA TO";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuTransfer,
            this.mnuSepar,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuTransfer
            // 
            this.mnuTransfer.Index = 0;
            this.mnuTransfer.Name = "mnuTransfer";
            this.mnuTransfer.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuTransfer.Text = "Transfer";
            this.mnuTransfer.Click += new System.EventHandler(this.mnuTransfer_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 1;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // frmTransferData
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1088, 700);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmTransferData";
            this.Text = "Transfer Itemized/Leased Information";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmTransferData_Load);
            this.Activated += new System.EventHandler(this.frmTransferData_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmTransferData_KeyDown);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
