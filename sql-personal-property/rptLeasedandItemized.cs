﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptLeasedandItemized.
	/// </summary>
	public partial class rptLeasedandItemized : BaseSectionReport
	{
		public rptLeasedandItemized()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Original Cost Format";
		}

		public static rptLeasedandItemized InstancePtr
		{
			get
			{
				return (rptLeasedandItemized)Sys.GetInstance(typeof(rptLeasedandItemized));
			}
		}

		protected rptLeasedandItemized _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}

            if (disposing)
            {
				rsLoad?.Dispose();
                rsLoad = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptLeasedandItemized	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intPage;
		int intRtype;
		public bool boolPQuantity;
		private clsDRWrapper rsLoad = new clsDRWrapper();

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			txtMuni.Text = modGlobalConstants.Statics.MuniName + " ME";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsLoad.EndOfFile();
			if (eArgs.EOF)
				return;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
            int const_printtoolid;
			int intIndex;
            txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intPage = 1;
        }
		
		private void Detail_AfterPrint(object sender, EventArgs e)
		{
			intPage = 1;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (rsLoad.EndOfFile())
				return;
			if ((intRtype == 0) || (intRtype == 2))
			{
				SubReport1.Report.UserData = Conversion.Val(rsLoad.GetData("masteraccount"));
			}
			if (intRtype > 0)
			{
				// TODO Get_Fields: Field [masteraccount] not found!! (maybe it is an alias?)
				SubReport2.Report.UserData = Conversion.Val(rsLoad.Get_Fields("masteraccount"));
			}
			rsLoad.MoveNext();
			// intPage = 1
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
            txtPage.Text = "Page " + FCConvert.ToString(intPage);
			intPage += 1;
		}

		public void Start(short intType, bool boolq, string strSQL)
        {
            rsLoad.OpenRecordset(strSQL, "PersonalProperty");
			string strFName = "";
			intRtype = intType;
			boolPQuantity = boolq;
			switch (intType)
			{
				case 0:
					{
						SubReport1.Report = new srptLeasedOriginalCost();
						strFName = "LeasedOrigCost";
						break;
					}
				case 1:
					{
						SubReport2.Report = new srptItemizedOriginalCost();
						strFName = "ItemizedOrigCost";
						break;
					}
				case 2:
					{
						SubReport1.Report = new srptLeasedOriginalCost();
						SubReport2.Report = new srptItemizedOriginalCost();
						strFName = "LeasedItemizedOrigCost";
						break;
					}
			}
			//end switch
			frmReportViewer.InstancePtr.Init(rptObj: this, boolAllowEmail: true, strAttachmentName: strFName);
			;
		}

		
	}
}
