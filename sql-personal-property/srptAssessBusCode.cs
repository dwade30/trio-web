﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for srptAssessBusCode.
	/// </summary>
	public partial class srptAssessBusCode : FCSectionReport
	{
		public srptAssessBusCode()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ActiveReport1";
		}

		public static srptAssessBusCode InstancePtr
		{
			get
			{
				return (srptAssessBusCode)Sys.GetInstance(typeof(srptAssessBusCode));
			}
		}

		protected srptAssessBusCode _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsLoad?.Dispose();
                clsLoad = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptAssessBusCode	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsLoad = new clsDRWrapper();
		int lngTotalValue;
		int lngTotalExempt;
		int lngTotalTotal;
		int lngTotalCount;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsLoad.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQL = "";
			string strTBL1 = "";
			string strTBL2 = "";
			// check the tag to see if we are billing or current
			if (Strings.UCase(FCConvert.ToString(this.UserData)) == "BILLINGBUSCODE")
			{
				// easier since you don't worry about overrides
				lblRptType.Text = "Business";
				clsLoad.OpenRecordset("select businesscode as bcode,sum([value]) AS ValSum, sum(exemption) as ExemptSum,count(account) as TheCount from ppmaster where not deleted = 1 group by businesscode  order by businesscode ", "twpp0000.vb1");
			}
			else if (Strings.UCase(FCConvert.ToString(this.UserData)) == "CURRENTBUSCODE")
			{
				// current must take into account the overrides
				lblRptType.Text = "Business";
				lblBillingExemption.Visible = true;
				strSQL = "select buscode  as bcode ,sum(TheValue) as valsum,sum(exemption1) as exemptsum,count(buscode) as TheCount from (select * from   ((select businesscode  as buscode ,category1  + category2 + category3  + category4  + category5 + category6  + category7  + category8  + category9  AS TheValue, exemption  as exemption1  from ppmaster inner join ppvaluations on (ppvaluations.valuekey = ppmaster.account) where  NOT deleted = 1 and (not (orcode  = 'Y')))  union all (select businesscode   as buscode,overrideamount  as TheValue,exemption  as exemption1 from ppmaster inner join ppvaluations on (ppvaluations.valuekey = ppmaster.account) where not deleted = 1 and orcode   = 'Y') ) as abc) as def group by buscode order by buscode";
				clsLoad.OpenRecordset(strSQL, "twpp0000.vb1");
			}
			else if (Strings.UCase(FCConvert.ToString(this.UserData)) == "BILLINGSTREETCODE")
			{
				lblRptType.Text = "Street";
				clsLoad.OpenRecordset("select streetcode as bcode,sum([value]) AS ValSum, sum(exemption) as ExemptSum,count(account) as TheCount from ppmaster where not deleted = 1 group by streetcode  order by streetcode ", "twpp0000.vb1");
			}
			else if (Strings.UCase(FCConvert.ToString(this.UserData)) == "CURRENTSTREETCODE")
			{
				lblBillingExemption.Visible = true;
				lblRptType.Text = "Street";
				strSQL = "select scode  as bcode ,sum(TheValue) as valsum,sum(exemption1) as exemptsum,count(scode) as TheCount from (select * from   ((select streetcode  as scode ,category1  + category2  + category3  + category4  + category5  + category6  + category7  + category8  + category9 AS TheValue, exemption  as exemption1  from ppmaster inner join ppvaluations on (ppvaluations.valuekey = ppmaster.account) where not deleted = 1 and (not (orcode  = 'Y')))  union all (select street   as scode,overrideamount  as TheValue,exemption  as exemption1 from ppmaster inner join ppvaluations on (ppvaluations.valuekey = ppmaster.account) where not deleted = 1 and orcode  = 'Y')) as abc ) as def group by scode order by scode";
				clsLoad.OpenRecordset(strSQL, "twpp0000.vb1");
			}
			else if (Strings.UCase(FCConvert.ToString(this.UserData)) == "BILLINGTRANCODE")
			{
				lblRptType.Text = "Tran";
				clsLoad.OpenRecordset("select trancode as bcode,sum([value]) AS ValSum, sum(exemption) as ExemptSum,count(account) as TheCount from ppmaster where not deleted = 1 group by trancode  order by trancode ", "twpp0000.vb1");
			}
			else if (Strings.UCase(FCConvert.ToString(this.UserData)) == "CURRENTTRANCODE")
			{
				lblRptType.Text = "Tran";
				lblBillingExemption.Visible = true;
				strSQL = "select tcode  as bcode ,sum(TheValue) as valsum,sum(exemption1) as exemptsum,count(tcode) as TheCount from (select * from   ((select trancode as tcode ,category1  + category2  + category3  + category4  + category5  + category6  + category7  + category8  + category9 AS TheValue, exemption  as exemption1  from ppmaster inner join ppvaluations on (ppvaluations.valuekey = ppmaster.account) where not deleted = 1 and (not (orcode  = 'Y')))  union all (select trancode   as tcode,overrideamount  as TheValue,exemption  as exemption1 from ppmaster inner join ppvaluations on (ppvaluations.valuekey = ppmaster.account) where not deleted = 1 and orcode   = 'Y')) as abc ) as def group by tcode order by tcode";
				clsLoad.OpenRecordset(strSQL, "twpp0000.vb1");
			}
			if (clsLoad.EndOfFile())
			{
				//this.Close();
				return;
			}
			lngTotalValue = 0;
			lngTotalExempt = 0;
			lngTotalTotal = 0;
			lngTotalCount = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			int lngValue = 0;
			int lngExempt = 0;
			int lngTotal = 0;
			int lngCount = 0;
			if (!clsLoad.EndOfFile())
			{
				// TODO Get_Fields: Field [bcode] not found!! (maybe it is an alias?)
				txtCode.Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("bcode")));
				// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
				lngCount = FCConvert.ToInt32(clsLoad.Get_Fields("thecount"));
				txtCount.Text = FCConvert.ToString(lngCount);
				// TODO Get_Fields: Field [valsum] not found!! (maybe it is an alias?)
				lngValue = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("valsum"))));
				txtValue.Text = Strings.Format(lngValue, "#,###,###,##0");
				// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
				lngExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("exemptsum"))));
				txtExemption.Text = Strings.Format(lngExempt, "#,###,###,##0");
				lngTotal = lngValue - lngExempt;
				txtTotal.Text = Strings.Format(lngTotal, "#,###,###,##0");
				lngTotalValue += lngValue;
				lngTotalExempt += lngExempt;
				lngTotalTotal += lngTotal;
				lngTotalCount += lngCount;
				clsLoad.MoveNext();
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtTotalCount.Text = FCConvert.ToString(lngTotalCount);
			txtTotalValue.Text = Strings.Format(lngTotalValue, "#,###,###,##0");
			txttotalExempt.Text = Strings.Format(lngTotalExempt, "#,###,###,##0");
			txtTotalTotal.Text = Strings.Format(lngTotalTotal, "#,###,###,##0");
		}

		
	}
}
