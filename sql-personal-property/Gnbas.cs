﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Runtime.InteropServices;
using Wisej.Core;
using System.IO;

namespace TWPP0000
{
	public class modGlobal
	{
		//=========================================================
		/// <summary>
		/// Public Const strSystemSettings = "TWGN0000.vb1"
		/// </summary>
		public const int CNSTALLOWUNLOAD = 0;
		public const int CNSTDONTUNLOADCOMPACTING = 1;
		/// <summary>
		/// Public clssecurityclass As clsTrioSecurity
		/// </summary>
		public const uint HKEY_LOCAL_MACHINE = 0x80000002;
		/// <summary>
		/// These Declares are used to get the block cursor
		/// </summary>
		[DllImport("user32")]
		public static extern int CreateCaret(int hwnd, int hBitmap, int nWidth, int nHeight);

		[DllImport("user32")]
		public static extern int ShowCaret(int hwnd);

		[DllImport("user32")]
		public static extern int GetFocus();

		[DllImport("user32")]
		public static extern object SetCursorPos(int x, int y);
		/// <summary>
		/// screen setup items
		/// </summary>
		const string Logo = "TRIO Software Corporation";
		public const string SearchInputBoxTitle = "Real Estate Assessment Search";

		[DllImport("kernel32")]
		public static extern void Sleep(int dwMilliseconds);

		[DllImport("kernel32")]
		public static extern int WinExec(string lpCmdLine, int nCmdShow);

		[DllImport("kernel32")]
		public static extern int GlobalAlloc(int wFlags, int dwBytes);

		public const string DEFAULTDATABASE = "TWGN0000.VB1";

		public struct ControlProportions
		{
			public float WidthProportions;
			public float HeightProportions;
			public float TopProportions;
			public float LeftProportions;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public ControlProportions(int unusedParam)
			{
				this.WidthProportions = 0;
				this.HeightProportions = 0;
				this.TopProportions = 0;
				this.LeftProportions = 0;
			}
		};

		public struct TownSpecificCustomize
		{
			public double TaxRate;
			public double Ratio;
			public double BETEReimburseRate;
			public short MunicipalCode;
			public string MunicipalName;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public TownSpecificCustomize(int unusedParam)
			{
				this.TaxRate = 0;
				this.Ratio = 0;
				this.BETEReimburseRate = 0;
				this.MunicipalCode = 0;
				this.MunicipalName = string.Empty;
			}
		};

		public static void ConvertToString()
		{
			Statics.IntVarTemp = Conversion.Str(Statics.IntvarNumeric);
			Statics.INTVARSTRING = "";
			Statics.IntVarTemp = Strings.LTrim(Statics.IntVarTemp);
			Statics.INTVARSTRING = Strings.StrDup(Statics.intLen - Statics.IntVarTemp.Length, "0") + Statics.IntVarTemp;
		}
		// vbPorter upgrade warning: intValue As object	OnWrite(object, short, double)
		// vbPorter upgrade warning: intLength As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		//public static string PadToString_6(ref object intValue, int intLength)
		//{
		//	return PadToString(ref intValue, ref intLength);
		//}
		//public static string PadToString_8(object intValue, int intLength)
		//{
		//	return PadToString(ref intValue, ref intLength);
		//}
		public static string PadToString(object intValue, int intLength)
		{
			string PadToString = "";
			// converts an integer to string
			// this routine is faster than ConvertToString
			// to use this function pass it a value and then length of string
			// Example:  XYZStr = PadToString(XYZInt, 6)
			// if XYZInt is equal to 123 then XYZStr is equal to "000123"
			// 
			if (fecherFoundation.FCUtils.IsNull(intValue) == true)
				intValue = 0;
			PadToString = Strings.StrDup(intLength - FCConvert.ToString(intValue).Length, "0") + intValue;
			return PadToString;
		}

		public static object SetInsertMode(int InsertMode)
		{
			object SetInsertMode = null;
			// 0 = Insert Mode        = SelLength of 0
			// 1 = OverType Mode      = SelLength of 1
			// The Variable "OverType" returns either 0 or 1 for SelLength
			// 
			if (FCGlobal.Screen.ActiveControl is TextBoxBase && (FCGlobal.Screen.ActiveControl as TextBoxBase).SelectionLength == 1)
			{
				Statics.OverType = 0;
			}
			else
			{
				Statics.OverType = 1;
			}
			return SetInsertMode;
		}

		public static double RoundDbl(double dValue, int iDigits)
		{
			double RoundDbl = 0;
			RoundDbl = Conversion.Int(dValue * (Math.Pow(10, iDigits)) + 0.5) / (Math.Pow(10, iDigits));
			return RoundDbl;
		}

		public static double Round(double dValue, int iDigits)
		{
			double Round = 0;
			// Round = Int(dValue * (10 ^ iDigits) + 0.5) / (10 ^ iDigits)
			int intValue;
			// vbPorter upgrade warning: intR As short --> As int	OnWriteFCConvert.ToDouble(
			int intR;
			// intValue = Val(WPP.Fields("WPPRound"))
			intValue = Statics.CustomizeStuff.Round;
			intR = FCConvert.ToInt32(Math.Pow(10, intValue));
			// If WPP.GetData("WPPRound") = 3 Then
			// If WPP.GetData("wppround2") = "Y" And dValue < 1000 Then
			// intValue = 2
			// Else
			// If dValue < 1000 Then
			// Round = 0
			// Exit Function
			// End If
			// End If
			// End If
			if (dValue < intR)
			{
				switch (modPPCalculate.Statics.RoundContingency)
				{
					case 0:
						{
							intValue -= 1;
							break;
						}
					case 1:
						{
							if (intR == 1)
							{
								Round = 0;
								return Round;
							}
							if (dValue != 0)
							{
								if (dValue < (intR / 2.0))
									Round = (intR / 2.0);
							}
							return Round;
						}
					case 2:
						{
							// just use the formula below
							break;
						}
				}
				//end switch
			}
			Round = (fecherFoundation.FCUtils.iDiv(((dValue + 0.01) / (Math.Pow(10, intValue))), 1)) * (Math.Pow(10, intValue));
			// Round = Int(Val(dValue * (10 ^ iDigits)) + 0.5) / (10 ^ iDigits)
			return Round;
		}

		public static string Quotes(ref string strValue)
		{
			string Quotes = "";
			Quotes = FCConvert.ToString(Convert.ToChar(34)) + strValue + FCConvert.ToString(Convert.ToChar(34));
			return Quotes;
		}

		public static void WriteYY()
		{
			modReplaceWorkFiles.EntryFlagFile(true, "PP", true);
		}
		// vbPorter upgrade warning: frm As Form	OnWrite(frmPPItemized, frmPPCostRatios, frmPPMaster, frmPPLeased, frmTrendExempt, frmPrintSummary, frmPPPrinting, frmPPReimbursementNotices, frmPPTop40Taxpayers, frmPPGetAccount, frmPPBusinessCodes, frmPPStreetTranCodes, frmPPFileMaintenance, frmPPMasterShort, frmPPAuditBilling)
		public static void SaveWindowSize(Form frm)
		{
			Interaction.SaveSetting("TWGNENTY", "GENERAL_SETTINGS", "MainLeft", FCConvert.ToString(frm.Left));
			Interaction.SaveSetting("TWGNENTY", "GENERAL_SETTINGS", "MainTop", FCConvert.ToString(frm.Top));
			Interaction.SaveSetting("TWGNENTY", "GENERAL_SETTINGS", "MainWidth", FCConvert.ToString(frm.Width));
			Interaction.SaveSetting("TWGNENTY", "GENERAL_SETTINGS", "MainHeight", FCConvert.ToString(frm.Height));
		}
		/// <summary>
		/// Public Sub GetWindowSize(frm As Form)
		/// </summary>
		/// <summary>
		/// Dim lngHeight   As Long
		/// </summary>
		/// <summary>
		/// Dim lngWidth    As Long
		/// </summary>
		/// <summary>
		/// Dim lngTop      As Long
		/// </summary>
		/// <summary>
		/// Dim lngLeft     As Long
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// lngHeight = Screen.Height
		/// </summary>
		/// <summary>
		/// lngLeft = 0
		/// </summary>
		/// <summary>
		/// lngTop = 0
		/// </summary>
		/// <summary>
		/// lngWidth = Screen.Width
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// If lngHeight <> frm.Height Then
		/// </summary>
		/// <summary>
		/// If lngWidth <> frm.Width Then
		/// </summary>
		/// <summary>
		/// If frm.WindowState <> vbNormal Then
		/// </summary>
		/// <summary>
		/// frm.WindowState = vbNormal
		/// </summary>
		/// <summary>
		/// End If
		/// </summary>
		/// <summary>
		/// frm.Height = lngHeight
		/// </summary>
		/// <summary>
		/// frm.Left = lngLeft
		/// </summary>
		/// <summary>
		/// frm.Top = lngTop
		/// </summary>
		/// <summary>
		/// frm.Width = lngWidth
		/// </summary>
		/// <summary>
		/// If Screen.Height - frm.Height <= 100 Then
		/// </summary>
		/// <summary>
		/// If Screen.Width - frm.Width <= 100 Then
		/// </summary>
		/// <summary>
		/// frm.WindowState = vbMaximized
		/// </summary>
		/// <summary>
		/// End If
		/// </summary>
		/// <summary>
		/// End If
		/// </summary>
		/// <summary>
		/// End If
		/// </summary>
		/// <summary>
		/// End If
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// Call SetFixedSize(frm, True)
		/// </summary>
		/// <summary>
		/// End Sub
		/// </summary>
		public static void CloseChildForms()
		{
			//FC:FINAL:DSE #925 Cannot change a collection inside a foreach statement
			//foreach (Form frm in Application.OpenForms)
			//{
			//    if (frm.Name != "MDIParent")
			//    {
			//        if (!Statics.boolLoadHide)
			//            frm.Close();
			//    }
			//}
			FCUtils.CloseFormsOnProject(Statics.boolLoadHide);
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As double	OnWrite(double, short)
		public static double GetCurrentAccount()
		{
			double GetCurrentAccount = 0;
			clsDRWrapper rsAccounts = new clsDRWrapper();
			rsAccounts.OpenRecordset("Select max(Account) as CurrentMaxAccounts from PPMaster", "Twpp0000.vb1");
			if (!rsAccounts.EndOfFile())
			{
				// TODO Get_Fields: Field [CurrentMaxAccounts] not found!! (maybe it is an alias?)
				GetCurrentAccount = Conversion.Val(rsAccounts.Get_Fields("CurrentMaxAccounts") + " ");
			}
			else
			{
				GetCurrentAccount = 1;
			}
			return GetCurrentAccount;
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(short, object)
		public static object GetNumAccounts()
		{
			object GetNumAccounts = null;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper clsTemp = new clsDRWrapper();
				GetNumAccounts = 0;
				clsTemp.OpenRecordset("select count(account) as thecount from ppmaster where deleted = 0", "TWPP0000.vb1");
				if (!clsTemp.EndOfFile())
				{
					// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
					GetNumAccounts = clsTemp.Get_Fields("thecount");
				}
				return GetNumAccounts;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In GetNumAccounts", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetNumAccounts;
		}

		public static void escapequote(ref string a)
		{
			// searches for a singlequote and puts another next to it
			string tstring = "";
			string t2string = "";
			int stringindex;
			int intpos = 0;
			if (a == string.Empty)
				return;
			stringindex = 1;
			while (stringindex <= a.Length)
			{
				intpos = Strings.InStr(stringindex, a, "'", CompareConstants.vbBinaryCompare);
				if (intpos == 0)
					break;
				tstring = Strings.Mid(a, 1, intpos);
				t2string = Strings.Mid(a, intpos + 1);
				if (intpos == a.Length)
				{
					// a = tstring & "' "
					a = Strings.Mid(a, 1, intpos - 1);
					break;
				}
				else
				{
					a = tstring + "'" + t2string;
				}
				stringindex = intpos + 2;
			}
			stringindex = 1;
			while (stringindex <= a.Length)
			{
				intpos = Strings.InStr(stringindex, a, FCConvert.ToString(Convert.ToChar(34)), CompareConstants.vbBinaryCompare);
				if (intpos == 0)
					break;
				tstring = Strings.Mid(a, 1, intpos);
				t2string = Strings.Mid(a, intpos + 1);
				if (intpos == a.Length)
				{
					a = Strings.Mid(a, 1, intpos - 1);
					break;
				}
				a = tstring + FCConvert.ToString(Convert.ToChar(34)) + t2string;
				stringindex = intpos + 2;
			}
		}

		public static void SetupSigInformation()
		{
			// dummy procedure
		}

		public class StaticVariables
		{
			//public class modGNBas
			//FC:FINAL:AM: renamed class
			public int lngDontAllowUnload;
			public double dblDefaultInterestRate;
			public double gdblCMFAdjustment;
			public double gdblLienAdjustmentTop;
			public double gdblLDNAdjustmentTop;
			public double gdblLabelsAdjustment;
			public double gdblLabelsAdjustmentDMH;
			public double gdblLabelsAdjustmentDMV;
			public bool gboolShowLastCLAccountInCR;
			public string gstrlastyearbilled = "";
			public int gintBasis;
			/// </summary>
			/// Public Const strCLDatabase = "TWCL0000.vb1"
			/// <summary>
			//public double dblOverPayRate;
			public bool boolDosPrint;
			public string LabelPrinterName = "";
			// vbPorter upgrade warning: BreakDown As string	OnWrite(string, DialogResult)
			public string BreakDown = string.Empty;
			public string DetailSummary = "";
			public int MenuChoice;
			public bool gboolPrint;
			public bool boolLoadHide;
			// vbPorter upgrade warning: gobjFormName As object	OnRead(Form)
			public Form gobjFormName;
			// vbPorter upgrade warning: gobjLabel As object --> As FCLabel
			public FCLabel gobjLabel;
			public int[] SpecLength = new int[10 + 1];
			public string[] SpecCase = new string[10 + 1];
			public string[] SpecKeyword = new string[10 + 1];
			public string[] SpecM1 = new string[10 + 1];
			public string[] SpecM2 = new string[10 + 1];
			public string[] SpecM3 = new string[10 + 1];
			public string[] SpecM4 = new string[10 + 1];
			public string[] SpecM5 = new string[10 + 1];
			public string[] SpecM6 = new string[10 + 1];
			public string[] SpecM7 = new string[10 + 1];
			public string[] SpecM8 = new string[10 + 1];
			public string[] SpecM9 = new string[10 + 1];
			public string[] SpecM10 = new string[10 + 1];
			public string[] SpecType = new string[10 + 1];
			public string[] SpecParam = new string[10 + 1];
			// vbPorter upgrade warning: SpecLow As string	OnWriteFCConvert.ToInt16(
			public string[] SpecLow = new string[10 + 1];
			// vbPorter upgrade warning: SpecHigh As string	OnWriteFCConvert.ToInt16(
			public string[] SpecHigh = new string[10 + 1];
			public int RespLength;
			public string RespCase = string.Empty;
			public string RespM1 = string.Empty;
			public string RespM2 = string.Empty;
			public string RespM3 = string.Empty;
			public string RespM4 = string.Empty;
			public string RespM5 = string.Empty;
			public string RespM6 = string.Empty;
			public string RespM7 = string.Empty;
			public string RespM8 = string.Empty;
			public string RespM9 = string.Empty;
			public string RespM10 = string.Empty;
			public string RespOK = "";
			public string RespType = string.Empty;
			public string RespParam = string.Empty;
			public string RespLow = string.Empty;
			public string RespHigh = string.Empty;
			public object Response;
			// vbPorter upgrade warning: RespKeyword As Variant --> As string
			public string RespKeyword = string.Empty;
			// vbPorter upgrade warning: Resp1 As Variant --> As string
			public string Resp1 = string.Empty;
			// vbPorter upgrade warning: Resp2 As Variant --> As string
			public string Resp2 = string.Empty;
			public object Resp3;
			public object Resp4;
			public object Resp5;
			public object Resp6;
			public object Resp7;
			public object Resp8;
			public object Resp9;
			public object Resp10;
			public bool SystemSetup;
			public bool LinkPicture;
			public object CARD2;
			public int Bp;
			// vbPorter upgrade warning: MessageResp1 As Variant --> As DialogResult
			/// </summary>
			/// These are used to hold a Message Box Response
			/// <summary>
			/// </summary>
			/// Message Box Response Variables
			/// <summary>
			public DialogResult MessageResp1;
			public object MessageResp2;
			public object MessageResp3;
			/// </summary>
			/// Public PictureLocation      As String
			/// <summary>
			public string DocumentLocationName = "";
			/// </summary>
			/// Prefixes for open statements
			/// <summary>
			/// </summary>
			/// Public LocksHeld(50)        As Long
			/// <summary>
			/// </summary>
			/// Public HighLock(50, 20)     As Long
			/// <summary>
			/// </summary>
			/// Public LowLock(50, 20)      As Long
			/// <summary>
			/// </summary>
			/// Public HighLockNumber       As Long
			/// <summary>
			/// </summary>
			/// Public LowLockNumber        As Long
			/// <summary>
			/// </summary>
			/// Public LockFileNumber       As Integer
			/// <summary>
			/// </summary>
			/// record locking fields
			/// <summary>
			/// </summary>
			/// Public SketchLocation       As String
			/// <summary>
			public string PPDatabase = "";
			public string TrioEXE = "";
			public string TrioDATA = "";
			public string TrioSDrive = "";
			/// </summary>
			/// File Editting copying, moving, deleting
			/// <summary>
			public FileInfo CopyFile;
			public FileInfo MoveFile;
			public FileInfo DelFile;
			public string Application = "";
			public string ProgramName = "";
			public string ProgramShort = "";
			public string TitleCaption = "";
			public string TitleTime = "";
			public string Ltime = "";
			/// </summary>
			/// used in format print statements
			/// <summary>
			public int fmtlgth;
			public int fmtval;
			public string fmttype = "";
			public char[] fmtcolon = new char[1];
			public int fmtspcbef;
			public int fmtspcaft;
			/// </summary>
			/// used in GNMenu
			/// <summary>
			public string MenuTitle = "";
			public string MenuShort = "";
			public int MenuCount;
			public char[] MenuOptions = new char[19];
			public string[] MenuTitles = new string[19 + 1];
			public char[] MenuSelected = new char[1];
			/// </summary>
			/// used in ConvertToString
			/// <summary>
			public double IntvarNumeric;
			public string INTVARSTRING = string.Empty;
			public string IntVarTemp = string.Empty;
			public int intLen;
			public string newfld = "";
			/// </summary>
			/// used by search routines
			/// <summary>
			public string SEQ = "";
			public int House;
			public string SEARCH = "";
			public bool LongScreenSearch;
			/// </summary>
			/// used for insert/overtype mode in text boxes
			/// <summary>
			public int OverType;
			/// </summary>
			/// used in getvalue
			/// <summary>
			public string NewData = "";
			public int NewValue;
			public char[] EntryMenuResponse = new char[2];
			public char[] MainMenuResponse = new char[1];
			public char[] MenuResponse = new char[1];
			public char[] CurrentApplication = new char[1];
			public string Escape = "";
			public int CommentAccount;
			public string CommentName = "";
			public int x;
			/// </summary>
			/// Public Bbort$
			/// <summary>
			/// </summary>
			/// Public ABORT$
			/// <summary>
			public string SortFileSpec = "";
			public int NextNumber;
			public bool SketcherCalled;
			/// </summary>
			/// Public Declare Function sendmessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long
			/// <summary>
			public string Display = "";
			/// </summary>
			/// Public CustomizeStuff As CustomizeType
			/// <summary>
			/// </summary>
			/// End Type
			/// <summary>
			/// </summary>
			/// dblRatio As Double
			/// <summary>
			/// </summary>
			/// boolRegionalTown As Boolean
			/// <summary>
			/// </summary>
			/// LimitPercentGood    As Boolean
			/// <summary>
			/// </summary>
			/// LeasedTrendTimesCost As Boolean
			/// <summary>
			/// </summary>
			/// DefaultYearFirstAssessed As Long
			/// <summary>
			/// </summary>
			/// ShowOriginalTotals As Boolean
			/// <summary>
			/// </summary>
			/// TrendTimesCost As Boolean
			/// <summary>
			/// </summary>
			/// TaxRate As Double
			/// <summary>
			/// </summary>
			/// Roundcontingency As Integer
			/// <summary>
			/// </summary>
			/// ShowRateOnShort As Boolean
			/// <summary>
			/// </summary>
			/// DepreciationYear As Integer
			/// <summary>
			/// </summary>
			/// Round   As Integer
			/// <summary>
			/// </summary>
			/// Type CustomizeType
			/// <summary>
			//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
			//public clsCustomizeInfo CustomizeStuff = new clsCustomizeInfo();
			public clsCustomizeInfo CustomizeStuff_AutoInitialized;

			public clsCustomizeInfo CustomizeStuff
			{
				get
				{
					if (CustomizeStuff_AutoInitialized == null)
					{
						CustomizeStuff_AutoInitialized = new clsCustomizeInfo();
					}
					return CustomizeStuff_AutoInitialized;
				}
				set
				{
					CustomizeStuff_AutoInitialized = value;
				}
			}

			public bool EntryMenuFlag;
			public int gintMaxAccounts;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
