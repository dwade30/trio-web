﻿namespace TWPP0000
{
	/// <summary>
	/// Summary description for srptCostSummary.
	/// </summary>
	partial class srptCostSummary
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptCostSummary));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAssessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCategory = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLeased = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtItemized = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRatio = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBETEExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCategory)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLeased)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtItemized)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRatio)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBETEExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAssessment,
				this.txtCategory,
				this.txtLeased,
				this.txtItemized,
				this.txtRatio,
				this.txtBETEExempt
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Line1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7
			});
			this.GroupHeader1.Height = 0.5625F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 3.71875F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.Label1.Text = "Cost Summary";
			this.Label1.Top = 0F;
			this.Label1.Width = 2.5F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.0625F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.1875F;
			this.Line1.Width = 9.8125F;
			this.Line1.X1 = 0.0625F;
			this.Line1.X2 = 9.875F;
			this.Line1.Y1 = 0.1875F;
			this.Line1.Y2 = 0.1875F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.0625F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label2.Text = "Category";
			this.Label2.Top = 0.34375F;
			this.Label2.Width = 1F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 4.125F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Label3.Text = "Leased";
			this.Label3.Top = 0.375F;
			this.Label3.Width = 0.5625F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 5.5F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Label4.Text = "Itemized";
			this.Label4.Top = 0.375F;
			this.Label4.Width = 0.6875F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 6.3125F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label5.Text = "Ratio";
			this.Label5.Top = 0.375F;
			this.Label5.Width = 0.4375F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 7.3125F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Label6.Text = "Assessment";
			this.Label6.Top = 0.375F;
			this.Label6.Width = 0.9375F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 8.364583F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Label7.Text = "BETE Exempt";
			this.Label7.Top = 0.375F;
			this.Label7.Width = 1.447917F;
			// 
			// txtAssessment
			// 
			this.txtAssessment.CanShrink = true;
			this.txtAssessment.Height = 0.19F;
			this.txtAssessment.Left = 6.75F;
			this.txtAssessment.Name = "txtAssessment";
			this.txtAssessment.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtAssessment.Text = null;
			this.txtAssessment.Top = 0F;
			this.txtAssessment.Width = 1.5F;
			// 
			// txtCategory
			// 
			this.txtCategory.CanShrink = true;
			this.txtCategory.Height = 0.19F;
			this.txtCategory.Left = 0.0625F;
			this.txtCategory.Name = "txtCategory";
			this.txtCategory.Style = "font-family: \'Tahoma\'";
			this.txtCategory.Text = null;
			this.txtCategory.Top = 0F;
			this.txtCategory.Width = 3.21875F;
			// 
			// txtLeased
			// 
			this.txtLeased.CanShrink = true;
			this.txtLeased.Height = 0.19F;
			this.txtLeased.Left = 3.375F;
			this.txtLeased.Name = "txtLeased";
			this.txtLeased.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtLeased.Text = null;
			this.txtLeased.Top = 0F;
			this.txtLeased.Width = 1.3125F;
			// 
			// txtItemized
			// 
			this.txtItemized.CanShrink = true;
			this.txtItemized.Height = 0.19F;
			this.txtItemized.Left = 4.75F;
			this.txtItemized.Name = "txtItemized";
			this.txtItemized.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtItemized.Text = null;
			this.txtItemized.Top = 0F;
			this.txtItemized.Width = 1.4375F;
			// 
			// txtRatio
			// 
			this.txtRatio.CanShrink = true;
			this.txtRatio.Height = 0.19F;
			this.txtRatio.Left = 6.3125F;
			this.txtRatio.Name = "txtRatio";
			this.txtRatio.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtRatio.Text = null;
			this.txtRatio.Top = 0F;
			this.txtRatio.Width = 0.375F;
			// 
			// txtBETEExempt
			// 
			this.txtBETEExempt.CanShrink = true;
			this.txtBETEExempt.Height = 0.19F;
			this.txtBETEExempt.Left = 8.3125F;
			this.txtBETEExempt.Name = "txtBETEExempt";
			this.txtBETEExempt.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtBETEExempt.Text = null;
			this.txtBETEExempt.Top = 0F;
			this.txtBETEExempt.Width = 1.5F;
			// 
			// srptCostSummary
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.Name = "ActiveReport1";
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 9.947917F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCategory)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLeased)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtItemized)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRatio)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBETEExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCategory;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLeased;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtItemized;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRatio;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBETEExempt;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
