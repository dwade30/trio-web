﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmPPMaster.
	/// </summary>
	public partial class frmPPMaster : BaseForm
	{
		public frmPPMaster()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Label2 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.lblstatistics = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.lblStatics = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.Label2.AddControlArrayElement(Label2_9, 9);
			this.Label2.AddControlArrayElement(Label2_0, 0);
			this.Label2.AddControlArrayElement(Label2_1, 1);
			this.lblstatistics.AddControlArrayElement(lblstatistics_2, 2);
			this.lblStatics.AddControlArrayElement(lblStatics_14, 14);
			this.lblStatics.AddControlArrayElement(lblStatics_15, 15);
			this.lblStatics.AddControlArrayElement(lblStatics_16, 16);
			this.lblStatics.AddControlArrayElement(lblStatics_17, 17);
			this.lblStatics.AddControlArrayElement(lblStatics_18, 18);
			this.lblStatics.AddControlArrayElement(lblStatics_19, 19);
			this.lblStatics.AddControlArrayElement(lblStatics_0, 0);
			this.lblStatics.AddControlArrayElement(lblStatics_20, 20);
			this.lblStatics.AddControlArrayElement(lblStatics_29, 29);
            this.mnuInventoryListing.Click += MnuPrintInventoryListing_Click;
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPPMaster InstancePtr
		{
			get
			{
				return (frmPPMaster)Sys.GetInstance(typeof(frmPPMaster));
			}
		}

		protected frmPPMaster _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		bool Saving;
		int NextAccount;
		string hold = "";
		string noaccount = "";
		bool Helping;
		bool NeedToSave;
		bool boolFormLoaded;
		bool boolDataChanged;

		private cPPAccount theAccount_AutoInitialized;

		private cPPAccount theAccount
		{
			get
			{
				if (theAccount_AutoInitialized == null)
				{
					theAccount_AutoInitialized = new cPPAccount();
				}
				return theAccount_AutoInitialized;
			}
			set
			{
				theAccount_AutoInitialized = value;
			}
		}

		private void cmdItemized_Click(object sender, System.EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			if (modPPGN.Statics.Adding)
			{
				SaveData();
			}
			modGlobal.SaveWindowSize(this);
			modPPGN.Statics.Exiting = true;
			frmPPMaster.InstancePtr.Close();
			frmPPLeased.InstancePtr.Close();
            if (frmPPItemized.InstancePtr.IsLoaded)
            {
                frmPPItemized.InstancePtr.Unload();
                frmPPItemized.InstancePtr.Dispose();
            }
            frmPPItemized.InstancePtr.Show(App.MainForm);
		}

		private void cmdLeased_Click(object sender, System.EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			if (modPPGN.Statics.Adding)
			{
				SaveData();
			}
			modGlobal.SaveWindowSize(this);
			modPPGN.Statics.Exiting = true;
			frmPPMaster.InstancePtr.Close();
			frmPPItemized.InstancePtr.Close();
            if (frmPPLeased.InstancePtr.IsLoaded)
            {
                frmPPLeased.InstancePtr.Unload();
                frmPPLeased.InstancePtr.Dispose();
            }
			frmPPLeased.InstancePtr.Show(App.MainForm);
			// Unload frmPPMaster
		}

		private void SetupGridTranCode()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			clsLoad.OpenRecordset("select * from trancodes where trancode = 0", modPPGN.strPPDatabase);
			if (clsLoad.EndOfFile())
			{
				if (!modGlobal.Statics.CustomizeStuff.boolRegionalTown)
				{
					strTemp = "#0;None" + "\t" + "0|";
				}
				else
				{
					strTemp = "#0;Default" + "\t" + "0|";
				}
			}
			clsLoad.OpenRecordset("select * from trancodes order by trancode", modPPGN.strPPDatabase);
			while (!clsLoad.EndOfFile())
			{
				strTemp += "#" + clsLoad.Get_Fields("trancode") + ";" + clsLoad.Get_Fields_String("trantype") + "\t" + clsLoad.Get_Fields("trancode") + "|";
				clsLoad.MoveNext();
			}
			if (strTemp != string.Empty)
			{
				strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
			}
			gridTranCode.ColComboList(0, strTemp);
			gridTranCode.TextMatrix(0, 0, FCConvert.ToString(0));
		}

		private void ResizeGridTranCode()
		{
			//gridTranCode.HeightOriginal = gridTranCode.RowHeight(0) + 60;
		}

		public void frmPPMaster_Activated(object sender, System.EventArgs e)
		{
			object answer;
			clsDRWrapper rs = new clsDRWrapper();
			clsDRWrapper datCostFiles = new clsDRWrapper();
			// If FormExist(Me) Then Exit Sub
			if (modPPGN.Statics.Searching)
			{
				mnuSearch.Enabled = true;
				cmdNextAccount.Visible = true;
				cmdPreviousAccount.Visible = true;
			}
			else
			{
				mnuSearch.Enabled = false;
				cmdNextAccount.Visible = false;
				cmdPreviousAccount.Visible = false;
			}
			if (!modSecurity.ValidPermissions_6(this, modSecurity.ACCOUNTMAINTENANCE))
			{
				MessageBox.Show("Your permission setting for Account Maintenance is set to none or is missing.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Close();
				return;
			}
			if (Helping == true)
			{
				Helping = false;
				return;
			}
			modPPGN.Statics.Exiting = false;
			if (!boolFormLoaded)
			{
				theAccount.Clear();
				theAccount.Account = modPPGN.Statics.CurrentAccount;
				datCostFiles.OpenRecordset("SELECT * FROM PPRatioOpens", modPPGN.strPPDatabase);
				if (theAccount.Account <= 0)
				{
					modPPGN.Statics.Adding = true;
					// Adding New Account Show Blank Form
					lblAccount.Text = "000000";
					boolDataChanged = true;
				}
				else
				{
					// Adding = False
					lblAccount.Text = Strings.Format(modPPGN.Statics.CurrentAccount, "000000");
					// If rs.EndOfFile Then Exit Sub
					noaccount = "";
					if (hold == "" && noaccount == "0")
					{
						Close();
						return;
					}
					FillBoxes();
					boolDataChanged = false;
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				boolFormLoaded = true;
			}
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
		}

		private void frmPPMaster_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				mnuExit_Click();
				return;
			}
			if (this.ActiveControl is FCTextBox)
			{
				if (Shift == 2)
				{
					if (KeyCode == Keys.Left || KeyCode == Keys.Right)
						KeyCode = (Keys)0;
				}
				else
				{
					if (KeyCode == Keys.Left || KeyCode == Keys.Right)
						(this.ActiveControl as TextBoxBase).SelectionLength = 0;
				}
				if (KeyCode == Keys.Up)
					Support.SendKeys("+{TAB}", false);
				if (KeyCode == Keys.Down)
					Support.SendKeys("{TAB}", false);
			}
		}

		private void frmPPMaster_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// 
			// Escape
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
				return;
				// Return/Enter
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
				// Backspace
			}
			else if (KeyAscii == Keys.Back)
			{
				if (modGlobal.Statics.OverType == 1)
				{
					if (this.ActiveControl is FCTextBox)
					{
						KeyAscii = (Keys)0;
						(this.ActiveControl as TextBoxBase).SelectionLength = 0;
						Support.SendKeys("{LEFT}", false);
						(this.ActiveControl as TextBoxBase).SelectionLength = 1;
					}
				}
			}
			if (this.ActiveControl is FCTextBox)
			{
				(this.ActiveControl as TextBoxBase).SelectionLength = modGlobal.Statics.OverType;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmPPMaster_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (this.ActiveControl is FCTextBox)
			{
				if (KeyCode == Keys.Insert)
					modGlobal.SetInsertMode((this.ActiveControl as TextBoxBase).SelectionLength);
				//FC:FINAL:MSH - i.issue #1331: if activecontrol isn't TextBoxBase - will be threw an exception
				if ((this.ActiveControl as TextBoxBase) != null)
					(this.ActiveControl as TextBoxBase).SelectionLength = modGlobal.Statics.OverType;
				// If Screen.ActiveControl.SelStart = Screen.ActiveControl.MaxLength Then SendKeys "{TAB}"
			}
		}

		private void frmPPMaster_Load(object sender, System.EventArgs e)
		{
			boolFormLoaded = false;
			if (modPPGN.Statics.Searching)
			{
				mnuSearch.Enabled = true;
				cmdNextAccount.Visible = true;
				cmdPreviousAccount.Visible = true;
			}
			else
			{
				mnuSearch.Enabled = false;
				cmdNextAccount.Visible = false;
				cmdPreviousAccount.Visible = false;
			}
			lblOpen1.Text = modPPGN.Statics.Open1Title;
			lblOpen2.Text = modPPGN.Statics.Open2Title;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			lblComment.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORRED);
			SetupGridTranCode();
			// lblAccount.Caption = NextPPAccount
			// if loading the account for first time then do this
			if (!modPPGN.Statics.ItemizedTable && !modPPGN.Statics.LeasedTable)
			{
				// Set pp = OpenDatabase(DatabaseName, False, False, ";PWD=" & DATABASEPASSWORD)
				// 
				// DepYear = Val(WPP.GetData("WDEPYearPP") & "")
				// Call GetCostData
			}
			// DepYear = Val(WPP.GetData("WDEPYearPP") & "")
			modPPCalculate.Statics.DepYear = modGlobal.Statics.CustomizeStuff.DepreciationYear;
			GetCostData();
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (boolDataChanged && boolFormLoaded)
			{
				// vbPorter upgrade warning: intret As short, int --> As DialogResult
				DialogResult intret;
				intret = MessageBox.Show("Data has been changed.  Do you want to save changes?", "Save Changes?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
				if (intret == DialogResult.Yes)
				{
					if (!SaveData())
					{
						e.Cancel = true;
					}
				}
				else if (intret == DialogResult.Cancel)
				{
					e.Cancel = true;
				}
			}
		}

		private void frmPPMaster_Resize(object sender, System.EventArgs e)
		{
			ResizeGridTranCode();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			try
			{
				clsDRWrapper rs = new clsDRWrapper();

				if (boolFormLoaded)
				{
					modPPGN.Check_RB_Fields();
				}
				// End If
				if (modPPGN.Statics.ItemizedTable)
				{
					frmPPItemized.InstancePtr.Unload();
				}
				if (modPPGN.Statics.LeasedTable)
				{
					frmPPItemized.InstancePtr.Unload();
				}
				if (modPPGN.Statics.boolFromBilling)
				{
					// establish a connection and send the new data back
					clsDRWrapper clsTemp = new clsDRWrapper();
					string strMessage = "";
					bool boolOR = false;
					int lngExempt = 0;
					int lngTotal = 0;
					clsTemp.OpenRecordset("select sum(category1  + category2  + category3  + category4  + category5  + category6  + category7  + category8  + category9 ) as thesum from ppvaluations where valuekey = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount), "twpp0000.vb1");
					if (!clsTemp.EndOfFile())
					{
						// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
						lngTotal = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields("thesum"))));
					}
					else
					{
						lngTotal = 0;
					}
					// Call clsTemp.Execute("update ppmaster set ppmaster.value = " & lngTotal & " where account = " & CurrentAccount, "twpp0000.vb1")
					rs.OpenRecordset("select * from ppmaster where account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount), modPPGN.strPPDatabase);
					rs.Edit();
					rs.Set_Fields("value", lngTotal);
					rs.Update();
					MDIParent.InstancePtr.txtCommDest.LinkMode = 0;
					MDIParent.InstancePtr.txtCommDest.LinkTopic = "TWBL0000|frmPPAudit";
					MDIParent.InstancePtr.txtCommDest.LinkItem = "txtcommunicate";
					MDIParent.InstancePtr.txtCommDest.LinkMode = LinkModeConstants.VbLinkAutomatic;
					/* On Error GoTo ErrorHandler */
					clsTemp.OpenRecordset("select * from ppmaster where account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount), "twpp0000.vb1");
					lngExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int32("exemption"))));
					// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
					strMessage = FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields("value"))) + "," + FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields_Int32("exemption"))) + ",";
					if (FCConvert.ToString(clsTemp.Get_Fields_String("ORCode")) == "Y")
					{
						boolOR = true;
					}
					else
					{
						boolOR = false;
					}
					clsTemp.OpenRecordset("select * from ppvaluations where VALUEKEY = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount), "twpp0000.vb1");
					if (clsTemp.EndOfFile())
					{
						strMessage += "0,0,0,0,0,0,0,0,0";
					}
					else
					{
						if (boolOR)
						{
							strMessage = FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields_Int32("overrideamount"))) + "," + FCConvert.ToString(lngExempt) + ",";
						}
						strMessage += FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields_Int32("category1"))) + "," + FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields_Int32("category2"))) + "," + FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields_Int32("category3"))) + ",";
						strMessage += FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields_Int32("category4"))) + "," + FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields_Int32("category5"))) + "," + FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields_Int32("category4"))) + ",";
						strMessage += FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields_Int32("category7"))) + "," + FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields_Int32("category8"))) + "," + FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields_Int32("category9")));
					}
					/*? On Error Resume Next  */// because we don't care if it takes too long to process the sent message
					// and any errors should be taken care of in billing
					modPPGN.Statics.boolFromBilling = false;
					//MDIParent.InstancePtr.Enabled = true;
					//MDIParent.InstancePtr.Show();
					////Application.DoEvents();
					MDIParent.InstancePtr.txtCommDest.LinkExecute(strMessage);
					/* On Error GoTo ErrorHandler */
					//MDIParent.InstancePtr.Enabled = true;
				}
				else
				{
					//MDIParent.InstancePtr.BringToFront();
					//MDIParent.InstancePtr.Show();
				}
				return;
			}
			catch (Exception ex)
			{
				ErrorHandler:
				;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Master Unload", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void gridTranCode_CellChanged(object sender, DataGridViewCellEventArgs e)
		{
			boolDataChanged = true;
		}

		private void mnuAddAccount_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: lngNewAcct As int	OnWriteFCConvert.ToDouble(
			int lngNewAcct;
			// Dim clsSave As New clsDRWrapper
			lngNewAcct = FCConvert.ToInt32(modGlobal.GetCurrentAccount());
			// If GetNumAccounts >= gintMaxAccounts Then
			// MsgBox "Max Accounts has been reached. Please call TRIO Software.", vbExclamation
			// Else
			modPPGN.Statics.Adding = true;
			theAccount.Clear();
			cPPAccountController tCont = new cPPAccountController();
			//FC:FINAL:DSE A property cannot be passed as a ref parameter
			//tCont.SaveAccount(ref theAccount);
			cPPAccount temp = theAccount;
			tCont.SaveAccount(ref temp);
			theAccount = temp;
			modPPGN.Statics.CurrentAccount = theAccount.Account;
			//FC:FINAL:DDU:#i1270 - close forms instead of comment their hide method
			//frmPPItemized.InstancePtr.Hide();
			//frmPPLeased.InstancePtr.Hide();
			ClearBoxes();
			this.lblAccount.Text = Strings.Format(modPPGN.Statics.CurrentAccount, "00000");
			// lblOpen1.Caption = Open1Title
			// lblOpen2.Caption = Open2Title
			// End If
		}

		private void mnuCalculateDisplay_Click(object sender, System.EventArgs e)
		{
			if (boolFormLoaded)
			{
				if (boolDataChanged)
				{
					if (MessageBox.Show("In order to calculate, current changes must be saved" + "\r\n" + "Save changes?", "Save Changes?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
					{
						if (!SaveData())
							return;
					}
					else
					{
						return;
					}
				}
				modPPCalculate.Statics.CalculateFromMaster = true;
				if (!modPPGN.Statics.boolFromBilling)
				{
					Close();
				}
				frmPPCalculations.InstancePtr.Show(App.MainForm);
			}
		}

		private void mnuCalculatePrint_Click(object sender, System.EventArgs e)
		{
			if (boolDataChanged)
			{
				if (MessageBox.Show("In order to calculate, current changes must be saved" + "\r\n" + "Save changes?", "Save Changes?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
				{
					if (!SaveData())
						return;
				}
				else
				{
					return;
				}
			}
			modPPCalculate.Statics.CalculateFromMaster = true;
			frmPPCalculations.InstancePtr.frmPPCalculations_Activated(frmPPCalculations.InstancePtr, EventArgs.Empty);
			frmPPCalculations.InstancePtr.mnuFPrint_Click();
		}

		private void mnuChangeAssociation_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngREAcct;
			int LngNumAccts;
			int lngPPGroup;
			int lngNumREAccts;
			bool boolOKToAdd;
			// can they be grouped?
			bool boolOKToAssociate;
			// can it be the primary associate
			int lngREAcctToAdd = 0;
			// vbPorter upgrade warning: intTemp As short, int --> As DialogResult
			DialogResult intTemp;
			int lngREGroup;
			clsDRWrapper clsTemp = new clsDRWrapper();
			bool boolAddPP = false;
			bool boolAddRE = false;
			try
			{
				// On Error GoTo ErrorHandler
				if (modPPGN.Statics.CurrentAccount < 1)
				{
					MessageBox.Show("You must save the account and assign an account number first", "Account not Created", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				//FC:FINAL:BBE - CallByRef works only with the correct ref type. 
				object objREAcctToAdd = lngREAcctToAdd;
				if (!frmInput.InstancePtr.Init(ref objREAcctToAdd, "Real Estate Associate", "Enter the Real Estate account to associate", intDataType: modGlobalConstants.InputDTypes.idtWholeNumber))
				{
					return;
				}
				//FC:FINAL:BBE - After CallByRef assign the value back correctly.
				lngREAcctToAdd = FCConvert.ToInt32(objREAcctToAdd);
				clsLoad.OpenRecordset("select * from moduleassociation where module = 'PP' and account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount), "CentralData");
				lngREAcct = 0;
				lngPPGroup = 0;
				lngREGroup = 0;
				LngNumAccts = 0;
				lngNumREAccts = 0;
				boolOKToAdd = false;
				boolOKToAssociate = false;
				if (!clsLoad.EndOfFile())
				{
					if (lngREAcctToAdd == 0)
					{
						// need to delete the association
						lngREAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("remasteracct"))));
						lngPPGroup = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("GROUPNUMBER"))));
						if (lngREAcct != 0)
						{
							clsLoad.Edit();
							clsLoad.Set_Fields("remasteracct", 0);
							clsLoad.Set_Fields("primaryassociate", false);
							clsLoad.Update();
							clsLoad.OpenRecordset("select * from moduleassociation where groupnumber = " + FCConvert.ToString(lngPPGroup), "CentralData");
							LngNumAccts = clsLoad.RecordCount();
							if (LngNumAccts < 3)
							{
								// the associated accounts were it so get rid of group
								clsLoad.Execute("delete from moduleassociation where groupnumber = " + FCConvert.ToString(lngPPGroup), "CentralData");
								clsLoad.Execute("delete from groupmaster where id = " + FCConvert.ToString(lngPPGroup), "CentralData");
							}
						}
						txtREAssociate.Text = "";
						return;
					}
					else
					{
						lngREAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("remasteracct"))));
						if (lngREAcct == lngREAcctToAdd && clsLoad.Get_Fields_Boolean("primaryassociate"))
						{
							MessageBox.Show("This account is already associated to RE " + FCConvert.ToString(lngREAcct), "Already associated", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return;
						}
						else if (clsLoad.Get_Fields_Boolean("primaryassociate") && lngREAcct > 0)
						{
							intTemp = MessageBox.Show("This account is already associated with RE account " + FCConvert.ToString(lngREAcct) + "." + "\r\n" + "Are you sure you want to change it?", "Already Associated", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (intTemp == DialogResult.No)
								return;
						}
						lngPPGroup = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("GROUPNUMBER"));
						boolOKToAdd = true;
					}
				}
				else
				{
					if (lngREAcctToAdd == 0)
						return;
					// nothing to delete
					boolOKToAdd = true;
				}
				clsLoad.OpenRecordset("select * from moduleassociation where module = 'RE' and account = " + FCConvert.ToString(lngREAcctToAdd), "CentralData");
				if (!clsLoad.EndOfFile())
				{
					lngREGroup = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("groupnumber"))));
					if (lngPPGroup > 0 && lngREGroup != lngPPGroup)
					{
						boolOKToAdd = false;
						MessageBox.Show("PP Account " + FCConvert.ToString(modPPGN.Statics.CurrentAccount) + " and RE Account " + FCConvert.ToString(lngREAcctToAdd) + " are in different groups and cannot be associated until you change the groups", "Cannot Associate", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					else
					{
						boolOKToAdd = true;
					}
				}
				if (lngREGroup > 0)
				{
					clsLoad.OpenRecordset("select * from moduleassociation where module = 'PP' and remasteracct = " + FCConvert.ToString(lngREAcctToAdd) + " and primaryassociate = 1 ", "CentralData");
					if (!clsLoad.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						if (FCConvert.ToInt32(clsLoad.Get_Fields("account")) == modPPGN.Statics.CurrentAccount)
						{
							boolOKToAssociate = true;
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
							intTemp = MessageBox.Show("PP Account " + clsLoad.Get_Fields("account") + " is already the primary associate with RE account " + FCConvert.ToString(lngREAcctToAdd) + "." + "\r\n" + "This account will be grouped with RE " + FCConvert.ToString(lngREAcctToAdd) + "." + "\r\n" + "Do you also want to make it the new primary associate?", "Already Associated", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (intTemp == DialogResult.Yes)
								boolOKToAssociate = true;
						}
					}
					else
					{
						boolOKToAssociate = true;
					}
				}
				else
				{
					boolOKToAssociate = true;
				}
				if (lngPPGroup == 0)
				{
					boolAddPP = true;
				}
				else
				{
					boolAddPP = false;
				}
				if (lngREGroup == 0)
				{
					boolAddRE = true;
				}
				else
				{
					boolAddRE = false;
				}
				if (lngPPGroup == 0 && lngREGroup == 0)
				{
					// easy just add new entries
					// make a new group and make the RE account the group master
					clsLoad.OpenRecordset("select * from groupmaster where id = -20", "CentralData");
					clsLoad.AddNew();
					clsTemp.OpenRecordset("select max(groupnumber) as highestgroup from groupmaster", "CentralData");
					// TODO Get_Fields: Field [highestgroup] not found!! (maybe it is an alias?)
					clsLoad.Set_Fields("groupnumber", clsTemp.Get_Fields("highestgroup") + 1);
					lngPPGroup = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("groupnumber"));
					lngREGroup = lngPPGroup;
					clsLoad.Set_Fields("datecreated", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
					clsLoad.Update();
					lngPPGroup = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("id"));
					lngREGroup = lngPPGroup;
				}
				if (lngPPGroup == 0)
					lngPPGroup = lngREGroup;
				if (lngREGroup == 0)
					lngREGroup = lngPPGroup;
				if (boolOKToAssociate)
				{
					clsLoad.Execute("update moduleassociation set primaryassociate = 0 where remasteracct = " + FCConvert.ToString(lngREAcctToAdd), "CentralData");
				}
				if (boolAddPP)
				{
					if (boolOKToAssociate)
					{
						clsLoad.Execute("insert into moduleassociation (Groupnumber,module,account,primaryassociate,remasteracct) values (" + FCConvert.ToString(lngPPGroup) + ",'PP'," + FCConvert.ToString(modPPGN.Statics.CurrentAccount) + ",1," + FCConvert.ToString(lngREAcctToAdd) + ")", "CentralData");
					}
					else
					{
						clsLoad.Execute("insert into moduleassociation (Groupnumber,module,account,primaryassociate,remasteracct) values (" + FCConvert.ToString(lngPPGroup) + ",'PP'," + FCConvert.ToString(modPPGN.Statics.CurrentAccount) + ",0," + FCConvert.ToString(lngREAcctToAdd) + ")", "CentralData");
					}
				}
				else
				{
					if (boolOKToAssociate)
					{
						clsLoad.Execute("update moduleassociation set primaryassociate = 1,remasteracct = " + FCConvert.ToString(lngREAcctToAdd) + " where module = 'PP' and account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount), "CentralData");
					}
					else
					{
						clsLoad.Execute("update moduleassociation set primaryassociate = 0,remasteracct = " + FCConvert.ToString(lngREAcctToAdd) + " where module = 'PP' and account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount), "CentralData");
					}
				}
				if (boolAddRE)
				{
					if (boolAddPP)
					{
						clsLoad.Execute("insert into moduleassociation (groupnumber,module,account,groupprimary) values (" + FCConvert.ToString(lngREGroup) + ",'RE'," + FCConvert.ToString(lngREAcctToAdd) + ",1)", "CentralData");
					}
					else
					{
						clsLoad.Execute("insert into moduleassociation (groupnumber,module,account,groupprimary) values (" + FCConvert.ToString(lngREGroup) + ",'RE'," + FCConvert.ToString(lngREAcctToAdd) + ",0)", "CentralData");
					}
				}
				txtREAssociate.Text = FCConvert.ToString(lngREAcctToAdd);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In mnuChangeAssociation_click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuComment_Click(object sender, System.EventArgs e)
		{
			frmRefGlobalComment refGC = new frmRefGlobalComment();
			//FC:FINAL:MSH - i.issue #1262: restore missed call of the form
			string module = "PP";
			string table = "PPMaster";
			string field = "Comment";
			string keyField = "Account";
			int lngID = modPPGN.Statics.CurrentAccount;
			theAccount.Comment = refGC.Init(ref module, ref table, ref field, ref keyField, ref lngID);
			// If frmGlobalComment.Init("PP", "PPMaster", "Comment", "Account", Val(CurrentAccount), , , TRIOWINDOWSIZEBIGGIE, , True) Then
			if (Strings.Trim(theAccount.Comment) == "")
			{
				lblComment.Visible = false;
			}
			else
			{
				lblComment.Visible = true;
			}
		}

		private void mnuDeleteAccount_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: varDelete As Variant --> As DialogResult
			DialogResult varDelete;
			clsDRWrapper rs = new clsDRWrapper();
			if (modPPGN.Statics.CurrentAccount < 1)
			{
				MessageBox.Show("The account hasn't been created yet", "Account not Created", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			varDelete = MessageBox.Show("Are you sure you want to delete this account?", "Delete?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (varDelete == DialogResult.Yes)
			{
				rs.OpenRecordset("select * from ppmaster where account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount), modPPGN.strPPDatabase);
				rs.Edit();
				rs.Set_Fields("Deleted", true);
				rs.Update();
				modGlobalFunctions.AddCYAEntry_26("PP", "Deleted account", modPPGN.Statics.CurrentAccount.ToString());
				Close();
				modPPGN.checkGroup_6(modPPGN.Statics.CurrentAccount, false);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuGotoLeased_Click()
		{
			modPPGN.Statics.Exiting = true;
			frmPPLeased.InstancePtr.Show(App.MainForm);
			Close();
		}

		private void mnuGotoSpecificAccount_Click()
		{
			FillBoxes();
		}

		private void mnuItemized_Click()
		{
			modPPGN.Statics.Exiting = true;
			frmPPItemized.InstancePtr.Show(App.MainForm);
			Close();
		}

		private void mnuGroup_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			// vbPorter upgrade warning: intRes As short, int --> As DialogResult
			DialogResult intRes;
			int lngGroupNumber = 0;
			clsLoad.OpenRecordset("select * from moduleassociation where module = 'PP' and account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount), "CentralData");
			if (!clsLoad.EndOfFile())
			{
				lngGroupNumber = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("groupnumber"));
				clsLoad.OpenRecordset("select * from groupmaster where id = " + FCConvert.ToString(lngGroupNumber), "CentralData");
				if (!clsLoad.EndOfFile())
				{
					//! Load frmGroup;
					frmGroup.InstancePtr.Init(lngGroupNumber);
				}
				else
				{
					MessageBox.Show("This account is associated with a group but the group does not exist.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			else
			{
				intRes = MessageBox.Show("This account is not in a group." + "\r\n" + "Would you like to create a new group?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (intRes == DialogResult.Yes)
				{
					//! Load frmGroup;
					frmGroup.InstancePtr.Init(0);
				}
			}
		}

		private void mnuInterestedParties_Click(object sender, System.EventArgs e)
		{
			if (modPPGN.Statics.CurrentAccount > 0)
			{
				frmInterestedParties.InstancePtr.Init(modPPGN.Statics.CurrentAccount, "PP", 0, FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modSecurity.EDITADDRESSESMENU)) == "F");
			}
		}

		private void mnuNext_Click(object sender, System.EventArgs e)
		{
			int lngAcct = 0;
			int oldRow;
			int lngRow;
			bool boolNotFound;
			bool boolUpdateItem;
			bool boolUpdateLeased;
			oldRow = frmPPSearch.InstancePtr.SearchGrid.Row;
			lngRow = frmPPSearch.InstancePtr.SearchGrid.Row + 1;
			boolNotFound = true;
			while (boolNotFound && lngRow < frmPPSearch.InstancePtr.SearchGrid.Rows)
			{
				if (FCConvert.ToBoolean(frmPPSearch.InstancePtr.SearchGrid.RowData(lngRow)))
				{
					lngRow += 1;
				}
				else
				{
					boolNotFound = false;
				}
			}
			boolUpdateItem = false;
			boolUpdateLeased = false;
			if (!boolNotFound)
			{
				frmPPSearch.InstancePtr.SearchGrid.Row = lngRow;
				lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(frmPPSearch.InstancePtr.SearchGrid.TextMatrix(lngRow, 0))));
				if (modPPGN.Statics.ItemizedTable)
				{
					boolUpdateItem = true;
					frmPPItemized.InstancePtr.UnloadOnlySelf();
				}
				if (modPPGN.Statics.LeasedTable)
				{
					frmPPLeased.InstancePtr.UnloadOnlySelf();
				}
				modPPGN.Statics.ItemizedTable = false;
				modPPGN.Statics.LeasedTable = false;
				ReloadAccount(lngAcct);
				boolDataChanged = false;
				// If boolUpdateItem Then
				// frmPPItemized.ReLoad
				// End If
				// If boolUpdateLeased Then
				// frmPPLeased.ReLoad
				// End If
			}
		}
		/// <summary>
		/// Private Sub mnuNextRecord_Click()
		/// </summary>
		/// <summary>
		/// Dim rs As New clsdrwrapper
		/// </summary>
		/// <summary>
		/// Call CheckForSave
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// If LeasedTable Then
		/// </summary>
		/// <summary>
		/// Unload frmPPLeased
		/// </summary>
		/// <summary>
		/// End If
		/// </summary>
		/// <summary>
		/// If ItemizedTable Then
		/// </summary>
		/// <summary>
		/// Unload frmPPItemized
		/// </summary>
		/// <summary>
		/// End If
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// Call rs.OpenRecordset("SELECT * FROM PPMaster WHERE Account = " & CurrentAccount + 1, modPPGN.strPPDatabase)
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// If Not rs.EndOfFile Then
		/// </summary>
		/// <summary>
		/// CurrentAccount = CurrentAccount + 1
		/// </summary>
		/// <summary>
		/// Call FillBoxes
		/// </summary>
		/// <summary>
		/// Else
		/// </summary>
		/// <summary>
		/// Call rs.OpenRecordset("select * from ppmaster where account = " & CurrentAccount, modPPGN.strPPDatabase)
		/// </summary>
		/// <summary>
		/// End If
		/// </summary>
		/// <summary>
		/// If LeasedTable Then
		/// </summary>
		/// <summary>
		/// Call frmPPLeased.Form_Load
		/// </summary>
		/// <summary>
		/// Else
		/// </summary>
		/// <summary>
		/// Load frmPPLeased
		/// </summary>
		/// <summary>
		/// End If
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// If ItemizedTable Then
		/// </summary>
		/// <summary>
		/// Call frmPPItemized.Form_Load
		/// </summary>
		/// <summary>
		/// Else
		/// </summary>
		/// <summary>
		/// Load frmPPItemized
		/// </summary>
		/// <summary>
		/// End If
		/// </summary>
		/// <summary>
		/// Me.Show
		/// </summary>
		/// <summary>
		/// End Sub
		/// </summary>
		private void mnuPrevious_Click(object sender, System.EventArgs e)
		{
			int lngAcct = 0;
			int oldRow;
			int lngRow;
			bool boolNotFound;
			oldRow = frmPPSearch.InstancePtr.SearchGrid.Row;
			lngRow = frmPPSearch.InstancePtr.SearchGrid.Row - 1;
			boolNotFound = true;
			while (boolNotFound && lngRow > 0)
			{
				if (FCConvert.ToBoolean(frmPPSearch.InstancePtr.SearchGrid.RowData(lngRow)))
				{
					lngRow -= 1;
				}
				else
				{
					boolNotFound = false;
				}
			}
			if (!boolNotFound)
			{
				frmPPSearch.InstancePtr.SearchGrid.Row = lngRow;
				lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(frmPPSearch.InstancePtr.SearchGrid.TextMatrix(lngRow, 0))));
				if (modPPGN.Statics.ItemizedTable)
				{
					frmPPItemized.InstancePtr.UnloadOnlySelf();
				}
				if (modPPGN.Statics.LeasedTable)
				{
					frmPPLeased.InstancePtr.UnloadOnlySelf();
				}
				modPPGN.Statics.ItemizedTable = false;
				modPPGN.Statics.LeasedTable = false;
				ReloadAccount(lngAcct);
				boolDataChanged = false;
			}
		}

		private void mnuPrintBETEApp_Click(object sender, System.EventArgs e)
		{
			int intYear;
			intYear = modGlobal.Statics.CustomizeStuff.CurrentBETEYear;
			if (intYear == 0)
				intYear = DateTime.Today.Year;
			int intOwner;
			int intBusiness;
			intOwner = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("BETEAppOwnerSetting", "PP", FCConvert.ToString(0)))));
			intBusiness = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("BETEAppBusinessSetting", "PP", FCConvert.ToString(4)))));
			bool boolCurrentBETEOnly;
			boolCurrentBETEOnly = FCConvert.CBool(modRegistry.GetRegistryKey("BETEAppIncludeSetting", "PP", FCConvert.ToString(false)));
			string strTemp;
			strTemp = " and account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount);
			// Call rptBETEApplication.Init(intYear, intYear, intOwner, intBusiness, boolCurrentBETEOnly, " mj.account", strTemp)
			cBETEApplicationReport cReport = new cBETEApplicationReport();
			cReport.BusinessSource = intBusiness;
			cReport.CurrentBETEOnly = boolCurrentBETEOnly;
			cReport.EffectiveYear = intYear;
			cReport.OrderByClause = " account";
			cReport.OwnerSource = intOwner;
			cReport.Pending = false;
			cReport.TaxYear = intYear;
			cReport.WhereClause = strTemp;
			cBETEApplicationController cController = new cBETEApplicationController();
			cController.LoadRange(ref cReport);
			rptNewBETEApplication.InstancePtr.Init(ref cReport);
		}
		
		private void mnuPrintInput_Click(object sender, System.EventArgs e)
        {
            var strSQL = "select * ,ppmaster.account as masteraccount from ppmaster where account = " +
                         FCConvert.ToString(modPPGN.Statics.CurrentAccount);
            rptLeasedandItemizedInput.InstancePtr.Start(2, true,strSQL);
		}

		private void mnuPrintLabel_Click(object sender, System.EventArgs e)
		{
			rptCustomLabels.InstancePtr.Close();
			switch (modGlobal.Statics.CustomizeStuff.AccountLabelType)
			{
				case modLabels.CNSTLBLTYPEDYMO30252:
				case modLabels.CNSTLBLTYPEDYMO30256:
				case modLabels.CNSTLBLTYPEDYMO4150:
					{
						//FC:FINAL:MSH - i.issue #1417: restore missing report call
						rptCustomLabels.InstancePtr.Init(false, 2, 1, 1, modGlobal.Statics.CustomizeStuff.AccountLabelType, 0, FCConvert.ToString(modPPGN.Statics.CurrentAccount), FCConvert.ToString(modPPGN.Statics.CurrentAccount), "", 0, modGlobal.Statics.LabelPrinterName, 0, 0, true);
						break;
					}
				default:
					{
						modPPGN.Statics.gboolPrintF2Label = true;
						rptCustomLabels.InstancePtr.Run(false);
						break;
					}
			}
			//end switch
		}

		private void mnuPrintMasterInfo_Click(object sender, System.EventArgs e)
		{
			rptMaster.InstancePtr.Init("select * from ppmaster where account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount));
		}

		private void mnuPrintOriginal_Click(object sender, System.EventArgs e)
        {
            var strSql = "select * ,ppmaster.account as masteraccount from ppmaster where account = " +
                         FCConvert.ToString(modPPGN.Statics.CurrentAccount);
            rptLeasedandItemized.InstancePtr.Start(2, true,strSql);
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			// If strLongScreen = "F" Then Call SaveData
			if (boolFormLoaded)
			{
				gridTranCode.Row = -1;
				SaveData();
			}
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			gridTranCode.Row = -1;
			if (boolFormLoaded)
			{
				if (SaveData())
				{
					mnuExit_Click();
				}
			}
		}

		private void t2kPhone_Change()
		{
			boolDataChanged = true;
		}

		private void txtApt_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtBusinessCode_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtBusinessCode_Enter(object sender, System.EventArgs e)
		{
			modGlobal.Statics.OverType = 1;
			txtBusinessCode.SelectionStart = 0;
			txtBusinessCode.SelectionLength = 1;
		}

		private void txtBusinessCode_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAl:SBE - #1433 - replaced F1 with F9
            //if (KeyCode == Keys.F1)
            if (KeyCode == Keys.F9)
			{
				Helping = true;
				modPPHelp.Statics.intHelpResponse = 0;
				modPPHelp.BusinessCodes();
				//FC:FINAL:MSH - i.issue #1331: ShowDialog replaced by Show(Modal) for correct positioning and initializing control values
				frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				if (modPPHelp.Statics.intHelpResponse != 0)
					txtBusinessCode.Text = Strings.Format(modPPHelp.Statics.intHelpResponse, "000");
				//FC:FINAL:MSH - i.issue #1331: extra code. Form will be closed by user or after selecting value
				//frmHelpCodes.InstancePtr.Hide();
			}
		}

		private void txtExemptCode1_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtExemptCode1_Enter(object sender, System.EventArgs e)
		{
			modGlobal.Statics.OverType = 1;
			txtExemptCode1.SelectionStart = 0;
			txtExemptCode1.SelectionLength = 1;
		}

		private void txtExemptCode1_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAl:SBE - #1433 - replaced F1 with F9
            //if (KeyCode == Keys.F1)
			if (KeyCode == Keys.F9)
			{
				Helping = true;
				modPPHelp.ExemptCodes();
				//FC:FINAL:MSH - i.issue #1331: ShowDialog replaced by Show(Modal) for correct positioning and initializing control values
				frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				if (modPPHelp.Statics.intHelpResponse != 0)
					txtExemptCode1.Text = Strings.Format(modPPHelp.Statics.intHelpResponse, "00");
				//FC:FINAL:MSH - i.issue #1331: extra code. Form will be closed by user or after selecting value
				//frmHelpCodes.InstancePtr.Hide();
			}
		}

		private void txtExemptCode2_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtExemptCode2_Enter(object sender, System.EventArgs e)
		{
			modGlobal.Statics.OverType = 1;
			txtExemptCode2.SelectionStart = 0;
			txtExemptCode2.SelectionLength = 1;
		}

		private void txtExemptCode2_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAl:SBE - #1433 - replaced F1 with F9
            //if (KeyCode == Keys.F1)
            if (KeyCode == Keys.F9)
			{
				Helping = true;
				modPPHelp.ExemptCodes();
				//FC:FINAL:MSH - i.issue #1331: ShowDialog replaced by Show(Modal) for correct positioning and initializing control values
				frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				if (modPPHelp.Statics.intHelpResponse != 0)
					txtExemptCode2.Text = Strings.Format(modPPHelp.Statics.intHelpResponse, "00");
				//FC:FINAL:MSH - i.issue #1331: extra code. Form will be closed by user or after selecting value
				//frmHelpCodes.InstancePtr.Hide();
			}
		}

		private void txtExemption_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtOpen1_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtOpen1_Enter(object sender, System.EventArgs e)
		{
			modGlobal.Statics.OverType = 1;
			txtOpen1.SelectionStart = 0;
			txtOpen1.SelectionLength = 1;
		}

		private void txtOpen2_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtOpen2_Enter(object sender, System.EventArgs e)
		{
			modGlobal.Statics.OverType = 1;
			txtOpen2.SelectionStart = 0;
			txtOpen2.SelectionLength = 1;
		}

		private void txtOwnerID_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtOwnerID.Text) != theAccount.PartyID)
			{
				cParty tParty = null;
				cPartyController tCont = new cPartyController();
				if (!tCont.LoadParty(ref tParty, FCConvert.ToInt32(Conversion.Val(txtOwnerID.Text))))
				{
					e.Cancel = true;
					MessageBox.Show("That party doesn't exist.", "Invalid Party", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtOwnerID.Text = FCConvert.ToString(theAccount.PartyID);
					return;
				}
				theAccount.PartyID = FCConvert.ToInt32(Math.Round(Conversion.Val(txtOwnerID.Text)));
				cParty tOwnerParty = theAccount.OwnerParty();
				tCont.LoadParty(ref tOwnerParty, FCConvert.ToInt32(Conversion.Val(txtOwnerID.Text)));
				ShowOwner();
				ShowAddress();
			}
		}

		private void txtSquareFootage_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtSquareFootage_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtSquareFootage.Text) > 0)
			{
				txtSquareFootage.Text = Strings.Format(txtSquareFootage.Text, "#,###,##0");
			}
			else
			{
				txtSquareFootage.Text = FCConvert.ToString(0);
			}
		}

		private void txtState_Change()
		{
			boolDataChanged = true;
		}

		private void txtStreetCode_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtStreetCode_Enter(object sender, System.EventArgs e)
		{
			modGlobal.Statics.OverType = 1;
			txtStreetCode.SelectionStart = 0;
			txtStreetCode.SelectionLength = 1;
		}

		private void txtStreetCode_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            //FC:FINAl:SBE - #1433 - replaced F1 with F9
            //if (KeyCode == Keys.F1)
			if (KeyCode == Keys.F9)
			{
				Helping = true;
				modPPHelp.Statics.intHelpResponse = 0;
				modPPHelp.StreetCodes();
				//FC:FINAL:MSH - i.issue #1331: ShowDialog replaced by Show(Modal) for correct positioning and initializing control values
				frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				if (modPPHelp.Statics.intHelpResponse != 0)
					txtStreetCode.Text = Strings.Format(modPPHelp.Statics.intHelpResponse, "00");
			}
		}

		private void txtStreetName_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtStreetName_Enter(object sender, System.EventArgs e)
		{
			modGlobal.Statics.OverType = 1;
			txtStreetName.SelectionStart = 0;
			txtStreetName.SelectionLength = 1;
		}

		private void txtStreetNumber_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtStreetNumber_Enter(object sender, System.EventArgs e)
		{
			modGlobal.Statics.OverType = 1;
			txtStreetNumber.SelectionStart = 0;
			txtStreetNumber.SelectionLength = 1;
		}

		private void txtValue_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtZip_Change()
		{
			boolDataChanged = true;
		}

		private void FillBoxes()
		{
			clsDRWrapper clsTemp = new clsDRWrapper();
			string strTemp = "";
			clsDRWrapper rs = new clsDRWrapper();
			theAccount.Account = modPPGN.Statics.CurrentAccount;
			cPPAccountController tCont = new cPPAccountController();
			cPPAccount temp = theAccount;
			tCont.LoadAccount(ref temp, modPPGN.Statics.CurrentAccount);
			theAccount = temp;
			if (modPPGN.Statics.CurrentAccount > 0)
			{
				lblAccount.Text = FCConvert.ToString(modPPGN.Statics.CurrentAccount);
			}
			else
			{
				lblAccount.Text = "0";
			}

			if (modPPGN.Statics.CurrentAccount > 0)
			{
				clsTemp.OpenRecordset("select * from moduleassociation where module = 'PP' and account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount), "CentralData");
				if (!clsTemp.EndOfFile())
				{
					txtREAssociate.Text = FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields_Int32("remasteracct")));
				}
				else
				{
					txtREAssociate.Text = FCConvert.ToString(0);
				}
				if (Strings.Trim(theAccount.Comment) == string.Empty)
				{
					lblComment.Visible = false;
				}
				else
				{
					lblComment.Visible = true;
				}
				txtStreetNumber.Text = FCConvert.ToString(theAccount.StreetNumber);
				txtApt.Text = theAccount.StreetApt;
				txtStreetName.Text = theAccount.Street;
				txtOpen1.Text = theAccount.Open1;
				txtOpen2.Text = theAccount.Open2;
				txtValue.Text = FCConvert.ToString(theAccount.TotValue);
				txtExemption.Text = FCConvert.ToString(theAccount.Exemption);
				txtExemptCode1.Text = FCConvert.ToString(theAccount.Get_ExemptCode(1));
				txtExemptCode2.Text = FCConvert.ToString(theAccount.Get_ExemptCode(2));
				gridTranCode.TextMatrix(0, 0, FCConvert.ToString(theAccount.TranCode));
				txtBusinessCode.Text = FCConvert.ToString(theAccount.BusinessCode);
				txtStreetCode.Text = FCConvert.ToString(theAccount.StreetCode);
				txtSquareFootage.Text = Strings.Format(theAccount.SquareFootage, "#,###,##0");
				clsTemp.OpenRecordset("select * from ppvaluations where valuekey = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount), modPPGN.strPPDatabase);
				if (!clsTemp.EndOfFile())
				{
					// vbPorter upgrade warning: lngTemp As int	OnWrite(short, double)
					int lngTemp = 0;
					int x;
					lngTemp = 0;
					for (x = 1; x <= 9; x++)
					{
						// TODO Get_Fields: Field [cat] not found!! (maybe it is an alias?)
						lngTemp += FCConvert.ToInt32(Conversion.Val(clsTemp.Get_Fields("cat" + FCConvert.ToString(x) + "exempt")));
					}
					// x
					// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
					txtTotWithBETE.Text = Strings.Format(lngTemp + Conversion.Val(rs.Get_Fields("value")), "#,###,###,##0");
					txtTotalExempt.Text = Strings.Format(lngTemp + Conversion.Val(rs.Get_Fields_Int32("exemption")), "#,###,###,##0");
				}
				else
				{
					txtTotWithBETE.Text = txtValue.Text;
					txtTotalExempt.Text = txtExemption.Text;
				}
				ShowOwner();
				ShowAddress();
			}
			else
			{
				ClearBoxes();
			}
		}

		private bool SaveData(bool boolDontShowSaveMsg = false)
		{
			bool SaveData = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper clsSave = new clsDRWrapper();
				clsDRWrapper clsExe = new clsDRWrapper();
				bool boolAddedAccount;
				boolAddedAccount = false;
				SaveData = false;
				if (ValidateMaster() == false)
					return SaveData;
				if (modPPGN.Statics.Adding == true || theAccount.Account < 1)
				{
					boolAddedAccount = true;
				}

				if (Conversion.Val(txtStreetNumber.Text) != 0)
				{
					theAccount.StreetNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(txtStreetNumber.Text)));
				}
				else
				{
					theAccount.StreetNumber = 0;
				}
				if (txtApt.Text.Length > 1)
				{
					txtApt.Text = Strings.Left(txtApt.Text, 1);
				}
				theAccount.StreetApt = Strings.Trim(txtApt.Text);
				theAccount.Street = txtStreetName.Text;
				theAccount.Open1 = txtOpen1.Text;
				theAccount.Open2 = txtOpen2.Text;
				theAccount.TranCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTranCode.TextMatrix(0, 0))));
				theAccount.BusinessCode = FCConvert.ToInt32(Math.Round(Conversion.Val(txtBusinessCode.Text)));
				theAccount.StreetCode = FCConvert.ToInt32(Math.Round(Conversion.Val(txtStreetCode.Text)));
				if (Conversion.Val(txtExemptCode1.Text) != 0)
				{
					theAccount.Set_ExemptCode(1, FCConvert.ToInt32(Math.Round(Conversion.Val(txtExemptCode1.Text))));
				}
				else
				{
					theAccount.Set_ExemptCode(1, 0);
				}
				if (Conversion.Val(txtExemptCode2.Text) != 0)
				{
					theAccount.Set_ExemptCode(2, FCConvert.ToInt32(Math.Round(Conversion.Val(txtExemptCode2.Text))));
				}
				else
				{
					theAccount.Set_ExemptCode(2, 0);
				}
				if (Conversion.Val(txtSquareFootage.Text) > 0)
				{
					if (Information.IsNumeric(txtSquareFootage.Text))
					{
						theAccount.SquareFootage = FCConvert.ToInt32(FCConvert.ToDouble(txtSquareFootage.Text));
					}
					else
					{
						theAccount.SquareFootage = FCConvert.ToInt32(Math.Round(Conversion.Val(txtSquareFootage.Text)));
					}
				}
				else
				{
					theAccount.SquareFootage = 0;
				}
				// rs.Update
				cPPAccountController tCont = new cPPAccountController();
                theAccount.Account = modPPGN.Statics.CurrentAccount;
				//FC:FINAL:DSE A property cannot be passed as a ref parameter
				//tCont.SaveAccount(ref theAccount);
				cPPAccount temp = theAccount;
				tCont.SaveAccount(ref temp);
				theAccount = temp;
				modPPGN.Statics.CurrentAccount = theAccount.Account;
				if (modPPGN.Statics.Adding == true)
				{
					modPPGN.Statics.Adding = false;
					clsExe.Execute("insert into ppvaluations (valuekey) values (" + FCConvert.ToString(modPPGN.Statics.CurrentAccount) + ")", modPPGN.strPPDatabase);
					modGlobalFunctions.AddCYAEntry_80("PP", "Created New Account", FCConvert.ToString(theAccount.Account), "Master Screen");
					MessageBox.Show("New account was saved.  Account Number = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount));
				}
				boolDataChanged = false;
				SaveData = true;
				// Put Last Accessed Account into work record
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "ppLastAccountNumber", FCConvert.ToString(modPPGN.Statics.CurrentAccount));
				modPPGN.Statics.Adding = false;
				if (!boolDontShowSaveMsg)
				{
					MessageBox.Show("Save Complete", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				return SaveData;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SaveData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveData;
		}

		private void ClearBoxes()
		{
			/* object obj; */
			lblAccount.Text = "000000";
			theAccount.Clear();
			var controls = this.GetAllControls();
			foreach (Control obj in controls)
			{
				if (obj is FCTextBox)
					obj.Text = "";
			}
		}

		private bool ValidateMaster()
		{
			bool ValidateMaster = false;
			ValidateMaster = true;
			// ValidateMaster = False
			if (modGlobal.Statics.CustomizeStuff.boolRegionalTown)
			{
				int lngTemp = 0;
				lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTranCode.TextMatrix(0, 0))));
				if (lngTemp < 1)
				{
					lngTemp = frmPickRegionalTown.InstancePtr.Init(-1, false, "A town has not been specified");
					if (lngTemp > 0)
					{
						gridTranCode.TextMatrix(0, 0, FCConvert.ToString(lngTemp));
					}
				}
			}
			if (FCConvert.ToDouble(Strings.Trim(FCConvert.ToString(theAccount.PartyID))) < 1)
			{
				MessageBox.Show("Warning.  You have not selected an owner.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			else
			{
				ValidateMaster = true;
			}
			return ValidateMaster;
		}

		private void IncreaseNextAccountByOne()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsDRWrapper clsTemp = new clsDRWrapper();
			int x;
			x = 0;
			clsLoad.OpenRecordset("select account from ppmaster order by account", modPPGN.strPPDatabase);
			if (!clsLoad.EndOfFile())
			{
				while (!clsLoad.EndOfFile())
				{
					x += 1;
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					if (x < clsLoad.Get_Fields("account"))
					{
						clsTemp.OpenRecordset("select account from ppmaster where account = " + FCConvert.ToString(x), modPPGN.strPPDatabase);
						if (clsTemp.EndOfFile())
						{
							modPPCalculate.Statics.NextPPAccount = x;
							return;
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
							x = FCConvert.ToInt32(clsLoad.Get_Fields("account"));
						}
					}
					clsLoad.MoveNext();
				}
				modPPCalculate.Statics.NextPPAccount = x + 1;
			}
			else
			{
				modPPCalculate.Statics.NextPPAccount = 1;
			}
		}

		public void HandlePartialPermission(ref int lngFuncID)
		{
			// takes a function number as a parameter
			// Then gets all the child functions that belong to it and
			// disables the appropriate controls
			clsDRWrapper clsChildList = new clsDRWrapper();
			string strPerm = "";
			int lngChild = 0;
			bool booleditaddresses = false;
			bool booleditvalues = false;
			/* Control ctl = new Control(); */
			modGlobalConstants.Statics.clsSecurityClass.Get_Children(ref clsChildList, ref lngFuncID);
			while (!clsChildList.EndOfFile())
			{
				lngChild = FCConvert.ToInt32(clsChildList.GetData("childid"));
				strPerm = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(lngChild));
				switch (lngChild)
				{
					case modSecurity.CALCULATEMENUSECURITY:
						{
							if (strPerm == "F")
							{
								mnuCalculateDisplay.Enabled = true;
								cmdCalculateDisplay.Visible = true;
							}
							else
							{
								mnuCalculateDisplay.Enabled = false;
								cmdCalculateDisplay.Visible = false;
							}
							break;
						}
					case modSecurity.EDITMASTERVALUESMENU:
						{
							if (strPerm == "F")
							{
								booleditvalues = true;
							}
							else
							{
								booleditvalues = false;
							}
							break;
						}
					case modSecurity.ITEMIZEDSCREENMENU:
						{
							if (strPerm == "F")
							{
								cmdItemized.Enabled = true;
							}
							else
							{
								cmdItemized.Enabled = false;
							}
							break;
						}
					case modSecurity.LEASEDSCREENMENU:
						{
							if (strPerm == "F")
							{
								cmdLeased.Enabled = true;
							}
							else
							{
								cmdLeased.Enabled = false;
							}
							break;
						}
					case modSecurity.ADDNEWACCOUNTMENU:
						{
							if (strPerm == "F")
							{
								mnuAddAccount.Enabled = true;
							}
							else
							{
								mnuAddAccount.Enabled = false;
							}
							break;
						}
					case modSecurity.DELETEACCOUNTSMENU:
						{
							if (strPerm == "F")
							{
								mnuDeleteAccount.Enabled = true;
							}
							else
							{
								mnuDeleteAccount.Enabled = false;
							}
							break;
						}
					case modSecurity.EDITADDRESSESMENU:
						{
							if (strPerm == "F")
							{
								booleditaddresses = true;
							}
							else
							{
								booleditaddresses = false;
							}
							break;
						}
				}
				//end switch
				clsChildList.MoveNext();
			}
			if (!booleditaddresses || !booleditvalues)
			{
				var controls = this.GetAllControls();
				foreach (Control ctl in controls)
				{
					if (!booleditaddresses)
					{
						if (FCConvert.ToString(ctl.Tag) == "address")
						{
							ctl.Enabled = false;
						}
					}
					if (!booleditvalues)
					{
						if (FCConvert.ToString(ctl.Tag) == "values")
						{
							ctl.Enabled = false;
						}
					}
				}
			}
		}

		private void GetCostData()
		{
			// vbPorter upgrade warning: xx As object	OnWrite(short, object)
			int xx;
			clsDRWrapper rsCost = new clsDRWrapper();
			rsCost.OpenRecordset("SELECT * FROM RatioTrends", modPPGN.strPPDatabase);
			// .MoveFirst
			for (xx = 1; xx <= 9; xx++)
			{
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				xx = rsCost.Get_Fields("Type");
				// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
				modPPCalculate.Statics.HighPct[xx] = FCConvert.ToInt32(Math.Round(Conversion.Val(rsCost.Get_Fields("High"))));
				// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
				modPPCalculate.Statics.LowPct[xx] = FCConvert.ToInt32(Math.Round(Conversion.Val(rsCost.Get_Fields("Low"))));
				modPPCalculate.Statics.Exponent[xx] = Conversion.Val(rsCost.Get_Fields_Double("Exponent"));
				// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
				modPPCalculate.Statics.DefLife[xx] = FCConvert.ToInt32(Math.Round(Conversion.Val(rsCost.Get_Fields("Life"))));
				modPPCalculate.Statics.DepMeth[xx] = FCConvert.ToString(rsCost.Get_Fields_String("SD"));
				// TODO Get_Fields: Check the table for the column [Trend] and replace with corresponding Get_Field method
				modPPCalculate.Statics.TrendCode[xx] = FCConvert.ToString(rsCost.Get_Fields("Trend"));
				modPPCalculate.Statics.DefDesc[xx] = FCConvert.ToString(rsCost.Get_Fields_String("Description"));
				rsCost.MoveNext();
			}
			// xx
		}

		private void ReloadAccount(int lngAccount)
		{
			try
			{
				// On Error GoTo ErrorHandler
				modPPGN.Statics.CurrentAccount = lngAccount;
				FillBoxes();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ReloadAccount", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		
		private void ShowOwner()
		{
			if (theAccount.PartyID > 0)
			{
				if (!(theAccount.OwnerParty() == null))
				{
					lblOwner1.Text = theAccount.OwnerParty().FullName;
				}
				else
				{
					lblOwner1.Text = "";
				}
				txtOwnerID.Text = FCConvert.ToString(theAccount.PartyID);
			}
			else
			{
				lblOwner1.Text = "";
				txtOwnerID.Text = FCConvert.ToString(0);
			}
		}

		private void ShowAddress()
		{
			if (!(theAccount.OwnerParty() == null))
			{
				cPartyAddress tAdd;
				tAdd = theAccount.OwnerParty().GetPrimaryAddress();
				if (!(tAdd == null))
				{
					lblAddress.Text = tAdd.GetFormattedAddress();
				}
				else
				{
					lblAddress.Text = "";
				}
			}
			else
			{
				lblAddress.Text = "";
			}
		}

		private void cmdRemoveOwner_Click(object sender, System.EventArgs e)
		{
			theAccount.PartyID = 0;
			if (!(theAccount.OwnerParty() == null))
			{
				theAccount.OwnerParty().Clear();
			}
			ShowOwner();
			ShowAddress();
		}

		private void cmdEditOwner_Click(object sender, System.EventArgs e)
		{
			if (!(theAccount == null))
			{
				if (theAccount.PartyID > 0)
				{
					int lngReturn;
					int lngPartyID = theAccount.PartyID;
					lngReturn = frmEditCentralParties.InstancePtr.Init(ref lngPartyID);
					theAccount.PartyID = lngPartyID;
					cPartyController tCont = new cPartyController();
					cParty tOwnerParty = theAccount.OwnerParty();
					tCont.LoadParty(ref tOwnerParty, theAccount.PartyID);
					ShowOwner();
					ShowAddress();
				}
			}
		}

		private void cmdSearchOwner_Click(object sender, System.EventArgs e)
		{
			int lngPrice;
			int intType;
			int intFinancing;
			int intVerified;
			int intValidity;
			DateTime dtDate;
			clsDRWrapper clsGroup = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				int lngReturnID;
				lngReturnID = frmCentralPartySearch.InstancePtr.Init();
				cPartyController tCont = new cPartyController();
				cParty tOwnerParty = theAccount.OwnerParty();
				tCont.LoadParty(ref tOwnerParty, lngReturnID);
				theAccount.PartyID = lngReturnID;
				ShowOwner();
				ShowAddress();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SearchOwner", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

        private void MnuPrintInventoryListing_Click(object sender, EventArgs e)
        {
            var strSQL =
                "select *,ppmaster.account from ppmaster inner join ppvaluations on (ppmaster.account = ppvaluations.valuekey) where account = " +
                modPPGN.Statics.CurrentAccount;
            rptInventoryListing.InstancePtr.Init(strSQL);
        }
    }
}
