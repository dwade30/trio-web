﻿namespace TWPP0000
{
	/// <summary>
	/// Summary description for srptLeasedOutItemizednput.
	/// </summary>
	partial class srptLeasedOutItemizednput
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptLeasedOutItemizednput));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtLine = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTY = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRB = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLeasedTo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemptYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			((System.ComponentModel.ISupportInitialize)(this.txtLine)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTY)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLeasedTo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtLine,
				this.txtCD,
				this.txtQTY,
				this.txtDescription,
				this.txtCost,
				this.txtRB,
				this.txtLeasedTo,
				this.txtExemptYear
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			// 
			// txtLine
			// 
			this.txtLine.Height = 0.1875F;
			this.txtLine.Left = 0F;
			this.txtLine.Name = "txtLine";
			this.txtLine.Style = "font-family: \'Tahoma\'";
			this.txtLine.Text = "Field1";
			this.txtLine.Top = 0F;
			this.txtLine.Width = 0.5F;
			// 
			// txtCD
			// 
			this.txtCD.Height = 0.1875F;
			this.txtCD.Left = 1.5625F;
			this.txtCD.Name = "txtCD";
			this.txtCD.Style = "font-family: \'Tahoma\'";
			this.txtCD.Text = "Field2";
			this.txtCD.Top = 0F;
			this.txtCD.Width = 0.3125F;
			// 
			// txtQTY
			// 
			this.txtQTY.Height = 0.1875F;
			this.txtQTY.Left = 1.895833F;
			this.txtQTY.Name = "txtQTY";
			this.txtQTY.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtQTY.Text = "Field3";
			this.txtQTY.Top = 0F;
			this.txtQTY.Width = 0.375F;
			// 
			// txtDescription
			// 
			this.txtDescription.CanShrink = true;
			this.txtDescription.Height = 0.1875F;
			this.txtDescription.Left = 2.333333F;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Style = "font-family: \'Tahoma\'";
			this.txtDescription.Text = "Field4";
			this.txtDescription.Top = 0F;
			this.txtDescription.Width = 4.854167F;
			// 
			// txtCost
			// 
			this.txtCost.Height = 0.1875F;
			this.txtCost.Left = 7.270833F;
			this.txtCost.Name = "txtCost";
			this.txtCost.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtCost.Text = null;
			this.txtCost.Top = 0F;
			this.txtCost.Width = 1.0625F;
			// 
			// txtRB
			// 
			this.txtRB.Height = 0.1875F;
			this.txtRB.Left = 0.5625F;
			this.txtRB.Name = "txtRB";
			this.txtRB.Style = "font-family: \'Tahoma\'";
			this.txtRB.Text = null;
			this.txtRB.Top = 0F;
			this.txtRB.Width = 0.25F;
			// 
			// txtLeasedTo
			// 
			this.txtLeasedTo.Height = 0.1875F;
			this.txtLeasedTo.Left = 9.1875F;
			this.txtLeasedTo.Name = "txtLeasedTo";
			this.txtLeasedTo.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtLeasedTo.Text = null;
			this.txtLeasedTo.Top = 0F;
			this.txtLeasedTo.Width = 0.6354167F;
			// 
			// txtExemptYear
			// 
			this.txtExemptYear.Height = 0.1875F;
			this.txtExemptYear.Left = 0.9583333F;
			this.txtExemptYear.Name = "txtExemptYear";
			this.txtExemptYear.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtExemptYear.Text = null;
			this.txtExemptYear.Top = 0F;
			this.txtExemptYear.Width = 0.4375F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label9,
				this.Label12,
				this.Label13,
				this.Label14
			});
			this.ReportHeader.Height = 0.6666667F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.958333F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.Label1.Text = "-------------- Leased Out Not Claimed Description --------------";
			this.Label1.Top = 0F;
			this.Label1.Width = 6F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.19F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label2.Text = "Line #";
			this.Label2.Top = 0.28125F;
			this.Label2.Width = 0.5F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 1.5625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label3.Text = "CD";
			this.Label3.Top = 0.28125F;
			this.Label3.Width = 0.25F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 1.895833F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Label4.Text = "QTY";
			this.Label4.Top = 0.28125F;
			this.Label4.Width = 0.375F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 2.333333F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label5.Text = "Description";
			this.Label5.Top = 0.28125F;
			this.Label5.Width = 1.75F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 7.645833F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Label9.Text = "Cost";
			this.Label9.Top = 0.28125F;
			this.Label9.Width = 0.6875F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.19F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0.5625F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label12.Text = "RB";
			this.Label12.Top = 0.28125F;
			this.Label12.Width = 0.3125F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.34375F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 9.1875F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label13.Text = "Leased To";
			this.Label13.Top = 0.28125F;
			this.Label13.Width = 0.6354167F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.333F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 0.826F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label14.Text = "Exempt Year";
			this.Label14.Top = 0.281F;
			this.Label14.Width = 0.652F;
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// ActiveReport1
			// 
			this.MasterReport = false;
			this.Name = "ActiveReport1";
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 9.927083F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.txtLine)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTY)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLeasedTo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTY;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRB;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLeasedTo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemptYear;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
	}
}
