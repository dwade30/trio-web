﻿namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptAssessByCode.
	/// </summary>
	partial class rptAssessByCode
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptAssessByCode));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.SubBillingBusCode = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.SubBillingStreetCode = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.SubBillingTranCode = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.SubCurrentBusCode = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.SubCurrentStreetCode = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.SubCurrentTranCode = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label3,
				this.SubBillingBusCode,
				this.SubBillingStreetCode,
				this.SubBillingTranCode,
				this.Label4,
				this.SubCurrentBusCode,
				this.SubCurrentStreetCode,
				this.SubCurrentTranCode
			});
			this.Detail.Height = 1.041667F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMuni,
				this.txtTime,
				this.txtDate,
				this.txtPage,
				this.Label1,
				this.Label2
			});
			this.PageHeader.Height = 0.59375F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtMuni
			// 
			this.txtMuni.Height = 0.1875F;
			this.txtMuni.HyperLink = null;
			this.txtMuni.Left = 0F;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Style = "";
			this.txtMuni.Text = null;
			this.txtMuni.Top = 0F;
			this.txtMuni.Width = 1.375F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.HyperLink = null;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.1875F;
			this.txtTime.Width = 1.375F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.HyperLink = null;
			this.txtDate.Left = 6.0625F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.375F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.HyperLink = null;
			this.txtPage.Left = 6.0625F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.1875F;
			this.txtPage.Width = 1.375F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
            this.Label1.HyperLink = null;
			this.Label1.Left = 1.4375F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.Label1.Text = "Personal Property";
			this.Label1.Top = 0F;
			this.Label1.Width = 4.5625F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.21875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 1.4375F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.Label2.Text = "Assessment Summary By Codes";
			this.Label2.Top = 0.1875F;
			this.Label2.Width = 4.5625F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 1.5625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold; text-align: center";
			this.Label3.Text = "Billing Amounts";
			this.Label3.Top = 0.03125F;
			this.Label3.Width = 2.875F;
			// 
			// SubBillingBusCode
			// 
			this.SubBillingBusCode.CloseBorder = false;
			this.SubBillingBusCode.Height = 0.03125F;
			this.SubBillingBusCode.Left = 0F;
			this.SubBillingBusCode.Name = "SubBillingBusCode";
			this.SubBillingBusCode.Report = null;
			this.SubBillingBusCode.Top = 0.21875F;
			this.SubBillingBusCode.Width = 7.4375F;
			// 
			// SubBillingStreetCode
			// 
			this.SubBillingStreetCode.CloseBorder = false;
			this.SubBillingStreetCode.Height = 0.03125F;
			this.SubBillingStreetCode.Left = 0F;
			this.SubBillingStreetCode.Name = "SubBillingStreetCode";
			this.SubBillingStreetCode.Report = null;
			this.SubBillingStreetCode.Top = 0.28125F;
			this.SubBillingStreetCode.Width = 7.4375F;
			// 
			// SubBillingTranCode
			// 
			this.SubBillingTranCode.CloseBorder = false;
			this.SubBillingTranCode.Height = 0.03125F;
			this.SubBillingTranCode.Left = 0F;
			this.SubBillingTranCode.Name = "SubBillingTranCode";
			this.SubBillingTranCode.Report = null;
			this.SubBillingTranCode.Top = 0.34375F;
			this.SubBillingTranCode.Width = 7.4375F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 1.5625F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-weight: bold; text-align: center";
			this.Label4.Text = "Current Amounts";
			this.Label4.Top = 0.65625F;
			this.Label4.Width = 2.875F;
			// 
			// SubCurrentBusCode
			// 
			this.SubCurrentBusCode.CloseBorder = false;
			this.SubCurrentBusCode.Height = 0.03125F;
			this.SubCurrentBusCode.Left = 0F;
			this.SubCurrentBusCode.Name = "SubCurrentBusCode";
			this.SubCurrentBusCode.Report = null;
			this.SubCurrentBusCode.Top = 0.875F;
			this.SubCurrentBusCode.Width = 7.4375F;
			// 
			// SubCurrentStreetCode
			// 
			this.SubCurrentStreetCode.CloseBorder = false;
			this.SubCurrentStreetCode.Height = 0.03125F;
			this.SubCurrentStreetCode.Left = 0F;
			this.SubCurrentStreetCode.Name = "SubCurrentStreetCode";
			this.SubCurrentStreetCode.Report = null;
			this.SubCurrentStreetCode.Top = 0.9375F;
			this.SubCurrentStreetCode.Width = 7.4375F;
			// 
			// SubCurrentTranCode
			// 
			this.SubCurrentTranCode.CloseBorder = false;
			this.SubCurrentTranCode.Height = 0.03125F;
			this.SubCurrentTranCode.Left = 0F;
			this.SubCurrentTranCode.Name = "SubCurrentTranCode";
			this.SubCurrentTranCode.Report = null;
			this.SubCurrentTranCode.Top = 1F;
			this.SubCurrentTranCode.Width = 7.4375F;
			// 
			// rptAssessByCode
			// 
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.479167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubBillingBusCode;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubBillingStreetCode;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubBillingTranCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubCurrentBusCode;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubCurrentStreetCode;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubCurrentTranCode;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
