﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Core;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptNameLocStreetBus.
	/// </summary>
	public partial class rptNameLocStreetBus : BaseSectionReport
	{
        private clsDRWrapper rsLoad = new clsDRWrapper();

		public rptNameLocStreetBus()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Name Location Open Report";
		}

        public void Init(string strsql)
        {
            rsLoad.OpenRecordset(strsql, "PersonalProperty");
            if (rsLoad.EndOfFile())
            {
                MessageBox.Show("No records found");
                this.Unload();
                return;
            }
            frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "NameLocStreetBus");
        }

		public static rptNameLocStreetBus InstancePtr
		{
			get
			{
				return (rptNameLocStreetBus)Sys.GetInstance(typeof(rptNameLocStreetBus));
			}
		}

		protected rptNameLocStreetBus _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsLoad?.Dispose();
                rsLoad = null;
				rsBus_AutoInitialized?.Dispose();
				rsStreet_AutoInitialized?.Dispose();
                rsBus_AutoInitialized = null;
                rsStreet_AutoInitialized = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptNameLocStreetBus	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private int intPage;
		private int intAccount;
		private bool boolPrint;
		private string strName = "";
		private string strLocation = "";
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsDRWrapper rsBus = new clsDRWrapper();
		private clsDRWrapper rsBus_AutoInitialized;

		private clsDRWrapper rsBus
		{
			get
			{
				if (rsBus_AutoInitialized == null)
				{
					rsBus_AutoInitialized = new clsDRWrapper();
				}
				return rsBus_AutoInitialized;
			}
			set
			{
				rsBus_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsDRWrapper rsStreet = new clsDRWrapper();
		private clsDRWrapper rsStreet_AutoInitialized;

		private clsDRWrapper rsStreet
		{
			get
			{
				if (rsStreet_AutoInitialized == null)
				{
					rsStreet_AutoInitialized = new clsDRWrapper();
				}
				return rsStreet_AutoInitialized;
			}
			set
			{
				rsStreet_AutoInitialized = value;
			}
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			clsDRWrapper clsTemp = new clsDRWrapper();
			string strO1 = "";
			string strO2 = "";
			Fields.Add("AccountNumber");
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			ShowRecord:
			;
			eArgs.EOF = rsLoad.EndOfFile();
			if (eArgs.EOF)
				return;
			txttime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			if (FCConvert.ToInt32(rsLoad.GetData("MasterAccount")) == intAccount)
			{
				if (!rsLoad.EndOfFile())
				{
					rsLoad.MoveNext();
				}
				goto ShowRecord;
			}
			txtName.Text = rsLoad.GetData("Name").ToString();
			txtLocation.Text = rsLoad.GetData("StreetNumber") + rsLoad.Get_Fields_String("streetapt") + " " + rsLoad.GetData("Street");
			txtAccount.Text = rsLoad.GetData("MasterAccount").ToString();
			if (rsBus.FindFirstRecord("Businesscode", Conversion.Val(rsLoad.Get_Fields_Int32("businesscode"))))
			{
				txtOpen1.Text = Strings.Left(rsLoad.Get_Fields_Int32("businesscode") + "     ", 5) + rsBus.Get_Fields_String("businesstype");
			}
			else
			{
				txtOpen1.Text = rsLoad.GetData("businesscode").ToString();
			}
			// TODO Get_Fields: Check the table for the column [streetcode] and replace with corresponding Get_Field method
			if (rsStreet.FindFirstRecord("Streetcode", Conversion.Val(rsLoad.Get_Fields("streetcode"))))
			{
				// TODO Get_Fields: Check the table for the column [streetcode] and replace with corresponding Get_Field method
				txtOpen2.Text = Strings.Left(rsLoad.Get_Fields("streetcode") + "    ", 4) + rsStreet.Get_Fields_String("streetname");
			}
			else
			{
				txtOpen2.Text = rsLoad.GetData("streetcode").ToString();
			}
			intAccount = FCConvert.ToInt32(rsLoad.GetData("MasterAccount"));
			if (!rsLoad.EndOfFile())
				rsLoad.MoveNext();
			eArgs.EOF = false;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//this.Printer.RenderMode = 1;
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "M/dd/yyyy");
			txtTitle.Text = "Name / Location / Business / Street";
			txttime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			rsBus.OpenRecordset("select * from BUSINESSCODES order by businesscode", modPPGN.strPPDatabase);
			rsStreet.OpenRecordset("select * from streetcodes order by streetcode", modPPGN.strPPDatabase);
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			intPage += 1;
			txtPage.Text = "Page " + FCConvert.ToString(intPage);
		}

		private void rptNameLocStreetBus_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptNameLocStreetBus properties;
			//rptNameLocStreetBus.Caption	= "Name Location Open Report";
			//rptNameLocStreetBus.Icon	= "rptNameLocStreetBus.dsx":0000";
			//rptNameLocStreetBus.Left	= 0;
			//rptNameLocStreetBus.Top	= 0;
			//rptNameLocStreetBus.Width	= 12120;
			//rptNameLocStreetBus.Height	= 8595;
			//rptNameLocStreetBus.StartUpPosition	= 2;
			//rptNameLocStreetBus.SectionData	= "rptNameLocStreetBus.dsx":058A;
			//End Unmaped Properties
		}
	}
}
