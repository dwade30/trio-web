//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmPPCostRatios.
	/// </summary>
	partial class frmPPCostRatios : BaseForm
	{
		public fecherFoundation.FCTextBox txtRatio;
		public fecherFoundation.FCButton cmdQuit;
		public fecherFoundation.FCButton cmdSave;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCTextBox txtOpen2;
		public fecherFoundation.FCTextBox txtOpen1;
		public fecherFoundation.FCGrid vs;
		public fecherFoundation.FCGrid gridTownCode;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuQuit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPPCostRatios));
			this.txtRatio = new fecherFoundation.FCTextBox();
			this.cmdQuit = new fecherFoundation.FCButton();
			this.cmdSave = new fecherFoundation.FCButton();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.Label2 = new fecherFoundation.FCLabel();
			this.txtOpen2 = new fecherFoundation.FCTextBox();
			this.txtOpen1 = new fecherFoundation.FCTextBox();
			this.vs = new fecherFoundation.FCGrid();
			this.gridTownCode = new fecherFoundation.FCGrid();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vs)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridTownCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			this.BottomPanel.Size = new System.Drawing.Size(898, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtRatio);
			this.ClientArea.Controls.Add(this.cmdQuit);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.txtOpen2);
			this.ClientArea.Controls.Add(this.txtOpen1);
			this.ClientArea.Controls.Add(this.vs);
			this.ClientArea.Controls.Add(this.gridTownCode);
			this.ClientArea.Controls.Add(this.Label4);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(898, 520);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdPrint);
			this.TopPanel.Size = new System.Drawing.Size(898, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(330, 30);
			this.HeaderText.Text = "Certified Ratio / Opens Table";
			// 
			// txtRatio
			// 
			this.txtRatio.AutoSize = false;
			this.txtRatio.BackColor = System.Drawing.SystemColors.Window;
			this.txtRatio.LinkItem = null;
			this.txtRatio.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtRatio.LinkTopic = null;
			this.txtRatio.Location = new System.Drawing.Point(133, 30);
			this.txtRatio.Name = "txtRatio";
			this.txtRatio.Size = new System.Drawing.Size(73, 40);
			this.txtRatio.TabIndex = 1;
			this.txtRatio.TextChanged += new System.EventHandler(this.txtRatio_TextChanged);
			// 
			// cmdQuit
			// 
			this.cmdQuit.AppearanceKey = "toolbarButton";
			this.cmdQuit.Location = new System.Drawing.Point(632, 607);
			this.cmdQuit.Name = "cmdQuit";
			this.cmdQuit.Size = new System.Drawing.Size(99, 23);
			this.cmdQuit.TabIndex = 8;
			this.cmdQuit.Text = "Quit";
			this.cmdQuit.Visible = false;
			this.cmdQuit.Click += new System.EventHandler(this.cmdQuit_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.ForeColor = System.Drawing.Color.White;
			this.cmdSave.Location = new System.Drawing.Point(302, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(110, 48);
			this.cmdSave.TabIndex = 9;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.Label2);
			this.Frame1.Location = new System.Drawing.Point(534, 506);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(273, 95);
			this.Frame1.TabIndex = 7;
			this.Frame1.Text = "S/D  =  Depreciation Method";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(20, 30);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(238, 34);
			this.Label2.TabIndex = 0;
			this.Label2.Text = "LABEL2";
			// 
			// txtOpen2
			// 
			this.txtOpen2.AutoSize = false;
			this.txtOpen2.BackColor = System.Drawing.SystemColors.Window;
			this.txtOpen2.LinkItem = null;
			this.txtOpen2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtOpen2.LinkTopic = null;
			this.txtOpen2.Location = new System.Drawing.Point(295, 561);
			this.txtOpen2.MaxLength = 20;
			this.txtOpen2.Name = "txtOpen2";
			this.txtOpen2.Size = new System.Drawing.Size(209, 40);
			this.txtOpen2.TabIndex = 6;
			// 
			// txtOpen1
			// 
			this.txtOpen1.AutoSize = false;
			this.txtOpen1.BackColor = System.Drawing.SystemColors.Window;
			this.txtOpen1.LinkItem = null;
			this.txtOpen1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtOpen1.LinkTopic = null;
			this.txtOpen1.Location = new System.Drawing.Point(295, 501);
			this.txtOpen1.MaxLength = 20;
			this.txtOpen1.Name = "txtOpen1";
			this.txtOpen1.Size = new System.Drawing.Size(209, 40);
			this.txtOpen1.TabIndex = 4;
			// 
			// vs
			// 
			this.vs.AllowSelection = false;
			this.vs.AllowUserToResizeColumns = false;
			this.vs.AllowUserToResizeRows = false;
			this.vs.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vs.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vs.BackColorAlternate = System.Drawing.Color.Empty;
			this.vs.BackColorBkg = System.Drawing.Color.Empty;
			this.vs.BackColorFixed = System.Drawing.Color.Empty;
			this.vs.BackColorSel = System.Drawing.Color.Empty;
			this.vs.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vs.Cols = 10;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vs.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vs.ColumnHeadersHeight = 30;
			this.vs.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vs.DefaultCellStyle = dataGridViewCellStyle2;
			this.vs.DragIcon = null;
			this.vs.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vs.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vs.ExtendLastCol = true;
			this.vs.ForeColorFixed = System.Drawing.Color.Empty;
			this.vs.FrozenCols = 0;
			this.vs.GridColor = System.Drawing.Color.Empty;
			this.vs.GridColorFixed = System.Drawing.Color.Empty;
			this.vs.Location = new System.Drawing.Point(30, 90);
			this.vs.Name = "vs";
			this.vs.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vs.RowHeightMin = 0;
			this.vs.Rows = 50;
			this.vs.ScrollTipText = null;
			this.vs.ShowColumnVisibilityMenu = false;
			this.vs.ShowFocusCell = false;
			this.vs.Size = new System.Drawing.Size(800, 393);
			this.vs.StandardTab = true;
			this.vs.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vs.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vs.TabIndex = 2;
			this.vs.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vs_ValidateEdit);
			this.vs.CurrentCellChanged += new System.EventHandler(this.vs_EnterCell);
			// 
			// gridTownCode
			// 
			this.gridTownCode.AllowSelection = false;
			this.gridTownCode.AllowUserToResizeColumns = false;
			this.gridTownCode.AllowUserToResizeRows = false;
			this.gridTownCode.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.gridTownCode.BackColorAlternate = System.Drawing.SystemColors.Window;
			this.gridTownCode.BackColorBkg = System.Drawing.SystemColors.AppWorkspace;
			this.gridTownCode.BackColorFixed = System.Drawing.SystemColors.Control;
			this.gridTownCode.BackColorSel = System.Drawing.SystemColors.Highlight;
			this.gridTownCode.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.gridTownCode.Cols = 1;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.gridTownCode.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.gridTownCode.ColumnHeadersHeight = 30;
			this.gridTownCode.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.gridTownCode.ColumnHeadersVisible = false;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.gridTownCode.DefaultCellStyle = dataGridViewCellStyle4;
			this.gridTownCode.DragIcon = null;
			this.gridTownCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.gridTownCode.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.gridTownCode.ExtendLastCol = true;
			this.gridTownCode.FixedCols = 0;
			this.gridTownCode.FixedRows = 0;
			this.gridTownCode.ForeColorFixed = System.Drawing.SystemColors.ControlText;
			this.gridTownCode.FrozenCols = 0;
			this.gridTownCode.GridColor = System.Drawing.SystemColors.Control;
			this.gridTownCode.GridColorFixed = System.Drawing.SystemColors.ControlDark;
			this.gridTownCode.HighLight = fecherFoundation.FCGrid.HighLightSettings.flexHighlightNever;
			this.gridTownCode.Location = new System.Drawing.Point(509, 30);
			this.gridTownCode.Name = "gridTownCode";
			this.gridTownCode.RowHeadersVisible = false;
			this.gridTownCode.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.gridTownCode.RowHeightMin = 0;
			this.gridTownCode.Rows = 1;
			this.gridTownCode.ScrollTipText = null;
			this.gridTownCode.ShowColumnVisibilityMenu = false;
			this.gridTownCode.ShowFocusCell = false;
			this.gridTownCode.Size = new System.Drawing.Size(144, 22);
			this.gridTownCode.StandardTab = true;
			this.gridTownCode.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTAbove;
			this.gridTownCode.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.gridTownCode.TabIndex = 11;
			this.gridTownCode.Visible = false;
			this.gridTownCode.ComboCloseUp += new System.EventHandler(this.gridTownCode_ComboCloseUp);
			this.gridTownCode.Leave += new System.EventHandler(this.gridTownCode_Leave);
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(30, 515);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(183, 18);
			this.Label4.TabIndex = 3;
			this.Label4.Text = "USER DEFINABLE FIELD \'OPEN 1\'";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(30, 575);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(193, 18);
			this.Label3.TabIndex = 5;
			this.Label3.Text = "USER DEFINABLE FIELD \'OPEN 2\'";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(61, 18);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "RATIO";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuPrint,
				this.mnuSepar,
				this.mnuSave,
				this.mnuSaveExit,
				this.mnuSepar2,
				this.mnuQuit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuPrint
			// 
			this.mnuPrint.Index = 0;
			this.mnuPrint.Name = "mnuPrint";
			this.mnuPrint.Text = "Print";
			this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 1;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuSave
			// 
			this.mnuSave.Index = 2;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuSave.Text = "Save";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Index = 3;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Text = "Save & Exit";
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// mnuSepar2
			// 
			this.mnuSepar2.Index = 4;
			this.mnuSepar2.Name = "mnuSepar2";
			this.mnuSepar2.Text = "-";
			// 
			// mnuQuit
			// 
			this.mnuQuit.Index = 5;
			this.mnuQuit.Name = "mnuQuit";
			this.mnuQuit.Text = "Exit";
			this.mnuQuit.Click += new System.EventHandler(this.mnuQuit_Click);
			// 
			// cmdPrint
			// 
			this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrint.AppearanceKey = "toolbarButton";
			this.cmdPrint.Location = new System.Drawing.Point(828, 29);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Size = new System.Drawing.Size(42, 24);
			this.cmdPrint.TabIndex = 1;
			this.cmdPrint.Text = "Print";
			this.cmdPrint.Click += new System.EventHandler(this.mnuPrint_Click);
			// 
			// frmPPCostRatios
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(898, 688);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmPPCostRatios";
			this.Text = "Certified Ratio / Opens Table";
			this.Load += new System.EventHandler(this.frmPPCostRatios_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPPCostRatios_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPPCostRatios_KeyPress);
			this.Resize += new System.EventHandler(this.frmPPCostRatios_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vs)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridTownCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdPrint;
	}
}