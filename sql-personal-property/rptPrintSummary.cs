﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;
using fecherFoundation.VisualBasicLayer;
using System.IO;
using TWSharedLibrary;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptPrintSummary.
	/// </summary>
	public partial class rptPrintSummary : BaseSectionReport
	{
		public rptPrintSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Print Summary Range of Accounts";
		}

		public static rptPrintSummary InstancePtr
		{
			get
			{
				return (rptPrintSummary)Sys.GetInstance(typeof(rptPrintSummary));
			}
		}

		protected rptPrintSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPrintSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intCounter;
		string strTemp = "";

		public void Init()
		{
			//FC:FINAL:MSH - i.issue #1330: restore missing report call
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "Summary");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (FCFileSystem.EOF(40))
			{
				eArgs.EOF = true;
				return;
			}
			else
			{
				strTemp = string.Empty;
				strTemp = FCFileSystem.LineInput(40);
			}
			txtData.Text = strTemp;
			eArgs.EOF = false;
		}

		private void ActiveReport_Initialize()
		{
			//FileSystemObject fs = new FileSystemObject();
			// If fs.FileExists("PrintSum.txt") Then
			// Open "PrintSum.txt" For Input As #40
			// Call SetFixedSizeReport(Me, MDIParent.Grid)
			// End If
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
            using (clsDRWrapper clsLoad = new clsDRWrapper())
            {
                int Ratio;
                //this.Printer.RenderMode = 1;
                clsLoad.OpenRecordset("SELECT * FROM PPRatioOpens", "twpp0000.vb1");
                // TODO Get_Fields: Check the table for the column [Ratio] and replace with corresponding Get_Field method
                Ratio = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("Ratio"))));
                strTemp = " x " + FCConvert.ToString(Ratio) + "%";
                if (Ratio < 100)
                {
                    if (Ratio < 10)
                    {
                        strTemp = "    " + "Total" + strTemp;
                    }
                    else
                    {
                        strTemp = "   " + "Total" + strTemp;
                    }
                }
                else if (Ratio > 100)
                {
                    strTemp = "  " + "Total" + strTemp;
                }
                else
                {
                    // ratio is 100
                    strTemp = "        Total";
                }

                // txtPageHeader = "Account        Leased      Itemized        Total        BETE            Override Amount"
                txtPageHeader.Text = "Account   Leased     Itemized" + strTemp +
                                     "         BETE    With BETE    Override Amount";
                //FC:FINAL:MSH - i.issue #1330: incorrect checking of the file existing
                string strP;
                strP = FCFileSystem.CurDir();
                if (Strings.Right(strP, 1) != "\\")
                    strP += "\\";
                //FileInfo fs = new FileInfo("PrintSum.txt");
                if (!File.Exists(strP + "PrintSum.txt"))
                {
                    MessageBox.Show("No data found", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Cancel();
                    this.Close();
                }
                else
                {
                    FCFileSystem.FileOpen(40, "PrintSum.txt", OpenMode.Input, (OpenAccess) (-1), (OpenShare) (-1), -1);
                    //modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
                }
            }
        }

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			frmPrintSummary.InstancePtr.Show(App.MainForm);
            FCFileSystem.FileClose(40);
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
		}

		
	}
}
