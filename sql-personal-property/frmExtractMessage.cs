﻿//Fecher vbPorter - Version 1.0.0.32
using fecherFoundation;
using Wisej.Web;
using Wisej.Core;
using Global;
using System;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmExtractMessage.
	/// </summary>
	public partial class frmExtractMessage : BaseForm
	{
		public frmExtractMessage()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmExtractMessage InstancePtr
		{
			get
			{
				return (frmExtractMessage)Sys.GetInstance(typeof(frmExtractMessage));
			}
		}

		protected frmExtractMessage _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private void frmExtractMessage_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				// esc
				modPPGN.Statics.boolAbortExtract = true;
				Close();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmExtractMessage_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmExtractMessage properties;
			//frmExtractMessage.ScaleWidth	= 5145;
			//frmExtractMessage.ScaleHeight	= 1320;
			//frmExtractMessage.LinkTopic	= "Form1";
			//End Unmaped Properties
			this.Left = (FCGlobal.Screen.Width - frmMessage.InstancePtr.Width) / 2;
			this.Top = (FCGlobal.Screen.Height - frmMessage.InstancePtr.Height) / 2;
		}
	}
}
