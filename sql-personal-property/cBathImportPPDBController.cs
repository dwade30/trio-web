//Fecher vbPorter - Version 1.0.0.93
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;


namespace TWPP0000
{
	public class cBathImportPPDBController
	{

		//=========================================================

		public delegate void ProcessedRecordEventHandler(cPersonalPropertyValues tProp);
		public event ProcessedRecordEventHandler ProcessedRecord;
		public delegate void CreatedAccountEventHandler(cPersonalPropertyValues tProp);
		public event CreatedAccountEventHandler CreatedAccount;
		public delegate void UnmatchedAccountEventHandler(cPersonalPropertyValues tProp);
		public event UnmatchedAccountEventHandler UnmatchedAccount;
		cPartyUtil tPu = new cPartyUtil();
		
		private int[] arParties = null;
		
		public bool SaveCollection(IEnumerable<cBathImportMaster> tCollection, cBathImportPreferences tPrefs)
		{
			bool SaveCollection = false;
			try
			{
				bool boolReturn;
				boolReturn = true;
				if (!(tCollection==null) && !(tPrefs==null)) {
					clsDRWrapper rsSave = new/*AsNew*/ clsDRWrapper();
					if (tPrefs.MarkAsDeleted) {
						rsSave.Execute("update ppmaster set deleted = 'true'", modPPGN.strPPDatabase);
					}


					foreach (cBathImportMaster tMast in tCollection) {
						if (!SaveRecord(tMast, tPrefs)) {
							boolReturn = false;
						}
					}
				}
				SaveCollection = boolReturn;
				return SaveCollection;
			}
			catch (Exception ex)
			{	
				MessageBox.Show("Error Number "+FCConvert.ToString(fecherFoundation.Information.Err(ex).Number)+" "+fecherFoundation.Information.Err(ex).Description+"\n"+"In SaveCollection", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveCollection;
		}

		private bool SaveRecord(cBathImportMaster tMast, cBathImportPreferences tPrefs)
		{
			bool SaveRecord = false;
			try
			{	
				bool boolReturn;
				int lngAccount = 0;
				bool boolNew = false;

				cPersonalPropertyValues tValues = new cPersonalPropertyValues();
				short x;

				cPartyController tPartyCont = new cPartyController();
				cParty tParty;
				cPartyAddress tAddr;

				cCreateGUID tGUID = new cCreateGUID();
				string strOwner1 = "";
				string strOwner2 = "";
				string strAddress1 = "";
				string strAddress2 = "";
				string strCity = "";
				string strState = "";
				string strZip = "";
				string strZip4 = "";
				string strZipCode = "";

				string[] tempArray = null;
				// vbPorter upgrade warning: testParty As cParty	OnWrite(CParty)
				cParty testParty;
				Array.Resize(ref arParties, 1);


				boolReturn = true;
				if (!(tMast==null)) {
					clsDRWrapper rsSave = new clsDRWrapper();
					clsDRWrapper rsVals = new clsDRWrapper();

					string[] strary = null;
					tValues.Account = tMast.IDNum;
					tValues.BETE = tMast.BETETotal;
					tValues.Name = tMast.DoingBusinessAs;
					tValues.NonBETE = tMast.NonBETE;
					tValues.Total = tMast.TotalAppraised;

					rsSave.OpenRecordset("select * from ppmaster where ACCOUNT = "+tMast.IDNum, modPPGN.strPPDatabase);

					if (!rsSave.EndOfFile()) {
						rsSave.Edit();
						lngAccount = Convert.ToInt32(rsSave.Get_Fields("account"));
						if (this.ProcessedRecord != null) this.ProcessedRecord(tValues);
					} else {

						rsSave.AddNew();
						boolNew = true;
						lngAccount = tMast.IDNum;
						if (lngAccount<1) {
							lngAccount = IncreaseNextAccountByOne();
						}


						rsSave.Set_Fields("account", lngAccount);

						rsSave.Set_Fields("DELETED", false);

					}

					rsSave.Set_Fields("open1", tMast.OwnerName);
					
					strOwner1 = tMast.DoingBusinessAs.Trim();
					strAddress1 = tMast.Address1.Trim();
					strAddress2 = tMast.Address2.Trim();
					strCity = tMast.City.Trim();
					strState = tMast.State.Trim();
					strZip = tMast.Zip.Trim();

					if (strOwner1 != "") {
						tParty = new cParty();

						tParty.DateCreated = DateTime.Today;
						tParty.CreatedBy = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
						tParty.FirstName = tMast.DoingBusinessAs.Trim();
						tParty.PartyGUID = tGUID.CreateGUID();
						tParty.PartyType = 1;
						tAddr = new cPartyAddress();
						tAddr.AddressType = "Primary";
						tAddr.Address1 = strAddress1;
						tAddr.Address2 = strAddress2;
						tAddr.Address3 = "";
						tAddr.City = strCity;
						tAddr.State = strState;
						tAddr.Zip = strZip;

						tParty.Addresses.Add(tAddr);

						testParty = tPartyCont.FindFirstMatchingParty(ref tParty);
						if (!(testParty==null)) {
							if (!tPartyCont.PartiesHaveSameAddress(ref tParty, ref testParty)) {
								tPartyCont.SaveParty(ref tParty);
							} else {
								tParty = testParty;
							}
						} else {
							tPartyCont.SaveParty(ref tParty);
						}
						rsSave.Set_Fields("PartyID", tParty.ID);
						Array.Resize(ref arParties, fecherFoundation.Information.UBound(arParties, 1)+1 + 1);
						arParties[arParties.UBound()] = tParty.ID;


					} else {
						rsSave.Set_Fields("PartyID", 0);
					}

					// 

					rsSave.Set_Fields("streetnumber", tMast.StreetNo);
					rsSave.Set_Fields("street", tMast.Street);
					rsSave.Set_Fields("comment", tMast.Comment);
					rsSave.Set_Fields("value", tMast.NonBETE);

					rsVals.OpenRecordset("select * from ppvaluations where valuekey = "+FCConvert.ToString(lngAccount), modPPGN.strPPDatabase);
					if (!rsVals.EndOfFile()) {
						rsVals.Edit();
					} else {
						rsVals.AddNew();
						rsVals.Set_Fields("valuekey", lngAccount);
					}
					rsVals.Set_Fields("category1", tMast.NonBETE);
					rsVals.Set_Fields("cat1exempt", tMast.BETETotal);
					for(x=2; x<=9; x++) {
						rsVals.Set_Fields("category"+x, 0);
						rsVals.Set_Fields("cat"+x+"Exempt", 0);
					} // x
					rsVals.Update();


					tValues.Account = lngAccount;
					tValues.BETE = tMast.BETETotal;
					tValues.Name = tMast.DoingBusinessAs;
					tValues.NonBETE = tMast.NonBETE;
					tValues.Total = tMast.TotalAppraised;
					if (boolNew) {
						if (this.CreatedAccount != null) this.CreatedAccount(tValues);
					}
					if (this.ProcessedRecord != null) this.ProcessedRecord(tValues);
					rsSave.Set_Fields("deleted", false);
					rsSave.Update();
				}
				SaveRecord = boolReturn;
				return SaveRecord;
			}
			catch (Exception ex)
			{	
				MessageBox.Show("Error Number "+FCConvert.ToString(fecherFoundation.Information.Err(ex).Number)+" "+fecherFoundation.Information.Err(ex).Description+"\n"+"In SaveRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveRecord;
		}


		private int IncreaseNextAccountByOne()
		{
			int increaseNextAccountByOne = 0;
			clsDRWrapper clsLoad = new/*AsNew*/ clsDRWrapper();
			clsDRWrapper clsTemp = new/*AsNew*/ clsDRWrapper();
			int x = 0;

			clsLoad.OpenRecordset("select account from ppmaster order by account", modPPGN.strPPDatabase);
			if (!clsLoad.EndOfFile()) {
				while (!clsLoad.EndOfFile()) {
					x += 1;
					if (x<clsLoad.Get_Fields("account")) {
						clsTemp.OpenRecordset("select account from ppmaster where account = "+FCConvert.ToString(x), modPPGN.strPPDatabase);
						if (clsTemp.EndOfFile()) {
							increaseNextAccountByOne = x;
							return increaseNextAccountByOne;
						} else {
							x = Convert.ToInt32(clsLoad.Get_Fields("account"));
						}
					}
					clsLoad.MoveNext();
				}
				increaseNextAccountByOne = x+1;
			} else {
				increaseNextAccountByOne = 1;
			}
			return increaseNextAccountByOne;
		}
	}
}
