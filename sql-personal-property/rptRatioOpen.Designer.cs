﻿namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptRatioOpen.
	/// </summary>
	partial class rptRatioOpen
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptRatioOpen));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHigh = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLow = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExponent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLife = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTrend = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtRatio = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOpen1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOpen2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.txtType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHigh)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLow)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExponent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLife)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTrend)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRatio)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtType,
				this.txtHigh,
				this.txtLow,
				this.txtExponent,
				this.txtLife,
				this.txtSD,
				this.txtTrend,
				this.txtDescription
			});
			this.Detail.Height = 0.2291667F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// txtType
			// 
			this.txtType.CanGrow = false;
			this.txtType.Height = 0.1875F;
			this.txtType.Left = 0.3125F;
			this.txtType.MultiLine = false;
			this.txtType.Name = "txtType";
			this.txtType.Style = "font-family: \'Tahoma\'; text-align: center";
			this.txtType.Text = "Field1";
			this.txtType.Top = 0.03125F;
			this.txtType.Width = 0.4375F;
			// 
			// txtHigh
			// 
			this.txtHigh.CanGrow = false;
			this.txtHigh.Height = 0.1875F;
			this.txtHigh.Left = 0.9375F;
			this.txtHigh.MultiLine = false;
			this.txtHigh.Name = "txtHigh";
			this.txtHigh.Style = "font-family: \'Tahoma\'; text-align: center";
			this.txtHigh.Text = "Field2";
			this.txtHigh.Top = 0.03125F;
			this.txtHigh.Width = 0.4375F;
			// 
			// txtLow
			// 
			this.txtLow.CanGrow = false;
			this.txtLow.Height = 0.1875F;
			this.txtLow.Left = 1.5625F;
			this.txtLow.MultiLine = false;
			this.txtLow.Name = "txtLow";
			this.txtLow.Style = "font-family: \'Tahoma\'; text-align: center";
			this.txtLow.Text = "Field3";
			this.txtLow.Top = 0.03125F;
			this.txtLow.Width = 0.375F;
			// 
			// txtExponent
			// 
			this.txtExponent.CanGrow = false;
			this.txtExponent.Height = 0.1875F;
			this.txtExponent.Left = 2.125F;
			this.txtExponent.MultiLine = false;
			this.txtExponent.Name = "txtExponent";
			this.txtExponent.Style = "font-family: \'Tahoma\'; text-align: center";
			this.txtExponent.Text = "Field4";
			this.txtExponent.Top = 0.03125F;
			this.txtExponent.Width = 0.6875F;
			// 
			// txtLife
			// 
			this.txtLife.CanGrow = false;
			this.txtLife.Height = 0.1875F;
			this.txtLife.Left = 3F;
			this.txtLife.MultiLine = false;
			this.txtLife.Name = "txtLife";
			this.txtLife.Style = "font-family: \'Tahoma\'; text-align: center";
			this.txtLife.Text = "Field5";
			this.txtLife.Top = 0.03125F;
			this.txtLife.Width = 0.4375F;
			// 
			// txtSD
			// 
			this.txtSD.CanGrow = false;
			this.txtSD.Height = 0.1875F;
			this.txtSD.Left = 3.625F;
			this.txtSD.MultiLine = false;
			this.txtSD.Name = "txtSD";
			this.txtSD.Style = "font-family: \'Tahoma\'; text-align: center";
			this.txtSD.Text = "Field6";
			this.txtSD.Top = 0.03125F;
			this.txtSD.Width = 0.4375F;
			// 
			// txtTrend
			// 
			this.txtTrend.CanGrow = false;
			this.txtTrend.Height = 0.1875F;
			this.txtTrend.Left = 4.25F;
			this.txtTrend.MultiLine = false;
			this.txtTrend.Name = "txtTrend";
			this.txtTrend.Style = "font-family: \'Tahoma\'; text-align: center";
			this.txtTrend.Text = "Field7";
			this.txtTrend.Top = 0.03125F;
			this.txtTrend.Width = 0.4375F;
			// 
			// txtDescription
			// 
			this.txtDescription.Height = 0.1875F;
			this.txtDescription.Left = 4.875F;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Style = "font-family: \'Tahoma\'";
			this.txtDescription.Text = "Field8";
			this.txtDescription.Top = 0.03125F;
			this.txtDescription.Width = 2.5625F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.txtRatio,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Label8,
				this.Label9,
				this.Line1,
				this.Field1,
				this.txtMuni,
				this.txtTime,
				this.txtDate,
				this.txtPage
			});
			this.ReportHeader.Height = 1.041667F;
			this.ReportHeader.Name = "ReportHeader";
			this.ReportHeader.Format += new System.EventHandler(this.ReportHeader_Format);
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 3.1875F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label1.Text = "Ratio:";
			this.Label1.Top = 0.53125F;
			this.Label1.Width = 0.5F;
			// 
			// txtRatio
			// 
			this.txtRatio.Height = 0.21875F;
			this.txtRatio.Left = 3.75F;
			this.txtRatio.Name = "txtRatio";
			this.txtRatio.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtRatio.Text = null;
			this.txtRatio.Top = 0.53125F;
			this.txtRatio.Width = 0.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.3125F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label2.Text = "Type";
			this.Label2.Top = 0.8125F;
			this.Label2.Width = 0.4375F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0.9375F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label3.Text = "High";
			this.Label3.Top = 0.8125F;
			this.Label3.Width = 0.4375F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 1.5625F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label4.Text = "Low";
			this.Label4.Top = 0.8125F;
			this.Label4.Width = 0.375F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 2.125F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label5.Text = "Exponent";
			this.Label5.Top = 0.8125F;
			//FC:FINAL:CHN - issue #1405: Not fully visible text.
			// this.Label5.Width = 0.6875F;
			this.Label5.Width = 0.71F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 3F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label6.Text = "Life";
			this.Label6.Top = 0.8125F;
			this.Label6.Width = 0.4375F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 3.625F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label7.Text = "S/D";
			this.Label7.Top = 0.8125F;
			this.Label7.Width = 0.4375F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 4.25F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label8.Text = "Trend";
			this.Label8.Top = 0.8125F;
			this.Label8.Width = 0.4375F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 4.875F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label9.Text = "Description";
			this.Label9.Top = 0.8125F;
			this.Label9.Width = 1.5F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.125F;
			this.Line1.LineWeight = 3F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1F;
			this.Line1.Width = 7.25F;
			this.Line1.X1 = 0.125F;
			this.Line1.X2 = 7.375F;
			this.Line1.Y1 = 1F;
			this.Line1.Y2 = 1F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.21875F;
			this.Field1.Left = 2.1875F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.Field1.Text = "Ratio Opens Table";
			this.Field1.Top = 0.03125F;
			this.Field1.Width = 3.125F;
			// 
			// txtMuni
			// 
			this.txtMuni.Height = 0.19F;
			this.txtMuni.Left = 0.0625F;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Style = "font-family: \'Tahoma\'";
			this.txtMuni.Text = "Field2";
			this.txtMuni.Top = 0.03125F;
			this.txtMuni.Width = 2F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.19F;
			this.txtTime.Left = 0.0625F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'";
			this.txtTime.Text = "Field2";
			this.txtTime.Top = 0.1875F;
			this.txtTime.Width = 2F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.19F;
			this.txtDate.Left = 5.4375F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtDate.Text = "Field2";
			this.txtDate.Top = 0.03125F;
			this.txtDate.Width = 2F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.19F;
			this.txtPage.Left = 5.4375F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtPage.Text = "Page  1";
			this.txtPage.Top = 0.1875F;
			this.txtPage.Width = 2F;
			// 
			// ReportFooter
			// 
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label10,
				this.Label11,
				this.Shape1,
				this.Label12,
				this.Label13,
				this.Label14,
				this.txtOpen1,
				this.txtOpen2
			});
			this.ReportFooter.Height = 0.6458333F;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 0.25F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label10.Text = "User Defined \'Open 1\'";
			this.Label10.Top = 0.09375F;
			this.Label10.Width = 1.5F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 0.25F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label11.Text = "User Defined \'Open 2\'";
			this.Label11.Top = 0.3125F;
			this.Label11.Width = 1.5F;
			// 
			// Shape1
			// 
			this.Shape1.Height = 0.57F;
			this.Shape1.Left = 4.188F;
			this.Shape1.Name = "Shape1";
			this.Shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape1.Top = 0.031F;
			this.Shape1.Width = 2.75F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 4.3125F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label12.Text = "S/D = Depreciation Method";
			this.Label12.Top = 0.03125F;
			this.Label12.Width = 2.375F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.19F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 4.4375F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'";
			this.Label13.Text = "S = Standard";
			this.Label13.Top = 0.21875F;
			this.Label13.Width = 1.25F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.19F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 4.4375F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'";
			this.Label14.Text = "D = Declining Balance";
			this.Label14.Top = 0.40625F;
			this.Label14.Width = 1.5625F;
			// 
			// txtOpen1
			// 
			this.txtOpen1.Height = 0.1875F;
			this.txtOpen1.Left = 1.8125F;
			this.txtOpen1.Name = "txtOpen1";
			this.txtOpen1.Style = "font-family: \'Tahoma\'";
			this.txtOpen1.Text = "Field1";
			this.txtOpen1.Top = 0.09375F;
			this.txtOpen1.Width = 2.1875F;
			// 
			// txtOpen2
			// 
			this.txtOpen2.Height = 0.1875F;
			this.txtOpen2.Left = 1.8125F;
			this.txtOpen2.Name = "txtOpen2";
			this.txtOpen2.Style = "font-family: \'Tahoma\'";
			this.txtOpen2.Text = "Field2";
			this.txtOpen2.Top = 0.3125F;
			this.txtOpen2.Width = 2.1875F;
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// rptRatioOpen
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.Disposed += new System.EventHandler(ActiveReport_Terminate);
			((System.ComponentModel.ISupportInitialize)(this.txtType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHigh)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLow)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExponent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLife)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTrend)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRatio)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHigh;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLow;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExponent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLife;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTrend;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRatio;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOpen1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOpen2;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
