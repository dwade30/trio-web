﻿//Fecher vbPorter - Version 1.0.0.32
using Wisej.Web;
using Wisej.Core;
using Global;
using System;
using fecherFoundation;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmImportItem.
	/// </summary>
	public partial class frmImportItem : BaseForm
	{
		public frmImportItem()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private void Dir1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			File1.Path = Dir1.Path;
		}

		private void Drive1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Dir1.Path = Drive1.Drive;
		}

		private void frmImportItem_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmImportItem_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmImportItem properties;
			//frmImportItem.ScaleWidth	= 6105;
			//frmImportItem.ScaleHeight	= 4095;
			//frmImportItem.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void mnuContinue_Click(object sender, System.EventArgs e)
		{
			frmPickItemFormat.InstancePtr.Init(File1.Path + "\\" + File1.FileName);
			Close();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}
	}
}
