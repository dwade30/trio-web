﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmPPBusinessCodes.
	/// </summary>
	public partial class frmPPBusinessCodes : BaseForm
	{
		public frmPPBusinessCodes()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPPBusinessCodes InstancePtr
		{
			get
			{
				return (frmPPBusinessCodes)Sys.GetInstance(typeof(frmPPBusinessCodes));
			}
		}

		protected frmPPBusinessCodes _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		string strSQL;
		int[] aryDeleted = null;
		int NextNum;
		int lngNumDeleteds;

		private void frmPPBusinessCodes_Activated(object sender, System.EventArgs e)
		{
			if (modGNWork.FormExist(this))
				return;
			FillGrid();
			// menuPPCost.Hide
			vs1.Visible = true;
		}

		private void frmPPBusinessCodes_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				frmPPBusinessCodes.InstancePtr.Close();
			}
		}

		private void frmPPBusinessCodes_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPPBusinessCodes properties;
			//frmPPBusinessCodes.ScaleWidth	= 9060;
			//frmPPBusinessCodes.ScaleHeight	= 7410;
			//frmPPBusinessCodes.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			lngNumDeleteds = 0;
		}

		private void frmPPBusinessCodes_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			if (this.WindowState != FormWindowState.Minimized)
				modGlobal.SaveWindowSize(this);
			// menuPPCost.Show
		}

		private void FillGrid()
		{
			SetGridProperties();
			strSQL = "SELECT * FROM BusinessCodes ORDER BY BusinessCode";
			rs.OpenRecordset(strSQL, modPPGN.strPPDatabase);
			while (!rs.EndOfFile())
			{
				vs1.AddItem(rs.Get_Fields_Int32("businesscode") + "\t" + rs.Get_Fields_String("businesstype" + ""));
				rs.MoveNext();
			}
		}

		private void SetGridProperties()
		{
			vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vs1.Cols = 2;
			vs1.ExtendLastCol = true;
			vs1.TextMatrix(0, 0, "Code");
			vs1.TextMatrix(0, 1, "Description");
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = vs1.WidthOriginal;
			vs1.ColWidth(0, FCConvert.ToInt32(0.12 * GridWidth));
		}

		private void SaveData()
		{
			try
			{
				// On Error GoTo ErrorTag
				int ii;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				vs1.Row = 0;
				////Application.DoEvents();
				if (lngNumDeleteds > 0)
				{
					for (ii = 0; ii <= Information.UBound(aryDeleted, 1) - 1; ii++)
					{
						rs.Execute("delete from businesscodes where businesscode = " + FCConvert.ToString(aryDeleted[ii]), modPPGN.strPPDatabase);
					}
					// ii
				}
				lngNumDeleteds = 0;
				FCUtils.EraseSafe(aryDeleted);
				strSQL = "SELECT * FROM BusinessCodes ORDER BY BusinessCode";
				rs.OpenRecordset(strSQL, modPPGN.strPPDatabase);
				for (ii = 1; ii <= vs1.Rows - 1; ii++)
				{
					if (FCConvert.ToBoolean(vs1.RowData(ii)))
					{
						if (Conversion.Val(vs1.TextMatrix(ii, 0)) > 0)
						{
							rs.FindFirstRecord("BusinessCode", Conversion.Val(vs1.TextMatrix(ii, 0)));
							if (rs.NoMatch)
								rs.AddNew();
							else
								rs.Edit();
							rs.Set_Fields("BusinessCode", vs1.TextMatrix(ii, 0));
							rs.Set_Fields("BusinessType", vs1.TextMatrix(ii, 1));
							rs.Update();
						}
					}
					vs1.RowData(ii, false);
				}
				// ii
				MessageBox.Show("Save Complete", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
			catch (Exception ex)
			{
				// ErrorTag:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
		}

		private void mnuAddCode_Click(object sender, System.EventArgs e)
		{
			vs1.Rows += 1;
			vs1.TopRow = vs1.Rows - 1;
		}

		private void mnuDeleteCode_Click(object sender, System.EventArgs e)
		{
			if (vs1.Row > 0)
			{
				Array.Resize(ref aryDeleted, lngNumDeleteds + 1 + 1);
				aryDeleted[lngNumDeleteds] = FCConvert.ToInt32(Math.Round(Conversion.Val(vs1.TextMatrix(vs1.Row, 0))));
				lngNumDeleteds += 1;
				vs1.RemoveItem(vs1.Row);
			}
		}

		public void mnuDeleteCode_Click()
		{
			mnuDeleteCode_Click(mnuDeleteCode, new System.EventArgs());
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			rptBusinessCodes.InstancePtr.Init(1);
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			vs1.Row = 0;
			SaveData();
		}

		public void mnuSave_Click()
		{
			mnuSave_Click(mnuSave, new System.EventArgs());
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			mnuSave_Click();
			mnuExit_Click();
		}

		private void vs1_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			vs1.EditMaxLength = 38;
		}

		private void vs1_ChangeEdit(object sender, System.EventArgs e)
		{
			if (vs1.Row < 1)
				return;
			vs1.RowData(vs1.Row, true);
		}

		private void vs1_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Delete:
					{
						KeyCode = 0;
						if (vs1.Row > 0)
						{
							mnuDeleteCode_Click();
						}
						break;
					}
			}
			//end switch
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuSave_Click(mnuSave, EventArgs.Empty);
		}

		private void cmdPrint_Click(object sender, EventArgs e)
		{
			mnuPrint_Click(mnuPrint, EventArgs.Empty);
		}

		private void cmdAddCode_Click(object sender, EventArgs e)
		{
			mnuAddCode_Click(mnuAddCode, EventArgs.Empty);
		}

		private void cmdDeleteCode_Click(object sender, EventArgs e)
		{
			mnuDeleteCode_Click(mnuDeleteCode, EventArgs.Empty);
		}
	}
}
