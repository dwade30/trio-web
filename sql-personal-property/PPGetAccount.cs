﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmPPGetAccount.
	/// </summary>
	public partial class frmPPGetAccount : BaseForm
	{
		public frmPPGetAccount()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPPGetAccount InstancePtr
		{
			get
			{
				return (frmPPGetAccount)Sys.GetInstance(typeof(frmPPGetAccount));
			}
		}

		protected frmPPGetAccount _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			cmbHidden.SelectedIndex = 0;
			txtSearch.Text = "";
		}

		public void GetAcctNum()
		{
			cmdGetAccountNumber_Click();
		}

		private void cmdGetAccountNumber_Click(object sender, System.EventArgs e)
		{
			int holdaccount = 0;
			clsDRWrapper rs = new clsDRWrapper();
			string strSQL = "";
			string gotowhere = "";
			// vbPorter upgrade warning: intResp As short, int --> As DialogResult
			DialogResult intResp;
			clsDRWrapper clsTemp = new clsDRWrapper();
			modPPGN.Statics.Adding = false;
			//FC:FINAL:DDU:#i1270 - close forms instead of comment their hide method
			if (modPPGN.Statics.LeasedTable)
			{
				frmPPLeased.InstancePtr.Close();
			}
			if (modPPGN.Statics.ItemizedTable)
			{
				frmPPItemized.InstancePtr.Close();
			}
			//FC:FINAL:MSH - i.issue #1265: unload form instead of hiding
			frmPPMaster.InstancePtr.Close();
			frmPPMasterShort.InstancePtr.Close();
			frmPPMaster.InstancePtr.Unload();
			frmPPMasterShort.InstancePtr.Unload();
			//FC:FINAL:MSH - i.issue #1265: only after disposing Show will work correct at first launch of form
			frmPPMaster.InstancePtr.Dispose();
			frmPPMasterShort.InstancePtr.Dispose();
			if (txtHold.Text == "L")
				gotowhere = "Master";
			if (txtHold.Text == "S")
				gotowhere = "Short";
			if (Conversion.Val(txtGetAccountNumber.Text) != 0)
			{
				holdaccount = FCConvert.ToInt32(FCConvert.ToDouble(txtGetAccountNumber.Text));
				strSQL = "SELECT * FROM PPMaster WHERE Account = " + FCConvert.ToString(holdaccount);
				rs.OpenRecordset(strSQL, modPPGN.strPPDatabase);
				RetryTag:
				;
				object Answer;
				// If CheckLocks(gotowhere, CStr(CurrentAccount)) = True Then
				// Answer = MsgBox("Account " & CurrentAccount & " is locked by User " & UserWithLock & ".", vbRetryCancel + vbExclamation)
				// If Answer = vbRetry Then GoTo RetryTag Else Exit Sub
				// Else
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					if (rs.RecordCount() > 1)
					{
						MessageBox.Show("More than one Personal Property Record has the same Account Number.  CALL TRIO.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
						rs = null;
						cmdQuit_Click();
					}
					if (rs.Get_Fields_Boolean("Deleted") == true || FCConvert.ToInt32(rs.Get_Fields_Boolean("Deleted")) == 1)
					{
						if (!modSecurity.ValidPermissions_6(this, modSecurity.UNDELETEACCOUNTSMENU, false))
						{
							MessageBox.Show("This account is deleted.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
						intResp = MessageBox.Show("This account is deleted." + "\r\n" + "Would you like to undelete it?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
						if (intResp == DialogResult.Yes)
						{
							intResp = MessageBox.Show("This will bring back information and make the account active again." + "\r\n" + "Are you sure you want to un-delete it?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (intResp == DialogResult.Yes)
							{
								modGlobalFunctions.AddCYAEntry_26("PP", "Undeleted Account", holdaccount.ToString());
								modPPGN.Statics.CurrentAccount = holdaccount;
								intResp = MessageBox.Show("Do you wish to restore the information?" + "\r\n" + "If you answer no you will restore the account number only." + "\r\n" + "All information for this account will be gone forever.", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
								if (intResp == DialogResult.Yes)
								{
									rs.Edit();
									rs.Set_Fields("deleted", false);
									rs.Update();
									intResp = MessageBox.Show("Master information will be restored." + "\r\n" + "Do you wish to restore itemized and leased as well?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
									if (intResp == DialogResult.No)
									{
										clsTemp.Execute("delete from ppleased where account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount), "twpp0000.vb1");
										clsTemp.Execute("delete from ppitemized where account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount), "twpp0000.vb1");
										clsTemp.Execute("update ppvaluations set category1 = 0,category2 = 0,category3 = 0,category4 = 0,category5 = 0,category6 = 0,category7 = 0,category8 = 0 ,category9 = 0,totalitemized = 0,totalleased = 0,overrideamount = 0 where valuekey = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount), "twpp0000.vb1");
									}
								}
								else
								{
									rs = null;
									clsTemp.Execute("delete from ppmaster where account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount), "twpp0000.vb1");
									clsTemp.Execute("delete from ppleased where account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount), "twpp0000.vb1");
									clsTemp.Execute("delete from ppitemized where account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount), "twpp0000.vb1");
									clsTemp.Execute("delete from ppvaluations where valuekey = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount), "twpp0000.vb1");
									modPPGN.Statics.Adding = true;
									string vbPorterVar = txtHold.Text;
									if (vbPorterVar == "L")
									{
										// Long Screen
										frmPPMaster.InstancePtr.Show(App.MainForm);
									}
									else if (vbPorterVar == "S")
									{
										// Short Screen
										frmPPMasterShort.InstancePtr.Show(App.MainForm);
									}
									Close();
								}
								// MsgBox ("Account " & CurrentAccount & " has been undeleted and is now active again.")
							}
							else
							{
								txtGetAccountNumber.SelectionStart = 0;
								txtGetAccountNumber.SelectionLength = txtGetAccountNumber.Text.Length;
							}
						}
						else
						{
							txtGetAccountNumber.SelectionStart = 0;
							txtGetAccountNumber.SelectionLength = txtGetAccountNumber.Text.Length;
						}
						return;
					}
					else
					{
						modPPGN.Statics.CurrentAccount = holdaccount;
						string vbPorterVar1 = txtHold.Text;
						if (vbPorterVar1 == "L")
						{
							// Long Screen
							frmPPMaster.InstancePtr.Show(App.MainForm);
							// Call UnlockAllRecordsByUser
							// Call PPLockRecord(User, "Master", CStr(CurrentAccount))
						}
						else if (vbPorterVar1 == "S")
						{
							// Short Screen
							frmPPMasterShort.InstancePtr.Show(App.MainForm);
							// Call UnlockAllRecordsByUser
							// Call PPLockRecord(User, "Short", CStr(CurrentAccount))
						}
						else if (vbPorterVar1 == "C")
						{
							// Calculate Screen
							//MDIParent.InstancePtr.Show();
						}
						if (modPPGN.Statics.CurrentAccount == -1)
							modPPGN.Statics.CurrentAccount = 1;
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "PPLastAccountNumber", FCConvert.ToString(modPPGN.Statics.CurrentAccount));
						// Call WPP.SetData("WPPACCOUNT", PadToString(CurrentAccount, 6))
						// Call SaveLocalVariables
					}
				}
				else
				{
					intResp = MessageBox.Show("Account does not exist yet." + "\r\n" + "Would you like to create it?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (intResp == DialogResult.Yes)
					{
						if (!modSecurity.ValidPermissions_6(this, modSecurity.ADDNEWACCOUNTMENU, false))
						{
							MessageBox.Show("Your permission setting for Adding a New Account is either set to none or is missing.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
						modPPGN.Statics.Adding = true;
						modPPGN.Statics.CurrentAccount = holdaccount;
						rs = null;
						string vbPorterVar2 = txtHold.Text;
						if (vbPorterVar2 == "L")
						{
							// Long Screen
							modGlobalFunctions.AddCYAEntry_80("PP", "Created Specific Account", holdaccount.ToString(), "Master Screen");
							frmPPMaster.InstancePtr.Show(App.MainForm);
						}
						else if (vbPorterVar2 == "S")
						{
							// Short Screen
							modGlobalFunctions.AddCYAEntry_80("PP", "Created Specific Account", holdaccount.ToString(), "Short Screen");
							frmPPMasterShort.InstancePtr.Show(App.MainForm);
						}
						Close();
					}
					return;
				}
				// End If
			}
			else
			{
				if (txtGetAccountNumber.Text == "0")
				{
					// Add new account number
					if (!modSecurity.ValidPermissions_6(this, modSecurity.ADDNEWACCOUNTMENU, false))
					{
						MessageBox.Show("Your permission setting for Adding a New Account iseither set to none or is missing.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					// If GetCurrentAccount >= gintMaxAccounts Then
					// MsgBox "Max Accounts of " & gintMaxAccounts & " has been reached. Please call TRIO Software.", vbExclamation
					// Exit Sub
					// End If
					// 
					modPPGN.Statics.CurrentAccount = -1;
					string vbPorterVar3 = txtHold.Text;
					if (vbPorterVar3 == "L")
					{
						// Long Screen
						frmPPMaster.InstancePtr.Show(App.MainForm);
					}
					else if (vbPorterVar3 == "S")
					{
						// Short Screen
						frmPPMasterShort.InstancePtr.Show(App.MainForm);
					}
				}
				else
				{
					MessageBox.Show("Please enter a valid account number.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			// Unload frmPPGetAccount
		}

		public void cmdGetAccountNumber_Click()
		{
			cmdGetAccountNumber_Click(cmdGetAccountNumber, new System.EventArgs());
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			if (txtHold.Text == "C")
			{
				//MDIParent.InstancePtr.Show();
				frmPPGetAccount.InstancePtr.Close();
			}
			else
			{
				//MDIParent.InstancePtr.Show();
			}
			frmPPGetAccount.InstancePtr.Close();
		}

		public void cmdQuit_Click()
		{
			//cmdQuit_Click(cmdQuit, new System.EventArgs());
		}

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			string strSearchString;
			int GridWidth;
			string strType = "";
			if (cmbHidden.SelectedIndex == 0)
			{
				strType = "Name";
			}
			else if (cmbHidden.SelectedIndex == 1)
			{
				strType = "Street";
			}
			else if (cmbHidden.SelectedIndex == 2)
			{
				strType = "Open1";
			}
			else if (cmbHidden.SelectedIndex == 3)
			{
				strType = "Open2";
			}
			else if (cmbHidden.SelectedIndex == 4)
			{
				strType = "businesscode";
			}
			else
			{
				MessageBox.Show("You must select a field to search by from the 'Search By' box.", "Search By", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (Strings.Trim(txtSearch.Text) == string.Empty && txtSearch.Text != string.Empty)
			{
				txtSearch.Text = "";
			}
			else if (Strings.Trim(txtSearch.Text) == "" && cmbHidden.SelectedIndex != 4)
			{
				MessageBox.Show("You must type a search criteria in the white box.", "Search Criteria", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			// 
			// Fill list box on search form
			string SQL = "";
			clsDRWrapper rs = new clsDRWrapper();
			string strResp1 = "";
			string strResp2 = "";
			strSearchString = txtSearch.Text;
			if (Strings.UCase(strType) == "NAME" || Strings.UCase(strType) == "STREET")
			{
				strSearchString = Strings.Replace(strSearchString, "'", "''", 1, -1, CompareConstants.vbTextCompare);
			}
			string strMasterJoin;
			string strMasterJoinJoin;
			strMasterJoin = modPPGN.GetMasterJoin();
			strMasterJoinJoin = modPPGN.GetMasterJoinForJoin();
			strMasterJoinJoin = "Select * from " + strMasterJoinJoin;
			if (cmbContain.SelectedIndex == 1)
			{
				// SQL = "SELECT * FROM PPMaster WHERE " & strType & " LIKE '*" & strSearchString & "*' ORDER BY " & strType
				SQL = strMasterJoinJoin + " WHERE " + strType + " LIKE '%" + strSearchString + "%' ORDER BY " + strType;
			}
			else
			{
				// SQL = "SELECT * FROM PPMaster WHERE " & strType & " LIKE '" & strSearchString & "*' ORDER BY " & strType
				SQL = strMasterJoinJoin + " WHERE " + strType + " LIKE '" + strSearchString + "%' ORDER BY " + strType;
			}
			if (strType == "Street")
			{
				SQL += ", StreetNumber";
			}
			if (strType == "businesscode")
			{
				strResp1 = Interaction.InputBox("Please enter the minimum business code" + "\r\n" + "Enter a min and max of nothing to search all", "");
				strResp2 = Interaction.InputBox("Please enter the maximum business code", "");
				if (Strings.Trim(strResp1 + "") == string.Empty)
				{
					// SQL = "select * from ppmaster where not deleted = 1"
					SQL = strMasterJoinJoin + " where not deleted = 1";
				}
				else
				{
					if (Strings.Trim(strResp2 + "") == string.Empty)
					{
						// SQL = "select * from ppmaster where not deleted = 1  and businesscode = " & Val(strResp1) & " order by businesscode"
						SQL = strMasterJoinJoin + " where not deleted = 1  and businesscode = " + FCConvert.ToString(Conversion.Val(strResp1)) + " order by businesscode";
					}
					else
					{
						if (Conversion.Val(strResp2) < Conversion.Val(strResp1))
						{
							MessageBox.Show("The maximum you entered was less than the minimum.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
						// SQL = "select * from ppmaster where not deleted = 1 and businesscode between " & Val(strResp1) & " and " & Val(strResp2) & " order by businesscode"
						SQL = strMasterJoinJoin + " where not deleted = 1 and businesscode between " + FCConvert.ToString(Conversion.Val(strResp1)) + " and " + FCConvert.ToString(Conversion.Val(strResp2)) + " order by businesscode";
					}
				}
				// SQL = "select * from ppmaster where businesscode between"
			}
			int intSearchType = 0;
			if (cmbHidden.SelectedIndex == 0)
			{
				intSearchType = 0;
			}
			else if (cmbHidden.SelectedIndex == 1)
			{
				intSearchType = 1;
			}
			else if (cmbHidden.SelectedIndex == 2)
			{
				intSearchType = 2;
			}
			else if (cmbHidden.SelectedIndex == 3)
			{
				intSearchType = 3;
			}
			else if (cmbHidden.SelectedIndex == 4)
			{
				intSearchType = 4;
			}
			//FC:FINAL:MSH - i.issue #1278: the form will be showed correctly only after unloading and disposing
			frmPPSearch.InstancePtr.Unload();
			frmPPSearch.InstancePtr.Dispose();
			frmPPSearch.InstancePtr.Init(SQL, intSearchType, txtHold.Text);
			// If Searching = True Then
			// End If
		}

		public void cmdSearch_Click()
		{
			cmdSearch_Click(cmdSearch, new System.EventArgs());
		}

		private void frmPPGetAccount_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorTag
				clsDRWrapper rs = new clsDRWrapper();
				if (modGNWork.FormExist(this))
					return;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				// 
				modPPGN.Statics.CurrentAccount = FCConvert.ToInt32(modRegistry.GetRegistryKey("PPLastAccountNumber") != string.Empty ? Conversion.Val(modReplaceWorkFiles.Statics.gstrReturn) : 0);
				txtGetAccountNumber.Text = FCConvert.ToString(modPPGN.Statics.CurrentAccount);
				// 
				rs.OpenRecordset("SELECT * FROM PPRatioOpens", modPPGN.strPPDatabase);
				if (Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("openfield1"))) == string.Empty)
				{
					if (cmbHidden.Items.Contains(""))
					{
						int index = cmbHidden.Items.IndexOf("");
						if (index == 2)
						{
							cmbHidden.Items.RemoveAt(index);
							cmbHidden.Items.Insert(index, "Open 1");
						}
					}
				}
				else
				{
					if (cmbHidden.Items.Contains(""))
					{
						int index = cmbHidden.Items.IndexOf("");
						if (index == 2)
						{
							cmbHidden.Items.RemoveAt(index);
							cmbHidden.Items.Insert(index, Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("openfield1"))));
						}
					}
				}
				if (Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("openfield2"))) == string.Empty)
				{
					if (cmbHidden.Items.Contains(""))
					{
						int index = cmbHidden.Items.IndexOf("");
						if (index == 3)
						{
							cmbHidden.Items.RemoveAt(index);
							cmbHidden.Items.Insert(index, "Open 2");
						}
					}
				}
				else
				{
					if (cmbHidden.Items.Contains(""))
					{
						int index = cmbHidden.Items.IndexOf("");
						if (index == 3)
						{
							cmbHidden.Items.RemoveAt(index);
							cmbHidden.Items.Insert(index, Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("openfield2"))));
						}
					}
				}
				rs = null;
				// 
				txtGetAccountNumber.SelectionStart = 0;
				txtGetAccountNumber.SelectionLength = txtGetAccountNumber.Text.Length;
				txtGetAccountNumber.Focus();
				// menuPPMain.Hide
				FCGlobal.Screen.MousePointer = 0;
				return;
			}
			catch (Exception ex)
			{
				// ErrorTag:
				FCGlobal.Screen.MousePointer = 0;
			}
		}

		private void frmPPGetAccount_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				cmdQuit_Click();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmPPGetAccount_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPPGetAccount properties;
			//frmPPGetAccount.ScaleWidth	= 8880;
			//frmPPGetAccount.ScaleHeight	= 7560;
			//frmPPGetAccount.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			modGlobal.SaveWindowSize(this);
		}

		private void txtGetAccountNumber_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Return)
			{
				KeyCode = (Keys)0;
				cmdGetAccountNumber_Click();
			}
		}

		private void txtSearch_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Return)
			{
				KeyCode = (Keys)0;
				cmdSearch_Click();
			}
		}
	}
}
