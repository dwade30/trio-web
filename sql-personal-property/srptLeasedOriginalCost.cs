﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Core;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for srptLeasedOriginalCost.
	/// </summary>
	public partial class srptLeasedOriginalCost : FCSectionReport
	{
		public srptLeasedOriginalCost()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ActiveReport1";
		}

		public static srptLeasedOriginalCost InstancePtr
		{
			get
			{
				return (srptLeasedOriginalCost)Sys.GetInstance(typeof(srptLeasedOriginalCost));
			}
		}

		protected srptLeasedOriginalCost _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsReports?.Dispose();
				clsTemp?.Dispose();
                clsTemp = null;
                clsReports = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptLeasedOriginalCost	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool boolPrint;
		int lngAcct;
		clsDRWrapper clsReports = new clsDRWrapper();
		clsDRWrapper clsTemp = new clsDRWrapper();

		public void PrintLeased()
		{
			if (FCConvert.ToString(clsReports.Get_Fields_String("CD")) == "-")
			{
				// TODO Get_Fields: Check the table for the column [Line] and replace with corresponding Get_Field method
				txtLine.Text = Strings.Format(clsReports.Get_Fields("Line"), "0000");
				txtCD.Text = clsReports.Get_Fields_String("CD");
				txtQty.Text = string.Empty;
				txtDescription.Text = Strings.Trim(FCConvert.ToString(clsReports.Get_Fields_String("Description")));
				txtCost.Text = string.Empty;
				txtLeaseDate.Text = string.Empty;
				txtMonths.Text = string.Empty;
				txtL1P2.Text = string.Empty;
				txtRent.Text = string.Empty;
				boolPrint = true;
			}
			// TODO Get_Fields: Check the table for the column [Cost] and replace with corresponding Get_Field method
			else if (Conversion.Val(clsReports.Get_Fields("Cost")) != 0)
			{
				if (Strings.Trim(FCConvert.ToString(clsReports.Get_Fields_String("Description"))) != string.Empty)
				{
					// TODO Get_Fields: Check the table for the column [Line] and replace with corresponding Get_Field method
					txtLine.Text = Strings.Format(clsReports.Get_Fields("Line"), "0000");
					txtCD.Text = clsReports.Get_Fields_String("CD");
					if (rptLeasedandItemized.InstancePtr.boolPQuantity)
					{
						// TODO Get_Fields: Check the table for the column [Quantity] and replace with corresponding Get_Field method
						//FC:FINAL:MSH - i.issue #1556: changed method to avoid the conversion exception
						txtQty.Text = clsReports.Get_Fields_String("Quantity");
					}
					else
					{
						txtQty.Text = "__";
					}
					txtDescription.Text = Strings.Trim(FCConvert.ToString(clsReports.Get_Fields_String("Description")));
					// TODO Get_Fields: Check the table for the column [Cost] and replace with corresponding Get_Field method
					txtCost.Text = Strings.Format(clsReports.Get_Fields("Cost"), "###,###,###");
					txtLeaseDate.Text = (Strings.InStr(1, FCConvert.ToString(clsReports.Get_Fields_String("LEASEDATE")), "/", CompareConstants.vbBinaryCompare) == 0 ? Strings.Format(clsReports.Get_Fields_String("LEASEDATE"), "00/00") : clsReports.Get_Fields_String("LEASEDATE"));
					txtMonths.Text = FCConvert.ToString(clsReports.Get_Fields_Int16("MOS"));
					txtL1P2.Text = FCConvert.ToString(clsReports.Get_Fields_Int16("L1P2"));
					txtRent.Text = Strings.Format(clsReports.Get_Fields_Decimal("MONTHLYRENT"), "###,###,###");
					boolPrint = true;
				}
			}
				// TODO Get_Fields: Check the table for the column [Cost] and replace with corresponding Get_Field method
				else if (Conversion.Val(clsReports.Get_Fields("Cost")) == 0)
			{
				if (Strings.Trim(FCConvert.ToString(clsReports.Get_Fields_String("Description"))) != string.Empty)
				{
					// TODO Get_Fields: Check the table for the column [Line] and replace with corresponding Get_Field method
					txtLine.Text = Strings.Format(clsReports.Get_Fields("Line"), "0000");
					txtCD.Text = clsReports.Get_Fields_String("CD");
					if (rptLeasedandItemized.InstancePtr.boolPQuantity)
					{
						// TODO Get_Fields: Check the table for the column [Quantity] and replace with corresponding Get_Field method
						//FC:FINAL:MSH - i.issue #1556: changed method to avoid the conversion exception
						txtQty.Text = clsReports.Get_Fields_String("Quantity");
					}
					else
					{
						txtQty.Text = "__";
					}
					txtDescription.Text = Strings.Trim(FCConvert.ToString(clsReports.Get_Fields_String("Description")));
					txtCost.Text = string.Empty;
					txtLeaseDate.Text = (Strings.InStr(1, FCConvert.ToString(clsReports.Get_Fields_String("LEASEDATE")), "/", CompareConstants.vbBinaryCompare) == 0 ? Strings.Format(clsReports.Get_Fields_String("LEASEDATE"), "00/00") : clsReports.Get_Fields_String("LEASEDATE"));
					txtMonths.Text = FCConvert.ToString(clsReports.Get_Fields_Int16("MOS"));
					txtL1P2.Text = FCConvert.ToString(clsReports.Get_Fields_Int16("L1P2"));
					txtRent.Text = Strings.Format(clsReports.Get_Fields_Decimal("MONTHLYRENT"), "###,###,###");
					boolPrint = true;
				}
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			TopOfSub:
			;
			eArgs.EOF = clsReports.EndOfFile();
			if (eArgs.EOF)
				return;
			boolPrint = false;
			PrintLeased();
			if (!boolPrint)
			{
				clsReports.MoveNext();
				goto TopOfSub;
			}
			else
			{
				clsReports.MoveNext();
			}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			rptLeasedandItemized.InstancePtr.txtAcct.Text = this.UserData.ToString();
			rptLeasedandItemized.InstancePtr.txtName.Text = clsTemp.Get_Fields_String("name");
			// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
			rptLeasedandItemized.InstancePtr.txtLocation.Text = clsTemp.Get_Fields("streetnumber") + " " + clsTemp.Get_Fields_String("street");
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lngAcct = FCConvert.ToInt32(this.UserData);
			string strMasterJoinJoin;
			strMasterJoinJoin = modPPGN.GetMasterJoinForJoin(true);
			// Set dbTemp = OpenDatabase("twpp0000.vb1", False, False, ";pwd = " & DATABASEPASSWORD)
			// Set clsReports = dbTemp.OpenRecordset("select * from ppleased where ACCOUNT = " & lngAcct & " order by line")
			clsReports.OpenRecordset("select * from ppleased where ACCOUNT = " + FCConvert.ToString(lngAcct) + " order by line", "twpp0000.vb1");
			// Call clsTemp.OpenRecordset("select name,street,streetnumber from ppmaster where account = " & lngAcct, "twpp0000.vb1")
			// Call clsTemp.OpenRecordset("select name,street,streetnumber from ppmaster where account = " & lngAcct, "twpp0000.vb1")
			clsTemp.OpenRecordset("select name,street,streetnumber from " + strMasterJoinJoin + " where account = " + FCConvert.ToString(lngAcct), "twpp0000.vb1");
			if (clsReports.EndOfFile())
			{
				Detail.Visible = false;
				GroupHeader1.Visible = false;
			}
			else
			{
				Detail.Visible = true;
				GroupHeader1.Visible = true;
			}
		}

		
	}
}
