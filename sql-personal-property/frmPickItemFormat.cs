﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Runtime.InteropServices;
using Wisej.Core;
using System.IO;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmPickItemFormat.
	/// </summary>
	public partial class frmPickItemFormat : BaseForm
	{
		public frmPickItemFormat()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.lblLine = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.lblLine.AddControlArrayElement(lblLine_2, 2);
			this.lblLine.AddControlArrayElement(lblLine_1, 1);
			this.lblLine.AddControlArrayElement(lblLine_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPickItemFormat InstancePtr
		{
			get
			{
				return (frmPickItemFormat)Sys.GetInstance(typeof(frmPickItemFormat));
			}
		}

		protected frmPickItemFormat _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int colQTY;
		int colCode;
		int colDsc;
		int colMO;
		int colYR;
		int colDYR;
		int colSRO;
		int colRYR;
		int colCst;
		int colReIm;
		int colBETE;
		int colYearsClaimed;
		string FilePath;

		public void Init(string strPath)
		{
			FilePath = strPath;
			this.Show(App.MainForm);
		}

		private void Check1_CheckedChanged(object sender, System.EventArgs e)
		{
			if (Check1.CheckState == CheckState.Checked)
			{
				Grid1.ColHidden(colCode, false);
			}
			else
			{
				Grid1.ColHidden(colCode, true);
			}
			FillExample();
		}

		private void Check2_CheckedChanged(object sender, System.EventArgs e)
		{
			if (Check2.CheckState == CheckState.Checked)
			{
				Grid1.ColHidden(colQTY, false);
			}
			else
			{
				Grid1.ColHidden(colQTY, true);
			}
			FillExample();
		}

		private void Check3_CheckedChanged(object sender, System.EventArgs e)
		{
			if (Check3.CheckState == CheckState.Checked)
			{
				Grid1.ColHidden(colDsc, false);
			}
			else
			{
				Grid1.ColHidden(colDsc, true);
			}
			FillExample();
		}

		private void Check4_CheckedChanged(object sender, System.EventArgs e)
		{
			if (Check4.CheckState == CheckState.Checked)
			{
				Grid1.ColHidden(colMO, false);
			}
			else
			{
				Grid1.ColHidden(colMO, true);
			}
			FillExample();
		}

		private void Check5_CheckedChanged(object sender, System.EventArgs e)
		{
			if (Check5.CheckState == CheckState.Checked)
			{
				Grid1.ColHidden(colYR, false);
			}
			else
			{
				Grid1.ColHidden(colYR, true);
			}
			FillExample();
		}

		private void Check6_CheckedChanged(object sender, System.EventArgs e)
		{
			if (Check6.CheckState == CheckState.Checked)
			{
				Grid1.ColHidden(colDYR, false);
			}
			else
			{
				Grid1.ColHidden(colDYR, true);
			}
			FillExample();
		}

		private void Check7_CheckedChanged(object sender, System.EventArgs e)
		{
			if (Check7.CheckState == CheckState.Checked)
			{
				Grid1.ColHidden(colSRO, false);
			}
			else
			{
				Grid1.ColHidden(colSRO, true);
			}
			FillExample();
		}

		private void Check8_CheckedChanged(object sender, System.EventArgs e)
		{
			if (Check8.CheckState == CheckState.Checked)
			{
				Grid1.ColHidden(colRYR, false);
			}
			else
			{
				Grid1.ColHidden(colRYR, true);
			}
			FillExample();
		}

		private void Check9_CheckedChanged(object sender, System.EventArgs e)
		{
			if (Check9.CheckState == CheckState.Checked)
			{
				Grid1.ColHidden(colCst, false);
			}
			else
			{
				Grid1.ColHidden(colCst, true);
			}
			FillExample();
		}

		private void cmdImport_Click()
		{
			StreamReader tsImport = null;
			// Dim strImport As String
			// Dim strSplit() As String
			// vbPorter upgrade warning: intCurLine As short --> As int	OnWrite(double, int)
			int intCurLine = 0;
			int intCurCol;
			clsDRWrapper clsTemp = new clsDRWrapper();
			int X;
			string strCode;
			int intQTY;
			string strDesc;
			int intMO;
			int intYR;
			int intDYR;
			string strSRO;
			int intRYR;
			// vbPorter upgrade warning: lngCost As int	OnWrite(short, Decimal)
			int lngCost;
			int lngAcct;
			string strSQL = "";
			string strCols;
			string strSplitChar = "";
			string[] strAry = null;
			int intMaxCol;
			int intUbound = 0;
			int intCol;
			int intField = 0;
			int intMaxChar;
			int intTemp;
			bool boolInQuote;
			string strLastChar = "";
			string strNewLine = "";
			string strLine = "";
			bool boolReim;
			string strReim = "";
			bool boolBETE = false;
			int intYearsClaimed;
			try
			{
				// On Error GoTo ErrorHandler
				if (Conversion.Val(txtAcct.Text) == 0)
				{
					MessageBox.Show("You must enter an account number to import to.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(txtAcct.Text)));
				clsTemp.OpenRecordset("select account from ppmaster where account = " + FCConvert.ToString(lngAcct), "twpp0000.vb1");
				if (clsTemp.EndOfFile())
				{
					MessageBox.Show("Account Number " + FCConvert.ToString(lngAcct) + " not found.  Cannot Import to it.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (!File.Exists(FilePath))
				{
					MessageBox.Show("The file " + FilePath + " could not be found.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					Close();
					return;
				}
				// If UCase(fso.GetExtensionName(FilePath)) = "XLS" Then
				// ImportExcel
				// Exit Sub
				// End If
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				tsImport = File.OpenText(FilePath);
				if (chkFirstLine.CheckState == CheckState.Checked)
				{
					tsImport.ReadLine();
				}
				strCode = "";
				intQTY = 0;
				strDesc = "";
				intMO = 0;
				intYR = 0;
				intDYR = 0;
				strSRO = "";
				intRYR = 0;
				lngCost = 0;
				intYearsClaimed = 0;
				boolReim = false;
				int lngYearFirstAssessed;
				int lngFctr;
				lngFctr = FCConvert.ToInt32(Math.Round(Conversion.Val(txtFactor.Text)));
				lngYearFirstAssessed = FCConvert.ToInt32(Math.Round(Conversion.Val(txtYearFirstAssessed.Text)));
				strCols = "(account,line,cd,quantity,description,[month],[year],dpyr,sro,rcyr,cost,RB,yearfirstassessed,fctr,gd,yearsclaimed)";
				if (cmbAppend.SelectedIndex == 0)
				{
					clsTemp.OpenRecordset("select max(line) as mline from ppitemized where account = " + FCConvert.ToString(lngAcct), "twpp0000.vb1");
					if (!clsTemp.EndOfFile())
					{
						// TODO Get_Fields: Field [mline] not found!! (maybe it is an alias?)
						intCurLine = FCConvert.ToInt32(Conversion.Val(clsTemp.Get_Fields("mline")) + 1);
					}
					else
					{
						intCurLine = 1;
					}
				}
				else
				{
					clsTemp.Execute("delete from ppitemized where account = " + FCConvert.ToString(lngAcct), "twpp0000.vb1");
					intCurLine = 1;
				}
				strSplitChar = ",";
				intMaxCol = Grid1.Cols - 1;
				while (!tsImport.EndOfStream)
				{
					boolReim = false;
					strLine = tsImport.ReadLine();
					if (chkAllDatainQuotes.CheckState == CheckState.Checked || chkQuotes.CheckState == CheckState.Checked)
					{
						strAry = modPPGN.SplitQuotedText(strLine, strSplitChar);
					}
					else
					{
						strAry = Strings.Split(strLine, strSplitChar, -1, CompareConstants.vbTextCompare);
					}
					// strAry = Split(strLine, strSplitChar, , vbTextCompare)
					intUbound = Information.UBound(strAry, 1);
					intField = 0;
					for (intCol = 0; intCol <= Grid1.Cols - 1; intCol++)
					{
						if (intField <= intUbound)
						{
							if (!Grid1.ColHidden(intCol))
							{
								// .TextMatrix(lngRow, intCol) = strAry(intField)
								string vbPorterVar = FCConvert.ToString(Grid1.Cell(FCGrid.CellPropertySettings.flexcpData, 0, intCol));
								if (vbPorterVar == "CODE")
								{
									strCode = strAry[intField];
								}
								else if (vbPorterVar == "QTY")
								{
									intQTY = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[intField])));
								}
								else if (vbPorterVar == "DSC")
								{
									strDesc = strAry[intField];
									strDesc = modGlobalFunctions.EscapeQuotes(strDesc);
								}
								else if (vbPorterVar == "MO")
								{
									intMO = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[intField])));
								}
								else if (vbPorterVar == "YR")
								{
									intYR = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[intField])));
									if (intYR < 26)
									{
										intYR = 2000 + intYR;
									}
									else
									{
										if (intYR < 100)
										{
											intYR += 1900;
										}
									}
								}
								else if (vbPorterVar == "DYR")
								{
									intDYR = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[intField])));
									if (intDYR < 26)
									{
										intDYR = 2000 + intDYR;
									}
									else
									{
										if (Marshal.SizeOf(intDYR) < 4)
										{
											intDYR += 1900;
										}
									}
								}
								else if (vbPorterVar == "SRO")
								{
									strSRO = strAry[intField];
									strSRO = Strings.Trim(strSRO);
									if (Strings.LCase(strSRO) == "s")
									{
									}
									else if (Strings.LCase(strSRO) == "r")
									{
									}
									else
									{
										strSRO = "O";
									}
								}
								else if (vbPorterVar == "RYR")
								{
									intRYR = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[intField])));
									if (intRYR < 26)
									{
										intRYR = 2000 + intRYR;
									}
									else
									{
										if (Marshal.SizeOf(intRYR) < 4)
										{
											intRYR += 1900;
										}
									}
								}
								else if (vbPorterVar == "CST")
								{
									if (Conversion.Val(strAry[intField]) > 0)
									{
										lngCost = FCConvert.ToInt32((FCConvert.ToDecimal(strAry[intField])));
									}
									else
									{
										lngCost = 0;
									}
									// lngCost = Val(strAry(intField))
								}
								else if (vbPorterVar == "REIM")
								{
									if (Strings.Trim(strAry[intField]) != string.Empty)
									{
										if (Strings.Trim(strAry[intField]) == "*")
										{
											boolReim = true;
										}
										else if (Strings.LCase(Strings.Trim(strAry[intField])) == "t" || Strings.LCase(Strings.Trim(strAry[intField])) == "true")
										{
											boolReim = true;
										}
										else if (Strings.LCase(Strings.Trim(strAry[intField])) == "y" || Strings.LCase(Strings.Trim(strAry[intField])) == "yes")
										{
											boolReim = true;
										}
										else if (Strings.LCase(Strings.Trim(strAry[intField])) == "r")
										{
											boolReim = true;
										}
										else
										{
											boolReim = false;
										}
									}
									else
									{
										boolReim = false;
									}
								}
								else if (vbPorterVar == "BETE")
								{
									boolBETE = false;
									if (Strings.Trim(strAry[intField]) != string.Empty)
									{
										if (Strings.LCase(Strings.Trim(strAry[intField])) == "t" || Strings.LCase(Strings.Trim(strAry[intField])) == "true" || Strings.LCase(Strings.Trim(strAry[intField])) == "yes" || Strings.LCase(Strings.Trim(strAry[intField])) == "y")
										{
											boolBETE = true;
										}
										else if (FCConvert.ToDouble(Strings.LCase(Strings.Trim(strAry[intField]))) == modGlobal.Statics.CustomizeStuff.CurrentBETEYear && modGlobal.Statics.CustomizeStuff.CurrentBETEYear > 0)
										{
											boolBETE = true;
										}
										else if (FCConvert.ToDouble(Strings.LCase(Strings.Trim(strAry[intField]))) == DateTime.Today.Year && modGlobal.Statics.CustomizeStuff.CurrentBETEYear == 0)
										{
											boolBETE = true;
										}
										else if (Strings.LCase(Strings.Trim(strAry[intField])) == "*")
										{
											boolBETE = true;
										}
									}
									else
									{
										boolBETE = false;
									}
								}
								else if (vbPorterVar == "YearsClaimed")
								{
									if (Conversion.Val(strAry[intField]) > 0)
									{
										intYearsClaimed = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[intField])));
									}
									else
									{
										intYearsClaimed = 0;
									}
								}
								intField += 1;
							}
							// intField = intField + 1
						}
					}
					// intCol
					// Loop
					if (boolReim)
					{
						strReim = "*";
					}
					else
					{
						strReim = " ";
					}
					if (boolBETE)
					{
						strReim = "P";
					}
					if (lngCost > 0 || Strings.Trim(strDesc) != string.Empty)
					{
						strSQL = "(";
						strSQL += FCConvert.ToString(lngAcct) + ",";
						strSQL += FCConvert.ToString(intCurLine) + ",'";
						strSQL += strCode + "',";
						strSQL += FCConvert.ToString(intQTY) + ",'";
						strSQL += strDesc + "',";
						strSQL += FCConvert.ToString(intMO) + ",";
						strSQL += FCConvert.ToString(intYR) + ",";
						strSQL += FCConvert.ToString(intDYR) + ",'";
						strSQL += strSRO + "',";
						strSQL += FCConvert.ToString(intRYR) + ",";
						strSQL += FCConvert.ToString(lngCost) + ",";
						strSQL += "'" + strReim + "'" + ",";
						strSQL += FCConvert.ToString(lngYearFirstAssessed) + ",";
						strSQL += FCConvert.ToString(lngFctr) + ",";
						strSQL += "0";
						strSQL += "," + FCConvert.ToString(intYearsClaimed);
						strSQL += ")";
						clsTemp.Execute("insert into ppitemized " + strCols + " values " + strSQL, "twpp0000.vb1");
						intCurLine += 1;
					}
				}
				tsImport.Close();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Import completed successfully", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
				Close();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Import_Click", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
			finally
			{
				if (tsImport != null)
				{
					tsImport.Close();
				}
			}
		}

		private void cmdQuit_Click()
		{
			Close();
		}

		private void chkAllDatainQuotes_CheckedChanged(object sender, System.EventArgs e)
		{
			FillExample();
		}

		private void chkBETEExempt_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkBETEExempt.CheckState == CheckState.Checked)
			{
				Grid1.ColHidden(colBETE, false);
			}
			else
			{
				Grid1.ColHidden(colBETE, true);
			}
			FillExample();
		}

		private void chkFirstLine_CheckedChanged(object sender, System.EventArgs e)
		{
			FillExample();
		}

		private void chkQuotes_CheckedChanged(object sender, System.EventArgs e)
		{
			FillExample();
		}

		private void chkReimbursable_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkReimbursable.CheckState == CheckState.Checked)
			{
				Grid1.ColHidden(colReIm, false);
			}
			else
			{
				Grid1.ColHidden(colReIm, true);
			}
			FillExample();
		}

		private void chkYearsClaimed_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkYearsClaimed.CheckState == CheckState.Checked)
			{
				Grid1.ColHidden(colYearsClaimed, false);
			}
			else
			{
				Grid1.ColHidden(colYearsClaimed, true);
			}
			FillExample();
		}

		private void cmdPlaceHolder_Click(object sender, System.EventArgs e)
		{
			Grid1.Cols += 1;
			Grid1.TextMatrix(0, Grid1.Cols - 1, "Misc");
			FillExample();
		}

		private void frmPickItemFormat_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmPickItemFormat_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPickItemFormat properties;
			//frmPickItemFormat.ScaleWidth	= 9045;
			//frmPickItemFormat.ScaleHeight	= 7620;
			//frmPickItemFormat.LinkTopic	= "Form1";
			//frmPickItemFormat.LockControls	= true;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
			if (modGlobal.Statics.CustomizeStuff.DefaultYearFirstAssessed == 0)
			{
				txtYearFirstAssessed.Text = FCConvert.ToString(DateTime.Today.Year);
			}
			else
			{
				txtYearFirstAssessed.Text = FCConvert.ToString(modGlobal.Statics.CustomizeStuff.DefaultYearFirstAssessed);
			}
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid1.WidthOriginal;
			Grid1.ColWidth(0, FCConvert.ToInt32(0.07 * GridWidth));
			Grid1.ColWidth(1, FCConvert.ToInt32(0.07 * GridWidth));
			Grid1.ColWidth(2, FCConvert.ToInt32(0.29 * GridWidth));
			Grid1.ColWidth(3, FCConvert.ToInt32(0.09 * GridWidth));
			Grid1.ColWidth(4, FCConvert.ToInt32(0.09 * GridWidth));
			Grid1.ColWidth(5, FCConvert.ToInt32(0.1 * GridWidth));
			Grid1.ColWidth(6, FCConvert.ToInt32(0.09 * GridWidth));
			Grid1.ColWidth(7, FCConvert.ToInt32(0.1 * GridWidth));
			Grid1.ColWidth(8, FCConvert.ToInt32(0.09 * GridWidth));
		}

		private void SetupGrid()
		{
			colCode = 0;
			colQTY = 1;
			colDsc = 2;
			colMO = 3;
			colYR = 4;
			colDYR = 5;
			colSRO = 6;
			colRYR = 7;
			colCst = 8;
			colReIm = 9;
			colBETE = 10;
			colYearsClaimed = 11;
			Grid1.Cols = 12;
			// .ColWidth(0) = 500
			Grid1.ColHidden(0, true);
			Grid1.Cell(FCGrid.CellPropertySettings.flexcpData, 0, 0, "CODE");
			// .ColWidth(1) = 500
			Grid1.ColHidden(1, true);
			Grid1.Cell(FCGrid.CellPropertySettings.flexcpData, 0, 1, "QTY");
			// .ColWidth(2) = 1800
			Grid1.ColHidden(2, true);
			Grid1.Cell(FCGrid.CellPropertySettings.flexcpData, 0, 2, "DSC");
			// .ColWidth(3) = 720
			Grid1.ColHidden(3, true);
			Grid1.Cell(FCGrid.CellPropertySettings.flexcpData, 0, 3, "MO");
			// .ColWidth(4) = 720
			Grid1.ColHidden(4, true);
			Grid1.Cell(FCGrid.CellPropertySettings.flexcpData, 0, 4, "YR");
			// .ColWidth(5) = 720
			Grid1.ColHidden(5, true);
			Grid1.Cell(FCGrid.CellPropertySettings.flexcpData, 0, 5, "DYR");
			// .ColWidth(6) = 720
			Grid1.ColHidden(6, true);
			Grid1.Cell(FCGrid.CellPropertySettings.flexcpData, 0, 6, "SRO");
			// .ColWidth(7) = 720
			Grid1.ColHidden(7, true);
			Grid1.Cell(FCGrid.CellPropertySettings.flexcpData, 0, 7, "RYR");
			// .ColWidth(8) = 720
			Grid1.ColHidden(8, true);
			Grid1.Cell(FCGrid.CellPropertySettings.flexcpData, 0, 8, "CST");
			Grid1.ColHidden(colReIm, true);
			Grid1.Cell(FCGrid.CellPropertySettings.flexcpData, 0, colReIm, "REIM");
			Grid1.ColHidden(colBETE, true);
			Grid1.Cell(FCGrid.CellPropertySettings.flexcpData, 0, colBETE, "BETE");
			Grid1.ColHidden(colYearsClaimed, true);
			Grid1.Cell(FCGrid.CellPropertySettings.flexcpData, 0, colYearsClaimed, "YearsClaimed");
			Grid1.TextMatrix(0, 0, "Code");
			Grid1.TextMatrix(0, 1, "QTY");
			Grid1.TextMatrix(0, 2, "Description");
			Grid1.TextMatrix(0, 3, "Month");
			Grid1.TextMatrix(0, 4, "Year");
			Grid1.TextMatrix(0, 5, "Dep. YR");
			Grid1.TextMatrix(0, 6, "SRO");
			Grid1.TextMatrix(0, 7, "Rep. YR");
			Grid1.TextMatrix(0, 8, "Cost");
			Grid1.TextMatrix(0, colReIm, "Reimb.");
			Grid1.TextMatrix(0, colBETE, "BETE");
			Grid1.TextMatrix(0, colYearsClaimed, "Years Claimed");
			FillExample();
		}

		private void frmPickItemFormat_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Grid1_AfterMoveColumn(object sender, DataGridViewColumnEventArgs e)
		{
			int X;
			for (X = 0; X <= (Grid1.Cols - 1); X++)
			{
				string vbPorterVar = Grid1.Cell(FCGrid.CellPropertySettings.flexcpData, 0, X);
				if (vbPorterVar == "CODE")
				{
					colCode = X;
				}
				else if (vbPorterVar == "QTY")
				{
					colQTY = X;
				}
				else if (vbPorterVar == "DSC")
				{
					colDsc = X;
				}
				else if (vbPorterVar == "MO")
				{
					colMO = X;
				}
				else if (vbPorterVar == "YR")
				{
					colYR = X;
				}
				else if (vbPorterVar == "DYR")
				{
					colDYR = X;
				}
				else if (vbPorterVar == "SRO")
				{
					colSRO = X;
				}
				else if (vbPorterVar == "RYR")
				{
					colRYR = X;
				}
				else if (vbPorterVar == "CST")
				{
					colCst = X;
				}
				else if (vbPorterVar == "REIM")
				{
					colReIm = X;
				}
				else if (vbPorterVar == "BETE")
				{
					colBETE = X;
				}
				else if (vbPorterVar == "YearsClaimed")
				{
					colYearsClaimed = X;
				}
			}
			// X
			FillExample();
		}

		private void mnuContinue_Click(object sender, System.EventArgs e)
		{
			cmdImport_Click();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void FillExample()
		{
			StreamReader tsImport = null;
			string strLine = "";
			int X = 0;
			int lngRow;
			string strSplitChar = "";
			string[] strAry = null;
			int intMaxCol;
			int intUbound = 0;
			int intCol;
			int intField = 0;
			int intMaxChar = 0;
			int intTemp;
			bool boolInQuote = false;
			string strLastChar = "";
			string strNewLine = "";
			try
			{
				// On Error GoTo ErrorHandler
				tsImport = File.OpenText(FilePath);
				if (chkFirstLine.CheckState == CheckState.Checked)
				{
					tsImport.ReadLine();
				}
				Grid1.Rows = 1;
				strSplitChar = ",";
				intMaxCol = Grid1.Cols - 1;
				X = 0;
				while (!tsImport.EndOfStream && X < 3)
				{
					X += 1;
					Grid1.Rows += 1;
					lngRow = Grid1.Rows - 1;
					strLine = tsImport.ReadLine();
					lblLine[FCConvert.ToInt16(X - 1)].Text = strLine;
					if (chkAllDatainQuotes.CheckState == CheckState.Checked)
					{
						strSplitChar = "^";
						strLine = Strings.Replace(strLine, FCConvert.ToString(Convert.ToChar(34)) + "," + FCConvert.ToString(Convert.ToChar(34)), "^", 1, -1, CompareConstants.vbTextCompare);
						strLine = Strings.Mid(strLine, 2, strLine.Length - 2);
						// get rid of first and last quote
					}
					else if (chkQuotes.CheckState == CheckState.Checked)
					{
						strSplitChar = "^";
						intMaxChar = strLine.Length;
						boolInQuote = false;
						strLastChar = "";
						strNewLine = "";
						for (intTemp = 1; intTemp <= intMaxChar; intTemp++)
						{
							if (Strings.Mid(strLine, intTemp, 1) == FCConvert.ToString(Convert.ToChar(34)))
							{
								if (boolInQuote)
								{
									if (intTemp == intMaxChar)
									{
										strLastChar = FCConvert.ToString(Convert.ToChar(34));
										boolInQuote = false;
									}
									else
									{
										if (Strings.Mid(strLine, intTemp + 1, 1) == ",")
										{
											boolInQuote = false;
											strLastChar = FCConvert.ToString(Convert.ToChar(34));
										}
										else
										{
											strLastChar = FCConvert.ToString(Convert.ToChar(34));
											strNewLine += FCConvert.ToString(Convert.ToChar(34));
										}
									}
								}
								else
								{
									if (intTemp > 1)
									{
										if (strLastChar == ",")
										{
											boolInQuote = true;
											strLastChar = FCConvert.ToString(Convert.ToChar(34));
										}
										else
										{
											strLastChar = FCConvert.ToString(Convert.ToChar(34));
											strNewLine += FCConvert.ToString(Convert.ToChar(34));
										}
									}
									else
									{
										boolInQuote = true;
										strLastChar = FCConvert.ToString(Convert.ToChar(34));
									}
								}
							}
							else if (Strings.Mid(strLine, intTemp, 1) == ",")
							{
								strLastChar = ",";
								if (!boolInQuote)
								{
									strNewLine += "^";
								}
								else
								{
									strNewLine += ",";
								}
							}
							else
							{
								strLastChar = Strings.Mid(strLine, intTemp, 1);
								strNewLine += Strings.Mid(strLine, intTemp, 1);
							}
						}
						// intTemp
						strLine = strNewLine;
					}
					strAry = Strings.Split(strLine, strSplitChar, -1, CompareConstants.vbTextCompare);
					intUbound = Information.UBound(strAry, 1);
					intField = 0;
					for (intCol = 0; intCol <= Grid1.Cols - 1; intCol++)
					{
						if (!Grid1.ColHidden(intCol))
						{
							if (intField <= intUbound)
							{
								Grid1.TextMatrix(lngRow, intCol, strAry[intField]);
								intField += 1;
							}
						}
					}
					// intCol
				}
				//tsImport.Close();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				//tsImport.Close();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In FillExample", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			finally
			{
				if (tsImport != null)
				{
					tsImport.Close();
				}
			}
		}
	}
}
