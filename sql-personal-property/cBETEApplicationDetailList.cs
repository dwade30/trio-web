﻿//Fecher vbPorter - Version 1.0.0.32
using fecherFoundation;

namespace TWPP0000
{
	public class cBETEApplicationDetailList
	{
		//=========================================================
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private FCCollection lstDetails = new FCCollection();
		private FCCollection lstDetails_AutoInitialized;

		private FCCollection lstDetails
		{
			get
			{
				if (lstDetails_AutoInitialized == null)
				{
					lstDetails_AutoInitialized = new FCCollection();
				}
				return lstDetails_AutoInitialized;
			}
			set
			{
				lstDetails_AutoInitialized = value;
			}
		}

		private int intCurrentIndex;

		public cBETEApplicationDetailList() : base()
		{
			intCurrentIndex = -1;
		}

		public void ClearList()
		{
			//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
			if (!(lstDetails_AutoInitialized == null))
			{
				foreach (cBETEApplicationDetail tRec in lstDetails)
				{
					lstDetails.Remove(1);
				}
				// tRec
			}
		}

		public void AddItem(ref cBETEApplicationDetail tItem)
		{
			if (!(tItem == null))
			{
				lstDetails.Add(tItem);
			}
		}

		public void MoveFirst()
		{
			//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
			if (!(lstDetails_AutoInitialized == null))
			{
				if (!fecherFoundation.FCUtils.IsEmpty(lstDetails))
				{
					if (lstDetails.Count >= 0)
					{
						intCurrentIndex = -1;
						MoveNext();
					}
					else
					{
						intCurrentIndex = -1;
					}
				}
				else
				{
					intCurrentIndex = -1;
				}
			}
			else
			{
				intCurrentIndex = -1;
			}
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short MoveNext()
		{
			short MoveNext = 0;
			int intReturn;
			intReturn = -1;
			MoveNext = -1;
			if (intCurrentIndex == -1)
				intCurrentIndex = 0;
			if (!FCUtils.IsEmpty(lstDetails))
			{
				if (intCurrentIndex > lstDetails.Count)
				{
					intReturn = -1;
				}
				else
				{
					while (intCurrentIndex <= lstDetails.Count)
					{
						intCurrentIndex += 1;
						if (intCurrentIndex > lstDetails.Count)
						{
							intReturn = -1;
							break;
						}
						else if (lstDetails[intCurrentIndex] == null)
						{
						}
						else
						{
							intReturn = intCurrentIndex;
							break;
						}
					}
				}
				if (intReturn == 0)
					intReturn = -1;
				intCurrentIndex = intReturn;
				MoveNext = FCConvert.ToInt16(intReturn);
			}
			else
			{
				intCurrentIndex = -1;
				MoveNext = -1;
			}
			return MoveNext;
		}

		public bool IsCurrent()
		{
			bool IsCurrent = false;
			if (intCurrentIndex > 0)
			{
				IsCurrent = true;
			}
			else
			{
				IsCurrent = false;
			}
			return IsCurrent;
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short ItemCount()
		{
			short ItemCount = 0;
			if (!fecherFoundation.FCUtils.IsEmpty(lstDetails))
			{
				ItemCount = FCConvert.ToInt16(lstDetails.Count);
			}
			else
			{
				ItemCount = 0;
			}
			return ItemCount;
		}

		public cBETEApplicationDetail GetCurrentDetail()
		{
			cBETEApplicationDetail GetCurrentDetail = null;
			cBETEApplicationDetail tRec;
			tRec = null;
			if (!fecherFoundation.FCUtils.IsEmpty(lstDetails))
			{
				if (intCurrentIndex > 0)
				{
					if (!(lstDetails[intCurrentIndex] == null))
					{
						tRec = lstDetails[intCurrentIndex];
					}
				}
			}
			GetCurrentDetail = tRec;
			return GetCurrentDetail;
		}

		public cBETEApplicationDetail GetDetailByIndex(ref short intIndex)
		{
			cBETEApplicationDetail GetDetailByIndex = null;
			cBETEApplicationDetail tRec;
			tRec = null;
			if (!fecherFoundation.FCUtils.IsEmpty(lstDetails) && intIndex > 0)
			{
				if (!(lstDetails[intIndex] == null))
				{
					intCurrentIndex = intIndex;
					tRec = lstDetails[intIndex];
				}
			}
			GetDetailByIndex = tRec;
			return GetDetailByIndex;
		}

		public void CreateDetail(double dblAssessedValue, int intAge, bool boolBETEEligible, string strDescription, string strEquipLocation, double dblEstimatedValue, string strPlacedInService, bool boolTIF, double dblValueNew)
		{
			cBETEApplicationDetail tRec = new cBETEApplicationDetail();
			tRec.AssessedValue = dblAssessedValue;
			tRec.Age = intAge;
			tRec.BETEEligible = boolBETEEligible;
			tRec.Description = strDescription;
			tRec.EquipmentLocation = strEquipLocation;
			tRec.EstimatedValue = dblEstimatedValue;
			tRec.PlacedInService = strPlacedInService;
			tRec.TIF = boolTIF;
			tRec.ValueNew = dblValueNew;
			AddItem(ref tRec);
		}
	}
}
