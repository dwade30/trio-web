﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptNameLocOpen.
	/// </summary>
	public partial class rptNameLocOpen : BaseSectionReport
	{
		private clsDRWrapper rsLoad = new clsDRWrapper();
		public rptNameLocOpen()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Name Location Open Report";
		}

        public void Init(string strsql)
        {
            rsLoad.OpenRecordset(strsql, "PersonalProperty");
            if (rsLoad.EndOfFile())
            {
                MessageBox.Show("No records found");
                this.Unload();
                return;
            }
            frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "NameLocOpen");
        }
		public static rptNameLocOpen InstancePtr
		{
			get
			{
				return (rptNameLocOpen)Sys.GetInstance(typeof(rptNameLocOpen));
			}
		}

		protected rptNameLocOpen _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}

            if (disposing)
            {
				rsLoad?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptNameLocOpen	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private int intPage;
		private int intAccount;
		private bool boolPrint;
		private string strName = "";
		private string strLocation = "";

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
            using (clsDRWrapper clsTemp = new clsDRWrapper())
            {
                string strO1;
                string strO2;
                clsTemp.OpenRecordset("select * from ppratioopens", "twpp0000.vb1");
                strO1 = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("openfield1")));
                strO2 = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("openfield2")));
                if (Strings.Trim(strO1) == string.Empty)
                    strO1 = "Open 1";
                if (Strings.Trim(strO2) == string.Empty)
                    strO2 = "Open 2";
                Label7.Text = strO1;
                Label6.Text = strO2;
                Fields.Add("AccountNumber");
                //modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
            }
        }

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			ShowRecord:
			;
			eArgs.EOF = rsLoad.EndOfFile();
			if (eArgs.EOF)
				return;
			txttime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			if (FCConvert.ToInt32(rsLoad.GetData("MasterAccount")) == intAccount)
			{
				if (!rsLoad.EndOfFile())
				{
					rsLoad.MoveNext();
				}
				goto ShowRecord;
			}
			txtName.Text = rsLoad.GetData("Name").ToString();
			txtLocation.Text = rsLoad.GetData("StreetNumber") + rsLoad.Get_Fields_String("streetapt") + " " + rsLoad.GetData("Street");
			txtAccount.Text = rsLoad.GetData("MasterAccount").ToString();
			txtOpen1.Text = rsLoad.GetData("Open1").ToString();
			txtOpen2.Text = rsLoad.GetData("Open2").ToString();
			intAccount = FCConvert.ToInt32(rsLoad.GetData("MasterAccount"));
			if (!rsLoad.EndOfFile())
				rsLoad.MoveNext();
			eArgs.EOF = false;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//this.Printer.RenderMode = 1;
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTitle.Text = "Name / Location / Open";
			txttime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			intPage += 1;
			txtPage.Text = "Page: " + FCConvert.ToString(intPage);
		}

		
	}
}
