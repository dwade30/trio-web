﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptDeletedAccounts.
	/// </summary>
	public partial class rptDeletedAccounts : BaseSectionReport
	{
		public rptDeletedAccounts()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Deleted Accounts";
		}

		public static rptDeletedAccounts InstancePtr
		{
			get
			{
				return (rptDeletedAccounts)Sys.GetInstance(typeof(rptDeletedAccounts));
			}
		}

		protected rptDeletedAccounts _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsDeleted?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptDeletedAccounts	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsDeleted = new clsDRWrapper();
		int intPage;

		public void Init()
		{
			//FC:FINAL:MSH - i.issue #1336: initializing report data moved for showing message, selecting order type and correct data initializing
			string strOrder = "";
			// vbPorter upgrade warning: intReturn As short, int --> As DialogResult
			DialogResult intReturn;
			intReturn = MessageBox.Show("Print the list in Account order?" + "\r\n" + "No will print in Name order", "Report Order", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (intReturn == DialogResult.Yes)
			{
				strOrder = " order by account";
			}
			else
			{
				strOrder = " order by Name";
			}
			string strMasterJoin;
			strMasterJoin = modPPGN.GetMasterJoin(false);
			clsDeleted.OpenRecordset(strMasterJoin + " where deleted = 1" + strOrder, "twpp0000.vb1");
			intPage = 1;
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "DeletedAccounts");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsDeleted.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			//this.Printer.RenderMode = 1;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!clsDeleted.EndOfFile())
			{
				txtAccount.Text = clsDeleted.GetData("account").ToString();
				txtName.Text = clsDeleted.GetData("name").ToString();
			}
			else
			{
				txtAccount.Text = "";
				txtName.Text = "No deleted accounts found";
			}
			clsDeleted.MoveNext();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page  " + FCConvert.ToString(intPage);
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			intPage += 1;
		}

		
	}
}
