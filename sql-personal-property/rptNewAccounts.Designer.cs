﻿namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptNewAccounts.
	/// </summary>
	partial class rptNewAccounts
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptNewAccounts));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtdate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txttime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStreet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtdate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txttime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStreet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAcct,
				this.txtName,
				this.txtStNo,
				this.txtStreet
			});
			this.Detail.Height = 0.2395833F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label2,
				this.Label4,
				this.txtPage,
				this.Field1,
				this.txtMuni,
				this.txtdate,
				this.txttime
			});
			this.PageHeader.Height = 0.8125F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Label1
			// 
			this.Label1.Height = 0.19F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.0625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-weight: bold; text-align: right";
			this.Label1.Text = "Account";
			this.Label1.Top = 0.625F;
			this.Label1.Width = 0.75F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.19F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.9375F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold";
			this.Label2.Text = "Name";
			this.Label2.Top = 0.625F;
			this.Label2.Width = 0.875F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 3.125F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-weight: bold";
			this.Label4.Text = "Location";
			this.Label4.Top = 0.625F;
			this.Label4.Width = 2.375F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.19F;
			this.txtPage.Left = 6.5F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.1875F;
			this.txtPage.Width = 0.875F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 2F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.Field1.Text = "New Accounts (Never Calculated)";
			this.Field1.Top = 0F;
			this.Field1.Width = 3.5625F;
			// 
			// txtMuni
			// 
			this.txtMuni.Height = 0.19F;
			this.txtMuni.Left = 0.0625F;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Text = "Field2";
			this.txtMuni.Top = 0.03125F;
			this.txtMuni.Width = 1.3125F;
			// 
			// txtdate
			// 
			this.txtdate.Height = 0.19F;
			this.txtdate.Left = 6.5F;
			this.txtdate.Name = "txtdate";
			this.txtdate.Style = "text-align: right";
			this.txtdate.Text = null;
			this.txtdate.Top = 0.03125F;
			this.txtdate.Width = 0.875F;
			// 
			// txttime
			// 
			this.txttime.Height = 0.19F;
			this.txttime.Left = 0.0625F;
			this.txttime.Name = "txttime";
			this.txttime.Text = "Field2";
			this.txttime.Top = 0.1875F;
			this.txttime.Width = 1.3125F;
			// 
			// txtAcct
			// 
			this.txtAcct.Height = 0.19F;
			this.txtAcct.Left = 0.0625F;
			this.txtAcct.Name = "txtAcct";
			this.txtAcct.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtAcct.Text = "Field2";
			this.txtAcct.Top = 0.03125F;
			this.txtAcct.Width = 0.6875F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.19F;
			this.txtName.Left = 0.9375F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-family: \'Tahoma\'";
			this.txtName.Text = "Field3";
			this.txtName.Top = 0.03125F;
			this.txtName.Width = 2.0625F;
			// 
			// txtStNo
			// 
			this.txtStNo.Height = 0.19F;
			this.txtStNo.Left = 3.125F;
			this.txtStNo.Name = "txtStNo";
			this.txtStNo.Style = "font-family: \'Tahoma\'; text-align: left";
			this.txtStNo.Text = "Field4";
			this.txtStNo.Top = 0.03125F;
			this.txtStNo.Width = 0.5F;
			// 
			// txtStreet
			// 
			this.txtStreet.Height = 0.19F;
			this.txtStreet.Left = 3.6875F;
			this.txtStreet.Name = "txtStreet";
			this.txtStreet.Style = "font-family: \'Tahoma\'";
			this.txtStreet.Text = "Field5";
			this.txtStreet.Top = 0.03125F;
			this.txtStreet.Width = 1.8125F;
			// 
			// rptNewAccounts
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtdate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txttime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStreet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcct;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStNo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStreet;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtdate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txttime;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
