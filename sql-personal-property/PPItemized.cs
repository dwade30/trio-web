﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmPPItemized.
	/// </summary>
	public partial class frmPPItemized : BaseForm
	{
		public frmPPItemized()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.lblStatics = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.lblStatics.AddControlArrayElement(lblStatics_29, 29);
			this.lblStatics.AddControlArrayElement(lblStatics_27, 27);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPPItemized InstancePtr
		{
			get
			{
				return (frmPPItemized)Sys.GetInstance(typeof(frmPPItemized));
			}
		}

		protected frmPPItemized _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		bool bolStart;
		/// <summary>
		/// Used when user Deletes row
		/// </summary>
		/// <summary>
		/// Dim DeletedRows(1000)       As Long
		/// </summary>
		int lngDeletes;
		/// <summary>
		/// Used when user updates row
		/// </summary>
		int UpdatedRows;
		/// <summary>
		/// Dim UpdateItemized(1000)    As Long
		/// </summary>
		/// <summary>
		/// Used when user adds new row
		/// </summary>
		/// <summary>
		/// Dim AddedRows               As Long
		/// </summary>
		/// <summary>
		/// Dim NewItemized(1000)       As Long
		/// </summary>
		string[] gstrcategory = new string[10 + 1];
		bool boolDataChanged;
		// vbPorter upgrade warning: ItemizedAccountTotal As int	OnWrite(short, double)
		int ItemizedAccountTotal;
		bool Formactivating;
		bool Calculating;
		clsDRWrapper ppi = new clsDRWrapper();
		string strSQL;
		int CurrentRow;
		int CurrentCol;
		int colYFA;
		/// <summary>
		/// Year First Assessed
		/// </summary>
		int colreplacingvalue;
		/// <summary>
		/// Amount of value that counts as replacing, not as new valuation
		/// </summary>
		int colLN;
		int colKY;
		int colRB;
		int colCD;
		int colQT;
		int colDS;
		int colMO;
		int colYR;
		int colDY;
		int colSR;
		int colRC;
		int colCS;
		int colGD;
		int colFC;
		int colVL;
		int colLF;
		int colChange;
		// vbPorter upgrade warning: colYearsClaimed As Variant, short --> As int	OnWriteFCConvert.ToInt16(
		int colYearsClaimed;
		const int CNSTCOLEXEMPT = 2;
		private bool boolDontUnloadOthers;

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			framTransfer.Visible = false;
		}

		private void cmdLeased_Click(object sender, System.EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			modGlobal.SaveWindowSize(this);
			modPPGN.Statics.Exiting = true;
			//FC:FINAL:MSH - i.issue #1276: hide another forms and dispose the form before openning for correct focusing
			//FC:FINAL:DDU:#i1270 - close forms instead of comment their hide method
			frmPPItemized.InstancePtr.Close();
			frmPPMaster.InstancePtr.Close();
            //FC:FINAL:SBE - unload and dispose form only if it was loaded before
            if (frmPPLeased.InstancePtr.IsLoaded)
            {
                frmPPLeased.InstancePtr.Unload();
                frmPPLeased.InstancePtr.Dispose();
            }
			frmPPLeased.InstancePtr.Show(App.MainForm);
			// Unload frmPPItemized
		}

		private void cmdMaster_Click(object sender, System.EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
            // Call SaveWindowSize(Me)
            //FC:FINAL:MSH - i.issue #1276: hide another forms and dispose the form before openning for correct focusing
            //FC:FINAL:DDU:#i1270 - close forms instead of comment their hide method
            //frmPPItemized.InstancePtr.Hide();
            //frmPPLeased.InstancePtr.Hide();
            //FC:FINAL:SBE - unload and dispose form only if it was loaded before
            if (frmPPMaster.InstancePtr.IsLoaded)
            {
                frmPPMaster.InstancePtr.Unload();
                frmPPMaster.InstancePtr.Dispose();
            }
			frmPPMaster.InstancePtr.Show(App.MainForm);
			// frmPPItemized.Hide
			// Unload frmPPItemized
			// CallByName frmPPMaster, "Form_Activate", VbMethod
			// frmPPMaster.Show
		}

		private void cmdOk_Click(object sender, System.EventArgs e)
		{
			int lngStart = 0;
			int lngEnd = 0;
			int x;
			clsDRWrapper clsSave = new clsDRWrapper();
			int lngMaxLine;
			int lngCurLine;
			if (vs1.Row < 1)
			{
				MessageBox.Show("You must choose at least one row of data to transfer", "No Data Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (cmbTransferTo.SelectedIndex < 0)
			{
				MessageBox.Show("You must choose an account to transfer data to", "No Account Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (cmbTransferTo.ItemData(cmbTransferTo.SelectedIndex) <= 0)
			{
				MessageBox.Show("You must choose an account to transfer data to", "No Account Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (vs1.Row > vs1.RowSel)
			{
				lngStart = vs1.RowSel;
				lngEnd = vs1.Row;
			}
			else
			{
				lngStart = vs1.Row;
				lngEnd = vs1.RowSel;
			}
			lngMaxLine = 0;
			clsSave.Execute("update status set changeddata = '" + FCConvert.ToString(DateTime.Now) + "'", "twpp0000.vb1");
			clsSave.OpenRecordset("select * from ppitemized where account = " + FCConvert.ToString(cmbTransferTo.ItemData(cmbTransferTo.SelectedIndex)) + " order by line", modPPGN.strPPDatabase);
			if (!clsSave.EndOfFile())
			{
				clsSave.MoveLast();
				// TODO Get_Fields: Check the table for the column [line] and replace with corresponding Get_Field method
				lngMaxLine = FCConvert.ToInt32(clsSave.Get_Fields("line"));
			}
			lngCurLine = lngMaxLine + 1;
			for (x = lngStart; x <= lngEnd; x++)
			{
				clsSave.AddNew();
				clsSave.Set_Fields("line", lngCurLine);
				clsSave.Set_Fields("account", cmbTransferTo.ItemData(cmbTransferTo.SelectedIndex));
				clsSave.Set_Fields("rb", vs1.TextMatrix(x, colRB));
				clsSave.Set_Fields("cd", vs1.TextMatrix(x, colCD));
				clsSave.Set_Fields("quantity", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colQT))));
				clsSave.Set_Fields("description", Strings.Trim(vs1.TextMatrix(x, colDS)));
				clsSave.Set_Fields("month", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colMO))));
				clsSave.Set_Fields("year", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colYR))));
				clsSave.Set_Fields("dpyr", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colDY))));
				clsSave.Set_Fields("sro", vs1.TextMatrix(x, colSR));
				clsSave.Set_Fields("yearfirstassessed", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colYFA))));
				if (Conversion.Val(vs1.TextMatrix(x, colCS)) > 0)
				{
					clsSave.Set_Fields("amountreplacing", FCConvert.ToInt32(FCConvert.ToDouble(vs1.TextMatrix(x, colreplacingvalue))));
				}
				else
				{
					clsSave.Set_Fields("amountreplacing", 0);
				}
				clsSave.Set_Fields("exemptyear", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, CNSTCOLEXEMPT))));
				// clssave.Fields("beteexempt") = val(vs1.TextMatrix(x,))
				clsSave.Set_Fields("rcyr", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colRC))));
				if (Conversion.Val(vs1.TextMatrix(x, colCS)) > 0)
				{
					clsSave.Set_Fields("cost", FCConvert.ToInt32(FCConvert.ToDouble(vs1.TextMatrix(x, colCS))));
				}
				else
				{
					clsSave.Set_Fields("cost", 0);
				}
				clsSave.Set_Fields("gd", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colGD))));
				clsSave.Set_Fields("fctr", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colFC))));
				clsSave.Set_Fields("value", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colVL))));
				clsSave.Set_Fields("leasedto", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colLF))));
				clsSave.Update();
				lngCurLine += 1;
			}
			// x
			if (cmbTransfer_1.SelectedIndex == 1)
			{
				// delete these rows
				for (x = lngEnd; x >= lngStart; x--)
				{
					if (Conversion.Val(vs1.TextMatrix(x, colKY)) > 0)
					{
						GridDeleted.AddItem(FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colKY))));
					}
					vs1.RemoveItem(x);
				}
				// x
				vs1.Select(vs1.Row, vs1.Col);
				//Application.DoEvents();
				RefreshNumbers();
			}
			framTransfer.Visible = false;
		}

		public void frmPPItemized_Activated(object sender, System.EventArgs e)
		{
			if (!modSecurity.ValidPermissions_6(this, modSecurity.ACCOUNTMAINTENANCE))
			{
				MessageBox.Show("Your permission setting for account maintenance is set to none or is missing.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Close();
				return;
			}
			Formactivating = false;
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			// Call SetColWidths
			// vs1.FontSize = 20
			//FC:FINAL:RPU:#1263 - Make RowHeader height smaller
			vs1.RowHeight(0, 400);
		}

		private void frmPPItemized_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				mnuQuit_Click();
			}
		}

		private void frmPPItemized_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				mnuQuit_Click();
				return;
			}
			else if (KeyAscii == Keys.Return)
			{
				if (frmPPItemized.InstancePtr.ActiveControl.GetName() == vs1.Name)
				{
					KeyAscii = (Keys)0;
					if (vs1.Col < colLF)
					{
						vs1.Col += 1;
					}
				}
				else
				{
					KeyAscii = (Keys)0;
					Support.SendKeys("{TAB}", false);
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmPPItemized_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.End:
					{
						// Add Rows to End
						// See vs1_KeyDown
						break;
					}
				case Keys.Insert:
					{
						// Insert Row in Middle
						if (Shift == 2)
						{
							// ADD TO MIDDLE OF GRID
							vs1.AddItem("\t" + FCConvert.ToString(FCConvert.ToInt32(vs1.TextMatrix(vs1.Rows - 1, colLN)) + 1), vs1.Row);
							vs1.TextMatrix(vs1.Row, colLN, "NEW");
							vs1.TextMatrix(vs1.Row, colKY, "NEW");
							vs1.TextMatrix(vs1.Row, colRB, "");
							CurrentRow = vs1.Row;
							// 
							// vs1.Select vs1.Row, colRB
							// vs1.EditCell
						}
						break;
					}
				case Keys.F5:
					{
						// Refresh Grid
						// REFRESH Grid
						vs1.Refresh();
						// Case vbKeyF9                    'Calculate
						// vs1.Select vs1.Row, 1
						// Call Calculate
						break;
					}
			}
			//end switch
		}

		public void ReLoad()
		{
			clsDRWrapper clsTemp = new clsDRWrapper();
			int x;
			// vs1.FontSize = 7
			UpdatedRows = 0;
			GridDeleted.Rows = 0;
			// AddedRows = 0
			// Erase NewItemized()
			// Erase UpdateItemized()
			colLN = 0;
			colRB = 1;
			colCD = 3;
			colQT = 4;
			colDS = 5;
			colYFA = 6;
			colreplacingvalue = 8;
			colMO = 9;
			colYR = 10;
			colDY = 11;
			colSR = 12;
			colRC = 13;
			colCS = 14;
			colGD = 15;
			colFC = 16;
			colKY = 17;
			colVL = 18;
			colLF = 19;
			// actually leased TO not leased from
			colChange = 20;
			colYearsClaimed = 7;
			SetGridProperties();
			// 
			// Erase DeletedRows
			lngDeletes = 0;
			// 
			clsTemp.OpenRecordset("select * from ratiotrends", "twpp0000.vb1");
			if (!clsTemp.EndOfFile())
			{
				for (x = 1; x <= 9; x++)
				{
					gstrcategory[x] = FCConvert.ToString(clsTemp.Get_Fields_String("description"));
					clsTemp.MoveNext();
				}
				// x
			}
			strSQL = "SELECT * FROM PPItemized WHERE Account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount) + " ORDER BY [Line]";
			ppi.OpenRecordset(strSQL, modPPGN.strPPDatabase);
			// 
			modPPGN.Statics.ItemizedTable = true;
			modPPGN.Statics.Exiting = false;
			// Formactivating = True
			// 
			// 
			// Open "TSGNGN55.VB1" For Random As #31 Len = Len(WPP)
			// Get #31, 10, WPP
			// Close #31
			// DepYear = Val(WPP.GetData("WDEPYearPP") & "")
			// 
			// Call GetCostData
			// 
			// Fill Master Update Fields
			FillMasterLabels();
			// Check to see if there is data, if not add a blank line
            vs1.Rows = 1;
			if (ppi.EndOfFile() != true && ppi.BeginningOfFile() != true)
            {
                vs1.Redraw = false;
                FillGrid();
                vs1.Redraw = true;
			}
            else
            {
                AddNewItemized();
			}
			
            // vs1.Rows = 2
            
            // Call SetFixedSize(Me, TRIOWINDOWSIZEBIGGIE)
            // If MDIParent.Width > 250 Then
            // Me.Width = MDIParent.Width
            // Me.Left = 0
            // End If
            // vs1.FontSize = 8
            boolDataChanged = false;
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
		}

		public void frmPPItemized_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			Formactivating = true;
			ReLoad();
            //FC:FINAL:SBE - Modeless forms should be displayed as MDI child of MainForm
            //this.Show(FCForm.FormShowEnum.Modeless);
            this.Show(App.MainForm);
        }
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (boolDataChanged)
			{
				vs1.Row = 0;
				//Application.DoEvents();
				// vbPorter upgrade warning: intret As short, int --> As DialogResult
				DialogResult intret;
				intret = MessageBox.Show("Data has been changed" + "\r\n" + "Save changes made?", "Save Changes?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
				if (intret == DialogResult.Yes)
				{
					if (!NewSaveItemized())
					{
						e.Cancel = true;
						return;
					}
				}
				else if (intret == DialogResult.Cancel)
				{
					e.Cancel = true;
					return;
				}
			}
			Calculating = false;
			modPPGN.Statics.ItemizedTable = false;
		}

		private void frmPPItemized_Resize(object sender, System.EventArgs e)
		{
			SetColWidths();
			// vs1.Cell(flexcpFontSize, 0, 0, 0, vs1.Cols - 1) = 0.66 * vs1.FontSize
			// vs1.Cell(flexcpFontSize, 0, 0, 0, vs1.Cols - 1) = 0.9 * vs1.FontSize
		}

		public void UnloadOnlySelf()
		{
			boolDontUnloadOthers = true;
			Close();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			// Call SaveWindowSize(Me)
			// Erase DeletedRows
			lngDeletes = 0;
			GridDeleted.Rows = 0;
			modPPGN.Check_RB_Fields();
			modPPGN.Statics.ItemizedTable = false;
			if (!boolDontUnloadOthers)
			{
				//FC:FINAL:DDU:#i1270 - close forms instead of comment their hide method
				frmPPMaster.InstancePtr.Close();
				if (modPPGN.Statics.LeasedTable)
				{
					frmPPLeased.InstancePtr.Close();
				}
				//MDIParent.InstancePtr.BringToFront();
				//MDIParent.InstancePtr.Show();
			}
			// Set ppi = Nothing
			// Set pp = Nothing
		}

		private void mnuAdd_Click(object sender, System.EventArgs e)
		{
			int intCounter;
			int intTemp;
			vs1.Col = 0;
			intTemp = vs1.Row;
			// vs1.AddItem Format(vs1.Row, "0000"), vs1.Row
			// vs1.AddItem "NEW", vs1.Row
			// vs1.AddItem "NEW"
			if (vs1.Rows > 1)
			{
				if (vs1.Row == 0)
				{
					vs1.AddItem("NEW");
					CurrentRow = vs1.Rows - 1;
				}
				else
				{
					vs1.AddItem("NEW", intTemp);
					CurrentRow = intTemp;
				}
			}
			else
			{
				vs1.AddItem("NEW");
				CurrentRow = 1;
			}
			// CurrentRow = vs1.Row
			// vs1.Cell(flexcpText, CurrentRow, colLN) = Format(vs1.Row, "0000")
			vs1.TextMatrix(CurrentRow, colChange, FCConvert.ToString(true));
			vs1.TextMatrix(CurrentRow, colLN, "NEW");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colCD, "1");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colQT, "1");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colMO, "00");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colYR, "0000");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colreplacingvalue, "0");
			// If Month(Date) < 4 Then
			// default to previous year if before april first
			// vs1.Cell(flexcpText, CurrentRow, colYFA) = Year(Date) - 1
			// Else
			// vs1.Cell(flexcpText, CurrentRow, colYFA) = Year(Date)
			// End If
			if (modGlobal.Statics.CustomizeStuff.DefaultYearFirstAssessed == 0)
			{
				vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colYFA, DateTime.Today.Year);
			}
			else
			{
				vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colYFA, modGlobal.Statics.CustomizeStuff.DefaultYearFirstAssessed);
			}
			vs1.TextMatrix(CurrentRow, colYearsClaimed, FCConvert.ToString(0));
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colDY, "00");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colSR, "O");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colRC, "00");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colCS, "0");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colGD, "000");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colFC, "100");
			// For intCounter = vs1.Row + 1 To vs1.Rows - 1
			// If vs1.TextMatrix(intCounter, 0) <> "NEW" Then
			// vs1.TextMatrix(intCounter, 0) = Format(vs1.TextMatrix(intCounter, 0) + 1, "0000")
			// End If
			// Next
			RefreshNumbers();
		}

		public void mnuAdd_Click()
		{
			mnuAdd_Click(mnuAdd, new System.EventArgs());
		}

		private void mnuAddMultipleRows_Click(object sender, System.EventArgs e)
		{
			int intRows;
			int intCounter;
			intRows = FCConvert.ToInt32(Math.Round(Conversion.Val(Interaction.InputBox("Enter number of rows to add", "TRIO Software"))));
			for (intCounter = 1; intCounter <= intRows; intCounter++)
			{
				mnuAdd_Click();
			}
			RefreshNumbers();
		}

		private void mnuBackToMasterScreen_Click(object sender, System.EventArgs e)
		{
			// frmPPMaster.Show , MDIParent
			// frmPPItemized.Hide
			// Unload frmPPItemized
		}

		private void mnuCalc_Click(object sender, System.EventArgs e)
		{
			if (boolDataChanged)
			{
				if (MessageBox.Show("In order to calculate, current changes must be saved" + "\r\n" + "Save changes?", "Save Changes?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
				{
					if (!NewSaveItemized())
						return;
				}
				else
				{
					return;
				}
			}
			modPPCalculate.Statics.CalculateFromMaster = true;
			//FC:FINAL:DDU:#i1270 - fixed correct focus on frmPPCalculations when is opened
			this.Unload();
			frmPPCalculations.InstancePtr.Show(App.MainForm);
			frmPPItemized.InstancePtr.Close();
		}

		private void mnuLastBETEtoBETE_Click(object sender, System.EventArgs e)
		{
			int lngRow;
			int intYear;
			intYear = modGlobal.Statics.CustomizeStuff.CurrentBETEYear;
			if (intYear == 0)
				intYear = DateTime.Today.Year;
			if (MessageBox.Show("This will change all items marked as BETE exempt for the year " + FCConvert.ToString(intYear - 1) + " as BETE exempt for the year " + FCConvert.ToString(intYear) + "\r\n" + "Continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
			{
				for (lngRow = 1; lngRow <= vs1.Rows - 1; lngRow++)
				{
					if (Conversion.Val(vs1.TextMatrix(lngRow, CNSTCOLEXEMPT)) == intYear - 1)
					{
						vs1.TextMatrix(lngRow, CNSTCOLEXEMPT, FCConvert.ToString(intYear));
						vs1.TextMatrix(lngRow, colRB, "");
						vs1.TextMatrix(lngRow, colChange, FCConvert.ToString(true));
						boolDataChanged = true;
					}
				}
				// lngRow
			}
		}

		private void mnuPendingtoBETE_Click(object sender, System.EventArgs e)
		{
			int lngRow;
			int intYear;
			intYear = modGlobal.Statics.CustomizeStuff.CurrentBETEYear;
			if (intYear == 0)
				intYear = DateTime.Today.Year;
			if (MessageBox.Show("This will change all items marked as pending to be BETE exempt for the year " + FCConvert.ToString(intYear) + "\r\n" + "Continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
			{
				for (lngRow = 1; lngRow <= vs1.Rows - 1; lngRow++)
				{
					if (Strings.UCase(vs1.TextMatrix(lngRow, colRB)) == "P")
					{
						if (modGlobal.Statics.CustomizeStuff.CurrentBETEYear > 0)
						{
							vs1.TextMatrix(lngRow, CNSTCOLEXEMPT, FCConvert.ToString(modGlobal.Statics.CustomizeStuff.CurrentBETEYear));
						}
						else
						{
							vs1.TextMatrix(lngRow, CNSTCOLEXEMPT, FCConvert.ToString(DateTime.Today.Year));
						}
						vs1.TextMatrix(lngRow, colRB, "");
						vs1.TextMatrix(lngRow, colChange, FCConvert.ToString(true));
						boolDataChanged = true;
					}
				}
				// lngRow
			}
		}

		private void mnuQuit_Click(object sender, System.EventArgs e)
		{
			modPPGN.Statics.ItemizedTable = false;
			frmPPItemized.InstancePtr.Close();
		}

		public void mnuQuit_Click()
		{
			mnuQuit_Click(mnuQuit, new System.EventArgs());
		}

		private void mnuRemove_Click(object sender, System.EventArgs e)
		{
			//FC:FINAL:MSH - i.issue #1269: replace SendKeys, because it is not implemented
			//Support.SendKeys("{DELETE}", false);
			vs1_KeyDownEvent(vs1, new KeyEventArgs(Keys.Delete));
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			// Call SaveItemized
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			if (NewSaveItemized())
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
			else
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Save Failed", "Not Saved", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void mnuSaveQuit_Click(object sender, System.EventArgs e)
		{
			// Call SaveItemized
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			if (NewSaveItemized())
			{
				modPPGN.Statics.ItemizedTable = false;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				Close();
			}
			else
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Save Failed", "Not Saved", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void mnuTransfer_Click(object sender, System.EventArgs e)
		{
			// will copy or move highlighted rows to another account
			FillcmbTransfer();
			framTransfer.Visible = true;
		}

		private void FillcmbTransfer()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strJoin;
			strJoin = modPPGN.GetMasterJoinForJoin(true);
			cmbTransferTo.Clear();
			// Call clsLoad.OpenRecordset("select account,name from ppmaster where not deleted = 1 order by account", modPPGN.strPPDatabase)
			clsLoad.OpenRecordset("select account,name from " + strJoin + " where not deleted = 1 order by account", modPPGN.strPPDatabase);
			while (!clsLoad.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				cmbTransferTo.AddItem(clsLoad.Get_Fields("account") + "   " + clsLoad.Get_Fields_String("name"));
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				cmbTransferTo.ItemData(cmbTransferTo.NewIndex, FCConvert.ToInt32(clsLoad.Get_Fields("Account")));
				clsLoad.MoveNext();
			}
		}

		private void vs1_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			int fn;
			// For fn = 1 To 1000
			// If NewItemized(fn) = Row Then Exit Sub
			// If UpdateItemized(fn) = Row Then Exit Sub
			// If UpdateItemized(fn) = 0 Then Exit For
			// Next fn
			// UpdatedRows = UpdatedRows + 1
			// If UpdatedRows >= 999 Then
			// UpdatedRows = UpdatedRows - 1
			// vs1.Select 1, colCD
			// MsgBox "You must save your work and then proceed."
			// Exit Sub
			// End If
			// If Val(vs1.TextMatrix(Row, colKY)) <> 0 Then UpdateItemized(UpdatedRows) = vs1.TextMatrix(Row, colKY)
			boolDataChanged = true;
			if (CurrentCol == colCD)
			{
				if (vs1.TextMatrix(CurrentRow, colCD) == "-")
				{
					if (Strings.Trim(vs1.TextMatrix(CurrentRow, colDS)) != "")
					{
						vs1.TextMatrix(CurrentRow, colQT, "");
						// vs1.TextMatrix(CurrentRow, colDS) = ""
						vs1.TextMatrix(CurrentRow, colMO, "");
						vs1.TextMatrix(CurrentRow, colYR, "");
						vs1.TextMatrix(CurrentRow, colDY, "");
						vs1.TextMatrix(CurrentRow, colSR, "");
						vs1.TextMatrix(CurrentRow, colSR, "");
						vs1.TextMatrix(CurrentRow, colRC, "");
						vs1.TextMatrix(CurrentRow, colCS, "");
						vs1.TextMatrix(CurrentRow, colGD, "");
						vs1.TextMatrix(CurrentRow, colFC, "");
						vs1.TextMatrix(CurrentRow, colVL, "");
						vs1.TextMatrix(CurrentRow, colYFA, "");
						vs1.TextMatrix(CurrentRow, colreplacingvalue, "");
					}
				}
				else if (Strings.UCase(Strings.Trim(vs1.TextMatrix(CurrentRow, colCD))) == "P")
				{
				}
				else if (Conversion.Val(vs1.TextMatrix(vs1.Row, colCD)) > 0)
				{
					if (Strings.Trim(vs1.TextMatrix(vs1.Row, colDS)) == string.Empty)
					{
						// if description is blank
						switch (FCConvert.ToInt32(Conversion.Val(vs1.TextMatrix(vs1.Row, colCD))))
						{
							case 1:
								{
									vs1.TextMatrix(vs1.Row, colDS, gstrcategory[1]);
									break;
								}
							case 2:
								{
									vs1.TextMatrix(vs1.Row, colDS, gstrcategory[2]);
									break;
								}
							case 3:
								{
									vs1.TextMatrix(vs1.Row, colDS, gstrcategory[3]);
									break;
								}
							case 4:
								{
									vs1.TextMatrix(vs1.Row, colDS, gstrcategory[4]);
									break;
								}
							case 5:
								{
									vs1.TextMatrix(vs1.Row, colDS, gstrcategory[5]);
									break;
								}
							case 6:
								{
									vs1.TextMatrix(vs1.Row, colDS, gstrcategory[6]);
									break;
								}
							case 7:
								{
									vs1.TextMatrix(vs1.Row, colDS, gstrcategory[7]);
									break;
								}
							case 8:
								{
									vs1.TextMatrix(vs1.Row, colDS, gstrcategory[8]);
									break;
								}
							case 9:
								{
									vs1.TextMatrix(vs1.Row, colDS, gstrcategory[9]);
									break;
								}
						}
						//end switch
					}
				}
			}
			vs1.TextMatrix(vs1.Row, colChange, FCConvert.ToString(true));
		}

		private void ResetChanges()
		{
			int x;
			for (x = 1; x <= vs1.Rows - 1; x++)
			{
				vs1.TextMatrix(x, colChange, FCConvert.ToString(false));
			}
			// x
		}
		//FC:TODO:AM
		//private void vs1_BeforeScrollTip(object sender, AxVSFlex7L._IVSFlexGridEvents_BeforeScrollTipEvent e)
		//{
		//	vs1.ScrollTipText = FCConvert.ToString(vs1.MouseRow);
		//}
		private void vs1_CellChanged(object sender, DataGridViewCellEventArgs e)
		{
			boolDataChanged = true;
		}

		private void vs1_ChangeEdit(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void vs1_DragDrop(object sender, Wisej.Web.DragEventArgs e)
		{
			if (FCConvert.ToString(vs1.Tag) == "")
				return;
			if (vs1.MouseRow == 0)
				return;
			vs1.Redraw = false;
			int xxx;
			int rr;
			rr = vs1.RowSel - vs1.Row;
			if (rr < 0)
				rr *= -1;
			if (vs1.MouseRow < vs1.Row)
			{
				for (xxx = rr; xxx >= 1; xxx--)
				{
					vs1.RowPosition(FCConvert.ToInt32(Conversion.Val(vs1.Tag) - 1 + rr), vs1.MouseRow);
				}
				// xxx
			}
			else
			{
				for (xxx = 1; xxx <= rr; xxx++)
				{
					vs1.RowPosition(FCConvert.ToInt32(Conversion.Val(vs1.Tag)), vs1.MouseRow);
				}
				// xxx
			}
			vs1.RowPosition(FCConvert.ToInt32(Conversion.Val(vs1.Tag)), vs1.MouseRow);
			RefreshNumbers();
			vs1.Redraw = true;
			vs1.Select(1, colLN);
		}

		private void vs1_DragOver(object sender, Wisej.Web.DragEventArgs e)
		{
			if (vs1.MouseRow == 0)
			{
				// vs1.DragIcon = LoadPicture("NO_1.CUR")
			}
			else
			{
				// vs1.DragIcon = LoadPicture("H_MOVE.CUR")
			}
		}

		private void vs1_EnterCell(object sender, DataGridViewCellEventArgs e)
		{
			//int Col;
			//Col = vs1.Col;
			//FC:FINAL:MSH - i.issue #1268: save correct indexes of the cell
			int row = vs1.GetFlexRowIndex(e.RowIndex);
			int col = vs1.GetFlexColIndex(e.ColumnIndex);
			// If calculating then no edit in last row.
			if (Calculating == true)
			{
				if (row == vs1.Rows - 1)
				{
					vs1.Editable = FCGrid.EditableSettings.flexEDNone;
					return;
				}
			}
			else
			{
				// Line & Value   
				if (col == colLN || col == colVL)
				{
					// Input NOT Allowed
					vs1.Editable = FCGrid.EditableSettings.flexEDNone;
				}
				else
				{
					// Input Allowed
					vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					// vs1.EditCell
					vs1.EditSelStart = 0;
					// vs1.EditSelLength = 0
					vs1.EditSelLength = vs1.TextMatrix(row, col).Length;
				}
			}
		}

		private void vs1_Enter(object sender, System.EventArgs e)
		{
			if (bolStart == false)
			{
				CurrentCol = 1;
				CurrentRow = 1;
				// vs1.Editable = True
				bolStart = true;
			}
		}

		private void vs1_KeyDownEvent(object sender, KeyEventArgs e)
		{
			int lngTemp1 = 0;
			int lngTemp2 = 0;
			// vbPorter upgrade warning: strTemp As string	OnWriteFCConvert.ToInt32(
			string strTemp = "";
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.F9)
			{
				KeyCode = 0;
				if (vs1.Col == colCD)
				{
					modPPHelp.HelpCatNumbers();
					frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				}
				else if (vs1.Col == colDS)
				{
					modPPHelp.HelpDescription();
                    frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				}
				else if (vs1.Col == colYR)
				{
					modPPHelp.HelpYear();
					frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				}
				else if (vs1.Col == colDY)
				{
					modPPHelp.HelpDepreciationYear();
					frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				}
				else if (vs1.Col == colSR)
				{
					modPPHelp.HelpSRO();
					frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				}
				else if (vs1.Col == colRC)
				{
					modPPHelp.HelpRCYear();
					frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				}
				else if (vs1.Col == colCS)
				{
					modPPHelp.HelpReplaceCost();
					frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				}
				else if (vs1.Col == colGD)
				{
					modPPHelp.HelpPercGood();
					frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				}
				else if (vs1.Col == colFC)
				{
					modPPHelp.HelpFactor();
					frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				}
				return;
			}
			if (KeyCode == Keys.F2)
			{
				KeyCode = 0;
				if (vs1.Row > 0)
				{
					lngTemp1 = frmDefaultItems.InstancePtr.Init();
					if (lngTemp1 > 0)
					{
						// fill it in
						clsDRWrapper rsLoad = new clsDRWrapper();
						rsLoad.OpenRecordset("select * from defaultitems where id = " + FCConvert.ToString(lngTemp1), "twpp0000.vb1");
						if (!rsLoad.EndOfFile())
						{
							// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
							vs1.TextMatrix(vs1.Row, colCD, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("category"))));
							// TODO Get_Fields: Check the table for the column [cost] and replace with corresponding Get_Field method
							vs1.TextMatrix(vs1.Row, colCS, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("cost"))));
							vs1.TextMatrix(vs1.Row, colDY, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("depreciationyears"))));
							// TODO Get_Fields: Check the table for the column [sro] and replace with corresponding Get_Field method
							switch (FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields("sro"))))
							{
								case 1:
									{
										vs1.TextMatrix(vs1.Row, colSR, "S");
										vs1.TextMatrix(vs1.Row, colYR, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("purchaseyear"))));
										break;
									}
								case 2:
									{
										vs1.TextMatrix(vs1.Row, colSR, "R");
										lngTemp2 = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("purchaseyear"))));
										if (lngTemp2 > 1000)
										{
											vs1.TextMatrix(vs1.Row, colRC, Strings.Right(FCConvert.ToString(rsLoad.Get_Fields_Int32("purchaseyear")), 2));
										}
										break;
									}
								default:
									{
										vs1.TextMatrix(vs1.Row, colSR, "O");
										vs1.TextMatrix(vs1.Row, colYR, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("purchaseyear"))));
										break;
									}
							}
							//end switch
							vs1.TextMatrix(vs1.Row, colDS, FCConvert.ToString(rsLoad.Get_Fields_String("description")));
						}
					}
				}
				return;
			}
			if ((vs1.Row == (vs1.Rows - 1)) && KeyCode == Keys.Down)
			{
				KeyCode = 0;
				AddNewItemized();
				vs1.Select(CurrentRow, colRB);
				return;
			}
			if (KeyCode == Keys.Delete)
			{
				int JJ;
				if (vs1.Row < 1)
					return;
				if (vs1.Row > vs1.RowSel)
				{
					lngTemp1 = vs1.Row;
					lngTemp2 = vs1.RowSel;
					vs1.Row = lngTemp2;
					vs1.RowSel = lngTemp1;
					lngTemp1 = vs1.Row;
					lngTemp2 = vs1.RowSel;
				}
				else
				{
					lngTemp1 = vs1.Row;
					lngTemp2 = vs1.RowSel;
				}
				lngTemp1 = vs1.Row;
				lngTemp2 = vs1.RowSel;
				if (lngTemp1 < 1)
					lngTemp1 = 1;
				for (JJ = lngTemp2; JJ >= lngTemp1; JJ--)
				{
					// If vs1.Col = 1 Then
					// If Val(vs1.TextMatrix(JJ, colLN)) <> 0 Then
					// lngDeletes = lngDeletes + 1
					// If lngDeletes >= 1000 Then
					// MsgBox "You must save your work before deleting anymore lines.", vbOKOnly + vbExclamation, "DELETE"
					// Else
					if (Conversion.Val(vs1.TextMatrix(JJ, colKY)) > 0)
					{
						GridDeleted.AddItem(FCConvert.ToString(Conversion.Val(vs1.TextMatrix(JJ, colKY))));
						// DeletedRows(lngDeletes) = Val(vs1.TextMatrix(vs1.RowSel, colKY))
					}
					vs1.RemoveItem(JJ);
					// End If
					// ElseIf vs1.TextMatrix(JJ, colLN) = "NEW" Then
					// vs1.RemoveItem (JJ)
					// End If
					// End If
				}
				// JJ
				// vs1.Select vs1.Row, vs1.Col
				boolDataChanged = true;
				//Application.DoEvents();
				RefreshNumbers();
			}
			// If KeyCode = 13 And CurrentCol = colval Then
			if (CurrentCol == colRB)
			{
				// * key from keypad
				bool boolIgnore = false;
				boolIgnore = false;
				//FC:FINAL:BBE - use enum Keys for KeyCode == 106
				if (KeyCode == Keys.Multiply)
				{
					if (Strings.Trim(vs1.TextMatrix(vs1.Row, colRB) + "") == "*")
					{
						vs1.TextMatrix(vs1.Row, colRB, " ");
					}
					else
					{
						vs1.TextMatrix(vs1.Row, colRB, "*");
						vs1.TextMatrix(vs1.Row, CNSTCOLEXEMPT, "");
					}
					vs1.TextMatrix(vs1.Row, colChange, FCConvert.ToString(true));
					if (vs1.Row < vs1.Rows - 1)
					{
						Support.SendKeys("{DOWN}", false);
					}
					// KeyCode = 0
				}
				//FC:FINAL:BBE - use enum Keys for KeyCode == 56
				else if (KeyCode == Keys.D8 && e.Shift)
				{
					// MsgBox "D"
					if (Strings.Trim(vs1.TextMatrix(vs1.Row, colRB) + "") == "*")
					{
						vs1.TextMatrix(vs1.Row, colRB, " ");
					}
					else
					{
						vs1.TextMatrix(vs1.Row, colRB, "*");
						vs1.TextMatrix(vs1.Row, CNSTCOLEXEMPT, "");
					}
					vs1.TextMatrix(vs1.Row, colChange, FCConvert.ToString(true));
					// SendKeys "{DOWN}"
					if (vs1.Row < (vs1.Rows - 1))
					{
						vs1.Row += 1;
					}
					// DoEvents
					// If (vs1.Rows - 1) - vs1.Row < 5 Then
					// Call AddNewItemized
					// vs1.Row = vs1.Row - 1
					// End If
					// vs1.Select vs1.Row, vs1.Col, vs1.Row, vs1.Col
					// vs1.Select vs1.Row + 1, vs1.Col, vs1.Row + 1, vs1.Col
					// DoEvents
				}
				else
				{
					switch (KeyCode)
					{
						case Keys.D1:
						case Keys.NumPad1:
							{
								lngTemp1 = 1;
								break;
							}
						case Keys.D2:
						case Keys.NumPad2:
							{
								lngTemp1 = 2;
								break;
							}
						case Keys.D3:
						case Keys.NumPad3:
							{
								lngTemp1 = 3;
								break;
							}
						case Keys.D4:
						case Keys.NumPad4:
							{
								lngTemp1 = 4;
								break;
							}
						case Keys.D5:
						case Keys.NumPad5:
							{
								lngTemp1 = 5;
								break;
							}
						case Keys.D6:
						case Keys.NumPad6:
							{
								lngTemp1 = 6;
								break;
							}
						case Keys.D7:
						case Keys.NumPad7:
							{
								lngTemp1 = 7;
								break;
							}
						case Keys.D8:
						case Keys.NumPad8:
							{
								lngTemp1 = 8;
								break;
							}
						case Keys.D9:
						case Keys.NumPad9:
							{
								lngTemp1 = 9;
								break;
							}
						case Keys.D0:
						case Keys.NumPad0:
							{
								lngTemp1 = 0;
								break;
							}
						case Keys.P:
							{
								if (Strings.UCase(Strings.Trim(vs1.TextMatrix(vs1.Row, colRB) + "")) == "P")
								{
									vs1.TextMatrix(vs1.Row, colRB, " ");
								}
								else
								{
									vs1.TextMatrix(vs1.Row, colRB, "P");
									vs1.TextMatrix(vs1.Row, CNSTCOLEXEMPT, "");
								}
								vs1.TextMatrix(vs1.Row, colChange, FCConvert.ToString(true));
								if (e.Shift)
								{
									if (vs1.Row < vs1.Rows - 1)
									{
										Support.SendKeys("{DOWN}", false);
									}
								}
								else
								{
									if (vs1.Row < (vs1.Rows - 1))
									{
										vs1.Row += 1;
									}
								}
								return;
							}
						case Keys.Space:
							{
								vs1.TextMatrix(vs1.Row, colRB, " ");
								vs1.TextMatrix(vs1.Row, CNSTCOLEXEMPT, "");
								vs1.TextMatrix(vs1.Row, colChange, FCConvert.ToString(true));
								if (e.Shift)
								{
									if (vs1.Row < vs1.Rows - 1)
									{
										Support.SendKeys("{DOWN}", false);
									}
								}
								else
								{
									if (vs1.Row < (vs1.Rows - 1))
									{
										vs1.Row += 1;
									}
								}
								return;
							}
						default:
							{
								boolIgnore = true;
								break;
							}
					}
					//end switch
					if (!boolIgnore)
					{
						lngTemp2 = DateTime.Today.Year;
						strTemp = FCConvert.ToString(DateTime.Today.Year);
						fecherFoundation.Strings.MidSet(ref strTemp, 4, 1, FCConvert.ToString(lngTemp1));
						if (Conversion.Val(strTemp) > lngTemp2 + 1)
						{
							strTemp = FCConvert.ToString(DateTime.Today.Year);
						}
						else if (Conversion.Val(strTemp) < lngTemp2 - 8)
						{
							strTemp = FCConvert.ToString(DateTime.Today.Year + 1);
						}
						KeyCode = 0;
						vs1.TextMatrix(vs1.Row, colRB, " ");
						vs1.TextMatrix(vs1.Row, CNSTCOLEXEMPT, strTemp);
						// vs1.Col = colCD
						vs1.TextMatrix(vs1.Row, colChange, FCConvert.ToString(true));
						if (e.Shift)
						{
							if (vs1.Row < vs1.Rows - 1)
							{
								Support.SendKeys("{DOWN}", false);
							}
						}
						else
						{
							if (vs1.Row < (vs1.Rows - 1))
							{
								vs1.Row += 1;
							}
						}
					}
				}
			}
			else if (CurrentCol == colCD)
			{
				if (KeyCode == Keys.P)
				{
					if (CurrentRow > 0)
					{
						string strNewRow = "";
						string strCD = "";
						strNewRow = FCConvert.ToString(CurrentRow + 1) + "\t";
						// colln
						strNewRow += vs1.TextMatrix(CurrentRow, colRB) + "\t";
						// RB
						strNewRow += "\t";
						// ?
						strNewRow += vs1.TextMatrix(CurrentRow, colCD) + "\t";
						// colcd
						strNewRow += vs1.TextMatrix(CurrentRow, colQT) + "\t";
						// colqt
						strNewRow += vs1.TextMatrix(CurrentRow, colDS) + "\t";
						// colds
						if (modGlobal.Statics.CustomizeStuff.DefaultYearFirstAssessed == 0)
						{
							strNewRow += FCConvert.ToString(DateTime.Today.Year) + "\t";
						}
						else
						{
							strNewRow += FCConvert.ToString(modGlobal.Statics.CustomizeStuff.DefaultYearFirstAssessed) + "\t";
						}
						strNewRow += vs1.TextMatrix(CurrentRow, colreplacingvalue) + "\t";
						// replacing value
						strNewRow += vs1.TextMatrix(CurrentRow, colMO) + "\t";
						// colmo
						strNewRow += vs1.TextMatrix(CurrentRow, colYR) + "\t";
						strNewRow += vs1.TextMatrix(CurrentRow, colDY) + "\t";
						// dy
						strNewRow += vs1.TextMatrix(CurrentRow, colSR) + "\t";
						// sr
						strNewRow += vs1.TextMatrix(CurrentRow, colRC) + "\t";
						// rc
						strNewRow += vs1.TextMatrix(CurrentRow, colCS) + "\t";
						// colcs
						strNewRow += vs1.TextMatrix(CurrentRow, colGD) + "\t";
						// colgd
						strNewRow += vs1.TextMatrix(CurrentRow, colFC) + "\t";
						// colfc
						strNewRow += "\t" + 0;
						// colky
						strNewRow += "\t";
						// colvl
						strNewRow += "\t";
						// collf
						strNewRow += "\t" + true;
						// colchange
						vs1.AddItem(strNewRow, CurrentRow + 1);
						vs1.TextMatrix(CurrentRow + 1, colChange, FCConvert.ToString(true));
						RefreshNumbers();
						// last or second to last row...
						// If CurrentRow = vs1.Rows - 1 Then
						// add a row and copy defaults into the cells
						// vs1.Rows = vs1.Rows + 1
						// vs1.Cell(flexcpText, CurrentRow + 1, colLN) = Format(vs1.Row + 1, "0000")
						// vs1.Cell(flexcpText, CurrentRow + 1, colCD) = "1"
						// vs1.Cell(flexcpText, CurrentRow + 1, colQT) = "1"
						// vs1.Cell(flexcpText, CurrentRow + 1, colMO) = "00"
						// vs1.Cell(flexcpText, CurrentRow + 1, colYR) = "0000"
						// If CustomizeStuff.DefaultYearFirstAssessed = 0 Then
						// vs1.Cell(flexcpText, CurrentRow + 1, colYFA) = Year(Date)
						// Else
						// vs1.Cell(flexcpText, CurrentRow + 1, colYFA) = CustomizeStuff.DefaultYearFirstAssessed
						// End If
						// vs1.Cell(flexcpText, CurrentRow + 1, colreplacingvalue) = "0"
						// vs1.Cell(flexcpText, CurrentRow + 1, colDY) = "00"
						// vs1.Cell(flexcpText, CurrentRow + 1, colSR) = "O"
						// vs1.Cell(flexcpText, CurrentRow + 1, colRC) = "00"
						// vs1.Cell(flexcpText, CurrentRow + 1, colCS) = "0"
						// vs1.Cell(flexcpText, CurrentRow + 1, colGD) = "000"
						// vs1.Cell(flexcpText, CurrentRow + 1, colFC) = "100"
						// End If
						// 
						// actually move the data from the line before
						// Dim iii As Integer
						// For iii = colRB To colFC
						// vs1.TextMatrix(CurrentRow, iii) = vs1.TextMatrix(CurrentRow - 1, iii)
						// Next iii
						// vs1.Rows = vs1.Rows + 1
						KeyCode = 0;
						// vs1.Row = vs1.Row + 1
						Support.SendKeys("{DOWN}", false);
						// vs1.Editable = True
						// SendKeys "{DOWN}"
					}
				}
				else if (KeyCode == Keys.D)
				{
					vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colLN, Strings.Format(vs1.Row, "0000"));
					vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colCD, "1");
					vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colQT, "1");
					vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colMO, "00");
					vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colYR, "0000");
					if (modGlobal.Statics.CustomizeStuff.DefaultYearFirstAssessed == 0)
					{
						vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colYFA, DateTime.Today.Year);
					}
					else
					{
						vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colYFA, modGlobal.Statics.CustomizeStuff.DefaultYearFirstAssessed);
					}
					vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colreplacingvalue, "0");
					vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colDY, "00");
					vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colSR, "O");
					vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colRC, "00");
					vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colCS, "0");
					vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colGD, "000");
					vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colFC, "100");
					vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colDS, "");
					// vs1.Rows = vs1.Rows + 1
					KeyCode = 0;
					Support.SendKeys("{DOWN}", false);
					// SendKeys "{DOWN}"
				}
				else
				{
				}
			}
			// make the arrow keys work
			//FC:FINAL:BBE - use enum Keys for KeyCode == 37
			if (vs1.Col == 1 && KeyCode == Keys.Left)
			{
				if (vs1.Row > 1)
				{
					vs1.Row -= 1;
					vs1.Col = colLF;
					KeyCode = 0;
				}
			}
			//FC:FINAL:BBE - use enum Keys for KeyCode == 39
			else if (vs1.Col == colLF && KeyCode == Keys.Right)
			{
				if (vs1.Row != vs1.Rows - 1)
				{
					vs1.Row += 1;
					vs1.Col = 1;
					KeyCode = 0;
				}
			}
			if (KeyCode == Keys.Return)
			{
				if (vs1.Row < (vs1.Rows - 1))
				{
					if (vs1.Col == colLF)
					{
						vs1.Col = colRB;
						vs1.Row += 1;
						KeyCode = 0;
					}
				}
				if (CurrentRow == vs1.Rows - 1)
				{
					if (vs1.Col == colLF)
					{
						KeyCode = 0;
						AddNewItemized();
						vs1.Select(CurrentRow, colCD);
						return;
					}
				}
			}
			// If KeyCode = vbKeyReturn Then
			// vs1.EditSelStart = Len(Trim(vs1.TextMatrix(vs1.Row, vs1.Col)))
			// End If
		}

		private void vs1_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.F2)
			{
				KeyCode = 0;
				if (vs1.Row > 0)
				{
					int lngTemp1 = 0;
					int lngTemp2 = 0;
					lngTemp1 = frmDefaultItems.InstancePtr.Init();
					if (lngTemp1 > 0)
					{
						// fill it in
						clsDRWrapper rsLoad = new clsDRWrapper();
						rsLoad.OpenRecordset("select * from defaultitems where id = " + FCConvert.ToString(lngTemp1), "twpp0000.vb1");
						if (!rsLoad.EndOfFile())
						{
							// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
							vs1.TextMatrix(vs1.Row, colCD, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("category"))));
							// TODO Get_Fields: Check the table for the column [cost] and replace with corresponding Get_Field method
							vs1.TextMatrix(vs1.Row, colCS, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("cost"))));
							vs1.TextMatrix(vs1.Row, colDY, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("depreciationyears"))));
							// TODO Get_Fields: Check the table for the column [sro] and replace with corresponding Get_Field method
							switch (FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields("sro"))))
							{
								case 1:
									{
										vs1.TextMatrix(vs1.Row, colSR, "S");
										vs1.TextMatrix(vs1.Row, colYR, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("purchaseyear"))));
										break;
									}
								case 2:
									{
										vs1.TextMatrix(vs1.Row, colSR, "R");
										lngTemp2 = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("purchaseyear"))));
										if (lngTemp2 > 1000)
										{
											vs1.TextMatrix(vs1.Row, colRC, Strings.Right(FCConvert.ToString(rsLoad.Get_Fields_Int32("purchaseyear")), 2));
										}
										break;
									}
								default:
									{
										vs1.TextMatrix(vs1.Row, colSR, "O");
										vs1.TextMatrix(vs1.Row, colYR, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("purchaseyear"))));
										break;
									}
							}
							//end switch
							vs1.TextMatrix(vs1.Row, colDS, FCConvert.ToString(rsLoad.Get_Fields_String("description")));
						}
					}
				}
				return;
			}
			if (vs1.Col == colCD)
			{
				if (KeyCode == Keys.P)
				{
					if (CurrentRow > 0)
					{
						vs1.EditText = vs1.TextMatrix(CurrentRow, colCD);
						string strNewRow = "";
						string strCD = "";
						strNewRow = FCConvert.ToString(CurrentRow + 1) + "\t";
						// colln
						strNewRow += vs1.TextMatrix(CurrentRow, colRB) + "\t";
						// RB
						strNewRow += "\t";
						// ?
						strNewRow += vs1.TextMatrix(CurrentRow, colCD) + "\t";
						// colcd
						strNewRow += vs1.TextMatrix(CurrentRow, colQT) + "\t";
						// colqt
						strNewRow += vs1.TextMatrix(CurrentRow, colDS) + "\t";
						// colds
						if (modGlobal.Statics.CustomizeStuff.DefaultYearFirstAssessed == 0)
						{
							strNewRow += FCConvert.ToString(DateTime.Today.Year) + "\t";
						}
						else
						{
							strNewRow += FCConvert.ToString(modGlobal.Statics.CustomizeStuff.DefaultYearFirstAssessed) + "\t";
						}
						strNewRow += vs1.TextMatrix(CurrentRow, colreplacingvalue) + "\t";
						// replacing value
						strNewRow += vs1.TextMatrix(CurrentRow, colMO) + "\t";
						// colmo
						strNewRow += vs1.TextMatrix(CurrentRow, colYR) + "\t";
						strNewRow += vs1.TextMatrix(CurrentRow, colDY) + "\t";
						// dy
						strNewRow += vs1.TextMatrix(CurrentRow, colSR) + "\t";
						// sr
						strNewRow += vs1.TextMatrix(CurrentRow, colRC) + "\t";
						// rc
						strNewRow += vs1.TextMatrix(CurrentRow, colCS) + "\t";
						// colcs
						strNewRow += vs1.TextMatrix(CurrentRow, colGD) + "\t";
						// colgd
						strNewRow += vs1.TextMatrix(CurrentRow, colFC) + "\t";
						// colfc
						strNewRow += "\t" + 0;
						// colky
						strNewRow += "\t";
						// colvl
						strNewRow += "\t";
						// collf
						strNewRow += "\t" + true;
						// colchange
						vs1.AddItem(strNewRow, CurrentRow + 1);
						vs1.TextMatrix(CurrentRow + 1, colChange, FCConvert.ToString(true));
						RefreshNumbers();
						// last or second to last row...
						// If CurrentRow = vs1.Rows - 1 Then
						// KeyCode = 0
						// add a row and copy defaults into the cells
						// vs1.Rows = vs1.Rows + 1
						// vs1.Cell(flexcpText, CurrentRow + 1, colLN) = Format(vs1.Row, "0000")
						// vs1.Cell(flexcpText, CurrentRow + 1, colCD) = "1"
						// vs1.Cell(flexcpText, CurrentRow + 1, colQT) = "1"
						// vs1.Cell(flexcpText, CurrentRow + 1, colMO) = "00"
						// vs1.Cell(flexcpText, CurrentRow + 1, colYR) = "0000"
						// If CustomizeStuff.DefaultYearFirstAssessed = 0 Then
						// vs1.Cell(flexcpText, CurrentRow + 1, colYFA) = Year(Date)
						// Else
						// vs1.Cell(flexcpText, CurrentRow + 1, colYFA) = CustomizeStuff.DefaultYearFirstAssessed
						// End If
						// vs1.Cell(flexcpText, CurrentRow + 1, colreplacingvalue) = "0"
						// vs1.Cell(flexcpText, CurrentRow + 1, colDY) = "00"
						// vs1.Cell(flexcpText, CurrentRow + 1, colSR) = "O"
						// vs1.Cell(flexcpText, CurrentRow + 1, colRC) = "00"
						// vs1.Cell(flexcpText, CurrentRow + 1, colCS) = "0"
						// vs1.Cell(flexcpText, CurrentRow + 1, colGD) = "000"
						// vs1.Cell(flexcpText, CurrentRow + 1, colFC) = "100"
						// End If
						// 
						// actually move the data from the line before
						// Dim iii As Integer
						// For iii = colRB To colFC
						// vs1.TextMatrix(CurrentRow, iii) = vs1.TextMatrix(CurrentRow - 1, iii)
						// Next iii
						// vs1.Rows = vs1.Rows + 1
						KeyCode = 0;
						// vs1.Row = vs1.Row + 1
						Support.SendKeys("{DOWN}", false);
						// vs1.Editable = True
						// SendKeys "{DOWN}"
					}
				}
			}
			else if (vs1.Col == colRB)
			{
				vs1.TextMatrix(vs1.Row, colChange, FCConvert.ToString(true));
			}
			switch (KeyCode)
			{
				case Keys.F9:
					{
						KeyCode = 0;
						if (vs1.Col == colCD)
						{
							modPPHelp.HelpCatNumbers();
							frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
						}
						else if (vs1.Col == colDS)
						{
							modPPHelp.HelpDescription();
							frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
						}
						else if (vs1.Col == colYR)
						{
							modPPHelp.HelpYear();
							frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
						}
						else if (vs1.Col == colDY)
						{
							modPPHelp.HelpDepreciationYear();
							frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
						}
						else if (vs1.Col == colSR)
						{
							modPPHelp.HelpSRO();
							frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
						}
						else if (vs1.Col == colRC)
						{
							modPPHelp.HelpRCYear();
							frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
						}
						else if (vs1.Col == colCS)
						{
							modPPHelp.HelpReplaceCost();
							frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
						}
						else if (vs1.Col == colGD)
						{
							modPPHelp.HelpPercGood();
							frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
						}
						else if (vs1.Col == colFC)
						{
							modPPHelp.HelpFactor();
							frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
						}
						return;
					}
			}
			//end switch
			if (KeyCode == Keys.Return)
			{
				// If vs1.Col = colCD Then
				// If Val(vs1.TextMatrix(vs1.Row, colCD)) > 0 Then
				// If Trim(vs1.TextMatrix(vs1.Row, colDS)) = vbNullString Then
				// if description is blank
				// Select Case Val(vs1.TextMatrix(vs1.Row, colCD))
				// Case 1
				// vs1.TextMatrix(vs1.Row, colDS) = gstrcategory(1)
				// Case 2
				// vs1.TextMatrix(vs1.Row, colDS) = gstrcategory(2)
				// Case 3
				// vs1.TextMatrix(vs1.Row, colDS) = gstrcategory(3)
				// Case 4
				// vs1.TextMatrix(vs1.Row, colDS) = gstrcategory(4)
				// Case 5
				// vs1.TextMatrix(vs1.Row, colDS) = gstrcategory(5)
				// Case 6
				// vs1.TextMatrix(vs1.Row, colDS) = gstrcategory(6)
				// Case 7
				// vs1.TextMatrix(vs1.Row, colDS) = gstrcategory(7)
				// Case 8
				// vs1.TextMatrix(vs1.Row, colDS) = gstrcategory(8)
				// Case 9
				// vs1.TextMatrix(vs1.Row, colDS) = gstrcategory(9)
				// End Select
				// 
				// End If
				// End If
				// 
				// End If
				KeyCode = 0;
				if (CurrentRow == vs1.Rows - 1)
				{
					if (vs1.Col == colLF)
					{
						AddNewItemized();
						vs1.Select(CurrentRow, colCD);
						return;
					}
				}
				if (vs1.Col < colLF)
				{
					vs1.Col += 1;
				}
				else if (vs1.Row < (vs1.Rows - 1))
				{
					vs1.Col = colRB;
					vs1.Row += 1;
				}
				if (vs1.Col == colVL)
				{
					if (CurrentRow < (vs1.Rows - 1))
					{
						vs1.Select(CurrentRow + 1, colRB);
					}
				}
			}
		}

		private void vs1_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii == 13)
			{
				KeyAscii = 0;
				if (CurrentRow == vs1.Rows - 1)
				{
					if (vs1.Col == colLF)
					{
						AddNewItemized();
						vs1.Select(CurrentRow, colCD);
					}
				}
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void vs1_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii == 13)
			{
				if (vs1.Row < vs1.Rows - 1)
				{
					if (vs1.Col == colLF)
					{
						KeyAscii = 0;
						vs1.Row += 1;
						vs1.Col = colCD;
					}
				}
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void vs1_LeaveCell(object sender, DataGridViewCellEventArgs e)
		{
			if (Formactivating == true)
				CurrentRow = vs1.Row;
			// 
			if (CurrentRow < 1 || CurrentRow > vs1.Rows - 1)
				return;
			if (CurrentCol == colQT)
			{
				vs1.TextMatrix(CurrentRow, colQT, Strings.Trim(Strings.Format(Strings.Format(vs1.TextMatrix(CurrentRow, colQT), "###"), "@@@")));
			}
			else if (CurrentCol == colDS)
			{
				int hold = 0;
				string hold1 = "";
				if (Strings.Mid(vs1.TextMatrix(CurrentRow, colDS), 1, 1) == ".")
				{
					hold1 = Strings.Mid(vs1.TextMatrix(CurrentRow, colDS), 2, 5);
					hold = FCConvert.ToInt32(Math.Round(Conversion.Val(hold1)));
					if (hold > 0 && hold < vs1.Rows)
					{
						vs1.TextMatrix(CurrentRow, colCD, vs1.TextMatrix(hold, colCD));
						vs1.TextMatrix(CurrentRow, colQT, vs1.TextMatrix(hold, colQT));
						vs1.TextMatrix(CurrentRow, colDS, vs1.TextMatrix(hold, colDS));
						vs1.TextMatrix(CurrentRow, colMO, vs1.TextMatrix(hold, colMO));
						vs1.TextMatrix(CurrentRow, colYR, vs1.TextMatrix(hold, colYR));
						vs1.TextMatrix(CurrentRow, colDY, vs1.TextMatrix(hold, colDY));
						vs1.TextMatrix(CurrentRow, colSR, vs1.TextMatrix(hold, colSR));
						vs1.TextMatrix(CurrentRow, colRC, vs1.TextMatrix(hold, colRC));
						vs1.TextMatrix(CurrentRow, colCS, vs1.TextMatrix(hold, colCS));
						vs1.TextMatrix(CurrentRow, colGD, vs1.TextMatrix(hold, colGD));
						vs1.TextMatrix(CurrentRow, colFC, vs1.TextMatrix(hold, colFC));
						vs1.TextMatrix(CurrentRow, colYFA, vs1.TextMatrix(hold, colYFA));
						vs1.TextMatrix(CurrentRow, colreplacingvalue, vs1.TextMatrix(hold, colreplacingvalue));
					}
				}
			}
			else if (CurrentCol == colCS)
			{
				// vs1.TextMatrix(CurrentRow, colCS) = Trim(Format(Format(vs1.TextMatrix(CurrentRow, colCS), "#,###"), "@@@@@@@@@@"))
			}
			else if (CurrentCol == colreplacingvalue)
			{
				int lngTemp = 0;
				if (Strings.Mid(vs1.TextMatrix(CurrentRow, colreplacingvalue) + " ", 1, 1) == ".")
				{
					lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(vs1.TextMatrix(CurrentRow, colreplacingvalue) + " ", 2))));
					if (lngTemp > 0 && lngTemp < vs1.Rows)
					{
						vs1.TextMatrix(CurrentRow, colreplacingvalue, FCConvert.ToString(Conversion.Val(vs1.TextMatrix(lngTemp, colVL))));
					}
					else
					{
						vs1.TextMatrix(CurrentRow, colreplacingvalue, FCConvert.ToString(0));
					}
				}
			}
		}

		private void vs1_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			int mRow;
			mRow = vs1.MouseRow;
			vs1.Tag = (System.Object)("");
			if (vs1.MouseRow < 1)
			{
				if (mRow < 0)
				{
					vs1.Row = 0;
					vs1.Focus();
				}
				return;
			}
			if (vs1.MouseCol != colLN)
			{
				if ((vs1.MouseCol != colVL) && (vs1.MouseCol != colRB))
				{
					vs1.Row = vs1.MouseRow;
					vs1.Col = vs1.MouseCol;
					vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vs1.EditCell();
				}
				return;
			}
			if (vs1.MouseRow == 0)
				return;
			if (Calculating == true)
				return;
			if (e.Button != MouseButtons.Right)
			{
				// vs1.Row = vs1.MouseRow
				// vs1.Col = vs1.MouseCol
				// vs1.Editable = True
				// vs1.EditCell
				return;
			}
			// If vs1.ColSel < vs1.Cols - 1 Then Exit Sub
			vs1.Row = vs1.MouseRow;
			if (vs1.RowSel == vs1.Rows - 1)
				return;
			vs1.Tag = (System.Object)(Conversion.Str(vs1.Row));
			vs1.Drag(1);
		}

		private void vs1_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
			string strToolTip;
			int lngMouseCol;
			strToolTip = "";
			lngMouseCol = vs1.GetFlexColIndex(e.ColumnIndex);
			//FC:FINAL:MSH - added an extra comparing to avoid exceptions if index less than 0
			if (e.ColumnIndex > -1 && e.RowIndex > -1)
			{
				DataGridViewCell cell = vs1[e.ColumnIndex, e.RowIndex];
				if (vs1.Rows > 0)
				{
					if (lngMouseCol == colCD)
					{
						strToolTip = "Code used to indicate the property category this item belongs to.";
					}
					else if (lngMouseCol == colCS)
					{
						strToolTip = "Purchase cost.  If using sound value, this is just the sound value of the item";
					}
					else if (lngMouseCol == CNSTCOLEXEMPT)
					{
						strToolTip = "Year last exemption was claimed";
					}
					else if (lngMouseCol == colDS)
					{
						strToolTip = "Description of this item";
					}
					else if (lngMouseCol == colDY)
					{
						strToolTip = "Depreciation years (Life span).  Leave 0 to use the category default";
					}
					else if (lngMouseCol == colFC)
					{
						strToolTip = "Percent multiplier to adjust value of the item";
					}
					else if (lngMouseCol == colGD)
					{
						strToolTip = "Physical percent good. Leave this 0 to have the percent good calculated";
					}
					else if (lngMouseCol == colLF)
					{
						strToolTip = "Account this item is leased to.  Used for items owned by vending companies";
					}
					else if (lngMouseCol == colMO)
					{
					}
					else if (lngMouseCol == colQT)
					{
						strToolTip = "The quantity of this item.  Multiplied by cost to determine total cost";
					}
					else if (lngMouseCol == colRB)
					{
						strToolTip = "Enter an asterisk if this item is BETR eligible, a P if it is BETE pending";
					}
					else if (lngMouseCol == colRC)
					{
						strToolTip = "If replacement cost is used this is the year the replacement cost is valid";
					}
					else if (lngMouseCol == colSR)
					{
						strToolTip = "Sound, Replacement or Original cost";
					}
					else if (lngMouseCol == colYFA)
					{
						strToolTip = "The year this item was first assessed for.  Used for determining new valuation for a particular year";
					}
					else if (lngMouseCol == colYR)
					{
						strToolTip = "Year this item was purchased.  Year new if using replacement cost";
					}
					else if (lngMouseCol == colreplacingvalue)
					{
						strToolTip = "Value of equipment this equipment replaces.  Used in determining new valuation for a particular year.";
					}
				}
				cell.ToolTipText = strToolTip;
			}
		}

		private void vs1_RowColChange(object sender, System.EventArgs e)
		{
			CurrentRow = vs1.Row;
			CurrentCol = vs1.Col;
			if (CurrentRow < 1)
				return;
			// If vs1.Col = colRB Then
			// vs1.Editable = False
			// Exit Sub
			// End If
			// vs1.Editable = True
			if (Conversion.Val(Strings.Trim(FCConvert.ToString(vs1.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vs1.Row, colQT)))) == 0)
				vs1.TextMatrix(vs1.Row, colQT, FCConvert.ToString(1));
			if (Conversion.Val(Strings.Trim(FCConvert.ToString(vs1.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vs1.Row, colMO)))) == 0)
				vs1.TextMatrix(vs1.Row, colMO, "00");
			// If Val(Trim(vs1.Cell(flexcpTextDisplay, vs1.Row, colYR))) = 0 Then vs1.TextMatrix(vs1.Row, colYR) = Year(Now)
			if (Conversion.Val(Strings.Trim(FCConvert.ToString(vs1.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vs1.Row, colDY)))) == 0)
				vs1.TextMatrix(vs1.Row, colDY, "00");
			// If Val(Trim(vs1.Cell(flexcpTextDisplay, vs1.Row, colFC))) = 0 Then vs1.TextMatrix(vs1.Row, colFC) = "100"
			if (Strings.Trim(FCConvert.ToString(vs1.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vs1.Row, colSR))) == "R")
			{
				if (Strings.Trim(FCConvert.ToString(vs1.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vs1.Row, colRC))) == string.Empty)
				{
					vs1.TextMatrix(vs1.Row, colRC, Strings.Right(FCConvert.ToString(DateTime.Now.Year), 2));
					// vs1.TextMatrix(vs1.Row, colRC) = "00"
				}
			}
			else
			{
				// If Trim(vs1.Cell(flexcpTextDisplay, vs1.Row, colRC)) = vbNullString Then
				// vs1.TextMatrix(vs1.Row, colRC) = "00"
				// End If
			}
			// vs1.TextMatrix(vs1.Row, vs1.Col) = Trim(vs1.Cell(flexcpTextDisplay, vs1.Row, vs1.Col))
		}

		private void vs1_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
            // Validate Entries Made in Cell
            //Application.DoEvents();
            boolDataChanged = true;
			//FC:FINAL:MSH - i.issue #1268: save correct indexes of the cell
			int row = vs1.GetFlexRowIndex(e.RowIndex);
			int col = vs1.GetFlexColIndex(e.ColumnIndex);
			if (col == colRB)
			{
				// RB
				if (Strings.Trim(vs1.EditText) == "*")
				{
					if (vs1.TextMatrix(row, colRB) == "*")
					{
						// MsgBox "A"
						//FC:FINAL:MSH - i.issue #1268: in original app running this row doesn't give any results
						//vs1.EditText = " ";
					}
					else
					{
						// MsgBox "B"
						vs1.EditText = "*";
					}
				}
				else if (Strings.Trim(vs1.EditText) == "P" || Strings.Trim(vs1.EditText) == "p")
				{
					if (Strings.UCase(vs1.TextMatrix(row, colRB)) == "P")
					{
						//FC:FINAL:MSH - i.issue #1268: in original app running this row doesn't give any results
						//vs1.EditText = " ";
					}
					else
					{
						vs1.EditText = "P";
					}
				}
				else if (Strings.Trim(vs1.EditText) != "")
				{
					e.Cancel = true;
				}
			}
			else if (col == colCD)
			{
				// CD
				//FC:FINAL:MSH - issue #1325: remove masked part from entered text for preventing errors and correct comparing
				//string vbPorterVar = vs1.EditText;
				string vbPorterVar = Strings.Trim(vs1.EditText);
				if ((vbPorterVar == "1") || (vbPorterVar == "2") || (vbPorterVar == "3") || (vbPorterVar == "4") || (vbPorterVar == "5") || (vbPorterVar == "6") || (vbPorterVar == "7") || (vbPorterVar == "8") || (vbPorterVar == "9") || (vbPorterVar == "-"))
				{
					// Input OK
				}
				else if ((vbPorterVar == "p") || (vbPorterVar == "P"))
				{
					if (row > 1)
					{
						vs1.EditText = vs1.TextMatrix(row - 1, colCD);
					}
				}
				else if ((vbPorterVar == "d") || (vbPorterVar == "D"))
				{
					vs1.EditText = "1";
					return;
				}
				else
				{
					e.Cancel = true;
				}
				if (Conversion.Val(vs1.EditText) > 0)
				{
					if (Strings.Trim(vs1.TextMatrix(row, colDS)) == string.Empty)
					{
						// if description is blank
						switch (FCConvert.ToInt32(Conversion.Val(vs1.EditText)))
						{
							case 1:
								{
									vs1.TextMatrix(row, colDS, gstrcategory[1]);
									break;
								}
							case 2:
								{
									vs1.TextMatrix(row, colDS, gstrcategory[2]);
									break;
								}
							case 3:
								{
									vs1.TextMatrix(row, colDS, gstrcategory[3]);
									break;
								}
							case 4:
								{
									vs1.TextMatrix(row, colDS, gstrcategory[4]);
									break;
								}
							case 5:
								{
									vs1.TextMatrix(row, colDS, gstrcategory[5]);
									break;
								}
							case 6:
								{
									vs1.TextMatrix(row, colDS, gstrcategory[6]);
									break;
								}
							case 7:
								{
									vs1.TextMatrix(row, colDS, gstrcategory[7]);
									break;
								}
							case 8:
								{
									vs1.TextMatrix(row, colDS, gstrcategory[8]);
									break;
								}
							case 9:
								{
									vs1.TextMatrix(row, colDS, gstrcategory[9]);
									break;
								}
						}
						//end switch
					}
				}
			}
			else if (col == colSR)
			{
				// SRO
				//FC:FINAL:MSH - issue #1325: remove masked part from entered text for preventing errors and correct comparing
				//string vbPorterVar1 = vs1.EditText;
				string vbPorterVar1 = Strings.Trim(vs1.EditText);
				if (vbPorterVar1 == "S")
				{
					// Input OK
				}
				else if (vbPorterVar1 == "R")
				{
					// Input OK
				}
				else if (vbPorterVar1 == "O")
				{
					if (Conversion.Val(vs1.TextMatrix(row, colCD)) > 0)
					{
						if (FCConvert.ToInt32(vs1.Cell(FCGrid.CellPropertySettings.flexcpValue, row, colYR)) == 0)
						{
							MessageBox.Show("You must enter a year to use Original Cost.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							// DoEvents
							// vs1.Select Row, colYR
							// vs1.Col = colYR
							// Cancel = True
						}
						else
						{
							// Input OK
						}
					}
				}
				else
				{
					vs1.EditText = "O";
				}
			}
			else if (col == colFC)
			{
				if (Conversion.Val(vs1.EditText) == 100)
				{
					vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, row, col, vs1.ForeColor);
				}
				else
				{
					vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, row, col, modGlobalConstants.Statics.TRIOCOLORRED);
				}
			}
			else
			{
				// Input is either OK or Not Allowed
			}
		}

		private void AddNewItemized()
		{
			int intVisibleRows;
			int lngCRow;
			boolDataChanged = true;
			vs1.Rows += 1;
			if (vs1.Rows == 2)
			{
				vs1.Select(vs1.Row, colCD);
			}
			else
			{
				vs1.Select(vs1.Row + 1, colCD);
			}
			if (CurrentRow == 0)
				CurrentRow = 1;
			AlreadyAdded:
			;
			// 
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colLN, vs1.Rows - 1);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colCD, "1");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colQT, "1");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colMO, "00");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colYR, "0000");
			if (modGlobal.Statics.CustomizeStuff.DefaultYearFirstAssessed == 0)
			{
				vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colYFA, DateTime.Today.Year);
			}
			else
			{
				vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colYFA, modGlobal.Statics.CustomizeStuff.DefaultYearFirstAssessed);
			}
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colreplacingvalue, "0");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colDY, "00");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colSR, "O");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colRC, "00");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colCS, "0");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colGD, "000");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, CurrentRow, colFC, "100");
			lngCRow = vs1.Rows - 1;
			intVisibleRows = FCConvert.ToInt32(FCConvert.ToDouble(vs1.HeightOriginal - vs1.RowHeight(0)) / vs1.RowHeight(1));
			if (lngCRow > intVisibleRows)
			{
				if (vs1.TopRow <= lngCRow - intVisibleRows)
				{
					vs1.TopRow = lngCRow - intVisibleRows + 1;
				}
			}
		}

		private void SetGridProperties()
		{
			// vs1.ColHidden(vs1.Cols - 1) = True
			vs1.Cols = 21;
			vs1.Visible = true;
			//FC:FINAL:MSH - i.issue #1269: set correct string format
			//vs1.ColFormat(colCS, "#,###,###,###");
			//vs1.ColFormat(colreplacingvalue, "#,###,###,###");
			//vs1.ColFormat(colVL, "##,###,###");
			vs1.ColFormat(colCS, "#,###,###,##0");
			vs1.ColFormat(colreplacingvalue, "#,###,###,##0");
			vs1.ColFormat(colVL, "##,###,##0");
			// Set Column Masks for Cells
			// vs1.ColEditMask(colRB, "&;); "
			vs1.ColComboList(colRB, " " + "\t" + "No Reimbursement|*" + "\t" + "BETR Reimbursable|P" + "\t" + "BETE Pending");
			//FC:FINAL:MSH - i.issue #1268: remove incorrect masking elements()
			//vs1.ColEditMask(colCD, "&;; ");
			//vs1.ColEditMask(colQT, "99999;; ");
			//// vs1.ColEditMask(colQT, "999;); "
			//vs1.ColEditMask(colMO, "99;; ");
			//vs1.ColEditMask(colYR, "9999;; ");
			//vs1.ColEditMask(colDY, "99;; ");
			//vs1.ColEditMask(colSR, ">L;; ");
			//vs1.ColEditMask(colRC, "99;; ");
			//vs1.ColEditMask(colGD, "999;; ");
			//vs1.ColEditMask(colFC, "999;; ");
			vs1.ColEditMask(colCD, "# ");
			vs1.ColEditMask(colQT, "99999 ");
			// vs1.ColEditMask(colQT, "999;); "
			vs1.ColEditMask(colMO, "99 ");
			vs1.ColEditMask(colYR, "9999 ");
			vs1.ColEditMask(colDY, "99 ");
			//FC:FINAL:MSH - incorrect mask format(same with i.issue #1281)
			//vs1.ColEditMask(colSR, ">L ");
			vs1.ColEditMask(colSR, "L> ");
			vs1.ColEditMask(colRC, "99 ");
			vs1.ColEditMask(colGD, "999 ");
			vs1.ColEditMask(colFC, "999 ");
			// setting header height and titles
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, colLN, 0, colLN, "Line");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, colRB, 0, colRB, "RB");
			vs1.TextMatrix(0, CNSTCOLEXEMPT, "Year Exempt");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, colCD, 0, colCD, "CD");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, colQT, 0, colQT, "Qty");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, colDS, 0, colDS, "Description");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, colMO, 0, colMO, "MO");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, colYR, 0, colYR, "Year");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, colDY, 0, colDY, "DP YR");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, colSR, 0, colSR, "S R O");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, colRC, 0, colRC, "RC YR");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, colCS, 0, colCS, "Cost");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, colGD, 0, colGD, "% GD");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, colFC, 0, colFC, "Fctr");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, colVL, 0, colVL, "Value");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 0, colLF, 0, colLF, "Leased To");
			vs1.TextMatrix(0, colYearsClaimed, "Years Claimed");
			//FC:FINAL:BBE:#528 - font size used in original for samll row height
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, 0, 0, vs1.Cols - 1, 8);
			vs1.TextMatrix(0, colYFA, "Year First Assessed");
			vs1.TextMatrix(0, colreplacingvalue, "Value Replacing");
			// Cell Alignment
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, colLN, 0, colVL, 4);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, colLN, vs1.Rows - 1, colLN, 4);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, colRB, vs1.Rows - 1, colRB, 4);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, colCD, vs1.Rows - 1, colCD, 4);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, colQT, vs1.Rows - 1, colQT, 4);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, colDS, vs1.Rows - 1, colDS, 1);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, colMO, vs1.Rows - 1, colMO, 1);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, colYR, vs1.Rows - 1, colYR, 1);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, colDY, vs1.Rows - 1, colDY, 1);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, colSR, vs1.Rows - 1, colSR, 4);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, colRC, vs1.Rows - 1, colRC, 1);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, colCS, vs1.Rows - 1, colCS, 7);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, colGD, vs1.Rows - 1, colGD, 1);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, colFC, vs1.Rows - 1, colFC, 1);
			vs1.ColAlignment(colYearsClaimed, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(colLF, FCGrid.AlignmentSettings.flexAlignRightCenter);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, colLF, 0, colLF, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, colYearsClaimed, 0, colYearsClaimed, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			// SetColumnWidths
			// vs1.ColWidth(colLN) = 535 * intScreenFactor       ' 4  Line
			vs1.ColHidden(colVL, true);
			vs1.ColHidden(colChange, true);
			// vs1.ColWidth(colVL) = 1160 * intScreenFactor      '10  Value
			//FC:FINAL:RPU:#1263 - align columns
			vs1.ColAlignment(colLN, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(colRB, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(CNSTCOLEXEMPT, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(colCD, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(colQT, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(colDS, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(colYFA, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(colYearsClaimed, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(colreplacingvalue, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.ColAlignment(colMO, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vs1.ColAlignment(colYR, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(colDY, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(colSR, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(colRC, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(colCS, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.ColAlignment(colGD, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vs1.ColAlignment(colFC, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(colKY, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(colVL, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(colLF, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(colChange, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		}

		private void SetColWidths()
		{
			int GridWidth;
			GridWidth = vs1.WidthOriginal;
			vs1.ColWidth(colLN, FCConvert.ToInt32(0.037 * GridWidth));
			vs1.ColWidth(colRB, FCConvert.ToInt32(0.035 * GridWidth));
			vs1.ColWidth(CNSTCOLEXEMPT, FCConvert.ToInt32(0.05 * GridWidth));
			vs1.ColWidth(colCD, FCConvert.ToInt32(0.034 * GridWidth));
			vs1.ColWidth(colQT, FCConvert.ToInt32(0.04 * GridWidth));
			vs1.ColWidth(colYearsClaimed, FCConvert.ToInt32(0.05 * GridWidth));
			vs1.ColWidth(colYFA, FCConvert.ToInt32(0.07 * GridWidth));
			vs1.ColWidth(colreplacingvalue, FCConvert.ToInt32(0.07 * GridWidth));
			vs1.ColWidth(colDS, FCConvert.ToInt32(0.23 * GridWidth));
			vs1.ColWidth(colMO, FCConvert.ToInt32(0.047 * GridWidth));
			vs1.ColWidth(colYR, FCConvert.ToInt32(0.048 * GridWidth));
			vs1.ColWidth(colDY, FCConvert.ToInt32(0.032 * GridWidth));
			//FC:FINAL:RPU:#1263 - Expand column to avoid text wrapping
			//vs1.ColWidth(colSR, FCConvert.ToInt32(0.024 * GridWidth));
			vs1.ColWidth(colSR, FCConvert.ToInt32(0.025 * GridWidth));
			vs1.ColWidth(colRC, FCConvert.ToInt32(0.032 * GridWidth));
			//FC:FINAL:RPU:#1263 - Minimize column a little bit
			//vs1.ColWidth(colCS, FCConvert.ToInt32(0.099 * GridWidth));
			vs1.ColWidth(colCS, FCConvert.ToInt32(0.09 * GridWidth));
			vs1.ColWidth(colGD, FCConvert.ToInt32(0.051 * GridWidth));
			vs1.ColWidth(colFC, FCConvert.ToInt32(0.04 * GridWidth));
			vs1.ColWidth(colKY, 0);
			// Key
			vs1.ColWidth(colLF, FCConvert.ToInt32(0.06 * GridWidth));
			if (vs1.Rows > 1)
			{
				vs1.RowHeight(0, 3 * vs1.RowHeight(1));
			}
		}

		private void CheckForRB()
		{
			/*? On Error Resume Next  */
			try
			{
				clsDRWrapper mst = new clsDRWrapper();
				int ii;
				// 
				// Set Recordset
				strSQL = "SELECT * FROM PPMaster WHERE Account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount);
				mst.OpenRecordset(strSQL, modPPGN.strPPDatabase);
				mst.Edit();
				// Start Loop to determine if any rows have Reimbursement Items
				for (ii = 1; ii <= vs1.Rows - 1; ii++)
				{
					if (vs1.TextMatrix(ii, 2) == "*")
					{
						mst.Set_Fields("RBCode", "Y");
						break;
					}
					else
					{
						mst.Set_Fields("RBCode", "N");
					}
				}
				// ii
				mst.Update();
			}
			catch (Exception ex)
			{
			}
		}

		private void GetCostData()
		{
			// vbPorter upgrade warning: xx As object	OnWrite(short, object)
			int xx;
			clsDRWrapper rsCost = new clsDRWrapper();
			rsCost.OpenRecordset("SELECT * FROM RatioTrends", modPPGN.strPPDatabase);
			rsCost.MoveFirst();
			for (xx = 1; xx <= 9; xx++)
			{
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				xx = rsCost.Get_Fields("Type");
				// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
				modPPCalculate.Statics.HighPct[xx] = FCConvert.ToInt32(rsCost.Get_Fields("High"));
				// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
				modPPCalculate.Statics.LowPct[xx] = FCConvert.ToInt32(rsCost.Get_Fields("Low"));
				modPPCalculate.Statics.Exponent[xx] = rsCost.Get_Fields_Double("Exponent");
				// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
				modPPCalculate.Statics.DefLife[xx] = FCConvert.ToInt32(rsCost.Get_Fields("Life"));
				modPPCalculate.Statics.DepMeth[xx] = FCConvert.ToString(rsCost.Get_Fields_String("SD"));
				// TODO Get_Fields: Check the table for the column [Trend] and replace with corresponding Get_Field method
				modPPCalculate.Statics.TrendCode[xx] = FCConvert.ToString(rsCost.Get_Fields("Trend"));
				modPPCalculate.Statics.DefDesc[xx] = FCConvert.ToString(rsCost.Get_Fields_String("Description"));
				rsCost.MoveNext();
			}
			// xx
		}

		private void Calculate()
		{
			ItemizedAccountTotal = 0;
			modPPCalculate.Statics.xRow = 0;
			if (vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, colVL, vs1.Rows - 1, colVL) != modGlobalConstants.Statics.TRIOCOLORBLUE)
			{
				Calculating = true;
				vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, colVL, vs1.Rows - 1, colVL, Color.Blue);
				vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 1, colVL, vs1.Rows - 1, colVL, SystemColors.Window);
				// PERFORM CALCULATIONS HERE
				modPPCalculate.Statics.SoundCost = 0;
				modPPCalculate.Statics.SoundValue = 0;
				modPPCalculate.Statics.ReplacementCost = 0;
				modPPCalculate.Statics.ReplacementValue = 0;
				modPPCalculate.Statics.OriginalCost = 0;
				modPPCalculate.Statics.OriginalValue = 0;
				modPPCalculate.Statics.ItemAccount = 0;
				//FCUtils.EraseSafe(modPPCalculate.Statics.Item);
				//FCUtils.EraseSafe(modPPCalculate.Statics.TotalItem);
				modPPCalculate.GetTrendValues();
				for (modPPCalculate.Statics.xRow = 1; modPPCalculate.Statics.xRow <= vs1.Rows - 1; modPPCalculate.Statics.xRow++)
				{
					if (vs1.TextMatrix(modPPCalculate.Statics.xRow, colCD) != "-")
					{
						// Set Variables equal to CellText from Current Line
						modPPCalculate.Statics.Work1 = 0;
						modPPCalculate.Statics.clCode = vs1.TextMatrix(modPPCalculate.Statics.xRow, colCD);
						modPPCalculate.Statics.clQuantity = FCConvert.ToInt32(vs1.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, colQT));
						modPPCalculate.Statics.CLYEAR = FCConvert.ToInt32(vs1.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, colYR));
						modPPCalculate.Statics.clDPYR = FCConvert.ToInt32(Math.Round(Conversion.Val(vs1.TextMatrix(modPPCalculate.Statics.xRow, colDY))));
						modPPCalculate.Statics.clSRO = vs1.TextMatrix(modPPCalculate.Statics.xRow, colSR);
						modPPCalculate.Statics.clRCYR = FCConvert.ToInt32(Math.Round(Conversion.Val(vs1.TextMatrix(modPPCalculate.Statics.xRow, colRC))));
						modPPCalculate.Statics.clCost = FCConvert.ToInt32(vs1.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, colCS));
						modPPCalculate.Statics.clPercentGood = Conversion.Val(vs1.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, colGD));
						modPPCalculate.Statics.clFactor = FCConvert.ToDouble(Strings.Format(vs1.Cell(FCGrid.CellPropertySettings.flexcpValue, modPPCalculate.Statics.xRow, colFC) / 100, "0.00"));
						// 
						if (modPPCalculate.Statics.clCost > 0)
							modPPCalculate.OriginalCostValue();
						if (modPPCalculate.Statics.Work1 > 1)
						{
							vs1.TextMatrix(modPPCalculate.Statics.xRow, colVL, FCConvert.ToString(modPPCalculate.Statics.Work1));
						}
						else
						{
							vs1.TextMatrix(modPPCalculate.Statics.xRow, colVL, "");
						}
						ItemizedAccountTotal += FCConvert.ToInt32(modPPCalculate.Statics.Work1);
						if (modPPCalculate.Statics.xRow == vs1.Rows - 1)
						{
							vs1.SubtotalPosition = FCGrid.SubtotalPositionSettings.flexSTBelow;
							//FC:FINAL:MSH - i.issue #1269: set correct string format
							//vs1.Subtotal(FCGrid.SubtotalSettings.flexSTSum, -1, colVL, "$#,###", Color.Red, Color.White, true, " ");
							vs1.Subtotal(FCGrid.SubtotalSettings.flexSTSum, -1, colVL, "$#,##0", Color.Red, Color.White, true, " ");
							vs1.Cell(FCGrid.CellPropertySettings.flexcpText, vs1.Rows - 1, colDS, "Total Value");
						}
					}
					else
					{
						vs1.TextMatrix(modPPCalculate.Statics.xRow, colVL, "");
					}
				}
				// xRow
				//FC:FINAL:MSH - i.issue #1269: set correct string format
				//vs1.Cell(FCGrid.CellPropertySettings.flexcpText, vs1.Rows - 1, colVL, Strings.Format(ItemizedAccountTotal, "#,###"));
				vs1.Cell(FCGrid.CellPropertySettings.flexcpText, vs1.Rows - 1, colVL, Strings.Format(ItemizedAccountTotal, "#,##0"));
			}
			else
			{
				Calculating = false;
				vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 13, vs1.Rows - 1, colVL, SystemColors.Window);
				vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 1, 13, vs1.Rows - 1, colVL, Color.Black);
				vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 1, colVL, vs1.Rows - 1, colVL, 0);
				vs1.Cell(FCGrid.CellPropertySettings.flexcpText, 1, colGD, vs1.Rows - 1, colGD, "");
				int xx;
				for (xx = 1; xx <= vs1.Cols; xx++)
				{
					if (FCConvert.ToString(vs1.Cell(FCGrid.CellPropertySettings.flexcpText, vs1.Rows - 1, xx)) == "Total Value")
					{
						vs1.RemoveItem(vs1.Rows - 1);
						break;
					}
				}
				// xx
				ItemizedAccountTotal = 0;
			}
		}

		private void FillGrid()
		{
			int intCounter = 0;
			string strTemp = "";
			string strYear = "";
			vs1.Rows = 1;
			if (ppi.EndOfFile() != true && ppi.BeginningOfFile() != true)
			{
				ppi.MoveLast();
				ppi.MoveFirst();
				// vs1.Rows = .RecordCount + 1
				do
				{
					intCounter += 1;
					if (Conversion.Val(ppi.Get_Fields_Int32("leasedto") + "") > 0)
					{
						strTemp = FCConvert.ToString(ppi.Get_Fields_Int32("leasedto"));
					}
					else
					{
						strTemp = "";
					}
					if (Conversion.Val(ppi.Get_Fields_Int32("exemptyear") + "") > 0)
					{
						strYear = FCConvert.ToString(ppi.Get_Fields_Int32("exemptyear"));
					}
					else
					{
						strYear = "";
					}
					// vs1.AddItem (Format(intCounter, "0000") & vbTab & .Fields("rb") & vbTab & strYear & vbTab & (.Fields("cd") & "") & vbTab & (Val(.Fields("quantity") & "")) & vbTab & Trim(.Fields("description") & "") & vbTab & .Fields("Yearfirstassessed") & vbTab & .Fields("amountreplacing") & vbTab & Format(.Fields("month"), "00") & vbTab & Format(.Fields("year"), "0000") & vbTab & Format(.Fields("dpyr"), "00") & vbTab & .Fields("sro") & vbTab & Format(.Fields("rcyr"), "00") & vbTab & .Fields("cost") & vbTab & Format(.Fields("gd"), "000") & vbTab & Format(.Fields("fctr"), "0") & vbTab & .Fields("key") & vbTab & .Fields("value") & vbTab & strTemp)
					// TODO Get_Fields: Check the table for the column [rb] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [quantity] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [year] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [sro] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [cost] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
					vs1.AddItem(FCConvert.ToString(intCounter) + "\t" + ppi.Get_Fields("rb") + "\t" + strYear + "\t" + (ppi.Get_Fields_String("cd") + "") + "\t" + (FCConvert.ToString(Conversion.Val(ppi.Get_Fields("quantity") + "")) + "\t" + Strings.Trim(ppi.Get_Fields_String("description") + "") + "\t" + ppi.Get_Fields_Int32("Yearfirstassessed") + "\t" + FCConvert.ToString(Conversion.Val(ppi.Get_Fields_Int32("yearsclaimed")))) + "\t" + ppi.Get_Fields_Int32("amountreplacing") + "\t" + Strings.Format(ppi.Get_Fields_Int16("month"), "00") + "\t" + Strings.Format(ppi.Get_Fields("year"), "0000") + "\t" + Strings.Format(ppi.Get_Fields_Int16("dpyr"), "00") + "\t" + ppi.Get_Fields("sro") + "\t" + Strings.Format(ppi.Get_Fields_Int16("rcyr"), "00") + "\t" + ppi.Get_Fields("cost") + "\t" + Strings.Format(ppi.Get_Fields_Int16("gd"), "000") + "\t" + Strings.Format(ppi.Get_Fields_Int16("fctr"), "0") + "\t" + ppi.Get_Fields_Int32("id") + "\t" + ppi.Get_Fields("value") + "\t" + strTemp);
					if (Conversion.Val(ppi.Get_Fields_Int16("fctr") + "") != 100)
					{
						vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vs1.Rows - 1, colFC, modGlobalConstants.Statics.TRIOCOLORRED);
					}
					ppi.MoveNext();
				}
				while (!ppi.EndOfFile());
				// vs1.RemoveItem (1)
			}
			if (vs1.Rows == 1)
			{
				mnuAdd_Click();
				// none, so add a blank line
			}
			int ii;
			for (ii = 1; ii <= vs1.Rows - 1; ii++)
			{
				if (Strings.Trim(vs1.TextMatrix(ii, colDS)) == "")
				{
					if (Conversion.Val(vs1.TextMatrix(ii, colLN)) == 0)
						vs1.TextMatrix(ii, colLN, Strings.Format(ii, "0000"));
					// If Val(vs1.TextMatrix(ii, colCD)) = 0 And vs1.TextMatrix(ii, colCD) <> "-" Then vs1.TextMatrix(ii, colCD) = "1"
					if (Strings.Trim(vs1.TextMatrix(ii, colCD)) == string.Empty)
						vs1.TextMatrix(ii, colCD, "-");
					if (vs1.TextMatrix(ii, colSR) == "")
						vs1.TextMatrix(ii, colSR, "O");
					if (Conversion.Val(vs1.TextMatrix(ii, colGD)) == 0)
						vs1.TextMatrix(ii, colGD, "000");
					if (vs1.Rows == 2)
					{
						vs1.TextMatrix(ii, colFC, "100");
					}
				}
			}
			// ii
		}

		private void FillMasterLabels()
		{
			clsDRWrapper rsMaster = new clsDRWrapper();
			lblAccount.Text = FCConvert.ToString(modPPGN.Statics.CurrentAccount);
			string strMasterJoin;
			strMasterJoin = modPPGN.GetMasterJoin();
			// Call rsMaster.OpenRecordset("SELECT * FROM PPMaster WHERE Account = " & CurrentAccount, modPPGN.strPPDatabase)
			rsMaster.OpenRecordset(strMasterJoin + " WHERE Account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount), modPPGN.strPPDatabase);
			if (rsMaster.EndOfFile())
				return;
			string strLOC = "";
			// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
			if (fecherFoundation.FCUtils.IsNull(rsMaster.Get_Fields("StreetNumber")) == false)
			{
				// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
				strLOC = FCConvert.ToString(rsMaster.Get_Fields("StreetNumber"));
				strLOC += " ";
			}
			if (fecherFoundation.FCUtils.IsNull(rsMaster.Get_Fields_String("Street")) == false)
			{
				strLOC += FCConvert.ToString(rsMaster.Get_Fields_String("Street"));
			}
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			lblAccount.Text = Strings.Format(rsMaster.Get_Fields("Account"), "00000");
			lblName.Text = FCConvert.ToString(rsMaster.Get_Fields_String("Name"));
		}

		public void HandlePartialPermission(ref int lngFuncID)
		{
			// takes a function number as a parameter
			// Then gets all the child functions that belong to it and
			// disables the appropriate controls
			clsDRWrapper clsChildList = new clsDRWrapper();
			// Dim dcTemp As New clsdrwrapper
			string strPerm = "";
			int lngChild = 0;
			Control ctl = new Control();
			// Set clsChildList = New clsdrwrapper
			modGlobalConstants.Statics.clsSecurityClass.Get_Children(ref clsChildList, ref lngFuncID);
			while (!clsChildList.EndOfFile())
			{
				lngChild = FCConvert.ToInt32(clsChildList.GetData("childid"));
				strPerm = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(lngChild));
				switch (lngChild)
				{
					case modSecurity.LEASEDSCREENMENU:
						{
							if (strPerm == "F")
							{
								cmdLeased.Enabled = true;
							}
							else
							{
								cmdLeased.Enabled = false;
							}
							break;
						}
					case modSecurity.EDITITEMIZEDSCREENMENU:
						{
							if (strPerm == "F")
							{
								mnuAdd.Enabled = true;
								mnuAddMultipleRows.Enabled = true;
								mnuRemove.Enabled = true;
                                vs1.Enabled = true;
							}
							else
							{
								mnuAdd.Enabled = false;
								mnuAddMultipleRows.Enabled = false;
								mnuRemove.Enabled = false;
								vs1.Enabled = false;
							}
							break;
						}
				}
				//end switch
				clsChildList.MoveNext();
			}
		}

		private void RefreshNumbers()
		{
            vs1.Redraw = false;
			int x;
			for (x = 1; x <= vs1.Rows - 1; x++)
			{
				if (Conversion.Val(vs1.TextMatrix(x, 0)) != x)
				{
					// if this is renumbered then mark as changed
					vs1.TextMatrix(x, colChange, FCConvert.ToString(true));
				}
				// vs1.TextMatrix(x, 0) = Format(x, "0000")
				vs1.TextMatrix(x, 0, FCConvert.ToString(x));
			}
            // x
            //vs1.Refresh();
            vs1.Redraw = true;
		}

		private void DelRow()
		{
			Support.SendKeys("{DELETE}", false);
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		private bool NewSaveItemized()
		{
			bool NewSaveItemized = false;
			try
			{
				clsDRWrapper clsSave = new clsDRWrapper();
				string strSQL = "";
				string strCols;
				int x;
				string strTemp = "";
				int lngLastRowChecked;
				// Dim boolReimbursable As Boolean
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				NewSaveItemized = false;
				/* On Error GoTo ErrorHandler */
				clsSave.Execute("update status set changeddata = '" + FCConvert.ToString(DateTime.Now) + "'", "twpp0000.vb1");
				// If (vs1.Row > 0) And (vs1.Col >= 0) Then
				vs1.Row = 0;
				// End If
				//Application.DoEvents();
				strCols = "(Account,Line,RB,CD,quantity,description,month,year,dpyr,sro,rcyr,cost,gd,fctr,[value],leasedto)";
				// delete the deleted rows
				if (GridDeleted.Rows > 0)
				{
					modGlobalFunctions.AddCYAEntry_26("PP", "Deleted Itemized Line(s)", "Account " + FCConvert.ToString(modPPGN.Statics.CurrentAccount));
					for (x = 0; x <= GridDeleted.Rows - 1; x++)
					{
						clsSave.Execute("delete from ppitemized where id = " + FCConvert.ToString(Conversion.Val(GridDeleted.TextMatrix(x, 0))), "twpp0000.vb1");
					}
					// x
					GridDeleted.Rows = 0;
				}
				// Call clsSave.Execute("delete from ppitemized where account = " & CurrentAccount, "twpp0000.vb1")
				lblScreenTitle.Visible = true;
				/* On Error GoTo LoopError */// so only one line of data will be lost if there is a problem
				// boolReimbursable = False
				// For x = 1 To vs1.Rows - 1
				lngLastRowChecked = 1;
				x = vs1.FindRow("True", lngLastRowChecked, colChange);
				while (x > 0)
				{
					// DoEvents
					goto SkipError;
					LoopError:
					;
					MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err().Number) + "  " + Information.Err().Description + "\r\n" + "In save itemized" + "\r\n" + "There was an error during the save process" + "\r\n" + "You should try saving again before exiting, otherwise the itemized data may be lost.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
					NewSaveItemized = false;
					goto EndOfLoop;
					SkipError:
					;
					// If vs1.TextMatrix(x, colRB) = "*" Then
					// boolReimbursable = True
					// End If
					lngLastRowChecked = x;
					this.Refresh();
					lblScreenTitle.Text = "Saving Line: " + FCConvert.ToString(x);
					if (Conversion.Val(vs1.TextMatrix(x, colKY)) == 0)
					{
						// this is a new one so insert it
						clsSave.OpenRecordset("select * from ppitemized where id = -1", "twpp0000.vb1");
						// can't get a hit with this
						clsSave.AddNew();
						clsSave.Set_Fields("account", modPPGN.Statics.CurrentAccount);
						clsSave.Set_Fields("line", x);
						clsSave.Set_Fields("rb", vs1.TextMatrix(x, colRB));
						clsSave.Set_Fields("cd", vs1.TextMatrix(x, colCD));
						clsSave.Set_Fields("exemptyear", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, CNSTCOLEXEMPT))));
						clsSave.Set_Fields("quantity", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colQT))));
						clsSave.Set_Fields("description", Strings.Trim(vs1.TextMatrix(x, colDS)));
						clsSave.Set_Fields("month", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colMO))));
						clsSave.Set_Fields("year", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colYR))));
						clsSave.Set_Fields("yearsclaimed", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colYearsClaimed))));
						clsSave.Set_Fields("dpyr", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colDY))));
						clsSave.Set_Fields("sro", vs1.TextMatrix(x, colSR));
						clsSave.Set_Fields("yearfirstassessed", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colYFA))));
						if (Conversion.Val(vs1.TextMatrix(x, colreplacingvalue)) > 0)
						{
							clsSave.Set_Fields("amountreplacing", FCConvert.ToInt32(FCConvert.ToDouble(vs1.TextMatrix(x, colreplacingvalue))));
						}
						else
						{
							clsSave.Set_Fields("amountreplacing", 0);
						}
						clsSave.Set_Fields("rcyr", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colRC))));
						if (Conversion.Val(vs1.TextMatrix(x, colCS)) > 0)
						{
							clsSave.Set_Fields("cost", FCConvert.ToInt32(FCConvert.ToDouble(vs1.TextMatrix(x, colCS))));
						}
						else
						{
							clsSave.Set_Fields("cost", 0);
						}
						clsSave.Set_Fields("gd", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colGD))));
						clsSave.Set_Fields("fctr", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colFC))));
						clsSave.Set_Fields("value", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colVL))));
						clsSave.Set_Fields("leasedto", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colLF))));
						clsSave.Update();
						vs1.TextMatrix(x, colKY, FCConvert.ToString(clsSave.Get_Fields_Int32("id")));
					}
					else
					{
						// just update it
						strSQL = "update ppitemized set ";
						strSQL += "line = " + FCConvert.ToString(x);
						strSQL += ",rb = '" + vs1.TextMatrix(x, colRB) + "'";
						strSQL += ",exemptyear = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, CNSTCOLEXEMPT)));
						strSQL += ",yearsclaimed = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colYearsClaimed)));
						strSQL += ",cd = '" + vs1.TextMatrix(x, colCD) + "'";
						strSQL += ",quantity = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colQT)));
						strSQL += ",description = '" + Strings.Trim(vs1.TextMatrix(x, colDS)) + "'";
						strSQL += ",yearfirstassessed = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colYFA)));
						if (Conversion.Val(vs1.TextMatrix(x, colreplacingvalue)) > 0)
						{
							strSQL += ",amountreplacing = " + FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(vs1.TextMatrix(x, colreplacingvalue))));
						}
						else
						{
							strSQL += ",amountreplacing = 0";
						}
						strSQL += ",month = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colMO)));
						strSQL += ",year = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colYR)));
						strSQL += ",dpyr = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colDY)));
						strSQL += ",sro = '" + vs1.TextMatrix(x, colSR) + "'";
						strSQL += ",rcyr = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colRC)));
						if (Conversion.Val(vs1.TextMatrix(x, colCS)) > 0)
						{
							strSQL += ",cost = " + FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(vs1.TextMatrix(x, colCS))));
						}
						else
						{
							strSQL += ",cost = 0";
						}
						// strSQL = strSQL & ",cost = " & Val(vs1.TextMatrix(x, colCS))
						strSQL += ",gd = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colGD)));
						strSQL += ",fctr = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colFC)));
						strSQL += ",[value] = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colVL)));
						strSQL += ",leasedto = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colLF)));
						strSQL += " where id = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colKY)));
						if (!clsSave.Execute(strSQL, "twpp0000.vb1", false))
						{
							strTemp = Strings.Trim(vs1.TextMatrix(x, colDS) + "");
							modGlobal.escapequote(ref strTemp);
							strSQL = "update ppitemized set ";
							strSQL += "line = " + FCConvert.ToString(x);
							strSQL += ",rb = '" + vs1.TextMatrix(x, colRB) + "'";
							strSQL += ",exemptyear = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, CNSTCOLEXEMPT)));
							strSQL += ",yearsclaimed = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colYearsClaimed)));
							strSQL += ",cd = '" + vs1.TextMatrix(x, colCD) + "'";
							strSQL += ",quantity = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colQT)));
							strSQL += ",description = '" + strTemp + "'";
							strSQL += ",yearfirstassessed = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colYFA)));
							if (Conversion.Val(vs1.TextMatrix(x, colreplacingvalue)) > 0)
							{
								strSQL += ",amountreplacing = " + FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(vs1.TextMatrix(x, colreplacingvalue))));
							}
							else
							{
								strSQL += ",amountreplacing = 0";
							}
							strSQL += ",[month] = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colMO)));
							strSQL += ",[year] = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colYR)));
							strSQL += ",dpyr = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colDY)));
							strSQL += ",sro = '" + vs1.TextMatrix(x, colSR) + "'";
							strSQL += ",rcyr = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colRC)));
							if (Conversion.Val(vs1.TextMatrix(x, colCS)) > 0)
							{
								strSQL += ",cost = " + FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(vs1.TextMatrix(x, colCS))));
							}
							else
							{
								strSQL += ",cost = 0";
							}
							// strSQL = strSQL & ",cost = " & Val(vs1.TextMatrix(x, colCS))
							strSQL += ",gd = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colGD)));
							strSQL += ",fctr = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colFC)));
							strSQL += ",[value] = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colVL)));
							strSQL += ",leasedto = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colLF)));
							strSQL += " where id = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(x, colKY)));
							if (!clsSave.Execute(strSQL, "twpp0000.vb1", false))
							{
								MessageBox.Show("Error saving line " + FCConvert.ToString(x) + ".  Please try again before quitting, or information may be lost.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
								NewSaveItemized = false;
								return NewSaveItemized;
							}
						}
					}
					vs1.TextMatrix(x, colChange, FCConvert.ToString(false));
					EndOfLoop:
					;
					// Next x
					x = vs1.FindRow("True", lngLastRowChecked, colChange);
				}
				/* On Error GoTo ErrorHandler */// Call ResetChanges
				NewSaveItemized = true;
				boolDataChanged = false;
				MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				// If boolReimbursable Then
				// Call clsSave.Execute("update ppmaster set rbcode = 'Y' where account = " & CurrentAccount, "twpp0000.vb1")
				// Else
				// Call clsSave.OpenRecordset("select * from ppleased where rb = '*' and account = " & CurrentAccount, "twpp0000.vb1")
				// If clsSave.EndOfFile Then
				// Call clsSave.Execute("update ppmaster set rbcode = 'N' where account = " & CurrentAccount, "twpp0000.vb1")
				// End If
				// End If
				// 
				ppi.OpenRecordset("select * from ppitemized where account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount) + " order by line", modPPGN.strPPDatabase);
				lblScreenTitle.Visible = false;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return NewSaveItemized;
			}
			catch (Exception ex)
			{
				ErrorHandler:
				;
				lblScreenTitle.Visible = false;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In save itemized" + "\r\n" + "There was an error during the save process" + "\r\n" + "You should try saving again before exiting, otherwise the itemized data may be lost.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return NewSaveItemized;
		}
		//FC:FINAL:CHN: Delete underline from mask.
		private void vs1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
            e.Control.KeyDown -= new KeyEventHandler(this.vs1_KeyDownEdit);
            e.Control.KeyDown += new KeyEventHandler(this.vs1_KeyDownEdit);

            var editingControl = e.Control as FCMaskedTextBox;
			if (editingControl == null)
				return;
			editingControl.PromptChar = ' ';
        }

        private void mnuViewGroupInformation_Click(object sender, EventArgs e)
        {
            clsDRWrapper clsLoad = new clsDRWrapper();
            // vbPorter upgrade warning: intRes As short, int --> As DialogResult
            DialogResult intRes;
            int lngGroupNumber = 0;
            clsLoad.OpenRecordset("select * from moduleassociation where module = 'PP' and account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount), "CentralData");
            if (!clsLoad.EndOfFile())
            {
                lngGroupNumber = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("groupnumber"));
                clsLoad.OpenRecordset("select * from groupmaster where id = " + FCConvert.ToString(lngGroupNumber), "CentralData");
                if (!clsLoad.EndOfFile())
                {
                    //! Load frmGroup;
                    frmGroup.InstancePtr.Init(lngGroupNumber);
                }
                else
                {
                    MessageBox.Show("This account is associated with a group but the group does not exist.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                intRes = MessageBox.Show("This account is not in a group." + "\r\n" + "Would you like to create a new group?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (intRes == DialogResult.Yes)
                {
                    //! Load frmGroup;
                    frmGroup.InstancePtr.Init(0);
                }
            }
		}
    }
}
