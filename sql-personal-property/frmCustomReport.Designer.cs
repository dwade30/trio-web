//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmCustomReport.
	/// </summary>
	partial class frmCustomReport : BaseForm
	{
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCComboBox cmbTypeOfReport;
		public fecherFoundation.FCFrame fraWhere;
		public fecherFoundation.FCGrid vsWhere;
		public fecherFoundation.FCFrame fraSort;
		public fecherFoundation.FCDraggableListBox lstSort;
		public fecherFoundation.FCFrame fraFields;
		public fecherFoundation.FCListBox lstFields;
		public fecherFoundation.FCFrame fraMessage;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCFrame fraLayout;
		public fecherFoundation.FCGrid vsLayout;
		public fecherFoundation.FCPictureBox Image1;
		public Wisej.Web.ImageList ImageList1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuClear;
		public fecherFoundation.FCToolStripMenuItem mnuSP11;
		public fecherFoundation.FCToolStripMenuItem mnuAddRow;
		public fecherFoundation.FCToolStripMenuItem mnuAddColumn;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteRow;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteColumn;
		public fecherFoundation.FCToolStripMenuItem mnuChangeParameters;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomReport));
			Wisej.Web.ImageListEntry imageListEntry1 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images"))));
			this.Frame1 = new fecherFoundation.FCFrame();
			this.cmbTypeOfReport = new fecherFoundation.FCComboBox();
			this.fraWhere = new fecherFoundation.FCFrame();
			this.vsWhere = new fecherFoundation.FCGrid();
			this.fraSort = new fecherFoundation.FCFrame();
			this.lstSort = new fecherFoundation.FCDraggableListBox();
			this.fraFields = new fecherFoundation.FCFrame();
			this.lstFields = new fecherFoundation.FCListBox();
			this.fraMessage = new fecherFoundation.FCFrame();
			this.Label3 = new fecherFoundation.FCLabel();
			this.fraLayout = new fecherFoundation.FCFrame();
			this.vsLayout = new fecherFoundation.FCGrid();
			this.Image1 = new fecherFoundation.FCPictureBox();
			this.ImageList1 = new Wisej.Web.ImageList(this.components);
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuClear = new fecherFoundation.FCToolStripMenuItem();
			this.mnuChangeParameters = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP11 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddRow = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddColumn = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDeleteRow = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDeleteColumn = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.cmdAddRow = new fecherFoundation.FCButton();
			this.cmdAddColumn = new fecherFoundation.FCButton();
			this.cmdDeleteRow = new fecherFoundation.FCButton();
			this.cmdDeleteColumn = new fecherFoundation.FCButton();
			this.cmdClearCriteria = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraWhere)).BeginInit();
			this.fraWhere.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsWhere)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSort)).BeginInit();
			this.fraSort.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraFields)).BeginInit();
			this.fraFields.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraMessage)).BeginInit();
			this.fraMessage.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraLayout)).BeginInit();
			this.fraLayout.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsLayout)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddRow)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddColumn)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteRow)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteColumn)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClearCriteria)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdPrint);
			this.BottomPanel.Location = new System.Drawing.Point(0, 660);
			this.BottomPanel.Size = new System.Drawing.Size(1053, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.fraWhere);
			this.ClientArea.Controls.Add(this.fraSort);
			this.ClientArea.Controls.Add(this.fraFields);
			this.ClientArea.Controls.Add(this.fraLayout);
			this.ClientArea.Size = new System.Drawing.Size(1053, 600);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdDeleteColumn);
			this.TopPanel.Controls.Add(this.cmdDeleteRow);
			this.TopPanel.Controls.Add(this.cmdAddColumn);
			this.TopPanel.Controls.Add(this.cmdClearCriteria);
			this.TopPanel.Controls.Add(this.cmdAddRow);
			this.TopPanel.Size = new System.Drawing.Size(1053, 60);
			this.TopPanel.Controls.SetChildIndex(this.cmdAddRow, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdClearCriteria, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdAddColumn, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDeleteRow, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDeleteColumn, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(177, 30);
			this.HeaderText.Text = "Custom Report";
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.cmbTypeOfReport);
			this.Frame1.Location = new System.Drawing.Point(678, 214);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(283, 186);
			this.Frame1.TabIndex = 13;
			this.Frame1.Text = "Type Of Report";
			// 
			// cmbTypeOfReport
			// 
			this.cmbTypeOfReport.AutoSize = false;
			this.cmbTypeOfReport.BackColor = System.Drawing.SystemColors.Window;
			this.cmbTypeOfReport.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbTypeOfReport.FormattingEnabled = true;
			this.cmbTypeOfReport.Location = new System.Drawing.Point(20, 30);
			this.cmbTypeOfReport.Name = "cmbTypeOfReport";
			this.cmbTypeOfReport.Size = new System.Drawing.Size(243, 40);
			this.cmbTypeOfReport.TabIndex = 14;
			this.cmbTypeOfReport.Text = "Combo1";
			// 
			// fraWhere
			// 
			this.fraWhere.Controls.Add(this.vsWhere);
			this.fraWhere.Location = new System.Drawing.Point(30, 430);
			this.fraWhere.Name = "fraWhere";
			this.fraWhere.Size = new System.Drawing.Size(931, 205);
			this.fraWhere.TabIndex = 4;
			this.fraWhere.Text = "Select Search Criteria";
			// 
			// vsWhere
			// 
			this.vsWhere.AllowSelection = false;
			this.vsWhere.AllowUserToResizeColumns = false;
			this.vsWhere.AllowUserToResizeRows = false;
			this.vsWhere.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsWhere.BackColorAlternate = System.Drawing.SystemColors.Window;
			this.vsWhere.BackColorBkg = System.Drawing.SystemColors.AppWorkspace;
			this.vsWhere.BackColorFixed = System.Drawing.SystemColors.Control;
			this.vsWhere.BackColorSel = System.Drawing.SystemColors.Highlight;
			this.vsWhere.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsWhere.Cols = 10;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsWhere.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsWhere.ColumnHeadersHeight = 30;
			this.vsWhere.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.vsWhere.ColumnHeadersVisible = false;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsWhere.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsWhere.DragIcon = null;
			this.vsWhere.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsWhere.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsWhere.FixedRows = 0;
			this.vsWhere.ForeColorFixed = System.Drawing.SystemColors.ControlText;
			this.vsWhere.FrozenCols = 0;
			this.vsWhere.GridColor = System.Drawing.SystemColors.Control;
			this.vsWhere.GridColorFixed = System.Drawing.SystemColors.ControlDark;
			this.vsWhere.Location = new System.Drawing.Point(20, 30);
			this.vsWhere.Name = "vsWhere";
			this.vsWhere.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsWhere.RowHeightMin = 0;
			this.vsWhere.Rows = 0;
			this.vsWhere.ScrollTipText = null;
			this.vsWhere.ShowColumnVisibilityMenu = false;
			this.vsWhere.ShowFocusCell = false;
			this.vsWhere.Size = new System.Drawing.Size(891, 155);
			this.vsWhere.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsWhere.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsWhere.TabIndex = 5;
			// 
			// fraSort
			// 
			this.fraSort.Controls.Add(this.lstSort);
			this.fraSort.Location = new System.Drawing.Point(354, 214);
			this.fraSort.Name = "fraSort";
			this.fraSort.Size = new System.Drawing.Size(294, 186);
			this.fraSort.TabIndex = 2;
			this.fraSort.Text = "Fields To Sort By";
			// 
			// lstSort
			// 
			this.lstSort.Appearance = 0;
			this.lstSort.BackColor = System.Drawing.SystemColors.Window;
			this.lstSort.Location = new System.Drawing.Point(20, 30);
			this.lstSort.MultiSelect = 0;
			this.lstSort.Name = "lstSort";
			this.lstSort.Size = new System.Drawing.Size(254, 136);
			this.lstSort.Sorted = false;
			this.lstSort.TabIndex = 3;
			// 
			// fraFields
			// 
			this.fraFields.Controls.Add(this.lstFields);
			this.fraFields.Location = new System.Drawing.Point(30, 214);
			this.fraFields.Name = "fraFields";
			this.fraFields.Size = new System.Drawing.Size(294, 186);
			this.fraFields.TabIndex = 0;
			this.fraFields.Text = "Fields To Display On Report";
			// 
			// lstFields
			// 
			this.lstFields.Appearance = 0;
			this.lstFields.BackColor = System.Drawing.SystemColors.Window;
			this.lstFields.Location = new System.Drawing.Point(20, 30);
			this.lstFields.MultiSelect = 0;
			this.lstFields.Name = "lstFields";
			this.lstFields.Size = new System.Drawing.Size(254, 136);
			this.lstFields.Sorted = false;
			this.lstFields.TabIndex = 1;
			this.lstFields.SelectedIndexChanged += new System.EventHandler(this.lstFields_SelectedIndexChanged);
			// 
			// fraMessage
			// 
			this.fraMessage.AppearanceKey = "groupBoxNoBorders";
			this.fraMessage.Controls.Add(this.Label3);
			this.fraMessage.Location = new System.Drawing.Point(17, 38);
			this.fraMessage.Name = "fraMessage";
			this.fraMessage.Size = new System.Drawing.Size(976, 102);
			this.fraMessage.TabIndex = 11;
			this.fraMessage.Visible = false;
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(22, 30);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(505, 52);
			this.Label3.TabIndex = 12;
			this.Label3.Text = "THIS REPORT IS PRE-DEFINED. SOME AREAS ON THIS FORM MAY NOT BE ACCESSIBLE";
			this.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// fraLayout
			// 
			this.fraLayout.AppearanceKey = "groupBoxNoBorders";
			this.fraLayout.Controls.Add(this.fraMessage);
			this.fraLayout.Controls.Add(this.vsLayout);
			this.fraLayout.Controls.Add(this.Image1);
			this.fraLayout.Location = new System.Drawing.Point(17, 30);
			this.fraLayout.Name = "fraLayout";
			this.fraLayout.Size = new System.Drawing.Size(1037, 164);
			this.fraLayout.TabIndex = 7;
			// 
			// vsLayout
			// 
			this.vsLayout.AllowBigSelection = false;
			this.vsLayout.AllowSelection = false;
			this.vsLayout.AllowUserToResizeColumns = false;
			this.vsLayout.AllowUserToResizeRows = false;
			this.vsLayout.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsLayout.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsLayout.BackColorBkg = System.Drawing.Color.Empty;
			this.vsLayout.BackColorFixed = System.Drawing.Color.Empty;
			this.vsLayout.BackColorSel = System.Drawing.Color.Empty;
			this.vsLayout.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsLayout.Cols = 0;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsLayout.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.vsLayout.ColumnHeadersHeight = 30;
			this.vsLayout.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsLayout.DefaultCellStyle = dataGridViewCellStyle4;
			this.vsLayout.DragIcon = null;
			this.vsLayout.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsLayout.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsLayout.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExMove;
			this.vsLayout.FixedCols = 0;
			this.vsLayout.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsLayout.FrozenCols = 0;
			this.vsLayout.GridColor = System.Drawing.Color.Empty;
			this.vsLayout.GridColorFixed = System.Drawing.Color.Empty;
			this.vsLayout.Location = new System.Drawing.Point(20, 42);
			this.vsLayout.Name = "vsLayout";
			this.vsLayout.RowHeadersVisible = false;
			this.vsLayout.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsLayout.RowHeightMin = 0;
			this.vsLayout.Rows = 1;
			this.vsLayout.ScrollTipText = null;
			this.vsLayout.ShowColumnVisibilityMenu = false;
			this.vsLayout.ShowFocusCell = false;
			this.vsLayout.Size = new System.Drawing.Size(973, 98);
			this.vsLayout.StandardTab = true;
			this.vsLayout.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsLayout.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsLayout.TabIndex = 10;
			this.vsLayout.ColumnWidthChanged += new Wisej.Web.DataGridViewColumnEventHandler(this.vsLayout_ColumnWidthChanged);
			this.vsLayout.RowHeightChanged += new Wisej.Web.DataGridViewRowEventHandler(this.vsLayout_RowHeightChanged);
			// 
			// Image1
			// 
			this.Image1.AllowDrop = true;
			this.Image1.BorderStyle = Wisej.Web.BorderStyle.None;
			this.Image1.DrawStyle = ((short)(0));
			this.Image1.DrawWidth = ((short)(1));
			this.Image1.FillStyle = ((short)(1));
			this.Image1.FontTransparent = true;
			this.Image1.Image = ((System.Drawing.Image)(resources.GetObject("Image1.Image")));
			this.Image1.Location = new System.Drawing.Point(17, 15);
			this.Image1.Name = "Image1";
			this.Image1.Picture = ((System.Drawing.Image)(resources.GetObject("Image1.Picture")));
			this.Image1.Size = new System.Drawing.Size(976, 22);
			this.Image1.TabIndex = 11;
			// 
			// ImageList1
			// 
			this.ImageList1.Images.AddRange(new Wisej.Web.ImageListEntry[] {
				imageListEntry1
			});
			this.ImageList1.ImageSize = new System.Drawing.Size(256, 18);
			this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuClear,
				this.mnuChangeParameters
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuClear
			// 
			this.mnuClear.Index = 0;
			this.mnuClear.Name = "mnuClear";
			this.mnuClear.Text = "Clear Search Criteria";
			this.mnuClear.Click += new System.EventHandler(this.mnuClear_Click);
			// 
			// mnuChangeParameters
			// 
			this.mnuChangeParameters.Index = 1;
			this.mnuChangeParameters.Name = "mnuChangeParameters";
			this.mnuChangeParameters.Text = "Change Parameters";
			// 
			// mnuSP11
			// 
			this.mnuSP11.Index = -1;
			this.mnuSP11.Name = "mnuSP11";
			this.mnuSP11.Text = "-";
			// 
			// mnuAddRow
			// 
			this.mnuAddRow.Index = -1;
			this.mnuAddRow.Name = "mnuAddRow";
			this.mnuAddRow.Shortcut = Wisej.Web.Shortcut.F2;
			this.mnuAddRow.Text = "Add Row";
			this.mnuAddRow.Click += new System.EventHandler(this.mnuAddRow_Click);
			// 
			// mnuAddColumn
			// 
			this.mnuAddColumn.Index = -1;
			this.mnuAddColumn.Name = "mnuAddColumn";
			this.mnuAddColumn.Shortcut = Wisej.Web.Shortcut.F3;
			this.mnuAddColumn.Text = "Add Column";
			this.mnuAddColumn.Click += new System.EventHandler(this.mnuAddColumn_Click);
			// 
			// mnuDeleteRow
			// 
			this.mnuDeleteRow.Index = -1;
			this.mnuDeleteRow.Name = "mnuDeleteRow";
			this.mnuDeleteRow.Shortcut = Wisej.Web.Shortcut.F4;
			this.mnuDeleteRow.Text = "Delete Row";
			this.mnuDeleteRow.Click += new System.EventHandler(this.mnuDeleteRow_Click);
			// 
			// mnuDeleteColumn
			// 
			this.mnuDeleteColumn.Index = -1;
			this.mnuDeleteColumn.Name = "mnuDeleteColumn";
			this.mnuDeleteColumn.Shortcut = Wisej.Web.Shortcut.F5;
			this.mnuDeleteColumn.Text = "Delete Column";
			this.mnuDeleteColumn.Click += new System.EventHandler(this.mnuDeleteColumn_Click);
			// 
			// mnuSP2
			// 
			this.mnuSP2.Index = -1;
			this.mnuSP2.Name = "mnuSP2";
			this.mnuSP2.Text = "-";
			// 
			// mnuPrint
			// 
			this.mnuPrint.Index = -1;
			this.mnuPrint.Name = "mnuPrint";
			this.mnuPrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuPrint.Text = "Print Report";
			// 
			// mnuSP1
			// 
			this.mnuSP1.Index = -1;
			this.mnuSP1.Name = "mnuSP1";
			this.mnuSP1.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = -1;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdPrint
			// 
			this.cmdPrint.AppearanceKey = "acceptButton";
			this.cmdPrint.AutoSize = true;
			this.cmdPrint.Location = new System.Drawing.Point(242, 30);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Size = new System.Drawing.Size(118, 48);
			this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPrint.TabIndex = 0;
			this.cmdPrint.Text = "Print Report";
			// 
			// cmdAddRow
			// 
			this.cmdAddRow.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdAddRow.AppearanceKey = "toolbarButton";
			this.cmdAddRow.Location = new System.Drawing.Point(754, 30);
			this.cmdAddRow.Name = "cmdAddRow";
			this.cmdAddRow.Shortcut = Wisej.Web.Shortcut.F2;
			this.cmdAddRow.Size = new System.Drawing.Size(71, 24);
			this.cmdAddRow.TabIndex = 1;
			this.cmdAddRow.Text = "Add Row";
			this.cmdAddRow.Click += new System.EventHandler(this.mnuAddRow_Click);
			// 
			// cmdAddColumn
			// 
			this.cmdAddColumn.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdAddColumn.AppearanceKey = "toolbarButton";
			this.cmdAddColumn.Location = new System.Drawing.Point(830, 30);
			this.cmdAddColumn.Name = "cmdAddColumn";
			this.cmdAddColumn.Shortcut = Wisej.Web.Shortcut.F3;
			this.cmdAddColumn.Size = new System.Drawing.Size(91, 24);
			this.cmdAddColumn.TabIndex = 1;
			this.cmdAddColumn.Text = "Add Column";
			this.cmdAddColumn.Click += new System.EventHandler(this.mnuAddColumn_Click);
			// 
			// cmdDeleteRow
			// 
			this.cmdDeleteRow.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDeleteRow.AppearanceKey = "toolbarButton";
			this.cmdDeleteRow.Location = new System.Drawing.Point(558, 30);
			this.cmdDeleteRow.Name = "cmdDeleteRow";
			this.cmdDeleteRow.Shortcut = Wisej.Web.Shortcut.F5;
			this.cmdDeleteRow.Size = new System.Drawing.Size(87, 24);
			this.cmdDeleteRow.TabIndex = 1;
			this.cmdDeleteRow.Text = "Delete Row";
			this.cmdDeleteRow.Click += new System.EventHandler(this.mnuDeleteRow_Click);
			// 
			// cmdDeleteColumn
			// 
			this.cmdDeleteColumn.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDeleteColumn.AppearanceKey = "toolbarButton";
			this.cmdDeleteColumn.Location = new System.Drawing.Point(651, 30);
			this.cmdDeleteColumn.Name = "cmdDeleteColumn";
			this.cmdDeleteColumn.Shortcut = Wisej.Web.Shortcut.F4;
			this.cmdDeleteColumn.Size = new System.Drawing.Size(98, 24);
			this.cmdDeleteColumn.TabIndex = 1;
			this.cmdDeleteColumn.Text = "Delete Column";
			this.cmdDeleteColumn.Click += new System.EventHandler(this.mnuDeleteColumn_Click);
			// 
			// cmdClearCriteria
			// 
			this.cmdClearCriteria.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdClearCriteria.AppearanceKey = "toolbarButton";
			this.cmdClearCriteria.Location = new System.Drawing.Point(926, 30);
			this.cmdClearCriteria.Name = "cmdClearCriteria";
			this.cmdClearCriteria.Size = new System.Drawing.Size(97, 24);
			this.cmdClearCriteria.TabIndex = 1;
			this.cmdClearCriteria.Text = "Clear Criteria";
			this.cmdClearCriteria.Click += new System.EventHandler(this.mnuClear_Click);
			// 
			// frmCustomReport
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(1053, 768);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmCustomReport";
			this.Text = "Custom Report";
			this.Load += new System.EventHandler(this.frmCustomReport_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCustomReport_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCustomReport_KeyPress);
			this.Resize += new System.EventHandler(this.frmCustomReport_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.BottomPanel.PerformLayout();
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraWhere)).EndInit();
			this.fraWhere.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsWhere)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSort)).EndInit();
			this.fraSort.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraFields)).EndInit();
			this.fraFields.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraMessage)).EndInit();
			this.fraMessage.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraLayout)).EndInit();
			this.fraLayout.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsLayout)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddRow)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddColumn)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteRow)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteColumn)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClearCriteria)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdPrint;
		private FCButton cmdAddRow;
		private FCButton cmdAddColumn;
		private FCButton cmdDeleteRow;
		private FCButton cmdDeleteColumn;
		private FCButton cmdClearCriteria;
	}
}