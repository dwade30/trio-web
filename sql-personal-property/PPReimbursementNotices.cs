﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmPPReimbursementNotices.
	/// </summary>
	public partial class frmPPReimbursementNotices : BaseForm
	{
		public frmPPReimbursementNotices()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			cmbRange.SelectedIndex = 0;
			cmbType.SelectedIndex = 0;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPPReimbursementNotices InstancePtr
		{
			get
			{
				return (frmPPReimbursementNotices)Sys.GetInstance(typeof(frmPPReimbursementNotices));
			}
		}

		protected frmPPReimbursementNotices _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		bool bolStopProcessing;
		string[] strSplit = new string[11 + 1];
		string Used;
		// vbPorter upgrade warning: PPMONTH As Variant, short --> As int	OnWriteFCConvert.ToInt16(
		int PPMONTH;
		string UsePPMonth;
		// vbPorter upgrade warning: Page1 As Variant, short --> As int	OnWriteFCConvert.ToInt16(
		int Page1;
		// vbPorter upgrade warning: rndg2 As Variant, short --> As int	OnWriteFCConvert.ToInt16(
		int rndg2;
		// vbPorter upgrade warning: holdrndg As Variant, short --> As int	OnWriteFCConvert.ToInt16(
		int holdrndg;
		/// <summary>
		/// Used For Mailing Address Compression
		/// </summary>
		string PPName;
		string PPAddress1;
		string PPAddress2;
		string PPAddress3;
		/// <summary>
		///
		/// </summary>
		string HoldDescription;
		bool NoItemized;
		bool NoLeased;
		// vbPorter upgrade warning: HeadingFlag As FixedString	OnWrite(string)
		char[] HeadingFlag = new char[1];
		// vbPorter upgrade warning: FormActivated As FixedString	OnWrite(string)
		char[] FormActivated = new char[1];
		clsDRWrapper rs = new clsDRWrapper();
		string strSQL;
		int ii;
		/// <summary>
		/// only used in For....Next loops
		/// </summary>
		// vbPorter upgrade warning: Range As FixedString	OnWrite(string)
		char[] Range = new char[1];
		int LinesOnPage;
		int LineCount;
		int PageCount;
		// vbPorter upgrade warning: Start As Variant --> As string
		string Start;
		// vbPorter upgrade warning: Ending As Variant --> As string
		string Ending;
		string SQLWhere;
		string SQLOrderBy;
		double PercentGood;
		int rbYear;
		/// <summary>
		/// Variables Used to hold Leased/Itemized/Account Totals
		/// </summary>
		int FinalTotal;
		double TotalAccountTax;
		double AccountTax;
		int AccountTotal;
		int TotalLeasedCost;
		int TotalLeasedValue;
		int TotalItemizedCost;
		int TotalItemizedValue;

		private void print801b()
		{
			c801Controller cont801 = new c801Controller();
			c801Report rep801 = new c801Report();
			//frmBusy bWait = new frmBusy();
			FCUtils.StartTask(this, () =>
			{
				this.ShowWait();
				//bWait.Message = "Loading 801 information";
				//bWait.StartBusy();
				//bWait.Show(FCForm.FormShowEnum.Modeless);
				this.UpdateWait("Loading Data ...");
				rep801.RangeStart = Strings.Trim(txtStart.Text);
				rep801.RangeEnd = Strings.Trim(txtStop.Text);
				if (cmbRange.SelectedIndex == 1)
				{
					rep801.ByRange = true;
				}
				rep801.RangeType = modPPGN.Statics.PrintMenuSelection;
				rep801.TaxYear = DateTime.Today.Year;
				if (Conversion.Val(txtTaxYear.Text) > 1900)
				{
					rep801.TaxYear = FCConvert.ToInt32(Math.Round(Conversion.Val(txtTaxYear.Text)));
				}
				if (Conversion.Val(txtTaxRate.Text) > 0)
				{
					rep801.TaxRate = Conversion.Val(txtTaxRate.Text) / 1000;
					rep801.DisplayedRate = txtTaxRate.Text;
				}
				cont801.Fill801Report(rep801);
				//bWait.StopBusy();
				//bWait.Unload();
				/*- bWait = null; */// Call rpt801b.Init(rep801)
				if (rep801.Records.ItemCount() > 0)
				{
					rpt801.InstancePtr.Init(rep801);
				}
				else
				{
					MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
                this.EndWait();
            });
		}

		private void PrintListing()
		{
			print801b();
			return;
			// Dim strStart As String
			// Dim strEnd As String
			// Dim strFieldName As String
			// 
			// Set Database
			// 
			// 
			// 
			// strStart = Trim(txtStart.Text)
			// strEnd = Trim(txtStop.Text)
			// 
			// If optRange(1).Value Then
			// SQLWhere = " between "
			// End If
			// 
			// SQLOrderBy = "Line"
			// Select Case PrintMenuSelection
			// Case 0
			// name
			// strStart = "'" & strStart & "'"
			// strEnd = "'" & strEnd & "zzz'"
			// strFieldName = "Name "
			// Case 1
			// account
			// strFieldName = "Account "
			// Case 2
			// location
			// strStart = "'" & strStart & "'"
			// strEnd = "'" & strEnd & "zzz'"
			// strFieldName = "StreetName "
			// Case 3
			// businesscode
			// strFieldName = "Businesscode "
			// Case 4
			// trancode
			// strFieldName = "TranCode "
			// Case 6
			// open1
			// strStart = "'" & strStart & "'"
			// strEnd = "'" & strEnd & "zzz'"
			// strFieldName = "Open1 "
			// Case 7
			// open2
			// strStart = "'" & strStart & "'"
			// strEnd = "'" & strEnd & "zzz'"
			// strFieldName = "Open 2 "
			// End Select
			// 
			// If optRange(0).Value Then
			// SQLWhere = "WHERE RBCode = 'Y' and not deleted = 1 ORDER BY " & strFieldName
			// Else
			// SQLWhere = "where rbcode = 'Y' and not deleted  = 1 and " & strFieldName & " between " & strStart & " and " & strEnd & " order by " & strFieldName
			// End If
			// 
			// Dim strMasterJoin As String
			// strMasterJoin = GetMasterJoin
			// strSQL = "SELECT * FROM ppmaster " & SQLWhere
			// strSQL = strMasterJoin & " " & SQLWhere
			// Call rptReimbursement2.Start(strSQL, Not (chkQty.Value = vbChecked))
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			bolStopProcessing = true;
			frmPPReimbursementNotices.InstancePtr.Close();
		}

		public void cmdCancel_Click()
		{
			//cmdCancel_Click(cmdCancel, new System.EventArgs());
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			int x;
			if (cmbRange.SelectedIndex == 1 && (Strings.Trim(txtStart.Text) == string.Empty || Strings.Trim(txtStop.Text) == string.Empty))
			{
				MessageBox.Show("You must specify a valid range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			for (x = 0; x <= 7; x++)
			{
				if (cmbType.SelectedIndex == x)
				{
					modPPGN.Statics.PrintMenuSelection = x;
					break;
				}
			}
			// x
			strSQL = "SELECT * FROM ReimbursementInfo";
			rs.OpenRecordset(strSQL, modPPGN.strPPDatabase);
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.Edit();
			}
			else
			{
				rs.AddNew();
				rs.Set_Fields("TaxYear", 0);
				rs.Set_Fields("DueDate1", 0);
				rs.Set_Fields("DueDate2", 0);
				rs.Set_Fields("TaxRate", 0);
				rs.Set_Fields("Signer", "");
				rs.Set_Fields("SignersTitle", "");
				rs.Set_Fields("Message", "");
			}
			// If txtTaxYear.DataChanged = True Then
			if (txtTaxYear.Text != "")
			{
				rs.Set_Fields("TaxYear", Strings.Format(txtTaxYear.Text, "0000"));
			}
			else
			{
				rs.Set_Fields("TaxYear", 0);
			}
			// End If
			// If txtFirstPayment.DataChanged = True Then
			if (txtFirstPayment.Text != "")
			{
				rs.Set_Fields("DueDate1", Strings.Format(txtFirstPayment.Text, "MM/dd/yyyy"));
			}
			else
			{
				rs.Set_Fields("DueDate1", 0);
			}
			// End If
			// If txtSecondPayment.DataChanged = True Then
			if (txtSecondPayment.Text != "")
			{
				rs.Set_Fields("DueDate2", Strings.Format(txtSecondPayment.Text, "MM/dd/yyyy"));
			}
			else
			{
				rs.Set_Fields("DueDate2", 0);
			}
			// End If
			// If txtTaxRate.DataChanged = True Then
			if (txtTaxRate.Text != "")
			{
				rs.Set_Fields("TaxRate", Strings.Format(txtTaxRate.Text, "00.000"));
			}
			else
			{
				rs.Set_Fields("TaxRate", 0);
			}
			// End If
			modPPGN.Statics.rbTaxRate = FCConvert.ToSingle(rs.Get_Fields_Double("taxrate") / 1000);
			// If txtSigner.DataChanged = True Then
			if (txtSigner.Text != "")
			{
				rs.Set_Fields("Signer", txtSigner.Text);
			}
			else
			{
				rs.Set_Fields("Signer", " ");
			}
			// End If
			// If txtSignerTitle.DataChanged = True Then
			if (txtSignerTitle.Text != "")
			{
				rs.Set_Fields("SignersTitle", txtSignerTitle.Text);
			}
			else
			{
				rs.Set_Fields("SignersTitle", " ");
			}
			// End If
			// If txtMessage.DataChanged = True Then
			if (txtMessage.Text != "")
			{
				rs.Set_Fields("Message", txtMessage.Text);
			}
			else
			{
				rs.Set_Fields("Message", " ");
			}
			modPPGN.Statics.rbMessage = txtMessage.Text;
			// End If
			rs.Update();
			PrintListing();
		}

		public void cmdOK_Click()
		{
			cmdOK_Click(cmdOK, new System.EventArgs());
		}

		private void frmPPReimbursementNotices_Activated(object sender, System.EventArgs e)
		{
			if (modGNWork.FormExist(this))
				return;
			SetRBInfo();
			this.fraRange.Visible = false;
		}

		private void frmPPReimbursementNotices_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						cmdCancel_Click();
						break;
					}
			}
			//end switch
		}

		private void frmPPReimbursementNotices_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				frmPPReimbursementNotices.InstancePtr.Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmPPReimbursementNotices_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPPReimbursementNotices properties;
			//frmPPReimbursementNotices.ScaleWidth	= 9045;
			//frmPPReimbursementNotices.ScaleHeight	= 7545;
			//frmPPReimbursementNotices.LinkTopic	= "Form1";
			//End Unmaped Properties
			modPPGN.Check_RB_Fields();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			modPPGN.GetOpenFields();
			if (Strings.Trim(modPPGN.Statics.Open1Title) != string.Empty)
			{
				if (cmbType.Items.Contains("Open 1"))
				{
					cmbType.Items.Insert(6, modPPGN.Statics.Open1Title);
					cmbType.Items.Remove("Open 1");
				}
			}
			if (Strings.Trim(modPPGN.Statics.Open2Title) != string.Empty)
			{
				if (cmbType.Items.Contains("Open 2"))
				{
					cmbType.Items.Insert(7, modPPGN.Statics.Open2Title);
					cmbType.Items.Remove("Open 2");
				}
			}
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			FCUtils.EraseSafe(strSplit);
			modPPGN.Statics.Msg1 = "";
			modPPGN.Statics.Msg2 = "";
			modPPGN.Statics.Msg3 = "";
			modPPGN.Statics.Msg4 = "";
			modPPGN.Statics.Msg5 = "";
			modPPGN.Statics.Msg6 = "";
			modPPGN.Statics.Msg7 = "";
			modPPGN.Statics.Msg8 = "";
			modPPGN.Statics.Msg9 = "";
			Used = "";
			PPMONTH = 0;
			UsePPMonth = "";
			Page1 = 0;
			rndg2 = 0;
			holdrndg = 0;
			PPName = "";
			PPAddress1 = "";
			PPAddress2 = "";
			PPAddress3 = "";
			HoldDescription = "";
			NoItemized = false;
			NoLeased = false;
			HeadingFlag = "".ToCharArray();
			FormActivated = "".ToCharArray();
			strSQL = "";
			ii = 0;
			Range = "".ToCharArray();
			LinesOnPage = 0;
			LineCount = 0;
			PageCount = 0;
			Start = "";
			Ending = "";
			SQLWhere = "";
			SQLOrderBy = "";
			PercentGood = 0;
			rbYear = 0;
			FinalTotal = 0;
			TotalAccountTax = 0;
			AccountTax = 0;
			AccountTotal = 0;
			TotalLeasedCost = 0;
			TotalLeasedValue = 0;
			TotalItemizedCost = 0;
			TotalItemizedValue = 0;
			/*? On Error GoTo 0 */
			try
			{
				// 
				modGlobal.SaveWindowSize(this);
				// menuPPPrint.Show
			}
			catch (Exception ex)
			{
			}
		}

		private void SetRBInfo()
		{
			// Set Database & Recordset
			strSQL = "SELECT * FROM ReimbursementInfo";
			rs.OpenRecordset(strSQL, modPPGN.strPPDatabase);
			// 
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				/*? On Error Resume Next  */// used for null values in the default info
				txtTaxYear.Text = Strings.Format(rs.Get_Fields_Int32("TaxYear"), "0000");
				if (FCConvert.ToDateTime(rs.Get_Fields_DateTime("duedate1") as object).ToOADate() == 0)
				{
					txtFirstPayment.Text = "";
				}
				else
				{
					txtFirstPayment.Text = Strings.Format(rs.Get_Fields_DateTime("DueDate1"), "MM/dd/yyyy");
				}
				if (FCConvert.ToDateTime(rs.Get_Fields_DateTime("duedate2") as object).ToOADate() == 0)
				{
					txtSecondPayment.Text = "";
				}
				else
				{
					txtSecondPayment.Text = Strings.Format(rs.Get_Fields_DateTime("DueDate2"), "MM/dd/yyyy");
				}
				txtTaxRate.Text = Strings.Format(rs.Get_Fields_Double("TaxRate"), "00.000");
				txtSigner.Text = FCConvert.ToString(rs.Get_Fields_String("Signer"));
				txtSignerTitle.Text = FCConvert.ToString(rs.Get_Fields_String("SignersTitle"));
				txtMessage.Text = FCConvert.ToString(rs.Get_Fields_String("Message"));
				// 
				modPPGN.Statics.rbMessage = FCConvert.ToString(rs.Get_Fields_String("message"));
				modPPGN.Statics.rbTaxYear = FCConvert.ToInt32(txtTaxYear.Text);
				modPPGN.Statics.rbDueDate1 = FCConvert.ToDateTime(txtFirstPayment.Text);
				modPPGN.Statics.rbDueDate2 = FCConvert.ToDateTime(txtSecondPayment.Text);
				modPPGN.Statics.rbTaxRate = FCConvert.ToSingle(rs.Get_Fields_Double("TaxRate") / 1000);
				modPPGN.Statics.rbSigner = txtSigner.Text;
				modPPGN.Statics.rbSignerTitle = txtSignerTitle.Text;
				if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("Message")) == false)
					Set_Message_String();
				// Set DataChanged = False
				txtTaxYear.DataChanged = false;
				txtFirstPayment.DataChanged = false;
				txtSecondPayment.DataChanged = false;
				txtTaxRate.DataChanged = false;
				txtSigner.DataChanged = false;
				txtSignerTitle.DataChanged = false;
				txtMessage.DataChanged = false;
				/*? On Error GoTo 0 */
			}
			else
			{
				// Do Nothing
			}
		}

		private void mnuContinue_Click(object sender, System.EventArgs e)
		{
			cmdOK_Click();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}
		/// <summary>
		/// Private Sub optFormat_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, Y As Single)
		/// </summary>
		/// <summary>
		/// If Index = 4 Then
		/// </summary>
		/// <summary>
		/// txtSpecificAccount.Enabled = True
		/// </summary>
		/// <summary>
		/// Else
		/// </summary>
		/// <summary>
		/// txtSpecificAccount.Enabled = False
		/// </summary>
		/// <summary>
		/// End If
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// PrintMenuSelection = Index + 1
		/// </summary>
		/// <summary>
		/// End Sub
		/// </summary>
		private void optRange_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
					{
						fraRange.Visible = false;
						break;
					}
				case 1:
					{
						fraRange.Visible = true;
						break;
					}
			}
			//end switch
		}

		private void optRange_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbRange.SelectedIndex;
			optRange_CheckedChanged(index, sender, e);
		}

		private void txtFirstPayment_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (txtFirstPayment.Text != "")
				modPPGN.Statics.rbDueDate1 = FCConvert.ToDateTime(txtFirstPayment.Text);
		}

		private void txtMessage_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			Set_Message_String();
			modPPGN.Statics.rbMessage = txtMessage.Text;
		}

		private void txtSecondPayment_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (txtSecondPayment.Text != "" && Information.IsDate(txtSecondPayment.Text))
			{
				modPPGN.Statics.rbDueDate2 = FCConvert.ToDateTime(txtSecondPayment.Text);
			}
			else
			{
				modPPGN.Statics.rbDueDate2 = DateTime.FromOADate(0);
			}
		}

		private void txtSigner_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			modPPGN.Statics.rbSigner = txtSigner.Text;
		}

		private void txtSignerTitle_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			modPPGN.Statics.rbSignerTitle = txtSignerTitle.Text;
		}
		/// <summary>
		/// Private Sub txtSpecificAccount_KeyDown(KeyCode As Integer, Shift As Integer)
		/// </summary>
		/// <summary>
		/// If KeyCode = vbKeyReturn Then
		/// </summary>
		/// <summary>
		/// Call cmdOK_Click
		/// </summary>
		/// <summary>
		/// End If
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// End Sub
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// Private Sub txtSpecificAccount_Validate(Cancel As Boolean)
		/// </summary>
		/// <summary>
		/// If txtSpecificAccount.Text = vbNullString Then Exit Sub
		/// </summary>
		/// <summary>
		/// If IsNumeric(txtSpecificAccount.Text) = False Then
		/// </summary>
		/// <summary>
		/// MsgBox "Enter numbers only in this field.", vbExclamation
		/// </summary>
		/// <summary>
		/// End If
		/// </summary>
		/// <summary>
		/// End Sub
		/// </summary>
		private void txtTaxRate_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			/*? On Error Resume Next  */
			try
			{
				if (Conversion.Val(txtTaxRate.Text) != 0)
				{
					modPPGN.Statics.rbTaxRate = FCConvert.ToSingle(FCConvert.ToDouble(txtTaxRate.Text) / 1000);
				}
			}
			catch (Exception ex)
			{
			}
		}

		private void txtTaxYear_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			modPPGN.Statics.rbTaxYear = FCConvert.ToInt32(txtTaxYear.Text);
		}

		public void Set_Message_String()
		{
			// Dim str1
			// str1 = Split(txtMessage.Text, vbCrLf, -1, vbTextCompare)
			// On Error Resume Next
			// strSplit(0) = str1(0)
			// strSplit(1) = str1(1)
			// strSplit(2) = str1(2)
			// strSplit(3) = str1(3)
			// strSplit(4) = str1(4)
			// strSplit(5) = str1(5)
			// strSplit(6) = str1(6)
			// strSplit(7) = str1(7)
			// strSplit(8) = str1(8)
			// strSplit(9) = str1(9)
			// If Len(strSplit(0)) > 65 Then Call Fix_MsgX(0)
			// If Len(strSplit(1)) > 65 Then Call Fix_MsgX(1)
			// If Len(strSplit(2)) > 65 Then Call Fix_MsgX(2)
			// If Len(strSplit(3)) > 65 Then Call Fix_MsgX(3)
			// If Len(strSplit(4)) > 65 Then Call Fix_MsgX(4)
			// If Len(strSplit(5)) > 65 Then Call Fix_MsgX(5)
			// If Len(strSplit(6)) > 65 Then Call Fix_MsgX(6)
			// If Len(strSplit(7)) > 65 Then Call Fix_MsgX(7)
			// If Len(strSplit(8)) > 65 Then Call Fix_MsgX(8)
			// Msg1 = strSplit(0)
			// Msg2 = strSplit(1)
			// Msg3 = strSplit(2)
			// Msg4 = strSplit(3)
			// Msg5 = strSplit(4)
			// Msg6 = strSplit(5)
			// Msg7 = strSplit(6)
			// Msg8 = strSplit(7)
			// Msg9 = strSplit(8)
			// On Error GoTo 0
		}

		public void Fix_MsgX(ref short int1)
		{
			string strHold = "";
			int intX = 0;
			/*? On Error GoTo 0 */
			try
			{
				if (int1 < 8)
				{
					strHold = strSplit[int1];
					intX = Strings.InStrRev(strHold, " ", 65, CompareConstants.vbTextCompare/*?*/);
					strHold = Strings.Mid(strSplit[int1], 1, intX);
					strSplit[int1 + 1] = Strings.Mid(strSplit[int1], intX + 1, strSplit[int1].Length);
					strSplit[int1] = strHold;
					strHold = "";
				}
				else
				{
					strSplit[8] = Strings.Mid(strSplit[8], 1, 65);
				}
			}
			catch (Exception ex)
			{
			}
		}
	}
}
