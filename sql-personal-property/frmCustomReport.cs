﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmCustomReport.
	/// </summary>
	public partial class frmCustomReport : BaseForm
	{
		public frmCustomReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// THIS FORM IS FOR THE CREATION OF CUSTOM REPORTS. IT ALLOWS THE USER
		/// </summary>
		/// <summary>
		/// TO SELECT THE FIELDS TO BE DISPLAYED, THE ORDER TO DISPLAY THEM IN
		/// </summary>
		/// <summary>
		/// THE SORT ORDER TO SHOW THEM IN AND THE ABILITY TO FILTER THE RECORDS
		/// </summary>
		/// <summary>
		/// BY ANY FIELD EVEN IF IT IS NOT BEING SHOWN.
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// THIS FORM ***MUST*** WORK WITH modCustomReport.mod and rptCustomReport.rpt
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// THE DEVELOPER NEEDS ONLY ADD A CASE STATEMENT IN THE ROUNTING
		/// </summary>
		/// <summary>
		/// SetFormFieldCaptions IN modCustomReport.mod
		/// </summary>
		/// <summary>
		/// NO CHANGES TO THIS FORM OR THE REPORT NEEDS TO BE DONE.
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// A CALL TO THIS FORM WOULD BE:
		/// </summary>
		/// <summary>
		/// frmCustomReport.Show , MDIParent
		/// </summary>
		/// <summary>
		/// Call SetFormFieldCaptions(frmCustomReport, "Births")
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		///
		/// </summary>
		int intCounter;
		int intStart;
		int intEnd;
		int intID;
		double dblPicResizeRatio;
		string strTemp = "";
		bool boolSaveReport;

		private void cmdClear_Click()
		{
			// CLEAR THE WHERE GRID SO THE USER CAN START FROM SCRATCH
			int intCounter;
			for (intCounter = 0; intCounter <= vsWhere.Rows - 1; intCounter++)
			{
				vsWhere.TextMatrix(intCounter, 1, string.Empty);
				vsWhere.TextMatrix(intCounter, 2, string.Empty);
				vsWhere.TextMatrix(intCounter, 3, string.Empty);
			}
		}

		private void cmdExit_Click()
		{
			Close();
		}

		private void frmCustomReport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.F2:
					{
						mnuAddRow_Click();
						break;
					}
				case Keys.F3:
					{
						mnuAddColumn_Click();
						break;
					}
				case Keys.F4:
					{
						mnuDeleteRow_Click();
						break;
					}
				case Keys.F5:
					{
						mnuDeleteColumn_Click();
						break;
					}
			}
			//end switch
			// make the enter key work like the tab
			// If KeyCode = 13 Then
			// KeyCode = 0
			// SendKeys "{TAB}"
			// Exit Sub
			// End If
			// 
			// If KeyCode = vbKeyF11 Then Call mnuSave_Click
			// If KeyCode = vbKeyF12 Then
			// Call mnuSave_Click
			// Call mnuExit_Click
			// End If
		}

		private void frmCustomReport_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// was esc key pressed
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmCustomReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomReport properties;
			//frmCustomReport.ScaleWidth	= 8955;
			//frmCustomReport.ScaleHeight	= 7365;
			//frmCustomReport.LinkTopic	= "Form1";
			//End Unmaped Properties
			// LOAD THE FORM AND FILL THE CONTROLS WITH DATA IF THERE IS ANY
			clsDRWrapper rsCreateTable = new clsDRWrapper();
			dblPicResizeRatio = 1;
			//Image1.Width = 14 * 1440;
			Image1.Image = ImageList1.Images[0];
			Image1.SizeMode = PictureBoxSizeMode.StretchImage;
			//HScroll1.Maximum = Image1.Width - fraLayout.Width;
			modGlobalFunctions.SetFixedSize(this, 0);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
		}

		private void ResizeGrid()
		{
			int x;
			for (x = 0; x <= vsLayout.Cols - 1; x++)
			{
				vsLayout.ColWidth(x, FCConvert.ToInt32(FCConvert.ToDouble(vsLayout.ColData(x)) / dblPicResizeRatio));
			}
			// x
		}

		private void frmCustomReport_Resize(object sender, System.EventArgs e)
		{
			double dblOldposition;
			//dblPicResizeRatio = FCConvert.ToDouble(14 * 1440) / Image1.Width;
			//dblOldposition = FCConvert.ToDouble(HScroll1.Value / HScroll1.Maximum;
			//HScroll1.Maximum = Image1.Width - fraLayout.Width;
			ResizeGrid();
			// grid resizing moves everything so move it back
			//HScroll1.Value = FCConvert.ToInt32(dblOldposition * HScroll1.Maximum);
			//Image1.Left = -HScroll1.Value + 30;
			//vsLayout.Left = -HScroll1.Value + 30;
		}

		private void HScroll1_ValueChanged(object sender, System.EventArgs e)
		{
			//Image1.Left = -HScroll1.Value + 30;
			//vsLayout.Left = -HScroll1.Value + 30;
		}

		private void HScroll1_Scroll(object sender, Wisej.Web.ScrollEventArgs e)
		{
			//Image1.Left = -HScroll1.Value + 30;
			//vsLayout.Left = -HScroll1.Value + 30;
		}

		private void lstFields_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (lstFields.SelectedIndex < 0)
				return;
			if (vsLayout.Row < 1)
				vsLayout.Row = 1;
			vsLayout.TextMatrix(vsLayout.Row, vsLayout.Col, lstFields.Items[lstFields.SelectedIndex].Text);
			vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Col, lstFields.ItemData(lstFields.SelectedIndex));
		}

		//private void lstSort_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS IS USED SO THAT WHEN THE USER CLICKS ON AN ITEM IN THIS
		//	// CONTROL AND DRAGS IT TO A DIFFERENT LOCATION THEN WE WILL KNOW
		//	// WHERE TO SWAP THE TWO ITEMS.
		//	// 
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	intStart = lstSort.SelectedIndex;
		//}

		//private void lstSort_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS WILL CHANGE THE CAPTION AND THE ITEM DATA NUMBER FOR THE TWO
		//	// ITEMS THAT ARE TO BE SWAPED
		//	// 
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	// IF THIS IS A CHANGE (DRAG AND DROP) AND NOT JUST A CLICK THEN...
		//	if (intStart != lstSort.SelectedIndex)
		//	{
		//		// SAVE THE CAPTION AND ID FOR THE NEW ITEM
		//		strTemp = lstSort.Items[lstSort.SelectedIndex].Text;
		//		intID = lstSort.ItemData(lstSort.SelectedIndex);
		//		// CHANGE THE NEW ITEM
		//		lstSort.Items[lstSort.ListIndex].Text = lstSort.Items[intStart].Text;
		//		lstSort.ItemData(lstSort.ListIndex, lstSort.ItemData(intStart));
		//		// SAVE THE OLD ITEM
		//		lstSort.Items[intStart].Text = strTemp;
		//		lstSort.ItemData(intStart, intID);
		//		// SET BOTH ITEMS TO BE SELECTED
		//		lstSort.SetSelected(lstSort.ListIndex, true);
		//		lstSort.SetSelected(intStart, true);
		//	}
		//}

		private void mnuAddColumn_Click(object sender, System.EventArgs e)
		{
			vsLayout.Cols += 1;
			vsLayout.Select(vsLayout.Row, vsLayout.Cols - 1);
			vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Cols - 1, -1);
		}

		public void mnuAddColumn_Click()
		{
			mnuAddColumn_Click(mnuAddColumn, new System.EventArgs());
		}

		private void mnuAddRow_Click(object sender, System.EventArgs e)
		{
			vsLayout.Rows += 1;
            //FC:FINAL:AM:#i2291 - don't merge the row because the text in the columns won't be visible
            //vsLayout.MergeRow(vsLayout.Rows - 1, true);
			vsLayout.Select(vsLayout.Rows - 1, 0);
		}

		public void mnuAddRow_Click()
		{
			mnuAddRow_Click(mnuAddRow, new System.EventArgs());
		}

		private void mnuClear_Click(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}

		private void mnuDeleteColumn_Click(object sender, System.EventArgs e)
		{
			if (vsLayout.Cols > 1)
			{
				vsLayout.ColPosition(vsLayout.Col, vsLayout.Cols - 1);
				vsLayout.Cols -= 1;
			}
			else
			{
				MessageBox.Show("You must have at least one column in a report.");
			}
		}

		public void mnuDeleteColumn_Click()
		{
			mnuDeleteColumn_Click(mnuDeleteColumn, new System.EventArgs());
		}

		private void mnuDeleteRow_Click(object sender, System.EventArgs e)
		{
			if (vsLayout.Rows > 2)
			{
				vsLayout.RemoveItem(vsLayout.Row);
			}
			else
			{
				MessageBox.Show("You must have at least one row in a report.");
			}
		}

		public void mnuDeleteRow_Click()
		{
			mnuDeleteRow_Click(mnuDeleteRow, new System.EventArgs());
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void SetupGrid()
		{
			vsLayout.Width = Image1.Width;
			vsLayout.Rows = 2;
			vsLayout.Cols = 1;
			vsLayout.ColWidth(0, FCConvert.ToInt32(1440 / dblPicResizeRatio));
			vsLayout.ColData(0, 1440);
			vsLayout.Row = 1;
			vsLayout.Col = 0;
			vsLayout.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
            //FC:FINAL:AM:#i2291 - don't merge the row because the text in the columns won't be visible
            //vsLayout.MergeRow(1, true);
            vsLayout.Left = Image1.Left;
		}
		//FC:FINAL:BBE - replace FlexGrid event AfterUserResize with ColumnWidthChanged, RowHeightChanged
		private void vsLayout_RowHeightChanged(object sender, DataGridViewRowEventArgs e)
		{
			FCGrid grid = sender as FCGrid;
			vsLayout_AfterUserResize(grid.GetFlexRowIndex(e.RowIndex), -1);
		}

		private void vsLayout_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
		{
			FCGrid grid = sender as FCGrid;
			vsLayout_AfterUserResize(-1, grid.GetFlexColIndex(e.Column.Index));
		}

		private void vsLayout_AfterUserResize(int row, int col)
		{
			vsLayout.ColData(vsLayout.Col, vsLayout.ColWidth(vsLayout.Col) * dblPicResizeRatio);
		}
	}
}
