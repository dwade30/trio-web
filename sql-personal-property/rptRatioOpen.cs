﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Core;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptRatioOpen.
	/// </summary>
	public partial class rptRatioOpen : BaseSectionReport
	{
		public rptRatioOpen()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Ratio/Opens";
		}

		public static rptRatioOpen InstancePtr
		{
			get
			{
				return (rptRatioOpen)Sys.GetInstance(typeof(rptRatioOpen));
			}
		}

		protected rptRatioOpen _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptRatioOpen	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intLine;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = intLine > 9;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			intLine = 1;
			//this.Printer.RenderMode = 1;
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			frmPPCostRatios.InstancePtr.Show(App.MainForm);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (intLine <= 9)
			{
				txtType.Text = frmPPCostRatios.InstancePtr.vs.TextMatrix(intLine, 1);
				txtHigh.Text = frmPPCostRatios.InstancePtr.vs.TextMatrix(intLine, 2);
				txtLow.Text = frmPPCostRatios.InstancePtr.vs.TextMatrix(intLine, 3);
				txtExponent.Text = frmPPCostRatios.InstancePtr.vs.TextMatrix(intLine, 4);
				txtLife.Text = frmPPCostRatios.InstancePtr.vs.TextMatrix(intLine, 5);
				txtSD.Text = frmPPCostRatios.InstancePtr.vs.TextMatrix(intLine, 6);
				txtTrend.Text = frmPPCostRatios.InstancePtr.vs.TextMatrix(intLine, 7);
				txtDescription.Text = frmPPCostRatios.InstancePtr.vs.TextMatrix(intLine, 8);
			}
			intLine += 1;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtOpen1.Text = frmPPCostRatios.InstancePtr.txtOpen1.Text;
			txtOpen2.Text = frmPPCostRatios.InstancePtr.txtOpen2.Text;
		}

		private void ReportHeader_Format(object sender, EventArgs e)
		{
			txtRatio.Text = frmPPCostRatios.InstancePtr.txtRatio;
		}

		
	}
}
