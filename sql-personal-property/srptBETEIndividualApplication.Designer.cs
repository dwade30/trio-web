﻿namespace TWPP0000
{
	/// <summary>
	/// Summary description for srptBETEIndividualApplication.
	/// </summary>
	partial class srptBETEIndividualApplication
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptBETEIndividualApplication));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtDescription1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtServiceDate1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAge1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNew1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEstimate1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEquipmentLocation1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTIF1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBETEEligible1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAssessment1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtBusiness = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtOwner = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTypeOfBusiness = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line34 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line35 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtDescription2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtServiceDate2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAge2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNew2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEstimate2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEquipmentLocation2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTIF2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBETEEligible2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAssessment2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line36 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line37 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtDescription3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtServiceDate3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAge3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNew3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEstimate3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEquipmentLocation3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTIF3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBETEEligible3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAssessment3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line38 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line39 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtDescription4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtServiceDate4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAge4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNew4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEstimate4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEquipmentLocation4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTIF4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBETEEligible4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAssessment4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line40 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line41 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtDescription5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtServiceDate5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAge5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNew5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEstimate5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEquipmentLocation5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTIF5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBETEEligible5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAssessment5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line42 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line43 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtDescription6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtServiceDate6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAge6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNew6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEstimate6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEquipmentLocation6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTIF6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBETEEligible6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAssessment6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line44 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line45 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtDescription7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtServiceDate7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAge7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNew7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEstimate7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEquipmentLocation7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTIF7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBETEEligible7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAssessment7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line46 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line47 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtDescription8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtServiceDate8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAge8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNew8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEstimate8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEquipmentLocation8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTIF8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBETEEligible8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAssessment8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line48 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line49 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtDescription9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtServiceDate9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAge9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNew9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEstimate9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEquipmentLocation9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTIF9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBETEEligible9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAssessment9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line50 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line51 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtDescription10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtServiceDate10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAge10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNew10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEstimate10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEquipmentLocation10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTIF10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBETEEligible10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAssessment10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line52 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line53 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtDescription11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtServiceDate11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAge11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNew11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEstimate11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEquipmentLocation11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTIF11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBETEEligible11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAssessment11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line54 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line55 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtDescription12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtServiceDate12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAge12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNew12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEstimate12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEquipmentLocation12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTIF12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBETEEligible12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAssessment12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line56 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line57 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtDescription13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtServiceDate13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAge13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNew13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEstimate13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEquipmentLocation13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTIF13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBETEEligible13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAssessment13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line58 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line59 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtDescription14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtServiceDate14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAge14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNew14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEstimate14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEquipmentLocation14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTIF14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBETEEligible14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAssessment14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line60 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line61 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtDescription15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtServiceDate15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAge15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNew15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEstimate15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEquipmentLocation15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTIF15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBETEEligible15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAssessment15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line62 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line63 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtDescription16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtServiceDate16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAge16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNew16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEstimate16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEquipmentLocation16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTIF16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBETEEligible16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAssessment16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line64 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line65 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtDescription17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtServiceDate17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAge17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNew17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEstimate17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEquipmentLocation17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTIF17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBETEEligible17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAssessment17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line66 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line67 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtDescription18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtServiceDate18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAge18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNew18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEstimate18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEquipmentLocation18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTIF18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBETEEligible18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAssessment18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line68 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line69 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtDescription19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtServiceDate19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAge19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNew19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEstimate19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEquipmentLocation19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTIF19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBETEEligible19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAssessment19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line32 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line33 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line70 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line71 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtDescription20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtServiceDate20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAge20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtNew20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEstimate20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtEquipmentLocation20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTIF20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBETEEligible20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAssessment20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line72 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line73 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line74 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line75 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line76 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line77 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line78 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line79 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line80 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line81 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBusiness)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOwner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTypeOfBusiness)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtDescription1,
            this.txtServiceDate1,
            this.txtAge1,
            this.txtNew1,
            this.txtEstimate1,
            this.txtEquipmentLocation1,
            this.txtTIF1,
            this.txtBETEEligible1,
            this.txtAssessment1,
            this.Label1,
            this.Label2,
            this.Label3,
            this.Label4,
            this.Label5,
            this.Label6,
            this.Label7,
            this.txtBusiness,
            this.txtOwner,
            this.txtAddress,
            this.Label8,
            this.txtTypeOfBusiness,
            this.Label9,
            this.txtAccount,
            this.Line1,
            this.Line2,
            this.Label10,
            this.Label11,
            this.Line3,
            this.Label12,
            this.Label13,
            this.Line4,
            this.Line5,
            this.Line6,
            this.Line7,
            this.Line8,
            this.Line9,
            this.Line10,
            this.Line11,
            this.Line12,
            this.Line13,
            this.Line14,
            this.Line15,
            this.Label14,
            this.Label15,
            this.Label16,
            this.Label17,
            this.Label18,
            this.Line34,
            this.Line35,
            this.txtDescription2,
            this.txtServiceDate2,
            this.txtAge2,
            this.txtNew2,
            this.txtEstimate2,
            this.txtEquipmentLocation2,
            this.txtTIF2,
            this.txtBETEEligible2,
            this.txtAssessment2,
            this.Line36,
            this.Line37,
            this.txtDescription3,
            this.txtServiceDate3,
            this.txtAge3,
            this.txtNew3,
            this.txtEstimate3,
            this.txtEquipmentLocation3,
            this.txtTIF3,
            this.txtBETEEligible3,
            this.txtAssessment3,
            this.Line38,
            this.Line39,
            this.txtDescription4,
            this.txtServiceDate4,
            this.txtAge4,
            this.txtNew4,
            this.txtEstimate4,
            this.txtEquipmentLocation4,
            this.txtTIF4,
            this.txtBETEEligible4,
            this.txtAssessment4,
            this.Line40,
            this.Line41,
            this.txtDescription5,
            this.txtServiceDate5,
            this.txtAge5,
            this.txtNew5,
            this.txtEstimate5,
            this.txtEquipmentLocation5,
            this.txtTIF5,
            this.txtBETEEligible5,
            this.txtAssessment5,
            this.Line42,
            this.Line43,
            this.txtDescription6,
            this.txtServiceDate6,
            this.txtAge6,
            this.txtNew6,
            this.txtEstimate6,
            this.txtEquipmentLocation6,
            this.txtTIF6,
            this.txtBETEEligible6,
            this.txtAssessment6,
            this.Line44,
            this.Line45,
            this.txtDescription7,
            this.txtServiceDate7,
            this.txtAge7,
            this.txtNew7,
            this.txtEstimate7,
            this.txtEquipmentLocation7,
            this.txtTIF7,
            this.txtBETEEligible7,
            this.txtAssessment7,
            this.Line46,
            this.Line47,
            this.txtDescription8,
            this.txtServiceDate8,
            this.txtAge8,
            this.txtNew8,
            this.txtEstimate8,
            this.txtEquipmentLocation8,
            this.txtTIF8,
            this.txtBETEEligible8,
            this.txtAssessment8,
            this.Line48,
            this.Line49,
            this.txtDescription9,
            this.txtServiceDate9,
            this.txtAge9,
            this.txtNew9,
            this.txtEstimate9,
            this.txtEquipmentLocation9,
            this.txtTIF9,
            this.txtBETEEligible9,
            this.txtAssessment9,
            this.Line50,
            this.Line51,
            this.txtDescription10,
            this.txtServiceDate10,
            this.txtAge10,
            this.txtNew10,
            this.txtEstimate10,
            this.txtEquipmentLocation10,
            this.txtTIF10,
            this.txtBETEEligible10,
            this.txtAssessment10,
            this.Line52,
            this.Line53,
            this.txtDescription11,
            this.txtServiceDate11,
            this.txtAge11,
            this.txtNew11,
            this.txtEstimate11,
            this.txtEquipmentLocation11,
            this.txtTIF11,
            this.txtBETEEligible11,
            this.txtAssessment11,
            this.Line54,
            this.Line55,
            this.txtDescription12,
            this.txtServiceDate12,
            this.txtAge12,
            this.txtNew12,
            this.txtEstimate12,
            this.txtEquipmentLocation12,
            this.txtTIF12,
            this.txtBETEEligible12,
            this.txtAssessment12,
            this.Line56,
            this.Line57,
            this.txtDescription13,
            this.txtServiceDate13,
            this.txtAge13,
            this.txtNew13,
            this.txtEstimate13,
            this.txtEquipmentLocation13,
            this.txtTIF13,
            this.txtBETEEligible13,
            this.txtAssessment13,
            this.Line58,
            this.Line59,
            this.txtDescription14,
            this.txtServiceDate14,
            this.txtAge14,
            this.txtNew14,
            this.txtEstimate14,
            this.txtEquipmentLocation14,
            this.txtTIF14,
            this.txtBETEEligible14,
            this.txtAssessment14,
            this.Line60,
            this.Line61,
            this.txtDescription15,
            this.txtServiceDate15,
            this.txtAge15,
            this.txtNew15,
            this.txtEstimate15,
            this.txtEquipmentLocation15,
            this.txtTIF15,
            this.txtBETEEligible15,
            this.txtAssessment15,
            this.Line62,
            this.Line63,
            this.txtDescription16,
            this.txtServiceDate16,
            this.txtAge16,
            this.txtNew16,
            this.txtEstimate16,
            this.txtEquipmentLocation16,
            this.txtTIF16,
            this.txtBETEEligible16,
            this.txtAssessment16,
            this.Line64,
            this.Line65,
            this.txtDescription17,
            this.txtServiceDate17,
            this.txtAge17,
            this.txtNew17,
            this.txtEstimate17,
            this.txtEquipmentLocation17,
            this.txtTIF17,
            this.txtBETEEligible17,
            this.txtAssessment17,
            this.Line66,
            this.Line67,
            this.txtDescription18,
            this.txtServiceDate18,
            this.txtAge18,
            this.txtNew18,
            this.txtEstimate18,
            this.txtEquipmentLocation18,
            this.txtTIF18,
            this.txtBETEEligible18,
            this.txtAssessment18,
            this.Line68,
            this.Line69,
            this.txtDescription19,
            this.txtServiceDate19,
            this.txtAge19,
            this.txtNew19,
            this.txtEstimate19,
            this.txtEquipmentLocation19,
            this.txtTIF19,
            this.txtBETEEligible19,
            this.txtAssessment19,
            this.Line26,
            this.Line27,
            this.Label19,
            this.Label20,
            this.Label21,
            this.Line28,
            this.Line29,
            this.Label22,
            this.Label23,
            this.lblDate,
            this.Line30,
            this.Line31,
            this.Label26,
            this.Line32,
            this.Line33,
            this.Label27,
            this.Line70,
            this.Line71,
            this.txtDescription20,
            this.txtServiceDate20,
            this.txtAge20,
            this.txtNew20,
            this.txtEstimate20,
            this.txtEquipmentLocation20,
            this.txtTIF20,
            this.txtBETEEligible20,
            this.txtAssessment20,
            this.Line72,
            this.Line73,
            this.Line74,
            this.Line75,
            this.Line76,
            this.Line77,
            this.Line78,
            this.Line79,
            this.Line80,
            this.Line81,
            this.Label28,
            this.Label29,
            this.Label30,
            this.lblPage});
            this.Detail.Height = 7.885417F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // txtDescription1
            // 
            this.txtDescription1.Height = 0.1666667F;
            this.txtDescription1.Left = 0.08333334F;
            this.txtDescription1.Name = "txtDescription1";
            this.txtDescription1.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtDescription1.Text = null;
            this.txtDescription1.Top = 2.15625F;
            this.txtDescription1.Width = 3.208333F;
            // 
            // txtServiceDate1
            // 
            this.txtServiceDate1.Height = 0.1666667F;
            this.txtServiceDate1.Left = 3.395833F;
            this.txtServiceDate1.Name = "txtServiceDate1";
            this.txtServiceDate1.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtServiceDate1.Text = null;
            this.txtServiceDate1.Top = 2.15625F;
            this.txtServiceDate1.Width = 0.84375F;
            // 
            // txtAge1
            // 
            this.txtAge1.Height = 0.1666667F;
            this.txtAge1.Left = 4.3125F;
            this.txtAge1.Name = "txtAge1";
            this.txtAge1.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAge1.Text = null;
            this.txtAge1.Top = 2.15625F;
            this.txtAge1.Width = 0.4479167F;
            // 
            // txtNew1
            // 
            this.txtNew1.Height = 0.1666667F;
            this.txtNew1.Left = 4.90625F;
            this.txtNew1.Name = "txtNew1";
            this.txtNew1.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtNew1.Text = null;
            this.txtNew1.Top = 2.15625F;
            this.txtNew1.Width = 0.5625F;
            // 
            // txtEstimate1
            // 
            this.txtEstimate1.Height = 0.1666667F;
            this.txtEstimate1.Left = 5.552083F;
            this.txtEstimate1.Name = "txtEstimate1";
            this.txtEstimate1.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtEstimate1.Text = null;
            this.txtEstimate1.Top = 2.15625F;
            this.txtEstimate1.Width = 0.5833333F;
            // 
            // txtEquipmentLocation1
            // 
            this.txtEquipmentLocation1.Height = 0.1666667F;
            this.txtEquipmentLocation1.Left = 6.291667F;
            this.txtEquipmentLocation1.Name = "txtEquipmentLocation1";
            this.txtEquipmentLocation1.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtEquipmentLocation1.Text = null;
            this.txtEquipmentLocation1.Top = 2.15625F;
            this.txtEquipmentLocation1.Width = 2.15625F;
            // 
            // txtTIF1
            // 
            this.txtTIF1.Height = 0.1666667F;
            this.txtTIF1.Left = 8.8125F;
            this.txtTIF1.Name = "txtTIF1";
            this.txtTIF1.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtTIF1.Text = null;
            this.txtTIF1.Top = 2.15625F;
            this.txtTIF1.Width = 0.2604167F;
            // 
            // txtBETEEligible1
            // 
            this.txtBETEEligible1.Height = 0.1666667F;
            this.txtBETEEligible1.Left = 9.1875F;
            this.txtBETEEligible1.Name = "txtBETEEligible1";
            this.txtBETEEligible1.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtBETEEligible1.Text = null;
            this.txtBETEEligible1.Top = 2.15625F;
            this.txtBETEEligible1.Width = 0.3229167F;
            // 
            // txtAssessment1
            // 
            this.txtAssessment1.Height = 0.1666667F;
            this.txtAssessment1.Left = 9.625F;
            this.txtAssessment1.Name = "txtAssessment1";
            this.txtAssessment1.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAssessment1.Text = null;
            this.txtAssessment1.Top = 2.15625F;
            this.txtAssessment1.Width = 0.5625F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.1666667F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 0.9895833F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-size: 8.5pt; text-align: center";
            this.Label1.Text = "Maine Revenue Services - Property Tax Division";
            this.Label1.Top = 0F;
            this.Label1.Width = 7.979167F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.1666667F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 0.9895833F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "font-size: 9pt; font-weight: bold; text-align: center";
            this.Label2.Text = "Business Equipment Tax Exemption Application";
            this.Label2.Top = 0.1666667F;
            this.Label2.Width = 7.979167F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.1666667F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 0.9895833F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "font-size: 9pt; text-align: center";
            this.Label3.Text = "(Title 36 § 691 - 700B)";
            this.Label3.Top = 0.3333333F;
            this.Label3.Width = 7.979167F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.1666667F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 0.9895833F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-size: 8.5pt; text-align: center";
            this.Label4.Text = "This application must be filed every year with the municipal assessor no later th" +
    "an May 1st";
            this.Label4.Top = 0.5F;
            this.Label4.Width = 7.979167F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.1666667F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 0.2395833F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-size: 8.5pt; font-weight: bold";
            this.Label5.Text = "Name of Business:";
            this.Label5.Top = 0.7395833F;
            this.Label5.Width = 1.354167F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.1666667F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 0.2395833F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-size: 8.5pt; font-weight: bold";
            this.Label6.Text = "Business Address:";
            this.Label6.Top = 0.90625F;
            this.Label6.Width = 1.354167F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.1666667F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 0.2395833F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-size: 8.5pt; font-weight: bold";
            this.Label7.Text = "Name of Owner:";
            this.Label7.Top = 1.072917F;
            this.Label7.Width = 1.354167F;
            // 
            // txtBusiness
            // 
            this.txtBusiness.Height = 0.1666667F;
            this.txtBusiness.Left = 1.40625F;
            this.txtBusiness.Name = "txtBusiness";
            this.txtBusiness.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtBusiness.Text = null;
            this.txtBusiness.Top = 0.7395833F;
            this.txtBusiness.Width = 3.8125F;
            // 
            // txtOwner
            // 
            this.txtOwner.Height = 0.1666667F;
            this.txtOwner.Left = 1.40625F;
            this.txtOwner.Name = "txtOwner";
            this.txtOwner.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtOwner.Text = null;
            this.txtOwner.Top = 1.072917F;
            this.txtOwner.Width = 3.8125F;
            // 
            // txtAddress
            // 
            this.txtAddress.Height = 0.1666667F;
            this.txtAddress.Left = 1.40625F;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtAddress.Text = null;
            this.txtAddress.Top = 0.90625F;
            this.txtAddress.Width = 3.8125F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.1666667F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 5.427083F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-size: 8.5pt; font-weight: bold";
            this.Label8.Text = "Type of Business";
            this.Label8.Top = 0.7395833F;
            this.Label8.Width = 1.354167F;
            // 
            // txtTypeOfBusiness
            // 
            this.txtTypeOfBusiness.Height = 0.1666667F;
            this.txtTypeOfBusiness.Left = 6.84375F;
            this.txtTypeOfBusiness.Name = "txtTypeOfBusiness";
            this.txtTypeOfBusiness.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtTypeOfBusiness.Text = null;
            this.txtTypeOfBusiness.Top = 0.7395833F;
            this.txtTypeOfBusiness.Width = 2.375F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.1666667F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 5.427083F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-size: 8.5pt; font-weight: bold";
            this.Label9.Text = "Account #";
            this.Label9.Top = 0.90625F;
            this.Label9.Width = 1.354167F;
            // 
            // txtAccount
            // 
            this.txtAccount.Height = 0.1875F;
            this.txtAccount.Left = 6.84375F;
            this.txtAccount.Name = "txtAccount";
            this.txtAccount.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtAccount.Text = null;
            this.txtAccount.Top = 0.90625F;
            this.txtAccount.Width = 2.375F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 2F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 1.510417F;
            this.Line1.Width = 8.5F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 8.5F;
            this.Line1.Y1 = 1.510417F;
            this.Line1.Y2 = 1.510417F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0F;
            this.Line2.LineWeight = 2F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 2.125F;
            this.Line2.Width = 8.5F;
            this.Line2.X1 = 0F;
            this.Line2.X2 = 8.5F;
            this.Line2.Y1 = 2.125F;
            this.Line2.Y2 = 2.125F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.19F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 0.5625F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "font-size: 7pt; font-weight: bold; text-align: center";
            this.Label10.Text = "Description of the Exempt Equipment";
            this.Label10.Top = 1.53125F;
            this.Label10.Width = 2.291667F;
            // 
            // Label11
            // 
            this.Label11.Height = 0.19F;
            this.Label11.HyperLink = null;
            this.Label11.Left = 0.5625F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "font-size: 7pt; font-weight: bold; text-align: center";
            this.Label11.Text = "(please be specific)";
            this.Label11.Top = 1.666667F;
            this.Label11.Width = 2.291667F;
            // 
            // Line3
            // 
            this.Line3.Height = 4.583333F;
            this.Line3.Left = 3.333333F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 2.125F;
            this.Line3.Width = 0F;
            this.Line3.X1 = 3.333333F;
            this.Line3.X2 = 3.333333F;
            this.Line3.Y1 = 2.125F;
            this.Line3.Y2 = 6.708333F;
            // 
            // Label12
            // 
            this.Label12.Height = 0.40625F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 3.375F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "font-size: 7pt; font-weight: bold; text-align: center";
            this.Label12.Text = "Purchased / Placed in Service in Maine";
            this.Label12.Top = 1.53125F;
            this.Label12.Width = 0.8541667F;
            // 
            // Label13
            // 
            this.Label13.Height = 0.19F;
            this.Label13.HyperLink = null;
            this.Label13.Left = 3.375F;
            this.Label13.Name = "Label13";
            this.Label13.Style = "font-size: 7pt; font-weight: bold; text-align: center";
            this.Label13.Text = "(Month / Year)";
            this.Label13.Top = 1.9375F;
            this.Label13.Width = 0.8541667F;
            // 
            // Line4
            // 
            this.Line4.Height = 4.583333F;
            this.Line4.Left = 4.25F;
            this.Line4.LineWeight = 1F;
            this.Line4.Name = "Line4";
            this.Line4.Top = 2.125F;
            this.Line4.Width = 0F;
            this.Line4.X1 = 4.25F;
            this.Line4.X2 = 4.25F;
            this.Line4.Y1 = 2.125F;
            this.Line4.Y2 = 6.708333F;
            // 
            // Line5
            // 
            this.Line5.Height = 4.583333F;
            this.Line5.Left = 4.833333F;
            this.Line5.LineWeight = 1F;
            this.Line5.Name = "Line5";
            this.Line5.Top = 2.125F;
            this.Line5.Width = 0F;
            this.Line5.X1 = 4.833333F;
            this.Line5.X2 = 4.833333F;
            this.Line5.Y1 = 2.125F;
            this.Line5.Y2 = 6.708333F;
            // 
            // Line6
            // 
            this.Line6.Height = 4.583333F;
            this.Line6.Left = 5.5F;
            this.Line6.LineWeight = 1F;
            this.Line6.Name = "Line6";
            this.Line6.Top = 2.125F;
            this.Line6.Width = 0F;
            this.Line6.X1 = 5.5F;
            this.Line6.X2 = 5.5F;
            this.Line6.Y1 = 2.125F;
            this.Line6.Y2 = 6.708333F;
            // 
            // Line7
            // 
            this.Line7.Height = 4.583333F;
            this.Line7.Left = 6.166667F;
            this.Line7.LineWeight = 1F;
            this.Line7.Name = "Line7";
            this.Line7.Top = 2.125F;
            this.Line7.Width = 0F;
            this.Line7.X1 = 6.166667F;
            this.Line7.X2 = 6.166667F;
            this.Line7.Y1 = 2.125F;
            this.Line7.Y2 = 6.708333F;
            // 
            // Line8
            // 
            this.Line8.Height = 4.583333F;
            this.Line8.Left = 8.5F;
            this.Line8.LineWeight = 1F;
            this.Line8.Name = "Line8";
            this.Line8.Top = 2.125F;
            this.Line8.Width = 0F;
            this.Line8.X1 = 8.5F;
            this.Line8.X2 = 8.5F;
            this.Line8.Y1 = 2.125F;
            this.Line8.Y2 = 6.708333F;
            // 
            // Line9
            // 
            this.Line9.Height = 4.583333F;
            this.Line9.Left = 8.75F;
            this.Line9.LineWeight = 1F;
            this.Line9.Name = "Line9";
            this.Line9.Top = 2.125F;
            this.Line9.Width = 0F;
            this.Line9.X1 = 8.75F;
            this.Line9.X2 = 8.75F;
            this.Line9.Y1 = 2.125F;
            this.Line9.Y2 = 6.708333F;
            // 
            // Line10
            // 
            this.Line10.Height = 0F;
            this.Line10.Left = 8.75F;
            this.Line10.LineWeight = 2F;
            this.Line10.Name = "Line10";
            this.Line10.Top = 1.510417F;
            this.Line10.Width = 1.48958F;
            this.Line10.X1 = 8.75F;
            this.Line10.X2 = 10.23958F;
            this.Line10.Y1 = 1.510417F;
            this.Line10.Y2 = 1.510417F;
            // 
            // Line11
            // 
            this.Line11.Height = 4.583333F;
            this.Line11.Left = 10.23958F;
            this.Line11.LineWeight = 1F;
            this.Line11.Name = "Line11";
            this.Line11.Top = 2.125F;
            this.Line11.Width = 0F;
            this.Line11.X1 = 10.23958F;
            this.Line11.X2 = 10.23958F;
            this.Line11.Y1 = 2.125F;
            this.Line11.Y2 = 6.708333F;
            // 
            // Line12
            // 
            this.Line12.Height = 0F;
            this.Line12.Left = 8.75F;
            this.Line12.LineWeight = 2F;
            this.Line12.Name = "Line12";
            this.Line12.Top = 1.229167F;
            this.Line12.Width = 1.48958F;
            this.Line12.X1 = 8.75F;
            this.Line12.X2 = 10.23958F;
            this.Line12.Y1 = 1.229167F;
            this.Line12.Y2 = 1.229167F;
            // 
            // Line13
            // 
            this.Line13.Height = 0F;
            this.Line13.Left = 8.75F;
            this.Line13.LineWeight = 2F;
            this.Line13.Name = "Line13";
            this.Line13.Top = 2.125F;
            this.Line13.Width = 1.48958F;
            this.Line13.X1 = 8.75F;
            this.Line13.X2 = 10.23958F;
            this.Line13.Y1 = 2.125F;
            this.Line13.Y2 = 2.125F;
            // 
            // Line14
            // 
            this.Line14.Height = 4.583333F;
            this.Line14.Left = 9.5625F;
            this.Line14.LineWeight = 1F;
            this.Line14.Name = "Line14";
            this.Line14.Top = 2.125F;
            this.Line14.Width = 0F;
            this.Line14.X1 = 9.5625F;
            this.Line14.X2 = 9.5625F;
            this.Line14.Y1 = 2.125F;
            this.Line14.Y2 = 6.708333F;
            // 
            // Line15
            // 
            this.Line15.Height = 4.583333F;
            this.Line15.Left = 9.125F;
            this.Line15.LineWeight = 1F;
            this.Line15.Name = "Line15";
            this.Line15.Top = 2.125F;
            this.Line15.Width = 0F;
            this.Line15.X1 = 9.125F;
            this.Line15.X2 = 9.125F;
            this.Line15.Y1 = 2.125F;
            this.Line15.Y2 = 6.708333F;
            // 
            // Label14
            // 
            this.Label14.Height = 0.28125F;
            this.Label14.HyperLink = null;
            this.Label14.Left = 8.8125F;
            this.Label14.Name = "Label14";
            this.Label14.Style = "font-size: 7pt; font-weight: bold; text-align: center";
            this.Label14.Text = "To be Completed by the Assessor";
            this.Label14.Top = 1.25F;
            this.Label14.Width = 1.416667F;
            // 
            // Label15
            // 
            this.Label15.Height = 0.40625F;
            this.Label15.HyperLink = null;
            this.Label15.Left = 4.3125F;
            this.Label15.Name = "Label15";
            this.Label15.Style = "font-size: 7pt; font-weight: bold; text-align: center";
            this.Label15.Text = "Current Age";
            this.Label15.Top = 1.53125F;
            this.Label15.Width = 0.4791667F;
            // 
            // Label16
            // 
            this.Label16.Height = 0.53125F;
            this.Label16.HyperLink = null;
            this.Label16.Left = 5F;
            this.Label16.Name = "Label16";
            this.Label16.Style = "font-size: 7pt; font-weight: bold; text-align: center";
            this.Label16.Text = "Cost or Value New";
            this.Label16.Top = 1.53125F;
            this.Label16.Width = 0.2916667F;
            // 
            // Label17
            // 
            this.Label17.Height = 0.4557501F;
            this.Label17.HyperLink = null;
            this.Label17.Left = 5.614583F;
            this.Label17.Name = "Label17";
            this.Label17.Style = "font-size: 7pt; font-weight: bold; text-align: center";
            this.Label17.Text = "Estimate of Current Value";
            this.Label17.Top = 1.53125F;
            this.Label17.Width = 0.4791667F;
            // 
            // Label18
            // 
            this.Label18.Height = 0.40625F;
            this.Label18.HyperLink = null;
            this.Label18.Left = 6.5625F;
            this.Label18.Name = "Label18";
            this.Label18.Style = "font-size: 7pt; font-weight: bold; text-align: center";
            this.Label18.Text = "Physical Address Of Equipment (Leased Equipment Only)";
            this.Label18.Top = 1.53125F;
            this.Label18.Width = 1.541667F;
            // 
            // Line34
            // 
            this.Line34.Height = 0F;
            this.Line34.Left = 0F;
            this.Line34.LineWeight = 1F;
            this.Line34.Name = "Line34";
            this.Line34.Top = 2.583333F;
            this.Line34.Width = 8.5F;
            this.Line34.X1 = 0F;
            this.Line34.X2 = 8.5F;
            this.Line34.Y1 = 2.583333F;
            this.Line34.Y2 = 2.583333F;
            // 
            // Line35
            // 
            this.Line35.Height = 0F;
            this.Line35.Left = 8.75F;
            this.Line35.LineWeight = 1F;
            this.Line35.Name = "Line35";
            this.Line35.Top = 2.583333F;
            this.Line35.Width = 1.48958F;
            this.Line35.X1 = 8.75F;
            this.Line35.X2 = 10.23958F;
            this.Line35.Y1 = 2.583333F;
            this.Line35.Y2 = 2.583333F;
            // 
            // txtDescription2
            // 
            this.txtDescription2.Height = 0.1666667F;
            this.txtDescription2.Left = 0.08333334F;
            this.txtDescription2.Name = "txtDescription2";
            this.txtDescription2.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtDescription2.Text = null;
            this.txtDescription2.Top = 2.385417F;
            this.txtDescription2.Width = 3.208333F;
            // 
            // txtServiceDate2
            // 
            this.txtServiceDate2.Height = 0.1666667F;
            this.txtServiceDate2.Left = 3.395833F;
            this.txtServiceDate2.Name = "txtServiceDate2";
            this.txtServiceDate2.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtServiceDate2.Text = null;
            this.txtServiceDate2.Top = 2.385417F;
            this.txtServiceDate2.Width = 0.84375F;
            // 
            // txtAge2
            // 
            this.txtAge2.Height = 0.1666667F;
            this.txtAge2.Left = 4.3125F;
            this.txtAge2.Name = "txtAge2";
            this.txtAge2.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAge2.Text = null;
            this.txtAge2.Top = 2.385417F;
            this.txtAge2.Width = 0.4479167F;
            // 
            // txtNew2
            // 
            this.txtNew2.Height = 0.1666667F;
            this.txtNew2.Left = 4.90625F;
            this.txtNew2.Name = "txtNew2";
            this.txtNew2.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtNew2.Text = null;
            this.txtNew2.Top = 2.385417F;
            this.txtNew2.Width = 0.5625F;
            // 
            // txtEstimate2
            // 
            this.txtEstimate2.Height = 0.1666667F;
            this.txtEstimate2.Left = 5.552083F;
            this.txtEstimate2.Name = "txtEstimate2";
            this.txtEstimate2.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtEstimate2.Text = null;
            this.txtEstimate2.Top = 2.385417F;
            this.txtEstimate2.Width = 0.5833333F;
            // 
            // txtEquipmentLocation2
            // 
            this.txtEquipmentLocation2.Height = 0.1666667F;
            this.txtEquipmentLocation2.Left = 6.291667F;
            this.txtEquipmentLocation2.Name = "txtEquipmentLocation2";
            this.txtEquipmentLocation2.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtEquipmentLocation2.Text = null;
            this.txtEquipmentLocation2.Top = 2.385417F;
            this.txtEquipmentLocation2.Width = 2.15625F;
            // 
            // txtTIF2
            // 
            this.txtTIF2.Height = 0.1666667F;
            this.txtTIF2.Left = 8.8125F;
            this.txtTIF2.Name = "txtTIF2";
            this.txtTIF2.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtTIF2.Text = null;
            this.txtTIF2.Top = 2.385417F;
            this.txtTIF2.Width = 0.2604167F;
            // 
            // txtBETEEligible2
            // 
            this.txtBETEEligible2.Height = 0.1666667F;
            this.txtBETEEligible2.Left = 9.1875F;
            this.txtBETEEligible2.Name = "txtBETEEligible2";
            this.txtBETEEligible2.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtBETEEligible2.Text = null;
            this.txtBETEEligible2.Top = 2.385417F;
            this.txtBETEEligible2.Width = 0.3229167F;
            // 
            // txtAssessment2
            // 
            this.txtAssessment2.Height = 0.1666667F;
            this.txtAssessment2.Left = 9.625F;
            this.txtAssessment2.Name = "txtAssessment2";
            this.txtAssessment2.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAssessment2.Text = null;
            this.txtAssessment2.Top = 2.385417F;
            this.txtAssessment2.Width = 0.5625F;
            // 
            // Line36
            // 
            this.Line36.Height = 0F;
            this.Line36.Left = 0F;
            this.Line36.LineWeight = 1F;
            this.Line36.Name = "Line36";
            this.Line36.Top = 2.8125F;
            this.Line36.Width = 8.5F;
            this.Line36.X1 = 0F;
            this.Line36.X2 = 8.5F;
            this.Line36.Y1 = 2.8125F;
            this.Line36.Y2 = 2.8125F;
            // 
            // Line37
            // 
            this.Line37.Height = 0F;
            this.Line37.Left = 8.75F;
            this.Line37.LineWeight = 1F;
            this.Line37.Name = "Line37";
            this.Line37.Top = 2.8125F;
            this.Line37.Width = 1.48958F;
            this.Line37.X1 = 8.75F;
            this.Line37.X2 = 10.23958F;
            this.Line37.Y1 = 2.8125F;
            this.Line37.Y2 = 2.8125F;
            // 
            // txtDescription3
            // 
            this.txtDescription3.Height = 0.1666667F;
            this.txtDescription3.Left = 0.08333334F;
            this.txtDescription3.Name = "txtDescription3";
            this.txtDescription3.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtDescription3.Text = null;
            this.txtDescription3.Top = 2.614583F;
            this.txtDescription3.Width = 3.208333F;
            // 
            // txtServiceDate3
            // 
            this.txtServiceDate3.Height = 0.1666667F;
            this.txtServiceDate3.Left = 3.395833F;
            this.txtServiceDate3.Name = "txtServiceDate3";
            this.txtServiceDate3.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtServiceDate3.Text = null;
            this.txtServiceDate3.Top = 2.614583F;
            this.txtServiceDate3.Width = 0.84375F;
            // 
            // txtAge3
            // 
            this.txtAge3.Height = 0.1666667F;
            this.txtAge3.Left = 4.3125F;
            this.txtAge3.Name = "txtAge3";
            this.txtAge3.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAge3.Text = null;
            this.txtAge3.Top = 2.614583F;
            this.txtAge3.Width = 0.4479167F;
            // 
            // txtNew3
            // 
            this.txtNew3.Height = 0.1666667F;
            this.txtNew3.Left = 4.90625F;
            this.txtNew3.Name = "txtNew3";
            this.txtNew3.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtNew3.Text = null;
            this.txtNew3.Top = 2.614583F;
            this.txtNew3.Width = 0.5625F;
            // 
            // txtEstimate3
            // 
            this.txtEstimate3.Height = 0.1666667F;
            this.txtEstimate3.Left = 5.552083F;
            this.txtEstimate3.Name = "txtEstimate3";
            this.txtEstimate3.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtEstimate3.Text = null;
            this.txtEstimate3.Top = 2.614583F;
            this.txtEstimate3.Width = 0.5833333F;
            // 
            // txtEquipmentLocation3
            // 
            this.txtEquipmentLocation3.Height = 0.1666667F;
            this.txtEquipmentLocation3.Left = 6.291667F;
            this.txtEquipmentLocation3.Name = "txtEquipmentLocation3";
            this.txtEquipmentLocation3.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtEquipmentLocation3.Text = null;
            this.txtEquipmentLocation3.Top = 2.614583F;
            this.txtEquipmentLocation3.Width = 2.15625F;
            // 
            // txtTIF3
            // 
            this.txtTIF3.Height = 0.1666667F;
            this.txtTIF3.Left = 8.8125F;
            this.txtTIF3.Name = "txtTIF3";
            this.txtTIF3.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtTIF3.Text = null;
            this.txtTIF3.Top = 2.614583F;
            this.txtTIF3.Width = 0.2604167F;
            // 
            // txtBETEEligible3
            // 
            this.txtBETEEligible3.Height = 0.1666667F;
            this.txtBETEEligible3.Left = 9.1875F;
            this.txtBETEEligible3.Name = "txtBETEEligible3";
            this.txtBETEEligible3.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtBETEEligible3.Text = null;
            this.txtBETEEligible3.Top = 2.614583F;
            this.txtBETEEligible3.Width = 0.3229167F;
            // 
            // txtAssessment3
            // 
            this.txtAssessment3.Height = 0.1666667F;
            this.txtAssessment3.Left = 9.625F;
            this.txtAssessment3.Name = "txtAssessment3";
            this.txtAssessment3.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAssessment3.Text = null;
            this.txtAssessment3.Top = 2.614583F;
            this.txtAssessment3.Width = 0.5625F;
            // 
            // Line38
            // 
            this.Line38.Height = 0F;
            this.Line38.Left = 0F;
            this.Line38.LineWeight = 1F;
            this.Line38.Name = "Line38";
            this.Line38.Top = 3.041667F;
            this.Line38.Width = 8.5F;
            this.Line38.X1 = 0F;
            this.Line38.X2 = 8.5F;
            this.Line38.Y1 = 3.041667F;
            this.Line38.Y2 = 3.041667F;
            // 
            // Line39
            // 
            this.Line39.Height = 0F;
            this.Line39.Left = 8.75F;
            this.Line39.LineWeight = 1F;
            this.Line39.Name = "Line39";
            this.Line39.Top = 3.041667F;
            this.Line39.Width = 1.48958F;
            this.Line39.X1 = 8.75F;
            this.Line39.X2 = 10.23958F;
            this.Line39.Y1 = 3.041667F;
            this.Line39.Y2 = 3.041667F;
            // 
            // txtDescription4
            // 
            this.txtDescription4.Height = 0.1666667F;
            this.txtDescription4.Left = 0.08333334F;
            this.txtDescription4.Name = "txtDescription4";
            this.txtDescription4.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtDescription4.Text = null;
            this.txtDescription4.Top = 2.84375F;
            this.txtDescription4.Width = 3.208333F;
            // 
            // txtServiceDate4
            // 
            this.txtServiceDate4.Height = 0.1666667F;
            this.txtServiceDate4.Left = 3.395833F;
            this.txtServiceDate4.Name = "txtServiceDate4";
            this.txtServiceDate4.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtServiceDate4.Text = null;
            this.txtServiceDate4.Top = 2.84375F;
            this.txtServiceDate4.Width = 0.84375F;
            // 
            // txtAge4
            // 
            this.txtAge4.Height = 0.1666667F;
            this.txtAge4.Left = 4.3125F;
            this.txtAge4.Name = "txtAge4";
            this.txtAge4.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAge4.Text = null;
            this.txtAge4.Top = 2.84375F;
            this.txtAge4.Width = 0.4479167F;
            // 
            // txtNew4
            // 
            this.txtNew4.Height = 0.1666667F;
            this.txtNew4.Left = 4.90625F;
            this.txtNew4.Name = "txtNew4";
            this.txtNew4.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtNew4.Text = null;
            this.txtNew4.Top = 2.84375F;
            this.txtNew4.Width = 0.5625F;
            // 
            // txtEstimate4
            // 
            this.txtEstimate4.Height = 0.1666667F;
            this.txtEstimate4.Left = 5.552083F;
            this.txtEstimate4.Name = "txtEstimate4";
            this.txtEstimate4.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtEstimate4.Text = null;
            this.txtEstimate4.Top = 2.84375F;
            this.txtEstimate4.Width = 0.5833333F;
            // 
            // txtEquipmentLocation4
            // 
            this.txtEquipmentLocation4.Height = 0.1666667F;
            this.txtEquipmentLocation4.Left = 6.291667F;
            this.txtEquipmentLocation4.Name = "txtEquipmentLocation4";
            this.txtEquipmentLocation4.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtEquipmentLocation4.Text = null;
            this.txtEquipmentLocation4.Top = 2.84375F;
            this.txtEquipmentLocation4.Width = 2.15625F;
            // 
            // txtTIF4
            // 
            this.txtTIF4.Height = 0.1666667F;
            this.txtTIF4.Left = 8.8125F;
            this.txtTIF4.Name = "txtTIF4";
            this.txtTIF4.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtTIF4.Text = null;
            this.txtTIF4.Top = 2.84375F;
            this.txtTIF4.Width = 0.2604167F;
            // 
            // txtBETEEligible4
            // 
            this.txtBETEEligible4.Height = 0.1666667F;
            this.txtBETEEligible4.Left = 9.1875F;
            this.txtBETEEligible4.Name = "txtBETEEligible4";
            this.txtBETEEligible4.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtBETEEligible4.Text = null;
            this.txtBETEEligible4.Top = 2.84375F;
            this.txtBETEEligible4.Width = 0.3229167F;
            // 
            // txtAssessment4
            // 
            this.txtAssessment4.Height = 0.1666667F;
            this.txtAssessment4.Left = 9.625F;
            this.txtAssessment4.Name = "txtAssessment4";
            this.txtAssessment4.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAssessment4.Text = null;
            this.txtAssessment4.Top = 2.84375F;
            this.txtAssessment4.Width = 0.5625F;
            // 
            // Line40
            // 
            this.Line40.Height = 0F;
            this.Line40.Left = 0F;
            this.Line40.LineWeight = 1F;
            this.Line40.Name = "Line40";
            this.Line40.Top = 3.270833F;
            this.Line40.Width = 8.5F;
            this.Line40.X1 = 0F;
            this.Line40.X2 = 8.5F;
            this.Line40.Y1 = 3.270833F;
            this.Line40.Y2 = 3.270833F;
            // 
            // Line41
            // 
            this.Line41.Height = 0F;
            this.Line41.Left = 8.75F;
            this.Line41.LineWeight = 1F;
            this.Line41.Name = "Line41";
            this.Line41.Top = 3.270833F;
            this.Line41.Width = 1.48958F;
            this.Line41.X1 = 8.75F;
            this.Line41.X2 = 10.23958F;
            this.Line41.Y1 = 3.270833F;
            this.Line41.Y2 = 3.270833F;
            // 
            // txtDescription5
            // 
            this.txtDescription5.Height = 0.1666667F;
            this.txtDescription5.Left = 0.08333334F;
            this.txtDescription5.Name = "txtDescription5";
            this.txtDescription5.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtDescription5.Text = null;
            this.txtDescription5.Top = 3.072917F;
            this.txtDescription5.Width = 3.208333F;
            // 
            // txtServiceDate5
            // 
            this.txtServiceDate5.Height = 0.1666667F;
            this.txtServiceDate5.Left = 3.395833F;
            this.txtServiceDate5.Name = "txtServiceDate5";
            this.txtServiceDate5.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtServiceDate5.Text = null;
            this.txtServiceDate5.Top = 3.072917F;
            this.txtServiceDate5.Width = 0.84375F;
            // 
            // txtAge5
            // 
            this.txtAge5.Height = 0.1666667F;
            this.txtAge5.Left = 4.3125F;
            this.txtAge5.Name = "txtAge5";
            this.txtAge5.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAge5.Text = null;
            this.txtAge5.Top = 3.072917F;
            this.txtAge5.Width = 0.4479167F;
            // 
            // txtNew5
            // 
            this.txtNew5.Height = 0.1666667F;
            this.txtNew5.Left = 4.90625F;
            this.txtNew5.Name = "txtNew5";
            this.txtNew5.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtNew5.Text = null;
            this.txtNew5.Top = 3.072917F;
            this.txtNew5.Width = 0.5625F;
            // 
            // txtEstimate5
            // 
            this.txtEstimate5.Height = 0.1666667F;
            this.txtEstimate5.Left = 5.552083F;
            this.txtEstimate5.Name = "txtEstimate5";
            this.txtEstimate5.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtEstimate5.Text = null;
            this.txtEstimate5.Top = 3.072917F;
            this.txtEstimate5.Width = 0.5833333F;
            // 
            // txtEquipmentLocation5
            // 
            this.txtEquipmentLocation5.Height = 0.1666667F;
            this.txtEquipmentLocation5.Left = 6.291667F;
            this.txtEquipmentLocation5.Name = "txtEquipmentLocation5";
            this.txtEquipmentLocation5.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtEquipmentLocation5.Text = null;
            this.txtEquipmentLocation5.Top = 3.072917F;
            this.txtEquipmentLocation5.Width = 2.15625F;
            // 
            // txtTIF5
            // 
            this.txtTIF5.Height = 0.1666667F;
            this.txtTIF5.Left = 8.8125F;
            this.txtTIF5.Name = "txtTIF5";
            this.txtTIF5.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtTIF5.Text = null;
            this.txtTIF5.Top = 3.072917F;
            this.txtTIF5.Width = 0.2604167F;
            // 
            // txtBETEEligible5
            // 
            this.txtBETEEligible5.Height = 0.1666667F;
            this.txtBETEEligible5.Left = 9.1875F;
            this.txtBETEEligible5.Name = "txtBETEEligible5";
            this.txtBETEEligible5.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtBETEEligible5.Text = null;
            this.txtBETEEligible5.Top = 3.072917F;
            this.txtBETEEligible5.Width = 0.3229167F;
            // 
            // txtAssessment5
            // 
            this.txtAssessment5.Height = 0.1666667F;
            this.txtAssessment5.Left = 9.625F;
            this.txtAssessment5.Name = "txtAssessment5";
            this.txtAssessment5.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAssessment5.Text = null;
            this.txtAssessment5.Top = 3.072917F;
            this.txtAssessment5.Width = 0.5625F;
            // 
            // Line42
            // 
            this.Line42.Height = 0F;
            this.Line42.Left = 0F;
            this.Line42.LineWeight = 1F;
            this.Line42.Name = "Line42";
            this.Line42.Top = 3.5F;
            this.Line42.Width = 8.5F;
            this.Line42.X1 = 0F;
            this.Line42.X2 = 8.5F;
            this.Line42.Y1 = 3.5F;
            this.Line42.Y2 = 3.5F;
            // 
            // Line43
            // 
            this.Line43.Height = 0F;
            this.Line43.Left = 8.75F;
            this.Line43.LineWeight = 1F;
            this.Line43.Name = "Line43";
            this.Line43.Top = 3.5F;
            this.Line43.Width = 1.48958F;
            this.Line43.X1 = 8.75F;
            this.Line43.X2 = 10.23958F;
            this.Line43.Y1 = 3.5F;
            this.Line43.Y2 = 3.5F;
            // 
            // txtDescription6
            // 
            this.txtDescription6.Height = 0.1666667F;
            this.txtDescription6.Left = 0.08333334F;
            this.txtDescription6.Name = "txtDescription6";
            this.txtDescription6.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtDescription6.Text = null;
            this.txtDescription6.Top = 3.302083F;
            this.txtDescription6.Width = 3.208333F;
            // 
            // txtServiceDate6
            // 
            this.txtServiceDate6.Height = 0.1666667F;
            this.txtServiceDate6.Left = 3.395833F;
            this.txtServiceDate6.Name = "txtServiceDate6";
            this.txtServiceDate6.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtServiceDate6.Text = null;
            this.txtServiceDate6.Top = 3.302083F;
            this.txtServiceDate6.Width = 0.84375F;
            // 
            // txtAge6
            // 
            this.txtAge6.Height = 0.1666667F;
            this.txtAge6.Left = 4.3125F;
            this.txtAge6.Name = "txtAge6";
            this.txtAge6.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAge6.Text = null;
            this.txtAge6.Top = 3.302083F;
            this.txtAge6.Width = 0.4479167F;
            // 
            // txtNew6
            // 
            this.txtNew6.Height = 0.1666667F;
            this.txtNew6.Left = 4.90625F;
            this.txtNew6.Name = "txtNew6";
            this.txtNew6.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtNew6.Text = null;
            this.txtNew6.Top = 3.302083F;
            this.txtNew6.Width = 0.5625F;
            // 
            // txtEstimate6
            // 
            this.txtEstimate6.Height = 0.1666667F;
            this.txtEstimate6.Left = 5.552083F;
            this.txtEstimate6.Name = "txtEstimate6";
            this.txtEstimate6.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtEstimate6.Text = null;
            this.txtEstimate6.Top = 3.302083F;
            this.txtEstimate6.Width = 0.5833333F;
            // 
            // txtEquipmentLocation6
            // 
            this.txtEquipmentLocation6.Height = 0.1666667F;
            this.txtEquipmentLocation6.Left = 6.291667F;
            this.txtEquipmentLocation6.Name = "txtEquipmentLocation6";
            this.txtEquipmentLocation6.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtEquipmentLocation6.Text = null;
            this.txtEquipmentLocation6.Top = 3.302083F;
            this.txtEquipmentLocation6.Width = 2.15625F;
            // 
            // txtTIF6
            // 
            this.txtTIF6.Height = 0.1666667F;
            this.txtTIF6.Left = 8.8125F;
            this.txtTIF6.Name = "txtTIF6";
            this.txtTIF6.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtTIF6.Text = null;
            this.txtTIF6.Top = 3.302083F;
            this.txtTIF6.Width = 0.2604167F;
            // 
            // txtBETEEligible6
            // 
            this.txtBETEEligible6.Height = 0.1666667F;
            this.txtBETEEligible6.Left = 9.1875F;
            this.txtBETEEligible6.Name = "txtBETEEligible6";
            this.txtBETEEligible6.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtBETEEligible6.Text = null;
            this.txtBETEEligible6.Top = 3.302083F;
            this.txtBETEEligible6.Width = 0.3229167F;
            // 
            // txtAssessment6
            // 
            this.txtAssessment6.Height = 0.1666667F;
            this.txtAssessment6.Left = 9.625F;
            this.txtAssessment6.Name = "txtAssessment6";
            this.txtAssessment6.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAssessment6.Text = null;
            this.txtAssessment6.Top = 3.302083F;
            this.txtAssessment6.Width = 0.5625F;
            // 
            // Line44
            // 
            this.Line44.Height = 0F;
            this.Line44.Left = 0F;
            this.Line44.LineWeight = 1F;
            this.Line44.Name = "Line44";
            this.Line44.Top = 3.729167F;
            this.Line44.Width = 8.5F;
            this.Line44.X1 = 0F;
            this.Line44.X2 = 8.5F;
            this.Line44.Y1 = 3.729167F;
            this.Line44.Y2 = 3.729167F;
            // 
            // Line45
            // 
            this.Line45.Height = 0F;
            this.Line45.Left = 8.75F;
            this.Line45.LineWeight = 1F;
            this.Line45.Name = "Line45";
            this.Line45.Top = 3.729167F;
            this.Line45.Width = 1.48958F;
            this.Line45.X1 = 8.75F;
            this.Line45.X2 = 10.23958F;
            this.Line45.Y1 = 3.729167F;
            this.Line45.Y2 = 3.729167F;
            // 
            // txtDescription7
            // 
            this.txtDescription7.Height = 0.1666667F;
            this.txtDescription7.Left = 0.08333334F;
            this.txtDescription7.Name = "txtDescription7";
            this.txtDescription7.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtDescription7.Text = null;
            this.txtDescription7.Top = 3.53125F;
            this.txtDescription7.Width = 3.208333F;
            // 
            // txtServiceDate7
            // 
            this.txtServiceDate7.Height = 0.1666667F;
            this.txtServiceDate7.Left = 3.395833F;
            this.txtServiceDate7.Name = "txtServiceDate7";
            this.txtServiceDate7.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtServiceDate7.Text = null;
            this.txtServiceDate7.Top = 3.53125F;
            this.txtServiceDate7.Width = 0.84375F;
            // 
            // txtAge7
            // 
            this.txtAge7.Height = 0.1666667F;
            this.txtAge7.Left = 4.3125F;
            this.txtAge7.Name = "txtAge7";
            this.txtAge7.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAge7.Text = null;
            this.txtAge7.Top = 3.53125F;
            this.txtAge7.Width = 0.4479167F;
            // 
            // txtNew7
            // 
            this.txtNew7.Height = 0.1666667F;
            this.txtNew7.Left = 4.90625F;
            this.txtNew7.Name = "txtNew7";
            this.txtNew7.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtNew7.Text = null;
            this.txtNew7.Top = 3.53125F;
            this.txtNew7.Width = 0.5625F;
            // 
            // txtEstimate7
            // 
            this.txtEstimate7.Height = 0.1666667F;
            this.txtEstimate7.Left = 5.552083F;
            this.txtEstimate7.Name = "txtEstimate7";
            this.txtEstimate7.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtEstimate7.Text = null;
            this.txtEstimate7.Top = 3.53125F;
            this.txtEstimate7.Width = 0.5833333F;
            // 
            // txtEquipmentLocation7
            // 
            this.txtEquipmentLocation7.Height = 0.1666667F;
            this.txtEquipmentLocation7.Left = 6.291667F;
            this.txtEquipmentLocation7.Name = "txtEquipmentLocation7";
            this.txtEquipmentLocation7.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtEquipmentLocation7.Text = null;
            this.txtEquipmentLocation7.Top = 3.53125F;
            this.txtEquipmentLocation7.Width = 2.15625F;
            // 
            // txtTIF7
            // 
            this.txtTIF7.Height = 0.1666667F;
            this.txtTIF7.Left = 8.8125F;
            this.txtTIF7.Name = "txtTIF7";
            this.txtTIF7.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtTIF7.Text = null;
            this.txtTIF7.Top = 3.53125F;
            this.txtTIF7.Width = 0.2604167F;
            // 
            // txtBETEEligible7
            // 
            this.txtBETEEligible7.Height = 0.1666667F;
            this.txtBETEEligible7.Left = 9.1875F;
            this.txtBETEEligible7.Name = "txtBETEEligible7";
            this.txtBETEEligible7.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtBETEEligible7.Text = null;
            this.txtBETEEligible7.Top = 3.53125F;
            this.txtBETEEligible7.Width = 0.3229167F;
            // 
            // txtAssessment7
            // 
            this.txtAssessment7.Height = 0.1666667F;
            this.txtAssessment7.Left = 9.625F;
            this.txtAssessment7.Name = "txtAssessment7";
            this.txtAssessment7.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAssessment7.Text = null;
            this.txtAssessment7.Top = 3.53125F;
            this.txtAssessment7.Width = 0.5625F;
            // 
            // Line46
            // 
            this.Line46.Height = 0F;
            this.Line46.Left = 0F;
            this.Line46.LineWeight = 1F;
            this.Line46.Name = "Line46";
            this.Line46.Top = 3.958333F;
            this.Line46.Width = 8.5F;
            this.Line46.X1 = 0F;
            this.Line46.X2 = 8.5F;
            this.Line46.Y1 = 3.958333F;
            this.Line46.Y2 = 3.958333F;
            // 
            // Line47
            // 
            this.Line47.Height = 0F;
            this.Line47.Left = 8.75F;
            this.Line47.LineWeight = 1F;
            this.Line47.Name = "Line47";
            this.Line47.Top = 3.958333F;
            this.Line47.Width = 1.48958F;
            this.Line47.X1 = 8.75F;
            this.Line47.X2 = 10.23958F;
            this.Line47.Y1 = 3.958333F;
            this.Line47.Y2 = 3.958333F;
            // 
            // txtDescription8
            // 
            this.txtDescription8.Height = 0.1666667F;
            this.txtDescription8.Left = 0.08333334F;
            this.txtDescription8.Name = "txtDescription8";
            this.txtDescription8.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtDescription8.Text = null;
            this.txtDescription8.Top = 3.760417F;
            this.txtDescription8.Width = 3.208333F;
            // 
            // txtServiceDate8
            // 
            this.txtServiceDate8.Height = 0.1666667F;
            this.txtServiceDate8.Left = 3.395833F;
            this.txtServiceDate8.Name = "txtServiceDate8";
            this.txtServiceDate8.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtServiceDate8.Text = null;
            this.txtServiceDate8.Top = 3.760417F;
            this.txtServiceDate8.Width = 0.84375F;
            // 
            // txtAge8
            // 
            this.txtAge8.Height = 0.1666667F;
            this.txtAge8.Left = 4.3125F;
            this.txtAge8.Name = "txtAge8";
            this.txtAge8.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAge8.Text = null;
            this.txtAge8.Top = 3.760417F;
            this.txtAge8.Width = 0.4479167F;
            // 
            // txtNew8
            // 
            this.txtNew8.Height = 0.1666667F;
            this.txtNew8.Left = 4.90625F;
            this.txtNew8.Name = "txtNew8";
            this.txtNew8.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtNew8.Text = null;
            this.txtNew8.Top = 3.760417F;
            this.txtNew8.Width = 0.5625F;
            // 
            // txtEstimate8
            // 
            this.txtEstimate8.Height = 0.1666667F;
            this.txtEstimate8.Left = 5.552083F;
            this.txtEstimate8.Name = "txtEstimate8";
            this.txtEstimate8.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtEstimate8.Text = null;
            this.txtEstimate8.Top = 3.760417F;
            this.txtEstimate8.Width = 0.5833333F;
            // 
            // txtEquipmentLocation8
            // 
            this.txtEquipmentLocation8.Height = 0.1666667F;
            this.txtEquipmentLocation8.Left = 6.291667F;
            this.txtEquipmentLocation8.Name = "txtEquipmentLocation8";
            this.txtEquipmentLocation8.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtEquipmentLocation8.Text = null;
            this.txtEquipmentLocation8.Top = 3.760417F;
            this.txtEquipmentLocation8.Width = 2.15625F;
            // 
            // txtTIF8
            // 
            this.txtTIF8.Height = 0.1666667F;
            this.txtTIF8.Left = 8.8125F;
            this.txtTIF8.Name = "txtTIF8";
            this.txtTIF8.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtTIF8.Text = null;
            this.txtTIF8.Top = 3.760417F;
            this.txtTIF8.Width = 0.2604167F;
            // 
            // txtBETEEligible8
            // 
            this.txtBETEEligible8.Height = 0.1666667F;
            this.txtBETEEligible8.Left = 9.1875F;
            this.txtBETEEligible8.Name = "txtBETEEligible8";
            this.txtBETEEligible8.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtBETEEligible8.Text = null;
            this.txtBETEEligible8.Top = 3.760417F;
            this.txtBETEEligible8.Width = 0.3229167F;
            // 
            // txtAssessment8
            // 
            this.txtAssessment8.Height = 0.1666667F;
            this.txtAssessment8.Left = 9.625F;
            this.txtAssessment8.Name = "txtAssessment8";
            this.txtAssessment8.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAssessment8.Text = null;
            this.txtAssessment8.Top = 3.760417F;
            this.txtAssessment8.Width = 0.5625F;
            // 
            // Line48
            // 
            this.Line48.Height = 0F;
            this.Line48.Left = 0F;
            this.Line48.LineWeight = 1F;
            this.Line48.Name = "Line48";
            this.Line48.Top = 4.1875F;
            this.Line48.Width = 8.5F;
            this.Line48.X1 = 0F;
            this.Line48.X2 = 8.5F;
            this.Line48.Y1 = 4.1875F;
            this.Line48.Y2 = 4.1875F;
            // 
            // Line49
            // 
            this.Line49.Height = 0F;
            this.Line49.Left = 8.75F;
            this.Line49.LineWeight = 1F;
            this.Line49.Name = "Line49";
            this.Line49.Top = 4.1875F;
            this.Line49.Width = 1.48958F;
            this.Line49.X1 = 8.75F;
            this.Line49.X2 = 10.23958F;
            this.Line49.Y1 = 4.1875F;
            this.Line49.Y2 = 4.1875F;
            // 
            // txtDescription9
            // 
            this.txtDescription9.Height = 0.1666667F;
            this.txtDescription9.Left = 0.08333334F;
            this.txtDescription9.Name = "txtDescription9";
            this.txtDescription9.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtDescription9.Text = null;
            this.txtDescription9.Top = 3.989583F;
            this.txtDescription9.Width = 3.208333F;
            // 
            // txtServiceDate9
            // 
            this.txtServiceDate9.Height = 0.1666667F;
            this.txtServiceDate9.Left = 3.395833F;
            this.txtServiceDate9.Name = "txtServiceDate9";
            this.txtServiceDate9.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtServiceDate9.Text = null;
            this.txtServiceDate9.Top = 3.989583F;
            this.txtServiceDate9.Width = 0.84375F;
            // 
            // txtAge9
            // 
            this.txtAge9.Height = 0.1666667F;
            this.txtAge9.Left = 4.3125F;
            this.txtAge9.Name = "txtAge9";
            this.txtAge9.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAge9.Text = null;
            this.txtAge9.Top = 3.989583F;
            this.txtAge9.Width = 0.4479167F;
            // 
            // txtNew9
            // 
            this.txtNew9.Height = 0.1666667F;
            this.txtNew9.Left = 4.90625F;
            this.txtNew9.Name = "txtNew9";
            this.txtNew9.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtNew9.Text = null;
            this.txtNew9.Top = 3.989583F;
            this.txtNew9.Width = 0.5625F;
            // 
            // txtEstimate9
            // 
            this.txtEstimate9.Height = 0.1666667F;
            this.txtEstimate9.Left = 5.552083F;
            this.txtEstimate9.Name = "txtEstimate9";
            this.txtEstimate9.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtEstimate9.Text = null;
            this.txtEstimate9.Top = 3.989583F;
            this.txtEstimate9.Width = 0.5833333F;
            // 
            // txtEquipmentLocation9
            // 
            this.txtEquipmentLocation9.Height = 0.1666667F;
            this.txtEquipmentLocation9.Left = 6.291667F;
            this.txtEquipmentLocation9.Name = "txtEquipmentLocation9";
            this.txtEquipmentLocation9.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtEquipmentLocation9.Text = null;
            this.txtEquipmentLocation9.Top = 3.989583F;
            this.txtEquipmentLocation9.Width = 2.15625F;
            // 
            // txtTIF9
            // 
            this.txtTIF9.Height = 0.1666667F;
            this.txtTIF9.Left = 8.8125F;
            this.txtTIF9.Name = "txtTIF9";
            this.txtTIF9.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtTIF9.Text = null;
            this.txtTIF9.Top = 3.989583F;
            this.txtTIF9.Width = 0.2604167F;
            // 
            // txtBETEEligible9
            // 
            this.txtBETEEligible9.Height = 0.1666667F;
            this.txtBETEEligible9.Left = 9.1875F;
            this.txtBETEEligible9.Name = "txtBETEEligible9";
            this.txtBETEEligible9.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtBETEEligible9.Text = null;
            this.txtBETEEligible9.Top = 3.989583F;
            this.txtBETEEligible9.Width = 0.3229167F;
            // 
            // txtAssessment9
            // 
            this.txtAssessment9.Height = 0.1666667F;
            this.txtAssessment9.Left = 9.625F;
            this.txtAssessment9.Name = "txtAssessment9";
            this.txtAssessment9.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAssessment9.Text = null;
            this.txtAssessment9.Top = 3.989583F;
            this.txtAssessment9.Width = 0.5625F;
            // 
            // Line50
            // 
            this.Line50.Height = 0F;
            this.Line50.Left = 0F;
            this.Line50.LineWeight = 1F;
            this.Line50.Name = "Line50";
            this.Line50.Top = 4.416667F;
            this.Line50.Width = 8.5F;
            this.Line50.X1 = 0F;
            this.Line50.X2 = 8.5F;
            this.Line50.Y1 = 4.416667F;
            this.Line50.Y2 = 4.416667F;
            // 
            // Line51
            // 
            this.Line51.Height = 0F;
            this.Line51.Left = 8.75F;
            this.Line51.LineWeight = 1F;
            this.Line51.Name = "Line51";
            this.Line51.Top = 4.416667F;
            this.Line51.Width = 1.48958F;
            this.Line51.X1 = 8.75F;
            this.Line51.X2 = 10.23958F;
            this.Line51.Y1 = 4.416667F;
            this.Line51.Y2 = 4.416667F;
            // 
            // txtDescription10
            // 
            this.txtDescription10.Height = 0.1666667F;
            this.txtDescription10.Left = 0.08333334F;
            this.txtDescription10.Name = "txtDescription10";
            this.txtDescription10.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtDescription10.Text = null;
            this.txtDescription10.Top = 4.21875F;
            this.txtDescription10.Width = 3.208333F;
            // 
            // txtServiceDate10
            // 
            this.txtServiceDate10.Height = 0.1666667F;
            this.txtServiceDate10.Left = 3.395833F;
            this.txtServiceDate10.Name = "txtServiceDate10";
            this.txtServiceDate10.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtServiceDate10.Text = null;
            this.txtServiceDate10.Top = 4.21875F;
            this.txtServiceDate10.Width = 0.84375F;
            // 
            // txtAge10
            // 
            this.txtAge10.Height = 0.1666667F;
            this.txtAge10.Left = 4.3125F;
            this.txtAge10.Name = "txtAge10";
            this.txtAge10.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAge10.Text = null;
            this.txtAge10.Top = 4.21875F;
            this.txtAge10.Width = 0.4479167F;
            // 
            // txtNew10
            // 
            this.txtNew10.Height = 0.1666667F;
            this.txtNew10.Left = 4.90625F;
            this.txtNew10.Name = "txtNew10";
            this.txtNew10.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtNew10.Text = null;
            this.txtNew10.Top = 4.21875F;
            this.txtNew10.Width = 0.5625F;
            // 
            // txtEstimate10
            // 
            this.txtEstimate10.Height = 0.1666667F;
            this.txtEstimate10.Left = 5.552083F;
            this.txtEstimate10.Name = "txtEstimate10";
            this.txtEstimate10.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtEstimate10.Text = null;
            this.txtEstimate10.Top = 4.21875F;
            this.txtEstimate10.Width = 0.5833333F;
            // 
            // txtEquipmentLocation10
            // 
            this.txtEquipmentLocation10.Height = 0.1666667F;
            this.txtEquipmentLocation10.Left = 6.291667F;
            this.txtEquipmentLocation10.Name = "txtEquipmentLocation10";
            this.txtEquipmentLocation10.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtEquipmentLocation10.Text = null;
            this.txtEquipmentLocation10.Top = 4.21875F;
            this.txtEquipmentLocation10.Width = 2.15625F;
            // 
            // txtTIF10
            // 
            this.txtTIF10.Height = 0.1666667F;
            this.txtTIF10.Left = 8.8125F;
            this.txtTIF10.Name = "txtTIF10";
            this.txtTIF10.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtTIF10.Text = null;
            this.txtTIF10.Top = 4.21875F;
            this.txtTIF10.Width = 0.2604167F;
            // 
            // txtBETEEligible10
            // 
            this.txtBETEEligible10.Height = 0.1666667F;
            this.txtBETEEligible10.Left = 9.1875F;
            this.txtBETEEligible10.Name = "txtBETEEligible10";
            this.txtBETEEligible10.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtBETEEligible10.Text = null;
            this.txtBETEEligible10.Top = 4.21875F;
            this.txtBETEEligible10.Width = 0.3229167F;
            // 
            // txtAssessment10
            // 
            this.txtAssessment10.Height = 0.1666667F;
            this.txtAssessment10.Left = 9.625F;
            this.txtAssessment10.Name = "txtAssessment10";
            this.txtAssessment10.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAssessment10.Text = null;
            this.txtAssessment10.Top = 4.21875F;
            this.txtAssessment10.Width = 0.5625F;
            // 
            // Line52
            // 
            this.Line52.Height = 0F;
            this.Line52.Left = 0F;
            this.Line52.LineWeight = 1F;
            this.Line52.Name = "Line52";
            this.Line52.Top = 4.645833F;
            this.Line52.Width = 8.5F;
            this.Line52.X1 = 0F;
            this.Line52.X2 = 8.5F;
            this.Line52.Y1 = 4.645833F;
            this.Line52.Y2 = 4.645833F;
            // 
            // Line53
            // 
            this.Line53.Height = 0F;
            this.Line53.Left = 8.75F;
            this.Line53.LineWeight = 1F;
            this.Line53.Name = "Line53";
            this.Line53.Top = 4.645833F;
            this.Line53.Width = 1.48958F;
            this.Line53.X1 = 8.75F;
            this.Line53.X2 = 10.23958F;
            this.Line53.Y1 = 4.645833F;
            this.Line53.Y2 = 4.645833F;
            // 
            // txtDescription11
            // 
            this.txtDescription11.Height = 0.1666667F;
            this.txtDescription11.Left = 0.08333334F;
            this.txtDescription11.Name = "txtDescription11";
            this.txtDescription11.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtDescription11.Text = null;
            this.txtDescription11.Top = 4.447917F;
            this.txtDescription11.Width = 3.208333F;
            // 
            // txtServiceDate11
            // 
            this.txtServiceDate11.Height = 0.1666667F;
            this.txtServiceDate11.Left = 3.395833F;
            this.txtServiceDate11.Name = "txtServiceDate11";
            this.txtServiceDate11.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtServiceDate11.Text = null;
            this.txtServiceDate11.Top = 4.447917F;
            this.txtServiceDate11.Width = 0.84375F;
            // 
            // txtAge11
            // 
            this.txtAge11.Height = 0.1666667F;
            this.txtAge11.Left = 4.3125F;
            this.txtAge11.Name = "txtAge11";
            this.txtAge11.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAge11.Text = null;
            this.txtAge11.Top = 4.447917F;
            this.txtAge11.Width = 0.4479167F;
            // 
            // txtNew11
            // 
            this.txtNew11.Height = 0.1666667F;
            this.txtNew11.Left = 4.90625F;
            this.txtNew11.Name = "txtNew11";
            this.txtNew11.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtNew11.Text = null;
            this.txtNew11.Top = 4.447917F;
            this.txtNew11.Width = 0.5625F;
            // 
            // txtEstimate11
            // 
            this.txtEstimate11.Height = 0.1666667F;
            this.txtEstimate11.Left = 5.552083F;
            this.txtEstimate11.Name = "txtEstimate11";
            this.txtEstimate11.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtEstimate11.Text = null;
            this.txtEstimate11.Top = 4.447917F;
            this.txtEstimate11.Width = 0.5833333F;
            // 
            // txtEquipmentLocation11
            // 
            this.txtEquipmentLocation11.Height = 0.1666667F;
            this.txtEquipmentLocation11.Left = 6.291667F;
            this.txtEquipmentLocation11.Name = "txtEquipmentLocation11";
            this.txtEquipmentLocation11.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtEquipmentLocation11.Text = null;
            this.txtEquipmentLocation11.Top = 4.447917F;
            this.txtEquipmentLocation11.Width = 2.15625F;
            // 
            // txtTIF11
            // 
            this.txtTIF11.Height = 0.1666667F;
            this.txtTIF11.Left = 8.8125F;
            this.txtTIF11.Name = "txtTIF11";
            this.txtTIF11.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtTIF11.Text = null;
            this.txtTIF11.Top = 4.447917F;
            this.txtTIF11.Width = 0.2604167F;
            // 
            // txtBETEEligible11
            // 
            this.txtBETEEligible11.Height = 0.1666667F;
            this.txtBETEEligible11.Left = 9.1875F;
            this.txtBETEEligible11.Name = "txtBETEEligible11";
            this.txtBETEEligible11.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtBETEEligible11.Text = null;
            this.txtBETEEligible11.Top = 4.447917F;
            this.txtBETEEligible11.Width = 0.3229167F;
            // 
            // txtAssessment11
            // 
            this.txtAssessment11.Height = 0.1666667F;
            this.txtAssessment11.Left = 9.625F;
            this.txtAssessment11.Name = "txtAssessment11";
            this.txtAssessment11.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAssessment11.Text = null;
            this.txtAssessment11.Top = 4.447917F;
            this.txtAssessment11.Width = 0.5625F;
            // 
            // Line54
            // 
            this.Line54.Height = 0F;
            this.Line54.Left = 0F;
            this.Line54.LineWeight = 1F;
            this.Line54.Name = "Line54";
            this.Line54.Top = 4.875F;
            this.Line54.Width = 8.5F;
            this.Line54.X1 = 0F;
            this.Line54.X2 = 8.5F;
            this.Line54.Y1 = 4.875F;
            this.Line54.Y2 = 4.875F;
            // 
            // Line55
            // 
            this.Line55.Height = 0F;
            this.Line55.Left = 8.75F;
            this.Line55.LineWeight = 1F;
            this.Line55.Name = "Line55";
            this.Line55.Top = 4.875F;
            this.Line55.Width = 1.48958F;
            this.Line55.X1 = 8.75F;
            this.Line55.X2 = 10.23958F;
            this.Line55.Y1 = 4.875F;
            this.Line55.Y2 = 4.875F;
            // 
            // txtDescription12
            // 
            this.txtDescription12.Height = 0.1666667F;
            this.txtDescription12.Left = 0.08333334F;
            this.txtDescription12.Name = "txtDescription12";
            this.txtDescription12.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtDescription12.Text = null;
            this.txtDescription12.Top = 4.677083F;
            this.txtDescription12.Width = 3.208333F;
            // 
            // txtServiceDate12
            // 
            this.txtServiceDate12.Height = 0.1666667F;
            this.txtServiceDate12.Left = 3.395833F;
            this.txtServiceDate12.Name = "txtServiceDate12";
            this.txtServiceDate12.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtServiceDate12.Text = null;
            this.txtServiceDate12.Top = 4.677083F;
            this.txtServiceDate12.Width = 0.84375F;
            // 
            // txtAge12
            // 
            this.txtAge12.Height = 0.1666667F;
            this.txtAge12.Left = 4.3125F;
            this.txtAge12.Name = "txtAge12";
            this.txtAge12.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAge12.Text = null;
            this.txtAge12.Top = 4.677083F;
            this.txtAge12.Width = 0.4479167F;
            // 
            // txtNew12
            // 
            this.txtNew12.Height = 0.1666667F;
            this.txtNew12.Left = 4.90625F;
            this.txtNew12.Name = "txtNew12";
            this.txtNew12.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtNew12.Text = null;
            this.txtNew12.Top = 4.677083F;
            this.txtNew12.Width = 0.5625F;
            // 
            // txtEstimate12
            // 
            this.txtEstimate12.Height = 0.1666667F;
            this.txtEstimate12.Left = 5.552083F;
            this.txtEstimate12.Name = "txtEstimate12";
            this.txtEstimate12.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtEstimate12.Text = null;
            this.txtEstimate12.Top = 4.677083F;
            this.txtEstimate12.Width = 0.5833333F;
            // 
            // txtEquipmentLocation12
            // 
            this.txtEquipmentLocation12.Height = 0.1666667F;
            this.txtEquipmentLocation12.Left = 6.291667F;
            this.txtEquipmentLocation12.Name = "txtEquipmentLocation12";
            this.txtEquipmentLocation12.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtEquipmentLocation12.Text = null;
            this.txtEquipmentLocation12.Top = 4.677083F;
            this.txtEquipmentLocation12.Width = 2.15625F;
            // 
            // txtTIF12
            // 
            this.txtTIF12.Height = 0.1666667F;
            this.txtTIF12.Left = 8.8125F;
            this.txtTIF12.Name = "txtTIF12";
            this.txtTIF12.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtTIF12.Text = null;
            this.txtTIF12.Top = 4.677083F;
            this.txtTIF12.Width = 0.2604167F;
            // 
            // txtBETEEligible12
            // 
            this.txtBETEEligible12.Height = 0.1666667F;
            this.txtBETEEligible12.Left = 9.1875F;
            this.txtBETEEligible12.Name = "txtBETEEligible12";
            this.txtBETEEligible12.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtBETEEligible12.Text = null;
            this.txtBETEEligible12.Top = 4.677083F;
            this.txtBETEEligible12.Width = 0.3229167F;
            // 
            // txtAssessment12
            // 
            this.txtAssessment12.Height = 0.1666667F;
            this.txtAssessment12.Left = 9.625F;
            this.txtAssessment12.Name = "txtAssessment12";
            this.txtAssessment12.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAssessment12.Text = null;
            this.txtAssessment12.Top = 4.677083F;
            this.txtAssessment12.Width = 0.5625F;
            // 
            // Line56
            // 
            this.Line56.Height = 0F;
            this.Line56.Left = 0F;
            this.Line56.LineWeight = 1F;
            this.Line56.Name = "Line56";
            this.Line56.Top = 5.104167F;
            this.Line56.Width = 8.5F;
            this.Line56.X1 = 0F;
            this.Line56.X2 = 8.5F;
            this.Line56.Y1 = 5.104167F;
            this.Line56.Y2 = 5.104167F;
            // 
            // Line57
            // 
            this.Line57.Height = 0F;
            this.Line57.Left = 8.75F;
            this.Line57.LineWeight = 1F;
            this.Line57.Name = "Line57";
            this.Line57.Top = 5.104167F;
            this.Line57.Width = 1.48958F;
            this.Line57.X1 = 8.75F;
            this.Line57.X2 = 10.23958F;
            this.Line57.Y1 = 5.104167F;
            this.Line57.Y2 = 5.104167F;
            // 
            // txtDescription13
            // 
            this.txtDescription13.Height = 0.1666667F;
            this.txtDescription13.Left = 0.08333334F;
            this.txtDescription13.Name = "txtDescription13";
            this.txtDescription13.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtDescription13.Text = null;
            this.txtDescription13.Top = 4.90625F;
            this.txtDescription13.Width = 3.208333F;
            // 
            // txtServiceDate13
            // 
            this.txtServiceDate13.Height = 0.1666667F;
            this.txtServiceDate13.Left = 3.395833F;
            this.txtServiceDate13.Name = "txtServiceDate13";
            this.txtServiceDate13.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtServiceDate13.Text = null;
            this.txtServiceDate13.Top = 4.90625F;
            this.txtServiceDate13.Width = 0.84375F;
            // 
            // txtAge13
            // 
            this.txtAge13.Height = 0.1666667F;
            this.txtAge13.Left = 4.3125F;
            this.txtAge13.Name = "txtAge13";
            this.txtAge13.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAge13.Text = null;
            this.txtAge13.Top = 4.90625F;
            this.txtAge13.Width = 0.4479167F;
            // 
            // txtNew13
            // 
            this.txtNew13.Height = 0.1666667F;
            this.txtNew13.Left = 4.90625F;
            this.txtNew13.Name = "txtNew13";
            this.txtNew13.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtNew13.Text = null;
            this.txtNew13.Top = 4.90625F;
            this.txtNew13.Width = 0.5625F;
            // 
            // txtEstimate13
            // 
            this.txtEstimate13.Height = 0.1666667F;
            this.txtEstimate13.Left = 5.552083F;
            this.txtEstimate13.Name = "txtEstimate13";
            this.txtEstimate13.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtEstimate13.Text = null;
            this.txtEstimate13.Top = 4.90625F;
            this.txtEstimate13.Width = 0.5833333F;
            // 
            // txtEquipmentLocation13
            // 
            this.txtEquipmentLocation13.Height = 0.1666667F;
            this.txtEquipmentLocation13.Left = 6.291667F;
            this.txtEquipmentLocation13.Name = "txtEquipmentLocation13";
            this.txtEquipmentLocation13.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtEquipmentLocation13.Text = null;
            this.txtEquipmentLocation13.Top = 4.90625F;
            this.txtEquipmentLocation13.Width = 2.15625F;
            // 
            // txtTIF13
            // 
            this.txtTIF13.Height = 0.1666667F;
            this.txtTIF13.Left = 8.8125F;
            this.txtTIF13.Name = "txtTIF13";
            this.txtTIF13.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtTIF13.Text = null;
            this.txtTIF13.Top = 4.90625F;
            this.txtTIF13.Width = 0.2604167F;
            // 
            // txtBETEEligible13
            // 
            this.txtBETEEligible13.Height = 0.1666667F;
            this.txtBETEEligible13.Left = 9.1875F;
            this.txtBETEEligible13.Name = "txtBETEEligible13";
            this.txtBETEEligible13.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtBETEEligible13.Text = null;
            this.txtBETEEligible13.Top = 4.90625F;
            this.txtBETEEligible13.Width = 0.3229167F;
            // 
            // txtAssessment13
            // 
            this.txtAssessment13.Height = 0.1666667F;
            this.txtAssessment13.Left = 9.625F;
            this.txtAssessment13.Name = "txtAssessment13";
            this.txtAssessment13.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAssessment13.Text = null;
            this.txtAssessment13.Top = 4.90625F;
            this.txtAssessment13.Width = 0.5625F;
            // 
            // Line58
            // 
            this.Line58.Height = 0F;
            this.Line58.Left = 0F;
            this.Line58.LineWeight = 1F;
            this.Line58.Name = "Line58";
            this.Line58.Top = 5.333333F;
            this.Line58.Width = 8.5F;
            this.Line58.X1 = 0F;
            this.Line58.X2 = 8.5F;
            this.Line58.Y1 = 5.333333F;
            this.Line58.Y2 = 5.333333F;
            // 
            // Line59
            // 
            this.Line59.Height = 0F;
            this.Line59.Left = 8.75F;
            this.Line59.LineWeight = 1F;
            this.Line59.Name = "Line59";
            this.Line59.Top = 5.333333F;
            this.Line59.Width = 1.48958F;
            this.Line59.X1 = 8.75F;
            this.Line59.X2 = 10.23958F;
            this.Line59.Y1 = 5.333333F;
            this.Line59.Y2 = 5.333333F;
            // 
            // txtDescription14
            // 
            this.txtDescription14.Height = 0.1666667F;
            this.txtDescription14.Left = 0.08333334F;
            this.txtDescription14.Name = "txtDescription14";
            this.txtDescription14.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtDescription14.Text = null;
            this.txtDescription14.Top = 5.135417F;
            this.txtDescription14.Width = 3.208333F;
            // 
            // txtServiceDate14
            // 
            this.txtServiceDate14.Height = 0.1666667F;
            this.txtServiceDate14.Left = 3.395833F;
            this.txtServiceDate14.Name = "txtServiceDate14";
            this.txtServiceDate14.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtServiceDate14.Text = null;
            this.txtServiceDate14.Top = 5.135417F;
            this.txtServiceDate14.Width = 0.84375F;
            // 
            // txtAge14
            // 
            this.txtAge14.Height = 0.1666667F;
            this.txtAge14.Left = 4.3125F;
            this.txtAge14.Name = "txtAge14";
            this.txtAge14.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAge14.Text = null;
            this.txtAge14.Top = 5.135417F;
            this.txtAge14.Width = 0.4479167F;
            // 
            // txtNew14
            // 
            this.txtNew14.Height = 0.1666667F;
            this.txtNew14.Left = 4.90625F;
            this.txtNew14.Name = "txtNew14";
            this.txtNew14.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtNew14.Text = null;
            this.txtNew14.Top = 5.135417F;
            this.txtNew14.Width = 0.5625F;
            // 
            // txtEstimate14
            // 
            this.txtEstimate14.Height = 0.1666667F;
            this.txtEstimate14.Left = 5.552083F;
            this.txtEstimate14.Name = "txtEstimate14";
            this.txtEstimate14.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtEstimate14.Text = null;
            this.txtEstimate14.Top = 5.135417F;
            this.txtEstimate14.Width = 0.5833333F;
            // 
            // txtEquipmentLocation14
            // 
            this.txtEquipmentLocation14.Height = 0.1666667F;
            this.txtEquipmentLocation14.Left = 6.291667F;
            this.txtEquipmentLocation14.Name = "txtEquipmentLocation14";
            this.txtEquipmentLocation14.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtEquipmentLocation14.Text = null;
            this.txtEquipmentLocation14.Top = 5.135417F;
            this.txtEquipmentLocation14.Width = 2.15625F;
            // 
            // txtTIF14
            // 
            this.txtTIF14.Height = 0.1666667F;
            this.txtTIF14.Left = 8.8125F;
            this.txtTIF14.Name = "txtTIF14";
            this.txtTIF14.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtTIF14.Text = null;
            this.txtTIF14.Top = 5.135417F;
            this.txtTIF14.Width = 0.2604167F;
            // 
            // txtBETEEligible14
            // 
            this.txtBETEEligible14.Height = 0.1666667F;
            this.txtBETEEligible14.Left = 9.1875F;
            this.txtBETEEligible14.Name = "txtBETEEligible14";
            this.txtBETEEligible14.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtBETEEligible14.Text = null;
            this.txtBETEEligible14.Top = 5.135417F;
            this.txtBETEEligible14.Width = 0.3229167F;
            // 
            // txtAssessment14
            // 
            this.txtAssessment14.Height = 0.1666667F;
            this.txtAssessment14.Left = 9.625F;
            this.txtAssessment14.Name = "txtAssessment14";
            this.txtAssessment14.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAssessment14.Text = null;
            this.txtAssessment14.Top = 5.135417F;
            this.txtAssessment14.Width = 0.5625F;
            // 
            // Line60
            // 
            this.Line60.Height = 0F;
            this.Line60.Left = 0F;
            this.Line60.LineWeight = 1F;
            this.Line60.Name = "Line60";
            this.Line60.Top = 5.5625F;
            this.Line60.Width = 8.5F;
            this.Line60.X1 = 0F;
            this.Line60.X2 = 8.5F;
            this.Line60.Y1 = 5.5625F;
            this.Line60.Y2 = 5.5625F;
            // 
            // Line61
            // 
            this.Line61.Height = 0F;
            this.Line61.Left = 8.75F;
            this.Line61.LineWeight = 1F;
            this.Line61.Name = "Line61";
            this.Line61.Top = 5.5625F;
            this.Line61.Width = 1.48958F;
            this.Line61.X1 = 8.75F;
            this.Line61.X2 = 10.23958F;
            this.Line61.Y1 = 5.5625F;
            this.Line61.Y2 = 5.5625F;
            // 
            // txtDescription15
            // 
            this.txtDescription15.Height = 0.1666667F;
            this.txtDescription15.Left = 0.08333334F;
            this.txtDescription15.Name = "txtDescription15";
            this.txtDescription15.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtDescription15.Text = null;
            this.txtDescription15.Top = 5.364583F;
            this.txtDescription15.Width = 3.208333F;
            // 
            // txtServiceDate15
            // 
            this.txtServiceDate15.Height = 0.1666667F;
            this.txtServiceDate15.Left = 3.395833F;
            this.txtServiceDate15.Name = "txtServiceDate15";
            this.txtServiceDate15.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtServiceDate15.Text = null;
            this.txtServiceDate15.Top = 5.364583F;
            this.txtServiceDate15.Width = 0.84375F;
            // 
            // txtAge15
            // 
            this.txtAge15.Height = 0.1666667F;
            this.txtAge15.Left = 4.3125F;
            this.txtAge15.Name = "txtAge15";
            this.txtAge15.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAge15.Text = null;
            this.txtAge15.Top = 5.364583F;
            this.txtAge15.Width = 0.4479167F;
            // 
            // txtNew15
            // 
            this.txtNew15.Height = 0.1666667F;
            this.txtNew15.Left = 4.90625F;
            this.txtNew15.Name = "txtNew15";
            this.txtNew15.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtNew15.Text = null;
            this.txtNew15.Top = 5.364583F;
            this.txtNew15.Width = 0.5625F;
            // 
            // txtEstimate15
            // 
            this.txtEstimate15.Height = 0.1666667F;
            this.txtEstimate15.Left = 5.552083F;
            this.txtEstimate15.Name = "txtEstimate15";
            this.txtEstimate15.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtEstimate15.Text = null;
            this.txtEstimate15.Top = 5.364583F;
            this.txtEstimate15.Width = 0.5833333F;
            // 
            // txtEquipmentLocation15
            // 
            this.txtEquipmentLocation15.Height = 0.1666667F;
            this.txtEquipmentLocation15.Left = 6.291667F;
            this.txtEquipmentLocation15.Name = "txtEquipmentLocation15";
            this.txtEquipmentLocation15.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtEquipmentLocation15.Text = null;
            this.txtEquipmentLocation15.Top = 5.364583F;
            this.txtEquipmentLocation15.Width = 2.15625F;
            // 
            // txtTIF15
            // 
            this.txtTIF15.Height = 0.1666667F;
            this.txtTIF15.Left = 8.8125F;
            this.txtTIF15.Name = "txtTIF15";
            this.txtTIF15.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtTIF15.Text = null;
            this.txtTIF15.Top = 5.364583F;
            this.txtTIF15.Width = 0.2604167F;
            // 
            // txtBETEEligible15
            // 
            this.txtBETEEligible15.Height = 0.1666667F;
            this.txtBETEEligible15.Left = 9.1875F;
            this.txtBETEEligible15.Name = "txtBETEEligible15";
            this.txtBETEEligible15.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtBETEEligible15.Text = null;
            this.txtBETEEligible15.Top = 5.364583F;
            this.txtBETEEligible15.Width = 0.3229167F;
            // 
            // txtAssessment15
            // 
            this.txtAssessment15.Height = 0.1666667F;
            this.txtAssessment15.Left = 9.625F;
            this.txtAssessment15.Name = "txtAssessment15";
            this.txtAssessment15.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAssessment15.Text = null;
            this.txtAssessment15.Top = 5.364583F;
            this.txtAssessment15.Width = 0.5625F;
            // 
            // Line62
            // 
            this.Line62.Height = 0F;
            this.Line62.Left = 0F;
            this.Line62.LineWeight = 1F;
            this.Line62.Name = "Line62";
            this.Line62.Top = 5.791667F;
            this.Line62.Width = 8.5F;
            this.Line62.X1 = 0F;
            this.Line62.X2 = 8.5F;
            this.Line62.Y1 = 5.791667F;
            this.Line62.Y2 = 5.791667F;
            // 
            // Line63
            // 
            this.Line63.Height = 0F;
            this.Line63.Left = 8.75F;
            this.Line63.LineWeight = 1F;
            this.Line63.Name = "Line63";
            this.Line63.Top = 5.791667F;
            this.Line63.Width = 1.48958F;
            this.Line63.X1 = 8.75F;
            this.Line63.X2 = 10.23958F;
            this.Line63.Y1 = 5.791667F;
            this.Line63.Y2 = 5.791667F;
            // 
            // txtDescription16
            // 
            this.txtDescription16.Height = 0.1666667F;
            this.txtDescription16.Left = 0.08333334F;
            this.txtDescription16.Name = "txtDescription16";
            this.txtDescription16.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtDescription16.Text = null;
            this.txtDescription16.Top = 5.59375F;
            this.txtDescription16.Width = 3.208333F;
            // 
            // txtServiceDate16
            // 
            this.txtServiceDate16.Height = 0.1666667F;
            this.txtServiceDate16.Left = 3.395833F;
            this.txtServiceDate16.Name = "txtServiceDate16";
            this.txtServiceDate16.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtServiceDate16.Text = null;
            this.txtServiceDate16.Top = 5.59375F;
            this.txtServiceDate16.Width = 0.84375F;
            // 
            // txtAge16
            // 
            this.txtAge16.Height = 0.1666667F;
            this.txtAge16.Left = 4.3125F;
            this.txtAge16.Name = "txtAge16";
            this.txtAge16.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAge16.Text = null;
            this.txtAge16.Top = 5.59375F;
            this.txtAge16.Width = 0.4479167F;
            // 
            // txtNew16
            // 
            this.txtNew16.Height = 0.1666667F;
            this.txtNew16.Left = 4.90625F;
            this.txtNew16.Name = "txtNew16";
            this.txtNew16.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtNew16.Text = null;
            this.txtNew16.Top = 5.59375F;
            this.txtNew16.Width = 0.5625F;
            // 
            // txtEstimate16
            // 
            this.txtEstimate16.Height = 0.1666667F;
            this.txtEstimate16.Left = 5.552083F;
            this.txtEstimate16.Name = "txtEstimate16";
            this.txtEstimate16.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtEstimate16.Text = null;
            this.txtEstimate16.Top = 5.59375F;
            this.txtEstimate16.Width = 0.5833333F;
            // 
            // txtEquipmentLocation16
            // 
            this.txtEquipmentLocation16.Height = 0.1666667F;
            this.txtEquipmentLocation16.Left = 6.291667F;
            this.txtEquipmentLocation16.Name = "txtEquipmentLocation16";
            this.txtEquipmentLocation16.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtEquipmentLocation16.Text = null;
            this.txtEquipmentLocation16.Top = 5.59375F;
            this.txtEquipmentLocation16.Width = 2.15625F;
            // 
            // txtTIF16
            // 
            this.txtTIF16.Height = 0.1666667F;
            this.txtTIF16.Left = 8.8125F;
            this.txtTIF16.Name = "txtTIF16";
            this.txtTIF16.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtTIF16.Text = null;
            this.txtTIF16.Top = 5.59375F;
            this.txtTIF16.Width = 0.2604167F;
            // 
            // txtBETEEligible16
            // 
            this.txtBETEEligible16.Height = 0.1666667F;
            this.txtBETEEligible16.Left = 9.1875F;
            this.txtBETEEligible16.Name = "txtBETEEligible16";
            this.txtBETEEligible16.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtBETEEligible16.Text = null;
            this.txtBETEEligible16.Top = 5.59375F;
            this.txtBETEEligible16.Width = 0.3229167F;
            // 
            // txtAssessment16
            // 
            this.txtAssessment16.Height = 0.1666667F;
            this.txtAssessment16.Left = 9.625F;
            this.txtAssessment16.Name = "txtAssessment16";
            this.txtAssessment16.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAssessment16.Text = null;
            this.txtAssessment16.Top = 5.59375F;
            this.txtAssessment16.Width = 0.5625F;
            // 
            // Line64
            // 
            this.Line64.Height = 0F;
            this.Line64.Left = 0F;
            this.Line64.LineWeight = 1F;
            this.Line64.Name = "Line64";
            this.Line64.Top = 6.020833F;
            this.Line64.Width = 8.5F;
            this.Line64.X1 = 0F;
            this.Line64.X2 = 8.5F;
            this.Line64.Y1 = 6.020833F;
            this.Line64.Y2 = 6.020833F;
            // 
            // Line65
            // 
            this.Line65.Height = 0F;
            this.Line65.Left = 8.75F;
            this.Line65.LineWeight = 1F;
            this.Line65.Name = "Line65";
            this.Line65.Top = 6.020833F;
            this.Line65.Width = 1.48958F;
            this.Line65.X1 = 8.75F;
            this.Line65.X2 = 10.23958F;
            this.Line65.Y1 = 6.020833F;
            this.Line65.Y2 = 6.020833F;
            // 
            // txtDescription17
            // 
            this.txtDescription17.Height = 0.1666667F;
            this.txtDescription17.Left = 0.08333334F;
            this.txtDescription17.Name = "txtDescription17";
            this.txtDescription17.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtDescription17.Text = null;
            this.txtDescription17.Top = 5.822917F;
            this.txtDescription17.Width = 3.208333F;
            // 
            // txtServiceDate17
            // 
            this.txtServiceDate17.Height = 0.1666667F;
            this.txtServiceDate17.Left = 3.395833F;
            this.txtServiceDate17.Name = "txtServiceDate17";
            this.txtServiceDate17.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtServiceDate17.Text = null;
            this.txtServiceDate17.Top = 5.822917F;
            this.txtServiceDate17.Width = 0.84375F;
            // 
            // txtAge17
            // 
            this.txtAge17.Height = 0.1666667F;
            this.txtAge17.Left = 4.3125F;
            this.txtAge17.Name = "txtAge17";
            this.txtAge17.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAge17.Text = null;
            this.txtAge17.Top = 5.822917F;
            this.txtAge17.Width = 0.4479167F;
            // 
            // txtNew17
            // 
            this.txtNew17.Height = 0.1666667F;
            this.txtNew17.Left = 4.90625F;
            this.txtNew17.Name = "txtNew17";
            this.txtNew17.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtNew17.Text = null;
            this.txtNew17.Top = 5.822917F;
            this.txtNew17.Width = 0.5625F;
            // 
            // txtEstimate17
            // 
            this.txtEstimate17.Height = 0.1666667F;
            this.txtEstimate17.Left = 5.552083F;
            this.txtEstimate17.Name = "txtEstimate17";
            this.txtEstimate17.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtEstimate17.Text = null;
            this.txtEstimate17.Top = 5.822917F;
            this.txtEstimate17.Width = 0.5833333F;
            // 
            // txtEquipmentLocation17
            // 
            this.txtEquipmentLocation17.Height = 0.1666667F;
            this.txtEquipmentLocation17.Left = 6.291667F;
            this.txtEquipmentLocation17.Name = "txtEquipmentLocation17";
            this.txtEquipmentLocation17.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtEquipmentLocation17.Text = null;
            this.txtEquipmentLocation17.Top = 5.822917F;
            this.txtEquipmentLocation17.Width = 2.15625F;
            // 
            // txtTIF17
            // 
            this.txtTIF17.Height = 0.1666667F;
            this.txtTIF17.Left = 8.8125F;
            this.txtTIF17.Name = "txtTIF17";
            this.txtTIF17.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtTIF17.Text = null;
            this.txtTIF17.Top = 5.822917F;
            this.txtTIF17.Width = 0.2604167F;
            // 
            // txtBETEEligible17
            // 
            this.txtBETEEligible17.Height = 0.1666667F;
            this.txtBETEEligible17.Left = 9.1875F;
            this.txtBETEEligible17.Name = "txtBETEEligible17";
            this.txtBETEEligible17.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtBETEEligible17.Text = null;
            this.txtBETEEligible17.Top = 5.822917F;
            this.txtBETEEligible17.Width = 0.3229167F;
            // 
            // txtAssessment17
            // 
            this.txtAssessment17.Height = 0.1666667F;
            this.txtAssessment17.Left = 9.625F;
            this.txtAssessment17.Name = "txtAssessment17";
            this.txtAssessment17.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAssessment17.Text = null;
            this.txtAssessment17.Top = 5.822917F;
            this.txtAssessment17.Width = 0.5625F;
            // 
            // Line66
            // 
            this.Line66.Height = 0F;
            this.Line66.Left = 0F;
            this.Line66.LineWeight = 1F;
            this.Line66.Name = "Line66";
            this.Line66.Top = 6.25F;
            this.Line66.Width = 8.5F;
            this.Line66.X1 = 0F;
            this.Line66.X2 = 8.5F;
            this.Line66.Y1 = 6.25F;
            this.Line66.Y2 = 6.25F;
            // 
            // Line67
            // 
            this.Line67.Height = 0F;
            this.Line67.Left = 8.75F;
            this.Line67.LineWeight = 1F;
            this.Line67.Name = "Line67";
            this.Line67.Top = 6.25F;
            this.Line67.Width = 1.48958F;
            this.Line67.X1 = 8.75F;
            this.Line67.X2 = 10.23958F;
            this.Line67.Y1 = 6.25F;
            this.Line67.Y2 = 6.25F;
            // 
            // txtDescription18
            // 
            this.txtDescription18.Height = 0.1666667F;
            this.txtDescription18.Left = 0.08333334F;
            this.txtDescription18.Name = "txtDescription18";
            this.txtDescription18.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtDescription18.Text = null;
            this.txtDescription18.Top = 6.052083F;
            this.txtDescription18.Width = 3.208333F;
            // 
            // txtServiceDate18
            // 
            this.txtServiceDate18.Height = 0.1666667F;
            this.txtServiceDate18.Left = 3.395833F;
            this.txtServiceDate18.Name = "txtServiceDate18";
            this.txtServiceDate18.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtServiceDate18.Text = null;
            this.txtServiceDate18.Top = 6.052083F;
            this.txtServiceDate18.Width = 0.84375F;
            // 
            // txtAge18
            // 
            this.txtAge18.Height = 0.1666667F;
            this.txtAge18.Left = 4.3125F;
            this.txtAge18.Name = "txtAge18";
            this.txtAge18.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAge18.Text = null;
            this.txtAge18.Top = 6.052083F;
            this.txtAge18.Width = 0.4479167F;
            // 
            // txtNew18
            // 
            this.txtNew18.Height = 0.1666667F;
            this.txtNew18.Left = 4.90625F;
            this.txtNew18.Name = "txtNew18";
            this.txtNew18.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtNew18.Text = null;
            this.txtNew18.Top = 6.052083F;
            this.txtNew18.Width = 0.5625F;
            // 
            // txtEstimate18
            // 
            this.txtEstimate18.Height = 0.1666667F;
            this.txtEstimate18.Left = 5.552083F;
            this.txtEstimate18.Name = "txtEstimate18";
            this.txtEstimate18.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtEstimate18.Text = null;
            this.txtEstimate18.Top = 6.052083F;
            this.txtEstimate18.Width = 0.5833333F;
            // 
            // txtEquipmentLocation18
            // 
            this.txtEquipmentLocation18.Height = 0.1666667F;
            this.txtEquipmentLocation18.Left = 6.291667F;
            this.txtEquipmentLocation18.Name = "txtEquipmentLocation18";
            this.txtEquipmentLocation18.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtEquipmentLocation18.Text = null;
            this.txtEquipmentLocation18.Top = 6.052083F;
            this.txtEquipmentLocation18.Width = 2.15625F;
            // 
            // txtTIF18
            // 
            this.txtTIF18.Height = 0.1666667F;
            this.txtTIF18.Left = 8.8125F;
            this.txtTIF18.Name = "txtTIF18";
            this.txtTIF18.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtTIF18.Text = null;
            this.txtTIF18.Top = 6.052083F;
            this.txtTIF18.Width = 0.2604167F;
            // 
            // txtBETEEligible18
            // 
            this.txtBETEEligible18.Height = 0.1666667F;
            this.txtBETEEligible18.Left = 9.1875F;
            this.txtBETEEligible18.Name = "txtBETEEligible18";
            this.txtBETEEligible18.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtBETEEligible18.Text = null;
            this.txtBETEEligible18.Top = 6.052083F;
            this.txtBETEEligible18.Width = 0.3229167F;
            // 
            // txtAssessment18
            // 
            this.txtAssessment18.Height = 0.1666667F;
            this.txtAssessment18.Left = 9.625F;
            this.txtAssessment18.Name = "txtAssessment18";
            this.txtAssessment18.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAssessment18.Text = null;
            this.txtAssessment18.Top = 6.052083F;
            this.txtAssessment18.Width = 0.5625F;
            // 
            // Line68
            // 
            this.Line68.Height = 0F;
            this.Line68.Left = 0F;
            this.Line68.LineWeight = 1F;
            this.Line68.Name = "Line68";
            this.Line68.Top = 6.479167F;
            this.Line68.Width = 8.5F;
            this.Line68.X1 = 0F;
            this.Line68.X2 = 8.5F;
            this.Line68.Y1 = 6.479167F;
            this.Line68.Y2 = 6.479167F;
            // 
            // Line69
            // 
            this.Line69.Height = 0F;
            this.Line69.Left = 8.75F;
            this.Line69.LineWeight = 1F;
            this.Line69.Name = "Line69";
            this.Line69.Top = 6.479167F;
            this.Line69.Width = 1.48958F;
            this.Line69.X1 = 8.75F;
            this.Line69.X2 = 10.23958F;
            this.Line69.Y1 = 6.479167F;
            this.Line69.Y2 = 6.479167F;
            // 
            // txtDescription19
            // 
            this.txtDescription19.Height = 0.1666667F;
            this.txtDescription19.Left = 0.08333334F;
            this.txtDescription19.Name = "txtDescription19";
            this.txtDescription19.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtDescription19.Text = null;
            this.txtDescription19.Top = 6.28125F;
            this.txtDescription19.Width = 3.208333F;
            // 
            // txtServiceDate19
            // 
            this.txtServiceDate19.Height = 0.1666667F;
            this.txtServiceDate19.Left = 3.395833F;
            this.txtServiceDate19.Name = "txtServiceDate19";
            this.txtServiceDate19.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtServiceDate19.Text = null;
            this.txtServiceDate19.Top = 6.28125F;
            this.txtServiceDate19.Width = 0.84375F;
            // 
            // txtAge19
            // 
            this.txtAge19.Height = 0.1666667F;
            this.txtAge19.Left = 4.3125F;
            this.txtAge19.Name = "txtAge19";
            this.txtAge19.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAge19.Text = null;
            this.txtAge19.Top = 6.28125F;
            this.txtAge19.Width = 0.4479167F;
            // 
            // txtNew19
            // 
            this.txtNew19.Height = 0.1666667F;
            this.txtNew19.Left = 4.90625F;
            this.txtNew19.Name = "txtNew19";
            this.txtNew19.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtNew19.Text = null;
            this.txtNew19.Top = 6.28125F;
            this.txtNew19.Width = 0.5625F;
            // 
            // txtEstimate19
            // 
            this.txtEstimate19.Height = 0.1666667F;
            this.txtEstimate19.Left = 5.552083F;
            this.txtEstimate19.Name = "txtEstimate19";
            this.txtEstimate19.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtEstimate19.Text = null;
            this.txtEstimate19.Top = 6.28125F;
            this.txtEstimate19.Width = 0.5833333F;
            // 
            // txtEquipmentLocation19
            // 
            this.txtEquipmentLocation19.Height = 0.1666667F;
            this.txtEquipmentLocation19.Left = 6.291667F;
            this.txtEquipmentLocation19.Name = "txtEquipmentLocation19";
            this.txtEquipmentLocation19.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtEquipmentLocation19.Text = null;
            this.txtEquipmentLocation19.Top = 6.28125F;
            this.txtEquipmentLocation19.Width = 2.15625F;
            // 
            // txtTIF19
            // 
            this.txtTIF19.Height = 0.1666667F;
            this.txtTIF19.Left = 8.8125F;
            this.txtTIF19.Name = "txtTIF19";
            this.txtTIF19.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtTIF19.Text = null;
            this.txtTIF19.Top = 6.28125F;
            this.txtTIF19.Width = 0.2604167F;
            // 
            // txtBETEEligible19
            // 
            this.txtBETEEligible19.Height = 0.1666667F;
            this.txtBETEEligible19.Left = 9.1875F;
            this.txtBETEEligible19.Name = "txtBETEEligible19";
            this.txtBETEEligible19.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtBETEEligible19.Text = null;
            this.txtBETEEligible19.Top = 6.28125F;
            this.txtBETEEligible19.Width = 0.3229167F;
            // 
            // txtAssessment19
            // 
            this.txtAssessment19.Height = 0.1666667F;
            this.txtAssessment19.Left = 9.625F;
            this.txtAssessment19.Name = "txtAssessment19";
            this.txtAssessment19.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAssessment19.Text = null;
            this.txtAssessment19.Top = 6.28125F;
            this.txtAssessment19.Width = 0.5625F;
            // 
            // Line26
            // 
            this.Line26.Height = 0F;
            this.Line26.Left = 0F;
            this.Line26.LineWeight = 1F;
            this.Line26.Name = "Line26";
            this.Line26.Top = 2.354167F;
            this.Line26.Width = 8.5F;
            this.Line26.X1 = 0F;
            this.Line26.X2 = 8.5F;
            this.Line26.Y1 = 2.354167F;
            this.Line26.Y2 = 2.354167F;
            // 
            // Line27
            // 
            this.Line27.Height = 0F;
            this.Line27.Left = 8.75F;
            this.Line27.LineWeight = 1F;
            this.Line27.Name = "Line27";
            this.Line27.Top = 2.354167F;
            this.Line27.Width = 1.48958F;
            this.Line27.X1 = 8.75F;
            this.Line27.X2 = 10.23958F;
            this.Line27.Y1 = 2.354167F;
            this.Line27.Y2 = 2.354167F;
            // 
            // Label19
            // 
            this.Label19.Height = 0.4895833F;
            this.Label19.HyperLink = null;
            this.Label19.Left = 0.04166667F;
            this.Label19.Name = "Label19";
            this.Label19.Style = "font-size: 7pt";
            this.Label19.Text = resources.GetString("Label19.Text");
            this.Label19.Top = 6.854167F;
            this.Label19.Width = 4.614583F;
            // 
            // Label20
            // 
            this.Label20.Height = 0.1875F;
            this.Label20.HyperLink = null;
            this.Label20.Left = 0.04166667F;
            this.Label20.Name = "Label20";
            this.Label20.Style = "font-size: 8.5pt; font-weight: bold";
            this.Label20.Text = "Applicant";
            this.Label20.Top = 7.364583F;
            this.Label20.Width = 0.7291667F;
            // 
            // Label21
            // 
            this.Label21.Height = 0.1875F;
            this.Label21.HyperLink = null;
            this.Label21.Left = 0.04166667F;
            this.Label21.Name = "Label21";
            this.Label21.Style = "font-size: 8.5pt; font-weight: bold";
            this.Label21.Text = "Preparer";
            this.Label21.Top = 7.677083F;
            this.Label21.Width = 0.7291667F;
            // 
            // Line28
            // 
            this.Line28.Height = 0F;
            this.Line28.Left = 0.65625F;
            this.Line28.LineWeight = 1F;
            this.Line28.Name = "Line28";
            this.Line28.Top = 7.520833F;
            this.Line28.Width = 2.916667F;
            this.Line28.X1 = 0.65625F;
            this.Line28.X2 = 3.572917F;
            this.Line28.Y1 = 7.520833F;
            this.Line28.Y2 = 7.520833F;
            // 
            // Line29
            // 
            this.Line29.Height = 0F;
            this.Line29.Left = 0.65625F;
            this.Line29.LineWeight = 1F;
            this.Line29.Name = "Line29";
            this.Line29.Top = 7.822917F;
            this.Line29.Width = 2.916667F;
            this.Line29.X1 = 0.65625F;
            this.Line29.X2 = 3.572917F;
            this.Line29.Y1 = 7.822917F;
            this.Line29.Y2 = 7.822917F;
            // 
            // Label22
            // 
            this.Label22.Height = 0.4895833F;
            this.Label22.HyperLink = null;
            this.Label22.Left = 5.3125F;
            this.Label22.Name = "Label22";
            this.Label22.Style = "font-size: 7pt";
            this.Label22.Text = resources.GetString("Label22.Text");
            this.Label22.Top = 6.854167F;
            this.Label22.Width = 4.104167F;
            // 
            // Label23
            // 
            this.Label23.Height = 0.1875F;
            this.Label23.HyperLink = null;
            this.Label23.Left = 3.666667F;
            this.Label23.Name = "Label23";
            this.Label23.Style = "font-size: 8.5pt; font-weight: bold";
            this.Label23.Text = "Date";
            this.Label23.Top = 7.364583F;
            this.Label23.Width = 0.3541667F;
            // 
            // lblDate
            // 
            this.lblDate.Height = 0.1875F;
            this.lblDate.HyperLink = null;
            this.lblDate.Left = 3.666667F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "font-size: 8.5pt; font-weight: bold";
            this.lblDate.Text = "Date";
            this.lblDate.Top = 7.677083F;
            this.lblDate.Width = 0.3541667F;
            // 
            // Line30
            // 
            this.Line30.Height = 0F;
            this.Line30.Left = 4.03125F;
            this.Line30.LineWeight = 1F;
            this.Line30.Name = "Line30";
            this.Line30.Top = 7.520833F;
            this.Line30.Width = 0.84375F;
            this.Line30.X1 = 4.03125F;
            this.Line30.X2 = 4.875F;
            this.Line30.Y1 = 7.520833F;
            this.Line30.Y2 = 7.520833F;
            // 
            // Line31
            // 
            this.Line31.Height = 0F;
            this.Line31.Left = 4.03125F;
            this.Line31.LineWeight = 1F;
            this.Line31.Name = "Line31";
            this.Line31.Top = 7.822917F;
            this.Line31.Width = 0.84375F;
            this.Line31.X1 = 4.03125F;
            this.Line31.X2 = 4.875F;
            this.Line31.Y1 = 7.822917F;
            this.Line31.Y2 = 7.822917F;
            // 
            // Label26
            // 
            this.Label26.Height = 0.1875F;
            this.Label26.HyperLink = null;
            this.Label26.Left = 5.291667F;
            this.Label26.Name = "Label26";
            this.Label26.Style = "font-size: 8.5pt; font-weight: bold";
            this.Label26.Text = "Date";
            this.Label26.Top = 7.677083F;
            this.Label26.Width = 0.3541667F;
            // 
            // Line32
            // 
            this.Line32.Height = 0F;
            this.Line32.Left = 7.96875F;
            this.Line32.LineWeight = 1F;
            this.Line32.Name = "Line32";
            this.Line32.Top = 7.520833F;
            this.Line32.Width = 2.21875F;
            this.Line32.X1 = 7.96875F;
            this.Line32.X2 = 10.1875F;
            this.Line32.Y1 = 7.520833F;
            this.Line32.Y2 = 7.520833F;
            // 
            // Line33
            // 
            this.Line33.Height = 0F;
            this.Line33.Left = 5.65625F;
            this.Line33.LineWeight = 1F;
            this.Line33.Name = "Line33";
            this.Line33.Top = 7.822917F;
            this.Line33.Width = 1.46875F;
            this.Line33.X1 = 5.65625F;
            this.Line33.X2 = 7.125F;
            this.Line33.Y1 = 7.822917F;
            this.Line33.Y2 = 7.822917F;
            // 
            // Label27
            // 
            this.Label27.Height = 0.1875F;
            this.Label27.HyperLink = null;
            this.Label27.Left = 5.291667F;
            this.Label27.Name = "Label27";
            this.Label27.Style = "font-size: 8.5pt; font-weight: bold";
            this.Label27.Text = "Assessor (or Agent of the Assessor) Signature";
            this.Label27.Top = 7.364583F;
            this.Label27.Width = 2.854167F;
            // 
            // Line70
            // 
            this.Line70.Height = 0F;
            this.Line70.Left = 0F;
            this.Line70.LineWeight = 1F;
            this.Line70.Name = "Line70";
            this.Line70.Top = 6.708333F;
            this.Line70.Width = 8.5F;
            this.Line70.X1 = 0F;
            this.Line70.X2 = 8.5F;
            this.Line70.Y1 = 6.708333F;
            this.Line70.Y2 = 6.708333F;
            // 
            // Line71
            // 
            this.Line71.Height = 0F;
            this.Line71.Left = 8.75F;
            this.Line71.LineWeight = 1F;
            this.Line71.Name = "Line71";
            this.Line71.Top = 6.708333F;
            this.Line71.Width = 1.48958F;
            this.Line71.X1 = 8.75F;
            this.Line71.X2 = 10.23958F;
            this.Line71.Y1 = 6.708333F;
            this.Line71.Y2 = 6.708333F;
            // 
            // txtDescription20
            // 
            this.txtDescription20.Height = 0.1666667F;
            this.txtDescription20.Left = 0.08333334F;
            this.txtDescription20.Name = "txtDescription20";
            this.txtDescription20.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtDescription20.Text = null;
            this.txtDescription20.Top = 6.510417F;
            this.txtDescription20.Width = 3.208333F;
            // 
            // txtServiceDate20
            // 
            this.txtServiceDate20.Height = 0.1666667F;
            this.txtServiceDate20.Left = 3.395833F;
            this.txtServiceDate20.Name = "txtServiceDate20";
            this.txtServiceDate20.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtServiceDate20.Text = null;
            this.txtServiceDate20.Top = 6.510417F;
            this.txtServiceDate20.Width = 0.84375F;
            // 
            // txtAge20
            // 
            this.txtAge20.Height = 0.1666667F;
            this.txtAge20.Left = 4.3125F;
            this.txtAge20.Name = "txtAge20";
            this.txtAge20.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAge20.Text = null;
            this.txtAge20.Top = 6.510417F;
            this.txtAge20.Width = 0.4479167F;
            // 
            // txtNew20
            // 
            this.txtNew20.Height = 0.1666667F;
            this.txtNew20.Left = 4.90625F;
            this.txtNew20.Name = "txtNew20";
            this.txtNew20.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtNew20.Text = null;
            this.txtNew20.Top = 6.510417F;
            this.txtNew20.Width = 0.5625F;
            // 
            // txtEstimate20
            // 
            this.txtEstimate20.Height = 0.1666667F;
            this.txtEstimate20.Left = 5.552083F;
            this.txtEstimate20.Name = "txtEstimate20";
            this.txtEstimate20.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtEstimate20.Text = null;
            this.txtEstimate20.Top = 6.510417F;
            this.txtEstimate20.Width = 0.5833333F;
            // 
            // txtEquipmentLocation20
            // 
            this.txtEquipmentLocation20.Height = 0.1666667F;
            this.txtEquipmentLocation20.Left = 6.291667F;
            this.txtEquipmentLocation20.Name = "txtEquipmentLocation20";
            this.txtEquipmentLocation20.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
            this.txtEquipmentLocation20.Text = null;
            this.txtEquipmentLocation20.Top = 6.510417F;
            this.txtEquipmentLocation20.Width = 2.15625F;
            // 
            // txtTIF20
            // 
            this.txtTIF20.Height = 0.1666667F;
            this.txtTIF20.Left = 8.8125F;
            this.txtTIF20.Name = "txtTIF20";
            this.txtTIF20.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtTIF20.Text = null;
            this.txtTIF20.Top = 6.510417F;
            this.txtTIF20.Width = 0.2604167F;
            // 
            // txtBETEEligible20
            // 
            this.txtBETEEligible20.Height = 0.1666667F;
            this.txtBETEEligible20.Left = 9.1875F;
            this.txtBETEEligible20.Name = "txtBETEEligible20";
            this.txtBETEEligible20.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: center";
            this.txtBETEEligible20.Text = null;
            this.txtBETEEligible20.Top = 6.510417F;
            this.txtBETEEligible20.Width = 0.3229167F;
            // 
            // txtAssessment20
            // 
            this.txtAssessment20.Height = 0.1666667F;
            this.txtAssessment20.Left = 9.625F;
            this.txtAssessment20.Name = "txtAssessment20";
            this.txtAssessment20.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; text-align: right";
            this.txtAssessment20.Text = null;
            this.txtAssessment20.Top = 6.510417F;
            this.txtAssessment20.Width = 0.5625F;
            // 
            // Line72
            // 
            this.Line72.Height = 0.614583F;
            this.Line72.Left = 3.333333F;
            this.Line72.LineWeight = 2F;
            this.Line72.Name = "Line72";
            this.Line72.Top = 1.510417F;
            this.Line72.Width = 0F;
            this.Line72.X1 = 3.333333F;
            this.Line72.X2 = 3.333333F;
            this.Line72.Y1 = 1.510417F;
            this.Line72.Y2 = 2.125F;
            // 
            // Line73
            // 
            this.Line73.Height = 0.614583F;
            this.Line73.Left = 4.25F;
            this.Line73.LineWeight = 2F;
            this.Line73.Name = "Line73";
            this.Line73.Top = 1.510417F;
            this.Line73.Width = 0F;
            this.Line73.X1 = 4.25F;
            this.Line73.X2 = 4.25F;
            this.Line73.Y1 = 1.510417F;
            this.Line73.Y2 = 2.125F;
            // 
            // Line74
            // 
            this.Line74.Height = 0.614583F;
            this.Line74.Left = 4.833333F;
            this.Line74.LineWeight = 2F;
            this.Line74.Name = "Line74";
            this.Line74.Top = 1.510417F;
            this.Line74.Width = 0F;
            this.Line74.X1 = 4.833333F;
            this.Line74.X2 = 4.833333F;
            this.Line74.Y1 = 1.510417F;
            this.Line74.Y2 = 2.125F;
            // 
            // Line75
            // 
            this.Line75.Height = 0.614583F;
            this.Line75.Left = 5.5F;
            this.Line75.LineWeight = 2F;
            this.Line75.Name = "Line75";
            this.Line75.Top = 1.510417F;
            this.Line75.Width = 0F;
            this.Line75.X1 = 5.5F;
            this.Line75.X2 = 5.5F;
            this.Line75.Y1 = 1.510417F;
            this.Line75.Y2 = 2.125F;
            // 
            // Line76
            // 
            this.Line76.Height = 0.614583F;
            this.Line76.Left = 6.166667F;
            this.Line76.LineWeight = 2F;
            this.Line76.Name = "Line76";
            this.Line76.Top = 1.510417F;
            this.Line76.Width = 0F;
            this.Line76.X1 = 6.166667F;
            this.Line76.X2 = 6.166667F;
            this.Line76.Y1 = 1.510417F;
            this.Line76.Y2 = 2.125F;
            // 
            // Line77
            // 
            this.Line77.Height = 0.614583F;
            this.Line77.Left = 8.5F;
            this.Line77.LineWeight = 2F;
            this.Line77.Name = "Line77";
            this.Line77.Top = 1.510417F;
            this.Line77.Width = 0F;
            this.Line77.X1 = 8.5F;
            this.Line77.X2 = 8.5F;
            this.Line77.Y1 = 1.510417F;
            this.Line77.Y2 = 2.125F;
            // 
            // Line78
            // 
            this.Line78.Height = 0.895833F;
            this.Line78.Left = 8.75F;
            this.Line78.LineWeight = 2F;
            this.Line78.Name = "Line78";
            this.Line78.Top = 1.229167F;
            this.Line78.Width = 0F;
            this.Line78.X1 = 8.75F;
            this.Line78.X2 = 8.75F;
            this.Line78.Y1 = 1.229167F;
            this.Line78.Y2 = 2.125F;
            // 
            // Line79
            // 
            this.Line79.Height = 0.614583F;
            this.Line79.Left = 9.125F;
            this.Line79.LineWeight = 2F;
            this.Line79.Name = "Line79";
            this.Line79.Top = 1.510417F;
            this.Line79.Width = 0F;
            this.Line79.X1 = 9.125F;
            this.Line79.X2 = 9.125F;
            this.Line79.Y1 = 1.510417F;
            this.Line79.Y2 = 2.125F;
            // 
            // Line80
            // 
            this.Line80.Height = 0.614583F;
            this.Line80.Left = 9.5625F;
            this.Line80.LineWeight = 2F;
            this.Line80.Name = "Line80";
            this.Line80.Top = 1.510417F;
            this.Line80.Width = 0F;
            this.Line80.X1 = 9.5625F;
            this.Line80.X2 = 9.5625F;
            this.Line80.Y1 = 1.510417F;
            this.Line80.Y2 = 2.125F;
            // 
            // Line81
            // 
            this.Line81.Height = 0.895833F;
            this.Line81.Left = 10.23958F;
            this.Line81.LineWeight = 2F;
            this.Line81.Name = "Line81";
            this.Line81.Top = 1.229167F;
            this.Line81.Width = 0F;
            this.Line81.X1 = 10.23958F;
            this.Line81.X2 = 10.23958F;
            this.Line81.Y1 = 1.229167F;
            this.Line81.Y2 = 2.125F;
            // 
            // Label28
            // 
            this.Label28.Height = 0.40625F;
            this.Label28.HyperLink = null;
            this.Label28.Left = 8.791667F;
            this.Label28.Name = "Label28";
            this.Label28.Style = "font-size: 7pt; font-weight: bold; text-align: center";
            this.Label28.Text = "TIF (Y/N)";
            this.Label28.Top = 1.53125F;
            this.Label28.Width = 0.2916667F;
            // 
            // Label29
            // 
            this.Label29.Height = 0.53125F;
            this.Label29.HyperLink = null;
            this.Label29.Left = 9.15F;
            this.Label29.Name = "Label29";
            this.Label29.Style = "font-size: 7pt; font-weight: bold; text-align: center";
            this.Label29.Text = "BETE Eligible (Y/N)";
            this.Label29.Top = 1.53125F;
            this.Label29.Width = 0.4F;
            // 
            // Label30
            // 
            this.Label30.Height = 0.40625F;
            this.Label30.HyperLink = null;
            this.Label30.Left = 9.65625F;
            this.Label30.Name = "Label30";
            this.Label30.Style = "font-size: 7pt; font-weight: bold; text-align: center";
            this.Label30.Text = "Assessed Value";
            this.Label30.Top = 1.53125F;
            this.Label30.Width = 0.53075F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1458333F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 9.208333F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-size: 8.5pt; font-weight: bold";
            this.lblPage.Text = "Page";
            this.lblPage.Top = 7.677083F;
            this.lblPage.Width = 0.875F;
            // 
            // BETE
            // 
            this.MasterReport = false;
            this.Name = "BETE";
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.375F;
            this.PageSettings.Margins.Right = 0.375F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 8.5F;
            this.PageSettings.PaperWidth = 11F;
            this.PrintWidth = 10.24931F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.Detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBusiness)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOwner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTypeOfBusiness)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDate20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNew20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstimate20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentLocation20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIF20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETEEligible20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAssessment20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtServiceDate1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAge1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNew1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEstimate1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEquipmentLocation1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTIF1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBETEEligible1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBusiness;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOwner;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTypeOfBusiness;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line8;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line9;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line10;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line11;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line12;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line13;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line14;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line34;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line35;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtServiceDate2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAge2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNew2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEstimate2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEquipmentLocation2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTIF2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBETEEligible2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line36;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line37;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtServiceDate3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAge3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNew3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEstimate3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEquipmentLocation3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTIF3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBETEEligible3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line38;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line39;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtServiceDate4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAge4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNew4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEstimate4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEquipmentLocation4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTIF4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBETEEligible4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment4;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line40;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line41;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtServiceDate5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAge5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNew5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEstimate5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEquipmentLocation5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTIF5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBETEEligible5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment5;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line42;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line43;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtServiceDate6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAge6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNew6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEstimate6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEquipmentLocation6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTIF6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBETEEligible6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment6;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line44;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line45;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtServiceDate7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAge7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNew7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEstimate7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEquipmentLocation7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTIF7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBETEEligible7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment7;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line46;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line47;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtServiceDate8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAge8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNew8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEstimate8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEquipmentLocation8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTIF8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBETEEligible8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment8;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line48;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line49;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtServiceDate9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAge9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNew9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEstimate9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEquipmentLocation9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTIF9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBETEEligible9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment9;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line50;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line51;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtServiceDate10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAge10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNew10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEstimate10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEquipmentLocation10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTIF10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBETEEligible10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment10;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line52;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line53;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtServiceDate11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAge11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNew11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEstimate11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEquipmentLocation11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTIF11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBETEEligible11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment11;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line54;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line55;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtServiceDate12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAge12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNew12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEstimate12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEquipmentLocation12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTIF12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBETEEligible12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment12;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line56;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line57;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtServiceDate13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAge13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNew13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEstimate13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEquipmentLocation13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTIF13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBETEEligible13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment13;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line58;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line59;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtServiceDate14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAge14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNew14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEstimate14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEquipmentLocation14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTIF14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBETEEligible14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment14;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line60;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line61;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtServiceDate15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAge15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNew15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEstimate15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEquipmentLocation15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTIF15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBETEEligible15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment15;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line62;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line63;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtServiceDate16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAge16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNew16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEstimate16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEquipmentLocation16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTIF16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBETEEligible16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment16;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line64;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line65;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtServiceDate17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAge17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNew17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEstimate17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEquipmentLocation17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTIF17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBETEEligible17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment17;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line66;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line67;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtServiceDate18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAge18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNew18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEstimate18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEquipmentLocation18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTIF18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBETEEligible18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment18;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line68;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line69;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtServiceDate19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAge19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNew19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEstimate19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEquipmentLocation19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTIF19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBETEEligible19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment19;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line26;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line27;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line28;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line29;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line30;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line31;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line32;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line33;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line70;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line71;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtServiceDate20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAge20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNew20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEstimate20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEquipmentLocation20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTIF20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBETEEligible20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment20;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line72;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line73;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line74;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line75;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line76;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line77;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line78;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line79;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line80;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line81;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
	}
}
