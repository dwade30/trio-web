﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;
using System.IO;
using fecherFoundation.VisualBasicLayer;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmTrendExempt.
	/// </summary>
	public partial class frmTrendExempt : BaseForm
	{
		public frmTrendExempt()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmTrendExempt InstancePtr
		{
			get
			{
				return (frmTrendExempt)Sys.GetInstance(typeof(frmTrendExempt));
			}
		}

		protected frmTrendExempt _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		const int CNSTGRIDTRENDCOLid = 0;
		const int CNSTGRIDTRENDCOLYEAR = 1;
		const int CNSTGRIDTRENDCOLTREND = 2;
		const int CNSTGRIDTRENDCOLCAPRATE = 3;
		const int CNSTGRIDEXEMPTCOLCODE = 0;
		const int CNSTGRIDEXEMPTCOLAMOUNT = 1;
		const int CNSTGRIDEXEMPTCOLDESCRIPTION = 2;
		const int CNSTGRIDEXEMPTCOLMVRCATEGORY = 3;
		//CRAXDRT.Database pp = new CRAXDRT.Database();
		private void ResizeGrids()
		{
			ResizeGridTrend();
			ResizeGridExempt();
		}

		private void SetupGridTrend()
		{
			gridTrend.Rows = 1;
			gridTrend.ColHidden(CNSTGRIDTRENDCOLid, true);
			gridTrend.TextMatrix(0, CNSTGRIDTRENDCOLCAPRATE, "Cap Rate");
			gridTrend.TextMatrix(0, CNSTGRIDTRENDCOLTREND, "Trend");
			gridTrend.TextMatrix(0, CNSTGRIDTRENDCOLYEAR, "Year");
			gridTrend.ColAlignment(CNSTGRIDTRENDCOLCAPRATE, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			gridTrend.ColAlignment(CNSTGRIDTRENDCOLTREND, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			gridTrend.ColAlignment(CNSTGRIDTRENDCOLYEAR, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		}

		private void SetupGridExempt()
		{
			GridExempt.Rows = 1;
			GridExempt.TextMatrix(0, CNSTGRIDEXEMPTCOLCODE, "Code");
			GridExempt.TextMatrix(0, CNSTGRIDEXEMPTCOLAMOUNT, "Amount");
			GridExempt.TextMatrix(0, CNSTGRIDEXEMPTCOLDESCRIPTION, "Description");
			GridExempt.TextMatrix(0, CNSTGRIDEXEMPTCOLMVRCATEGORY, "MVR Category");
		}

		private void ResizeGridExempt()
		{
			int GridWidth = 0;
			GridWidth = GridExempt.WidthOriginal;
			GridExempt.ColWidth(CNSTGRIDEXEMPTCOLCODE, FCConvert.ToInt32(0.15 * GridWidth));
			GridExempt.ColWidth(CNSTGRIDEXEMPTCOLAMOUNT, FCConvert.ToInt32(0.15 * GridWidth));
			GridExempt.ColWidth(CNSTGRIDEXEMPTCOLMVRCATEGORY, FCConvert.ToInt32(0.4 * GridWidth));
		}

		private void LoadGridExempt()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				int lngRow;
				rsLoad.OpenRecordset("select * from exemptcodes order by code", modPPGN.strPPDatabase);
				GridExempt.Rows = 1;
				while (!rsLoad.EndOfFile())
				{
					GridExempt.Rows += 1;
					//FC:FINAL:CHN - issue #1453: Set BackColor on grayed out column.
					GridExempt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, GridExempt.Rows - 1, 0, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
					lngRow = GridExempt.Rows - 1;
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					GridExempt.TextMatrix(lngRow, CNSTGRIDEXEMPTCOLCODE, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("code"))));
					// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
					GridExempt.TextMatrix(lngRow, CNSTGRIDEXEMPTCOLAMOUNT, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("amount"))));
					GridExempt.TextMatrix(lngRow, CNSTGRIDEXEMPTCOLDESCRIPTION, FCConvert.ToString(rsLoad.Get_Fields_String("description")));
					GridExempt.TextMatrix(lngRow, CNSTGRIDEXEMPTCOLMVRCATEGORY, FCConvert.ToString(rsLoad.Get_Fields_String("mvrcategory")));
					GridExempt.ColComboList(CNSTGRIDEXEMPTCOLMVRCATEGORY, " |Snowmobile Trail Grooming Equipment|Solar and Wind Energy Equipment");
					GridExempt.RowData(lngRow, false);
					rsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In LoadGridExempt", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ResizeGridTrend()
		{
			int GridWidth = 0;
			GridWidth = gridTrend.WidthOriginal;
			gridTrend.ColWidth(CNSTGRIDTRENDCOLYEAR, FCConvert.ToInt32(0.2 * GridWidth));
			gridTrend.ColWidth(CNSTGRIDTRENDCOLTREND, FCConvert.ToInt32(0.25 * GridWidth));
		}

		private void frmTrendExempt_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmTrendExempt_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				frmTrendExempt.InstancePtr.Close();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmTrendExempt_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTrendExempt properties;
			//frmTrendExempt.ScaleWidth	= 9045;
			//frmTrendExempt.ScaleHeight	= 7110;
			//frmTrendExempt.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGridTrend();
			LoadGridTrend();
			SetupGridExempt();
			LoadGridExempt();
		}

		private void frmTrendExempt_Resize(object sender, System.EventArgs e)
		{
			ResizeGrids();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			modGlobal.SaveWindowSize(this);
			// Call UnlockAllRecordsByUser
		}

		private void GridExempt_EnterCell(object sender, System.EventArgs e)
		{
			//FC:FINAL:AM:#3684 - no need for edit mask
            //int Col;
			//Col = GridExempt.Col;
			//switch (Col)
			//{
			//	case CNSTGRIDEXEMPTCOLDESCRIPTION:
			//		{
			//			GridExempt.EditMask = "??????????????????????";
			//			//"??????????????????????;q; ";
			//			break;
			//		}
			//	default:
			//		{
			//			GridExempt.EditMask = "";
			//			break;
			//		}
			//}
			//FC:FINAL:DDU:#i1411 - moved change of text here
			GridExempt.RowData(GridExempt.Row, true);
			//end switch
		}

		private void GridExempt_KeyDownEdit(object sender, KeyEventArgs e)
		{
			GridExempt.RowData(GridExempt.Row, true);
		}

		private void gridTrend_KeyDownEdit(object sender, KeyEventArgs e)
		{
			gridTrend.RowData(gridTrend.Row, true);
		}

		private void gridTrend_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = gridTrend.GetFlexRowIndex(e.RowIndex);
			int col = gridTrend.GetFlexColIndex(e.ColumnIndex);
			if (col == CNSTGRIDTRENDCOLTREND)
			{
				if (Conversion.Val(gridTrend.EditText) < 1)
				{
					e.Cancel = true;
				}
			}
			//FC:FINAL:DDU:#i1411 - moved change of text here
			gridTrend.RowData(row, true);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			frmTrendExempt.InstancePtr.Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuExport_Click(object sender, System.EventArgs e)
		{
			string strFile = "";
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsDRWrapper clsSave = new clsDRWrapper();
			string[] strAry = null;
			try
			{
				//FC:FINAL:MSH - i.issue #1360: Save dialog not implemented
				// On Error GoTo ErrorHandler
				// MDIParent.InstancePtr.CommonDialog1.Flags = vbPorterConverter.cdlOFNExplorer+vbPorterConverter.cdlOFNNoChangeDir+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNCreatePrompt	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//FC:FINAL:DDU:#i1291 - fixed null exception
				//if (MDIParent.InstancePtr.CommonDialog1 == null)
				//{
				//	MDIParent.InstancePtr.CommonDialog1 = new FCCommonDialog();
				//}
				//MDIParent.InstancePtr.CommonDialog1.DialogTitle = "Export File To Create";
				// .FileTitle = "Choose a directory and a name for the file to export"
				//MDIParent.InstancePtr.CommonDialog1.InitDir = FCFileSystem.CurDir();
				//MDIParent.InstancePtr.CommonDialog1.FileName = "";
				//MDIParent.InstancePtr.CommonDialog1.Filter = "TRIO Export files  (*.JSON)|*.JSON";
				//MDIParent.InstancePtr.CommonDialog1.DefaultExt = "JSON";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				//MDIParent.InstancePtr.CommonDialog1.ShowSave();
				//strFile = MDIParent.InstancePtr.CommonDialog1.FileName;
				string tempFileName = Path.Combine(FCFileSystem.Statics.UserDataFolder, "Temp", "ExportTrendRates_" + Path.GetRandomFileName());
				tempFileName = Path.ChangeExtension(tempFileName, "JSON");
				if (!Directory.Exists(Path.GetDirectoryName(tempFileName)))
				{
					Directory.CreateDirectory(Path.GetDirectoryName(tempFileName));
				}
				strFile = tempFileName;
				if (strFile != string.Empty)
				{
					strAry = Strings.Split(strFile, ".", -1, CompareConstants.vbTextCompare);
					// force the file extension
					if (Information.UBound(strAry, 1) < 1)
					{
						strFile += ".json";
					}
					else
					{
						strFile = strAry[0] + ".json";
					}
					if (File.Exists(strFile))
					{
						File.Delete(strFile);
					}
					if (CreateExportDB(ref strFile))
					{
						//FC:FINAL:MSH - i.issue #1360: save file on client side
						using (FileStream downloadStream = new FileStream(strFile, FileMode.Open))
						{
							FCUtils.Download(downloadStream, Path.GetFileName(strFile));
						}
						MessageBox.Show("Export File Created", "File Exported", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (Information.Err().Number == 32755)
				{
					// cancel error
					return;
				}
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In mnuExport_click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool FillExportDB(ref string strDBPath, ref string strDBFile)
		{
			bool FillExportDB = false;
			TableCollection tTabColl = new TableCollection();
			tTabColl.DBName = "TrendCapRates";
			TableItem tTabItem = new TableItem();
			RecordItem tRec = new RecordItem();
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsDRWrapper clsSave = new clsDRWrapper();
			StreamWriter ts;
			FillExportDB = false;
			// Call clsSave.Execute("delete from trendcaprates", strDBPath & strDBFile)
			clsLoad.OpenRecordset("select * from trendcaprates order by [year]", modPPGN.strPPDatabase);
			tTabItem = clsLoad.TableToTableItem();
			tTabItem.TableName = "TrendCapRates";
			tTabColl.AddItem(tTabItem);
			ts = File.CreateText(strDBPath + strDBFile);
			GenericJSONTableSerializer tSer = new GenericJSONTableSerializer();
			ts.Write(tSer.GetJSONFromCollection(ref tTabColl));
			ts.Close();
			FillExportDB = true;
			return FillExportDB;
			ErrorHandler:
			;
			MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err().Number) + " " + Information.Err().Description + "\r\n" + "In FillExportDB", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			return FillExportDB;
		}

		private bool CreateExportDB(ref string strFile)
		{
			bool CreateExportDB = false;
			string strDBPath;
			string strDBFile;
			clsDRWrapper clsCreate = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				CreateExportDB = false;
				strDBPath = "";
				strDBFile = "";
				strDBPath = Path.GetDirectoryName(strFile);
				if (Strings.Right(strDBPath, 1) != "\\")
				{
					strDBPath += "\\";
				}
				strDBFile = Path.GetFileName(strFile);
				if (FillExportDB(ref strDBPath, ref strDBFile))
				{
					CreateExportDB = true;
				}
				return CreateExportDB;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In CreateExportDB", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateExportDB;
		}

		private void mnuImport_Click(object sender, System.EventArgs e)
		{
			string strFile = "";
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsDRWrapper clsSave = new clsDRWrapper();
			clsDRWrapper rsSave = new clsDRWrapper();
			string strDBPath = "";
			string strDBFile = "";
			StreamReader ts = null;
			int x;
			try
			{
				// On Error GoTo ErrorHandler
				// MDIParent.InstancePtr.CommonDialog1.Flags = vbPorterConverter.cdlOFNExplorer+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNNoChangeDir+vbPorterConverter.cdlOFNFileMustExist	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//FC:FINAL:DDU:#i1291 - fixed null exception
				if (MDIParent.InstancePtr.CommonDialog1 == null)
				{
					MDIParent.InstancePtr.CommonDialog1 = new FCCommonDialog();
				}
				MDIParent.InstancePtr.CommonDialog1.Filter = "TRIO Export files  (*.JSON)|*.JSON";
				//FC:FINAL:CHN - issue #1413: Delete showing error on cancel select file.
				MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				MDIParent.InstancePtr.CommonDialog1.ShowOpen();
				strFile = MDIParent.InstancePtr.CommonDialog1.FileName;
				if (File.Exists(strFile))
				{
					strDBPath = Path.GetDirectoryName(strFile);
					if (Strings.Trim(strDBPath) != string.Empty)
					{
						if (Strings.Right(strDBPath, 1) != "")
						{
							strDBPath += "\\";
						}
					}
					strDBFile = Path.GetFileName(strFile);
					ts = File.OpenText(strFile);
					TableCollection tTabColl = new TableCollection();
					TableItem tTabItem = new TableItem();
					if (!(ts == null))
					{
						GenericJSONTableSerializer tSer = new GenericJSONTableSerializer();
						tTabColl = tSer.GetCollectionFromJSON(ts.ReadToEnd());
						if (!(tTabColl == null))
						{
							frmWait.InstancePtr.lblMessage.Text = "Importing Exemption Codes";
							frmWait.InstancePtr.lblMessage.Refresh();
							for (x = 0; x <= tTabColl.TableCount() - 1; x++)
							{
								if (Strings.LCase(tTabColl.TableName(x)) == "trendcaprates")
								{
									clsSave.Execute("delete from trendcaprates", modPPGN.strPPDatabase);
									clsSave.OpenRecordset("select top 1 * from trendcaprates", modPPGN.strPPDatabase);
									//FC:FINAL:BBE - function needs an assigning variable
									TableItem tTableItem = tTabColl.Item(tTabColl.TableName(x));
									clsSave.TableItemToTable(true, ref tTableItem, "ID");
									rsSave.OpenRecordset("select top 1 * from trendcaprates", modPPGN.strPPDatabase);
									clsSave.MoveFirst();
									while (!clsSave.EndOfFile())
									{
										rsSave.AddNew();
										// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
										rsSave.Set_Fields("Year", clsSave.Get_Fields("Year"));
										// TODO Get_Fields: Check the table for the column [trend] and replace with corresponding Get_Field method
										rsSave.Set_Fields("Trend", FCConvert.ToString(Conversion.Val(clsSave.Get_Fields("trend"))));
										rsSave.Set_Fields("CapRate", FCConvert.ToString(Conversion.Val(clsSave.Get_Fields_Double("caprate"))));
										rsSave.Update();
										clsSave.MoveNext();
									}
									break;
								}
							}
							// x
							LoadGridTrend();
							MessageBox.Show("Import Complete", "Imported", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
						else
						{
							MessageBox.Show("Could not find trend rate information", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
					}
					else
					{
						MessageBox.Show("Could not open file " + strFile, "Could not open file", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return;
					}
				}
				else
				{
					MessageBox.Show("Could not find file " + strFile, "No File", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (Information.Err().Number == 32755)
				{
					// cancel error
					return;
				}
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In mnuImport_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			finally
			{
				if (ts != null)
				{
					ts.Close();
				}
			}
		}

		private void LoadGridTrend()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				int lngRow;
				gridTrend.Rows = 1;
				rsLoad.OpenRecordset("select * from TRENDcaprates order by [year]", modPPGN.strPPDatabase);
				while (!rsLoad.EndOfFile())
				{
					gridTrend.Rows += 1;
					//FC:FINAL:CHN - issue #1453: Set BackColor on grayed out column.
					gridTrend.Cell(FCGrid.CellPropertySettings.flexcpBackColor, gridTrend.Rows - 1, 1, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
					lngRow = gridTrend.Rows - 1;
					gridTrend.TextMatrix(lngRow, CNSTGRIDTRENDCOLCAPRATE, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Double("caprate"))));
					// TODO Get_Fields: Check the table for the column [trend] and replace with corresponding Get_Field method
					gridTrend.TextMatrix(lngRow, CNSTGRIDTRENDCOLTREND, FCConvert.ToString(rsLoad.Get_Fields("trend")));
					// TODO Get_Fields: Check the table for the column [year] and replace with corresponding Get_Field method
					gridTrend.TextMatrix(lngRow, CNSTGRIDTRENDCOLYEAR, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("year"))));
					gridTrend.RowData(lngRow, false);
					rsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In LoadGridTrend", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool SaveGridTrend()
		{
			bool SaveGridTrend = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsSave = new clsDRWrapper();
				int lngRow;
				SaveGridTrend = false;
				if (modPPGN.Statics.strCostFiles != "N")
				{
					gridTrend.Col = 0;
					GridExempt.Row = 0;
					////Application.DoEvents();
					for (lngRow = 1; lngRow <= gridTrend.Rows - 1; lngRow++)
					{
						if (FCConvert.ToBoolean(gridTrend.RowData(lngRow)))
						{
							rsSave.Execute("update trendcaprates set trend = " + FCConvert.ToString(Conversion.Val(gridTrend.TextMatrix(lngRow, CNSTGRIDTRENDCOLTREND))) + ",caprate = " + FCConvert.ToString(Conversion.Val(gridTrend.TextMatrix(lngRow, CNSTGRIDTRENDCOLCAPRATE))) + " where [year] = " + FCConvert.ToString(Conversion.Val(gridTrend.TextMatrix(lngRow, CNSTGRIDTRENDCOLYEAR))), modPPGN.strPPDatabase);
							gridTrend.RowData(lngRow, false);
						}
					}
					// lngRow
					for (lngRow = 1; lngRow <= GridExempt.Rows - 1; lngRow++)
					{
						if (FCConvert.ToBoolean(GridExempt.RowData(lngRow)))
						{
							rsSave.Execute("update exemptcodes set [amount] = " + FCConvert.ToString(Conversion.Val(GridExempt.TextMatrix(lngRow, CNSTGRIDEXEMPTCOLAMOUNT))) + ", " +
							                                                 "description = '" + Strings.Replace(GridExempt.TextMatrix(lngRow, CNSTGRIDEXEMPTCOLDESCRIPTION), "'", "", 1, -1, CompareConstants.vbTextCompare) + "', " +
																			 "MVRCategory = '" + GridExempt.TextMatrix(lngRow, CNSTGRIDEXEMPTCOLMVRCATEGORY).Trim() + "' " +
							                                                 "where code = " + FCConvert.ToString(Conversion.Val(GridExempt.TextMatrix(lngRow, CNSTGRIDEXEMPTCOLCODE))), modPPGN.strPPDatabase);
							GridExempt.RowData(lngRow, false);
						}
					}
					// lngRow
					MessageBox.Show("Save Complete", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				SaveGridTrend = true;
				return SaveGridTrend;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SaveGridTrend", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveGridTrend;
		}

		private void mnuPrintExemptions_Click(object sender, System.EventArgs e)
		{
			rptTrendExemptions.InstancePtr.Init(2);
		}

		private void mnuPrintTrending_Click(object sender, System.EventArgs e)
		{
			rptTrends.InstancePtr.Init();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveGridTrend();
			// Call cmdSave_Click
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			// mnuSave_Click
			if (SaveGridTrend())
			{
				mnuExit_Click();
			}
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuSave_Click(mnuSave, EventArgs.Empty);
		}
	}
}
