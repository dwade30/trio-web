﻿//Fecher vbPorter - Version 1.0.0.32
using Global;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmSearching.
	/// </summary>
	partial class frmSearching : BaseForm
	{
		public fecherFoundation.FCProgressBar pb1;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSearching));
			this.pb1 = new fecherFoundation.FCProgressBar();
			this.Label1 = new fecherFoundation.FCLabel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 198);
			this.BottomPanel.Size = new System.Drawing.Size(349, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.pb1);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(349, 138);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(349, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(123, 30);
			this.HeaderText.Text = "Searching";
			// 
			// pb1
			// 
			this.pb1.Location = new System.Drawing.Point(148, 30);
			this.pb1.Name = "pb1";
			this.pb1.Size = new System.Drawing.Size(179, 19);
			this.pb1.TabIndex = 1;
			// 
			// Label1
			// 
			this.Label1.AutoSize = true;
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(81, 15);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "SEARCHING";
			// 
			// frmSearching
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(349, 306);
			this.FillColor = 0;
			this.FormBorderStyle = Wisej.Web.FormBorderStyle.None;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmSearching";
			this.ShowInTaskbar = false;
			this.Text = "Form1";
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
