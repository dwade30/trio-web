using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using SharedApplication.Extensions;
using Wisej.Web;

namespace TWPP0000
{
    /// <summary>
    /// Summary description for srptItemizedInventory.
    /// </summary>
    public partial class srptItemizedInventory : FCSectionReport
    {
        int lngAcct;
        clsDRWrapper rsReport = new/*AsNew*/ clsDRWrapper();
        double dblTotValue;
        double dblTotCost;
        double dblTotBETE;
        double[] dblCat = new double[9 + 1];
        double[] dblBETE = new double[9 + 1];
        string[] txtCat = new string[9 + 1];

        public srptItemizedInventory()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            this.FetchData += SrptItemizedInventory_FetchData;
            this.ReportStart += SrptItemizedInventory_ReportStart;
            this.detail.Format += Detail_Format;
            this.reportFooter1.Format += ReportFooter1_Format;
            
        }

        private void ReportFooter1_Format(object sender, EventArgs e)
        {
            txtTotalBETE.Text = Convert.ToInt32(dblTotBETE).FormatAsNumber();
            txtTotalCost.Text = Convert.ToInt32(dblTotCost).FormatAsNumber();
            txtTotalValue.Text = Convert.ToInt32(dblTotValue).FormatAsNumber();
            lblCat1.Text = ((rptInventoryListing)this.ParentReport).lblCat1.Text;
            lblCat2.Text = ((rptInventoryListing)this.ParentReport).lblCat2.Text;
            lblCat3.Text = ((rptInventoryListing)this.ParentReport).lblCat3.Text;
            lblCat4.Text = ((rptInventoryListing)this.ParentReport).lblCat4.Text;
            lblCat5.Text = ((rptInventoryListing)this.ParentReport).lblCat5.Text;
            lblCat6.Text = ((rptInventoryListing)this.ParentReport).lblCat6.Text;
            lblCat7.Text = ((rptInventoryListing)this.ParentReport).lblCat7.Text;
            lblCat8.Text = ((rptInventoryListing)this.ParentReport).lblCat8.Text;
            lblCat9.Text = ((rptInventoryListing)this.ParentReport).lblCat9.Text;
            txtTotCat1.Text = fecherFoundation.Strings.Format(dblCat[1], "#,###,###,##0");
            txtTotCat2.Text = fecherFoundation.Strings.Format(dblCat[2], "#,###,###,##0");
            txtTotCat3.Text = fecherFoundation.Strings.Format(dblCat[3], "#,###,###,##0");
            txtTotCat4.Text = fecherFoundation.Strings.Format(dblCat[4], "#,###,###,##0");
            txtTotCat5.Text = fecherFoundation.Strings.Format(dblCat[5], "#,###,###,##0");
            txtTotCat6.Text = fecherFoundation.Strings.Format(dblCat[6], "#,###,###,##0");
            txtTotCat7.Text = fecherFoundation.Strings.Format(dblCat[7], "#,###,###,##0");
            txtTotCat8.Text = fecherFoundation.Strings.Format(dblCat[8], "#,###,###,##0");
            txtTotCat9.Text = fecherFoundation.Strings.Format(dblCat[9], "#,###,###,##0");
            txtTotBETE1.Text = fecherFoundation.Strings.Format(dblBETE[1], "#,###,###,##0");
            txtTotBETE2.Text = fecherFoundation.Strings.Format(dblBETE[2], "#,###,###,##0");
            txtTotBETE3.Text = fecherFoundation.Strings.Format(dblBETE[3], "#,###,###,##0");
            txtTotBETE4.Text = fecherFoundation.Strings.Format(dblBETE[4], "#,###,###,##0");
            txtTotBETE5.Text = fecherFoundation.Strings.Format(dblBETE[5], "#,###,###,##0");
            txtTotBETE6.Text = fecherFoundation.Strings.Format(dblBETE[6], "#,###,###,##0");
            txtTotBETE7.Text = fecherFoundation.Strings.Format(dblBETE[7], "#,###,###,##0");
            txtTotBETE8.Text = fecherFoundation.Strings.Format(dblBETE[8], "#,###,###,##0");
            txtTotBETE9.Text = fecherFoundation.Strings.Format(dblBETE[9], "#,###,###,##0");
        }

        private void Detail_Format(object sender, EventArgs e)
        {
            if (!rsReport.EndOfFile())
            {
                try
                {
                    if (rsReport.Get_Fields_Int32("quantity") > 0)
                    {
                        txtQuantity.Text = rsReport.Get_Fields_Int32("quantity").ToString();
                    }
                    else
                    {
                        txtQuantity.Text = "";
                    }

                    if (rsReport.Get_Fields_String("cd").ToIntegerValue() > 0)
                    {
                        txtCategory.Text =
                            (txtCat[rsReport.Get_Fields_String("Cd").ToIntegerValue()] + " ").Left(1);
                        dblCat[rsReport.Get_Fields_String("cd").ToIntegerValue()] =
                            dblCat[rsReport.Get_Fields_String("cd").ToIntegerValue()] + rsReport.Get_Fields_Int32("value");
                        dblBETE[rsReport.Get_Fields_String("cd").ToIntegerValue()] =
                            dblBETE[rsReport.Get_Fields_String("cd").ToIntegerValue()] + rsReport.Get_Fields_Int32("beteEXEMPT");
                    }
                    else
                    {
                        txtCategory.Text = "";
                    }

                    txtDescription.Text = rsReport.Get_Fields_String("description").Trim();
                    if (rsReport.Get_Fields_Int32("year") > 0)
                    {
                        txtYear.Text = rsReport.Get_Fields_Int32("year").ToString();
                    }
                    else
                    {
                        txtYear.Text = "";
                    }

                    txtCost.Text = rsReport.Get_Fields_Int32("cost").FormatAsNumber();
                    dblTotCost += rsReport.Get_Fields_Int32("cost") * rsReport.Get_Fields_Int32("quantity");
                    txtGood.Text = rsReport.Get_Fields_Double("CalcGood").ToString();
                    txtValue.Text = rsReport.Get_Fields_Int32("value").FormatAsNumber();
                    dblTotValue += rsReport.Get_Fields_Int32("value");
                    txtBETE.Text = rsReport.Get_Fields_Int32("beteexempt").FormatAsNumber();
                    if (rsReport.Get_Fields_Int32("beteexempt") > 0)
                    {
                        txtBET.Text = "E";
                        dblTotBETE +=
                            rsReport.Get_Fields_Int32("beteexempt");
                    }
                    else if (rsReport.Get_Fields_String("rb") == "*")
                    {
                        txtBET.Text = "R";
                    }
                    else
                    {
                        txtBET.Text = "";
                    }


                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                rsReport.MoveNext();
            }
        }

        private void SrptItemizedInventory_ReportStart(object sender, EventArgs e)
        {
            lngAcct = (int)Math.Round(fecherFoundation.Conversion.Val(FCConvert.ToString(this.UserData)));
            rsReport.OpenRecordset("select * from RATIOTRENDS", "PersonalProperty");
            while (!rsReport.EndOfFile())
            {
                txtCat[rsReport.Get_Fields_Int32("type")] = rsReport.Get_Fields_String("Description");
                rsReport.MoveNext();
            }
            rsReport.OpenRecordset("select * from ppitemized where account = " + FCConvert.ToString(lngAcct) + " order by [line]", "PersonalProperty");
            if (rsReport.EndOfFile())
            {
                this.detail.Visible = false;
                this.groupHeader1.Visible = false;
                this.reportHeader1.Visible = false;
                this.reportFooter1.Visible = false;
            }
            else
            {
                this.detail.Visible = true;
                this.groupHeader1.Visible = true;
                this.reportHeader1.Visible = true;
                this.reportFooter1.Visible = true;
                dblTotBETE = 0;
                dblTotCost = 0;
                dblTotValue = 0;
                int x;
                for (x = 1; x <= 9; x++)
                {
                    dblCat[x] = 0;
                    dblBETE[x] = 0;
                } // x
            }
        }

        private void SrptItemizedInventory_FetchData(object sender, FetchEventArgs eArgs)
        {
            eArgs.EOF = rsReport.EndOfFile();
        }
    }
}
