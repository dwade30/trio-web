﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmExemptAudit.
	/// </summary>
	partial class frmExemptAudit : BaseForm
	{
		public fecherFoundation.FCComboBox cmbAll;
		public fecherFoundation.FCLabel lblAll;
		public fecherFoundation.FCComboBox cmbSubTotals;
		public fecherFoundation.FCLabel lblSubTotals;
		public fecherFoundation.FCComboBox cmbAccount;
		public fecherFoundation.FCLabel lblAccount;
		public fecherFoundation.FCGrid GridTownCode;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmExemptAudit));
            this.cmbAll = new fecherFoundation.FCComboBox();
            this.lblAll = new fecherFoundation.FCLabel();
            this.cmbSubTotals = new fecherFoundation.FCComboBox();
            this.lblSubTotals = new fecherFoundation.FCLabel();
            this.cmbAccount = new fecherFoundation.FCComboBox();
            this.lblAccount = new fecherFoundation.FCLabel();
            this.GridTownCode = new fecherFoundation.FCGrid();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridTownCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 385);
            this.BottomPanel.Size = new System.Drawing.Size(414, 33);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdSave);
            this.ClientArea.Controls.Add(this.cmbAll);
            this.ClientArea.Controls.Add(this.lblAll);
            this.ClientArea.Controls.Add(this.GridTownCode);
            this.ClientArea.Controls.Add(this.cmbSubTotals);
            this.ClientArea.Controls.Add(this.lblSubTotals);
            this.ClientArea.Controls.Add(this.cmbAccount);
            this.ClientArea.Controls.Add(this.lblAccount);
            this.ClientArea.Size = new System.Drawing.Size(414, 325);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(414, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(158, 30);
            this.HeaderText.Text = "Exempt Audit";
            // 
            // cmbAll
            // 
            this.cmbAll.Items.AddRange(new object[] {
            "All",
            "Specific Exemptions"});
            this.cmbAll.Location = new System.Drawing.Point(161, 130);
            this.cmbAll.Name = "cmbAll";
            this.cmbAll.Size = new System.Drawing.Size(224, 40);
            this.cmbAll.Text = "All";
            // 
            // lblAll
            // 
            this.lblAll.AutoSize = true;
            this.lblAll.Location = new System.Drawing.Point(30, 144);
            this.lblAll.Name = "lblAll";
            this.lblAll.Size = new System.Drawing.Size(78, 15);
            this.lblAll.TabIndex = 1;
            this.lblAll.Text = "SELECTION";
            // 
            // cmbSubTotals
            // 
            this.cmbSubTotals.Items.AddRange(new object[] {
            "Subtotals Only",
            "Details"});
            this.cmbSubTotals.Location = new System.Drawing.Point(161, 190);
            this.cmbSubTotals.Name = "cmbSubTotals";
            this.cmbSubTotals.Size = new System.Drawing.Size(224, 40);
            this.cmbSubTotals.TabIndex = 2;
            this.cmbSubTotals.Text = "Details";
            // 
            // lblSubTotals
            // 
            this.lblSubTotals.AutoSize = true;
            this.lblSubTotals.Location = new System.Drawing.Point(30, 204);
            this.lblSubTotals.Name = "lblSubTotals";
            this.lblSubTotals.Size = new System.Drawing.Size(46, 15);
            this.lblSubTotals.TabIndex = 3;
            this.lblSubTotals.Text = "SHOW";
            // 
            // cmbAccount
            // 
            this.cmbAccount.Items.AddRange(new object[] {
            "Account",
            "Name"});
            this.cmbAccount.Location = new System.Drawing.Point(161, 70);
            this.cmbAccount.Name = "cmbAccount";
            this.cmbAccount.Size = new System.Drawing.Size(224, 40);
            this.cmbAccount.TabIndex = 4;
            this.cmbAccount.Text = "Account";
            // 
            // lblAccount
            // 
            this.lblAccount.AutoSize = true;
            this.lblAccount.Location = new System.Drawing.Point(30, 84);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(72, 15);
            this.lblAccount.TabIndex = 5;
            this.lblAccount.Text = "ORDER BY";
            // 
            // GridTownCode
            // 
            this.GridTownCode.Cols = 1;
            this.GridTownCode.ColumnHeadersVisible = false;
            this.GridTownCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridTownCode.ExtendLastCol = true;
            this.GridTownCode.FixedCols = 0;
            this.GridTownCode.FixedRows = 0;
            this.GridTownCode.Location = new System.Drawing.Point(30, 30);
            this.GridTownCode.Name = "GridTownCode";
            this.GridTownCode.ReadOnly = false;
            this.GridTownCode.RowHeadersVisible = false;
            this.GridTownCode.Rows = 1;
            this.GridTownCode.Size = new System.Drawing.Size(355, 20);
            this.GridTownCode.Visible = false;
            this.GridTownCode.ComboCloseUp += new System.EventHandler(this.GridTownCode_ComboCloseUp);
            this.GridTownCode.Leave += new System.EventHandler(this.GridTownCode_Leave);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSaveContinue
            // 
            this.mnuSaveContinue.Index = 0;
            this.mnuSaveContinue.Name = "mnuSaveContinue";
            this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveContinue.Text = "Save & Continue";
            this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(30, 260);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(100, 48);
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // frmExemptAudit
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(414, 418);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmExemptAudit";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Exempt Audit";
            this.Load += new System.EventHandler(this.frmExemptAudit_Load);
            this.Resize += new System.EventHandler(this.frmExemptAudit_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmExemptAudit_KeyDown);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridTownCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
		private System.ComponentModel.IContainer components;
	}
}
