﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptCalculation.
	/// </summary>
	public partial class rptCalculation : BaseSectionReport
	{
		public rptCalculation()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Personal Property Assessments";
		}

		public static rptCalculation InstancePtr
		{
			get
			{
				return (rptCalculation)Sys.GetInstance(typeof(rptCalculation));
			}
		}

		protected rptCalculation _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsPPMast?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCalculation	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intPage;
		bool boolByRange;
		bool boolFirstTime;
		clsDRWrapper clsPPMast = new clsDRWrapper();
		//clsDRWrapper clsPPLeased = new clsDRWrapper();
		//clsDRWrapper clsPPItemized = new clsDRWrapper();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!boolByRange)
			{
				eArgs.EOF = !boolFirstTime;
				boolFirstTime = false;
			}
			else
			{
				eArgs.EOF = clsPPMast.EndOfFile();
			}
		}

		private void ShowOpens(string strOpen1, string strOpen2)
		{
			string strOpen1ToUse;
			string strOpen2ToUse;
			strOpen1ToUse = "";
			strOpen2ToUse = "";
			if (modGlobal.Statics.CustomizeStuff.ShowOpen1OnCalc)
			{
				strOpen1ToUse = modGlobal.Statics.CustomizeStuff.Open1Description + ": " + Strings.Trim(strOpen1);
			}
			if (modGlobal.Statics.CustomizeStuff.ShowOpen2OnCalc)
			{
				strOpen2ToUse = modGlobal.Statics.CustomizeStuff.Open2Description + ": " + Strings.Trim(strOpen2);
			}
			if (Strings.Trim(strOpen1ToUse) == "")
			{
				strOpen1ToUse = strOpen2ToUse;
				strOpen2ToUse = "";
			}
			txtOpen1.Text = strOpen1ToUse;
			txtOpen2.Text = strOpen2ToUse;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//this.Printer.RenderMode = 1;
			//clsDRWrapper clsLoad = new clsDRWrapper();
			SubReport1.Report = new srptItemizedCalc();
			if (frmPPCalculations.InstancePtr.vsLeased.Rows > 2)
			{
				if (Conversion.Val(frmPPCalculations.InstancePtr.vsLeased.TextMatrix(1, 1)) > 0)
				{
					SubReport2.Report = new srptLeasedCalc();
				}
			}
			if (!modGlobal.Statics.CustomizeStuff.ShowOriginalTotals)
			{
				SubReport3.Report = new srptCostSummary();
			}
			else
			{
				SubReport3.Report = new srptCostSummaryWithOrig();
			}
			if (!boolByRange)
			{
				txtAccount.Text = "Account " + Strings.Trim(frmPPCalculations.InstancePtr.lblAccount.Text);
				txtName.Text = Strings.Trim(frmPPCalculations.InstancePtr.lblName.Text);
				txtLocation.Text = Strings.Trim(frmPPCalculations.InstancePtr.lblLocation.Text) + " " + Strings.Trim(frmPPCalculations.InstancePtr.lblLocationName.Text);
				ShowOpens(frmPPCalculations.InstancePtr.Open1, frmPPCalculations.InstancePtr.Open2);
			}
			// Call clsLoad.OpenRecordset("select * from tbldefaultinformation", "SystemSettings")
			// If Not clsLoad.EndOfFile Then
			// txtMuni.Text = MuniName & "," & clsLoad.Fields("state")
			// Else
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			// End If
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			LabelDate.Text = FCConvert.ToString(DateTime.Today);
			txtPage.Text = "Page 1";
			intPage = 1;
			boolFirstTime = true;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (boolByRange)
			{
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				modPPGN.Statics.CurrentAccount = FCConvert.ToInt32(clsPPMast.Get_Fields("account"));
				modPPGN.Statics.boolDoneCalc = false;
				//frmPPCalculations.InstancePtr.Hide();
				//! Load frmPPCalculations;
				// Me.ZOrder (0)
				// frmPPCalculations.Hide
				////Application.DoEvents();
				//FC:FINAL:RPU:#i1505 - Call the right method
				//FCUtils.CallByName(frmPPCalculations.InstancePtr, "Form_Activate", CallType.Method);
				FCUtils.CallByName(frmPPCalculations.InstancePtr, "frmPPCalculations_Activated", CallType.Method, this, System.EventArgs.Empty);
				// Me.ZOrder (0)
				////Application.DoEvents();
				// txtPage.Text = "Page 1"
				// intPage = 1
				txtName.Text = Strings.Trim(frmPPCalculations.InstancePtr.lblName.Text);
				txtLocation.Text = Strings.Trim(frmPPCalculations.InstancePtr.lblLocation.Text) + " " + Strings.Trim(frmPPCalculations.InstancePtr.lblLocationName.Text);
				txtAccount.Text = "Account " + Strings.Trim(frmPPCalculations.InstancePtr.lblAccount.Text);
				ShowOpens(frmPPCalculations.InstancePtr.Open1, frmPPCalculations.InstancePtr.Open2);
				clsPPMast.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + FCConvert.ToString(intPage);
			intPage += 1;
			txtName.Text = Strings.Trim(frmPPCalculations.InstancePtr.lblName.Text);
			txtLocation.Text = Strings.Trim(frmPPCalculations.InstancePtr.lblLocation.Text) + " " + Strings.Trim(frmPPCalculations.InstancePtr.lblLocationName.Text);
			txtAccount.Text = "Account " + Strings.Trim(frmPPCalculations.InstancePtr.lblAccount.Text);
			ShowOpens(frmPPCalculations.InstancePtr.Open1, frmPPCalculations.InstancePtr.Open2);
		}

		public void Start()
		{
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), false, false, "Pages", false, "", "TRIO Software", false, true, "Calculation", showModal: true);
		}
		// vbPorter upgrade warning: strStart As Variant --> As string
		// vbPorter upgrade warning: strEnd As Variant --> As string
		// vbPorter upgrade warning: intWhich As Variant --> As string
		public void Init(int lngStart = 0, int lngEnd = 0, string strStart = "", string strEnd = "", string intWhich = "")
		{
			boolByRange = true;
			modPPCalculate.Statics.CalculateFromMaster = false;
			string strMasterJoin;
			strMasterJoin = modPPGN.GetMasterJoin();
			if (!Information.IsNothing(intWhich))
			{
				switch (FCConvert.ToInt32(intWhich))
				{
					case 0:
						{
							// account
							if (Conversion.Val(strEnd) > 0)
							{
								if (Conversion.Val(strStart) > 0)
								{
									// Call clsPPMast.OpenRecordset("select * from ppmaster where not deleted = 1 and account between " & Val(strStart) & " and " & Val(strEnd) & " order by account", "TWPP0000.vb1")
									clsPPMast.OpenRecordset(strMasterJoin + " where not deleted = 1 and account between " + FCConvert.ToString(Conversion.Val(strStart)) + " and " + FCConvert.ToString(Conversion.Val(strEnd)) + " order by account", "TWPP0000.vb1");
								}
								else
								{
									// Call clsPPMast.OpenRecordset("select * from ppmaster where not deleted = 1 and account <= " & Val(strEnd) & " order by account", "twpp0000.vb1")
									clsPPMast.OpenRecordset(strMasterJoin + " where not deleted = 1 and account <= " + FCConvert.ToString(Conversion.Val(strEnd)) + " order by account", "twpp0000.vb1");
								}
							}
							else
							{
								if (Conversion.Val(strStart) > 0)
								{
									// Call clsPPMast.OpenRecordset("select * from ppmaster where not deleted = 1 and account >= " & Val(strStart) & " order by account", "TWPP0000.vb1")
									clsPPMast.OpenRecordset(strMasterJoin + " where not deleted = 1 and account >= " + FCConvert.ToString(Conversion.Val(strStart)) + " order by account", "TWPP0000.vb1");
								}
								else
								{
									// Call clsPPMast.OpenRecordset("select * from ppmaster where not deleted = 1 order by account", "twpp0000.vb1")
									clsPPMast.OpenRecordset(strMasterJoin + " where not deleted = 1 order by account", "twpp0000.vb1");
								}
							}
							break;
						}
					case 1:
						{
							// name
							if (Strings.Trim(strEnd) != string.Empty)
							{
								if (Strings.Trim(strStart) != string.Empty)
								{
									// Call clsPPMast.OpenRecordset("select * from ppmaster where not deleted = 1 and name between '" & Trim(strStart) & "' and '" & Trim(strEnd) & "' order by name", "TWPP0000.vb1")
									clsPPMast.OpenRecordset(strMasterJoin + " where not deleted = 1 and name between '" + Strings.Trim(strStart) + "' and '" + Strings.Trim(strEnd) + "' order by name", "TWPP0000.vb1");
								}
								else
								{
									// Call clsPPMast.OpenRecordset("select * from ppmaster where not deleted = 1 and name <= '" & Trim(strEnd) & "' order by name", "twpp0000.vb1")
									clsPPMast.OpenRecordset(strMasterJoin + " where not deleted = 1 and name <= '" + Strings.Trim(strEnd) + "' order by name", "twpp0000.vb1");
								}
							}
							else
							{
								if (Strings.Trim(strStart) != string.Empty)
								{
									// Call clsPPMast.OpenRecordset("select * from ppmaster where not deleted = 1 and name >= '" & Trim(strStart) & "' order by name", "TWPP0000.vb1")
									clsPPMast.OpenRecordset(strMasterJoin + " where not deleted = 1 and name >= '" + Strings.Trim(strStart) + "' order by name", "TWPP0000.vb1");
								}
								else
								{
									// Call clsPPMast.OpenRecordset("select * from ppmaster where not deleted = 1 order by name", "twpp0000.vb1")
									clsPPMast.OpenRecordset(strMasterJoin + " where not deleted = 1 order by name", "twpp0000.vb1");
								}
							}
							break;
						}
					case 3:
						{
							// business code
							if (Conversion.Val(strEnd) > 0)
							{
								if (Conversion.Val(strStart) > 0)
								{
									// Call clsPPMast.OpenRecordset("select * from ppmaster where not deleted = 1 and businesscode between " & Val(strStart) & " and " & Val(strEnd) & " order by businesscode", "TWPP0000.vb1")
									clsPPMast.OpenRecordset(strMasterJoin + " where not deleted = 1 and businesscode between " + FCConvert.ToString(Conversion.Val(strStart)) + " and " + FCConvert.ToString(Conversion.Val(strEnd)) + " order by businesscode", "TWPP0000.vb1");
								}
								else
								{
									// Call clsPPMast.OpenRecordset("select * from ppmaster where not deleted = 1 and businesscode <= " & Val(strEnd) & " order by businesscode", "twpp0000.vb1")
									clsPPMast.OpenRecordset(strMasterJoin + " where not deleted = 1 and businesscode <= " + FCConvert.ToString(Conversion.Val(strEnd)) + " order by businesscode", "twpp0000.vb1");
								}
							}
							else
							{
								if (Conversion.Val(strStart) > 0)
								{
									// Call clsPPMast.OpenRecordset("select * from ppmaster where not deleted = 1 and businesscode >= " & Val(strStart) & " order by businesscode", "TWPP0000.vb1")
									clsPPMast.OpenRecordset(strMasterJoin + " where not deleted = 1 and businesscode >= " + FCConvert.ToString(Conversion.Val(strStart)) + " order by businesscode", "TWPP0000.vb1");
								}
								else
								{
									// Call clsPPMast.OpenRecordset("select * from ppmaster where not deleted = 1 order by businesscode", "twpp0000.vb1")
									clsPPMast.OpenRecordset(strMasterJoin + " where not deleted = 1 order by businesscode", "twpp0000.vb1");
								}
							}
							break;
						}
					case 4:
						{
							// open1
							if (Strings.Trim(strEnd) != string.Empty)
							{
								if (Strings.Trim(strStart) != string.Empty)
								{
									// Call clsPPMast.OpenRecordset("select * from ppmaster where not deleted = 1 and open1 between '" & Trim(strStart) & "' and '" & Trim(strEnd) & "' order by open1", "TWPP0000.vb1")
									clsPPMast.OpenRecordset(strMasterJoin + " where not deleted = 1 and open1 between '" + Strings.Trim(strStart) + "' and '" + Strings.Trim(strEnd) + "' order by open1", "TWPP0000.vb1");
								}
								else
								{
									// Call clsPPMast.OpenRecordset("select * from ppmaster where not deleted = 1 and open1 <= '" & Trim(strEnd) & "' order by open1", "twpp0000.vb1")
									clsPPMast.OpenRecordset(strMasterJoin + " where not deleted = 1 and open1 <= '" + Strings.Trim(strEnd) + "' order by open1", "twpp0000.vb1");
								}
							}
							else
							{
								if (Strings.Trim(strStart) != string.Empty)
								{
									// Call clsPPMast.OpenRecordset("select * from ppmaster where not deleted = 1 and open1 >= '" & Trim(strStart) & "' order by open1", "TWPP0000.vb1")
									clsPPMast.OpenRecordset(strMasterJoin + " where not deleted = 1 and open1 >= '" + Strings.Trim(strStart) + "' order by open1", "TWPP0000.vb1");
								}
								else
								{
									// Call clsPPMast.OpenRecordset("select * from ppmaster where not deleted = 1 order by open1", "twpp0000.vb1")
									clsPPMast.OpenRecordset(strMasterJoin + " where not deleted = 1 order by open1", "twpp0000.vb1");
								}
							}
							break;
						}
					case 5:
						{
							// open2
							if (Strings.Trim(strEnd) != string.Empty)
							{
								if (Strings.Trim(strStart) != string.Empty)
								{
									// Call clsPPMast.OpenRecordset("select * from ppmaster where not deleted = 1 and open2 between '" & Trim(strStart) & "' and '" & Trim(strEnd) & "' order by open2", "TWPP0000.vb1")
									clsPPMast.OpenRecordset(strMasterJoin + " where not deleted = 1 and open2 between '" + Strings.Trim(strStart) + "' and '" + Strings.Trim(strEnd) + "' order by open2", "TWPP0000.vb1");
								}
								else
								{
									// Call clsPPMast.OpenRecordset("select * from ppmaster where not deleted = 1 and open2 <= '" & Trim(strEnd) & "' order by open2", "twpp0000.vb1")
									clsPPMast.OpenRecordset(strMasterJoin + " where not deleted = 1 and open2 <= '" + Strings.Trim(strEnd) + "' order by open2", "twpp0000.vb1");
								}
							}
							else
							{
								if (Strings.Trim(strStart) != string.Empty)
								{
									// Call clsPPMast.OpenRecordset("select * from ppmaster where not deleted = 1 and open2 >= '" & Trim(strStart) & "' order by open2", "TWPP0000.vb1")
									clsPPMast.OpenRecordset(strMasterJoin + " where not deleted = 1 and open2 >= '" + Strings.Trim(strStart) + "' order by open2", "TWPP0000.vb1");
								}
								else
								{
									// Call clsPPMast.OpenRecordset("select * from ppmaster where not deleted = 1 order by open2", "twpp0000.vb1")
									clsPPMast.OpenRecordset(strMasterJoin + " where not deleted = 1 order by open2", "twpp0000.vb1");
								}
							}
							break;
						}
				
				}
				//end switch
			}
			else
			{
				// Call clsPPMast.OpenRecordset("select * from ppmaster where not deleted = 1 and account between " & lngStart & " and " & lngEnd & " order by account", "TWPP0000.vb1")
				clsPPMast.OpenRecordset(strMasterJoin + " where not deleted = 1 and account between " + FCConvert.ToString(lngStart) + " and " + FCConvert.ToString(lngEnd) + " order by account", "TWPP0000.vb1");
			}
			// Call clsPPLeased.OpenRecordset("select * from ppleased where account between " & lngStart & " and " & lngEnd & " order by account,line", "TWPP0000.vb1")
			// Call clsPPItemized.OpenRecordset("select * from ppitemized where account between " & lngStart & " and " & lngEnd & " order by account,line", "twpp0000.vb1")
			;
			// Call SetFixedsizeReport(Me, MDIParent.Grid)
			// Me.Width = MDIParent.Width - MDIParent.GRID.Width - 500
			// Me.Height = MDIParent.GRID.Height
			// 
			// 
			// Me.Left = MDIParent.GRID.Left + MDIParent.GRID.Width + 100
			// Me.Top = MDIParent.Top + 600
			//FC:FINAL:RPU: #i1505 - Show the report
			frmReportViewer.InstancePtr.Init(this, boolLandscape: true, boolAllowEmail: true, strAttachmentName: "Calculation");
		}

		
	}
}
