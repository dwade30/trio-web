﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmTransferData.
	/// </summary>
	public partial class frmTransferData : BaseForm
	{
		public frmTransferData()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			cmbLeasedItemized.SelectedIndex = 2;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmTransferData InstancePtr
		{
			get
			{
				return (frmTransferData)Sys.GetInstance(typeof(frmTransferData));
			}
		}

		protected frmTransferData _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		/// <summary>
		/// *************************************************
		/// </summary>
		/// <summary>
		/// PROPERTY OF TRIO SOFTWARE CORPORATION
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// WRITTEN BY: MATTHEW S. LARRABEE
		/// </summary>
		/// <summary>
		/// DATE:       SEPTEMBER 14,2001
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// NOTES:
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// **************************************************
		/// </summary>
		/// <summary>
		/// private local variables
		/// </summary>
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsDRWrapper rsDeductions = new clsDRWrapper();
		private clsDRWrapper rsDeductions_AutoInitialized;

		private clsDRWrapper rsDeductions
		{
			get
			{
				if (rsDeductions_AutoInitialized == null)
				{
					rsDeductions_AutoInitialized = new clsDRWrapper();
				}
				return rsDeductions_AutoInitialized;
			}
			set
			{
				rsDeductions_AutoInitialized = value;
			}
		}

		private int intCounter;
		private int intDataChanged;

		private void cboFromAccount_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cboFromAccount.SelectedIndex < 0)
				return;
			ShowAccountInformaiton(ref lblFromData, cboFromAccount.ItemData(cboFromAccount.SelectedIndex));
		}

		private void ShowAccountInformaiton(ref FCLabel LabelName, int intValue)
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			string strMasterJoin;
			strMasterJoin = modPPGN.GetMasterJoin();
			rsInfo.OpenRecordset(strMasterJoin + " where Account = " + FCConvert.ToString(intValue), "Twpp0000.vb1");
			if (!rsInfo.EndOfFile())
			{
				LabelName.Text = Strings.Trim(rsInfo.Get_Fields_String("Name") + " ");
				LabelName.Text = LabelName.Text + Strings.StrDup(5, " ");
				LabelName.Text = LabelName.Text + Strings.Trim(rsInfo.Get_Fields_String("Address1") + " ");
				LabelName.Text = LabelName.Text + Strings.StrDup(5, " ");
				LabelName.Text = LabelName.Text + Strings.Trim(rsInfo.Get_Fields_String("City") + " ");
				LabelName.Text = LabelName.Text + ", ";
				LabelName.Text = LabelName.Text + Strings.Trim(rsInfo.Get_Fields_String("State") + " ");
				LabelName.Text = LabelName.Text + Strings.StrDup(1, " ");
				LabelName.Text = LabelName.Text + Strings.Trim(rsInfo.Get_Fields_String("Zip") + " ");
			}
		}

		private void cboToAccount_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cboToAccount.SelectedIndex < 0)
				return;
			ShowAccountInformaiton(ref lblToData, cboToAccount.ItemData(cboToAccount.SelectedIndex));
		}

		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				bool boolResponse;
				// vbPorter upgrade warning: boolItemized As short --> As int	OnWrite(bool)
				int boolItemized;
				// vbPorter upgrade warning: boolLeased As short --> As int	OnWrite(bool)
				int boolLeased;
				clsDRWrapper rsInfo = new clsDRWrapper();
				string strCols = "";
				string strSQL = "";
				clsDRWrapper clsDest = new clsDRWrapper();
				string strTemp = "";
				int lngHighline;
				int lngHighLLine;
				if (cboToAccount.SelectedIndex < 0)
					return;
				if (cboFromAccount.SelectedIndex < 0)
					return;
				lngHighline = 0;
				lngHighLLine = 0;
				boolResponse = true;
				rsInfo.OpenRecordset("Select * from PPItemized where Account = " + FCConvert.ToString(cboToAccount.ItemData(cboToAccount.SelectedIndex)), "Twpp0000.vb1");
				if (!rsInfo.EndOfFile())
				{
					rsInfo.OpenRecordset("select max(line) as mline from ppitemized where account = " + FCConvert.ToString(cboToAccount.ItemData(cboToAccount.SelectedIndex)), "twpp0000.vb1");
					// TODO Get_Fields: Field [mline] not found!! (maybe it is an alias?)
					lngHighline = FCConvert.ToInt32(Math.Round(Conversion.Val(rsInfo.Get_Fields("mline"))));
				}
				rsInfo.OpenRecordset("Select * from PPLeased where Account = " + FCConvert.ToString(cboToAccount.ItemData(cboToAccount.SelectedIndex)), "Twpp0000.vb1");
				if (!rsInfo.EndOfFile())
				{
					rsInfo.OpenRecordset("select max(line) as mline from ppleased where account = " + FCConvert.ToString(cboToAccount.ItemData(cboToAccount.SelectedIndex)), "TWPP0000.vb1");
					// TODO Get_Fields: Field [mline] not found!! (maybe it is an alias?)
					lngHighLLine = FCConvert.ToInt32(Math.Round(Conversion.Val(rsInfo.Get_Fields("mline"))));
				}
				boolItemized = ((cmbLeasedItemized.SelectedIndex == 0 || cmbLeasedItemized.SelectedIndex == 2) ? -1 : 0);
				boolLeased = ((cmbLeasedItemized.SelectedIndex == 1 || cmbLeasedItemized.SelectedIndex == 2) ? -1 : 0);
				if (boolResponse)
				{
					if (FCConvert.ToBoolean(boolItemized))
					{
						modGlobalFunctions.AddCYAEntry_80("PP", "Transferred Itemized", "From " + FCConvert.ToString(cboFromAccount.ItemData(cboFromAccount.SelectedIndex)), "To " + FCConvert.ToString(cboToAccount.ItemData(cboToAccount.SelectedIndex)));
						strCols = "(Account,Line,RB,CD,quantity,description,[month],[year],dpyr,sro,rcyr,cost,gd,fctr,[value])";
						rsInfo.OpenRecordset("Select * from PPItemized where Account = " + FCConvert.ToString(cboFromAccount.ItemData(cboFromAccount.SelectedIndex)) + " order by line", "Twpp0000.vb1");
						while (!rsInfo.EndOfFile())
						{
							strSQL = "(";
							strSQL += FCConvert.ToString(cboToAccount.ItemData(cboToAccount.SelectedIndex));
							// TODO Get_Fields: Check the table for the column [line] and replace with corresponding Get_Field method
							strSQL += "," + FCConvert.ToString(rsInfo.Get_Fields("line") + lngHighline);
							// add so you don't duplicate lines and screw up the order
							// TODO Get_Fields: Check the table for the column [rb] and replace with corresponding Get_Field method
							strSQL += ",'" + rsInfo.Get_Fields("rb") + "'";
							strSQL += ",'" + rsInfo.Get_Fields_String("cd") + "'";
							// TODO Get_Fields: Check the table for the column [quantity] and replace with corresponding Get_Field method
							strSQL += "," + rsInfo.Get_Fields("quantity");
							strTemp = FCConvert.ToString(rsInfo.Get_Fields_String("description"));
							modGlobal.escapequote(ref strTemp);
							strSQL += ",'" + strTemp + "'";
							strSQL += "," + rsInfo.Get_Fields_Int16("month");
							// TODO Get_Fields: Check the table for the column [year] and replace with corresponding Get_Field method
							strSQL += "," + rsInfo.Get_Fields("year");
							strSQL += "," + rsInfo.Get_Fields_Int16("dpyr");
							// TODO Get_Fields: Check the table for the column [sro] and replace with corresponding Get_Field method
							strSQL += ",'" + rsInfo.Get_Fields("sro") + "'";
							strSQL += "," + rsInfo.Get_Fields_Int16("rcyr");
							// TODO Get_Fields: Check the table for the column [cost] and replace with corresponding Get_Field method
							strSQL += "," + rsInfo.Get_Fields("cost");
							strSQL += "," + rsInfo.Get_Fields_Int16("gd");
							strSQL += "," + rsInfo.Get_Fields_Int16("fctr");
							// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
							strSQL += "," + rsInfo.Get_Fields("value");
							strSQL += ")";
							clsDest.Execute("insert into ppitemized " + strCols + " values " + strSQL, "twpp0000.vb1");
							rsInfo.MoveNext();
						}
					}
					if (FCConvert.ToBoolean(boolLeased))
					{
						modGlobalFunctions.AddCYAEntry_80("PP", "Transferred Leased", "From " + FCConvert.ToString(cboFromAccount.ItemData(cboFromAccount.SelectedIndex)), "To " + FCConvert.ToString(cboToAccount.ItemData(cboToAccount.SelectedIndex)));
						strCols = "(Account,Line,RB,CD,quantity,description,[month],cost,leasedate,mos,l1p2,monthlyrent,depyrs,fctr,[value])";
						rsInfo.OpenRecordset("Select * from PPLeased where Account = " + FCConvert.ToString(cboFromAccount.ItemData(cboFromAccount.SelectedIndex)) + " order by line", "Twpp0000.vb1");
						while (!rsInfo.EndOfFile())
						{
							strSQL = "(";
							strSQL += FCConvert.ToString(cboToAccount.ItemData(cboToAccount.SelectedIndex));
							// TODO Get_Fields: Check the table for the column [line] and replace with corresponding Get_Field method
							strSQL += "," + FCConvert.ToString(rsInfo.Get_Fields("line") + lngHighLLine);
							// TODO Get_Fields: Check the table for the column [rb] and replace with corresponding Get_Field method
							strSQL += ",'" + rsInfo.Get_Fields("rb") + "'";
							strSQL += ",'" + rsInfo.Get_Fields_String("cd") + "'";
							// TODO Get_Fields: Check the table for the column [quantity] and replace with corresponding Get_Field method
							strSQL += "," + rsInfo.Get_Fields("quantity");
							strTemp = FCConvert.ToString(rsInfo.Get_Fields_String("description"));
							modGlobal.escapequote(ref strTemp);
							strSQL += ",'" + strTemp + "'";
							strSQL += "," + rsInfo.Get_Fields_Int16("month");
							// TODO Get_Fields: Check the table for the column [cost] and replace with corresponding Get_Field method
							strSQL += "," + rsInfo.Get_Fields("cost");
							strSQL += ",'" + rsInfo.Get_Fields_String("leasedate") + "'";
							strSQL += "," + rsInfo.Get_Fields_Int16("mos");
							strSQL += "," + rsInfo.Get_Fields_Int16("l1p2");
							strSQL += "," + rsInfo.Get_Fields_Decimal("monthlyrent");
							strSQL += "," + rsInfo.Get_Fields_Int16("depyrs");
							strSQL += "," + rsInfo.Get_Fields_Int16("fctr");
							// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
							strSQL += "," + rsInfo.Get_Fields("value");
							strSQL += ")";
							clsDest.Execute("insert into ppleased " + strCols + " values " + strSQL, "twpp0000.vb1");
							rsInfo.MoveNext();
						}
					}
					MessageBox.Show("Transfer completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description, null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void cmdProcess_Click()
		{
			cmdProcess_Click(cmdProcess, new System.EventArgs());
		}

		private void frmTransferData_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Activate";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				goto ResumeCode;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
			ResumeCode:
			;
			if (modGNWork.FormExist(this))
				return;
		}

		private void frmTransferData_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.F10:
					{
						KeyCode = (Keys)0;
						mnuTransfer_Click();
						break;
					}
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmTransferData_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTransferData properties;
			//frmTransferData.ScaleWidth	= 5880;
			//frmTransferData.ScaleHeight	= 4260;
			//frmTransferData.LinkTopic	= "Form1";
			//End Unmaped Properties
			try
			{
				// On Error GoTo CallErrorRoutine
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
				modGlobalFunctions.SetTRIOColors(this);
				Label1.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
				Label2.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
				modErrorHandler.Statics.gstrCurrentRoutine = "Form_Load";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				goto ResumeCode;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
			ResumeCode:
			;
			// open the forms global database connection
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			LoadToCombo();
			LoadFromCombo();
			// set the size of the form
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
		}

		public void LoadToCombo()
		{
			clsDRWrapper rsAccount = new clsDRWrapper();
			string strMasterJoin;
			strMasterJoin = modPPGN.GetMasterJoin();
			rsAccount.OpenRecordset(strMasterJoin + " where not deleted = 1 order by account", "Twpp0000.vb1");
			while (!rsAccount.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				cboToAccount.AddItem(rsAccount.Get_Fields("Account") + "  " + rsAccount.Get_Fields_String("Name"));
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				cboToAccount.ItemData(cboToAccount.NewIndex, FCConvert.ToInt32(rsAccount.Get_Fields("Account")));
				rsAccount.MoveNext();
			}
		}

		public void LoadFromCombo()
		{
			clsDRWrapper rsAccount = new clsDRWrapper();
			string strMasterJoin;
			strMasterJoin = modPPGN.GetMasterJoinForJoin();
			rsAccount.OpenRecordset("select account,name from " + strMasterJoin + " order by account", "Twpp0000.vb1");
			while (!rsAccount.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				cboFromAccount.AddItem(rsAccount.Get_Fields("Account") + "  " + rsAccount.Get_Fields_String("Name"));
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				cboFromAccount.ItemData(cboFromAccount.NewIndex, FCConvert.ToInt32(rsAccount.Get_Fields("Account")));
				rsAccount.MoveNext();
			}
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				modErrorHandler.Statics.gstrCurrentRoutine = "cmdExit_Click";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				goto ResumeCode;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
			ResumeCode:
			;
			// validate that there is not changes that need to be saved
			SaveChanges();
			// unload the form
			Close();
		}

		public void cmdExit_Click()
		{
			//cmdExit_Click(cmdExit, new System.EventArgs());
		}

		public void SaveChanges()
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				goto ResumeCode;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
			ResumeCode:
			;
			// have any rows been altered?
			if (intDataChanged > 0)
			{
				if (MessageBox.Show("Current changes have not been saved. Do so now?", "Payroll", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					// save all changes
					// Call cmdSave_Click
				}
			}
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			// set focus back to the menu options of the MDIParent
			// CallByName MDIParent, "Grid_GotFocus", VbMethod
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuTransfer_Click(object sender, System.EventArgs e)
		{
			cmdProcess_Click();
		}

		public void mnuTransfer_Click()
		{
			mnuTransfer_Click(mnuTransfer, new System.EventArgs());
		}
	}
}
