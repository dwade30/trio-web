//Fecher vbPorter - Version 1.0.0.93

using fecherFoundation;
using fecherFoundation.Extensions;

namespace TWPP0000
{
	public class cPersonalPropertyValues
	{

		//=========================================================


		private int lngAccount;
		private double dblTotal;
		private double dblBETE;
		private double dblNonBETE;
		private string strName;

		public int Account
		{
			set
			{
				lngAccount = value;
			}

			get
			{
					int Account = 0;
				Account = lngAccount;
				return Account;
			}
		}



		public double Total
		{
			set
			{
				dblTotal = value;
			}

			get
			{
					double Total = 0;
				Total = dblTotal;
				return Total;
			}
		}



		public double BETE
		{
			set
			{
				dblBETE = value;
			}

			get
			{
					double BETE = 0;
				BETE = dblBETE;
				return BETE;
			}
		}



		public double NonBETE
		{
			set
			{
				dblNonBETE = value;
			}

			get
			{
					double NonBETE = 0;
				NonBETE = dblNonBETE;
				return NonBETE;
			}
		}



		public string Name
		{
			set
			{
				strName = value;
			}

			get
			{
					string Name = "";
				Name = strName;
				return Name;
			}
		}



	}
}
