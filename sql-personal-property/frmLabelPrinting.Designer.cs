//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmLabelPrinting.
	/// </summary>
	partial class frmLabelPrinting : BaseForm
	{
		public fecherFoundation.FCComboBox cmbOrder;
		public fecherFoundation.FCLabel lblOrder;
		public fecherFoundation.FCComboBox cmbtPrint;
		public fecherFoundation.FCComboBox cmbtLabelType;
		public fecherFoundation.FCLabel lbltLabelType;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtRange;
		public fecherFoundation.FCCheckBox chkEliminate;
		public fecherFoundation.FCTextBox txtNumDuplicates;
		public fecherFoundation.FCFrame framLaserOptions;
		public fecherFoundation.FCCheckBox chkChooseLabelStart;
		public fecherFoundation.FCCheckBox chkAdjust;
		public fecherFoundation.FCTextBox txtAlignment;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCFrame Frame5;
		public fecherFoundation.FCComboBox cmbLabelType;
		public fecherFoundation.FCLabel lblDescription;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCCheckBox chkETax;
		public fecherFoundation.FCCheckBox chkValue;
		public fecherFoundation.FCCheckBox ChkLocation;
		public fecherFoundation.FCCheckBox chkOpen2;
		public fecherFoundation.FCCheckBox chkAccount;
		public fecherFoundation.FCCheckBox chkAddress;
		public fecherFoundation.FCCheckBox chkOpen1;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtRange_1;
		public fecherFoundation.FCTextBox txtRange_0;
		public fecherFoundation.FCLabel lblTo;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLabelPrinting));
            this.cmbOrder = new fecherFoundation.FCComboBox();
            this.lblOrder = new fecherFoundation.FCLabel();
            this.cmbtPrint = new fecherFoundation.FCComboBox();
            this.cmbtLabelType = new fecherFoundation.FCComboBox();
            this.lbltLabelType = new fecherFoundation.FCLabel();
            this.chkEliminate = new fecherFoundation.FCCheckBox();
            this.txtNumDuplicates = new fecherFoundation.FCTextBox();
            this.framLaserOptions = new fecherFoundation.FCFrame();
            this.chkChooseLabelStart = new fecherFoundation.FCCheckBox();
            this.chkAdjust = new fecherFoundation.FCCheckBox();
            this.txtAlignment = new fecherFoundation.FCTextBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Frame5 = new fecherFoundation.FCFrame();
            this.cmbLabelType = new fecherFoundation.FCComboBox();
            this.lblDescription = new fecherFoundation.FCLabel();
            this.Frame4 = new fecherFoundation.FCFrame();
            this.chkETax = new fecherFoundation.FCCheckBox();
            this.chkValue = new fecherFoundation.FCCheckBox();
            this.ChkLocation = new fecherFoundation.FCCheckBox();
            this.chkOpen2 = new fecherFoundation.FCCheckBox();
            this.chkAccount = new fecherFoundation.FCCheckBox();
            this.chkAddress = new fecherFoundation.FCCheckBox();
            this.chkOpen1 = new fecherFoundation.FCCheckBox();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.txtRange_1 = new fecherFoundation.FCTextBox();
            this.txtRange_0 = new fecherFoundation.FCTextBox();
            this.lblTo = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintPreview = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdPrintPreview = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkEliminate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framLaserOptions)).BeginInit();
            this.framLaserOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkChooseLabelStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAdjust)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
            this.Frame5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            this.Frame4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkETax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOpen2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOpen1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPrintPreview);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(942, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.chkEliminate);
            this.ClientArea.Controls.Add(this.txtNumDuplicates);
            this.ClientArea.Controls.Add(this.framLaserOptions);
            this.ClientArea.Controls.Add(this.Frame5);
            this.ClientArea.Controls.Add(this.Frame4);
            this.ClientArea.Controls.Add(this.cmbOrder);
            this.ClientArea.Controls.Add(this.lblOrder);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.cmbtLabelType);
            this.ClientArea.Controls.Add(this.lbltLabelType);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(942, 520);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(942, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(84, 30);
            this.HeaderText.Text = "Labels";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbOrder
            // 
            this.cmbOrder.Items.AddRange(new object[] {
            "Account Number",
            "Name",
            "Street",
            "Zip Code"});
            this.cmbOrder.Location = new System.Drawing.Point(642, 284);
            this.cmbOrder.Name = "cmbOrder";
            this.cmbOrder.Size = new System.Drawing.Size(254, 40);
            this.cmbOrder.TabIndex = 35;
            this.cmbOrder.Text = "Account Number";
            this.ToolTip1.SetToolTip(this.cmbOrder, null);
            // 
            // lblOrder
            // 
            this.lblOrder.AutoSize = true;
            this.lblOrder.Location = new System.Drawing.Point(516, 299);
            this.lblOrder.Name = "lblOrder";
            this.lblOrder.Size = new System.Drawing.Size(72, 15);
            this.lblOrder.TabIndex = 36;
            this.lblOrder.Text = "ORDER BY";
            this.ToolTip1.SetToolTip(this.lblOrder, null);
            // 
            // cmbtPrint
            // 
            this.cmbtPrint.Items.AddRange(new object[] {
            "All Accounts",
            "Individual",
            "Range"});
            this.cmbtPrint.Location = new System.Drawing.Point(20, 30);
            this.cmbtPrint.Name = "cmbtPrint";
            this.cmbtPrint.Size = new System.Drawing.Size(342, 40);
            this.cmbtPrint.TabIndex = 37;
            this.cmbtPrint.Text = "All Accounts";
            this.ToolTip1.SetToolTip(this.cmbtPrint, null);
            this.cmbtPrint.SelectedIndexChanged += new System.EventHandler(this.OptPrint_CheckedChanged);
            // 
            // cmbtLabelType
            // 
            this.cmbtLabelType.Items.AddRange(new object[] {
            "Regular Address Labels",
            "Address Labels with Location",
            "Name and Location Labels",
            "Assessment Labels - Card",
            "Declaration Labels",
            "Name and Selected Items"});
            this.cmbtLabelType.Location = new System.Drawing.Point(162, 30);
            this.cmbtLabelType.Name = "cmbtLabelType";
            this.cmbtLabelType.Size = new System.Drawing.Size(324, 40);
            this.cmbtLabelType.TabIndex = 1;
            this.cmbtLabelType.Text = "Regular Address Labels";
            this.ToolTip1.SetToolTip(this.cmbtLabelType, null);
            this.cmbtLabelType.SelectedIndexChanged += new System.EventHandler(this.OptLabelType_CheckedChanged);
            // 
            // lbltLabelType
            // 
            this.lbltLabelType.AutoSize = true;
            this.lbltLabelType.Location = new System.Drawing.Point(30, 44);
            this.lbltLabelType.Name = "lbltLabelType";
            this.lbltLabelType.Size = new System.Drawing.Size(102, 15);
            this.lbltLabelType.TabIndex = 38;
            this.lbltLabelType.Text = "TYPE OF LABEL";
            this.ToolTip1.SetToolTip(this.lbltLabelType, null);
            // 
            // chkEliminate
            // 
            this.chkEliminate.Location = new System.Drawing.Point(516, 344);
            this.chkEliminate.Name = "chkEliminate";
            this.chkEliminate.Size = new System.Drawing.Size(222, 27);
            this.chkEliminate.TabIndex = 34;
            this.chkEliminate.Text = "Eliminate Duplicate names";
            this.ToolTip1.SetToolTip(this.chkEliminate, null);
            // 
            // txtNumDuplicates
            // 
            this.txtNumDuplicates.BackColor = System.Drawing.SystemColors.Window;
            this.txtNumDuplicates.Location = new System.Drawing.Point(577, 391);
            this.txtNumDuplicates.Name = "txtNumDuplicates";
            this.txtNumDuplicates.Size = new System.Drawing.Size(65, 40);
            this.txtNumDuplicates.TabIndex = 31;
            this.txtNumDuplicates.Text = "0";
            this.txtNumDuplicates.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtNumDuplicates, null);
            // 
            // framLaserOptions
            // 
            this.framLaserOptions.Controls.Add(this.chkChooseLabelStart);
            this.framLaserOptions.Controls.Add(this.chkAdjust);
            this.framLaserOptions.Controls.Add(this.txtAlignment);
            this.framLaserOptions.Controls.Add(this.Label2);
            this.framLaserOptions.Location = new System.Drawing.Point(512, 451);
            this.framLaserOptions.Name = "framLaserOptions";
            this.framLaserOptions.Size = new System.Drawing.Size(384, 184);
            this.framLaserOptions.TabIndex = 24;
            this.framLaserOptions.Text = "Laser Printer Alignment";
            this.ToolTip1.SetToolTip(this.framLaserOptions, null);
            // 
            // chkChooseLabelStart
            // 
            this.chkChooseLabelStart.Location = new System.Drawing.Point(20, 30);
            this.chkChooseLabelStart.Name = "chkChooseLabelStart";
            this.chkChooseLabelStart.Size = new System.Drawing.Size(229, 27);
            this.chkChooseLabelStart.TabIndex = 27;
            this.chkChooseLabelStart.Text = "Choose Label to Start From";
            this.ToolTip1.SetToolTip(this.chkChooseLabelStart, null);
            // 
            // chkAdjust
            // 
            this.chkAdjust.Location = new System.Drawing.Point(21, 77);
            this.chkAdjust.Name = "chkAdjust";
            this.chkAdjust.Size = new System.Drawing.Size(189, 27);
            this.chkAdjust.TabIndex = 28;
            this.chkAdjust.Text = "Use Laser Adjustment";
            this.ToolTip1.SetToolTip(this.chkAdjust, null);
            // 
            // txtAlignment
            // 
            this.txtAlignment.BackColor = System.Drawing.SystemColors.Window;
            this.txtAlignment.Location = new System.Drawing.Point(20, 124);
            this.txtAlignment.Name = "txtAlignment";
            this.txtAlignment.Size = new System.Drawing.Size(65, 40);
            this.txtAlignment.TabIndex = 30;
            this.txtAlignment.Text = "0";
            this.txtAlignment.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtAlignment, null);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(108, 138);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(53, 15);
            this.Label2.TabIndex = 26;
            this.Label2.Text = "INCHES";
            this.ToolTip1.SetToolTip(this.Label2, null);
            // 
            // Frame5
            // 
            this.Frame5.Controls.Add(this.cmbLabelType);
            this.Frame5.Controls.Add(this.lblDescription);
            this.Frame5.Location = new System.Drawing.Point(30, 332);
            this.Frame5.Name = "Frame5";
            this.Frame5.Size = new System.Drawing.Size(456, 186);
            this.Frame5.TabIndex = 22;
            this.Frame5.Text = "Label Type";
            this.ToolTip1.SetToolTip(this.Frame5, null);
            // 
            // cmbLabelType
            // 
            this.cmbLabelType.BackColor = System.Drawing.SystemColors.Window;
            this.cmbLabelType.Location = new System.Drawing.Point(20, 30);
            this.cmbLabelType.Name = "cmbLabelType";
            this.cmbLabelType.Size = new System.Drawing.Size(418, 40);
            this.cmbLabelType.TabIndex = 23;
            this.ToolTip1.SetToolTip(this.cmbLabelType, null);
            this.cmbLabelType.SelectedIndexChanged += new System.EventHandler(this.cmbLabelType_SelectedIndexChanged);
            // 
            // lblDescription
            // 
            this.lblDescription.Location = new System.Drawing.Point(20, 90);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(418, 76);
            this.lblDescription.TabIndex = 25;
            this.lblDescription.Text = "LABEL1";
            this.ToolTip1.SetToolTip(this.lblDescription, null);
            // 
            // Frame4
            // 
            this.Frame4.Controls.Add(this.chkETax);
            this.Frame4.Controls.Add(this.chkValue);
            this.Frame4.Controls.Add(this.ChkLocation);
            this.Frame4.Controls.Add(this.chkOpen2);
            this.Frame4.Controls.Add(this.chkAccount);
            this.Frame4.Controls.Add(this.chkAddress);
            this.Frame4.Controls.Add(this.chkOpen1);
            this.Frame4.Location = new System.Drawing.Point(30, 94);
            this.Frame4.Name = "Frame4";
            this.Frame4.Size = new System.Drawing.Size(456, 218);
            this.Frame4.TabIndex = 15;
            this.Frame4.Text = "Include";
            this.ToolTip1.SetToolTip(this.Frame4, null);
            // 
            // chkETax
            // 
            this.chkETax.Enabled = false;
            this.chkETax.Location = new System.Drawing.Point(20, 171);
            this.chkETax.Name = "chkETax";
            this.chkETax.Size = new System.Drawing.Size(132, 27);
            this.chkETax.TabIndex = 38;
            this.chkETax.Text = "Estimated Tax";
            this.ToolTip1.SetToolTip(this.chkETax, "To change whether book & page or Reference 1 is printed go to File Maintenance ->" +
        " Customize");
            // 
            // chkValue
            // 
            this.chkValue.Enabled = false;
            this.chkValue.Location = new System.Drawing.Point(224, 77);
            this.chkValue.Name = "chkValue";
            this.chkValue.Size = new System.Drawing.Size(69, 27);
            this.chkValue.TabIndex = 21;
            this.chkValue.Text = "Value";
            this.ToolTip1.SetToolTip(this.chkValue, null);
            // 
            // ChkLocation
            // 
            this.ChkLocation.Enabled = false;
            this.ChkLocation.Location = new System.Drawing.Point(20, 77);
            this.ChkLocation.Name = "ChkLocation";
            this.ChkLocation.Size = new System.Drawing.Size(89, 27);
            this.ChkLocation.TabIndex = 20;
            this.ChkLocation.Text = "Location";
            this.ToolTip1.SetToolTip(this.ChkLocation, null);
            // 
            // chkOpen2
            // 
            this.chkOpen2.Enabled = false;
            this.chkOpen2.Location = new System.Drawing.Point(224, 124);
            this.chkOpen2.Name = "chkOpen2";
            this.chkOpen2.Size = new System.Drawing.Size(81, 27);
            this.chkOpen2.TabIndex = 19;
            this.chkOpen2.Text = "Open 2";
            this.ToolTip1.SetToolTip(this.chkOpen2, null);
            // 
            // chkAccount
            // 
            this.chkAccount.Enabled = false;
            this.chkAccount.Location = new System.Drawing.Point(20, 30);
            this.chkAccount.Name = "chkAccount";
            this.chkAccount.Size = new System.Drawing.Size(86, 27);
            this.chkAccount.TabIndex = 18;
            this.chkAccount.Text = "Account";
            this.ToolTip1.SetToolTip(this.chkAccount, null);
            // 
            // chkAddress
            // 
            this.chkAddress.Enabled = false;
            this.chkAddress.Location = new System.Drawing.Point(224, 30);
            this.chkAddress.Name = "chkAddress";
            this.chkAddress.Size = new System.Drawing.Size(87, 27);
            this.chkAddress.TabIndex = 17;
            this.chkAddress.Text = "Address";
            this.ToolTip1.SetToolTip(this.chkAddress, null);
            // 
            // chkOpen1
            // 
            this.chkOpen1.Enabled = false;
            this.chkOpen1.Location = new System.Drawing.Point(20, 124);
            this.chkOpen1.Name = "chkOpen1";
            this.chkOpen1.Size = new System.Drawing.Size(81, 27);
            this.chkOpen1.TabIndex = 16;
            this.chkOpen1.Text = "Open 1";
            this.ToolTip1.SetToolTip(this.chkOpen1, "To change whether book & page or Reference 1 is printed go to File Maintenance ->" +
        " Customize");
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.txtRange_1);
            this.Frame2.Controls.Add(this.cmbtPrint);
            this.Frame2.Controls.Add(this.txtRange_0);
            this.Frame2.Controls.Add(this.lblTo);
            this.Frame2.Location = new System.Drawing.Point(516, 30);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(380, 234);
            this.Frame2.TabIndex = 6;
            this.Frame2.Text = "Print";
            this.ToolTip1.SetToolTip(this.Frame2, null);
            // 
            // txtRange_1
            // 
            this.txtRange_1.BackColor = System.Drawing.SystemColors.Window;
            this.txtRange_1.Location = new System.Drawing.Point(20, 175);
            this.txtRange_1.Name = "txtRange_1";
            this.txtRange_1.Size = new System.Drawing.Size(342, 40);
            this.txtRange_1.TabIndex = 36;
            this.ToolTip1.SetToolTip(this.txtRange_1, null);
            this.txtRange_1.Visible = false;
            // 
            // txtRange_0
            // 
            this.txtRange_0.BackColor = System.Drawing.SystemColors.Window;
            this.txtRange_0.Location = new System.Drawing.Point(20, 90);
            this.txtRange_0.Name = "txtRange_0";
            this.txtRange_0.Size = new System.Drawing.Size(342, 40);
            this.txtRange_0.TabIndex = 35;
            this.ToolTip1.SetToolTip(this.txtRange_0, null);
            this.txtRange_0.Visible = false;
            // 
            // lblTo
            // 
            this.lblTo.Alignment = Wisej.Web.HorizontalAlignment.Center;
            this.lblTo.Location = new System.Drawing.Point(20, 153);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(342, 15);
            this.lblTo.TabIndex = 37;
            this.lblTo.Text = "TO";
            this.lblTo.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.lblTo, null);
            this.lblTo.Visible = false;
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(665, 405);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(133, 15);
            this.Label3.TabIndex = 32;
            this.Label3.Text = "DUPLICATE LABELS";
            this.ToolTip1.SetToolTip(this.Label3, null);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(516, 405);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(38, 15);
            this.Label1.TabIndex = 29;
            this.Label1.Text = "PRINT";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.ToolTip1.SetToolTip(this.Label1, null);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPrintPreview,
            this.mnuSepar,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuPrintPreview
            // 
            this.mnuPrintPreview.Index = 0;
            this.mnuPrintPreview.Name = "mnuPrintPreview";
            this.mnuPrintPreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuPrintPreview.Text = "Print Preview";
            this.mnuPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 1;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdPrintPreview
            // 
            this.cmdPrintPreview.AppearanceKey = "acceptButton";
            this.cmdPrintPreview.Location = new System.Drawing.Point(398, 30);
            this.cmdPrintPreview.Name = "cmdPrintPreview";
            this.cmdPrintPreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrintPreview.Size = new System.Drawing.Size(122, 48);
            this.cmdPrintPreview.Text = "Print Preview";
            this.cmdPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
            // 
            // frmLabelPrinting
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(942, 688);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmLabelPrinting";
            this.Text = "Labels";
            this.ToolTip1.SetToolTip(this, null);
            this.Load += new System.EventHandler(this.frmLabelPrinting_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmLabelPrinting_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkEliminate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framLaserOptions)).EndInit();
            this.framLaserOptions.ResumeLayout(false);
            this.framLaserOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkChooseLabelStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAdjust)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
            this.Frame5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            this.Frame4.ResumeLayout(false);
            this.Frame4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkETax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOpen2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOpen1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdPrintPreview;
	}
}