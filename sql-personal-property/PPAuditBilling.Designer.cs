﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmPPAuditBilling.
	/// </summary>
	partial class frmPPAuditBilling : BaseForm
	{
		public fecherFoundation.FCComboBox cmbSequence;
		public fecherFoundation.FCLabel lblSequence;
		public fecherFoundation.FCComboBox cmbDetails;
		public fecherFoundation.FCLabel lblDetails;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtStarting;
		public fecherFoundation.FCTextBox txtEnding;
		public fecherFoundation.FCLabel lblStarting;
		public fecherFoundation.FCLabel lblEnding;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCCheckBox chkLabels;
		public FCCommonDialog dlgPrint;
		public fecherFoundation.FCLabel lblScreenTitle;
		public fecherFoundation.FCLabel lblDate;
		public fecherFoundation.FCLabel lblTime;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPPAuditBilling));
			this.cmbSequence = new fecherFoundation.FCComboBox();
			this.lblSequence = new fecherFoundation.FCLabel();
			this.cmbDetails = new fecherFoundation.FCComboBox();
			this.lblDetails = new fecherFoundation.FCLabel();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.txtStarting = new fecherFoundation.FCTextBox();
			this.txtEnding = new fecherFoundation.FCTextBox();
			this.lblStarting = new fecherFoundation.FCLabel();
			this.lblEnding = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.chkLabels = new fecherFoundation.FCCheckBox();
			this.dlgPrint = new fecherFoundation.FCCommonDialog();
			this.lblScreenTitle = new fecherFoundation.FCLabel();
			this.lblDate = new fecherFoundation.FCLabel();
			this.lblTime = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkLabels)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdPrint);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Controls.Add(this.cmbDetails);
			this.ClientArea.Controls.Add(this.lblDetails);
			this.ClientArea.Controls.Add(this.chkLabels);
			this.ClientArea.Controls.Add(this.lblScreenTitle);
			this.ClientArea.Controls.Add(this.lblDate);
			this.ClientArea.Controls.Add(this.lblTime);
			this.ClientArea.Size = new System.Drawing.Size(1078, 498);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(180, 30);
			this.HeaderText.Text = "Audit Summary";
			// 
			// cmbSequence
			// 
			this.cmbSequence.AutoSize = false;
			this.cmbSequence.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbSequence.FormattingEnabled = true;
			this.cmbSequence.Items.AddRange(new object[] {
				"Account",
				"Name",
				"Location",
				"",
				"",
				"Tran Code"
			});
			this.cmbSequence.Location = new System.Drawing.Point(206, 30);
			this.cmbSequence.Name = "cmbSequence";
			this.cmbSequence.Size = new System.Drawing.Size(257, 40);
			//FC:FINAL:CHN - issue #1477: Incorrect tabbing.
			// this.cmbSequence.TabIndex = 10;
			this.cmbSequence.TabIndex = 4;
			this.cmbSequence.Text = "Account";
			this.cmbSequence.SelectedIndexChanged += new System.EventHandler(this.optSequence_CheckedChanged);
			// 
			// lblSequence
			// 
			this.lblSequence.AutoSize = true;
			this.lblSequence.Location = new System.Drawing.Point(20, 44);
			this.lblSequence.Name = "lblSequence";
			this.lblSequence.Size = new System.Drawing.Size(77, 15);
			this.lblSequence.TabIndex = 11;
			this.lblSequence.Text = "SEQUENCE";
			// 
			// cmbDetails
			// 
			this.cmbDetails.AutoSize = false;
			this.cmbDetails.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbDetails.FormattingEnabled = true;
			this.cmbDetails.Items.AddRange(new object[] {
				"Detailed",
				"Final Totals"
			});
			this.cmbDetails.Location = new System.Drawing.Point(236, 30);
			this.cmbDetails.Name = "cmbDetails";
			this.cmbDetails.Size = new System.Drawing.Size(257, 40);
			//FC:FINAL:CHN - issue #1477: Incorrect tabbing.
			// this.cmbDetails.TabIndex = 18;
			this.cmbDetails.TabIndex = 0;
			this.cmbDetails.Text = "Detailed";
			this.cmbDetails.SelectedIndexChanged += new System.EventHandler(this.optDetails_CheckedChanged);
			// 
			// lblDetails
			// 
			this.lblDetails.AutoSize = true;
			this.lblDetails.Location = new System.Drawing.Point(30, 44);
			this.lblDetails.Name = "lblDetails";
			this.lblDetails.Size = new System.Drawing.Size(95, 15);
			this.lblDetails.TabIndex = 19;
			this.lblDetails.Text = "REPORT TYPE";
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.txtStarting);
			this.Frame2.Controls.Add(this.cmbSequence);
			this.Frame2.Controls.Add(this.lblSequence);
			this.Frame2.Controls.Add(this.txtEnding);
			this.Frame2.Controls.Add(this.lblStarting);
			this.Frame2.Controls.Add(this.lblEnding);
			this.Frame2.Controls.Add(this.Label3);
			this.Frame2.Location = new System.Drawing.Point(30, 132);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(626, 245);
			this.Frame2.TabIndex = 17;
			this.Frame2.Text = "Sequence & Range";
			// 
			// txtStarting
			// 
			this.txtStarting.AutoSize = false;
			this.txtStarting.BackColor = System.Drawing.SystemColors.Window;
			this.txtStarting.LinkItem = null;
			this.txtStarting.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtStarting.LinkTopic = null;
			this.txtStarting.Location = new System.Drawing.Point(206, 125);
			this.txtStarting.MaxLength = 15;
			this.txtStarting.Name = "txtStarting";
			this.txtStarting.Size = new System.Drawing.Size(257, 40);
			this.txtStarting.TabIndex = 9;
			// 
			// txtEnding
			// 
			this.txtEnding.AutoSize = false;
			this.txtEnding.BackColor = System.Drawing.SystemColors.Window;
			this.txtEnding.LinkItem = null;
			this.txtEnding.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtEnding.LinkTopic = null;
			this.txtEnding.Location = new System.Drawing.Point(206, 185);
			this.txtEnding.MaxLength = 15;
			this.txtEnding.Name = "txtEnding";
			this.txtEnding.Size = new System.Drawing.Size(257, 40);
			this.txtEnding.TabIndex = 10;
			// 
			// lblStarting
			// 
			this.lblStarting.Location = new System.Drawing.Point(20, 139);
			this.lblStarting.Name = "lblStarting";
			this.lblStarting.Size = new System.Drawing.Size(160, 16);
			this.lblStarting.TabIndex = 20;
			this.lblStarting.Text = "STARTING ACCOUNT";
			// 
			// lblEnding
			// 
			this.lblEnding.Location = new System.Drawing.Point(20, 199);
			this.lblEnding.Name = "lblEnding";
			this.lblEnding.Size = new System.Drawing.Size(160, 16);
			this.lblEnding.TabIndex = 19;
			this.lblEnding.Text = "ENDING ACCOUNT";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(20, 90);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(586, 15);
			this.Label3.TabIndex = 18;
			this.Label3.Text = "IF BOTH STARTING AND ENDING RANGES ARE LEFT BLANK, THEN THE WHOLE FILE WILL BE PR" + "OCESSED";
			// 
			// cmdPrint
			// 
			this.cmdPrint.AppearanceKey = "acceptButton";
			this.cmdPrint.Location = new System.Drawing.Point(454, 30);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPrint.Size = new System.Drawing.Size(127, 48);
			this.cmdPrint.TabIndex = 11;
			this.cmdPrint.Text = "Print Preview";
			this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
			// 
			// chkLabels
			// 
			this.chkLabels.Location = new System.Drawing.Point(30, 85);
			this.chkLabels.Name = "chkLabels";
			this.chkLabels.Size = new System.Drawing.Size(145, 27);
			this.chkLabels.TabIndex = 3;
			this.chkLabels.Text = "Include Address";
			// 
			// dlgPrint
			// 
			this.dlgPrint.Color = System.Drawing.Color.Black;
			this.dlgPrint.DefaultExt = null;
			this.dlgPrint.FilterIndex = ((short)(0));
			this.dlgPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.dlgPrint.FontName = "Microsoft Sans Serif";
			this.dlgPrint.FontSize = 8.25F;
			this.dlgPrint.ForeColor = System.Drawing.Color.Black;
			this.dlgPrint.FromPage = 0;
			this.dlgPrint.Location = new System.Drawing.Point(0, 0);
			this.dlgPrint.Name = "dlgPrint";
			this.dlgPrint.PrinterSettings = null;
			this.dlgPrint.Size = new System.Drawing.Size(0, 0);
			this.dlgPrint.TabIndex = 0;
			this.dlgPrint.ToPage = 0;
			// 
			// lblScreenTitle
			// 
			this.lblScreenTitle.Location = new System.Drawing.Point(79, 24);
			this.lblScreenTitle.Name = "lblScreenTitle";
			this.lblScreenTitle.Size = new System.Drawing.Size(452, 22);
			this.lblScreenTitle.TabIndex = 15;
			this.lblScreenTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.lblScreenTitle.Visible = false;
			// 
			// lblDate
			// 
			this.lblDate.Location = new System.Drawing.Point(805, 24);
			this.lblDate.Name = "lblDate";
			this.lblDate.Size = new System.Drawing.Size(110, 24);
			this.lblDate.TabIndex = 14;
			this.lblDate.TextAlign = System.Drawing.ContentAlignment.TopRight;
			//FC:FINAL:CHN - issue #1478: Delete extra date (at original app was out of screen).
			this.lblDate.Visible = false;
			// 
			// lblTime
			// 
			this.lblTime.Location = new System.Drawing.Point(805, 46);
			this.lblTime.Name = "lblTime";
			this.lblTime.Size = new System.Drawing.Size(110, 24);
			this.lblTime.TabIndex = 13;
			this.lblTime.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuPrint,
				this.mnuSepar,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuPrint
			// 
			this.mnuPrint.Index = 0;
			this.mnuPrint.Name = "mnuPrint";
			this.mnuPrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuPrint.Text = "Print Preview";
			this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 1;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// frmPPAuditBilling
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmPPAuditBilling";
			this.ShowInTaskbar = false;
			this.Text = "Audit Summary";
			this.Load += new System.EventHandler(this.frmPPAuditBilling_Load);
			this.Activated += new System.EventHandler(this.frmPPAuditBilling_Activated);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPPAuditBilling_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPPAuditBilling_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			this.Frame2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkLabels)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
