﻿//Fecher vbPorter - Version 1.0.0.32
namespace TWPP0000
{
	public class c801ADetail
	{
		//=========================================================
		private string strDescription = string.Empty;
		private string strStateOfOrigin = string.Empty;
		private int intNumberOfYearsClaimed;
		private double dblOriginalCost;
		private double dblAssessedValue;
		private int intMonthPlacedInService;
		private int intYearPlacedInService;

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public string StateOfOrigin
		{
			set
			{
				strStateOfOrigin = value;
			}
			get
			{
				string StateOfOrigin = "";
				StateOfOrigin = strStateOfOrigin;
				return StateOfOrigin;
			}
		}

		public int NumberOfYearsClaimed
		{
			set
			{
				intNumberOfYearsClaimed = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				int NumberOfYearsClaimed = 0;
				NumberOfYearsClaimed = intNumberOfYearsClaimed;
				return NumberOfYearsClaimed;
			}
		}

		public double OriginalCost
		{
			set
			{
				dblOriginalCost = value;
			}
			get
			{
				double OriginalCost = 0;
				OriginalCost = dblOriginalCost;
				return OriginalCost;
			}
		}

		public double AssessedValue
		{
			set
			{
				dblAssessedValue = value;
			}
			get
			{
				double AssessedValue = 0;
				AssessedValue = dblAssessedValue;
				return AssessedValue;
			}
		}

		public int MonthPlacedInService
		{
			set
			{
				intMonthPlacedInService = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				int MonthPlacedInService = 0;
				MonthPlacedInService = intMonthPlacedInService;
				return MonthPlacedInService;
			}
		}

		public int YearPlacedInService
		{
			set
			{
				intYearPlacedInService = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				int YearPlacedInService = 0;
				YearPlacedInService = intYearPlacedInService;
				return YearPlacedInService;
			}
		}
	}
}
