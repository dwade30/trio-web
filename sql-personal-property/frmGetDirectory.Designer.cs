﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmGetDirectory.
	/// </summary>
	partial class frmGetDirectory : BaseForm
	{
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCDriveListBox Drive1;
		public fecherFoundation.FCButton cmdDone;
		public fecherFoundation.FCDirListBox Dir1;
		public fecherFoundation.FCPictureBox image1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGetDirectory));
			this.cmdCancel = new fecherFoundation.FCButton();
			this.Drive1 = new fecherFoundation.FCDriveListBox();
			this.cmdDone = new fecherFoundation.FCButton();
			this.Dir1 = new fecherFoundation.FCDirListBox();
			this.image1 = new fecherFoundation.FCPictureBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.image1)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 398);
			this.BottomPanel.Size = new System.Drawing.Size(423, 10);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdCancel);
			this.ClientArea.Controls.Add(this.Drive1);
			this.ClientArea.Controls.Add(this.cmdDone);
			this.ClientArea.Controls.Add(this.Dir1);
			this.ClientArea.Controls.Add(this.image1);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(423, 338);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(423, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(227, 30);
			this.HeaderText.Text = "Trio Setup Program";
			// 
			// cmdCancel
			// 
			this.cmdCancel.AppearanceKey = "actionButton";
			this.cmdCancel.ForeColor = System.Drawing.Color.White;
			this.cmdCancel.Location = new System.Drawing.Point(157, 275);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(96, 40);
			this.cmdCancel.TabIndex = 5;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.Visible = false;
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// Drive1
			// 
			this.Drive1.Location = new System.Drawing.Point(30, 65);
			this.Drive1.Name = "Drive1";
			this.Drive1.Size = new System.Drawing.Size(215, 22);
			this.Drive1.TabIndex = 4;
			this.Drive1.SelectedIndexChanged += new System.EventHandler(this.Drive1_SelectedIndexChanged);
			// 
			// cmdDone
			// 
			this.cmdDone.AppearanceKey = "actionButton";
			this.cmdDone.ForeColor = System.Drawing.Color.White;
			this.cmdDone.Location = new System.Drawing.Point(30, 275);
			this.cmdDone.Name = "cmdDone";
			this.cmdDone.Size = new System.Drawing.Size(96, 40);
			this.cmdDone.TabIndex = 3;
			this.cmdDone.Text = "Continue";
			this.cmdDone.Click += new System.EventHandler(this.cmdDone_Click);
			// 
			// Dir1
			// 
			this.Dir1.Location = new System.Drawing.Point(30, 107);
			this.Dir1.Name = "Dir1";
			this.Dir1.Size = new System.Drawing.Size(215, 103);
			this.Dir1.TabIndex = 0;
			this.Dir1.Click += new System.EventHandler(this.Dir1_Click);
			// 
			// image1
			// 
			this.image1.AllowDrop = true;
			this.image1.BorderStyle = Wisej.Web.BorderStyle.None;
			this.image1.DrawStyle = ((short)(0));
			this.image1.DrawWidth = ((short)(1));
			this.image1.FillStyle = ((short)(1));
			this.image1.FontTransparent = true;
			this.image1.Image = ((System.Drawing.Image)(resources.GetObject("image1.Image")));
			this.image1.Location = new System.Drawing.Point(275, 107);
			this.image1.Name = "image1";
			this.image1.Picture = ((System.Drawing.Image)(resources.GetObject("image1.Picture")));
			this.image1.Size = new System.Drawing.Size(59, 55);
			this.image1.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.image1.TabIndex = 6;
			this.image1.Visible = false;
			this.image1.MouseDown += new Wisej.Web.MouseEventHandler(this.image1_MouseDown);
			this.image1.MouseUp += new Wisej.Web.MouseEventHandler(this.image1_MouseUp);
			this.image1.Click += new System.EventHandler(this.image1_Click);
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 240);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(369, 15);
			this.Label2.TabIndex = 2;
			this.Label2.Text = "THEN CLICK CONTINUE TO PROCEED WITH THE SETUP PROCESS";
			this.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(307, 15);
			this.Label1.TabIndex = 1;
			this.Label1.Text = "PLEASE SELECT YOUR EXECUTABLE (TRIOVB) FOLDER";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// frmGetDirectory
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(423, 408);
			this.ControlBox = false;
			this.FillColor = 0;
			this.Name = "frmGetDirectory";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Trio Setup Program";
			this.Load += new System.EventHandler(this.frmGetDirectory_Load);
			this.Activated += new System.EventHandler(this.frmGetDirectory_Activated);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.image1)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
