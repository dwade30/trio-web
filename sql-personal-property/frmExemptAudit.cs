﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmExemptAudit.
	/// </summary>
	public partial class frmExemptAudit : BaseForm
	{
		public frmExemptAudit()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmExemptAudit InstancePtr
		{
			get
			{
				return (frmExemptAudit)Sys.GetInstance(typeof(frmExemptAudit));
			}
		}

		protected frmExemptAudit _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		/// <summary>
		/// ********************************************************
		/// </summary>
		/// <summary>
		/// Property of TRIO Software Corporation
		/// </summary>
		/// <summary>
		/// Written By
		/// </summary>
		/// <summary>
		/// Date
		/// </summary>
		/// <summary>
		/// ********************************************************
		/// </summary>
		private bool boolLoading;

		private void frmExemptAudit_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmExemptAudit_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmExemptAudit properties;
			//frmExemptAudit.FillStyle	= 0;
			//frmExemptAudit.ScaleWidth	= 5880;
			//frmExemptAudit.ScaleHeight	= 4050;
			//frmExemptAudit.LinkTopic	= "Form2";
			//frmExemptAudit.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			boolLoading = true;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGridTownCode();
			if (modGlobal.Statics.CustomizeStuff.boolRegionalTown)
			{
				GridTownCode.TextMatrix(0, 0, FCConvert.ToString(1));
				modGlobal.Statics.CustomizeStuff.CurrentTown = 1;
			}
			else
			{
				GridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
			}
			boolLoading = false;
		}

		private void SetupGridTownCode()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			if (modGlobal.Statics.CustomizeStuff.boolRegionalTown)
			{
				strTemp = "";
				clsLoad.OpenRecordset("select * from TranCodes where trancode > 0 order by trancode", modPPGN.strPPDatabase);
				while (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [trancode] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [trancode] and replace with corresponding Get_Field method
					strTemp += "#" + clsLoad.Get_Fields("trancode") + ";" + clsLoad.Get_Fields_String("trantype") + "\t" + clsLoad.Get_Fields("trancode") + "|";
					clsLoad.MoveNext();
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				GridTownCode.ColComboList(0, strTemp);
				GridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
				GridTownCode.Visible = true;
			}
			else
			{
				GridTownCode.Rows = 1;
				GridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
				GridTownCode.Visible = false;
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void ResizeGridTownCode()
		{
			//GridTownCode.HeightOriginal = GridTownCode.RowHeight(0) + 60;
		}

		private void GridTownCode_ComboCloseUp(object sender, EventArgs e)
		{
			// CustomizeStuff.TaxRate = Val(txtTaxRate.Text)
			if (!boolLoading)
			{
				if (modGlobal.Statics.CustomizeStuff.boolRegionalTown)
				{
					modGlobal.Statics.CustomizeStuff.CurrentTown = FCConvert.ToInt32(Math.Round(Conversion.Val(GridTownCode.ComboData())));
				}
			}
		}

		private void GridTownCode_Leave(object sender, System.EventArgs e)
		{
			// DoEvents
			// If Not boolLoading Then
			// If lngLastTownCode = Val(GridTownCode.TextMatrix(0, 0)) Then Exit Sub
			// lngLastTownCode = Val(GridTownCode.TextMatrix(0, 0))
			// End If
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			// VB6 Bad Scope Dim:
			bool boolDetail = false;
			int intOrder = 0;
			string strList;
			strList = "";
			if (cmbAll.SelectedIndex == 1)
			{
				// make list
				string strReturn = "";
				clsDRWrapper rsLoad = new clsDRWrapper();
				GridTownCode.Row = -1;
				////Application.DoEvents();
				rsLoad.OpenRecordset("select * from exemptcodes where amount > 0 or description <> 'EXEMPTION' order by description", modPPGN.strPPDatabase);
				while (!rsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					strList += rsLoad.Get_Fields("code") + ";" + rsLoad.Get_Fields_String("Description") + "|";
					rsLoad.MoveNext();
				}
				if (strList != "")
				{
					strList = Strings.Mid(strList, 1, strList.Length - 1);
					strReturn = frmListChoiceWithIDs.InstancePtr.Init(ref strList, "Select Exemptions", "Select exemption(s) to audit", true, "", true);
					if (strReturn != "")
					{
						strList = strReturn;
					}
					else
					{
						return;
					}
				}
				else
				{
					MessageBox.Show("No setup exemptions found", "No Exemptions", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (cmbSubTotals.SelectedIndex == 1)
			{
				boolDetail = true;
			}
			else
			{
				boolDetail = false;
			}
			if (cmbAccount.SelectedIndex == 0)
			{
				intOrder = 0;
			}
			else
			{
				intOrder = 1;
			}
			rptExemptAudit.InstancePtr.Init(boolDetail, intOrder, strList, modGlobal.Statics.CustomizeStuff.CurrentTown);
		}

		private void frmExemptAudit_Resize(object sender, System.EventArgs e)
		{
			ResizeGridTownCode();
		}
	}
}
