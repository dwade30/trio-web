﻿//Fecher vbPorter - Version 1.0.0.32
using fecherFoundation;

namespace TWPP0000
{
	public class c801BDetail
	{
		//=========================================================
		private double dblME;
		private double dblFurniture;
		private double dblOther;
		// vbPorter upgrade warning: dblValueLimitation As Variant --> As double
		private double dblValueLimitation;
		private double dblMEAssessed;
		private double dblFurnitureAssessed;
		private double dblOtherAssessed;
		private int intYearsClaimed;

		public short YearsClaimed
		{
			set
			{
				intYearsClaimed = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short YearsClaimed = 0;
				YearsClaimed = FCConvert.ToInt16(intYearsClaimed);
				return YearsClaimed;
			}
		}

		public double MachineryAndEquimpmentAssessed
		{
			set
			{
				dblMEAssessed = value;
			}
			get
			{
				double MachineryAndEquimpmentAssessed = 0;
				MachineryAndEquimpmentAssessed = dblMEAssessed;
				return MachineryAndEquimpmentAssessed;
			}
		}

		public double FurnitureAssessed
		{
			set
			{
				dblFurnitureAssessed = value;
			}
			get
			{
				double FurnitureAssessed = 0;
				FurnitureAssessed = dblFurnitureAssessed;
				return FurnitureAssessed;
			}
		}

		public double OtherAssessed
		{
			set
			{
				dblOtherAssessed = value;
			}
			get
			{
				double OtherAssessed = 0;
				OtherAssessed = dblOtherAssessed;
				return OtherAssessed;
			}
		}

		public double AssessedTotals
		{
			get
			{
				double AssessedTotals = 0;
				AssessedTotals = dblMEAssessed + dblFurnitureAssessed + dblOtherAssessed;
				return AssessedTotals;
			}
		}

		public double OriginalTotals
		{
			get
			{
				double OriginalTotals = 0;
				OriginalTotals = dblME + dblFurniture + dblOther;
				return OriginalTotals;
			}
		}
		// vbPorter upgrade warning: 'Return' As double	OnWrite(string)
		public double AllowableAssessedValue
		{
			get
			{
				double AllowableAssessedValue = 0;
				AllowableAssessedValue = FCConvert.ToDouble(Strings.Format(AssessedTotals * dblValueLimitation, "0"));
				return AllowableAssessedValue;
			}
		}

		public double ValueLimitation
		{
			set
			{
				dblValueLimitation = value;
			}
			get
			{
				double ValueLimitation = 0;
				ValueLimitation = dblValueLimitation;
				return ValueLimitation;
			}
		}

		public double Other
		{
			set
			{
				dblOther = value;
			}
			get
			{
				double Other = 0;
				Other = dblOther;
				return Other;
			}
		}

		public double Furniture
		{
			set
			{
				dblFurniture = value;
			}
			get
			{
				double Furniture = 0;
				Furniture = dblFurniture;
				return Furniture;
			}
		}

		public double MachineryAndEquipment
		{
			set
			{
				dblME = value;
			}
			get
			{
				double MachineryAndEquipment = 0;
				MachineryAndEquipment = dblME;
				return MachineryAndEquipment;
			}
		}
	}
}
