﻿namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptExemptAudit.
	/// </summary>
	partial class rptExemptAudit
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptExemptAudit));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtProperty = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.txtTotAssessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotProperty = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.txtTown = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.txtGroup = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.txtSubAssess = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSubExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSubProperty = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtProperty)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotAssessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotProperty)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubAssess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubProperty)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAccount,
				this.txtName,
				this.txtAssessment,
				this.txtExemption,
				this.txtProperty
			});
			this.Detail.Height = 0.1666667F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.1875F;
			this.txtAccount.Left = 0F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtAccount.Text = "Account";
			this.txtAccount.Top = 0F;
			this.txtAccount.Width = 0.6875F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1875F;
			this.txtName.Left = 0.75F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtName.Text = "Name";
			this.txtName.Top = 0F;
			this.txtName.Width = 3.395833F;
			// 
			// txtAssessment
			// 
			this.txtAssessment.Height = 0.1875F;
			this.txtAssessment.Left = 6.375F;
			this.txtAssessment.Name = "txtAssessment";
			this.txtAssessment.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtAssessment.Text = "Assessment";
			this.txtAssessment.Top = 0F;
			this.txtAssessment.Width = 1.0625F;
			// 
			// txtExemption
			// 
			this.txtExemption.Height = 0.1875F;
			this.txtExemption.Left = 5.333333F;
			this.txtExemption.Name = "txtExemption";
			this.txtExemption.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtExemption.Text = "Exemption";
			this.txtExemption.Top = 0F;
			this.txtExemption.Width = 1F;
			// 
			// txtProperty
			// 
			this.txtProperty.Height = 0.1875F;
			this.txtProperty.Left = 4.21875F;
			this.txtProperty.Name = "txtProperty";
			this.txtProperty.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtProperty.Text = "Property";
			this.txtProperty.Top = 0F;
			this.txtProperty.Width = 1.072917F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTotAssessment,
				this.txtTotExempt,
				this.txtTotProperty,
				this.Line2,
				this.Field19
			});
			this.ReportFooter.Height = 0.2916667F;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			// 
			// txtTotAssessment
			// 
			this.txtTotAssessment.Height = 0.1875F;
			this.txtTotAssessment.Left = 6.375F;
			this.txtTotAssessment.Name = "txtTotAssessment";
			this.txtTotAssessment.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.txtTotAssessment.Text = "Assessment";
			this.txtTotAssessment.Top = 0.0625F;
			this.txtTotAssessment.Width = 1.0625F;
			// 
			// txtTotExempt
			// 
			this.txtTotExempt.Height = 0.1875F;
			this.txtTotExempt.Left = 5.333333F;
			this.txtTotExempt.Name = "txtTotExempt";
			this.txtTotExempt.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.txtTotExempt.Text = "Exemption";
			this.txtTotExempt.Top = 0.0625F;
			this.txtTotExempt.Width = 1F;
			// 
			// txtTotProperty
			// 
			this.txtTotProperty.Height = 0.1875F;
			this.txtTotProperty.Left = 4.21875F;
			this.txtTotProperty.Name = "txtTotProperty";
			this.txtTotProperty.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.txtTotProperty.Text = "Property";
			this.txtTotProperty.Top = 0.0625F;
			this.txtTotProperty.Width = 1.072917F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 4.052083F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.03125F;
			this.Line2.Width = 3.375F;
			this.Line2.X1 = 4.052083F;
			this.Line2.X2 = 7.427083F;
			this.Line2.Y1 = 0.03125F;
			this.Line2.Y2 = 0.03125F;
			// 
			// Field19
			// 
			this.Field19.Height = 0.1875F;
			this.Field19.Left = 2.875F;
			this.Field19.Name = "Field19";
			this.Field19.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.Field19.Text = "Total";
			this.Field19.Top = 0.0625F;
			this.Field19.Width = 1.072917F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTown,
				this.Field1,
				this.txtDate,
				this.txtPage,
				this.txtTime,
				this.Field2,
				this.Field3,
				this.Field4,
				this.Field5,
				this.Field6
			});
			this.PageHeader.Height = 0.7708333F;
			this.PageHeader.Name = "PageHeader";
			// 
			// txtTown
			// 
			this.txtTown.Height = 0.1875F;
			this.txtTown.Left = 0.0625F;
			this.txtTown.Name = "txtTown";
			this.txtTown.Style = "font-family: \'Tahoma\'";
			this.txtTown.Text = null;
			this.txtTown.Top = 0.0625F;
			this.txtTown.Width = 1.9375F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.21875F;
			this.Field1.Left = 1.5F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.Field1.Text = "Exempt Audit";
			this.Field1.Top = 0.0625F;
			this.Field1.Width = 4.4375F;
			// 
			// txtDate
			// 
			this.txtDate.CanGrow = false;
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 6.125F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtDate.Text = "Field7";
			this.txtDate.Top = 0.0625F;
			this.txtDate.Width = 1.3125F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 6.125F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtPage.Text = "Field7";
			this.txtPage.Top = 0.25F;
			this.txtPage.Width = 1.3125F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0.0625F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.1875F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.1875F;
			this.Field2.Left = 0F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.Field2.Text = "Account";
			this.Field2.Top = 0.5625F;
			this.Field2.Width = 0.6875F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.1875F;
			this.Field3.Left = 0.75F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.Field3.Text = "Name";
			this.Field3.Top = 0.5625F;
			this.Field3.Width = 3.395833F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.1875F;
			this.Field4.Left = 6.375F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.Field4.Text = "Assessment";
			this.Field4.Top = 0.5625F;
			this.Field4.Width = 1.0625F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.1875F;
			this.Field5.Left = 5.333333F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.Field5.Text = "Exemption";
			this.Field5.Top = 0.5625F;
			this.Field5.Width = 1F;
			// 
			// Field6
			// 
			this.Field6.Height = 0.1875F;
			this.Field6.Left = 4.21875F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.Field6.Text = "Property";
			this.Field6.Top = 0.5625F;
			this.Field6.Width = 1.072917F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtGroup
			});
			this.GroupHeader1.DataField = "grpHeader";
			this.GroupHeader1.Height = 0.3125F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			// 
			// txtGroup
			// 
			this.txtGroup.Height = 0.1875F;
			this.txtGroup.Left = 1.9375F;
			this.txtGroup.Name = "txtGroup";
			this.txtGroup.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center";
			this.txtGroup.Text = null;
			this.txtGroup.Top = 0.125F;
			this.txtGroup.Width = 3.395833F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtSubAssess,
				this.txtSubExempt,
				this.txtSubProperty,
				this.Line1,
				this.Field15
			});
			this.GroupFooter1.Height = 0.3125F;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			// 
			// txtSubAssess
			// 
			this.txtSubAssess.Height = 0.1875F;
			this.txtSubAssess.Left = 6.375F;
			this.txtSubAssess.Name = "txtSubAssess";
			this.txtSubAssess.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.txtSubAssess.Text = "Assessment";
			this.txtSubAssess.Top = 0.0625F;
			this.txtSubAssess.Width = 1.0625F;
			// 
			// txtSubExempt
			// 
			this.txtSubExempt.Height = 0.1875F;
			this.txtSubExempt.Left = 5.333333F;
			this.txtSubExempt.Name = "txtSubExempt";
			this.txtSubExempt.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.txtSubExempt.Text = "Exemption";
			this.txtSubExempt.Top = 0.0625F;
			this.txtSubExempt.Width = 1F;
			// 
			// txtSubProperty
			// 
			this.txtSubProperty.Height = 0.1875F;
			this.txtSubProperty.Left = 4.21875F;
			this.txtSubProperty.Name = "txtSubProperty";
			this.txtSubProperty.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.txtSubProperty.Text = "Property";
			this.txtSubProperty.Top = 0.0625F;
			this.txtSubProperty.Width = 1.072917F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 4.052083F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.03125F;
			this.Line1.Width = 3.375F;
			this.Line1.X1 = 4.052083F;
			this.Line1.X2 = 7.427083F;
			this.Line1.Y1 = 0.03125F;
			this.Line1.Y2 = 0.03125F;
			// 
			// Field15
			// 
			this.Field15.Height = 0.1875F;
			this.Field15.Left = 2.875F;
			this.Field15.Name = "Field15";
			this.Field15.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.Field15.Text = "Subtotal";
			this.Field15.Top = 0.0625F;
			this.Field15.Width = 1.072917F;
			// 
			// rptExemptAudit
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.458333F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.DataInitialize += new System.EventHandler(this.rptExemptAudit_DataInitialize);
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtProperty)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotAssessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotProperty)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubAssess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubProperty)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtProperty;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotAssessment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotProperty;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field19;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTown;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroup;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubAssess;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubProperty;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field15;
	}
}
