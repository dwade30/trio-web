﻿namespace TWPP0000
{
	/// <summary>
	/// Summary description for srptLeasedInput.
	/// </summary>
	partial class srptLeasedInput
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptLeasedInput));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtLine = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRB = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTY = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMOs = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLorP = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFCTR = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLeasedFrom = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemptYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			((System.ComponentModel.ISupportInitialize)(this.txtLine)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTY)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMOs)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLorP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFCTR)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLeasedFrom)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtLine,
				this.txtRB,
				this.txtCD,
				this.txtQTY,
				this.txtDescription,
				this.txtCost,
				this.txtLDate,
				this.txtMOs,
				this.txtLorP,
				this.txtRent,
				this.txtGD,
				this.txtFCTR,
				this.txtLeasedFrom,
				this.txtExemptYear
			});
			this.Detail.Height = 0.25F;
			this.Detail.Name = "Detail";
			// 
			// txtLine
			// 
			this.txtLine.CanGrow = false;
			this.txtLine.Height = 0.1875F;
			this.txtLine.Left = 0F;
			this.txtLine.MultiLine = false;
			this.txtLine.Name = "txtLine";
			this.txtLine.Style = "font-family: \'Tahoma\'; text-align: left";
			this.txtLine.Text = "Field1";
			this.txtLine.Top = 0.03125F;
			this.txtLine.Width = 0.4375F;
			// 
			// txtRB
			// 
			this.txtRB.CanGrow = false;
			this.txtRB.Height = 0.1875F;
			this.txtRB.Left = 0.5F;
			this.txtRB.MultiLine = false;
			this.txtRB.Name = "txtRB";
			this.txtRB.Style = "font-family: \'Tahoma\'; text-align: center";
			this.txtRB.Text = "Field2";
			this.txtRB.Top = 0.03125F;
			this.txtRB.Width = 0.25F;
			// 
			// txtCD
			// 
			this.txtCD.CanGrow = false;
			this.txtCD.Height = 0.1875F;
			this.txtCD.Left = 1.4375F;
			this.txtCD.MultiLine = false;
			this.txtCD.Name = "txtCD";
			this.txtCD.Style = "font-family: \'Tahoma\'";
			this.txtCD.Text = "Field3";
			this.txtCD.Top = 0.03125F;
			this.txtCD.Width = 0.3125F;
			// 
			// txtQTY
			// 
			this.txtQTY.CanGrow = false;
			this.txtQTY.Height = 0.1875F;
			this.txtQTY.Left = 1.8125F;
			this.txtQTY.MultiLine = false;
			this.txtQTY.Name = "txtQTY";
			this.txtQTY.Style = "font-family: \'Tahoma\'";
			this.txtQTY.Text = "Field4";
			this.txtQTY.Top = 0.03125F;
			this.txtQTY.Width = 0.4375F;
			// 
			// txtDescription
			// 
			this.txtDescription.CanShrink = true;
			this.txtDescription.Height = 0.1875F;
			this.txtDescription.Left = 2.3125F;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Style = "font-family: \'Tahoma\'";
			this.txtDescription.Text = "Field5";
			this.txtDescription.Top = 0.03125F;
			this.txtDescription.Width = 2.5625F;
			// 
			// txtCost
			// 
			this.txtCost.CanGrow = false;
			this.txtCost.Height = 0.1875F;
			this.txtCost.Left = 4.9375F;
			this.txtCost.MultiLine = false;
			this.txtCost.Name = "txtCost";
			this.txtCost.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtCost.Text = "Field6";
			this.txtCost.Top = 0.03125F;
			this.txtCost.Width = 0.875F;
			// 
			// txtLDate
			// 
			this.txtLDate.CanGrow = false;
			this.txtLDate.Height = 0.1875F;
			this.txtLDate.Left = 5.875F;
			this.txtLDate.MultiLine = false;
			this.txtLDate.Name = "txtLDate";
			this.txtLDate.Style = "font-family: \'Tahoma\'";
			this.txtLDate.Text = "Field7";
			this.txtLDate.Top = 0.03125F;
			this.txtLDate.Width = 0.6875F;
			// 
			// txtMOs
			// 
			this.txtMOs.CanGrow = false;
			this.txtMOs.Height = 0.1875F;
			this.txtMOs.Left = 6.625F;
			this.txtMOs.MultiLine = false;
			this.txtMOs.Name = "txtMOs";
			this.txtMOs.Style = "font-family: \'Tahoma\'";
			this.txtMOs.Text = "Field8";
			this.txtMOs.Top = 0.03125F;
			this.txtMOs.Width = 0.375F;
			// 
			// txtLorP
			// 
			this.txtLorP.CanGrow = false;
			this.txtLorP.Height = 0.1875F;
			this.txtLorP.Left = 7.125F;
			this.txtLorP.MultiLine = false;
			this.txtLorP.Name = "txtLorP";
			this.txtLorP.Style = "font-family: \'Tahoma\'";
			this.txtLorP.Text = "Field9";
			this.txtLorP.Top = 0.03125F;
			this.txtLorP.Width = 0.25F;
			// 
			// txtRent
			// 
			this.txtRent.CanGrow = false;
			this.txtRent.Height = 0.1875F;
			this.txtRent.Left = 7.5F;
			this.txtRent.MultiLine = false;
			this.txtRent.Name = "txtRent";
			this.txtRent.Style = "font-family: \'Tahoma\'";
			this.txtRent.Text = "Field10";
			this.txtRent.Top = 0.03125F;
			this.txtRent.Width = 0.75F;
			// 
			// txtGD
			// 
			this.txtGD.CanGrow = false;
			this.txtGD.Height = 0.1875F;
			this.txtGD.Left = 8.3125F;
			this.txtGD.MultiLine = false;
			this.txtGD.Name = "txtGD";
			this.txtGD.Style = "font-family: \'Tahoma\'";
			this.txtGD.Text = null;
			this.txtGD.Top = 0.03125F;
			this.txtGD.Width = 0.3125F;
			// 
			// txtFCTR
			// 
			this.txtFCTR.CanGrow = false;
			this.txtFCTR.Height = 0.1875F;
			this.txtFCTR.Left = 8.75F;
			this.txtFCTR.MultiLine = false;
			this.txtFCTR.Name = "txtFCTR";
			this.txtFCTR.Style = "font-family: \'Tahoma\'";
			this.txtFCTR.Text = "Field12";
			this.txtFCTR.Top = 0.03125F;
			this.txtFCTR.Width = 0.375F;
			// 
			// txtLeasedFrom
			// 
			this.txtLeasedFrom.CanGrow = false;
			this.txtLeasedFrom.Height = 0.1875F;
			this.txtLeasedFrom.Left = 9.1875F;
			this.txtLeasedFrom.MultiLine = false;
			this.txtLeasedFrom.Name = "txtLeasedFrom";
			this.txtLeasedFrom.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtLeasedFrom.Text = "Field12";
			this.txtLeasedFrom.Top = 0.03125F;
			this.txtLeasedFrom.Width = 0.625F;
			// 
			// txtExemptYear
			// 
			this.txtExemptYear.Height = 0.1875F;
			this.txtExemptYear.Left = 0.8333333F;
			this.txtExemptYear.Name = "txtExemptYear";
			this.txtExemptYear.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtExemptYear.Text = null;
			this.txtExemptYear.Top = 0.03125F;
			this.txtExemptYear.Width = 0.5520833F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.SubReport1
			});
			this.ReportFooter.Name = "ReportFooter";
			// 
			// SubReport1
			// 
			this.SubReport1.CloseBorder = false;
			this.SubReport1.Height = 0.0625F;
			this.SubReport1.Left = 0F;
			this.SubReport1.Name = "SubReport1";
			this.SubReport1.Report = null;
			this.SubReport1.Top = 0.15625F;
			this.SubReport1.Width = 9.8125F;
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Label8,
				this.Label9,
				this.Label10,
				this.Label11,
				this.Label12,
				this.Label13,
				this.Label14,
				this.Label15
			});
			this.GroupHeader1.Height = 0.75F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// Label1
			// 
			this.Label1.Height = 0.19F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 3.15625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label1.Text = "-------------- Leased Description --------------";
			this.Label1.Top = 0F;
			this.Label1.Width = 3.5625F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.19F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label2.Text = "Line #";
			this.Label2.Top = 0.34375F;
			this.Label2.Width = 0.4375F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.19F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0.5625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.Label3.Text = "R";
			this.Label3.Top = 0.34375F;
			this.Label3.Width = 0.1875F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 1.5F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label4.Text = "CD";
			this.Label4.Top = 0.34375F;
			this.Label4.Width = 0.25F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 1.8125F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label5.Text = "QTY";
			this.Label5.Top = 0.34375F;
			this.Label5.Width = 0.4375F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 2.3125F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label6.Text = "Description";
			this.Label6.Top = 0.34375F;
			this.Label6.Width = 1.75F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 5.0625F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Label7.Text = "Cost";
			this.Label7.Top = 0.34375F;
			this.Label7.Width = 0.75F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.375F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 5.875F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label8.Text = "Lease Date";
			this.Label8.Top = 0.34375F;
			this.Label8.Width = 0.625F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 6.625F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label9.Text = "MOS";
			this.Label9.Top = 0.34375F;
			this.Label9.Width = 0.375F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.375F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 7.0625F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label10.Text = "1=L 2=P";
			this.Label10.Top = 0.34375F;
			this.Label10.Width = 0.375F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 7.5F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label11.Text = "MO Rent";
			this.Label11.Top = 0.34375F;
			this.Label11.Width = 0.75F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.375F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 8.3125F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label12.Text = "% GD";
			this.Label12.Top = 0.34375F;
			this.Label12.Width = 0.3125F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 8.6875F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label13.Text = "FCTR";
			this.Label13.Top = 0.34375F;
			this.Label13.Width = 0.4375F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.375F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 9.1875F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label14.Text = "Leased From";
			this.Label14.Top = 0.34375F;
			this.Label14.Width = 0.625F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.333F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 0.803F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label15.Text = "Exempt Year";
			this.Label15.Top = 0.281F;
			this.Label15.Width = 0.652F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// ActiveReport1
			// 
			this.MasterReport = false;
			this.Name = "ActiveReport1";
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 9.885417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
			((System.ComponentModel.ISupportInitialize)(this.txtLine)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTY)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMOs)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLorP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFCTR)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLeasedFrom)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRB;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTY;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMOs;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLorP;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFCTR;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLeasedFrom;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemptYear;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
