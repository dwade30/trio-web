﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptBETEApplication.
	/// </summary>
	public partial class rptBETEApplication : BaseSectionReport
	{
		public rptBETEApplication()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "BETE Application";
		}

		public static rptBETEApplication InstancePtr
		{
			get
			{
				return (rptBETEApplication)Sys.GetInstance(typeof(rptBETEApplication));
			}
		}

		protected rptBETEApplication _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsReport?.Dispose();
				rsBusCode?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBETEApplication	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsReport = new clsDRWrapper();
		clsDRWrapper rsBusCode = new clsDRWrapper();
		int intBusinessSrc;
		int intOwnerSrc;
		bool boolCurrentOnly;
		int intPage;
		int intTaxYear;
		// vbPorter upgrade warning: intYear As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intEffYear As short	OnWriteFCConvert.ToInt32(
		public void Init(int intYear, int intEffYear, int intOwner, int intBusiness, ref bool boolCurrentBETEOnly, string strOrderBy, string strWhere, bool boolPending = false)
		{
			string strSQL = "";
			string strMasterJoin;
			strMasterJoin = modPPGN.GetMasterJoinForJoin();
			intPage = 1;
			if (intYear == 0)
				intYear = DateTime.Today.Year;
			if (intEffYear == 0)
				intEffYear = DateTime.Today.Year;
			intTaxYear = intYear;
			txtEffectiveTaxYear.Text = FCConvert.ToString(intEffYear);
			intBusinessSrc = intBusiness;
			intOwnerSrc = intOwner;
			boolCurrentOnly = boolCurrentBETEOnly;
			lblApplication.Text = "Annual Application for Tax year April 1, " + FCConvert.ToString(intYear);
			rsReport.OpenRecordset("select betemessage from customize", modPPGN.strPPDatabase);
			lblMessage.Text = rsReport.Get_Fields_String("BETEMessage");
			// strWhere = Replace(strWhere, "ppmaster.account", "account", , , vbTextCompare)
			// strOrderBy = Replace(strOrderBy, "ppmaster.account", "account", , , vbTextCompare)
			if (boolPending)
			{
				// strSQL = " select * from ppmaster inner join (select account from ppitemized where (RB = 'P')  group by account)  tbl1 on (tbl1.account = ppmaster.account)  where not deleted = 1 " & strWhere & " order by " & strOrderBy
				strSQL = " select * from " + strMasterJoin + " inner join (select account from ppitemized where (RB = 'P')  group by account)  tbl1 on (tbl1.account = mj.account)  where not deleted = 1 " + strWhere + " order by " + strOrderBy;
			}
			else if (boolCurrentOnly)
			{
				// strSQL = " select * from ppmaster inner join (select account from ppitemized where exemptyear = " & intYear & " group by account)  tbl1 on (tbl1.account = ppmaster.account)  where not deleted = 1 " & strWhere & " order by " & strOrderBy
				strSQL = " select * from " + strMasterJoin + " inner join (select account from ppitemized where exemptyear = " + FCConvert.ToString(intYear) + " group by account)  tbl1 on (tbl1.account = mj.account)  where not deleted = 1 " + strWhere + " order by " + strOrderBy;
			}
			else
			{
				// strSQL = " select * from ppmaster inner join (select account from ppitemized where (RB = 'P' or exemptyear >= 2008)  group by account) tbl1 on (tbl1.account = ppmaster.account)  where not deleted = 1 " & strWhere & " order by " & strOrderBy
				strSQL = " select * from " + strMasterJoin + " inner join (select account from ppitemized where (RB = 'P' or exemptyear >= 2008)  group by account) tbl1 on (tbl1.account = mj.account)  where not deleted = 1 " + strWhere + " order by " + strOrderBy;
			}
			rsReport.OpenRecordset(strSQL, modPPGN.strPPDatabase);
			if (rsReport.EndOfFile())
			{
				MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			frmReportViewer.InstancePtr.Init(rptObj: this, boolAllowEmail: true, strAttachmentName: "BETEApplication");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsReport.EndOfFile();
			if (!rsReport.EndOfFile())
			{
				// GroupHeader1.GroupValue = rsReport.Fields("ppmaster.account")
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtSignedDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			rsBusCode.OpenRecordset("select * from businesscodes order by businesscode", modPPGN.strPPDatabase);
			// TODO Get_Fields: Field [ppmaster.account] not found!! (maybe it is an alias?)
			this.Fields["grpHeader"].Value = rsReport.Get_Fields("ppmaster.account");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsReport.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				this.Fields["grpHeader"].Value = rsReport.Get_Fields_String("account");
				// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
				if (Conversion.Val(rsReport.Get_Fields("streetnumber")) > 0)
				{
					// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
					txtLocation.Text = FCConvert.ToString(Conversion.Val(rsReport.Get_Fields("streetnumber"))) + " " + rsReport.Get_Fields_String("street");
				}
				else
				{
					txtLocation.Text = rsReport.Get_Fields_String("street");
				}
				if (rsBusCode.FindFirstRecord("businesscode", Conversion.Val(rsReport.Get_Fields_Int32("businesscode"))))
				{
					txtBusinessType.Text = rsBusCode.Get_Fields_String("businesstype");
				}
				switch (intOwnerSrc)
				{
					case 0:
						{
							txtOwner.Text = rsReport.Get_Fields_String("name");
							break;
						}
					case 1:
						{
							txtOwner.Text = rsReport.Get_Fields_String("open1");
							break;
						}
					case 2:
						{
							txtOwner.Text = rsReport.Get_Fields_String("open2");
							break;
						}
					case 3:
						{
							txtOwner.Text = rsReport.Get_Fields_String("address1");
							break;
						}
					case 4:
						{
							txtOwner.Text = "";
							break;
						}
					default:
						{
							txtOwner.Text = "";
							break;
						}
				}
				//end switch
				switch (intBusinessSrc)
				{
					case 0:
						{
							txtBusiness.Text = rsReport.Get_Fields_String("name");
							break;
						}
					case 1:
						{
							txtBusiness.Text = rsReport.Get_Fields_String("open1");
							break;
						}
					case 2:
						{
							txtBusiness.Text = rsReport.Get_Fields_String("open2");
							break;
						}
					case 3:
						{
							txtBusiness.Text = rsReport.Get_Fields_String("address1");
							break;
						}
					default:
						{
							txtBusiness.Text = "";
							break;
						}
				}
				//end switch
				txtAddr1.Text = Strings.Trim(FCConvert.ToString(rsReport.Get_Fields_String("name")));
				txtAddr2.Text = Strings.Trim(FCConvert.ToString(rsReport.Get_Fields_String("address1")));
				txtAddr3.Text = Strings.Trim(FCConvert.ToString(rsReport.Get_Fields_String("address2")));
				txtAddr4.Text = Strings.Trim(rsReport.Get_Fields_String("city") + " " + rsReport.Get_Fields_String("state") + "  " + rsReport.Get_Fields_String("zip") + " " + rsReport.Get_Fields_String("zip4"));
				// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
				if (Conversion.Val(rsReport.Get_Fields("streetnumber")) > 0)
				{
					// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
					txtLocation.Text = FCConvert.ToString(Conversion.Val(rsReport.Get_Fields("streetnumber"))) + " " + rsReport.Get_Fields_String("street");
				}
				else
				{
					txtLocation.Text = rsReport.Get_Fields_String("street");
				}
				if (rsBusCode.FindFirstRecord("businesscode", Conversion.Val(rsReport.Get_Fields_Int32("businesscode"))))
				{
					txtBusinessType.Text = rsBusCode.Get_Fields_String("businesstype");
				}
				if (txtAddr3.Text == "")
				{
					txtAddr3.Text = txtAddr4.Text;
					txtAddr4.Text = "";
				}
				if (txtAddr2.Text == "")
				{
					txtAddr2.Text = txtAddr3.Text;
					txtAddr3.Text = txtAddr4.Text;
					txtAddr4.Text = "";
				}
				if (txtAddr1.Text == "")
				{
					txtAddr1.Text = txtAddr2.Text;
					txtAddr2.Text = txtAddr3.Text;
					txtAddr3.Text = txtAddr4.Text;
					txtAddr4.Text = "";
				}
				SubReport1.Report = srptBETEApplication.InstancePtr;
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				SubReport1.Report.UserData = rsReport.Get_Fields("account") + "," + FCConvert.ToString(boolCurrentOnly) + "," + FCConvert.ToString(intTaxYear);
				rsReport.MoveNext();
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			if (txtAcct.Text != FCConvert.ToString(this.Fields["grpHeader"].Value))
			{
				intPage = 1;
			}
			txtAcct.Text = FCConvert.ToString(this.Fields["grpHeader"].Value);
			lblPage.Text = "Page " + FCConvert.ToString(intPage);
			intPage += 1;
		}

		

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("grpHeader");
		}
	}
}
