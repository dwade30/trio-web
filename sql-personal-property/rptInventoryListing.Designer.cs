namespace TWPP0000
{
    /// <summary>
    /// Summary description for rptInventoryListing.
    /// </summary>
    partial class rptInventoryListing
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>


        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(rptInventoryListing));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.lblMuniname = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAssessmentYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.Shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddr1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddr2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZip4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPhone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtOpen1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblOpen1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtOpen2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblOpen2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtBusinesscode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtRatioMessage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.SubReport2 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.lblCat1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTotCat1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotBETE1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblCat2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTotCat2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotBETE2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblCat3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTotCat3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotBETE3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblCat4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTotCat4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotBETE4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblCat5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTotCat5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotBETE5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblCat6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTotCat6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotBETE6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblCat7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTotCat7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotBETE7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblCat8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTotCat8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotBETE8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblCat9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTotCat9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotBETE9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTotValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtComment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotBETE = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddr1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddr2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOpen1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOpen1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOpen2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOpen2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBusinesscode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRatioMessage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblMuniname,
            this.lblTime,
            this.lblDate,
            this.lblPage,
            this.lblTitle,
            this.lblAssessmentYear});
            this.pageHeader.Height = 0.542F;
            this.pageHeader.Name = "pageHeader";
            // 
            // lblMuniname
            // 
            this.lblMuniname.Height = 0.1666667F;
            this.lblMuniname.HyperLink = null;
            this.lblMuniname.Left = 0.03666663F;
            this.lblMuniname.Name = "lblMuniname";
            this.lblMuniname.Style = "";
            this.lblMuniname.Text = null;
            this.lblMuniname.Top = 0.06266665F;
            this.lblMuniname.Width = 2.833333F;
            // 
            // lblTime
            // 
            this.lblTime.Height = 0.1666667F;
            this.lblTime.HyperLink = null;
            this.lblTime.Left = 0.03666663F;
            this.lblTime.Name = "lblTime";
            this.lblTime.Style = "";
            this.lblTime.Text = null;
            this.lblTime.Top = 0.2293333F;
            this.lblTime.Width = 2.25F;
            // 
            // lblDate
            // 
            this.lblDate.Height = 0.1666667F;
            this.lblDate.HyperLink = null;
            this.lblDate.Left = 5.786667F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "text-align: right";
            this.lblDate.Text = null;
            this.lblDate.Top = 0.06266665F;
            this.lblDate.Width = 1.666667F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1666667F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 5.786667F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "text-align: right";
            this.lblPage.Text = null;
            this.lblPage.Top = 0.2293333F;
            this.lblPage.Width = 1.666667F;
            // 
            // lblTitle
            // 
            this.lblTitle.Height = 0.25F;
            this.lblTitle.HyperLink = null;
            this.lblTitle.Left = 2.27625F;
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Style = "font-size: 12pt; font-weight: bold; text-align: center";
            this.lblTitle.Text = "Inventory Listing";
            this.lblTitle.Top = 0.06266665F;
            this.lblTitle.Width = 3F;
            // 
            // lblAssessmentYear
            // 
            this.lblAssessmentYear.Height = 0.1666667F;
            this.lblAssessmentYear.HyperLink = null;
            this.lblAssessmentYear.Left = 2.27625F;
            this.lblAssessmentYear.Name = "lblAssessmentYear";
            this.lblAssessmentYear.Style = "text-align: center";
            this.lblAssessmentYear.Text = null;
            this.lblAssessmentYear.Top = 0.3126667F;
            this.lblAssessmentYear.Width = 2.979167F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Shape1,
            this.txtAccount,
            this.txtName,
            this.txtAddr1,
            this.txtAddr2,
            this.txtCity,
            this.txtState,
            this.txtZip,
            this.txtZip4,
            this.Label2,
            this.Label3,
            this.txtPhone,
            this.txtLocation,
            this.Label4,
            this.txtOpen1,
            this.lblOpen1,
            this.txtOpen2,
            this.lblOpen2,
            this.txtBusinesscode,
            this.Label11,
            this.Label14,
            this.Line1,
            this.txtRatioMessage,
            this.SubReport1,
            this.SubReport2,
            this.lblCat1,
            this.txtTotCat1,
            this.txtTotBETE1,
            this.lblCat2,
            this.txtTotCat2,
            this.txtTotBETE2,
            this.lblCat3,
            this.txtTotCat3,
            this.txtTotBETE3,
            this.lblCat4,
            this.txtTotCat4,
            this.txtTotBETE4,
            this.lblCat5,
            this.txtTotCat5,
            this.txtTotBETE5,
            this.lblCat6,
            this.txtTotCat6,
            this.txtTotBETE6,
            this.lblCat7,
            this.txtTotCat7,
            this.txtTotBETE7,
            this.lblCat8,
            this.txtTotCat8,
            this.txtTotBETE8,
            this.lblCat9,
            this.txtTotCat9,
            this.txtTotBETE9,
            this.Label15,
            this.Label16,
            this.txtTotValue,
            this.txtComment,
            this.txtTotBETE,
            this.Line2});
            this.detail.Height = 4.965F;
            this.detail.Name = "detail";
            // 
            // Shape1
            // 
            this.Shape1.Height = 4.28125F;
            this.Shape1.Left = 0.03145822F;
            this.Shape1.Name = "Shape1";
            this.Shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape1.Top = 0.02937508F;
            this.Shape1.Width = 7.34375F;
            // 
            // txtAccount
            // 
            this.txtAccount.Height = 0.1875F;
            this.txtAccount.Left = 1.531458F;
            this.txtAccount.Name = "txtAccount";
            this.txtAccount.Style = "text-align: left";
            this.txtAccount.Text = null;
            this.txtAccount.Top = 0.1231251F;
            this.txtAccount.Width = 0.8125F;
            // 
            // txtName
            // 
            this.txtName.Height = 0.1875F;
            this.txtName.Left = 1.531458F;
            this.txtName.Name = "txtName";
            this.txtName.Style = "text-align: left";
            this.txtName.Text = null;
            this.txtName.Top = 0.4668751F;
            this.txtName.Width = 4.5F;
            // 
            // txtAddr1
            // 
            this.txtAddr1.Height = 0.1875F;
            this.txtAddr1.Left = 1.531458F;
            this.txtAddr1.Name = "txtAddr1";
            this.txtAddr1.Style = "text-align: left";
            this.txtAddr1.Text = null;
            this.txtAddr1.Top = 0.6543751F;
            this.txtAddr1.Width = 4.5F;
            // 
            // txtAddr2
            // 
            this.txtAddr2.Height = 0.1875F;
            this.txtAddr2.Left = 1.531458F;
            this.txtAddr2.Name = "txtAddr2";
            this.txtAddr2.Style = "text-align: left";
            this.txtAddr2.Text = null;
            this.txtAddr2.Top = 0.8418751F;
            this.txtAddr2.Width = 4.5F;
            // 
            // txtCity
            // 
            this.txtCity.Height = 0.1875F;
            this.txtCity.Left = 1.531458F;
            this.txtCity.Name = "txtCity";
            this.txtCity.Style = "text-align: left";
            this.txtCity.Text = null;
            this.txtCity.Top = 1.029375F;
            this.txtCity.Width = 1.5F;
            // 
            // txtState
            // 
            this.txtState.Height = 0.1875F;
            this.txtState.Left = 3.093958F;
            this.txtState.Name = "txtState";
            this.txtState.Style = "text-align: left";
            this.txtState.Text = null;
            this.txtState.Top = 1.029375F;
            this.txtState.Width = 0.3125F;
            // 
            // txtZip
            // 
            this.txtZip.Height = 0.1875F;
            this.txtZip.Left = 3.468958F;
            this.txtZip.Name = "txtZip";
            this.txtZip.Style = "text-align: left";
            this.txtZip.Text = null;
            this.txtZip.Top = 1.029375F;
            this.txtZip.Width = 1.125F;
            // 
            // txtZip4
            // 
            this.txtZip4.Height = 0.1875F;
            this.txtZip4.Left = 4.687708F;
            this.txtZip4.Name = "txtZip4";
            this.txtZip4.Style = "text-align: left";
            this.txtZip4.Text = null;
            this.txtZip4.Top = 1.029375F;
            this.txtZip4.Width = 0.4375F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.1875F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 0.1356249F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "font-weight: bold; text-align: left";
            this.Label2.Text = "Account";
            this.Label2.Top = 0.1231251F;
            this.Label2.Width = 0.75F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.1875F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 0.1356249F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "font-weight: bold; text-align: left";
            this.Label3.Text = "Name & Address";
            this.Label3.Top = 0.4668751F;
            this.Label3.Width = 1.322917F;
            // 
            // txtPhone
            // 
            this.txtPhone.Height = 0.1875F;
            this.txtPhone.Left = 1.531458F;
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Text = null;
            this.txtPhone.Top = 1.331458F;
            this.txtPhone.Width = 1.5F;
            // 
            // txtLocation
            // 
            this.txtLocation.Height = 0.1875F;
            this.txtLocation.Left = 1.531458F;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.Style = "text-align: left";
            this.txtLocation.Text = null;
            this.txtLocation.Top = 1.518958F;
            this.txtLocation.Width = 4.5F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.1875F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 0.1356249F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-weight: bold; text-align: left";
            this.Label4.Text = "Location";
            this.Label4.Top = 1.518958F;
            this.Label4.Width = 0.9270833F;
            // 
            // txtOpen1
            // 
            this.txtOpen1.Height = 0.1875F;
            this.txtOpen1.Left = 1.531458F;
            this.txtOpen1.Name = "txtOpen1";
            this.txtOpen1.Style = "text-align: left";
            this.txtOpen1.Text = null;
            this.txtOpen1.Top = 1.706458F;
            this.txtOpen1.Width = 4.5F;
            // 
            // lblOpen1
            // 
            this.lblOpen1.Height = 0.1875F;
            this.lblOpen1.HyperLink = null;
            this.lblOpen1.Left = 0.1356249F;
            this.lblOpen1.Name = "lblOpen1";
            this.lblOpen1.Style = "font-weight: bold; text-align: left";
            this.lblOpen1.Text = "Open 1";
            this.lblOpen1.Top = 1.706458F;
            this.lblOpen1.Width = 1.385417F;
            // 
            // txtOpen2
            // 
            this.txtOpen2.Height = 0.1875F;
            this.txtOpen2.Left = 1.531458F;
            this.txtOpen2.Name = "txtOpen2";
            this.txtOpen2.Style = "text-align: left";
            this.txtOpen2.Text = null;
            this.txtOpen2.Top = 1.893958F;
            this.txtOpen2.Width = 4.5F;
            // 
            // lblOpen2
            // 
            this.lblOpen2.Height = 0.1875F;
            this.lblOpen2.HyperLink = null;
            this.lblOpen2.Left = 0.1356249F;
            this.lblOpen2.Name = "lblOpen2";
            this.lblOpen2.Style = "font-weight: bold; text-align: left";
            this.lblOpen2.Text = "Open 2";
            this.lblOpen2.Top = 1.893958F;
            this.lblOpen2.Width = 1.322917F;
            // 
            // txtBusinesscode
            // 
            this.txtBusinesscode.Height = 0.1875F;
            this.txtBusinesscode.Left = 4.343958F;
            this.txtBusinesscode.Name = "txtBusinesscode";
            this.txtBusinesscode.Style = "text-align: left";
            this.txtBusinesscode.Text = null;
            this.txtBusinesscode.Top = 0.1231251F;
            this.txtBusinesscode.Width = 1.6875F;
            // 
            // Label11
            // 
            this.Label11.Height = 0.1875F;
            this.Label11.HyperLink = null;
            this.Label11.Left = 2.948125F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "font-weight: bold; text-align: left";
            this.Label11.Text = "Business Code";
            this.Label11.Top = 0.1231251F;
            this.Label11.Width = 1.1875F;
            // 
            // Label14
            // 
            this.Label14.Height = 0.1875F;
            this.Label14.HyperLink = null;
            this.Label14.Left = 0.1356249F;
            this.Label14.Name = "Label14";
            this.Label14.Style = "font-weight: bold; text-align: left";
            this.Label14.Text = "Phone";
            this.Label14.Top = 1.331458F;
            this.Label14.Width = 1F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0.03145822F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 2.206458F;
            this.Line1.Width = 7.34375F;
            this.Line1.X1 = 0.03145822F;
            this.Line1.X2 = 7.375208F;
            this.Line1.Y1 = 2.206458F;
            this.Line1.Y2 = 2.206458F;
            // 
            // txtRatioMessage
            // 
            this.txtRatioMessage.Height = 0.1875F;
            this.txtRatioMessage.Left = 0.2606249F;
            this.txtRatioMessage.Name = "txtRatioMessage";
            this.txtRatioMessage.Style = "text-align: left";
            this.txtRatioMessage.Text = "Totals reflect a X% assessment ratio";
            this.txtRatioMessage.Top = 4.060625F;
            this.txtRatioMessage.Width = 2.572917F;
            // 
            // SubReport1
            // 
            this.SubReport1.CloseBorder = false;
            this.SubReport1.Height = 0.09375F;
            this.SubReport1.Left = 0F;
            this.SubReport1.Name = "SubReport1";
            this.SubReport1.Report = null;
            this.SubReport1.Top = 4.591875F;
            this.SubReport1.Width = 7.499F;
            // 
            // SubReport2
            // 
            this.SubReport2.CloseBorder = false;
            this.SubReport2.Height = 0.09375F;
            this.SubReport2.Left = 0F;
            this.SubReport2.Name = "SubReport2";
            this.SubReport2.Report = null;
            this.SubReport2.Top = 4.841875F;
            this.SubReport2.Width = 7.499F;
            // 
            // lblCat1
            // 
            this.lblCat1.Height = 0.1666667F;
            this.lblCat1.HyperLink = null;
            this.lblCat1.Left = 0.6772916F;
            this.lblCat1.Name = "lblCat1";
            this.lblCat1.Style = "font-weight: bold; text-align: right";
            this.lblCat1.Text = "Category 1";
            this.lblCat1.Top = 2.508542F;
            this.lblCat1.Width = 2.416667F;
            // 
            // txtTotCat1
            // 
            this.txtTotCat1.Height = 0.1666667F;
            this.txtTotCat1.Left = 3.177292F;
            this.txtTotCat1.Name = "txtTotCat1";
            this.txtTotCat1.Style = "text-align: right";
            this.txtTotCat1.Text = "0";
            this.txtTotCat1.Top = 2.508542F;
            this.txtTotCat1.Width = 1.104167F;
            // 
            // txtTotBETE1
            // 
            this.txtTotBETE1.Height = 0.1666667F;
            this.txtTotBETE1.Left = 4.489791F;
            this.txtTotBETE1.Name = "txtTotBETE1";
            this.txtTotBETE1.Style = "text-align: right";
            this.txtTotBETE1.Text = "0";
            this.txtTotBETE1.Top = 2.508542F;
            this.txtTotBETE1.Width = 1.104167F;
            // 
            // lblCat2
            // 
            this.lblCat2.Height = 0.1666667F;
            this.lblCat2.HyperLink = null;
            this.lblCat2.Left = 0.6772916F;
            this.lblCat2.Name = "lblCat2";
            this.lblCat2.Style = "font-weight: bold; text-align: right";
            this.lblCat2.Text = "Category 2";
            this.lblCat2.Top = 2.675208F;
            this.lblCat2.Width = 2.416667F;
            // 
            // txtTotCat2
            // 
            this.txtTotCat2.Height = 0.1666667F;
            this.txtTotCat2.Left = 3.177292F;
            this.txtTotCat2.Name = "txtTotCat2";
            this.txtTotCat2.Style = "text-align: right";
            this.txtTotCat2.Text = "0";
            this.txtTotCat2.Top = 2.675208F;
            this.txtTotCat2.Width = 1.104167F;
            // 
            // txtTotBETE2
            // 
            this.txtTotBETE2.Height = 0.1666667F;
            this.txtTotBETE2.Left = 4.489791F;
            this.txtTotBETE2.Name = "txtTotBETE2";
            this.txtTotBETE2.Style = "text-align: right";
            this.txtTotBETE2.Text = "0";
            this.txtTotBETE2.Top = 2.675208F;
            this.txtTotBETE2.Width = 1.104167F;
            // 
            // lblCat3
            // 
            this.lblCat3.Height = 0.1666667F;
            this.lblCat3.HyperLink = null;
            this.lblCat3.Left = 0.6772916F;
            this.lblCat3.Name = "lblCat3";
            this.lblCat3.Style = "font-weight: bold; text-align: right";
            this.lblCat3.Text = "Category 3";
            this.lblCat3.Top = 2.841875F;
            this.lblCat3.Width = 2.416667F;
            // 
            // txtTotCat3
            // 
            this.txtTotCat3.Height = 0.1666667F;
            this.txtTotCat3.Left = 3.177292F;
            this.txtTotCat3.Name = "txtTotCat3";
            this.txtTotCat3.Style = "text-align: right";
            this.txtTotCat3.Text = "0";
            this.txtTotCat3.Top = 2.841875F;
            this.txtTotCat3.Width = 1.104167F;
            // 
            // txtTotBETE3
            // 
            this.txtTotBETE3.Height = 0.1666667F;
            this.txtTotBETE3.Left = 4.489791F;
            this.txtTotBETE3.Name = "txtTotBETE3";
            this.txtTotBETE3.Style = "text-align: right";
            this.txtTotBETE3.Text = "0";
            this.txtTotBETE3.Top = 2.841875F;
            this.txtTotBETE3.Width = 1.104167F;
            // 
            // lblCat4
            // 
            this.lblCat4.Height = 0.1666667F;
            this.lblCat4.HyperLink = null;
            this.lblCat4.Left = 0.6772916F;
            this.lblCat4.Name = "lblCat4";
            this.lblCat4.Style = "font-weight: bold; text-align: right";
            this.lblCat4.Text = "Category 4";
            this.lblCat4.Top = 3.008542F;
            this.lblCat4.Width = 2.416667F;
            // 
            // txtTotCat4
            // 
            this.txtTotCat4.Height = 0.1666667F;
            this.txtTotCat4.Left = 3.177292F;
            this.txtTotCat4.Name = "txtTotCat4";
            this.txtTotCat4.Style = "text-align: right";
            this.txtTotCat4.Text = "0";
            this.txtTotCat4.Top = 3.008542F;
            this.txtTotCat4.Width = 1.104167F;
            // 
            // txtTotBETE4
            // 
            this.txtTotBETE4.Height = 0.1666667F;
            this.txtTotBETE4.Left = 4.489791F;
            this.txtTotBETE4.Name = "txtTotBETE4";
            this.txtTotBETE4.Style = "text-align: right";
            this.txtTotBETE4.Text = "0";
            this.txtTotBETE4.Top = 3.008542F;
            this.txtTotBETE4.Width = 1.104167F;
            // 
            // lblCat5
            // 
            this.lblCat5.Height = 0.1666667F;
            this.lblCat5.HyperLink = null;
            this.lblCat5.Left = 0.6772916F;
            this.lblCat5.Name = "lblCat5";
            this.lblCat5.Style = "font-weight: bold; text-align: right";
            this.lblCat5.Text = "Category 5";
            this.lblCat5.Top = 3.175208F;
            this.lblCat5.Width = 2.416667F;
            // 
            // txtTotCat5
            // 
            this.txtTotCat5.Height = 0.1666667F;
            this.txtTotCat5.Left = 3.177292F;
            this.txtTotCat5.Name = "txtTotCat5";
            this.txtTotCat5.Style = "text-align: right";
            this.txtTotCat5.Text = "0";
            this.txtTotCat5.Top = 3.175208F;
            this.txtTotCat5.Width = 1.104167F;
            // 
            // txtTotBETE5
            // 
            this.txtTotBETE5.Height = 0.1666667F;
            this.txtTotBETE5.Left = 4.489791F;
            this.txtTotBETE5.Name = "txtTotBETE5";
            this.txtTotBETE5.Style = "text-align: right";
            this.txtTotBETE5.Text = "0";
            this.txtTotBETE5.Top = 3.175208F;
            this.txtTotBETE5.Width = 1.104167F;
            // 
            // lblCat6
            // 
            this.lblCat6.Height = 0.1666667F;
            this.lblCat6.HyperLink = null;
            this.lblCat6.Left = 0.6772916F;
            this.lblCat6.Name = "lblCat6";
            this.lblCat6.Style = "font-weight: bold; text-align: right";
            this.lblCat6.Text = "Category 6";
            this.lblCat6.Top = 3.341875F;
            this.lblCat6.Width = 2.416667F;
            // 
            // txtTotCat6
            // 
            this.txtTotCat6.Height = 0.1666667F;
            this.txtTotCat6.Left = 3.177292F;
            this.txtTotCat6.Name = "txtTotCat6";
            this.txtTotCat6.Style = "text-align: right";
            this.txtTotCat6.Text = "0";
            this.txtTotCat6.Top = 3.341875F;
            this.txtTotCat6.Width = 1.104167F;
            // 
            // txtTotBETE6
            // 
            this.txtTotBETE6.Height = 0.1666667F;
            this.txtTotBETE6.Left = 4.489791F;
            this.txtTotBETE6.Name = "txtTotBETE6";
            this.txtTotBETE6.Style = "text-align: right";
            this.txtTotBETE6.Text = "0";
            this.txtTotBETE6.Top = 3.341875F;
            this.txtTotBETE6.Width = 1.104167F;
            // 
            // lblCat7
            // 
            this.lblCat7.Height = 0.1666667F;
            this.lblCat7.HyperLink = null;
            this.lblCat7.Left = 0.6772916F;
            this.lblCat7.Name = "lblCat7";
            this.lblCat7.Style = "font-weight: bold; text-align: right";
            this.lblCat7.Text = "Category 7";
            this.lblCat7.Top = 3.508542F;
            this.lblCat7.Width = 2.416667F;
            // 
            // txtTotCat7
            // 
            this.txtTotCat7.Height = 0.1666667F;
            this.txtTotCat7.Left = 3.177292F;
            this.txtTotCat7.Name = "txtTotCat7";
            this.txtTotCat7.Style = "text-align: right";
            this.txtTotCat7.Text = "0";
            this.txtTotCat7.Top = 3.508542F;
            this.txtTotCat7.Width = 1.104167F;
            // 
            // txtTotBETE7
            // 
            this.txtTotBETE7.Height = 0.1666667F;
            this.txtTotBETE7.Left = 4.489791F;
            this.txtTotBETE7.Name = "txtTotBETE7";
            this.txtTotBETE7.Style = "text-align: right";
            this.txtTotBETE7.Text = "0";
            this.txtTotBETE7.Top = 3.508542F;
            this.txtTotBETE7.Width = 1.104167F;
            // 
            // lblCat8
            // 
            this.lblCat8.Height = 0.1666667F;
            this.lblCat8.HyperLink = null;
            this.lblCat8.Left = 0.6772916F;
            this.lblCat8.Name = "lblCat8";
            this.lblCat8.Style = "font-weight: bold; text-align: right";
            this.lblCat8.Text = "Category 8";
            this.lblCat8.Top = 3.675208F;
            this.lblCat8.Width = 2.416667F;
            // 
            // txtTotCat8
            // 
            this.txtTotCat8.Height = 0.1666667F;
            this.txtTotCat8.Left = 3.177292F;
            this.txtTotCat8.Name = "txtTotCat8";
            this.txtTotCat8.Style = "text-align: right";
            this.txtTotCat8.Text = "0";
            this.txtTotCat8.Top = 3.675208F;
            this.txtTotCat8.Width = 1.104167F;
            // 
            // txtTotBETE8
            // 
            this.txtTotBETE8.Height = 0.1666667F;
            this.txtTotBETE8.Left = 4.489791F;
            this.txtTotBETE8.Name = "txtTotBETE8";
            this.txtTotBETE8.Style = "text-align: right";
            this.txtTotBETE8.Text = "0";
            this.txtTotBETE8.Top = 3.675208F;
            this.txtTotBETE8.Width = 1.104167F;
            // 
            // lblCat9
            // 
            this.lblCat9.Height = 0.1666667F;
            this.lblCat9.HyperLink = null;
            this.lblCat9.Left = 0.6772916F;
            this.lblCat9.Name = "lblCat9";
            this.lblCat9.Style = "font-weight: bold; text-align: right";
            this.lblCat9.Text = "Category 9";
            this.lblCat9.Top = 3.841875F;
            this.lblCat9.Width = 2.416667F;
            // 
            // txtTotCat9
            // 
            this.txtTotCat9.Height = 0.1666667F;
            this.txtTotCat9.Left = 3.177292F;
            this.txtTotCat9.Name = "txtTotCat9";
            this.txtTotCat9.Style = "text-align: right";
            this.txtTotCat9.Text = "0";
            this.txtTotCat9.Top = 3.841875F;
            this.txtTotCat9.Width = 1.104167F;
            // 
            // txtTotBETE9
            // 
            this.txtTotBETE9.Height = 0.1666667F;
            this.txtTotBETE9.Left = 4.489791F;
            this.txtTotBETE9.Name = "txtTotBETE9";
            this.txtTotBETE9.Style = "text-align: right";
            this.txtTotBETE9.Text = "0";
            this.txtTotBETE9.Top = 3.841875F;
            this.txtTotBETE9.Width = 1.104167F;
            // 
            // Label15
            // 
            this.Label15.Height = 0.1875F;
            this.Label15.HyperLink = null;
            this.Label15.Left = 3.281458F;
            this.Label15.Name = "Label15";
            this.Label15.Style = "font-weight: bold; text-align: right";
            this.Label15.Text = "Values";
            this.Label15.Top = 2.310625F;
            this.Label15.Width = 1F;
            // 
            // Label16
            // 
            this.Label16.Height = 0.1875F;
            this.Label16.HyperLink = null;
            this.Label16.Left = 4.593958F;
            this.Label16.Name = "Label16";
            this.Label16.Style = "font-weight: bold; text-align: right";
            this.Label16.Text = "BETE Exempt";
            this.Label16.Top = 2.310625F;
            this.Label16.Width = 1F;
            // 
            // txtTotValue
            // 
            this.txtTotValue.Height = 0.1666667F;
            this.txtTotValue.Left = 3.177292F;
            this.txtTotValue.Name = "txtTotValue";
            this.txtTotValue.Style = "text-align: right";
            this.txtTotValue.Text = "0";
            this.txtTotValue.Top = 4.060625F;
            this.txtTotValue.Width = 1.104167F;
            // 
            // txtComment
            // 
            this.txtComment.Height = 0.1979167F;
            this.txtComment.Left = 0.01062489F;
            this.txtComment.Name = "txtComment";
            this.txtComment.Text = null;
            this.txtComment.Top = 4.341875F;
            this.txtComment.Width = 7.229167F;
            // 
            // txtTotBETE
            // 
            this.txtTotBETE.Height = 0.1666667F;
            this.txtTotBETE.Left = 4.489791F;
            this.txtTotBETE.Name = "txtTotBETE";
            this.txtTotBETE.Style = "text-align: right";
            this.txtTotBETE.Text = "0";
            this.txtTotBETE.Top = 4.060625F;
            this.txtTotBETE.Width = 1.104167F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 3.156458F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 4.018958F;
            this.Line2.Width = 2.447917F;
            this.Line2.X1 = 5.604375F;
            this.Line2.X2 = 3.156458F;
            this.Line2.Y1 = 4.018958F;
            this.Line2.Y2 = 4.018958F;
            // 
            // pageFooter
            // 
            this.pageFooter.Name = "pageFooter";
            // 
            // rptInventoryListing
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.49F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
            "l; font-size: 10pt; color: Black", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
            "lic", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddr1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddr2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOpen1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOpen1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOpen2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOpen2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBusinesscode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRatioMessage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniname;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAssessmentYear;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddr1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddr2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCity;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip4;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhone;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOpen1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblOpen1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOpen2;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblOpen2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBusinesscode;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRatioMessage;
        private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport1;
        private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport2;
        public GrapeCity.ActiveReports.SectionReportModel.Label lblCat1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotCat1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotBETE1;
        public GrapeCity.ActiveReports.SectionReportModel.Label lblCat2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotCat2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotBETE2;
        public GrapeCity.ActiveReports.SectionReportModel.Label lblCat3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotCat3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotBETE3;
        public GrapeCity.ActiveReports.SectionReportModel.Label lblCat4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotCat4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotBETE4;
        public GrapeCity.ActiveReports.SectionReportModel.Label lblCat5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotCat5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotBETE5;
        public GrapeCity.ActiveReports.SectionReportModel.Label lblCat6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotCat6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotBETE6;
        public GrapeCity.ActiveReports.SectionReportModel.Label lblCat7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotCat7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotBETE7;
        public GrapeCity.ActiveReports.SectionReportModel.Label lblCat8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotCat8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotBETE8;
        public GrapeCity.ActiveReports.SectionReportModel.Label lblCat9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotCat9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotBETE9;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotValue;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtComment;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotBETE;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
    }
}
