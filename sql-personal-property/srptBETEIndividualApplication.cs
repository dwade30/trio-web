﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for srptBETEIndividualApplication.
	/// </summary>
	public partial class srptBETEIndividualApplication : FCSectionReport
	{
		public srptBETEIndividualApplication()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "BETE";
		}

		public static srptBETEIndividualApplication InstancePtr
		{
			get
			{
				return (srptBETEIndividualApplication)Sys.GetInstance(typeof(srptBETEIndividualApplication));
			}
		}

		protected srptBETEIndividualApplication _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptBETEIndividualApplication	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private int intNumRecs;
		private bool boolLastPage;
		private cBETEApplication theBETEApp;
		private int intPage;

		public object SetBETEApp(cBETEApplication theApp)
		{
			object SetBETEApp = null;
			theBETEApp = theApp;
			return SetBETEApp;
		}
		/// <summary>
		/// Private Sub ClearDetails()
		/// </summary>
		/// <summary>
		/// txtDescription.Text = ""
		/// </summary>
		/// <summary>
		/// txtServiceDate.Text = ""
		/// </summary>
		/// <summary>
		/// txtAge.Text = ""
		/// </summary>
		/// <summary>
		/// txtNew.Text = ""
		/// </summary>
		/// <summary>
		/// txtEstimate.Text = ""
		/// </summary>
		/// <summary>
		/// txtEquipmentLocation.Text = ""
		/// </summary>
		/// <summary>
		/// txtTIF.Text = ""
		/// </summary>
		/// <summary>
		/// txtBETEEligible.Text = ""
		/// </summary>
		/// <summary>
		/// txtAssessment.Text = ""
		/// </summary>
		/// <summary>
		/// End Sub
		/// </summary>
		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = !theBETEApp.Details.IsCurrent();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtBusiness.Text = theBETEApp.Business;
			txtAddress.Text = theBETEApp.BusinessAddress;
			txtOwner.Text = theBETEApp.Owner;
			txtTypeOfBusiness.Text = theBETEApp.TypeOfBusiness;
			txtAccount.Text = FCConvert.ToString(theBETEApp.Account);
			theBETEApp.Details.MoveFirst();
			intNumRecs = 20;
			boolLastPage = false;
			intPage = 1;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (theBETEApp.Details.IsCurrent())
			{
				ClearFields();
				int intCurrentLine;
				cBETEApplicationDetail bDetail;
				lblPage.Text = "Page " + FCConvert.ToString(intPage);
				intPage += 1;
				for (intCurrentLine = 1; intCurrentLine <= intNumRecs; intCurrentLine++)
				{
					if (!theBETEApp.Details.IsCurrent())
					{
						boolLastPage = true;
						break;
					}
					bDetail = theBETEApp.Details.GetCurrentDetail();
					SetFieldsText("txtDescription", intCurrentLine, bDetail.Description);
					if (bDetail.Age > 0)
					{
						SetFieldsText("txtAge", intCurrentLine, FCConvert.ToString(bDetail.Age));
					}
					else
					{
						SetFieldsText("txtAge", intCurrentLine, "");
					}
					if (bDetail.BETEEligible)
					{
						SetFieldsText("txtBETEEligible", intCurrentLine, "Y");
					}
					if (bDetail.AssessedValue > 0)
					{
						SetFieldsText("txtAssessment", intCurrentLine, Strings.Format(bDetail.AssessedValue, "#,###,###,##0"));
					}
					if (bDetail.EstimatedValue > 0)
					{
						SetFieldsText("txtEstimate", intCurrentLine, Strings.Format(bDetail.EstimatedValue, "#,###,###,##0"));
					}
					if (bDetail.ValueNew > 0)
					{
						//FC:FINAL:MSH - i.issue #1264: incorrect field name
						//SetFieldsText("txtnew", intCurrentLine, Strings.Format(bDetail.ValueNew, "#,###,###,##0"));
						SetFieldsText("txtNew", intCurrentLine, Strings.Format(bDetail.ValueNew, "#,###,###,##0"));
					}
					//FC:FINAL:MSH - i.issue #1264: incorrect field name
					//SetFieldsText("txtservicedate", intCurrentLine, bDetail.PlacedInService);
					SetFieldsText("txtServiceDate", intCurrentLine, bDetail.PlacedInService);
					if (bDetail.TIF)
					{
						SetFieldsText("txtTIF", intCurrentLine, "Y");
					}
					//FC:FINAL:MSH - i.issue #1264: incorrect field name
					//SetFieldsText("txtequipmentlocation", intCurrentLine, bDetail.EquipmentLocation);
					SetFieldsText("txtEquipmentLocation", intCurrentLine, bDetail.EquipmentLocation);
					theBETEApp.Details.MoveNext();
				}
				// intCurrentLine
			}
		}

		private void ClearFields()
		{
			int x;
			for (x = 1; x <= 20; x++)
			{
				//FC:FINAL:MSH - i.issue #1264: incorrect field name
				//SetFieldsText("txtdescription", x, "");
				SetFieldsText("txtDescription", x, "");
				SetFieldsText("txtAge", x, "");
				//FC:FINAL:MSH - i.issue #1264: incorrect field name
				//SetFieldsText("txtservicedate", x, "");
				SetFieldsText("txtServiceDate", x, "");
				SetFieldsText("txtNew", x, "");
				SetFieldsText("txtEstimate", x, "");
				SetFieldsText("txtEquipmentLocation", x, "");
				SetFieldsText("txtTIF", x, "");
				SetFieldsText("txtBETEEligible", x, "");
				SetFieldsText("txtAssessment", x, "");
			}
			// x
		}
		// vbPorter upgrade warning: strValue As string	OnWrite(string, short)
		private void SetFieldsText(string strFieldName, int intLine, string strValue)
		{
			(Detail.Controls[strFieldName + intLine] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strValue;
		}

		
	}
}
