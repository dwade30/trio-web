﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Core;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for srptItemizedOriginalCost.
	/// </summary>
	public partial class srptItemizedOriginalCost : FCSectionReport
	{
		public srptItemizedOriginalCost()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptItemizedOriginalCost InstancePtr
		{
			get
			{
				return (srptItemizedOriginalCost)Sys.GetInstance(typeof(srptItemizedOriginalCost));
			}
		}

		protected srptItemizedOriginalCost _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsReports?.Dispose();
				clsTemp?.Dispose();
                clsTemp = null;
                clsReports = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptItemizedOriginalCost	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool boolPrint;
		int lngAcct;
		clsDRWrapper clsReports = new clsDRWrapper();
		clsDRWrapper clsTemp = new clsDRWrapper();

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			rptLeasedandItemized.InstancePtr.txtAcct.Text = this.UserData.ToString();
			rptLeasedandItemized.InstancePtr.txtName.Text = clsTemp.Get_Fields_String("name");
			// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
			rptLeasedandItemized.InstancePtr.txtLocation.Text = clsTemp.Get_Fields("streetnumber") + " " + clsTemp.Get_Fields_String("street");
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lngAcct = FCConvert.ToInt32(this.UserData);
			clsReports.OpenRecordset("select * from ppitemized where ACCOUNT = " + FCConvert.ToString(lngAcct) + " order by line", "twpp0000.vb1");
			string strMasterJoinJoin;
			strMasterJoinJoin = modPPGN.GetMasterJoinForJoin(true);
			// Call clsTemp.OpenRecordset("select name,street,streetnumber from ppmaster where account = " & lngAcct, "twpp0000.vb1")
			clsTemp.OpenRecordset("select name,street,streetnumber from " + strMasterJoinJoin + " where account = " + FCConvert.ToString(lngAcct), "twpp0000.vb1");
			// If clsReports.EndOfFile Then
			// Unload Me
			// End If
			if (clsReports.EndOfFile())
			{
				Detail.Visible = false;
				GroupHeader1.Visible = false;
			}
			else
			{
				Detail.Visible = true;
				GroupHeader1.Visible = true;
			}
		}

		public void PrintItemized()
		{
			if (FCConvert.ToString(clsReports.Get_Fields_String("CD")) == "-")
			{
				// TODO Get_Fields: Check the table for the column [Line] and replace with corresponding Get_Field method
				txtLine.Text = Strings.Format(clsReports.Get_Fields("Line"), "0000");
				txtCD.Text = Strings.Trim(clsReports.Get_Fields_String("CD") + "");
				txtQTY.Text = string.Empty;
				txtDescription.Text = Strings.Trim(FCConvert.ToString(clsReports.Get_Fields_String("Description")));
				txtCost.Text = string.Empty;
				txtYear.Text = string.Empty;
				txtMO.Text = string.Empty;
				boolPrint = true;
			}
			// TODO Get_Fields: Check the table for the column [Cost] and replace with corresponding Get_Field method
			else if (Conversion.Val(clsReports.Get_Fields("Cost")) != 0)
			{
				if (Strings.Trim(clsReports.Get_Fields_String("Description") + "") != string.Empty)
				{
					// TODO Get_Fields: Check the table for the column [Line] and replace with corresponding Get_Field method
					txtLine.Text = Strings.Format(clsReports.Get_Fields("Line"), "0000");
					txtCD.Text = Strings.Trim(clsReports.Get_Fields_String("CD") + "");
					if (rptLeasedandItemized.InstancePtr.boolPQuantity)
					{
						// TODO Get_Fields: Check the table for the column [Quantity] and replace with corresponding Get_Field method
						txtQTY.Text = FCConvert.ToString(clsReports.Get_Fields("Quantity"));
					}
					else
					{
						txtQTY.Text = "__";
					}
					txtDescription.Text = Strings.Trim(FCConvert.ToString(clsReports.Get_Fields_String("Description")));
					// TODO Get_Fields: Check the table for the column [sro] and replace with corresponding Get_Field method
					if (Strings.UCase(FCConvert.ToString(clsReports.Get_Fields("sro"))) == "S")
					{
						txtMO.Text = "";
						txtYear.Text = "";
						txtCost.Text = "";
					}
						// TODO Get_Fields: Check the table for the column [sro] and replace with corresponding Get_Field method
						else if (Strings.UCase(FCConvert.ToString(clsReports.Get_Fields("sro"))) == "R")
					{
						txtMO.Text = "";
						txtCost.Text = "";
						txtYear.Text = "";
					}
					else
					{
						if (clsReports.Get_Fields_Int16("Month") > 0)
						{
							txtMO.Text = FCConvert.ToString(clsReports.Get_Fields_Int16("Month"));
						}
						else
						{
							txtMO.Text = "";
						}
						// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
						txtYear.Text = FCConvert.ToString(clsReports.Get_Fields("Year"));
						// TODO Get_Fields: Check the table for the column [Cost] and replace with corresponding Get_Field method
						txtCost.Text = Strings.Format(clsReports.Get_Fields("Cost"), "###,###,###");
					}
					boolPrint = true;
				}
			}
				// TODO Get_Fields: Check the table for the column [Cost] and replace with corresponding Get_Field method
				else if (Conversion.Val(clsReports.Get_Fields("Cost")) == 0)
			{
				if (Strings.Trim(clsReports.Get_Fields_String("Description") + "") != string.Empty)
				{
					// TODO Get_Fields: Check the table for the column [Line] and replace with corresponding Get_Field method
					txtLine.Text = Strings.Format(clsReports.Get_Fields("Line"), "0000");
					txtCD.Text = Strings.Trim(clsReports.Get_Fields_String("CD") + "");
					if (rptLeasedandItemized.InstancePtr.boolPQuantity)
					{
						// TODO Get_Fields: Check the table for the column [Quantity] and replace with corresponding Get_Field method
						txtQTY.Text = FCConvert.ToString(clsReports.Get_Fields("Quantity"));
					}
					else
					{
						txtQTY.Text = "__";
					}
					txtDescription.Text = Strings.Trim(clsReports.Get_Fields_String("Description") + "");
					txtMO.Text = string.Empty;
					txtYear.Text = string.Empty;
					txtCost.Text = string.Empty;
					boolPrint = true;
				}
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			TopOfSub:
			;
			eArgs.EOF = clsReports.EndOfFile();
			if (eArgs.EOF)
				return;
			boolPrint = false;
			PrintItemized();
			if (!boolPrint)
			{
				clsReports.MoveNext();
				goto TopOfSub;
			}
			else
			{
				clsReports.MoveNext();
			}
		}

		
	}
}
