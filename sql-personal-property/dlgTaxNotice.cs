﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for dlgTaxNotice.
	/// </summary>
	public partial class dlgTaxNotice : BaseForm
	{
		public dlgTaxNotice()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.chkInclude = new System.Collections.Generic.List<fecherFoundation.FCCheckBox>();
			this.chkInclude.AddControlArrayElement(chkInclude_3, 3);
			this.chkInclude.AddControlArrayElement(chkInclude_2, 2);
			this.chkInclude.AddControlArrayElement(chkInclude_1, 1);
			this.chkInclude.AddControlArrayElement(chkInclude_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static dlgTaxNotice InstancePtr
		{
			get
			{
				return (dlgTaxNotice)Sys.GetInstance(typeof(dlgTaxNotice));
			}
		}

		protected dlgTaxNotice _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int intWhatOrder;

		private void chkInclude_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			if (chkInclude[1].CheckState != CheckState.Checked)
			{
				Label1.Visible = false;
				Label2.Visible = false;
				txtYear.Visible = false;
				txtRate.Visible = false;
			}
			else
			{
				if (cmbFormat.SelectedIndex == 1)
				{
					Label1.Visible = true;
					Label2.Visible = true;
					txtYear.Visible = true;
					txtRate.Visible = true;
				}
			}
		}

		private void chkInclude_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = chkInclude.GetIndex((FCCheckBox)sender);
			chkInclude_CheckedChanged(index, sender, e);
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void cmdCancel_Click()
		{
			//cmdCancel_Click(cmdCancel, new System.EventArgs());
		}

		private void cmdOkay_Click(object sender, System.EventArgs e)
		{
			string strReturn = "";
			// vbPorter upgrade warning: minaccount As int	OnWrite(string)
			int minaccount = 0;
			int maxaccount;
			string strOrder = "";
			string strResponse = "";
			int intARorS = 0;
			// All, Range or Specific
			if (cmbOrder.SelectedIndex == 0)
			{
				intWhatOrder = 1;
			}
			else if (cmbOrder.SelectedIndex == 1)
			{
				intWhatOrder = 2;
			}
			else
			{
				intWhatOrder = 3;
			}
			switch (intWhatOrder)
			{
				case 1:
					{
						strOrder = "Account";
						break;
					}
				case 2:
					{
						strOrder = "Name";
						break;
					}
				case 3:
					{
						strOrder = "Location";
						break;
					}
			}
			//end switch
			if (cmbaccounts.SelectedIndex == 0)
			{
				intARorS = 1;
			}
			else if (cmbaccounts.SelectedIndex == 1)
			{
				intARorS = 2;
			}
			else
			{
				intARorS = 3;
			}
			if (cmbFormat.SelectedIndex == 1 && (chkInclude[1].CheckState == CheckState.Checked))
			{
				if ((txtRate.Text == string.Empty) || (txtYear.Text == string.Empty))
				{
					MessageBox.Show("You must fill in both the tax rate and the tax year to continue", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (cmbaccounts.SelectedIndex == 1)
			{
				minagain:
				;
				strResponse = Interaction.InputBox("Please enter the minimum " + strOrder + " value", "Enter Minimum");
				if (strResponse == string.Empty)
					return;
				if (intWhatOrder == 1)
				{
					if (Conversion.Val(strResponse) < 1)
					{
						MessageBox.Show("This is an invalid minimum account", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
						goto minagain;
					}
					minaccount = FCConvert.ToInt32(Strings.Trim(strResponse));
					modPPGN.Statics.gintMinAccountrange = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Trim(strResponse) + "")));
				}
				else
				{
					modPPGN.Statics.gstrMinAccountRange = Strings.Trim(strResponse);
				}
				maxagain:
				;
				strResponse = Interaction.InputBox("Please enter the maximum " + strOrder + " value", "Enter Maximum");
				if (strResponse == string.Empty)
					return;
				if (intWhatOrder == 1)
				{
					if ((Conversion.Val(strResponse) < 1) || (Conversion.Val(strResponse) < minaccount))
					{
						MessageBox.Show("This is an invalid range", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
						goto minagain;
					}
					modPPGN.Statics.gintMaxAccountrange = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Trim(strResponse) + "")));
				}
				else
				{
					modPPGN.Statics.gstrMaxAccountRange = Strings.Trim(strResponse + "zzz");
				}
			}
			else if (cmbaccounts.SelectedIndex == 2)
			{
				modPPGN.Statics.CancelledIt = false;
				frmGetAccountsToPrint.InstancePtr.Show(FormShowEnum.Modal);
				if (modPPGN.Statics.CancelledIt)
					return;
			}
			//! Load dlgTaxMessage;
			dlgTaxMessage.InstancePtr.Init(cmbFormat.SelectedIndex == 0, chkInclude[1].CheckState == CheckState.Checked, chkInclude[2].CheckState == CheckState.Checked, chkInclude[3].CheckState == CheckState.Checked, chkInclude[0].CheckState == CheckState.Checked, txtYear.Text, txtRate.Text, intARorS, strOrder);
			Close();
		}

		private void dlgTaxNotice_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void dlgTaxNotice_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//dlgTaxNotice properties;
			//dlgTaxNotice.ScaleWidth	= 5910;
			//dlgTaxNotice.ScaleHeight	= 4350;
			//dlgTaxNotice.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void mnuContinue_Click(object sender, System.EventArgs e)
		{
			//FC:FINAL:MSH - i.issue #1338: restore handle for 'Continue' clicking
			cmdOkay_Click(sender, e);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdCancel_Click();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void optFormat_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			if (Index == 0)
			{
				chkInclude[1].CheckState = Wisej.Web.CheckState.Unchecked;
				chkInclude[1].Enabled = false;
				chkInclude[2].Enabled = false;
				chkInclude[2].CheckState = Wisej.Web.CheckState.Unchecked;
				Label1.Visible = false;
				Label2.Visible = false;
				txtYear.Visible = false;
				txtRate.Visible = false;
			}
			else
			{
				chkInclude[1].Enabled = true;
				chkInclude[2].Enabled = true;
				chkInclude[1].CheckState = CheckState.Checked;
				chkInclude[2].CheckState = CheckState.Checked;
				if (chkInclude[1].CheckState == CheckState.Checked)
				{
					Label1.Visible = true;
					Label2.Visible = true;
					txtYear.Visible = true;
					txtRate.Visible = true;
				}
			}
		}

		private void optFormat_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = FCConvert.ToInt16(cmbFormat.SelectedIndex);
			optFormat_CheckedChanged(index, sender, e);
		}

		private void cmbFormat_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbFormat.SelectedIndex == 0)
			{
				optFormat_CheckedChanged(sender, e);
			}
			else if (cmbFormat.SelectedIndex == 1)
			{
				optFormat_CheckedChanged(sender, e);
			}
		}
	}
}
