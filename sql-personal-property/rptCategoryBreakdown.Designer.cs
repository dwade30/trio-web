﻿namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptCategoryBreakdown.
	/// </summary>
	partial class rptCategoryBreakdown
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptCategoryBreakdown));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniname = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtID1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDescription1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtID2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDescription2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSubTot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtGrandTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtID1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtID2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubTot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrandTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtID2,
				this.txtDescription2,
				this.txtValue
			});
			this.Detail.Height = 0.25F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtGrandTotal,
				this.Label2,
				this.Line1
			});
			this.ReportFooter.Height = 0.2708333F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblTitle,
				this.lblMuniname,
				this.lblTime,
				this.lblDate,
				this.lblPage,
				this.Label3
			});
			this.PageHeader.Height = 0.6770833F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtID1,
				this.txtDescription1
			});
			this.GroupHeader1.DataField = "fldgroupid";
			this.GroupHeader1.GroupKeepTogether = GrapeCity.ActiveReports.SectionReportModel.GroupKeepTogether.FirstDetail;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtSubTot,
				this.Label1
			});
			this.GroupFooter1.Height = 0.21875F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.25F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 2.333333F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.lblTitle.Text = "Category Breakdown";
			this.lblTitle.Top = 0F;
			this.lblTitle.Width = 3F;
			// 
			// lblMuniname
			// 
			this.lblMuniname.Height = 0.1666667F;
			this.lblMuniname.HyperLink = null;
			this.lblMuniname.Left = 0F;
			this.lblMuniname.Name = "lblMuniname";
			this.lblMuniname.Style = "";
			this.lblMuniname.Text = null;
			this.lblMuniname.Top = 0F;
			this.lblMuniname.Width = 2.833333F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1666667F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.1666667F;
			this.lblTime.Width = 2.25F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1666667F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 5.75F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "text-align: right";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1.666667F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1666667F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 5.75F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "text-align: right";
			this.lblPage.Text = null;
			this.lblPage.Top = 0.1666667F;
			this.lblPage.Width = 1.666667F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1666667F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.572917F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold; text-align: right";
			this.Label3.Text = "Value";
			this.Label3.Top = 0.5F;
			this.Label3.Width = 0.8541667F;
			// 
			// txtID1
			// 
			this.txtID1.DataField = "fldgroupid";
			this.txtID1.Height = 0.1666667F;
			this.txtID1.Left = 0.09375F;
			this.txtID1.Name = "txtID1";
			this.txtID1.Style = "text-align: right";
			this.txtID1.Text = null;
			this.txtID1.Top = 0.05208333F;
			this.txtID1.Width = 0.6770833F;
			// 
			// txtDescription1
			// 
			this.txtDescription1.DataField = "fldgroupdesc";
			this.txtDescription1.Height = 0.1666667F;
			this.txtDescription1.Left = 0.8333333F;
			this.txtDescription1.Name = "txtDescription1";
			this.txtDescription1.Style = "text-align: left";
			this.txtDescription1.Text = null;
			this.txtDescription1.Top = 0.05208333F;
			this.txtDescription1.Width = 4.15625F;
			// 
			// txtID2
			// 
			this.txtID2.Height = 0.1666667F;
			this.txtID2.Left = 0.7604167F;
			this.txtID2.Name = "txtID2";
			this.txtID2.Style = "text-align: right";
			this.txtID2.Text = null;
			this.txtID2.Top = 0.05208333F;
			this.txtID2.Width = 0.6770833F;
			// 
			// txtDescription2
			// 
			this.txtDescription2.Height = 0.1666667F;
			this.txtDescription2.Left = 1.5F;
			this.txtDescription2.Name = "txtDescription2";
			this.txtDescription2.Style = "text-align: left";
			this.txtDescription2.Text = null;
			this.txtDescription2.Top = 0.05208333F;
			this.txtDescription2.Width = 4.15625F;
			// 
			// txtValue
			// 
			this.txtValue.Height = 0.1666667F;
			this.txtValue.Left = 6F;
			this.txtValue.Name = "txtValue";
			this.txtValue.Style = "text-align: right";
			this.txtValue.Text = null;
			this.txtValue.Top = 0.05208333F;
			this.txtValue.Width = 1.427083F;
			// 
			// txtSubTot
			// 
			this.txtSubTot.Height = 0.1666667F;
			this.txtSubTot.Left = 6F;
			this.txtSubTot.Name = "txtSubTot";
			this.txtSubTot.Style = "text-align: right";
			this.txtSubTot.Text = null;
			this.txtSubTot.Top = 0.02083333F;
			this.txtSubTot.Width = 1.427083F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1666667F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 5.052083F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-weight: bold; text-align: right";
			this.Label1.Text = "Total";
			this.Label1.Top = 0.02083333F;
			this.Label1.Width = 0.8541667F;
			// 
			// txtGrandTotal
			// 
			this.txtGrandTotal.Height = 0.1666667F;
			this.txtGrandTotal.Left = 6F;
			this.txtGrandTotal.Name = "txtGrandTotal";
			this.txtGrandTotal.Style = "text-align: right";
			this.txtGrandTotal.Text = null;
			this.txtGrandTotal.Top = 0.1041667F;
			this.txtGrandTotal.Width = 1.427083F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1666667F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 5.052083F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold; text-align: right";
			this.Label2.Text = "Grand Total";
			this.Label2.Top = 0.1041667F;
			this.Label2.Width = 0.8541667F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 5.010417F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.05208333F;
			this.Line1.Width = 2.447917F;
			this.Line1.X1 = 5.010417F;
			this.Line1.X2 = 7.458333F;
			this.Line1.Y1 = 0.05208333F;
			this.Line1.Y2 = 0.05208333F;
			// 
			// rptCategoryBreakdown
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.479167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtID1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtID2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubTot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrandTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtID2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrandTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniname;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtID1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubTot;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
	}
}
