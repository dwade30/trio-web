using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using SharedApplication.Extensions;
using Wisej.Web;

namespace TWPP0000
{
    /// <summary>
    /// Summary description for srptLeasedInventory.
    /// </summary>
    public partial class srptLeasedInventory : FCSectionReport
    {
        int lngAcct;
        clsDRWrapper rsReport = new/*AsNew*/ clsDRWrapper();
        double dblTotValue;
        double dblTotCost;
        double dblTotBETE;
        double[] dblCat = new double[9 + 1];
        double[] dblBETE = new double[9 + 1];
       
        public srptLeasedInventory()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            this.FetchData += SrptLeasedInventory_FetchData;
            this.ReportStart += SrptLeasedInventory_ReportStart;
            this.detail.Format += Detail_Format;
            this.reportFooter1.Format += ReportFooter1_Format;
        }

        private void ReportFooter1_Format(object sender, EventArgs e)
        {
            txtTotalBETE.Text = Convert.ToInt32(dblTotBETE).FormatAsNumber();
            txtTotalCost.Text = Convert.ToInt32(dblTotCost).FormatAsNumber();
            txtTotalValue.Text = Convert.ToInt32(dblTotValue).FormatAsNumber();
            lblCat1.Text = ((rptInventoryListing)this.ParentReport).lblCat1.Text;
            lblCat2.Text = ((rptInventoryListing)this.ParentReport).lblCat2.Text;
            lblCat3.Text = ((rptInventoryListing)this.ParentReport).lblCat3.Text;
            lblCat4.Text = ((rptInventoryListing)this.ParentReport).lblCat4.Text;
            lblCat5.Text = ((rptInventoryListing)this.ParentReport).lblCat5.Text;
            lblCat6.Text = ((rptInventoryListing)this.ParentReport).lblCat6.Text;
            lblCat7.Text = ((rptInventoryListing)this.ParentReport).lblCat7.Text;
            lblCat8.Text = ((rptInventoryListing)this.ParentReport).lblCat8.Text;
            lblCat9.Text = ((rptInventoryListing)this.ParentReport).lblCat9.Text;
            txtTotCat1.Text = Convert.ToInt32(dblCat[1]).FormatAsNumber();
            txtTotCat2.Text = Convert.ToInt32(dblCat[2]).FormatAsNumber();
            txtTotCat3.Text = Convert.ToInt32(dblCat[3]).FormatAsNumber();
            txtTotCat4.Text = Convert.ToInt32(dblCat[4]).FormatAsNumber();
            txtTotCat5.Text = Convert.ToInt32(dblCat[5]).FormatAsNumber();
            txtTotCat6.Text = Convert.ToInt32(dblCat[6]).FormatAsNumber();
            txtTotCat7.Text = Convert.ToInt32(dblCat[7]).FormatAsNumber();
            txtTotCat8.Text = Convert.ToInt32(dblCat[8]).FormatAsNumber();
            txtTotCat9.Text = Convert.ToInt32(dblCat[9]).FormatAsNumber();
            txtTotBETE1.Text = Convert.ToInt32(dblBETE[1]).FormatAsNumber();
            txtTotBETE2.Text = Convert.ToInt32(dblBETE[2]).FormatAsNumber();
            txtTotBETE3.Text = Convert.ToInt32(dblBETE[3]).FormatAsNumber();
            txtTotBETE4.Text = Convert.ToInt32(dblBETE[4]).FormatAsNumber();
            txtTotBETE5.Text = Convert.ToInt32(dblBETE[5]).FormatAsNumber();
            txtTotBETE6.Text = Convert.ToInt32(dblBETE[6]).FormatAsNumber();
            txtTotBETE7.Text = Convert.ToInt32(dblBETE[7]).FormatAsNumber();
            txtTotBETE8.Text = Convert.ToInt32(dblBETE[8]).FormatAsNumber();
            txtTotBETE9.Text = Convert.ToInt32(dblBETE[9]).FormatAsNumber();
        }

        private void Detail_Format(object sender, EventArgs e)
        {
            if (!rsReport.EndOfFile())
            {
                try
                {
                    if (rsReport.Get_Fields_Int32("quantity") > 0)
                    {
                        txtQuantity.Text = rsReport.Get_Fields_Int32("quantity").ToString();
                    }
                    else
                    {
                        txtQuantity.Text = "";
                    }

                    if (rsReport.Get_Fields_String("cd").ToIntegerValue() > 0)
                    {
                        txtCategory.Text = rsReport.Get_Fields_String("cd");
                        dblCat[rsReport.Get_Fields_String("cd").ToIntegerValue()] =
                            dblCat[rsReport.Get_Fields_String("cd").ToIntegerValue()] + rsReport.Get_Fields_Int32("value");
                        dblBETE[rsReport.Get_Fields_String("cd").ToIntegerValue()] =
                            dblBETE[rsReport.Get_Fields_String("cd").ToIntegerValue()] + rsReport.Get_Fields_Int32("beteEXEMPT");

                    }
                    else
                    {
                        txtCategory.Text = "";
                    }

                    txtDescription.Text = rsReport.Get_Fields_String("description");
                    txtCost.Text = rsReport.Get_Fields_Int32("cost").FormatAsNumber();
                    dblTotCost += rsReport.Get_Fields_Int32("cost") * rsReport.Get_Fields_Int32("quantity");
                    txtGood.Text = rsReport.Get_Fields_Double("CalcGood").ToString();
                    txtValue.Text = rsReport.Get_Fields_Int32("value").FormatAsNumber();
                    dblTotValue += rsReport.Get_Fields_Int32("value");
                    txtBETE.Text = rsReport.Get_Fields_Int32("beteexempt").FormatAsNumber();
                    if (rsReport.Get_Fields_Int32("beteexempt") > 0)
                    {
                        txtBET.Text = "E";
                        dblTotBETE += rsReport.Get_Fields_Int32("beteexempt");
                    }
                    else if (rsReport.Get_Fields_String("rb") == "*")
                    {
                        txtBET.Text = "R";
                    }
                    else
                    {
                        txtBET.Text = "";
                    }

                    string strYear = "";
                    strYear = "";
                    string[] ary = null;
                    if (rsReport.Get_Fields_String("leasedate") != "")
                    {
                        ary = fecherFoundation.Strings.Split(rsReport.Get_Fields_String("leasedate"), @"/", -1,
                            fecherFoundation.CompareConstants.vbBinaryCompare);
                        if (fecherFoundation.Information.UBound(ary, 1) > 0)
                        {
                            if (fecherFoundation.Conversion.Val(ary[1]) > 50)
                            {
                                strYear = "19" + ary[1];
                            }
                            else
                            {
                                strYear = "20" + ary[1];
                            }
                        }
                    }

                    txtYear.Text = strYear;
                    rsReport.MoveNext();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void SrptLeasedInventory_ReportStart(object sender, EventArgs e)
        {
            lngAcct = (int)Math.Round(fecherFoundation.Conversion.Val(FCConvert.ToString(this.UserData)));
            rsReport.OpenRecordset("select * from ppleased where account = " + FCConvert.ToString(lngAcct) + " order by [line]", "PersonalProperty");
            if (rsReport.EndOfFile())
            {
                this.detail.Visible = false;
                this.groupHeader1.Visible = false;
                this.reportHeader1.Visible = false;
                this.reportFooter1.Visible = false;
            }
            else
            {
                this.detail.Visible = true;
                this.groupHeader1.Visible = true;
                this.reportHeader1.Visible = true;
                this.reportFooter1.Visible = true;
                dblTotValue = 0;
                dblTotCost = 0;
                dblTotBETE = 0;
                int x;
                for (x = 1; x <= 9; x++)
                {
                    dblCat[x] = 0;
                    dblBETE[x] = 0;
                } // x
            }
        }

        private void SrptLeasedInventory_FetchData(object sender, FetchEventArgs eArgs)
        {
            eArgs.EOF = rsReport.EndOfFile();
        }
    }
}
