﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmPPStreetTranCodes.
	/// </summary>
	public partial class frmPPStreetTranCodes : BaseForm
	{
		public frmPPStreetTranCodes()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPPStreetTranCodes InstancePtr
		{
			get
			{
				return (frmPPStreetTranCodes)Sys.GetInstance(typeof(frmPPStreetTranCodes));
			}
		}

		protected frmPPStreetTranCodes _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		clsDRWrapper rsTran = new clsDRWrapper();
		clsDRWrapper rsStreet = new clsDRWrapper();
		string strSQL;
		int[] aryDeletedStreets = null;
		int[] aryDeletedTrans = null;
		int lngStreetIndex;
		int lngTranIndex;
		int TranNum;
		int StreetNum;

		private void frmPPStreetTranCodes_Activated(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			if (modGNWork.FormExist(this))
				return;
			FillGrid();
			// menuPPCost.Hide
			vsTran.Visible = true;
			vsStreet.Visible = true;
		}

		private void frmPPStreetTranCodes_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				frmPPStreetTranCodes.InstancePtr.Close();
			}
		}

		private void frmPPStreetTranCodes_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPPStreetTranCodes properties;
			//frmPPStreetTranCodes.ScaleWidth	= 8880;
			//frmPPStreetTranCodes.ScaleHeight	= 7350;
			//frmPPStreetTranCodes.LinkTopic	= "Form1";
			//frmPPStreetTranCodes.LockControls	= true;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			lngTranIndex = 0;
			lngStreetIndex = 0;
			if (modGlobal.Statics.CustomizeStuff.boolRegionalTown)
			{
				vsTran.Editable = FCGrid.EditableSettings.flexEDNone;
				vsTran.Enabled = false;
				mnuAddTran.Visible = false;
				mnuDeleteTran.Visible = false;
			}
			FCUtils.EraseSafe(aryDeletedStreets);
			FCUtils.EraseSafe(aryDeletedTrans);
		}

		private void frmPPStreetTranCodes_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			modGlobal.SaveWindowSize(this);
			// menuPPCost.Show
		}

		private void FillGrid()
		{
			SetGridProperties();
			// 
			// Tran Codes
			strSQL = "SELECT * FROM TranCodes ORDER BY TranCode";
			rsTran.OpenRecordset(strSQL, modPPGN.strPPDatabase);
			while (!rsTran.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [TranCode] and replace with corresponding Get_Field method
				vsTran.AddItem(FCConvert.ToString(Conversion.Val(rsTran.Get_Fields("TranCode"))) + "\t" + rsTran.Get_Fields_String("TranType") + "");
				rsTran.MoveNext();
			}
			// 
			// Street Codes
			strSQL = "SELECT * FROM StreetCodes ORDER BY StreetCode";
			rsStreet.OpenRecordset(strSQL, modPPGN.strPPDatabase);
			while (!rsStreet.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [StreetCode] and replace with corresponding Get_Field method
				vsStreet.AddItem(FCConvert.ToString(Conversion.Val(rsStreet.Get_Fields("StreetCode"))) + "\t" + rsStreet.Get_Fields_String("StreetName") + "");
				rsStreet.MoveNext();
			}
		}

		private void ResizeGrid()
		{
			// vbPorter upgrade warning: GridWidth As Variant --> As int
			int GridWidth = 0;
			GridWidth = vsTran.WidthOriginal;
			vsTran.ColWidth(0, FCConvert.ToInt32(0.25 * GridWidth));
			GridWidth = vsStreet.WidthOriginal;
			vsStreet.ColWidth(0, FCConvert.ToInt32(0.25 * GridWidth));
		}

		private void SetGridProperties()
		{
			vsTran.TextMatrix(0, 0, "Tran Code");
			vsTran.TextMatrix(0, 1, "Tran Name");
			vsTran.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, 1, true);
			vsTran.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vsTran.Cols = 2;
			vsTran.Rows = 1;
			vsTran.ExtendLastCol = true;
			// 
			vsStreet.TextMatrix(0, 0, "Street Code");
			vsStreet.TextMatrix(0, 1, "Street Name");
			vsStreet.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, 1, true);
			vsStreet.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vsStreet.Cols = 2;
			vsStreet.Rows = 1;
			vsStreet.ExtendLastCol = true;
			//FC:FINAL:CHN - issue #1325: Fix changing cell alignment after edit.
			vsStreet.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsStreet.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsTran.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsTran.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		}

		private bool SaveData()
		{
			bool SaveData = false;
			try
			{
				// On Error GoTo ErrorTag
				int ii;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				// 
				vsStreet.Row = 0;
				vsTran.Row = 0;
				////Application.DoEvents();
				SaveData = false;
				if (lngTranIndex > 0)
				{
					for (ii = 0; ii <= Information.UBound(aryDeletedTrans, 1) - 1; ii++)
					{
						rsTran.Execute("delete from trancodes where trancode = " + FCConvert.ToString(aryDeletedTrans[ii]), modPPGN.strPPDatabase);
					}
					// ii
				}
				lngTranIndex = 0;
				FCUtils.EraseSafe(aryDeletedTrans);
				if (lngStreetIndex > 0)
				{
					for (ii = 0; ii <= Information.UBound(aryDeletedStreets, 1) - 1; ii++)
					{
						rsTran.Execute("delete from streetcodes where streetcode = " + FCConvert.ToString(aryDeletedStreets[ii]), modPPGN.strPPDatabase);
					}
					// ii
				}
				lngStreetIndex = 0;
				FCUtils.EraseSafe(aryDeletedStreets);
				rsTran.OpenRecordset("select * from trancodes order by trancode", modPPGN.strPPDatabase);
				// Tran Codes Save
				for (ii = 1; ii <= vsTran.Rows - 1; ii++)
				{
					if (FCConvert.ToBoolean(vsTran.RowData(ii)))
					{
						rsTran.FindFirstRecord("TranCode", Conversion.Val(vsTran.TextMatrix(ii, 0)));
						if (rsTran.NoMatch)
							rsTran.AddNew();
						else
							rsTran.Edit();
						rsTran.Set_Fields("TranCode", FCConvert.ToString(Conversion.Val(vsTran.TextMatrix(ii, 0))));
						rsTran.Set_Fields("TranType", vsTran.TextMatrix(ii, 1));
						rsTran.Update();
						vsTran.RowData(ii, false);
					}
				}
				// ii
				// 
				rsStreet.OpenRecordset("select * from streetcodes order by streetcode", modPPGN.strPPDatabase);
				// Street Codes Save
				for (ii = 1; ii <= vsStreet.Rows - 1; ii++)
				{
					if (FCConvert.ToBoolean(vsStreet.RowData(ii)))
					{
						rsStreet.FindFirstRecord("StreetCode", Conversion.Val(vsStreet.TextMatrix(ii, 0)));
						if (rsStreet.NoMatch)
							rsStreet.AddNew();
						else
							rsStreet.Edit();
						rsStreet.Set_Fields("StreetCode", FCConvert.ToString(Conversion.Val(vsStreet.TextMatrix(ii, 0))));
						rsStreet.Set_Fields("StreetName", vsStreet.TextMatrix(ii, 1));
						rsStreet.Update();
						vsStreet.RowData(ii, false);
					}
				}
				// ii
				// 
				SaveData = true;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Save Complete", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveData;
			}
			catch (Exception ex)
			{
				// ErrorTag:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In SaveData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveData;
		}

		private void mnuAddStreet_Click(object sender, System.EventArgs e)
		{
			vsStreet.Rows += 1;
		}

		private void mnuAddTran_Click(object sender, System.EventArgs e)
		{
			vsTran.Rows += 1;
		}

		private void mnuDeleteStreet_Click(object sender, System.EventArgs e)
		{
			if (vsStreet.Row > 0)
			{
				Array.Resize(ref aryDeletedStreets, lngStreetIndex + 1 + 1);
				aryDeletedStreets[lngStreetIndex] = FCConvert.ToInt32(Math.Round(Conversion.Val(vsStreet.TextMatrix(vsStreet.Row, 0))));
				lngStreetIndex += 1;
				vsStreet.RemoveItem(vsStreet.Row);
			}
		}

		public void mnuDeleteStreet_Click()
		{
			mnuDeleteStreet_Click(mnuDeleteStreet, new System.EventArgs());
		}

		private void mnuDeleteTran_Click(object sender, System.EventArgs e)
		{
			if (vsTran.Row > 0)
			{
				Array.Resize(ref aryDeletedTrans, lngTranIndex + 1 + 1);
				aryDeletedTrans[lngTranIndex] = FCConvert.ToInt32(Math.Round(Conversion.Val(vsTran.TextMatrix(vsTran.Row, 0))));
				lngTranIndex += 1;
				vsTran.RemoveItem(vsTran.Row);
			}
		}

		public void mnuDeleteTran_Click()
		{
			mnuDeleteTran_Click(mnuDeleteTran, new System.EventArgs());
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuPrintStreet_Click(object sender, System.EventArgs e)
		{
			rptBusinessCodes.InstancePtr.Init(2);
		}

		private void mnuPrintTran_Click(object sender, System.EventArgs e)
		{
			rptBusinessCodes.InstancePtr.Init(3);
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveData();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveData())
			{
				mnuExit_Click();
			}
		}

		private void vsStreet_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			vsStreet.EditMaxLength = 30;
		}

		private void vsStreet_ChangeEdit(object sender, System.EventArgs e)
		{
			vsStreet.RowData(vsStreet.Row, true);
		}

		private void vsStreet_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Delete:
					{
						if (vsStreet.Row > 0)
						{
							KeyCode = 0;
							mnuDeleteStreet_Click();
						}
						break;
					}
			}
			//end switch
		}

		private void vsTran_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			vsTran.EditMaxLength = 30;
		}

		private void vsTran_ChangeEdit(object sender, System.EventArgs e)
		{
			vsTran.RowData(vsTran.Row, true);
		}

		private void vsTran_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Delete:
					{
						if (vsTran.Row > 0)
						{
							KeyCode = 0;
							mnuDeleteTran_Click();
						}
						break;
					}
			}
			//end switch
		}
	}
}
