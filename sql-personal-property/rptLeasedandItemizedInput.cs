﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptLeasedandItemizedInput.
	/// </summary>
	partial class rptLeasedandItemizedInput : BaseSectionReport
	{
		public rptLeasedandItemizedInput()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Input Format";
		}

		public static rptLeasedandItemizedInput InstancePtr
		{
			get
			{
				return (rptLeasedandItemizedInput)Sys.GetInstance(typeof(rptLeasedandItemizedInput));
			}
		}

		protected rptLeasedandItemizedInput _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}

            if (disposing)
            {
				rsLoad?.Dispose();
                rsLoad = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptLeasedandItemizedInput	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intPage;
		int intRtype;
		public bool boolPQTY;
		private clsDRWrapper rsLoad = new clsDRWrapper();
		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			txtMuni.Text = modGlobalConstants.Statics.MuniName + " ME";
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs e)
		{
			e.EOF = rsLoad.EndOfFile();
			if (e.EOF)
				return;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
        }

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			intPage = 1;
            txtDate.Text = DateTime.Today.ToShortDateString();
        }

		public void Start(short intType, bool boolq,string strSQL)
        {
            rsLoad.OpenRecordset(strSQL, "PersonalProperty");
			string strFName = "";
			intRtype = intType;
			boolPQTY = boolq;
			switch (intType)
			{
				case 0:
					{
						SubReport1.Report = new srptLeasedInput();
						strFName = "LeasedInput";
						break;
					}
				case 1:
					{
						SubReport2.Report = new srptItemizedInput();
						strFName = "ItemizedInput";
						break;
					}
				case 2:
					{
						SubReport1.Report = new srptLeasedInput();
						SubReport2.Report = new srptItemizedInput();
						strFName = "LeasedItemizedInput";
						break;
					}
			}
			//end switch
			frmReportViewer.InstancePtr.Init(rptObj: this, boolAllowEmail: true, strAttachmentName: strFName);
			;
		}
		
		private void Detail_AfterPrint(object sender, EventArgs e)
		{
			intPage = 1;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (rsLoad.EndOfFile())
				return;
			if ((intRtype == 0) || (intRtype == 2))
			{
				// TODO Get_Fields: Field [masteraccount] not found!! (maybe it is an alias?)
				SubReport1.Report.UserData = Conversion.Val(rsLoad.Get_Fields("masteraccount"));
			}
			if (intRtype > 0)
			{
				// TODO Get_Fields: Field [masteraccount] not found!! (maybe it is an alias?)
				SubReport2.Report.UserData = Conversion.Val(rsLoad.Get_Fields("masteraccount"));
			}
			rsLoad.MoveNext();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + FCConvert.ToString(intPage);
			intPage += 1;
		}

	
	}
}
