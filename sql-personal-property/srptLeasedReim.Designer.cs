﻿namespace TWPP0000
{
	/// <summary>
	/// Summary description for srptLeasedReim.
	/// </summary>
	partial class srptLeasedReim
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptLeasedReim));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLine = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTY = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDesc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMO = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYr = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOriginal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssess = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYrs = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtLsOriginal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLsAssessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTY)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMO)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYr)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYrs)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLsOriginal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLsAssessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtLine,
				this.txtCD,
				this.txtQTY,
				this.txtDesc,
				this.txtMO,
				this.txtYr,
				this.txtOriginal,
				this.txtAssess,
				this.txtYrs,
				this.Line1
			});
			this.Detail.Height = 0.2083333F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label5,
				this.Line2
			});
			this.ReportHeader.Height = 0.3333333F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtLsOriginal,
				this.txtLsAssessment,
				this.Label14,
				this.Line8
			});
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.Visible = false;
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label26,
				this.Label27,
				this.Label28,
				this.Label29,
				this.Label30,
				this.Label31,
				this.Label32,
				this.Label33
			});
			this.GroupHeader1.Height = 0.375F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 2.5F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label5.Text = "LEASED  DESCRIPTION";
			this.Label5.Top = 0.0625F;
			this.Label5.Width = 2.4375F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0.0625F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.3125F;
			this.Line2.Width = 7.375F;
			this.Line2.X1 = 0.0625F;
			this.Line2.X2 = 7.4375F;
			this.Line2.Y1 = 0.3125F;
			this.Line2.Y2 = 0.3125F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.1875F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 6.625F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-family: \'Tahoma\'";
			this.Label26.Text = "Assessment";
			this.Label26.Top = 0.15625F;
			this.Label26.Width = 0.8125F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.34375F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 5.625F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-family: \'Tahoma\'; text-align: center";
			this.Label27.Text = "Original  Cost";
			this.Label27.Top = 0.03125F;
			this.Label27.Width = 0.6875F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.34375F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 4.75F;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-family: \'Tahoma\'; text-align: center";
			this.Label28.Text = "#YRS Claimed";
			this.Label28.Top = 0.03125F;
			this.Label28.Width = 0.5625F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.34375F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 3.9375F;
			this.Label29.Name = "Label29";
			this.Label29.Style = "font-family: \'Tahoma\'; text-align: center";
			this.Label29.Text = "In Service Month Year";
			this.Label29.Top = 0.03125F;
			this.Label29.Width = 0.75F;
			// 
			// Label30
			// 
			this.Label30.Height = 0.1875F;
			this.Label30.HyperLink = null;
			this.Label30.Left = 1.6875F;
			this.Label30.Name = "Label30";
			this.Label30.Style = "font-family: \'Tahoma\'";
			this.Label30.Text = "Description";
			this.Label30.Top = 0.1875F;
			this.Label30.Width = 1.625F;
			// 
			// Label31
			// 
			this.Label31.Height = 0.1875F;
			this.Label31.HyperLink = null;
			this.Label31.Left = 1.041667F;
			this.Label31.Name = "Label31";
			this.Label31.Style = "font-family: \'Tahoma\'; text-align: center";
			this.Label31.Text = "QTY";
			this.Label31.Top = 0.1875F;
			this.Label31.Width = 0.5625F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.1875F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 0.6875F;
			this.Label32.Name = "Label32";
			this.Label32.Style = "font-family: \'Tahoma\'; text-align: center";
			this.Label32.Text = "CD";
			this.Label32.Top = 0.1875F;
			this.Label32.Width = 0.25F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.1875F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 0.3125F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-family: \'Tahoma\'";
			this.Label33.Text = "Line";
			this.Label33.Top = 0.1875F;
			this.Label33.Width = 0.3125F;
			// 
			// txtLine
			// 
			this.txtLine.Height = 0.19F;
			this.txtLine.Left = 0.0625F;
			this.txtLine.Name = "txtLine";
			this.txtLine.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtLine.Text = "Field1";
			this.txtLine.Top = 0.03125F;
			this.txtLine.Width = 0.625F;
			// 
			// txtCD
			// 
			this.txtCD.Height = 0.1875F;
			this.txtCD.Left = 0.75F;
			this.txtCD.Name = "txtCD";
			this.txtCD.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtCD.Text = "Field2";
			this.txtCD.Top = 0.03125F;
			this.txtCD.Width = 0.25F;
			// 
			// txtQTY
			// 
			this.txtQTY.Height = 0.19F;
			this.txtQTY.Left = 1.041667F;
			this.txtQTY.Name = "txtQTY";
			this.txtQTY.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtQTY.Text = "Field3";
			this.txtQTY.Top = 0.03125F;
			this.txtQTY.Width = 0.5625F;
			// 
			// txtDesc
			// 
			this.txtDesc.CanShrink = true;
			this.txtDesc.Height = 0.19F;
			this.txtDesc.Left = 1.6875F;
			this.txtDesc.Name = "txtDesc";
			this.txtDesc.Style = "font-family: \'Tahoma\'";
			this.txtDesc.Text = "Field4";
			this.txtDesc.Top = 0.03125F;
			this.txtDesc.Width = 2.1875F;
			// 
			// txtMO
			// 
			this.txtMO.Height = 0.19F;
			this.txtMO.Left = 3.9375F;
			this.txtMO.Name = "txtMO";
			this.txtMO.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtMO.Text = "Field5";
			this.txtMO.Top = 0.03125F;
			this.txtMO.Width = 0.25F;
			// 
			// txtYr
			// 
			this.txtYr.Height = 0.19F;
			this.txtYr.Left = 4.3125F;
			this.txtYr.Name = "txtYr";
			this.txtYr.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtYr.Text = "Field6";
			this.txtYr.Top = 0.03125F;
			this.txtYr.Width = 0.375F;
			// 
			// txtOriginal
			// 
			this.txtOriginal.Height = 0.19F;
			this.txtOriginal.Left = 5.375F;
			this.txtOriginal.Name = "txtOriginal";
			this.txtOriginal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtOriginal.Text = "Field7";
			this.txtOriginal.Top = 0.03125F;
			this.txtOriginal.Width = 0.9375F;
			// 
			// txtAssess
			// 
			this.txtAssess.Height = 0.19F;
			this.txtAssess.Left = 6.375F;
			this.txtAssess.Name = "txtAssess";
			this.txtAssess.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtAssess.Text = "Field8";
			this.txtAssess.Top = 0.03125F;
			this.txtAssess.Width = 1.0625F;
			// 
			// txtYrs
			// 
			this.txtYrs.Height = 0.19F;
			this.txtYrs.Left = 4.8125F;
			this.txtYrs.Name = "txtYrs";
			this.txtYrs.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtYrs.Text = "Field9";
			this.txtYrs.Top = 0.03125F;
			this.txtYrs.Width = 0.5F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 4.8125F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.1875F;
			this.Line1.Width = 0.4375F;
			this.Line1.X1 = 4.8125F;
			this.Line1.X2 = 5.25F;
			this.Line1.Y1 = 0.1875F;
			this.Line1.Y2 = 0.1875F;
			// 
			// txtLsOriginal
			// 
			this.txtLsOriginal.Height = 0.1875F;
			this.txtLsOriginal.Left = 5.1875F;
			this.txtLsOriginal.Name = "txtLsOriginal";
			this.txtLsOriginal.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtLsOriginal.Text = "Field8";
			this.txtLsOriginal.Top = 0.0625F;
			this.txtLsOriginal.Width = 1.0625F;
			// 
			// txtLsAssessment
			// 
			this.txtLsAssessment.Height = 0.1875F;
			this.txtLsAssessment.Left = 6.3125F;
			this.txtLsAssessment.Name = "txtLsAssessment";
			this.txtLsAssessment.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtLsAssessment.Text = "Field9";
			this.txtLsAssessment.Top = 0.0625F;
			this.txtLsAssessment.Width = 1.125F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.21875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 0F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Label14.Text = "Leased Total  (With ratio applied and rounded by Category):";
			this.Label14.Top = 0.0625F;
			this.Label14.Width = 5.0625F;
			// 
			// Line8
			// 
			this.Line8.Height = 0F;
			this.Line8.Left = 5F;
			this.Line8.LineWeight = 1F;
			this.Line8.Name = "Line8";
			this.Line8.Top = 0.03125F;
			this.Line8.Width = 2.5F;
			this.Line8.X1 = 5F;
			this.Line8.X2 = 7.5F;
			this.Line8.Y1 = 0.03125F;
			this.Line8.Y2 = 0.03125F;
			// 
			// srptLeasedReim
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.Name = "ActiveReport1";
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTY)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMO)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYr)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYrs)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLsOriginal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLsAssessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTY;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDesc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMO;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYr;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOriginal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssess;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYrs;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLsOriginal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLsAssessment;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line8;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
