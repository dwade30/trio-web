//Fecher vbPorter - Version 1.0.0.93

using fecherFoundation;
using fecherFoundation.Extensions;

namespace TWPP0000
{
	public class cBathImportMaster
	{

		//=========================================================

		private string strDBA;
		private int lngIDNum;
		private string strName;
		private string strOpen1;
		private string strOpen2;
		private int lngStreetNo;
		private string strStreet;
		private string strAddress1;
		private string strAddress2;
		private string strCity;
		private string strZip;
		private string strState;
		private string strBusinessTypeCode;
		private double dblTotalAppraised;
		private double dblBETETotal;
		private double dblNonBETE;
		private string strComment;

		public string Comment
		{
			set
			{
				strComment = value;
			}

			get
			{
					string Comment = "";
				Comment = strComment;
				return Comment;
			}
		}



		public string City
		{
			set
			{
				strCity = value;
			}

			get
			{
					string City = "";
				City = strCity;
				return City;
			}
		}



		public string DoingBusinessAs
		{
			set
			{
				strDBA = value;
			}

			get
			{
					string DoingBusinessAs = "";
				DoingBusinessAs = strDBA;
				return DoingBusinessAs;
			}
		}



		public int IDNum
		{
			set
			{
				lngIDNum = value;
			}

			get
			{
					int IDNum = 0;
				IDNum = lngIDNum;
				return IDNum;
			}
		}



		public string OwnerName
		{
			set
			{
				strName = value;
			}

			get
			{
					string OwnerName = "";
				OwnerName = strName;
				return OwnerName;
			}
		}



		public string Open1
		{
			set
			{
				strOpen1 = value;
			}

			get
			{
					string Open1 = "";
				Open1 = strOpen1;
				return Open1;
			}
		}



		public string Open2
		{
			set
			{
				strOpen2 = value;
			}

			get
			{
					string Open2 = "";
				Open2 = strOpen2;
				return Open2;
			}
		}




		public int StreetNo
		{
			set
			{
				lngStreetNo = value;
			}

			get
			{
					int StreetNo = 0;
				StreetNo = lngStreetNo;
				return StreetNo;
			}
		}



		public string Street
		{
			set
			{
				strStreet = value;
			}

			get
			{
					string Street = "";
				Street = strStreet;
				return Street;
			}
		}



		public string Address1
		{
			set
			{
				strAddress1 = value;
			}

			get
			{
					string Address1 = "";
				Address1 = strAddress1;
				return Address1;
			}
		}



		public string Address2
		{
			set
			{
				strAddress2 = value;
			}

			get
			{
					string Address2 = "";
				Address2 = strAddress2;
				return Address2;
			}
		}



		public string Zip
		{
			set
			{
				strZip = value;
			}

			get
			{
					string Zip = "";
				Zip = strZip;
				return Zip;
			}
		}



		public string State
		{
			set
			{
				strState = value;
			}

			get
			{
					string State = "";
				State = strState;
				return State;
			}
		}



		public string BusinessTypeCode
		{
			set
			{
				strBusinessTypeCode = value;
			}

			get
			{
					string BusinessTypeCode = "";
				BusinessTypeCode = strBusinessTypeCode;
				return BusinessTypeCode;
			}
		}



		public double TotalAppraised
		{
			set
			{
				dblTotalAppraised = value;
			}

			get
			{
					double TotalAppraised = 0;
				TotalAppraised = dblTotalAppraised;
				return TotalAppraised;
			}
		}



		public double BETETotal
		{
			set
			{
				dblBETETotal = value;
			}

			get
			{
					double BETETotal = 0;
				BETETotal = dblBETETotal;
				return BETETotal;
			}
		}




		public double NonBETE
		{
			set
			{
				dblNonBETE = value;
			}

			get
			{
					double NonBETE = 0;
				NonBETE = dblNonBETE;
				return NonBETE;
			}
		}




	}
}
