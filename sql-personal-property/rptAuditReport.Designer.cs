﻿namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptAuditReport.
	/// </summary>
	partial class rptAuditReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptAuditReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.txtTown = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtProperty = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSummary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSubOverRideTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBilling = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBETE = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSummaryValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSummaryBETE = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroupCaption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSubPropertyTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSubExemptionTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSubAssessmentTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtSubBilling = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSubBETE = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPropertyTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemptionTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessmentTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCatTotals = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCatCaptions = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOverRideTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalBilling = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBETETotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtTown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtProperty)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSummary)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubOverRideTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBilling)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBETE)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSummaryValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSummaryBETE)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupCaption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubPropertyTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubExemptionTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubAssessmentTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubBilling)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubBETE)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPropertyTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptionTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessmentTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCatTotals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCatCaptions)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOverRideTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalBilling)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBETETotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAccount,
				this.txtName,
				this.txtProperty,
				this.txtExemption,
				this.txtAssessment,
				this.txtSummary,
				this.Field10,
				this.Field11,
				this.txtSubOverRideTotal,
				this.txtBilling,
				this.Field12,
				this.txtBETE,
				this.txtSummaryValue,
				this.txtSummaryBETE
			});
			this.Detail.Height = 0.84375F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTown,
				this.Field1,
				this.txtDate,
				this.txtPage,
				this.txtTime
			});
			this.ReportHeader.Height = 0.4791667F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field7,
				this.txtPropertyTotal,
				this.txtExemptionTotal,
				this.txtAssessmentTotal,
				this.Field8,
				this.txtCatTotals,
				this.txtCatCaptions,
				this.Line2,
				this.Field9,
				this.txtOverRideTotal,
				this.txtTotalBilling,
				this.Field16,
				this.txtBETETotal
			});
			this.ReportFooter.Height = 1.427083F;
			this.ReportFooter.KeepTogether = true;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field2,
				this.Field3,
				this.Field4,
				this.Field5,
				this.Field6,
				this.Line1,
				this.Field17
			});
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.KeepTogether = true;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.CanShrink = true;
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtGroupCaption,
				this.txtSubPropertyTotal,
				this.txtSubExemptionTotal,
				this.txtSubAssessmentTotal,
				this.Line3,
				this.txtSubBilling,
				this.Field14,
				this.txtSubBETE
			});
			this.GroupFooter1.Height = 0.6145833F;
			this.GroupFooter1.KeepTogether = true;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// txtTown
			// 
			this.txtTown.Height = 0.1875F;
			this.txtTown.Left = 0.0625F;
			this.txtTown.Name = "txtTown";
			this.txtTown.Style = "font-family: \'Tahoma\'";
			this.txtTown.Text = null;
			this.txtTown.Top = 0.0625F;
			this.txtTown.Width = 1.9375F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.21875F;
			this.Field1.Left = 1.5F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.Field1.Text = "Personal Property Audit Summary";
			this.Field1.Top = 0.0625F;
			this.Field1.Width = 4.4375F;
			// 
			// txtDate
			// 
			this.txtDate.CanGrow = false;
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 6.0625F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtDate.Text = "Field7";
			this.txtDate.Top = 0.0625F;
			this.txtDate.Width = 1.3125F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 6.0625F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtPage.Text = "Field7";
			this.txtPage.Top = 0.25F;
			this.txtPage.Width = 1.3125F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0.0625F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.1875F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.1875F;
			this.Field2.Left = 0.0625F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.Field2.Text = "Account";
			this.Field2.Top = 0F;
			this.Field2.Width = 0.6875F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.1875F;
			this.Field3.Left = 0.8125F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.Field3.Text = "Name";
			this.Field3.Top = 0F;
			this.Field3.Width = 2F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.1875F;
			this.Field4.Left = 4.75F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.Field4.Text = "Property";
			this.Field4.Top = 0F;
			this.Field4.Width = 0.75F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.1875F;
			this.Field5.Left = 5.5625F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.Field5.Text = "Exemption";
			this.Field5.Top = 0F;
			this.Field5.Width = 0.875F;
			// 
			// Field6
			// 
			this.Field6.Height = 0.1875F;
			this.Field6.Left = 6.5F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.Field6.Text = "Assessment";
			this.Field6.Top = 0F;
			this.Field6.Width = 0.875F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.0625F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.1875F;
			this.Line1.Width = 7.375F;
			this.Line1.X1 = 0.0625F;
			this.Line1.X2 = 7.4375F;
			this.Line1.Y1 = 0.1875F;
			this.Line1.Y2 = 0.1875F;
			// 
			// Field17
			// 
			this.Field17.Height = 0.1875F;
			this.Field17.Left = 3.34375F;
			this.Field17.Name = "Field17";
			this.Field17.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.Field17.Text = "BETE Exempt";
			this.Field17.Top = 0F;
			this.Field17.Width = 1.03125F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.1875F;
			this.txtAccount.Left = 0.0625F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.txtAccount.Text = "Account";
			this.txtAccount.Top = 0.0625F;
			this.txtAccount.Width = 0.6875F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1875F;
			this.txtName.Left = 0.8125F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.txtName.Text = "Name";
			this.txtName.Top = 0.0625F;
			this.txtName.Width = 3.59375F;
			// 
			// txtProperty
			// 
			this.txtProperty.Height = 0.1875F;
			this.txtProperty.Left = 4.625F;
			this.txtProperty.Name = "txtProperty";
			this.txtProperty.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right";
			this.txtProperty.Text = "Property";
			this.txtProperty.Top = 0.4375F;
			this.txtProperty.Width = 0.875F;
			// 
			// txtExemption
			// 
			this.txtExemption.Height = 0.1875F;
			this.txtExemption.Left = 5.5625F;
			this.txtExemption.Name = "txtExemption";
			this.txtExemption.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right";
			this.txtExemption.Text = "Exemption";
			this.txtExemption.Top = 0.4375F;
			this.txtExemption.Width = 0.875F;
			// 
			// txtAssessment
			// 
			this.txtAssessment.Height = 0.1875F;
			this.txtAssessment.Left = 6.5F;
			this.txtAssessment.Name = "txtAssessment";
			this.txtAssessment.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right";
			this.txtAssessment.Text = "Assessment";
			this.txtAssessment.Top = 0.4375F;
			this.txtAssessment.Width = 0.9166667F;
			// 
			// txtSummary
			// 
			this.txtSummary.Height = 0.1875F;
			this.txtSummary.Left = 1.125F;
			this.txtSummary.Name = "txtSummary";
			this.txtSummary.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.txtSummary.Text = "Summary";
			this.txtSummary.Top = 0.25F;
			this.txtSummary.Width = 2.3125F;
			// 
			// Field10
			// 
			this.Field10.Height = 0.1875F;
			this.Field10.Left = 3.25F;
			this.Field10.Name = "Field10";
			this.Field10.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.Field10.Text = "OverRide Total";
			this.Field10.Top = 0.625F;
			this.Field10.Width = 1.125F;
			// 
			// Field11
			// 
			this.Field11.Height = 0.1875F;
			this.Field11.Left = 2.5625F;
			this.Field11.Name = "Field11";
			this.Field11.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.Field11.Text = "Totals";
			this.Field11.Top = 0.4375F;
			this.Field11.Width = 0.6875F;
			// 
			// txtSubOverRideTotal
			// 
			this.txtSubOverRideTotal.Height = 0.1875F;
			this.txtSubOverRideTotal.Left = 4.625F;
			this.txtSubOverRideTotal.Name = "txtSubOverRideTotal";
			this.txtSubOverRideTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right";
			this.txtSubOverRideTotal.Text = "Property";
			this.txtSubOverRideTotal.Top = 0.625F;
			this.txtSubOverRideTotal.Width = 0.875F;
			// 
			// txtBilling
			// 
			this.txtBilling.Height = 0.1875F;
			this.txtBilling.Left = 6.5F;
			this.txtBilling.Name = "txtBilling";
			this.txtBilling.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right";
			this.txtBilling.Text = "Assessment";
			this.txtBilling.Top = 0.625F;
			this.txtBilling.Width = 0.9166667F;
			// 
			// Field12
			// 
			this.Field12.Height = 0.1875F;
			this.Field12.Left = 5.552083F;
			this.Field12.Name = "Field12";
			this.Field12.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.Field12.Text = "Billing Value";
			this.Field12.Top = 0.625F;
			this.Field12.Width = 0.8854167F;
			// 
			// txtBETE
			// 
			this.txtBETE.Height = 0.1875F;
			this.txtBETE.Left = 3.3125F;
			this.txtBETE.Name = "txtBETE";
			this.txtBETE.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right";
			this.txtBETE.Text = null;
			this.txtBETE.Top = 0.4375F;
			this.txtBETE.Width = 1.0625F;
			// 
			// txtSummaryValue
			// 
			this.txtSummaryValue.Height = 0.1875F;
			this.txtSummaryValue.Left = 4.625F;
			this.txtSummaryValue.Name = "txtSummaryValue";
			this.txtSummaryValue.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right";
			this.txtSummaryValue.Text = "Summary";
			this.txtSummaryValue.Top = 0.25F;
			this.txtSummaryValue.Width = 0.875F;
			// 
			// txtSummaryBETE
			// 
			this.txtSummaryBETE.Height = 0.1875F;
			this.txtSummaryBETE.Left = 3.5F;
			this.txtSummaryBETE.Name = "txtSummaryBETE";
			this.txtSummaryBETE.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right";
			this.txtSummaryBETE.Text = "Summary";
			this.txtSummaryBETE.Top = 0.25F;
			this.txtSummaryBETE.Width = 0.875F;
			// 
			// txtGroupCaption
			// 
			this.txtGroupCaption.Height = 0.1875F;
			this.txtGroupCaption.Left = 2.333333F;
			this.txtGroupCaption.Name = "txtGroupCaption";
			this.txtGroupCaption.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.txtGroupCaption.Text = "Totals";
			this.txtGroupCaption.Top = 0.1875F;
			this.txtGroupCaption.Width = 0.9166667F;
			// 
			// txtSubPropertyTotal
			// 
			this.txtSubPropertyTotal.Height = 0.1875F;
			this.txtSubPropertyTotal.Left = 4.4375F;
			this.txtSubPropertyTotal.Name = "txtSubPropertyTotal";
			this.txtSubPropertyTotal.OutputFormat = resources.GetString("txtSubPropertyTotal.OutputFormat");
			this.txtSubPropertyTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.txtSubPropertyTotal.Text = "Totals";
			this.txtSubPropertyTotal.Top = 0.1875F;
			this.txtSubPropertyTotal.Width = 1.0625F;
			// 
			// txtSubExemptionTotal
			// 
			this.txtSubExemptionTotal.Height = 0.1875F;
			this.txtSubExemptionTotal.Left = 5.5625F;
			this.txtSubExemptionTotal.Name = "txtSubExemptionTotal";
			this.txtSubExemptionTotal.OutputFormat = resources.GetString("txtSubExemptionTotal.OutputFormat");
			this.txtSubExemptionTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.txtSubExemptionTotal.Text = "Totals";
			this.txtSubExemptionTotal.Top = 0.1875F;
			this.txtSubExemptionTotal.Width = 0.875F;
			// 
			// txtSubAssessmentTotal
			// 
			this.txtSubAssessmentTotal.Height = 0.1875F;
			this.txtSubAssessmentTotal.Left = 6.5F;
			this.txtSubAssessmentTotal.Name = "txtSubAssessmentTotal";
			this.txtSubAssessmentTotal.OutputFormat = resources.GetString("txtSubAssessmentTotal.OutputFormat");
			this.txtSubAssessmentTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.txtSubAssessmentTotal.Text = "Totals";
			this.txtSubAssessmentTotal.Top = 0.1875F;
			this.txtSubAssessmentTotal.Width = 0.9166667F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 2.75F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0.125F;
			this.Line3.Width = 4.6875F;
			this.Line3.X1 = 2.75F;
			this.Line3.X2 = 7.4375F;
			this.Line3.Y1 = 0.125F;
			this.Line3.Y2 = 0.125F;
			// 
			// txtSubBilling
			// 
			this.txtSubBilling.Height = 0.1875F;
			this.txtSubBilling.Left = 6.5F;
			this.txtSubBilling.Name = "txtSubBilling";
			this.txtSubBilling.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.txtSubBilling.Text = "Assessment";
			this.txtSubBilling.Top = 0.3958333F;
			this.txtSubBilling.Width = 0.9166667F;
			// 
			// Field14
			// 
			this.Field14.Height = 0.1875F;
			this.Field14.Left = 5.552083F;
			this.Field14.Name = "Field14";
			this.Field14.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.Field14.Text = "Billing Value";
			this.Field14.Top = 0.3958333F;
			this.Field14.Width = 0.8854167F;
			// 
			// txtSubBETE
			// 
			this.txtSubBETE.Height = 0.1875F;
			this.txtSubBETE.Left = 3.3125F;
			this.txtSubBETE.Name = "txtSubBETE";
			this.txtSubBETE.OutputFormat = resources.GetString("txtSubBETE.OutputFormat");
			this.txtSubBETE.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.txtSubBETE.Text = null;
			this.txtSubBETE.Top = 0.1875F;
			this.txtSubBETE.Width = 1.0625F;
			// 
			// Field7
			// 
			this.Field7.Height = 0.1875F;
			this.Field7.Left = 2.5625F;
			this.Field7.Name = "Field7";
			this.Field7.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.Field7.Text = "Totals";
			this.Field7.Top = 0.375F;
			this.Field7.Width = 0.6875F;
			// 
			// txtPropertyTotal
			// 
			this.txtPropertyTotal.Height = 0.1875F;
			this.txtPropertyTotal.Left = 4.4375F;
			this.txtPropertyTotal.Name = "txtPropertyTotal";
			this.txtPropertyTotal.OutputFormat = resources.GetString("txtPropertyTotal.OutputFormat");
			this.txtPropertyTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.txtPropertyTotal.Text = "Totals";
			this.txtPropertyTotal.Top = 0.375F;
			this.txtPropertyTotal.Width = 1.0625F;
			// 
			// txtExemptionTotal
			// 
			this.txtExemptionTotal.Height = 0.1875F;
			this.txtExemptionTotal.Left = 5.5625F;
			this.txtExemptionTotal.Name = "txtExemptionTotal";
			this.txtExemptionTotal.OutputFormat = resources.GetString("txtExemptionTotal.OutputFormat");
			this.txtExemptionTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.txtExemptionTotal.Text = "Totals";
			this.txtExemptionTotal.Top = 0.375F;
			this.txtExemptionTotal.Width = 0.875F;
			// 
			// txtAssessmentTotal
			// 
			this.txtAssessmentTotal.Height = 0.1875F;
			this.txtAssessmentTotal.Left = 6.5F;
			this.txtAssessmentTotal.Name = "txtAssessmentTotal";
			this.txtAssessmentTotal.OutputFormat = resources.GetString("txtAssessmentTotal.OutputFormat");
			this.txtAssessmentTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.txtAssessmentTotal.Text = "Totals";
			this.txtAssessmentTotal.Top = 0.375F;
			this.txtAssessmentTotal.Width = 0.9166667F;
			// 
			// Field8
			// 
			this.Field8.Height = 0.1875F;
			this.Field8.Left = 0.0625F;
			this.Field8.Name = "Field8";
			this.Field8.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center";
			this.Field8.Text = "- - - - - - - - - - CATEGORY BREAKDOWN - - - - - - - - - - ";
			this.Field8.Top = 0.875F;
			this.Field8.Width = 7.3125F;
			// 
			// txtCatTotals
			// 
			this.txtCatTotals.Height = 0.1875F;
			this.txtCatTotals.Left = 2.1875F;
			this.txtCatTotals.Name = "txtCatTotals";
			this.txtCatTotals.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.txtCatTotals.Text = "Totals";
			this.txtCatTotals.Top = 1.125F;
			this.txtCatTotals.Width = 0.9375F;
			// 
			// txtCatCaptions
			// 
			this.txtCatCaptions.Height = 0.1875F;
			this.txtCatCaptions.Left = 3.1875F;
			this.txtCatCaptions.Name = "txtCatCaptions";
			this.txtCatCaptions.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
			this.txtCatCaptions.Text = "Captions";
			this.txtCatCaptions.Top = 1.125F;
			this.txtCatCaptions.Width = 4.1875F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 2.75F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.3125F;
			this.Line2.Width = 4.6875F;
			this.Line2.X1 = 2.75F;
			this.Line2.X2 = 7.4375F;
			this.Line2.Y1 = 0.3125F;
			this.Line2.Y2 = 0.3125F;
			// 
			// Field9
			// 
			this.Field9.Height = 0.1875F;
			this.Field9.Left = 3.25F;
			this.Field9.Name = "Field9";
			this.Field9.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.Field9.Text = "OverRide Total";
			this.Field9.Top = 0.5625F;
			this.Field9.Width = 1.125F;
			// 
			// txtOverRideTotal
			// 
			this.txtOverRideTotal.Height = 0.1875F;
			this.txtOverRideTotal.Left = 4.4375F;
			this.txtOverRideTotal.Name = "txtOverRideTotal";
			this.txtOverRideTotal.OutputFormat = resources.GetString("txtOverRideTotal.OutputFormat");
			this.txtOverRideTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.txtOverRideTotal.Text = null;
			this.txtOverRideTotal.Top = 0.5625F;
			this.txtOverRideTotal.Width = 1.0625F;
			// 
			// txtTotalBilling
			// 
			this.txtTotalBilling.Height = 0.1875F;
			this.txtTotalBilling.Left = 6.5F;
			this.txtTotalBilling.Name = "txtTotalBilling";
			this.txtTotalBilling.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.txtTotalBilling.Text = "Assessment";
			this.txtTotalBilling.Top = 0.5625F;
			this.txtTotalBilling.Width = 0.9166667F;
			// 
			// Field16
			// 
			this.Field16.Height = 0.1875F;
			this.Field16.Left = 5.552083F;
			this.Field16.Name = "Field16";
			this.Field16.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.Field16.Text = "Billing Value";
			this.Field16.Top = 0.5625F;
			this.Field16.Width = 0.8854167F;
			// 
			// txtBETETotal
			// 
			this.txtBETETotal.Height = 0.1875F;
			this.txtBETETotal.Left = 3.3125F;
			this.txtBETETotal.Name = "txtBETETotal";
			this.txtBETETotal.OutputFormat = resources.GetString("txtBETETotal.OutputFormat");
			this.txtBETETotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.txtBETETotal.Text = null;
			this.txtBETETotal.Top = 0.375F;
			this.txtBETETotal.Width = 1.0625F;
			// 
			// rptAuditReport
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.PageEnd += new System.EventHandler(this.ActiveReport_PageEnd);
			this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.3472222F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.3472222F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.447917F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtTown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtProperty)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSummary)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubOverRideTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBilling)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBETE)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSummaryValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSummaryBETE)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupCaption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubPropertyTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubExemptionTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubAssessmentTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubBilling)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubBETE)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPropertyTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptionTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessmentTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCatTotals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCatCaptions)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOverRideTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalBilling)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBETETotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtProperty;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSummary;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubOverRideTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBilling;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBETE;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSummaryValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSummaryBETE;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTown;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPropertyTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemptionTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessmentTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatTotals;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCatCaptions;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOverRideTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalBilling;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBETETotal;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field17;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupCaption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubPropertyTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubExemptionTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubAssessmentTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubBilling;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubBETE;
	}
}
