﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptBusinessCodes.
	/// </summary>
	public partial class rptBusinessCodes : BaseSectionReport
	{
		public rptBusinessCodes()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Business Codes";
		}

		public static rptBusinessCodes InstancePtr
		{
			get
			{
				return (rptBusinessCodes)Sys.GetInstance(typeof(rptBusinessCodes));
			}
		}

		protected rptBusinessCodes _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsRpt?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBusinessCodes	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsRpt = new clsDRWrapper();
		int intType;
		string strCode = "";
		string strDesc = "";

		public void Init(short intCode)
		{
			try
			{
				// On Error GoTo ErrorHandler
				string strTable = "";
				intType = intCode;
				switch (intCode)
				{
					case 1:
						{
							// business code
							strTable = "BusinessCodes";
							strCode = "BusinessCode";
							strDesc = "BusinessType";
							lblTitle.Text = "Business Codes";
							break;
						}
					case 2:
						{
							// street code
							lblTitle.Text = "Street Codes";
							strTable = "StreetCodes";
							strCode = "StreetCode";
							strDesc = "StreetName";
							break;
						}
					case 3:
						{
							// tran code
							strTable = "TranCodes";
							strCode = "TranCode";
							strDesc = "TranType";
							lblTitle.Text = "Tran Codes";
							break;
						}
				}
				//end switch
				this.Name = lblTitle.Text;
				clsRpt.OpenRecordset("select * from " + strTable + " order by " + strCode, modPPGN.strPPDatabase);
				if (clsRpt.EndOfFile())
				{
					MessageBox.Show("No codes defined", "No Codes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					this.Close();
					return;
				}
				frmReportViewer.InstancePtr.Init(rptObj: this, boolAllowEmail: true, strAttachmentName: "BusinessCodes");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Init", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsRpt.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblMuniname.Text = modGlobalConstants.Statics.MuniName;
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if (lblTitle.Text == "Business Codes")
			{
				frmPPBusinessCodes.InstancePtr.Show(App.MainForm);
			}
			else
			{
				frmPPStreetTranCodes.InstancePtr.Show(App.MainForm);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!clsRpt.EndOfFile())
			{
				txtCode.Text = FCConvert.ToString(clsRpt.Get_Fields(strCode));
				txtDescription.Text = clsRpt.Get_Fields(strDesc);
				clsRpt.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		
	}
}
