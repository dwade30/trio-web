﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;
using System.IO;
using fecherFoundation.VisualBasicLayer;
using TWSharedLibrary;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptTaxNotice.
	/// </summary>
	public partial class rptTaxNotice : BaseSectionReport
	{
		public rptTaxNotice()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Tax Notice";
		}

		public static rptTaxNotice InstancePtr
		{
			get
			{
				return (rptTaxNotice)Sys.GetInstance(typeof(rptTaxNotice));
			}
		}

		protected rptTaxNotice _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsSngAccount?.Dispose();
				clsTaxAccounts?.Dispose();
                clsTaxAccounts = null;
                clsSngAccount = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptTaxNotice	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool boolUseFreeForm;
		bool boolEstimatedTax;
		bool boolShowExemptions;
		bool boolIncExemptAccounts;
		string strYear = "";
		string strRate = "";
		int lngTotal;
		string[] strCat = new string[9 + 1];
		string[] strLine = new string[15 + 1];
		clsDRWrapper clsSngAccount = new clsDRWrapper();
		clsDRWrapper clsTaxAccounts = new clsDRWrapper();
		// vbPorter upgrade warning: intWhichAccounts As short	OnWriteFCConvert.ToInt32(
		public void Start(bool boolFreeForm, bool boolShowEstimatedTax, bool boolShowExempt, bool boolIncExempts, int intWhichAccounts, string strOrder, string strTaxYear = "", string strTaxRate = "")
		{
			try
			{
				// On Error GoTo ErrorHandler
				string strSQL1 = "";
				string strSQL2 = "";
				string strSelect = "";
				string strExempts = "";
				string strMasterJoin;
				strMasterJoin = modPPGN.GetMasterJoin();
				boolUseFreeForm = boolFreeForm;
				boolEstimatedTax = boolShowEstimatedTax;
				boolShowExemptions = boolShowExempt;
				boolIncExemptAccounts = boolIncExempts;
				if (!Information.IsNothing(strTaxYear))
				{
					strYear = strTaxYear;
				}
				if (!Information.IsNothing(strTaxRate))
				{
					strRate = strTaxRate;
				}
				if (!boolIncExempts)
				{
					strExempts = " ( (([VALUE] > 0)) and (exemption < [value])) ";
				}
				else
				{
					strExempts = "";
				}
				// Select Case UCase(strOrder)
				// Case "ACCOUNT"
				// strSelect = "ppmaster"
				// Case "NAME"
				// strSelect = "(select * from ppmaster order by name)"
				// Case "LOCATION"
				// strSelect = "(select * from ppmaster order by street,streetnumber)"
				// End Select
				switch (intWhichAccounts)
				{
					case 1:
						{
							strSQL1 = "Select * from ppvaluations ";
							strSQL2 = "select * from ppmaster where account > 0 and not deleted = 1 ";
							if (strExempts != string.Empty)
							{
								// strSQL2 = "select * from ppmaster where account > 0 and  (not deleted = 1) and account in (select account from ppmaster where " & strExempts & ") "
								strSQL2 = strMasterJoin + " where account > 0 and  (not deleted = 1) and account in (select account from ppmaster where " + strExempts + ") ";
							}
							break;
						}
					case 2:
						{
							if (Strings.UCase(strOrder) == "ACCOUNT")
							{
								strSQL1 = "select * from ppvaluations ";
								// strSQL2 = "select * from ppmaster where  (account between " & gintMinAccountrange & " and " & gintMaxAccountrange & ") and (not deleted = 1)"
								strSQL2 = strMasterJoin + " where  (account between " + FCConvert.ToString(modPPGN.Statics.gintMinAccountrange) + " and " + FCConvert.ToString(modPPGN.Statics.gintMaxAccountrange) + ") and (not deleted = 1)";
							}
							else if (Strings.UCase(strOrder) == "NAME")
							{
								strSQL1 = "select * from ppvaluations ";
								// strSQL2 = "select * from ppmaster where (name between '" & gstrMinAccountRange & "' and '" & gstrMaxAccountRange & "') and (not deleted = 1)"
								strSQL2 = strMasterJoin + " where (name between '" + modPPGN.Statics.gstrMinAccountRange + "' and '" + modPPGN.Statics.gstrMaxAccountRange + "') and (not deleted = 1)";
							}
							else if (Strings.UCase(strOrder) == "LOCATION")
							{
								strSQL1 = "select * from ppvaluations ";
								// strSQL2 = "select * from ppmaster where  (street between '" & gstrMinAccountRange & "' and '" & gstrMaxAccountRange & "') and (not deleted = 1)"
								strSQL2 = strMasterJoin + " where  (street between '" + modPPGN.Statics.gstrMinAccountRange + "' and '" + modPPGN.Statics.gstrMaxAccountRange + "') and (not deleted = 1)";
							}
							if (strExempts != string.Empty)
							{
								strSQL2 += " and  " + strExempts + " ";
							}
							break;
						}
					case 3:
						{
							strSQL1 = "select * from ppvaluations ";
							// strSQL2 = "select * from ppmaster where  (deleted <> 1) and account in (select account from accountlist)"
							strSQL2 = strMasterJoin + " where  (deleted <> 1) and account in (select account from accountlist)";
							if (strExempts != string.Empty)
							{
								// strSQL2 = "select * from ppmaster where  (deleted <> 1) and " & strExempts & " and account in (select account from accountlist)"
								strSQL2 = strMasterJoin + " where  (deleted <> 1) and " + strExempts + " and account in (select account from accountlist)";
							}
							break;
						}
				}
				//end switch
				strSQL1 += " order by valuekey";
				if (Strings.UCase(strOrder) == "ACCOUNT")
				{
					strSQL2 += " order by account";
				}
				else if (Strings.UCase(strOrder) == "NAME")
				{
					strSQL2 += " order by name";
				}
				else if (Strings.UCase(strOrder) == "LOCATION")
				{
					strSQL2 += " order by street,streetnumber";
				}
				if (!clsTaxAccounts.OpenRecordset(strSQL1, "twpp0000.vb1"))
				{
					MessageBox.Show("Unable to open records to complete this report", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
					this.Close();
					return;
				}
				if (!clsSngAccount.OpenRecordset(strSQL2, "twpp0000.vb1"))
				{
					MessageBox.Show("Unable to open records to complete this report", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
					this.Close();
					return;
				}
				frmReportViewer.InstancePtr.Init(rptObj: this, boolAllowEmail: true, strAttachmentName: "TaxNotice");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description, null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				this.Close();
				return;
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsSngAccount.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			StreamReader tsTax;
            using (clsDRWrapper clsTemp = new clsDRWrapper())
            {
                //this.Printer.RenderMode = 1;
                //modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
                txtTax1.Text = "";
                txtTax2.Text = "";
                txtTax3.Text = "";
                txtTax4.Text = "";
                txtTax5.Text = "";
                txtTax6.Text = "";
                txtTax7.Text = "";
                txtTax8.Text = "";
                txtTax9.Text = "";
                txtTax10.Text = "";
                txttax11.Text = "";
                txtTax12.Text = "";
                txtTax13.Text = "";
                txtTax14.Text = "";
                txtTax15.Text = "";
                if (File.Exists(FCFileSystem.CurDir() + "\\ppnotice.txt"))
                {
                    tsTax = File.OpenText(FCFileSystem.CurDir() + "\\PPnotice.txt");
                    txtTitle.Text = tsTax.ReadLine();
                    txtTax1.Text = tsTax.ReadLine();
                    txtTax2.Text = tsTax.ReadLine();
                    txtTax3.Text = tsTax.ReadLine();
                    txtTax4.Text = tsTax.ReadLine();
                    txtTax5.Text = tsTax.ReadLine();
                    txtTax6.Text = tsTax.ReadLine();
                    txtTax7.Text = tsTax.ReadLine();
                    if (boolUseFreeForm)
                    {
                        if (!tsTax.EndOfStream)
                        {
                            txtTax8.Text = tsTax.ReadLine();
                            txtTax9.Text = tsTax.ReadLine();
                            txtTax10.Text = tsTax.ReadLine();
                            txttax11.Text = tsTax.ReadLine();
                            txtTax12.Text = tsTax.ReadLine();
                            txtTax13.Text = tsTax.ReadLine();
                            txtTax14.Text = tsTax.ReadLine();
                            txtTax15.Text = tsTax.ReadLine();
                        }

                        txtCat1.Visible = false;
                        txtCat2.Visible = false;
                        txtCat3.Visible = false;
                        txtCat4.Visible = false;
                        txtCat5.Visible = false;
                        txtCat6.Visible = false;
                        txtCat7.Visible = false;
                        txtCat8.Visible = false;
                        txtCat9.Visible = false;
                        txtVal1.Visible = false;
                        txtVal2.Visible = false;
                        txtVal3.Visible = false;
                        txtVal4.Visible = false;
                        txtVal5.Visible = false;
                        txtVal6.Visible = false;
                        txtVal7.Visible = false;
                        txtVal8.Visible = false;
                        txtVal9.Visible = false;
                        txtTotal.Visible = false;
                        txtTotalLabel.Visible = false;
                        txtTaxLabel.Visible = false;
                        txtTax.Visible = false;
                    }
                    else
                    {
                        txtTax8.Visible = false;
                        txtTax9.Visible = false;
                        txttax11.Visible = false;
                        txtTax10.Visible = false;
                        txttax11.Visible = false;
                        txtTax12.Visible = false;
                        txtTax13.Visible = false;
                        txtTax14.Visible = false;
                        txtTax15.Visible = false;
                        // If boolShowExemptions Then
                        // txtExempt.Visible = True
                        // Else
                        // txtExempt.Visible = False
                        // End If
                        txtTotal.Visible = true;
                        txtTotalLabel.Visible = true;
                        if (boolEstimatedTax)
                        {
                            txtTax.Visible = true;
                            txtTaxLabel.Visible = true;
                        }
                        else
                        {
                            txtTax.Visible = false;
                            txtTaxLabel.Visible = false;
                        }
                    }

                    tsTax.Close();
                }

                clsTemp.OpenRecordset("select * from ratiotrends", "twpp0000.vb1");
                while (!clsTemp.EndOfFile())
                {
                    // TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
                    strCat[clsTemp.Get_Fields("type")] = FCConvert.ToString(clsTemp.Get_Fields_String("description"));
                    clsTemp.MoveNext();
                }
            }
        }

		private void Detail_Format(object sender, EventArgs e)
		{
			string strTemp;
			int intTBox = 0;
			int lngExempt = 0;
			int x;
			// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
			clsTaxAccounts.FindFirstRecord("valuekey", Conversion.Val(clsSngAccount.Get_Fields("account")));
			// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
			strTemp = clsSngAccount.Get_Fields("streetnumber") + " ";
			if (Conversion.Val(strTemp) != 0)
			{
				txtLocation.Text = Conversion.Str(Conversion.Val(strTemp + ""));
			}
			else
			{
				txtLocation.Text = "";
			}
			txtLocation.Text = Strings.Trim(txtLocation.Text + " " + clsSngAccount.Get_Fields_String("street"));
			txtMail1.Text = clsSngAccount.Get_Fields_String("name");
			txtMail2.Text = clsSngAccount.Get_Fields_String("address1");
			txtMail3.Text = clsSngAccount.Get_Fields_String("address2");
			txtMail4.Text = Strings.Trim(FCConvert.ToString(clsSngAccount.Get_Fields_String("city"))) + ",";
			txtMail4.Text = txtMail4.Text + Strings.Trim(FCConvert.ToString(clsSngAccount.Get_Fields_String("state"))) + " ";
			txtMail4.Text = txtMail4.Text + Strings.Trim(FCConvert.ToString(clsSngAccount.Get_Fields_String("zip"))) + " ";
			txtMail4.Text = txtMail4.Text + Strings.Trim(FCConvert.ToString(clsSngAccount.Get_Fields_String("zip4")));
			// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
			txtAccount.Text = "ACCT: " + clsSngAccount.Get_Fields("account");
			// txtMapLot.Text = "Map/Lot:" & clsSngAccount.fields("rsmaplot")
			if (!boolUseFreeForm)
			{
				txtCat1.Text = "";
				txtVal1.Text = "";
				txtCat2.Text = "";
				txtVal2.Text = "";
				txtCat3.Text = "";
				txtVal3.Text = "";
				txtCat4.Text = "";
				txtVal4.Text = "";
				txtCat5.Text = "";
				txtVal5.Text = "";
				txtCat6.Text = "";
				txtVal6.Text = "";
				txtCat7.Text = "";
				txtVal7.Text = "";
				txtCat8.Text = "";
				txtVal8.Text = "";
				txtCat9.Text = "";
				txtVal9.Text = "";
				// lngLand = Val(clsTaxAccounts.fields("landsum"))
				// txtLand.Text = Format(lngLand, "#,###,##0")
				// lngBldg = Val(clsTaxAccounts.fields("bldgsum"))
				// txtBldg.Text = Format(lngBldg, "###,###,##0")
				// TODO Get_Fields: Field [[value]] not found!! (maybe it is an alias?)
				lngTotal = FCConvert.ToInt32(Math.Round(Conversion.Val(clsSngAccount.Get_Fields("[value]"))));
				if (!clsTaxAccounts.NoMatch)
				{
					intTBox = 1;
					for (x = 1; x <= 9; x++)
					{
						// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
						if (Conversion.Val(clsTaxAccounts.Get_Fields("category" + FCConvert.ToString(x))) > 0)
						{
							switch (intTBox)
							{
								case 1:
									{
										txtCat1.Text = strCat[x];
										// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
										txtVal1.Text = Strings.Format(clsTaxAccounts.Get_Fields("category" + FCConvert.ToString(x)), "#,###,##0");
										break;
									}
								case 2:
									{
										txtCat2.Text = strCat[x];
										// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
										txtVal2.Text = Strings.Format(clsTaxAccounts.Get_Fields("category" + FCConvert.ToString(x)), "#,###,##0");
										break;
									}
								case 3:
									{
										txtCat3.Text = strCat[x];
										// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
										txtVal3.Text = Strings.Format(clsTaxAccounts.Get_Fields("category" + FCConvert.ToString(x)), "#,###,##0");
										break;
									}
								case 4:
									{
										txtCat4.Text = strCat[x];
										// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
										txtVal4.Text = Strings.Format(clsTaxAccounts.Get_Fields("category" + FCConvert.ToString(x)), "#,###,##0");
										break;
									}
								case 5:
									{
										txtCat5.Text = strCat[x];
										// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
										txtVal5.Text = Strings.Format(clsTaxAccounts.Get_Fields("category" + FCConvert.ToString(x)), "#,###,##0");
										break;
									}
								case 6:
									{
										txtCat6.Text = strCat[x];
										// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
										txtVal6.Text = Strings.Format(clsTaxAccounts.Get_Fields("category" + FCConvert.ToString(x)), "#,###,##0");
										break;
									}
								case 7:
									{
										txtCat7.Text = strCat[x];
										// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
										txtVal7.Text = Strings.Format(clsTaxAccounts.Get_Fields("category" + FCConvert.ToString(x)), "#,###,##0");
										break;
									}
								case 8:
									{
										txtCat8.Text = strCat[x];
										// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
										txtVal8.Text = Strings.Format(clsTaxAccounts.Get_Fields("category" + FCConvert.ToString(x)), "#,###,##0");
										break;
									}
								case 9:
									{
										txtCat9.Text = strCat[x];
										// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
										txtVal9.Text = Strings.Format(clsTaxAccounts.Get_Fields("category" + FCConvert.ToString(x)), "#,###,##0");
										break;
									}
							}
							//end switch
							intTBox += 1;
						}
					}
					// x
				}
				if (boolShowExemptions)
				{
					lngExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(clsSngAccount.Get_Fields_Int32("exemption"))));
					// txtExempt.Text = Format(lngExempt, "##,###,##0")
					lngTotal -= lngExempt;
				}
				else
				{
					// txtExempt.Text = ""
				}
				txtTotal.Text = Strings.Format(lngTotal, "##,###,###,##0");
				if (boolEstimatedTax)
				{
					txtTaxLabel.Text = "ESTIMATED " + strYear + " TAX:";
					txtTax.Text = Strings.Format(GetTax(), "###,##0.00");
				}
			}
			if (txtMail3.Text == "")
			{
				txtMail3.Text = txtMail2.Text;
				txtMail2.Text = txtMail1.Text;
				txtMail1.Text = "";
			}
			if (txtMail2.Text == "")
			{
				txtMail2.Text = txtMail1.Text;
				txtMail1.Text = "";
			}
			clsSngAccount.MoveNext();
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As float
		private float GetTax()
		{
			float GetTax = 0;
			float snglTax;
			snglTax = FCConvert.ToSingle(Conversion.Val(strRate + ""));
			snglTax *= lngTotal / 1000;
			GetTax = snglTax;
			return GetTax;
		}

		
	}
}
