﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptBETE.
	/// </summary>
	public partial class rptBETE : BaseSectionReport
	{
		public rptBETE()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "BETE";
		}

		public static rptBETE InstancePtr
		{
			get
			{
				return (rptBETE)Sys.GetInstance(typeof(rptBETE));
			}
		}

		protected rptBETE _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsReport?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBETE	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsDRWrapper rsReport = new clsDRWrapper();
		private clsDRWrapper rsReport_AutoInitialized;

		private clsDRWrapper rsReport
		{
			get
			{
				if (rsReport_AutoInitialized == null)
				{
					rsReport_AutoInitialized = new clsDRWrapper();
				}
				return rsReport_AutoInitialized;
			}
			set
			{
				rsReport_AutoInitialized = value;
			}
		}

		private string[] strCatDesc = new string[9 + 1];
		private double[] dblCatAmt = new double[9 + 1];

		public void Init(ref string strSQL)
		{
			// Dim strSQL As String
			// strSQL = "select account,name,cat1exempt,cat2exempt,cat3exempt,cat4exempt,cat5exempt,cat6exempt,cat7exempt,cat8exempt,cat9exempt from ppmaster INNER join ppvaluations on (ppmaster.account = ppvaluations.valuekey) where not deleted and cat1exempt > 0 or cat2exempt > 0 or cat3exempt > 0 or cat4exempt > 0 or cat5exempt > 0 or cat6exempt > 0 or cat7exempt > 0 or cat8Exempt > 0 or cat9exempt > 0 order by account"
			rsReport.OpenRecordset(strSQL, modPPGN.strPPDatabase);
			if (rsReport.EndOfFile())
			{
				MessageBox.Show("No records found", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Close();
				return;
			}
			frmReportViewer.InstancePtr.Init(rptObj: this, strFileTitle: "BETE Report", strAttachmentName: "BETERPT");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsReport.EndOfFile();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + PageNumber;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblMuniname.Text = modGlobalConstants.Statics.MuniName;
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			lblPage.Text = "Page 1";
			int x;
			for (x = 1; x <= 9; x++)
			{
				dblCatAmt[x] = 0;
			}
			// x
            using (clsDRWrapper rsTemp = new clsDRWrapper())
            {
                rsTemp.OpenRecordset("select * from ratiotrends order by type", modPPGN.strPPDatabase);
                while (!rsTemp.EndOfFile())
                {
                    // TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
                    strCatDesc[rsTemp.Get_Fields("type")] = FCConvert.ToString(rsTemp.Get_Fields_String("Description"));
                    rsTemp.MoveNext();
                }
            }
        }

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsReport.EndOfFile())
			{
				//clsDRWrapper rsTemp = new clsDRWrapper();
				txtName.Text = rsReport.Get_Fields_String("name");
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				txtAccount.Text = FCConvert.ToString(rsReport.Get_Fields("account"));
				string strCat = "";
				string strCatValue = "";
				strCat = "";
				strCatValue = "";
				int x;
				for (x = 1; x <= 9; x++)
				{
					// TODO Get_Fields: Field [cat] not found!! (maybe it is an alias?)
					if (Conversion.Val(rsReport.Get_Fields("cat" + FCConvert.ToString(x) + "exempt")) > 0)
					{
						strCat += strCatDesc[x] + "\r\n";
						// TODO Get_Fields: Field [cat] not found!! (maybe it is an alias?)
						strCatValue += Strings.Format(Conversion.Val(rsReport.Get_Fields("cat" + FCConvert.ToString(x) + "exempt")), "###,###,###,##0") + "\r\n";
						// TODO Get_Fields: Field [cat] not found!! (maybe it is an alias?)
						dblCatAmt[x] += Conversion.Val(rsReport.Get_Fields("cat" + FCConvert.ToString(x) + "exempt"));
					}
				}
				// x
				if (!(strCat == ""))
				{
					strCat = Strings.Mid(strCat, 1, strCat.Length - 1);
					strCatValue = Strings.Mid(strCatValue, 1, strCatValue.Length - 1);
				}
				txtCat.Text = strCat;
				txtCatValue.Text = strCatValue;
				rsReport.MoveNext();
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			int x;
			string strCat;
			string strCatValue;
			strCat = "";
			strCatValue = "";
			for (x = 1; x <= 9; x++)
			{
				if (dblCatAmt[x] > 0)
				{
					strCat += strCatDesc[x] + "\r\n";
					strCatValue += Strings.Format(dblCatAmt[x], "#,###,###,##0") + "\r\n";
				}
			}
			// x
			if (!(strCat == ""))
			{
				strCat = Strings.Mid(strCat, 1, strCat.Length - 1);
				strCatValue = Strings.Mid(strCatValue, 1, strCatValue.Length - 1);
			}
			txtTotCat.Text = strCat;
			txtTotValue.Text = strCatValue;
		}

		
	}
}
