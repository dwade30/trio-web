﻿//Fecher vbPorter - Version 1.0.0.32
using Wisej.Web;
using Wisej.Core;
using fecherFoundation;
using Global;
using System;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmGenericMessage.
	/// </summary>
	public partial class frmGenericMessage : BaseForm
	{
		public frmGenericMessage()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGenericMessage InstancePtr
		{
			get
			{
				return (frmGenericMessage)Sys.GetInstance(typeof(frmGenericMessage));
			}
		}

		protected frmGenericMessage _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		public void ChangeSize(ref int lngHeight, ref int lngWidth)
		{
			this.Height = lngHeight;
			this.Width = lngWidth;
			RichTextBox1.Width = this.Width - 300;
			RichTextBox1.Left = 100;
			RichTextBox1.Height = this.Height - 805;
			RichTextBox1.Top = 50;
		}

		public void Changemessage(string strMessage)
		{
			RichTextBox1.Text = strMessage;
		}

		private void frmGenericMessage_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmGenericMessage_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGenericMessage properties;
			//frmGenericMessage.ScaleWidth	= 9300;
			//frmGenericMessage.ScaleHeight	= 7710;
			//frmGenericMessage.LinkTopic	= "Form1";
			//RichTextBox1 properties;
			//RichTextBox1.ScrollBars	= 2;
			//RichTextBox1.TextRTF	= $"frmGenericMessage.frx":058A;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			rptGenericMessage.InstancePtr.Init("Personal Property Extract Format");
			rptGenericMessage.InstancePtr.Unload();
		}
	}
}
