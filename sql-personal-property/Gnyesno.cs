﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Core;
using Wisej.Web;

namespace TWPP0000
{
	public class modGNYesNo
	{
		public static void ClearSpecs()
		{
			int X;
			for (X = 1; X <= 10; X++)
			{
				modGlobal.Statics.SpecLength[X] = 0;
				modGlobal.Statics.SpecCase[X] = "";
				modGlobal.Statics.SpecKeyword[X] = "";
				modGlobal.Statics.SpecM1[X] = "";
				modGlobal.Statics.SpecM2[X] = "";
				modGlobal.Statics.SpecM3[X] = "";
				modGlobal.Statics.SpecM4[X] = "";
				modGlobal.Statics.SpecM5[X] = "";
				modGlobal.Statics.SpecM6[X] = "";
				modGlobal.Statics.SpecM7[X] = "";
				modGlobal.Statics.SpecM8[X] = "";
				modGlobal.Statics.SpecM9[X] = "";
				modGlobal.Statics.SpecM10[X] = "";
				modGlobal.Statics.SpecType[X] = "";
				modGlobal.Statics.SpecParam[X] = "";
				modGlobal.Statics.SpecLow[X] = Convert.ToString(0);
				modGlobal.Statics.SpecHigh[X] = Convert.ToString(0);
			}
			// X
			frmGNYesNo.lblm1.Caption = "";
			frmGNYesNo.lblm2.Caption = "";
			frmGNYesNo.lblm3.Caption = "";
			frmGNYesNo.lblM4.Caption = "";
			frmGNYesNo.lblM5.Caption = "";
			frmGNYesNo.lblM6.Caption = "";
			frmGNYesNo.lblM7.Caption = "";
			frmGNYesNo.lblM8.Caption = "";
			frmGNYesNo.lblM9.Caption = "";
			frmGNYesNo.lblM10.Caption = "";
		}

		public static void Get1_10(ref int Fld)
		{
			modGlobal.Statics.RespLength = modGlobal.Statics.SpecLength[Fld];
			modGlobal.Statics.RespCase = modGlobal.Statics.SpecCase[Fld];
			modGlobal.Statics.RespKeyword = modGlobal.Statics.SpecKeyword[Fld];
			modGlobal.Statics.RespM1 = modGlobal.Statics.SpecM1[Fld];
			modGlobal.Statics.RespM2 = modGlobal.Statics.SpecM2[Fld];
			modGlobal.Statics.RespM3 = modGlobal.Statics.SpecM3[Fld];
			modGlobal.Statics.RespM4 = modGlobal.Statics.SpecM4[Fld];
			modGlobal.Statics.RespM5 = modGlobal.Statics.SpecM5[Fld];
			modGlobal.Statics.RespM6 = modGlobal.Statics.SpecM6[Fld];
			modGlobal.Statics.RespM7 = modGlobal.Statics.SpecM7[Fld];
			modGlobal.Statics.RespM8 = modGlobal.Statics.SpecM8[Fld];
			modGlobal.Statics.RespM9 = modGlobal.Statics.SpecM9[Fld];
			modGlobal.Statics.RespM10 = modGlobal.Statics.SpecM10[Fld];
			modGlobal.Statics.RespType = modGlobal.Statics.SpecType[Fld];
			modGlobal.Statics.RespParam = modGlobal.Statics.SpecParam[Fld];
			modGlobal.Statics.RespLow = modGlobal.Statics.SpecLow[Fld];
			modGlobal.Statics.RespHigh = modGlobal.Statics.SpecHigh[Fld];
			if (Fld > 1)
			{
				frmGNYesNo.cmdBack.Visible = true;
			}
			else
			{
				frmGNYesNo.cmdBack.Visible = false;
			}
		}

		public class StaticVariables
		{
			//=========================================================
			public string Ans = "";
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
