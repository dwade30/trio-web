﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Core;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptCustom.
	/// </summary>
	public partial class rptCustom : BaseSectionReport
	{
		private clsDRWrapper rsLoad = new clsDRWrapper();
		public rptCustom()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Custom Report";
		}

        public void Init(string strsql)
        {
            rsLoad.OpenRecordset(strsql, "PersonalProperty");
            if (rsLoad.EndOfFile())
            {
                MessageBox.Show("No records found");
                this.Unload();
                return;
            }
            frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "Custom");
        }

		public static rptCustom InstancePtr
		{
			get
			{
				return (rptCustom)Sys.GetInstance(typeof(rptCustom));
			}
		}

		protected rptCustom _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}

            if (disposing)
            {
				rsLoad?.Dispose();
                rsLoad = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCustom	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private int intPage;
		private int intAccount;
		private bool boolPrint;
		private string strName = "";
		private string strLocation = "";

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			Fields.Add("AccountNumber");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			ShowRecord:
			;
			eArgs.EOF = rsLoad.EndOfFile();
			if (eArgs.EOF)
				return;
			if (FCConvert.ToInt32(rsLoad.GetData("masterAccount")) == intAccount)
			{
				if (!rsLoad.EndOfFile())
				{
					rsLoad.MoveNext();
				}
				goto ShowRecord;
			}
			txtName.Text = rsLoad.GetData("Name").ToString();
			if (frmPPPrinting.InstancePtr.chkMailingAddress.Checked)
			{
				txtName.Text = txtName.Text + "\r\n" + GetAddress();
			}
			txtLocation.Text = rsLoad.GetData("StreetNumber") + " " + rsLoad.GetData("Street");
			txtAccount.Text = rsLoad.GetData("masterAccount").ToString();
			// txtAddress = GetAddress
			txtOpen1.Text = rsLoad.GetData("Open1").ToString();
			txtOpen2.Text = rsLoad.GetData("Open2").ToString();
			txtName.Visible = frmPPPrinting.InstancePtr.chkName;
			lblName.Visible = frmPPPrinting.InstancePtr.chkName;
			txtLocation.Visible = frmPPPrinting.InstancePtr.chkLocation;
			lblLocation.Visible = frmPPPrinting.InstancePtr.chkLocation;
			txtAccount.Visible = frmPPPrinting.InstancePtr.chkAccount;
			lblAccount.Visible = frmPPPrinting.InstancePtr.chkAccount;
			// txtAddress.Visible = frmPPPrinting.chkMailingAddress
			lblAddress.Visible = frmPPPrinting.InstancePtr.chkMailingAddress;
			txtOpen1.Visible = frmPPPrinting.InstancePtr.chkOpen1;
			lblOpen1.Visible = frmPPPrinting.InstancePtr.chkOpen1;
			txtOpen2.Visible = frmPPPrinting.InstancePtr.chkOpen2;
			lblOpen2.Visible = frmPPPrinting.InstancePtr.chkOpen2;
			intAccount = FCConvert.ToInt32(rsLoad.GetData("masterAccount"));
			if (!rsLoad.EndOfFile())
				rsLoad.MoveNext();
			eArgs.EOF = false;
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		private string GetAddress()
		{
			string GetAddress = "";
			string strTemp = "";
			if (Strings.Trim(FCConvert.ToString(rsLoad.GetData("Address1"))) != string.Empty)
			{
				strTemp += rsLoad.GetData("Address1") + "\r\n";
			}
			if (Strings.Trim(FCConvert.ToString(rsLoad.GetData("Address2"))) != string.Empty)
			{
				strTemp += rsLoad.GetData("Address2") + "\r\n";
			}
			strTemp += Strings.Trim(FCConvert.ToString(rsLoad.GetData("City"))) + ", " + rsLoad.GetData("State") + " " + Strings.Trim(FCConvert.ToString(rsLoad.GetData("Zip")));
			if (Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("zip4"))) != string.Empty)
			{
				strTemp += " " + rsLoad.Get_Fields_String("zip4");
			}
			GetAddress = strTemp;
			return GetAddress;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//this.Printer.RenderMode = 1;
			if (Strings.Trim(modPPGN.Statics.Open1Title) != string.Empty)
			{
				lblOpen1.Text = modPPGN.Statics.Open1Title;
			}
			if (Strings.Trim(modPPGN.Statics.Open2Title) != string.Empty)
			{
				lblOpen2.Text = modPPGN.Statics.Open2Title;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTitle.Text = "Custom Report";
			intPage += 1;
			txtPage.Text = "Page: " + FCConvert.ToString(intPage);
		}

		
	}
}
