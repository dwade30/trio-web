﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Core;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for srptItemizedInput.
	/// </summary>
	public partial class srptItemizedInput : FCSectionReport
	{
		public srptItemizedInput()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ActiveReport1";
		}

		public static srptItemizedInput InstancePtr
		{
			get
			{
				return (srptItemizedInput)Sys.GetInstance(typeof(srptItemizedInput));
			}
		}

		protected srptItemizedInput _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsReports?.Dispose();
				clsTemp?.Dispose();
                clsReports = null;
                clsTemp = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptItemizedInput	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool boolPrint;
		clsDRWrapper clsReports = new clsDRWrapper();
		clsDRWrapper clsTemp = new clsDRWrapper();
		int lngAcct;

		public void PrintLeased()
		{
			if (FCConvert.ToString(clsReports.Get_Fields_String("CD")) == "-")
			{
				// TODO Get_Fields: Check the table for the column [Line] and replace with corresponding Get_Field method
				txtLine.Text = Strings.Format(clsReports.Get_Fields("Line"), "0000");
				txtCD.Text = clsReports.Get_Fields_String("CD");
				// TODO Get_Fields: Check the table for the column [rb] and replace with corresponding Get_Field method
				txtRB.Tag = clsReports.Get_Fields("rb");
				txtQTY.Text = string.Empty;
				txtExemptYear.Text = "";
				txtDescription.Text = Strings.Trim(FCConvert.ToString(clsReports.Get_Fields_String("Description")));
				txtCost.Text = string.Empty;
				txtGD.Text = string.Empty;
				txtFCTR.Text = string.Empty;
				txtSRO.Text = string.Empty;
				txtDepYrs.Text = string.Empty;
				txtYear.Text = string.Empty;
				txtLeasedTo.Text = string.Empty;
				boolPrint = true;
			}
			// TODO Get_Fields: Check the table for the column [Cost] and replace with corresponding Get_Field method
			else if (Conversion.Val(clsReports.Get_Fields("Cost")) != 0)
			{
				if (Strings.Trim(FCConvert.ToString(clsReports.Get_Fields_String("Description"))) != string.Empty)
				{
					// TODO Get_Fields: Check the table for the column [Line] and replace with corresponding Get_Field method
					txtLine.Text = Strings.Format(clsReports.Get_Fields("Line"), "0000");
					txtCD.Text = clsReports.Get_Fields_String("CD");
					// TODO Get_Fields: Check the table for the column [rb] and replace with corresponding Get_Field method
					txtRB.Text = clsReports.Get_Fields_String("rb");
					if (clsReports.Get_Fields_Int32("exemptyear") > 0)
					{
						txtExemptYear.Text = FCConvert.ToString(clsReports.Get_Fields_Int32("exemptyear"));
					}
					else
					{
						txtExemptYear.Text = "";
					}
					if (rptLeasedandItemizedInput.InstancePtr.boolPQTY)
					{
						// TODO Get_Fields: Check the table for the column [Quantity] and replace with corresponding Get_Field method
						txtQTY.Text = clsReports.Get_Fields_String("Quantity");
					}
					else
					{
						txtQTY.Text = "__";
					}
					txtDescription.Text = Strings.Trim(FCConvert.ToString(clsReports.Get_Fields_String("Description")));
					// TODO Get_Fields: Check the table for the column [Cost] and replace with corresponding Get_Field method
					txtCost.Text = Strings.Format(clsReports.Get_Fields("Cost"), "###,###,###");
					txtGD.Text = FCConvert.ToString(clsReports.Get_Fields_Int16("GD"));
					txtFCTR.Text = FCConvert.ToString(clsReports.Get_Fields_Int16("FCTR"));
					// TODO Get_Fields: Check the table for the column [SRO] and replace with corresponding Get_Field method
					txtSRO.Text = clsReports.Get_Fields_String("SRO");
					if (Conversion.Val(clsReports.Get_Fields_Int32("leasedto")) > 0)
					{
						txtLeasedTo.Text = FCConvert.ToString(clsReports.Get_Fields_Int32("leasedto"));
					}
					else
					{
						txtLeasedTo.Text = string.Empty;
					}
					txtDepYrs.Text = FCConvert.ToString(clsReports.Get_Fields_Int16("dpyr"));
					// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
					txtYear.Text = clsReports.Get_Fields_String("Year");
					boolPrint = true;
				}
			}
				// TODO Get_Fields: Check the table for the column [Cost] and replace with corresponding Get_Field method
				else if (Conversion.Val(clsReports.Get_Fields("Cost")) == 0)
			{
				if (Strings.Trim(FCConvert.ToString(clsReports.Get_Fields_String("Description"))) != string.Empty)
				{
					// TODO Get_Fields: Check the table for the column [Line] and replace with corresponding Get_Field method
					txtLine.Text = Strings.Format(clsReports.Get_Fields("Line"), "0000");
					txtCD.Text = clsReports.Get_Fields_String("CD");
					if (rptLeasedandItemizedInput.InstancePtr.boolPQTY)
					{
						// TODO Get_Fields: Check the table for the column [Quantity] and replace with corresponding Get_Field method
						txtQTY.Text = clsReports.Get_Fields_String("Quantity");
					}
					else
					{
						txtQTY.Text = "__";
					}
					if (clsReports.Get_Fields_Int32("exemptyear") > 0)
					{
						txtExemptYear.Text = FCConvert.ToString(clsReports.Get_Fields_Int32("exemptyear"));
					}
					else
					{
						txtExemptYear.Text = "";
					}
					txtDescription.Text = Strings.Trim(FCConvert.ToString(clsReports.Get_Fields_String("Description")));
					txtCost.Text = string.Empty;
					txtGD.Text = FCConvert.ToString(clsReports.Get_Fields_Int16("GD"));
					txtFCTR.Text = FCConvert.ToString(clsReports.Get_Fields_Int16("FCTR"));
					// TODO Get_Fields: Check the table for the column [SRO] and replace with corresponding Get_Field method
					txtSRO.Text = clsReports.Get_Fields_String("SRO");
					if (Conversion.Val(clsReports.Get_Fields_Int32("leasedto")) > 0)
					{
						txtLeasedTo.Text = FCConvert.ToString(clsReports.Get_Fields_Int32("leasedto"));
					}
					else
					{
						txtLeasedTo.Text = string.Empty;
					}
					txtDepYrs.Text = FCConvert.ToString(clsReports.Get_Fields_Int16("dpyr"));
					// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
					txtYear.Text = clsReports.Get_Fields_String("Year");
					boolPrint = true;
				}
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			ShowRecord:
			;
			eArgs.EOF = clsReports.EndOfFile();
			if (eArgs.EOF)
				return;
			boolPrint = false;
			PrintLeased();
			if (!boolPrint)
			{
				clsReports.MoveNext();
				goto ShowRecord;
			}
			else
			{
				clsReports.MoveNext();
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			//SubReport1.Report = null;
			//SubReport1 = null;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData)));
			clsReports.OpenRecordset("select * from ppitemized where account = " + FCConvert.ToString(lngAcct) + " order by line", "twpp0000.vb1");
			// If Not SubReport1 Is Nothing Then
			// If Not SubReport1.object Is Nothing Then
			// Unload SubReport1.object
			// End If
			// End If
			SubReport1.Report = new srptLeasedOutItemizednput();
			SubReport1.Report.UserData = this.UserData;
			string strMasterJoinJoin;
			strMasterJoinJoin = modPPGN.GetMasterJoinForJoin(false);
			// Call clsTemp.OpenRecordset("select name,street,streetnumber from ppmaster where account = " & lngAcct, "twpp0000.vb1")
			clsTemp.OpenRecordset("select name,street,streetnumber from " + strMasterJoinJoin + " where account = " + FCConvert.ToString(lngAcct), "twpp0000.vb1");
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			rptLeasedandItemizedInput.InstancePtr.txtAcct.Text = FCConvert.ToString(this.UserData);
			rptLeasedandItemizedInput.InstancePtr.txtName.Text = clsTemp.Get_Fields_String("name");
			// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
			rptLeasedandItemizedInput.InstancePtr.txtLocation.Text = clsTemp.Get_Fields_String("streetnumber") + " " + clsTemp.Get_Fields_String("street");
		}

		
	}
}
