﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmHelpCodes.
	/// </summary>
	public partial class frmHelpCodes : BaseForm
	{
		public frmHelpCodes()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmHelpCodes InstancePtr
		{
			get
			{
				return (frmHelpCodes)Sys.GetInstance(typeof(frmHelpCodes));
			}
		}

		protected frmHelpCodes _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private void frmHelpCodes_Activated(object sender, System.EventArgs e)
		{
			if (modGNWork.FormExist(this))
				return;
		}

		private void frmHelpCodes_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;				
				
				frmHelpCodes.InstancePtr.Close();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void ListChoice()
		{
			int intIndex;
			// vbPorter upgrade warning: varResponse As Variant --> As string
			string varResponse;
			//FC:FINAL:MSH - i.issue #1331: clear errors info for correct work on the first start
			Information.Err().Clear();
			modPPHelp.Statics.intHelpResponse = 0;
			intIndex = lstCodes.SelectedIndex;
			//FC:FINAL:MSH - i.issue #1330: incorrect getting text from selected item(lstCodex.Text is empty)
			//varResponse = Strings.Mid(lstCodes.Text, 1, 5);
			varResponse = Strings.Mid(lstCodes.Items[intIndex].Text, 1, 5);
			/*? On Error Resume Next  */
			try
			{
				modPPHelp.Statics.intHelpResponse = FCConvert.ToInt16(FCConvert.ToDouble(varResponse));
				if (Information.Err().Number != 0)
				{
					modPPHelp.Statics.intHelpResponse = 0;
					Information.Err().Clear();
				}
				
				//FC:FINAL:MSH - i.issue #1331: restore closing form after selecting item
				frmHelpCodes.InstancePtr.Unload();
			}
			catch (Exception ex)
			{
			}
		}

		private void frmHelpCodes_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmHelpCodes properties;
			//frmHelpCodes.ScaleWidth	= 5910;
			//frmHelpCodes.ScaleHeight	= 4305;
			//frmHelpCodes.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void lstCodes_DoubleClick(object sender, System.EventArgs e)
		{
			ListChoice();
		}

		private void lstCodes_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Return)
			{
				KeyCode = (Keys)0;
				ListChoice();
			}
		}
	}
}
