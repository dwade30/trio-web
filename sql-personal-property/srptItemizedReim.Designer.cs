﻿namespace TWPP0000
{
	/// <summary>
	/// Summary description for srptItemizedReim.
	/// </summary>
	partial class srptItemizedReim
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptItemizedReim));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtYR = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMO = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDesc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssess = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOriginal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQTY = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtAssessmentTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOriginalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYR)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMO)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTY)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessmentTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtYR,
				this.txtMO,
				this.txtLine,
				this.txtCD,
				this.txtDesc,
				this.txtAssess,
				this.txtOriginal,
				this.txtQTY,
				this.Line1
			});
			this.Detail.Height = 0.2083333F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label4,
				this.Line2
			});
			this.ReportHeader.Height = 0.3125F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAssessmentTotal,
				this.txtOriginalTotal,
				this.Label13,
				this.Line7
			});
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.Visible = false;
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label15,
				this.Label16,
				this.Label17,
				this.Label18,
				this.Label19,
				this.Label20,
				this.Label21,
				this.Label22,
				this.Label23,
				this.Label24,
				this.Label25
			});
			this.GroupHeader1.Height = 0.3854167F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Label4
			// 
			this.Label4.Height = 0.19F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 2.4375F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label4.Text = "ITEMIZED  DESCRIPTION";
			this.Label4.Top = 0.0625F;
			this.Label4.Width = 2.5F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0.0625F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.25F;
			this.Line2.Width = 7.4375F;
			this.Line2.X1 = 0.0625F;
			this.Line2.X2 = 7.5F;
			this.Line2.Y1 = 0.25F;
			this.Line2.Y2 = 0.25F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.19F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 0.375F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'";
			this.Label15.Text = "Line";
			this.Label15.Top = 0.1875F;
			this.Label15.Width = 0.3125F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.19F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 0.75F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Tahoma\'; text-align: center";
			this.Label16.Text = "CD";
			this.Label16.Top = 0.1875F;
			this.Label16.Width = 0.25F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.19F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 1.0625F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Tahoma\'";
			this.Label17.Text = "QTY";
			this.Label17.Top = 0.1875F;
			this.Label17.Width = 0.33F;
            // 
            // Label18
            // 
            this.Label18.Height = 0.19F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 1.6875F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Tahoma\'";
			this.Label18.Text = "Description";
			this.Label18.Top = 0.1875F;
			this.Label18.Width = 1.625F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.19F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 4F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Tahoma\'";
			this.Label19.Text = "Month Year";
			this.Label19.Top = 0.1875F;
			this.Label19.Width = 0.75F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.19F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 4.75F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \'Tahoma\'";
			this.Label20.Text = "Claimed";
			this.Label20.Top = 0.1875F;
			this.Label20.Width = 0.5625F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.19F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 5.875F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Tahoma\'";
			this.Label21.Text = "Cost";
			this.Label21.Top = 0.1875F;
			this.Label21.Width = 0.375F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.19F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 6.625F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-family: \'Tahoma\'";
			this.Label22.Text = "Assessment";
			this.Label22.Top = 0.1875F;
			this.Label22.Width = 0.875F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.19F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 4F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-family: \'Tahoma\'; text-align: center";
			this.Label23.Text = "In Service";
			this.Label23.Top = 0.03125F;
			this.Label23.Width = 0.75F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.19F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 5.75F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-family: \'Tahoma\'";
			this.Label24.Text = "Original";
			this.Label24.Top = 0.03125F;
			this.Label24.Width = 0.5625F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.19F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 4.8125F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-family: \'Tahoma\'";
			this.Label25.Text = "#YRS";
			this.Label25.Top = 0.03125F;
			this.Label25.Width = 0.4375F;
			// 
			// txtYR
			// 
			this.txtYR.Height = 0.19F;
			this.txtYR.Left = 4.3125F;
			this.txtYR.Name = "txtYR";
			this.txtYR.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtYR.Text = "Field5";
			this.txtYR.Top = 0.03125F;
			this.txtYR.Width = 0.375F;
			// 
			// txtMO
			// 
			this.txtMO.Height = 0.19F;
			this.txtMO.Left = 3.9375F;
			this.txtMO.Name = "txtMO";
			this.txtMO.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtMO.Text = "Field4";
			this.txtMO.Top = 0.03125F;
			this.txtMO.Width = 0.25F;
			// 
			// txtLine
			// 
			this.txtLine.Height = 0.19F;
			this.txtLine.Left = 0.0625F;
			this.txtLine.Name = "txtLine";
			this.txtLine.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtLine.Text = "Field1";
			this.txtLine.Top = 0.03125F;
			this.txtLine.Width = 0.625F;
			// 
			// txtCD
			// 
			this.txtCD.Height = 0.19F;
			this.txtCD.Left = 0.75F;
			this.txtCD.Name = "txtCD";
			this.txtCD.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.txtCD.Text = "Field2";
			this.txtCD.Top = 0.03125F;
			this.txtCD.Width = 0.25F;
			// 
			// txtDesc
			// 
			this.txtDesc.CanShrink = true;
			this.txtDesc.Height = 0.19F;
			this.txtDesc.Left = 1.6875F;
			this.txtDesc.Name = "txtDesc";
			this.txtDesc.Style = "font-family: \'Tahoma\'";
			this.txtDesc.Text = "Field3";
			this.txtDesc.Top = 0.03125F;
			this.txtDesc.Width = 2.1875F;
			// 
			// txtAssess
			// 
			this.txtAssess.CanGrow = false;
			this.txtAssess.Height = 0.19F;
			this.txtAssess.Left = 6.375F;
			this.txtAssess.Name = "txtAssess";
			this.txtAssess.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtAssess.Text = "Field7";
			this.txtAssess.Top = 0.03125F;
			this.txtAssess.Width = 1.0625F;
			// 
			// txtOriginal
			// 
			this.txtOriginal.CanGrow = false;
			this.txtOriginal.Height = 0.19F;
			this.txtOriginal.Left = 5.375F;
			this.txtOriginal.Name = "txtOriginal";
			this.txtOriginal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtOriginal.Text = "Field8";
			this.txtOriginal.Top = 0.03125F;
			this.txtOriginal.Width = 0.9375F;
			// 
			// txtQTY
			// 
			this.txtQTY.Height = 0.19F;
			this.txtQTY.Left = 1.0625F;
			this.txtQTY.Name = "txtQTY";
			this.txtQTY.Style = "font-family: \'Tahoma\'";
			this.txtQTY.Text = null;
			this.txtQTY.Top = 0.03125F;
			this.txtQTY.Width = 0.5520833F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 4.75F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.1875F;
			this.Line1.Width = 0.5F;
			this.Line1.X1 = 4.75F;
			this.Line1.X2 = 5.25F;
			this.Line1.Y1 = 0.1875F;
			this.Line1.Y2 = 0.1875F;
			// 
			// txtAssessmentTotal
			// 
			this.txtAssessmentTotal.Height = 0.19F;
			this.txtAssessmentTotal.Left = 6.3125F;
			this.txtAssessmentTotal.Name = "txtAssessmentTotal";
			this.txtAssessmentTotal.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtAssessmentTotal.Text = "Field6";
			this.txtAssessmentTotal.Top = 0.0625F;
			this.txtAssessmentTotal.Width = 1.125F;
			// 
			// txtOriginalTotal
			// 
			this.txtOriginalTotal.Height = 0.19F;
			this.txtOriginalTotal.Left = 5.1875F;
			this.txtOriginalTotal.Name = "txtOriginalTotal";
			this.txtOriginalTotal.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtOriginalTotal.Text = "Field7";
			this.txtOriginalTotal.Top = 0.0625F;
			this.txtOriginalTotal.Width = 1.0625F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 0F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Label13.Text = "Itemized Total (With ratio applied and rounded by Category):";
			this.Label13.Top = 0.0625F;
			this.Label13.Width = 5.25F;
			// 
			// Line7
			// 
			this.Line7.Height = 0F;
			this.Line7.Left = 5F;
			this.Line7.LineWeight = 1F;
			this.Line7.Name = "Line7";
			this.Line7.Top = 0.03125F;
			this.Line7.Width = 2.5F;
			this.Line7.X1 = 5F;
			this.Line7.X2 = 7.5F;
			this.Line7.Y1 = 0.03125F;
			this.Line7.Y2 = 0.03125F;
			// 
			// srptItemizedReim
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.Name = "ActiveReport1";
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5625F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYR)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMO)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQTY)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessmentTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOriginalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYR;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMO;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDesc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssess;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOriginal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQTY;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessmentTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOriginalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
