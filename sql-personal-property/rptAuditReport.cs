﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Core;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptAuditReport.
	/// </summary>
	public partial class rptAuditReport : BaseSectionReport
	{
		public rptAuditReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Audit Summary";
		}

		public static rptAuditReport InstancePtr
		{
			get
			{
				return (rptAuditReport)Sys.GetInstance(typeof(rptAuditReport));
			}
		}

		protected rptAuditReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptAuditReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// vbPorter upgrade warning: lngCategory1 As int	OnWrite(double, int)
		int lngCategory1;
		// vbPorter upgrade warning: lngCategory2 As int	OnWrite(double, int)
		int lngCategory2;
		// vbPorter upgrade warning: lngCategory3 As int	OnWrite(double, int)
		int lngCategory3;
		// vbPorter upgrade warning: lngCategory4 As int	OnWrite(double, int)
		int lngCategory4;
		// vbPorter upgrade warning: lngCategory5 As int	OnWrite(double, int)
		int lngCategory5;
		// vbPorter upgrade warning: lngCategory6 As int	OnWrite(double, int)
		int lngCategory6;
		// vbPorter upgrade warning: lngCategory7 As int	OnWrite(double, int)
		int lngCategory7;
		// vbPorter upgrade warning: lngCategory8 As int	OnWrite(double, int)
		int lngCategory8;
		// vbPorter upgrade warning: lngCategory9 As int	OnWrite(double, int)
		int lngCategory9;
		int intPageNumber;
		string strTemp;
		// vbPorter upgrade warning: lngTotal As int	OnWrite(int, double)
		int lngTotal;
		// vbPorter upgrade warning: lngBilling As int	OnWrite(short, double)
		int lngBilling;
		string strgroup = "";
		int intRecordCounter;
		bool boolExit;
		int lngCurrent;
		int lngCurrentExemption;
		int lngTotExemption;
		// vbPorter upgrade warning: lngOverRideAmount As int	OnWrite(double, int)
		int lngOverRideAmount;
		int lngOverRide;
		// vbPorter upgrade warning: lngTotalAssessment As int	OnWrite(double, int)
		int lngTotalAssessment;
		int lngSubPropertyTotal;
		// vbPorter upgrade warning: lngSubExemptionTotal As int	OnWrite(double, int)
		int lngSubExemptionTotal;
		// vbPorter upgrade warning: lngSubAssessmentTotal As int	OnWriteFCConvert.ToDouble(
		int lngSubAssessmentTotal;
		int lngHoldRound;
		double lngSubBilling;
		double lngTotalBilling;
		clsCategories clsCats = new clsCategories();
		double dblBETETot;
		double dblBETESub;
		double dblBETE;
		bool IsAfterReportStart = false;

		public void Init()
		{
			//FC:FINAL:MSH - I.Issue #820: initialize and show report
			frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "Audit");
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			if (modPPGN.Statics.boolShortScreenOnly)
			{
				// don't want to round the categories
				lngHoldRound = modGlobal.Statics.CustomizeStuff.Round;
				modGlobal.Statics.CustomizeStuff.Round = 0;
				// nearest 1
			}
			//FC:FINAL:CHN - issue #1368: Return missing report part.
			this.Fields.Add("GroupHeader1");
			GroupHeader1.DataField = "GroupHeader1";
			this.Fields["GroupHeader1"].Value = string.Empty;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			//FC:FINAL:MSH - issue #1479: code moved here, because in .NET version of GrapeCity will be called FetchData if we try to change value of the 
			// 'Fields["GroupHeader1"].Value' from 'ActiveReport_ReportStart' and all data will be calculated incorrectly
			if (IsAfterReportStart)
			{
				if (modGlobal.Statics.DetailSummary != "F" || modPPGN.Statics.Sequence != "A")
				{
					if (Strings.UCase(modPPGN.Statics.Sequence) == "N")
					{
						if (!modPrinting.Statics.rsAudit.EndOfFile())
							//FC:FINAL:CHN - issue #1368: Return missing report part.
							// this.GroupHeader1.DataField = Strings.Left(modPrinting.Statics.rsAudit.Get_Fields_String("Name") + " ", 1);
							this.Fields["GroupHeader1"].Value = Strings.Left(modPrinting.Statics.rsAudit.Get_Fields_String("Name") + " ", 1);
						strgroup = Strings.UCase(Strings.Left(modPrinting.Statics.rsAudit.Get_Fields_String("Name") + " ", 1));
					}
					else if (Strings.UCase(modPPGN.Statics.Sequence) == "L")
					{
						if (!modPrinting.Statics.rsAudit.EndOfFile())
								//FC:FINAL:CHN - issue #1368: Return missing report part.
								// this.GroupHeader1.DataField = Strings.Left(modPrinting.Statics.rsAudit.Get_Fields_String("street") + " ", 1);
								this.Fields["GroupHeader1"].Value = Strings.Left(modPrinting.Statics.rsAudit.Get_Fields_String("street") + " ", 1);
						strgroup = Strings.UCase(Strings.Left(modPrinting.Statics.rsAudit.Get_Fields_String("Street") + " ", 1));
					}
					else if (Strings.UCase(modPPGN.Statics.Sequence) == "1")
					{
						if (!modPrinting.Statics.rsAudit.EndOfFile())
									//FC:FINAL:CHN - issue #1368: Return missing report part.
									// this.GroupHeader1.DataField = Strings.Left(modPrinting.Statics.rsAudit.Get_Fields_String("open1") + " ", 1);
									this.Fields["GroupHeader1"].Value = Strings.Left(modPrinting.Statics.rsAudit.Get_Fields_String("open1") + " ", 1);
						strgroup = Strings.UCase(Strings.Left(modPrinting.Statics.rsAudit.Get_Fields_String("open1") + " ", 1));
					}
					else if (Strings.UCase(modPPGN.Statics.Sequence) == "2")
					{
						if (!modPrinting.Statics.rsAudit.EndOfFile())
										//FC:FINAL:CHN - issue #1368: Return missing report part.
										// this.GroupHeader1.DataField = Strings.Left(modPrinting.Statics.rsAudit.Get_Fields_String("open2") + " ", 1);
										this.Fields["GroupHeader1"].Value = Strings.Left(modPrinting.Statics.rsAudit.Get_Fields_String("open2") + " ", 1);
						strgroup = Strings.UCase(Strings.Left(modPrinting.Statics.rsAudit.Get_Fields_String("open2") + " ", 1));
					}
					else if (Strings.UCase(modPPGN.Statics.Sequence) == "T")
					{
						if (!modPrinting.Statics.rsAudit.EndOfFile())
											//FC:FINAL:CHN - issue #1368: Return missing report part.
											// TODO Get_Fields: Check the table for the column [trancode] and replace with corresponding Get_Field method
											// this.GroupHeader1.DataField = FCConvert.ToString(Conversion.Val(modPrinting.Statics.rsAudit.Get_Fields("trancode") + ""));
											this.Fields["GroupHeader1"].Value = FCConvert.ToString(Conversion.Val(modPrinting.Statics.rsAudit.Get_Fields("trancode") + ""));
						// TODO Get_Fields: Check the table for the column [trancode] and replace with corresponding Get_Field method
						strgroup = FCConvert.ToString(Conversion.Val(modPrinting.Statics.rsAudit.Get_Fields("trancode") + ""));
					}
				}
				else
				{
					this.GroupFooter1.Visible = false;
				}
				IsAfterReportStart = false;
			}
			if (modGlobal.Statics.DetailSummary == "F" && modPPGN.Statics.Sequence == "A")
			{
				// just show final details
				eArgs.EOF = true;
				return;
			}
			intRecordCounter += 1;
			if (modPPGN.Statics.Sequence == "N")
			{
				if (boolExit)
				{
					this.Detail.Visible = false;
					return;
				}
				if (modPrinting.Statics.rsAudit.EndOfFile())
				{
					strgroup = FCConvert.ToString(Convert.ToChar(Convert.ToByte(strgroup[0]) + 1));
					boolExit = true;
					eArgs.EOF = true;
					return;
				}
			}
			else
			{
				if (modPrinting.Statics.rsAudit.EndOfFile())
				{
					return;
				}
			}
			string strSumValue;
			string strSumBETE;
			strSumBETE = "";
			strSumValue = "";
			strTemp = "";
			lngTotal = 0;
			lngBilling = 0;
			lngOverRide = 0;
			dblBETE = 0;
			txtAccount.Text = Strings.Format(Conversion.Val(modPrinting.Statics.rsAudit.Get_Fields_Int32("ValueKey") + ""), "00000");
			int x;
			if (frmPPAuditBilling.InstancePtr.chkLabels.Checked)
			{
				txtName.Text = modPrinting.Statics.rsAudit.Get_Fields_String("Name") + "" + "\r\n";
				txtName.Text = txtName.Text + modPrinting.Statics.rsAudit.Get_Fields_String("Address1") + "" + "\r\n";
				txtName.Text = txtName.Text + Strings.Trim(modPrinting.Statics.rsAudit.Get_Fields_String("City") + "") + ", " + modPrinting.Statics.rsAudit.Get_Fields_String("State") + " " + Strings.Trim(modPrinting.Statics.rsAudit.Get_Fields_String("Zip") + "");
			}
			else
			{
				txtName.Text = modPrinting.Statics.rsAudit.Get_Fields_String("Name") + "";
			}
			if (Strings.Trim(modPrinting.Statics.rsAudit.Get_Fields_String("ORCODE") + "") == "Y")
			{
				lngOverRide = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Trim(modPrinting.Statics.rsAudit.Get_Fields_Int32("OverRideAmount") + " "))));
			}
			else
			{
				lngOverRide = 0;
			}
			// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
			lngBilling = FCConvert.ToInt32(Conversion.Val(modPrinting.Statics.rsAudit.Get_Fields("value") + "") - Conversion.Val(Strings.Trim(modPrinting.Statics.rsAudit.Get_Fields_Int32("Exemption") + "")));
			txtBilling.Text = Strings.Format(lngBilling, "#,###,###,##0");
			// If Val(Trim(rsAudit.Fields("Category1") & " ")) <> 0 Then
			// Call rs.FindFirst("Type = 1")
			// strTemp = clsCats.CategoryDescription(1) & " - " & Format(Trim(rsAudit.Fields("Category1") & " "), "###,###,##0") & vbCrLf
			// If Trim(rsAudit.Fields("ORCODE") & "") <> "Y" Then
			// lngTotal = lngTotal + Val(Trim(rsAudit.Fields("Category1") & " "))
			// lngCategory1 = lngCategory1 + Round(Val(Trim(rsAudit.Fields("Category1") & " ")), PPRounding)
			// 
			// End If
			// End If
			double dblTemp = 0;
			double dblVal = 0;
			for (x = 1; x <= 9; x++)
			{
				// TODO Get_Fields: Field [cat] not found!! (maybe it is an alias?)
				dblTemp = Conversion.Val(modPrinting.Statics.rsAudit.Get_Fields("cat" + FCConvert.ToString(x) + "exempt"));
				// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
				dblVal = Conversion.Val(modPrinting.Statics.rsAudit.Get_Fields("category" + FCConvert.ToString(x)));
				if (dblVal != 0 || dblTemp != 0)
				{
					strTemp += clsCats.Get_CategoryDescription(x) + "\r\n";
					if (dblVal != 0)
					{
						strSumValue += Strings.Format(dblVal, "#,###,###,##0") + "\r\n";
						if (Strings.Trim(modPrinting.Statics.rsAudit.Get_Fields_String("orcode") + "") != "Y")
						{
							lngTotal += FCConvert.ToInt32(dblVal);
							switch (x)
							{
								case 1:
									{
										lngCategory1 += FCConvert.ToInt32(modGlobal.Round(dblVal, modPPCalculate.Statics.PPRounding));
										break;
									}
								case 2:
									{
										lngCategory2 += FCConvert.ToInt32(modGlobal.Round(dblVal, modPPCalculate.Statics.PPRounding));
										break;
									}
								case 3:
									{
										lngCategory3 += FCConvert.ToInt32(modGlobal.Round(dblVal, modPPCalculate.Statics.PPRounding));
										break;
									}
								case 4:
									{
										lngCategory4 += FCConvert.ToInt32(modGlobal.Round(dblVal, modPPCalculate.Statics.PPRounding));
										break;
									}
								case 5:
									{
										lngCategory5 += FCConvert.ToInt32(modGlobal.Round(dblVal, modPPCalculate.Statics.PPRounding));
										break;
									}
								case 6:
									{
										lngCategory6 += FCConvert.ToInt32(modGlobal.Round(dblVal, modPPCalculate.Statics.PPRounding));
										break;
									}
								case 7:
									{
										lngCategory7 += FCConvert.ToInt32(modGlobal.Round(dblVal, modPPCalculate.Statics.PPRounding));
										break;
									}
								case 8:
									{
										lngCategory8 += FCConvert.ToInt32(modGlobal.Round(dblVal, modPPCalculate.Statics.PPRounding));
										break;
									}
								case 9:
									{
										lngCategory9 += FCConvert.ToInt32(modGlobal.Round(dblVal, modPPCalculate.Statics.PPRounding));
										break;
									}
							}
							//end switch
						}
					}
					else
					{
						strSumValue += "\r\n";
					}
					if (dblTemp != 0)
					{
						dblBETETot += dblTemp;
						dblBETE += dblTemp;
						clsCats.Set_BETEValue(x, clsCats.Get_BETEValue(x) + modGlobal.Round(dblTemp, modPPCalculate.Statics.PPRounding));
						// strTemp = strTemp & clsCats.CategoryDescription(x) & " BETE" & vbNewLine
						// strSumValue = strSumValue & Format(dblTemp, "###,###,##0") & vbNewLine
						strSumBETE += Strings.Format(dblTemp, "#,###,###,##0") + "\r\n";
					}
					else
					{
						strSumBETE += "\r\n";
					}
				}
			}
			// x
			// If Val(Trim(rsAudit.Fields("Category2") & " ")) <> 0 Then
			// 
			// strTemp = strTemp & clsCats.CategoryDescription(2) & " - " & Format(Trim(rsAudit.Fields("Category2") & " "), "###,###,##0") & vbCrLf
			// If Trim(rsAudit.Fields("ORCODE") & "") <> "Y" Then
			// lngTotal = lngTotal + Val(Trim(rsAudit.Fields("Category2") & " "))
			// lngCategory2 = lngCategory2 + Round(Val(Trim(rsAudit.Fields("Category2") & " ")), PPRounding)
			// 
			// End If
			// End If
			// If Trim(rsAudit.Fields("Category3") & " ") <> vbNullString And Val(Trim(rsAudit.Fields("Category3") & " ")) <> 0 Then
			// 
			// strTemp = strTemp & clsCats.CategoryDescription(3) & " - " & Format(Trim(rsAudit.Fields("Category3") & " "), "###,###,##0") & vbCrLf
			// If Trim(rsAudit.Fields("ORCODE") & "") <> "Y" Then
			// lngTotal = lngTotal + Val(Trim(rsAudit.Fields("Category3") & " "))
			// lngCategory3 = lngCategory3 + Round(Val(Trim(rsAudit.Fields("Category3") & " ")), PPRounding)
			// 
			// End If
			// End If
			// If Trim(rsAudit.Fields("Category4") & " ") <> vbNullString And Val(Trim(rsAudit.Fields("Category4") & " ")) <> 0 Then
			// 
			// strTemp = strTemp & clsCats.CategoryDescription(4) & " - " & Format(Trim(rsAudit.Fields("Category4") & " "), "###,###,###") & vbCrLf
			// If Trim(rsAudit.Fields("ORCODE") & "") <> "Y" Then
			// lngTotal = lngTotal + Val(Trim(rsAudit.Fields("Category4") & " "))
			// lngCategory4 = lngCategory4 + Round(Val(Trim(rsAudit.Fields("Category4") & " ")), PPRounding)
			// 
			// End If
			// End If
			// If Trim(rsAudit.Fields("Category5") & " ") <> vbNullString And Val(Trim(rsAudit.Fields("Category5") & " ")) <> 0 Then
			// 
			// strTemp = strTemp & clsCats.CategoryDescription(5) & " - " & Format(Trim(rsAudit.Fields("Category5") & " "), "###,###,###") & vbCrLf
			// If Trim(rsAudit.Fields("ORCODE") & "") <> "Y" Then
			// lngTotal = lngTotal + Val(Trim(rsAudit.Fields("Category5") & " "))
			// lngCategory5 = lngCategory5 + Round(Val(Trim(rsAudit.Fields("Category5") & " ")), PPRounding)
			// 
			// End If
			// End If
			// If Trim(rsAudit.Fields("Category6") & " ") <> vbNullString And Val(Trim(rsAudit.Fields("Category6") & " ")) <> 0 Then
			// 
			// strTemp = strTemp & clsCats.CategoryDescription(6) & " - " & Format(Trim(rsAudit.Fields("Category6") & " "), "###,###,###") & vbCrLf
			// If Trim(rsAudit.Fields("ORCODE") & "") <> "Y" Then
			// lngTotal = lngTotal + Val(Trim(rsAudit.Fields("Category6") & " "))
			// lngCategory6 = lngCategory6 + Round(Val(Trim(rsAudit.Fields("Category6") & " ")), PPRounding)
			// 
			// End If
			// End If
			// If Trim(rsAudit.Fields("Category7") & " ") <> vbNullString And Val(Trim(rsAudit.Fields("Category7") & " ")) <> 0 Then
			// 
			// strTemp = strTemp & clsCats.CategoryDescription(7) & " - " & Format(Trim(rsAudit.Fields("Category7") & " "), "###,###,###") & vbCrLf
			// If Trim(rsAudit.Fields("ORCODE") & "") <> "Y" Then
			// lngTotal = lngTotal + Val(Trim(rsAudit.Fields("Category7") & " "))
			// lngCategory7 = lngCategory7 + Round(Val(Trim(rsAudit.Fields("Category7") & " ")), PPRounding)
			// 
			// End If
			// End If
			// If Trim(rsAudit.Fields("Category8") & " ") <> vbNullString And Val(Trim(rsAudit.Fields("Category8") & " ")) <> 0 Then
			// 
			// strTemp = strTemp & clsCats.CategoryDescription(8) & " - " & Format(Trim(rsAudit.Fields("Category8") & " "), "###,###,###") & vbCrLf
			// If Trim(rsAudit.Fields("ORCODE") & "") <> "Y" Then
			// lngTotal = lngTotal + Val(Trim(rsAudit.Fields("Category8") & " "))
			// lngCategory8 = lngCategory8 + Round(Val(Trim(rsAudit.Fields("Category8") & " ")), PPRounding)
			// 
			// End If
			// End If
			// If Trim(rsAudit.Fields("Category9") & " ") <> vbNullString And Val(Trim(rsAudit.Fields("Category9") & " ")) <> 0 Then
			// 
			// strTemp = strTemp & clsCats.CategoryDescription(9) & " - " & Format(Trim(rsAudit.Fields("Category9") & " "), "###,###,###") & vbCrLf
			// If Trim(rsAudit.Fields("ORCODE") & "") <> "Y" Then
			// lngTotal = lngTotal + Val(Trim(rsAudit.Fields("Category9") & " "))
			// lngCategory9 = lngCategory9 + Round(Val(Trim(rsAudit.Fields("Category9") & " ")), PPRounding)
			// End If
			// End If
			// lngTotal = Val(rsAudit.Fields("value") & "")
			// 
			// If lngOverRide <> 0 Then
			// lngCategory1 = lngCategory1 + lngOverRide
			// End If
			if (Strings.Trim(modPrinting.Statics.rsAudit.Get_Fields_String("ORCODE") + "") == "Y")
			{
				lngOverRideAmount += FCConvert.ToInt32(Conversion.Val(Strings.Trim(modPrinting.Statics.rsAudit.Get_Fields_Int32("OverRideAmount") + "")));
			}
			else
			{
			}
			lngCurrent = lngTotal;
			txtSummary.Text = strTemp;
			txtSummaryBETE.Text = strSumBETE;
			txtSummaryValue.Text = strSumValue;
			if (modGlobal.Statics.BreakDown == "Y")
			{
				txtSummary.Visible = true;
				txtSummaryBETE.Visible = true;
				txtSummaryValue.Visible = true;
				// lblBETE.Visible = True
				// lblProperty.Visible = True
			}
			else
			{
				txtSummary.Visible = false;
				txtSummaryValue.Visible = false;
				txtSummaryBETE.Visible = false;
				// lblBETE.Visible = False
				// lblProperty.Visible = False
			}
			if (Strings.UCase(modPPGN.Statics.Sequence) == "N")
			{
				strgroup = Strings.UCase(Strings.Left(modPrinting.Statics.rsAudit.Get_Fields_String("Name") + " ", 1));
			}
			else if (Strings.UCase(modPPGN.Statics.Sequence) == "L")
			{
				strgroup = Strings.UCase(Strings.Left(modPrinting.Statics.rsAudit.Get_Fields_String("street") + " ", 1));
			}
			else if (Strings.UCase(modPPGN.Statics.Sequence) == "1")
			{
				strgroup = Strings.UCase(Strings.Left(modPrinting.Statics.rsAudit.Get_Fields_String("open1") + " ", 1));
			}
			else if (Strings.UCase(modPPGN.Statics.Sequence) == "2")
			{
				strgroup = Strings.UCase(Strings.Left(modPrinting.Statics.rsAudit.Get_Fields_String("open2") + " ", 1));
			}
			else if (Strings.UCase(modPPGN.Statics.Sequence) == "T")
			{
				// TODO Get_Fields: Check the table for the column [trancode] and replace with corresponding Get_Field method
				strgroup = FCConvert.ToString(Conversion.Val(modPrinting.Statics.rsAudit.Get_Fields("trancode") + ""));
			}
			//FC:FINAL:CHN - issue #1368: Return missing report part.
			// if (strgroup != this.GroupHeader1.DataField)
			if (strgroup != FCConvert.ToString(this.Fields["GroupHeader1"].Value))
			{
				txtSubPropertyTotal.Text = Strings.Format(lngSubPropertyTotal, "#,###,###,##0");
				txtSubExemptionTotal.Text = Strings.Format(lngSubExemptionTotal, "###,###,##0");
				txtSubAssessmentTotal.Text = Strings.Format(lngSubAssessmentTotal, "#,###,###,###,##0");
				txtSubBETE.Text = Strings.Format(dblBETESub, "#,###,###,##0");
			}
			txtProperty.Text = Strings.Format(lngTotal, "###,###,##0");
			txtBETE.Text = Strings.Format(dblBETE, "#,###,###,##0");
			txtBETETotal.Text = Strings.Format(dblBETETot, "#,###,###,##0");
			txtPropertyTotal.Text = Strings.Format(Conversion.Val(Strings.Format(txtPropertyTotal.Text, "#########")) + lngTotal, "###,###,##0");
			// txtSubPropertyTotal.Text = Format(Val(Format(txtSubPropertyTotal.Text, "#########")) + lngTotal, "###,###,##0")
			if (FCConvert.ToString(modPrinting.Statics.rsAudit.Get_Fields_String("orcode")) == "Y")
			{
				txtSubOverRideTotal.Text = Strings.Format(lngOverRide, "###,###,##0");
			}
			else
			{
				txtSubOverRideTotal.Text = "";
			}
			txtOverRideTotal.Text = Strings.Format(Conversion.Val(Strings.Format(txtOverRideTotal.Text, "#########")) + lngOverRide, "###,###,##0");
			lngCurrentExemption = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Trim(modPrinting.Statics.rsAudit.Get_Fields_Int32("Exemption") + " "))));
			lngTotExemption += lngCurrentExemption;
			txtExemption.Text = Strings.Format(Conversion.Val(modPrinting.Statics.rsAudit.Get_Fields_Int32("Exemption") + ""), "###,###,##0");
			txtExemptionTotal.Text = Strings.Format(Conversion.Val(Strings.Format(txtExemptionTotal.Text, "#########")) + Conversion.Val(modPrinting.Statics.rsAudit.Get_Fields_Int32("Exemption") + ""), "###,###,##0");
			// txtSubExemptionTotal = Format(Val(Format(txtSubExemptionTotal, "#########")) + Val(rsAudit.Fields("Exemption") & ""), "###,###,##0")
			if (FCConvert.ToString(modPrinting.Statics.rsAudit.Get_Fields_String("orcode")) == "Y")
			{
				txtAssessment.Text = Strings.Format(lngOverRide - Conversion.Val(modPrinting.Statics.rsAudit.Get_Fields_Int32("exemption") + ""), "###,###,##0");
				txtAssessmentTotal.Text = Strings.Format(Conversion.Val(Strings.Format(txtAssessmentTotal.Text, "#########")) + lngOverRide - Conversion.Val(Strings.Trim(modPrinting.Statics.rsAudit.Get_Fields_Int32("Exemption") + " ")), "###,###,##0");
				// txtSubAssessmentTotal = Format(Val(Format(txtSubAssessmentTotal, "#########")) + Val(lngOverRide) - Val(Trim(rsAudit.Fields("Exemption") & " ")), "###,###,##0")
				lngTotalAssessment += (lngOverRide - modPrinting.Statics.rsAudit.Get_Fields_Int32("Exemption"));
			}
			else
			{
				txtAssessment.Text = Strings.Format(lngTotal - Conversion.Val(Strings.Trim(modPrinting.Statics.rsAudit.Get_Fields_Int32("Exemption") + " ")), "###,###,##0");
				txtAssessmentTotal.Text = Strings.Format(Conversion.Val(Strings.Format(txtAssessmentTotal.Text, "#########")) + lngTotal - Conversion.Val(Strings.Trim(modPrinting.Statics.rsAudit.Get_Fields_Int32("Exemption") + " ")), "###,###,##0");
				// txtSubAssessmentTotal = Format(Val(Format(txtSubAssessmentTotal, "#########")) + Val(lngTotal) - Val(Trim(rsAudit.Fields("Exemption") & " ")), "###,###,##0")
				lngTotalAssessment += (lngTotal - modPrinting.Statics.rsAudit.Get_Fields_Int32("Exemption"));
			}
			//FC:FINAL:CHN - issue #1368: Return missing report part.
			// if (strgroup == this.GroupHeader1.DataField)
			if (strgroup == FCConvert.ToString(this.Fields["GroupHeader1"].Value))
			{
				lngSubPropertyTotal += lngTotal;
				txtSubPropertyTotal.Text = Strings.Format(lngSubPropertyTotal, "#,###,###,##0");
				lngSubExemptionTotal += modPrinting.Statics.rsAudit.Get_Fields_Int32("Exemption");
				txtSubExemptionTotal.Text = Strings.Format(lngSubExemptionTotal, "###,###,##0");
				dblBETESub += dblBETE;
				txtSubBETE.Text = Strings.Format(dblBETESub, "#,###,###,##0");
				if (FCConvert.ToString(modPrinting.Statics.rsAudit.Get_Fields_String("orcode")) == "Y")
				{
					lngSubAssessmentTotal += lngOverRide - modPrinting.Statics.rsAudit.Get_Fields_Int32("exemption");
				}
				else
				{
					lngSubAssessmentTotal += lngTotal - modPrinting.Statics.rsAudit.Get_Fields_Int32("Exemption");
				}
				txtSubAssessmentTotal.Text = Strings.Format(lngSubAssessmentTotal, "#,###,###,###,##0");
				lngSubBilling += lngBilling;
				txtSubBilling.Text = Strings.Format(lngSubBilling, "#,###,###,##0");
			}
			else
			{
				lngSubPropertyTotal = lngTotal;
				lngSubExemptionTotal = FCConvert.ToInt32(Math.Round(Conversion.Val(modPrinting.Statics.rsAudit.Get_Fields_Int32("Exemption") + "")));
				if (FCConvert.ToString(modPrinting.Statics.rsAudit.Get_Fields_String("orcode")) == "Y")
				{
					lngSubAssessmentTotal = FCConvert.ToInt32(lngOverRide - Conversion.Val(Strings.Trim(modPrinting.Statics.rsAudit.Get_Fields_Int32("exemption") + "")));
				}
				else
				{
					lngSubAssessmentTotal = FCConvert.ToInt32(lngTotal - Conversion.Val(Strings.Trim(modPrinting.Statics.rsAudit.Get_Fields_Int32("Exemption") + "")));
				}
				dblBETESub = dblBETE;
				// txtSubBETE.Text = Format(dblBETESub, "#,###,###,##0")
				lngSubBilling = lngBilling;
			}
			lngTotalBilling += lngBilling;
			txtTotalBilling.Text = Strings.Format(lngTotalBilling, "#,###,###,##0");
			this.Detail.Visible = modGlobal.Statics.DetailSummary != "F";
			// Select Case UCase(Sequence)
			// Case "N"
			// strGroup = Left(rsAudit.Fields("Name").Value & " ", 1)
			// Case "L"
			// strGroup = Left(rsAudit.Fields("street").Value & " ", 1)
			// Case "1"
			// strGroup = Left(rsAudit.Fields("open1").Value & " ", 1)
			// Case "2"
			// strGroup = Left(rsAudit.Fields("open2").Value & " ", 1)
			// End Select
			if (!modPrinting.Statics.rsAudit.EndOfFile())
				modPrinting.Statics.rsAudit.MoveNext();
			eArgs.EOF = false;
			//FC:FINAL:DDU:#1480 - moved code here from Detail_Format
			if (!modPrinting.Statics.rsAudit.EndOfFile())
			{
				//FC:FINAL:CHN - issue #1368: Return missing report part.
				// this.GroupHeader1.DataField = strgroup;
				if (this.Fields["GroupHeader1"].Value.ToString() != strgroup)
				{
					this.Fields["GroupHeader1"].Value = strgroup;
				}
			}
		}

		private void ActiveReport_PageEnd(object sender, EventArgs e)
		{
			intPageNumber += 1;
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			txtPage.Text = "Page #" + FCConvert.ToString(intPageNumber);
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			if (modPPGN.Statics.boolShortScreenOnly)
			{
				// set it back
				modGlobal.Statics.CustomizeStuff.Round = FCConvert.ToInt16(lngHoldRound);
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtTown.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intPageNumber = 1;
			lngTotExemption = 0;
			lngTotal = 0;
			lngCategory1 = 0;
			lngCategory2 = 0;
			lngCategory3 = 0;
			lngCategory4 = 0;
			lngCategory5 = 0;
			lngCategory6 = 0;
			lngCategory7 = 0;
			lngCategory8 = 0;
			lngCategory9 = 0;
			lngSubBilling = 0;
			lngTotalBilling = 0;
			clsCats.Clear();
			
			if (modPPGN.Statics.boolShortScreenOnly)
			{
				Field12.Visible = false;
				txtBilling.Visible = false;
				Field14.Visible = false;
				txtSubBilling.Visible = false;
				txtTotalBilling.Visible = false;
				Field16.Visible = false;
			}
			else
			{
				Field12.Visible = true;
				txtBilling.Visible = true;
				Field14.Visible = true;
				txtSubBilling.Visible = true;
				txtTotalBilling.Visible = true;
				Field16.Visible = true;
			}
			IsAfterReportStart = true;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			//FC:FINAL:DDU:#1480 - moved code to FetchData
			//if (!modPrinting.Statics.rsAudit.EndOfFile())
			//    //FC:FINAL:CHN - issue #1368: Return missing report part.
			//    // this.GroupHeader1.DataField = strgroup;
			//    this.Fields["GroupHeader1"].Value = strgroup;
			if (Strings.Trim(txtSummary.Text) == string.Empty || modGlobal.Statics.BreakDown != "Y")
			{
				// Field11.Top = txtName.Top
				Field11.Top = txtSummary.Top;
				txtProperty.Top = Field11.Top;
				txtExemption.Top = Field11.Top;
				txtAssessment.Top = Field11.Top;
				txtBETE.Top = Field11.Top;
				Field10.Top = Field11.Top + 270 / 1440f;
				txtSubOverRideTotal.Top = Field10.Top;
				Field12.Top = Field10.Top;
				txtBilling.Top = Field10.Top;
			}
			else
			{
				// Field11.Top = 540
				Field11.Top = 630 / 1440f;
				txtProperty.Top = Field11.Top;
				txtExemption.Top = Field11.Top;
				txtAssessment.Top = Field11.Top;
				txtBETE.Top = Field11.Top;
				Field10.Top = Field11.Top + 270 / 1440f;
				txtSubOverRideTotal.Top = Field10.Top;
				Field12.Top = Field10.Top;
				txtBilling.Top = Field10.Top;
			}
			if (Strings.Trim(txtSubOverRideTotal.Text) == string.Empty)
			{
				txtSubOverRideTotal.Visible = false;
				Field10.Visible = false;
			}
			else
			{
				txtSubOverRideTotal.Visible = true;
				Field10.Visible = true;
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			if (!modPrinting.Statics.rsAudit.EndOfFile())
			{
				// txtSubPropertyTotal = Format(CLng(Format(txtSubPropertyTotal.Text & "", "#########0")) - lngCurrent, "###,###,###,##0")
				// txtSubExemptionTotal = CLng(Format(txtSubExemptionTotal, "##########0")) - lngCurrentExemption
				// txtSubAssessmentTotal = Format(CLng(Format(txtSubAssessmentTotal.Text, "########0")) - Val(lngCurrent) + Val(lngCurrentExemption), "###,###,##0")
				// 
				// If Not Trim(txtSubOverRideTotal.Text) = vbNullString Then
				// txtSubOverRideTotal.Text = Format(CLng(Format(txtSubOverRideTotal.Text & "", "########0")) - lngOverRide, "###,###,##0")
				// Else
				// txtSubOverRideTotal.Text = Format(-lngOverRide, "###,###,##0")
				// End If
			}
			this.GroupFooter1.Visible = ((modPPGN.Statics.Sequence == "N" || modPPGN.Statics.Sequence == "L" || modPPGN.Statics.Sequence == "1" || modPPGN.Statics.Sequence == "2" || modPPGN.Statics.Sequence == "T") && (modGlobal.Statics.DetailSummary == "B" || modGlobal.Statics.DetailSummary == "F"));
			// If strGroup <> vbNullString Then
			// If strGroup <> " " And strGroup <> "A" Then
			// txtGroupCaption.Text = "Totals " & Chr(34) & strGroup & Chr(34)
			// Else
			// txtGroupCaption.Text = "Totals " & Chr(34) & " " & Chr(34)
			// End If
			// 
			// End If
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			if (intRecordCounter != 1)
			{
				txtSubPropertyTotal.Text = FCConvert.ToString(lngCurrent);
				txtSubExemptionTotal.Text = FCConvert.ToString(lngCurrentExemption);
				// txtSubAssessmentTotal = CLng(Format(Val(txtSubAssessmentTotal.Text), "########0")) + Val(lngCurrent) - Val(lngCurrentExemption)
				txtSubAssessmentTotal.Text = FCConvert.ToString(lngCurrent - lngCurrentExemption);
				txtSubOverRideTotal.Text = Strings.Format(lngOverRide, "###,###,###");
				txtSubBETE.Text = Strings.Format(dblBETESub, "#,###,###,##0");
			}
			// If strGroup <> vbNullString Then
			// If strGroup <> " " And strGroup <> "A" Then
			txtGroupCaption.Text = "Totals " + FCConvert.ToString(Convert.ToChar(34)) + strgroup + FCConvert.ToString(Convert.ToChar(34));
			// Else
			// txtGroupCaption.Text = "Totals " & Chr(34) & " " & Chr(34)
			// End If
			// End If
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			bool boolShowCat = false;
			txtCatTotals.Text = string.Empty;
			txtCatCaptions.Text = string.Empty;
			if (modGlobal.Statics.DetailSummary == "F" && modPPGN.Statics.Sequence == "A")
			{
                using (clsDRWrapper rsaudits = new clsDRWrapper())
                {
                    string strSQL = "";
                    string strOVSQL = "";
                    string strTemp = "";
                    // TODO Get_Fields: Field [cat1] not found!! (maybe it is an alias?)
                    lngCategory1 =
                        FCConvert.ToInt32(Math.Round(Conversion.Val(modPrinting.Statics.rsAudit.Get_Fields("cat1"))));
                    // TODO Get_Fields: Field [cat2] not found!! (maybe it is an alias?)
                    lngCategory2 =
                        FCConvert.ToInt32(Math.Round(Conversion.Val(modPrinting.Statics.rsAudit.Get_Fields("cat2"))));
                    // TODO Get_Fields: Field [cat3] not found!! (maybe it is an alias?)
                    lngCategory3 =
                        FCConvert.ToInt32(Math.Round(Conversion.Val(modPrinting.Statics.rsAudit.Get_Fields("cat3"))));
                    // TODO Get_Fields: Field [cat4] not found!! (maybe it is an alias?)
                    lngCategory4 =
                        FCConvert.ToInt32(Math.Round(Conversion.Val(modPrinting.Statics.rsAudit.Get_Fields("cat4"))));
                    // TODO Get_Fields: Field [cat5] not found!! (maybe it is an alias?)
                    lngCategory5 =
                        FCConvert.ToInt32(Math.Round(Conversion.Val(modPrinting.Statics.rsAudit.Get_Fields("cat5"))));
                    // TODO Get_Fields: Field [cat6] not found!! (maybe it is an alias?)
                    lngCategory6 =
                        FCConvert.ToInt32(Math.Round(Conversion.Val(modPrinting.Statics.rsAudit.Get_Fields("cat6"))));
                    // TODO Get_Fields: Field [cat7] not found!! (maybe it is an alias?)
                    lngCategory7 =
                        FCConvert.ToInt32(Math.Round(Conversion.Val(modPrinting.Statics.rsAudit.Get_Fields("cat7"))));
                    // TODO Get_Fields: Field [cat8] not found!! (maybe it is an alias?)
                    lngCategory8 =
                        FCConvert.ToInt32(Math.Round(Conversion.Val(modPrinting.Statics.rsAudit.Get_Fields("cat8"))));
                    // TODO Get_Fields: Field [cat9] not found!! (maybe it is an alias?)
                    lngCategory9 =
                        FCConvert.ToInt32(Math.Round(Conversion.Val(modPrinting.Statics.rsAudit.Get_Fields("cat9"))));
                    int x;
                    for (x = 1; x <= 9; x++)
                    {
                        // TODO Get_Fields: Field [cat] not found!! (maybe it is an alias?)
                        clsCats.Set_BETEValue(x,
                            FCConvert.ToInt32(Math.Round(Conversion.Val(
                                modPrinting.Statics.rsAudit.Get_Fields("cat" + FCConvert.ToString(x) + "exempttot")))));
                    }

                    // x
                    string strMasterJoinJoin = "";
                    strMasterJoinJoin = modPPGN.GetMasterJoinForJoin();
                    // strSQL = "select sum(exemption) as exemptsum from ppmaster where not deleted = 1 "
                    strSQL = "select sum(exemption) as exemptsum from " + strMasterJoinJoin + " where not deleted = 1 ";
                    strTemp = "";
                    if (modPPGN.Statics.gstrMinAccountRange != "" && modPPGN.Statics.gstrMaxAccountRange != "")
                    {
                        if (modPPGN.Statics.Sequence == "A")
                        {
                            // account
                            strTemp = " and account between " + modPPGN.Statics.gstrMinAccountRange + " and " +
                                      modPPGN.Statics.gstrMaxAccountRange;
                        }
                        else if (modPPGN.Statics.Sequence == "N")
                        {
                            // name
                            strTemp = " and name between '" + modPPGN.Statics.gstrMinAccountRange + "' and '" +
                                      modPPGN.Statics.gstrMaxAccountRange + "'";
                        }
                        else if (modPPGN.Statics.Sequence == "L")
                        {
                            // location
                            strTemp = " and street between '" + modPPGN.Statics.gstrMinAccountRange + "' and '" +
                                      modPPGN.Statics.gstrMaxAccountRange + "'";
                        }
                        else if (modPPGN.Statics.Sequence == "1")
                        {
                            // open1
                            strTemp = " and open1 between '" + modPPGN.Statics.gstrMinAccountRange + "' and '" +
                                      modPPGN.Statics.gstrMaxAccountRange + "'";
                        }
                        else if (modPPGN.Statics.Sequence == "2")
                        {
                            // open2
                            strTemp = " and open2 between '" + modPPGN.Statics.gstrMinAccountRange + "' and '" +
                                      modPPGN.Statics.gstrMaxAccountRange + "'";
                        }
                        else if (modPPGN.Statics.Sequence == "T")
                        {
                            strTemp = " and trancode between '" + modPPGN.Statics.gstrMinAccountRange + "' and '" +
                                      modPPGN.Statics.gstrMaxAccountRange + "'";
                        }
                    }

                    rsaudits.OpenRecordset(strSQL + strTemp, "twpp0000.vb1");
                    // TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
                    lngTotExemption = FCConvert.ToInt32(Math.Round(Conversion.Val(rsaudits.Get_Fields("exemptsum"))));
                    strSQL =
                        "select sum(overrideamount)as overamount from ppmaster inner join ppvaluations on ppmaster.account = ppvaluations.valuekey where not deleted = 1 and orcode  = 'Y'";
                    rsaudits.OpenRecordset(strSQL + strTemp, "twpp0000.vb1");
                    if (!rsaudits.EndOfFile())
                    {
                        // TODO Get_Fields: Field [overamount] not found!! (maybe it is an alias?)
                        lngOverRideAmount =
                            FCConvert.ToInt32(Math.Round(Conversion.Val(rsaudits.Get_Fields("overamount"))));
                    }
                    else
                    {
                        lngOverRideAmount = 0;
                    }

                    strSQL =
                        "select sum([value]) as totval from ppmaster inner join ppvaluations on ppmaster.account = ppvaluations.valuekey where not deleted = 1";
                    rsaudits.OpenRecordset(strSQL + strTemp, "twpp0000.vb1");
                    if (!rsaudits.EndOfFile())
                    {
                        // TODO Get_Fields: Field [totval] not found!! (maybe it is an alias?)
                        lngTotalBilling = Conversion.Val(rsaudits.Get_Fields("totval"));
                    }
                    else
                    {
                        lngTotalBilling = 0;
                    }

                    lngTotalBilling -= lngTotExemption;
                    strSQL =
                        "select  sum(cat1exempt + cat2exempt + cat3exempt + cat4exempt + cat5exempt + cat6exempt + cat7exempt + cat8exempt + cat9exempt) as betesum,sum(category1 + category2 + category3 + category4 + category5 + category6 + category7 + category8 + category9) as catsum from ppmaster inner join ppvaluations on ppmaster.account = ppvaluations.valuekey where not deleted = 1 and orcode  <> 'Y' ";
                    rsaudits.OpenRecordset(strSQL + strTemp, "twpp0000.vb1");
                    // TODO Get_Fields: Field [catsum] not found!! (maybe it is an alias?)
                    lngTotal = FCConvert.ToInt32(Math.Round(Conversion.Val(rsaudits.Get_Fields("catsum"))));
                    // TODO Get_Fields: Field [betesum] not found!! (maybe it is an alias?)
                    dblBETETot = Conversion.Val(rsaudits.Get_Fields("betesum"));
                    txtBETETotal.Text = Strings.Format(dblBETETot, "#,###,###,##0");
                    txtExemptionTotal.Text = Strings.Format(lngTotExemption, "#,###,###,##0");
                    txtOverRideTotal.Text = Strings.Format(lngOverRideAmount, "#,###,###,##0");
                    txtPropertyTotal.Text = Strings.Format(lngTotal, "#,###,###,##0");
                    // txtAssessmentTotal.Text = Format(lngTotal + lngOverRideAmount - lngTotExemption, "#,###,###,##0")
                    lngTotalAssessment = lngTotal + lngOverRideAmount - lngTotExemption;
                    txtAssessmentTotal.Text = Strings.Format(lngTotalAssessment, "#,###,###,##0");
                    txtTotalBilling.Text = Strings.Format(lngTotalBilling, "#,###,###,##0");
                }
            }
			
			int intCounter;
			for (intCounter = 1; intCounter <= 9; intCounter++)
			{
				boolShowCat = false;
				switch (intCounter)
				{
					case 1:
						{
							if (lngCategory1 > 0)
							{
								txtCatTotals.Text = txtCatTotals.Text + Strings.Format(lngCategory1, "###,###,##0") + "\r\n";
								boolShowCat = true;
							}
							break;
						}
					case 2:
						{
							if (lngCategory2 > 0)
							{
								txtCatTotals.Text = txtCatTotals.Text + Strings.Format(lngCategory2, "###,###,##0") + "\r\n";
								boolShowCat = true;
							}
							break;
						}
					case 3:
						{
							if (lngCategory3 > 0)
							{
								txtCatTotals.Text = txtCatTotals.Text + Strings.Format(lngCategory3, "###,###,##0") + "\r\n";
								boolShowCat = true;
							}
							break;
						}
					case 4:
						{
							if (lngCategory4 > 0)
							{
								txtCatTotals.Text = txtCatTotals.Text + Strings.Format(lngCategory4, "###,###,##0") + "\r\n";
								boolShowCat = true;
							}
							break;
						}
					case 5:
						{
							if (lngCategory5 > 0)
							{
								txtCatTotals.Text = txtCatTotals.Text + Strings.Format(lngCategory5, "###,###,##0") + "\r\n";
								boolShowCat = true;
							}
							break;
						}
					case 6:
						{
							if (lngCategory6 > 0)
							{
								txtCatTotals.Text = txtCatTotals.Text + Strings.Format(lngCategory6, "###,###,##0") + "\r\n";
								boolShowCat = true;
							}
							break;
						}
					case 7:
						{
							if (lngCategory7 > 0)
							{
								txtCatTotals.Text = txtCatTotals.Text + Strings.Format(lngCategory7, "###,###,##0") + "\r\n";
								boolShowCat = true;
							}
							break;
						}
					case 8:
						{
							if (lngCategory8 > 0)
							{
								txtCatTotals.Text = txtCatTotals.Text + Strings.Format(lngCategory8, "###,###,##0") + "\r\n";
								boolShowCat = true;
							}
							break;
						}
					case 9:
						{
							if (lngCategory9 > 0)
							{
								txtCatTotals.Text = txtCatTotals.Text + Strings.Format(lngCategory9, "###,###,##0") + "\r\n";
								boolShowCat = true;
							}
							break;
						}
				}
				//end switch
				if (boolShowCat)
				{
					// Call rs.FindFirst("Type = " & intCounter)
					txtCatCaptions.Text = txtCatCaptions.Text + clsCats.Get_CategoryDescription(intCounter) + "\r\n";
				}
			}
			// intCounter
			bool boolFirst;
			boolFirst = true;
			for (intCounter = 1; intCounter <= 9; intCounter++)
			{
				boolShowCat = false;
				if (clsCats.Get_BETEValue(intCounter) > 0)
				{
					if (boolFirst)
					{
						boolFirst = false;
						txtCatCaptions.Text = txtCatCaptions.Text + "\r\n";
						txtCatTotals.Text = txtCatTotals.Text + "\r\n";
					}
					txtCatTotals.Text = txtCatTotals.Text + Strings.Format(clsCats.Get_BETEValue(intCounter), "#,###,###,##0") + "\r\n";
					txtCatCaptions.Text = txtCatCaptions.Text + clsCats.Get_CategoryDescription(intCounter) + " BETE Exempt" + "\r\n";
				}
			}
			// intCounter
			if (modGlobal.Statics.DetailSummary != "F" || modPPGN.Statics.Sequence != "A")
			{
				txtCatTotals.Text = txtCatTotals.Text + "\r\n";
				txtCatTotals.Text = txtCatTotals.Text + txtBETETotal.Text;
				txtCatTotals.Text = txtCatTotals.Text + "\r\n";
				txtCatTotals.Text = txtCatTotals.Text + txtPropertyTotal.Text;
				txtCatTotals.Text = txtCatTotals.Text + "\r\n";
				txtCatTotals.Text = txtCatTotals.Text + Strings.Format(lngTotExemption, "###,###,###,##0") + "\r\n";
				txtCatTotals.Text = txtCatTotals.Text + Strings.Format(lngOverRideAmount, "###,###,##0") + "\r\n";
				txtCatTotals.Text = txtCatTotals.Text + Strings.Format(lngTotalAssessment, "###,###,###,##0");
				if (!modPPGN.Statics.boolShortScreenOnly)
				{
					txtCatTotals.Text = txtCatTotals.Text + "\r\n" + Strings.Format(lngTotalBilling, "###,###,###,##0");
				}
				txtCatCaptions.Text = txtCatCaptions.Text + "\r\n";
				txtCatCaptions.Text = txtCatCaptions.Text + "Total BETE Exemptions";
				txtCatCaptions.Text = txtCatCaptions.Text + "\r\n";
				txtCatCaptions.Text = txtCatCaptions.Text + "Total Property";
				txtCatCaptions.Text = txtCatCaptions.Text + "\r\n";
				txtCatCaptions.Text = txtCatCaptions.Text + "Total Exemptions" + "\r\n";
				txtCatCaptions.Text = txtCatCaptions.Text + "OverRide Amount" + "\r\n";
				txtCatCaptions.Text = txtCatCaptions.Text + "Total Assessment";
				if (!modPPGN.Statics.boolShortScreenOnly)
				{
					txtCatCaptions.Text = txtCatCaptions.Text + "\r\n" + "Total Billing Value";
				}
			}
		}

		
	}
}
