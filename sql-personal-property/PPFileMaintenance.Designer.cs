//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmPPFileMaintenance.
	/// </summary>
	partial class frmPPFileMaintenance : BaseForm
	{
		public fecherFoundation.FCComboBox cmbLeasedTrend;
		public fecherFoundation.FCLabel lblLeasedTrend;
		public fecherFoundation.FCComboBox cmbTrend;
		public fecherFoundation.FCComboBox cmbWhattodo;
		public fecherFoundation.FCComboBox cmbRounding;
		public fecherFoundation.FCLabel lblRounding;
		public fecherFoundation.FCComboBox cmbSRO;
		public fecherFoundation.FCCheckBox chkRoundIndividually;
		public fecherFoundation.FCTextBox txtMunicipalCode;
		public fecherFoundation.FCFrame Frame7;
		public fecherFoundation.FCComboBox cmbLabelType;
		public fecherFoundation.FCLabel lblDescription;
		public fecherFoundation.FCCheckBox chkBETEinExtract;
		public fecherFoundation.FCTextBox txtBETEReimburseRate;
		public fecherFoundation.FCCheckBox chkPreviousBETEYear;
		public fecherFoundation.FCTextBox txtBETEYear;
		public fecherFoundation.FCTextBox txtDefaultYFA;
		public fecherFoundation.FCFrame Frame5;
		public fecherFoundation.FCCheckBox chkShowOpen1OnCalc;
		public fecherFoundation.FCCheckBox chkShowOpen2OnCalc;
		public fecherFoundation.FCCheckBox chkShowOriginalTotals;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCCheckBox chkPercentGood;
		public fecherFoundation.FCTextBox txtTaxRate;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCTextBox txtDepreciation;
		public fecherFoundation.FCCheckBox chkTaxes;
		public fecherFoundation.FCGrid gridTownCode;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel lblActiveAccounts;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel lblTotalAccounts;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label1;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPPFileMaintenance));
            this.cmbLeasedTrend = new fecherFoundation.FCComboBox();
            this.lblLeasedTrend = new fecherFoundation.FCLabel();
            this.cmbTrend = new fecherFoundation.FCComboBox();
            this.cmbWhattodo = new fecherFoundation.FCComboBox();
            this.cmbRounding = new fecherFoundation.FCComboBox();
            this.lblRounding = new fecherFoundation.FCLabel();
            this.cmbSRO = new fecherFoundation.FCComboBox();
            this.chkRoundIndividually = new fecherFoundation.FCCheckBox();
            this.txtMunicipalCode = new fecherFoundation.FCTextBox();
            this.Frame7 = new fecherFoundation.FCFrame();
            this.cmbLabelType = new fecherFoundation.FCComboBox();
            this.lblDescription = new fecherFoundation.FCLabel();
            this.chkBETEinExtract = new fecherFoundation.FCCheckBox();
            this.txtBETEReimburseRate = new fecherFoundation.FCTextBox();
            this.chkPreviousBETEYear = new fecherFoundation.FCCheckBox();
            this.txtBETEYear = new fecherFoundation.FCTextBox();
            this.txtDefaultYFA = new fecherFoundation.FCTextBox();
            this.Frame5 = new fecherFoundation.FCFrame();
            this.chkShowOpen1OnCalc = new fecherFoundation.FCCheckBox();
            this.chkShowOpen2OnCalc = new fecherFoundation.FCCheckBox();
            this.chkShowOriginalTotals = new fecherFoundation.FCCheckBox();
            this.Frame4 = new fecherFoundation.FCFrame();
            this.chkPercentGood = new fecherFoundation.FCCheckBox();
            this.txtTaxRate = new fecherFoundation.FCTextBox();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.txtDepreciation = new fecherFoundation.FCTextBox();
            this.chkTaxes = new fecherFoundation.FCCheckBox();
            this.gridTownCode = new fecherFoundation.FCGrid();
            this.Label12 = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.lblActiveAccounts = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.lblTotalAccounts = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkRoundIndividually)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame7)).BeginInit();
            this.Frame7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkBETEinExtract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPreviousBETEYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
            this.Frame5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowOpen1OnCalc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowOpen2OnCalc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowOriginalTotals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            this.Frame4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkPercentGood)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkTaxes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTownCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(1078, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.chkRoundIndividually);
            this.ClientArea.Controls.Add(this.Frame4);
            this.ClientArea.Controls.Add(this.txtMunicipalCode);
            this.ClientArea.Controls.Add(this.Frame7);
            this.ClientArea.Controls.Add(this.chkBETEinExtract);
            this.ClientArea.Controls.Add(this.txtBETEReimburseRate);
            this.ClientArea.Controls.Add(this.chkPreviousBETEYear);
            this.ClientArea.Controls.Add(this.txtBETEYear);
            this.ClientArea.Controls.Add(this.cmbLeasedTrend);
            this.ClientArea.Controls.Add(this.lblLeasedTrend);
            this.ClientArea.Controls.Add(this.txtDefaultYFA);
            this.ClientArea.Controls.Add(this.Frame5);
            this.ClientArea.Controls.Add(this.txtTaxRate);
            this.ClientArea.Controls.Add(this.txtDepreciation);
            this.ClientArea.Controls.Add(this.cmbRounding);
            this.ClientArea.Controls.Add(this.lblRounding);
            this.ClientArea.Controls.Add(this.chkTaxes);
            this.ClientArea.Controls.Add(this.cmbSRO);
            this.ClientArea.Controls.Add(this.gridTownCode);
            this.ClientArea.Controls.Add(this.Label12);
            this.ClientArea.Controls.Add(this.Label10);
            this.ClientArea.Controls.Add(this.Label9);
            this.ClientArea.Controls.Add(this.Label8);
            this.ClientArea.Controls.Add(this.Label7);
            this.ClientArea.Controls.Add(this.lblActiveAccounts);
            this.ClientArea.Controls.Add(this.Label6);
            this.ClientArea.Controls.Add(this.lblTotalAccounts);
            this.ClientArea.Controls.Add(this.Label5);
            this.ClientArea.Controls.Add(this.Label4);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Size = new System.Drawing.Size(1078, 498);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(1078, 60);
            this.TopPanel.TabIndex = 0;
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(128, 30);
            this.HeaderText.Text = "Customize";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbLeasedTrend
            // 
            this.cmbLeasedTrend.Items.AddRange(new object[] {
            "Show as trend factor times original cost",
            "Show as trend factor times percent good"});
            this.cmbLeasedTrend.Location = new System.Drawing.Point(200, 523);
            this.cmbLeasedTrend.Name = "cmbLeasedTrend";
            this.cmbLeasedTrend.Size = new System.Drawing.Size(349, 40);
            this.cmbLeasedTrend.TabIndex = 16;
            this.cmbLeasedTrend.Text = "Show as trend factor times original cost";
            this.ToolTip1.SetToolTip(this.cmbLeasedTrend, null);
            // 
            // lblLeasedTrend
            // 
            this.lblLeasedTrend.AutoSize = true;
            this.lblLeasedTrend.Location = new System.Drawing.Point(41, 537);
            this.lblLeasedTrend.Name = "lblLeasedTrend";
            this.lblLeasedTrend.Size = new System.Drawing.Size(124, 15);
            this.lblLeasedTrend.TabIndex = 15;
            this.lblLeasedTrend.Text = "LEASED TRENDING";
            this.ToolTip1.SetToolTip(this.lblLeasedTrend, null);
            // 
            // cmbTrend
            // 
            this.cmbTrend.Items.AddRange(new object[] {
            "Show as trend factor times original cost",
            "Show as trend factor times percent good"});
            this.cmbTrend.Location = new System.Drawing.Point(20, 30);
            this.cmbTrend.Name = "cmbTrend";
            this.cmbTrend.Size = new System.Drawing.Size(475, 40);
            this.cmbTrend.Text = "Show as trend factor times percent good";
            this.ToolTip1.SetToolTip(this.cmbTrend, null);
            // 
            // cmbWhattodo
            // 
            this.cmbWhattodo.Items.AddRange(new object[] {
            "Use next lower option. ",
            "If value less than half rounding make it half",
            "Use chosen rounding option only"});
            this.cmbWhattodo.Location = new System.Drawing.Point(20, 30);
            this.cmbWhattodo.Name = "cmbWhattodo";
            this.cmbWhattodo.Size = new System.Drawing.Size(380, 40);
            this.ToolTip1.SetToolTip(this.cmbWhattodo, null);
            this.cmbWhattodo.SelectedIndexChanged += new System.EventHandler(this.cmbWhattodo_SelectedIndexChanged);
            // 
            // cmbRounding
            // 
            this.cmbRounding.Items.AddRange(new object[] {
            "Nearest 1000",
            "Nearest 100",
            "Nearest 10",
            "Nearest 1"});
            this.cmbRounding.Location = new System.Drawing.Point(200, 299);
            this.cmbRounding.Name = "cmbRounding";
            this.cmbRounding.Size = new System.Drawing.Size(202, 40);
            this.cmbRounding.TabIndex = 13;
            this.ToolTip1.SetToolTip(this.cmbRounding, null);
            // 
            // lblRounding
            // 
            this.lblRounding.AutoSize = true;
            this.lblRounding.Location = new System.Drawing.Point(29, 314);
            this.lblRounding.Name = "lblRounding";
            this.lblRounding.Size = new System.Drawing.Size(125, 15);
            this.lblRounding.TabIndex = 12;
            this.lblRounding.Text = "ROUNDING OPTION";
            this.ToolTip1.SetToolTip(this.lblRounding, null);
            // 
            // cmbSRO
            // 
            this.cmbSRO.Items.AddRange(new object[] {
            "Do Nothing",
            "Show on Printout",
            "Show on Display",
            "Show on Display & Printout",
            ""});
            this.cmbSRO.Location = new System.Drawing.Point(584, 607);
            this.cmbSRO.Name = "cmbSRO";
            this.cmbSRO.Size = new System.Drawing.Size(420, 40);
            this.cmbSRO.TabIndex = 18;
            this.ToolTip1.SetToolTip(this.cmbSRO, null);
            this.cmbSRO.Visible = false;
            // 
            // chkRoundIndividually
            // 
            this.chkRoundIndividually.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkRoundIndividually.Location = new System.Drawing.Point(30, 257);
            this.chkRoundIndividually.Name = "chkRoundIndividually";
            this.chkRoundIndividually.Size = new System.Drawing.Size(206, 27);
            this.chkRoundIndividually.TabIndex = 11;
            this.chkRoundIndividually.Text = "Round Items Individually";
            this.ToolTip1.SetToolTip(this.chkRoundIndividually, "When calculating, round each item rather than just rounding the category total");
            // 
            // txtMunicipalCode
            // 
            this.txtMunicipalCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtMunicipalCode.Location = new System.Drawing.Point(911, 120);
            this.txtMunicipalCode.Name = "txtMunicipalCode";
            this.txtMunicipalCode.Size = new System.Drawing.Size(93, 40);
            this.txtMunicipalCode.TabIndex = 25;
            this.ToolTip1.SetToolTip(this.txtMunicipalCode, null);
            this.txtMunicipalCode.TextChanged += new System.EventHandler(this.txtMunicipalCode_TextChanged);
            // 
            // Frame7
            // 
            this.Frame7.Controls.Add(this.cmbLabelType);
            this.Frame7.Controls.Add(this.lblDescription);
            this.Frame7.ForeColor = System.Drawing.Color.FromArgb(0, 0, 0);
            this.Frame7.Location = new System.Drawing.Point(584, 397);
            this.Frame7.Name = "Frame7";
            this.Frame7.Size = new System.Drawing.Size(420, 166);
            this.Frame7.TabIndex = 30;
            this.Frame7.Text = "Account Label";
            this.ToolTip1.SetToolTip(this.Frame7, null);
            // 
            // cmbLabelType
            // 
            this.cmbLabelType.BackColor = System.Drawing.SystemColors.Window;
            this.cmbLabelType.Location = new System.Drawing.Point(20, 30);
            this.cmbLabelType.Name = "cmbLabelType";
            this.cmbLabelType.Size = new System.Drawing.Size(380, 40);
            this.ToolTip1.SetToolTip(this.cmbLabelType, null);
            this.cmbLabelType.SelectedIndexChanged += new System.EventHandler(this.cmbLabelType_SelectedIndexChanged);
            // 
            // lblDescription
            // 
            this.lblDescription.Location = new System.Drawing.Point(20, 90);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(380, 30);
            this.lblDescription.TabIndex = 1;
            this.lblDescription.Text = "LABEL1";
            this.ToolTip1.SetToolTip(this.lblDescription, null);
            // 
            // chkBETEinExtract
            // 
            this.chkBETEinExtract.AutoSize = false;
            this.chkBETEinExtract.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkBETEinExtract.Location = new System.Drawing.Point(30, 220);
            this.chkBETEinExtract.Name = "chkBETEinExtract";
            this.chkBETEinExtract.Size = new System.Drawing.Size(241, 27);
            this.chkBETEinExtract.TabIndex = 9;
            this.chkBETEinExtract.Text = "Include BETE in user extract";
            this.ToolTip1.SetToolTip(this.chkBETEinExtract, "This will create a different record layout by adding BETE to the record");
            // 
            // txtBETEReimburseRate
            // 
            this.txtBETEReimburseRate.BackColor = System.Drawing.SystemColors.Window;
            this.txtBETEReimburseRate.Location = new System.Drawing.Point(354, 170);
            this.txtBETEReimburseRate.Name = "txtBETEReimburseRate";
            this.txtBETEReimburseRate.Size = new System.Drawing.Size(84, 40);
            this.txtBETEReimburseRate.TabIndex = 7;
            this.ToolTip1.SetToolTip(this.txtBETEReimburseRate, null);
            // 
            // chkPreviousBETEYear
            // 
            this.chkPreviousBETEYear.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkPreviousBETEYear.Location = new System.Drawing.Point(30, 220);
            this.chkPreviousBETEYear.Name = "chkPreviousBETEYear";
            this.chkPreviousBETEYear.Size = new System.Drawing.Size(321, 27);
            this.chkPreviousBETEYear.TabIndex = 10;
            this.chkPreviousBETEYear.Text = "Allow BETE exemption for previous year";
            this.ToolTip1.SetToolTip(this.chkPreviousBETEYear, "If the BETE application year is the year prior to the current BETE year, allow th" +
        "e exemption");
            this.chkPreviousBETEYear.Visible = false;
            // 
            // txtBETEYear
            // 
            this.txtBETEYear.BackColor = System.Drawing.SystemColors.Window;
            this.txtBETEYear.Location = new System.Drawing.Point(354, 120);
            this.txtBETEYear.MaxLength = 4;
            this.txtBETEYear.Name = "txtBETEYear";
            this.txtBETEYear.Size = new System.Drawing.Size(111, 40);
            this.txtBETEYear.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.txtBETEYear, null);
            // 
            // txtDefaultYFA
            // 
            this.txtDefaultYFA.BackColor = System.Drawing.SystemColors.Window;
            this.txtDefaultYFA.Location = new System.Drawing.Point(911, 70);
            this.txtDefaultYFA.MaxLength = 4;
            this.txtDefaultYFA.Name = "txtDefaultYFA";
            this.txtDefaultYFA.Size = new System.Drawing.Size(93, 40);
            this.txtDefaultYFA.TabIndex = 22;
            this.ToolTip1.SetToolTip(this.txtDefaultYFA, null);
            // 
            // Frame5
            // 
            this.Frame5.Controls.Add(this.chkShowOpen1OnCalc);
            this.Frame5.Controls.Add(this.chkShowOpen2OnCalc);
            this.Frame5.Controls.Add(this.chkShowOriginalTotals);
            this.Frame5.Location = new System.Drawing.Point(30, 588);
            this.Frame5.Name = "Frame5";
            this.Frame5.Size = new System.Drawing.Size(975, 70);
            this.Frame5.TabIndex = 17;
            this.Frame5.Text = "Calculation Report";
            this.ToolTip1.SetToolTip(this.Frame5, null);
            // 
            // chkShowOpen1OnCalc
            // 
            this.chkShowOpen1OnCalc.Location = new System.Drawing.Point(308, 30);
            this.chkShowOpen1OnCalc.Name = "chkShowOpen1OnCalc";
            this.chkShowOpen1OnCalc.Size = new System.Drawing.Size(126, 27);
            this.chkShowOpen1OnCalc.TabIndex = 1;
            this.chkShowOpen1OnCalc.Text = "Show Open 1";
            this.ToolTip1.SetToolTip(this.chkShowOpen1OnCalc, null);
            // 
            // chkShowOpen2OnCalc
            // 
            this.chkShowOpen2OnCalc.Location = new System.Drawing.Point(550, 30);
            this.chkShowOpen2OnCalc.Name = "chkShowOpen2OnCalc";
            this.chkShowOpen2OnCalc.Size = new System.Drawing.Size(126, 27);
            this.chkShowOpen2OnCalc.TabIndex = 2;
            this.chkShowOpen2OnCalc.Text = "Show Open 1";
            this.ToolTip1.SetToolTip(this.chkShowOpen2OnCalc, null);
            // 
            // chkShowOriginalTotals
            // 
            this.chkShowOriginalTotals.Location = new System.Drawing.Point(20, 30);
            this.chkShowOriginalTotals.Name = "chkShowOriginalTotals";
            this.chkShowOriginalTotals.Size = new System.Drawing.Size(228, 27);
            this.chkShowOriginalTotals.Text = "Show totals for original cost";
            this.ToolTip1.SetToolTip(this.chkShowOriginalTotals, null);
            // 
            // Frame4
            // 
            this.Frame4.Controls.Add(this.chkPercentGood);
            this.Frame4.Controls.Add(this.cmbTrend);
            this.Frame4.Location = new System.Drawing.Point(30, 369);
            this.Frame4.Name = "Frame4";
            this.Frame4.Size = new System.Drawing.Size(519, 127);
            this.Frame4.TabIndex = 14;
            this.Frame4.Text = "Itemized Trending";
            this.ToolTip1.SetToolTip(this.Frame4, null);
            // 
            // chkPercentGood
            // 
            this.chkPercentGood.Location = new System.Drawing.Point(20, 80);
            this.chkPercentGood.Name = "chkPercentGood";
            this.chkPercentGood.Size = new System.Drawing.Size(207, 27);
            this.chkPercentGood.TabIndex = 1;
            this.chkPercentGood.Text = "Cap percent good at 100";
            this.ToolTip1.SetToolTip(this.chkPercentGood, null);
            // 
            // txtTaxRate
            // 
            this.txtTaxRate.BackColor = System.Drawing.SystemColors.Window;
            this.txtTaxRate.Location = new System.Drawing.Point(911, 170);
            this.txtTaxRate.Name = "txtTaxRate";
            this.txtTaxRate.Size = new System.Drawing.Size(93, 40);
            this.txtTaxRate.TabIndex = 27;
            this.ToolTip1.SetToolTip(this.txtTaxRate, null);
            this.txtTaxRate.TextChanged += new System.EventHandler(this.txtTaxRate_TextChanged);
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.Label3);
            this.Frame3.Controls.Add(this.cmbWhattodo);
            this.Frame3.Controls.Add(this.Label2);
            this.Frame3.Location = new System.Drawing.Point(584, 257);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(420, 130);
            this.Frame3.TabIndex = 29;
            this.Frame3.Text = "If Value Less Than Rounding Option";
            this.ToolTip1.SetToolTip(this.Frame3, null);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 100);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(380, 14);
            this.Label3.TabIndex = 2;
            this.Label3.Text = "EX  - 305 IS < 500 (HALF OF 1000) SO MAKE IT 500";
            this.ToolTip1.SetToolTip(this.Label3, null);
            this.Label3.Visible = false;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 80);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(380, 14);
            this.Label2.TabIndex = 1;
            this.Label2.Text = "EX - 649 IS < 1000 SO ROUND TO 600";
            this.ToolTip1.SetToolTip(this.Label2, null);
            this.Label2.Visible = false;
            // 
            // txtDepreciation
            // 
            this.txtDepreciation.BackColor = System.Drawing.SystemColors.Window;
            this.txtDepreciation.Location = new System.Drawing.Point(354, 70);
            this.txtDepreciation.MaxLength = 4;
            this.txtDepreciation.Name = "txtDepreciation";
            this.txtDepreciation.Size = new System.Drawing.Size(111, 40);
            this.txtDepreciation.TabIndex = 3;
            this.txtDepreciation.Text = "0000";
            this.ToolTip1.SetToolTip(this.txtDepreciation, null);
            this.txtDepreciation.Leave += new System.EventHandler(this.txtDepreciation_Leave);
            // 
            // chkTaxes
            // 
            this.chkTaxes.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkTaxes.Location = new System.Drawing.Point(584, 220);
            this.chkTaxes.Name = "chkTaxes";
            this.chkTaxes.Size = new System.Drawing.Size(328, 27);
            this.chkTaxes.TabIndex = 28;
            this.chkTaxes.Text = "Show Year/Rate/Taxes on Short Screen?";
            this.ToolTip1.SetToolTip(this.chkTaxes, null);
            // 
            // gridTownCode
            // 
            this.gridTownCode.Cols = 1;
            this.gridTownCode.ColumnHeadersVisible = false;
            this.gridTownCode.ExtendLastCol = true;
            this.gridTownCode.FixedCols = 0;
            this.gridTownCode.FixedRows = 0;
            this.gridTownCode.HighLight = fecherFoundation.FCGrid.HighLightSettings.flexHighlightNever;
            this.gridTownCode.Location = new System.Drawing.Point(584, 120);
            this.gridTownCode.Name = "gridTownCode";
            this.gridTownCode.RowHeadersVisible = false;
            this.gridTownCode.Rows = 1;
            this.gridTownCode.ShowFocusCell = false;
            this.gridTownCode.Size = new System.Drawing.Size(144, 40);
            this.gridTownCode.TabIndex = 23;
            this.ToolTip1.SetToolTip(this.gridTownCode, null);
            this.gridTownCode.Visible = false;
            this.gridTownCode.ComboCloseUp += new System.EventHandler(this.gridTownCode_ComboCloseUp);
            this.gridTownCode.Leave += new System.EventHandler(this.gridTownCode_Leave);
            // 
            // Label12
            // 
            this.Label12.Location = new System.Drawing.Point(753, 134);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(123, 10);
            this.Label12.TabIndex = 24;
            this.Label12.Text = "MUNICIPAL CODE";
            this.ToolTip1.SetToolTip(this.Label12, null);
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(444, 182);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(23, 12);
            this.Label10.TabIndex = 8;
            this.Label10.Text = "%";
            this.ToolTip1.SetToolTip(this.Label10, null);
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(30, 184);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(169, 15);
            this.Label9.TabIndex = 6;
            this.Label9.Text = "BETE REIMBURSEMENT RATE";
            this.ToolTip1.SetToolTip(this.Label9, null);
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(30, 134);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(229, 29);
            this.Label8.TabIndex = 4;
            this.Label8.Text = "ENTER THE YEAR TO USE FOR BETE. 0 FOR CURRENT YEAR";
            this.ToolTip1.SetToolTip(this.Label8, null);
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(584, 84);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(292, 33);
            this.Label7.TabIndex = 21;
            this.Label7.Text = "ENTER THE DEFAULT YEAR FIRST ASSESSED. ENTER 0 OR LEAVE BLANK TO USE THE CURRENT " +
    "YEAR";
            this.ToolTip1.SetToolTip(this.Label7, null);
            // 
            // lblActiveAccounts
            // 
            this.lblActiveAccounts.Location = new System.Drawing.Point(717, 30);
            this.lblActiveAccounts.Name = "lblActiveAccounts";
            this.lblActiveAccounts.Size = new System.Drawing.Size(54, 20);
            this.lblActiveAccounts.TabIndex = 20;
            this.ToolTip1.SetToolTip(this.lblActiveAccounts, null);
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(584, 30);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(138, 20);
            this.Label6.TabIndex = 19;
            this.Label6.Text = "ACTIVE ACCOUNTS";
            this.ToolTip1.SetToolTip(this.Label6, null);
            // 
            // lblTotalAccounts
            // 
            this.lblTotalAccounts.Location = new System.Drawing.Point(203, 30);
            this.lblTotalAccounts.Name = "lblTotalAccounts";
            this.lblTotalAccounts.Size = new System.Drawing.Size(54, 20);
            this.lblTotalAccounts.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.lblTotalAccounts, null);
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(30, 30);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(138, 20);
            this.Label5.Text = "TOTAL ACCOUNTS";
            this.ToolTip1.SetToolTip(this.Label5, null);
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(753, 184);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(71, 10);
            this.Label4.TabIndex = 26;
            this.Label4.Text = "TAX RATE";
            this.ToolTip1.SetToolTip(this.Label4, null);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 84);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(289, 33);
            this.Label1.TabIndex = 2;
            this.Label1.Text = "ENTER THE DEFAULT DEPRECIATION YEAR.  ENTER \'0000\' TO USE THE CURRENT YEAR";
            this.ToolTip1.SetToolTip(this.Label1, null);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSepar,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 0;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 2;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 3;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(68, 36);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(86, 48);
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // frmPPFileMaintenance
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1078, 666);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmPPFileMaintenance";
            this.Text = "Customize";
            this.ToolTip1.SetToolTip(this, null);
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmPPFileMaintenance_Load);
            this.Activated += new System.EventHandler(this.frmPPFileMaintenance_Activated);
            this.Resize += new System.EventHandler(this.frmPPFileMaintenance_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPPFileMaintenance_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPPFileMaintenance_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkRoundIndividually)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame7)).EndInit();
            this.Frame7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkBETEinExtract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPreviousBETEYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
            this.Frame5.ResumeLayout(false);
            this.Frame5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowOpen1OnCalc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowOpen2OnCalc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowOriginalTotals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            this.Frame4.ResumeLayout(false);
            this.Frame4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkPercentGood)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkTaxes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTownCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		public FCLabel Label3;
	}
}