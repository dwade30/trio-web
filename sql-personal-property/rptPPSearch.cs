﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Core;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptNameAddressLoc.
	/// </summary>
	public partial class rptPPSearch : BaseSectionReport
	{
		public rptPPSearch()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Search Results Report";
		}

		public static rptPPSearch InstancePtr
		{
			get
			{
				return (rptPPSearch)Sys.GetInstance(typeof(rptPPSearch));
			}
		}

		protected rptPPSearch _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptNameAddressLoc	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private int intPage;
		private int intAccount;
		private bool boolPrint;
		private string strName = "";
		private string strLocation = "";
		private clsDRWrapper rsLoad = new clsDRWrapper();

        
		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			txttime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsLoad.EndOfFile();
			if (eArgs.EOF)
				return;			
			txtName.Text = rsLoad.GetData("Name").ToString();
			txtAccount.Text = rsLoad.GetData("Account").ToString();
			txtAddress.Text = rsLoad.GetData("Street").ToString();
            txtStNo.Text = rsLoad.GetData("StreetNumber").ToString();
            if (!rsLoad.EndOfFile())
				rsLoad.MoveNext();
			eArgs.EOF = false;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intPage += 1;
			txtPage.Text = "Page: " + FCConvert.ToString(intPage);
		}
	}
}
