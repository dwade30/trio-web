﻿namespace TWPP0000
{
	/// <summary>
	/// Summary description for srptAssessBusCode.
	/// </summary>
	partial class srptAssessBusCode
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptAssessBusCode));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblRptType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBillingExemption = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txttotalExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblRptType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillingExemption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotalExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtCode,
				this.txtCount,
				this.txtValue,
				this.txtExemption,
				this.txtTotal
			});
			this.Detail.Height = 0.19F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0.1770833F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label7,
				this.txtTotalCount,
				this.txtTotalValue,
				this.txttotalExempt,
				this.txtTotalTotal
			});
			this.ReportFooter.Height = 0.3541667F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblRptType,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.lblBillingExemption
			});
			this.GroupHeader1.Height = 0.3541667F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblRptType
			// 
			this.lblRptType.Height = 0.19F;
			this.lblRptType.HyperLink = null;
			this.lblRptType.Left = 0.0625F;
			this.lblRptType.Name = "lblRptType";
			this.lblRptType.Style = "font-weight: bold; text-align: center";
			this.lblRptType.Text = "Business";
			this.lblRptType.Top = 0.03125F;
			this.lblRptType.Width = 0.9375F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.19F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.0625F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold; text-align: center";
			this.Label2.Text = "Code";
			this.Label2.Top = 0.1875F;
			this.Label2.Width = 0.9375F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.19F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 1.0625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold; text-align: right";
			this.Label3.Text = "Count";
			this.Label3.Top = 0.1875F;
			this.Label3.Width = 0.5625F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.19F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 1.6875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-weight: bold; text-align: right";
			this.Label4.Text = "Value";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.1875F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.19F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 2.9375F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-weight: bold; text-align: right";
			this.Label5.Text = "Exemption";
			this.Label5.Top = 0.1875F;
			this.Label5.Width = 1.0625F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.19F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 4.0625F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-weight: bold; text-align: right";
			this.Label6.Text = "Total";
			this.Label6.Top = 0.1875F;
			this.Label6.Width = 1.25F;
			// 
			// lblBillingExemption
			// 
			this.lblBillingExemption.Height = 0.1875F;
			this.lblBillingExemption.HyperLink = null;
			this.lblBillingExemption.Left = 3.0625F;
			this.lblBillingExemption.Name = "lblBillingExemption";
			this.lblBillingExemption.Style = "font-weight: bold; text-align: right";
			this.lblBillingExemption.Text = "Billing";
			this.lblBillingExemption.Top = 0.03125F;
			this.lblBillingExemption.Visible = false;
			this.lblBillingExemption.Width = 0.9375F;
			// 
			// txtCode
			// 
			this.txtCode.Height = 0.19F;
			this.txtCode.Left = 0.0625F;
			this.txtCode.Name = "txtCode";
			this.txtCode.Style = "text-align: right";
			this.txtCode.Text = null;
			this.txtCode.Top = 0F;
			this.txtCode.Width = 0.6875F;
			// 
			// txtCount
			// 
			this.txtCount.CanGrow = false;
			this.txtCount.Height = 0.19F;
			this.txtCount.Left = 1.0625F;
			this.txtCount.Name = "txtCount";
			this.txtCount.Style = "text-align: right";
			this.txtCount.Text = null;
			this.txtCount.Top = 0F;
			this.txtCount.Width = 0.5625F;
			// 
			// txtValue
			// 
			this.txtValue.CanGrow = false;
			this.txtValue.Height = 0.19F;
			this.txtValue.Left = 1.6875F;
			this.txtValue.Name = "txtValue";
			this.txtValue.Style = "text-align: right";
			this.txtValue.Text = null;
			this.txtValue.Top = 0F;
			this.txtValue.Width = 1.1875F;
			// 
			// txtExemption
			// 
			this.txtExemption.CanGrow = false;
			this.txtExemption.Height = 0.19F;
			this.txtExemption.Left = 2.9375F;
			this.txtExemption.Name = "txtExemption";
			this.txtExemption.Style = "text-align: right";
			this.txtExemption.Text = null;
			this.txtExemption.Top = 0F;
			this.txtExemption.Width = 1.0625F;
			// 
			// txtTotal
			// 
			this.txtTotal.CanGrow = false;
			this.txtTotal.Height = 0.19F;
			this.txtTotal.Left = 4.0625F;
			this.txtTotal.Name = "txtTotal";
			this.txtTotal.Style = "text-align: right";
			this.txtTotal.Text = null;
			this.txtTotal.Top = 0F;
			this.txtTotal.Width = 1.25F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.19F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-weight: bold";
			this.Label7.Text = "Total";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 0.9375F;
			// 
			// txtTotalCount
			// 
			this.txtTotalCount.CanGrow = false;
			this.txtTotalCount.Height = 0.19F;
			this.txtTotalCount.Left = 1.0625F;
			this.txtTotalCount.Name = "txtTotalCount";
			this.txtTotalCount.Style = "text-align: right";
			this.txtTotalCount.Text = null;
			this.txtTotalCount.Top = 0.1875F;
			this.txtTotalCount.Width = 0.5625F;
			// 
			// txtTotalValue
			// 
			this.txtTotalValue.CanGrow = false;
			this.txtTotalValue.Height = 0.19F;
			this.txtTotalValue.Left = 1.6875F;
			this.txtTotalValue.Name = "txtTotalValue";
			this.txtTotalValue.Style = "text-align: right";
			this.txtTotalValue.Text = null;
			this.txtTotalValue.Top = 0.1875F;
			this.txtTotalValue.Width = 1.1875F;
			// 
			// txttotalExempt
			// 
			this.txttotalExempt.CanGrow = false;
			this.txttotalExempt.Height = 0.19F;
			this.txttotalExempt.Left = 2.9375F;
			this.txttotalExempt.Name = "txttotalExempt";
			this.txttotalExempt.Style = "text-align: right";
			this.txttotalExempt.Text = null;
			this.txttotalExempt.Top = 0.1875F;
			this.txttotalExempt.Width = 1.0625F;
			// 
			// txtTotalTotal
			// 
			this.txtTotalTotal.CanGrow = false;
			this.txtTotalTotal.Height = 0.19F;
			this.txtTotalTotal.Left = 4.0625F;
			this.txtTotalTotal.Name = "txtTotalTotal";
			this.txtTotalTotal.Style = "text-align: right";
			this.txtTotalTotal.Text = null;
			this.txtTotalTotal.Top = 0.1875F;
			this.txtTotalTotal.Width = 1.25F;
			// 
			// srptAssessBusCode
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.Name = "ActiveReport1";
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.4375F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblRptType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillingExemption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotalExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txttotalExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRptType;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBillingExemption;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
