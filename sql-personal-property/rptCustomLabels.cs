﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using SharedApplication.Extensions;
using TWSharedLibrary;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptCustomLabels.
	/// </summary>
	public partial class rptCustomLabels : BaseSectionReport
	{
		public rptCustomLabels()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:MSH - i.issue #819: incorrect report name
			//this.Name = "rptCustomLabels";
			this.Name = "Custom Labels";
		}

		public static rptCustomLabels InstancePtr
		{
			get
			{
				return (rptCustomLabels)Sys.GetInstance(typeof(rptCustomLabels));
			}
		}

		protected rptCustomLabels _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
				rsValues?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCustomLabels	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		/// <summary>
		/// THIS REPORT IS TOTOALLY GENERIC AND IF USED BY WITH
		/// </summary>
		/// <summary>
		/// MODCUSTOMREPORT.MOD AND frmCustomLabels.FRM THEN THERE
		/// </summary>
		/// <summary>
		/// DOES NOT NEED TO HAVE ANY CHANGES DONE TO THIS REPORT
		/// </summary>
		clsDRWrapper rsData = new clsDRWrapper();
		// vbPorter upgrade warning: intLabelWidth As short --> As int	OnWrite(int, double)
		float intLabelWidth;
		bool boolDifferentPageSize;
		int intTypeOfLabel;
		string strFont = "";
		string[] strLine = null;
		string[] strLabelLine = null;
		string strOrder = "";
		bool boolFirstLabel;
		// vbPorter upgrade warning: intNumChars As short --> As int	OnWrite(double, short)
		int intNumChars;
		clsDRWrapper rsValues = new clsDRWrapper();
		bool boolWidePrinter;
		private bool boolDotMatrix;
		string REC = "";
		string strFirstLabel = "";
		int intCurrCol;
		string strOldPrinter = "";
		int intBlankLabelsLeftToPrint;
		const int COLACCOUNT = 0;
		const int COLNAME = 1;
		const int COLADDR1 = 2;
		const int COLADDR2 = 3;
		const int COLCITY = 4;
		const int COLSTATE = 5;
		const int COLZIP = 6;
		const int COLZIP4 = 7;
		const int COLLOCNUM = 8;
		const int COLLOCSTREET = 9;
		const int COLTELEPHONE = 10;
		const int COLITEMIZEDVAL = 13;
		const int COLLEASEDVAL = 14;
		const int COLEXEMPTION = 15;
		const int COLTOTALVALUE = 16;
		const int COLOPEN1 = 11;
		const int COLOPEN2 = 12;
		const int COLTAX = 17;
		const int COLAddr3 = 18;
		int lngCurrRow;
		bool boolIncLoc;
		bool boolIncOpen1;
		bool boolIncOpen2;
		bool boolIncAcct;
		bool boolIncValue;
		bool boolIncAddr;
		bool boolIncETax;
		string strTaxYear;
		float snglTax;
		float sngAdjustment;
		int intNumberofDuplicates;
		int intDuplicateCount;
		int intLabelToPrint;
		// vbPorter upgrade warning: intNumberLines As short --> As int	OnWrite(double, short)
		int intNumberLines;
		bool boolNotShowing;
		clsPrintLabel labLabels = new clsPrintLabel();
		// vbPorter upgrade warning: lngPrintWidth As int	OnWrite(string, double)
		float lngPrintWidth;
		bool IsReportStartHandled = false;
		const int const_printtoolid = 9950;
		FCGrid Grid;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			if (IsReportStartHandled)
			{
				IsReportStartHandled = false;
				return;
			}
			int const_printtoolid;
			int cnt;
			string strMasterJoin;
			string strMasterJoinJoin;
			strMasterJoin = modPPGN.GetMasterJoin();
			strMasterJoinJoin = modPPGN.GetMasterJoinForJoin();
			// DJW@121014 changed to only set variable if it was not already set because it was causing the default printer to not be set back after printing the label TROPP-55
			if (strOldPrinter == "")
			{
				strOldPrinter = this.Document.Printer.PrinterName;
			}
			// Me.FCGlobal.Printer.DeviceName = ""
			intCurrCol = 0;
			if (modPPGN.Statics.gboolPrintF2Label)
			{
				//this.Document.Printer.PrinterName = modGlobalConstants.Statics.LabelPrinter;
				boolWidePrinter = false;
				intNumberofDuplicates = 0;
				sngAdjustment = 0;
				lngCurrRow = 0;
				intTypeOfLabel = modGlobal.Statics.CustomizeStuff.AccountLabelType;
				intLabelToPrint = 1;
				// Call rsData.OpenRecordset("select * from ppmaster where account = " & CurrentAccount, modPPGN.strPPDatabase)
				rsData.OpenRecordset(strMasterJoin + " where account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount), modPPGN.strPPDatabase);
				if (rsData.EndOfFile())
				{
					this.Close();
					return;
				}
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [streetNumber] and replace with corresponding Get_Field method
				this.Grid.AddItem(rsData.Get_Fields("account") + "\t" + rsData.Get_Fields_String("Name") + "\t" + rsData.Get_Fields_String("Address1") + "\t" + rsData.Get_Fields_String("Address2") + "\t" + rsData.Get_Fields_String("City") + "\t" + rsData.Get_Fields_String("state") + "\t" + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("zip"))) + "\t" + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("zip4"))) + "\t" + rsData.Get_Fields("streetNumber") + "\t" + rsData.Get_Fields_String("Street") + "\t" + rsData.Get_Fields_String("phonenumber") + "\t" + rsData.Get_Fields_String("open1") + "\t" + rsData.Get_Fields_String("open2"));
				Grid.TextMatrix(Grid.Rows - 1, COLAddr3, rsData.Get_Fields_String("address3"));
				boolDifferentPageSize = false;
				int intIndex = 0;
				intIndex = labLabels.Get_IndexFromID(intTypeOfLabel);
				if (labLabels.Get_IsDymoLabel(intIndex))
				{
					switch (labLabels.Get_ID(intIndex))
					{
						case modLabels.CNSTLBLTYPEDYMO30256:
							{
								intNumberLines = 12;
								this.Document.Printer.DefaultPageSettings.Landscape = true;
								PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
								PageSettings.Margins.Top = FCConvert.ToSingle(0.128);
								PageSettings.Margins.Bottom = 0;
								PageSettings.Margins.Right = 0;
								PageSettings.Margins.Left = FCConvert.ToSingle(0.128);
								PrintWidth = FCConvert.ToSingle(2.31F) - PageSettings.Margins.Left - PageSettings.Margins.Right;
								intLabelWidth = 4F;
								lngPrintWidth = 4F - PageSettings.Margins.Left - PageSettings.Margins.Right;
								intNumChars = 4 * 10 - 3;
								Detail.Height = 2.3125F - PageSettings.Margins.Top - PageSettings.Margins.Bottom - 10;
								Detail.ColumnCount = 1;
								PageSettings.PaperHeight = 4;
								PageSettings.PaperWidth = FCConvert.ToSingle(2.31);
								Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
								this.Document.Printer.DefaultPageSettings.Landscape = true;
								PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
								break;
							}
						case modLabels.CNSTLBLTYPEDYMO30252:
							{
								intNumberLines = 6;
								this.Document.Printer.DefaultPageSettings.Landscape = true;
								PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
								PageSettings.Margins.Top = FCConvert.ToSingle(0.128);
								PageSettings.Margins.Bottom = 0;
								PageSettings.Margins.Right = 0;
								PageSettings.Margins.Left = FCConvert.ToSingle(0.128);
								PrintWidth = 3.5F - PageSettings.Margins.Left - PageSettings.Margins.Right;
								intLabelWidth = 3.5F;
								lngPrintWidth = 3.5F - PageSettings.Margins.Left - PageSettings.Margins.Right;
								intNumChars = FCConvert.ToInt32(3.5 * 10 - 3);
								Detail.Height = (1.1f) - PageSettings.Margins.Top - PageSettings.Margins.Bottom - 10 / 1440f;
								Detail.ColumnCount = 1;
								PageSettings.PaperHeight = 3.5F;
								PageSettings.PaperWidth = 1.1F;
								Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
								this.Document.Printer.DefaultPageSettings.Landscape = true;
								PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
								break;
							}
					}
				}
				strLine = new string[intNumberLines + 1];
				strLabelLine = new string[intNumberLines + 1];
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			int x;
			// IF THIS IS THE END OF THE RECORDSET THEN WE NEED A WAY TO GET OUT
			eArgs.EOF = lngCurrRow >= Grid.Rows;
			if (eArgs.EOF && boolDotMatrix)
			{
				if (intCurrCol < Detail.ColumnCount)
				{
					for (x = 1; x <= intNumberLines; x++)
					{
						if (!boolFirstLabel)
						{
							REC += strLabelLine[x] + "\r\n";
						}
						else
						{
							strFirstLabel += strLabelLine[x] + "\r\n";
						}
					}
					// x
					boolFirstLabel = false;
				}
			}
		}
		public void Init(bool boolElim, short intHowToPrint, short intTypePrint, short intOrdBy, short intLabelType, byte bytWhatInclude = 0, string strMinAccount = "", string strMaxAccount = "", string strYear = "", float sngTax = 0, string strPrinterName = "", float sngAdjust = 0, int intNumDuplicates = 0, bool boolDontShow = false, short intLabelStartPosition = 1)
		{
			this.Grid = new FCGrid();
			this.Grid.Cols = 21;
			this.Grid.FixedRows = 0;
			this.Grid.Rows = 0;
			string strDBName;
			int intReturn;
			string strSQL = "";
			int x = 0;
			string strTempOrder = "";
			bool boolUseFont;
			string strCard = "";
			string strName = "";
			string strAddr1 = "";
			string strRefBookPage = "";
			double dblTemp = 0;
			string strPrinterToUse;
			string strMasterJoin;
			try
			{
				// On Error GoTo ErrorHandler
				strOldPrinter = this.Document.Printer.PrinterName;
				strMasterJoin = modPPGN.GetMasterJoin();
				string strMasterJoinJoin;
				strMasterJoinJoin = modPPGN.GetMasterJoinForJoin();
				strPrinterToUse = "";
				boolFirstLabel = true;
				intBlankLabelsLeftToPrint = intLabelStartPosition - 1;
				boolNotShowing = boolDontShow;
				REC = "";
				strFirstLabel = "";
				strTaxYear = strYear;
				this.Document.Printer.PrinterName = "";
				boolWidePrinter = false;
				iLabel1:
				intNumberofDuplicates = intNumDuplicates;
				iLabel2:
				intDuplicateCount = intNumberofDuplicates;
				iLabel3:
				sngAdjustment = sngAdjust;
				iLabel4:
				if (sngAdjustment < -0.5)
					sngAdjustment = FCConvert.ToSingle(-0.5);
				iLabel5:
				if (sngAdjustment > 0.5)
					sngAdjustment = 0.5f;
				iLabel6:
				snglTax = sngTax;
				iLabel7:
				lngCurrRow = 0;
				iLabel8:
				if (strPrinterName != string.Empty)
					this.Document.Printer.PrinterName = strPrinterName;
				// 9    intTypeOfLabel = intTypePrint
				intTypeOfLabel = intLabelType;
				intLabelToPrint = intTypePrint;
				iLabel10:
				strDBName = modPPGN.strPPDatabase;
				iLabel11:
				switch (intOrdBy)
				{
					case 1:
						{
							// account
							iLabel13:
							strOrder = "ACCOUNT";
							break;
						}
					case 2:
						{
							// name
							iLabel14:
							strOrder = "Name";
							break;
						}
					case 3:
						{
							// street
							iLabel15:
							strOrder = "Street";
							break;
						}
					case 4:
						{
							// zip
							iLabel16:
							strOrder = "Zip";
							break;
						}
				}
				//end switch
				iLabel21:
				if ((bytWhatInclude & 64) > 0)
				{
					iLabel22:
					boolIncLoc = true;
				}
				else
				{
					iLabel24:
					boolIncLoc = false;
				}
				iLabel25:
				if ((bytWhatInclude & 32) > 0)
				{
					iLabel26:
					boolIncOpen2 = true;
				}
				else
				{
					iLabel27:
					boolIncOpen2 = false;
				}
				iLabel28:
				if ((bytWhatInclude & 16) > 0)
				{
					iLabel29:
					boolIncAcct = true;
				}
				else
				{
					iLabel30:
					boolIncAcct = false;
				}
				iLabel31:
				if ((bytWhatInclude & 8) > 0)
				{
					iLabel32:
					boolIncOpen1 = true;
				}
				else
				{
					iLabel33:
					boolIncOpen1 = false;
				}
				iLabel34:
				if ((bytWhatInclude & 4) > 0)
				{
					iLabel35:
					boolIncAddr = true;
				}
				else
				{
					iLabel36:
					boolIncAddr = false;
				}
				iLabel37:
				if ((bytWhatInclude & 2) > 0)
				{
					iLabel38:
					boolIncValue = true;
				}
				else
				{
					iLabel39:
					boolIncValue = false;
				}
				iLabel40:
				if ((bytWhatInclude & 1) > 0)
				{
					iLabel41:
					boolIncETax = true;
				}
				else
				{
					iLabel42:
					boolIncETax = false;
				}
				iLabel46:
				if (boolElim)
				{
					iLabel47:
					strTempOrder = "name,address1,ppmaster.account";
				}
				else
				{
					iLabel48:
					strTempOrder = strOrder;
					iLabel49:
					if (intOrdBy == 3)
					{
						iLabel50:
						strTempOrder = " street,streetnumber ";
					}
				}
				iLabel51:
				switch (intHowToPrint)
				{
					case 1:
						{
							// all
							// 52            strSQL = "Select * from ppmaster where  not deleted = 1 order by " & strTempOrder
							strSQL = strMasterJoin + " where  not deleted = 1 order by " + strTempOrder;
							break;
						}
					case 2:
						{
							iLabel53:
							// range
							iLabel54:
							if (intOrdBy == 1)
							{
								iLabel55:
								// strSQL = "select * from ppmaster where not deleted = 1 and account between " & Val(strMinAccount) & " and " & Val(strMaxAccount) & " order by " & strTempOrder
								strSQL = strMasterJoin + " where not deleted = 1 and account between " + FCConvert.ToString(Conversion.Val(strMinAccount)) + " and " + FCConvert.ToString(Conversion.Val(strMaxAccount)) + " order by " + strTempOrder;
							}
							else
							{
								iLabel56:
								// strSQL = "select * from ppmaster where  not deleted = 1 and " & strOrder & " between '" & strMinAccount & "' and '" & strMaxAccount & "' order by " & strTempOrder
								strSQL = strMasterJoin + " where  not deleted = 1 and " + strOrder + " between '" + strMinAccount + "' and '" + strMaxAccount + "' order by " + strTempOrder;
							}
							break;
						}
					case 3:
						{
							// individual
							iLabel57:
							// strSQL = "select  ppmaster.* from ppmaster inner join accountlist on (ppmaster.account = accountlist.account) where (not deleted = 1)  order by " & strTempOrder
							strSQL = "select  mj.* from " + strMasterJoinJoin + " inner join accountlist on (mj.account = accountlist.account) where (not deleted = 1)  order by " + strTempOrder;
							break;
						}
				}
				//end switch
				if (intLabelToPrint == 4 || boolIncETax || boolIncValue)
				{
					// assessment labels or values in custom label
					rsValues.OpenRecordset("select * from ppvaluations order by valuekey", "twpp0000.vb1");
				}
				iLabel59:
				rsData.OpenRecordset(strSQL, strDBName);
				while (!rsData.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [streetNumber] and replace with corresponding Get_Field method
					iLabel60:
					this.Grid.AddItem(rsData.Get_Fields("account") + "\t" + rsData.Get_Fields_String("Name") + "\t" + rsData.Get_Fields_String("Address1") + "\t" + rsData.Get_Fields_String("Address2") + "\t" + rsData.Get_Fields_String("City") + "\t" + rsData.Get_Fields_String("state") + "\t" + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("zip"))) + "\t" + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("zip4"))) + "\t" + rsData.Get_Fields("streetNumber") + "\t" + rsData.Get_Fields_String("Street") + "\t" + rsData.Get_Fields_String("phonenumber") + "\t" + rsData.Get_Fields_String("open1") + "\t" + rsData.Get_Fields_String("open2"));
					this.Grid.TextMatrix(this.Grid.Rows - 1, COLAddr3, Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address3"))));
					// if tax info then add that
					// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
					this.Grid.TextMatrix(this.Grid.Rows - 1, COLTOTALVALUE, Strings.Format(rsData.Get_Fields("value"), "#,###,###,##0"));
					if (intLabelToPrint == 4 || boolIncETax || boolIncValue)
					{
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						if (rsValues.FindFirstRecord("valuekey", rsData.Get_Fields("account")))
						{
							if (Conversion.Val(rsValues.Get_Fields_Int32("TOTALitemized")) > 0)
							{
								this.Grid.TextMatrix(this.Grid.Rows - 1, COLITEMIZEDVAL, Strings.Format(Conversion.Val(rsValues.Get_Fields_Int32("totalitemized")), "#,###,###,##0"));
							}
							if (Conversion.Val(rsValues.Get_Fields_Int32("totalleased")) > 0)
							{
								this.Grid.TextMatrix(this.Grid.Rows - 1, COLLEASEDVAL, Strings.Format(Conversion.Val(rsValues.Get_Fields_Int32("totalleased")), "#,###,###,##0"));
							}
							if (boolIncETax || intLabelToPrint == 4)
							{
								// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
								dblTemp = (sngTax / 1000) * Conversion.Val(rsData.Get_Fields("value"));
								this.Grid.TextMatrix(this.Grid.Rows - 1, COLTAX, Strings.Format(dblTemp, "#,###,###,##0.00"));
							}
							if (Conversion.Val(rsData.Get_Fields_Int32("exemption")) > 0)
							{
								this.Grid.TextMatrix(this.Grid.Rows - 1, COLEXEMPTION, Strings.Format(Conversion.Val(rsData.Get_Fields_Int32("exemption")), "#,###,###,##0"));
							}
						}
					}
					iLabel61:
					rsData.MoveNext();
				}
				if (boolElim)
				{
					// must eliminate duplicates then reorder the grid
					iLabel62:
					strName = "";
					iLabel63:
					strAddr1 = "";
					iLabel64:
					x = 0;
					iLabel65:
					while (x < Grid.Rows)
					{
						iLabel66:
						if ((Strings.UCase(Strings.Trim(Grid.TextMatrix(x, COLNAME))) == Strings.UCase(Strings.Trim(strName))) && (Strings.UCase(Strings.Trim(Grid.TextMatrix(x, COLADDR1))) == Strings.UCase(Strings.Trim(strAddr1))))
						{
							iLabel67:
							Grid.RemoveItem(x);
						}
						else
						{
							iLabel68:
							strName = Grid.TextMatrix(x, COLNAME);
							iLabel69:
							strAddr1 = Grid.TextMatrix(x, COLADDR1);
							iLabel70:
							x += 1;
						}
					}
					switch (intOrdBy)
					{
						case 1:
							{
								// account
								Grid.Sort(Grid.Columns[COLACCOUNT], System.ComponentModel.ListSortDirection.Ascending);
								break;
							}
						case 2:
							{
								// name
								Grid.Sort(Grid.Columns[COLNAME], System.ComponentModel.ListSortDirection.Ascending);
								break;
							}
						case 3:
							{
								// location
								Grid.Sort(Grid.Columns[COLLOCNUM], System.ComponentModel.ListSortDirection.Ascending);
								Grid.Sort(Grid.Columns[COLLOCSTREET], System.ComponentModel.ListSortDirection.Ascending);
								break;
							}
						case 4:
							{
								// zip
								Grid.Sort(Grid.Columns[COLZIP], System.ComponentModel.ListSortDirection.Ascending);
								break;
							}
					}
					//end switch
				}
				iLabel79:
				boolDifferentPageSize = false;
				int intIndex;
				int cnt;
				intIndex = labLabels.Get_IndexFromID(intTypeOfLabel);
				if (labLabels.Get_IsDymoLabel(intIndex))
				{
					switch (labLabels.Get_ID(intIndex))
					{
						case modLabels.CNSTLBLTYPEDYMO30256:
							{
								strPrinterName = this.Document.Printer.PrinterName;
								intNumberLines = 12;
								this.Document.Printer.DefaultPageSettings.Landscape = true;
								PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
								PageSettings.Margins.Top = FCConvert.ToSingle(0.128);
								PageSettings.Margins.Bottom = 0;
								PageSettings.Margins.Right = 0;
								PageSettings.Margins.Left = FCConvert.ToSingle(0.128);
								PrintWidth = 3.31F - PageSettings.Margins.Left - PageSettings.Margins.Right;
								intLabelWidth = 4F;
								lngPrintWidth = 4F - PageSettings.Margins.Left - PageSettings.Margins.Right;
								intNumChars = 4 * 10 - 3;
								Detail.Height = (2.3125f) - PageSettings.Margins.Top - PageSettings.Margins.Bottom - 10 / 1440f;
								Detail.ColumnCount = 1;
								PageSettings.PaperHeight = 4;
								PageSettings.PaperWidth = FCConvert.ToSingle(3.31);
								Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
								this.Document.Printer.DefaultPageSettings.Landscape = true;
								PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
								break;
							}
						case modLabels.CNSTLBLTYPEDYMO30252:
							{
								strPrinterName = this.Document.Printer.PrinterName;
								intNumberLines = 6;
								this.Document.Printer.DefaultPageSettings.Landscape = true;
								PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
								PageSettings.Margins.Top = 0.128F;
								PageSettings.Margins.Bottom = 0;
								PageSettings.Margins.Right = 0;
								PageSettings.Margins.Left = 0.128F;
								PrintWidth = FCConvert.ToSingle((3.5) - PageSettings.Margins.Left - PageSettings.Margins.Right);
								intLabelWidth = 3.5F;
								lngPrintWidth = 3.5F - PageSettings.Margins.Left - PageSettings.Margins.Right;
								intNumChars = FCConvert.ToInt32(3.5 * 10 - 3);
								Detail.Height = 1.1F - PageSettings.Margins.Top - PageSettings.Margins.Bottom - 10 / 1440f;
								Detail.ColumnCount = 1;
								PageSettings.PaperHeight = 3.5F;
								PageSettings.PaperWidth = 1.1F;
								Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
								this.Document.Printer.DefaultPageSettings.Landscape = true;
								PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
								break;
							}
					}
				}
				else if (labLabels.Get_IsLaserLabel(intIndex))
				{
					PageSettings.Margins.Top = 0.5F;
					PageSettings.Margins.Top += (sngAdjustment);
					PageSettings.Margins.Left = labLabels.Get_LeftMargin(intIndex);
					PageSettings.Margins.Right = labLabels.Get_RightMargin(intIndex);
					PrintWidth = (labLabels.Get_PageWidth(intIndex) - labLabels.Get_LeftMargin(intIndex) - labLabels.Get_RightMargin(intIndex));
					intLabelWidth = labLabels.Get_LabelWidth(intIndex);
					Detail.Height = labLabels.Get_LabelHeight(intIndex) + labLabels.Get_VerticalSpace(intIndex);
					if (labLabels.Get_LabelsWide(intIndex) > 0)
					{
						Detail.ColumnCount = labLabels.Get_LabelsWide(intIndex);
						Detail.ColumnSpacing = labLabels.Get_HorizontalSpace(intIndex);
					}
					intNumChars = FCConvert.ToInt32(intLabelWidth * 10);
					intNumberLines = FCConvert.ToInt32(Detail.Height * 6);
					lngPrintWidth = PrintWidth;
				}

				strLine = new string[intNumberLines + 1];
				strLabelLine = new string[intNumberLines + 1];
				iLabel142:
				CreateDataFields();
				if (!boolDontShow)
				{
					frmReportViewer.InstancePtr.Init(this, strPrinterName, 0, false, false, "Pages", false, "", "TRIO Software", false, true, "Labels");
				}
				else
				{
					string strTemp = "";
					this.PrintReport(false);
                    // DJW@121014 Added unload line to get terminate event to fire to reset default printer TROPP-75
                    //FC:FINAL:AM:#3591 - don't close the report
                    //this.Close();
					// Me.Run (False)
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In line " + Information.Erl() + " in Init", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			//if (modPPGN.Statics.gboolPrintF2Label)
			//{
			//	Information.Err().Clear();
			//	modPrinting.StartthePrinter_2(false, 0/*this.Document.Printer.hdc*/, this.Document.Printer.PrinterName);
			//	modPrinting.PrintTheHardWay(ref REC, ref strFirstLabel);
			//	modPrinting.DoneThePrinting();
			//	modPPGN.Statics.gboolPrintF2Label = false;
			//	this.Close();
			//}
			modPPGN.Statics.gboolPrintF2Label = false;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//FC:FINAL:MSH - i.issue #1417: grid won't be initialized if report called by using InstancePtr.Run() (without calling Init(...))
			if (this.Grid == null)
			{
				this.Grid = new FCGrid();
				this.Grid.Cols = 21;
				this.Grid.FixedRows = 0;
				this.Grid.Rows = 0;
			}
			ActiveReport_DataInitialize(null, null);
			CreateDataFields();
			int intReturn;
			int const_printtoolid = 0;
			int cnt;
			try
			{
				// On Error GoTo ErrorHandler
				//iLabel1: modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
				// override the print button so the print dialog doesn't come up again
				if (boolDotMatrix)
				{
					const_printtoolid = 9950;
				}
				if (intTypeOfLabel == modLabels.CNSTLBLTYPEDYMO30256 || intTypeOfLabel == modLabels.CNSTLBLTYPEDYMO30252)
				{
					for (cnt = 0; cnt <= this.Document.Printer.PaperSizes.Count - 1; cnt++)
					{
						switch (intTypeOfLabel)
						{
							case modLabels.CNSTLBLTYPEDYMO30252:
								{
									if (Strings.UCase(this.Document.Printer.PaperSizes[cnt].PaperName) == "30252 ADDRESS")
									{
										this.Document.Printer.PaperSize = this.Document.Printer.PaperSizes[cnt];
										this.Document.Printer.DefaultPageSettings.Landscape = true;
										break;
									}
									break;
								}
							case modLabels.CNSTLBLTYPEDYMO30256:
								{
									if (Strings.UCase(this.Document.Printer.PaperSizes[cnt].PaperName) == "30256 SHIPPING")
									{
										this.Document.Printer.PaperSize = this.Document.Printer.PaperSizes[cnt];
										this.Document.Printer.DefaultPageSettings.Landscape = true;
										break;
									}
									break;
								}
						}
						//end switch
					}
					// cnt
				}

				IsReportStartHandled = true;
				boolFirstLabel = true;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In line " + Information.Erl() + " in ReportStart", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void CreateDataFields()
		{
			int intControlNumber;
			GrapeCity.ActiveReports.SectionReportModel.TextBox NewField;
			int intRow;
			int intCol;
			try
			{
				// On Error GoTo ErrorHandler
				// CREATE THE CONTROLS AND SET THE POSITION OF THE CONTROLS
				// this will be module specific
				// cases 0,1,2,5 could be used generically
				// they have 6 lines of data each
				// other cases uses textboxes placed where the data appears
				// Select Case intTypeOfLabel
				for (intRow = 1; intRow <= intNumberLines; intRow++)
				{
					iLabel5:
					NewField = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.TextBox>("txtData" + FCConvert.ToString(intRow));
					iLabel6:
					NewField.CanGrow = false;
					iLabel7:
					//NewField.Name = "txtData" + FCConvert.ToString(intRow);
					iLabel8:
					NewField.Top = (intRow - 1) * 225 / 1440f;
					iLabel9:
					NewField.Left = 144 / 1440f;
					iLabel10:
					NewField.Width = ((lngPrintWidth / Detail.ColumnCount) - ((Detail.ColumnCount - 1) * Detail.ColumnSpacing)) - 145 / 1440f;
					iLabel11:
					NewField.Height = 225 / 1440f;
					iLabel12:
					NewField.Text = string.Empty;
					iLabel13:
					NewField.MultiLine = false;
					iLabel14:
					NewField.WordWrap = true;

				}
				// intRow
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In line " + Information.Erl() + " in CreateDataFields.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{

		}
		
		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (intBlankLabelsLeftToPrint <= 0)
				{
					iLabel1:
					if (lngCurrRow < Grid.Rows)
					{
						//FC:FINAl:SBE - no need to set current row. Throw different thread exception, and it is not used
						//iLabel2: Grid.Row = lngCurrRow;
						// 3        Select Case intTypeOfLabel
						switch (intLabelToPrint)
						{
						// the functions called are all module specific functions
							case 1:
								{
									// address label
									iLabel4:
									MakeMailing();
									break;
								}
							case 2:
								{
									// address with location
									iLabel5:
									MakeAddressLoc();
									break;
								}
							case 3:
								{
									// name location
									iLabel6:
									MakeNameLoc();
									break;
								}
							case 4:
								{
									// assessment
									iLabel7:
									makeAssessmentcard();
									break;
								}
							case 5:
								{
									// Declaration
									iLabel8:
									MakeDeclaration();
									break;
								}
							case 6:
								{
									// custom
									MakeCustom();
									break;
								}
						}
						
						iLabel9:
						if (intDuplicateCount == 0)
						{
							iLabel10:
							lngCurrRow += 1;
							iLabel11:
							intDuplicateCount = intNumberofDuplicates;
						}
						else
						{
							iLabel12:
							intDuplicateCount -= 1;
							iLabel13:
							if (intDuplicateCount < 0)
								intDuplicateCount = 0;
						}
					}
				}
				else
				{
					intBlankLabelsLeftToPrint -= 1;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In line " + Information.Erl() + " in Detail Format");
			}
		}

		private void MakeAddressLoc()
		{
			string strTemp = "";
			string strNam = "";
			string strA1 = "";
			string strA2 = "";
			string strCity = "";
			string strZip = "";
			string strZip4 = "";
			string strST = "";
			// vbPorter upgrade warning: lngAct As int	OnRead(string)
			int lngAct = 0;
			int intChars;
			// vbPorter upgrade warning: strTempNum As string	OnWriteFCConvert.ToInt32(
			string strTempNum = "";
			int intTemp;
			string strLOC = "";
			string strA3 = "";
			try
			{
				// On Error GoTo ErrorHandler
				intChars = intNumChars - 2;
				// sub 2 chars to allow some room
				iLabel1:
				strNam = Grid.TextMatrix(lngCurrRow, COLNAME);
				if (strNam.Length > intChars)
					strNam = Strings.Left(strNam, intChars);
				iLabel2:
				strA1 = Grid.TextMatrix(lngCurrRow, COLADDR1);
				if (strA1.Length > intChars)
					strA1 = Strings.Left(strA1, intChars);
				iLabel5:
				strA2 = Grid.TextMatrix(lngCurrRow, COLADDR2);
				if (strA2.Length > intChars)
					strA2 = Strings.Left(strA2, intChars);
				strA3 = Grid.TextMatrix(lngCurrRow, COLAddr3);
				if (strA3.Length > intChars)
					strA3 = Strings.Left(strA3, intChars);
				iLabel6:
				strCity = Grid.TextMatrix(lngCurrRow, COLCITY);
				iLabel8:
				strZip = Grid.TextMatrix(lngCurrRow, COLZIP);
				iLabel9:
				strZip4 = Grid.TextMatrix(lngCurrRow, COLZIP4);
				if (intChars < 38)
				{
					// label is < 4 inches wide
					strZip4 = "";
				}
				iLabel10:
				strST = Grid.TextMatrix(lngCurrRow, COLSTATE);
				iLabel12:
				lngAct = FCConvert.ToInt32(Grid.TextMatrix(lngCurrRow, COLACCOUNT));
				strLOC = Grid.TextMatrix(lngCurrRow, COLLOCNUM) + " " + Grid.TextMatrix(lngCurrRow, COLLOCSTREET);
				strLOC = Strings.Trim(strLOC);
				strLOC = "LOCATION- " + strLOC;
				if (strLOC.Length > intChars)
					strLOC = Strings.Left(strLOC, intChars);
				iLabel14:
				strLine[2] = "";
				iLabel15:
				strLine[1] = Strings.StrDup(intChars, " ");
				iLabel16:
				strLine[3] = "";
				iLabel17:
				strLine[4] = "";
				iLabel18:
				strLine[5] = "";
				iLabel19:
				strLine[6] = "";
				strTempNum = FCConvert.ToString(lngAct);
				intTemp = strTempNum.Length;
				iLabel20:
				fecherFoundation.Strings.MidSet(ref strLine[1], 1, Strings.Left(strNam + "", intChars));
				iLabel21:
				fecherFoundation.Strings.MidSet(ref strLine[1], intChars - intTemp, intTemp + 1, " " + strTempNum);
				strLine[2] = Strings.Trim(strA1);
				strLine[3] = Strings.Trim(strA2);
				strLine[4] = Strings.Trim(strA3);
				strLine[5] = Strings.Trim(strCity) + " " + strST + "  " + Strings.Trim(strZip + " " + strZip4);
				if (strLine[5].Length > intChars)
				{
                    int tempInt;
                    int intLeft;
                    tempInt = (" " + strST + "  " + (strZip + strZip4).Trim()).Length;
                    intLeft = intChars - tempInt;
                    strLine[5] = strCity.Trim().PadRight(intLeft).Left(intLeft) + " " + strST + " " +
                                 (strZip + strZip4).Trim();
                }
				strLine[6] = strLOC;
				CondenseAndFillData();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In line " + Information.Erl() + " in MakeAddressLoc");
			}
		}

		private void MakeMailing()
		{
			try
			{
				string strTemp = "";
				string strNam = "";
				string strA1 = "";
				string strA2 = "";
				string strA3 = "";
				string strCity = "";
				string strZip = "";
				string strZip4 = "";
				string strST = "";
				int lngAct = 0;
				int intChars;

				string strTempNum = "";
				int intTemp;
				intChars = intNumChars - 2;

				iLabel1:
				strNam = Grid.TextMatrix(lngCurrRow, COLNAME);
				if (strNam.Length > intChars)
					strNam = Strings.Left(strNam, intChars);
				iLabel2:
				strA1 = Grid.TextMatrix(lngCurrRow, COLADDR1);
				if (strA1.Length > intChars)
					strA1 = Strings.Left(strA1, intChars);
				iLabel5:
				strA2 = Grid.TextMatrix(lngCurrRow, COLADDR2);
				if (strA2.Length > intChars)
					strA2 = Strings.Left(strA2, intChars);
				strA3 = Strings.Trim(Grid.TextMatrix(lngCurrRow, COLAddr3));
				if (strA3.Length > intChars)
					strA3 = Strings.Left(strA3, intChars);
				iLabel6:
				strCity = Grid.TextMatrix(lngCurrRow, COLCITY);
				iLabel8:
				strZip = Grid.TextMatrix(lngCurrRow, COLZIP);
				iLabel9:
				strZip4 = Grid.TextMatrix(lngCurrRow, COLZIP4);
				iLabel10:
				strST = Grid.TextMatrix(lngCurrRow, COLSTATE);
				iLabel12:
				lngAct = FCConvert.ToInt32(Grid.TextMatrix(lngCurrRow, COLACCOUNT));
				iLabel14:
				strLine[2] = "";
				iLabel15:
				strLine[1] = Strings.StrDup(intChars, " ");
				iLabel16:
				strLine[3] = "";
				iLabel17:
				strLine[4] = "";
				iLabel18:
				strLine[5] = "";
				iLabel19:
				strLine[6] = "";
				strTempNum = FCConvert.ToString(lngAct);
				intTemp = strTempNum.Length;
				iLabel20:
				fecherFoundation.Strings.MidSet(ref strLine[1], 1, strNam.Length, Strings.Left(strNam + "", intChars));
				iLabel21:
				fecherFoundation.Strings.MidSet(ref strLine[1], intChars - intTemp, intTemp + 1, " " + strTempNum);
				strLine[2] = Strings.Trim(strA1);
				strLine[3] = Strings.Trim(strA2);
				strLine[4] = Strings.Trim(strA3);
				strLine[5] = Strings.Trim(strCity) + " " + strST + "  " + Strings.Trim(strZip + " " + strZip4);
				if (strLine[5].Length > intChars)
				{
                    int tempInt;
                    int intLeft;
                    tempInt = (" " + strST + "  " + (strZip + strZip4).Trim()).Length;
                    intLeft = intChars - tempInt;
                    strLine[5] = strCity.Trim().PadRight(intLeft).Left(intLeft) + " " + strST + " " +
                                 (strZip + strZip4).Trim();
                }
				strLine[1] = " " + strLine[1];
				strLine[2] = " " + strLine[2];
				strLine[3] = " " + strLine[3];
				strLine[4] = " " + strLine[4];
				strLine[5] = " " + strLine[5];
				CondenseAndFillData();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In line " + Information.Erl() + " in MakeMailing", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void makeAssessmentcard()
		{
			try
			{
				// On Error GoTo ErrorHandler
				string strTYear = "";
				string strTax = "";
				string strAct = "";
				string strLeased = "";
				string strItemized = "";
				string strTotal = "";
				int intChars;
				intChars = intNumChars - 2;
				iLabel1:
				strAct = Grid.TextMatrix(lngCurrRow, COLACCOUNT);
				iLabel2:
				strTYear = strTaxYear;
				iLabel3:
				strLeased = Grid.TextMatrix(lngCurrRow, COLLEASEDVAL);
				iLabel4:
				strItemized = Grid.TextMatrix(lngCurrRow, COLITEMIZEDVAL);
				iLabel5:
				strTotal = Grid.TextMatrix(lngCurrRow, COLTOTALVALUE);
				strTax = "E. Tax: " + Grid.TextMatrix(lngCurrRow, COLTAX);
				iLabel6:
				strLine[1] = strTYear + Strings.StrDup(intChars - strTYear.Length, " ");
				if (strLeased != string.Empty)
				{
					iLabel7:
					fecherFoundation.Strings.MidSet(ref strLine[1], 16 - Strings.Len(strLeased), Strings.Len(strLeased), strLeased);
				}
				if (strItemized != string.Empty)
				{
					iLabel8:
					fecherFoundation.Strings.MidSet(ref strLine[1], 30 - Strings.Len(strItemized), Strings.Len(strItemized), strItemized);
				}
				if (strTotal != string.Empty)
				{
					iLabel9:
					fecherFoundation.Strings.MidSet(ref strLine[1], 42 - Strings.Len(strTotal), Strings.Len(strTotal), strTotal);
				}
				iLabel10:
				strLine[2] = strAct;
				if (boolIncETax)
				{
					strLine[2] += Strings.StrDup(intChars - strLine[2].Length, " ");
					fecherFoundation.Strings.MidSet(ref strLine[2], 10, Strings.Len(strTax), strTax);
				}
				iLabel11:
				CondenseAndFillData();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In line " + Information.Erl() + " in MakeAssessmentCard", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void MakeNameLoc()
		{
			try
			{
				// On Error GoTo ErrorHandler
				// module specific function
				string strTemp = "";
				string strNam = "";
				string strLOC = "";
				// vbPorter upgrade warning: lngAct As int	OnRead(string)
				int lngAct = 0;
				int intChars;
				// vbPorter upgrade warning: strTempNum As string	OnWriteFCConvert.ToInt32(
				string strTempNum = "";
				int intTemp;
				intChars = intNumChars - 2;
				// sub 2 chars to allow some room
				iLabel1:
				strNam = Grid.TextMatrix(lngCurrRow, COLNAME);
				if (strNam.Length > intChars)
					strNam = Strings.Left(strNam, intChars);
				strLOC = Grid.TextMatrix(lngCurrRow, COLLOCNUM) + " " + Grid.TextMatrix(lngCurrRow, COLLOCSTREET);
				strLOC = Strings.Trim(strLOC);
				strLOC = "LOCATION- " + strLOC;
				if (strLOC.Length > intChars)
					strLOC = Strings.Left(strLOC, intChars);
				iLabel12:
				lngAct = FCConvert.ToInt32(Grid.TextMatrix(lngCurrRow, COLACCOUNT));
				iLabel14:
				strLine[2] = "";
				iLabel15:
				strLine[1] = Strings.StrDup(intChars, " ");
				iLabel16:
				strLine[3] = "";
				iLabel17:
				strLine[4] = "";
				iLabel18:
				strLine[5] = "";
				iLabel19:
				strLine[6] = "";
				strTempNum = FCConvert.ToString(lngAct);
				intTemp = strTempNum.Length;
				iLabel20:
				fecherFoundation.Strings.MidSet(ref strLine[1], 1, Strings.Left(strNam + "", intChars));
				iLabel21:
				fecherFoundation.Strings.MidSet(ref strLine[1], intChars - intTemp - 1, intTemp + 1, " " + strTempNum);
				strLine[2] = strLOC;
				CondenseAndFillData();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "in line " + Information.Erl() + " in MakeNameLoc", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void MakeDeclaration()
		{
			string strTemp = "";
			string strNam = "";
			string strA1 = "";
			string strA2 = "";
			string strA3 = "";
			string strCity = "";
			string strZip = "";
			string strZip4 = "";
			string strST = "";
			// vbPorter upgrade warning: lngAct As int	OnRead(string)
			int lngAct = 0;
			int intChars;
			// vbPorter upgrade warning: strTempNum As string	OnWriteFCConvert.ToInt32(
			string strTempNum = "";
			int intTemp;
			string strLOC = "";
			try
			{
				// On Error GoTo ErrorHandler
				intChars = intNumChars - 2;
				// sub 2 chars to allow some room
				iLabel1:
				strNam = Grid.TextMatrix(lngCurrRow, COLNAME);
				if (strNam.Length > intChars)
					strNam = Strings.Left(strNam, intChars);
				iLabel2:
				strA1 = Grid.TextMatrix(lngCurrRow, COLADDR1);
				if (strA1.Length > intChars)
					strA1 = Strings.Left(strA1, intChars);
				iLabel5:
				strA2 = Grid.TextMatrix(lngCurrRow, COLADDR2);
				if (strA2.Length > intChars)
					strA2 = Strings.Left(strA2, intChars);
				strA3 = Strings.Trim(Grid.TextMatrix(lngCurrRow, COLAddr3));
				if (strA3.Length > intChars)
					strA3 = Strings.Left(strA3, intChars);
				iLabel6:
				strCity = Grid.TextMatrix(lngCurrRow, COLCITY);
				iLabel8:
				strZip = Grid.TextMatrix(lngCurrRow, COLZIP);
				iLabel9:
				strZip4 = Grid.TextMatrix(lngCurrRow, COLZIP4);
				if (intChars < 38)
				{
					// label is < 4 inches wide
					strZip4 = "";
				}
				iLabel10:
				strST = Grid.TextMatrix(lngCurrRow, COLSTATE);
				iLabel12:
				lngAct = FCConvert.ToInt32(Grid.TextMatrix(lngCurrRow, COLACCOUNT));
				strLOC = Grid.TextMatrix(lngCurrRow, COLLOCNUM) + " " + Grid.TextMatrix(lngCurrRow, COLLOCSTREET);
				strLOC = Strings.Trim(strLOC);
				strLOC = "LOCATION- " + strLOC;
				if (strLOC.Length > intChars)
					strLOC = Strings.Left(strLOC, intChars);
				iLabel14:
				strLine[2] = Strings.StrDup(intChars, " ");
				iLabel15:
				strLine[1] = "";
				iLabel16:
				strLine[3] = Strings.StrDup(intChars, " ");
				iLabel17:
				strLine[4] = "";
				iLabel18:
				strLine[5] = "";
				iLabel19:
				strLine[6] = "";
				strTempNum = FCConvert.ToString(lngAct);
				intTemp = strTempNum.Length;
				if (Strings.Trim(strA3) == "")
				{
					iLabel21:
					fecherFoundation.Strings.MidSet(ref strLine[2], intChars - intTemp - 1, intTemp + 1, " " + strTempNum);
					fecherFoundation.Strings.MidSet(ref strLine[3], 1, Strings.Left(strNam + "", intChars));
					strLine[4] = Strings.Trim(strA1);
					strLine[5] = Strings.Trim(strA2);
				}
				else
				{
					fecherFoundation.Strings.MidSet(ref strLine[2], 1, Strings.Left(strNam + "", intChars));
					fecherFoundation.Strings.MidSet(ref strLine[2], intChars - intTemp - 1, intTemp + 1, " " + strTempNum);
					strLine[3] = Strings.Trim(strA1);
					strLine[4] = Strings.Trim(strA2);
					strLine[5] = Strings.Trim(strA3);
				}
				strLine[6] = Strings.Trim(strCity) + " " + strST + "  " + Strings.Trim(strZip + " " + strZip4);
				if (strLine[6].Length > intChars)
				{
                    int tempInt;
                    int intLeft;
                    tempInt = (" " + strST + "  " + (strZip + strZip4).Trim()).Length;
                    intLeft = intChars - tempInt;
                    strLine[6] = strCity.Trim().PadRight(intLeft).Left(intLeft) + " " + strST + " " +
                                 (strZip + strZip4).Trim();
                }
                
				strLine[1] = strLOC;
				CondenseAndFillData();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In MakeCardLabel", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void MakeCustom()
		{
			string strTemp = "";
			string strNam = "";
			string strA1 = "";
			string strA2 = "";
			string strCity = "";
			string strZip = "";
			string strZip4 = "";
			string strST = "";
			int lngAct = 0;
			int intChars;
			string strTempNum = "";
			int intTemp;
			string strLOC = "";
			int intLine;
			string strOpen1 = "";
			string strOpen2 = "";
			string strValue = "";
			string strTax = "";
			string strA3 = "";
			try
			{
				// On Error GoTo ErrorHandler
				intChars = intNumChars - 2;
				// sub 2 chars to allow some room
				intLine = 1;
				iLabel1:
				strNam = Grid.TextMatrix(lngCurrRow, COLNAME);
				if (strNam.Length > intChars)
					strNam = Strings.Left(strNam, intChars);
				iLabel2:
				strA1 = Grid.TextMatrix(lngCurrRow, COLADDR1);
				if (strA1.Length > intChars)
					strA1 = Strings.Left(strA1, intChars);
				iLabel5:
				strA2 = Grid.TextMatrix(lngCurrRow, COLADDR2);
				if (strA2.Length > intChars)
					strA2 = Strings.Left(strA2, intChars);
				strA3 = Grid.TextMatrix(lngCurrRow, COLAddr3);
				if (strA3.Length > intChars)
					strA3 = Strings.Left(strA3, intChars);
				iLabel6:
				strCity = Grid.TextMatrix(lngCurrRow, COLCITY);
				iLabel8:
				strZip = Grid.TextMatrix(lngCurrRow, COLZIP);
				iLabel9:
				strZip4 = Grid.TextMatrix(lngCurrRow, COLZIP4);
				if (intChars < 38)
				{
					// label is < 4 inches wide
					strZip4 = "";
				}
				strOpen1 = (Grid.TextMatrix(lngCurrRow, COLOPEN1));
				if (strOpen1.Length > intChars)
				{
					strOpen1 = Strings.Left(strOpen1, intChars);
				}
				strOpen2 = Strings.Trim(Grid.TextMatrix(lngCurrRow, COLOPEN2));
				if (strOpen2.Length > intChars)
				{
					strOpen2 = Strings.Left(strOpen2, intChars);
				}
				iLabel10:
				strST = Grid.TextMatrix(lngCurrRow, COLSTATE);
				strValue = Grid.TextMatrix(lngCurrRow, COLTOTALVALUE);
				strTax = Grid.TextMatrix(lngCurrRow, COLTAX);
				iLabel12:
				lngAct = FCConvert.ToInt32(Grid.TextMatrix(lngCurrRow, COLACCOUNT));
				strLOC = Grid.TextMatrix(lngCurrRow, COLLOCNUM) + " " + Grid.TextMatrix(lngCurrRow, COLLOCSTREET);
				strLOC = Strings.Trim(strLOC);
				strLOC = "Location- " + strLOC;
				if (strLOC.Length > intChars)
					strLOC = Strings.Left(strLOC, intChars);
				for (intTemp = 1; intTemp <= Information.UBound(strLine, 1); intTemp++)
				{
					strLine[intTemp] = "";
				}
				// intTemp
				iLabel15:
				strLine[1] = Strings.StrDup(intChars, " ");
				iLabel20:
				fecherFoundation.Strings.MidSet(ref strLine[1], 1, Strings.Left(strNam + "", intChars));
				if (boolIncAcct)
				{
					strTempNum = FCConvert.ToString(lngAct);
					intTemp = strTempNum.Length;
					iLabel21:
					fecherFoundation.Strings.MidSet(ref strLine[1], intChars - intTemp, intTemp + 1, " " + strTempNum);
				}
				intLine += 1;
				if (boolIncAddr)
				{
					strLine[intLine] = Strings.Trim(strA1);
					if (Strings.Trim(strLine[intLine]) != string.Empty)
					{
						intLine += 1;
					}
					strLine[intLine] = Strings.Trim(strA2);
					if (Strings.Trim(strLine[intLine]) != string.Empty)
					{
						intLine += 1;
					}
					if (Strings.Trim(strA3) != string.Empty)
					{
						strLine[intLine] = Strings.Trim(strA3);
						intLine += 1;
					}
					strLine[intLine] = Strings.Trim(strCity) + " " + strST + "  " + Strings.Trim(strZip + " " + strZip4);
					if (strLine[intLine].Length > intChars)
					{
                        int tempInt;
                        int intLeft;
                        tempInt = (" " + strST + "  " + (strZip + strZip4).Trim()).Length;
                        intLeft = intChars - tempInt;
                        strLine[intLine] = strCity.Trim().PadRight(intLeft).Left(intLeft) + " " + strST + " " +
                                     (strZip + strZip4).Trim();
                    }

					if (Strings.Trim(strLine[intLine]) != string.Empty)
					{
						intLine += 1;
					}
				}
				if (intLine <= intNumberLines)
				{
					if (boolIncLoc)
					{
						strLine[intLine] = strLOC;
						if (strLine[intLine].Length > intChars)
						{
							strLine[intLine] = Strings.Left(strLine[intLine], intChars);
						}
						if (Strings.Trim(strLine[intLine]) != string.Empty)
						{
							intLine += 1;
						}
					}
				}
				if (intLine <= intNumberLines)
				{
					if (boolIncOpen1)
					{
						strLine[intLine] = Strings.Trim(strOpen1);
						if (Strings.Trim(strLine[intLine]) != string.Empty)
						{
							intLine += 1;
						}
					}
				}
				if (intLine <= intNumberLines)
				{
					if (boolIncOpen2)
					{
						strLine[intLine] = Strings.Trim(strOpen2);
						if (Strings.Trim(strLine[intLine]) != string.Empty)
						{
							intLine += 1;
						}
					}
				}
				if (intLine <= intNumberLines)
				{
					if (boolIncValue)
					{
						strLine[intLine] = "Value: " + Strings.Trim(strValue);
					}
					if (boolIncETax)
					{
						if (boolIncValue)
						{
							strLine[intLine] += Strings.StrDup(intChars - strLine[intLine].Length, " ");
							fecherFoundation.Strings.MidSet(ref strLine[intLine], intChars - (Strings.Len(Strings.Trim(strTax)) + Strings.Len("E.Tax: ")) + 1, "E.Tax: " + Strings.Trim(strTax));
						}
						else
						{
							strLine[intLine] = "E.Tax: " + Strings.Trim(strTax);
						}
					}
					if (Strings.Trim(strLine[intLine]) != string.Empty)
					{
						intLine += 1;
					}
				}
				CondenseAndFillData();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In line " + Information.Erl() + " in MakeCustom", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void CondenseAndFillData()
		{
			int x;
			int y;
			// condense them
			for (x = (intNumberLines - 1); x >= 1; x--)
			{
				if (Strings.Trim(strLine[x]) == string.Empty)
				{
					for (y = x; y <= intNumberLines - 1; y++)
					{
						strLine[y] = strLine[y + 1];
					}
					// y
					strLine[intNumberLines] = "";
				}
			}
			// x
			for (x = 1; x <= intNumberLines; x++)
			{
				iLabel59:
				(this.Detail.Controls["txtData" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLine[x];
			}
			// x
			intCurrCol += 1;
			if (intCurrCol > Detail.ColumnCount)
				intCurrCol = 1;
			if (boolDotMatrix)
			{
				for (x = 1; x <= intNumberLines; x++)
				{
					if (intCurrCol == 1 && (Detail.ColumnCount == 1))
					{
						strLabelLine[x] = strLine[x];
						if (!boolFirstLabel)
						{
							REC += strLabelLine[x] + "\r\n";
						}
						else
						{
							strFirstLabel += strLabelLine[x] + "\r\n";
						}
					}
					else if (intCurrCol == 1 && (Detail.ColumnCount > 1))
					{
						strLabelLine[x] = Strings.Mid(strLine[x] + Strings.StrDup(intNumChars, " "), 1, intNumChars);
					}
					else if (intCurrCol < Detail.ColumnCount)
					{
						strLabelLine[x] += Strings.Mid(strLine[x] + Strings.StrDup(intNumChars, " "), 1, intNumChars);
					}
					else
					{
						strLabelLine[x] += strLine[x];
						if (!boolFirstLabel)
						{
							REC += strLabelLine[x] + "\r\n";
						}
						else
						{
							strFirstLabel += strLabelLine[x] + "\r\n";
						}
					}
				}
				// x
				if (intCurrCol == Detail.ColumnCount)
				{
					boolFirstLabel = false;
				}
			}
		}

		
	}
}
