﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Runtime.InteropServices;
using Wisej.Core;
using System.IO;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmPickItemFormat.
	/// </summary>
	partial class frmPickItemFormat : BaseForm
	{
		public fecherFoundation.FCComboBox cmbAppend;
		public fecherFoundation.FCLabel lblAppend;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblLine;
		public fecherFoundation.FCTextBox txtYearFirstAssessed;
		public fecherFoundation.FCTextBox txtFactor;
		public fecherFoundation.FCCheckBox chkFirstLine;
		public fecherFoundation.FCCheckBox chkAllDatainQuotes;
		public fecherFoundation.FCCheckBox chkQuotes;
		public fecherFoundation.FCTextBox txtAcct;
		public fecherFoundation.FCGrid Grid1;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCCheckBox chkYearsClaimed;
		public fecherFoundation.FCCheckBox chkBETEExempt;
		public fecherFoundation.FCCheckBox chkReimbursable;
		public fecherFoundation.FCButton cmdPlaceHolder;
		public fecherFoundation.FCCheckBox Check9;
		public fecherFoundation.FCCheckBox Check8;
		public fecherFoundation.FCCheckBox Check7;
		public fecherFoundation.FCCheckBox Check6;
		public fecherFoundation.FCCheckBox Check5;
		public fecherFoundation.FCCheckBox Check4;
		public fecherFoundation.FCCheckBox Check3;
		public fecherFoundation.FCCheckBox Check2;
		public fecherFoundation.FCCheckBox Check1;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel lblLine_2;
		public fecherFoundation.FCLabel lblLine_1;
		public fecherFoundation.FCLabel lblLine_0;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPickItemFormat));
			this.cmbAppend = new fecherFoundation.FCComboBox();
			this.lblAppend = new fecherFoundation.FCLabel();
			this.txtYearFirstAssessed = new fecherFoundation.FCTextBox();
			this.txtFactor = new fecherFoundation.FCTextBox();
			this.chkFirstLine = new fecherFoundation.FCCheckBox();
			this.chkAllDatainQuotes = new fecherFoundation.FCCheckBox();
			this.chkQuotes = new fecherFoundation.FCCheckBox();
			this.txtAcct = new fecherFoundation.FCTextBox();
			this.Grid1 = new fecherFoundation.FCGrid();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.chkYearsClaimed = new fecherFoundation.FCCheckBox();
			this.chkBETEExempt = new fecherFoundation.FCCheckBox();
			this.chkReimbursable = new fecherFoundation.FCCheckBox();
			this.cmdPlaceHolder = new fecherFoundation.FCButton();
			this.Check9 = new fecherFoundation.FCCheckBox();
			this.Check8 = new fecherFoundation.FCCheckBox();
			this.Check7 = new fecherFoundation.FCCheckBox();
			this.Check6 = new fecherFoundation.FCCheckBox();
			this.Check5 = new fecherFoundation.FCCheckBox();
			this.Check4 = new fecherFoundation.FCCheckBox();
			this.Check3 = new fecherFoundation.FCCheckBox();
			this.Check2 = new fecherFoundation.FCCheckBox();
			this.Check1 = new fecherFoundation.FCCheckBox();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.lblLine_2 = new fecherFoundation.FCLabel();
			this.lblLine_1 = new fecherFoundation.FCLabel();
			this.lblLine_0 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuContinue = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdSaveContinue = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkFirstLine)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAllDatainQuotes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkQuotes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkYearsClaimed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkBETEExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkReimbursable)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPlaceHolder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Check9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Check8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Check7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Check6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Check5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Check4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Check3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Check2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Check1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSaveContinue);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			this.BottomPanel.Size = new System.Drawing.Size(996, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtYearFirstAssessed);
			this.ClientArea.Controls.Add(this.txtFactor);
			this.ClientArea.Controls.Add(this.chkFirstLine);
			this.ClientArea.Controls.Add(this.chkAllDatainQuotes);
			this.ClientArea.Controls.Add(this.chkQuotes);
			this.ClientArea.Controls.Add(this.txtAcct);
			this.ClientArea.Controls.Add(this.cmbAppend);
			this.ClientArea.Controls.Add(this.lblAppend);
			this.ClientArea.Controls.Add(this.Grid1);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.Label5);
			this.ClientArea.Controls.Add(this.Label4);
			this.ClientArea.Controls.Add(this.lblLine_2);
			this.ClientArea.Controls.Add(this.lblLine_1);
			this.ClientArea.Controls.Add(this.lblLine_0);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(996, 520);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(996, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(243, 30);
			this.HeaderText.Text = "Pick Itemized Format";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// cmbAppend
			// 
			this.cmbAppend.AutoSize = false;
			this.cmbAppend.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAppend.FormattingEnabled = true;
			this.cmbAppend.Items.AddRange(new object[] {
				"Append to Current Entries",
				"Replace Current Itemized Entries"
			});
			this.cmbAppend.Location = new System.Drawing.Point(503, 749);
			this.cmbAppend.Name = "cmbAppend";
			this.cmbAppend.Size = new System.Drawing.Size(289, 40);
			this.cmbAppend.TabIndex = 17;
			this.cmbAppend.Text = "Append to Current Entries";
			this.ToolTip1.SetToolTip(this.cmbAppend, null);
			// 
			// lblAppend
			// 
			this.lblAppend.AutoSize = true;
			this.lblAppend.Location = new System.Drawing.Point(373, 763);
			this.lblAppend.Name = "lblAppend";
			this.lblAppend.Size = new System.Drawing.Size(78, 15);
			this.lblAppend.TabIndex = 16;
			this.lblAppend.Text = "SELECTION";
			this.ToolTip1.SetToolTip(this.lblAppend, null);
			// 
			// txtYearFirstAssessed
			// 
			this.txtYearFirstAssessed.AutoSize = false;
			this.txtYearFirstAssessed.BackColor = System.Drawing.SystemColors.Window;
			this.txtYearFirstAssessed.LinkItem = null;
			this.txtYearFirstAssessed.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtYearFirstAssessed.LinkTopic = null;
			this.txtYearFirstAssessed.Location = new System.Drawing.Point(500, 93);
			this.txtYearFirstAssessed.Name = "txtYearFirstAssessed";
			this.txtYearFirstAssessed.Size = new System.Drawing.Size(68, 40);
			this.txtYearFirstAssessed.TabIndex = 4;
			this.ToolTip1.SetToolTip(this.txtYearFirstAssessed, null);
			// 
			// txtFactor
			// 
			this.txtFactor.AutoSize = false;
			this.txtFactor.BackColor = System.Drawing.SystemColors.Window;
			this.txtFactor.LinkItem = null;
			this.txtFactor.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtFactor.LinkTopic = null;
			this.txtFactor.Location = new System.Drawing.Point(145, 93);
			this.txtFactor.Name = "txtFactor";
			this.txtFactor.Size = new System.Drawing.Size(63, 40);
			this.txtFactor.TabIndex = 2;
			this.txtFactor.Text = "100";
			this.txtFactor.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtFactor, null);
			// 
			// chkFirstLine
			// 
			this.chkFirstLine.Location = new System.Drawing.Point(30, 153);
			this.chkFirstLine.Name = "chkFirstLine";
			this.chkFirstLine.Size = new System.Drawing.Size(411, 27);
			this.chkFirstLine.TabIndex = 5;
			this.chkFirstLine.Text = "First line is column headers or other unreadable data";
			this.ToolTip1.SetToolTip(this.chkFirstLine, null);
			this.chkFirstLine.CheckedChanged += new System.EventHandler(this.chkFirstLine_CheckedChanged);
			// 
			// chkAllDatainQuotes
			// 
			this.chkAllDatainQuotes.Location = new System.Drawing.Point(783, 153);
			this.chkAllDatainQuotes.Name = "chkAllDatainQuotes";
			this.chkAllDatainQuotes.Size = new System.Drawing.Size(169, 27);
			this.chkAllDatainQuotes.TabIndex = 7;
			this.chkAllDatainQuotes.Text = "All data is in quotes";
			this.ToolTip1.SetToolTip(this.chkAllDatainQuotes, null);
			this.chkAllDatainQuotes.CheckedChanged += new System.EventHandler(this.chkAllDatainQuotes_CheckedChanged);
			// 
			// chkQuotes
			// 
			this.chkQuotes.Location = new System.Drawing.Point(529, 153);
			this.chkQuotes.Name = "chkQuotes";
			this.chkQuotes.Size = new System.Drawing.Size(146, 27);
			this.chkQuotes.TabIndex = 6;
			this.chkQuotes.Text = "Text is in quotes ";
			this.ToolTip1.SetToolTip(this.chkQuotes, null);
			this.chkQuotes.CheckedChanged += new System.EventHandler(this.chkQuotes_CheckedChanged);
			// 
			// txtAcct
			// 
			this.txtAcct.AutoSize = false;
			this.txtAcct.BackColor = System.Drawing.SystemColors.Window;
			this.txtAcct.LinkItem = null;
			this.txtAcct.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAcct.LinkTopic = null;
			this.txtAcct.Location = new System.Drawing.Point(176, 749);
			this.txtAcct.Name = "txtAcct";
			this.txtAcct.Size = new System.Drawing.Size(97, 40);
			this.txtAcct.TabIndex = 15;
			this.ToolTip1.SetToolTip(this.txtAcct, null);
			// 
			// Grid1
			// 
			this.Grid1.AllowBigSelection = false;
			this.Grid1.AllowSelection = false;
			this.Grid1.AllowUserToResizeColumns = false;
			this.Grid1.AllowUserToResizeRows = false;
			this.Grid1.AllowUserToOrderColumns = true;
			this.Grid1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.Grid1.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.Grid1.BackColorAlternate = System.Drawing.Color.Empty;
			this.Grid1.BackColorBkg = System.Drawing.Color.Empty;
			this.Grid1.BackColorFixed = System.Drawing.Color.Empty;
			this.Grid1.BackColorSel = System.Drawing.Color.Empty;
			this.Grid1.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.Grid1.Cols = 12;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.Grid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.Grid1.ColumnHeadersHeight = 30;
			this.Grid1.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.Grid1.DefaultCellStyle = dataGridViewCellStyle2;
			this.Grid1.DragIcon = null;
			this.Grid1.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.Grid1.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExMove;
			this.Grid1.FixedCols = 0;
			this.Grid1.FocusRect = fecherFoundation.FCGrid.FocusRectSettings.flexFocusNone;
			this.Grid1.ForeColorFixed = System.Drawing.Color.Empty;
			this.Grid1.FrozenCols = 0;
			this.Grid1.GridColor = System.Drawing.Color.Empty;
			this.Grid1.GridColorFixed = System.Drawing.Color.Empty;
			this.Grid1.HighLight = fecherFoundation.FCGrid.HighLightSettings.flexHighlightNever;
			this.Grid1.Location = new System.Drawing.Point(30, 567);
			this.Grid1.Name = "Grid1";
			this.Grid1.ReadOnly = true;
			this.Grid1.RowHeadersVisible = false;
			this.Grid1.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.Grid1.RowHeightMin = 0;
			this.Grid1.Rows = 4;
			this.Grid1.ScrollTipText = null;
			this.Grid1.ShowColumnVisibilityMenu = false;
			this.Grid1.ShowFocusCell = false;
			this.Grid1.Size = new System.Drawing.Size(854, 154);
			this.Grid1.StandardTab = true;
			this.Grid1.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.Grid1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.Grid1.TabIndex = 13;
			this.Grid1.ColumnDisplayIndexChanged += new DataGridViewColumnEventHandler(this.Grid1_AfterMoveColumn);
			this.ToolTip1.SetToolTip(this.Grid1, null);
			// 
			// Frame1
			// 
			this.Frame1.AppearanceKey = "groupBoxNoBorders";
			this.Frame1.Controls.Add(this.chkYearsClaimed);
			this.Frame1.Controls.Add(this.chkBETEExempt);
			this.Frame1.Controls.Add(this.chkReimbursable);
			this.Frame1.Controls.Add(this.cmdPlaceHolder);
			this.Frame1.Controls.Add(this.Check9);
			this.Frame1.Controls.Add(this.Check8);
			this.Frame1.Controls.Add(this.Check7);
			this.Frame1.Controls.Add(this.Check6);
			this.Frame1.Controls.Add(this.Check5);
			this.Frame1.Controls.Add(this.Check4);
			this.Frame1.Controls.Add(this.Check3);
			this.Frame1.Controls.Add(this.Check2);
			this.Frame1.Controls.Add(this.Check1);
			this.Frame1.Location = new System.Drawing.Point(30, 186);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(922, 221);
			this.Frame1.TabIndex = 8;
			this.ToolTip1.SetToolTip(this.Frame1, null);
			// 
			// chkYearsClaimed
			// 
			this.chkYearsClaimed.Location = new System.Drawing.Point(678, 124);
			this.chkYearsClaimed.Name = "chkYearsClaimed";
			this.chkYearsClaimed.Size = new System.Drawing.Size(135, 27);
			this.chkYearsClaimed.TabIndex = 11;
			this.chkYearsClaimed.Text = "Years Claimed";
			this.ToolTip1.SetToolTip(this.chkYearsClaimed, null);
			this.chkYearsClaimed.CheckedChanged += new System.EventHandler(this.chkYearsClaimed_CheckedChanged);
			// 
			// chkBETEExempt
			// 
			this.chkBETEExempt.Location = new System.Drawing.Point(678, 77);
			this.chkBETEExempt.Name = "chkBETEExempt";
			this.chkBETEExempt.Size = new System.Drawing.Size(130, 27);
			this.chkBETEExempt.TabIndex = 10;
			this.chkBETEExempt.Text = "BETE Exempt";
			this.ToolTip1.SetToolTip(this.chkBETEExempt, "True/False, T/F, Yes/No, Y/N, *  or Current BETE Year");
			this.chkBETEExempt.CheckedChanged += new System.EventHandler(this.chkBETEExempt_CheckedChanged);
			// 
			// chkReimbursable
			// 
			this.chkReimbursable.Location = new System.Drawing.Point(678, 30);
			this.chkReimbursable.Name = "chkReimbursable";
			this.chkReimbursable.Size = new System.Drawing.Size(129, 27);
			this.chkReimbursable.TabIndex = 9;
			this.chkReimbursable.Text = "Reimbursable";
			this.ToolTip1.SetToolTip(this.chkReimbursable, "True/False, T/F, Yes/No, Y/N or *");
			this.chkReimbursable.CheckedChanged += new System.EventHandler(this.chkReimbursable_CheckedChanged);
			// 
			// cmdPlaceHolder
			// 
			this.cmdPlaceHolder.AppearanceKey = "actionButton";
			this.cmdPlaceHolder.ForeColor = System.Drawing.Color.White;
			this.cmdPlaceHolder.Location = new System.Drawing.Point(20, 171);
			this.cmdPlaceHolder.Name = "cmdPlaceHolder";
			this.cmdPlaceHolder.Size = new System.Drawing.Size(211, 40);
			this.cmdPlaceHolder.TabIndex = 12;
			this.cmdPlaceHolder.Text = "Add Place Holder Field";
			this.ToolTip1.SetToolTip(this.cmdPlaceHolder, null);
			this.cmdPlaceHolder.Click += new System.EventHandler(this.cmdPlaceHolder_Click);
			// 
			// Check9
			// 
			this.Check9.Location = new System.Drawing.Point(433, 124);
			this.Check9.Name = "Check9";
			this.Check9.Size = new System.Drawing.Size(61, 27);
			this.Check9.TabIndex = 8;
			this.Check9.Text = "Cost";
			this.ToolTip1.SetToolTip(this.Check9, null);
			this.Check9.CheckedChanged += new System.EventHandler(this.Check9_CheckedChanged);
			// 
			// Check8
			// 
			this.Check8.Location = new System.Drawing.Point(188, 124);
			this.Check8.Name = "Check8";
			this.Check8.Size = new System.Drawing.Size(163, 27);
			this.Check8.TabIndex = 5;
			this.Check8.Text = "Replacement Year";
			this.ToolTip1.SetToolTip(this.Check8, null);
			this.Check8.CheckedChanged += new System.EventHandler(this.Check8_CheckedChanged);
			// 
			// Check7
			// 
			this.Check7.Location = new System.Drawing.Point(20, 124);
			this.Check7.Name = "Check7";
			this.Check7.Size = new System.Drawing.Size(63, 27);
			this.Check7.TabIndex = 2;
			this.Check7.Text = "SRO";
			this.ToolTip1.SetToolTip(this.Check7, null);
			this.Check7.CheckedChanged += new System.EventHandler(this.Check7_CheckedChanged);
			// 
			// Check6
			// 
			this.Check6.Location = new System.Drawing.Point(433, 77);
			this.Check6.Name = "Check6";
			this.Check6.Size = new System.Drawing.Size(159, 27);
			this.Check6.TabIndex = 7;
			this.Check6.Text = "Depreciation Year";
			this.ToolTip1.SetToolTip(this.Check6, null);
			this.Check6.CheckedChanged += new System.EventHandler(this.Check6_CheckedChanged);
			// 
			// Check5
			// 
			this.Check5.Location = new System.Drawing.Point(188, 77);
			this.Check5.Name = "Check5";
			this.Check5.Size = new System.Drawing.Size(62, 27);
			this.Check5.TabIndex = 4;
			this.Check5.Text = "Year";
			this.ToolTip1.SetToolTip(this.Check5, null);
			this.Check5.CheckedChanged += new System.EventHandler(this.Check5_CheckedChanged);
			// 
			// Check4
			// 
			this.Check4.Location = new System.Drawing.Point(20, 77);
			this.Check4.Name = "Check4";
			this.Check4.Size = new System.Drawing.Size(73, 27);
			this.Check4.TabIndex = 1;
			this.Check4.Text = "Month";
			this.ToolTip1.SetToolTip(this.Check4, null);
			this.Check4.CheckedChanged += new System.EventHandler(this.Check4_CheckedChanged);
			// 
			// Check3
			// 
			this.Check3.Location = new System.Drawing.Point(433, 30);
			this.Check3.Name = "Check3";
			this.Check3.Size = new System.Drawing.Size(109, 27);
			this.Check3.TabIndex = 6;
			this.Check3.Text = "Description";
			this.ToolTip1.SetToolTip(this.Check3, null);
			this.Check3.CheckedChanged += new System.EventHandler(this.Check3_CheckedChanged);
			// 
			// Check2
			// 
			this.Check2.Location = new System.Drawing.Point(188, 30);
			this.Check2.Name = "Check2";
			this.Check2.Size = new System.Drawing.Size(88, 27);
			this.Check2.TabIndex = 3;
			this.Check2.Text = "Quantity";
			this.ToolTip1.SetToolTip(this.Check2, null);
			this.Check2.CheckedChanged += new System.EventHandler(this.Check2_CheckedChanged);
			// 
			// Check1
			// 
			this.Check1.Location = new System.Drawing.Point(20, 30);
			this.Check1.Name = "Check1";
			this.Check1.Size = new System.Drawing.Size(66, 27);
			this.Check1.TabIndex = 0;
			this.Check1.Text = "Code";
			this.ToolTip1.SetToolTip(this.Check1, null);
			this.Check1.CheckedChanged += new System.EventHandler(this.Check1_CheckedChanged);
			// 
			// Label5
			// 
			this.Label5.AutoSize = true;
			this.Label5.Location = new System.Drawing.Point(305, 107);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(150, 15);
			this.Label5.TabIndex = 3;
			this.Label5.Text = "YEAR FIRST ASSESSED";
			this.ToolTip1.SetToolTip(this.Label5, null);
			// 
			// Label4
			// 
			this.Label4.AutoSize = true;
			this.Label4.Location = new System.Drawing.Point(30, 107);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(58, 15);
			this.Label4.TabIndex = 1;
			this.Label4.Text = "FACTOR";
			this.ToolTip1.SetToolTip(this.Label4, null);
			// 
			// lblLine_2
			// 
			this.lblLine_2.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.lblLine_2.Location = new System.Drawing.Point(30, 532);
			this.lblLine_2.Name = "lblLine_2";
			this.lblLine_2.Size = new System.Drawing.Size(854, 15);
			this.lblLine_2.TabIndex = 12;
			this.ToolTip1.SetToolTip(this.lblLine_2, null);
			// 
			// lblLine_1
			// 
			this.lblLine_1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.lblLine_1.Location = new System.Drawing.Point(30, 497);
			this.lblLine_1.Name = "lblLine_1";
			this.lblLine_1.Size = new System.Drawing.Size(854, 15);
			this.lblLine_1.TabIndex = 11;
			this.ToolTip1.SetToolTip(this.lblLine_1, null);
			// 
			// lblLine_0
			// 
			this.lblLine_0.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.lblLine_0.Location = new System.Drawing.Point(30, 462);
			this.lblLine_0.Name = "lblLine_0";
			this.lblLine_0.Size = new System.Drawing.Size(854, 15);
			this.lblLine_0.TabIndex = 10;
			this.ToolTip1.SetToolTip(this.lblLine_0, null);
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(30, 427);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(130, 15);
			this.Label3.TabIndex = 9;
			this.Label3.Text = "EXAMPLE";
			this.ToolTip1.SetToolTip(this.Label3, null);
			// 
			// Label2
			// 
			this.Label2.AutoSize = true;
			this.Label2.Location = new System.Drawing.Point(30, 763);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(90, 15);
			this.Label2.TabIndex = 14;
			this.Label2.Text = "ACCOUNT NO";
			this.ToolTip1.SetToolTip(this.Label2, null);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(922, 31);
			this.Label1.TabIndex = 0;
			this.Label1.Text = resources.GetString("Label1.Text");
			this.ToolTip1.SetToolTip(this.Label1, null);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuContinue,
				this.mnuSepar1,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuContinue
			// 
			this.mnuContinue.Index = 0;
			this.mnuContinue.Name = "mnuContinue";
			this.mnuContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuContinue.Text = "Save & Continue";
			this.mnuContinue.Click += new System.EventHandler(this.mnuContinue_Click);
			// 
			// mnuSepar1
			// 
			this.mnuSepar1.Index = 1;
			this.mnuSepar1.Name = "mnuSepar1";
			this.mnuSepar1.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSaveContinue
			// 
			this.cmdSaveContinue.AppearanceKey = "acceptButton";
			this.cmdSaveContinue.Location = new System.Drawing.Point(280, 30);
			this.cmdSaveContinue.Name = "cmdSaveContinue";
			this.cmdSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSaveContinue.Size = new System.Drawing.Size(150, 48);
			this.cmdSaveContinue.TabIndex = 0;
			this.cmdSaveContinue.Text = "Save & Continue";
			this.ToolTip1.SetToolTip(this.cmdSaveContinue, null);
			this.cmdSaveContinue.Click += new System.EventHandler(this.mnuContinue_Click);
			// 
			// frmPickItemFormat
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(996, 688);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmPickItemFormat";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Pick Itemized Format";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmPickItemFormat_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPickItemFormat_KeyDown);
			this.Resize += new System.EventHandler(this.frmPickItemFormat_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkFirstLine)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAllDatainQuotes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkQuotes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.Frame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkYearsClaimed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkBETEExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkReimbursable)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPlaceHolder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Check9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Check8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Check7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Check6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Check5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Check4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Check3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Check2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Check1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSaveContinue;
	}
}
