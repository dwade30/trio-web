﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmPPGetAccount.
	/// </summary>
	partial class frmPPGetAccount : BaseForm
	{
		public fecherFoundation.FCComboBox cmbContain;
		public fecherFoundation.FCLabel lblContain;
		public fecherFoundation.FCComboBox cmbHidden;
		public fecherFoundation.FCLabel lblHidden;
		public fecherFoundation.FCTextBox txtHold;
		public fecherFoundation.FCButton cmdGetAccountNumber;
		public fecherFoundation.FCTextBox txtGetAccountNumber;
		public fecherFoundation.FCTextBox txtSearch;
		public fecherFoundation.FCButton cmdSearch;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCLabel lblSearchInfo;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblLastAccount;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPPGetAccount));
			this.cmbContain = new fecherFoundation.FCComboBox();
			this.lblContain = new fecherFoundation.FCLabel();
			this.cmbHidden = new fecherFoundation.FCComboBox();
			this.lblHidden = new fecherFoundation.FCLabel();
			this.txtHold = new fecherFoundation.FCTextBox();
			this.cmdGetAccountNumber = new fecherFoundation.FCButton();
			this.txtGetAccountNumber = new fecherFoundation.FCTextBox();
			this.txtSearch = new fecherFoundation.FCTextBox();
			this.cmdSearch = new fecherFoundation.FCButton();
			this.cmdClear = new fecherFoundation.FCButton();
			this.lblSearchInfo = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.lblLastAccount = new fecherFoundation.FCLabel();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdGetAccountNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdGetAccountNumber);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbContain);
			this.ClientArea.Controls.Add(this.lblContain);
			this.ClientArea.Controls.Add(this.cmbHidden);
			this.ClientArea.Controls.Add(this.lblSearchInfo);
			this.ClientArea.Controls.Add(this.lblHidden);
			this.ClientArea.Controls.Add(this.txtSearch);
			this.ClientArea.Controls.Add(this.txtHold);
			this.ClientArea.Controls.Add(this.txtGetAccountNumber);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.lblLastAccount);
			this.ClientArea.Size = new System.Drawing.Size(1078, 520);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdSearch);
			this.TopPanel.Controls.Add(this.cmdClear);
			this.TopPanel.Controls.SetChildIndex(this.cmdClear, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdSearch, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(147, 30);
			this.HeaderText.Text = "Get Account";
			// 
			// cmbContain
			// 
			this.cmbContain.AutoSize = false;
			this.cmbContain.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbContain.FormattingEnabled = true;
			this.cmbContain.Items.AddRange(new object[] {
				"Start with search criteria",
				"Contain search criteria"
			});
			this.cmbContain.Location = new System.Drawing.Point(1128, 60);
			this.cmbContain.Name = "cmbContain";
			this.cmbContain.Size = new System.Drawing.Size(255, 40);
			this.cmbContain.TabIndex = 4;
			//FC:FINAL:CHN - issue #1449: Change default value.
			// this.cmbContain.Text = "Contain search criteria";
			this.cmbContain.Text = "Start with search criteria";
			// 
			// lblContain
			// 
			this.lblContain.AutoSize = true;
			this.lblContain.Location = new System.Drawing.Point(954, 74);
			this.lblContain.Name = "lblContain";
			this.lblContain.Size = new System.Drawing.Size(160, 15);
			this.lblContain.TabIndex = 3;
			this.lblContain.Text = "DISPLAY RECORDS THAT";
			// 
			// cmbHidden
			// 
			this.cmbHidden.AutoSize = false;
			this.cmbHidden.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbHidden.FormattingEnabled = true;
			this.cmbHidden.Items.AddRange(new object[] {
				"Name",
				"Location",
				"",
				"",
				"Business Code"
			});
			this.cmbHidden.Location = new System.Drawing.Point(379, 60);
			this.cmbHidden.Name = "cmbHidden";
			this.cmbHidden.Size = new System.Drawing.Size(255, 40);
			this.cmbHidden.TabIndex = 0;
			this.cmbHidden.Text = "Name";
			// 
			// lblHidden
			// 
			this.lblHidden.AutoSize = true;
			this.lblHidden.Location = new System.Drawing.Point(280, 74);
			this.lblHidden.Name = "lblHidden";
			this.lblHidden.Size = new System.Drawing.Size(79, 15);
			this.lblHidden.TabIndex = 1;
			this.lblHidden.Text = "SEARCH BY";
			// 
			// txtHold
			// 
			this.txtHold.AutoSize = false;
			this.txtHold.BackColor = System.Drawing.SystemColors.Window;
			this.txtHold.LinkItem = null;
			this.txtHold.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtHold.LinkTopic = null;
			this.txtHold.Location = new System.Drawing.Point(124, 122);
			this.txtHold.Name = "txtHold";
			this.txtHold.Size = new System.Drawing.Size(126, 40);
			this.txtHold.TabIndex = 18;
			this.txtHold.Visible = false;
			// 
			// cmdGetAccountNumber
			// 
			this.cmdGetAccountNumber.AppearanceKey = "acceptButton";
			this.cmdGetAccountNumber.Location = new System.Drawing.Point(472, 30);
			this.cmdGetAccountNumber.Name = "cmdGetAccountNumber";
			this.cmdGetAccountNumber.Size = new System.Drawing.Size(156, 48);
			this.cmdGetAccountNumber.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdGetAccountNumber.TabIndex = 1;
			this.cmdGetAccountNumber.Text = "Process";
			this.cmdGetAccountNumber.Click += new System.EventHandler(this.cmdGetAccountNumber_Click);
			// 
			// txtGetAccountNumber
			// 
			this.txtGetAccountNumber.AutoSize = false;
			this.txtGetAccountNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtGetAccountNumber.LinkItem = null;
			this.txtGetAccountNumber.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtGetAccountNumber.LinkTopic = null;
			this.txtGetAccountNumber.Location = new System.Drawing.Point(121, 60);
			this.txtGetAccountNumber.Name = "txtGetAccountNumber";
			this.txtGetAccountNumber.Size = new System.Drawing.Size(126, 40);
			this.txtGetAccountNumber.TabIndex = 0;
			this.txtGetAccountNumber.KeyDown += new Wisej.Web.KeyEventHandler(this.txtGetAccountNumber_KeyDown);
			// 
			// txtSearch
			// 
			this.txtSearch.AutoSize = false;
			this.txtSearch.BackColor = System.Drawing.SystemColors.Window;
			this.txtSearch.LinkItem = null;
			this.txtSearch.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtSearch.LinkTopic = null;
			this.txtSearch.Location = new System.Drawing.Point(666, 61);
			this.txtSearch.Name = "txtSearch";
			this.txtSearch.Size = new System.Drawing.Size(255, 40);
			this.txtSearch.TabIndex = 2;
			this.txtSearch.KeyDown += new Wisej.Web.KeyEventHandler(this.txtSearch_KeyDown);
			// 
			// cmdSearch
			// 
			this.cmdSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdSearch.AppearanceKey = "toolbarButton";
            this.cmdSearch.ImageSource = "button-search";
            this.cmdSearch.Location = new System.Drawing.Point(964, 29);
			this.cmdSearch.Name = "cmdSearch";
			this.cmdSearch.Size = new System.Drawing.Size(81, 24);
			this.cmdSearch.TabIndex = 9;
			this.cmdSearch.Text = "Search";
			this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
			// 
			// cmdClear
			// 
			this.cmdClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdClear.AppearanceKey = "toolbarButton";
			this.cmdClear.Location = new System.Drawing.Point(854, 29);
			this.cmdClear.Name = "cmdClear";
			this.cmdClear.Size = new System.Drawing.Size(98, 24);
			this.cmdClear.TabIndex = 10;
			this.cmdClear.Text = "Clear Search";
			this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
			// 
			// lblSearchInfo
			// 
			this.lblSearchInfo.AutoSize = true;
			this.lblSearchInfo.Location = new System.Drawing.Point(510, 277);
			this.lblSearchInfo.Name = "lblSearchInfo";
			this.lblSearchInfo.Size = new System.Drawing.Size(165, 15);
			this.lblSearchInfo.TabIndex = 14;
			this.lblSearchInfo.Text = "ENTER SEARCH CRITERIA";
			this.lblSearchInfo.Visible = false;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(345, 15);
			this.Label1.TabIndex = 17;
			this.Label1.Text = "ENTER AN ACCOUNT NUMBER OR 0 TO ADD AN ACCOUNT";
			this.Label1.Visible = true;
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 74);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(68, 15);
			this.Label2.TabIndex = 16;
			this.Label2.Text = "ACCOUNT";
			// 
			// lblLastAccount
			// 
			this.lblLastAccount.AutoSize = true;
			this.lblLastAccount.Location = new System.Drawing.Point(324, 82);
			this.lblLastAccount.Name = "lblLastAccount";
			this.lblLastAccount.Size = new System.Drawing.Size(4, 14);
			this.lblLastAccount.TabIndex = 15;
			// 
			// frmPPGetAccount
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmPPGetAccount";
			this.ShowInTaskbar = false;
			this.Text = "Get Account";
			this.Load += new System.EventHandler(this.frmPPGetAccount_Load);
			this.Activated += new System.EventHandler(this.frmPPGetAccount_Activated);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPPGetAccount_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdGetAccountNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
