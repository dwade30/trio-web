﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptBETEAccounts.
	/// </summary>
	public partial class rptBETEAccounts : BaseSectionReport
	{
		public rptBETEAccounts()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "BETE Accounts";
		}

		public static rptBETEAccounts InstancePtr
		{
			get
			{
				return (rptBETEAccounts)Sys.GetInstance(typeof(rptBETEAccounts));
			}
		}

		protected rptBETEAccounts _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsReport_AutoInitialized?.Dispose();
				rsBusinessType_AutoInitialized?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBETEAccounts	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsDRWrapper rsReport = new clsDRWrapper();
		private clsDRWrapper rsReport_AutoInitialized = null;

		private clsDRWrapper rsReport
		{
			get
			{
				if (rsReport_AutoInitialized == null)
				{
					rsReport_AutoInitialized = new clsDRWrapper();
				}
				return rsReport_AutoInitialized;
			}
			set
			{
				rsReport_AutoInitialized = value;
			}
		}

		private double dblTotal;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsDRWrapper rsBusinessType = new clsDRWrapper();
		private clsDRWrapper rsBusinessType_AutoInitialized;

		private clsDRWrapper rsBusinessType
		{
			get
			{
				if (rsBusinessType_AutoInitialized == null)
				{
					rsBusinessType_AutoInitialized = new clsDRWrapper();
				}
				return rsBusinessType_AutoInitialized;
			}
			set
			{
				rsBusinessType_AutoInitialized = value;
			}
		}

		private bool boolisPending;

		public void Init(ref string strSQL, bool boolPending = false)
		{
			rsReport.OpenRecordset(strSQL, modPPGN.strPPDatabase);
			if (rsReport.EndOfFile())
			{
				MessageBox.Show("No records found", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Close();
				return;
			}
			boolisPending = boolPending;
			if (boolPending)
			{
				lblTitle.Text = "Accounts with Pending BETE";
				Label4.Text = "Pending Amount";
			}
			frmReportViewer.InstancePtr.Init(rptObj: this, strFileTitle: "BETE Report", strAttachmentName: "BETERPT");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsReport.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblMuniname.Text = modGlobalConstants.Statics.MuniName;
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			lblPage.Text = "Page 1";
			dblTotal = 0;
			rsBusinessType.OpenRecordset("select * from businesscodes order by businesscode", modPPGN.strPPDatabase);
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + PageNumber;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtTotValue.Text = Strings.Format(dblTotal, "#,###,###,##0");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsReport.EndOfFile())
			{
				//clsDRWrapper rsTemp = new clsDRWrapper();
				txtName.Text = rsReport.Get_Fields_String("name");
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				txtAccount.Text = FCConvert.ToString(rsReport.Get_Fields("account"));
				int x;
				double dblTemp = 0;
				dblTemp = 0;
				if (!boolisPending)
				{
					for (x = 1; x <= 9; x++)
					{
						// TODO Get_Fields: Field [cat] not found!! (maybe it is an alias?)
						dblTemp += Conversion.Val(rsReport.Get_Fields("cat" + FCConvert.ToString(x) + "Exempt"));
					}
					// x
				}
				else
				{
					// TODO Get_Fields: Field [PendingAmount] not found!! (maybe it is an alias?)
					dblTemp = Conversion.Val(rsReport.Get_Fields("PendingAmount"));
				}
				txtAmount.Text = Strings.Format(dblTemp, "#,###,###,##0");
				dblTotal += dblTemp;
				if (rsBusinessType.FindFirstRecord("BusinessCode", Conversion.Val(rsReport.Get_Fields_Int32("businesscode"))))
				{
					txtBusinessCode.Text = rsBusinessType.Get_Fields_String("businesstype");
				}
				else
				{
					txtBusinessCode.Text = rsReport.Get_Fields_String("businesscode");
				}
				rsReport.MoveNext();
			}
		}

		
	}
}
