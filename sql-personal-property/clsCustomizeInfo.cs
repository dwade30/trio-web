﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPP0000
{
	public class clsCustomizeInfo
	{
		//=========================================================
		private int intRound;
		private int intDepreciationYear;
		private bool boolShowRateOnShort;
		private int intRoundContingency;
		private double dblTaxRate;
		private bool boolTrendTimesCost;
		private bool boolShowOriginalTotals;
		private int lngDefaultYearFirstAssessed;
		private bool boolLeasedTrendTimesCost;
		private bool boolLimitPercentGood;
		private bool blRegionalTown;
		private double dlRatio;
		private int lngCurrentTown;
		private modGlobal.TownSpecificCustomize[] TownSpecifics = null;
		private double dblBETEReimburseRate;
		private bool boolUserExtractIncBETE;
		/// <summary>
		/// Private boolAllowPreviousBETEYear As Boolean
		/// </summary>
		private int intCurrentBETEYear;
		// vbPorter upgrade warning: intAccountLabelType As short --> As int	OnWrite(short, string)
		private int intAccountLabelType;
		private int intMunicipalCode;
		private string strMunicipalName = "";
		private bool boolShowOpen1OnCalc;
		private bool boolShowOpen2OnCalc;
		private string strOpen1Description = string.Empty;
		private string strOpen2Description = string.Empty;
		private bool boolRoundIndividualItems;

		public bool RoundIndividualItems
		{
			set
			{
				boolRoundIndividualItems = value;
			}
			get
			{
				bool RoundIndividualItems = false;
				RoundIndividualItems = boolRoundIndividualItems;
				return RoundIndividualItems;
			}
		}

		public string Open1Description
		{
			set
			{
				strOpen1Description = value;
			}
			get
			{
				string Open1Description = "";
				Open1Description = strOpen1Description;
				return Open1Description;
			}
		}

		public string Open2Description
		{
			set
			{
				strOpen2Description = value;
			}
			get
			{
				string Open2Description = "";
				Open2Description = strOpen2Description;
				return Open2Description;
			}
		}

		public bool ShowOpen2OnCalc
		{
			set
			{
				boolShowOpen2OnCalc = value;
			}
			get
			{
				bool ShowOpen2OnCalc = false;
				ShowOpen2OnCalc = boolShowOpen2OnCalc;
				return ShowOpen2OnCalc;
			}
		}

		public bool ShowOpen1OnCalc
		{
			set
			{
				boolShowOpen1OnCalc = value;
			}
			get
			{
				bool ShowOpen1OnCalc = false;
				ShowOpen1OnCalc = boolShowOpen1OnCalc;
				return ShowOpen1OnCalc;
			}
		}

		public string MunicipalName
		{
			get
			{
				string MunicipalName = "";
				if (!blRegionalTown)
				{
					MunicipalName = strMunicipalName;
				}
				else
				{
					MunicipalName = TownSpecifics[lngCurrentTown].MunicipalName;
				}
				return MunicipalName;
			}
		}

		public short MunicipalCode
		{
			set
			{
				if (!blRegionalTown || lngCurrentTown == 0)
				{
					intMunicipalCode = value;
				}
				else
				{
					TownSpecifics[lngCurrentTown].MunicipalCode = FCConvert.ToInt16(value);
				}
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short MunicipalCode = 0;
				if (!blRegionalTown || lngCurrentTown == 0)
				{
					MunicipalCode = FCConvert.ToInt16(intMunicipalCode);
				}
				else
				{
					MunicipalCode = TownSpecifics[lngCurrentTown].MunicipalCode;
				}
				return MunicipalCode;
			}
		}

		public short AccountLabelType
		{
			set
			{
				intAccountLabelType = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short AccountLabelType = 0;
				AccountLabelType = FCConvert.ToInt16(intAccountLabelType);
				return AccountLabelType;
			}
		}

		public bool UserExtractIncBETE
		{
			set
			{
				boolUserExtractIncBETE = value;
			}
			get
			{
				bool UserExtractIncBETE = false;
				UserExtractIncBETE = boolUserExtractIncBETE;
				return UserExtractIncBETE;
			}
		}

		public double BETEReimburseRate
		{
			set
			{
				if (!blRegionalTown || lngCurrentTown == 0)
				{
					dblBETEReimburseRate = value;
				}
				else
				{
					TownSpecifics[lngCurrentTown].BETEReimburseRate = value;
				}
			}
			get
			{
				double BETEReimburseRate = 0;
				if (!blRegionalTown || lngCurrentTown == 0)
				{
					BETEReimburseRate = dblBETEReimburseRate;
				}
				else
				{
					BETEReimburseRate = TownSpecifics[lngCurrentTown].BETEReimburseRate;
				}
				return BETEReimburseRate;
			}
		}

		public int Round
		{
			set
			{
				intRound = value;
			}
			get
			{
				return intRound;
			}
		}

		public int DepreciationYear
		{
			set
			{
				intDepreciationYear = value;
			}
			get
			{
				return intDepreciationYear;
			}
		}

		public bool ShowRateOnShort
		{
			set
			{
				boolShowRateOnShort = value;
			}
			get
			{
				return boolShowRateOnShort;
			}
		}

		public int RoundContingency
		{
			set
			{
				intRoundContingency = value;
			}
			get
			{
				return intRoundContingency;
			}
		}

		public double TaxRate
		{
			set
			{
				if (!blRegionalTown || lngCurrentTown == 0)
				{
					dblTaxRate = value;
				}
				else
				{
					TownSpecifics[lngCurrentTown].TaxRate = value;
				}
			}
			get
			{
				double TaxRate = 0;
				if (!blRegionalTown || lngCurrentTown == 0)
				{
					TaxRate = dblTaxRate;
				}
				else
				{
					TaxRate = TownSpecifics[lngCurrentTown].TaxRate;
				}
				return TaxRate;
			}
		}

		public bool TrendTimesCost
		{
			set
			{
				boolTrendTimesCost = value;
			}
			get
			{
				return boolTrendTimesCost;
			}
		}

		public bool ShowOriginalTotals
		{
			set
			{
				boolShowOriginalTotals = value;
			}
			get
			{
				return boolShowOriginalTotals;
			}
		}

		public int DefaultYearFirstAssessed
		{
			set
			{
				lngDefaultYearFirstAssessed = value;
			}
			get
			{
				return lngDefaultYearFirstAssessed;
			}
		}

		public int CurrentBETEYear
		{
			set
			{
				intCurrentBETEYear = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				return intCurrentBETEYear;
			}
		}
		
		public bool LeasedTrendTimesCost
		{
			set
			{
				boolLeasedTrendTimesCost = value;
			}
			get
			{
				return boolLeasedTrendTimesCost;
			}
		}

		public bool LimitPercentGood
		{
			set
			{
				boolLimitPercentGood = value;
			}
			get
			{
				return boolLimitPercentGood;
			}
		}

		public bool boolRegionalTown
		{
			set
			{
				blRegionalTown = value;
			}
			get
			{
				return blRegionalTown;
			}
		}

		public double dblRatio
		{
			set
			{
				if (!blRegionalTown || lngCurrentTown == 0)
				{
					dlRatio = value;
				}
				else
				{
					TownSpecifics[lngCurrentTown].Ratio = value;
				}
			}
			get
			{
				double dblRatio = 0;
				if (!blRegionalTown || lngCurrentTown == 0)
				{
					dblRatio = dlRatio;
				}
				else
				{
					dblRatio = TownSpecifics[lngCurrentTown].Ratio;
				}
				return dblRatio;
			}
		}

		public int CurrentTown
		{
			set
			{
				lngCurrentTown = value;
			}
			get
			{
				return lngCurrentTown;
			}
		}

		public int EffectiveDepreciationYear
		{
			get
			{
				int EffectiveDepreciationYear = 0;
				if (intDepreciationYear > 0)
				{
					EffectiveDepreciationYear = intDepreciationYear;
				}
				else
				{
					EffectiveDepreciationYear = DateTime.Now.Year;
				}
				return EffectiveDepreciationYear;
			}
		}

		public void LoadCustomInfo()
		{
			try
			{
				// On Error GoTo ErrorHandler
				intAccountLabelType = FCConvert.ToInt32(modRegistry.GetRegistryKey("AccountLabelType", "PP", FCConvert.ToString(modLabels.CNSTLBLTYPE4013)));
				if (modRegionalTown.IsRegionalTown())
				{
					boolRegionalTown = true;
				}
				else
				{
					boolRegionalTown = false;
				}
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select * from ppratioopens", modPPGN.strPPDatabase);
				strOpen1Description = "";
				strOpen2Description = "";
				if (!rsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [ratio] and replace with corresponding Get_Field method
					dlRatio = Conversion.Val(rsLoad.Get_Fields("ratio")) / 100;
					if (dlRatio == 0)
						dlRatio = 1;
					strOpen1Description = Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("openfield1")));
					strOpen2Description = Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("openfield2")));
				}
				else
				{
					dlRatio = 1;
				}
				if (strOpen1Description == "")
				{
					strOpen1Description = "Open 1";
				}
				if (strOpen2Description == "")
				{
					strOpen2Description = "Open 2";
				}
				strMunicipalName = modGlobalConstants.Statics.MuniName;
				rsLoad.OpenRecordset("select * from customize", modPPGN.strPPDatabase);
				if (rsLoad.EndOfFile())
				{
					intDepreciationYear = 0;
					intRoundContingency = 2;
					intRound = 3;
					dblTaxRate = 1;
					boolShowRateOnShort = true;
					boolTrendTimesCost = false;
					boolShowOriginalTotals = false;
					lngDefaultYearFirstAssessed = DateTime.Today.Year;
					boolLeasedTrendTimesCost = true;
					boolLimitPercentGood = false;
					// boolAllowPreviousBETEYear = False
					intCurrentBETEYear = DateTime.Today.Year;
					dblBETEReimburseRate = 1;
					boolUserExtractIncBETE = false;
				}
				else
				{
					intRound = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int16("Round"))));
					boolLimitPercentGood = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("limitpercentgood"));
					lngDefaultYearFirstAssessed = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("defaultyfa"))));
					if (lngDefaultYearFirstAssessed < 1990 || lngDefaultYearFirstAssessed > DateTime.Today.Year + 2)
					{
						lngDefaultYearFirstAssessed = 0;
					}
					intRoundContingency = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int16("roundcontingency"))));
					intDepreciationYear = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int16("depreciationyear"))));
					boolShowRateOnShort = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("showrateonshort"));
					dblTaxRate = Conversion.Val(rsLoad.Get_Fields_Double("taxrate"));
					boolTrendTimesCost = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("trendtimescost"));
					boolLeasedTrendTimesCost = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("leasedtrendtimescost"));
					boolShowOriginalTotals = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("showoriginaltotals"));
					intCurrentBETEYear = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("BETEYear"))));
					if (intCurrentBETEYear < 2007 || intCurrentBETEYear > DateTime.Today.Year + 2)
					{
						intCurrentBETEYear = 0;
					}
					boolLimitPercentGood = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("limitpercentgood"));
					dblBETEReimburseRate = Conversion.Val(rsLoad.Get_Fields_Double("BETEReimburseRate"));
					// boolAllowPreviousBETEYear = rsLoad.Fields("AllowPreviousBETEYear")
					boolUserExtractIncBETE = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("UserExtractIncBETE"));
				}
				cSettingsController contSet = new cSettingsController();
				string strTemp;
				strTemp = contSet.GetSettingValue("ShowOpen1OnCalc", "PersonalProperty", "", "", "");
				if (strTemp != "")
				{
					boolShowOpen1OnCalc = FCConvert.CBool(strTemp);
				}
				else
				{
					boolShowOpen1OnCalc = false;
				}
				strTemp = contSet.GetSettingValue("ShowOpen2OnCalc", "PersonalProperty", "", "", "");
				if (strTemp != "")
				{
					boolShowOpen2OnCalc = FCConvert.CBool(strTemp);
				}
				else
				{
					boolShowOpen2OnCalc = false;
				}
				strTemp = contSet.GetSettingValue("RoundItems", "PersonalProperty", "", "", "");
				if (strTemp != "")
				{
					boolRoundIndividualItems = FCConvert.CBool(strTemp);
				}
				else
				{
					boolRoundIndividualItems = false;
				}
				if (boolRegionalTown)
				{
					int x;
					int intTemp;
					rsLoad.OpenRecordset("select max (townnumber) as maxcode from tblregionS", "CentralData");
					// TODO Get_Fields: Field [maxcode] not found!! (maybe it is an alias?)
					intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("maxcode"))));
					TownSpecifics = new modGlobal.TownSpecificCustomize[intTemp + 1];
					rsLoad.OpenRecordset("select * from townspecific", modPPGN.strPPDatabase);
					while (!rsLoad.EndOfFile())
					{
						//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
						// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
						TownSpecifics[rsLoad.Get_Fields("townnumber")] = new modGlobal.TownSpecificCustomize(0);
						// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Ratio] and replace with corresponding Get_Field method
						TownSpecifics[rsLoad.Get_Fields("townnumber")].Ratio = Conversion.Val(rsLoad.Get_Fields("Ratio"));
						// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
						TownSpecifics[rsLoad.Get_Fields("townnumber")].TaxRate = Conversion.Val(rsLoad.Get_Fields_Double("taxrate"));
						// TODO Get_Fields: Check the table for the column [TownNumber] and replace with corresponding Get_Field method
						TownSpecifics[rsLoad.Get_Fields("TownNumber")].BETEReimburseRate = Conversion.Val(rsLoad.Get_Fields_Double("BETEReimburseRate"));
						// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
						TownSpecifics[rsLoad.Get_Fields("townnumber")].MunicipalCode = FCConvert.ToInt16(Math.Round(Conversion.Val(contSet.GetSetting("MaineMunicipalCode", "", "EnvironmentID", FCConvert.ToString(FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("townnumber"))))), ""))));
						// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
						TownSpecifics[rsLoad.Get_Fields("townnumber")].MunicipalName = modRegionalTown.GetTownKeyName_2(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("townnumber")))));
						rsLoad.MoveNext();
					}
				}
				else
				{
					intMunicipalCode = FCConvert.ToInt32(Math.Round(Conversion.Val(contSet.GetSettingValue("MaineMunicipalCode", "", "EnvironmentID", "0", ""))));
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In LoadCustomInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public bool SaveCustomInfo()
		{
			bool SaveCustomInfo = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsSave = new clsDRWrapper();
				string strSQL = "";
				int x;
				cSettingsController contSet = new cSettingsController();
				modRegistry.SaveRegistryKey("AccountLabelType", FCConvert.ToString(intAccountLabelType), "PP");
				if (boolRegionalTown)
				{
					for (x = 1; x <= Information.UBound(TownSpecifics, 1); x++)
					{
						strSQL = "update townspecific set ratio = " + FCConvert.ToString(TownSpecifics[x].Ratio);
						strSQL += ",taxrate = " + FCConvert.ToString(TownSpecifics[x].TaxRate);
						strSQL += ",BETEReimburseRate = " + FCConvert.ToString(TownSpecifics[x].BETEReimburseRate);
						strSQL += " where townnumber = " + FCConvert.ToString(x);
						rsSave.Execute(strSQL, modPPGN.strPPDatabase);
						contSet.SaveSetting(FCConvert.ToString(TownSpecifics[x].MunicipalCode), "MaineMunicipalCode", "", "EnvironmentID", FCConvert.ToString(x), "");
					}
					// x
				}
				else
				{
					contSet.SaveSetting(FCConvert.ToString(intMunicipalCode), "MaineMunicipalCode", "", "EnvironmentID", FCConvert.ToString(0), "");
				}
				if (boolShowOpen1OnCalc)
				{
					contSet.SaveSetting("True", "ShowOpen1OnCalc", "PersonalProperty", "", "", "");
				}
				else
				{
					contSet.SaveSetting("False", "ShowOpen1OnCalc", "PersonalProperty", "", "", "");
				}
				if (boolShowOpen2OnCalc)
				{
					contSet.SaveSetting("True", "ShowOpen2OnCalc", "PersonalProperty", "", "", "");
				}
				else
				{
					contSet.SaveSetting("False", "ShowOpen2OnCalc", "PersonalProperty", "", "", "");
				}
				if (boolRoundIndividualItems)
				{
					contSet.SaveSetting("True", "RoundItems", "PersonalProperty", "", "", "");
				}
				else
				{
					contSet.SaveSetting("False", "RoundItems", "PersonalProperty", "", "", "");
				}
				strSQL = "select * from customize";
				rsSave.OpenRecordset(strSQL, modPPGN.strPPDatabase);
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
				}
				rsSave.Set_Fields("roundcontingency", intRoundContingency);
				rsSave.Set_Fields("depreciationyear", intDepreciationYear);
				rsSave.Set_Fields("showrateonshort", boolShowRateOnShort);
				rsSave.Set_Fields("taxrate", dblTaxRate);
				rsSave.Set_Fields("trendtimescost", boolTrendTimesCost);
				rsSave.Set_Fields("leasedtrendtimescost", boolLeasedTrendTimesCost);
				rsSave.Set_Fields("showoriginaltotals", boolShowOriginalTotals);
				rsSave.Set_Fields("round", intRound);
				rsSave.Set_Fields("defaultyfa", lngDefaultYearFirstAssessed);
				rsSave.Set_Fields("beteyear", intCurrentBETEYear);
				rsSave.Set_Fields("limitpercentgood", boolLimitPercentGood);
				rsSave.Set_Fields("BETEReimburseRate", dblBETEReimburseRate);
				rsSave.Set_Fields("UserExtractIncBETE", boolUserExtractIncBETE);
				// rsSave.Fields("allowpreviousbeteyear") = boolAllowPreviousBETEYear
				rsSave.Update();
				strSQL = "select * from PPRATIOOPENS";
				rsSave.OpenRecordset(strSQL, modPPGN.strPPDatabase);
				rsSave.Edit();
				rsSave.Set_Fields("ratio", dlRatio * 100);
				rsSave.Set_Fields("openfield1", strOpen1Description);
				rsSave.Set_Fields("openfield2", strOpen2Description);
				rsSave.Update();
				return SaveCustomInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SaveCustomInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveCustomInfo;
		}
	}
}
