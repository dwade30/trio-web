﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Core;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptNameAddressLocAssesOpen.
	/// </summary>
	public partial class rptNameAddressLocAssesOpen : BaseSectionReport
	{
        private clsDRWrapper rsLoad = new clsDRWrapper();

		public rptNameAddressLocAssesOpen()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        private void InitializeComponentEx()
        {
            if (_InstancePtr == null)
                _InstancePtr = this;
            this.Name = "Name Address Location Assessment Open Report";
        }

        public void Init(string strsql)
        {
            rsLoad.OpenRecordset(strsql, "PersonalProperty");
            if (rsLoad.EndOfFile())
            {
                MessageBox.Show("No records found");
                this.Unload();
                return;
            }
            frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "NameAddrLocAssess");
        }

		public static rptNameAddressLocAssesOpen InstancePtr
		{
			get
			{
				return (rptNameAddressLocAssesOpen)Sys.GetInstance(typeof(rptNameAddressLocAssesOpen));
			}
		}

		protected rptNameAddressLocAssesOpen _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}

            if (disposing)
            {
				rsLoad?.Dispose();
                rsLoad = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptNameAddressLocAssesOpen	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private int intPage;
		private int intAccount;
		private bool boolPrint;
		private string strName = "";
		private string strLocation = "";
		private double dblTotal;
		private int lngCount;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
            using (clsDRWrapper clsTemp = new clsDRWrapper())
            {
                string strO1;
                string strO2;
                clsTemp.OpenRecordset("Select * from ppratioopens", "twpp0000.vb1");
                strO1 = FCConvert.ToString(clsTemp.Get_Fields_String("openFIELD1"));
                strO2 = FCConvert.ToString(clsTemp.Get_Fields_String("openfield2"));
                if (Strings.Trim(strO1) == string.Empty)
                    strO1 = "Open 1";
                if (Strings.Trim(strO2) == string.Empty)
                    strO2 = "Open 2";
                Label8.Text = Strings.Trim(strO1);
                Label6.Text = Strings.Trim(strO2);
                txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
                Fields.Add("AccountNumber");
                //modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
            }
        }

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			ShowRecord:
			;
			eArgs.EOF = rsLoad.EndOfFile();
			if (eArgs.EOF)
				return;
			if (FCConvert.ToInt32(rsLoad.GetData("masterAccount")) == intAccount)
			{
				if (!rsLoad.EndOfFile())
				{
					rsLoad.MoveNext();
				}
				goto ShowRecord;
			}
			lngCount += 1;
			txtLocation.Text = rsLoad.GetData("StreetNumber") + rsLoad.Get_Fields_String("streetapt") + " " + rsLoad.GetData("Street");
			txtAccount.Text = rsLoad.GetData("masterAccount").ToString();
			txtName.Text = GetAddress();
			txtAssessment.Text = Strings.Format(rsLoad.GetData("Value"), "###,###,###,##0");
			// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
			dblTotal += Conversion.Val(rsLoad.Get_Fields("value"));
			txtOpen1.Text = rsLoad.GetData("Open1").ToString();
			txtOpen2.Text = rsLoad.GetData("Open2").ToString();
			intAccount = FCConvert.ToInt32(rsLoad.GetData("masterAccount"));
			if (!rsLoad.EndOfFile())
				rsLoad.MoveNext();
			eArgs.EOF = false;
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		private string GetAddress()
		{
			string GetAddress = "";
			string strTemp = "";
			strTemp += rsLoad.GetData("Name") + "\r\n";
			if (Strings.Trim(FCConvert.ToString(rsLoad.GetData("Address1"))) != string.Empty)
			{
				strTemp += rsLoad.GetData("Address1") + "\r\n";
			}
			if (Strings.Trim(FCConvert.ToString(rsLoad.GetData("Address2"))) != string.Empty)
			{
				strTemp += rsLoad.GetData("Address2") + "\r\n";
			}
			strTemp += Strings.Trim(FCConvert.ToString(rsLoad.GetData("City"))) + ", " + rsLoad.GetData("State") + " " + Strings.Trim(FCConvert.ToString(rsLoad.GetData("Zip")));
			if (Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("zip4"))) != string.Empty)
			{
				strTemp += " " + Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("zip4")));
			}
			GetAddress = strTemp;
			return GetAddress;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//this.Printer.RenderMode = 1;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTitle.Text = "Name/Address/Location/Assessment/Opens";
			intPage += 1;
			txtPage.Text = "Page: " + FCConvert.ToString(intPage);
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtCount.Text = Strings.Format(lngCount, "#,###,###,##0");
			txtTotalAssess.Text = Strings.Format(dblTotal, "#,###,###,###,##0");
		}

		
	}
}
