﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Runtime.InteropServices;
using Wisej.Core;
using fecherFoundation.VisualBasicLayer;

namespace TWPP0000
{
	public class modPrinting
	{
		

		public class StaticVariables
		{
			
			
			public clsDRWrapper rsReports_AutoInitialized;

			public clsDRWrapper rsReports
			{
				get
				{
					if (rsReports_AutoInitialized == null)
					{
						rsReports_AutoInitialized = new clsDRWrapper();
					}
					return rsReports_AutoInitialized;
				}
				set
				{
					rsReports_AutoInitialized = value;
				}
			}

			

			public clsDRWrapper rsAudit_AutoInitialized;

			public clsDRWrapper rsAudit
			{
				get
				{
					if (rsAudit_AutoInitialized == null)
					{
						rsAudit_AutoInitialized = new clsDRWrapper();
					}
					return rsAudit_AutoInitialized;
				}
				set
				{
					rsAudit_AutoInitialized = value;
				}
			}
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
