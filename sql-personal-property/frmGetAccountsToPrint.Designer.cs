//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmGetAccountsToPrint.
	/// </summary>
	partial class frmGetAccountsToPrint : BaseForm
	{
		public fecherFoundation.FCGrid destGrid;
		public fecherFoundation.FCGrid srcGrid;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuClear;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSep;
		public fecherFoundation.FCToolStripMenuItem mnuCancel;
		public fecherFoundation.FCToolStripMenuItem mnuSrc;
		public fecherFoundation.FCToolStripMenuItem mnuAdd;
		public fecherFoundation.FCToolStripMenuItem mnuCancel2;
		public fecherFoundation.FCToolStripMenuItem mnuDest;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuCancel3;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle5 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGetAccountsToPrint));
			this.destGrid = new fecherFoundation.FCGrid();
			this.srcGrid = new fecherFoundation.FCGrid();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuClear = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSep = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCancel = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSrc = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAdd = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCancel2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDest = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCancel3 = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSave = new fecherFoundation.FCButton();
			this.cmdClear = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.destGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.srcGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			this.BottomPanel.Size = new System.Drawing.Size(1267, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.destGrid);
			this.ClientArea.Controls.Add(this.srcGrid);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(1267, 520);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdClear);
			this.TopPanel.Size = new System.Drawing.Size(1267, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdClear, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(189, 30);
			this.HeaderText.Text = "Select Accounts";
			// 
			// destGrid
			// 
			this.destGrid.AllowBigSelection = false;
			this.destGrid.AllowSelection = false;
			this.destGrid.AllowUserToResizeColumns = false;
			this.destGrid.AllowUserToResizeRows = false;
			this.destGrid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left)));
			this.destGrid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.destGrid.BackColorAlternate = System.Drawing.Color.Empty;
			this.destGrid.BackColorBkg = System.Drawing.Color.Empty;
			this.destGrid.BackColorFixed = System.Drawing.Color.Empty;
			this.destGrid.BackColorSel = System.Drawing.Color.Empty;
			this.destGrid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.destGrid.Cols = 2;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.destGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.destGrid.ColumnHeadersHeight = 30;
			this.destGrid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.destGrid.DefaultCellStyle = dataGridViewCellStyle2;
			this.destGrid.DragIcon = null;
			this.destGrid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.destGrid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.destGrid.ExtendLastCol = true;
			this.destGrid.FixedCols = 0;
			this.destGrid.ForeColorFixed = System.Drawing.Color.Empty;
			this.destGrid.FrozenCols = 0;
			this.destGrid.GridColor = System.Drawing.Color.Empty;
			this.destGrid.GridColorFixed = System.Drawing.Color.Empty;
			this.destGrid.Location = new System.Drawing.Point(619, 56);
			this.destGrid.Name = "destGrid";
			this.destGrid.RowHeadersVisible = false;
			this.destGrid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.destGrid.RowHeightMin = 0;
			this.destGrid.Rows = 2;
			this.destGrid.ScrollTipText = null;
			this.destGrid.SelectionMode = fecherFoundation.FCGrid.SelectionModeSettings.flexSelectionByRow;
			this.destGrid.ShowColumnVisibilityMenu = false;
			this.destGrid.ShowFocusCell = false;
			this.destGrid.Size = new System.Drawing.Size(543, 459);
			this.destGrid.StandardTab = true;
			this.destGrid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.destGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.destGrid.TabIndex = 1;
			this.destGrid.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.destGrid_AfterEdit);
			this.destGrid.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.destGrid_ValidateEdit);
			this.destGrid.CurrentCellChanged += new System.EventHandler(this.destGrid_RowColChange);
			this.destGrid.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.destGrid_MouseDownEvent);
			this.destGrid.CellMouseUp += new Wisej.Web.DataGridViewCellMouseEventHandler(this.destGrid_MouseUpEvent);
			// 
			// srcGrid
			// 
			this.srcGrid.AllowBigSelection = false;
			this.srcGrid.AllowSelection = false;
			this.srcGrid.AllowUserToResizeColumns = false;
			this.srcGrid.AllowUserToResizeRows = false;
			this.srcGrid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left)));
			this.srcGrid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.srcGrid.BackColorAlternate = System.Drawing.SystemColors.Window;
			this.srcGrid.BackColorBkg = System.Drawing.SystemColors.AppWorkspace;
			this.srcGrid.BackColorFixed = System.Drawing.SystemColors.Control;
			this.srcGrid.BackColorSel = System.Drawing.SystemColors.Highlight;
			this.srcGrid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.srcGrid.Cols = 2;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.srcGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.srcGrid.ColumnHeadersHeight = 30;
			this.srcGrid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.srcGrid.DefaultCellStyle = dataGridViewCellStyle4;
			this.srcGrid.DragIcon = null;
			this.srcGrid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.srcGrid.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.srcGrid.ExtendLastCol = true;
			this.srcGrid.FixedCols = 0;
			this.srcGrid.FocusRect = fecherFoundation.FCGrid.FocusRectSettings.flexFocusNone;
			this.srcGrid.ForeColorFixed = System.Drawing.SystemColors.ControlText;
			this.srcGrid.FrozenCols = 0;
			this.srcGrid.GridColor = System.Drawing.SystemColors.Control;
			this.srcGrid.GridColorFixed = System.Drawing.SystemColors.ControlDark;
			this.srcGrid.Location = new System.Drawing.Point(30, 55);
			this.srcGrid.Name = "srcGrid";
			this.srcGrid.ReadOnly = true;
			this.srcGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
			this.srcGrid.RowHeadersVisible = false;
			this.srcGrid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.srcGrid.RowHeightMin = 0;
			this.srcGrid.Rows = 1;
			this.srcGrid.ScrollTipText = null;
			this.srcGrid.SelectionMode = fecherFoundation.FCGrid.SelectionModeSettings.flexSelectionByRow;
			this.srcGrid.ShowColumnVisibilityMenu = false;
			this.srcGrid.ShowFocusCell = false;
			this.srcGrid.Size = new System.Drawing.Size(543, 459);
			this.srcGrid.StandardTab = true;
			this.srcGrid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.srcGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.srcGrid.TabIndex = 0;
			this.srcGrid.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.srcGrid_MouseDownEvent);
			this.srcGrid.CellMouseUp += new Wisej.Web.DataGridViewCellMouseEventHandler(this.srcGrid_MouseUpEvent);
			this.srcGrid.DoubleClick += new System.EventHandler(this.srcGrid_DblClick);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(619, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(289, 20);
			this.Label1.TabIndex = 2;
			this.Label1.Text = "ACCOUNTS TO PRINT";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuClear,
				this.mnuSepar,
				this.mnuSave,
				this.mnuSep,
				this.mnuCancel
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuClear
			// 
			this.mnuClear.Index = 0;
			this.mnuClear.Name = "mnuClear";
			this.mnuClear.Text = "Clear List";
			this.mnuClear.Click += new System.EventHandler(this.mnuClear_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 1;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuSave
			// 
			this.mnuSave.Index = 2;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSave.Text = "Save & Continue";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuSep
			// 
			this.mnuSep.Index = 3;
			this.mnuSep.Name = "mnuSep";
			this.mnuSep.Text = "-";
			// 
			// mnuCancel
			// 
			this.mnuCancel.Index = 4;
			this.mnuCancel.Name = "mnuCancel";
			this.mnuCancel.Text = "Exit";
			this.mnuCancel.Click += new System.EventHandler(this.mnuCancel_Click);
			// 
			// mnuSrc
			// 
			this.mnuSrc.Index = -1;
			this.mnuSrc.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuAdd,
				this.mnuCancel2
			});
			this.mnuSrc.Name = "mnuSrc";
			this.mnuSrc.Text = "SourceMenu";
			this.mnuSrc.Visible = false;
			// 
			// mnuAdd
			// 
			this.mnuAdd.Index = 0;
			this.mnuAdd.Name = "mnuAdd";
			this.mnuAdd.Text = "Add to List";
			this.mnuAdd.Click += new System.EventHandler(this.mnuAdd_Click);
			// 
			// mnuCancel2
			// 
			this.mnuCancel2.Index = 1;
			this.mnuCancel2.Name = "mnuCancel2";
			this.mnuCancel2.Text = "Cancel";
			this.mnuCancel2.Click += new System.EventHandler(this.mnuCancel2_Click);
			// 
			// mnuDest
			// 
			this.mnuDest.Index = -1;
			this.mnuDest.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuDelete,
				this.mnuCancel3
			});
			this.mnuDest.Name = "mnuDest";
			this.mnuDest.Text = "DestMenu";
			this.mnuDest.Visible = false;
			// 
			// mnuDelete
			// 
			this.mnuDelete.Index = 0;
			this.mnuDelete.Name = "mnuDelete";
			this.mnuDelete.Text = "Delete from List";
			this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// mnuCancel3
			// 
			this.mnuCancel3.Index = 1;
			this.mnuCancel3.Name = "mnuCancel3";
			this.mnuCancel3.Text = "Cancel";
			this.mnuCancel3.Click += new System.EventHandler(this.mnuCancel3_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(583, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(100, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// cmdClear
			// 
			this.cmdClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdClear.AppearanceKey = "toolbarButton";
			this.cmdClear.Location = new System.Drawing.Point(1039, 29);
			this.cmdClear.Name = "cmdClear";
			this.cmdClear.Size = new System.Drawing.Size(73, 24);
			this.cmdClear.TabIndex = 1;
			this.cmdClear.Text = "Clear List";
			this.cmdClear.Click += new System.EventHandler(this.mnuClear_Click);
			// 
			// frmGetAccountsToPrint
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1190, 688);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmGetAccountsToPrint";
			this.ShowInTaskbar = false;
			this.Text = "Select Accounts";
			this.Load += new System.EventHandler(this.frmGetAccountsToPrint_Load);
			this.Activated += new System.EventHandler(this.frmGetAccountsToPrint_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmGetAccountsToPrint_KeyDown);
			this.Resize += new System.EventHandler(this.frmGetAccountsToPrint_Resize);
			//FC:FINAL:CHN - issue #1354: Fix not handled form closing. 
			this.FormClosed += FrmGetAccountsToPrint_FormClosed;
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.destGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.srcGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		private FCButton cmdClear;
	}
}