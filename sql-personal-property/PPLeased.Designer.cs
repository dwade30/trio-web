﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmPPLeased.
	/// </summary>
	partial class frmPPLeased : BaseForm
	{
		public fecherFoundation.FCComboBox cmbTransfer_1;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblStatics;
		public fecherFoundation.FCFrame framTransfer;
		public fecherFoundation.FCComboBox cmbTransferTo;
		public fecherFoundation.FCButton cmdOk;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCButton cmdLeased;
		public fecherFoundation.FCButton cmdItemized;
		public fecherFoundation.FCButton cmdMaster;
		public fecherFoundation.FCButton Command1;
		public fecherFoundation.FCButton Command2;
		public fecherFoundation.FCButton Command3;
		public fecherFoundation.FCButton Command4;
		public fecherFoundation.FCButton Command5;
		public fecherFoundation.FCGrid vs2;
		public fecherFoundation.FCGrid vs1;
		public fecherFoundation.FCGrid GridDeleted;
		public fecherFoundation.FCLabel lblStatics_27;
		public fecherFoundation.FCLabel lblStatics_29;
		public fecherFoundation.FCLabel lblAccount;
		public fecherFoundation.FCLabel lblName;
		public fecherFoundation.FCLabel lblScreenTitle;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuBack;
		public fecherFoundation.FCToolStripMenuItem mnuCalc;
		public fecherFoundation.FCToolStripMenuItem mnuTransfer;
		public fecherFoundation.FCToolStripMenuItem mnuPendingToBETE;
		public fecherFoundation.FCToolStripMenuItem mnuLayout;
		public fecherFoundation.FCToolStripMenuItem mnuAdd;
		public fecherFoundation.FCToolStripMenuItem mnuAddMultipleRows;
		public fecherFoundation.FCToolStripMenuItem mnuRemove;
		public fecherFoundation.FCToolStripMenuItem mnuSP;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveQuit;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuQuitNoSave;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPPLeased));
            this.cmbTransfer_1 = new fecherFoundation.FCComboBox();
            this.framTransfer = new fecherFoundation.FCFrame();
            this.cmbTransferTo = new fecherFoundation.FCComboBox();
            this.cmdOk = new fecherFoundation.FCButton();
            this.cmdCancel = new fecherFoundation.FCButton();
            this.Label1 = new fecherFoundation.FCLabel();
            this.cmdLeased = new fecherFoundation.FCButton();
            this.cmdItemized = new fecherFoundation.FCButton();
            this.cmdMaster = new fecherFoundation.FCButton();
            this.Command1 = new fecherFoundation.FCButton();
            this.Command2 = new fecherFoundation.FCButton();
            this.Command3 = new fecherFoundation.FCButton();
            this.Command4 = new fecherFoundation.FCButton();
            this.Command5 = new fecherFoundation.FCButton();
            this.vs2 = new fecherFoundation.FCGrid();
            this.vs1 = new fecherFoundation.FCGrid();
            this.GridDeleted = new fecherFoundation.FCGrid();
            this.lblStatics_27 = new fecherFoundation.FCLabel();
            this.lblStatics_29 = new fecherFoundation.FCLabel();
            this.lblAccount = new fecherFoundation.FCLabel();
            this.lblName = new fecherFoundation.FCLabel();
            this.lblScreenTitle = new fecherFoundation.FCLabel();
            this.MainMenu1 = new Wisej.Web.MainMenu(this.components);
            this.mnuTransfer = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPendingToBETE = new fecherFoundation.FCToolStripMenuItem();
            this.mnuViewGroupInfo = new fecherFoundation.FCToolStripMenuItem();
            this.mnuLayout = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAdd = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddMultipleRows = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRemove = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuBack = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCalc = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveQuit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuQuitNoSave = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdCalc = new fecherFoundation.FCButton();
            this.cmdRemove = new fecherFoundation.FCButton();
            this.cmdAdd = new fecherFoundation.FCButton();
            this.cmdAddMultiple = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framTransfer)).BeginInit();
            this.framTransfer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdLeased)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdItemized)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMaster)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Command1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Command2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Command3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Command4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Command5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vs2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
            this.vs1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDeleted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCalc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRemove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddMultiple)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 608);
            this.BottomPanel.Size = new System.Drawing.Size(1099, 119);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Command1);
            this.ClientArea.Controls.Add(this.Command2);
            this.ClientArea.Controls.Add(this.Command3);
            this.ClientArea.Controls.Add(this.Command4);
            this.ClientArea.Controls.Add(this.Command5);
            this.ClientArea.Controls.Add(this.vs2);
            this.ClientArea.Controls.Add(this.vs1);
            this.ClientArea.Controls.Add(this.GridDeleted);
            this.ClientArea.Controls.Add(this.lblStatics_27);
            this.ClientArea.Controls.Add(this.lblStatics_29);
            this.ClientArea.Controls.Add(this.lblAccount);
            this.ClientArea.Controls.Add(this.lblName);
            this.ClientArea.Controls.Add(this.lblScreenTitle);
            this.ClientArea.Size = new System.Drawing.Size(1119, 611);
            this.ClientArea.Controls.SetChildIndex(this.lblScreenTitle, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblName, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblAccount, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblStatics_29, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblStatics_27, 0);
            this.ClientArea.Controls.SetChildIndex(this.GridDeleted, 0);
            this.ClientArea.Controls.SetChildIndex(this.vs1, 0);
            this.ClientArea.Controls.SetChildIndex(this.vs2, 0);
            this.ClientArea.Controls.SetChildIndex(this.Command5, 0);
            this.ClientArea.Controls.SetChildIndex(this.Command4, 0);
            this.ClientArea.Controls.SetChildIndex(this.Command3, 0);
            this.ClientArea.Controls.SetChildIndex(this.Command2, 0);
            this.ClientArea.Controls.SetChildIndex(this.Command1, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdMaster);
            this.TopPanel.Controls.Add(this.cmdItemized);
            this.TopPanel.Controls.Add(this.cmdLeased);
            this.TopPanel.Controls.Add(this.cmdCalc);
            this.TopPanel.Controls.Add(this.cmdAdd);
            this.TopPanel.Controls.Add(this.cmdAddMultiple);
            this.TopPanel.Controls.Add(this.cmdRemove);
            this.TopPanel.Size = new System.Drawing.Size(1119, 60);
            this.TopPanel.Controls.SetChildIndex(this.cmdRemove, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddMultiple, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAdd, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdCalc, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdLeased, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdItemized, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdMaster, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(235, 28);
            this.HeaderText.Text = "Account Maintenance";
            // 
            // cmbTransfer_1
            // 
            this.cmbTransfer_1.Items.AddRange(new object[] {
            "Copy Data",
            "Move Data"});
            this.cmbTransfer_1.Location = new System.Drawing.Point(20, 125);
            this.cmbTransfer_1.Name = "cmbTransfer_1";
            this.cmbTransfer_1.Size = new System.Drawing.Size(148, 40);
            this.cmbTransfer_1.TabIndex = 18;
            this.cmbTransfer_1.Text = "Copy Data";
            // 
            // framTransfer
            // 
            this.framTransfer.BackColor = System.Drawing.Color.White;
            this.framTransfer.Controls.Add(this.cmbTransferTo);
            this.framTransfer.Controls.Add(this.cmbTransfer_1);
            this.framTransfer.Controls.Add(this.cmdOk);
            this.framTransfer.Controls.Add(this.cmdCancel);
            this.framTransfer.Controls.Add(this.Label1);
            this.framTransfer.Location = new System.Drawing.Point(434, 67);
            this.framTransfer.Name = "framTransfer";
            this.framTransfer.Size = new System.Drawing.Size(516, 185);
            this.framTransfer.TabIndex = 12;
            this.framTransfer.Text = "Transfer";
            this.framTransfer.Visible = false;
            // 
            // cmbTransferTo
            // 
            this.cmbTransferTo.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.cmbTransferTo.BackColor = System.Drawing.SystemColors.Window;
            this.cmbTransferTo.Location = new System.Drawing.Point(20, 65);
            this.cmbTransferTo.Name = "cmbTransferTo";
            this.cmbTransferTo.Size = new System.Drawing.Size(476, 40);
            this.cmbTransferTo.TabIndex = 17;
            // 
            // cmdOk
            // 
            this.cmdOk.AppearanceKey = "actionButton";
            this.cmdOk.Location = new System.Drawing.Point(337, 125);
            this.cmdOk.Name = "cmdOk";
            this.cmdOk.Size = new System.Drawing.Size(57, 40);
            this.cmdOk.TabIndex = 14;
            this.cmdOk.Text = "Ok";
            this.cmdOk.Click += new System.EventHandler(this.cmdOk_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.AppearanceKey = "actionButton";
            this.cmdCancel.Location = new System.Drawing.Point(413, 125);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(83, 40);
            this.cmdCancel.TabIndex = 13;
            this.cmdCancel.Text = "Cancel";
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(82, 15);
            this.Label1.TabIndex = 18;
            this.Label1.Text = "TRANSFER TO";
            // 
            // cmdLeased
            // 
            this.cmdLeased.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdLeased.Enabled = false;
            this.cmdLeased.Location = new System.Drawing.Point(987, 29);
            this.cmdLeased.Name = "cmdLeased";
            this.cmdLeased.Size = new System.Drawing.Size(104, 24);
            this.cmdLeased.TabIndex = 2;
            this.cmdLeased.Text = "Leased Screen";
            // 
            // cmdItemized
            // 
            this.cmdItemized.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdItemized.Location = new System.Drawing.Point(867, 29);
            this.cmdItemized.Name = "cmdItemized";
            this.cmdItemized.Size = new System.Drawing.Size(114, 24);
            this.cmdItemized.TabIndex = 1;
            this.cmdItemized.Text = "Itemized Screen";
            this.cmdItemized.Click += new System.EventHandler(this.cmdItemized_Click);
            // 
            // cmdMaster
            // 
            this.cmdMaster.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdMaster.Location = new System.Drawing.Point(757, 29);
            this.cmdMaster.Name = "cmdMaster";
            this.cmdMaster.Size = new System.Drawing.Size(104, 24);
            this.cmdMaster.TabIndex = 0;
            this.cmdMaster.Text = "Master Screen";
            this.cmdMaster.Click += new System.EventHandler(this.cmdMaster_Click);
            // 
            // Command1
            // 
            this.Command1.Location = new System.Drawing.Point(205, 458);
            this.Command1.Name = "Command1";
            this.Command1.Size = new System.Drawing.Size(24, 23);
            this.Command1.TabIndex = 9;
            this.Command1.Text = "ADD BOTTOM";
            this.Command1.Visible = false;
            // 
            // Command2
            // 
            this.Command2.Location = new System.Drawing.Point(231, 458);
            this.Command2.Name = "Command2";
            this.Command2.Size = new System.Drawing.Size(24, 23);
            this.Command2.TabIndex = 8;
            this.Command2.Text = "DELETE";
            this.Command2.Visible = false;
            // 
            // Command3
            // 
            this.Command3.Location = new System.Drawing.Point(284, 458);
            this.Command3.Name = "Command3";
            this.Command3.Size = new System.Drawing.Size(24, 23);
            this.Command3.TabIndex = 7;
            this.Command3.Text = "QUIT";
            this.Command3.Visible = false;
            // 
            // Command4
            // 
            this.Command4.Location = new System.Drawing.Point(179, 458);
            this.Command4.Name = "Command4";
            this.Command4.Size = new System.Drawing.Size(24, 23);
            this.Command4.TabIndex = 6;
            this.Command4.Text = "ADD MIDDLE";
            this.Command4.Visible = false;
            // 
            // Command5
            // 
            this.Command5.Location = new System.Drawing.Point(257, 458);
            this.Command5.Name = "Command5";
            this.Command5.Size = new System.Drawing.Size(24, 23);
            this.Command5.TabIndex = 4;
            this.Command5.Text = "REFRESH";
            this.Command5.Visible = false;
            // 
            // vs2
            // 
            this.vs2.Cols = 10;
            this.vs2.Location = new System.Drawing.Point(114, 458);
            this.vs2.Name = "vs2";
            this.vs2.Rows = 50;
            this.vs2.ShowFocusCell = false;
            this.vs2.Size = new System.Drawing.Size(59, 18);
            this.vs2.TabIndex = 5;
            this.vs2.Visible = false;
            // 
            // vs1
            // 
            this.vs1.AllowUserResizing = fecherFoundation.FCGrid.AllowUserResizeSettings.flexResizeColumns;
            this.vs1.AllowUserToResizeColumns = true;
            this.vs1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vs1.Cols = 21;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.WrapMode = Wisej.Web.DataGridViewTriState.True;
            this.vs1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vs1.Controls.Add(this.framTransfer);
            this.vs1.Location = new System.Drawing.Point(30, 110);
            this.vs1.Name = "vs1";
            this.vs1.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.EnableResizing;
            this.vs1.Rows = 50;
            this.vs1.ShowFocusCell = false;
            this.vs1.Size = new System.Drawing.Size(1057, 498);
            this.vs1.StandardTab = false;
            this.vs1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vs1.TabIndex = 5;
            this.vs1.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEdit);
            this.vs1.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vs1_MouseMoveEvent);
            this.vs1.EditingControlShowing += new Wisej.Web.DataGridViewEditingControlShowingEventHandler(this.vs1_EditingControlShowing);
            this.vs1.CellEnter += new Wisej.Web.DataGridViewCellEventHandler(this.vs1_EnterCell);
            this.vs1.CellLeave += new Wisej.Web.DataGridViewCellEventHandler(this.vs1_LeaveCell);
            this.vs1.CurrentCellChanged += new System.EventHandler(this.vs1_RowColChange);
            this.vs1.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vs1_MouseDownEvent);
            this.vs1.KeyDown += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEvent);
            this.vs1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vs1_KeyPressEvent);
            this.vs1.DragDrop += new Wisej.Web.DragEventHandler(this.vs1_DragDrop);
            this.vs1.DragOver += new Wisej.Web.DragEventHandler(this.vs1_DragOver);
            // 
            // GridDeleted
            // 
            this.GridDeleted.ColumnHeadersVisible = false;
            this.GridDeleted.FixedCols = 0;
            this.GridDeleted.FixedRows = 0;
            this.GridDeleted.Location = new System.Drawing.Point(651, 26);
            this.GridDeleted.Name = "GridDeleted";
            this.GridDeleted.RowHeadersVisible = false;
            this.GridDeleted.Rows = 0;
            this.GridDeleted.ShowFocusCell = false;
            this.GridDeleted.Size = new System.Drawing.Size(25, 22);
            this.GridDeleted.TabIndex = 11;
            this.GridDeleted.Visible = false;
            // 
            // lblStatics_27
            // 
            this.lblStatics_27.Location = new System.Drawing.Point(302, 70);
            this.lblStatics_27.Name = "lblStatics_27";
            this.lblStatics_27.Size = new System.Drawing.Size(35, 18);
            this.lblStatics_27.TabIndex = 3;
            this.lblStatics_27.Text = "NAME";
            // 
            // lblStatics_29
            // 
            this.lblStatics_29.Location = new System.Drawing.Point(30, 70);
            this.lblStatics_29.Name = "lblStatics_29";
            this.lblStatics_29.Size = new System.Drawing.Size(69, 18);
            this.lblStatics_29.TabIndex = 1;
            this.lblStatics_29.Text = "ACCOUNT";
            // 
            // lblAccount
            // 
            this.lblAccount.Location = new System.Drawing.Point(156, 70);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(43, 18);
            this.lblAccount.TabIndex = 2;
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(404, 70);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(250, 18);
            this.lblName.TabIndex = 4;
            // 
            // lblScreenTitle
            // 
            this.lblScreenTitle.Location = new System.Drawing.Point(30, 30);
            this.lblScreenTitle.Name = "lblScreenTitle";
            this.lblScreenTitle.Size = new System.Drawing.Size(598, 18);
            this.lblScreenTitle.TabIndex = 0;
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuTransfer,
            this.mnuPendingToBETE,
            this.mnuViewGroupInfo});
            this.MainMenu1.Name = "MainMenu1";
            // 
            // mnuTransfer
            // 
            this.mnuTransfer.Index = 0;
            this.mnuTransfer.Name = "mnuTransfer";
            this.mnuTransfer.Text = "Transfer";
            this.mnuTransfer.Click += new System.EventHandler(this.mnuTransfer_Click);
            // 
            // mnuPendingToBETE
            // 
            this.mnuPendingToBETE.Index = 1;
            this.mnuPendingToBETE.Name = "mnuPendingToBETE";
            this.mnuPendingToBETE.Text = "Mark all pending as BETE exempt";
            this.mnuPendingToBETE.Click += new System.EventHandler(this.mnuPendingToBETE_Click);
            // 
            // mnuViewGroupInfo
            // 
            this.mnuViewGroupInfo.Index = 2;
            this.mnuViewGroupInfo.Name = "mnuViewGroupInfo";
            this.mnuViewGroupInfo.Text = "View Group Information\r\n";
            this.mnuViewGroupInfo.Click += new System.EventHandler(this.mnuViewGroupInfo_Click);
            // 
            // mnuLayout
            // 
            this.mnuLayout.Index = -1;
            this.mnuLayout.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAdd,
            this.mnuAddMultipleRows,
            this.mnuRemove});
            this.mnuLayout.Name = "mnuLayout";
            this.mnuLayout.Text = "Layout";
            // 
            // mnuAdd
            // 
            this.mnuAdd.Index = 0;
            this.mnuAdd.Name = "mnuAdd";
            this.mnuAdd.Shortcut = Wisej.Web.Shortcut.ShiftInsert;
            this.mnuAdd.Text = "&Add Grid Row";
            this.mnuAdd.Click += new System.EventHandler(this.mnuAdd_Click);
            // 
            // mnuAddMultipleRows
            // 
            this.mnuAddMultipleRows.Index = 1;
            this.mnuAddMultipleRows.Name = "mnuAddMultipleRows";
            this.mnuAddMultipleRows.Text = "Add Multiple Grid Rows";
            this.mnuAddMultipleRows.Click += new System.EventHandler(this.mnuAddMultipleRows_Click);
            // 
            // mnuRemove
            // 
            this.mnuRemove.Index = 2;
            this.mnuRemove.Name = "mnuRemove";
            this.mnuRemove.Text = "Remove Grid Row";
            this.mnuRemove.Click += new System.EventHandler(this.mnuRemove_Click);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuBack
            // 
            this.mnuBack.Enabled = false;
            this.mnuBack.Index = -1;
            this.mnuBack.Name = "mnuBack";
            this.mnuBack.Shortcut = Wisej.Web.Shortcut.F7;
            this.mnuBack.Text = "Back to Master Screen";
            this.mnuBack.Visible = false;
            this.mnuBack.Click += new System.EventHandler(this.mnuBack_Click);
            // 
            // mnuCalc
            // 
            this.mnuCalc.Index = -1;
            this.mnuCalc.Name = "mnuCalc";
            this.mnuCalc.Shortcut = Wisej.Web.Shortcut.F6;
            this.mnuCalc.Text = "Calculate";
            this.mnuCalc.Click += new System.EventHandler(this.mnuCalc_Click);
            // 
            // mnuSP
            // 
            this.mnuSP.Index = -1;
            this.mnuSP.Name = "mnuSP";
            this.mnuSP.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = -1;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveQuit
            // 
            this.mnuSaveQuit.Index = -1;
            this.mnuSaveQuit.Name = "mnuSaveQuit";
            this.mnuSaveQuit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveQuit.Text = "Save & Exit";
            this.mnuSaveQuit.Click += new System.EventHandler(this.mnuSaveQuit_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = -1;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuQuitNoSave
            // 
            this.mnuQuitNoSave.Index = -1;
            this.mnuQuitNoSave.Name = "mnuQuitNoSave";
            this.mnuQuitNoSave.Text = "Exit";
            this.mnuQuitNoSave.Click += new System.EventHandler(this.mnuQuitNoSave_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(395, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(100, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // cmdCalc
            // 
            this.cmdCalc.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdCalc.Location = new System.Drawing.Point(681, 29);
            this.cmdCalc.Name = "cmdCalc";
            this.cmdCalc.Shortcut = Wisej.Web.Shortcut.F6;
            this.cmdCalc.Size = new System.Drawing.Size(70, 24);
            this.cmdCalc.TabIndex = 0;
            this.cmdCalc.Text = "Calculate";
            this.cmdCalc.Click += new System.EventHandler(this.mnuCalc_Click);
            // 
            // cmdRemove
            // 
            this.cmdRemove.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdRemove.Location = new System.Drawing.Point(548, 29);
            this.cmdRemove.Name = "cmdRemove";
            this.cmdRemove.Size = new System.Drawing.Size(129, 24);
            this.cmdRemove.TabIndex = 3;
            this.cmdRemove.Text = "Remove Grid Row";
            this.cmdRemove.Click += new System.EventHandler(this.mnuRemove_Click);
            // 
            // cmdAdd
            // 
            this.cmdAdd.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAdd.Location = new System.Drawing.Point(439, 29);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Shortcut = Wisej.Web.Shortcut.ShiftInsert;
            this.cmdAdd.Size = new System.Drawing.Size(103, 24);
            this.cmdAdd.TabIndex = 4;
            this.cmdAdd.Text = "Add Grid Row";
            this.cmdAdd.Click += new System.EventHandler(this.mnuAdd_Click);
            // 
            // cmdAddMultiple
            // 
            this.cmdAddMultiple.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddMultiple.Location = new System.Drawing.Point(114, 29);
            this.cmdAddMultiple.Name = "cmdAddMultiple";
            this.cmdAddMultiple.Size = new System.Drawing.Size(153, 24);
            this.cmdAddMultiple.TabIndex = 5;
            this.cmdAddMultiple.Text = "Add Multiple Grid Rows";
            this.cmdAddMultiple.Click += new System.EventHandler(this.mnuAddMultipleRows_Click);
            // 
            // frmPPLeased
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1119, 671);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmPPLeased";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Account Maintenance";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmPPLeased_Load);
            this.Activated += new System.EventHandler(this.frmPPLeased_Activated);
            this.Resize += new System.EventHandler(this.frmPPLeased_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPPLeased_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPPLeased_KeyPress);
            this.KeyUp += new Wisej.Web.KeyEventHandler(this.frmPPLeased_KeyUp);
            this.DragDrop += new Wisej.Web.DragEventHandler(this.frmPPLeased_DragDrop);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framTransfer)).EndInit();
            this.framTransfer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdLeased)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdItemized)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMaster)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Command1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Command2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Command3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Command4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Command5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vs2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
            this.vs1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridDeleted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCalc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRemove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddMultiple)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		public FCButton cmdCalc;
		public FCButton cmdAdd;
		public FCButton cmdRemove;
		public FCButton cmdAddMultiple;
        private FCToolStripMenuItem mnuViewGroupInfo;
    }
}
