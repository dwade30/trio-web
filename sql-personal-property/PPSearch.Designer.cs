﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmPPSearch.
	/// </summary>
	partial class frmPPSearch : BaseForm
	{
		public fecherFoundation.FCListBox lstPPSearch;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Shape1;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lbl1;
		public fecherFoundation.FCLabel lbl2;
		public fecherFoundation.FCLabel lbl4;
		public fecherFoundation.FCLabel lbl3;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPPSearch));
			this.lstPPSearch = new fecherFoundation.FCListBox();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Shape1 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.lbl1 = new fecherFoundation.FCLabel();
			this.lbl2 = new fecherFoundation.FCLabel();
			this.lbl4 = new fecherFoundation.FCLabel();
			this.lbl3 = new fecherFoundation.FCLabel();
			this.SearchGrid = new fecherFoundation.FCGrid();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.SearchGrid)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdPrint);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.SearchGrid);
			this.ClientArea.Controls.Add(this.lstPPSearch);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Shape1);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.lbl1);
			this.ClientArea.Controls.Add(this.lbl2);
			this.ClientArea.Controls.Add(this.lbl4);
			this.ClientArea.Controls.Add(this.lbl3);
			this.ClientArea.Size = new System.Drawing.Size(1078, 520);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(179, 30);
			this.HeaderText.Text = "Search Results";
			// 
			// lstPPSearch
			// 
			this.lstPPSearch.Appearance = 0;
			this.lstPPSearch.BackColor = System.Drawing.SystemColors.Window;
			this.lstPPSearch.Location = new System.Drawing.Point(30, 133);
			this.lstPPSearch.MultiSelect = 0;
			this.lstPPSearch.Name = "lstPPSearch";
			this.lstPPSearch.Size = new System.Drawing.Size(1018, 18);
			this.lstPPSearch.Sorted = false;
			this.lstPPSearch.TabIndex = 2;
			this.lstPPSearch.Visible = false;
			this.lstPPSearch.KeyDown += new Wisej.Web.KeyEventHandler(this.lstPPSearch_KeyDown);
			this.lstPPSearch.DoubleClick += new System.EventHandler(this.lstPPSearch_DoubleClick);
			// 
			// cmdPrint
			// 
			this.cmdPrint.AppearanceKey = "acceptButton";
			this.cmdPrint.Location = new System.Drawing.Point(46, 30);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Size = new System.Drawing.Size(117, 48);
			this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPrint.TabIndex = 0;
			this.cmdPrint.Text = "Print List";
			this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(77, 100);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(188, 19);
			this.Label2.TabIndex = 8;
			this.Label2.Text = "DELETED ACCOUNT";
			// 
			// Shape1
			// 
			this.Shape1.BackColor = System.Drawing.Color.Black;
			this.Shape1.Location = new System.Drawing.Point(30, 97);
			this.Shape1.Name = "Shape1";
			this.Shape1.Size = new System.Drawing.Size(17, 17);
			this.Shape1.TabIndex = 9;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(694, 37);
			this.Label1.TabIndex = 7;
			this.Label1.Text = "DOUBLE CLICK A SELECTION TO VIEW THAT ACCOUNT. CLICK ON A COLUMN HEADING TO SORT " + "BY THAT COLUMN";
			// 
			// lbl1
			// 
			this.lbl1.AutoSize = true;
			this.lbl1.Location = new System.Drawing.Point(63, 78);
			this.lbl1.Name = "lbl1";
			this.lbl1.Size = new System.Drawing.Size(4, 14);
			this.lbl1.TabIndex = 6;
			// 
			// lbl2
			// 
			this.lbl2.AutoSize = true;
			this.lbl2.Location = new System.Drawing.Point(199, 78);
			this.lbl2.Name = "lbl2";
			this.lbl2.Size = new System.Drawing.Size(4, 14);
			this.lbl2.TabIndex = 5;
			// 
			// lbl4
			// 
			this.lbl4.AutoSize = true;
			this.lbl4.Location = new System.Drawing.Point(471, 78);
			this.lbl4.Name = "lbl4";
			this.lbl4.Size = new System.Drawing.Size(4, 14);
			this.lbl4.TabIndex = 4;
			// 
			// lbl3
			// 
			this.lbl3.AutoSize = true;
			this.lbl3.Location = new System.Drawing.Point(373, 51);
			this.lbl3.Name = "lbl3";
			this.lbl3.Size = new System.Drawing.Size(4, 14);
			this.lbl3.TabIndex = 3;
			// 
			// SearchGrid
			// 
			this.SearchGrid.AllowBigSelection = false;
			this.SearchGrid.AllowSelection = false;
			this.SearchGrid.AllowUserResizing = fecherFoundation.FCGrid.AllowUserResizeSettings.flexResizeColumns;
			this.SearchGrid.AllowUserToResizeRows = false;
			this.SearchGrid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.SearchGrid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.SearchGrid.BackColorAlternate = System.Drawing.Color.Empty;
			this.SearchGrid.BackColorBkg = System.Drawing.Color.Empty;
			this.SearchGrid.BackColorFixed = System.Drawing.Color.Empty;
			this.SearchGrid.BackColorSel = System.Drawing.Color.Empty;
			this.SearchGrid.Cols = 4;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.SearchGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.SearchGrid.ColumnHeadersHeight = 30;
			this.SearchGrid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.SearchGrid.DefaultCellStyle = dataGridViewCellStyle2;
			this.SearchGrid.DragIcon = null;
			this.SearchGrid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.SearchGrid.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.SearchGrid.ExtendLastCol = true;
			this.SearchGrid.FixedCols = 0;
			this.SearchGrid.FocusRect = fecherFoundation.FCGrid.FocusRectSettings.flexFocusNone;
			this.SearchGrid.ForeColorFixed = System.Drawing.Color.Empty;
			this.SearchGrid.FrozenCols = 0;
			this.SearchGrid.GridColor = System.Drawing.Color.Empty;
			this.SearchGrid.GridColorFixed = System.Drawing.Color.Empty;
			this.SearchGrid.Location = new System.Drawing.Point(30, 157);
			this.SearchGrid.Name = "SearchGrid";
			this.SearchGrid.ReadOnly = true;
			this.SearchGrid.RowHeadersVisible = false;
			this.SearchGrid.RowHeightMin = 0;
			this.SearchGrid.Rows = 1;
			this.SearchGrid.ScrollTipText = null;
			this.SearchGrid.SelectionMode = fecherFoundation.FCGrid.SelectionModeSettings.flexSelectionByRow;
			this.SearchGrid.ShowColumnVisibilityMenu = false;
			this.SearchGrid.Size = new System.Drawing.Size(1018, 342);
			this.SearchGrid.StandardTab = true;
			this.SearchGrid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.SearchGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.SearchGrid.TabIndex = 1;
			this.SearchGrid.KeyDown += new Wisej.Web.KeyEventHandler(this.SearchGrid_KeyDownEvent);
			this.SearchGrid.DoubleClick += new System.EventHandler(this.SearchGrid_DblClick);
            // 
            // frmPPSearch
            // 
            this.AcceptButton = this.cmdPrint;
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.FillColor = 0;
			this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmPPSearch";
			this.ShowInTaskbar = false;
			this.Text = "Search Results";
			this.Load += new System.EventHandler(this.frmPPSearch_Load);
			this.Activated += new System.EventHandler(this.frmPPSearch_Activated);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPPSearch_KeyPress);
			this.Resize += new System.EventHandler(this.frmPPSearch_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.SearchGrid)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		public FCGrid SearchGrid;
		public FCButton cmdPrint;
	}
}
