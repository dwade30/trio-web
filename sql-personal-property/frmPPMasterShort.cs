﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;
using fecherFoundation.VisualBasicLayer;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmPPMasterShort.
	/// </summary>
	public partial class frmPPMasterShort : BaseForm
	{
		public frmPPMasterShort()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Label2 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.lblStatics = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.lblCategory = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.Label2.AddControlArrayElement(Label2_7, 7);
			this.Label2.AddControlArrayElement(Label2_5, 5);
			this.Label2.AddControlArrayElement(Label2_3, 3);
			this.Label2.AddControlArrayElement(Label2_9, 9);
			this.Label2.AddControlArrayElement(Label2_4, 4);
			this.Label2.AddControlArrayElement(Label2_0, 0);
			this.Label2.AddControlArrayElement(Label2_1, 1);
			this.Label2.AddControlArrayElement(Label2_2, 2);
			this.lblStatics.AddControlArrayElement(lblStatics_2, 2);
			this.lblStatics.AddControlArrayElement(lblStatics_0, 0);
			this.lblStatics.AddControlArrayElement(lblStatics_14, 14);
			this.lblStatics.AddControlArrayElement(lblStatics_15, 15);
			this.lblStatics.AddControlArrayElement(lblStatics_16, 16);
			this.lblStatics.AddControlArrayElement(lblStatics_17, 17);
			this.lblStatics.AddControlArrayElement(lblStatics_18, 18);
			this.lblStatics.AddControlArrayElement(lblStatics_20, 20);
			this.lblStatics.AddControlArrayElement(lblStatics_29, 29);
			this.lblCategory.AddControlArrayElement(lblCategory_8, 8);
			this.lblCategory.AddControlArrayElement(lblCategory_7, 7);
			this.lblCategory.AddControlArrayElement(lblCategory_6, 6);
			this.lblCategory.AddControlArrayElement(lblCategory_5, 5);
			this.lblCategory.AddControlArrayElement(lblCategory_4, 4);
			this.lblCategory.AddControlArrayElement(lblCategory_3, 3);
			this.lblCategory.AddControlArrayElement(lblCategory_2, 2);
			this.lblCategory.AddControlArrayElement(lblCategory_1, 1);
			this.lblCategory.AddControlArrayElement(lblCategory_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPPMasterShort InstancePtr
		{
			get
			{
				return (frmPPMasterShort)Sys.GetInstance(typeof(frmPPMasterShort));
			}
		}

		protected frmPPMasterShort _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		string strSQL;
		object hold;
		bool Helping;
		/// <summary>
		/// Dim Adding      As Boolean
		/// </summary>
		bool boolLoading;
		bool boolDataChanged;
		// vbPorter upgrade warning: lngExemption1 As int	OnWrite(int, double)
		int lngExemption1;
		// vbPorter upgrade warning: lngExemption2 As int	OnWrite(int, double)
		int lngExemption2;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cPPAccount theAccount = new cPPAccount();
		private cPPAccount theAccount_AutoInitialized;

		private cPPAccount theAccount
		{
			get
			{
				if (theAccount_AutoInitialized == null)
				{
					theAccount_AutoInitialized = new cPPAccount();
				}
				return theAccount_AutoInitialized;
			}
			set
			{
				theAccount_AutoInitialized = value;
			}
		}

		private void frmPPMasterShort_Activated(object sender, System.EventArgs e)
		{
			if (modGNWork.FormExist(this))
				return;
			if (!modSecurity.ValidPermissions_6(this, modSecurity.SHORTMAINTENANCE))
			{
				MessageBox.Show("Your permission setting for Short Maintenance is set to none or is missing.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Close();
				return;
			}
			if (Helping == true)
			{
				Helping = false;
				return;
			}
			// lblScreenTitle.Caption = "Print Account Listing"
			// lblTime.Caption = Format(Time, "h:mm")
			lblOpen1.Text = modPPGN.Statics.Open1Title;
			lblOpen2.Text = modPPGN.Statics.Open2Title;
			// 
			// Set pp = OpenDatabase(DatabaseName, False, False, ";PWD=" & DATABASEPASSWORD)
			// 
			// If CurrentAccount = 9999 Then
			// Adding New Account Show Blank Form
			// Adding = True
			// Call CategoryLabels
			// txtCategory1.Text = 0
			// txtCategory2.Text = 0
			// txtCategory3.Text = 0
			// txtCategory4.Text = 0
			// txtCategory5.Text = 0
			// txtCategory6.Text = 0
			// txtCategory7.Text = 0
			// txtCategory8.Text = 0
			// txtCategory9.Text = 0
			// lblDate.Caption = Format(Date, "MM/dd/yyyy")
			// Else
			// Select CurrentAccount if none.
			// If Not Adding Then
			// Call GetAccount
			// Call CategoryLabels
			// Call CategoryTextBoxes
			// Call FillMasterData
			// Call AddCategoriesforTotalLabel
			// Else
			// lblAccount.Caption = CurrentAccount
			// 
			// End If
			// End If
			// 
			// menuPPMain.Hide
		}

		private void frmPPMasterShort_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				mnuExit_Click();
				return;
			}
			if (frmPPMasterShort.InstancePtr.ActiveControl is FCTextBox)
			{
				if (Shift == 2)
				{
					if (KeyCode == Keys.Left || KeyCode == Keys.Right)
						KeyCode = (Keys)0;
				}
				else
				{
                    if (KeyCode == Keys.Left || KeyCode == Keys.Right)
                        (frmPPMasterShort.InstancePtr.ActiveControl as TextBoxBase).SelectionLength = 0;
				}
				if (KeyCode == Keys.Up)
					Support.SendKeys("+{TAB}", false);
				if (KeyCode == Keys.Down)
					Support.SendKeys("{TAB}", false);
			}
		}

		private void frmPPMasterShort_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// 
			// Escape
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				frmPPMasterShort.InstancePtr.Close();
				return;
				// Return/Enter
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
				// Backspace
			}
			else if (KeyAscii == Keys.Back)
			{
				// If OverType = 1 Then
				// If TypeOf frmPPMasterShort.ActiveControl Is TextBox Then
				// KeyAscii = 0
				// Screen.ActiveControl.SelLength = 0
				// SendKeys "{LEFT}"
				// Screen.ActiveControl.SelLength = 1
				// End If
				// End If
			}
			// 
			if (frmPPMasterShort.InstancePtr.ActiveControl is FCTextBox)
			{
				(frmPPMasterShort.InstancePtr.ActiveControl as TextBoxBase).SelectionLength = modGlobal.Statics.OverType;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmPPMasterShort_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (frmPPMasterShort.InstancePtr.ActiveControl == null)
				return;
			if (frmPPMasterShort.InstancePtr.ActiveControl is FCTextBox)
			{
				//FC:FINAL:DDU:#i1645 - add optional check if activecontrol is null
                //  DSE:#i1435 exception if active control is not TextBox
				if (FCGlobal.Screen.ActiveControl == null)
				{
					return;
				}
				if (KeyCode == Keys.Insert)
					modGlobal.SetInsertMode((frmPPMasterShort.InstancePtr.ActiveControl as TextBoxBase).SelectionLength);
				(frmPPMasterShort.InstancePtr.ActiveControl as TextBoxBase).SelectionLength = modGlobal.Statics.OverType;
				if ((frmPPMasterShort.InstancePtr.ActiveControl as TextBoxBase).SelectionStart == (frmPPMasterShort.InstancePtr.ActiveControl as TextBoxBase).MaxLength)
					Support.SendKeys("{TAB}", false);
			}
		}

		private void CategoryLabels()
		{
			clsDRWrapper rs = new clsDRWrapper();
			strSQL = "SELECT * FROM RatioTrends";
			rs.OpenRecordset(strSQL, modPPGN.strPPDatabase);
			int ii;
			// rs.MoveFirst
			for (ii = 0; ii <= 8; ii++)
			{
				lblCategory[FCConvert.ToInt16(ii)].Text = FCConvert.ToString(rs.Get_Fields_String("Description"));
				rs.MoveNext();
			}
			// ii
		}

		private void AddCategoriesforTotalLabel()
		{
			int holdvalue;
			int BEx;
			BEx = 0;
			holdvalue = 0;
			// strSQL = "SELECT * FROM PPMaster WHERE Account = " & CurrentAccount
			// Set rs = pp.OpenRecordset(strSQL)
			if (Conversion.Val(txtCategory1.Text) != 0)
				holdvalue = FCConvert.ToInt32(FCConvert.ToDouble(txtCategory1.Text));
			if (Conversion.Val(txtCategory2.Text) != 0)
				holdvalue += FCConvert.ToInt32(FCConvert.ToDouble(txtCategory2.Text));
			if (Conversion.Val(txtCategory3.Text) != 0)
				holdvalue += FCConvert.ToInt32(FCConvert.ToDouble(txtCategory3.Text));
			if (Conversion.Val(txtCategory4.Text) != 0)
				holdvalue += FCConvert.ToInt32(FCConvert.ToDouble(txtCategory4.Text));
			if (Conversion.Val(txtCategory5.Text) != 0)
				holdvalue += FCConvert.ToInt32(FCConvert.ToDouble(txtCategory5.Text));
			if (Conversion.Val(txtCategory6.Text) != 0)
				holdvalue += FCConvert.ToInt32(FCConvert.ToDouble(txtCategory6.Text));
			if (Conversion.Val(txtCategory7.Text) != 0)
				holdvalue += FCConvert.ToInt32(FCConvert.ToDouble(txtCategory7.Text));
			if (Conversion.Val(txtCategory8.Text) != 0)
				holdvalue += FCConvert.ToInt32(FCConvert.ToDouble(txtCategory8.Text));
			if (Conversion.Val(txtCategory9.Text) != 0)
				holdvalue += FCConvert.ToInt32(FCConvert.ToDouble(txtCategory9.Text));
			if (Conversion.Val(txtBETE1.Text) > 0)
				BEx = FCConvert.ToInt32(FCConvert.ToDouble(txtBETE1.Text));
			if (Conversion.Val(txtBETE2.Text) > 0)
				BEx += FCConvert.ToInt32(FCConvert.ToDouble(txtBETE2.Text));
			if (Conversion.Val(txtBETE3.Text) > 0)
				BEx += FCConvert.ToInt32(FCConvert.ToDouble(txtBETE3.Text));
			if (Conversion.Val(txtBETE4.Text) > 0)
				BEx += FCConvert.ToInt32(FCConvert.ToDouble(txtBETE4.Text));
			if (Conversion.Val(txtBETE5.Text) > 0)
				BEx += FCConvert.ToInt32(FCConvert.ToDouble(txtBETE5.Text));
			if (Conversion.Val(txtBETE6.Text) > 0)
				BEx += FCConvert.ToInt32(FCConvert.ToDouble(txtBETE6.Text));
			if (Conversion.Val(txtBETE7.Text) > 0)
				BEx += FCConvert.ToInt32(FCConvert.ToDouble(txtBETE7.Text));
			if (Conversion.Val(txtBETE8.Text) > 0)
				BEx += FCConvert.ToInt32(FCConvert.ToDouble(txtBETE8.Text));
			if (Conversion.Val(txtBETE9.Text) > 0)
				BEx += FCConvert.ToInt32(FCConvert.ToDouble(txtBETE9.Text));
			// lblTotal.Caption = Format(rs.Fields("value"), "#,###")
			// If Not rs.EOF Then
			// txtTotal.Text = Format(rs.Fields("value"), "#,###,###,##0")
			// Else
			// txtTotal.Text = "0"
			// End If
			if (modPPGN.Statics.boolShortScreenOnly)
			{
				txtTotal.Text = Strings.Format(holdvalue, "#,###,###,##0");
				// Else
				// If Not rs.EOF Then
				// txtTotal.Text = Format(rs.Fields("value"), "#,###,###,##0")
				// Else
				// txtTotal.Text = Format(holdvalue, "#,###,###,##0")
				// End If
			}
			else
			{
				if (Conversion.Val(txtTotal.Text) > 0)
				{
					holdvalue = FCConvert.ToInt32(FCConvert.ToDouble(txtTotal.Text));
				}
				else
				{
					holdvalue = 0;
				}
			}
			lblTotWithBETE.Text = Strings.Format(BEx + holdvalue, "#,###,###,##0");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			if (Conversion.Val(txtExemption.Text) > 0)
			{
				holdvalue -= FCConvert.ToInt32(FCConvert.ToDouble(txtExemption.Text));
				lblTotExempt.Text = Strings.Format(BEx + FCConvert.ToInt32(FCConvert.ToDouble(txtExemption.Text)), "#,###,###,##0");
			}
			else
			{
				lblTotExempt.Text = Strings.Format(BEx, "#,###,###,##0");
			}
			if (modGlobal.Statics.CustomizeStuff.ShowRateOnShort)
			{
				fraRate.Visible = true;
				// If Not rs.EOF Then
				lblTotalValue.Text = "$" + Strings.Format(holdvalue, "#,###,###,##0");
				// Else
				// lblTotalValue.Caption = "$0"
				// End If
				// lblTaxRate.Caption = "$" & Format(Val(WPP.Fields("TaxRate")) * 1000, "##0.000")
				lblTaxRate.Text = "$" + Strings.Format(modGlobal.Statics.CustomizeStuff.TaxRate * 1000, "##0.000");
				// lblTotalTax.Caption = "$" & Format(RoundDbl(holdvalue * (Val(WPP.Fields("TaxRate") & "")), 2), "#,###,##0.00")
				lblTotalTax.Text = "$" + Strings.Format(modGlobal.RoundDbl(holdvalue * modGlobal.Statics.CustomizeStuff.TaxRate, 2), "#,###,##0.00");
				int holdleft;
				int hold1;
				int hold2;
				int hold3;
				int holdwidth;
				// holdleft = lblTotalValue.Left
				// hold1 = lblTotalValue.Width
				// hold2 = lblTaxRate.Width
				// hold3 = lblTotalTax.Width
				// If hold1 < hold2 Then holdwidth = hold2 Else holdwidth = hold1
				// If holdwidth < hold3 Then holdwidth = hold3
				// lblTotalValue.Width = holdwidth
				// lblTaxRate.Width = holdwidth
				// lblTotalTax.Width = holdwidth
				// lblTotalValue.Left = holdleft
				// lblTaxRate.Left = holdleft
				// lblTotalTax.Left = holdleft
			}
			else
			{
				fraRate.Visible = false;
			}
			FCFileSystem.FileClose(99);
		}

		private void frmPPMasterShort_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPPMasterShort properties;
			//frmPPMasterShort.ScaleWidth	= 9045;
			//frmPPMasterShort.ScaleHeight	= 7350;
			//frmPPMasterShort.LinkTopic	= "Form1";
			//frmPPMasterShort.LockControls	= true;
			//End Unmaped Properties
			if (modPPGN.Statics.boolShortScreenOnly)
				txtTotal.Enabled = false;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			theAccount.Clear();
			lblOpen1.Text = modPPGN.Statics.Open1Title;
			lblOpen2.Text = modPPGN.Statics.Open2Title;
			// 
			// Set pp = OpenDatabase(DatabaseName, False, False, ";PWD=" & DATABASEPASSWORD)
			SetupGridTranCode();
			if (modPPGN.Statics.CurrentAccount == -1)
			{
				// Adding New Account Show Blank Form
				modPPGN.Statics.Adding = true;
				CategoryLabels();
				txtCategory1.Text = FCConvert.ToString(0);
				txtCategory2.Text = FCConvert.ToString(0);
				txtCategory3.Text = FCConvert.ToString(0);
				txtCategory4.Text = FCConvert.ToString(0);
				txtCategory5.Text = FCConvert.ToString(0);
				txtCategory6.Text = FCConvert.ToString(0);
				txtCategory7.Text = FCConvert.ToString(0);
				txtCategory8.Text = FCConvert.ToString(0);
				txtCategory9.Text = FCConvert.ToString(0);
				lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			}
			else
			{
				// Select CurrentAccount if none.
				if (!modPPGN.Statics.Adding)
				{
					GetAccount();
					CategoryLabels();
					CategoryTextBoxes();
					FillMasterData();
					AddCategoriesforTotalLabel();
					boolDataChanged = false;
				}
				else
				{
					lblAccount.Text = FCConvert.ToString(modPPGN.Statics.CurrentAccount);
					boolDataChanged = true;
				}
			}
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (boolDataChanged)
			{
				// vbPorter upgrade warning: intret As short, int --> As DialogResult
				DialogResult intret;
				intret = MessageBox.Show("Data has been changed. Do you want to save changes?", "Save Changes?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
				if (intret == DialogResult.Yes)
				{
					if (!SaveAccount())
					{
						e.Cancel = true;
					}
				}
				else if (intret == DialogResult.Cancel)
				{
					e.Cancel = true;
				}
			}
		}

		private void frmPPMasterShort_Resize(object sender, System.EventArgs e)
		{
			ResizeGridTranCode();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			const int curOnErrorGoToLabel_Default = 0;
			const int curOnErrorGoToLabel_ErrorHandler = 1;
			int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
			try
			{
				modGlobal.SaveWindowSize(this);
				if (modPPGN.Statics.boolFromBilling)
				{
					// establish a connection and send the new data back
					string strMessage = "";
					bool boolOR;
					int lngExempt = 0;
					int lngTotal = 0;
					if (Conversion.Val(txtTotal.Text) > 0)
					{
						lngTotal = FCConvert.ToInt32(FCConvert.ToDouble(txtTotal.Text));
					}
					else
					{
						lngTotal = 0;
					}
					lngExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(txtExemption.Text)));
					MDIParent.InstancePtr.txtCommDest.LinkMode = 0;
					MDIParent.InstancePtr.txtCommDest.LinkTopic = "TWBL0000|frmPPAudit";
					MDIParent.InstancePtr.txtCommDest.LinkItem = "txtcommunicate";
					MDIParent.InstancePtr.txtCommDest.LinkMode = LinkModeConstants.VbLinkAutomatic;
					strMessage = FCConvert.ToString(lngTotal) + "," + FCConvert.ToString(lngExempt) + ",";
					strMessage += FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(txtCategory1.Text))) + "," + FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(txtCategory2.Text))) + "," + FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(txtCategory3.Text))) + ",";
					strMessage += FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(txtCategory4.Text))) + "," + FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(txtCategory5.Text))) + "," + FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(txtCategory4.Text))) + ",";
					strMessage += FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(txtCategory7.Text))) + "," + FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(txtCategory8.Text))) + "," + FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(txtCategory9.Text)));
					/*? On Error Resume Next  */// because we don't care if it takes too long to process the sent message
					// and any errors should be taken care of in billing
					modPPGN.Statics.boolFromBilling = false;
					//MDIParent.InstancePtr.Enabled = true;
					//MDIParent.InstancePtr.Show();
					////Application.DoEvents();
					MDIParent.InstancePtr.txtCommDest.LinkExecute(strMessage);
					Information.Err().Clear();
					vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler;
					/* On Error GoTo ErrorHandler */
					//MDIParent.InstancePtr.Enabled = true;
				}
				else
				{
					//MDIParent.InstancePtr.Show();
				}
				return;
				ErrorHandler:
				;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err().Number) + "  " + Information.Err().Description + "\r\n" + "In Unload", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			catch (Exception ex)
			{
				switch (vOnErrorGoToLabel)
				{
					default:
					case curOnErrorGoToLabel_Default:
						// ...
						break;
					case curOnErrorGoToLabel_ErrorHandler:
						//? goto ErrorHandler;
						break;
				}
			}
		}

		private void gridTranCode_CellChanged(object sender, DataGridViewCellEventArgs e)
		{
			boolDataChanged = true;
		}

		private void gridTranCode_ComboCloseUp(object sender, EventArgs e)
		{
			if (modGlobal.Statics.CustomizeStuff.boolRegionalTown)
			{
				// CustomizeStuff.CurrentTown = Val(gridTranCode.TextMatrix(0, 0))
				modGlobal.Statics.CustomizeStuff.CurrentTown = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTranCode.ComboData())));
				ReCalcTax();
			}
		}

		private void ReCalcExemption()
		{
			int lngRegExempt = 0;
			int lngTotal = 0;
			int lngExemption;
			int lngTemp;
			int lngTemp2;
			if (Conversion.Val(txtExemption.Text) > 0)
			{
				lngRegExempt = FCConvert.ToInt32(FCConvert.ToDouble(txtExemption.Text));
			}
			else
			{
				lngRegExempt = 0;
			}
			txtExemption.Text = Strings.Format(lngRegExempt, "#,###,###,##0");
			lngExemption = lngRegExempt;
			if (Conversion.Val(txtTotal.Text) > 0)
			{
				lngTotal = FCConvert.ToInt32(FCConvert.ToDouble(txtTotal.Text));
			}
			else
			{
				lngTotal = 0;
			}
			lngTotal -= lngExemption;
			if (modGlobal.Statics.CustomizeStuff.ShowRateOnShort)
			{
				lblTotalValue.Text = "$" + Strings.Format(lngTotal, "#,###,###,##0");
				lblTaxRate.Text = "$" + Strings.Format(modGlobal.Statics.CustomizeStuff.TaxRate * 1000, "##0.000");
				lblTotalTax.Text = "$" + Strings.Format(modGlobal.RoundDbl(lngTotal * modGlobal.Statics.CustomizeStuff.TaxRate, 2), "#,###,##0.00");
			}
		}

		private void ReCalcTax()
		{
			if (modGlobal.Statics.CustomizeStuff.ShowRateOnShort)
			{
				int lngTotal = 0;
				int lngExemption = 0;
				if (Conversion.Val(txtExemption.Text) > 0)
				{
					lngExemption = FCConvert.ToInt32(FCConvert.ToDouble(txtExemption.Text));
				}
				else
				{
					lngExemption = 0;
				}
				if (Conversion.Val(txtTotal.Text) > 0)
				{
					lngTotal = FCConvert.ToInt32(FCConvert.ToDouble(txtTotal.Text));
				}
				else
				{
					lngTotal = 0;
				}
				lngTotal -= lngExemption;
				lblTaxRate.Text = "$" + Strings.Format(modGlobal.Statics.CustomizeStuff.TaxRate * 1000, "##0.000");
				lblTotalTax.Text = "$" + Strings.Format(modGlobal.RoundDbl(lngTotal * modGlobal.Statics.CustomizeStuff.TaxRate, 2), "#,###,##0.00");
			}
		}

		private void gridTranCode_Leave(object sender, System.EventArgs e)
		{
			if (modGlobal.Statics.CustomizeStuff.boolRegionalTown)
			{
				////Application.DoEvents();
				modGlobal.Statics.CustomizeStuff.CurrentTown = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTranCode.TextMatrix(0, 0))));
				if (modGlobal.Statics.CustomizeStuff.ShowRateOnShort)
				{
					int lngTotal = 0;
					int lngExemption = 0;
					if (Conversion.Val(txtExemption.Text) > 0)
					{
						lngExemption = FCConvert.ToInt32(FCConvert.ToDouble(txtExemption.Text));
					}
					else
					{
						lngExemption = 0;
					}
					if (Conversion.Val(txtTotal.Text) > 0)
					{
						lngTotal = FCConvert.ToInt32(FCConvert.ToDouble(txtTotal.Text));
					}
					else
					{
						lngTotal = 0;
					}
					lngTotal -= lngExemption;
					lblTaxRate.Text = "$" + Strings.Format(modGlobal.Statics.CustomizeStuff.TaxRate * 1000, "##0.000");
					lblTotalTax.Text = "$" + Strings.Format(modGlobal.RoundDbl(lngTotal * modGlobal.Statics.CustomizeStuff.TaxRate, 2), "#,###,##0.00");
				}
			}
		}

		private void mnuComment_Click(object sender, System.EventArgs e)
		{
			frmRefGlobalComment refGC = new frmRefGlobalComment();
			// If frmGlobalComment.Init("PP", "PPMaster", "Comment", "Account", CurrentAccount, , , TRIOWINDOWSIZEBIGGIE, , True) Then
			//FC:FINAL:MSH - restore missed call of the comment form
			string module = "PP";
			string table = "PPMaster";
			string field = "Comment";
			string keyField = "Account";
			int lngID = modPPGN.Statics.CurrentAccount;
			theAccount.Comment = refGC.Init(ref module, ref table, ref field, ref keyField, ref lngID);
			if (Strings.Trim(theAccount.Comment) == "")
			{
				lblComment.Visible = false;
			}
			else
			{
				lblComment.Visible = true;
			}
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: varDelete As Variant --> As DialogResult
			DialogResult varDelete;
			clsDRWrapper rs = new clsDRWrapper();
			if (modPPGN.Statics.CurrentAccount < 1)
			{
				MessageBox.Show("This account hasn't been created yet", "Account not Created", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			varDelete = MessageBox.Show("Are you sure you want to delete this account?", "Delete?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (varDelete == DialogResult.Yes)
			{
				rs.OpenRecordset("select * from ppmaster where account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount), modPPGN.strPPDatabase);
				rs.Edit();
				rs.Set_Fields("Deleted", true);
				rs.Update();
				modGlobalFunctions.AddCYAEntry_26("PP", "Deleted account", modPPGN.Statics.CurrentAccount.ToString());
				
				frmPPMasterShort.InstancePtr.Close();
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				modPPGN.checkGroup_8(rs.Get_Fields("account"), false);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuGroup_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			// vbPorter upgrade warning: intRes As short, int --> As DialogResult
			DialogResult intRes;
			int lngGroupNumber = 0;
			clsLoad.OpenRecordset("select * from moduleassociation where module = 'PP' and account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount), "CentralData");
			if (!clsLoad.EndOfFile())
			{
				lngGroupNumber = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("groupnumber"));
				clsLoad.OpenRecordset("select * from groupmaster where id = " + FCConvert.ToString(lngGroupNumber), "CentralData");
				if (!clsLoad.EndOfFile())
				{
					//! Load frmGroup;
					frmGroup.InstancePtr.Init(lngGroupNumber);
				}
				else
				{
					MessageBox.Show("This account is associated with a group but the group does not exist.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			else
			{
				intRes = MessageBox.Show("This account is not in a group." + "\r\n" + "Would you like to create a new group?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (intRes == DialogResult.Yes)
				{
					//! Load frmGroup;
					frmGroup.InstancePtr.Init(0);
				}
			}
		}

		private void mnuInterestedParties_Click(object sender, System.EventArgs e)
		{
			if (modPPGN.Statics.CurrentAccount > 0)
			{
				frmInterestedParties.InstancePtr.Init(modPPGN.Statics.CurrentAccount, "PP", 0, FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modSecurity.EDITADDRESSESMENU)) == "F");
			}
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			//FC:FINAL:AM: use the report viewer
			//rptShortScreen.InstancePtr.Show(App.MainForm);
			frmReportViewer.InstancePtr.Init(rptShortScreen.InstancePtr);
		}

		private void mnuPrintLabel_Click(object sender, System.EventArgs e)
		{
			rptCustomLabels.InstancePtr.Close();
			// 
			switch (modGlobal.Statics.CustomizeStuff.AccountLabelType)
			{
				case modLabels.CNSTLBLTYPEDYMO30252:
				case modLabels.CNSTLBLTYPEDYMO30256:
				case modLabels.CNSTLBLTYPEDYMO4150:
					{
						//FC:FINAL:MSH - i.issue #1417: restore missing report call
						rptCustomLabels.InstancePtr.Init(false, 2, 1, 1, modGlobal.Statics.CustomizeStuff.AccountLabelType, 0, FCConvert.ToString(modPPGN.Statics.CurrentAccount), FCConvert.ToString(modPPGN.Statics.CurrentAccount), "", 0, modGlobal.Statics.LabelPrinterName, 0, 0, true);
						break;
					}
				default:
					{
						modPPGN.Statics.gboolPrintF2Label = true;
						rptCustomLabels.InstancePtr.Run(false);
						break;
					}
			}
			//end switch
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			gridTranCode.Row = -1;
			SaveAccount();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			gridTranCode.Row = -1;
			if (SaveAccount())
			{
				mnuExit_Click();
			}
		}

		private void txtApt_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtBETE1_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtBETE1_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtBETE1.Text) > 0)
			{
				txtBETE1.Text = Strings.Format(FCConvert.ToInt32(FCConvert.ToDouble(txtBETE1.Text)), "#,###,###,##0");
			}
		}

		private void txtBETE2_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtBETE2_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtBETE2.Text) > 0)
			{
				txtBETE2.Text = Strings.Format(FCConvert.ToInt32(FCConvert.ToDouble(txtBETE2.Text)), "#,###,###,##0");
			}
		}

		private void txtBETE3_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtBETE3_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtBETE3.Text) > 0)
			{
				txtBETE3.Text = Strings.Format(FCConvert.ToInt32(FCConvert.ToDouble(txtBETE3.Text)), "#,###,###,##0");
			}
		}

		private void txtBETE4_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtBETE4_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtBETE4.Text) > 0)
			{
				txtBETE4.Text = Strings.Format(FCConvert.ToInt32(FCConvert.ToDouble(txtBETE4.Text)), "#,###,###,##0");
			}
		}

		private void txtBETE5_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtBETE5_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtBETE5.Text) > 0)
			{
				txtBETE5.Text = Strings.Format(FCConvert.ToInt32(FCConvert.ToDouble(txtBETE5.Text)), "#,###,###,##0");
			}
		}

		private void txtBETE6_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtBETE6_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtBETE6.Text) > 0)
			{
				txtBETE6.Text = Strings.Format(FCConvert.ToInt32(FCConvert.ToDouble(txtBETE6.Text)), "#,###,###,##0");
			}
		}

		private void txtBETE7_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtBETE7_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtBETE7.Text) > 0)
			{
				txtBETE7.Text = Strings.Format(FCConvert.ToInt32(FCConvert.ToDouble(txtBETE7.Text)), "#,###,###,##0");
			}
		}

		private void txtBETE8_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtBETE8_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtBETE8.Text) > 0)
			{
				txtBETE8.Text = Strings.Format(FCConvert.ToInt32(FCConvert.ToDouble(txtBETE8.Text)), "#,###,###,##0");
			}
		}

		private void txtBETE9_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtBETE9_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtBETE9.Text) > 0)
			{
				txtBETE9.Text = Strings.Format(FCConvert.ToInt32(FCConvert.ToDouble(txtBETE9.Text)), "#,###,###,##0");
			}
		}

		private void txtBusinessCode_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtBusinessCode_Enter(object sender, System.EventArgs e)
		{
			modGlobal.Statics.OverType = 1;
			txtBusinessCode.SelectionStart = 0;
			txtBusinessCode.SelectionLength = 1;
		}

		private void txtBusinessCode_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F9)
			{
				Helping = true;
				modPPHelp.Statics.intHelpResponse = 0;
				modPPHelp.BusinessCodes();
				frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				if (modPPHelp.Statics.intHelpResponse != 0)
					txtBusinessCode.Text = Strings.Format(modPPHelp.Statics.intHelpResponse, "000");
				frmHelpCodes.InstancePtr.Unload();
			}
		}

		private void txtCategory1_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtCategory1_Enter(object sender, System.EventArgs e)
		{
			txtCategory1.SelectionStart = 11;
		}

		private void txtCategory1_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (Information.IsNumeric(txtCategory1.Text) == false)
			{
				if (txtCategory1.Text.Length > 0)
				{
					txtCategory1.Text = Strings.Mid(txtCategory1.Text, 1, txtCategory1.Text.Length - 1);
				}
				else
				{
					txtCategory1.Text = "0";
				}
			}
		}

		private void txtCategory1_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// vbPorter upgrade warning: hold As Variant --> As int
			int hold;
			if (KeyCode == Keys.C)
			{
				txtCategory1.Text = "0";
				txtCategory1.SelectionStart = 11;
				return;
			}
			if (Conversion.Val(txtCategory1.Text) != 0)
				hold = FCConvert.ToInt32(FCConvert.ToDouble(txtCategory1.Text));
			// txtCategory1.Text = Format(hold, "#,##0")
			// txtCategory1.SelStart = 11
		}

		private void txtCategory1_Leave(object sender, System.EventArgs e)
		{
			AddCategoriesforTotalLabel();
		}

		private void txtCategory1_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtCategory1.Text) == 0)
			{
				txtCategory1.Text = "0";
			}
			else
			{
				txtCategory1.Text = Strings.Format(txtCategory1.Text, "#,###,##0");
			}
			ReCalcExemption();
		}

		private void txtCategory2_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtCategory2_Enter(object sender, System.EventArgs e)
		{
			txtCategory2.SelectionStart = 11;
		}

		private void txtCategory2_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (Information.IsNumeric(txtCategory2.Text) == false)
			{
				if (txtCategory2.Text.Length > 0)
				{
					txtCategory2.Text = Strings.Mid(txtCategory2.Text, 1, txtCategory2.Text.Length - 1);
				}
				else
				{
					txtCategory2.Text = "0";
				}
			}
		}

		private void txtCategory2_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// vbPorter upgrade warning: hold As Variant --> As int
			int hold;
			if (KeyCode == Keys.C)
			{
				txtCategory2.Text = "0";
				txtCategory2.SelectionStart = 11;
				return;
			}
			if (Conversion.Val(txtCategory2.Text) != 0)
				hold = FCConvert.ToInt32(FCConvert.ToDouble(txtCategory2.Text));
			// txtCategory2.Text = Format(hold, "#,##0")
			// txtCategory2.SelStart = 11
		}

		private void txtCategory2_Leave(object sender, System.EventArgs e)
		{
			AddCategoriesforTotalLabel();
		}

		private void txtCategory2_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtCategory2.Text) == 0)
			{
				txtCategory2.Text = "0";
			}
			else
			{
				txtCategory2.Text = Strings.Format(FCConvert.ToInt32(FCConvert.ToDouble(txtCategory2.Text)), "#,##0");
			}
			ReCalcExemption();
		}

		private void txtCategory3_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtCategory3_Enter(object sender, System.EventArgs e)
		{
			txtCategory3.SelectionStart = 11;
		}

		private void txtCategory3_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (Information.IsNumeric(txtCategory3.Text) == false)
			{
				if (txtCategory3.Text.Length > 0)
				{
					txtCategory3.Text = Strings.Mid(txtCategory3.Text, 1, txtCategory3.Text.Length - 1);
				}
				else
				{
					txtCategory3.Text = "0";
				}
			}
		}

		private void txtCategory3_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// vbPorter upgrade warning: hold As Variant --> As int
			int hold;
			if (KeyCode == Keys.C)
			{
				txtCategory3.Text = "";
				txtCategory3.SelectionStart = 11;
				return;
			}
			hold = 0;
			if (Conversion.Val(txtCategory3.Text) != 0)
				hold = FCConvert.ToInt32(FCConvert.ToDouble(txtCategory3.Text));
			// txtCategory3.Text = Format(hold, "#,##0")
			// txtCategory3.SelStart = 11
		}

		private void txtCategory3_Leave(object sender, System.EventArgs e)
		{
			AddCategoriesforTotalLabel();
		}

		private void txtCategory3_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtCategory3.Text) == 0)
			{
				txtCategory3.Text = "0";
			}
			else
			{
				txtCategory3.Text = Strings.Format(txtCategory3.Text, "#,###,##0");
			}
			ReCalcExemption();
		}

		private void txtCategory4_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtCategory4_Enter(object sender, System.EventArgs e)
		{
			txtCategory4.SelectionStart = 11;
		}

		private void txtCategory4_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (Information.IsNumeric(txtCategory4.Text) == false)
			{
				if (txtCategory4.Text.Length > 0)
				{
					txtCategory4.Text = Strings.Mid(txtCategory4.Text, 1, txtCategory4.Text.Length - 1);
				}
				else
				{
					txtCategory4.Text = "0";
				}
			}
		}

		private void txtCategory4_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// vbPorter upgrade warning: hold As Variant --> As int
			int hold;
			if (KeyCode == Keys.C)
			{
				txtCategory4.Text = "0";
				txtCategory4.SelectionStart = 11;
				return;
			}
			hold = 0;
			if (Conversion.Val(txtCategory4.Text) != 0)
				hold = FCConvert.ToInt32(FCConvert.ToDouble(txtCategory4.Text));
			// txtCategory4.Text = Format(hold, "#,##0")
			// txtCategory4.SelStart = 11
		}

		private void txtCategory4_Leave(object sender, System.EventArgs e)
		{
			AddCategoriesforTotalLabel();
		}

		private void txtCategory4_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtCategory4.Text) == 0)
			{
				txtCategory4.Text = "0";
			}
			else
			{
				txtCategory4.Text = Strings.Format(txtCategory4.Text, "#,###,##0");
			}
			ReCalcExemption();
		}

		private void txtCategory5_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtCategory5_Enter(object sender, System.EventArgs e)
		{
			txtCategory5.SelectionStart = 11;
		}

		private void txtCategory5_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (Information.IsNumeric(txtCategory5.Text) == false)
			{
				if (txtCategory5.Text.Length > 0)
				{
					txtCategory5.Text = Strings.Mid(txtCategory5.Text, 1, txtCategory5.Text.Length - 1);
				}
				else
				{
					txtCategory5.Text = "0";
				}
			}
		}

		private void txtCategory5_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// vbPorter upgrade warning: hold As Variant --> As int
			int hold;
			if (KeyCode == Keys.C)
			{
				txtCategory5.Text = "0";
				txtCategory5.SelectionStart = 11;
				return;
			}
			if (Conversion.Val(txtCategory5.Text) != 0)
				hold = FCConvert.ToInt32(FCConvert.ToDouble(txtCategory5.Text));
			// txtCategory5.Text = Format(hold, "#,##0")
			// txtCategory5.SelStart = 11
		}

		private void txtCategory5_Leave(object sender, System.EventArgs e)
		{
			AddCategoriesforTotalLabel();
		}

		private void txtCategory5_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtCategory5.Text) == 0)
			{
				txtCategory5.Text = "0";
			}
			else
			{
				txtCategory5.Text = Strings.Format(txtCategory5.Text, "#,###,##0");
			}
			ReCalcExemption();
		}

		private void txtCategory6_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtCategory6_Enter(object sender, System.EventArgs e)
		{
			txtCategory6.SelectionStart = 11;
		}

		private void txtCategory6_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (Information.IsNumeric(txtCategory6.Text) == false)
			{
				if (txtCategory6.Text.Length > 0)
				{
					txtCategory6.Text = Strings.Mid(txtCategory6.Text, 1, txtCategory6.Text.Length - 1);
				}
				else
				{
					txtCategory6.Text = "0";
				}
			}
		}

		private void txtCategory6_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// vbPorter upgrade warning: hold As Variant --> As int
			int hold;
			if (KeyCode == Keys.C)
			{
				txtCategory6.Text = "0";
				txtCategory6.SelectionStart = 11;
				return;
			}
			if (Conversion.Val(txtCategory6.Text) != 0)
				hold = FCConvert.ToInt32(FCConvert.ToDouble(txtCategory6.Text));
			// txtCategory6.Text = Format(hold, "#,##0")
			// txtCategory6.SelStart = 11
		}

		private void txtCategory6_Leave(object sender, System.EventArgs e)
		{
			AddCategoriesforTotalLabel();
		}

		private void txtCategory6_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtCategory6.Text) == 0)
			{
				txtCategory6.Text = "0";
			}
			else
			{
				txtCategory6.Text = Strings.Format(txtCategory6.Text, "#,###,##0");
			}
			ReCalcExemption();
		}

		private void txtCategory7_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtCategory7_Enter(object sender, System.EventArgs e)
		{
			txtCategory7.SelectionStart = 11;
		}

		private void txtCategory7_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (Information.IsNumeric(txtCategory7.Text) == false)
			{
				if (txtCategory7.Text.Length > 0)
				{
					txtCategory7.Text = Strings.Mid(txtCategory7.Text, 1, txtCategory7.Text.Length - 1);
				}
				else
				{
					txtCategory7.Text = "0";
				}
			}
		}

		private void txtCategory7_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// vbPorter upgrade warning: hold As Variant --> As int
			int hold;
			if (KeyCode == Keys.C)
			{
				txtCategory7.Text = "0";
				txtCategory7.SelectionStart = 11;
				return;
			}
			if (Conversion.Val(txtCategory7.Text) != 0)
				hold = FCConvert.ToInt32(FCConvert.ToDouble(txtCategory7.Text));
			// txtCategory7.Text = Format(hold, "#,##0")
			// txtCategory7.SelStart = 11
		}

		private void txtCategory7_Leave(object sender, System.EventArgs e)
		{
			AddCategoriesforTotalLabel();
		}

		private void txtCategory7_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtCategory7.Text) == 0)
			{
				txtCategory7.Text = "0";
			}
			else
			{
				txtCategory7.Text = Strings.Format(txtCategory7.Text, "#,###,##0");
			}
			ReCalcExemption();
		}

		private void txtCategory8_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtCategory8_Enter(object sender, System.EventArgs e)
		{
			txtCategory8.SelectionStart = 11;
		}

		private void txtCategory8_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (Information.IsNumeric(txtCategory8.Text) == false)
			{
				if (txtCategory8.Text.Length > 0)
				{
					txtCategory8.Text = Strings.Mid(txtCategory8.Text, 1, txtCategory8.Text.Length - 1);
				}
				else
				{
					txtCategory8.Text = "0";
				}
			}
		}

		private void txtCategory8_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// vbPorter upgrade warning: hold As Variant --> As int
			int hold;
			if (KeyCode == Keys.C)
			{
				txtCategory8.Text = "0";
				txtCategory8.SelectionStart = 11;
				return;
			}
			if (Conversion.Val(txtCategory8.Text) != 0)
				hold = FCConvert.ToInt32(FCConvert.ToDouble(txtCategory8.Text));
			// txtCategory8.Text = Format(hold, "#,##0")
			// txtCategory8.SelStart = 11
		}

		private void txtCategory8_Leave(object sender, System.EventArgs e)
		{
			AddCategoriesforTotalLabel();
		}

		private void txtCategory8_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtCategory8.Text) == 0)
			{
				txtCategory8.Text = "0";
			}
			else
			{
				txtCategory8.Text = Strings.Format(txtCategory8.Text, "#,###,##0");
			}
			ReCalcExemption();
		}

		private void txtCategory9_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtCategory9_Enter(object sender, System.EventArgs e)
		{
			txtCategory9.SelectionStart = 11;
		}

		private void txtCategory9_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (Information.IsNumeric(txtCategory9.Text) == false)
			{
				if (txtCategory9.Text.Length > 0)
				{
					txtCategory9.Text = Strings.Mid(txtCategory9.Text, 1, txtCategory9.Text.Length - 1);
				}
				else
				{
					txtCategory9.Text = "0";
				}
			}
		}

		private void txtCategory9_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// vbPorter upgrade warning: hold As Variant --> As int
			int hold;
			if (KeyCode == Keys.C)
			{
				txtCategory9.Text = "0";
				txtCategory9.SelectionStart = 11;
				return;
			}
			if (Conversion.Val(txtCategory9.Text) != 0)
				hold = FCConvert.ToInt32(FCConvert.ToDouble(txtCategory9.Text));
			// txtCategory9.Text = Format(hold, "#,##0")
			// txtCategory9.SelStart = 11
		}

		private void txtCategory9_Leave(object sender, System.EventArgs e)
		{
			AddCategoriesforTotalLabel();
		}

		private void CategoryTextBoxes()
		{
			clsDRWrapper rs = new clsDRWrapper();
			strSQL = "SELECT * FROM PPValuations WHERE ValueKey = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount);
			rs.OpenRecordset(strSQL, modPPGN.strPPDatabase);
			if (rs.EndOfFile())
			{
				txtCategory1.Text = FCConvert.ToString(0);
				txtCategory2.Text = FCConvert.ToString(0);
				txtCategory3.Text = FCConvert.ToString(0);
				txtCategory4.Text = FCConvert.ToString(0);
				txtCategory5.Text = FCConvert.ToString(0);
				txtCategory6.Text = FCConvert.ToString(0);
				txtCategory7.Text = FCConvert.ToString(0);
				txtCategory8.Text = FCConvert.ToString(0);
				txtCategory9.Text = FCConvert.ToString(0);
				txtBETE1.Text = FCConvert.ToString(0);
				txtBETE2.Text = FCConvert.ToString(0);
				txtBETE3.Text = FCConvert.ToString(0);
				txtBETE4.Text = FCConvert.ToString(0);
				txtBETE5.Text = FCConvert.ToString(0);
				txtBETE6.Text = FCConvert.ToString(0);
				txtBETE7.Text = FCConvert.ToString(0);
				txtBETE8.Text = FCConvert.ToString(0);
				txtBETE9.Text = FCConvert.ToString(0);
			}
			else
			{
				modGNWork.GetLocalVariables2();
				txtCategory1.Text = Strings.Format(modGlobal.Round(Conversion.Val(rs.Get_Fields_Int32("category1")), modGlobal.Statics.CustomizeStuff.Round), "#,###,##0");
				txtCategory2.Text = Strings.Format(modGlobal.Round(Conversion.Val(rs.Get_Fields_Int32("category2")), modGlobal.Statics.CustomizeStuff.Round), "#,###,##0");
				txtCategory3.Text = Strings.Format(modGlobal.Round(Conversion.Val(rs.Get_Fields_Int32("category3")), modGlobal.Statics.CustomizeStuff.Round), "#,###,##0");
				txtCategory4.Text = Strings.Format(modGlobal.Round(Conversion.Val(rs.Get_Fields_Int32("category4")), modGlobal.Statics.CustomizeStuff.Round), "#,###,##0");
				txtCategory5.Text = Strings.Format(modGlobal.Round(Conversion.Val(rs.Get_Fields_Int32("category5")), modGlobal.Statics.CustomizeStuff.Round), "#,###,##0");
				txtCategory6.Text = Strings.Format(modGlobal.Round(Conversion.Val(rs.Get_Fields_Int32("category6")), modGlobal.Statics.CustomizeStuff.Round), "#,###,##0");
				txtCategory7.Text = Strings.Format(modGlobal.Round(Conversion.Val(rs.Get_Fields_Int32("category7")), modGlobal.Statics.CustomizeStuff.Round), "#,###,##0");
				txtCategory8.Text = Strings.Format(modGlobal.Round(Conversion.Val(rs.Get_Fields_Int32("category8")), modGlobal.Statics.CustomizeStuff.Round), "#,###,##0");
				txtCategory9.Text = Strings.Format(modGlobal.Round(Conversion.Val(rs.Get_Fields_Int32("Category9")), modGlobal.Statics.CustomizeStuff.Round), "#,###,##0");
				txtBETE1.Text = Strings.Format(modGlobal.Round(Conversion.Val(rs.Get_Fields_Int32("cat1exempt")), modGlobal.Statics.CustomizeStuff.Round), "#,###,##0");
				txtBETE2.Text = Strings.Format(modGlobal.Round(Conversion.Val(rs.Get_Fields_Int32("cat2exempt")), modGlobal.Statics.CustomizeStuff.Round), "#,###,##0");
				txtBETE3.Text = Strings.Format(modGlobal.Round(Conversion.Val(rs.Get_Fields_Int32("cat3exempt")), modGlobal.Statics.CustomizeStuff.Round), "#,###,##0");
				txtBETE4.Text = Strings.Format(modGlobal.Round(Conversion.Val(rs.Get_Fields_Int32("cat4exempt")), modGlobal.Statics.CustomizeStuff.Round), "#,###,##0");
				txtBETE5.Text = Strings.Format(modGlobal.Round(Conversion.Val(rs.Get_Fields_Int32("cat5exempt")), modGlobal.Statics.CustomizeStuff.Round), "#,###,##0");
				txtBETE6.Text = Strings.Format(modGlobal.Round(Conversion.Val(rs.Get_Fields_Int32("cat6exempt")), modGlobal.Statics.CustomizeStuff.Round), "#,###,##0");
				txtBETE7.Text = Strings.Format(modGlobal.Round(Conversion.Val(rs.Get_Fields_Int32("cat7exempt")), modGlobal.Statics.CustomizeStuff.Round), "#,###,##0");
				txtBETE8.Text = Strings.Format(modGlobal.Round(Conversion.Val(rs.Get_Fields_Int32("cat8exempt")), modGlobal.Statics.CustomizeStuff.Round), "#,###,##0");
				txtBETE9.Text = Strings.Format(modGlobal.Round(Conversion.Val(rs.Get_Fields_Int32("cat9exempt")), modGlobal.Statics.CustomizeStuff.Round), "#,###,##0");
			}
		}

		private bool SaveAccount()
		{
			bool SaveAccount = false;

			clsDRWrapper clsSave = new clsDRWrapper();
			// vbPorter upgrade warning: lngTotalValue As int	OnWrite(int, double)
			int lngTotalValue = 0;
			clsDRWrapper rs = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				// Update Values
				SaveAccount = false;
				if (modGlobal.Statics.CustomizeStuff.boolRegionalTown)
				{
					int lngTemp = 0;
					lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTranCode.TextMatrix(0, 0))));
					if (lngTemp < 1)
					{
						lngTemp = frmPickRegionalTown.InstancePtr.Init(-1, false, "A town has not been specified");
						if (lngTemp > 0)
						{
							gridTranCode.TextMatrix(0, 0, FCConvert.ToString(lngTemp));
						}
					}
				}
				double[] dblCatVal = new double[9 + 1];
				// vbPorter upgrade warning: dblBETEVal As Variant --> As int
				int[] dblBETEVal = new int[9 + 1];
				int x;
				for (x = 1; x <= 9; x++)
				{
					dblCatVal[x] = 0;
					dblBETEVal[x] = 0;
				}
				// x
				if (Conversion.Val(txtCategory1.Text) != 0)
					dblCatVal[1] = FCConvert.ToInt32(FCConvert.ToDouble(txtCategory1.Text));
				if (Conversion.Val(txtCategory2.Text) != 0)
					dblCatVal[2] = FCConvert.ToInt32(FCConvert.ToDouble(txtCategory2.Text));
				if (Conversion.Val(txtCategory3.Text) != 0)
					dblCatVal[3] = FCConvert.ToInt32(FCConvert.ToDouble(txtCategory3.Text));
				if (Conversion.Val(txtCategory4.Text) != 0)
					dblCatVal[4] = FCConvert.ToInt32(FCConvert.ToDouble(txtCategory4.Text));
				if (Conversion.Val(txtCategory5.Text) != 0)
					dblCatVal[5] = FCConvert.ToInt32(FCConvert.ToDouble(txtCategory5.Text));
				if (Conversion.Val(txtCategory6.Text) != 0)
					dblCatVal[6] = FCConvert.ToInt32(FCConvert.ToDouble(txtCategory6.Text));
				if (Conversion.Val(txtCategory7.Text) != 0)
					dblCatVal[7] = FCConvert.ToInt32(FCConvert.ToDouble(txtCategory7.Text));
				if (Conversion.Val(txtCategory8.Text) != 0)
					dblCatVal[8] = FCConvert.ToInt32(FCConvert.ToDouble(txtCategory8.Text));
				if (Conversion.Val(txtCategory9.Text) != 0)
					dblCatVal[9] = FCConvert.ToInt32(FCConvert.ToDouble(txtCategory9.Text));
				if (Conversion.Val(txtBETE1.Text) != 0)
					dblBETEVal[1] = FCConvert.ToInt32(FCConvert.ToDouble(txtBETE1.Text));
				if (Conversion.Val(txtBETE2.Text) != 0)
					dblBETEVal[2] = FCConvert.ToInt32(FCConvert.ToDouble(txtBETE2.Text));
				if (Conversion.Val(txtBETE3.Text) != 0)
					dblBETEVal[3] = FCConvert.ToInt32(FCConvert.ToDouble(txtBETE3.Text));
				if (Conversion.Val(txtBETE4.Text) != 0)
					dblBETEVal[4] = FCConvert.ToInt32(FCConvert.ToDouble(txtBETE4.Text));
				if (Conversion.Val(txtBETE5.Text) != 0)
					dblBETEVal[5] = FCConvert.ToInt32(FCConvert.ToDouble(txtBETE5.Text));
				if (Conversion.Val(txtBETE6.Text) != 0)
					dblBETEVal[6] = FCConvert.ToInt32(FCConvert.ToDouble(txtBETE6.Text));
				if (Conversion.Val(txtBETE7.Text) != 0)
					dblBETEVal[7] = FCConvert.ToInt32(FCConvert.ToDouble(txtBETE7.Text));
				if (Conversion.Val(txtBETE8.Text) != 0)
					dblBETEVal[8] = FCConvert.ToInt32(FCConvert.ToDouble(txtBETE8.Text));
				if (Conversion.Val(txtBETE9.Text) != 0)
					dblBETEVal[9] = FCConvert.ToInt32(FCConvert.ToDouble(txtBETE9.Text));
				if (!modPPGN.Statics.boolShortScreenOnly)
				{
					if (Conversion.Val(txtTotal.Text) > 0)
					{
						lngTotalValue = FCConvert.ToInt32(FCConvert.ToDouble(txtTotal.Text));
					}
					else
					{
						lngTotalValue = 0;
					}
				}
				else
				{
					lngTotalValue = FCConvert.ToInt32(dblCatVal[1] + dblCatVal[2] + dblCatVal[3] + dblCatVal[4]);
					lngTotalValue += FCConvert.ToInt32(dblCatVal[5] + dblCatVal[6] + dblCatVal[7] + dblCatVal[8] + dblCatVal[9]);
				}
				// 
				theAccount.Street = txtStreetName.Text;
				theAccount.StreetNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(txtStreetNumber.Text)));
				if (txtApt.Text.Length > 1)
				{
					txtApt.Text = Strings.Left(txtApt.Text, 1);
				}
				theAccount.StreetApt = Strings.Trim(txtApt.Text);
				theAccount.Open1 = txtOpen1.Text;
				theAccount.Open2 = txtOpen2.Text;
				theAccount.TranCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTranCode.TextMatrix(0, 0))));
				theAccount.BusinessCode = FCConvert.ToInt32(Math.Round(Conversion.Val(txtBusinessCode.Text)));
				theAccount.StreetCode = FCConvert.ToInt32(Math.Round(Conversion.Val(txtStreetCode.Text)));
				theAccount.Set_ExemptCode(1, FCConvert.ToInt32(Math.Round(Conversion.Val(txtExemptCode1.Text))));
				theAccount.Set_ExemptCode(2, FCConvert.ToInt32(Math.Round(Conversion.Val(txtExemptCode2.Text))));
				theAccount.Set_ExemptVal(1, lngExemption1);
				theAccount.Set_ExemptVal(2, lngExemption2);
				if (lngTotalValue != theAccount.TotValue)
				{
					clsSave.Execute("update status set changedvalues = '" + FCConvert.ToString(DateTime.Now) + "'", "twpp0000.vb1");
				}
				theAccount.TotValue = lngTotalValue;
				if (Conversion.Val(txtExemption.Text) != theAccount.Exemption)
				{
					clsSave.Execute("update status set changedvalues = '" + FCConvert.ToString(DateTime.Now) + "'", "twpp0000.vb1");
				}
				theAccount.Exemption = Conversion.Val(txtExemption.Text);
				if (Conversion.Val(txtSquareFootage.Text) > 0)
				{
					if (Information.IsNumeric(txtSquareFootage.Text))
					{
						theAccount.SquareFootage = FCConvert.ToInt32(FCConvert.ToDouble(txtSquareFootage.Text));
					}
					else
					{
						theAccount.SquareFootage = FCConvert.ToInt32(Math.Round(Conversion.Val(txtSquareFootage.Text)));
					}
				}
				else
				{
					theAccount.SquareFootage = 0;
				}
				cPPAccountController tCont = new cPPAccountController();
                theAccount.Account = modPPGN.Statics.CurrentAccount;
                cPPAccount temp = theAccount;
				tCont.SaveAccount(ref temp);
				theAccount = temp;
				modPPGN.Statics.CurrentAccount = theAccount.Account;
				strSQL = "SELECT * FROM PPValuations WHERE ValueKey = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount);
				rs.OpenRecordset(strSQL, modPPGN.strPPDatabase);
				if (rs.EndOfFile())
				{
					rs.AddNew();
					rs.Set_Fields("valuekey", modPPGN.Statics.CurrentAccount);
				}
				else
				{
					rs.Edit();
				}
				for (x = 1; x <= 9; x++)
				{
					rs.Set_Fields("category" + x, dblCatVal[x]);
					rs.Set_Fields("cat" + x + "exempt", dblBETEVal[x]);
				}
				// x
				rs.Update();
				if (modPPGN.Statics.Adding == true)
				{
					// call rs.FindFirstRecord ("account",CurrentAccount)
					modPPGN.Statics.Adding = false;
					MessageBox.Show("New account was saved.  Account Number = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount), null, MessageBoxButtons.OK, MessageBoxIcon.Information);
					modGlobalFunctions.AddCYAEntry_80("PP", "Created New Account", FCConvert.ToString(modPPGN.Statics.CurrentAccount) + "", "Short Screen");
					lblAccount.Text = FCConvert.ToString(modPPGN.Statics.CurrentAccount);
				}
				boolDataChanged = false;
				SaveAccount = true;
				MessageBox.Show("Save Complete", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveAccount;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In SaveAccount");
				// MDIParent.Show
				// Unload frmPPMasterShort
			}
			return SaveAccount;
		}

		private void FillMasterData()
		{
			clsDRWrapper clsTemp = new clsDRWrapper();
			clsDRWrapper rs = new clsDRWrapper();
			lngExemption1 = 0;
			lngExemption2 = 0;
			cPPAccountController tCont = new cPPAccountController();
			//FC:FINAL:DSE A property cannot be passed as a ref parameter
			//tCont.LoadAccount(ref theAccount, modPPGN.Statics.CurrentAccount);
			cPPAccount temp = theAccount;
			tCont.LoadAccount(ref temp, modPPGN.Statics.CurrentAccount);
			theAccount = temp;
			// strSQL = "SELECT * FROM PPMaster WHERE Account = " & CurrentAccount
			// Call rs.OpenRecordset(strSQL, strPPDatabase)
			// If Not rs.EndOfFile Then
			lblAccount.Text = FCConvert.ToString(theAccount.Account);
			clsTemp.OpenRecordset("select * from moduleassociation where module = 'PP' and account = " + FCConvert.ToString(theAccount.Account), "CentralData");
			if (!clsTemp.EndOfFile())
			{
				txtREAssociate.Text = FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields_Int32("remasteracct")));
			}
			else
			{
				txtREAssociate.Text = FCConvert.ToString(0);
			}
			if (Strings.Trim(theAccount.Comment) != string.Empty)
			{
				lblComment.Visible = true;
			}
			else
			{
				lblComment.Visible = false;
			}
			ShowOwner();
			ShowAddress();
			txtStreetName.Text = theAccount.Street;
			txtStreetNumber.Text = Strings.Format(theAccount.StreetNumber, "00000");
			txtApt.Text = Strings.Trim(theAccount.StreetApt);
			txtOpen1.Text = Strings.Trim(theAccount.Open1);
			txtOpen2.Text = Strings.Trim(theAccount.Open2);
			txtSquareFootage.Text = Strings.Format(theAccount.SquareFootage, "#,###,##0");
			// txtTranCode.Text = Format(rs.fields("TranCode, "0")
			gridTranCode.TextMatrix(0, 0, FCConvert.ToString(theAccount.TranCode));
			if (modGlobal.Statics.CustomizeStuff.boolRegionalTown)
			{
				modGlobal.Statics.CustomizeStuff.CurrentTown = theAccount.TranCode;
			}
			txtBusinessCode.Text = Strings.Format(theAccount.BusinessCode, "000");
			txtStreetCode.Text = Strings.Format(theAccount.StreetCode, "0000");
			txtExemptCode1.Text = Strings.Format(theAccount.Get_ExemptCode(1), "00");
			txtExemptCode2.Text = Strings.Format(theAccount.Get_ExemptCode(2), "00");
			lngExemption1 = FCConvert.ToInt32(theAccount.Get_ExemptVal(1));
			lngExemption2 = FCConvert.ToInt32(theAccount.Get_ExemptVal(2));
			ToolTip1.SetToolTip(txtExemptCode1, Strings.Format(lngExemption1, "#,###,###,##0"));
			ToolTip1.SetToolTip(txtExemptCode2, Strings.Format(lngExemption2, "#,###,###,##0"));
			txtExemption.Text = Strings.Format(theAccount.Exemption, "#,##0");
			txtTotal.Text = Strings.Format(theAccount.TotValue, "#,###,###,##0");
			ReCalcTax();
			// Else
			// lblAccount.Caption = CurrentAccount
			// End If
		}

		private void txtCategory9_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtCategory9.Text) == 0)
			{
				txtCategory9.Text = "0";
			}
			else
			{
				txtCategory9.Text = Strings.Format(txtCategory9.Text, "#,###,##0");
			}
			ReCalcExemption();
		}

		private void txtExemptCode1_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtExemptCode1_Enter(object sender, System.EventArgs e)
		{
			modGlobal.Statics.OverType = 1;
			txtExemptCode1.SelectionStart = 0;
			txtExemptCode1.SelectionLength = 1;
		}

		private void txtExemptCode1_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F9)
			{
				Helping = true;
				modPPHelp.Statics.intHelpResponse = 0;
				modPPHelp.ExemptCodes();
				frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				if (modPPHelp.Statics.intHelpResponse != 0)
					txtExemptCode1.Text = Strings.Format(modPPHelp.Statics.intHelpResponse, "00");
				frmHelpCodes.InstancePtr.Unload();
			}
		}

		private void txtExemptCode1_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			ReCalcExemptions();
		}

		private void txtExemptCode2_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtExemptCode2_Enter(object sender, System.EventArgs e)
		{
			modGlobal.Statics.OverType = 1;
			txtExemptCode2.SelectionStart = 0;
			txtExemptCode2.SelectionLength = 1;
		}

		private void txtExemptCode2_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F9)
			{
				Helping = true;
				modPPHelp.Statics.intHelpResponse = 0;
				modPPHelp.ExemptCodes();
				frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				if (modPPHelp.Statics.intHelpResponse != 0)
					txtExemptCode2.Text = Strings.Format(modPPHelp.Statics.intHelpResponse, "00");
				frmHelpCodes.InstancePtr.Unload();
			}
		}

		private void txtExemptCode2_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			ReCalcExemptions();
		}

		private void txtExemption_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtExemption_Enter(object sender, System.EventArgs e)
		{
			txtExemption.SelectionStart = 11;
		}

		private void txtExemption_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			int lngTotal = 0;
			int lngExemption = 0;
			if (Conversion.Val(txtExemption.Text) > 0)
			{
				txtExemption.Text = FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(txtExemption.Text)));
				lngExemption = FCConvert.ToInt32(FCConvert.ToDouble(txtExemption.Text));
			}
			else
			{
				txtExemption.Text = "0";
				lngExemption = 0;
			}
			if (Conversion.Val(txtTotal.Text) > 0)
			{
				lngTotal = FCConvert.ToInt32(FCConvert.ToDouble(txtTotal.Text));
			}
			else
			{
				lngTotal = 0;
			}
			lngTotal -= lngExemption;
			if (modGlobal.Statics.CustomizeStuff.ShowRateOnShort)
			{
				lblTotalValue.Text = "$" + Strings.Format(lngTotal, "#,###,###,##0");
				lblTaxRate.Text = "$" + Strings.Format(modGlobal.Statics.CustomizeStuff.TaxRate * 1000, "##0.000");
				lblTotalTax.Text = "$" + Strings.Format(modGlobal.RoundDbl(lngTotal * modGlobal.Statics.CustomizeStuff.TaxRate, 2), "#,###,##0.00");
			}
			AddCategoriesforTotalLabel();
			ReCalcExemptions();
		}

		private void txtOpen1_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtOpen1_Enter(object sender, System.EventArgs e)
		{
			modGlobal.Statics.OverType = 1;
			txtOpen1.SelectionStart = 0;
			txtOpen1.SelectionLength = 1;
		}

		private void txtOpen2_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtOpen2_Enter(object sender, System.EventArgs e)
		{
			modGlobal.Statics.OverType = 1;
			txtOpen2.SelectionStart = 0;
			txtOpen2.SelectionLength = 1;
		}

		private void txtRegExempt_Change()
		{
			boolDataChanged = true;
		}

		private void txtRegExempt_Validate(ref bool Cancel)
		{
			ReCalcExemption();
		}

		private void txtSquareFootage_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtSquareFootage_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			txtSquareFootage.Text = Strings.Format(txtSquareFootage.Text, "#,###,##0");
		}

		private void txtStreetCode_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtStreetCode_Enter(object sender, System.EventArgs e)
		{
			modGlobal.Statics.OverType = 1;
			txtStreetCode.SelectionStart = 0;
			txtStreetCode.SelectionLength = 1;
		}

		private void txtStreetCode_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F9)
			{
				Helping = true;
				modPPHelp.Statics.intHelpResponse = 0;
				modPPHelp.StreetCodes();
				frmHelpCodes.InstancePtr.Show(FormShowEnum.Modal);
				if (modPPHelp.Statics.intHelpResponse != 0)
					txtStreetCode.Text = Strings.Format(modPPHelp.Statics.intHelpResponse, "0000");
				frmHelpCodes.InstancePtr.Unload();
			}
		}

		private void txtStreetName_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtStreetName_Enter(object sender, System.EventArgs e)
		{
			modGlobal.Statics.OverType = 1;
			txtStreetName.SelectionStart = 0;
			txtStreetName.SelectionLength = 1;
		}

		private void txtStreetNumber_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtStreetNumber_Enter(object sender, System.EventArgs e)
		{
			modGlobal.Statics.OverType = 1;
			txtStreetNumber.SelectionStart = 0;
			txtStreetNumber.SelectionLength = 1;
		}

		private void txtTotal_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			AddCategoriesforTotalLabel();
		}

		private void SetupGridTranCode()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			clsLoad.OpenRecordset("select * from trancodes where trancode = 0", modPPGN.strPPDatabase);
			if (clsLoad.EndOfFile())
			{
				if (!modGlobal.Statics.CustomizeStuff.boolRegionalTown)
				{
					strTemp = "#0;None" + "\t" + "0|";
				}
				else
				{
					strTemp = "#0;Default" + "\t" + "0|";
				}
			}
			clsLoad.OpenRecordset("select * from trancodes order by trancode", modPPGN.strPPDatabase);
			while (!clsLoad.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [trancode] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [trancode] and replace with corresponding Get_Field method
				strTemp += "#" + clsLoad.Get_Fields("trancode") + ";" + clsLoad.Get_Fields_String("trantype") + "\t" + clsLoad.Get_Fields("trancode") + "|";
				clsLoad.MoveNext();
			}
			if (strTemp != string.Empty)
			{
				strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
			}
			gridTranCode.ColComboList(0, strTemp);
			gridTranCode.TextMatrix(0, 0, FCConvert.ToString(0));
		}

		private void ResizeGridTranCode()
		{
			//gridTranCode.HeightOriginal = gridTranCode.RowHeight(0) + 60;
		}

		private void txtZip_Change()
		{
			boolDataChanged = true;
		}

		private void GetAccount()
		{
			object answer;
			int hldacct;
			// 
			hldacct = modPPGN.Statics.CurrentAccount;
		}

		private void IncreaseNextAccountByOne()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsDRWrapper clsTemp = new clsDRWrapper();
			int x;
			x = 0;
			clsLoad.OpenRecordset("select account from ppmaster order by account", modPPGN.strPPDatabase);
			if (!clsLoad.EndOfFile())
			{
				while (!clsLoad.EndOfFile())
				{
					x += 1;
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					if (x < clsLoad.Get_Fields("account"))
					{
						clsTemp.OpenRecordset("select account from ppmaster where account = " + FCConvert.ToString(x), modPPGN.strPPDatabase);
						if (clsTemp.EndOfFile())
						{
							modPPCalculate.Statics.NextPPAccount = x;
							return;
						}
					}
					clsLoad.MoveNext();
				}
				modPPCalculate.Statics.NextPPAccount = x + 1;
			}
			else
			{
				modPPCalculate.Statics.NextPPAccount = 1;
			}
		}

		public void HandlePartialPermission(ref int lngFuncID)
		{
			// takes a function number as a parameter
			// Then gets all the child functions that belong to it and
			// disables the appropriate controls
			clsDRWrapper clsChildList = new clsDRWrapper();
			// Dim dcTemp As New clsdrwrapper
			string strPerm = "";
			int lngChild = 0;
			bool booleditaddresses = false;
			bool booleditvalues = false;
			/* Control ctl = new Control(); */// Set clsChildList = New clsdrwrapper
			modGlobalConstants.Statics.clsSecurityClass.Get_Children(ref clsChildList, ref lngFuncID);
			while (!clsChildList.EndOfFile())
			{
				lngChild = FCConvert.ToInt32(clsChildList.GetData("childid"));
				strPerm = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(lngChild));
				switch (lngChild)
				{
					case modSecurity.EDITSHORTVALUES:
						{
							if (strPerm == "F")
							{
								booleditvalues = true;
							}
							else
							{
								booleditvalues = false;
							}
							break;
						}
					case modSecurity.EDITADDRESSESMENU:
						{
							if (strPerm == "F")
							{
								booleditaddresses = true;
							}
							else
							{
								booleditaddresses = false;
							}
							break;
						}
					case modSecurity.DELETEACCOUNTSMENU:
						{
							if (strPerm == "F")
							{
								mnuDelete.Enabled = true;
							}
							else
							{
								mnuDelete.Enabled = false;
							}
							break;
						}
				}
				//end switch
				clsChildList.MoveNext();
			}
			if (!booleditaddresses || !booleditvalues)
			{
				//FC:FINAL:MSH - in VB6 control contain all controls, include child controls in each control(same with i.issue #1649)
				//foreach (Control ctl in this.Controls)
				var controls = this.GetAllControls();
				foreach (Control ctl in controls)
				{
					if (!booleditaddresses)
					{
						if (FCConvert.ToString(ctl.Tag) == "address")
						{
							ctl.Enabled = false;
						}
					}
					if (!booleditvalues)
					{
						if (FCConvert.ToString(ctl.Tag) == "values")
						{
							ctl.Enabled = false;
						}
					}
				}
			}
		}

		private void mnuChangeAssociation_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngREAcct;
			int LngNumAccts;
			int lngPPGroup;
			int lngNumREAccts;
			bool boolOKToAdd;
			// can they be grouped?
			bool boolOKToAssociate;
			// can it be the primary associate
			int lngREAcctToAdd = 0;
			// vbPorter upgrade warning: intTemp As short, int --> As DialogResult
			DialogResult intTemp;
			int lngREGroup;
			clsDRWrapper clsTemp = new clsDRWrapper();
			bool boolAddPP = false;
			bool boolAddRE = false;
			try
			{
				// On Error GoTo ErrorHandler
				if (modPPGN.Statics.Adding == true || modPPGN.Statics.CurrentAccount < 1)
				{
					MessageBox.Show("This is a new account, you can't add an association until you have saved.", "New Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				//FC:FINAL:BBE - CallByRef works only with the correct ref type. 
				object objREAcctToAdd = lngREAcctToAdd;
				//FC:FINAL:MSH - issue #1290: increase size of textbox (in original can be displayed 7 symbols)
				//if (!frmInput.InstancePtr.Init(ref objREAcctToAdd, "Real Estate Associate", "Enter the Real Estate account to associate", 825, false, modGlobalConstants.InputDTypes.idtWholeNumber))
				if (!frmInput.InstancePtr.Init(ref objREAcctToAdd, "Real Estate Associate", "Enter the Real Estate account to associate", 1425, false, modGlobalConstants.InputDTypes.idtWholeNumber))
				{
					return;
				}
				//FC:FINAL:BBE - After CallByRef assign the value back correctly.
				lngREAcctToAdd = FCConvert.ToInt32(objREAcctToAdd);
				clsLoad.OpenRecordset("select * from moduleassociation where module = 'PP' and account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount), "CentralData");
				lngREAcct = 0;
				lngPPGroup = 0;
				lngREGroup = 0;
				LngNumAccts = 0;
				lngNumREAccts = 0;
				boolOKToAdd = false;
				boolOKToAssociate = false;
				if (!clsLoad.EndOfFile())
				{
					lngREAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("remasteracct"))));
					if (lngREAcct == lngREAcctToAdd && clsLoad.Get_Fields_Boolean("primaryassociate"))
					{
						MessageBox.Show("This account is already associated to RE " + FCConvert.ToString(lngREAcct), "Already associated", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}
					else if (clsLoad.Get_Fields_Boolean("primaryassociate") && lngREAcct > 0)
					{
						intTemp = MessageBox.Show("This account is already associated with RE account " + FCConvert.ToString(lngREAcct) + "." + "\r\n" + "Are you sure you want to change it?", "Already Associated", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (intTemp == DialogResult.No)
							return;
					}
					lngPPGroup = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("GROUPNUMBER"));
					boolOKToAdd = true;
				}
				else
				{
					boolOKToAdd = true;
				}
				clsLoad.OpenRecordset("select * from moduleassociation where module = 'RE' and account = " + FCConvert.ToString(lngREAcctToAdd), "CentralData");
				if (!clsLoad.EndOfFile())
				{
					lngREGroup = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("groupnumber"))));
					if (lngPPGroup > 0 && lngREGroup != lngPPGroup)
					{
						boolOKToAdd = false;
						MessageBox.Show("PP Account " + FCConvert.ToString(modPPGN.Statics.CurrentAccount) + " and RE Account " + FCConvert.ToString(lngREAcctToAdd) + " are in different groups and cannot be associated until you change the groups", "Cannot Associate", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					else
					{
						boolOKToAdd = true;
					}
				}
				// both are in the same group or only one is in a group so it is okay to group them
				// but it might not be ok to make it the primary associate
				if (lngREGroup > 0)
				{
					clsLoad.OpenRecordset("select * from moduleassociation where module = 'PP' and remasteracct = " + FCConvert.ToString(lngREAcctToAdd) + " and primaryassociate", "CentralData");
					if (!clsLoad.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						if (FCConvert.ToInt32(clsLoad.Get_Fields("account")) == modPPGN.Statics.CurrentAccount)
						{
							boolOKToAssociate = true;
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
							intTemp = MessageBox.Show("PP Account " + clsLoad.Get_Fields("account") + " is already the primary associate with RE account " + FCConvert.ToString(lngREAcctToAdd) + "." + "\r\n" + "This account will be grouped with RE " + FCConvert.ToString(lngREAcctToAdd) + "." + "\r\n" + "Do you also want to make it the new primary associate?", "Already Associated", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (intTemp == DialogResult.Yes)
								boolOKToAssociate = true;
						}
					}
					else
					{
						boolOKToAssociate = true;
					}
				}
				else
				{
					boolOKToAssociate = true;
				}
				if (lngPPGroup == 0)
				{
					boolAddPP = true;
				}
				else
				{
					boolAddPP = false;
				}
				if (lngREGroup == 0)
				{
					boolAddRE = true;
				}
				else
				{
					boolAddRE = false;
				}
				if (lngPPGroup == 0 && lngREGroup == 0)
				{
                    clsLoad.OpenRecordset("select * from groupmaster where id = -20", "CentralData");
                    clsLoad.AddNew();
                    clsTemp.OpenRecordset("select max(groupnumber) as highestgroup from groupmaster", "CentralData");
                    // TODO Get_Fields: Field [highestgroup] not found!! (maybe it is an alias?)
                    clsLoad.Set_Fields("groupnumber", clsTemp.Get_Fields("highestgroup") + 1);
                    lngPPGroup = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("groupnumber"));
                    lngREGroup = lngPPGroup;
                    clsLoad.Set_Fields("datecreated", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
                    clsLoad.Update();
                    lngPPGroup = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("id"));
                    lngREGroup = lngPPGroup;
				}
				if (lngPPGroup == 0)
					lngPPGroup = lngREGroup;
				if (lngREGroup == 0)
					lngREGroup = lngPPGroup;
				// add or update the association entries
				if (boolOKToAssociate)
				{
					clsLoad.Execute("update moduleassociation set primaryassociate = 0 where remasteracct = " + FCConvert.ToString(lngREAcctToAdd), "CentralData");
				}
				if (boolAddPP)
				{
					if (boolOKToAssociate)
					{
						clsLoad.Execute("insert into moduleassociation (Groupnumber,module,account,primaryassociate,remasteracct) values (" + FCConvert.ToString(lngPPGroup) + ",'PP'," + FCConvert.ToString(modPPGN.Statics.CurrentAccount) + ",1," + FCConvert.ToString(lngREAcctToAdd) + ")", "CentralData");
					}
					else
					{
						clsLoad.Execute("insert into moduleassociation (Groupnumber,module,account,primaryassociate,remasteracct) values (" + FCConvert.ToString(lngPPGroup) + ",'PP'," + FCConvert.ToString(modPPGN.Statics.CurrentAccount) + ",0," + FCConvert.ToString(lngREAcctToAdd) + ")", "CentralData");
					}
				}
				else
				{
					if (boolOKToAssociate)
					{
						clsLoad.Execute("update moduleassociation set primaryassociate = 1,remasteracct = " + FCConvert.ToString(lngREAcctToAdd) + " where module = 'PP' and account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount), "CentralData");
					}
					else
					{
						clsLoad.Execute("update moduleassociation set primaryassociate = 0,remasteracct = " + FCConvert.ToString(lngREAcctToAdd) + " where module = 'PP' and account = " + FCConvert.ToString(modPPGN.Statics.CurrentAccount), "CentralData");
					}
				}
				if (boolAddRE)
				{
					if (boolAddPP)
					{
						clsLoad.Execute("insert into moduleassociation (groupnumber,module,account,groupprimary) values (" + FCConvert.ToString(lngREGroup) + ",'RE'," + FCConvert.ToString(lngREAcctToAdd) + ",1)", "CentralData");
					}
					else
					{
						clsLoad.Execute("insert into moduleassociation (groupnumber,module,account,groupprimary) values (" + FCConvert.ToString(lngREGroup) + ",'RE'," + FCConvert.ToString(lngREAcctToAdd) + ",0)", "CentralData");
					}
				}
				txtREAssociate.Text = FCConvert.ToString(lngREAcctToAdd);
				// If (Trim(txtName.Text) = vbNullString And Trim(txtAddress1.Text) = vbNullString) Or (Trim(txtAddress1.Text) = vbNullString And Trim(txtAddress2.Text) = vbNullString And Trim(txtCity.Text) = vbNullString) Then
				// Call clsLoad.OpenRecordset("select * from master where rscard = 1 and rsaccount = " & lngREAcctToAdd, "twre0000.vb1")
				// If Not clsLoad.EndOfFile Then
				// If Trim(txtName.Text) = vbNullString Then
				// txtName.Text = Trim(clsLoad.Fields("rsname"))
				// End If
				// txtAddress1.Text = Trim(clsLoad.Fields("rsaddr1"))
				// txtAddress2.Text = Trim(clsLoad.Fields("rsaddr2"))
				// txtCity.Text = Trim(clsLoad.Fields("rsaddr3"))
				// txtState.Text = Trim(clsLoad.Fields("rsstate"))
				// txtZip.Text = clsLoad.Fields("rszip")
				// txtZip4.Text = clsLoad.Fields("rszip4")
				// If Trim(txtStreetName.Text) = vbNullString Then
				// txtStreetName.Text = Trim(clsLoad.Fields("rslocstreet"))
				// txtStreetNumber.Text = Val(Trim(clsLoad.Fields("rslocnumalph")))
				// End If
				// End If
				// End If
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In mnuChangeAssociation_click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ReCalcExemptions()
		{
			try
			{
				// On Error GoTo ErrorHandler
				// vbPorter upgrade warning: lngExempt1 As int	OnWrite(int, double)
				int lngExempt1 = 0;
				int lngExempt2 = 0;
				int lngExemption = 0;
				int intExemptcode1;
				int intExemptCode2;
				int lngTotalValue = 0;
				int x;
				int[] lngCat = new int[9 + 1];
				double dblRatio;
				int lngTemp = 0;
				// vbPorter upgrade warning: lngExemptAmount As int	OnWrite(double, int)
				int lngExemptAmount = 0;
				for (x = 0; x <= 9; x++)
				{
					lngCat[x] = 0;
				}
				// x
				if (Conversion.Val(txtTotal.Text) > 0)
				{
					lngTotalValue = FCConvert.ToInt32(FCConvert.ToDouble(txtTotal.Text));
				}
				else
				{
					lngTotalValue = 0;
				}
				dblRatio = modGlobal.Statics.CustomizeStuff.dblRatio;
				intExemptcode1 = FCConvert.ToInt32(Math.Round(Conversion.Val(txtExemptCode1.Text)));
				intExemptCode2 = FCConvert.ToInt32(Math.Round(Conversion.Val(txtExemptCode2.Text)));
				if (Conversion.Val(txtExemption.Text) > 0)
				{
					lngExemption = FCConvert.ToInt32(FCConvert.ToDouble(txtExemption.Text));
				}
				else
				{
					lngExemption = 0;
				}
				if (lngExemption > 0)
				{
					if (intExemptCode2 > 0)
					{
						clsDRWrapper rsExempts = new clsDRWrapper();
						rsExempts.OpenRecordset("select * from exemptcodes order by code", modPPGN.strPPDatabase);
						if (intExemptcode1 > 0)
						{
							if (rsExempts.FindFirstRecord("code", intExemptcode1))
							{
								if (intExemptcode1 > 60 && intExemptcode1 < 80 && intExemptcode1 != 70)
								{
									if ((intExemptcode1 - 70) > 0)
									{
										x = intExemptcode1 - 70;
									}
									else
									{
										x = intExemptcode1 - 60;
									}
									lngTemp = 0;
									switch (x)
									{
										case 1:
											{
												if (Conversion.Val(txtCategory1.Text) > 0)
												{
													lngTemp = FCConvert.ToInt32(FCConvert.ToDouble(txtCategory1.Text));
												}
												break;
											}
										case 2:
											{
												if (Conversion.Val(txtCategory2.Text) > 0)
												{
													lngTemp = FCConvert.ToInt32(FCConvert.ToDouble(txtCategory2.Text));
												}
												break;
											}
										case 3:
											{
												if (Conversion.Val(txtCategory3.Text) > 0)
												{
													lngTemp = FCConvert.ToInt32(FCConvert.ToDouble(txtCategory3.Text));
												}
												break;
											}
										case 4:
											{
												if (Conversion.Val(txtCategory4.Text) > 0)
												{
													lngTemp = FCConvert.ToInt32(FCConvert.ToDouble(txtCategory4.Text));
												}
												break;
											}
										case 5:
											{
												if (Conversion.Val(txtCategory5.Text) > 0)
												{
													lngTemp = FCConvert.ToInt32(FCConvert.ToDouble(txtCategory5.Text));
												}
												break;
											}
										case 6:
											{
												if (Conversion.Val(txtCategory6.Text) > 0)
												{
													lngTemp = FCConvert.ToInt32(FCConvert.ToDouble(txtCategory6.Text));
												}
												break;
											}
										case 7:
											{
												if (Conversion.Val(txtCategory7.Text) > 0)
												{
													lngTemp = FCConvert.ToInt32(FCConvert.ToDouble(txtCategory7.Text));
												}
												break;
											}
										case 8:
											{
												if (Conversion.Val(txtCategory8.Text) > 0)
												{
													lngTemp = FCConvert.ToInt32(FCConvert.ToDouble(txtCategory8.Text));
												}
												break;
											}
										case 9:
											{
												if (Conversion.Val(txtCategory9.Text) > 0)
												{
													lngTemp = FCConvert.ToInt32(FCConvert.ToDouble(txtCategory9.Text));
												}
												break;
											}
									}
									//end switch
									// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
									if (Conversion.Val(rsExempts.Get_Fields("amount")) == 0)
									{
										lngExempt1 = lngTemp;
										if (lngExempt1 > lngExemption)
											lngExempt1 = lngExemption;
										lngExemption -= lngExempt1;
									}
									else
									{
										// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
										lngExempt1 = FCConvert.ToInt32(Conversion.Val(rsExempts.Get_Fields("amount")) * dblRatio);
										if (lngExempt1 > lngTemp)
										{
											lngExempt1 = lngTemp;
										}
										if (lngExempt1 > lngExemption)
											lngExempt1 = lngExemption;
										lngExemption -= lngExempt1;
									}
								}
								else
								{
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									if (Conversion.Val(rsExempts.Get_Fields("Amount")) == 0)
									{
										lngExempt1 = lngTotalValue;
										if (lngExempt1 > lngExemption)
											lngExempt1 = lngExemption;
										lngExemption -= lngExempt1;
									}
									else
									{
										// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
										lngExemptAmount = FCConvert.ToInt32(Conversion.Val(rsExempts.Get_Fields("amount")) * dblRatio);
										if (lngExemptAmount > lngExemption)
											lngExemptAmount = lngExemption;
										lngExempt1 = lngExemptAmount;
										lngExemption -= lngExempt1;
									}
								}
							}
							if (intExemptCode2 > 0 && lngExemption > 0)
							{
								lngExempt2 = lngExemption - lngExempt1;
								// If rsExempts.FindFirstRecord("", intExemptCode2) Then
								// If intExemptCode2 > 60 And intExemptCode2 < 80 And intExemptCode2 <> 70 Then
								// If (intExemptCode2 - 70) > 0 Then
								// x = intExemptCode2 - 70
								// Else
								// x = intExemptCode2 - 60
								// End If
								// lngTemp = 0
								// Select Case x
								// Case 1
								// If Val(txtCategory1.Text) > 0 Then
								// lngTemp = CLng(txtCategory1.Text)
								// End If
								// Case 2
								// If Val(txtCategory2.Text) > 0 Then
								// lngTemp = CLng(txtCategory2.Text)
								// End If
								// Case 3
								// If Val(txtCategory3.Text) > 0 Then
								// lngTemp = CLng(txtCategory3.Text)
								// End If
								// Case 4
								// If Val(txtCategory4.Text) > 0 Then
								// lngTemp = CLng(txtCategory4.Text)
								// End If
								// Case 5
								// If Val(txtCategory5.Text) > 0 Then
								// lngTemp = CLng(txtCategory5.Text)
								// End If
								// Case 6
								// If Val(txtCategory6.Text) > 0 Then
								// lngTemp = CLng(txtCategory6.Text)
								// End If
								// Case 7
								// If Val(txtCategory7.Text) > 0 Then
								// lngTemp = CLng(txtCategory7.Text)
								// End If
								// Case 8
								// If Val(txtCategory8.Text) > 0 Then
								// lngTemp = CLng(txtCategory8.Text)
								// End If
								// Case 9
								// If Val(txtCategory9.Text) > 0 Then
								// lngTemp = CLng(txtCategory9.Text)
								// End If
								// End Select
								// If Val(rsExempts.Fields("amount")) = 0 Then
								// lngExempt2 = lngTemp
								// If lngExempt2 > lngExemption Then lngExempt2 = lngExemption
								// lngExemption = lngExemption - lngExempt2
								// Else
								// lngExempt2 = Val(rsExempts.Fields("amount")) * dblRatio
								// If lngExempt2 > lngTemp Then
								// lngExempt2 = lngTemp
								// End If
								// If lngExempt2 > lngExemption Then lngExempt2 = lngExemption
								// lngExemption = lngExemption - lngExempt2
								// End If
								// Else
								// If Val(rsExempts.Fields("Amount")) = 0 Then
								// lngExempt2 = lngTotalValue
								// If lngExempt2 > lngExemption Then lngExempt2 = lngExemption
								// lngExemption = lngExemption - lngExempt2
								// Else
								// lngExemptAmount = Val(rsExempts.Fields("amount")) * dblRatio
								// If lngExemptAmount > lngExemption Then lngExemptAmount = lngExemption
								// lngExempt2 = lngExemptAmount
								// lngExemption = lngExemption - lngExempt2
								// End If
								// End If
								// End If
							}
						}
						else
						{
							lngExempt1 = 0;
							lngExempt2 = lngExemption;
						}
					}
					else
					{
						lngExempt1 = lngExemption;
						lngExempt2 = 0;
					}
				}
				else
				{
					lngExempt1 = 0;
					lngExempt2 = 0;
				}
				lngExemption1 = lngExempt1;
				lngExemption2 = lngExempt2;
				ToolTip1.SetToolTip(txtExemptCode1, Strings.Format(lngExempt1, "#,###,###,##0"));
				ToolTip1.SetToolTip(txtExemptCode2, Strings.Format(lngExempt2, "#,###,###,##0"));
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ReCalcExemptions", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ShowOwner()
		{
			if (theAccount.PartyID > 0)
			{
				if (!(theAccount.OwnerParty() == null))
				{
					lblOwner1.Text = theAccount.OwnerParty().FullName;
				}
				else
				{
					lblOwner1.Text = "";
				}
				txtOwnerID.Text = FCConvert.ToString(theAccount.PartyID);
			}
			else
			{
				lblOwner1.Text = "";
				txtOwnerID.Text = FCConvert.ToString(0);
			}
		}

		private void ShowAddress()
		{
			if (!(theAccount.OwnerParty() == null))
			{
				cPartyAddress tAdd;
				tAdd = theAccount.OwnerParty().GetPrimaryAddress();
				if (!(tAdd == null))
				{
					lblAddress.Text = tAdd.GetFormattedAddress();
				}
				else
				{
					lblAddress.Text = "";
				}
			}
			else
			{
				lblAddress.Text = "";
			}
		}

		private void cmdRemoveOwner_Click(object sender, System.EventArgs e)
		{
			theAccount.PartyID = 0;
			if (!(theAccount.OwnerParty() == null))
			{
				theAccount.OwnerParty().Clear();
			}
			ShowOwner();
			ShowAddress();
		}

		private void cmdEditOwner_Click(object sender, System.EventArgs e)
		{
			if (!(theAccount == null))
			{
				if (theAccount.PartyID > 0)
				{
					int lngReturn;
					int lngPartyID = theAccount.PartyID;
					lngReturn = frmEditCentralParties.InstancePtr.Init(ref lngPartyID);
					theAccount.PartyID = lngPartyID;
					cPartyController tCont = new cPartyController();
					cParty tOwnerParty = theAccount.OwnerParty();
					tCont.LoadParty(ref tOwnerParty, theAccount.PartyID);
					ShowOwner();
					ShowAddress();
				}
			}
		}

		private void cmdSearchOwner_Click(object sender, System.EventArgs e)
		{
			int lngPrice;
			int intType;
			int intFinancing;
			int intVerified;
			int intValidity;
			DateTime dtDate;
			clsDRWrapper clsGroup = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				int lngReturnID;
				lngReturnID = frmCentralPartySearch.InstancePtr.Init();
				cPartyController tCont = new cPartyController();
				cParty tOwnerParty = theAccount.OwnerParty();
				tCont.LoadParty(ref tOwnerParty, lngReturnID);
				theAccount.PartyID = lngReturnID;
				ShowOwner();
				ShowAddress();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SearchOwner", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtOwnerID_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtOwnerID.Text) != theAccount.PartyID)
			{
				cParty tParty = new cParty();
				cPartyController tCont = new cPartyController();
				if (!tCont.LoadParty(ref tParty, FCConvert.ToInt32(Conversion.Val(txtOwnerID.Text))))
				{
					e.Cancel = true;
					MessageBox.Show("That party doesn't exist.", "Invalid Party", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtOwnerID.Text = FCConvert.ToString(theAccount.PartyID);
					return;
				}
				theAccount.PartyID = FCConvert.ToInt32(Math.Round(Conversion.Val(txtOwnerID.Text)));
				cParty tOwnerParty = theAccount.OwnerParty();
				tCont.LoadParty(ref tOwnerParty, FCConvert.ToInt32(Conversion.Val(txtOwnerID.Text)));
				ShowOwner();
				ShowAddress();
			}
		}
	}
}
