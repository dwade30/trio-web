﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptMaster.
	/// </summary>
	public partial class rptMaster : BaseSectionReport
	{
		public rptMaster()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Master Account Information";
		}

		public static rptMaster InstancePtr
		{
			get
			{
				return (rptMaster)Sys.GetInstance(typeof(rptMaster));
			}
		}

		protected rptMaster _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsLoad?.Dispose();
                clsLoad = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptMaster	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsLoad = new clsDRWrapper();
		int intPage;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsLoad.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			intPage = 0;
			txtDate.Text = Strings.Format(DateTime.Today, "M/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			txtMuni.Text = Strings.Trim(modGlobalConstants.Statics.MuniName);
			if (Strings.Trim(modPPGN.Statics.Open1Title) != string.Empty)
			{
				lblOpen1.Text = Strings.Trim(modPPGN.Statics.Open1Title);
			}
			if (Strings.Trim(modPPGN.Statics.Open2Title) != string.Empty)
			{
				lblOpen2.Text = Strings.Trim(modPPGN.Statics.Open2Title);
			}
		}

		public void Init(string strSQL)
		{
			clsLoad.OpenRecordset(strSQL, "twpp0000.vb1");
			if (clsLoad.EndOfFile())
			{
				MessageBox.Show("No records found");
				this.Close();
				return;
			}
			////Application.DoEvents();
			//FC:FINAL:AM: use the report viewer
			//this.Show(App.MainForm);
			frmReportViewer.InstancePtr.Init(this);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			string strTemp = "";
            using (clsDRWrapper clsTemp = new clsDRWrapper())
            {
                if (!clsLoad.EndOfFile())
                {
                    // TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
                    clsTemp.OpenRecordset(
                        "select * from moduleassociation where account = " + clsLoad.Get_Fields("account") +
                        " and module = 'PP' and primaryassociate = 1", modPPGN.Statics.strCentralData);
                    if (!clsTemp.EndOfFile())
                    {
                        txtREAssoc.Text = FCConvert.ToString(clsTemp.Get_Fields_Int32("remasteracct"));
                    }
                    else
                    {
                        txtREAssoc.Text = "";
                    }

                    cPartyController tPCont = new cPartyController();
                    cParty tParty;
                    txtName.Text = "";
                    txtAddr1.Text = "";
                    txtAddr2.Text = "";
                    txtCity.Text = "";
                    txtState.Text = "";
                    txtZip.Text = "";
                    // txtZip4.Text = ""
                    txtFullAddress.Text = "";
                    txtPhone.Text = "";
                    // TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
                    txtAccount.Text = FCConvert.ToString(clsLoad.Get_Fields("account"));
                    if (clsLoad.Get_Fields_Int32("PartyID") > 0)
                    {
                        tParty = tPCont.GetParty(clsLoad.Get_Fields_Int32("partyid"), false);
                        if (!(tParty == null))
                        {
                            txtName.Text = tParty.FullNameLastFirst;
                            if (tParty.Addresses.Count > 0)
                            {
                                cPartyAddress tAddr;
                                tAddr = tParty.GetPrimaryAddress();
                                if (!(tAddr == null))
                                {
                                    txtFullAddress.Text = tAddr.GetFormattedAddress();
                                    // txtAddr1.Text = tAddr.Address1
                                    // txtAddr2.Text = tAddr.Address2
                                    // txtCity.Text = tAddr.City
                                    // txtState.Text = tAddr.State
                                    // txtZip.Text = tAddr.Zip
                                }
                            }

                            if (tParty.PhoneNumbers.Count > 0)
                            {
                                // vbPorter upgrade warning: tPhon As cPartyPhoneNumber	OnWrite(Collection)
                                cPartyPhoneNumber tPhon;
                                tPhon = tParty.PhoneNumbers[1];
                                if (!(tPhon == null))
                                {
                                    txtPhone.Text = tPhon.PhoneNumber;
                                }
                            }
                        }
                    }

                    // TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
                    txtLocation.Text = Strings.Trim(clsLoad.Get_Fields("streetnumber") +
                                                    clsLoad.Get_Fields_String("streetapt") + " " +
                                                    clsLoad.Get_Fields_String("street"));
                   
                    txtValue.Text = FCConvert.ToString(clsLoad.Get_Fields("value"));
                    txtExemption.Text = FCConvert.ToString(clsLoad.Get_Fields_Int32("exemption"));
                    // TODO Get_Fields: Check the table for the column [exemptcode1] and replace with corresponding Get_Field method
                    txtCd1.Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("exemptcode1")));
                    // TODO Get_Fields: Check the table for the column [exemptcode2] and replace with corresponding Get_Field method
                    txtCd2.Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("exemptcode2")));
                    // TODO Get_Fields: Check the table for the column [trancode] and replace with corresponding Get_Field method
                    txtTrancode.Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("trancode")));
                    txtBusinesscode.Text = FCConvert.ToString(clsLoad.Get_Fields_Int32("businesscode"));
                    // TODO Get_Fields: Check the table for the column [streetcode] and replace with corresponding Get_Field method
                    txtStreetCode.Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("streetcode")));
                    txtSQFT.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields_Int32("squarefootage")),
                        "#,###,##0");
                    txtOpen1.Text = clsLoad.Get_Fields_String("open1");
                    txtOpen2.Text = clsLoad.Get_Fields_String("open2");
                    clsLoad.MoveNext();
                }
            }
        }

		private void PageHeader_Format(object sender, EventArgs e)
		{
			intPage += 1;
			txtPage.Text = "Page: " + FCConvert.ToString(intPage);
		}

		
	}
}
