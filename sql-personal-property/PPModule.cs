﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;
using System.Runtime.InteropServices;
using fecherFoundation.VisualBasicLayer;
using System.IO;
using SharedApplication.Enums;
using TWSharedLibrary;

namespace TWPP0000
{
	public class modPPGN
	{
        public static string strCLDatabase;
        public static string strREDatabase;
        public static string strPPDatabase = "";
       
		public static string StringToSpaces_8(object intValue, int intLength)
		{
			return StringToSpaces(ref intValue, ref intLength);
		}

		public static string StringToSpaces(ref object intValue, ref int intLength)
		{
			string StringToSpaces = "";
			if (fecherFoundation.FCUtils.IsNull(intValue))
				intValue = "0";
			if (intLength < FCConvert.ToString(intValue).Length)
			{
				StringToSpaces = Strings.Left(FCConvert.ToString(intValue), FCConvert.ToInt32(intLength));
			}
			else
			{
				StringToSpaces = Strings.Trim(FCConvert.ToString(intValue)) + Strings.StrDup(intLength - FCConvert.ToString(intValue).Length, " ");
			}
			return StringToSpaces;
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		public static string PadtoLong(ref string lngValue, ref short intDigits)
		{
			string PadtoLong = "";
			// takes a value and strips commas then adds zeros to
			// front to make a specific length.  Returns a long
			int LNG = 0;
			if (Conversion.Val(lngValue) > 0)
				LNG = FCConvert.ToInt32(FCConvert.ToDouble(lngValue));
			else
				LNG = 0;
			lngValue = LNG.ToString();
			PadtoLong = Strings.StrDup(intDigits - lngValue.Length, "0") + lngValue;
			return PadtoLong;
		}

		public static void Main()
		{
			//FileSystemObject fso = new FileSystemObject();
			DateTime tempDate;
			// The Password to get into the personal property database
			// Dim ff As New FileSystemObject
			try
			{
				// On Error GoTo ErrorHandler
				modGlobal.Statics.lngDontAllowUnload = modGlobal.CNSTALLOWUNLOAD;
				Statics.boolRE = false;
				strREDatabase = "RealEstate";
				strPPDatabase = "PersonalProperty";
				Statics.strSystemSettings = "SystemSettings";
				Statics.strCentralData = "CentralData";
				strCLDatabase = "Collections";
				Statics.strUTDatabase = "UtilityBilling";
				Statics.boolInArchiveMode = false;
				//FC:FINA:MHS - issue #1518: reset archive data for disabling archive mode after reloading module
				modGlobalConstants.Statics.gstrArchiveYear = "";
				string strDataPath;
				strDataPath = FCFileSystem.CurDir();
				if (Strings.Right(strDataPath, 1) != "\\")
				{
					strDataPath += "\\";
				}
				// kk04082015 troges-39  Change to single config file
				if (!modGlobalFunctions.LoadSQLConfig("TWPP0000.VB1"))
				{
					MessageBox.Show("Error loading connection information", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}
				clsDRWrapper rs = new clsDRWrapper();
				// Dim s                           As New SettingsInfo
				// S.LoadSettingsAndArchives
				// If Not S.LoadSettingsAndArchives(strDataPath & "trioworkstationsetup.xml") Then
				// Call MsgBox("Unable to load settings file", vbCritical, "Cannot Continue")
				// End
				// Exit Sub
				// End If
				// rs.DefaultGroup = "Live"
				// rs.DefaultDB = "PersonalProperty"
				// If rs.MegaGroupsCount > 0 Then
				// rs.DefaultMegaGroup = rs.AvailableMegaGroups(0).GroupName
				// End If
				modGlobalFunctions.GetGlobalRegistrySetting();
				// If App.PrevInstance Then
				// MsgBox "Warning. There is already a Personal Property running on this computer." & vbNewLine & "Although this is most likely harmless, if they are both using the same database this is probably unintended and one should be closed.", vbExclamation
				// End If
				// If File.Exists(App.Path & "\twpp0000.hlp") Then
				// App.HelpFile = "TWPP0000.HLP"
				// End If
				modGlobal.Statics.CustomizeStuff.boolRegionalTown = false;
				if (modRegionalTown.IsRegionalTown())
				{
					modGlobal.Statics.CustomizeStuff.boolRegionalTown = true;
				}
				CheckVersion();
				Statics.strDbCP = rs.Get_GetFullDBName("CentralParties") + ".dbo.";
				// 
				// If Not IsDate(GetRegistryKey("PPCheckCompactDate")) Then
				// CheckCompactDate
				// SaveRegistryKey "PPCheckCompactDate", Date
				// ElseIf CDate(GetRegistryKey("PPCheckCompactDate")) < Date Then
				// CheckCompactDate
				// SaveRegistryKey "PPCheckCompactDate", Date
				// End If
				rs.OpenRecordset("select * from modules", "SystemSettings");
				// TODO Get_Fields: Check the table for the column [pp] and replace with corresponding Get_Field method
				if (!FCConvert.ToBoolean(rs.Get_Fields("pp")))
				{
					if (!FCConvert.ToBoolean(rs.Get_Fields_Boolean("bl")))
					{
						MessageBox.Show("The database indicates you do not have Personal Property or Billing.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
						//Application.Exit();
					}
					else
					{
						//FC:FINAL:MSH - avoid exception if field is an empty string
						//tempDate = (DateTime)rs.Get_Fields("bldate");
						tempDate = FCConvert.ToDateTime(rs.Get_Fields_DateTime("bldate"));
						if (tempDate.ToOADate() < DateTime.Today.ToOADate())
						{
							MessageBox.Show("Billing has expired.  Please call Trio.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
							//Application.Exit();
						}
						else
						{
							Statics.boolShortScreenOnly = true;
						}
					}
				}
				else
				{
					//FC:FINAL:MSH - avoid exception if field is an empty string
					//tempDate = (DateTime)rs.Get_Fields("ppdate");
					tempDate = FCConvert.ToDateTime(rs.Get_Fields_DateTime("ppdate"));
					if (tempDate.ToOADate() < DateTime.Today.ToOADate())
					{
						if (!FCConvert.ToBoolean(rs.Get_Fields_Boolean("bl")))
						{
							MessageBox.Show("Personal Property has expired. Please call Trio", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
							//Application.Exit();
						}
						else
						{
							//FC:FINAL:MSH - avoid exception if field is an empty string
							//tempDate = (DateTime)rs.Get_Fields("bldate");
							tempDate = FCConvert.ToDateTime(rs.Get_Fields_DateTime("bldate"));
							if (tempDate.ToOADate() < DateTime.Today.ToOADate())
							{
								MessageBox.Show("Personal Property has expired. Please call Trio", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
								//Application.Exit();
							}
							else
							{
								Statics.boolShortScreenOnly = true;
							}
						}
					}
					else
					{
						Statics.boolShortScreenOnly = false;
					}
				}
				// Call rs.OpenRecordset("select PP_MaxAccounts from modules", "SystemSettings")
				// If rs.EndOfFile Then
				modGlobal.Statics.gintMaxAccounts = modGlobalConstants.EleventyBillion;
				
				modReplaceWorkFiles.Statics.gstrNetworkFlag = (modRegistry.GetRegistryKey("NetworkFlag") != string.Empty ? modReplaceWorkFiles.Statics.gstrReturn : "Y");
				//FC:FINAL:SBE - we cannot store userid as a global settings, because webapp is used by multiple users at the same time
				//modReplaceWorkFiles.Statics.gintSecurityID = FCConvert.ToInt32(modRegistry.GetRegistryKey("SecurityID") != string.Empty ? Conversion.Val(modReplaceWorkFiles.Statics.gstrReturn) : 0);
				modReplaceWorkFiles.Statics.gintSecurityID = TWSharedLibrary.Variables.Statics.IntUserID;
				modReplaceWorkFiles.GetGlobalVariables();
				modGNWork.GetLocalVariables2();
				modGlobalFunctions.UpdateUsedModules();
				modGlobal.Statics.CustomizeStuff.boolRegionalTown = false;
				if (modRegionalTown.IsRegionalTown())
				{
					modGlobal.Statics.CustomizeStuff.boolRegionalTown = true;
				}
				if (modGlobalConstants.Statics.gstrArchiveYear != string.Empty)
				{
					Statics.boolInArchiveMode = true;
				}
				if (modPPCalculate.Statics.PPRounding == 1)
					modPPCalculate.Statics.PPRounding = -3;
				if (modPPCalculate.Statics.PPRounding == 2)
					modPPCalculate.Statics.PPRounding = -2;
				if (modPPCalculate.Statics.PPRounding == 3)
					modPPCalculate.Statics.PPRounding = -1;
				if (modPPCalculate.Statics.PPRounding == 4)
					modPPCalculate.Statics.PPRounding = 0;
				// matthew
				// If WWK.WPPFLAG = "Y" Then
				Statics.PPBillingAssessing = true;
				// Else
				// PPBillingAssessing = False
				// End If
				// 
				Statics.DatabaseName = "TWPP0000.VB1";
				Statics.boolFromBilling = false;
				modReplaceWorkFiles.EntryFlagFile(true);
				if (modGlobalConstants.Statics.gstrEntryFlag == "BL")
				{
					Statics.boolFromBilling = true;
				}
				modReplaceWorkFiles.EntryFlagFile(true, "", true);
				// If Valid database name then put into workrecord
				// Call WPP.SetData("WDATABASELOCATION", DatabaseName)
				// Call SaveLocalVariables
				// 
				modReplaceWorkFiles.Statics.rsVariables = null;
				if (Statics.CurrentAccount == -1)
					Statics.CurrentAccount = 1;
				Statics.CurrentAccount = FCConvert.ToInt32(modRegistry.GetRegistryKey("PPLastAccountNumber") != string.Empty ? Conversion.Val(modReplaceWorkFiles.Statics.gstrReturn) : 0);

				FCFileSystem.FileClose(31);
				FCFileSystem.FileClose(32);
				// 
				// MsgBox Screen.Width
				GetOpenFields();
				// 
				// Gets User Name on Startup and unlocks any records that
				// may have been locked by that user that were not cleaned up
				// when user exited.  this will also ensure that if the user
				// exits personal property improperly that there are no locks
				// hanging around by that person.
				// 
				// Call UserName
				// 
				// If App.PrevInstance = False then we do not want to unlock
				// all records by User Name, this will ensure that User 'A' does
				// not open the same account with two applications
				// If App.PrevInstance = False Then
				// Call UnlockAllRecordsByUser
				// Else
				// MsgBox "There is another Personal Property program already running.", 16
				// End If
				// Call SecurityProcess
				// Call Check_Valuation_Table
				modGlobalConstants.Statics.clsSecurityClass = new clsTrioSecurity();
				modGlobalConstants.Statics.clsSecurityClass.Init("PP", "SystemSettings");
				Statics.strCostFiles = "";
				MDIParent.InstancePtr.Init();
				// mdiparent.show
				// 
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Main", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				//Interaction.Shell(FCFileSystem.Statics.UserDataFolder + "\\twgnenty.exe", System.Diagnostics.ProcessWindowStyle.Maximized, false, -1);
				//Application.Exit();
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public static bool CheckVersion()
		{
			bool CheckVersion = false;
			cPPDatabase pDB = new cPPDatabase();
			if (!pDB.CheckVersion())
			{
				cVersionInfo tVer;
				tVer = pDB.GetVersion();
				MessageBox.Show("Error checking database." + "\r\n" + tVer.VersionString);
				return false;
			}

            var util = new cArchiveUtility();
            var archives = util.GetArchiveGroupNames("CommitmentArchive", 0);
            var wrapper = new clsDRWrapper();
            var liveGroup = wrapper.DefaultGroup;
            try
            {
                foreach (string archive in archives)
                {
                    wrapper.DefaultGroup = archive;
                    var archiveDB = new cPPDatabase();
                    archiveDB.CheckVersion();
                    var archiveCentralData = new cCentralDataDB();
                    archiveCentralData.CheckVersion();
                    var archiveCentralParties = new cCentralParties();
                    archiveCentralParties.CheckVersion();
                }
			}
            catch
            {
                return false;
            }
            finally
            {
                wrapper.DefaultGroup = liveGroup;
            }
            
			CheckVersion = true;
			return CheckVersion;
		}

		public static string GetMasterJoin(bool boolNameOnly = false)
		{
			string GetMasterJoin = "";
			string strReturn = "";
			clsDRWrapper tLoad = new clsDRWrapper();
			string strTemp;
			string strPP;
			if (!boolNameOnly)
			{
				strReturn = "select ppmaster.*, cpo.FullNameLF as Name,cpo.address1 as address1,cpo.address2 as address2,cpo.address3 as address3,cpo.city as City, cpo.state as state,cpo.zip as zip, '' as zip4, cpo.email as email from ";
			}
			else
			{
				strReturn = "select ppmaster.*, cpo.FullNameLF as Name from ";
			}
			strTemp = tLoad.Get_GetFullDBName("PersonalProperty");
			strPP = strTemp;
			strReturn += strTemp + ".dbo.ppmaster left join ";
			strTemp = tLoad.Get_GetFullDBName("CentralParties");
			if (!boolNameOnly)
			{
				strTemp += ".dbo.PartyAndAddressView ";
				strReturn += strTemp + " as cpo on (" + strPP + ".dbo.ppmaster.partyid = cpo.PartyID)  ";
			}
			else
			{
				strTemp += ".dbo.PartyNameView ";
				strReturn += strTemp + " as cpo on (" + strPP + ".dbo.ppmaster.partyid = cpo.ID)  ";
			}
			strReturn = " select * from (" + strReturn + ") mparty ";
			GetMasterJoin = strReturn;
			return GetMasterJoin;
		}

		public static string GetMasterJoinForJoin(bool boolNameOnly = false)
		{
			string GetMasterJoinForJoin = "";
			string strReturn;
			strReturn = "(" + GetMasterJoin(boolNameOnly) + ") mj ";
			GetMasterJoinForJoin = strReturn;
			return GetMasterJoinForJoin;
		}

		public static string GetREMasterJoin()
		{
			string GetREMasterJoin = "";
			// select master.*, cpo.FullNameLastFirst as RSName,cpso.FullNameLastFirst as RSSecOwner,cpo.address1 as rsaddr1,cpo.address2 as rsaddr2,cpo.address3 as rsaddress3,cpo.city as rsaddr3, cpo.state as rsstate,cpo.zip as rszip, '' as rszip4 from TRIO_Test_Live_RealEstate.dbo.master left join TRIO_Test_Live_CentralParties.dbo.PartyAndAddressView as cpo on (master.ownerpartyid = cpo.PartyID) left join TRIO_Test_Live_CentralParties.dbo.PartyAndAddressView as cpso on (master.SecOwnerPartyID = cpso.PartyID)
			string strReturn;
			strReturn = "select master.*, cpo.FullNameLF as RSName,cpso.FullNameLF as RSSecOwner,cpo.address1 as rsaddr1,cpo.address2 as rsaddr2,cpo.address3 as rsaddress3,cpo.city as rsaddr3, cpo.state as rsstate,cpo.zip as rszip, '' as rszip4, cpo.email as email from ";
			clsDRWrapper tLoad = new clsDRWrapper();
			string strTemp;
			strTemp = tLoad.Get_GetFullDBName("RealEstate");
			strReturn += strTemp + ".dbo.master left join ";
			strTemp = tLoad.Get_GetFullDBName("CentralParties");
			strTemp += ".dbo.PartyAndAddressView ";
			strReturn += strTemp + " as cpo on (master.ownerpartyid = cpo.PartyID) left join ";
			strReturn += strTemp + " as cpso on (master.SecOwnerPartyID = cpso.PartyID)";
			strReturn = " select * from (" + strReturn + ") mparty ";
			GetREMasterJoin = strReturn;
			return GetREMasterJoin;
		}

		public static string GetREMasterJoinForJoin()
		{
			string GetREMasterJoinForJoin = "";
			string strReturn;
			strReturn = "(" + GetREMasterJoin() + ") mj ";
			GetREMasterJoinForJoin = strReturn;
			return GetREMasterJoinForJoin;
		}

		private static bool CheckTranTownTable()
		{
			bool CheckTranTownTable = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper clsSave = new clsDRWrapper();
				clsDRWrapper clsLoad = new clsDRWrapper();
				CheckTranTownTable = false;
				if (modGlobal.Statics.CustomizeStuff.boolRegionalTown)
				{
					// must force tran code table to be the same as town codes
					clsSave.Execute("delete * from trancodes", strPPDatabase);
					clsLoad.OpenRecordset("select * from tblregions", Statics.strSystemSettings);
					clsSave.OpenRecordset("select * from trancodes", strPPDatabase);
					while (!clsLoad.EndOfFile())
					{
						clsSave.AddNew();
						// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
						clsSave.Set_Fields("tranCODE", clsLoad.Get_Fields("townnumber"));
						clsSave.Set_Fields("trantype", clsLoad.Get_Fields_String("townname"));
						clsSave.Update();
						clsLoad.MoveNext();
					}
				}
				CheckTranTownTable = true;
				return CheckTranTownTable;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In CheckTranTownTable", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CheckTranTownTable;
		}

		public static void Ending()
		{
			Application.Exit();
		}

		public static void GetOpenFields()
		{
			clsDRWrapper rs = new clsDRWrapper();
			string strSQL;
			object strConnect;
			// OpenDatabase(DatabaseName, False, False, ";PWD=" & DATABASEPASSWORD)
			strSQL = "SELECT * FROM PPRatioOpens";
			rs.OpenRecordset(strSQL, strPPDatabase);
			Statics.Open1Title = Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("openfield1")));
			Statics.Open2Title = Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("openfield2")));
			// TODO Get_Fields: Check the table for the column [Ratio] and replace with corresponding Get_Field method
			Statics.PPRatio = FCConvert.ToSingle(FCConvert.ToSingle(rs.Get_Fields("Ratio")) / 100);
			// Put code here to get rounding option from 9,H,2
		}

		public static void UnloadFormPPGNMenu()
		{
			// Unload menuPPMain
		}

		public static void Transfer_Values_To_Billing()
		{
			object Ans;
			clsDRWrapper clsSave = new clsDRWrapper();
			clsDRWrapper rs = new clsDRWrapper();
			clsDRWrapper rsV = new clsDRWrapper();
			clsDRWrapper rsC = new clsDRWrapper();
			bool bolComputeExemptions = false;
			// vbPorter upgrade warning: lngTemp As int	OnWrite(short, double)
			int lngTemp = 0;
			int x;
			try
			{
				// On Error GoTo ErrorHandler
				ContinueTag:
				;
				rs.OpenRecordset("SELECT * FROM PPMaster WHERE NOT Deleted = 1 ORDER BY Account", modPPGN.strPPDatabase);
				rsV.OpenRecordset("SELECT * FROM PPValuations ORDER BY ValueKey", modPPGN.strPPDatabase);
				modGlobalFunctions.AddCYAEntry_8("PP", "Commit to Billing Values");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					// Load frmMessage
					// frmMessage.Label1.Caption = "Transferring New Data"
					// frmMessage.Show
					// frmMessage.Refresh
					frmWait.InstancePtr.Init("Committing Billing Values" + "\r\n" + "Please Wait...");
					clsSave.Execute("update status set xferbilling = '" + FCConvert.ToString(DateTime.Now) + "'", "twpp0000.vb1");
					if (bolComputeExemptions)
					{
						clsSave.Execute("update status set calcexemptions = '" + FCConvert.ToString(DateTime.Now) + "'", "twpp0000.vb1");
					}
					while (!rs.EndOfFile())
					{
						////Application.DoEvents();
						rs.Edit();
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsV.FindFirstRecord("ValueKey", rs.Get_Fields("Account"));
						rs.Set_Fields("Value", 0);
						lngTemp = 0;
						if (FCConvert.ToString(rs.Get_Fields_String("ORCode")) == "Y")
						{
							rs.Set_Fields("Value", FCConvert.ToString(Conversion.Val(rsV.Get_Fields_Int32("OverrideAmount"))));
							goto ExemptionTag;
						}
						if (rsV.NoMatch == false)
						{
							lngTemp += FCConvert.ToInt32(Conversion.Val(rsV.Get_Fields_Int32("category1")));
							lngTemp += FCConvert.ToInt32(Conversion.Val(rsV.Get_Fields_Int32("category2")));
							lngTemp += FCConvert.ToInt32(Conversion.Val(rsV.Get_Fields_Int32("category3")));
							lngTemp += FCConvert.ToInt32(Conversion.Val(rsV.Get_Fields_Int32("category4")));
							lngTemp += FCConvert.ToInt32(Conversion.Val(rsV.Get_Fields_Int32("category5")));
							lngTemp += FCConvert.ToInt32(Conversion.Val(rsV.Get_Fields_Int32("category6")));
							lngTemp += FCConvert.ToInt32(Conversion.Val(rsV.Get_Fields_Int32("category7")));
							lngTemp += FCConvert.ToInt32(Conversion.Val(rsV.Get_Fields_Int32("category8")));
							lngTemp += FCConvert.ToInt32(Conversion.Val(rsV.Get_Fields_Int32("Category9")));
							rs.Set_Fields("Value", lngTemp);
						}
						// Determine Exemption If Any are Applicable
						ExemptionTag:
						;
						rs.Update();
						rs.MoveNext();
					}
				}
				// Unload frmMessage
				frmWait.InstancePtr.Unload();
				////Application.DoEvents();
				MessageBox.Show("Billing values were committed successfully.", "Billing Values Committed", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Transfer Values To Billing");
			}
		}

		public static void ComputeExemptions()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				clsDRWrapper rsExempts = new clsDRWrapper();
				// vbPorter upgrade warning: x As short --> As int	OnWrite(short, double)
				int x = 0;
				int lngTemp = 0;
				clsDRWrapper rsValuation = new clsDRWrapper();
				double dblRatio;
				// vbPorter upgrade warning: lngExemptAmount As int	OnWriteFCConvert.ToDouble(
				int lngExemptAmount = 0;
				// vbPorter upgrade warning: lngBETEExempt As int	OnWrite(short, double)
				int lngBETEExempt;
				int lngExempt1 = 0;
				int lngExempt2 = 0;
				modGlobalFunctions.AddCYAEntry_8("PP", "Calculated Exemptions");
				rsLoad.OpenRecordset("SELECT * FROM PPMaster WHERE NOT Deleted = 1 ORDER BY Account", modPPGN.strPPDatabase);
				rsValuation.OpenRecordset("select * from ppvaluations order by valuekey", modPPGN.strPPDatabase);
				rsExempts.OpenRecordset("select * from exemptcodes order by code", modPPGN.strPPDatabase);
				dblRatio = modGlobal.Statics.CustomizeStuff.dblRatio;
				frmWait.InstancePtr.Init("Calculating ", true);
				frmWait.InstancePtr.Image1.Visible = false;
				while (!rsLoad.EndOfFile())
				{
					lngExempt1 = 0;
					lngExempt2 = 0;
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					frmWait.InstancePtr.lblMessage.Text = "Calculating " + rsLoad.Get_Fields("account");
					frmWait.InstancePtr.lblMessage.Refresh();
					frmWait.InstancePtr.IncrementProgress();
					rsLoad.Edit();
					rsLoad.Set_Fields("Exemption", 0);
					lngBETEExempt = 0;
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					if (rsValuation.FindFirstRecord("valuekey", Conversion.Val(rsLoad.Get_Fields("account"))))
					{
						lngBETEExempt = FCConvert.ToInt32(Conversion.Val(rsValuation.Get_Fields_Int32("cat1exempt")) + Conversion.Val(rsValuation.Get_Fields_Int32("cat2exempt")) + Conversion.Val(rsValuation.Get_Fields_Int32("cat3exempt")) + Conversion.Val(rsValuation.Get_Fields_Int32("cat4exempt")) + Conversion.Val(rsValuation.Get_Fields_Int32("cat5exempt")) + Conversion.Val(rsValuation.Get_Fields_Int32("cat6exempt")) + Conversion.Val(rsValuation.Get_Fields_Int32("cat7exempt")) + Conversion.Val(rsValuation.Get_Fields_Int32("cat8exempt")) + Conversion.Val(rsValuation.Get_Fields_Int32("cat9exempt")));
					}
					// TODO Get_Fields: Check the table for the column [ExemptCode1] and replace with corresponding Get_Field method
					if (Conversion.Val(rsLoad.Get_Fields("ExemptCode1")) != 0)
					{
						// TODO Get_Fields: Check the table for the column [ExemptCode1] and replace with corresponding Get_Field method
						if (rsExempts.FindFirstRecord("code", Conversion.Val(rsLoad.Get_Fields("ExemptCode1"))))
						{
							// TODO Get_Fields: Check the table for the column [exemptcode1] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [exemptcode1] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [exemptcode1] and replace with corresponding Get_Field method
							if (rsLoad.Get_Fields("exemptcode1") > 60 && rsLoad.Get_Fields("exemptcode1") < 80 && FCConvert.ToInt32(rsLoad.Get_Fields("exemptcode1")) != 70)
							{
								// TODO Get_Fields: Check the table for the column [exemptcode1] and replace with corresponding Get_Field method
								if ((rsLoad.Get_Fields("exemptcode1") - 70) > 0)
								{
									// TODO Get_Fields: Check the table for the column [exemptcode1] and replace with corresponding Get_Field method
									x = rsLoad.Get_Fields("exemptcode1") - 70;
								}
								else
								{
									// TODO Get_Fields: Check the table for the column [exemptcode1] and replace with corresponding Get_Field method
									x = rsLoad.Get_Fields("exemptcode1") - 60;
								}
								// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
								if (rsValuation.FindFirstRecord("valuekey", Conversion.Val(rsLoad.Get_Fields("account"))))
								{
									// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
									lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(rsValuation.Get_Fields("category" + FCConvert.ToString(x)))));
									// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
									if (Conversion.Val(rsExempts.Get_Fields("amount")) == 0)
									{
										rsLoad.Set_Fields("exemption", lngTemp);
										lngExempt1 = lngTemp;
									}
									else
									{
										// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
										lngExemptAmount = FCConvert.ToInt32(Conversion.Val(rsExempts.Get_Fields("amount")) * dblRatio);
										if (lngExemptAmount > lngTemp)
										{
											rsLoad.Set_Fields("exemption", lngTemp);
											lngExempt1 = lngTemp;
										}
										else
										{
											rsLoad.Set_Fields("exemption", lngExemptAmount);
											lngExempt1 = lngExemptAmount;
										}
									}
								}
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								if (Conversion.Val(rsExempts.Get_Fields("Amount")) == 0)
								{
									// TODO Get_Fields: Check the table for the column [Value] and replace with corresponding Get_Field method
									rsLoad.Set_Fields("Exemption", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("Value"))));
									// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
									lngExempt1 = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("value"))));
								}
								else
								{
									// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
									lngExemptAmount = FCConvert.ToInt32(Conversion.Val(rsExempts.Get_Fields("amount")) * dblRatio);
									rsLoad.Set_Fields("exemption", Conversion.Val(rsLoad.Get_Fields_Int32("Exemption")) + lngExemptAmount);
									lngExempt1 = lngExemptAmount;
								}
							}
						}
					}
					// TODO Get_Fields: Check the table for the column [ExemptCode2] and replace with corresponding Get_Field method
					if (Conversion.Val(rsLoad.Get_Fields("ExemptCode2")) != 0)
					{
						// TODO Get_Fields: Check the table for the column [exemptcode2] and replace with corresponding Get_Field method
						if (rsExempts.FindFirstRecord("code", Conversion.Val(rsLoad.Get_Fields("exemptcode2"))))
						{
							// TODO Get_Fields: Check the table for the column [exemptcode2] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [exemptcode2] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [exemptcode2] and replace with corresponding Get_Field method
							if (Conversion.Val(rsLoad.Get_Fields("exemptcode2")) > 60 && Conversion.Val(rsLoad.Get_Fields("exemptcode2")) < 80 && Conversion.Val(rsLoad.Get_Fields("exemptcode2")) != 70)
							{
								// TODO Get_Fields: Check the table for the column [exemptcode2] and replace with corresponding Get_Field method
								if ((Conversion.Val(rsLoad.Get_Fields("exemptcode2")) - 70) > 0)
								{
									// TODO Get_Fields: Check the table for the column [exemptcode2] and replace with corresponding Get_Field method
									x = FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields("exemptcode2")) - 70);
								}
								else
								{
									// TODO Get_Fields: Check the table for the column [exemptcode2] and replace with corresponding Get_Field method
									x = FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields("exemptcode2")) - 60);
								}
								// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
								if (rsValuation.FindFirstRecord("ValueKey", rsLoad.Get_Fields("account")))
								{
									// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
									lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(rsValuation.Get_Fields("category" + FCConvert.ToString(x)))));
									// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
									if (Conversion.Val(rsExempts.Get_Fields("amount")) == 0)
									{
										rsLoad.Set_Fields("exemption", Conversion.Val(rsLoad.Get_Fields_Int32("exemption")) + lngTemp);
										lngExempt2 = lngTemp;
									}
									else
									{
										// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
										lngExemptAmount = FCConvert.ToInt32(Conversion.Val(rsExempts.Get_Fields("amount")) * dblRatio);
										if (lngExemptAmount > lngTemp)
										{
											rsLoad.Set_Fields("exemption", rsLoad.Get_Fields_Int32("exemption") + lngTemp);
											lngExempt2 = lngTemp;
										}
										else
										{
											rsLoad.Set_Fields("exemption", rsLoad.Get_Fields_Int32("exemption") + lngExemptAmount);
											lngExempt2 = lngExemptAmount;
										}
									}
								}
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
								lngExemptAmount = FCConvert.ToInt32(Conversion.Val(rsExempts.Get_Fields("amount")) * dblRatio);
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								if (FCConvert.ToInt32(rsExempts.Get_Fields("Amount")) == 0)
								{
									// TODO Get_Fields: Check the table for the column [Value] and replace with corresponding Get_Field method
									rsLoad.Set_Fields("Exemption", rsLoad.Get_Fields("Value"));
									// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
									lngExempt2 = FCConvert.ToInt32(rsLoad.Get_Fields("value"));
								}
								else
								{
									rsLoad.Set_Fields("Exemption", rsLoad.Get_Fields_Int32("Exemption") + lngExemptAmount);
									lngExempt2 = lngExemptAmount;
								}
							}
						}
					}
					// .Fields("exemption") = .Fields("exemption") + lngBETEExempt
					// TODO Get_Fields: Check the table for the column [Value] and replace with corresponding Get_Field method
					if (rsLoad.Get_Fields_Int32("Exemption") > Conversion.Val(rsLoad.Get_Fields("Value")))
					{
						// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
						rsLoad.Set_Fields("Exemption", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("value"))));
					}
					rsLoad.Set_Fields("exemption1", lngExempt1);
					rsLoad.Set_Fields("exemption2", lngExempt2);
					rsLoad.Update();
					rsLoad.MoveNext();
				}
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Exemptions Calculated", "Done", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ComputeExemptions");
			}
		}

		public static void Create_User_Extract()
		{
			const int curOnErrorGoToLabel_Default = 0;
			const int curOnErrorGoToLabel_ErrorHandler = 1;
			const int curOnErrorGoToLabel_ErrorTag1 = 2;
			int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
			try
			{
				clsDRWrapper rs = new clsDRWrapper();
				clsDRWrapper rsV = new clsDRWrapper();
				clsDRWrapper clsTemp = new clsDRWrapper();
				string strLine = "";
				double[] dblBETE = new double[9 + 1];
				bool boolIncBETE;
				vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler;
				/* On Error GoTo ErrorHandler */
				boolIncBETE = modGlobal.Statics.CustomizeStuff.UserExtractIncBETE;
				if (!boolIncBETE)
				{
					strLine = Strings.StrDup(380, " ");
				}
				else
				{
					strLine = Strings.StrDup(462, " ");
				}
				Statics.boolAbortExtract = false;
				modGNWork.GetLocalVariables2();
				FCFileSystem.FileClose();
				// Set rs = pp.OpenRecordset("SELECT * FROM PPMaster WHERE not Deleted ORDER BY Account")
				// Set rs = pp.OpenRecordset("select ppmaster.account as account, * from ppmaster left join MODULEASSOCIATION on (moduleassociation.account = ppmaster.account) where  not ppmaster.deleted order by ppmaster.account")
				rs.OpenRecordset("select * from ppmaster where not deleted = 1 order by account", modPPGN.strPPDatabase);
				rsV.OpenRecordset("SELECT * FROM PPValuations ORDER BY ValueKey", modPPGN.strPPDatabase);
				//FC:FINAL:RPU: #i1524 - Show the form Modeless
				//frmExtractMessage.InstancePtr.Show(App.MainForm);
				frmExtractMessage.InstancePtr.Show(FCForm.FormShowEnum.Modeless);
				rs.MoveLast();
				frmExtractMessage.InstancePtr.pbrExtract.Maximum = rs.RecordCount() + 1;
				frmExtractMessage.InstancePtr.pbrExtract.Value = 0;
				rs.MoveFirst();
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					FCFileSystem.FileOpen(5, "TSPPAS21.ASC", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
					while (!rs.EndOfFile())
					{
						////Application.DoEvents();
						if (Statics.boolAbortExtract)
						{
							//FC:FINAL:MSH - i.issue #1293: unload form instead of hiding (in original form will be unloaded)
							//frmExtractMessage.InstancePtr.Hide();
							frmExtractMessage.InstancePtr.Unload();
							MessageBox.Show("Extract process aborted.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsV.FindFirstRecord("ValueKey", rs.Get_Fields("Account"));
						if (rsV.NoMatch == false)
						{
							if (!boolIncBETE)
							{
								strLine = Strings.StrDup(380, " ");
							}
							else
							{
								strLine = Strings.StrDup(462, " ");
							}
							fecherFoundation.Strings.MidSet(ref strLine, 1, 1, "P");
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							fecherFoundation.Strings.MidSet(ref strLine, 2, 5, Strings.Format(rs.Get_Fields("Account"), "00000"));
							if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("name")))
							{
								fecherFoundation.Strings.MidSet(ref strLine, 7, 34, rs.Get_Fields_String("Name") + Strings.StrDup(34 - FCConvert.ToString(rs.Get_Fields_String("Name")).Length, " "));
							}
							else
							{
								fecherFoundation.Strings.MidSet(ref strLine, 7, 34, Strings.StrDup(34, " "));
							}
							if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("address1")))
							{
								fecherFoundation.Strings.MidSet(ref strLine, 41, 34, rs.Get_Fields_String("Address1") + Strings.StrDup(34 - FCConvert.ToString(rs.Get_Fields_String("Address1")).Length, " "));
							}
							else
							{
								fecherFoundation.Strings.MidSet(ref strLine, 41, 34, Strings.StrDup(34, " "));
							}
							if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("address2")))
							{
								fecherFoundation.Strings.MidSet(ref strLine, 75, 34, rs.Get_Fields_String("Address2") + Strings.StrDup(34 - FCConvert.ToString(rs.Get_Fields_String("Address2")).Length, " "));
							}
							else
							{
								fecherFoundation.Strings.MidSet(ref strLine, 75, 34, Strings.StrDup(34, " "));
							}
							if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("city")))
							{
								fecherFoundation.Strings.MidSet(ref strLine, 109, 24, rs.Get_Fields_String("City") + Strings.StrDup(24 - FCConvert.ToString(rs.Get_Fields_String("City")).Length, " "));
							}
							else
							{
								fecherFoundation.Strings.MidSet(ref strLine, 109, 24, Strings.StrDup(24, " "));
							}
							if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("state")))
							{
								fecherFoundation.Strings.MidSet(ref strLine, 133, 2, rs.Get_Fields_String("State") + Strings.StrDup(2 - FCConvert.ToString(rs.Get_Fields_String("State")).Length, " "));
							}
							else
							{
								fecherFoundation.Strings.MidSet(ref strLine, 133, 2, "  ");
							}
							if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("zip")))
							{
								fecherFoundation.Strings.MidSet(ref strLine, 135, 9, modGlobalFunctions.PadStringWithSpaces(rs.Get_Fields_String("Zip"), 5) + modGlobalFunctions.PadStringWithSpaces(rs.Get_Fields_String("Zip4"), 4, false));
							}
							else
							{
								fecherFoundation.Strings.MidSet(ref strLine, 135, 9, Strings.StrDup(9, "0"));
							}
							// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
							if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields("streetnumber")))
							{
								// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
								fecherFoundation.Strings.MidSet(ref strLine, 144, 5, Strings.Format(rs.Get_Fields("StreetNumber"), "00000"));
							}
							else
							{
								fecherFoundation.Strings.MidSet(ref strLine, 144, 5, "00000");
							}
							if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("street")))
							{
								fecherFoundation.Strings.MidSet(ref strLine, 149, 26, rs.Get_Fields_String("Street") + Strings.StrDup(26 - FCConvert.ToString(rs.Get_Fields_String("Street")).Length, " "));
							}
							else
							{
								fecherFoundation.Strings.MidSet(ref strLine, 149, 26, Strings.StrDup(26, " "));
							}
							// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
							if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields("value")))
							{
								// TODO Get_Fields: Check the table for the column [Value] and replace with corresponding Get_Field method
								fecherFoundation.Strings.MidSet(ref strLine, 175, 9, Strings.Format(rs.Get_Fields("Value"), "000000000"));
							}
							else
							{
								fecherFoundation.Strings.MidSet(ref strLine, 175, 9, "000000000");
							}
							if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_Int32("exemption")))
							{
								fecherFoundation.Strings.MidSet(ref strLine, 184, 9, Strings.Format(rs.Get_Fields_Int32("Exemption"), "000000000"));
							}
							else
							{
								fecherFoundation.Strings.MidSet(ref strLine, 184, 9, Strings.StrDup(9, "0"));
							}
							// TODO Get_Fields: Check the table for the column [exemptcode1] and replace with corresponding Get_Field method
							if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields("exemptcode1")))
							{
								// TODO Get_Fields: Check the table for the column [ExemptCode1] and replace with corresponding Get_Field method
								fecherFoundation.Strings.MidSet(ref strLine, 193, 2, Strings.Format(rs.Get_Fields("ExemptCode1"), "00"));
							}
							else
							{
								fecherFoundation.Strings.MidSet(ref strLine, 193, 2, "00");
							}
							// TODO Get_Fields: Check the table for the column [exemptcode2] and replace with corresponding Get_Field method
							if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields("exemptcode2")))
							{
								// TODO Get_Fields: Check the table for the column [ExemptCode2] and replace with corresponding Get_Field method
								fecherFoundation.Strings.MidSet(ref strLine, 195, 2, Strings.Format(rs.Get_Fields("ExemptCode2"), "00"));
							}
							else
							{
								fecherFoundation.Strings.MidSet(ref strLine, 195, 2, "00");
							}
							// TODO Get_Fields: Check the table for the column [TranCode] and replace with corresponding Get_Field method
							fecherFoundation.Strings.MidSet(ref strLine, 197, 1, Strings.Format(Conversion.Val(rs.Get_Fields("TranCode")), "0"));
							fecherFoundation.Strings.MidSet(ref strLine, 198, 2, Strings.Format(Conversion.Val(rs.Get_Fields_Int32("BusinessCode")), "00"));
							// TODO Get_Fields: Check the table for the column [StreetCode] and replace with corresponding Get_Field method
							fecherFoundation.Strings.MidSet(ref strLine, 200, 4, Strings.Format(Conversion.Val(rs.Get_Fields("StreetCode")), "0000"));
							if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("open1")))
							{
								fecherFoundation.Strings.MidSet(ref strLine, 204, 34, rs.Get_Fields_String("Open1") + Strings.StrDup(34 - FCConvert.ToString(rs.Get_Fields_String("Open1")).Length, " "));
							}
							else
							{
								fecherFoundation.Strings.MidSet(ref strLine, 204, 34, Strings.StrDup(34, " "));
							}
							if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("open2")))
							{
								fecherFoundation.Strings.MidSet(ref strLine, 238, 34, rs.Get_Fields_String("Open2") + Strings.StrDup(34 - FCConvert.ToString(rs.Get_Fields_String("Open2")).Length, " "));
							}
							else
							{
								fecherFoundation.Strings.MidSet(ref strLine, 238, 34, Strings.StrDup(34, " "));
							}
							if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("ORCODE")))
							{
								if (FCConvert.ToString(rs.Get_Fields_String("ORCode")) == "Y")
								{
									// Or IsNull(!ORCode) Then
									// TODO Get_Fields: Check the table for the column [Value] and replace with corresponding Get_Field method
									fecherFoundation.Strings.MidSet(ref strLine, 272, 9, Strings.Format(Conversion.Val(rs.Get_Fields("Value") + ""), "000000000"));
									fecherFoundation.Strings.MidSet(ref strLine, 281, 9, "000000000");
									fecherFoundation.Strings.MidSet(ref strLine, 290, 9, "000000000");
									fecherFoundation.Strings.MidSet(ref strLine, 299, 9, "000000000");
									fecherFoundation.Strings.MidSet(ref strLine, 308, 9, "000000000");
									fecherFoundation.Strings.MidSet(ref strLine, 317, 9, "000000000");
									fecherFoundation.Strings.MidSet(ref strLine, 326, 9, "000000000");
									fecherFoundation.Strings.MidSet(ref strLine, 335, 9, "000000000");
									fecherFoundation.Strings.MidSet(ref strLine, 344, 9, "000000000");
								}
								else
								{
									// Mid$(strLine, 272, 9) = Format(Round(rsV!category1, WPP.GetData("WPPRound")), "000000000")
									// Mid$(strLine, 281, 9) = Format(Round(rsV!category2, WPP.GetData("WPPRound")), "000000000")
									// Mid$(strLine, 290, 9) = Format(Round(rsV!category3, WPP.GetData("WPPRound")), "000000000")
									// Mid$(strLine, 299, 9) = Format(Round(rsV!category4, WPP.GetData("WPPRound")), "000000000")
									// Mid$(strLine, 308, 9) = Format(Round(rsV!category5, WPP.GetData("WPPRound")), "000000000")
									// Mid$(strLine, 317, 9) = Format(Round(rsV!category6, WPP.GetData("WPPRound")), "000000000")
									// Mid$(strLine, 326, 9) = Format(Round(rsV!category7, WPP.GetData("WPPRound")), "000000000")
									// Mid$(strLine, 335, 9) = Format(Round(rsV!category8, WPP.GetData("WPPRound")), "000000000")
									// Mid$(strLine, 344, 9) = Format(Round(rsV!category9, WPP.GetData("WPPRound")), "000000000")
									fecherFoundation.Strings.MidSet(ref strLine, 272, 9, Strings.Format(modGlobal.Round(Conversion.Val(rsV.Get_Fields_Int32("category1")), modGlobal.Statics.CustomizeStuff.Round), "000000000"));
									fecherFoundation.Strings.MidSet(ref strLine, 281, 9, Strings.Format(modGlobal.Round(Conversion.Val(rsV.Get_Fields_Int32("category2")), modGlobal.Statics.CustomizeStuff.Round), "000000000"));
									fecherFoundation.Strings.MidSet(ref strLine, 290, 9, Strings.Format(modGlobal.Round(Conversion.Val(rsV.Get_Fields_Int32("category3")), modGlobal.Statics.CustomizeStuff.Round), "000000000"));
									fecherFoundation.Strings.MidSet(ref strLine, 299, 9, Strings.Format(modGlobal.Round(Conversion.Val(rsV.Get_Fields_Int32("category4")), modGlobal.Statics.CustomizeStuff.Round), "000000000"));
									fecherFoundation.Strings.MidSet(ref strLine, 308, 9, Strings.Format(modGlobal.Round(Conversion.Val(rsV.Get_Fields_Int32("category5")), modGlobal.Statics.CustomizeStuff.Round), "000000000"));
									fecherFoundation.Strings.MidSet(ref strLine, 317, 9, Strings.Format(modGlobal.Round(Conversion.Val(rsV.Get_Fields_Int32("category6")), modGlobal.Statics.CustomizeStuff.Round), "000000000"));
									fecherFoundation.Strings.MidSet(ref strLine, 326, 9, Strings.Format(modGlobal.Round(Conversion.Val(rsV.Get_Fields_Int32("category7")), modGlobal.Statics.CustomizeStuff.Round), "000000000"));
									fecherFoundation.Strings.MidSet(ref strLine, 335, 9, Strings.Format(modGlobal.Round(Conversion.Val(rsV.Get_Fields_Int32("category8")), modGlobal.Statics.CustomizeStuff.Round), "000000000"));
									fecherFoundation.Strings.MidSet(ref strLine, 344, 9, Strings.Format(modGlobal.Round(Conversion.Val(rsV.Get_Fields_Int32("Category9")), modGlobal.Statics.CustomizeStuff.Round), "000000000"));
								}
							}
							else
							{
								fecherFoundation.Strings.MidSet(ref strLine, 272, 9, Strings.Format(modGlobal.Round(Conversion.Val(rsV.Get_Fields_Int32("category1")), modGlobal.Statics.CustomizeStuff.Round), "000000000"));
								fecherFoundation.Strings.MidSet(ref strLine, 281, 9, Strings.Format(modGlobal.Round(Conversion.Val(rsV.Get_Fields_Int32("category2")), modGlobal.Statics.CustomizeStuff.Round), "000000000"));
								fecherFoundation.Strings.MidSet(ref strLine, 290, 9, Strings.Format(modGlobal.Round(Conversion.Val(rsV.Get_Fields_Int32("category3")), modGlobal.Statics.CustomizeStuff.Round), "000000000"));
								fecherFoundation.Strings.MidSet(ref strLine, 299, 9, Strings.Format(modGlobal.Round(Conversion.Val(rsV.Get_Fields_Int32("category4")), modGlobal.Statics.CustomizeStuff.Round), "000000000"));
								fecherFoundation.Strings.MidSet(ref strLine, 308, 9, Strings.Format(modGlobal.Round(Conversion.Val(rsV.Get_Fields_Int32("category5")), modGlobal.Statics.CustomizeStuff.Round), "000000000"));
								fecherFoundation.Strings.MidSet(ref strLine, 317, 9, Strings.Format(modGlobal.Round(Conversion.Val(rsV.Get_Fields_Int32("category6")), modGlobal.Statics.CustomizeStuff.Round), "000000000"));
								fecherFoundation.Strings.MidSet(ref strLine, 326, 9, Strings.Format(modGlobal.Round(Conversion.Val(rsV.Get_Fields_Int32("category7")), modGlobal.Statics.CustomizeStuff.Round), "000000000"));
								fecherFoundation.Strings.MidSet(ref strLine, 335, 9, Strings.Format(modGlobal.Round(Conversion.Val(rsV.Get_Fields_Int32("category8")), modGlobal.Statics.CustomizeStuff.Round), "000000000"));
								fecherFoundation.Strings.MidSet(ref strLine, 344, 9, Strings.Format(modGlobal.Round(Conversion.Val(rsV.Get_Fields_Int32("Category9")), modGlobal.Statics.CustomizeStuff.Round), "000000000"));
							}
							if (!rs.EndOfFile())
							{
								// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
								clsTemp.OpenRecordset("select * from moduleassociation where module = 'PP' and primaryassociate = 1 and account = " + rs.Get_Fields("account"), "CentralData");
								if (!clsTemp.EndOfFile())
								{
									fecherFoundation.Strings.MidSet(ref strLine, 353, 6, Strings.Format(Conversion.Val(clsTemp.Get_Fields_Int32("remasteracct")), "000000"));
								}
								else
								{
									fecherFoundation.Strings.MidSet(ref strLine, 353, 6, "000000");
								}
								// If .Fields("MODULE") & "" = "PP" And .Fields("primaryassociate") Then
								// Mid(strLine, 353, 6) = Format(Val(.Fields("remasteracct") & ""), "000000")
								// Else
								// Mid(strLine, 353, 6) = "000000"
								// End If
							}
							else
							{
								fecherFoundation.Strings.MidSet(ref strLine, 353, 6, "000000");
							}
							fecherFoundation.Strings.MidSet(ref strLine, 359, 22, Strings.StrDup(22, " "));
							if (boolIncBETE)
							{
								fecherFoundation.Strings.MidSet(ref strLine, 381, 9, Strings.Format(modGlobal.Round(Conversion.Val(rsV.Get_Fields_Int32("CAT1exempt") + ""), modGlobal.Statics.CustomizeStuff.Round), "000000000"));
								fecherFoundation.Strings.MidSet(ref strLine, 390, 9, Strings.Format(modGlobal.Round(Conversion.Val(rsV.Get_Fields_Int32("cat2exempt") + ""), modGlobal.Statics.CustomizeStuff.Round), "000000000"));
								fecherFoundation.Strings.MidSet(ref strLine, 399, 9, Strings.Format(modGlobal.Round(Conversion.Val(rsV.Get_Fields_Int32("Cat3exempt") + ""), modGlobal.Statics.CustomizeStuff.Round), "000000000"));
								fecherFoundation.Strings.MidSet(ref strLine, 408, 9, Strings.Format(modGlobal.Round(Conversion.Val(rsV.Get_Fields_Int32("cat4exempt") + ""), modGlobal.Statics.CustomizeStuff.Round), "000000000"));
								fecherFoundation.Strings.MidSet(ref strLine, 417, 9, Strings.Format(modGlobal.Round(Conversion.Val(rsV.Get_Fields_Int32("cat5exempt") + ""), modGlobal.Statics.CustomizeStuff.Round), "000000000"));
								fecherFoundation.Strings.MidSet(ref strLine, 426, 9, Strings.Format(modGlobal.Round(Conversion.Val(rsV.Get_Fields_Int32("cat6exempt") + ""), modGlobal.Statics.CustomizeStuff.Round), "000000000"));
								fecherFoundation.Strings.MidSet(ref strLine, 435, 9, Strings.Format(modGlobal.Round(Conversion.Val(rsV.Get_Fields_Int32("cat7exempt") + ""), modGlobal.Statics.CustomizeStuff.Round), "000000000"));
								fecherFoundation.Strings.MidSet(ref strLine, 444, 9, Strings.Format(modGlobal.Round(Conversion.Val(rsV.Get_Fields_Int32("cat8exempt") + ""), modGlobal.Statics.CustomizeStuff.Round), "000000000"));
								fecherFoundation.Strings.MidSet(ref strLine, 453, 9, Strings.Format(modGlobal.Round(Conversion.Val(rsV.Get_Fields_Int32("cat9exempt") + ""), modGlobal.Statics.CustomizeStuff.Round), "000000000"));
							}
							FCFileSystem.PrintLine(5, strLine);
						}
						rs.MoveNext();
						frmExtractMessage.InstancePtr.pbrExtract.Value = frmExtractMessage.InstancePtr.pbrExtract.Value + 1;
						// frmExtractMessage.Refresh
						//FC:FINAL:RPU: #i1524 - Refresh the progressbar
						FCUtils.ApplicationUpdate(frmExtractMessage.InstancePtr.pbrExtract);
					}
					FCFileSystem.FileClose(5);
				}
				//FC:FINAL:MSH - i.issue #1293: unload form instead of hiding (in original form will be unloaded)
				//frmExtractMessage.InstancePtr.Hide();
				frmExtractMessage.InstancePtr.Unload();
				// vbPorter upgrade warning: Ans As Variant --> As DialogResult
				DialogResult Ans;
				int intCounter = 0;
				string strRec = "";
				////Application.DoEvents();
				//FC:FINAL:DDU:#i1293 - download file on client PC not on Drive A(diskette)
				Ans = MessageBox.Show("Would you like to copy extract to PC?", "Copy to PC", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorTag1;
				/* On Error GoTo ErrorTag1 */
				if (Ans == DialogResult.Yes)
				{
					FCUtils.Download(FCFileSystem.Statics.UserDataFolder + "\\TSPPAS21.ASC", "TSPPAS21.ASC");
					//FCFileSystem.FileClose();
					//MessageBox.Show("Put the first diskette into Drive A.", " ", MessageBoxButtons.OK, MessageBoxIcon.Information);
					//frmMessage.InstancePtr.Label1.Text = "Copying To Diskette";
					//FCFileSystem.FileOpen(2, "TSPPAS21.ASC", OpenMode.Input, (OpenAccess)(-1), (OpenShare)(-1), -1);
					//FCFileSystem.FileOpen(1, "A:\\TSPPAS21.ASC", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
					//frmMessage.InstancePtr.Show(App.MainForm);
					//frmMessage.InstancePtr.Refresh();
					//NextDiskTag:;
					//if (intCounter >= 3000)
					//{
					//    frmMessage.InstancePtr.Hide();
					//    intCounter = 0;
					//    FCFileSystem.FileClose(1);
					//    MessageBox.Show("Be sure the light on Drive A is OFF, then insert disk and press Enter.", " ", MessageBoxButtons.OK, MessageBoxIcon.Information);
					//    frmMessage.InstancePtr.Label1.Text = "Copying To Diskette";
					//    FCFileSystem.FileOpen(1, "A:\\TSPPAS21.ASC", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
					//    frmMessage.InstancePtr.Show(App.MainForm);
					//    frmMessage.InstancePtr.Refresh();
					//}
					//while (!(FCFileSystem.EOF(2) || intCounter == 3000))
					//{
					//    intCounter += 1;
					//    strRec = FCFileSystem.LineInput(2);
					//    FCFileSystem.PrintLine(1, strRec);
					//}
					//if (intCounter >= 3000 && !FCFileSystem.EOF(2)) goto NextDiskTag;
				}
				//FC:FINAL:MSH - i.issue #1293: unload form instead of hiding (in original form will be unloaded)
				//frmMessage.InstancePtr.Hide();
				frmMessage.InstancePtr.Unload();
				FCFileSystem.FileClose();
				return;
				ErrorTag1:
				;
				// vbPorter upgrade warning: errAns As Variant --> As DialogResult
				DialogResult errAns;
				errAns = MessageBox.Show(Information.Err().Description, "Error", MessageBoxButtons.RetryCancel, MessageBoxIcon.Hand);
				if (errAns == DialogResult.Retry)
				{
					/*? Resume; */
				}
				else
				{
					Information.Err().Clear();
					//FC:FINAL:MSH - i.issue #1293: unload form instead of hiding (in original form will be unloaded)
					//frmMessage.InstancePtr.Hide();
					frmMessage.InstancePtr.Unload();
				}
				return;
				ErrorHandler:
				;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err().Number) + "  " + Information.Err().Description + "\r\n" + "In Create User Extract", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			catch (Exception ex)
			{
				switch (vOnErrorGoToLabel)
				{
					default:
					case curOnErrorGoToLabel_Default:
						// ...
						break;
					case curOnErrorGoToLabel_ErrorHandler:
						//? goto ErrorHandler;
						break;
					case curOnErrorGoToLabel_ErrorTag1:
						//? goto ErrorTag1;
						break;
				}
			}
		}

		public static void Check_RB_Fields()
		{
			clsDRWrapper clsTemp = new clsDRWrapper();
			bool boolRB;
			boolRB = false;
			clsTemp.OpenRecordset("select top 1 account from ppleased where rb = '*' and account = " + FCConvert.ToString(Statics.CurrentAccount), "twpp0000.vb1");
			if (!clsTemp.EndOfFile())
			{
				boolRB = true;
			}
			clsTemp.OpenRecordset("select top 1 account from ppitemized where rb = '*' and account = " + FCConvert.ToString(Statics.CurrentAccount), "twpp0000.vb1");
			if (!clsTemp.EndOfFile())
			{
				boolRB = true;
			}
			if (boolRB)
			{
				clsTemp.Execute("update ppmaster set rbcode = 'Y' where account = " + FCConvert.ToString(Statics.CurrentAccount), "twpp0000.vb1");
			}
			else
			{
				clsTemp.Execute("update ppmaster set rbcode = 'N' where account = " + FCConvert.ToString(Statics.CurrentAccount), "twpp0000.vb1");
			}
		}

		public static bool CheckDatabaseStructure()
		{
			bool CheckDatabaseStructure = false;
			CheckDatabaseStructure = false;
			Statics.boolMessageAlreadyShown = false;
			frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Updating Database Structure", true);
			frmWait.InstancePtr.prgProgress.Value = 10;
			CleanupValuationTable();
			// 10/20/2005
			CheckTranTownTable();
			// 11/15/2005
			CleanupAccounts();
			frmWait.InstancePtr.prgProgress.Value = 100;
			frmWait.InstancePtr.Unload();
			////Application.DoEvents();
			if (Statics.boolMessageAlreadyShown)
			{
				MessageBox.Show("Update complete.  It is safe to allow others into Personal Property now.", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			CheckDatabaseStructure = true;
			return CheckDatabaseStructure;
		}

		private static void CleanupAccounts()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper clsExecute = new clsDRWrapper();
				clsExecute.Execute("delete from ppmaster where account = 0", modPPGN.strPPDatabase);
				clsExecute.Execute("delete from ppitemized where account = 0", modPPGN.strPPDatabase);
				clsExecute.Execute("delete from ppleased where account = 0", modPPGN.strPPDatabase);
				clsExecute.Execute("delete from ppvaluations where valuekey = 0", modPPGN.strPPDatabase);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In CleanupAccounts", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private static void CleanupValuationTable()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper clsExecute = new clsDRWrapper();
				clsDRWrapper clsLoad = new clsDRWrapper();
				clsExecute.Execute("delete from ppvaluations where VALUEKEY not in (select account from ppmaster)", "twpp0000.vb1");
				clsExecute.OpenRecordset("select valuekey, count(valuekey) as numkeys from ppvaluations  group by valuekey having count(valuekey) > 1", "twpp0000.vb1");
				while (!clsExecute.EndOfFile())
				{
					clsLoad.OpenRecordset("select * from ppvaluations where valuekey = " + clsExecute.Get_Fields_Int32("valuekey"), "twpp0000.vb1");
					clsLoad.MoveNext();
					while (!clsLoad.EndOfFile())
					{
						clsLoad.Delete();
						clsLoad.MoveNext();
					}
					clsExecute.MoveNext();
				}
				if (Statics.boolShortScreenOnly)
				{
					clsExecute.Execute("update (select * from ppvaluations inner join ppmaster on (ppmaster.account = ppvaluations.valuekey)) set [value] = category1 + category2 + category3 + category4 + category5 + category6 + category7 + category8 + category9", modPPGN.strPPDatabase, false);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In CleanupValuationTable", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private static void CheckAssociations()
		{
			clsDRWrapper clsTemp = new clsDRWrapper();
			clsDRWrapper clsSave = new clsDRWrapper();
			//FileSystemObject fso = new FileSystemObject();
			int lngREAccount = 0;
			int lngPPAccount = 0;
			int lngID = 0;
			int lngLastRE;
			if (!File.Exists(FCFileSystem.CurDir() + "\\twre0000.vb1"))
			{
				// no real estate to associate with anyway
				return;
			}
			clsTemp.OpenRecordset("Select * from moduleassociation where module = 'PP'", "CentralData");
			if (!clsTemp.EndOfFile())
			{
				// already have some set up
				return;
			}
			lngLastRE = 0;
			// no associations so we may have to make them
			clsTemp.OpenRecordset("select * from ppmaster where realassoc  > 0 order by realassoc", "twpp0000.vb1");
			while (!clsTemp.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				lngPPAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields("account"))));
				lngREAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int32("reALassoc"))));
				if (lngREAccount != lngLastRE)
				{
					clsSave.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(lngREAccount) + " and rscard = 1 and not rsdeleted = 1", "twre0000.vb1");
					if (!clsSave.EndOfFile())
					{
						if (!FCConvert.ToBoolean(clsSave.Get_Fields_Boolean("rsdeleted")))
						{
							// create a new group
							clsSave.OpenRecordset("select * from groupmaster where id = " + FCConvert.ToString(modGlobalConstants.EleventyBillion), "CentralData");
							clsSave.AddNew();
							clsSave.Set_Fields("datecreated", DateTime.Today);
							clsSave.Update();
							lngID = FCConvert.ToInt32(clsSave.Get_Fields_Int32("id"));
							clsSave.Execute("update groupmaster set groupnumber = id where id = " + FCConvert.ToString(lngID), "CentralData");
							// first the re account
							clsSave.Execute("insert into moduleassociation (groupnumber,module,account,groupprimary,primaryassociate,remasteracct) values (" + FCConvert.ToString(lngID) + ",'RE'," + FCConvert.ToString(lngREAccount) + ",1,0,0)", "CentralData");
							// then the personal property account
							clsSave.Execute("insert into moduleassociation (groupnumber,module,account,groupprimary,primaryassociate,remasteracct) values (" + FCConvert.ToString(lngID) + ",'PP'," + FCConvert.ToString(lngPPAccount) + ",0,1," + FCConvert.ToString(lngREAccount) + ")", "CentralData");
						}
					}
				}
				clsTemp.Edit();
				clsTemp.Set_Fields("reaLAssoc", 0);
				clsTemp.Update();
				clsTemp.MoveNext();
			}
		}

		public static string FixPhoneNumber(ref string strNumber)
		{
			string FixPhoneNumber = "";
			int intPlace;
			string strPhoneNumber;
			string strTemp;
			// formats any string to a valid phone number mask
			strPhoneNumber = "(000)000-0000";
			strTemp = strPhoneNumber;
			intPlace = 1;
			FixPhoneNumber = "";
			if (strNumber.Length < 13)
			{
				fecherFoundation.Strings.MidSet(ref strTemp, 13 - Strings.Len(strNumber) + 1, strNumber);
			}
			else if (strNumber.Length == 13)
			{
				strTemp = strNumber;
			}
			for (intPlace = 1; intPlace <= strTemp.Length; intPlace++)
			{
				if (!Information.IsNumeric(Strings.Mid(strTemp, intPlace, 1)))
				{
					fecherFoundation.Strings.MidSet(ref strTemp, intPlace, 1, "0");
				}
			}
			// intPlace
			fecherFoundation.Strings.MidSet(ref strTemp, 1, 1, "(");
			fecherFoundation.Strings.MidSet(ref strTemp, 5, 1, ")");
			fecherFoundation.Strings.MidSet(ref strTemp, 9, 1, "-");
			FixPhoneNumber = strTemp;
			return FixPhoneNumber;
		}

		public static void checkGroup_6(int lngAccount, bool boolSelling)
		{
			checkGroup(ref lngAccount, ref boolSelling);
		}

		public static void checkGroup_8(int lngAccount, bool boolSelling)
		{
			checkGroup(ref lngAccount, ref boolSelling);
		}

		public static void checkGroup(ref int lngAccount, ref bool boolSelling)
		{
			// when an account is deleted or sold in RE and the account is in a group you must change the group
			// check to see if it's in a group
			// if it is then see what needs to be done
			clsDRWrapper clsTemp = new clsDRWrapper();
			clsDRWrapper clsGroup = new clsDRWrapper();
			int lngGroupNum;
			// first see if its in a group
			clsTemp.OpenRecordset("select * from MODULEassociation where account = " + FCConvert.ToString(lngAccount) + " and module = 'PP'", "CentralData");
			if (clsTemp.EndOfFile())
			{
				// not in a group so leave
				return;
			}
			int LngNumAccts;
			// it is in a group so lets see what we need to do about it
			lngGroupNum = FCConvert.ToInt32(clsTemp.Get_Fields_Int32("groupnumber"));
			clsGroup.OpenRecordset("select * from moduleassociation where groupnumber = " + clsTemp.Get_Fields_Int32("groupnumber"), "CentralData");
			clsGroup.MoveLast();
			clsGroup.MoveFirst();
			LngNumAccts = clsGroup.RecordCount();
			if (!boolSelling)
			{
				// just deleting the account
				if (LngNumAccts < 2)
				{
					// its the only one in the group so just get rid of the group
					clsGroup.Execute("delete * from moduleassociation where groupnumber = " + FCConvert.ToString(lngGroupNum), "CentralData");
					clsGroup.Execute("delete * from groupmaster where id = " + FCConvert.ToString(lngGroupNum), "CentralData");
					return;
				}
				// it's not the only one in the group.
				// take it out, give a warning and send them to the group screen to fix things
				clsGroup.Execute("delete from moduleassociation where account = " + FCConvert.ToString(lngAccount) + " and module = 'PP'", "CentralData");
				// now take out links to it
				// Call clsGroup.Execute("update moduleassociation  set primaryassociate = 0,REMASTERACCT = 0" & " where remasteracct = " & gintLastAccountNumber, "twre0000.vb1")
				// not give a message
				MessageBox.Show("This account was in a group and has been removed." + "\r\n" + "If it was the group primary a new one has been randomly assigned." + "\r\n" + "Please check that the group information is still correct.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				frmGroup.InstancePtr.Init(lngGroupNum);
				return;
			}
		}

		public static void ModSpecificReset()
		{
			CheckVersion();
			modGNWork.GetLocalVariables2();
			modGlobalFunctions.UpdateUsedModules();
			if (modPPCalculate.Statics.PPRounding == 1)
				modPPCalculate.Statics.PPRounding = -3;
			if (modPPCalculate.Statics.PPRounding == 2)
				modPPCalculate.Statics.PPRounding = -2;
			if (modPPCalculate.Statics.PPRounding == 3)
				modPPCalculate.Statics.PPRounding = -1;
			if (modPPCalculate.Statics.PPRounding == 4)
				modPPCalculate.Statics.PPRounding = 0;
			GetOpenFields();
			Statics.CurrentAccount = 1;
			//FC:FINAL:MSH - issue #1518: reload menu for disabling "Archive" option after running archive
			App.MainForm.NavigationMenu.Clear();
			MDIParent.InstancePtr.MainCaptions();
			//MDIParent.InstancePtr.FileCaptions();
		}

		public static object GetReimItemizedForCat(ref int lngOrigCat, ref int lngCat, int lngAcct, int intCat)
		{
			object GetReimItemizedForCat = null;
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsLoad.OpenRecordset("select sum(cost * quantity) as thecost,sum(value) as theValue from ppitemized where account = " + FCConvert.ToString(lngAcct) + " and rb = '*' and cd = '" + FCConvert.ToString(intCat) + "'", "twpp0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				// TODO Get_Fields: Field [thecost] not found!! (maybe it is an alias?)
				lngOrigCat = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("thecost"))));
				// TODO Get_Fields: Field [thevalue] not found!! (maybe it is an alias?)
				lngCat = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("thevalue"))));
			}
			else
			{
				lngOrigCat = 0;
				lngCat = 0;
			}
			return GetReimItemizedForCat;
		}

		public static string[] SplitQuotedText(string strInput, string strDelim = ",")
		{
			string[] SplitQuotedText = null;
			string strNewInput = "";
			bool boolInQuote;
			int intIndex;
			string strChar = "";
			try
			{
				// On Error GoTo ErrorHandler
				intIndex = 1;
				boolInQuote = false;
				while (intIndex <= strInput.Length)
				{
					strChar = Strings.Mid(strInput, intIndex, 1);
					if (strChar == FCConvert.ToString(Convert.ToChar(34)))
					{
						boolInQuote = !boolInQuote;
					}
					else
					{
						if (boolInQuote)
						{
							strNewInput += strChar;
						}
						else
						{
							if (strChar == strDelim)
							{
								strNewInput += "|";
							}
							else
							{
								strNewInput += strChar;
							}
						}
					}
					intIndex += 1;
				}
				SplitQuotedText = Strings.Split(strNewInput, "|", -1, CompareConstants.vbTextCompare);
				return SplitQuotedText;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SplitQuotedText", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SplitQuotedText;
		}
		// ErrorHandler:
		public class StaticVariables
		{
            //=========================================================
            public bool moduleInitialized = false;
            public string strDbCP = string.Empty;
			public string gstrArchiveParentDir = "";
			public bool gboolDefaultPaymentsToAuto;
			public bool boolRE;
			public double gdblReminderFormAdjustH;
			public double gdblReminderFormAdjustV;
			public bool gboolPrintF2Label;
			public bool boolInArchiveMode;
			public bool boolFromBilling;
			public bool CancelledIt;
			public int gintMinAccountrange;
			public int gintMaxAccountrange;
			public string gstrMinAccountRange = string.Empty;
			public string gstrMaxAccountRange = string.Empty;
			public bool boolShortScreenOnly;
			public bool boolMessageAlreadyShown;
			public bool boolAbortExtract;
			public bool boolDoneCalc;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1)]
			public string strShortScreen = string.Empty;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1)]
			public string strLongScreen = string.Empty;
			// vbPorter upgrade warning: strCostFiles As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1)]
			public string strCostFiles = string.Empty;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1)]
			public string strXFerToBilling = string.Empty;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1)]
			public string strPrintSummary = string.Empty;
			/// </summary>
			/// **** End of Security
			/// <summary>
			public bool Adding;
			/// </summary>
			/// Public PrintDisplayRange    As Boolean
			/// <summary>
			public bool PPBillingAssessing;
			public bool Networked;
			public bool LeasedTable;
			public bool ItemizedTable;
			public string DatabaseName = string.Empty;
			public int PrintMenuSelection;
			public string Sequence = "";
			public string SequenceTitle = "";
			public string Open1Title = "";
			public string Open2Title = "";
			public float PPRatio;
			public bool CalcTag;
			/// </summary>
			/// Public MuniName             As String
			/// <summary>
			public bool UnloadFormPPMaster;
			public bool Exiting;
			public int CurrentAccount;
			public bool AllFormsUnloaded;
			// vbPorter upgrade warning: rbTaxYear As short --> As int	OnWrite(string)
			/// </summary>
			/// Following Variables only used for PP Reimbursement
			/// <summary>
			public int rbTaxYear;
			// vbPorter upgrade warning: rbDueDate1 As DateTime	OnWrite(string)
			public DateTime rbDueDate1;
			// vbPorter upgrade warning: rbDueDate2 As DateTime	OnWrite(string, short)
			public DateTime rbDueDate2;
			public float rbTaxRate;
			public string rbSigner = string.Empty;
			public string rbSignerTitle = string.Empty;
			public string rbMessage = string.Empty;
			/// </summary>
			/// End of Reimbursement Variables
			/// <summary>
			public bool Searching;
			public string Msg1 = string.Empty;
			public string Msg2 = string.Empty;
			public string Msg3 = string.Empty;
			public string Msg4 = string.Empty;
			public string Msg5 = string.Empty;
			public string Msg6 = string.Empty;
			public string Msg7 = string.Empty;
			public string Msg8 = string.Empty;
			public string Msg9 = string.Empty;
			//FC:FINAL:AM:  moved variable from Gnbas
			public double dblOverPayRate;
			/// <summary>
			/// Public strSystemSettings As String
			/// </summary>
			public string strSystemSettings = string.Empty;
			public string strCentralData = string.Empty;
			public string strUTDatabase = string.Empty;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
// vbPorter upgrade warning: 'Return' As Variant --> As bool
/// <summary>
/// Public Function CheckVersion(Optional boolCheckDBStructure As Boolean = False) As Boolean
/// </summary>
/// <summary>
/// checks if this is a different version than what has been run before.
/// </summary>
/// <summary>
/// Dim clsTemp As New clsDRWrapper
/// </summary>
/// <summary>
/// Dim boolReturn As Boolean
/// </summary>
/// <summary>
///
/// </summary>
/// <summary>
///
/// </summary>
/// <summary>
/// CheckVersion = True
/// </summary>
/// <summary>
///
/// </summary>
/// <summary>
///
/// </summary>
/// <summary>
///
/// </summary>
/// <summary>
/// CheckFields:
/// </summary>
/// <summary>
/// Screen.MousePointer = vbHourglass
/// </summary>
/// <summary>
/// boolReturn = True
/// </summary>
/// <summary>
/// If boolCheckDBStructure Then
/// </summary>
/// <summary>
///
/// </summary>
/// <summary>
/// End If
/// </summary>
/// <summary>
///
/// </summary>
/// <summary>
///
/// </summary>
/// <summary>
/// Dim curVer As cVersionInfo
/// </summary>
/// <summary>
/// Dim boolFail As Boolean
/// </summary>
/// <summary>
/// boolFail = False
/// </summary>
/// <summary>
///
/// </summary>
/// <summary>
/// Dim tVer As cVersionInfo
/// </summary>
/// <summary>
/// Dim tVerCont As New cVersionController
/// </summary>
/// <summary>
/// Set tVer = tVerCont.GetVersion("DBVersion", "PersonalProperty")
/// </summary>
/// <summary>
/// If tVer Is Nothing Then
/// </summary>
/// <summary>
/// Set tVer = tVerCont.MakeVersion(0)
/// </summary>
/// <summary>
/// End If
/// </summary>
/// <summary>
/// Set curVer = tVerCont.MakeVersion(3)
/// </summary>
/// <summary>
/// If tVer.IsOlder(curVer) And Not boolFail Then
/// </summary>
/// <summary>
/// If MakeIndexes Then
/// </summary>
/// <summary>
/// Call tVer.Copy(curVer)
/// </summary>
/// <summary>
/// Call tVerCont.SaveVersion(tVer, "DBVersion", "PersonalProperty")
/// </summary>
/// <summary>
/// Else
/// </summary>
/// <summary>
/// boolFail = True
/// </summary>
/// <summary>
/// End If
/// </summary>
/// <summary>
/// End If
/// </summary>
/// <summary>
/// boolReturn = Not boolFail
/// </summary>
/// <summary>
/// CheckVersion = boolReturn
/// </summary>
/// <summary>
/// Screen.MousePointer = vbDefault
/// </summary>
/// <summary>
/// End Function
/// </summary>
/// <summary>
/// Public Function GetMasterJoin(Optional ByVal boolNameOnly As Boolean = False) As String
/// </summary>
/// <summary>
/// Dim strReturn As String
/// </summary>
/// <summary>
/// Dim rsTemp As New clsDRWrapper
/// </summary>
/// <summary>
/// Dim strFullDBName As String
/// </summary>
/// <summary>
/// strFullDBName = rsTemp.GetFullDBName("CentralParties")
/// </summary>
/// <summary>
/// If Not boolNameOnly Then
/// </summary>
/// <summary>
/// strReturn = "SELECT *, FullNameLF as Name, PartyState as State, '' as Zip4 FROM ppMaster CROSS APPLY " & strFullDBName & ".dbo.GetCentralPartyNameAndAddress(PartyID,NULL,'PP',NULL) "
/// </summary>
/// <summary>
/// Else
/// </summary>
/// <summary>
/// strReturn = "SELECT *, FullNameLF as Name FROM ppMaster CROSS APPLY " & strFullDBName & ".dbo.GetCentralPartyName(PartyID) "
/// </summary>
/// <summary>
/// End If
/// </summary>
/// <summary>
/// GetMasterJoin = strReturn
/// </summary>
/// <summary>
/// End Function
/// </summary>
// select master.*, cpo.FullNameLastFirst as RSName,cpso.FullNameLastFirst as RSSecOwner,cpo.address1 as rsaddr1,cpo.address2 as rsaddr2,cpo.address3 as rsaddress3,cpo.city as rsaddr3, cpo.state as rsstate,cpo.zip as rszip, '' as rszip4 from TRIO_Test_Live_RealEstate.dbo.master left join TRIO_Test_Live_CentralParties.dbo.PartyAndAddressView as cpo on (master.ownerpartyid = cpo.PartyID) left join TRIO_Test_Live_CentralParties.dbo.PartyAndAddressView as cpso on (master.SecOwnerPartyID = cpso.PartyID)
// On Error GoTo ErrorHandler
// must force tran code table to be the same as town codes
// ErrorHandler:
// OpenDatabase(DatabaseName, False, False, ";PWD=" & DATABASEPASSWORD)
// Put code here to get rounding option from 9,H,2
// Unload menuPPMain
// vbPorter upgrade warning: lngTemp As int	OnWrite(short, double)
// On Error GoTo ErrorHandler
// Load frmMessage
// frmMessage.Label1.Caption = "Transferring New Data"
// frmMessage.Show
// frmMessage.Refresh
////Application.DoEvents();
// Determine Exemption If Any are Applicable
// Unload frmMessage
////Application.DoEvents();
// ErrorHandler:
// On Error GoTo ErrorHandler
// vbPorter upgrade warning: x As short --> As int	OnWrite(short, double)
// vbPorter upgrade warning: lngExemptAmount As int	OnWriteFCConvert.ToDouble(
// vbPorter upgrade warning: lngBETEExempt As int	OnWrite(short, double)
// .Fields("exemption") = .Fields("exemption") + lngBETEExempt
// ErrorHandler:
/* On Error GoTo ErrorHandler */
// Set rs = pp.OpenRecordset("SELECT * FROM PPMaster WHERE not Deleted ORDER BY Account")
// Set rs = pp.OpenRecordset("select ppmaster.account as account, * from ppmaster left join MODULEASSOCIATION on (moduleassociation.account = ppmaster.account) where  not ppmaster.deleted order by ppmaster.account")
////Application.DoEvents();
// Or IsNull(!ORCode) Then
// Mid$(strLine, 272, 9) = Format(Round(rsV!category1, WPP.GetData("WPPRound")), "000000000")
// Mid$(strLine, 281, 9) = Format(Round(rsV!category2, WPP.GetData("WPPRound")), "000000000")
// Mid$(strLine, 290, 9) = Format(Round(rsV!category3, WPP.GetData("WPPRound")), "000000000")
// Mid$(strLine, 299, 9) = Format(Round(rsV!category4, WPP.GetData("WPPRound")), "000000000")
// Mid$(strLine, 308, 9) = Format(Round(rsV!category5, WPP.GetData("WPPRound")), "000000000")
// Mid$(strLine, 317, 9) = Format(Round(rsV!category6, WPP.GetData("WPPRound")), "000000000")
// Mid$(strLine, 326, 9) = Format(Round(rsV!category7, WPP.GetData("WPPRound")), "000000000")
// Mid$(strLine, 335, 9) = Format(Round(rsV!category8, WPP.GetData("WPPRound")), "000000000")
// Mid$(strLine, 344, 9) = Format(Round(rsV!category9, WPP.GetData("WPPRound")), "000000000")
// If .Fields("MODULE") & "" = "PP" And .Fields("primaryassociate") Then
// Mid(strLine, 353, 6) = Format(Val(.Fields("remasteracct") & ""), "000000")
// Else
// Mid(strLine, 353, 6) = "000000"
// End If
// frmExtractMessage.Refresh
// vbPorter upgrade warning: Ans As Variant --> As DialogResult
////Application.DoEvents();
/* On Error GoTo ErrorTag1 */
// vbPorter upgrade warning: errAns As Variant --> As DialogResult
/*? Resume; */
// ...
//? goto ErrorHandler;
//? goto ErrorTag1;
// 10/20/2005
// 11/15/2005
////Application.DoEvents();
// On Error GoTo ErrorHandler
// ErrorHandler:
// On Error GoTo ErrorHandler
// ErrorHandler:
// no real estate to associate with anyway
// already have some set up
// no associations so we may have to make them
// create a new group
// first the re account
// then the personal property account
// formats any string to a valid phone number mask
// intPlace
// when an account is deleted or sold in RE and the account is in a group you must change the group
// check to see if it's in a group
// if it is then see what needs to be done
// first see if its in a group
// not in a group so leave
// it is in a group so lets see what we need to do about it
// just deleting the account
// its the only one in the group so just get rid of the group
// it's not the only one in the group.
// take it out, give a warning and send them to the group screen to fix things
// now take out links to it
// Call clsGroup.Execute("update moduleassociation  set primaryassociate = 0,REMASTERACCT = 0" & " where remasteracct = " & gintLastAccountNumber, "twre0000.vb1")
// not give a message
/// <summary>
///
/// </summary>
/// <summary>
///
/// </summary>
/// <summary>
/// Private Function FillItemizedNewValFields()
/// </summary>
/// <summary>
/// try to fill out the year first assessed fields
/// </summary>
/// <summary>
/// Dim clsSave As New clsdrwrapper
/// </summary>
/// <summary>
/// Dim strSQL As String
/// </summary>
/// <summary>
///
/// </summary>
/// <summary>
/// On Error GoTo ErrorHandler
/// </summary>
/// <summary>
/// Call clsSave.Execute("update ppitemized set amountreplacing = 0", modPPGN.strPPDatabase)
/// </summary>
/// <summary>
/// start out as old (not new this past year)
/// </summary>
/// <summary>
/// Call clsSave.Execute("update ppitemized set yearfirstassessed = 2000", modPPGN.strPPDatabase)
/// </summary>
/// <summary>
/// update any that have a valid year bought
/// </summary>
/// <summary>
/// strSQL = "update ppitemized set yearfirstassessed = [year] where val([year] & '') > 1980"
/// </summary>
/// <summary>
/// Call clsSave.Execute(strSQL, modPPGN.strPPDatabase)
/// </summary>
/// <summary>
/// UPDATE ANY that are just comments
/// </summary>
/// <summary>
/// strSQL = "update ppitemized set yearfirstassessed = 0 where CD = '-'"
/// </summary>
/// <summary>
/// Call clsSave.Execute(strSQL, modPPGN.strPPDatabase)
/// </summary>
/// <summary>
///
/// </summary>
/// <summary>
/// Exit Function
/// </summary>
/// <summary>
/// ErrorHandler:
/// </summary>
/// <summary>
/// MsgBox "Error Number " & Err.Number & "  " & Err.Description & vbNewLine & "In FillItemizedNewValFields", vbCritical, "Error"
/// </summary>
/// <summary>
/// End Function
/// </summary>
/// <summary>
///
/// </summary>
/// <summary>
/// Public Function FillLeasedNewValFields()
/// </summary>
/// <summary>
/// try to fill out the year first assessed fields
/// </summary>
/// <summary>
/// Dim clsSave As New clsdrwrapper
/// </summary>
/// <summary>
/// Dim strSQL As String
/// </summary>
/// <summary>
/// On Error GoTo ErrorHandler
/// </summary>
/// <summary>
///
/// </summary>
/// <summary>
/// strSQL = "update ppleased set amountreplacing = 0"
/// </summary>
/// <summary>
/// Call clsSave.Execute(strSQL, modPPGN.strPPDatabase)
/// </summary>
/// <summary>
///
/// </summary>
/// <summary>
/// start out as old (not new this past year)
/// </summary>
/// <summary>
/// strSQL = "update ppleased set yearfirstassessed = 2000"
/// </summary>
/// <summary>
/// Call clsSave.Execute(strSQL, modPPGN.strPPDatabase)
/// </summary>
/// <summary>
///
/// </summary>
/// <summary>
/// now try to update the ones that have a lease date
/// </summary>
/// <summary>
/// strSQL = "update ppleased set yearfirstassessed = year(format('01/01/' & right(leasedate & '',2))) where isdate(leasedate & '')"
/// </summary>
/// <summary>
/// Call clsSave.Execute(strSQL, modPPGN.strPPDatabase)
/// </summary>
/// <summary>
///
/// </summary>
/// <summary>
/// update any that are just comments
/// </summary>
/// <summary>
/// strSQL = "update ppleased set yearfirstassessed = 0 where cd = '-'"
/// </summary>
/// <summary>
/// Call clsSave.Execute(strSQL, modPPGN.strPPDatabase)
/// </summary>
/// <summary>
///
/// </summary>
/// <summary>
/// Exit Function
/// </summary>
/// <summary>
/// ErrorHandler:
/// </summary>
/// <summary>
/// MsgBox "Error Number " & Err.Number & "  " & Err.Description & vbNewLine & "InFillLeasedNewValFields", vbCritical, "Error"
/// </summary>
/// <summary>
/// End Function
/// </summary>
// On Error GoTo ErrorHandler
// ErrorHandler:
