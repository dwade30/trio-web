﻿//Fecher vbPorter - Version 1.0.0.32
using fecherFoundation;
using Global;
using System;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptNewBETEApplication.
	/// </summary>
	public partial class rptNewBETEApplication : BaseSectionReport
	{
		public rptNewBETEApplication()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "BETE";
		}

		public static rptNewBETEApplication InstancePtr
		{
			get
			{
				return (rptNewBETEApplication)Sys.GetInstance(typeof(rptNewBETEApplication));
			}
		}

		protected rptNewBETEApplication _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptNewBETEApplication	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private cBETEApplicationReport theReport;

		public cBETEApplicationReport BETEReportObject
		{
			get
			{
				cBETEApplicationReport BETEReportObject = null;
				BETEReportObject = theReport;
				return BETEReportObject;
			}
		}

		public void Init(ref cBETEApplicationReport cReport)
		{
			theReport = cReport;
			if (cReport.BETEApplications.ItemCount() > 0)
			{
				cReport.BETEApplications.MoveFirst();
				frmReportViewer.InstancePtr.Init(rptObj: this, boolAllowEmail: true, strAttachmentName: "BETEApplication");
			}
			else
			{
				MessageBox.Show("No eligible items found", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = !theReport.BETEApplications.IsCurrent();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			theReport.BETEApplications.MoveFirst();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (theReport.BETEApplications.IsCurrent())
			{
				SubReport1.Report = srptBETEIndividualApplication.InstancePtr;
				srptBETEIndividualApplication.InstancePtr.SetBETEApp((cBETEApplication)theReport.BETEApplications.GetCurrentApplication());
				theReport.BETEApplications.MoveNext();
			}
		}

		
	}
}
