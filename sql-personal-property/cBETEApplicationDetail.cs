﻿//Fecher vbPorter - Version 1.0.0.32
namespace TWPP0000
{
	public class cBETEApplicationDetail
	{
		//=========================================================
		private string strDescription = string.Empty;
		private string strPurchased = string.Empty;
		private int intAge;
		private double dblValueNew;
		private double dblEstimatedValue;
		private string strEquipmentLocation = string.Empty;
		private bool boolTIF;
		private bool boolBETEEligible;
		private double dblAssessment;

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public string PlacedInService
		{
			set
			{
				strPurchased = value;
			}
			get
			{
				string PlacedInService = "";
				PlacedInService = strPurchased;
				return PlacedInService;
			}
		}

		public int Age
		{
			set
			{
				intAge = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				int Age = 0;
				Age = intAge;
				return Age;
			}
		}

		public double ValueNew
		{
			set
			{
				dblValueNew = value;
			}
			get
			{
				double ValueNew = 0;
				ValueNew = dblValueNew;
				return ValueNew;
			}
		}

		public double EstimatedValue
		{
			set
			{
				dblEstimatedValue = value;
			}
			get
			{
				double EstimatedValue = 0;
				EstimatedValue = dblEstimatedValue;
				return EstimatedValue;
			}
		}

		public string EquipmentLocation
		{
			set
			{
				strEquipmentLocation = value;
			}
			get
			{
				string EquipmentLocation = "";
				EquipmentLocation = strEquipmentLocation;
				return EquipmentLocation;
			}
		}

		public bool TIF
		{
			set
			{
				boolTIF = value;
			}
			get
			{
				bool TIF = false;
				TIF = boolTIF;
				return TIF;
			}
		}

		public bool BETEEligible
		{
			set
			{
				boolBETEEligible = value;
			}
			get
			{
				bool BETEEligible = false;
				BETEEligible = boolBETEEligible;
				return BETEEligible;
			}
		}

		public double AssessedValue
		{
			set
			{
				dblAssessment = value;
			}
			get
			{
				double AssessedValue = 0;
				AssessedValue = dblAssessment;
				return AssessedValue;
			}
		}
	}
}
