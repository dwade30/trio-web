﻿namespace TWPP0000
{
	/// <summary>
	/// Summary description for srptLeasedOriginalCost.
	/// </summary>
	partial class srptLeasedOriginalCost
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptLeasedOriginalCost));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQty = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLeaseDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonths = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtL1P2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQty)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLeaseDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonths)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtL1P2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtLine,
				this.txtCD,
				this.txtQty,
				this.txtDescription,
				this.txtCost,
				this.txtLeaseDate,
				this.txtMonths,
				this.txtL1P2,
				this.txtRent
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0.01041667F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Label8,
				this.Label9,
				this.Field1
			});
			this.GroupHeader1.Height = 0.53125F;
			this.GroupHeader1.KeepTogether = true;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Label1
			// 
			this.Label1.Height = 0.19F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.125F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label1.Text = "Line";
			this.Label1.Top = 0.3125F;
			this.Label1.Width = 0.375F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.19F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.9375F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label2.Text = "CD";
			this.Label2.Top = 0.3125F;
			this.Label2.Width = 0.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.21875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 1.3125F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label3.Text = "Qty";
			this.Label3.Top = 0.3125F;
			this.Label3.Width = 0.33F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.19F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 1.75F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label4.Text = "Description";
			this.Label4.Top = 0.3125F;
			this.Label4.Width = 1.875F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.19F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 3.75F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Label5.Text = "Org Cost";
			this.Label5.Top = 0.3125F;
			this.Label5.Width = 0.875F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.19F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 4.6875F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label6.Text = "Lease Date";
			this.Label6.Top = 0.3125F;
			this.Label6.Width = 0.8125F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.19F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 5.5F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label7.Text = "# MOs";
			this.Label7.Top = 0.3125F;
			this.Label7.Width = 0.5F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.34375F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 6F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label8.Text = "L=1 P=2";
			this.Label8.Top = 0.15625F;
			this.Label8.Width = 0.375F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.19F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 6.375F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Label9.Text = "Rent/Month";
			this.Label9.Top = 0.3125F;
			this.Label9.Width = 0.9375F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 1.5F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Field1.Text = "-------------  Leased Description -------------";
			this.Field1.Top = 0F;
			this.Field1.Width = 4.5F;
			// 
			// txtLine
			// 
			this.txtLine.CanGrow = false;
			this.txtLine.Height = 0.1875F;
			this.txtLine.Left = 0.125F;
			this.txtLine.Name = "txtLine";
			this.txtLine.Style = "font-family: \'Tahoma\'";
			this.txtLine.Text = "Field1";
			this.txtLine.Top = 0F;
			this.txtLine.Width = 0.75F;
			// 
			// txtCD
			// 
			this.txtCD.CanGrow = false;
			this.txtCD.Height = 0.1875F;
			this.txtCD.Left = 0.9375F;
			this.txtCD.Name = "txtCD";
			this.txtCD.Style = "font-family: \'Tahoma\'";
			this.txtCD.Text = "Field2";
			this.txtCD.Top = 0F;
			this.txtCD.Width = 0.3125F;
			// 
			// txtQty
			// 
			this.txtQty.CanGrow = false;
			this.txtQty.Height = 0.1875F;
			this.txtQty.Left = 1.3125F;
			this.txtQty.MultiLine = false;
			this.txtQty.Name = "txtQty";
			this.txtQty.Style = "font-family: \'Tahoma\'";
			this.txtQty.Text = "Field3";
			this.txtQty.Top = 0F;
			this.txtQty.Width = 0.375F;
			// 
			// txtDescription
			// 
			this.txtDescription.CanShrink = true;
			this.txtDescription.Height = 0.1875F;
			this.txtDescription.Left = 1.75F;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Style = "font-family: \'Tahoma\'";
			this.txtDescription.Text = "Field4";
			this.txtDescription.Top = 0F;
			this.txtDescription.Width = 1.875F;
			// 
			// txtCost
			// 
			this.txtCost.CanGrow = false;
			this.txtCost.Height = 0.1875F;
			this.txtCost.Left = 3.75F;
			this.txtCost.MultiLine = false;
			this.txtCost.Name = "txtCost";
			this.txtCost.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtCost.Text = "Field5";
			this.txtCost.Top = 0F;
			this.txtCost.Width = 0.875F;
			// 
			// txtLeaseDate
			// 
			this.txtLeaseDate.CanGrow = false;
			this.txtLeaseDate.Height = 0.1875F;
			this.txtLeaseDate.Left = 4.6875F;
			this.txtLeaseDate.MultiLine = false;
			this.txtLeaseDate.Name = "txtLeaseDate";
			this.txtLeaseDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtLeaseDate.Text = "Field6";
			this.txtLeaseDate.Top = 0F;
			this.txtLeaseDate.Width = 0.75F;
			// 
			// txtMonths
			// 
			this.txtMonths.CanGrow = false;
			this.txtMonths.Height = 0.1875F;
			this.txtMonths.Left = 5.5F;
			this.txtMonths.MultiLine = false;
			this.txtMonths.Name = "txtMonths";
			this.txtMonths.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtMonths.Text = "Field7";
			this.txtMonths.Top = 0F;
			this.txtMonths.Width = 0.5F;
			// 
			// txtL1P2
			// 
			this.txtL1P2.CanGrow = false;
			this.txtL1P2.Height = 0.1875F;
			this.txtL1P2.Left = 6.0625F;
			this.txtL1P2.MultiLine = false;
			this.txtL1P2.Name = "txtL1P2";
			this.txtL1P2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtL1P2.Text = "Field8";
			this.txtL1P2.Top = 0F;
			this.txtL1P2.Width = 0.25F;
			// 
			// txtRent
			// 
			this.txtRent.CanGrow = false;
			this.txtRent.Height = 0.1875F;
			this.txtRent.Left = 6.5F;
			this.txtRent.MultiLine = false;
			this.txtRent.Name = "txtRent";
			this.txtRent.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtRent.Text = "Field9";
			this.txtRent.Top = 0F;
			this.txtRent.Width = 0.8125F;
			// 
			// srptLeasedOriginalCost
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.Name = "ActiveReport1";
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.489583F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQty)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLeaseDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonths)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtL1P2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQty;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLeaseDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonths;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtL1P2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRent;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
