﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptNewValuation.
	/// </summary>
	public partial class rptNewValuation : BaseSectionReport
	{
		public rptNewValuation()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "New Valuation";
		}

		public static rptNewValuation InstancePtr
		{
			get
			{
				return (rptNewValuation)Sys.GetInstance(typeof(rptNewValuation));
			}
		}

		protected rptNewValuation _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsReport?.Dispose();
                clsReport = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptNewValuation	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngYear;
		clsDRWrapper clsReport = new clsDRWrapper();
		int lngCount;
		int lngTotalNew;
		int lngTotalReplacing;
		int lngTotalNet;
		string strWhere = "";

		public void Init(bool boolYearNew)
		{
			string strSQL = "";
			clsDRWrapper clsTotal = new clsDRWrapper();
            try
            {
                // On Error GoTo ErrorHandler
                string strMasterJoinJoin;
                strMasterJoinJoin = modPPGN.GetMasterJoinForJoin(true);
                lblMuniname.Text = modGlobalConstants.Statics.MuniName;
                lblTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
                lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
                lngYear = modGlobal.Statics.CustomizeStuff.DefaultYearFirstAssessed;
                if (lngYear == 0)
                {
                    lngYear = DateTime.Today.Year;
                }

                //FC:FINAL:BBE - used object and reassign it back
                object strInput = lngYear;
                if (!boolYearNew)
                {
                    if (!frmInput.InstancePtr.Init(ref strInput, "New Valuation Year", "Enter the Valuation Year", 1440,
                        false, modGlobalConstants.InputDTypes.idtWholeNumber, FCConvert.ToString(lngYear), true))
                    {
                        return;
                    }
                }
                else
                {
                    if (!frmInput.InstancePtr.Init(ref strInput, "Year New", "Enter the Year New", 1440, false,
                        modGlobalConstants.InputDTypes.idtWholeNumber, FCConvert.ToString(lngYear), true))
                    {
                        return;
                    }
                }

                lngYear = FCConvert.ToInt32(strInput);
                //FC:FINAL:DDU:#1335 - write explicit text as in designer
                lblTitle.Text = "New Valuation for Year " + FCConvert.ToString(lngYear);
                lngTotalNew = 0;
                lngTotalReplacing = 0;
                lngTotalNet = 0;
                lngCount = 0;
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
                ////Application.DoEvents();
                if (!boolYearNew)
                {
                    strSQL =
                        "select cd,sum(totnew) as totNewVal,sum(totreplacing) as totAmountReplacing from (select * from ((select cd,sum(ppitemized.[value]) as totnew,sum(amountreplacing) as totreplacing from ppitemized inner join ppmaster on (ppmaster.account = ppitemized.account) where not deleted = 1 and yearfirstassessed = " +
                        FCConvert.ToString(lngYear) + " and ppitemized.value > 0 and exemptyear <> " +
                        FCConvert.ToString(lngYear) +
                        " group by ppitemized.CD) union all (select cd,sum(ppleased.[value]) as totnew,sum(amountreplacing) as totreplacing from ppleased inner join ppmaster on (ppmaster.account = ppleased.account) where not deleted = 1 and yearfirstassessed = " +
                        FCConvert.ToString(lngYear) + " and ppleased.value > 0 and exemptyear <> " +
                        FCConvert.ToString(lngYear) +
                        " group by ppleased.CD) union all (select cd,sum([beteexempt]) as totnew,sum(amountreplacing) as totreplacing from ppitemized inner join ppmaster on (ppmaster.account = ppitemized.account) where not deleted = 1 and yearfirstassessed = " +
                        FCConvert.ToString(lngYear) + " and exemptyear = " + FCConvert.ToString(lngYear) +
                        " group by ppitemized.CD) union all (";
                    strSQL +=
                        "select cd,sum([beteexempt]) as totnew,sum(amountreplacing) as totreplacing from ppleased inner join ppmaster on (ppmaster.account = ppleased.account) where not deleted = 1 and yearfirstassessed = " +
                        FCConvert.ToString(lngYear) + " and beteexempt > 0 and exemptyear = " +
                        FCConvert.ToString(lngYear) + " group by ppleased.CD))as abc) as def group by cd";
                    clsTotal.OpenRecordset(strSQL, modPPGN.strPPDatabase);
                    strSQL =
                        "select account,sum(totnew) as totNewVal,sum(totreplacing) as totAmountReplacing from (select * from ((select ppitemized.account as account,sum(ppitemized.[value] ) as totnew,sum(amountreplacing) as totreplacing from ppitemized inner join ppmaster on (ppmaster.account = ppitemized.account) where not deleted = 1 and yearfirstassessed = " +
                        FCConvert.ToString(lngYear) + " and ppitemized.value > 0  and exemptyear <> " +
                        FCConvert.ToString(lngYear) +
                        " group by ppitemized.account)  union all (select ppleased.account,sum(ppleased.[value] ) as totnew,sum(amountreplacing) as totreplacing from ppleased inner join ppmaster on (ppmaster.account = ppleased.account) where not deleted = 1 and yearfirstassessed = " +
                        FCConvert.ToString(lngYear) + " and ppleased.value > 0 and exemptyear <> " +
                        FCConvert.ToString(lngYear) + " group by ppleased.account) union all (";
                    strSQL +=
                        "select ppitemized.account as account,sum([beteexempt] ) as totnew,sum(amountreplacing) as totreplacing from ppitemized inner join ppmaster on (ppmaster.account = ppitemized.account) where not deleted = 1 and yearfirstassessed = " +
                        FCConvert.ToString(lngYear) + "  and beteexempt > 0  and exemptyear = " +
                        FCConvert.ToString(lngYear) +
                        " group by ppitemized.account) union all (select ppleased.account,sum([beteexempt]) as totnew,sum(amountreplacing) as totreplacing from ppleased inner join ppmaster on (ppmaster.account = ppleased.account) where not deleted = 1 and yearfirstassessed = " +
                        FCConvert.ToString(lngYear) + " and  beteexempt > 0 and exemptyear = " +
                        FCConvert.ToString(lngYear) + " group by ppleased.account)) as abc) as def group by account";
                }
                else
                {
                    // strSQL = "select cd,sum(totnew) as totNewVal,sum(totreplacing) as totAmountReplacing from (select * from ((select cd,sum(ppitemized.[value] ) as totnew,sum(amountreplacing) as totreplacing from ppitemized inner join ppmaster on (ppmaster.account = ppitemized.account) where not deleted = 1 and [YEAR] = " & lngYear & " and ppitemized.value > 0  and exemptyear <> " & lngYear & " group by cd)  union all (select cd,sum(ppleased.[value] ) as totnew,sum(amountreplacing) as totreplacing from ppleased inner join ppmaster on (ppmaster.account = ppleased.account) where not deleted = 1 and YEAR(RIGHT('00' + LEASEDATE,2)) = " & lngYear & " and ppleased.value > 0  and exemptyear <> " & lngYear & " group by cd) union all (select cd,sum([beteexempt] ) as totnew,sum(amountreplacing) as totreplacing from ppitemized inner join ppmaster on (ppmaster.account = ppitemized.account) where not deleted = 1 and [YEAR] = " & lngYear & " and beteexempt > 0  and exemptyear = " & lngYear & " group by cd) union all ("
                    strSQL =
                        "select cd,sum(totnew) as totNewVal,sum(totreplacing) as totAmountReplacing from (select * from ((select cd,sum(ppitemized.[value] ) as totnew,sum(amountreplacing) as totreplacing from ppitemized inner join ppmaster on (ppmaster.account = ppitemized.account) where not deleted = 1 and [YEAR] = " +
                        FCConvert.ToString(lngYear) + " and ppitemized.value > 0  and exemptyear <> " +
                        FCConvert.ToString(lngYear) +
                        " group by cd)  union all (select cd,sum(ppleased.[value] ) as totnew,sum(amountreplacing) as totreplacing from ppleased inner join ppmaster on (ppmaster.account = ppleased.account) where not deleted = 1";
                    strSQL += " and YEAR(cast(RIGHT('00' + cast(LEASEDATE as varchar(10)),2) as int)) = " +
                              FCConvert.ToString(lngYear) + " and ppleased.value > 0  and exemptyear <> " +
                              FCConvert.ToString(lngYear) +
                              " group by cd) union all (select cd,sum([beteexempt] ) as totnew,sum(amountreplacing) as totreplacing from ppitemized inner join ppmaster on (ppmaster.account = ppitemized.account) where not deleted = 1 and [YEAR] = " +
                              FCConvert.ToString(lngYear) + " and beteexempt > 0  and exemptyear = " +
                              FCConvert.ToString(lngYear) + " group by cd) union all (";
                    strSQL +=
                        "select cd,sum([beteexempt] ) as totnew,sum(amountreplacing) as totreplacing from ppleased inner join ppmaster on (ppmaster.account = ppleased.account) where not deleted = 1 and ";
                    strSQL += " YEAR(cast(right('00' + cast(LEASEDATE as varchar(10)),2) as int)) = " +
                              FCConvert.ToString(lngYear) + " and beteexempt > 0  and exemptyear = " +
                              FCConvert.ToString(lngYear) + " group by cd)) as abc)as def group by cd";
                    clsTotal.OpenRecordset(strSQL, modPPGN.strPPDatabase);
                    strSQL =
                        "select account,sum(totnew) as totNewVal,sum(totreplacing) as totAmountReplacing from (select * from ((select ppitemized.account as account,sum(ppitemized.[value] ) as totnew,sum(amountreplacing) as totreplacing from ppitemized inner join ppmaster on (ppmaster.account = ppitemized.account) where not deleted = 1 and [YEAR] = " +
                        FCConvert.ToString(lngYear) + " and ppitemized.value > 0  and exemptyear <> " +
                        FCConvert.ToString(lngYear) +
                        " group by ppitemized.account)  union all (select ppleased.account,sum(ppleased.[value]) as totnew,sum(amountreplacing) as totreplacing from ppleased inner join ppmaster on (ppmaster.account = ppleased.account) where not deleted = 1 ";
                    strSQL += " and YEAR(cast(RIGHT('00' + cast(LEASEDATE as varchar(10)),2) as int)) = " +
                              FCConvert.ToString(lngYear) + " and ppleased.value > 0 and exemptyear <> " +
                              FCConvert.ToString(lngYear) + " group by ppleased.account) union all (";
                    strSQL +=
                        "select ppitemized.account as account,sum([beteexempt] ) as totnew,sum(amountreplacing) as totreplacing from ppitemized inner join ppmaster on (ppmaster.account = ppitemized.account) where not deleted = 1 and [YEAR] = " +
                        FCConvert.ToString(lngYear) + " and beteexempt > 0  and exemptyear = " +
                        FCConvert.ToString(lngYear) +
                        " group by ppitemized.account) union all (select ppleased.account,sum([beteexempt]) as totnew,sum(amountreplacing) as totreplacing from ppleased inner join ppmaster on (ppmaster.account = ppleased.account) where not deleted = 1 ";
                    strSQL += " and YEAR(cast(RIGHT('00' + cast(LEASEDATE as varchar(10)),2) as int)) = " +
                              FCConvert.ToString(lngYear) + " and beteexempt > 0 and exemptyear = " +
                              FCConvert.ToString(lngYear) +
                              " group by ppleased.account)) as abc) as def group by account";
                }

                clsReport.OpenRecordset(strSQL, modPPGN.strPPDatabase);
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                if (clsReport.EndOfFile())
                {
                    MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                int x;
                int lngNew = 0;
                int lngRepl = 0;
                int lngNet = 0;
                if (!clsTotal.EndOfFile())
                {
                    for (x = 1; x <= 9; x++)
                    {
                        if (clsTotal.FindFirstRecord("cd", "'" + FCConvert.ToString(x) + "'"))
                        {
                            // TODO Get_Fields: Field [totnewval] not found!! (maybe it is an alias?)
                            lngNew = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTotal.Get_Fields("totnewval"))));
                            // TODO Get_Fields: Field [totamountreplacing] not found!! (maybe it is an alias?)
                            lngRepl = FCConvert.ToInt32(
                                Math.Round(Conversion.Val(clsTotal.Get_Fields("totamountreplacing"))));
                            (ReportFooter.Controls["txtTotNewCat" + x] as
                                    GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                                Strings.Format(lngNew, "#,###,##0");
                            (ReportFooter.Controls["txtTotReplCat" + x] as
                                    GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                                Strings.Format(lngRepl, "#,###,##0");
                            lngNet = lngNew - lngRepl;
                            if (lngNet < 0)
                                lngNet = 0;
                            (ReportFooter.Controls["txtTotNetCat" + x] as
                                    GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                                Strings.Format(lngNet, "#,###,##0");
                        }
                    }

                    // x
                }

                clsTotal.OpenRecordset("select * from ratiotrends", modPPGN.strPPDatabase);
                while (!clsTotal.EndOfFile())
                {
                    if (Strings.Trim(FCConvert.ToString(clsTotal.Get_Fields_String("description"))) != string.Empty)
                    {
                        // TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
                        (ReportFooter.Controls["lblCat" + Conversion.Val(clsTotal.Get_Fields("type"))] as
                                GrapeCity.ActiveReports.SectionReportModel.Label).Text =
                            FCConvert.ToString(clsTotal.Get_Fields_String("description"));
                    }

                    clsTotal.MoveNext();
                }

                frmReportViewer.InstancePtr.Init(rptObj: this, boolAllowEmail: true, strAttachmentName: "NewValuation");
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                MessageBox.Show(
                    "Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " +
                    Information.Err(ex).Description + "\r\n" + "In Init", "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Hand);
            }
            finally
            {
                clsTotal.Dispose();
            }
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsReport.EndOfFile();
		}

        private void Detail_Format(object sender, EventArgs e)
        {
            int lngTemp = 0;
            int lngReplace = 0;
            string strMasterJoinJoin;
            strMasterJoinJoin = modPPGN.GetMasterJoinForJoin(true);
            using (clsDRWrapper clsLoad = new clsDRWrapper())
            {
                if (!clsReport.EndOfFile())
                {
                    // TODO Get_Fields: Field [totnewval] not found!! (maybe it is an alias?)
                    lngTemp = FCConvert.ToInt32(clsReport.Get_Fields("totnewval"));
                    lngTotalNew += lngTemp;
                    txtNewValuation.Text = Strings.Format(lngTemp, "#,###,###,##0");
                    // TODO Get_Fields: Field [totamountreplacing] not found!! (maybe it is an alias?)
                    lngReplace =
                        FCConvert.ToInt32(Math.Round(Conversion.Val(clsReport.Get_Fields("totamountreplacing"))));
                    lngTotalReplacing += lngReplace;
                    txtAmountReplacing.Text = Strings.Format(lngReplace, "#,###,###,##0");
                    lngTemp -= lngReplace;
                    lngTotalNet += lngTemp;
                    if (lngTemp < 0)
                        lngTemp = 0;
                    txtNetNew.Text = Strings.Format(lngTemp, "#,###,###,##0");
                    lngCount += 1;
                    // TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
                    txtAccount.Text = FCConvert.ToString(clsReport.Get_Fields("account"));
                    // Call clsLoad.OpenRecordset("select name from ppmaster where account = " & clsReport.Fields("account"), modPPGN.strPPDatabase)
                    // TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
                    clsLoad.OpenRecordset(
                        "select name from " + strMasterJoinJoin + " where account = " + clsReport.Get_Fields("account"),
                        modPPGN.strPPDatabase);
                    txtName.Text = clsLoad.Get_Fields_String("name");
                    clsReport.MoveNext();
                }

            }
        }

        private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtCount.Text = Strings.Format(lngCount, "#,###,##0");
			txtTotalNet.Text = Strings.Format(lngTotalNet, "#,###,###,##0");
			txtTotalNew.Text = Strings.Format(lngTotalNew, "#,###,###,##0");
			txtTotalReplacing.Text = Strings.Format(lngTotalReplacing, "#,###,###,##0");
		}

		
	}
}
