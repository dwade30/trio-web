using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using SharedApplication.Extensions;
using TWSharedLibrary;
using Wisej.Web;

namespace TWPP0000
{
    /// <summary>
    /// Summary description for rptInventoryListing.
    /// </summary>
    public partial class rptInventoryListing : BaseSectionReport
    {
        clsDRWrapper rsLoad = new/*AsNew*/ clsDRWrapper();
        int lngCurAcct;
        clsDRWrapper rsBusCodes = new/*AsNew*/ clsDRWrapper();

        public rptInventoryListing()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }
        private void InitializeComponentEx()
        {
            this.Name = "Inventory Listing";
            if (_InstancePtr == null)
                _InstancePtr = this;
            this.ReportStart += RptInventoryListing_ReportStart;
            this.FetchData += RptInventoryListing_FetchData;
            this.detail.Format += Detail_Format;
        }

        private void Detail_Format(object sender, EventArgs e)
        {
            try
            {
                if (!rsLoad.EndOfFile())
                {
                    string strTemp = "";
                    clsDRWrapper rsTemp = new /*AsNew*/ clsDRWrapper();
                    double dblAssess = 0;
                    double dblBETE = 0;

                    SubReport1.Report = new srptItemizedInventory();
                    SubReport2.Report = new srptLeasedInventory();
                    SubReport1.Report.UserData = rsLoad.Get_Fields_Int32("account");
                    SubReport2.Report.UserData = rsLoad.Get_Fields_Int32("account");
                    rsTemp.OpenRecordset(
                        "select * from ppvaluations where valuekey = " + rsLoad.Get_Fields_Int32("account"),
                        "PersonalProperty");
                    if (!rsTemp.EndOfFile())
                    {
                        txtTotCat1.Text = rsTemp.Get_Fields_Int32("category1").FormatAsNumber();
                        txtTotCat2.Text = rsTemp.Get_Fields_Int32("category2").FormatAsNumber();
                        txtTotCat3.Text = rsTemp.Get_Fields_Int32("category3").FormatAsNumber();
                        txtTotCat4.Text = rsTemp.Get_Fields_Int32("category4").FormatAsNumber();
                        txtTotCat5.Text = rsTemp.Get_Fields_Int32("category5").FormatAsNumber();
                        txtTotCat6.Text = rsTemp.Get_Fields_Int32("category6").FormatAsNumber();
                        txtTotCat7.Text = rsTemp.Get_Fields_Int32("category7").FormatAsNumber();
                        txtTotCat8.Text = rsTemp.Get_Fields_Int32("category8").FormatAsNumber();
                        txtTotCat9.Text = rsTemp.Get_Fields_Int32("category9").FormatAsNumber();
                        txtTotBETE1.Text = rsTemp.Get_Fields_Int32("cat1exempt").FormatAsNumber();
                        txtTotBETE2.Text = rsTemp.Get_Fields_Int32("cat2exempt").FormatAsNumber();
                        txtTotBETE3.Text = rsTemp.Get_Fields_Int32("cat3exempt").FormatAsNumber();
                        txtTotBETE4.Text = rsTemp.Get_Fields_Int32("cat4exempt").FormatAsNumber();
                        txtTotBETE5.Text = rsTemp.Get_Fields_Int32("cat5exempt").FormatAsNumber();
                        txtTotBETE6.Text = rsTemp.Get_Fields_Int32("cat6exempt").FormatAsNumber();
                        txtTotBETE7.Text = rsTemp.Get_Fields_Int32("cat7exempt").FormatAsNumber();
                        txtTotBETE8.Text = rsTemp.Get_Fields_Int32("cat8exempt").FormatAsNumber();
                        txtTotBETE9.Text = rsTemp.Get_Fields_Int32("cat9exempt").FormatAsNumber();
                        int x;
                        for (x = 1; x <= 9; x++)
                        {
                            dblAssess += rsTemp.Get_Fields_Int32("category" + x);
                            dblBETE += rsTemp.Get_Fields_Int32("cat" + x + "exempt");
                        } // x
                    }
                    else
                    {
                        dblAssess = 0;
                        txtTotCat1.Text = "0";
                        txtTotCat2.Text = "0";
                        txtTotCat3.Text = "0";
                        txtTotCat4.Text = "0";
                        txtTotCat5.Text = "0";
                        txtTotCat6.Text = "0";
                        txtTotCat7.Text = "0";
                        txtTotCat8.Text = "0";
                        txtTotCat9.Text = "0";
                        txtTotBETE1.Text = "0";
                        txtTotBETE2.Text = "0";
                        txtTotBETE3.Text = "0";
                        txtTotBETE4.Text = "0";
                        txtTotBETE5.Text = "0";
                        txtTotBETE6.Text = "0";
                        txtTotBETE7.Text = "0";
                        txtTotBETE8.Text = "0";
                        txtTotBETE9.Text = "0";
                        dblBETE = 0;
                    }

                    txtComment.Text = rsLoad.Get_Fields_String("comment");
                    txtTotValue.Text = Convert.ToInt32(dblAssess).FormatAsNumber();
                    txtTotBETE.Text = Convert.ToInt32(dblBETE).FormatAsNumber();
                    //SubReport1.Report.UserData = rsLoad.Get_Fields_Int32("account");
                    //SubReport2.Report.UserData = rsLoad.Get_Fields_Int32("account");
                    txtAccount.Text = rsLoad.Get_Fields_Int32("account").ToString();
                    txtName.Text = rsLoad.Get_Fields_String("name");
                    txtAddr1.Text = rsLoad.Get_Fields_String("address1");
                    txtAddr2.Text = rsLoad.Get_Fields_String("address2");
                    txtCity.Text = rsLoad.Get_Fields_String("city");
                    txtState.Text = rsLoad.Get_Fields_String("state");
                    txtZip.Text = rsLoad.Get_Fields_String("zip").Trim();
                    txtZip4.Text = rsLoad.Get_Fields_String("zip4").Trim();
                    if (rsLoad.Get_Fields_Int32("streetnumber") > 0)
                    {
                        txtLocation.Text = (rsLoad.Get_Fields_Int32("streetnumber").ToString() +
                                            rsLoad.Get_Fields_String("streetapt") + " " +
                                            rsLoad.Get_Fields_String("street")).Trim();
                    }
                    else
                    {
                        txtLocation.Text =
                            fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("street")));
                    }

                    if (!fecherFoundation.FCUtils.IsNull(rsLoad.Get_Fields_String("phonenumber")))
                    {
                        if (FCConvert.ToString(rsLoad.Get_Fields_String("phonenumber")).Length == 13)
                        {
                            strTemp = fecherFoundation.Strings.Mid(
                                FCConvert.ToString(rsLoad.Get_Fields_String("phonenumber")), 1, 1);
                            if (fecherFoundation.Information.IsNumeric(
                                fecherFoundation.Strings.Mid(
                                    FCConvert.ToString(rsLoad.Get_Fields_String("phonenumber")), 2, 3)))
                            {
                                strTemp += fecherFoundation.Strings.Mid(
                                    FCConvert.ToString(rsLoad.Get_Fields_String("phonenumber")), 2, 3);
                            }
                            else
                            {
                                strTemp += "000";
                            }

                            strTemp += ")";
                            if (fecherFoundation.Information.IsNumeric(
                                fecherFoundation.Strings.Mid(
                                    FCConvert.ToString(rsLoad.Get_Fields_String("phonenumber")), 6, 3)))
                            {
                                strTemp += fecherFoundation.Strings.Mid(
                                    FCConvert.ToString(rsLoad.Get_Fields_String("phonenumber")), 6, 3);
                            }
                            else
                            {
                                strTemp += "000";
                            }

                            strTemp += "-";
                            if (fecherFoundation.Information.IsNumeric(
                                fecherFoundation.Strings.Mid(
                                    FCConvert.ToString(rsLoad.Get_Fields_String("phonenumber")), 10)))
                            {
                                strTemp += fecherFoundation.Strings.Mid(
                                    FCConvert.ToString(rsLoad.Get_Fields_String("phonenumber")), 10);
                            }
                            else
                            {
                                strTemp += "0000";
                            }

                            txtPhone.Text = strTemp;
                        }
                        else
                        {
                            txtPhone.Text = rsLoad.Get_Fields_String("phonenumber");
                        }
                    }
                    else
                    {
                        txtPhone.Text = "";
                    }

                    txtOpen1.Text = rsLoad.Get_Fields_String("open1");
                    txtOpen2.Text = rsLoad.Get_Fields_String("open2");
                    if (rsBusCodes.FindFirstRecord("businesscode",
                        fecherFoundation.Conversion.Val(FCConvert.ToString(rsLoad.Get_Fields_Int32("businesscode")))))
                    {
                        txtBusinesscode.Text = rsBusCodes.Get_Fields_String("BUSINESSTYPE");
                    }
                    else
                    {
                        txtBusinesscode.Text = "";
                    }

                    rsLoad.MoveNext();
                }
                else
                {
                    SubReport1.Report = null;
                    SubReport2.Report = null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void RptInventoryListing_FetchData(object sender, FetchEventArgs eArgs)
        {            
            eArgs.EOF = rsLoad.EndOfFile();
            if (!rsLoad.EndOfFile())
            {
                lngCurAcct = Convert.ToInt32(rsLoad.Get_Fields_Int32("account"));
            }
        }

        private void RptInventoryListing_ReportStart(object sender, EventArgs e)
        {
            lblDate.Text = DateTime.Today.FormatAndPadShortDate();
            lblTime.Text = DateTime.Now.FormatAndPadTime();
            lblMuniname.Text = modGlobalConstants.Statics.MuniName.Trim();

            if (modPPGN.Statics.Open1Title.Trim() != string.Empty)
            {
                lblOpen1.Text = modPPGN.Statics.Open1Title.Trim();
            }
            if (modPPGN.Statics.Open2Title.Trim() != string.Empty)
            {
                lblOpen2.Text = modPPGN.Statics.Open2Title.Trim();
            }
            lblAssessmentYear.Text = "Assessment Year " + modGlobal.Statics.CustomizeStuff.EffectiveDepreciationYear;
            clsDRWrapper rsTemp = new/*AsNew*/ clsDRWrapper();
            rsTemp.OpenRecordset("select * from ratiotrends order by [type]", "PersonalProperty");
            while (!rsTemp.EndOfFile())
            {
                switch (fecherFoundation.Conversion.Val(FCConvert.ToString(rsTemp.Get_Fields_Int32("type"))))
                {

                    case 1:
                        {
                            lblCat1.Text = rsTemp.Get_Fields_String("description");
                            break;
                        }
                    case 2:
                        {
                            lblCat2.Text = rsTemp.Get_Fields_String("description");
                            break;
                        }
                    case 3:
                        {
                            lblCat3.Text = rsTemp.Get_Fields_String("description");
                            break;
                        }
                    case 4:
                        {
                            lblCat4.Text = rsTemp.Get_Fields_String("description");
                            break;
                        }
                    case 5:
                        {
                            lblCat5.Text = rsTemp.Get_Fields_String("description");
                            break;
                        }
                    case 6:
                        {
                            lblCat6.Text = rsTemp.Get_Fields_String("description");
                            break;
                        }
                    case 7:
                        {
                            lblCat7.Text = rsTemp.Get_Fields_String("description");
                            break;
                        }
                    case 8:
                        {
                            lblCat8.Text = rsTemp.Get_Fields_String("description");
                            break;
                        }
                    case 9:
                        {
                            lblCat9.Text = rsTemp.Get_Fields_String("description");
                            break;
                        }
                } //end switch
                rsTemp.MoveNext();
            }
            txtRatioMessage.Text = "Totals reflect a " + (modGlobal.Statics.CustomizeStuff.dblRatio * 100) + "% assessment ratio";
            if (!rsLoad.EndOfFile())
            {
                lngCurAcct = Convert.ToInt32(rsLoad.Get_Fields_Int32("account"));
                // GroupHeader1.GroupValue = lngCurAcct
                rsBusCodes.OpenRecordset("select * from businesscodes order by businesscode", "PersonalProperty");
            }
        }

        public static rptInventoryListing InstancePtr
        {
            get
            {
                return (rptInventoryListing)Sys.GetInstance(typeof(rptInventoryListing));
            }
        }

        protected rptInventoryListing _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
            {
                _InstancePtr = null;
                Sys.ClearInstance(this);
            }

            if (disposing)
            {
                rsBusCodes?.Dispose();
                rsLoad?.Dispose();
                rsLoad = null;
                rsBusCodes = null;
            }
            base.Dispose(disposing);
        }

        public void Init( string strsql)
        {
            rsLoad.OpenRecordset(strsql, "PersonalProperty");
            if (rsLoad.EndOfFile())
            {
                MessageBox.Show("No records found");
                this.Unload();
                return;
            }
            frmReportViewer.InstancePtr.Init(this);
        }
    }
}
