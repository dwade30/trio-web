namespace TWPP0000
{
    /// <summary>
    /// Summary description for srptLeasedInventory.
    /// </summary>
    partial class srptLeasedInventory
    {
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                rsReport?.Dispose();
                rsReport = null;
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(srptLeasedInventory));
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtQuantity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCategory = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGood = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBETE = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBET = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.txtTotalCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalBETE = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblCat1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTotCat1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotBETE1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblCat2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTotCat2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotBETE2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblCat3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTotCat3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotBETE3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblCat4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTotCat4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotBETE4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblCat5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTotCat5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotBETE5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblCat6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTotCat6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotBETE6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblCat7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTotCat7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotBETE7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblCat8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTotCat8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotBETE8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblCat9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTotCat9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotBETE9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.Shape2 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGood)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBET)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalBETE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtQuantity,
            this.txtCategory,
            this.txtDescription,
            this.txtCost,
            this.txtGood,
            this.txtValue,
            this.txtBETE,
            this.txtBET,
            this.Line1,
            this.Line3,
            this.Line5,
            this.Line6,
            this.Line18,
            this.Line19,
            this.Line20,
            this.Line25,
            this.txtYear});
            this.detail.Height = 0.1875F;
            this.detail.Name = "detail";
            // 
            // txtQuantity
            // 
            this.txtQuantity.Height = 0.1875F;
            this.txtQuantity.Left = 0.05F;
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.Text = null;
            this.txtQuantity.Top = 0F;
            this.txtQuantity.Width = 0.4270833F;
            // 
            // txtCategory
            // 
            this.txtCategory.Height = 0.1875F;
            this.txtCategory.Left = 0.51875F;
            this.txtCategory.Name = "txtCategory";
            this.txtCategory.Text = null;
            this.txtCategory.Top = 0F;
            this.txtCategory.Width = 0.4270833F;
            // 
            // txtDescription
            // 
            this.txtDescription.CanGrow = false;
            this.txtDescription.Height = 0.1875F;
            this.txtDescription.Left = 0.9666666F;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Style = "white-space: nowrap";
            this.txtDescription.Text = null;
            this.txtDescription.Top = 0F;
            this.txtDescription.Width = 2.322917F;
            // 
            // txtCost
            // 
            this.txtCost.Height = 0.1875F;
            this.txtCost.Left = 3.727083F;
            this.txtCost.Name = "txtCost";
            this.txtCost.Style = "text-align: right";
            this.txtCost.Text = null;
            this.txtCost.Top = 0F;
            this.txtCost.Width = 0.875F;
            // 
            // txtGood
            // 
            this.txtGood.Height = 0.1875F;
            this.txtGood.Left = 4.643751F;
            this.txtGood.Name = "txtGood";
            this.txtGood.Style = "text-align: right";
            this.txtGood.Text = null;
            this.txtGood.Top = 0F;
            this.txtGood.Width = 0.3645833F;
            // 
            // txtValue
            // 
            this.txtValue.Height = 0.1875F;
            this.txtValue.Left = 5.050001F;
            this.txtValue.Name = "txtValue";
            this.txtValue.Style = "text-align: right";
            this.txtValue.Text = null;
            this.txtValue.Top = 0F;
            this.txtValue.Width = 0.9895833F;
            // 
            // txtBETE
            // 
            this.txtBETE.Height = 0.1875F;
            this.txtBETE.Left = 6.091668F;
            this.txtBETE.Name = "txtBETE";
            this.txtBETE.Style = "text-align: right";
            this.txtBETE.Text = null;
            this.txtBETE.Top = 0F;
            this.txtBETE.Width = 0.9895833F;
            // 
            // txtBET
            // 
            this.txtBET.Height = 0.1875F;
            this.txtBET.Left = 7.112501F;
            this.txtBET.Name = "txtBET";
            this.txtBET.Style = "text-align: center";
            this.txtBET.Text = null;
            this.txtBET.Top = 0F;
            this.txtBET.Width = 0.3020833F;
            // 
            // Line1
            // 
            this.Line1.Height = 0.1875F;
            this.Line1.Left = 0.4979166F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0F;
            this.Line1.Width = 0F;
            this.Line1.X1 = 0.4979166F;
            this.Line1.X2 = 0.4979166F;
            this.Line1.Y1 = 0F;
            this.Line1.Y2 = 0.1875F;
            // 
            // Line3
            // 
            this.Line3.Height = 0.1875F;
            this.Line3.Left = 0.9493055F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0F;
            this.Line3.Width = 0F;
            this.Line3.X1 = 0.9493055F;
            this.Line3.X2 = 0.9493055F;
            this.Line3.Y1 = 0F;
            this.Line3.Y2 = 0.1875F;
            // 
            // Line5
            // 
            this.Line5.Height = 0.1875F;
            this.Line5.Left = 3.715F;
            this.Line5.LineWeight = 1F;
            this.Line5.Name = "Line5";
            this.Line5.Top = 0F;
            this.Line5.Width = 0F;
            this.Line5.X1 = 3.715F;
            this.Line5.X2 = 3.715F;
            this.Line5.Y1 = 0F;
            this.Line5.Y2 = 0.1875F;
            // 
            // Line6
            // 
            this.Line6.Height = 0.1875F;
            this.Line6.Left = 4.629862F;
            this.Line6.LineWeight = 1F;
            this.Line6.Name = "Line6";
            this.Line6.Top = 0F;
            this.Line6.Width = 0F;
            this.Line6.X1 = 4.629862F;
            this.Line6.X2 = 4.629862F;
            this.Line6.Y1 = 0F;
            this.Line6.Y2 = 0.1875F;
            // 
            // Line18
            // 
            this.Line18.Height = 0.1875F;
            this.Line18.Left = 5.03264F;
            this.Line18.LineWeight = 1F;
            this.Line18.Name = "Line18";
            this.Line18.Top = 0F;
            this.Line18.Width = 0F;
            this.Line18.X1 = 5.03264F;
            this.Line18.X2 = 5.03264F;
            this.Line18.Y1 = 0F;
            this.Line18.Y2 = 0.1875F;
            // 
            // Line19
            // 
            this.Line19.Height = 0.1875F;
            this.Line19.Left = 6.074307F;
            this.Line19.LineWeight = 1F;
            this.Line19.Name = "Line19";
            this.Line19.Top = 0F;
            this.Line19.Width = 0F;
            this.Line19.X1 = 6.074307F;
            this.Line19.X2 = 6.074307F;
            this.Line19.Y1 = 0F;
            this.Line19.Y2 = 0.1875F;
            // 
            // Line20
            // 
            this.Line20.Height = 0.1875F;
            this.Line20.Left = 7.091668F;
            this.Line20.LineWeight = 1F;
            this.Line20.Name = "Line20";
            this.Line20.Top = 0F;
            this.Line20.Width = 0F;
            this.Line20.X1 = 7.091668F;
            this.Line20.X2 = 7.091668F;
            this.Line20.Y1 = 0F;
            this.Line20.Y2 = 0.1875F;
            // 
            // Line25
            // 
            this.Line25.Height = 0.1875F;
            this.Line25.Left = 3.352083F;
            this.Line25.LineWeight = 1F;
            this.Line25.Name = "Line25";
            this.Line25.Top = 0F;
            this.Line25.Width = 0F;
            this.Line25.X1 = 3.352083F;
            this.Line25.X2 = 3.352083F;
            this.Line25.Y1 = 0F;
            this.Line25.Y2 = 0.1875F;
            // 
            // txtYear
            // 
            this.txtYear.Height = 0.1875F;
            this.txtYear.Left = 3.372917F;
            this.txtYear.Name = "txtYear";
            this.txtYear.Text = null;
            this.txtYear.Top = 0F;
            this.txtYear.Width = 0.3645833F;
            // 
            // reportHeader1
            // 
            this.reportHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label19});
            this.reportHeader1.Name = "reportHeader1";
            // 
            // Label19
            // 
            this.Label19.Height = 0.1875F;
            this.Label19.HyperLink = null;
            this.Label19.Left = 2.53125F;
            this.Label19.Name = "Label19";
            this.Label19.Style = "font-weight: bold";
            this.Label19.Text = "Leased";
            this.Label19.Top = 0.03125F;
            this.Label19.Width = 1.4375F;
            // 
            // reportFooter1
            // 
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtTotalCost,
            this.txtTotalValue,
            this.txtTotalBETE,
            this.Label20,
            this.Line22,
            this.Line23,
            this.lblCat1,
            this.txtTotCat1,
            this.txtTotBETE1,
            this.lblCat2,
            this.txtTotCat2,
            this.txtTotBETE2,
            this.lblCat3,
            this.txtTotCat3,
            this.txtTotBETE3,
            this.lblCat4,
            this.txtTotCat4,
            this.txtTotBETE4,
            this.lblCat5,
            this.txtTotCat5,
            this.txtTotBETE5,
            this.lblCat6,
            this.txtTotCat6,
            this.txtTotBETE6,
            this.lblCat7,
            this.txtTotCat7,
            this.txtTotBETE7,
            this.lblCat8,
            this.txtTotCat8,
            this.txtTotBETE8,
            this.lblCat9,
            this.txtTotCat9,
            this.txtTotBETE9,
            this.Label21,
            this.Label22});
            this.reportFooter1.Height = 2.146F;
            this.reportFooter1.Name = "reportFooter1";
            // 
            // txtTotalCost
            // 
            this.txtTotalCost.Height = 0.1875F;
            this.txtTotalCost.Left = 3.260417F;
            this.txtTotalCost.Name = "txtTotalCost";
            this.txtTotalCost.Style = "text-align: right";
            this.txtTotalCost.Text = null;
            this.txtTotalCost.Top = 0.0625F;
            this.txtTotalCost.Width = 1.333333F;
            // 
            // txtTotalValue
            // 
            this.txtTotalValue.Height = 0.1875F;
            this.txtTotalValue.Left = 4.71875F;
            this.txtTotalValue.Name = "txtTotalValue";
            this.txtTotalValue.Style = "text-align: right";
            this.txtTotalValue.Text = null;
            this.txtTotalValue.Top = 0.0625F;
            this.txtTotalValue.Width = 1.322917F;
            // 
            // txtTotalBETE
            // 
            this.txtTotalBETE.Height = 0.1875F;
            this.txtTotalBETE.Left = 6.09375F;
            this.txtTotalBETE.Name = "txtTotalBETE";
            this.txtTotalBETE.Style = "text-align: right";
            this.txtTotalBETE.Text = null;
            this.txtTotalBETE.Top = 0.0625F;
            this.txtTotalBETE.Width = 0.9895833F;
            // 
            // Label20
            // 
            this.Label20.Height = 0.1875F;
            this.Label20.HyperLink = null;
            this.Label20.Left = 0.1666667F;
            this.Label20.Name = "Label20";
            this.Label20.Style = "font-weight: bold";
            this.Label20.Text = "Totals:";
            this.Label20.Top = 0.0625F;
            this.Label20.Width = 0.96875F;
            // 
            // Line22
            // 
            this.Line22.Height = 0F;
            this.Line22.Left = -9.313226E-09F;
            this.Line22.LineWeight = 1F;
            this.Line22.Name = "Line22";
            this.Line22.Top = 0F;
            this.Line22.Width = 7.4375F;
            this.Line22.X1 = -9.313226E-09F;
            this.Line22.X2 = 7.4375F;
            this.Line22.Y1 = 0F;
            this.Line22.Y2 = 0F;
            // 
            // Line23
            // 
            this.Line23.Height = 0F;
            this.Line23.Left = 3.260417F;
            this.Line23.LineWeight = 1F;
            this.Line23.Name = "Line23";
            this.Line23.Top = 0.3020833F;
            this.Line23.Width = 3.885416F;
            this.Line23.X1 = 3.260417F;
            this.Line23.X2 = 7.145833F;
            this.Line23.Y1 = 0.3020833F;
            this.Line23.Y2 = 0.3020833F;
            // 
            // lblCat1
            // 
            this.lblCat1.Height = 0.1666667F;
            this.lblCat1.HyperLink = null;
            this.lblCat1.Left = 2.583333F;
            this.lblCat1.Name = "lblCat1";
            this.lblCat1.Style = "font-weight: bold; text-align: right";
            this.lblCat1.Text = "Category 1";
            this.lblCat1.Top = 0.6041667F;
            this.lblCat1.Width = 2.416667F;
            // 
            // txtTotCat1
            // 
            this.txtTotCat1.Height = 0.1666667F;
            this.txtTotCat1.Left = 5.125F;
            this.txtTotCat1.Name = "txtTotCat1";
            this.txtTotCat1.Style = "text-align: right";
            this.txtTotCat1.Text = "0";
            this.txtTotCat1.Top = 0.6041667F;
            this.txtTotCat1.Width = 0.9166667F;
            // 
            // txtTotBETE1
            // 
            this.txtTotBETE1.Height = 0.1666667F;
            this.txtTotBETE1.Left = 6.166667F;
            this.txtTotBETE1.Name = "txtTotBETE1";
            this.txtTotBETE1.Style = "text-align: right";
            this.txtTotBETE1.Text = "0";
            this.txtTotBETE1.Top = 0.6041667F;
            this.txtTotBETE1.Width = 0.9166667F;
            // 
            // lblCat2
            // 
            this.lblCat2.Height = 0.1666667F;
            this.lblCat2.HyperLink = null;
            this.lblCat2.Left = 2.583333F;
            this.lblCat2.Name = "lblCat2";
            this.lblCat2.Style = "font-weight: bold; text-align: right";
            this.lblCat2.Text = "Category 2";
            this.lblCat2.Top = 0.7708333F;
            this.lblCat2.Width = 2.416667F;
            // 
            // txtTotCat2
            // 
            this.txtTotCat2.Height = 0.1666667F;
            this.txtTotCat2.Left = 5.125F;
            this.txtTotCat2.Name = "txtTotCat2";
            this.txtTotCat2.Style = "text-align: right";
            this.txtTotCat2.Text = "0";
            this.txtTotCat2.Top = 0.7708333F;
            this.txtTotCat2.Width = 0.9166667F;
            // 
            // txtTotBETE2
            // 
            this.txtTotBETE2.Height = 0.1666667F;
            this.txtTotBETE2.Left = 6.166667F;
            this.txtTotBETE2.Name = "txtTotBETE2";
            this.txtTotBETE2.Style = "text-align: right";
            this.txtTotBETE2.Text = "0";
            this.txtTotBETE2.Top = 0.7708333F;
            this.txtTotBETE2.Width = 0.9166667F;
            // 
            // lblCat3
            // 
            this.lblCat3.Height = 0.1666667F;
            this.lblCat3.HyperLink = null;
            this.lblCat3.Left = 2.583333F;
            this.lblCat3.Name = "lblCat3";
            this.lblCat3.Style = "font-weight: bold; text-align: right";
            this.lblCat3.Text = "Category 3";
            this.lblCat3.Top = 0.9375F;
            this.lblCat3.Width = 2.416667F;
            // 
            // txtTotCat3
            // 
            this.txtTotCat3.Height = 0.1666667F;
            this.txtTotCat3.Left = 5.125F;
            this.txtTotCat3.Name = "txtTotCat3";
            this.txtTotCat3.Style = "text-align: right";
            this.txtTotCat3.Text = "0";
            this.txtTotCat3.Top = 0.9375F;
            this.txtTotCat3.Width = 0.9166667F;
            // 
            // txtTotBETE3
            // 
            this.txtTotBETE3.Height = 0.1666667F;
            this.txtTotBETE3.Left = 6.166667F;
            this.txtTotBETE3.Name = "txtTotBETE3";
            this.txtTotBETE3.Style = "text-align: right";
            this.txtTotBETE3.Text = "0";
            this.txtTotBETE3.Top = 0.9375F;
            this.txtTotBETE3.Width = 0.9166667F;
            // 
            // lblCat4
            // 
            this.lblCat4.Height = 0.1666667F;
            this.lblCat4.HyperLink = null;
            this.lblCat4.Left = 2.583333F;
            this.lblCat4.Name = "lblCat4";
            this.lblCat4.Style = "font-weight: bold; text-align: right";
            this.lblCat4.Text = "Category 4";
            this.lblCat4.Top = 1.104167F;
            this.lblCat4.Width = 2.416667F;
            // 
            // txtTotCat4
            // 
            this.txtTotCat4.Height = 0.1666667F;
            this.txtTotCat4.Left = 5.125F;
            this.txtTotCat4.Name = "txtTotCat4";
            this.txtTotCat4.Style = "text-align: right";
            this.txtTotCat4.Text = "0";
            this.txtTotCat4.Top = 1.104167F;
            this.txtTotCat4.Width = 0.9166667F;
            // 
            // txtTotBETE4
            // 
            this.txtTotBETE4.Height = 0.1666667F;
            this.txtTotBETE4.Left = 6.166667F;
            this.txtTotBETE4.Name = "txtTotBETE4";
            this.txtTotBETE4.Style = "text-align: right";
            this.txtTotBETE4.Text = "0";
            this.txtTotBETE4.Top = 1.104167F;
            this.txtTotBETE4.Width = 0.9166667F;
            // 
            // lblCat5
            // 
            this.lblCat5.Height = 0.1666667F;
            this.lblCat5.HyperLink = null;
            this.lblCat5.Left = 2.583333F;
            this.lblCat5.Name = "lblCat5";
            this.lblCat5.Style = "font-weight: bold; text-align: right";
            this.lblCat5.Text = "Category 5";
            this.lblCat5.Top = 1.270833F;
            this.lblCat5.Width = 2.416667F;
            // 
            // txtTotCat5
            // 
            this.txtTotCat5.Height = 0.1666667F;
            this.txtTotCat5.Left = 5.125F;
            this.txtTotCat5.Name = "txtTotCat5";
            this.txtTotCat5.Style = "text-align: right";
            this.txtTotCat5.Text = "0";
            this.txtTotCat5.Top = 1.270833F;
            this.txtTotCat5.Width = 0.9166667F;
            // 
            // txtTotBETE5
            // 
            this.txtTotBETE5.Height = 0.1666667F;
            this.txtTotBETE5.Left = 6.166667F;
            this.txtTotBETE5.Name = "txtTotBETE5";
            this.txtTotBETE5.Style = "text-align: right";
            this.txtTotBETE5.Text = "0";
            this.txtTotBETE5.Top = 1.270833F;
            this.txtTotBETE5.Width = 0.9166667F;
            // 
            // lblCat6
            // 
            this.lblCat6.Height = 0.1666667F;
            this.lblCat6.HyperLink = null;
            this.lblCat6.Left = 2.583333F;
            this.lblCat6.Name = "lblCat6";
            this.lblCat6.Style = "font-weight: bold; text-align: right";
            this.lblCat6.Text = "Category 6";
            this.lblCat6.Top = 1.4375F;
            this.lblCat6.Width = 2.416667F;
            // 
            // txtTotCat6
            // 
            this.txtTotCat6.Height = 0.1666667F;
            this.txtTotCat6.Left = 5.125F;
            this.txtTotCat6.Name = "txtTotCat6";
            this.txtTotCat6.Style = "text-align: right";
            this.txtTotCat6.Text = "0";
            this.txtTotCat6.Top = 1.4375F;
            this.txtTotCat6.Width = 0.9166667F;
            // 
            // txtTotBETE6
            // 
            this.txtTotBETE6.Height = 0.1666667F;
            this.txtTotBETE6.Left = 6.166667F;
            this.txtTotBETE6.Name = "txtTotBETE6";
            this.txtTotBETE6.Style = "text-align: right";
            this.txtTotBETE6.Text = "0";
            this.txtTotBETE6.Top = 1.4375F;
            this.txtTotBETE6.Width = 0.9166667F;
            // 
            // lblCat7
            // 
            this.lblCat7.Height = 0.1666667F;
            this.lblCat7.HyperLink = null;
            this.lblCat7.Left = 2.583333F;
            this.lblCat7.Name = "lblCat7";
            this.lblCat7.Style = "font-weight: bold; text-align: right";
            this.lblCat7.Text = "Category 7";
            this.lblCat7.Top = 1.604167F;
            this.lblCat7.Width = 2.416667F;
            // 
            // txtTotCat7
            // 
            this.txtTotCat7.Height = 0.1666667F;
            this.txtTotCat7.Left = 5.125F;
            this.txtTotCat7.Name = "txtTotCat7";
            this.txtTotCat7.Style = "text-align: right";
            this.txtTotCat7.Text = "0";
            this.txtTotCat7.Top = 1.604167F;
            this.txtTotCat7.Width = 0.9166667F;
            // 
            // txtTotBETE7
            // 
            this.txtTotBETE7.Height = 0.1666667F;
            this.txtTotBETE7.Left = 6.166667F;
            this.txtTotBETE7.Name = "txtTotBETE7";
            this.txtTotBETE7.Style = "text-align: right";
            this.txtTotBETE7.Text = "0";
            this.txtTotBETE7.Top = 1.604167F;
            this.txtTotBETE7.Width = 0.9166667F;
            // 
            // lblCat8
            // 
            this.lblCat8.Height = 0.1666667F;
            this.lblCat8.HyperLink = null;
            this.lblCat8.Left = 2.583333F;
            this.lblCat8.Name = "lblCat8";
            this.lblCat8.Style = "font-weight: bold; text-align: right";
            this.lblCat8.Text = "Category 8";
            this.lblCat8.Top = 1.770833F;
            this.lblCat8.Width = 2.416667F;
            // 
            // txtTotCat8
            // 
            this.txtTotCat8.Height = 0.1666667F;
            this.txtTotCat8.Left = 5.125F;
            this.txtTotCat8.Name = "txtTotCat8";
            this.txtTotCat8.Style = "text-align: right";
            this.txtTotCat8.Text = "0";
            this.txtTotCat8.Top = 1.770833F;
            this.txtTotCat8.Width = 0.9166667F;
            // 
            // txtTotBETE8
            // 
            this.txtTotBETE8.Height = 0.1666667F;
            this.txtTotBETE8.Left = 6.166667F;
            this.txtTotBETE8.Name = "txtTotBETE8";
            this.txtTotBETE8.Style = "text-align: right";
            this.txtTotBETE8.Text = "0";
            this.txtTotBETE8.Top = 1.770833F;
            this.txtTotBETE8.Width = 0.9166667F;
            // 
            // lblCat9
            // 
            this.lblCat9.Height = 0.1666667F;
            this.lblCat9.HyperLink = null;
            this.lblCat9.Left = 2.583333F;
            this.lblCat9.Name = "lblCat9";
            this.lblCat9.Style = "font-weight: bold; text-align: right";
            this.lblCat9.Text = "Category 9";
            this.lblCat9.Top = 1.9375F;
            this.lblCat9.Width = 2.416667F;
            // 
            // txtTotCat9
            // 
            this.txtTotCat9.Height = 0.1666667F;
            this.txtTotCat9.Left = 5.125F;
            this.txtTotCat9.Name = "txtTotCat9";
            this.txtTotCat9.Style = "text-align: right";
            this.txtTotCat9.Text = "0";
            this.txtTotCat9.Top = 1.9375F;
            this.txtTotCat9.Width = 0.9166667F;
            // 
            // txtTotBETE9
            // 
            this.txtTotBETE9.Height = 0.1666667F;
            this.txtTotBETE9.Left = 6.166667F;
            this.txtTotBETE9.Name = "txtTotBETE9";
            this.txtTotBETE9.Style = "text-align: right";
            this.txtTotBETE9.Text = "0";
            this.txtTotBETE9.Top = 1.9375F;
            this.txtTotBETE9.Width = 0.9166667F;
            // 
            // Label21
            // 
            this.Label21.Height = 0.1875F;
            this.Label21.HyperLink = null;
            this.Label21.Left = 5.041667F;
            this.Label21.Name = "Label21";
            this.Label21.Style = "font-weight: bold; text-align: right";
            this.Label21.Text = "Values";
            this.Label21.Top = 0.40625F;
            this.Label21.Width = 1F;
            // 
            // Label22
            // 
            this.Label22.Height = 0.1875F;
            this.Label22.HyperLink = null;
            this.Label22.Left = 6.083333F;
            this.Label22.Name = "Label22";
            this.Label22.Style = "font-weight: bold; text-align: right";
            this.Label22.Text = "BETE Exempt";
            this.Label22.Top = 0.40625F;
            this.Label22.Width = 1F;
            // 
            // groupHeader1
            // 
            this.groupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Shape2,
            this.Label10,
            this.Label11,
            this.Label12,
            this.Label14,
            this.Label15,
            this.Label16,
            this.Label17,
            this.Label18,
            this.Line10,
            this.Line11,
            this.Line13,
            this.Line14,
            this.Line15,
            this.Line16,
            this.Line17,
            this.Label23,
            this.Line24});
            this.groupHeader1.Height = 0.3090278F;
            this.groupHeader1.Name = "groupHeader1";
            // 
            // Shape2
            // 
            this.Shape2.Height = 0.2291667F;
            this.Shape2.Left = -9.313226E-09F;
            this.Shape2.Name = "Shape2";
            this.Shape2.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape2.Top = -9.313226E-10F;
            this.Shape2.Width = 7.469F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.1875F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 0.05208332F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "font-weight: bold";
            this.Label10.Text = "Qty";
            this.Label10.Top = 0.02083333F;
            this.Label10.Width = 0.4270833F;
            // 
            // Label11
            // 
            this.Label11.Height = 0.1875F;
            this.Label11.HyperLink = null;
            this.Label11.Left = 0.96875F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "font-weight: bold";
            this.Label11.Text = "Description";
            this.Label11.Top = 0.02083333F;
            this.Label11.Width = 2.135417F;
            // 
            // Label12
            // 
            this.Label12.Height = 0.1875F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 0.5208334F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "font-weight: bold";
            this.Label12.Text = "Cat.";
            this.Label12.Top = 0.02083333F;
            this.Label12.Width = 0.4270833F;
            // 
            // Label14
            // 
            this.Label14.Height = 0.1875F;
            this.Label14.HyperLink = null;
            this.Label14.Left = 3.729167F;
            this.Label14.Name = "Label14";
            this.Label14.Style = "font-weight: bold; text-align: right";
            this.Label14.Text = "Cost";
            this.Label14.Top = 0.02083333F;
            this.Label14.Width = 0.875F;
            // 
            // Label15
            // 
            this.Label15.Height = 0.1875F;
            this.Label15.HyperLink = null;
            this.Label15.Left = 5.052083F;
            this.Label15.Name = "Label15";
            this.Label15.Style = "font-weight: bold; text-align: right";
            this.Label15.Text = "Value";
            this.Label15.Top = 0.02083333F;
            this.Label15.Width = 0.9895833F;
            // 
            // Label16
            // 
            this.Label16.Height = 0.1875F;
            this.Label16.HyperLink = null;
            this.Label16.Left = 6.073F;
            this.Label16.Name = "Label16";
            this.Label16.Style = "font-weight: bold; text-align: right";
            this.Label16.Text = "BETE Exempt";
            this.Label16.Top = 0.021F;
            this.Label16.Width = 0.9872501F;
            // 
            // Label17
            // 
            this.Label17.Height = 0.1875F;
            this.Label17.HyperLink = null;
            this.Label17.Left = 4.635F;
            this.Label17.Name = "Label17";
            this.Label17.Style = "font-weight: bold; text-align: right";
            this.Label17.Text = "Good";
            this.Label17.Top = 0.021F;
            this.Label17.Width = 0.3961673F;
            // 
            // Label18
            // 
            this.Label18.Height = 0.1875F;
            this.Label18.HyperLink = null;
            this.Label18.Left = 7.09F;
            this.Label18.Name = "Label18";
            this.Label18.Style = "font-weight: bold";
            this.Label18.Text = "BETE";
            this.Label18.Top = 0.021F;
            this.Label18.Width = 0.418F;
            // 
            // Line10
            // 
            this.Line10.Height = 0.309F;
            this.Line10.Left = 0.5F;
            this.Line10.LineWeight = 1F;
            this.Line10.Name = "Line10";
            this.Line10.Top = -9.313226E-10F;
            this.Line10.Width = 0F;
            this.Line10.X1 = 0.5F;
            this.Line10.X2 = 0.5F;
            this.Line10.Y1 = -9.313226E-10F;
            this.Line10.Y2 = 0.309F;
            // 
            // Line11
            // 
            this.Line11.Height = 0.309F;
            this.Line11.Left = 0.9513888F;
            this.Line11.LineWeight = 1F;
            this.Line11.Name = "Line11";
            this.Line11.Top = -9.313226E-10F;
            this.Line11.Width = 0F;
            this.Line11.X1 = 0.9513888F;
            this.Line11.X2 = 0.9513888F;
            this.Line11.Y1 = -9.313226E-10F;
            this.Line11.Y2 = 0.309F;
            // 
            // Line13
            // 
            this.Line13.Height = 0.3020833F;
            this.Line13.Left = 3.715278F;
            this.Line13.LineWeight = 1F;
            this.Line13.Name = "Line13";
            this.Line13.Top = -9.313226E-10F;
            this.Line13.Width = 0F;
            this.Line13.X1 = 3.715278F;
            this.Line13.X2 = 3.715278F;
            this.Line13.Y1 = -9.313226E-10F;
            this.Line13.Y2 = 0.3020833F;
            // 
            // Line14
            // 
            this.Line14.Height = 0.309F;
            this.Line14.Left = 4.631944F;
            this.Line14.LineWeight = 1F;
            this.Line14.Name = "Line14";
            this.Line14.Top = -9.313226E-10F;
            this.Line14.Width = 0F;
            this.Line14.X1 = 4.631944F;
            this.Line14.X2 = 4.631944F;
            this.Line14.Y1 = -9.313226E-10F;
            this.Line14.Y2 = 0.309F;
            // 
            // Line15
            // 
            this.Line15.Height = 0.309F;
            this.Line15.Left = 5.034722F;
            this.Line15.LineWeight = 1F;
            this.Line15.Name = "Line15";
            this.Line15.Top = -9.313226E-10F;
            this.Line15.Width = 0F;
            this.Line15.X1 = 5.034722F;
            this.Line15.X2 = 5.034722F;
            this.Line15.Y1 = -9.313226E-10F;
            this.Line15.Y2 = 0.309F;
            // 
            // Line16
            // 
            this.Line16.Height = 0.309F;
            this.Line16.Left = 6.076389F;
            this.Line16.LineWeight = 1F;
            this.Line16.Name = "Line16";
            this.Line16.Top = -9.313226E-10F;
            this.Line16.Width = 0F;
            this.Line16.X1 = 6.076389F;
            this.Line16.X2 = 6.076389F;
            this.Line16.Y1 = -9.313226E-10F;
            this.Line16.Y2 = 0.309F;
            // 
            // Line17
            // 
            this.Line17.Height = 0.309F;
            this.Line17.Left = 7.09375F;
            this.Line17.LineWeight = 1F;
            this.Line17.Name = "Line17";
            this.Line17.Top = -9.313226E-10F;
            this.Line17.Width = 0F;
            this.Line17.X1 = 7.09375F;
            this.Line17.X2 = 7.09375F;
            this.Line17.Y1 = -9.313226E-10F;
            this.Line17.Y2 = 0.309F;
            // 
            // Label23
            // 
            this.Label23.Height = 0.1875F;
            this.Label23.HyperLink = null;
            this.Label23.Left = 3.375F;
            this.Label23.Name = "Label23";
            this.Label23.Style = "font-weight: bold";
            this.Label23.Text = "Year";
            this.Label23.Top = 0.02083333F;
            this.Label23.Width = 0.3645833F;
            // 
            // Line24
            // 
            this.Line24.Height = 0.309F;
            this.Line24.Left = 3.354167F;
            this.Line24.LineWeight = 1F;
            this.Line24.Name = "Line24";
            this.Line24.Top = -9.313226E-10F;
            this.Line24.Width = 0F;
            this.Line24.X1 = 3.354167F;
            this.Line24.X2 = 3.354167F;
            this.Line24.Y1 = -9.313226E-10F;
            this.Line24.Y2 = 0.309F;
            // 
            // groupFooter1
            // 
            this.groupFooter1.Height = 0F;
            this.groupFooter1.Name = "groupFooter1";
            // 
            // srptLeasedInventory
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.498F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter1);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
            "l; font-size: 10pt; color: Black", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
            "lic", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGood)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBETE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBET)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalBETE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCat9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotCat9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotBETE9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQuantity;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCategory;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCost;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGood;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBETE;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBET;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line18;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line19;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line20;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line25;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalCost;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalValue;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalBETE;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line22;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line23;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCat1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotCat1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotBETE1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCat2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotCat2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotBETE2;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCat3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotCat3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotBETE3;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCat4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotCat4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotBETE4;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCat5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotCat5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotBETE5;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCat6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotCat6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotBETE6;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCat7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotCat7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotBETE7;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCat8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotCat8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotBETE8;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCat9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotCat9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotBETE9;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.Shape Shape2;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line14;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line15;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line16;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line17;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line24;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
    }
}
