﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmPPAuditBilling.
	/// </summary>
	public partial class frmPPAuditBilling : BaseForm
	{
		public frmPPAuditBilling()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPPAuditBilling InstancePtr
		{
			get
			{
				return (frmPPAuditBilling)Sys.GetInstance(typeof(frmPPAuditBilling));
			}
		}

		protected frmPPAuditBilling _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int NumCopies;
		int[] PPCategory = new int[9 + 1];
		int TotalPPExempt;
		int TotalPPValue;
		int PageNumber;
		string strSQL = "";
		string LowRange;
		string HighRange;
		string HoldBreak = "";
		string PrintAddress = "";
		int BreakLength;
		string DetailCode = "";
		string DisplayPrint = "";
		int LineCount;
		int PageCount;
		int LinesOnPage;
		bool boolExit;

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			string strWhere;
			string strMasterJoin;
			strMasterJoin = modPPGN.GetMasterJoinForJoin();
			boolExit = false;
			strWhere = RangeSelected();
			if (boolExit)
				return;
			// If strSQL = vbNullString Then
			// If optDetails(1).Value Then
			// strSQL = "select sum(category1) as cat1,sum(category2) as cat2,sum(category3) as cat3,sum(category4) as cat4,sum(category5) as cat5,sum(category6) as cat6,sum(category7) as cat7,sum(category8) as cat8,sum(category9) as cat9 "
			// strSQL = strSQL & " from ppmaster inner join ppvaluations on ppvaluations.valuekey = ppmaster.account where not ppmaster.deleted and orcode & '' <> 'Y' "
			// Else
			if (modPPGN.Statics.Sequence == "A")
			{
				if (cmbDetails.SelectedIndex == 1)
				{
					strSQL = "select sum(category1) as cat1,sum(category2) as cat2,sum(category3) as cat3,sum(category4) as cat4,sum(category5) as cat5,sum(category6) as cat6,sum(category7) as cat7,sum(category8) as cat8,sum(category9) as cat9 ,sum(cat1exempt) as cat1exempttot,sum(cat2exempt) as cat2exempttot,sum(cat3exempt) as cat3exempttot,sum(cat4exempt) as cat4exempttot,sum(cat5exempt) as cat5exempttot,sum(cat6exempt) as cat6exempttot,sum(cat7exempt) as cat7exempttot,sum(cat8exempt) as cat8exempttot,sum(cat9exempt) as cat9exempttot ";
					// strSQL = strSQL & " from ppmaster inner join ppvaluations on ppvaluations.valuekey = ppmaster.account where not ppmaster.deleted = 1 and orcode & '' <> 'Y' " & strWhere '
					strSQL += " from " + strMasterJoin + " inner join ppvaluations on ppvaluations.valuekey = mj.account where not mj.deleted = 1 and orcode  <> 'Y' " + strWhere;
				}
				else
				{
					// strSQL = "SELECT PPMaster.*, PPValuations.* FROM PPMaster INNER JOIN PPValuations ON PPValuations.ValueKey = PPMaster.Account where ppmaster.account > 0 and PPMaster.Deleted = 0 " & strWhere & " Order by ValueKey"
					strSQL = "SELECT mj.*, PPValuations.* FROM " + strMasterJoin + " INNER JOIN PPValuations ON PPValuations.ValueKey = mj.Account where mj.account > 0 and mj.Deleted = 0 " + strWhere + " Order by ValueKey";
				}
			}
			else if (modPPGN.Statics.Sequence == "N")
			{
				// If optDetails(1).Value Then
				// 
				// Else
				// strSQL = "SELECT PPMaster.*, PPValuations.* FROM PPMaster INNER JOIN PPValuations ON PPValuations.ValueKey = PPMaster.Account where ppmaster.account > 0 and PPMaster.Deleted = 0 " & strWhere & " Order by Name"
				strSQL = "SELECT mj.*, PPValuations.* FROM " + strMasterJoin + " INNER JOIN PPValuations ON PPValuations.ValueKey = mj.Account where mj.account > 0 and mj.Deleted = 0 " + strWhere + " Order by Name";
				// End If
			}
			else if (modPPGN.Statics.Sequence == "L")
			{
				// strSQL = "SELECT PPMaster.*, PPValuations.* FROM PPMaster INNER JOIN PPValuations ON PPValuations.ValueKey = PPMaster.Account where ppmaster.account > 0 and PPMaster.Deleted = 0 " & strWhere & " Order by Street"
				strSQL = "SELECT mj.*, PPValuations.* FROM " + strMasterJoin + " INNER JOIN PPValuations ON PPValuations.ValueKey = mj.Account where mj.account > 0 and mj.Deleted = 0 " + strWhere + " Order by Street";
			}
			else if (modPPGN.Statics.Sequence == "1")
			{
				// strSQL = "SELECT PPMaster.*, PPValuations.* FROM PPMaster INNER JOIN PPValuations ON PPValuations.ValueKey = PPMaster.Account where ppmaster.account > 0 and PPMaster.Deleted = 0 " & strWhere & " Order by open1"
				strSQL = "SELECT mj.*, PPValuations.* FROM " + strMasterJoin + " INNER JOIN PPValuations ON PPValuations.ValueKey = mj.Account where mj.account > 0 and mj.Deleted = 0 " + strWhere + " Order by open1";
			}
			else if (modPPGN.Statics.Sequence == "2")
			{
				// strSQL = "SELECT PPMaster.*, PPValuations.* FROM PPMaster INNER JOIN PPValuations ON PPValuations.ValueKey = PPMaster.Account where ppmaster.account > 0 and PPMaster.Deleted = 0 " & strWhere & " Order by open2"
				strSQL = "SELECT mj.*, PPValuations.* FROM " + strMasterJoin + " INNER JOIN PPValuations ON PPValuations.ValueKey = mj.Account where mj.account > 0 and mj.Deleted = 0 " + strWhere + " Order by open2";
			}
			else if (modPPGN.Statics.Sequence == "T")
			{
				// strSQL = "select ppmaster.*,ppvaluations.* from ppmaster inner join ppvaluations on ppvaluations.valuekey = ppmaster.account where ppmaster.account > 0 and not ppmaster.deleted = 1 " & strWhere & " order by trancode,name"
				strSQL = "select mj.*,ppvaluations.* from " + strMasterJoin + " inner join ppvaluations on ppvaluations.valuekey = mj.account where mj.account > 0 and not mj.deleted = 1 " + strWhere + " order by trancode,name";
			}
			// End If
			// End If
			modPrinting.Statics.rsAudit.OpenRecordset(strSQL, modPPGN.strPPDatabase);
			if (modPrinting.Statics.rsAudit.EndOfFile())
			{
				MessageBox.Show("No matches found.", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			modPrinting.Statics.rsAudit.MoveLast();
			modPrinting.Statics.rsAudit.MoveFirst();
			if (modGlobal.Statics.DetailSummary == string.Empty)
				modGlobal.Statics.DetailSummary = "D";
			if (DetailCode == "")
				DetailCode = "D";
			modGlobal.Statics.BreakDown = "N";
			// If DetailCode = "D" Or DetailCode = "B" Then
			if (modGlobal.Statics.DetailSummary == "D" || modGlobal.Statics.DetailSummary == "B")
			{
				//FC:FINAL:MSH - I.Issue #820: converting to int is incorrect, because BreakDown will has converted to int DialogResult 
				// and will be compared with string results
				//modGlobal.Statics.BreakDown = FCConvert.ToString(FCConvert.ToInt32(MessageBox.Show("Do you want a Category Breakdown for Each Account?", "Breakdown", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question));
				modGlobal.Statics.BreakDown = FCConvert.ToString(MessageBox.Show("Do you want a Category Breakdown for Each Account?", "Breakdown", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question));
				if (modGlobal.Statics.BreakDown == FCConvert.ToString(DialogResult.Cancel))
				{
					modGlobal.Statics.BreakDown = "";
					return;
				}
				if (modGlobal.Statics.BreakDown == FCConvert.ToString(DialogResult.Yes))
				{
					modGlobal.Statics.BreakDown = "Y";
				}
				else if (modGlobal.Statics.BreakDown == FCConvert.ToString(DialogResult.No))
				{
					modGlobal.Statics.BreakDown = "N";
				}
			}
			// If DetailCode = "S" Or DisplayPrint = "D" Then
			// PrintAddress = MsgBox("Do you want the Mailing Address to Print?", vbYesNo, "Print Address")
			// If PrintAddress = vbYes Then PrintAddress = "Y"
			// If PrintAddress = vbNo Then PrintAddress = "N"
			// End If
			//FC:FINAL:MSH - I.Issue #820: missed report call
			rptAuditReport.InstancePtr.Init();
			ErrorHandlingRoutine:
			;
			// User pressed Cancel button.
			// Printer.KillDoc
			return;
		}

		public void cmdPrint_Click()
		{
			cmdPrint_Click(cmdPrint, new System.EventArgs());
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			// menuPPCalculate.Show
			frmPPAuditBilling.InstancePtr.Close();
		}

		public void cmdQuit_Click()
		{
			//cmdQuit_Click(cmdQuit, new System.EventArgs());
		}

		private void frmPPAuditBilling_Activated(object sender, System.EventArgs e)
		{
			// lblWMuniName.Caption = "- " & MuniName & " -"
			// lblScreenTitle.Caption = "Print Account Listing"
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			// lblTime.Caption = Format(Time, "h:mm")
		}

		private void frmPPAuditBilling_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmPPAuditBilling_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				frmPPAuditBilling.InstancePtr.Close();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmPPAuditBilling_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPPAuditBilling properties;
			//frmPPAuditBilling.ScaleWidth	= 9045;
			//frmPPAuditBilling.ScaleHeight	= 7560;
			//frmPPAuditBilling.LinkTopic	= "Form1";
			//frmPPAuditBilling.LockControls	= true;
			//End Unmaped Properties
			clsDRWrapper clsTemp = new clsDRWrapper();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			clsTemp.OpenRecordset("SELECT * from ppratioopens", "twpp0000.vb1");
			if (Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("openfield1"))) != string.Empty)
			{
				if (cmbSequence.Items.Contains("Open 1"))
				{
					cmbSequence.Items.Remove("Open 1");
					cmbSequence.Items.Insert(3, Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("openfield1"))));
				}
				else if (cmbSequence.Items.Contains(""))
				{
					cmbSequence.Items.Remove("");
					cmbSequence.Items.Insert(3, Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("openfield1"))));
				}
			}
			else
			{
				if (cmbSequence.Items.Contains("Open 1"))
				{
				}
				else if (cmbSequence.Items.Contains(""))
				{
					cmbSequence.Items.Insert(3, "Open 1");
				}
			}
			if (Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("openfield2"))) != string.Empty)
			{
				if (cmbSequence.Items.Contains("Open 2"))
				{
					cmbSequence.Items.Remove("Open 2");
					cmbSequence.Items.Insert(4, Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("openfield2"))));
				}
				else if (cmbSequence.Items.Contains(""))
				{
					cmbSequence.Items.Remove("");
					cmbSequence.Items.Insert(4, Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("openfield2"))));
				}
			}
			else
			{
				if (cmbSequence.Items.Contains("Open 2"))
				{
				}
				else if (cmbSequence.Items.Contains(""))
				{
					cmbSequence.Items.Insert(4, "Open 2");
				}
			}
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			if (this.WindowState != FormWindowState.Minimized)
				modGlobal.SaveWindowSize(this);
			// menuPPCalculate.Show
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdQuit_Click();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			cmdPrint_Click();
		}

		private void optDetails_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (Index == 0)
			{
				modGlobal.Statics.DetailSummary = "D";
			}
			else if (Index == 1)
			{
				modGlobal.Statics.DetailSummary = "F";
			}
			else
			{
				modGlobal.Statics.DetailSummary = "B";
			}
		}

		private void optDetails_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbDetails.SelectedIndex;
			optDetails_CheckedChanged(index, sender, e);
		}

		private void optSequence_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
					{
						modPPGN.Statics.Sequence = "A";
						modPPGN.Statics.SequenceTitle = "Account";
						if (cmbDetails.Items.Contains("Both"))
						{
							cmbDetails.Items.Remove("Both");
						}
						if (cmbDetails.Items.Contains("Subtotals"))
						{
							cmbDetails.Items.Insert(1, "Final Totals");
							cmbDetails.Items.Remove("Subtotals");
						}
						else if (!cmbDetails.Items.Contains("Final Totals"))
						{
							cmbDetails.Items.Insert(1, "Final Totals");
						}
						break;
					}
				case 1:
					{
						modPPGN.Statics.Sequence = "N";
						modPPGN.Statics.SequenceTitle = "Name";
						if (!cmbDetails.Items.Contains("Both"))
						{
							cmbDetails.Items.Insert(2, "Both");
						}
						if (cmbDetails.Items.Contains("Final Totals"))
						{
							cmbDetails.Items.Insert(1, "Subtotals");
							cmbDetails.Items.Remove("Final Totals");
						}
						else if (!cmbDetails.Items.Contains("Subtotals"))
						{
							cmbDetails.Items.Insert(1, "Subtotals");
						}
						break;
					}
				case 2:
					{
						modPPGN.Statics.Sequence = "L";
						modPPGN.Statics.SequenceTitle = "Location";
						if (!cmbDetails.Items.Contains("Both"))
						{
							cmbDetails.Items.Insert(2, "Both");
						}
						if (cmbDetails.Items.Contains("Final Totals"))
						{
							cmbDetails.Items.Insert(1, "Subtotals");
							cmbDetails.Items.Remove("Final Totals");
						}
						else if (!cmbDetails.Items.Contains("Subtotals"))
						{
							cmbDetails.Items.Insert(1, "Subtotals");
						}
						break;
					}
				case 3:
					{
						modPPGN.Statics.Sequence = "1";
						modPPGN.Statics.SequenceTitle = "Open1";
						if (!cmbDetails.Items.Contains("Both"))
						{
							cmbDetails.Items.Insert(2, "Both");
						}
						if (cmbDetails.Items.Contains("Final Totals"))
						{
							cmbDetails.Items.Insert(1, "Subtotals");
							cmbDetails.Items.Remove("Final Totals");
						}
						else if (!cmbDetails.Items.Contains("Subtotals"))
						{
							cmbDetails.Items.Insert(1, "Subtotals");
						}
						break;
					}
				case 4:
					{
						modPPGN.Statics.Sequence = "2";
						modPPGN.Statics.SequenceTitle = "Open2";
						if (!cmbDetails.Items.Contains("Both"))
						{
							cmbDetails.Items.Insert(2, "Both");
						}
						if (cmbDetails.Items.Contains("Final Totals"))
						{
							cmbDetails.Items.Insert(1, "Subtotals");
							cmbDetails.Items.Remove("Final Totals");
						}
						else if (!cmbDetails.Items.Contains("Subtotals"))
						{
							cmbDetails.Items.Insert(1, "Subtotals");
						}
						break;
					}
				case 5:
					{
						modPPGN.Statics.Sequence = "T";
						modPPGN.Statics.SequenceTitle = "Tran Code";
						if (!cmbDetails.Items.Contains("Both"))
						{
							cmbDetails.Items.Insert(2, "Both");
						}
						if (cmbDetails.Items.Contains("Final Totals"))
						{
							cmbDetails.Items.Insert(1, "Subtotals");
							cmbDetails.Items.Remove("Final Totals");
						}
						else if (!cmbDetails.Items.Contains("Subtotals"))
						{
							cmbDetails.Items.Insert(1, "Subtotals");
						}
						break;
					}
			}
			//end switch
			lblStarting.Text = "Starting " + modPPGN.Statics.SequenceTitle;
			lblEnding.Text = "Ending " + modPPGN.Statics.SequenceTitle;
		}

		private void optSequence_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbSequence.SelectedIndex;
			optSequence_CheckedChanged(index, sender, e);
		}

		private void AddPPCategories()
		{
			int I;
			TotalPPValue = 0;
			for (I = 1; I <= 9; I++)
			{
				TotalPPValue += PPCategory[I];
			}
			// I
		}

		private string RangeSelected()
		{
			string RangeSelected = "";
			// Set SQL statement to include range
			LowRange = txtStarting.Text;
			HighRange = txtEnding.Text;
			modPPGN.Statics.gstrMinAccountRange = txtStarting.Text;
			modPPGN.Statics.gstrMaxAccountRange = txtEnding.Text;
			if (modPPGN.Statics.Sequence == "")
				modPPGN.Statics.Sequence = "A";
			if (Strings.Trim(txtStarting.Text) == "" || Strings.Trim(txtEnding.Text) == "")
			{
				RangeSelected = "";
				return RangeSelected;
			}
			if (modPPGN.Statics.Sequence == "A")
			{
				if (!Information.IsNumeric(txtStarting.Text))
				{
					MessageBox.Show("Invalid Starting number", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					boolExit = true;
					txtStarting.Focus();
					return RangeSelected;
				}
				if (!Information.IsNumeric(txtEnding.Text))
				{
					MessageBox.Show("Invalid Ending number", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					boolExit = true;
					txtEnding.Focus();
					return RangeSelected;
				}
				// If optDetails(1).Value Then
				// strSQL = "select sum(category1) as cat1,sum(category2) as cat2,sum(category3) as cat3,sum(category4) as cat4,sum(category5) as cat5,sum(category6) as cat6,sum(category7) as cat7,sum(category8) as cat8,sum(category9) as cat9 "
				// strSQL = strSQL & " from ppmaster inner join ppvaluations on ppvaluations.valuekey = ppmaster.account where not ppmaster.deleted and orcode & '' <> 'Y' and valuekey between " & txtStarting & " and " & txtEnding
				// Else
				// strSQL = "SELECT PPMaster.*, PPValuations.* FROM PPMaster INNER JOIN PPValuations ON PPValuations.ValueKey = PPMaster.Account WHERE not ppmaster.deleted and  ValueKey BETWEEN " & txtStarting & " and " & txtEnding & " ORDER BY [ValueKey]"
				// End If
				RangeSelected = " and valuekey between " + FCConvert.ToString(Conversion.Val(txtStarting.Text)) + " and " + FCConvert.ToString(Conversion.Val(txtEnding.Text)) + " ";
			}
			else if (modPPGN.Statics.Sequence == "N")
			{
				// If optDetails(1).Value Then
				// strSQL = "select sum(category1) as cat1,sum(category2) as cat2,sum(category3) as cat3,sum(category4) as cat4,sum(category5) as cat5,sum(category6) as cat6,sum(category7) as cat7,sum(category8) as cat8,sum(category9) as cat9 "
				// strSQL = strSQL & " from ppmaster inner join ppvaluations on ppvaluations.valuekey = ppmaster.account where not ppmaster.deleted and orcode & '' <> 'Y' and name between '" & txtStarting & "' and '" & txtEnding & "'"
				// Else
				// strSQL = "SELECT PPMaster.*, PPValuations.* FROM PPMaster INNER JOIN PPValuations ON PPValuations.ValueKey = PPMaster.Account WHERE not ppmaster.deleted and Name BETWEEN '" & txtStarting & "' and '" & txtEnding & "' ORDER BY [Name]"
				// End If
				RangeSelected = " and name between '" + txtStarting.Text + "' and '" + txtEnding.Text + "zzz' ";
			}
			else if (modPPGN.Statics.Sequence == "L")
			{
				// If optDetails(1).Value Then
				// strSQL = "select sum(category1) as cat1,sum(category2) as cat2,sum(category3) as cat3,sum(category4) as cat4,sum(category5) as cat5,sum(category6) as cat6,sum(category7) as cat7,sum(category8) as cat8,sum(category9) as cat9 "
				// strSQL = strSQL & " from ppmaster inner join ppvaluations on ppvaluations.valuekey = ppmaster.account where not ppmaster.deleted and orcode & '' <> 'Y' and street between '" & txtStarting & "' and '" & txtEnding & "'"
				// Else
				// strSQL = "SELECT PPMaster.*, PPValuations.* FROM PPMaster INNER JOIN PPValuations ON PPValuations.ValueKey = PPMaster.Account WHERE not ppmaster.deleted and Street BETWEEN '" & txtStarting & "' and '" & txtEnding & "' ORDER BY [Street]"
				// End If
				RangeSelected = " and street between '" + txtStarting.Text + "' and '" + txtEnding.Text + "zzz' ";
			}
			else if (modPPGN.Statics.Sequence == "1")
			{
				// If optDetails(1).Value Then
				// strSQL = "select sum(category1) as cat1,sum(category2) as cat2,sum(category3) as cat3,sum(category4) as cat4,sum(category5) as cat5,sum(category6) as cat6,sum(category7) as cat7,sum(category8) as cat8,sum(category9) as cat9 "
				// strSQL = strSQL & " from ppmaster inner join ppvaluations on ppvaluations.valuekey = ppmaster.account where not ppmaster.deleted and orcode & '' <> 'Y' and open1 between '" & txtStarting & "' and '" & txtEnding & "'"
				// Else
				// strSQL = "SELECT PPMASTER.*, ppvaluations.* FROM ppmaster inner join PPValuations on ppvaluations.valuekey = ppmaster.account WHERE not ppmaster.deleted and Open1 BETWEEN '" & LowRange & "' and '" & HighRange & "' ORDER BY [Open1]"
				// End If
				RangeSelected = " and open1 between '" + txtStarting.Text + "' and '" + txtEnding.Text + "zzz' ";
			}
			else if (modPPGN.Statics.Sequence == "2")
			{
				// If optDetails(1).Value Then
				// strSQL = "select sum(category1) as cat1,sum(category2) as cat2,sum(category3) as cat3,sum(category4) as cat4,sum(category5) as cat5,sum(category6) as cat6,sum(category7) as cat7,sum(category8) as cat8,sum(category9) as cat9 "
				// strSQL = strSQL & " from ppmaster inner join ppvaluations on ppvaluations.valuekey = ppmaster.account where not ppmaster.deleted and orcode & '' <> 'Y' and open2 between '" & txtStarting & "' and '" & txtEnding & "'"
				// Else
				// strSQL = "SELECT ppmaster.*, ppvaluations.* FROM ppmaster inner join PPValuations on ppvaluations.valuekey = ppmaster.account WHERE not ppmaster.deleted and Open2 BETWEEN '" & LowRange & "' and '" & HighRange & "' ORDER BY [Open2]"
				// End If
				RangeSelected = " and open2 between '" + txtStarting.Text + "' and '" + txtEnding.Text + "zzz' ";
			}
			else if (modPPGN.Statics.Sequence == "T")
			{
				RangeSelected = " and trancode between " + txtStarting.Text + " and " + txtEnding.Text + " ";
			}
			return RangeSelected;
		}
	}
}
