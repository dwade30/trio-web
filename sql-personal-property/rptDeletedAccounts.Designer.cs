﻿namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptDeletedAccounts.
	/// </summary>
	partial class rptDeletedAccounts
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptDeletedAccounts));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.ColumnCount = 2;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAccount,
				this.txtName
			});
			this.Detail.Height = 0.25F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.1875F;
			this.txtAccount.Left = 0.0625F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-family: \'Tahoma\'";
			this.txtAccount.Text = "Field1";
			this.txtAccount.Top = 0.03125F;
			this.txtAccount.Width = 0.6875F;
			// 
			// txtName
			// 
			this.txtName.CanGrow = false;
			this.txtName.Height = 0.1875F;
			this.txtName.Left = 0.875F;
			this.txtName.MultiLine = false;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-family: \'Tahoma\'";
			this.txtName.Text = "Field2";
			this.txtName.Top = 0.03125F;
			this.txtName.Width = 2.6875F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.txtPage,
				this.txtMuni,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.txtDate,
				this.txtTime
			});
			this.PageHeader.Height = 0.7708333F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// Label1
			// 
			this.Label1.Height = 0.218F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.Label1.Text = "Personal Property Deleted Accounts Report";
			this.Label1.Top = 0.03125F;
			this.Label1.Width = 4.5F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 6.5F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtPage.Text = "Field1";
			this.txtPage.Top = 0.1875F;
			this.txtPage.Width = 0.9375F;
			// 
			// txtMuni
			// 
			this.txtMuni.Height = 0.1875F;
			this.txtMuni.Left = 0.0625F;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Style = "font-family: \'Tahoma\'";
			this.txtMuni.Text = "Field1";
			this.txtMuni.Top = 0.03125F;
			this.txtMuni.Width = 1.4375F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.0625F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label2.Text = "Account";
			this.Label2.Top = 0.5625F;
			this.Label2.Width = 0.75F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0.875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label3.Text = "Name";
			this.Label3.Top = 0.5625F;
			this.Label3.Width = 1F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.19F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 3.8125F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label4.Text = "Account";
			this.Label4.Top = 0.59375F;
			this.Label4.Width = 0.75F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.19F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 4.625F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label5.Text = "Name";
			this.Label5.Top = 0.59375F;
			this.Label5.Width = 1.0625F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.HyperLink = null;
			this.txtDate.Left = 6.4375F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtDate.Text = "Name";
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0.0625F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'";
			this.txtTime.Text = "Field1";
			this.txtTime.Top = 0.21875F;
			this.txtTime.Width = 1.4375F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// rptDeletedAccounts
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.489583F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
