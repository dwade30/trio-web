﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPP0000
{
	public class cPPAccountController
	{
		//=========================================================
		private string strConnectionName = string.Empty;
		private string strLastError = string.Empty;
		private int lngLastError;

		private void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public cPPAccountController() : base()
		{
			strConnectionName = "PersonalProperty";
		}

		public string ConnectionName
		{
			set
			{
				strConnectionName = value;
			}
			get
			{
				string ConnectionName = "";
				ConnectionName = strConnectionName;
				return ConnectionName;
			}
		}

		public bool LoadAccount(ref cPPAccount tAccount, int lngAccountNumber)
		{
			bool LoadAccount = false;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				LoadAccount = false;
				if (tAccount == null)
				{
					tAccount = new cPPAccount();
				}
				tAccount.Clear();
				if (lngAccountNumber > 0)
				{
					clsDRWrapper rsLoad = new clsDRWrapper();
					int x;
					rsLoad.OpenRecordset("select * from ppmaster where account = " + FCConvert.ToString(lngAccountNumber) + " and account > 0", modPPGN.strPPDatabase);
					if (!rsLoad.EndOfFile())
					{
						tAccount.Account = lngAccountNumber;
						tAccount.ID = rsLoad.Get_Fields_Int32("id");
						//FC:FINALL:MSH - i.issue #1277: can't implicitly convert double to int
						//tAccount.PartyID = Conversion.Val(rsLoad.Get_Fields("PartyID")));
						tAccount.PartyID = FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields_Int32("PartyID")));
						if (tAccount.PartyID > 0)
						{
							cPartyController tCont = new cPartyController();
							if (tAccount.PartyID > 0)
							{
								cParty tOwnerParty = tAccount.OwnerParty();
								tCont.LoadParty(ref tOwnerParty, tAccount.PartyID);
							}
						}
						//FC:FINALL:MSH - i.issue #1277: can't implicitly convert double to int
						//tAccount.BusinessCode = Conversion.Val(rsLoad.Get_Fields("businesscode")));
						tAccount.BusinessCode = FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields_Int32("businesscode")));
						tAccount.Comment = rsLoad.Get_Fields_String("comment");
						tAccount.CompValue = Conversion.Val(rsLoad.Get_Fields_Int32("compvalue"));
						tAccount.Deleted = rsLoad.Get_Fields_Boolean("deleted");
						//FC:FINALL:MSH - i.issue #1277: can't implicitly convert double to int
						//tAccount.Set_ExemptCode(1, Conversion.Val(rsLoad.Get_Fields("exemptcode1"))));
						//tAccount.Set_ExemptCode(2, Conversion.Val(rsLoad.Get_Fields("exemptcode2"))));
						// TODO Get_Fields: Check the table for the column [exemptcode1] and replace with corresponding Get_Field method
						tAccount.Set_ExemptCode(1, FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields("exemptcode1"))));
						// TODO Get_Fields: Check the table for the column [exemptcode2] and replace with corresponding Get_Field method
						tAccount.Set_ExemptCode(2, FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields("exemptcode2"))));
						tAccount.Exemption = Conversion.Val(rsLoad.Get_Fields_Int32("Exemption"));
						tAccount.Set_ExemptVal(1, Conversion.Val(rsLoad.Get_Fields_Int32("exemption1")));
						tAccount.Set_ExemptVal(2, Conversion.Val(rsLoad.Get_Fields_Int32("exemption2")));
						//FC:FINALL:MSH - i.issue #1277: can't implicitly convert to int
						//tAccount.ID = rsLoad.Get_Fields("ID");
						tAccount.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
						tAccount.Open1 = rsLoad.Get_Fields_String("open1");
						tAccount.Open2 = rsLoad.Get_Fields_String("open2");
						tAccount.ORCode = rsLoad.Get_Fields_String("orcode");
						tAccount.RBCode = rsLoad.Get_Fields_String("rbcode");
						//FC:FINALL:MSH - i.issue #1277: can't implicitly convert double to int
						//tAccount.RealAssoc = Conversion.Val(rsLoad.Get_Fields("realassoc")));
						tAccount.RealAssoc = FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields_Int32("realassoc")));
						//FC:FINALL:MSH - i.issue #1277: can't implicitly convert double to int
						//tAccount.SquareFootage = Conversion.Val(rsLoad.Get_Fields("squarefootage")));
						tAccount.SquareFootage = FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields_Int32("squarefootage")));
						tAccount.Street = rsLoad.Get_Fields_String("street");
						tAccount.StreetApt = rsLoad.Get_Fields_String("streetapt");
						//FC:FINALL:MSH - i.issue #1277: can't implicitly convert double to int
						//tAccount.StreetCode = Conversion.Val(rsLoad.Get_Fields("streetcode")));
						// TODO Get_Fields: Check the table for the column [streetcode] and replace with corresponding Get_Field method
						tAccount.StreetCode = FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields("streetcode")));
						//FC:FINALL:MSH - i.issue #1277: can't implicitly convert double to int
						//tAccount.StreetNumber = Conversion.Val(rsLoad.Get_Fields("streetnumber")));
						// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
						tAccount.StreetNumber = FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields("streetnumber")));
						// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
						tAccount.TotValue = Conversion.Val(rsLoad.Get_Fields("value"));
						//FC:FINALL:MSH - i.issue #1277: can't implicitly convert double to int
						//tAccount.TranCode = Conversion.Val(rsLoad.Get_Fields("trancode")));
						// TODO Get_Fields: Check the table for the column [trancode] and replace with corresponding Get_Field method
						tAccount.TranCode = FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields("trancode")));
						tAccount.UPDCode = rsLoad.Get_Fields_String("updcode");
						tAccount.AccountID = rsLoad.Get_Fields_String("accountid");
					}
				}
				LoadAccount = true;
				return LoadAccount;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return LoadAccount;
		}

		public object SaveAccount(ref cPPAccount tAccount)
		{
			object SaveAccount = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from ppmaster where account = " + tAccount.Account + " and account > 0", strConnectionName);
				if (rsSave.EndOfFile())
				{
					int lngAcct = 0;
					lngAcct = tAccount.Account;
					if (lngAcct <= 0)
					{
						rsSave.Execute("insert into ppmaster (Account,AccountID) select isnull(max(isnull(account,0)) + 1,1) as maxaccount,'" + tAccount.AccountID + "' as AcctID from ppmaster", modPPGN.strPPDatabase);
					}
					else
					{
						rsSave.Execute("insert into ppmaster (Account,AccountID) values(" + tAccount.Account + ",'" + tAccount.AccountID + "') ", modPPGN.strPPDatabase);
					}
					rsSave.OpenRecordset("select * from ppmaster where accountid = '" + tAccount.AccountID + "'", modPPGN.strPPDatabase);
					if (!rsSave.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						tAccount.Account = rsSave.Get_Fields("account");
					}
				}
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
					rsSave.Set_Fields("PartyID", tAccount.PartyID);
					rsSave.Set_Fields("trancode", tAccount.TranCode);
					int x;
					for (x = 1; x <= 2; x++)
					{
						rsSave.Set_Fields("exemptcode" + x, tAccount.Get_ExemptCode(x));
						rsSave.Set_Fields("Exemption" + x, tAccount.Get_ExemptVal(x));
					}
					// x
					rsSave.Set_Fields("RealAssoc", tAccount.RealAssoc);
					rsSave.Set_Fields("streetnumber", tAccount.StreetNumber);
					rsSave.Set_Fields("street", tAccount.Street);
					rsSave.Set_Fields("value", tAccount.TotValue);
					rsSave.Set_Fields("exemption", tAccount.Exemption);
					rsSave.Set_Fields("exemptcode1", tAccount.Get_ExemptCode(1));
					rsSave.Set_Fields("exemptcode2", tAccount.Get_ExemptCode(2));
					rsSave.Set_Fields("trancode", tAccount.TranCode);
					rsSave.Set_Fields("businesscode", tAccount.BusinessCode);
					rsSave.Set_Fields("streetcode", tAccount.StreetCode);
					rsSave.Set_Fields("open1", tAccount.Open1);
					rsSave.Set_Fields("open2", tAccount.Open2);
					rsSave.Set_Fields("compvalue", tAccount.CompValue);
					rsSave.Set_Fields("orcode", tAccount.ORCode);
					rsSave.Set_Fields("updcode", tAccount.UPDCode);
					rsSave.Set_Fields("deleted", tAccount.Deleted);
					rsSave.Set_Fields("rbcode", tAccount.RBCode);
					rsSave.Set_Fields("squarefootage", tAccount.SquareFootage);
					rsSave.Set_Fields("streetapt", tAccount.StreetApt);
					rsSave.Set_Fields("comment", tAccount.Comment);
					rsSave.Set_Fields("exemption1", tAccount.Get_ExemptVal(1));
					rsSave.Set_Fields("exemption2", tAccount.Get_ExemptVal(2));
					rsSave.Set_Fields("accountid", tAccount.AccountID);
					rsSave.Update();
				}
				return SaveAccount;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return SaveAccount;
		}
	}
}
