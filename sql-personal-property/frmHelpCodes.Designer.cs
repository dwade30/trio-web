﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmHelpCodes.
	/// </summary>
	partial class frmHelpCodes : BaseForm
	{
		public fecherFoundation.FCTextBox txtNotes;
		public fecherFoundation.FCListBox lstCodes;
		public fecherFoundation.FCLabel lblList;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmHelpCodes));
			this.txtNotes = new fecherFoundation.FCTextBox();
			this.lstCodes = new fecherFoundation.FCListBox();
			this.lblList = new fecherFoundation.FCLabel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 413);
			this.BottomPanel.Size = new System.Drawing.Size(598, 35);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtNotes);
			this.ClientArea.Controls.Add(this.lstCodes);
			this.ClientArea.Controls.Add(this.lblList);
			this.ClientArea.Size = new System.Drawing.Size(598, 353);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(598, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(62, 30);
			this.HeaderText.Text = "Help";
			// 
			// txtNotes
			// 
			this.txtNotes.AutoSize = false;
			this.txtNotes.BackColor = System.Drawing.SystemColors.Window;
			this.txtNotes.LinkItem = null;
			this.txtNotes.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNotes.LinkTopic = null;
			this.txtNotes.Location = new System.Drawing.Point(30, 210);
			this.txtNotes.LockedOriginal = true;
			this.txtNotes.Multiline = true;
			this.txtNotes.Name = "txtNotes";
			this.txtNotes.ReadOnly = true;
			this.txtNotes.ScrollBars = Wisej.Web.ScrollBars.Vertical;
			this.txtNotes.Size = new System.Drawing.Size(539, 120);
			this.txtNotes.TabIndex = 2;
			// 
			// lstCodes
			// 
			this.lstCodes.Appearance = 0;
			this.lstCodes.BackColor = System.Drawing.SystemColors.Window;
			//this.lstCodes.Columns = 0;
			this.lstCodes.Location = new System.Drawing.Point(30, 65);
			this.lstCodes.MultiSelect = 0;
			this.lstCodes.Name = "lstCodes";
			this.lstCodes.Size = new System.Drawing.Size(539, 125);
			this.lstCodes.Sorted = true;
			this.lstCodes.Sorting = Wisej.Web.SortOrder.Ascending;
			this.lstCodes.TabIndex = 0;
			this.lstCodes.KeyDown += new Wisej.Web.KeyEventHandler(this.lstCodes_KeyDown);
			this.lstCodes.DoubleClick += new System.EventHandler(this.lstCodes_DoubleClick);
			// 
			// lblList
			// 
			this.lblList.Location = new System.Drawing.Point(30, 30);
			this.lblList.Name = "lblList";
			this.lblList.Size = new System.Drawing.Size(539, 15);
			this.lblList.TabIndex = 1;
			// 
			// frmHelpCodes
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(598, 448);
			this.FillColor = 0;
			this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmHelpCodes";
			this.ShowInTaskbar = false;
			//FC:FINAL:MSH - i.issue #1331: show form in the center of the screen
			//this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Help";
			this.Load += new System.EventHandler(this.frmHelpCodes_Load);
			this.Activated += new System.EventHandler(this.frmHelpCodes_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmHelpCodes_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
