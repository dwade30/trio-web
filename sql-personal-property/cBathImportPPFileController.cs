//Fecher vbPorter - Version 1.0.0.93
using System;
using System.Collections.Generic;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using Wisej.Web;

namespace TWPP0000
{
	public class cBathImportPPFileController
	{

		//=========================================================

		private string strFileName;
		private bool boolStripTrailingZeroes;

		public string FileName
		{
			set
			{
				strFileName = value;
			}

			get
			{
					string FileName = "";
				FileName = strFileName;
				return FileName;
			}
		}

		public IEnumerable<cBathImportMaster> LoadFile()
		{
			try
			{
				List<cBathImportMaster> tReturn = new List<cBathImportMaster>();
				StreamReader ts = null;
				cBathImportMaster tMast;

				if (strFileName.Trim() != "")
				{
					ts = File.OpenText(strFileName);
					string strLine = "";

					while (!ts.EndOfStream)
					{
						strLine = ts.ReadLine();
						if (strLine.Length >= 314)
						{
							tMast = ParseRecord(strLine);
							if (!(tMast == null))
							{
								tReturn.Add(tMast);
							}
						}
					}

					ts.Close();
				}
				else
				{
					MessageBox.Show("No file name specified", "No File", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}

				return tReturn;
			}
			catch (Exception ex)
			{
				MessageBox.Show(
					"Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " +
					fecherFoundation.Information.Err(ex).Description + "\n" + "In LoadFile", "Error",
					MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return null;
			}
		}


		private cBathImportMaster ParseRecord(string strLine)
		{
			cBathImportMaster ParseRecord = null;
			try
			{	
				string strTemp;
				cBathImportMaster tMast = new/*AsNew*/ cBathImportMaster();
				string[] strary = null;

				strTemp = strLine.Substring(0, 15).Trim();
				tMast.IDNum = strTemp.ToIntegerValue();
				strTemp = strLine.Substring(15, 30).Trim();
				tMast.DoingBusinessAs = strTemp;
				strTemp = strLine.Substring(45, 5).Trim();
				tMast.StreetNo = strTemp.ToIntegerValue();
				strTemp = strLine.Substring(50, 25).Trim();
				tMast.Street = strTemp;
				strTemp =strLine.Substring(75, 30).Trim();
				tMast.OwnerName = strTemp;
				strTemp = strLine.Substring(105, 25).Trim();
				tMast.Address1 = strTemp;
				strTemp = strLine.Substring(130, 17).Trim();
				tMast.City = strTemp;
				strTemp = strLine.Substring(147, 2).Trim();
				tMast.State = strTemp;
				strTemp =strLine.Substring(149, 10).Trim();
				tMast.Zip = strTemp;
				strTemp = strLine.Substring(159, 4).Trim();
				tMast.BusinessTypeCode = strTemp;
				strTemp = strLine.Substring(163, 25).Trim();
				tMast.Address2 = strTemp;
				strTemp = strLine.Substring(188, 90).Trim();
				tMast.Comment = strTemp;
				strTemp = strLine.Substring(278, 9).Trim();
				tMast.TotalAppraised = strTemp.ToDoubleValue();
				strTemp = strLine.Substring(296, 9).Trim();
				tMast.BETETotal = strTemp.ToDoubleValue();
				strTemp = strLine.Substring(305, 9).Trim();
				tMast.NonBETE = strTemp.ToDoubleValue();

				ParseRecord = tMast;
				return ParseRecord;
			}
			catch (Exception ex)
			{	
				MessageBox.Show("Error "+FCConvert.ToString(fecherFoundation.Information.Err(ex).Number)+"  "+fecherFoundation.Information.Err(ex).Description+"\n"+"In ParseRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ParseRecord;
		}

	}
}
