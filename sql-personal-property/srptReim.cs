﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for srptReim.
	/// </summary>
	public partial class srptReim : FCSectionReport
	{
		public srptReim()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ActiveReport1";
		}

		public static srptReim InstancePtr
		{
			get
			{
				return (srptReim)Sys.GetInstance(typeof(srptReim));
			}
		}

		protected srptReim _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsReport?.Dispose();
                clsReport = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptReim	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsReport = new clsDRWrapper();

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strLocation;
			// Unload srptItemizedReim
			// Unload srptLeasedReim
			// SubReport1.object = New srptItemizedReim
			// SubReport2.object = New srptLeasedReim
			string strMasterJoin;
			strMasterJoin = modPPGN.GetMasterJoin();
			SubReport1.Report = srptItemizedReim.InstancePtr;
			SubReport2.Report = srptLeasedReim.InstancePtr;
			// Call clsReport.OpenRecordset("select * from ppmaster where account = " & Me.Tag, "twpp0000.vb1")
			clsReport.OpenRecordset(strMasterJoin + " where account = " + this.UserData, "twpp0000.vb1");
			if (clsReport.EndOfFile())
			{
				this.Close();
				return;
			}
			// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
			txtAcct.Text = clsReport.Get_Fields_String("account");
			strLocation = "";
			// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
			if (Conversion.Val(clsReport.Get_Fields("streetnumber")) > 0)
				strLocation = FCConvert.ToString(Conversion.Val(clsReport.Get_Fields("streetnumber")));
			strLocation = Strings.Trim(strLocation + " " + Strings.Trim(FCConvert.ToString(clsReport.Get_Fields_String("street"))));
			txtLoc.Text = strLocation;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			string strZip;
			string strZip4;
			int[] lngOrigCat = new int[10 + 1];
			int[] lngCat = new int[10 + 1];
			int[] lngLSOrigCat = new int[10 + 1];
			int[] lngLSCat = new int[10 + 1];
			int lngItOriginal;
			int lngItAssessment;
			int lngLsOriginal;
			int lngLsAssessment;
			
			int lngAssessment;
			strZip = Strings.Trim(FCConvert.ToString(clsReport.Get_Fields_String("zip")));
			strZip4 = FCConvert.ToString(clsReport.Get_Fields_String("zip4"));
			Field1.Text = clsReport.GetData("name").ToString();
			Field2.Text = clsReport.GetData("address1").ToString();
			Field3.Text = clsReport.GetData("address2").ToString();
			Field4.Text = clsReport.GetData("city") + " " + clsReport.GetData("state") + " " + strZip + " " + strZip4;
			// Set SubReport1.object = New srptItemizedReim
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			SubReport1.Report.UserData = clsReport.Get_Fields("Account");
			// Set SubReport2.object = New srptLeasedReim
			// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
			SubReport2.Report.UserData = clsReport.Get_Fields("account");
		}

		
	}
}
