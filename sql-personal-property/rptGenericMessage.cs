﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptGenericMessage.
	/// </summary>
	public partial class rptGenericMessage : BaseSectionReport
	{
		public rptGenericMessage()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ActiveReport1";
		}

		public static rptGenericMessage InstancePtr
		{
			get
			{
				return (rptGenericMessage)Sys.GetInstance(typeof(rptGenericMessage));
			}
		}

		protected rptGenericMessage _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptGenericMessage	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intPage;

		public void Init(string strTitle)
		{
			this.Name = strTitle;
			txtTitle.Text = strTitle;
			this.PrintReport(true);
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			Field1.Text = frmGenericMessage.InstancePtr.RichTextBox1.Text;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			intPage = 1;
			txtPage.Text = "Page " + FCConvert.ToString(intPage);
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + FCConvert.ToString(intPage);
			intPage += 1;
		}

		private void rptGenericMessage_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptGenericMessage properties;
			//rptGenericMessage.Caption	= "ActiveReport1";
			//rptGenericMessage.Icon	= "rptGenericMessage.dsx":0000";
			//rptGenericMessage.Left	= 0;
			//rptGenericMessage.Top	= 0;
			//rptGenericMessage.Width	= 12480;
			//rptGenericMessage.Height	= 5565;
			//rptGenericMessage.StartUpPosition	= 3;
			//rptGenericMessage.SectionData	= "rptGenericMessage.dsx":058A;
			//End Unmaped Properties
		}
	}
}
