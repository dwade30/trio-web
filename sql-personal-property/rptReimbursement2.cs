﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptReimbursement2.
	/// </summary>
	public partial class rptReimbursement2 : BaseSectionReport
	{
		public rptReimbursement2()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ActiveReport1";
		}

		public static rptReimbursement2 InstancePtr
		{
			get
			{
				return (rptReimbursement2)Sys.GetInstance(typeof(rptReimbursement2));
			}
		}

		protected rptReimbursement2 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsReport?.Dispose();
				clsItemized?.Dispose();
				clsLeased?.Dispose();
                clsReport = null;
                clsItemized = null;
                clsLeased = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptReimbursement2	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intPage;
		clsDRWrapper clsReport = new clsDRWrapper();
		clsDRWrapper clsItemized = new clsDRWrapper();
		clsDRWrapper clsLeased = new clsDRWrapper();
		int lngOriginal;
		int lngAssessment;
		int lngTotalTotal;
		int lngItOriginal;
		int lngItAssessment;
		int lngLsOriginal;
		int lngLsAssessment;
		string strSQL = "";
		int lngCurAcct;
		string strLocation = "";
		double dblRatio;
		public bool boolPrintQTY;
		double lngTotalOriginal;
		double lngTotalAssessment;
		double[] lngTotalOrigCat = new double[9 + 1];
		double[] lngTotalAssessCat = new double[9 + 1];
		int lngTotalCount;
		bool boolFirstGroup;
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// Private Sub ActiveReport_DataInitialize()
		/// </summary>
		/// <summary>
		/// Me.Fields.Add ("GroupBinder")
		/// </summary>
		/// <summary>
		/// End Sub
		/// </summary>
		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsReport.EndOfFile();
			// If Not EOF Then
			// Me.Fields("groupbinder").Value = clsReport.Fields("account")
			// 
			// End If
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
            using (clsDRWrapper clsLoad = new clsDRWrapper())
            {
                int x;
                string strTemp = "";
                boolFirstGroup = true;
                clsLoad.OpenRecordset("select * from RATIOTRENDS order by type", "twpp0000.vb1");
                lngTotalCount = 0;
                lngTotalOriginal = 0;
                lngTotalAssessment = 0;
                for (x = 1; x <= 9; x++)
                {
                    strTemp = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description")));
                    switch (x)
                    {
                        case 1:
                        {
                            if (strTemp != string.Empty)
                            {
                                lblCat1.Text = strTemp;
                            }
                            else
                            {
                                lblCat1.Text = "Category 1";
                            }

                            break;
                        }
                        case 2:
                        {
                            if (strTemp != string.Empty)
                            {
                                lblCat2.Text = strTemp;
                            }
                            else
                            {
                                lblCat2.Text = "Category 2";
                            }

                            break;
                        }
                        case 3:
                        {
                            if (strTemp != string.Empty)
                            {
                                lblCat3.Text = strTemp;
                            }
                            else
                            {
                                lblCat3.Text = "Category 3";
                            }

                            break;
                        }
                        case 4:
                        {
                            if (strTemp != string.Empty)
                            {
                                lblCat4.Text = strTemp;
                            }
                            else
                            {
                                lblCat4.Text = "Category 4";
                            }

                            break;
                        }
                        case 5:
                        {
                            if (strTemp != string.Empty)
                            {
                                lblCat5.Text = strTemp;
                            }
                            else
                            {
                                lblCat5.Text = "Category 5";
                            }

                            break;
                        }
                        case 6:
                        {
                            if (strTemp != string.Empty)
                            {
                                lblCat6.Text = strTemp;
                            }
                            else
                            {
                                lblCat6.Text = "Category 6";
                            }

                            break;
                        }
                        case 7:
                        {
                            if (strTemp != string.Empty)
                            {
                                lblCat7.Text = strTemp;
                            }
                            else
                            {
                                lblCat7.Text = "Category 7";
                            }

                            break;
                        }
                        case 8:
                        {
                            if (strTemp != string.Empty)
                            {
                                lblCat8.Text = strTemp;
                            }
                            else
                            {
                                lblCat8.Text = "Category 8";
                            }

                            break;
                        }
                        case 9:
                        {
                            if (strTemp != string.Empty)
                            {
                                lblCat9.Text = strTemp;
                            }
                            else
                            {
                                lblCat9.Text = "Category 9";
                            }

                            break;
                        }
                    }

                    //end switch
                    clsLoad.MoveNext();
                    lngTotalOrigCat[x] = 0;
                    lngTotalAssessCat[x] = 0;
                }

                // x
                //this.Printer.RenderMode = 1;
                //modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
                clsLoad.OpenRecordset("SELECT * FROM PPRatioOpens", "twpp0000.vb1");
                // TODO Get_Fields: Check the table for the column [Ratio] and replace with corresponding Get_Field method
                dblRatio = Conversion.Val(clsLoad.Get_Fields("Ratio")) / 100;
                Label6.Text = "Total Rounded By Category With " + FCConvert.ToString(dblRatio * 100) +
                              "% Ratio Applied:";
                // Label14.Caption = "Leased Total (With " & Val(clsLoad.Fields("ratio")) & "% ratio applied to assessment and rounded by Category):"
                clsLoad.OpenRecordset("select * from tbldefaultinformation", "SystemSettings");
                if (!clsLoad.EndOfFile())
                {
                    txtMuniName.Text = modGlobalConstants.Statics.MuniName + "," + clsLoad.Get_Fields_String("state");
                }
                else
                {
                    txtMuniName.Text = modGlobalConstants.Statics.MuniName;
                }

                txttime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
                clsReport.OpenRecordset(strSQL, "twpp0000.vb1");
                if (clsReport.EndOfFile())
                {
                    MessageBox.Show(
                        "No matches found.  The Account(s) are either deleted,don't exist or have no reimbursable items.",
                        null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    this.Close();
                    return;
                }

                intPage = 1;
                Label9.Text = "TOTAL TAX ON QUALIFIED PROPERTY FOR " + FCConvert.ToString(modPPGN.Statics.rbTaxYear);
                txtAssessor.Text = modPPGN.Statics.rbSigner;
                txtAssessorTitle.Text = modPPGN.Statics.rbSignerTitle;
                // txttaxrate.Text = Format(rbTaxRate, "#.00000")
                txttaxrate.Text = modPPGN.Statics.rbTaxRate.ToString();
                txt1st.Text = Strings.Format(modPPGN.Statics.rbDueDate1, "MM/dd/yy");
                if (modPPGN.Statics.rbDueDate2.ToOADate() == 0)
                {
                    txt2nd.Text = "";
                    Label8.Visible = false;
                }
                else
                {
                    txt2nd.Text = Strings.Format(modPPGN.Statics.rbDueDate2, "MM/dd/yy");
                }

                txtMessage.Text = "";
                txtMessage.Text = modPPGN.Statics.rbMessage;

                if (!clsReport.EndOfFile())
                {
                    lngCurAcct = FCConvert.ToInt32(clsReport.GetData("account"));
                    this.Fields["grpHeader"].Value = lngCurAcct.ToString();
                    // strLocation = ""
                    // If Val(clsReport.Fields("streetnumber")) > 0 Then strLocation = Val(clsReport.Fields("streetnumber"))
                    // strLocation = Trim(strLocation & " " & Trim(clsReport.Fields("street")))
                }
            }

            SubReport1.Report = new srptReim();
		}

		private void Detail_AfterPrint(object sender, EventArgs e)
		{
			// intPage = 1
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			string strZip4 = "";
			string strZip = "";
			// vbPorter upgrade warning: lngCat As int	OnWrite(int, double)
			int[] lngCat = new int[10 + 1];
			// vbPorter upgrade warning: lngOrigCat As int	OnWrite(int, double)
			int[] lngOrigCat = new int[10 + 1];
			// vbPorter upgrade warning: lngLSCat As int	OnWrite(int, double)
			int[] lngLSCat = new int[10 + 1];
			// vbPorter upgrade warning: lngLSOrigCat As int	OnWrite(int, double)
			int[] lngLSOrigCat = new int[10 + 1];
			// vbPorter upgrade warning: lngCurCat As int	OnWrite(short, double)
			int[] lngCurCat = new int[10 + 1];
			int x;
			int intTemp = 0;
			lngTotalCount += 1;
			// intPage = 1
			// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
			lngCurAcct = FCConvert.ToInt32(clsReport.Get_Fields("account"));
			// strLocation = ""
			// If Val(clsReport.Fields("streetnumber")) > 0 Then strLocation = Val(clsReport.Fields("streetnumber"))
			// strLocation = Trim(strLocation & " " & Trim(clsReport.Fields("street")))
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			SubReport1.Report.UserData = clsReport.Get_Fields("Account");
			// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
			this.Fields["grpHeader"].Value = clsReport.Get_Fields_String("account");
			lngOriginal = 0;
			for (x = 1; x <= 9; x++)
			{
				lngOrigCat[x] = 0;
				lngCat[x] = 0;
				lngLSOrigCat[x] = 0;
				lngLSCat[x] = 0;
				lngCurCat[x] = 0;
			}
			// x
			lngItOriginal = 0;
			lngItAssessment = 0;
			lngLsOriginal = 0;
			lngLsAssessment = 0;
			lngAssessment = 0;
			// Call clsItemized.OpenRecordset("Select sum(cost * quantity) as theCost,sum(value) as theValue from PPItemized where Account = " & clsReport.Fields("Account") & " and RB = '*'", "twpp0000.vb1")
			// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
			clsItemized.OpenRecordset("select cd,sum(cost * quantity) as thecost,sum(value) as theValue from ppitemized where account = " + clsReport.Get_Fields("account") + " and rb = '*' and srO = 'O' group by cd", "twpp0000.vb1");
			while (!clsItemized.EndOfFile())
			{
				intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(clsItemized.Get_Fields_String("cd"))));
				if (intTemp < 10 && intTemp > 0)
				{
					// TODO Get_Fields: Field [thecost] not found!! (maybe it is an alias?)
					lngOrigCat[intTemp] = FCConvert.ToInt32(Math.Round(Conversion.Val(clsItemized.Get_Fields("thecost"))));
					lngOrigCat[intTemp] = FCConvert.ToInt32(modGlobal.Round(FCConvert.ToDouble(lngOrigCat[intTemp]), modPPCalculate.Statics.PPRounding));
					lngItOriginal += lngOrigCat[intTemp];
				}
				clsItemized.MoveNext();
			}
			// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
			clsItemized.OpenRecordset("select cd,sum(cost * quantity) as thecost,sum(value) as theValue from ppitemized where account = " + clsReport.Get_Fields("account") + " and rb = '*' group by cd", "twpp0000.vb1");
			while (!clsItemized.EndOfFile())
			{
				intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(clsItemized.Get_Fields_String("cd"))));
				if (intTemp < 10 && intTemp > 0)
				{
					// TODO Get_Fields: Field [Thevalue] not found!! (maybe it is an alias?)
					lngCat[intTemp] = FCConvert.ToInt32(Math.Round(Conversion.Val(clsItemized.Get_Fields("Thevalue"))));
					// lngCat(intTemp) = Round(CDbl(lngCat(intTemp)) * dblRatio, PPRounding)
					lngCat[intTemp] = FCConvert.ToInt32(modGlobal.Round(FCConvert.ToDouble(lngCat[intTemp]), modPPCalculate.Statics.PPRounding));
					lngItAssessment += lngCat[intTemp];
				}
				clsItemized.MoveNext();
			}
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			clsLeased.OpenRecordset("Select cd,sum(cost * quantity) as theCost,sum(value) as theValue  from PPLeased where Account = " + clsReport.Get_Fields("Account") + " and RB = '*' group by cd", "twpp0000.vb1");
			while (!clsLeased.EndOfFile())
			{
				intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLeased.Get_Fields_String("cd"))));
				if (intTemp < 10 && intTemp > 0)
				{
					// TODO Get_Fields: Field [thecost] not found!! (maybe it is an alias?)
					lngLSOrigCat[intTemp] = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLeased.Get_Fields("thecost"))));
					// TODO Get_Fields: Field [thevalue] not found!! (maybe it is an alias?)
					lngLSCat[intTemp] = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLeased.Get_Fields("thevalue"))));
					lngLSOrigCat[intTemp] = FCConvert.ToInt32(modGlobal.Round(FCConvert.ToDouble(lngLSOrigCat[intTemp]), modPPCalculate.Statics.PPRounding));
					// lngLSCat(intTemp) = Round(CDbl(lngLSCat(intTemp)) * dblRatio, PPRounding)
					lngLSCat[intTemp] = FCConvert.ToInt32(modGlobal.Round(FCConvert.ToDouble(lngLSCat[intTemp]), modPPCalculate.Statics.PPRounding));
					lngLsOriginal += lngLSOrigCat[intTemp];
					lngLsAssessment += lngLSCat[intTemp];
				}
				clsLeased.MoveNext();
			}
			for (x = 1; x <= 9; x++)
			{
				// Call clsItemized.OpenRecordset("select sum(cost * quantity) as thecost,sum(value) as theValue from ppitemized where account = " & clsReport.Fields("account") & " and rb = '*' and cd = '" & x & "'", "twpp0000.vb1")
				// Call clsLeased.OpenRecordset("Select sum(cost * quantity) as theCost,sum(value) as theValue  from PPLeased where Account = " & clsReport.Fields("Account") & " and RB = '*' and cd = '" & x & "'", "twpp0000.vb1")
				// 
				// If Not clsItemized.EndOfFile Then
				// lngOrigCat(x) = Val(clsItemized.Fields("thecost"))
				// lngCat(x) = Val(clsItemized.Fields("thevalue"))
				// End If
				// If Not clsLeased.EndOfFile Then
				// lngLSOrigCat(x) = Val(clsLeased.Fields("thecost"))
				// lngLSCat(x) = Val(clsLeased.Fields("thevalue"))
				// End If
				// 
				// lngOrigCat(x) = Round(CDbl(lngOrigCat(x)), PPRounding)
				// lngCat(x) = Round(CDbl(lngCat(x)) * dblRatio, PPRounding)
				// lngLSOrigCat(x) = Round(CDbl(lngLSOrigCat(x)), PPRounding)
				// lngLSCat(x) = Round(CDbl(lngLSCat(x)) * dblRatio, PPRounding)
				// 
				// lngItOriginal = lngItOriginal + lngOrigCat(x)
				// lngItAssessment = lngItAssessment + lngCat(x)
				// lngLsOriginal = lngLsOriginal + lngLSOrigCat(x)
				// lngLsAssessment = lngLsAssessment + lngLSCat(x)
				lngCurCat[x] = FCConvert.ToInt32(modGlobal.Round((lngCat[x] + lngLSCat[x]) * dblRatio, modPPCalculate.Statics.PPRounding));
				lngTotalOrigCat[x] += modGlobal.Round((lngOrigCat[x] + lngLSOrigCat[x]) * dblRatio, modPPCalculate.Statics.PPRounding);
				lngTotalAssessCat[x] += lngCurCat[x];
				lngTotalOriginal += lngItOriginal + lngLsOriginal;
				// lngTotalAssessment = lngTotalAssessment + lngLsAssessment + lngItAssessment
				lngTotalAssessment += lngTotalAssessCat[x];
				lngAssessment += lngCurCat[x];
			}
			// x
			// Call clsLeased.OpenRecordset("Select sum(cost * quantity) as theCost,sum(value) as theValue  from PPLeased where Account = " & clsReport.Fields("Account") & " and RB = '*'", "twpp0000.vb1")
			// If Not clsItemized.EndOfFile Then
			// lngItOriginal = Val(clsItemized.GetData("thecost"))
			// lngItAssessment = Val(clsItemized.GetData("thevalue"))
			// Else
			// lngItOriginal = 0
			// lngItAssessment = 0
			// End If
			// 
			// If Not clsLeased.EndOfFile Then
			// lngLsOriginal = Val(clsLeased.GetData("thecost"))
			// lngLsAssessment = Val(clsLeased.GetData("thevalue"))
			// Else
			// lngLsOriginal = 0
			// lngLsAssessment = 0
			// End If
			lngOriginal = lngItOriginal + lngLsOriginal;
			// lngAssessment = lngItAssessment + lngLsAssessment
			// txtOriginal.Text = Format(lngItOriginal, "###,###,###,##0")
			// txtAssessment.Text = Format(lngItAssessment, "###,###,###,##0")
			txtOrigTot.Text = Strings.Format(lngOriginal, "#,###,###,##0");
			// lngAssessment = Round(CDbl(lngAssessment), PPRounding)
			txtRatioTot.Text = Strings.Format(lngAssessment, "###,###,###,##0");
			txtTotTax.Text = Strings.Format(modPPGN.Statics.rbTaxRate * lngAssessment, "###,###,###,##0.00");
			// If Not clsReport.EndOfFile Then
			// End If
			clsReport.MoveNext();
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			if (!boolFirstGroup)
			{
				intPage = 1;
			}
			boolFirstGroup = false;
		}
		
		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + FCConvert.ToString(intPage);
			intPage += 1;
			txtDate.Text = FCConvert.ToString(DateTime.Today);
			Label3.Text = "Tax Year " + FCConvert.ToString(modPPGN.Statics.rbTaxYear);
		}
		// vbPorter upgrade warning: boolPQTY As object	OnRead(bool)
		public void Start(ref string strSQLStatement, ref object boolPQTY)
		{
			boolPrintQTY = FCConvert.ToBoolean(boolPQTY);
			strSQL = strSQLStatement;
			;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: lngTemp As int	OnWrite(short, double)
			int lngTemp;
			// vbPorter upgrade warning: lngTemp2 As int	OnWrite(short, double)
			int lngTemp2;
			int x;
			if (lngTotalCount <= 1)
			{
				ReportFooter.Visible = false;
			}
			else
			{
				ReportFooter.Visible = true;
			}
			txtTotalCount.Text = FCConvert.ToString(lngTotalCount);
			lngTemp = 0;
			lngTemp2 = 0;
			for (x = 1; x <= 9; x++)
			{
				lngTemp = FCConvert.ToInt32(lngTotalOrigCat[x] + lngTemp);
				lngTemp2 = FCConvert.ToInt32(lngTotalAssessCat[x] + lngTemp2);
			}
			// x
			txtTotalAssessment.Text = Strings.Format(lngTemp2, "#,###,###,##0");
			txtTotalOriginal.Text = Strings.Format(lngTemp, "#,###,###,##0");
			txtOrigCat1.Text = Strings.Format(lngTotalOrigCat[1], "#,###,###,##0");
			txtOrigCat2.Text = Strings.Format(lngTotalOrigCat[2], "#,###,###,##0");
			txtOrigCat3.Text = Strings.Format(lngTotalOrigCat[3], "#,###,###,##0");
			txtOrigCat4.Text = Strings.Format(lngTotalOrigCat[4], "#,###,###,##0");
			txtOrigCat5.Text = Strings.Format(lngTotalOrigCat[5], "#,###,###,##0");
			txtOrigCat6.Text = Strings.Format(lngTotalOrigCat[6], "#,###,###,##0");
			txtOrigCat7.Text = Strings.Format(lngTotalOrigCat[7], "#,###,###,##0");
			txtOrigCat8.Text = Strings.Format(lngTotalOrigCat[8], "#,###,###,##0");
			txtOrigCat9.Text = Strings.Format(lngTotalOrigCat[9], "#,###,###,##0");
			txtAssessCat1.Text = Strings.Format(lngTotalAssessCat[1], "#,###,###,##0");
			txtAssessCat2.Text = Strings.Format(lngTotalAssessCat[2], "#,###,###,##0");
			txtAssessCat3.Text = Strings.Format(lngTotalAssessCat[3], "#,###,###,##0");
			txtAssessCat4.Text = Strings.Format(lngTotalAssessCat[4], "#,###,###,##0");
			txtAssessCat5.Text = Strings.Format(lngTotalAssessCat[5], "#,###,###,##0");
			txtAssessCat6.Text = Strings.Format(lngTotalAssessCat[6], "#,###,###,##0");
			txtAssessCat7.Text = Strings.Format(lngTotalAssessCat[7], "#,###,###,##0");
			txtAssessCat8.Text = Strings.Format(lngTotalAssessCat[8], "#,###,###,##0");
			txtAssessCat9.Text = Strings.Format(lngTotalAssessCat[9], "#,###,###,##0");
		}

		

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("grpHeader");
		}
	}
}
