﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmDefaultItems.
	/// </summary>
	public partial class frmDefaultItems : BaseForm
	{
		public frmDefaultItems()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmDefaultItems InstancePtr
		{
			get
			{
				return (frmDefaultItems)Sys.GetInstance(typeof(frmDefaultItems));
			}
		}

		protected frmDefaultItems _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		/// <summary>
		/// ********************************************************
		/// </summary>
		/// <summary>
		/// Property of TRIO Software Corporation
		/// </summary>
		/// <summary>
		/// Written By
		/// </summary>
		/// <summary>
		/// Date
		/// </summary>
		/// <summary>
		/// ********************************************************
		/// </summary>
		int lngReturn;
		const int CNSTGRIDCOLid = 0;
		const int CNSTGRIDCOLCATEGORY = 1;
		const int CNSTGRIDCOLDESCRIPTION = 2;
		const int CNSTGRIDCOLSRO = 3;
		const int CNSTGRIDCOLYEAR = 4;
		const int CNSTGRIDCOLDPYR = 5;
		const int CNSTGRIDCOLCOST = 6;
		const int CNSTGRIDCOLSOURCE = 7;

		public int Init()
		{
			int Init = 0;
			Init = 0;
			lngReturn = 0;
			this.Show(FormShowEnum.Modal);
			Init = lngReturn;
			return Init;
		}

		private void cmdFindNext_Click(object sender, System.EventArgs e)
		{
			string strFind;
			int lngRow;
			int lngCRow;
			strFind = txtFind.Text;
			if (strFind == string.Empty)
			{
				for (lngRow = 1; lngRow <= Grid.Rows - 1; lngRow++)
				{
					Grid.RowHidden(lngRow, false);
				}
				// lngRow
				return;
			}
			for (lngRow = 1; lngRow <= Grid.Rows - 1; lngRow++)
			{
				if (Strings.InStr(1, Grid.TextMatrix(lngRow, CNSTGRIDCOLDESCRIPTION), strFind, CompareConstants.vbTextCompare) <= 0)
				{
					Grid.RowHidden(lngRow, true);
				}
				else
				{
					Grid.RowHidden(lngRow, false);
				}
			}
			// lngRow
			// lngCRow = Grid.Row
			// lngCRow = lngCRow + 1
			// If lngCRow > Grid.Rows - 1 Then lngCRow = 1
			// If Grid.Rows <= 1 Then Exit Sub
			// lngRow = FindARow(txtFind.Text, lngCRow, CNSTGRIDCOLDESCRIPTION)
			// If lngRow > 0 Then
			// Grid.Row = lngRow
			// Grid.TopRow = lngRow
			// Else
			// lngRow = FindARow(txtFind.Text, 1, CNSTGRIDCOLDESCRIPTION)
			// If lngRow > 0 Then
			// Grid.Row = lngRow
			// Grid.TopRow = lngRow
			// End If
			// End If
		}

		public void cmdFindNext_Click()
		{
			cmdFindNext_Click(cmdFindNext, new System.EventArgs());
		}

		private int FindARow(ref string strFind, ref int lngStartRow, ref int lngCol)
		{
			int FindARow = 0;
			int lngReturn;
			int lngRow;
			FindARow = -1;
			lngReturn = -1;
			if (strFind == string.Empty)
			{
				return FindARow;
			}
			for (lngRow = lngStartRow; lngRow <= Grid.Rows - 1; lngRow++)
			{
				if (Grid.TextMatrix(lngRow, lngCol) != string.Empty)
				{
					if (Strings.InStr(1, Grid.TextMatrix(lngRow, lngCol), strFind, CompareConstants.vbTextCompare) > 0)
					{
						lngReturn = lngRow;
						break;
					}
				}
			}
			// lngRow
			FindARow = lngReturn;
			return FindARow;
		}

		private void frmDefaultItems_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmDefaultItems_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmDefaultItems properties;
			//frmDefaultItems.FillStyle	= 0;
			//frmDefaultItems.ScaleWidth	= 9300;
			//frmDefaultItems.ScaleHeight	= 7800;
			//frmDefaultItems.LinkTopic	= "Form2";
			//frmDefaultItems.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
			LoadGrid();
		}

		private void SetupGrid()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				string strTemp;
				rsLoad.OpenRecordset("select * from RATIOTRENDS order by type", "twpp0000.vb1");
				strTemp = "";
				while (!rsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
					strTemp += "#" + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("type"))) + ";" + rsLoad.Get_Fields_String("description") + "|";
					rsLoad.MoveNext();
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				Grid.TextMatrix(0, CNSTGRIDCOLCATEGORY, "Category");
				Grid.TextMatrix(0, CNSTGRIDCOLDESCRIPTION, "Description");
				Grid.TextMatrix(0, CNSTGRIDCOLSRO, "SRO");
				Grid.TextMatrix(0, CNSTGRIDCOLYEAR, "Year");
				Grid.TextMatrix(0, CNSTGRIDCOLCOST, "Cost");
				Grid.TextMatrix(0, CNSTGRIDCOLSOURCE, "Source");
				Grid.TextMatrix(0, CNSTGRIDCOLDPYR, "DPYr");
				Grid.ColHidden(CNSTGRIDCOLid, true);
				Grid.ColComboList(CNSTGRIDCOLCATEGORY, strTemp);
				Grid.ColComboList(CNSTGRIDCOLSRO, "#0;Original|#1;Sound|#2;Replacement");
				Grid.ColAlignment(CNSTGRIDCOLYEAR, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				Grid.ColAlignment(CNSTGRIDCOLCOST, FCGrid.AlignmentSettings.flexAlignRightCenter);
				Grid.ColDataType(CNSTGRIDCOLCOST, FCGrid.DataTypeSettings.flexDTLong);
				Grid.ColDataType(CNSTGRIDCOLYEAR, FCGrid.DataTypeSettings.flexDTLong);
				Grid.ColAlignment(CNSTGRIDCOLDPYR, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				Grid.ColDataType(CNSTGRIDCOLDPYR, FCGrid.DataTypeSettings.flexDTLong);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SetupGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTGRIDCOLCATEGORY, FCConvert.ToInt32(0.25 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLDESCRIPTION, FCConvert.ToInt32(0.3 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLSRO, FCConvert.ToInt32(0.12 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLYEAR, FCConvert.ToInt32(0.05 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLDPYR, FCConvert.ToInt32(0.05 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLCOST, FCConvert.ToInt32(0.08 * GridWidth));
		}

		private void LoadGrid()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			int lngRow;
			try
			{
				// On Error GoTo ErrorHandler
				rsLoad.OpenRecordset("select * from DefaultItems order by description", "twpp0000.vb1");
				Grid.Rows = 1;
				while (!rsLoad.EndOfFile())
				{
					Grid.Rows += 1;
					lngRow = Grid.Rows - 1;
					// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
					Grid.TextMatrix(lngRow, CNSTGRIDCOLCATEGORY, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("category"))));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLDESCRIPTION, Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("description"))));
					// TODO Get_Fields: Check the table for the column [sro] and replace with corresponding Get_Field method
					Grid.TextMatrix(lngRow, CNSTGRIDCOLSRO, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("sro"))));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLYEAR, FCConvert.ToString(rsLoad.Get_Fields_Int32("purchaseyear")));
					// TODO Get_Fields: Check the table for the column [cost] and replace with corresponding Get_Field method
					Grid.TextMatrix(lngRow, CNSTGRIDCOLCOST, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("cost"))));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLSOURCE, Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("source"))));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLid, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("id"))));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLDPYR, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("depreciationyears"))));
					rsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In LoadGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmDefaultItems_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Grid_DblClick(object sender, System.EventArgs e)
		{
			if (PickRow())
			{
				Close();
			}
		}

		private bool PickRow()
		{
			bool PickRow = false;
			PickRow = false;
			if (Grid.Row > 0)
			{
				lngReturn = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(Grid.Row, CNSTGRIDCOLid))));
				PickRow = true;
			}
			return PickRow;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (PickRow())
			{
				Close();
			}
		}

		private void txtFind_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Return)
			{
				cmdFindNext_Click();
			}
		}
	}
}
