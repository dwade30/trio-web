﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Enums;
using TWSharedLibrary;
using Wisej.Web;
using Wisej.Core;

namespace TWPP0000
{
	public class modGNWork
	{
		public static bool FormExist(Form FormName)
		{
			bool FormExist = false;
			foreach (Form frm in Application.OpenForms)
			{
				if (frm.Name == FormName.Name)
				{
					if (FCConvert.ToString(frm.Tag) == "Open")
					{
						frm.Refresh();
						FormExist = true;
						return FormExist;
					}
				}
			}
			FormName.Tag = "Open";
			return FormExist;
		}

		public static void GetLocalVariables2()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper clsLoad = new clsDRWrapper();
				// Dim ff As New FileSystemObject
				modGlobal.Statics.CustomizeStuff.LoadCustomInfo();
				// 
				// Call clsLoad.OpenRecordset("select * from ppratioopens", modPPGN.strPPDatabase)
				// If Not clsLoad.EndOfFile Then
				// CustomizeStuff.dblRatio = Val(clsLoad.Fields("ratio")) / 100
				// If CustomizeStuff.dblRatio = 0 Then CustomizeStuff.dblRatio = 1
				// Else
				// CustomizeStuff.dblRatio = 1
				// End If
				clsLoad.OpenRecordset("select * from customize", "twpp0000.vb1");
				iLabel4:
				if (clsLoad.EndOfFile())
				{
					// 5           CustomizeStuff.DepreciationYear = 0
					iLabel6:
					modPPCalculate.Statics.DepYear = 0;
					iLabel7:
					modPPCalculate.Statics.NextPPAccount = 0;
					// CustomizeStuff.Roundcontingency = 2
					iLabel8:
					modPPCalculate.Statics.RoundContingency = 2;
					iLabel9:
					modPPCalculate.Statics.PPRounding = 3;
					// CustomizeStuff.Round = 3
					iLabel10:
					modPPGN.Statics.DatabaseName = "TWPP0000.vb1";
					iLabel11:
					modGlobal.Statics.boolDosPrint = false;
					// CustomizeStuff.TaxRate = 1
					// CustomizeStuff.ShowRateOnShort = True
					// CustomizeStuff.TrendTimesCost = False
					// CustomizeStuff.ShowOriginalTotals = False
					// CustomizeStuff.DefaultYearFirstAssessed = Year(Date)
					// CustomizeStuff.LeasedTrendTimesCost = True
					// CustomizeStuff.LimitPercentGood = False
				}
				else
				{
					// 12              CustomizeStuff.Round = Val(clsLoad.Fields("Round"))
					// 13            PPRounding = Val(WPP.GetData("WPPRound") & "")
					modPPCalculate.Statics.PPRounding = modGlobal.Statics.CustomizeStuff.Round;
					// CustomizeStuff.LimitPercentGood = clsLoad.Fields("limitpercentgood")
					// 14            If WPP.Fields("roundcontingency") = "" Then
					// 15               If WPP.Fields("wppround2") = "Y" Then
					// 16                    RoundContingency = 0
					// Else
					// 17                    RoundContingency = 2
					// End If
					// CustomizeStuff.DefaultYearFirstAssessed = Val(clsLoad.Fields("defaultyfa"))
					// If CustomizeStuff.DefaultYearFirstAssessed < 1990 Or CustomizeStuff.DefaultYearFirstAssessed > Year(Date) + 2 Then
					// CustomizeStuff.DefaultYearFirstAssessed = 0
					// End If
					// CustomizeStuff.Roundcontingency = Val(clsLoad.Fields("roundcontingency"))
					modPPCalculate.Statics.RoundContingency = modGlobal.Statics.CustomizeStuff.RoundContingency;
					// Else
					// 18                RoundContingency = Val(WPP.Fields("roundcontingency"))
					// End If
					// 19            DepYear = Val(WPP.GetData("WDEPYearPP") & "")
					// CustomizeStuff.DepreciationYear = Val(clsLoad.Fields("depreciationyear"))
					modPPCalculate.Statics.DepYear = modGlobal.Statics.CustomizeStuff.DepreciationYear;
					// 20            DatabaseName = Trim(WPP.GetData("WDATABASELOCATION") & "")
					modPPGN.Statics.DatabaseName = "Twpp0000.vb1";
					// 21            NextPPAccount = Val(WPP.GetData("WNEXTPPACCT") & "") + 1
					iLabel22:
					modGlobal.Statics.boolDosPrint = false;
					// CustomizeStuff.ShowRateOnShort = clsLoad.Fields("showrateonshort")
					// CustomizeStuff.TaxRate = Val(clsLoad.Fields("taxrate"))
					// CustomizeStuff.TrendTimesCost = clsLoad.Fields("trendtimescost")
					// CustomizeStuff.LeasedTrendTimesCost = clsLoad.Fields("leasedtrendtimescost")
					// CustomizeStuff.ShowOriginalTotals = clsLoad.Fields("showoriginaltotals")
				}
				// End If
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In GetLocalVariables2 in line " + Information.Erl(), null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
	}
}
