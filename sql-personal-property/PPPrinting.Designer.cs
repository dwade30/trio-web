//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;
using fecherFoundation.VisualBasicLayer;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmPPPrinting.
	/// </summary>
	partial class frmPPPrinting : BaseForm
	{
		public fecherFoundation.FCComboBox cmbSequence;
		public fecherFoundation.FCLabel lblSequence;
		public fecherFoundation.FCComboBox cmbRange;
		public fecherFoundation.FCComboBox cmbLeased;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCCheckBox chkCategory;
		public fecherFoundation.FCCheckBox chkYearNew;
		public FCGrid Grid;
		public fecherFoundation.FCFrame fraRange;
		public fecherFoundation.FCTextBox txtStop;
		public fecherFoundation.FCTextBox txtStart;
		public fecherFoundation.FCLabel lblStop;
		public fecherFoundation.FCLabel lblStart;
		public fecherFoundation.FCFrame fraCustomFormatOnly;
		public fecherFoundation.FCCheckBox chkAccount;
		public fecherFoundation.FCCheckBox chkName;
		public fecherFoundation.FCCheckBox chkMailingAddress;
		public fecherFoundation.FCCheckBox chkLocation;
		public fecherFoundation.FCCheckBox chkOpen1;
		public fecherFoundation.FCCheckBox chkOpen2;
		public fecherFoundation.FCLabel lblPrintWidth;
		public fecherFoundation.FCFrame fraLeased;
		public fecherFoundation.FCCheckBox chkQTY;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCTimer Timer1;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCData datRatioOpens;
		public fecherFoundation.FCLabel lblPrintMessage;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPPPrinting));
			this.cmbSequence = new fecherFoundation.FCComboBox();
			this.lblSequence = new fecherFoundation.FCLabel();
			this.cmbRange = new fecherFoundation.FCComboBox();
			this.cmbLeased = new fecherFoundation.FCComboBox();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.chkCategory = new fecherFoundation.FCCheckBox();
			this.chkYearNew = new fecherFoundation.FCCheckBox();
			this.Grid = new fecherFoundation.FCGrid();
			this.fraRange = new fecherFoundation.FCFrame();
			this.txtStop = new fecherFoundation.FCTextBox();
			this.txtStart = new fecherFoundation.FCTextBox();
			this.lblStop = new fecherFoundation.FCLabel();
			this.lblStart = new fecherFoundation.FCLabel();
			this.fraCustomFormatOnly = new fecherFoundation.FCFrame();
			this.chkAccount = new fecherFoundation.FCCheckBox();
			this.chkName = new fecherFoundation.FCCheckBox();
			this.chkMailingAddress = new fecherFoundation.FCCheckBox();
			this.chkLocation = new fecherFoundation.FCCheckBox();
			this.chkOpen1 = new fecherFoundation.FCCheckBox();
			this.chkOpen2 = new fecherFoundation.FCCheckBox();
			this.lblPrintWidth = new fecherFoundation.FCLabel();
			this.fraLeased = new fecherFoundation.FCFrame();
			this.chkQTY = new fecherFoundation.FCCheckBox();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Timer1 = new fecherFoundation.FCTimer(this.components);
			this.cmdPrint = new fecherFoundation.FCButton();
			this.datRatioOpens = new fecherFoundation.FCData();
			this.lblPrintMessage = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintPreview = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.lblRange = new fecherFoundation.FCLabel();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkCategory)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkYearNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraRange)).BeginInit();
			this.fraRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraCustomFormatOnly)).BeginInit();
			this.fraCustomFormatOnly.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMailingAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkOpen1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkOpen2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraLeased)).BeginInit();
			this.fraLeased.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkQTY)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdPrint);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			this.BottomPanel.Size = new System.Drawing.Size(857, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.cmbSequence);
			this.ClientArea.Controls.Add(this.lblSequence);
			this.ClientArea.Controls.Add(this.fraRange);
			this.ClientArea.Controls.Add(this.cmbRange);
			this.ClientArea.Controls.Add(this.lblRange);
			this.ClientArea.Controls.Add(this.fraCustomFormatOnly);
			this.ClientArea.Controls.Add(this.fraLeased);
			this.ClientArea.Controls.Add(this.datRatioOpens);
			this.ClientArea.Controls.Add(this.lblPrintMessage);
			this.ClientArea.Size = new System.Drawing.Size(857, 498);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(857, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(204, 30);
			this.HeaderText.Text = "Print Account List";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// cmbSequence
			// 
			this.cmbSequence.AutoSize = false;
			this.cmbSequence.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbSequence.FormattingEnabled = true;
			this.cmbSequence.Items.AddRange(new object[] {
				"Account",
				"Name",
				"Street",
				"Open 1",
				"Open 2",
				"Business Code",
				"Street Code",
				"Tran Code"
			});
			this.cmbSequence.Location = new System.Drawing.Point(612, 30);
			this.cmbSequence.Name = "cmbSequence";
			this.cmbSequence.Size = new System.Drawing.Size(195, 40);
			this.cmbSequence.TabIndex = 36;
			this.cmbSequence.Text = "Account";
			this.ToolTip1.SetToolTip(this.cmbSequence, null);
			// 
			// lblSequence
			// 
			this.lblSequence.AutoSize = true;
			this.lblSequence.Location = new System.Drawing.Point(480, 44);
			this.lblSequence.Name = "lblSequence";
			this.lblSequence.Size = new System.Drawing.Size(97, 15);
			this.lblSequence.TabIndex = 37;
			this.lblSequence.Text = "SEQUENCE BY";
			this.ToolTip1.SetToolTip(this.lblSequence, null);
			// 
			// cmbRange
			// 
			this.cmbRange.AutoSize = false;
			this.cmbRange.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbRange.FormattingEnabled = true;
			this.cmbRange.Items.AddRange(new object[] {
				"All",
				"Range"
			});
			this.cmbRange.Location = new System.Drawing.Point(612, 100);
			this.cmbRange.Name = "cmbRange";
			this.cmbRange.Size = new System.Drawing.Size(195, 40);
			this.cmbRange.TabIndex = 38;
			this.cmbRange.Text = "All";
			this.ToolTip1.SetToolTip(this.cmbRange, null);
			this.cmbRange.SelectedIndexChanged += new System.EventHandler(this.cmbRange_SelectedIndexChanged);
			// 
			// cmbLeased
			// 
			this.cmbLeased.AutoSize = false;
			this.cmbLeased.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbLeased.FormattingEnabled = true;
			this.cmbLeased.Items.AddRange(new object[] {
				"Leased Report",
				"Itemized Report",
				"Both"
			});
			this.cmbLeased.Location = new System.Drawing.Point(20, 30);
			this.cmbLeased.Name = "cmbLeased";
			this.cmbLeased.Size = new System.Drawing.Size(285, 40);
			this.cmbLeased.TabIndex = 8;
			this.cmbLeased.Text = "Both";
			this.ToolTip1.SetToolTip(this.cmbLeased, null);
			// 
			// Frame1
			// 
			this.Frame1.AppearanceKey = "groupBoxNoBorders";
			this.Frame1.Controls.Add(this.chkCategory);
			this.Frame1.Controls.Add(this.chkYearNew);
			this.Frame1.Controls.Add(this.Grid);
			this.Frame1.Location = new System.Drawing.Point(30, 30);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(425, 677);
			this.Frame1.TabIndex = 35;
			this.Frame1.Text = "Printing Format";
			this.ToolTip1.SetToolTip(this.Frame1, null);
			// 
			// chkCategory
			// 
			this.chkCategory.Location = new System.Drawing.Point(20, 650);
			this.chkCategory.Name = "chkCategory";
			this.chkCategory.Size = new System.Drawing.Size(157, 27);
			this.chkCategory.TabIndex = 38;
			this.chkCategory.Text = "Category in name";
			this.ToolTip1.SetToolTip(this.chkCategory, "Breakdown by account");
			this.chkCategory.Visible = false;
			// 
			// chkYearNew
			// 
			this.chkYearNew.Location = new System.Drawing.Point(197, 650);
			this.chkYearNew.Name = "chkYearNew";
			this.chkYearNew.Size = new System.Drawing.Size(133, 27);
			this.chkYearNew.TabIndex = 37;
			this.chkYearNew.Text = "Use Year New";
			this.ToolTip1.SetToolTip(this.chkYearNew, null);
			this.chkYearNew.Visible = false;
			// 
			// Grid
			// 
			this.Grid.AllowSelection = false;
			this.Grid.AllowUserToResizeColumns = false;
			this.Grid.AllowUserToResizeRows = false;
			this.Grid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.Grid.BackColorAlternate = System.Drawing.Color.Empty;
			this.Grid.BackColorBkg = System.Drawing.Color.Empty;
			this.Grid.BackColorFixed = System.Drawing.Color.Empty;
			this.Grid.BackColorSel = System.Drawing.Color.Empty;
			this.Grid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.Grid.Cols = 2;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.Grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.Grid.ColumnHeadersHeight = 30;
			this.Grid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.Grid.ColumnHeadersVisible = false;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.Grid.DefaultCellStyle = dataGridViewCellStyle2;
			this.Grid.DragIcon = null;
			this.Grid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.Grid.ExtendLastCol = true;
			this.Grid.FixedCols = 0;
			this.Grid.FixedRows = 0;
			this.Grid.ForeColorFixed = System.Drawing.Color.Empty;
			this.Grid.FrozenCols = 0;
			this.Grid.GridColor = System.Drawing.Color.Empty;
			this.Grid.GridColorFixed = System.Drawing.Color.Empty;
			this.Grid.Location = new System.Drawing.Point(0, 30);
			this.Grid.Name = "Grid";
			this.Grid.ReadOnly = true;
			this.Grid.RowHeadersVisible = false;
			this.Grid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.Grid.RowHeightMin = 0;
			this.Grid.Rows = 0;
			this.Grid.ScrollTipText = null;
			this.Grid.ShowColumnVisibilityMenu = false;
			this.Grid.ShowFocusCell = false;
			this.Grid.Size = new System.Drawing.Size(400, 601);
			this.Grid.StandardTab = true;
			this.Grid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.Grid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.Grid.TabIndex = 36;
			this.ToolTip1.SetToolTip(this.Grid, null);
			this.Grid.CurrentCellChanged += new System.EventHandler(this.Grid_RowColChange);
			this.Grid.DoubleClick += new System.EventHandler(this.Grid_DblClick);
			// 
			// fraRange
			// 
			this.fraRange.Controls.Add(this.txtStop);
			this.fraRange.Controls.Add(this.txtStart);
			this.fraRange.Controls.Add(this.lblStop);
			this.fraRange.Controls.Add(this.lblStart);
			this.fraRange.Enabled = false;
			this.fraRange.Location = new System.Drawing.Point(480, 170);
			this.fraRange.Name = "fraRange";
			this.fraRange.Size = new System.Drawing.Size(346, 150);
			this.fraRange.TabIndex = 21;
			this.fraRange.Text = "Range";
			this.ToolTip1.SetToolTip(this.fraRange, null);
			// 
			// txtStop
			// 
			this.txtStop.AutoSize = false;
			this.txtStop.BackColor = System.Drawing.SystemColors.Window;
			this.txtStop.LinkItem = null;
			this.txtStop.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtStop.LinkTopic = null;
			this.txtStop.Location = new System.Drawing.Point(117, 90);
			this.txtStop.Name = "txtStop";
			this.txtStop.Size = new System.Drawing.Size(209, 40);
			this.txtStop.TabIndex = 23;
			this.ToolTip1.SetToolTip(this.txtStop, null);
			// 
			// txtStart
			// 
			this.txtStart.AutoSize = false;
			this.txtStart.BackColor = System.Drawing.SystemColors.Window;
			this.txtStart.LinkItem = null;
			this.txtStart.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtStart.LinkTopic = null;
			this.txtStart.Location = new System.Drawing.Point(117, 30);
			this.txtStart.Name = "txtStart";
			this.txtStart.Size = new System.Drawing.Size(209, 40);
			this.txtStart.TabIndex = 22;
			this.ToolTip1.SetToolTip(this.txtStart, null);
			// 
			// lblStop
			// 
			this.lblStop.Location = new System.Drawing.Point(20, 104);
			this.lblStop.Name = "lblStop";
			this.lblStop.Size = new System.Drawing.Size(60, 20);
			this.lblStop.TabIndex = 25;
			this.lblStop.Text = "ENDING";
			this.ToolTip1.SetToolTip(this.lblStop, null);
			// 
			// lblStart
			// 
			this.lblStart.Location = new System.Drawing.Point(20, 44);
			this.lblStart.Name = "lblStart";
			this.lblStart.Size = new System.Drawing.Size(77, 20);
			this.lblStart.TabIndex = 24;
			this.lblStart.Text = "STARTING";
			this.ToolTip1.SetToolTip(this.lblStart, null);
			// 
			// fraCustomFormatOnly
			// 
			this.fraCustomFormatOnly.Controls.Add(this.chkAccount);
			this.fraCustomFormatOnly.Controls.Add(this.chkName);
			this.fraCustomFormatOnly.Controls.Add(this.chkMailingAddress);
			this.fraCustomFormatOnly.Controls.Add(this.chkLocation);
			this.fraCustomFormatOnly.Controls.Add(this.chkOpen1);
			this.fraCustomFormatOnly.Controls.Add(this.chkOpen2);
			this.fraCustomFormatOnly.Controls.Add(this.lblPrintWidth);
			this.fraCustomFormatOnly.Enabled = false;
			this.fraCustomFormatOnly.Location = new System.Drawing.Point(480, 356);
			this.fraCustomFormatOnly.Name = "fraCustomFormatOnly";
			this.fraCustomFormatOnly.Size = new System.Drawing.Size(346, 175);
			this.fraCustomFormatOnly.TabIndex = 10;
			this.fraCustomFormatOnly.Text = "Custom Format Only";
			this.ToolTip1.SetToolTip(this.fraCustomFormatOnly, null);
			// 
			// chkAccount
			// 
			this.chkAccount.Enabled = false;
			this.chkAccount.Location = new System.Drawing.Point(20, 30);
			this.chkAccount.Name = "chkAccount";
			this.chkAccount.Size = new System.Drawing.Size(86, 27);
			this.chkAccount.TabIndex = 16;
			this.chkAccount.Text = "Account";
			this.ToolTip1.SetToolTip(this.chkAccount, null);
			this.chkAccount.CheckedChanged += new System.EventHandler(this.chkAccount_CheckedChanged);
			// 
			// chkName
			// 
			this.chkName.Enabled = false;
			this.chkName.Location = new System.Drawing.Point(20, 77);
			this.chkName.Name = "chkName";
			this.chkName.Size = new System.Drawing.Size(71, 27);
			this.chkName.TabIndex = 15;
			this.chkName.Text = "Name";
			this.ToolTip1.SetToolTip(this.chkName, null);
			this.chkName.CheckedChanged += new System.EventHandler(this.chkName_CheckedChanged);
			// 
			// chkMailingAddress
			// 
			this.chkMailingAddress.Enabled = false;
			this.chkMailingAddress.Location = new System.Drawing.Point(20, 124);
			this.chkMailingAddress.Name = "chkMailingAddress";
			this.chkMailingAddress.Size = new System.Drawing.Size(144, 27);
			this.chkMailingAddress.TabIndex = 14;
			this.chkMailingAddress.Text = "Mailing Address";
			this.ToolTip1.SetToolTip(this.chkMailingAddress, null);
			this.chkMailingAddress.CheckedChanged += new System.EventHandler(this.chkMailingAddress_CheckedChanged);
			// 
			// chkLocation
			// 
			this.chkLocation.Enabled = false;
			this.chkLocation.Location = new System.Drawing.Point(184, 30);
			this.chkLocation.Name = "chkLocation";
			this.chkLocation.Size = new System.Drawing.Size(89, 27);
			this.chkLocation.TabIndex = 13;
			this.chkLocation.Text = "Location";
			this.ToolTip1.SetToolTip(this.chkLocation, null);
			this.chkLocation.CheckedChanged += new System.EventHandler(this.chkLocation_CheckedChanged);
			// 
			// chkOpen1
			// 
			this.chkOpen1.Enabled = false;
			this.chkOpen1.Location = new System.Drawing.Point(184, 77);
			this.chkOpen1.Name = "chkOpen1";
			this.chkOpen1.Size = new System.Drawing.Size(81, 27);
			this.chkOpen1.TabIndex = 12;
			this.chkOpen1.Text = "Open 1";
			this.ToolTip1.SetToolTip(this.chkOpen1, null);
			this.chkOpen1.CheckedChanged += new System.EventHandler(this.chkOpen1_CheckedChanged);
			// 
			// chkOpen2
			// 
			this.chkOpen2.Enabled = false;
			this.chkOpen2.Location = new System.Drawing.Point(184, 124);
			this.chkOpen2.Name = "chkOpen2";
			this.chkOpen2.Size = new System.Drawing.Size(81, 27);
			this.chkOpen2.TabIndex = 11;
			this.chkOpen2.Text = "Open 2";
			this.ToolTip1.SetToolTip(this.chkOpen2, null);
			this.chkOpen2.CheckedChanged += new System.EventHandler(this.chkOpen2_CheckedChanged);
			// 
			// lblPrintWidth
			// 
			this.lblPrintWidth.Location = new System.Drawing.Point(20, 194);
			this.lblPrintWidth.Name = "lblPrintWidth";
			this.lblPrintWidth.Size = new System.Drawing.Size(167, 20);
			this.lblPrintWidth.TabIndex = 17;
			this.lblPrintWidth.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.lblPrintWidth, null);
			// 
			// fraLeased
			// 
			this.fraLeased.AppearanceKey = "groupBoxNoBorders";
			this.fraLeased.Controls.Add(this.chkQTY);
			this.fraLeased.Controls.Add(this.cmbLeased);
			this.fraLeased.Controls.Add(this.Label5);
			this.fraLeased.Controls.Add(this.Label4);
			this.fraLeased.Location = new System.Drawing.Point(30, 727);
			this.fraLeased.Name = "fraLeased";
			this.fraLeased.Size = new System.Drawing.Size(330, 156);
			this.fraLeased.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.fraLeased, null);
			this.fraLeased.Visible = false;
			// 
			// chkQTY
			// 
			this.chkQTY.Location = new System.Drawing.Point(20, 77);
			this.chkQTY.Name = "chkQTY";
			this.chkQTY.Size = new System.Drawing.Size(141, 27);
			this.chkQTY.TabIndex = 7;
			this.chkQTY.Text = "Don\'t Print QTY";
			this.ToolTip1.SetToolTip(this.chkQTY, null);
			// 
			// Label5
			// 
			this.Label5.Location = new System.Drawing.Point(169, 124);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(142, 15);
			this.Label5.TabIndex = 9;
			this.Label5.Text = "RECENT CONVERSIONS)";
			this.ToolTip1.SetToolTip(this.Label5, null);
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(20, 124);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(169, 15);
			this.Label4.TabIndex = 8;
			this.Label4.Text = "(USUALLY ONLY USED IN";
			this.ToolTip1.SetToolTip(this.Label4, null);
			// 
			// Timer1
			// 
			this.Timer1.Enabled = false;
			this.Timer1.Interval = 500;
			this.Timer1.Location = new System.Drawing.Point(0, 0);
			this.Timer1.Name = null;
			// 
			// cmdPrint
			// 
			this.cmdPrint.AppearanceKey = "acceptButton";
			this.cmdPrint.Location = new System.Drawing.Point(365, 30);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPrint.Size = new System.Drawing.Size(127, 48);
			this.cmdPrint.TabIndex = 0;
			this.cmdPrint.Text = "Print Preview";
			this.ToolTip1.SetToolTip(this.cmdPrint, null);
			this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
			// 
			// datRatioOpens
			// 
			this.datRatioOpens.BackColor = System.Drawing.Color.FromName("@window");
			this.datRatioOpens.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("datRatioOpens.BackgroundImage")));
			this.datRatioOpens.Caption = "PPRatioOpens";
			this.datRatioOpens.ConnectAsString = "Access";
			this.datRatioOpens.DatabaseName = "";
			this.datRatioOpens.Location = new System.Drawing.Point(0, 0);
			this.datRatioOpens.Margin = new Wisej.Web.Padding(0);
			this.datRatioOpens.Name = "datRatioOpens";
			this.datRatioOpens.RecordSource = "";
			this.datRatioOpens.Size = new System.Drawing.Size(202, 22);
			this.datRatioOpens.TabIndex = 40;
			this.ToolTip1.SetToolTip(this.datRatioOpens, null);
			this.datRatioOpens.UserControlBackColor = System.Drawing.Color.FromName("@window");
			this.datRatioOpens.UserControlDrawStyle = ((short)(0));
			this.datRatioOpens.UserControlDrawWidth = ((short)(1));
			this.datRatioOpens.UserControlFillStyle = ((short)(1));
			this.datRatioOpens.UserControlForeColor = System.Drawing.SystemColors.ControlText;
			this.datRatioOpens.UserControlHeight = 330;
			this.datRatioOpens.UserControlWidth = 3030;
			this.datRatioOpens.Visible = false;
			// 
			// lblPrintMessage
			// 
			this.lblPrintMessage.AutoSize = true;
			this.lblPrintMessage.Location = new System.Drawing.Point(12, 40);
			this.lblPrintMessage.Name = "lblPrintMessage";
			this.lblPrintMessage.Size = new System.Drawing.Size(4, 14);
			this.lblPrintMessage.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.lblPrintMessage, null);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuPrintPreview,
				this.mnuSepar1,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuPrintPreview
			// 
			this.mnuPrintPreview.Index = 0;
			this.mnuPrintPreview.Name = "mnuPrintPreview";
			this.mnuPrintPreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuPrintPreview.Text = "Print Preview";
			this.mnuPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
			// 
			// mnuSepar1
			// 
			this.mnuSepar1.Index = 1;
			this.mnuSepar1.Name = "mnuSepar1";
			this.mnuSepar1.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// lblRange
			// 
			this.lblRange.AutoSize = true;
			this.lblRange.Location = new System.Drawing.Point(480, 114);
			this.lblRange.Name = "lblRange";
			this.lblRange.Size = new System.Drawing.Size(51, 15);
			this.lblRange.TabIndex = 39;
			this.lblRange.Text = "RANGE";
			this.ToolTip1.SetToolTip(this.lblRange, null);
			// 
			// frmPPPrinting
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(857, 666);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmPPPrinting";
			this.ShowInTaskbar = false;
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Print Account List";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmPPPrinting_Load);
			this.Activated += new System.EventHandler(this.frmPPPrinting_Activated);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPPPrinting_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPPPrinting_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.Frame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkCategory)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkYearNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraRange)).EndInit();
			this.fraRange.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraCustomFormatOnly)).EndInit();
			this.fraCustomFormatOnly.ResumeLayout(false);
			this.fraCustomFormatOnly.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMailingAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkOpen1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkOpen2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraLeased)).EndInit();
			this.fraLeased.ResumeLayout(false);
			this.fraLeased.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkQTY)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		public FCLabel lblRange;
	}
}