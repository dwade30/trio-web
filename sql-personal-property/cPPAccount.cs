﻿//Fecher vbPorter - Version 1.0.0.32
using fecherFoundation;
using Global;

namespace TWPP0000
{
	public class cPPAccount
	{
		//=========================================================
		private int lngID;
		private int lngAccount;
		private int lngPartyID;
		private cParty pOwner;
		private int lngTranCode;
		private int lngRealAssoc;
		private int lngStreetNumber;
		private string strStreet = "";
		private double dblValue;
		private double dblExemption;
		private int[] intExemptCode = new int[2 + 1];
		private int intBusinessCode;
		private int intStreetCode;
		private string strOpen1 = "";
		private string strOpen2 = "";
		/// <summary>
		/// Private intMO As Integer
		/// </summary>
		/// <summary>
		/// Private intDa As Integer
		/// </summary>
		/// <summary>
		/// Private intYR As Integer
		/// </summary>
		private double dblCompValue;
		private string strORCode = "";
		private string strUPDCode = "";
		private bool boolDeleted;
		private string strRBCode = "";
		private int lngSquareFootage;
		private string strStreetApt = "";
		private string strComment = "";
		private double[] dblExemptVal = new double[2 + 1];
		private string strAccountID = "";

		public string AccountID
		{
			get
			{
				string AccountID = "";
				AccountID = strAccountID;
				return AccountID;
			}
			set
			{
				strAccountID = value;
			}
		}

		public int RealAssoc
		{
			get
			{
				int RealAssoc = 0;
				RealAssoc = lngRealAssoc;
				return RealAssoc;
			}
			set
			{
				lngRealAssoc = value;
			}
		}

		public int StreetNumber
		{
			get
			{
				int StreetNumber = 0;
				StreetNumber = lngStreetNumber;
				return StreetNumber;
			}
			set
			{
				lngStreetNumber = value;
			}
		}

		public string Street
		{
			get
			{
				string Street = "";
				Street = strStreet;
				return Street;
			}
			set
			{
				strStreet = value;
			}
		}

		public double TotValue
		{
			get
			{
				double TotValue = 0;
				TotValue = dblValue;
				return TotValue;
			}
			set
			{
				dblValue = value;
			}
		}

		public double Exemption
		{
			get
			{
				double Exemption = 0;
				Exemption = dblExemption;
				return Exemption;
			}
			set
			{
				dblExemption = value;
			}
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public int BusinessCode
		{
			get
			{
				int BusinessCode = 0;
				BusinessCode = intBusinessCode;
				return BusinessCode;
			}
			set
			{
				intBusinessCode = value;
			}
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public int StreetCode
		{
			get
			{
				int StreetCode = 0;
				StreetCode = intStreetCode;
				return StreetCode;
			}
			set
			{
				intStreetCode = value;
			}
		}

		public string Open2
		{
			get
			{
				string Open2 = "";
				Open2 = strOpen2;
				return Open2;
			}
			set
			{
				strOpen2 = value;
			}
		}

		public string Open1
		{
			get
			{
				string Open1 = "";
				Open1 = strOpen1;
				return Open1;
			}
			set
			{
				strOpen1 = value;
			}
		}
		/// <summary>
		/// Public Property Get MO() As Integer
		/// </summary>
		/// <summary>
		/// MO = intMO
		/// </summary>
		/// <summary>
		/// End Property
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// Public Property Let MO(ByVal intM As Integer)
		/// </summary>
		/// <summary>
		/// intMO = intM
		/// </summary>
		/// <summary>
		/// End Property
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// Public Property Get DA() As Integer
		/// </summary>
		/// <summary>
		/// DA = intDa
		/// </summary>
		/// <summary>
		/// End Property
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// Public Property Let DA(ByVal intD As Integer)
		/// </summary>
		/// <summary>
		/// intDa = intD
		/// </summary>
		/// <summary>
		/// End Property
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// Public Property Get YR() As Integer
		/// </summary>
		/// <summary>
		/// YR = intYR
		/// </summary>
		/// <summary>
		/// End Property
		/// </summary>
		/// <summary>
		///
		/// </summary>
		/// <summary>
		/// Public Property Let YR(ByVal intY As Integer)
		/// </summary>
		/// <summary>
		/// intYR = intY
		/// </summary>
		/// <summary>
		/// End Property
		/// </summary>
		public double CompValue
		{
			get
			{
				double CompValue = 0;
				CompValue = dblCompValue;
				return CompValue;
			}
			set
			{
				dblCompValue = value;
			}
		}

		public string ORCode
		{
			get
			{
				string ORCode = "";
				ORCode = strORCode;
				return ORCode;
			}
			set
			{
				strORCode = value;
			}
		}

		public string UPDCode
		{
			get
			{
				string UPDCode = "";
				UPDCode = strUPDCode;
				return UPDCode;
			}
			set
			{
				strUPDCode = value;
			}
		}

		public bool Deleted
		{
			get
			{
				bool Deleted = false;
				Deleted = boolDeleted;
				return Deleted;
			}
			set
			{
				boolDeleted = value;
			}
		}

		public string RBCode
		{
			get
			{
				string RBCode = "";
				RBCode = strRBCode;
				return RBCode;
			}
			set
			{
				strRBCode = value;
			}
		}

		public int SquareFootage
		{
			get
			{
				int SquareFootage = 0;
				SquareFootage = lngSquareFootage;
				return SquareFootage;
			}
			set
			{
				lngSquareFootage = value;
			}
		}

		public string StreetApt
		{
			get
			{
				string StreetApt = "";
				StreetApt = strStreetApt;
				return StreetApt;
			}
			set
			{
				strStreetApt = value;
			}
		}

		public string Comment
		{
			get
			{
				string Comment = "";
				Comment = strComment;
				return Comment;
			}
			set
			{
				strComment = value;
			}
		}

		public void Set_ExemptCode(int intIndex, int intCode)
		{
			intExemptCode[intIndex] = intCode;
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short Get_ExemptCode(int intIndex)
		{
			short ExemptCode = 0;
			ExemptCode = FCConvert.ToInt16(intExemptCode[intIndex]);
			return ExemptCode;
		}

		public double Get_ExemptVal(int intIndex)
		{
			double ExemptVal = 0;
			ExemptVal = dblExemptVal[intIndex];
			return ExemptVal;
		}

		public void Set_ExemptVal(int intIndex, double dblValue)
		{
			dblExemptVal[intIndex] = dblValue;
		}

		public int ID
		{
			get
			{
				int ID = 0;
				ID = lngID;
				return ID;
			}
			set
			{
				lngID = value;
			}
		}

		public int Account
		{
			get
			{
				int Account = 0;
				Account = lngAccount;
				return Account;
			}
			set
			{
				lngAccount = value;
			}
		}

		public int PartyID
		{
			get
			{
				int PartyID = 0;
				PartyID = lngPartyID;
				return PartyID;
			}
			set
			{
				lngPartyID = value;
			}
		}

		public cParty OwnerParty()
		{
			cParty OwnerParty = null;
			OwnerParty = pOwner;
			return OwnerParty;
		}

		public int TranCode
		{
			get
			{
				int TranCode = 0;
				TranCode = lngTranCode;
				return TranCode;
			}
			set
			{
				lngTranCode = value;
			}
		}

		public void Clear()
		{
			lngID = 0;
			lngAccount = 0;
			lngPartyID = 0;
			lngTranCode = 0;
			pOwner = new cParty();
			intBusinessCode = 0;
			boolDeleted = false;
			int x;
			for (x = 1; x <= 2; x++)
			{
				intExemptCode[x] = 0;
				dblExemptVal[x] = 0;
			}
			// x
			dblExemption = 0;
			intStreetCode = 0;
			dblCompValue = 0;
			dblValue = 0;
			strComment = "";
			strOpen1 = "";
			strOpen2 = "";
			strORCode = "";
			strRBCode = "";
			strStreet = "";
			strStreetApt = "";
			strUPDCode = "";
			lngRealAssoc = 0;
			lngSquareFootage = 0;
			lngStreetNumber = 0;
			cCreateGUID tGuid = new cCreateGUID();
			strAccountID = tGuid.CreateGUID();
		}

		public cPPAccount() : base()
		{
			Clear();
		}
	}
}
