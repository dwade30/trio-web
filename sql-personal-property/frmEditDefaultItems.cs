﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmEditDefaultItems.
	/// </summary>
	public partial class frmEditDefaultItems : BaseForm
	{
		public frmEditDefaultItems()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmEditDefaultItems InstancePtr
		{
			get
			{
				return (frmEditDefaultItems)Sys.GetInstance(typeof(frmEditDefaultItems));
			}
		}

		protected frmEditDefaultItems _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		/// <summary>
		/// ********************************************************
		/// </summary>
		/// <summary>
		/// Property of TRIO Software Corporation
		/// </summary>
		/// <summary>
		/// Written By
		/// </summary>
		/// <summary>
		/// Date
		/// </summary>
		/// <summary>
		/// ********************************************************
		/// </summary>
		const int CNSTGRIDCOLid = 0;
		const int CNSTGRIDCOLCATEGORY = 1;
		const int CNSTGRIDCOLDESCRIPTION = 2;
		const int CNSTGRIDCOLSRO = 3;
		const int CNSTGRIDCOLYEAR = 4;
		const int CNSTGRIDCOLDPYR = 5;
		const int CNSTGRIDCOLCOST = 6;
		const int CNSTGRIDCOLSOURCE = 7;
		private bool boolChangesMade;

		public void Init()
		{
			boolChangesMade = false;
			//FC:FINAL:MSH - i.issue #1288: replace ShowDialog by Show for correct positioning and filling control values
			//this.ShowDialog();
			this.Show(FormShowEnum.Modal);
		}

		private void frmEditDefaultItems_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmEditDefaultItems_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmEditDefaultItems properties;
			//frmEditDefaultItems.FillStyle	= 0;
			//frmEditDefaultItems.ScaleWidth	= 9300;
			//frmEditDefaultItems.ScaleHeight	= 7575;
			//frmEditDefaultItems.LinkTopic	= "Form2";
			//frmEditDefaultItems.LockControls	= true;
			//frmEditDefaultItems.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
			LoadGrid();
			boolChangesMade = false;
		}

		private void SetupGrid()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				string strTemp;
				rsLoad.OpenRecordset("select * from RATIOTRENDS order by type", "twpp0000.vb1");
				strTemp = "";
				while (!rsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
					strTemp += "#" + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("type"))) + ";" + rsLoad.Get_Fields_String("description") + "|";
					rsLoad.MoveNext();
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				Grid.TextMatrix(0, CNSTGRIDCOLCATEGORY, "Category");
				Grid.TextMatrix(0, CNSTGRIDCOLDESCRIPTION, "Description");
				Grid.TextMatrix(0, CNSTGRIDCOLSRO, "SRO");
				Grid.TextMatrix(0, CNSTGRIDCOLYEAR, "Year");
				Grid.TextMatrix(0, CNSTGRIDCOLCOST, "Cost");
				Grid.TextMatrix(0, CNSTGRIDCOLSOURCE, "Source");
				Grid.TextMatrix(0, CNSTGRIDCOLDPYR, "DPYr");
				Grid.ColHidden(CNSTGRIDCOLid, true);
				Grid.ColComboList(CNSTGRIDCOLCATEGORY, strTemp);
				Grid.ColComboList(CNSTGRIDCOLSRO, "#0;Original|#1;Sound|#2;Replacement");
				Grid.ColAlignment(CNSTGRIDCOLYEAR, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				Grid.ColAlignment(CNSTGRIDCOLCOST, FCGrid.AlignmentSettings.flexAlignRightCenter);
				Grid.ColAlignment(CNSTGRIDCOLDPYR, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				Grid.ColDataType(CNSTGRIDCOLCOST, FCGrid.DataTypeSettings.flexDTLong);
				Grid.ColDataType(CNSTGRIDCOLYEAR, FCGrid.DataTypeSettings.flexDTLong);
				Grid.ColDataType(CNSTGRIDCOLDPYR, FCGrid.DataTypeSettings.flexDTLong);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SetupGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTGRIDCOLCATEGORY, FCConvert.ToInt32(0.25 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLDESCRIPTION, FCConvert.ToInt32(0.3 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLSRO, FCConvert.ToInt32(0.12 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLYEAR, FCConvert.ToInt32(0.05 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLDPYR, FCConvert.ToInt32(0.05 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLCOST, FCConvert.ToInt32(0.08 * GridWidth));
		}

		private void LoadGrid()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			int lngRow;
			try
			{
				// On Error GoTo ErrorHandler
				rsLoad.OpenRecordset("select * from DefaultItems order by description", "twpp0000.vb1");
				Grid.Rows = 1;
				while (!rsLoad.EndOfFile())
				{
					Grid.Rows += 1;
					lngRow = Grid.Rows - 1;
					// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
					Grid.TextMatrix(lngRow, CNSTGRIDCOLCATEGORY, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("category"))));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLDESCRIPTION, Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("description"))));
					// TODO Get_Fields: Check the table for the column [sro] and replace with corresponding Get_Field method
					Grid.TextMatrix(lngRow, CNSTGRIDCOLSRO, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("sro"))));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLYEAR, FCConvert.ToString(rsLoad.Get_Fields_Int32("purchaseyear")));
					// TODO Get_Fields: Check the table for the column [cost] and replace with corresponding Get_Field method
					Grid.TextMatrix(lngRow, CNSTGRIDCOLCOST, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("cost"))));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLSOURCE, Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("source"))));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLid, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("id"))));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLDPYR, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("depreciationyears"))));
					Grid.RowData(lngRow, false);
					rsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In LoadGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (boolChangesMade)
			{
				if (MessageBox.Show("Changes have been made" + "\r\n" + "Do you want to save first?", "Save Changes?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					if (!SaveData())
					{
						e.Cancel = true;
					}
				}
			}
		}

		private void frmEditDefaultItems_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Grid_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			Grid.RowData(Grid.Row, true);
			boolChangesMade = true;
		}

		private void Grid_KeyDownEvent(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Insert:
					{
						AddRow();
						break;
					}
				case Keys.Delete:
					{
						DeleteRow();
						break;
					}
			}
			//end switch
		}

		private void AddRow()
		{
			int lngRow;
			boolChangesMade = true;
			Grid.Rows += 1;
			lngRow = Grid.Rows - 1;
			Grid.TextMatrix(lngRow, CNSTGRIDCOLid, FCConvert.ToString(0));
			Grid.TextMatrix(lngRow, CNSTGRIDCOLCATEGORY, FCConvert.ToString(1));
			Grid.TextMatrix(lngRow, CNSTGRIDCOLCOST, FCConvert.ToString(0));
			Grid.TextMatrix(lngRow, CNSTGRIDCOLSRO, FCConvert.ToString(0));
			Grid.TextMatrix(lngRow, CNSTGRIDCOLYEAR, FCConvert.ToString(DateTime.Today.Year));
			Grid.RowData(lngRow, true);
			Grid.TopRow = lngRow;
		}

		private void DeleteRow()
		{
			int lngRow;
			if (Conversion.Val(Grid.TextMatrix(Grid.Row, CNSTGRIDCOLid)) > 0)
			{
				boolChangesMade = true;
				GridDelete.Rows += 1;
				lngRow = GridDelete.Rows - 1;
				GridDelete.TextMatrix(lngRow, 0, FCConvert.ToString(Conversion.Val(Grid.TextMatrix(Grid.Row, CNSTGRIDCOLid))));
			}
			Grid.RemoveItem(Grid.Row);
		}

		private void Grid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			boolChangesMade = true;
		}

		private void mnuAddItem_Click(object sender, System.EventArgs e)
		{
			AddRow();
		}

		private void mnuDeleteItem_Click(object sender, System.EventArgs e)
		{
			DeleteRow();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveData())
			{
                //FC:FINAL:AKV:#3603 - Form should stay open after saving
                //Close();
			}
		}

		private bool SaveData()
		{
			bool SaveData = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsSave = new clsDRWrapper();
				int lngRow;
				Grid.Row = 0;
				SaveData = false;
				for (lngRow = 0; lngRow <= GridDelete.Rows - 1; lngRow++)
				{
					rsSave.Execute("delete from defaultitems where id = " + FCConvert.ToString(Conversion.Val(GridDelete.TextMatrix(lngRow, 0))), "twpp0000.vb1");
				}
				// lngRow
				GridDelete.Rows = 0;
				for (lngRow = 1; lngRow <= Grid.Rows - 1; lngRow++)
				{
					if (FCConvert.ToBoolean(Grid.RowData(lngRow)))
					{
						if (Strings.Trim(Grid.TextMatrix(lngRow, CNSTGRIDCOLDESCRIPTION)) != string.Empty)
						{
							rsSave.OpenRecordset("select * from defaultitems where id = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLid))), "twpp0000.vb1");
							if (!rsSave.EndOfFile())
							{
								rsSave.Edit();
							}
							else
							{
								rsSave.AddNew();
							}
							// rsSave.Fields("category") = Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLid))
							rsSave.Set_Fields("category", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCATEGORY))));
							rsSave.Set_Fields("cost", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCOST))));
							rsSave.Set_Fields("sro", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLSRO))));
							rsSave.Set_Fields("purchaseyear", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLYEAR))));
							rsSave.Set_Fields("description", Strings.Trim(Grid.TextMatrix(lngRow, CNSTGRIDCOLDESCRIPTION)));
							rsSave.Set_Fields("source", Strings.Trim(Grid.TextMatrix(lngRow, CNSTGRIDCOLSOURCE)));
							rsSave.Set_Fields("depreciationyears", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLDPYR))));
							rsSave.Update();
							Grid.TextMatrix(lngRow, CNSTGRIDCOLid, FCConvert.ToString(rsSave.Get_Fields_Int32("id")));
							Grid.RowData(lngRow, false);
						}
						else
						{
							MessageBox.Show("Row " + FCConvert.ToString(lngRow) + " has no description" + "\r\n" + "Cannot save records with blank descriptions", "No Description", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							Grid.TopRow = lngRow;
							Grid.Row = lngRow;
							return SaveData;
						}
					}
				}
				// lngRow
				boolChangesMade = false;
				SaveData = true;
				MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveData;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SaveData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveData;
		}
	}
}
