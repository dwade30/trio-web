﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Core;
using Wisej.Web;
using System.IO;
using TWSharedLibrary;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptHighestAssess.
	/// </summary>
	public partial class rptHighestAssess : BaseSectionReport
	{
		public rptHighestAssess()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Highest Assessment Report";
		}

		public static rptHighestAssess InstancePtr
		{
			get
			{
				return (rptHighestAssess)Sys.GetInstance(typeof(rptHighestAssess));
			}
		}

		protected rptHighestAssess _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsAssess?.Dispose();
				clsRE?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptHighestAssess	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intPage;
		clsDRWrapper clsAssess = new clsDRWrapper();
		clsDRWrapper clsRE = new clsDRWrapper();
		//FileSystemObject fso = new FileSystemObject();
		bool boolRE;
		bool boolAccountFormat;
		// vbPorter upgrade warning: lngAssess As int	OnWrite(int, double)
		int lngAssess;
		double dblTotal;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsAssess.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//this.Printer.RenderMode = 1;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			dblTotal = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (boolAccountFormat)
			{
				txtAccount.Text = clsAssess.GetData("account").ToString();
			}
			else
			{
				txtAccount.Text = "";
			}
			txtName.Text = clsAssess.GetData("name").ToString();
			lngAssess = FCConvert.ToInt32(Math.Round(Conversion.Val(clsAssess.GetData("theassessment"))));
			if (boolRE)
			{
				if (boolAccountFormat)
				{
				}
				else
				{
					string strMasterJoinJoin = "";
					strMasterJoinJoin = modPPGN.GetREMasterJoinForJoin();
					// Call clsRE.OpenRecordset("select rsname,sum(lastlandval - rlexemption + lastbldgval) as totval where not rsdeleted = 1 and rsname = '" & txtName.Text & "'", "twre0000.vb1")
					clsRE.OpenRecordset("select rsname,sum(lastlandval - rlexemption + lastbldgval) as totval from " + strMasterJoinJoin + "  where not rsdeleted = 1 and rsname = '" + txtName.Text + "'", "twre0000.vb1");
					lngAssess += FCConvert.ToInt32(Conversion.Val(clsRE.GetData("totval")));
				}
			}
			txtAssess.Text = Strings.Format(lngAssess, "###,###,###,##0");
			dblTotal += lngAssess;
			clsAssess.MoveNext();
		}
		// vbPorter upgrade warning: intTop As short	OnWriteFCConvert.ToInt32(
		public void Init(bool boolByAccount, int intTop, bool boolUseRE)
		{
			boolUseRE = false;
			string strMasterJoinJoin;
			strMasterJoinJoin = modPPGN.GetMasterJoinForJoin();
			boolAccountFormat = boolByAccount;
			if (boolByAccount)
			{
				// Call clsAssess.OpenRecordset("select top " & intTop & " (value - VAL(exemption & '')) as theassessment ,name, account from ppmaster where not deleted = 1 order by (value - VAL(exemption & '')) desc", "twpp0000.vb1")
				clsAssess.OpenRecordset("select top " + FCConvert.ToString(intTop) + " (value - exemption ) as theassessment ,name, account from " + strMasterJoinJoin + " where not deleted = 1 order by (value - exemption) desc", "twpp0000.vb1");
			}
			else
			{
				// Call clsAssess.OpenRecordset("select top " & intTop & " sum(value - VAL(exemption & '')) as theassessment, name from ppmaster where not deleted = 1 group by name order by sum(value - VAL(exemption & '')) desc", "twpp0000.vb1")
				clsAssess.OpenRecordset("select top " + FCConvert.ToString(intTop) + " sum(value - exemption ) as theassessment, name from " + strMasterJoinJoin + " where not deleted = 1 group by name order by sum(value - exemption) desc", "twpp0000.vb1");
			}
			intPage = 1;
			if (File.Exists("twre0000.vb1"))
			{
				boolRE = boolUseRE;
			}
			else
			{
				boolRE = false;
			}
			// Me.Show
			// Call SetFixedSizeReport(Me, MDIParent.Grid)
			frmReportViewer.InstancePtr.Init(rptObj: this, boolAllowEmail: true, strAttachmentName: "HighestAssess");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = FCConvert.ToString(intPage);
			intPage += 1;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtTotalAssessment.Text = Strings.Format(dblTotal, "#,###,###,##0");
		}

		
	}
}
