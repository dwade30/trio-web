﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;
using fecherFoundation.VisualBasicLayer;
using System.Drawing;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmDBExtract.
	/// </summary>
	public partial class frmDBExtract : BaseForm
	{
		public frmDBExtract()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmDBExtract InstancePtr
		{
			get
			{
				return (frmDBExtract)Sys.GetInstance(typeof(frmDBExtract));
			}
		}

		protected frmDBExtract _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		/// <summary>
		/// ********************************************************
		/// </summary>
		/// <summary>
		/// Property of TRIO Software Corporation
		/// </summary>
		/// <summary>
		/// Written By
		/// </summary>
		/// <summary>
		/// Date
		/// </summary>
		/// <summary>
		/// ********************************************************
		/// </summary>
		int intLeasedStart;
		int intItemizedStart;
		int intAccountInfoStart;
		bool boolAccountInfo;
		bool boolLeased;
		bool boolItemized;
		string strFormatMessage;
		string[] strCats = new string[9 + 1];
		bool boolUsedList;
		bool boolUseHeadings;

		private void frmDBExtract_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmDBExtract_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmDBExtract properties;
			//frmDBExtract.FillStyle	= 0;
			//frmDBExtract.ScaleWidth	= 9045;
			//frmDBExtract.ScaleHeight	= 7290;
			//frmDBExtract.LinkTopic	= "Form2";
			//frmDBExtract.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			FillCombos();
			if (cmbOrder.Items.Contains("Open 1"))
			{
				cmbOrder.Items.Remove("Open 1");
				cmbOrder.Items.Insert(2, modPPGN.Statics.Open1Title);
			}
			if (cmbOrder.Items.Contains("Open 2"))
			{
				cmbOrder.Items.Remove("Open 2");
				cmbOrder.Items.Insert(3, modPPGN.Statics.Open2Title);
			}
			boolUsedList = false;
		}

		//private void lstAccountInfo_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	//FC:FINAL:CHN - i.issue #1553: drag and drop not enabled to list.
		//	/*MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
  //          int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
  //          float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
  //          float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
  //          intAccountInfoStart = lstAccountInfo.SelectedIndex;*/
		//	if (this.lstAccountInfo.SelectedItems == null || this.lstAccountInfo.SelectedItems.Count == 0)
		//	{
		//		return;
		//	}
		//	this.lstAccountInfo.DoDragDrop(this.lstAccountInfo.SelectedItems[0], DragDropEffects.Move);
		//	intAccountInfoStart = lstAccountInfo.SelectedIndex;
		//}

		//private void lstAccountInfo_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	bool boolSelected = false;
		//	int intTempRow = 0;
		//	string strTemp = "";
		//	//if (intAccountInfoStart != lstAccountInfo.SelectedIndex)
		//	//{
		//	//strTemp = lstAccountInfo.Items[lstAccountInfo.SelectedIndex].Text;
		//	//intTempRow = lstAccountInfo.ItemData(lstAccountInfo.SelectedIndex);
		//	//lstAccountInfo.Items[lstAccountInfo.ListIndex].Text = lstAccountInfo.Items[intAccountInfoStart].Text;
		//	//lstAccountInfo.ItemData(lstAccountInfo.ListIndex, lstAccountInfo.ItemData(intAccountInfoStart));
		//	//lstAccountInfo.Items[intAccountInfoStart].Text = strTemp;
		//	//lstAccountInfo.ItemData(intAccountInfoStart, intTempRow);
		//	//boolSelected = lstAccountInfo.Selected(lstAccountInfo.SelectedIndex);
		//	//lstAccountInfo.SetSelected(lstAccountInfo.ListIndex, lstAccountInfo.Selected(intAccountInfoStart));
		//	//lstAccountInfo.SetSelected(intAccountInfoStart, boolSelected);
		//	//}
		//}

		//private void lstItemized_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	intItemizedStart = lstItemized.SelectedIndex;
		//}

		//private void lstItemized_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	bool boolSelected = false;
		//	int intTempRow = 0;
		//	string strTemp = "";
		//	//if (intItemizedStart != lstItemized.SelectedIndex)
		//	//{
		//	//strTemp = lstItemized.Items[lstItemized.SelectedIndex].Text;
		//	//intTempRow = lstItemized.ItemData(lstItemized.SelectedIndex);
		//	//lstItemized.Items[lstItemized.ListIndex].Text = lstItemized.Items[intItemizedStart].Text;
		//	//lstItemized.ItemData(lstItemized.ListIndex, lstItemized.ItemData(intItemizedStart));
		//	//lstItemized.Items[intItemizedStart].Text = strTemp;
		//	//lstItemized.ItemData(intItemizedStart, intTempRow);
		//	//boolSelected = lstItemized.Selected(lstItemized.SelectedIndex);
		//	//lstItemized.SetSelected(lstItemized.ListIndex, lstItemized.Selected(intItemizedStart));
		//	//lstItemized.SetSelected(intItemizedStart, boolSelected);
		//	//}
		//}

		//private void lstLeased_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	intLeasedStart = lstLeased.SelectedIndex;
		//}

		//private void lstLeased_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	bool boolSelected = false;
		//	int intTempRow = 0;
		//	string strTemp = "";
		//	//if (intLeasedStart != lstLeased.SelectedIndex)
		//	//{
		//	//    strTemp = lstLeased.Items[lstLeased.SelectedIndex].Text;
		//	//    intTempRow = lstLeased.ItemData(lstLeased.SelectedIndex);
		//	//    lstLeased.Items[lstLeased.ListIndex].Text = lstLeased.Items[intLeasedStart].Text;
		//	//    lstLeased.ItemData(lstLeased.ListIndex, lstLeased.ItemData(intLeasedStart));
		//	//    lstLeased.Items[intLeasedStart].Text = strTemp;
		//	//    lstLeased.ItemData(intLeasedStart, intTempRow);
		//	//boolSelected = lstLeased.Selected(lstLeased.SelectedIndex);
		//	//lstLeased.SetSelected(lstLeased.ListIndex, lstLeased.Selected(intLeasedStart));
		//	//lstLeased.SetSelected(intLeasedStart, boolSelected);
		//	//}
		//}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void FillCombos()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			int x;
			for (x = 1; x <= 9; x++)
			{
				strCats[x] = "Category " + FCConvert.ToString(x);
			}
			// x
			clsLoad.OpenRecordset("select description,type from ratiotrends order by type", "twpp0000.vb1");
			while (!clsLoad.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
				strCats[clsLoad.Get_Fields("type")] = FCConvert.ToString(clsLoad.Get_Fields_String("description"));
				clsLoad.MoveNext();
			}
			lstAccountInfo.Items.Clear();
			lstAccountInfo.AddItem("Account Number");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 0);
			lstAccountInfo.AddItem("Real Estate Account");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 1);
			lstAccountInfo.AddItem("Name");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 2);
			lstAccountInfo.AddItem("Address 1");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 3);
			lstAccountInfo.AddItem("Address 2");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 4);
			lstAccountInfo.AddItem("Address 3");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 53);
			lstAccountInfo.AddItem("City");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 5);
			lstAccountInfo.AddItem("State");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 6);
			lstAccountInfo.AddItem("Zip");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 7);
			lstAccountInfo.AddItem("Zip4");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 8);
			// .AddItem "Telephone"
			// .ItemData(.NewIndex) = 9
			lstAccountInfo.AddItem("Street Number");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 10);
			lstAccountInfo.AddItem("Apartment Number");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 11);
			lstAccountInfo.AddItem("Street Name");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 12);
			lstAccountInfo.AddItem("Assessment");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 13);
			lstAccountInfo.AddItem("Assessment with BETE");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 32);
			lstAccountInfo.AddItem("Exemption");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 14);
			lstAccountInfo.AddItem("Exemption with BETE");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 33);
			lstAccountInfo.AddItem("Exempt Code 1");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 15);
			lstAccountInfo.AddItem("Exempt Code 2");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 16);
			lstAccountInfo.AddItem("Tran Code");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 17);
			lstAccountInfo.AddItem("Business Code");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 18);
			lstAccountInfo.AddItem("Street Code");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 19);
			lstAccountInfo.AddItem(modPPGN.Statics.Open1Title);
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 20);
			lstAccountInfo.AddItem(modPPGN.Statics.Open2Title);
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 21);
			lstAccountInfo.AddItem("Square Footage");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 22);
			lstAccountInfo.AddItem(strCats[1]);
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 23);
			lstAccountInfo.AddItem(strCats[2]);
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 24);
			lstAccountInfo.AddItem(strCats[3]);
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 25);
			lstAccountInfo.AddItem(strCats[4]);
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 26);
			lstAccountInfo.AddItem(strCats[5]);
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 27);
			lstAccountInfo.AddItem(strCats[6]);
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 28);
			lstAccountInfo.AddItem(strCats[7]);
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 29);
			lstAccountInfo.AddItem(strCats[8]);
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 30);
			lstAccountInfo.AddItem(strCats[9]);
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 31);
			lstAccountInfo.AddItem(strCats[1] + " BETE Exempt");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 34);
			lstAccountInfo.AddItem(strCats[2] + " BETE Exempt");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 35);
			lstAccountInfo.AddItem(strCats[3] + " BETE Exempt");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 36);
			lstAccountInfo.AddItem(strCats[4] + " BETE Exempt");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 37);
			lstAccountInfo.AddItem(strCats[5] + " BETE Exempt");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 38);
			lstAccountInfo.AddItem(strCats[6] + " BETE Exempt");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 39);
			lstAccountInfo.AddItem(strCats[7] + " BETE Exempt");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 40);
			lstAccountInfo.AddItem(strCats[8] + " BETE Exempt");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 41);
			lstAccountInfo.AddItem(strCats[9] + " BETE Exempt");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 42);
			lstAccountInfo.AddItem(strCats[1] + " with BETE");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 43);
			lstAccountInfo.AddItem(strCats[2] + " with BETE");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 44);
			lstAccountInfo.AddItem(strCats[3] + " with BETE");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 45);
			lstAccountInfo.AddItem(strCats[4] + " with BETE");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 46);
			lstAccountInfo.AddItem(strCats[5] + " with BETE");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 47);
			lstAccountInfo.AddItem(strCats[6] + " with BETE");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 48);
			lstAccountInfo.AddItem(strCats[7] + " with BETE");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 49);
			lstAccountInfo.AddItem(strCats[8] + " with BETE");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 50);
			lstAccountInfo.AddItem(strCats[9] + " with BETE");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 51);
			lstAccountInfo.AddItem("Total BETE Exempt");
			lstAccountInfo.ItemData(lstAccountInfo.NewIndex, 52);
			// 
			// For x = 0 To .ListCount - 1
			// .ItemData(x) = x
			// Next x
			lstItemized.Items.Clear();
			lstItemized.AddItem("Account Number");
			lstItemized.ItemData(lstItemized.NewIndex, 0);
			lstItemized.AddItem("Name");
			lstItemized.ItemData(lstItemized.NewIndex, 1);
			lstItemized.AddItem("Line Number");
			lstItemized.ItemData(lstItemized.NewIndex, 2);
			lstItemized.AddItem("Reimbursable");
			lstItemized.ItemData(lstItemized.NewIndex, 3);
			lstItemized.AddItem("Code");
			lstItemized.ItemData(lstItemized.NewIndex, 4);
			lstItemized.AddItem("Quantity");
			lstItemized.ItemData(lstItemized.NewIndex, 5);
			lstItemized.AddItem("Description");
			lstItemized.ItemData(lstItemized.NewIndex, 6);
			lstItemized.AddItem("Year First Assessed");
			lstItemized.ItemData(lstItemized.NewIndex, 19);
			lstItemized.AddItem("Years Claimed");
			lstItemized.ItemData(lstItemized.NewIndex, 21);
			lstItemized.AddItem("Value Replacing");
			lstItemized.ItemData(lstItemized.NewIndex, 20);
			lstItemized.AddItem("Month");
			lstItemized.ItemData(lstItemized.NewIndex, 7);
			lstItemized.AddItem("Year");
			lstItemized.ItemData(lstItemized.NewIndex, 8);
			lstItemized.AddItem("Depreciation Years");
			lstItemized.ItemData(lstItemized.NewIndex, 9);
			lstItemized.AddItem("SRO");
			lstItemized.ItemData(lstItemized.NewIndex, 10);
			lstItemized.AddItem("Replacement Cost Year");
			lstItemized.ItemData(lstItemized.NewIndex, 11);
			lstItemized.AddItem("Cost");
			lstItemized.ItemData(lstItemized.NewIndex, 12);
			lstItemized.AddItem("Percent Good");
			lstItemized.ItemData(lstItemized.NewIndex, 13);
			lstItemized.AddItem("Factor");
			lstItemized.ItemData(lstItemized.NewIndex, 14);
			lstItemized.AddItem("Value");
			lstItemized.ItemData(lstItemized.NewIndex, 15);
			lstItemized.AddItem("Leased To");
			lstItemized.ItemData(lstItemized.NewIndex, 16);
			lstItemized.AddItem("Age");
			lstItemized.ItemData(lstItemized.NewIndex, 17);
			lstItemized.AddItem("Depreciation Age");
			lstItemized.ItemData(lstItemized.NewIndex, 18);
			lstLeased.Items.Clear();
			lstLeased.AddItem("Account Number");
			lstLeased.ItemData(lstLeased.NewIndex, 0);
			lstLeased.AddItem("Name");
			lstLeased.ItemData(lstLeased.NewIndex, 1);
			lstLeased.AddItem("Line Number");
			lstLeased.ItemData(lstLeased.NewIndex, 2);
			lstLeased.AddItem("Reimbursable");
			lstLeased.ItemData(lstLeased.NewIndex, 3);
			lstLeased.AddItem("Code");
			lstLeased.ItemData(lstLeased.NewIndex, 4);
			lstLeased.AddItem("Quantity");
			lstLeased.ItemData(lstLeased.NewIndex, 5);
			lstLeased.AddItem("Description");
			lstLeased.ItemData(lstLeased.NewIndex, 6);
			lstLeased.AddItem("Year First Assessed");
			lstLeased.ItemData(lstLeased.NewIndex, 16);
			lstLeased.AddItem("Years Claimed");
			lstLeased.ItemData(lstLeased.NewIndex, 20);
			lstLeased.AddItem("Value Replacing");
			lstLeased.ItemData(lstLeased.NewIndex, 17);
			lstLeased.AddItem("Cost");
			lstLeased.ItemData(lstLeased.NewIndex, 7);
			lstLeased.AddItem("Lease Date");
			lstLeased.ItemData(lstLeased.NewIndex, 8);
			lstLeased.AddItem("Months Leased");
			lstLeased.ItemData(lstLeased.NewIndex, 9);
			lstLeased.AddItem("Lease/Lease Purchase");
			lstLeased.ItemData(lstLeased.NewIndex, 10);
			lstLeased.AddItem("Monthly Rent");
			lstLeased.ItemData(lstLeased.NewIndex, 11);
			lstLeased.AddItem("Percent Good");
			lstLeased.ItemData(lstLeased.NewIndex, 12);
			lstLeased.AddItem("Factor");
			lstLeased.ItemData(lstLeased.NewIndex, 13);
			lstLeased.AddItem("Value");
			lstLeased.ItemData(lstLeased.NewIndex, 14);
			lstLeased.AddItem("Leased From");
			lstLeased.ItemData(lstLeased.NewIndex, 15);
			lstLeased.AddItem("Age");
			lstLeased.ItemData(lstLeased.NewIndex, 18);
			lstLeased.AddItem("Depreciation Age");
			lstLeased.ItemData(lstLeased.NewIndex, 19);
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			// first check that they chose at least one field
			// then ask for the filename and location
			// then create the file
			// then show a report with the file name and location, and the file format
			int x;
			bool boolSelected;
			string strTemp = "";
			try
			{
				// On Error GoTo ErrorHandler
				if (chkColumnHeaders.CheckState == CheckState.Checked)
				{
					boolUseHeadings = true;
				}
				else
				{
					boolUseHeadings = false;
				}
				boolLeased = false;
				boolItemized = false;
				boolAccountInfo = false;
				strFormatMessage = "";
				if (cmbFileType.SelectedIndex == 0)
				{
					boolAccountInfo = true;
				}
				else if (cmbFileType.SelectedIndex == 1)
				{
					boolItemized = true;
				}
				else if (cmbFileType.SelectedIndex == 2)
				{
					boolLeased = true;
				}
				boolSelected = false;
				if (boolLeased)
				{
					for (x = 0; x <= lstLeased.Items.Count - 1; x++)
					{
						if (lstLeased.Selected(x))
						{
							boolSelected = true;
							break;
						}
					}
					// x
				}
				else if (boolItemized)
				{
					for (x = 0; x <= lstItemized.Items.Count - 1; x++)
					{
						if (lstItemized.Selected(x))
						{
							boolSelected = true;
							break;
						}
					}
					// x
				}
				else
				{
					for (x = 0; x <= lstAccountInfo.Items.Count - 1; x++)
					{
						if (lstAccountInfo.Selected(x))
						{
							boolSelected = true;
							break;
						}
					}
					// x
				}
				if (!boolSelected)
				{
					MessageBox.Show("You have not selected any fields to save in the extract", "No Fields Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (cmbRange.SelectedItem == "Range")
				{
					if (Strings.Trim(txtStart.Text) == string.Empty || Strings.Trim(txtEnd.Text) == string.Empty)
					{
						MessageBox.Show("You have not chosen a valid range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (cmbOrder.SelectedIndex == 1 || cmbOrder.SelectedIndex == 2 || cmbOrder.SelectedIndex == 3)
					{
						// text field
						if (Strings.CompareString(txtStart.Text, ">", txtEnd.Text))
						{
							MessageBox.Show("You have entered an invalid range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
					}
					else
					{
						// numeric field
						if (Conversion.Val(txtStart.Text) > Conversion.Val(txtEnd.Text))
						{
							MessageBox.Show("You have entered an invalid range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
					}
				}
				// get filename and location
				// MDIParent.InstancePtr.CommonDialog1.Flags = vbPorterConverter.cdlOFNExplorer+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				MDIParent.InstancePtr.CommonDialog1.DefaultExt = "asc";
				if (boolLeased)
				{
					strTemp = "PPLeased";
				}
				else if (boolItemized)
				{
					strTemp = "PPItemized";
				}
				else
				{
					strTemp = "PPAccountInfo";
				}
				MDIParent.InstancePtr.CommonDialog1.FileName = strTemp + ".asc";
				// MDIParent.CommonDialog1.CancelError = False
				MDIParent.InstancePtr.CommonDialog1.InitDir = FCFileSystem.CurDir();
				//FC:FINAL:RPU:#i1469 - save file to UserData folder, and download to client
				//MDIParent.InstancePtr.CommonDialog1.ShowSave();
				strTemp = MDIParent.InstancePtr.CommonDialog1.FileName;
				if (Strings.Trim(strTemp) == string.Empty)
				{
					return;
				}
				if (cmbRange.SelectedItem == "Individual")
				{
					if (boolUsedList)
					{
						if (MessageBox.Show("Would you like to use the last account list created?", "Use Last List?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
						{
							if (!frmGetAccountsToPrint.InstancePtr.Init("Accounts to extract"))
							{
								return;
							}
						}
					}
					else
					{
						if (!frmGetAccountsToPrint.InstancePtr.Init("Accounts to extract"))
						{
							return;
						}
						else
						{
							boolUsedList = true;
						}
					}
				}
				if (boolLeased)
				{
					if (!CreateLeasedExtract(ref strTemp))
					{
						return;
					}
				}
				else if (boolItemized)
				{
					if (!CreateItemizedExtract(ref strTemp))
					{
						return;
					}
				}
				else
				{
					if (!CreateAccountExtract(ref strTemp))
					{
						return;
					}
				}
				MessageBox.Show("Database extract has finished writing", "Finished", MessageBoxButtons.OK, MessageBoxIcon.Information);
				//! Load frmGenericMessage;
				frmGenericMessage.InstancePtr.Changemessage(strFormatMessage);
				frmGenericMessage.InstancePtr.Show(App.MainForm);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (Information.Err().Number == 32755)
				{
					Information.Err().Clear();
					return;
				}
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In mnuSaveContinue", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void optFileType_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
					{
						lstAccountInfo.Visible = true;
						lstItemized.Visible = false;
						lstLeased.Visible = false;
						break;
					}
				case 1:
					{
						lstAccountInfo.Visible = false;
						lstItemized.Visible = true;
						lstLeased.Visible = false;
						break;
					}
				case 2:
					{
						lstAccountInfo.Visible = false;
						lstItemized.Visible = false;
						lstLeased.Visible = true;
						break;
					}
			}
			//end switch
		}

		private void optFileType_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = FCConvert.ToInt16(cmbFileType.SelectedIndex);
			optFileType_CheckedChanged(index, sender, e);
		}
		// vbPorter upgrade warning: strFileName As object	OnWrite(string)
		private bool CreateLeasedExtract(ref string strFileName)
		{
			bool CreateLeasedExtract = false;
			StreamWriter ts = null;
			string strTemp;
			int x;
			string strSQL = "";
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strWhere = "";
			string strOrder = "";
			bool boolRange = false;
			bool boolSpecific = false;
			string strStart = "";
			string strEnd = "";
			string strOutput = "";
			string strHeadings = "";
			string strLeaseDate = "";
			int lngTempYear = 0;
			int lngAge = 0;
			int lngYearNew = 0;
			int lngYearNow = 0;
			int lngDepYrs = 0;
			try
			{
				// On Error GoTo ErrorHandler
				CreateLeasedExtract = false;
				GetCostData();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				strTemp = Path.GetFileName(FCConvert.ToString(strFileName));
				//FC:FINAL:RPU:#1381 - Remove unneeded text
				//strFormatMessage = "The extract is named " + strTemp + ", is found in ";
				strFormatMessage = "The extract is named " + strTemp;
				strTemp = Path.GetDirectoryName(FCConvert.ToString(strFileName));
				strFormatMessage += strTemp + " and is in a comma delimited format in the following order:";
				//FC:FINAL:RPU:#i1554 - save file to UserData folder and download it to client
				string tempPath = Path.Combine(FCFileSystem.CurDir(), Path.GetRandomFileName());
				tempPath = Path.ChangeExtension(tempPath, ".asc");
				//ts = File.CreateText(strFileName);
				ts = File.CreateText(tempPath);
				for (x = 0; x <= lstLeased.Items.Count - 1; x++)
				{
					if (lstLeased.Selected(x))
					{
						switch (lstLeased.ItemData(x))
						{
							case 0:
								{
									strTemp = "Account Number";
									break;
								}
							case 1:
								{
									strTemp = "Name";
									break;
								}
							case 2:
								{
									strTemp = "Line Number";
									break;
								}
							case 3:
								{
									strTemp = "Reimbursable";
									break;
								}
							case 4:
								{
									strTemp = "Code";
									break;
								}
							case 5:
								{
									strTemp = "Quantity";
									break;
								}
							case 6:
								{
									strTemp = "Description";
									break;
								}
							case 7:
								{
									strTemp = "Cost";
									break;
								}
							case 8:
								{
									strTemp = "Lease Date";
									break;
								}
							case 9:
								{
									strTemp = "Months Leased";
									break;
								}
							case 10:
								{
									strTemp = "Lease/Lease Purchase";
									break;
								}
							case 11:
								{
									strTemp = "Monthly Rent";
									break;
								}
							case 12:
								{
									strTemp = "Percent Good";
									break;
								}
							case 13:
								{
									strTemp = "Factor";
									break;
								}
							case 14:
								{
									strTemp = "Value";
									break;
								}
							case 15:
								{
									strTemp = "Leased From";
									break;
								}
							case 16:
								{
									strTemp = "Year First Assessed";
									break;
								}
							case 17:
								{
									strTemp = "Value Replacing";
									break;
								}
							case 18:
								{
									strTemp = "Age";
									break;
								}
							case 19:
								{
									strTemp = "Depreciation Age";
									break;
								}
							case 20:
								{
									strTemp = "Years Claimed";
									break;
								}
						}
						//end switch
						strFormatMessage += "\r\n" + strTemp;
						strHeadings += FCConvert.ToString(Convert.ToChar(34)) + strTemp + FCConvert.ToString(Convert.ToChar(34)) + ",";
					}
				}
				// x
				if (strHeadings != string.Empty)
				{
					strHeadings = Strings.Mid(strHeadings, 1, strHeadings.Length - 1);
				}
				if (cmbRange.SelectedItem == "All")
				{
					boolRange = false;
					boolSpecific = false;
				}
				else if (cmbRange.SelectedItem == "Range")
				{
					boolRange = true;
					boolSpecific = false;
				}
				else if (cmbRange.SelectedItem == "Individual")
				{
					boolRange = false;
					boolSpecific = true;
				}
				string strMasterJoin;
				strMasterJoin = modPPGN.GetMasterJoinForJoin();
				if (!boolSpecific)
				{
					// strSQL = "Select ppleased.*,ppmaster.name from ppmaster inner join ppleased on (ppmaster.account = ppleased.account) "
					strSQL = "Select ppleased.*,name from " + strMasterJoin + " inner join ppleased on (mj.account = ppleased.account) ";
				}
				else
				{
					// strSQL = "Select ppleased.*,ppmaster.name from ppleased inner join (ppmaster inner join accountlist on (ppmaster.account = accountlist.account)) on (ppmaster.account = ppleased.account) "
					strSQL = "Select ppleased.*,mj.name from ppleased inner join (" + strMasterJoin + " inner join accountlist on (mj.account = accountlist.account)) on (mj.account = ppleased.account) ";
				}
				if (cmbOrder.SelectedIndex == 0)
				{
					// account
					strOrder = " order by ppleased.account ";
					if (boolRange)
					{
						strWhere = " where ppleased.account between " + FCConvert.ToString(Conversion.Val(txtStart.Text)) + " and " + FCConvert.ToString(Conversion.Val(txtEnd.Text)) + " ";
					}
				}
				else if (cmbOrder.SelectedIndex == 1)
				{
					// name
					strOrder = " order by name";
					if (boolRange)
					{
						strWhere = " where name between '" + txtStart.Text + "' and '" + txtEnd.Text + "zzz' ";
					}
				}
				else if (cmbOrder.SelectedIndex == 2)
				{
					// open1
					strOrder = " order by open1";
					if (boolRange)
					{
						strWhere = " where open1 between '" + txtStart.Text + "' and '" + txtEnd.Text + "zzz' ";
					}
				}
				else if (cmbOrder.SelectedIndex == 3)
				{
					// open2
					strOrder = " order by open2";
					if (boolRange)
					{
						strWhere = " where open2 between '" + txtStart.Text + "' and '" + txtEnd.Text + "zzz' ";
					}
				}
				else if (cmbOrder.SelectedIndex == 4)
				{
					// business code
					strOrder = " order by open2";
					if (boolRange)
					{
						strWhere = " where businesscode between " + FCConvert.ToString(Conversion.Val(txtStart.Text)) + " and " + FCConvert.ToString(Conversion.Val(txtEnd.Text)) + " ";
					}
				}
				else if (cmbOrder.SelectedIndex == 5)
				{
					// tran code
					strOrder = " order by trancode";
					if (boolRange)
					{
						strWhere = " where businesscode between " + FCConvert.ToString(Conversion.Val(txtStart.Text)) + " and " + FCConvert.ToString(Conversion.Val(txtEnd.Text)) + " ";
					}
				}
				else if (cmbOrder.SelectedIndex == 6)
				{
					// street code
					strOrder = " order by streetcode";
					if (boolRange)
					{
						strWhere = " where trancode between " + FCConvert.ToString(Conversion.Val(txtStart.Text)) + " and " + FCConvert.ToString(Conversion.Val(txtEnd.Text)) + " ";
					}
				}
				if (strWhere != string.Empty)
				{
					strWhere += " and not mj.deleted = 1 ";
				}
				else
				{
					strWhere = " where not mj.deleted = 1 ";
				}
				if (cmbOrder.SelectedIndex == 0)
				{
					strOrder += ",line";
				}
				else
				{
					strOrder += "ppleased.account,line";
				}
				if (boolUseHeadings)
				{
					ts.WriteLine(strHeadings);
				}
				clsLoad.OpenRecordset(strSQL + strWhere + strOrder, "twpp0000.vb1");
				while (!clsLoad.EndOfFile())
				{
					strOutput = "";
					for (x = 0; x <= lstLeased.Items.Count - 1; x++)
					{
						if (lstLeased.Selected(x))
						{
							switch (lstLeased.ItemData(x))
							{
								case 0:
									{
										// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
										strOutput += clsLoad.Get_Fields("account") + ",";
										break;
									}
								case 1:
									{
										strOutput += FCConvert.ToString(Convert.ToChar(34)) + clsLoad.Get_Fields_String("name") + FCConvert.ToString(Convert.ToChar(34)) + ",";
										break;
									}
								case 2:
									{
										// TODO Get_Fields: Check the table for the column [line] and replace with corresponding Get_Field method
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("line"))) + ",";
										break;
									}
								case 3:
									{
										// TODO Get_Fields: Check the table for the column [rb] and replace with corresponding Get_Field method
										if (Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("rb"))) == "*")
										{
											strOutput += "TRUE" + ",";
										}
										else
										{
											strOutput += "FALSE" + ",";
										}
										break;
									}
								case 4:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("cd"))) + ",";
										break;
									}
								case 5:
									{
										// TODO Get_Fields: Check the table for the column [quantity] and replace with corresponding Get_Field method
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("quantity"))) + ",";
										break;
									}
								case 6:
									{
										strOutput += FCConvert.ToString(Convert.ToChar(34)) + clsLoad.Get_Fields_String("description") + FCConvert.ToString(Convert.ToChar(34)) + ",";
										break;
									}
								case 7:
									{
										// TODO Get_Fields: Check the table for the column [cost] and replace with corresponding Get_Field method
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("cost"))) + ",";
										break;
									}
								case 8:
									{
										strOutput += FCConvert.ToString(Convert.ToChar(34)) + clsLoad.Get_Fields_String("Leasedate") + FCConvert.ToString(Convert.ToChar(34)) + ",";
										break;
									}
								case 9:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int16("Mos"))) + ",";
										break;
									}
								case 10:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int16("l1p2"))) + ",";
										break;
									}
								case 11:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Decimal("monthlyrent"))) + ",";
										break;
									}
								case 12:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int16("depyrs"))) + ",";
										// depyrs is actually used for percent good
										break;
									}
								case 13:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int16("fctr"))) + ",";
										break;
									}
								case 14:
									{
										// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("value"))) + ",";
										break;
									}
								case 15:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("leasedfrom"))) + ",";
										break;
									}
								case 16:
									{
										// year first assessed
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("YEARFIRSTASSESSED"))) + ",";
										break;
									}
								case 17:
									{
										// AMOUNT replacing
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("amountreplacing"))) + ",";
										break;
									}
								case 18:
									{
										// age
										if (Conversion.Val(clsLoad.Get_Fields_String("cd")) > 0)
										{
											lngYearNow = DateTime.Today.Year;
											if (modGlobal.Statics.CustomizeStuff.DepreciationYear > 1900)
											{
												lngYearNow = modGlobal.Statics.CustomizeStuff.DepreciationYear;
											}
											lngYearNew = lngYearNow;
											strLeaseDate = FCConvert.ToString(clsLoad.Get_Fields_String("Leasedate"));
											if (strLeaseDate.Length >= 4)
											{
												if (Strings.Mid(strLeaseDate, 3, 1) == "/")
												{
													lngTempYear = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(strLeaseDate, 4, 2))));
												}
												else
												{
													lngTempYear = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(strLeaseDate, 3, 2))));
												}
												lngTempYear = 1900 + lngTempYear;
												if (lngTempYear < 1926)
													lngTempYear += 100;
												lngYearNew = lngTempYear;
											}
											lngAge = lngYearNow - lngYearNew;
											if (lngAge < 0)
												lngAge = 0;
											strOutput += FCConvert.ToString(lngAge) + ",";
										}
										else
										{
											strOutput += "0,";
										}
										break;
									}
								case 19:
									{
										// depreciation Age
										if (Conversion.Val(clsLoad.Get_Fields_String("cd")) > 0)
										{
											lngYearNow = DateTime.Today.Year;
											if (modGlobal.Statics.CustomizeStuff.DepreciationYear > 1900)
											{
												lngYearNow = modGlobal.Statics.CustomizeStuff.DepreciationYear;
											}
											lngYearNew = lngYearNow;
											strLeaseDate = FCConvert.ToString(clsLoad.Get_Fields_String("Leasedate"));
											if (strLeaseDate.Length >= 4)
											{
												if (Strings.Mid(strLeaseDate, 3, 1) == "/")
												{
													lngTempYear = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(strLeaseDate, 4, 2))));
												}
												else
												{
													lngTempYear = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(strLeaseDate, 3, 2))));
												}
												lngTempYear = 1900 + lngTempYear;
												if (lngTempYear < 1926)
													lngTempYear += 100;
												lngYearNew = lngTempYear;
											}
											lngAge = lngYearNow - lngYearNew;
											if (lngAge < 0)
												lngAge = 0;
											lngDepYrs = modPPCalculate.Statics.DefLife[FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_String("cd"))))];
											if (lngAge > lngDepYrs)
											{
												lngAge = lngDepYrs;
											}
											strOutput += FCConvert.ToString(lngAge) + ",";
										}
										else
										{
											strOutput += "0,";
										}
										break;
									}
								case 20:
									{
										// years claimed
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("yearsclaimed"))) + ",";
										break;
									}
							}
							//end switch
						}
					}
					// x
					if (strOutput != string.Empty)
					{
						// get rid of last comma
						strOutput = Strings.Mid(strOutput, 1, strOutput.Length - 1);
					}
					ts.Write(strOutput);
					clsLoad.MoveNext();
					if (!clsLoad.EndOfFile())
					{
						ts.WriteLine();
						// if not the last line then add a new line
					}
				}
				ts.Close();
				//FC:FINAL:RPU:#i1554 - Download the file to client
				FCUtils.Download(tempPath, strFileName);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				CreateLeasedExtract = true;
				return CreateLeasedExtract;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				ts.Close();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In CreateleasedExtract", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateLeasedExtract;
		}

		private bool CreateItemizedExtract(ref string strFileName)
		{
			bool CreateItemizedExtract = false;
			StreamWriter ts = null;
			string strTemp;
			int x;
			string strSQL = "";
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strWhere = "";
			string strOrder = "";
			bool boolRange = false;
			bool boolSpecific = false;
			string strStart = "";
			string strEnd = "";
			string strOutput = "";
			string strHeadings;
			int lngYearNow = 0;
			int lngYearNew = 0;
			int lngAge = 0;
			int lngDepYrs = 0;
			try
			{
				// On Error GoTo ErrorHandler
				string strMasterJoin;
				strMasterJoin = modPPGN.GetMasterJoinForJoin();
				GetCostData();
				CreateItemizedExtract = false;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				strTemp = Path.GetFileName(strFileName);
				//FC:FINAL:RPU:#1381 - Remove unneeded text
				//strFormatMessage = "The extract is named " + strTemp + ", is found in ";
				strFormatMessage = "The extract is named " + strTemp;
				strTemp = Path.GetDirectoryName(strFileName);
				strFormatMessage += strTemp + " and is in a comma delimited format in the following order:";
				//FC:FINAL:RPU:#i1554 - save file to UserData folder and download it to client
				string tempPath = Path.Combine(FCFileSystem.CurDir(), Path.GetRandomFileName());
				tempPath = Path.ChangeExtension(tempPath, ".asc");
				//ts = File.CreateText(strFileName);
				ts = File.CreateText(tempPath);
				strHeadings = "";
				for (x = 0; x <= lstItemized.Items.Count - 1; x++)
				{
					if (lstItemized.Selected(x))
					{
						switch (lstItemized.ItemData(x))
						{
							case 0:
								{
									strTemp = "Account Number";
									break;
								}
							case 1:
								{
									strTemp = "Name";
									break;
								}
							case 2:
								{
									strTemp = "Line Number";
									break;
								}
							case 3:
								{
									strTemp = "Reimburseable";
									break;
								}
							case 4:
								{
									strTemp = "Code";
									break;
								}
							case 5:
								{
									strTemp = "Quantity";
									break;
								}
							case 6:
								{
									strTemp = "Description";
									break;
								}
							case 7:
								{
									strTemp = "Month";
									break;
								}
							case 8:
								{
									strTemp = "Year";
									break;
								}
							case 9:
								{
									strTemp = "Depreciation Years";
									break;
								}
							case 10:
								{
									strTemp = "SRO";
									break;
								}
							case 11:
								{
									strTemp = "Replacement Cost Year";
									break;
								}
							case 12:
								{
									strTemp = "Cost";
									break;
								}
							case 13:
								{
									strTemp = "Percent Good";
									break;
								}
							case 14:
								{
									strTemp = "Factor";
									break;
								}
							case 15:
								{
									strTemp = "Value";
									break;
								}
							case 16:
								{
									strTemp = "Leased To";
									break;
								}
							case 17:
								{
									strTemp = "Age";
									break;
								}
							case 18:
								{
									strTemp = "Depreciation Age";
									break;
								}
							case 19:
								{
									strTemp = "Year First Assessed";
									break;
								}
							case 20:
								{
									strTemp = "Value Replacing";
									break;
								}
							case 21:
								{
									strTemp = "Years Claimed";
									break;
								}
						}
						//end switch
						strFormatMessage += "\r\n" + strTemp;
						strHeadings += FCConvert.ToString(Convert.ToChar(34)) + strTemp + FCConvert.ToString(Convert.ToChar(34)) + ",";
					}
				}
				// x
				if (strHeadings != string.Empty)
				{
					strHeadings = Strings.Mid(strHeadings, 1, strHeadings.Length - 1);
				}
				if (cmbRange.SelectedItem == "All")
				{
					boolRange = false;
					boolSpecific = false;
				}
				else if (cmbRange.SelectedItem == "Range")
				{
					boolRange = true;
					boolSpecific = false;
				}
				else if (cmbRange.SelectedItem == "Individual")
				{
					boolRange = false;
					boolSpecific = true;
				}
				if (!boolSpecific)
				{
					// strSQL = "Select ppitemized.*,ppmaster.name from ppmaster inner join ppitemized on (ppmaster.account = ppitemized.account) "
					strSQL = "Select ppitemized.*,mj.name from " + strMasterJoin + " inner join ppitemized on (mj.account = ppitemized.account) ";
				}
				else
				{
					// strSQL = "Select ppitemized.*,ppmaster.name from ppitemized inner join (ppmaster inner join accountlist on (ppmaster.account = accountlist.account)) on (ppmaster.account = ppitemized.account) "
					strSQL = "Select ppitemized.*,mj.name from ppitemized inner join (" + strMasterJoin + " inner join accountlist on (mj.account = accountlist.account)) on (mj.account = ppitemized.account) ";
				}
				if (cmbOrder.SelectedIndex == 0)
				{
					// account
					strOrder = " order by ppitemized.account ";
					if (boolRange)
					{
						strWhere = " where ppitemized.account between " + FCConvert.ToString(Conversion.Val(txtStart.Text)) + " and " + FCConvert.ToString(Conversion.Val(txtEnd.Text)) + " ";
					}
				}
				else if (cmbOrder.SelectedIndex == 1)
				{
					// name
					strOrder = " order by name";
					if (boolRange)
					{
						strWhere = " where name between '" + txtStart.Text + "' and '" + txtEnd.Text + "' ";
					}
				}
				else if (cmbOrder.SelectedIndex == 2)
				{
					// open1
					strOrder = " order by open1";
					if (boolRange)
					{
						strWhere = " where open1 between '" + txtStart.Text + "' and '" + txtEnd.Text + "' ";
					}
				}
				else if (cmbOrder.SelectedIndex == 3)
				{
					// open2
					strOrder = " order by open2";
					if (boolRange)
					{
						strWhere = " where open2 between '" + txtStart.Text + "' and '" + txtEnd.Text + "' ";
					}
				}
				else if (cmbOrder.SelectedIndex == 4)
				{
					// business code
					strOrder = " order by open2";
					if (boolRange)
					{
						strWhere = " where businesscode between " + FCConvert.ToString(Conversion.Val(txtStart.Text)) + " and " + FCConvert.ToString(Conversion.Val(txtEnd.Text)) + " ";
					}
				}
				else if (cmbOrder.SelectedIndex == 5)
				{
					// tran code
					strOrder = " order by trancode";
					if (boolRange)
					{
						strWhere = " where businesscode between " + FCConvert.ToString(Conversion.Val(txtStart.Text)) + " and " + FCConvert.ToString(Conversion.Val(txtEnd.Text)) + " ";
					}
				}
				else if (cmbOrder.SelectedIndex == 6)
				{
					// street code
					strOrder = " order by streetcode";
					if (boolRange)
					{
						strWhere = " where trancode between " + FCConvert.ToString(Conversion.Val(txtStart.Text)) + " and " + FCConvert.ToString(Conversion.Val(txtEnd.Text)) + " ";
					}
				}
				if (cmbOrder.SelectedIndex == 0)
				{
					strOrder += ",line";
				}
				else
				{
					strOrder += "ppitemized.account,line";
				}
				if (strWhere != string.Empty)
				{
					strWhere += " and not mj.deleted = 1 ";
				}
				else
				{
					strWhere = " where not mj.deleted = 1 ";
				}
				if (boolUseHeadings)
				{
					ts.WriteLine(strHeadings);
				}
				clsLoad.OpenRecordset(strSQL + strWhere + strOrder, "twpp0000.vb1");
				while (!clsLoad.EndOfFile())
				{
					strOutput = "";
					for (x = 0; x <= lstItemized.Items.Count - 1; x++)
					{
						if (lstItemized.Selected(x))
						{
							switch (lstItemized.ItemData(x))
							{
								case 0:
									{
										// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
										strOutput += clsLoad.Get_Fields("account") + ",";
										break;
									}
								case 1:
									{
										strOutput += FCConvert.ToString(Convert.ToChar(34)) + clsLoad.Get_Fields_String("name") + FCConvert.ToString(Convert.ToChar(34)) + ",";
										break;
									}
								case 2:
									{
										// TODO Get_Fields: Check the table for the column [line] and replace with corresponding Get_Field method
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("line"))) + ",";
										break;
									}
								case 3:
									{
										// TODO Get_Fields: Check the table for the column [rb] and replace with corresponding Get_Field method
										if (Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("rb"))) == "*")
										{
											strOutput += "TRUE" + ",";
										}
										else
										{
											strOutput += "FALSE" + ",";
										}
										break;
									}
								case 4:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("cd"))) + ",";
										break;
									}
								case 5:
									{
										// TODO Get_Fields: Check the table for the column [quantity] and replace with corresponding Get_Field method
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("quantity"))) + ",";
										break;
									}
								case 6:
									{
										strOutput += FCConvert.ToString(Convert.ToChar(34)) + clsLoad.Get_Fields_String("description") + FCConvert.ToString(Convert.ToChar(34)) + ",";
										break;
									}
								case 7:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int16("month"))) + ",";
										break;
									}
								case 8:
									{
										// TODO Get_Fields: Check the table for the column [year] and replace with corresponding Get_Field method
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("year"))) + ",";
										break;
									}
								case 9:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int16("Dpyr"))) + ",";
										break;
									}
								case 10:
									{
										// TODO Get_Fields: Check the table for the column [sro] and replace with corresponding Get_Field method
										strOutput += FCConvert.ToString(Convert.ToChar(34)) + clsLoad.Get_Fields("sro") + FCConvert.ToString(Convert.ToChar(34)) + ",";
										break;
									}
								case 11:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int16("rcyr"))) + ",";
										break;
									}
								case 12:
									{
										// TODO Get_Fields: Check the table for the column [cost] and replace with corresponding Get_Field method
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("cost"))) + ",";
										break;
									}
								case 13:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int16("gd"))) + ",";
										break;
									}
								case 14:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int16("fctr"))) + ",";
										break;
									}
								case 15:
									{
										// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("value"))) + ",";
										break;
									}
								case 16:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("leasedto"))) + ",";
										break;
									}
								case 17:
									{
										// age
										if (Conversion.Val(clsLoad.Get_Fields_String("cd")) > 0)
										{
											lngYearNow = DateTime.Today.Year;
											if (modGlobal.Statics.CustomizeStuff.DepreciationYear > 1900)
											{
												lngYearNow = modGlobal.Statics.CustomizeStuff.DepreciationYear;
											}
											lngYearNew = lngYearNow;
											// TODO Get_Fields: Check the table for the column [year] and replace with corresponding Get_Field method
											if (Conversion.Val(clsLoad.Get_Fields("year")) > 0)
											{
												// TODO Get_Fields: Check the table for the column [year] and replace with corresponding Get_Field method
												lngYearNew = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("year"))));
											}
											lngAge = lngYearNow - lngYearNew;
											if (lngAge < 0)
												lngAge = 0;
											strOutput += FCConvert.ToString(lngAge) + ",";
										}
										else
										{
											strOutput += "0,";
										}
										break;
									}
								case 18:
									{
										// depreciation Age
										if (Conversion.Val(clsLoad.Get_Fields_String("cd")) > 0)
										{
											lngYearNow = DateTime.Today.Year;
											if (modGlobal.Statics.CustomizeStuff.DepreciationYear > 1900)
											{
												lngYearNow = modGlobal.Statics.CustomizeStuff.DepreciationYear;
											}
											lngYearNew = lngYearNow;
											// TODO Get_Fields: Check the table for the column [year] and replace with corresponding Get_Field method
											if (Conversion.Val(clsLoad.Get_Fields("year")) > 0)
											{
												// TODO Get_Fields: Check the table for the column [year] and replace with corresponding Get_Field method
												lngYearNew = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("year"))));
											}
											lngAge = lngYearNow - lngYearNew;
											if (lngAge < 0)
												lngAge = 0;
											lngDepYrs = modPPCalculate.Statics.DefLife[FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_String("cd"))))];
											if (Conversion.Val(clsLoad.Get_Fields_Int16("dpyr")) > 0)
											{
												lngDepYrs = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int16("dpyr"))));
											}
											if (lngAge > lngDepYrs)
											{
												lngAge = lngDepYrs;
											}
											strOutput += FCConvert.ToString(lngAge) + ",";
										}
										else
										{
											strOutput += "0,";
										}
										break;
									}
								case 19:
									{
										// year first assessed
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("YEARFIRSTASSESSED"))) + ",";
										break;
									}
								case 20:
									{
										// AMOUNT replacing
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("amountreplacing"))) + ",";
										break;
									}
								case 21:
									{
										// years claimed
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("yearsclaimed"))) + ",";
										break;
									}
							}
							//end switch
						}
					}
					// x
					if (strOutput != string.Empty)
					{
						// get rid of last comma
						strOutput = Strings.Mid(strOutput, 1, strOutput.Length - 1);
					}
					ts.Write(strOutput);
					clsLoad.MoveNext();
					if (!clsLoad.EndOfFile())
					{
						ts.WriteLine();
						// if not the last line then add a new line
					}
				}
				ts.Close();
				//FC:FINAL:RPU:#i1554 - Download the file to client
				FCUtils.Download(tempPath, strFileName);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				CreateItemizedExtract = true;
				return CreateItemizedExtract;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				ts.Close();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In CreateItemizedExtract", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateItemizedExtract;
		}

		private bool CreateAccountExtract(ref string strFileName)
		{
			bool CreateAccountExtract = false;
			StreamWriter ts = null;
			string strTemp;
			int x;
			string strSQL = "";
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strWhere = "";
			string strOrder = "";
			bool boolRange = false;
			bool boolSpecific = false;
			string strStart = "";
			string strEnd = "";
			string strOutput = "";
			string strHeadings;
			double[] dblBETE = new double[9 + 1];
			int y;
			double dblTotBETE = 0;
			try
			{
				// On Error GoTo ErrorHandler
				string strMasterJoin;
				strMasterJoin = modPPGN.GetMasterJoinForJoin();
				CreateAccountExtract = false;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				strTemp = Path.GetFileName(strFileName);
				strHeadings = "";
				//FC:FINAL:RPU:#1381 - Remove unneeded text
				//strFormatMessage = "The extract is named " + strTemp + ", is found in ";
				strFormatMessage = "The extract is named " + strTemp;
				strTemp = Path.GetDirectoryName(strFileName);
				strFormatMessage += strTemp + " and is in a comma delimited format in the following order:";
				//FC:FINAL:RPU:#i1469 - save file to UserData folder, and download to client
				string tempPath = Path.Combine(FCFileSystem.CurDir(), Path.GetRandomFileName());
				tempPath = Path.ChangeExtension(tempPath, ".asc");
				//ts = File.CreateText(strFileName);
				ts = File.CreateText(tempPath);
				for (x = 0; x <= lstAccountInfo.Items.Count - 1; x++)
				{
					if (lstAccountInfo.Selected(x))
					{
						switch (lstAccountInfo.ItemData(x))
						{
							case 0:
								{
									strTemp = "Account Number";
									break;
								}
							case 1:
								{
									strTemp = "Real Estate Account";
									break;
								}
							case 2:
								{
									strTemp = "Name";
									break;
								}
							case 3:
								{
									strTemp = "Address 1";
									break;
								}
							case 4:
								{
									strTemp = "Address 2";
									break;
								}
							case 53:
								{
									strTemp = "Address 3";
									break;
								}
							case 5:
								{
									strTemp = "City";
									break;
								}
							case 6:
								{
									strTemp = "State";
									break;
								}
							case 7:
								{
									strTemp = "Zip";
									break;
								}
							case 8:
								{
									strTemp = "Zip 4";
									break;
								}
							case 9:
								{
									strTemp = "Telephone";
									break;
								}
							case 10:
								{
									strTemp = "Street Number";
									break;
								}
							case 11:
								{
									strTemp = "Apartment";
									break;
								}
							case 12:
								{
									strTemp = "Street Name";
									break;
								}
							case 13:
								{
									strTemp = "Assessment";
									break;
								}
							case 14:
								{
									strTemp = "Exemption";
									break;
								}
							case 15:
								{
									strTemp = "Exempt Code 1";
									break;
								}
							case 16:
								{
									strTemp = "Exempt Code 2";
									break;
								}
							case 17:
								{
									strTemp = "Tran Code";
									break;
								}
							case 18:
								{
									strTemp = "Business Code";
									break;
								}
							case 19:
								{
									strTemp = "Street Code";
									break;
								}
							case 20:
								{
									strTemp = modPPGN.Statics.Open1Title;
									break;
								}
							case 21:
								{
									strTemp = modPPGN.Statics.Open2Title;
									break;
								}
							case 22:
								{
									strTemp = "Square Footage";
									break;
								}
							case 23:
								{
									strTemp = strCats[1];
									break;
								}
							case 24:
								{
									strTemp = strCats[2];
									break;
								}
							case 25:
								{
									strTemp = strCats[3];
									break;
								}
							case 26:
								{
									strTemp = strCats[4];
									break;
								}
							case 27:
								{
									strTemp = strCats[5];
									break;
								}
							case 28:
								{
									strTemp = strCats[6];
									break;
								}
							case 29:
								{
									strTemp = strCats[7];
									break;
								}
							case 30:
								{
									strTemp = strCats[8];
									break;
								}
							case 31:
								{
									strTemp = strCats[9];
									break;
								}
							case 32:
								{
									strTemp = "Assessment with BETE";
									break;
								}
							case 33:
								{
									strTemp = "Exemption with BETE";
									break;
								}
							case 34:
								{
									strTemp = strCats[1] + " BETE Exempt";
									break;
								}
							case 35:
								{
									strTemp = strCats[2] + " BETE Exempt";
									break;
								}
							case 36:
								{
									strTemp = strCats[3] + " BETE Exempt";
									break;
								}
							case 37:
								{
									strTemp = strCats[4] + " BETE Exempt";
									break;
								}
							case 38:
								{
									strTemp = strCats[5] + " BETE Exempt";
									break;
								}
							case 39:
								{
									strTemp = strCats[6] + " BETE Exempt";
									break;
								}
							case 40:
								{
									strTemp = strCats[7] + " BETE Exempt";
									break;
								}
							case 41:
								{
									strTemp = strCats[8] + " BETE Exempt";
									break;
								}
							case 42:
								{
									strTemp = strCats[9] + " BETE Exempt";
									break;
								}
							case 43:
								{
									strTemp = strCats[1] + " with BETE";
									break;
								}
							case 44:
								{
									strTemp = strCats[2] + " with BETE";
									break;
								}
							case 45:
								{
									strTemp = strCats[3] + " with BETE";
									break;
								}
							case 46:
								{
									strTemp = strCats[4] + " with BETE";
									break;
								}
							case 47:
								{
									strTemp = strCats[5] + " with BETE";
									break;
								}
							case 48:
								{
									strTemp = strCats[6] + " with BETE";
									break;
								}
							case 49:
								{
									strTemp = strCats[7] + " with BETE";
									break;
								}
							case 50:
								{
									strTemp = strCats[8] + " with BETE";
									break;
								}
							case 51:
								{
									strTemp = strCats[9] + " with BETE";
									break;
								}
							case 52:
								{
									strTemp = "Total BETE Exempt";
									break;
								}
						}
						//end switch
						strFormatMessage += "\r\n" + strTemp;
						strHeadings += FCConvert.ToString(Convert.ToChar(34)) + strTemp + FCConvert.ToString(Convert.ToChar(34)) + ",";
					}
				}
				// x
				if (strHeadings != string.Empty)
				{
					strHeadings = Strings.Mid(strHeadings, 1, strHeadings.Length - 1);
				}
				if (cmbRange.SelectedItem == "All")
				{
					boolRange = false;
					boolSpecific = false;
				}
				else if (cmbRange.SelectedItem == "Range")
				{
					boolRange = true;
					boolSpecific = false;
				}
				else if (cmbRange.SelectedItem == "Individual")
				{
					boolRange = false;
					boolSpecific = true;
				}
				if (!boolSpecific)
				{
					// strSQL = "Select * from ppmaster inner join ppvaluations on (ppmaster.account = ppvaluations.valuekey) "
					strSQL = "Select * from " + strMasterJoin + " inner join ppvaluations on (mj.account = ppvaluations.valuekey) ";
				}
				else
				{
					// strSQL = "Select ppvaluations.*,ppmaster.* from ppvaluations inner join (ppmaster inner join accountlist on (ppmaster.account = accountlist.account)) on (ppmaster.account = ppvaluations.valuekey) "
					strSQL = "Select ppvaluations.*,mj.* from ppvaluations inner join (" + strMasterJoin + " inner join accountlist on (mj.account = accountlist.account)) on (mj.account = ppvaluations.valuekey) ";
				}
				if (cmbOrder.SelectedIndex == 0)
				{
					// account
					if (!boolSpecific)
					{
						strOrder = " order by account";
					}
					else
					{
						strOrder = " order by mj.account";
					}
					if (boolRange)
					{
						strWhere = " where account between " + FCConvert.ToString(Conversion.Val(txtStart.Text)) + " and " + FCConvert.ToString(Conversion.Val(txtEnd.Text)) + " ";
					}
				}
				else if (cmbOrder.SelectedIndex == 1)
				{
					// name
					strOrder = " order by name";
					if (boolRange)
					{
						strWhere = " where name between '" + txtStart.Text + "' and '" + txtEnd.Text + "' ";
					}
				}
				else if (cmbOrder.SelectedIndex == 2)
				{
					// open1
					strOrder = " order by open1";
					if (boolRange)
					{
						strWhere = " where open1 between '" + txtStart.Text + "' and '" + txtEnd.Text + "' ";
					}
				}
				else if (cmbOrder.SelectedIndex == 3)
				{
					// open2
					strOrder = " order by open2";
					if (boolRange)
					{
						strWhere = " where open2 between '" + txtStart.Text + "' and '" + txtEnd.Text + "' ";
					}
				}
				else if (cmbOrder.SelectedIndex == 4)
				{
					// business code
					strOrder = " order by open2";
					if (boolRange)
					{
						strWhere = " where businesscode between " + FCConvert.ToString(Conversion.Val(txtStart.Text)) + " and " + FCConvert.ToString(Conversion.Val(txtEnd.Text)) + " ";
					}
				}
				else if (cmbOrder.SelectedIndex == 5)
				{
					// tran code
					strOrder = " order by trancode";
					if (boolRange)
					{
						strWhere = " where businesscode between " + FCConvert.ToString(Conversion.Val(txtStart.Text)) + " and " + FCConvert.ToString(Conversion.Val(txtEnd.Text)) + " ";
					}
				}
				else if (cmbOrder.SelectedIndex == 6)
				{
					// street code
					strOrder = " order by streetcode";
					if (boolRange)
					{
						strWhere = " where trancode between " + FCConvert.ToString(Conversion.Val(txtStart.Text)) + " and " + FCConvert.ToString(Conversion.Val(txtEnd.Text)) + " ";
					}
				}
				if (strWhere != string.Empty)
				{
					strWhere += " and not mj.deleted = 1 ";
				}
				else
				{
					strWhere = " where not mj.deleted = 1 ";
				}
				clsLoad.OpenRecordset(strSQL + strWhere + strOrder, "twpp0000.vb1");
				if (boolUseHeadings)
				{
					ts.WriteLine(strHeadings);
				}
				while (!clsLoad.EndOfFile())
				{
					strOutput = "";
					dblTotBETE = 0;
					for (y = 1; y <= 9; y++)
					{
						// TODO Get_Fields: Field [Cat] not found!! (maybe it is an alias?)
						dblBETE[y] = Conversion.Val(clsLoad.Get_Fields("Cat" + FCConvert.ToString(y) + "Exempt"));
						dblTotBETE += dblBETE[y];
					}
					// y
					for (x = 0; x <= lstAccountInfo.Items.Count - 1; x++)
					{
						if (lstAccountInfo.Selected(x))
						{
							switch (lstAccountInfo.ItemData(x))
							{
								case 0:
									{
										// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
										strOutput += clsLoad.Get_Fields("account") + ",";
										break;
									}
								case 1:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("realassoc"))) + ",";
										break;
									}
								case 2:
									{
										strOutput += FCConvert.ToString(Convert.ToChar(34)) + clsLoad.Get_Fields_String("name") + FCConvert.ToString(Convert.ToChar(34)) + ",";
										break;
									}
								case 3:
									{
										strOutput += FCConvert.ToString(Convert.ToChar(34)) + clsLoad.Get_Fields_String("Address1") + FCConvert.ToString(Convert.ToChar(34)) + ",";
										break;
									}
								case 4:
									{
										strOutput += FCConvert.ToString(Convert.ToChar(34)) + clsLoad.Get_Fields_String("address2") + FCConvert.ToString(Convert.ToChar(34)) + ",";
										break;
									}
								case 53:
									{
										strOutput += FCConvert.ToString(Convert.ToChar(34)) + clsLoad.Get_Fields_String("Address3") + FCConvert.ToString(Convert.ToChar(34)) + ",";
										break;
									}
								case 5:
									{
										strOutput += FCConvert.ToString(Convert.ToChar(34)) + clsLoad.Get_Fields_String("city") + FCConvert.ToString(Convert.ToChar(34)) + ",";
										break;
									}
								case 6:
									{
										strOutput += FCConvert.ToString(Convert.ToChar(34)) + clsLoad.Get_Fields_String("state") + FCConvert.ToString(Convert.ToChar(34)) + ",";
										break;
									}
								case 7:
									{
										strOutput += FCConvert.ToString(Convert.ToChar(34)) + clsLoad.Get_Fields_String("zip") + FCConvert.ToString(Convert.ToChar(34)) + ",";
										break;
									}
								case 8:
									{
										strOutput += FCConvert.ToString(Convert.ToChar(34)) + clsLoad.Get_Fields_String("zip4") + FCConvert.ToString(Convert.ToChar(34)) + ",";
										break;
									}
								case 9:
									{
										strOutput += FCConvert.ToString(Convert.ToChar(34)) + clsLoad.Get_Fields_String("phonenumber") + FCConvert.ToString(Convert.ToChar(34)) + ",";
										break;
									}
								case 10:
									{
										// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("streetnumber"))) + ",";
										break;
									}
								case 11:
									{
										strOutput += FCConvert.ToString(Convert.ToChar(34)) + clsLoad.Get_Fields_String("streetapt") + FCConvert.ToString(Convert.ToChar(34)) + ",";
										break;
									}
								case 12:
									{
										strOutput += FCConvert.ToString(Convert.ToChar(34)) + clsLoad.Get_Fields_String("street") + FCConvert.ToString(Convert.ToChar(34)) + ",";
										break;
									}
								case 13:
									{
										// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("value"))) + ",";
										break;
									}
								case 14:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("Exemption"))) + ",";
										break;
									}
								case 15:
									{
										// TODO Get_Fields: Check the table for the column [exemptcode1] and replace with corresponding Get_Field method
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("exemptcode1"))) + ",";
										break;
									}
								case 16:
									{
										// TODO Get_Fields: Check the table for the column [exemptcode2] and replace with corresponding Get_Field method
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("exemptcode2"))) + ",";
										break;
									}
								case 17:
									{
										// TODO Get_Fields: Check the table for the column [trancode] and replace with corresponding Get_Field method
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("trancode"))) + ",";
										break;
									}
								case 18:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("businesscode"))) + ",";
										break;
									}
								case 19:
									{
										// TODO Get_Fields: Check the table for the column [streetcode] and replace with corresponding Get_Field method
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("streetcode"))) + ",";
										break;
									}
								case 20:
									{
										strOutput += FCConvert.ToString(Convert.ToChar(34)) + clsLoad.Get_Fields_String("open1") + FCConvert.ToString(Convert.ToChar(34)) + ",";
										break;
									}
								case 21:
									{
										strOutput += FCConvert.ToString(Convert.ToChar(34)) + clsLoad.Get_Fields_String("open2") + FCConvert.ToString(Convert.ToChar(34)) + ",";
										break;
									}
								case 22:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("squarefootage"))) + ",";
										break;
									}
								case 23:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("category1"))) + ",";
										break;
									}
								case 24:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("category2"))) + ",";
										break;
									}
								case 25:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("category3"))) + ",";
										break;
									}
								case 26:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("category4"))) + ",";
										break;
									}
								case 27:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("category5"))) + ",";
										break;
									}
								case 28:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("category6"))) + ",";
										break;
									}
								case 29:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("category7"))) + ",";
										break;
									}
								case 30:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("category8"))) + ",";
										break;
									}
								case 31:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("category9"))) + ",";
										break;
									}
								case 32:
									{
										// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("value")) + dblTotBETE) + ",";
										break;
									}
								case 33:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("exemption")) + dblTotBETE) + ",";
										break;
									}
								case 34:
									{
										strOutput += FCConvert.ToString(dblBETE[1]) + ",";
										break;
									}
								case 35:
									{
										strOutput += FCConvert.ToString(dblBETE[2]) + ",";
										break;
									}
								case 36:
									{
										strOutput += FCConvert.ToString(dblBETE[3]) + ",";
										break;
									}
								case 37:
									{
										strOutput += FCConvert.ToString(dblBETE[4]) + ",";
										break;
									}
								case 38:
									{
										strOutput += FCConvert.ToString(dblBETE[5]) + ",";
										break;
									}
								case 39:
									{
										strOutput += FCConvert.ToString(dblBETE[6]) + ",";
										break;
									}
								case 40:
									{
										strOutput += FCConvert.ToString(dblBETE[7]) + ",";
										break;
									}
								case 41:
									{
										strOutput += FCConvert.ToString(dblBETE[8]) + ",";
										break;
									}
								case 42:
									{
										strOutput += FCConvert.ToString(dblBETE[9]) + ",";
										break;
									}
								case 43:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("category1")) + dblBETE[1]) + ",";
										break;
									}
								case 44:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("category2")) + dblBETE[2]) + ",";
										break;
									}
								case 45:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("category3")) + dblBETE[3]) + ",";
										break;
									}
								case 46:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("category4")) + dblBETE[4]) + ",";
										break;
									}
								case 47:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("category5")) + dblBETE[5]) + ",";
										break;
									}
								case 48:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("category6")) + dblBETE[6]) + ",";
										break;
									}
								case 49:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("category7")) + dblBETE[7]) + ",";
										break;
									}
								case 50:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("category8")) + dblBETE[8]) + ",";
										break;
									}
								case 51:
									{
										strOutput += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("category9")) + dblBETE[9]) + ",";
										break;
									}
								case 52:
									{
										strOutput += FCConvert.ToString(dblTotBETE) + ",";
										break;
									}
							}
							//end switch
						}
					}
					// x
					if (strOutput != string.Empty)
					{
						// get rid of last comma
						strOutput = Strings.Mid(strOutput, 1, strOutput.Length - 1);
					}
					ts.Write(strOutput);
					clsLoad.MoveNext();
					if (!clsLoad.EndOfFile())
					{
						ts.WriteLine();
						// if not the last line then add a new line
					}
				}
				ts.Close();
				//FC:FINAL:RPU:#i1469 - Download the file to client
				FCUtils.Download(tempPath, strFileName);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				CreateAccountExtract = true;
				return CreateAccountExtract;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				ts.Close();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In CreateAccountExtract", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateAccountExtract;
		}

		private void optRange_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 1:
					{
						txtStart.Visible = true;
						txtEnd.Visible = true;
						lblTo.Visible = true;
						break;
					}
				default:
					{
						txtStart.Visible = false;
						txtEnd.Visible = false;
						lblTo.Visible = false;
						break;
					}
			}
			//end switch
		}

		private void optRange_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbRange.SelectedIndex;
			optRange_CheckedChanged(index, sender, e);
		}

		private void GetCostData()
		{
			int xx;
			clsDRWrapper datCostFiles = new clsDRWrapper();
			datCostFiles.OpenRecordset("SELECT * FROM RatioTrends", "twpp0000.vb1");
			while (!datCostFiles.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				xx = FCConvert.ToInt32(datCostFiles.Get_Fields("Type"));
				// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
				modPPCalculate.Statics.HighPct[xx] = FCConvert.ToInt32(datCostFiles.Get_Fields("High"));
				// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
				modPPCalculate.Statics.LowPct[xx] = FCConvert.ToInt32(datCostFiles.Get_Fields("Low"));
				modPPCalculate.Statics.Exponent[xx] = datCostFiles.Get_Fields_Double("Exponent");
				// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
				modPPCalculate.Statics.DefLife[xx] = FCConvert.ToInt32(datCostFiles.Get_Fields("Life"));
				modPPCalculate.Statics.DepMeth[xx] = FCConvert.ToString(datCostFiles.Get_Fields_String("SD"));
				// TODO Get_Fields: Check the table for the column [Trend] and replace with corresponding Get_Field method
				modPPCalculate.Statics.TrendCode[xx] = FCConvert.ToString(datCostFiles.Get_Fields("Trend"));
				modPPCalculate.Statics.DefDesc[xx] = FCConvert.ToString(datCostFiles.Get_Fields_String("Description"));
				datCostFiles.MoveNext();
			}
		}

		private void cmbFileType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbFileType.SelectedIndex == 0)
			{
				optFileType_CheckedChanged(sender, e);
			}
			else if (cmbFileType.SelectedIndex == 1)
			{
				optFileType_CheckedChanged(sender, e);
			}
			else if (cmbFileType.SelectedIndex == 2)
			{
				optFileType_CheckedChanged(sender, e);
			}
		}
		//FC:FINAL:CHN - i.issue #1553: drag and drop not enabled to list.
		private void lstAccountInfo_DragDrop(object sender, DragEventArgs e)
		{
            int index = (e.DropTarget as ListViewItem).Index;
            ListViewItem data = (ListViewItem)e.Data.GetData(typeof(ListViewItem));
			this.lstAccountInfo.Items.Remove(data);
			this.lstAccountInfo.Items.Insert(index, data);
		}
        
        //FC:FINAL:CHN - issue #1493: Add adaptive Width of list on page resize.
        private void FrmDBExtract_Resize(object sender, EventArgs e)
        {
            lstAccountInfo.Width = FCConvert.ToInt32((this.Width * 0.55));
            Label2.Left = lstAccountInfo.Left + lstAccountInfo.Width + 28;
            Label2.Width = this.Width - Label2.Left - 25;

            //FC:FINAL:AKV:#3647 - Expand width of Itemized and Leased 

            lstItemized.Width = FCConvert.ToInt32((this.Width * 0.55));
            Label2.Left = lstItemized.Left + lstItemized.Width + 28;
            Label2.Width = this.Width - Label2.Left - 25;

            lstLeased.Width = FCConvert.ToInt32((this.Width * 0.55));
            Label2.Left = lstLeased.Left + lstLeased.Width + 28;
            Label2.Width = this.Width - Label2.Left - 25;
        }
    }
}
