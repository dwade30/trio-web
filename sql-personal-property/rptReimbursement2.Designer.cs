﻿namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptReimbursement2.
	/// </summary>
	partial class rptReimbursement2
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptReimbursement2));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txttime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txttaxrate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt1st = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txt2nd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRatioTot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOrigTot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtMessage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtAssessor = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAssessorTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalOriginal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalAssessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOrigCat1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblCat1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCat2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAssessCat1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOrigCat2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessCat2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOrigCat3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCat3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCat4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAssessCat3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOrigCat4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessCat4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOrigCat5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCat5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCat6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAssessCat5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOrigCat6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessCat6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOrigCat7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCat7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCat8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAssessCat7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOrigCat8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessCat8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCat9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOrigCat9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessCat9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txttime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txttaxrate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt1st)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txt2nd)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRatioTot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOrigTot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessorTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalOriginal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAssessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOrigCat1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessCat1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOrigCat2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessCat2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOrigCat3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessCat3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOrigCat4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessCat4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOrigCat5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessCat5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOrigCat6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessCat6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOrigCat7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessCat7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOrigCat8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessCat8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOrigCat9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessCat9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.AfterPrint += new System.EventHandler(this.Detail_AfterPrint);
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.SubReport1,
				this.Label6,
				this.Label7,
				this.Field10,
				this.Label8,
				this.Label9,
				this.txtTotTax,
				this.txttaxrate,
				this.txt1st,
				this.txt2nd,
				this.txtRatioTot,
				this.txtOrigTot,
				this.Label19
			});
			this.Detail.Height = 1.760417F;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label12,
				this.txtTotalCount,
				this.Label13,
				this.txtTotalOriginal,
				this.txtTotalAssessment,
				this.Label15,
				this.txtOrigCat1,
				this.Line7,
				this.lblCat1,
				this.lblCat2,
				this.txtAssessCat1,
				this.Label18,
				this.txtOrigCat2,
				this.txtAssessCat2,
				this.txtOrigCat3,
				this.lblCat3,
				this.lblCat4,
				this.txtAssessCat3,
				this.txtOrigCat4,
				this.txtAssessCat4,
				this.txtOrigCat5,
				this.lblCat5,
				this.lblCat6,
				this.txtAssessCat5,
				this.txtOrigCat6,
				this.txtAssessCat6,
				this.txtOrigCat7,
				this.lblCat7,
				this.lblCat8,
				this.txtAssessCat7,
				this.txtOrigCat8,
				this.txtAssessCat8,
				this.lblCat9,
				this.txtOrigCat9,
				this.txtAssessCat9
			});
			this.ReportFooter.Height = 3.208333F;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMuniName,
				this.Label1,
				this.txtDate,
				this.Label2,
				this.Label3,
				this.txtPage,
				this.txttime
			});
			this.PageHeader.Height = 0.5520833F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.DataField = "grpHeader";
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.CanShrink = true;
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label10,
				this.Line3,
				this.Line4,
				this.txtMessage,
				this.Line5,
				this.txtAssessor,
				this.Line6,
				this.Label11,
				this.txtAssessorTitle,
				this.Field13,
				this.Field14
			});
			this.GroupFooter1.Height = 1.614583F;
			this.GroupFooter1.KeepTogether = true;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1666667F;
			this.txtMuniName.Left = 0F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'";
			this.txtMuniName.Text = "Field1";
			this.txtMuniName.Top = 0.0625F;
			this.txtMuniName.Width = 2F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 2.0625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.Label1.Text = "PERSONAL PROPERTY ASSESSMENT";
			this.Label1.Top = 0.03125F;
			this.Label1.Width = 3.125F;
			// 
			// txtDate
			// 
			this.txtDate.CanGrow = false;
			this.txtDate.Height = 0.1666667F;
			this.txtDate.Left = 5.375F;
			this.txtDate.MultiLine = false;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtDate.Text = "Field1";
			this.txtDate.Top = 0.0625F;
			this.txtDate.Width = 2.0625F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 2.0625F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.Label2.Text = "-- FORM 801 SUBSTITUTE --";
			this.Label2.Top = 0.25F;
			this.Label2.Width = 3.125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1666667F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; text-align: left";
			this.Label3.Text = "Tax Year ";
			this.Label3.Top = 0.375F;
			this.Label3.Width = 1.125F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1666667F;
			this.txtPage.Left = 6.4375F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtPage.Text = "Field6";
			this.txtPage.Top = 0.2291667F;
			this.txtPage.Width = 1F;
			// 
			// txttime
			// 
			this.txttime.Height = 0.1666667F;
			this.txttime.Left = 0F;
			this.txttime.Name = "txttime";
			this.txttime.Style = "font-family: \'Tahoma\'";
			this.txttime.Text = "Field1";
			this.txttime.Top = 0.2291667F;
			this.txttime.Width = 1.75F;
			// 
			// SubReport1
			// 
			this.SubReport1.CloseBorder = false;
			this.SubReport1.Height = 0.0625F;
			this.SubReport1.Left = 0F;
			this.SubReport1.Name = "SubReport1";
			this.SubReport1.Report = null;
			this.SubReport1.Top = 0F;
			this.SubReport1.Width = 7.4375F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1666667F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0.08333334F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Label6.Text = "TOTAL WITH RATIO APPLIED:";
			this.Label6.Top = 0.4166667F;
			this.Label6.Width = 5.083333F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1666667F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 2.833333F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Label7.Text = "1ST TAX PAYMENT DUE DATE:";
			this.Label7.Top = 0.75F;
			this.Label7.Width = 2.333333F;
			// 
			// Field10
			// 
			this.Field10.Height = 0.25F;
			this.Field10.Left = 3.75F;
			this.Field10.Name = "Field10";
			this.Field10.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field10.Text = "TAX RATE:";
			this.Field10.Top = 0.9166667F;
			this.Field10.Width = 1.416667F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1666667F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 2.916667F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Label8.Text = "2ND TAX PAYMENT DUE DATE:";
			this.Label8.Top = 1.166667F;
			this.Label8.Width = 2.25F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1666667F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 1.916667F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Label9.Text = "TOTAL TAX ON QUALIFIED PROPERTY FOR ";
			this.Label9.Top = 1.583333F;
			this.Label9.Width = 4.166667F;
			// 
			// txtTotTax
			// 
			this.txtTotTax.Height = 0.1666667F;
			this.txtTotTax.Left = 6.416667F;
			this.txtTotTax.Name = "txtTotTax";
			this.txtTotTax.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtTotTax.Text = null;
			this.txtTotTax.Top = 1.583333F;
			this.txtTotTax.Width = 1F;
			// 
			// txttaxrate
			// 
			this.txttaxrate.Height = 0.1666667F;
			this.txttaxrate.Left = 6.5F;
			this.txttaxrate.Name = "txttaxrate";
			this.txttaxrate.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txttaxrate.Text = null;
			this.txttaxrate.Top = 0.9166667F;
			this.txttaxrate.Width = 0.9166667F;
			// 
			// txt1st
			// 
			this.txt1st.Height = 0.1666667F;
			this.txt1st.Left = 6.25F;
			this.txt1st.Name = "txt1st";
			this.txt1st.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txt1st.Text = null;
			this.txt1st.Top = 0.75F;
			this.txt1st.Width = 1.166667F;
			// 
			// txt2nd
			// 
			this.txt2nd.Height = 0.1666667F;
			this.txt2nd.Left = 6.25F;
			this.txt2nd.Name = "txt2nd";
			this.txt2nd.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txt2nd.Text = null;
			this.txt2nd.Top = 1.083333F;
			this.txt2nd.Width = 1.166667F;
			// 
			// txtRatioTot
			// 
			this.txtRatioTot.Height = 0.1666667F;
			this.txtRatioTot.Left = 6.166667F;
			this.txtRatioTot.Name = "txtRatioTot";
			this.txtRatioTot.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtRatioTot.Text = null;
			this.txtRatioTot.Top = 0.4166667F;
			this.txtRatioTot.Width = 1.25F;
			// 
			// txtOrigTot
			// 
			this.txtOrigTot.Height = 0.1666667F;
			this.txtOrigTot.Left = 5.166667F;
			this.txtOrigTot.Name = "txtOrigTot";
			this.txtOrigTot.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtOrigTot.Text = null;
			this.txtOrigTot.Top = 0.1666667F;
			this.txtOrigTot.Width = 1.083333F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.25F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 2.833333F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Label19.Text = "Total Original:";
			this.Label19.Top = 0.1666667F;
			this.Label19.Width = 2.333333F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1666667F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 2.416667F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label10.Text = "THIS IS NOT A TAX BILL";
			this.Label10.Top = 0.08333334F;
			this.Label10.Width = 2.75F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 0.08333334F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0.25F;
			this.Line3.Width = 7.333333F;
			this.Line3.X1 = 0.08333334F;
			this.Line3.X2 = 7.416667F;
			this.Line3.Y1 = 0.25F;
			this.Line3.Y2 = 0.25F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 0F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 0.3333333F;
			this.Line4.Width = 7.416667F;
			this.Line4.X1 = 0F;
			this.Line4.X2 = 7.416667F;
			this.Line4.Y1 = 0.3333333F;
			this.Line4.Y2 = 0.3333333F;
			// 
			// txtMessage
			// 
			this.txtMessage.Height = 0.1666667F;
			this.txtMessage.Left = 0.08333334F;
			this.txtMessage.Name = "txtMessage";
			this.txtMessage.Text = null;
			this.txtMessage.Top = 0.3333333F;
			this.txtMessage.Width = 7.333333F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 0.08333334F;
			this.Line5.LineWeight = 1F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 1F;
			this.Line5.Width = 2.916667F;
			this.Line5.X1 = 0.08333334F;
			this.Line5.X2 = 3F;
			this.Line5.Y1 = 1F;
			this.Line5.Y2 = 1F;
			// 
			// txtAssessor
			// 
			this.txtAssessor.Height = 0.1666667F;
			this.txtAssessor.Left = 0.08333334F;
			this.txtAssessor.Name = "txtAssessor";
			this.txtAssessor.Style = "font-family: \'Tahoma\'";
			this.txtAssessor.Text = null;
			this.txtAssessor.Top = 1F;
			this.txtAssessor.Width = 2.333333F;
			// 
			// Line6
			// 
			this.Line6.Height = 0F;
			this.Line6.Left = 4.25F;
			this.Line6.LineWeight = 1F;
			this.Line6.Name = "Line6";
			this.Line6.Top = 1F;
			this.Line6.Width = 3F;
			this.Line6.X1 = 4.25F;
			this.Line6.X2 = 7.25F;
			this.Line6.Y1 = 1F;
			this.Line6.Y2 = 1F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1666667F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 4.25F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'";
			this.Label11.Text = "TAXPAYER OR AGENT SIGNATURE";
			this.Label11.Top = 1F;
			this.Label11.Width = 3F;
			// 
			// txtAssessorTitle
			// 
			this.txtAssessorTitle.Height = 0.1666667F;
			this.txtAssessorTitle.Left = 0.08333334F;
			this.txtAssessorTitle.Name = "txtAssessorTitle";
			this.txtAssessorTitle.Style = "font-family: \'Tahoma\'";
			this.txtAssessorTitle.Text = null;
			this.txtAssessorTitle.Top = 1.166667F;
			this.txtAssessorTitle.Width = 2.333333F;
			// 
			// Field13
			// 
			this.Field13.Height = 0.1666667F;
			this.Field13.Left = 0.08333334F;
			this.Field13.Name = "Field13";
			this.Field13.Style = "font-family: \'Tahoma\'";
			this.Field13.Text = "DATE:  ____/___/___";
			this.Field13.Top = 1.416667F;
			this.Field13.Width = 2.333333F;
			// 
			// Field14
			// 
			this.Field14.Height = 0.1666667F;
			this.Field14.Left = 4.25F;
			this.Field14.Name = "Field14";
			this.Field14.Style = "font-family: \'Tahoma\'";
			this.Field14.Text = "DATE:  ___/___/___        ID: ____________";
			this.Field14.Top = 1.333333F;
			this.Field14.Width = 2.916667F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 2.8125F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label12.Text = "Count:";
			this.Label12.Top = 0.40625F;
			this.Label12.Width = 0.5625F;
			// 
			// txtTotalCount
			// 
			this.txtTotalCount.Height = 0.1875F;
			this.txtTotalCount.Left = 3.4375F;
			this.txtTotalCount.Name = "txtTotalCount";
			this.txtTotalCount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotalCount.Text = null;
			this.txtTotalCount.Top = 0.40625F;
			this.txtTotalCount.Width = 1F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 4F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Label13.Text = "Original";
			this.Label13.Top = 0.75F;
			this.Label13.Width = 0.6875F;
			// 
			// txtTotalOriginal
			// 
			this.txtTotalOriginal.Height = 0.1875F;
			this.txtTotalOriginal.Left = 3.375F;
			this.txtTotalOriginal.Name = "txtTotalOriginal";
			this.txtTotalOriginal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotalOriginal.Text = null;
			this.txtTotalOriginal.Top = 2.75F;
			this.txtTotalOriginal.Width = 1.3125F;
			// 
			// txtTotalAssessment
			// 
			this.txtTotalAssessment.Height = 0.1875F;
			this.txtTotalAssessment.Left = 4.75F;
			this.txtTotalAssessment.Name = "txtTotalAssessment";
			this.txtTotalAssessment.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotalAssessment.Text = null;
			this.txtTotalAssessment.Top = 2.75F;
			this.txtTotalAssessment.Width = 1.3125F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 5.0625F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Label15.Text = "Assessment";
			this.Label15.Top = 0.75F;
			this.Label15.Width = 1F;
			// 
			// txtOrigCat1
			// 
			this.txtOrigCat1.Height = 0.1875F;
			this.txtOrigCat1.Left = 3.375F;
			this.txtOrigCat1.Name = "txtOrigCat1";
			this.txtOrigCat1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtOrigCat1.Text = null;
			this.txtOrigCat1.Top = 0.9375F;
			this.txtOrigCat1.Width = 1.3125F;
			// 
			// Line7
			// 
			this.Line7.Height = 0F;
			this.Line7.Left = 3.375F;
			this.Line7.LineWeight = 2F;
			this.Line7.Name = "Line7";
			this.Line7.Top = 2.6875F;
			this.Line7.Width = 2.6875F;
			this.Line7.X1 = 3.375F;
			this.Line7.X2 = 6.0625F;
			this.Line7.Y1 = 2.6875F;
			this.Line7.Y2 = 2.6875F;
			// 
			// lblCat1
			// 
			this.lblCat1.Height = 0.1875F;
			this.lblCat1.HyperLink = null;
			this.lblCat1.Left = 1.375F;
			this.lblCat1.Name = "lblCat1";
			this.lblCat1.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblCat1.Text = "Category 1";
			this.lblCat1.Top = 0.9375F;
			this.lblCat1.Width = 1.9375F;
			// 
			// lblCat2
			// 
			this.lblCat2.Height = 0.1875F;
			this.lblCat2.HyperLink = null;
			this.lblCat2.Left = 1.375F;
			this.lblCat2.Name = "lblCat2";
			this.lblCat2.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblCat2.Text = "Category 2";
			this.lblCat2.Top = 1.125F;
			this.lblCat2.Width = 1.9375F;
			// 
			// txtAssessCat1
			// 
			this.txtAssessCat1.Height = 0.1875F;
			this.txtAssessCat1.Left = 4.75F;
			this.txtAssessCat1.Name = "txtAssessCat1";
			this.txtAssessCat1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtAssessCat1.Text = null;
			this.txtAssessCat1.Top = 0.9375F;
			this.txtAssessCat1.Width = 1.3125F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 3.25F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label18.Text = "Summary";
			this.Label18.Top = 0.03125F;
			this.Label18.Width = 1F;
			// 
			// txtOrigCat2
			// 
			this.txtOrigCat2.Height = 0.1875F;
			this.txtOrigCat2.Left = 3.375F;
			this.txtOrigCat2.Name = "txtOrigCat2";
			this.txtOrigCat2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtOrigCat2.Text = null;
			this.txtOrigCat2.Top = 1.125F;
			this.txtOrigCat2.Width = 1.3125F;
			// 
			// txtAssessCat2
			// 
			this.txtAssessCat2.Height = 0.1875F;
			this.txtAssessCat2.Left = 4.75F;
			this.txtAssessCat2.Name = "txtAssessCat2";
			this.txtAssessCat2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtAssessCat2.Text = null;
			this.txtAssessCat2.Top = 1.125F;
			this.txtAssessCat2.Width = 1.3125F;
			// 
			// txtOrigCat3
			// 
			this.txtOrigCat3.Height = 0.1875F;
			this.txtOrigCat3.Left = 3.375F;
			this.txtOrigCat3.Name = "txtOrigCat3";
			this.txtOrigCat3.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtOrigCat3.Text = null;
			this.txtOrigCat3.Top = 1.3125F;
			this.txtOrigCat3.Width = 1.3125F;
			// 
			// lblCat3
			// 
			this.lblCat3.Height = 0.1875F;
			this.lblCat3.HyperLink = null;
			this.lblCat3.Left = 1.375F;
			this.lblCat3.Name = "lblCat3";
			this.lblCat3.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblCat3.Text = "Category 3";
			this.lblCat3.Top = 1.3125F;
			this.lblCat3.Width = 1.9375F;
			// 
			// lblCat4
			// 
			this.lblCat4.Height = 0.1875F;
			this.lblCat4.HyperLink = null;
			this.lblCat4.Left = 1.375F;
			this.lblCat4.Name = "lblCat4";
			this.lblCat4.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblCat4.Text = "Category 4";
			this.lblCat4.Top = 1.5F;
			this.lblCat4.Width = 1.9375F;
			// 
			// txtAssessCat3
			// 
			this.txtAssessCat3.Height = 0.1875F;
			this.txtAssessCat3.Left = 4.75F;
			this.txtAssessCat3.Name = "txtAssessCat3";
			this.txtAssessCat3.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtAssessCat3.Text = null;
			this.txtAssessCat3.Top = 1.3125F;
			this.txtAssessCat3.Width = 1.3125F;
			// 
			// txtOrigCat4
			// 
			this.txtOrigCat4.Height = 0.1875F;
			this.txtOrigCat4.Left = 3.375F;
			this.txtOrigCat4.Name = "txtOrigCat4";
			this.txtOrigCat4.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtOrigCat4.Text = null;
			this.txtOrigCat4.Top = 1.5F;
			this.txtOrigCat4.Width = 1.3125F;
			// 
			// txtAssessCat4
			// 
			this.txtAssessCat4.Height = 0.1875F;
			this.txtAssessCat4.Left = 4.75F;
			this.txtAssessCat4.Name = "txtAssessCat4";
			this.txtAssessCat4.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtAssessCat4.Text = null;
			this.txtAssessCat4.Top = 1.5F;
			this.txtAssessCat4.Width = 1.3125F;
			// 
			// txtOrigCat5
			// 
			this.txtOrigCat5.Height = 0.1875F;
			this.txtOrigCat5.Left = 3.375F;
			this.txtOrigCat5.Name = "txtOrigCat5";
			this.txtOrigCat5.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtOrigCat5.Text = null;
			this.txtOrigCat5.Top = 1.6875F;
			this.txtOrigCat5.Width = 1.3125F;
			// 
			// lblCat5
			// 
			this.lblCat5.Height = 0.1875F;
			this.lblCat5.HyperLink = null;
			this.lblCat5.Left = 1.375F;
			this.lblCat5.Name = "lblCat5";
			this.lblCat5.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblCat5.Text = "Category 5";
			this.lblCat5.Top = 1.6875F;
			this.lblCat5.Width = 1.9375F;
			// 
			// lblCat6
			// 
			this.lblCat6.Height = 0.1875F;
			this.lblCat6.HyperLink = null;
			this.lblCat6.Left = 1.375F;
			this.lblCat6.Name = "lblCat6";
			this.lblCat6.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblCat6.Text = "Category 6";
			this.lblCat6.Top = 1.875F;
			this.lblCat6.Width = 1.9375F;
			// 
			// txtAssessCat5
			// 
			this.txtAssessCat5.Height = 0.1875F;
			this.txtAssessCat5.Left = 4.75F;
			this.txtAssessCat5.Name = "txtAssessCat5";
			this.txtAssessCat5.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtAssessCat5.Text = null;
			this.txtAssessCat5.Top = 1.6875F;
			this.txtAssessCat5.Width = 1.3125F;
			// 
			// txtOrigCat6
			// 
			this.txtOrigCat6.Height = 0.1875F;
			this.txtOrigCat6.Left = 3.375F;
			this.txtOrigCat6.Name = "txtOrigCat6";
			this.txtOrigCat6.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtOrigCat6.Text = null;
			this.txtOrigCat6.Top = 1.875F;
			this.txtOrigCat6.Width = 1.3125F;
			// 
			// txtAssessCat6
			// 
			this.txtAssessCat6.Height = 0.1875F;
			this.txtAssessCat6.Left = 4.75F;
			this.txtAssessCat6.Name = "txtAssessCat6";
			this.txtAssessCat6.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtAssessCat6.Text = null;
			this.txtAssessCat6.Top = 1.875F;
			this.txtAssessCat6.Width = 1.3125F;
			// 
			// txtOrigCat7
			// 
			this.txtOrigCat7.Height = 0.1875F;
			this.txtOrigCat7.Left = 3.375F;
			this.txtOrigCat7.Name = "txtOrigCat7";
			this.txtOrigCat7.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtOrigCat7.Text = null;
			this.txtOrigCat7.Top = 2.0625F;
			this.txtOrigCat7.Width = 1.3125F;
			// 
			// lblCat7
			// 
			this.lblCat7.Height = 0.1875F;
			this.lblCat7.HyperLink = null;
			this.lblCat7.Left = 1.375F;
			this.lblCat7.Name = "lblCat7";
			this.lblCat7.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblCat7.Text = "Category 7";
			this.lblCat7.Top = 2.0625F;
			this.lblCat7.Width = 1.9375F;
			// 
			// lblCat8
			// 
			this.lblCat8.Height = 0.1875F;
			this.lblCat8.HyperLink = null;
			this.lblCat8.Left = 1.375F;
			this.lblCat8.Name = "lblCat8";
			this.lblCat8.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblCat8.Text = "Category 8";
			this.lblCat8.Top = 2.25F;
			this.lblCat8.Width = 1.9375F;
			// 
			// txtAssessCat7
			// 
			this.txtAssessCat7.Height = 0.1875F;
			this.txtAssessCat7.Left = 4.75F;
			this.txtAssessCat7.Name = "txtAssessCat7";
			this.txtAssessCat7.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtAssessCat7.Text = null;
			this.txtAssessCat7.Top = 2.0625F;
			this.txtAssessCat7.Width = 1.3125F;
			// 
			// txtOrigCat8
			// 
			this.txtOrigCat8.Height = 0.1875F;
			this.txtOrigCat8.Left = 3.375F;
			this.txtOrigCat8.Name = "txtOrigCat8";
			this.txtOrigCat8.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtOrigCat8.Text = null;
			this.txtOrigCat8.Top = 2.25F;
			this.txtOrigCat8.Width = 1.3125F;
			// 
			// txtAssessCat8
			// 
			this.txtAssessCat8.Height = 0.1875F;
			this.txtAssessCat8.Left = 4.75F;
			this.txtAssessCat8.Name = "txtAssessCat8";
			this.txtAssessCat8.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtAssessCat8.Text = null;
			this.txtAssessCat8.Top = 2.25F;
			this.txtAssessCat8.Width = 1.3125F;
			// 
			// lblCat9
			// 
			this.lblCat9.Height = 0.1875F;
			this.lblCat9.HyperLink = null;
			this.lblCat9.Left = 1.375F;
			this.lblCat9.Name = "lblCat9";
			this.lblCat9.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblCat9.Text = "Category 9";
			this.lblCat9.Top = 2.4375F;
			this.lblCat9.Width = 1.9375F;
			// 
			// txtOrigCat9
			// 
			this.txtOrigCat9.Height = 0.1875F;
			this.txtOrigCat9.Left = 3.375F;
			this.txtOrigCat9.Name = "txtOrigCat9";
			this.txtOrigCat9.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtOrigCat9.Text = null;
			this.txtOrigCat9.Top = 2.4375F;
			this.txtOrigCat9.Width = 1.3125F;
			// 
			// txtAssessCat9
			// 
			this.txtAssessCat9.Height = 0.1875F;
			this.txtAssessCat9.Left = 4.75F;
			this.txtAssessCat9.Name = "txtAssessCat9";
			this.txtAssessCat9.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtAssessCat9.Text = null;
			this.txtAssessCat9.Top = 2.4375F;
			this.txtAssessCat9.Width = 1.3125F;
			// 
			// rptReimbursement2
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.499306F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txttime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txttaxrate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt1st)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txt2nd)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRatioTot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOrigTot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessorTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalOriginal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAssessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOrigCat1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessCat1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOrigCat2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessCat2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOrigCat3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessCat3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOrigCat4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessCat4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOrigCat5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessCat5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOrigCat6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessCat6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOrigCat7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessCat7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOrigCat8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessCat8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOrigCat9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessCat9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txttaxrate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt1st;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txt2nd;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRatioTot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOrigTot;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalCount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalOriginal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalAssessment;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOrigCat1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessCat1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOrigCat2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessCat2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOrigCat3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessCat3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOrigCat4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessCat4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOrigCat5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessCat5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOrigCat6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessCat6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOrigCat7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessCat7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOrigCat8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessCat8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOrigCat9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessCat9;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txttime;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMessage;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessor;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessorTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field14;
	}
}
