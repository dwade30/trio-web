﻿using fecherFoundation;
using System.Runtime.InteropServices;

namespace TWPP0000
{
	public class modPPMR
	{
		//=========================================================
		/// <summary>
		///
		/// </summary>
		public struct MASTERRECORDPP
		{
			public FCFixedString PLACCTNUM;
			// 1
			public FCFixedString PLREALACCT;
			// 7
			public FCFixedString PLREASSOC;
			// 13
			public FCFixedString PSNAME;
			// 19
			public FCFixedString PSADDR1;
			// 53
			public FCFixedString PSADDR2;
			// 87
			public FCFixedString PSADDR3;
			// 121
			public FCFixedString PSSTATE;
			// 145
			public FCFixedString PSZIP;
			// 147
			public FCFixedString PSZIP4;
			// 152
			public FCFixedString PSLOCNUM;
			// 156
			public FCFixedString PSLOCSTREET;
			// 161
			public FCFixedString PIVALUE;
			// 187
			public FCFixedString PIEXEMPTION;
			// 196
			public FCFixedString PIEXEMPTCD1;
			// 205
			public FCFixedString PIEXEMPTCD2;
			// 207
			public FCFixedString PITRANCODE;
			// 209
			public FCFixedString PIBUSCODE;
			// 210
			public FCFixedString PISTRCODE;
			// 212
			public FCFixedString PSOPEN1;
			// 216
			public FCFixedString PSOPEN2;
			// 250
			public FCFixedString PIDATEMO;
			// 284
			public FCFixedString PIDATEDAY;
			// 286
			public FCFixedString PIDATEYEAR;
			// 288
			public FCFixedString PICOMPVAL;
			// 290
			public FCFixedString PIORCODE;
			// 299
			public FCFixedString PIUPDCODE;
			// 300
			public FCFixedString PSEXPANSION;
			// 301      EXP
			public FCFixedString PSTYPE1;
			// 306
			public FCFixedString PLREC1;
			// 307
			public FCFixedString PSTYPE2;
			// 313
			public FCFixedString PLREC2;
			// 314
			public FCFixedString PSTYPE3;
			// 320
			public FCFixedString PLREC3;
			// 321
			public FCFixedString PSTYPE4;
			// 327
			public FCFixedString PLREC4;
			// 328
			public FCFixedString PSTYPE5;
			// 334
			public FCFixedString PLREC5;
			// 335
			public FCFixedString PSTYPE6;
			// 341
			public FCFixedString PLREC6;
			// 342
			public FCFixedString PSTYPE7;
			// 348
			public FCFixedString PLREC7;
			// 349
			public FCFixedString PSTYPE8;
			// 355
			public FCFixedString PLREC8;
			// 356
			public FCFixedString PSTYPE9;
			// 362
			public FCFixedString PLREC9;
			// 363
			public FCFixedString PLNEXTACCT;
			// 369
			// RECORD LENGTH = 374
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public MASTERRECORDPP(int unusedParam)
			{
				this.PLACCTNUM = new FCFixedString(6);
				this.PLREALACCT = new FCFixedString(6);
				this.PLREASSOC = new FCFixedString(6);
				this.PSNAME = new FCFixedString(34);
				this.PSADDR1 = new FCFixedString(34);
				this.PSADDR2 = new FCFixedString(34);
				this.PSADDR3 = new FCFixedString(24);
				this.PSSTATE = new FCFixedString(2);
				this.PSZIP = new FCFixedString(5);
				this.PSZIP4 = new FCFixedString(4);
				this.PSLOCNUM = new FCFixedString(5);
				this.PSLOCSTREET = new FCFixedString(26);
				this.PIVALUE = new FCFixedString(9);
				this.PIEXEMPTION = new FCFixedString(9);
				this.PIEXEMPTCD1 = new FCFixedString(2);
				this.PIEXEMPTCD2 = new FCFixedString(2);
				this.PITRANCODE = new FCFixedString(1);
				this.PIBUSCODE = new FCFixedString(2);
				this.PISTRCODE = new FCFixedString(4);
				this.PSOPEN1 = new FCFixedString(34);
				this.PSOPEN2 = new FCFixedString(34);
				this.PIDATEDAY = new FCFixedString(2);
				this.PIDATEMO = new FCFixedString(2);
				this.PIDATEYEAR = new FCFixedString(2);
				this.PICOMPVAL = new FCFixedString(9);
				this.PIORCODE = new FCFixedString(1);
				this.PIUPDCODE = new FCFixedString(1);
				this.PSEXPANSION = new FCFixedString(5);
				this.PSTYPE1 = new FCFixedString(1);
				this.PLREC1 = new FCFixedString(6);
				this.PSTYPE2 = new FCFixedString(1);
				this.PLREC2 = new FCFixedString(6);
				this.PSTYPE3 = new FCFixedString(1);
				this.PLREC3 = new FCFixedString(6);
				this.PSTYPE4 = new FCFixedString(1);
				this.PLREC4 = new FCFixedString(6);
				this.PSTYPE5 = new FCFixedString(1);
				this.PLREC5 = new FCFixedString(6);
				this.PSTYPE6 = new FCFixedString(1);
				this.PLREC6 = new FCFixedString(6);
				this.PSTYPE7 = new FCFixedString(1);
				this.PLREC7 = new FCFixedString(6);
				this.PSTYPE8 = new FCFixedString(1);
				this.PLREC8 = new FCFixedString(6);
				this.PSTYPE9 = new FCFixedString(1);
				this.PLREC9 = new FCFixedString(6);
				this.PLNEXTACCT = new FCFixedString(6);
			}
		};
		/// <summary>
		///
		/// </summary>
		[StructLayout(LayoutKind.Sequential)]
		public struct PPCOSTRECORD
		{
			public int CAMT;
			// 8
			public FCFixedString CDESC;
			public FCFixedString CSOURCE;
			public FCFixedString CYEAR;
			public FCFixedString CMISC;
			// rec length 40
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public PPCOSTRECORD(int unusedParam)
			{
				this.CAMT = 0;
				this.CDESC = new FCFixedString(22);
				this.CSOURCE = new FCFixedString(2);
				this.CYEAR = new FCFixedString(2);
				this.CMISC = new FCFixedString(6);
			}
		};
		/// <summary>
		///
		/// </summary>
		public struct VALUERECORD
		{
			public FCFixedString VSACCTNUM;
			public FCFixedString VSVALUE1;
			// 375
			public FCFixedString VSVALUE2;
			// 384
			public FCFixedString VSVALUE3;
			// 393
			public FCFixedString VSVALUE4;
			// 402
			public FCFixedString VSVALUE5;
			// 411
			public FCFixedString VSVALUE6;
			// 420
			public FCFixedString VSVALUE7;
			// 429
			public FCFixedString VSVALUE8;
			// 438
			public FCFixedString VSVALUE9;
			// 447
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public VALUERECORD(int unusedParam)
			{
				this.VSACCTNUM = new FCFixedString(6);
				this.VSVALUE1 = new FCFixedString(9);
				this.VSVALUE2 = new FCFixedString(9);
				this.VSVALUE3 = new FCFixedString(9);
				this.VSVALUE4 = new FCFixedString(9);
				this.VSVALUE5 = new FCFixedString(9);
				this.VSVALUE6 = new FCFixedString(9);
				this.VSVALUE7 = new FCFixedString(9);
				this.VSVALUE8 = new FCFixedString(9);
				this.VSVALUE9 = new FCFixedString(9);
			}
		};

		public struct LEASEDRECORD
		{
			public FCFixedString LRECNUMBER;
			public FCFixedString LGRP1;
			public FCFixedString LGRP2;
			public FCFixedString LGRP3;
			public FCFixedString LGRP4;
			public FCFixedString LGRP5;
			public FCFixedString LGRP6;
			public FCFixedString LGRP7;
			public FCFixedString LGRP8;
			public FCFixedString LGRP9;
			public FCFixedString LGRP10;
			public FCFixedString LGRP11;
			public FCFixedString LGRP12;
			public FCFixedString LGRP13;
			public FCFixedString LGRP14;
			public FCFixedString LGRP15;
			public FCFixedString LGRP16;
			// RECORD LENGTH = 1046
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public LEASEDRECORD(int unusedParam)
			{
				this.LRECNUMBER = new FCFixedString(6);
				this.LGRP1 = new FCFixedString(65);
				this.LGRP10 = new FCFixedString(65);
				this.LGRP11 = new FCFixedString(65);
				this.LGRP12 = new FCFixedString(65);
				this.LGRP13 = new FCFixedString(65);
				this.LGRP14 = new FCFixedString(65);
				this.LGRP15 = new FCFixedString(65);
				this.LGRP16 = new FCFixedString(65);
				this.LGRP2 = new FCFixedString(65);
				this.LGRP3 = new FCFixedString(65);
				this.LGRP4 = new FCFixedString(65);
				this.LGRP5 = new FCFixedString(65);
				this.LGRP6 = new FCFixedString(65);
				this.LGRP7 = new FCFixedString(65);
				this.LGRP8 = new FCFixedString(65);
				this.LGRP9 = new FCFixedString(65);
			}
		};

		public struct ITEMIZEDRECORD
		{
			public FCFixedString IRECNUMBER;
			public FCFixedString IGRP1;
			public FCFixedString IGRP2;
			public FCFixedString IGRP3;
			public FCFixedString IGRP4;
			public FCFixedString IGRP5;
			public FCFixedString IGRP6;
			public FCFixedString IGRP7;
			public FCFixedString IGRP8;
			public FCFixedString IGRP9;
			public FCFixedString IGRP10;
			public FCFixedString IGRP11;
			public FCFixedString IGRP12;
			public FCFixedString IGRP13;
			public FCFixedString IGRP14;
			public FCFixedString IGRP15;
			public FCFixedString IGRP16;
			// RECORD LENGTH = 982
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public ITEMIZEDRECORD(int unusedParam)
			{
				this.IRECNUMBER = new FCFixedString(6);
				this.IGRP1 = new FCFixedString(61);
				this.IGRP10 = new FCFixedString(61);
				this.IGRP11 = new FCFixedString(61);
				this.IGRP12 = new FCFixedString(61);
				this.IGRP13 = new FCFixedString(61);
				this.IGRP14 = new FCFixedString(61);
				this.IGRP15 = new FCFixedString(61);
				this.IGRP16 = new FCFixedString(61);
				this.IGRP2 = new FCFixedString(61);
				this.IGRP3 = new FCFixedString(61);
				this.IGRP4 = new FCFixedString(61);
				this.IGRP5 = new FCFixedString(61);
				this.IGRP6 = new FCFixedString(61);
				this.IGRP7 = new FCFixedString(61);
				this.IGRP8 = new FCFixedString(61);
				this.IGRP9 = new FCFixedString(61);
			}
		};

		public class StaticVariables
		{
			//Fecher vbPorter - Version 1.0.0.32
			public MASTERRECORDPP PMR = new MASTERRECORDPP(0);
			public PPCOSTRECORD PPCR = new PPCOSTRECORD(0);
			public VALUERECORD vr = new VALUERECORD(0);
			public LEASEDRECORD LLR = new LEASEDRECORD(0);
			public ITEMIZEDRECORD IR = new ITEMIZEDRECORD(0);
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
