﻿namespace TWPP0000
{
	/// <summary>
	/// Summary description for rptBETEApplication.
	/// </summary>
	partial class rptBETEApplication
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptBETEApplication));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblApplication = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAcct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAddr1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddr2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddr3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddr4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtBusinessType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtBusiness = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOwner = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblMessage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSignedDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEffectiveTaxYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblApplication)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddr1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddr2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddr3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddr4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBusinessType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBusiness)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSignedDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEffectiveTaxYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAddr1,
				this.txtAddr2,
				this.txtAddr3,
				this.txtAddr4,
				this.SubReport1,
				this.Label23,
				this.txtBusinessType,
				this.Label24,
				this.txtLocation,
				this.Label25,
				this.Label26,
				this.Label27,
				this.Label28,
				this.Label29,
				this.Label30,
				this.Line9,
				this.Line10,
				this.Line11,
				this.Line12,
				this.Line13,
				this.Label31,
				this.Label32,
				this.Label33,
				this.Line14,
				this.Line15,
				this.Label34,
				this.Label35,
				this.txtBusiness,
				this.txtOwner,
				this.lblMessage
			});
			this.Detail.Height = 2.614583F;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label2,
				this.lblApplication
			});
			this.PageHeader.Height = 0.6458333F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.DataField = "grpHeader";
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAcct,
				this.Label19,
				this.Label36,
				this.lblPage
			});
			this.GroupHeader1.GroupKeepTogether = GrapeCity.ActiveReports.SectionReportModel.GroupKeepTogether.FirstDetail;
			this.GroupHeader1.Height = 0.2083333F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label20,
				this.Label21,
				this.Field5,
				this.Field6,
				this.Line1,
				this.Field7,
				this.Line2,
				this.Field8,
				this.Line3,
				this.Field9,
				this.Line4,
				this.Field10,
				this.Field11,
				this.txtSignedDate,
				this.Label22,
				this.Line5,
				this.Line6,
				this.Field12,
				this.Field13,
				this.Line7,
				this.Field14,
				this.Line8,
				this.Field15,
				this.txtEffectiveTaxYear,
				this.Line16
			});
			this.GroupFooter1.Height = 2.96875F;
			this.GroupFooter1.KeepTogether = true;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 2.0625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.Label1.Text = "Business Equipment Tax Exemption Application";
			this.Label1.Top = 0F;
			this.Label1.Width = 3.510417F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 2.0625F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: center";
			this.Label2.Text = "(Title 36  Ã¯Â¿Â½ 691 - 700B)";
			this.Label2.Top = 0.21875F;
			this.Label2.Width = 3.125F;
			// 
			// lblApplication
			// 
			this.lblApplication.Height = 0.1875F;
			this.lblApplication.HyperLink = null;
			this.lblApplication.Left = 2.0625F;
			this.lblApplication.Name = "lblApplication";
			this.lblApplication.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center";
			this.lblApplication.Text = "Annual Application for Tax year April 1,";
			this.lblApplication.Top = 0.40625F;
			this.lblApplication.Width = 3.5F;
			// 
			// txtAcct
			// 
			this.txtAcct.DataField = "groupbinder";
			this.txtAcct.Height = 0.1770833F;
			this.txtAcct.Left = 0.75F;
			this.txtAcct.Name = "txtAcct";
			this.txtAcct.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left";
			this.txtAcct.Text = null;
			this.txtAcct.Top = 0.02083333F;
			this.txtAcct.Width = 0.7604167F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1770833F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 0F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-size: 9pt; font-weight: bold";
			this.Label19.Text = "Account";
			this.Label19.Top = 0.02083333F;
			this.Label19.Width = 0.6875F;
			// 
			// Label36
			// 
			this.Label36.Height = 0.1770833F;
			this.Label36.HyperLink = null;
			this.Label36.Left = 2.072917F;
			this.Label36.Name = "Label36";
			this.Label36.Style = "font-size: 9pt";
			this.Label36.Text = "Please sign this form and return to the assessor\'s office";
			this.Label36.Top = 0.02083333F;
			this.Label36.Width = 3.520833F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1770833F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 6.5625F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-size: 9pt; text-align: right";
			this.lblPage.Text = "Page";
			this.lblPage.Top = 0F;
			this.lblPage.Width = 0.8125F;
			// 
			// txtAddr1
			// 
			this.txtAddr1.Height = 0.1875F;
			this.txtAddr1.Left = 0.5F;
			this.txtAddr1.Name = "txtAddr1";
			this.txtAddr1.Style = "font-family: \'Tahoma\'";
			this.txtAddr1.Text = "Field1";
			this.txtAddr1.Top = 1.25F;
			this.txtAddr1.Width = 3.3125F;
			// 
			// txtAddr2
			// 
			this.txtAddr2.Height = 0.1875F;
			this.txtAddr2.Left = 0.5F;
			this.txtAddr2.Name = "txtAddr2";
			this.txtAddr2.Style = "font-family: \'Tahoma\'";
			this.txtAddr2.Text = "Field2";
			this.txtAddr2.Top = 1.4375F;
			this.txtAddr2.Width = 3.3125F;
			// 
			// txtAddr3
			// 
			this.txtAddr3.Height = 0.1875F;
			this.txtAddr3.Left = 0.5F;
			this.txtAddr3.Name = "txtAddr3";
			this.txtAddr3.Style = "font-family: \'Tahoma\'";
			this.txtAddr3.Text = "Field3";
			this.txtAddr3.Top = 1.625F;
			this.txtAddr3.Width = 3.3125F;
			// 
			// txtAddr4
			// 
			this.txtAddr4.Height = 0.1875F;
			this.txtAddr4.Left = 0.5F;
			this.txtAddr4.Name = "txtAddr4";
			this.txtAddr4.Style = "font-family: \'Tahoma\'";
			this.txtAddr4.Text = "Field4";
			this.txtAddr4.Top = 1.8125F;
			this.txtAddr4.Width = 3.3125F;
			// 
			// SubReport1
			// 
			this.SubReport1.CloseBorder = false;
			this.SubReport1.Height = 0.09375F;
			this.SubReport1.Left = 0F;
			this.SubReport1.Name = "SubReport1";
			this.SubReport1.Report = null;
			this.SubReport1.Top = 2.53125F;
			this.SubReport1.Width = 7.4375F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.1666667F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 3.71875F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-size: 9pt; font-weight: bold";
			this.Label23.Text = "Type of Business";
			this.Label23.Top = 0.02083333F;
			this.Label23.Width = 1.145833F;
			// 
			// txtBusinessType
			// 
			this.txtBusinessType.DataField = "groupbinder";
			this.txtBusinessType.Height = 0.1666667F;
			this.txtBusinessType.Left = 4.9375F;
			this.txtBusinessType.Name = "txtBusinessType";
			this.txtBusinessType.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left";
			this.txtBusinessType.Text = null;
			this.txtBusinessType.Top = 0.02083333F;
			this.txtBusinessType.Width = 2.4375F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.1666667F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 3.71875F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-size: 9pt; font-weight: bold";
			this.Label24.Text = "Location";
			this.Label24.Top = 0.1875F;
			this.Label24.Width = 1.145833F;
			// 
			// txtLocation
			// 
			this.txtLocation.DataField = "groupbinder";
			this.txtLocation.Height = 0.1666667F;
			this.txtLocation.Left = 4.9375F;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left";
			this.txtLocation.Text = null;
			this.txtLocation.Top = 0.1875F;
			this.txtLocation.Width = 2.4375F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.1666667F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 0.09375F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-size: 9pt; font-weight: bold";
			this.Label25.Text = "Ownership:";
			this.Label25.Top = 0.4166667F;
			this.Label25.Width = 0.7916667F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.1666667F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 1.15625F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-size: 9pt; font-weight: bold";
			this.Label26.Text = "Sole Proprietor";
			this.Label26.Top = 0.4166667F;
			this.Label26.Width = 1.010417F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.1666667F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 2.46875F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-size: 9pt; font-weight: bold";
			this.Label27.Text = "Corporation";
			this.Label27.Top = 0.4166667F;
			this.Label27.Width = 0.84375F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.1666667F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 3.59375F;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-size: 9pt; font-weight: bold";
			this.Label28.Text = "LLC";
			this.Label28.Top = 0.4166667F;
			this.Label28.Width = 0.28125F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.1666667F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 4.21875F;
			this.Label29.Name = "Label29";
			this.Label29.Style = "font-size: 9pt; font-weight: bold";
			this.Label29.Text = "Partnership";
			this.Label29.Top = 0.4166667F;
			this.Label29.Width = 0.8229167F;
			// 
			// Label30
			// 
			this.Label30.Height = 0.1666667F;
			this.Label30.HyperLink = null;
			this.Label30.Left = 5.15625F;
			this.Label30.Name = "Label30";
			this.Label30.Style = "font-size: 9pt; font-weight: bold";
			this.Label30.Text = "Other:";
			this.Label30.Top = 0.4166667F;
			this.Label30.Width = 0.4791667F;
			// 
			// Line9
			// 
			this.Line9.Height = 0F;
			this.Line9.Left = 0.8645833F;
			this.Line9.LineWeight = 1F;
			this.Line9.Name = "Line9";
			this.Line9.Top = 0.5625F;
			this.Line9.Width = 0.2604167F;
			this.Line9.X1 = 0.8645833F;
			this.Line9.X2 = 1.125F;
			this.Line9.Y1 = 0.5625F;
			this.Line9.Y2 = 0.5625F;
			// 
			// Line10
			// 
			this.Line10.Height = 0F;
			this.Line10.Left = 2.1875F;
			this.Line10.LineWeight = 1F;
			this.Line10.Name = "Line10";
			this.Line10.Top = 0.5625F;
			this.Line10.Width = 0.2604167F;
			this.Line10.X1 = 2.1875F;
			this.Line10.X2 = 2.447917F;
			this.Line10.Y1 = 0.5625F;
			this.Line10.Y2 = 0.5625F;
			// 
			// Line11
			// 
			this.Line11.Height = 0F;
			this.Line11.Left = 3.3125F;
			this.Line11.LineWeight = 1F;
			this.Line11.Name = "Line11";
			this.Line11.Top = 0.5625F;
			this.Line11.Width = 0.2604167F;
			this.Line11.X1 = 3.3125F;
			this.Line11.X2 = 3.572917F;
			this.Line11.Y1 = 0.5625F;
			this.Line11.Y2 = 0.5625F;
			// 
			// Line12
			// 
			this.Line12.Height = 0F;
			this.Line12.Left = 3.9375F;
			this.Line12.LineWeight = 1F;
			this.Line12.Name = "Line12";
			this.Line12.Top = 0.5625F;
			this.Line12.Width = 0.2604165F;
			this.Line12.X1 = 3.9375F;
			this.Line12.X2 = 4.197917F;
			this.Line12.Y1 = 0.5625F;
			this.Line12.Y2 = 0.5625F;
			// 
			// Line13
			// 
			this.Line13.Height = 0F;
			this.Line13.Left = 5.625F;
			this.Line13.LineWeight = 1F;
			this.Line13.Name = "Line13";
			this.Line13.Top = 0.5625F;
			this.Line13.Width = 1.697917F;
			this.Line13.X1 = 5.625F;
			this.Line13.X2 = 7.322917F;
			this.Line13.Y1 = 0.5625F;
			this.Line13.Y2 = 0.5625F;
			// 
			// Label31
			// 
			this.Label31.Height = 0.1666667F;
			this.Label31.HyperLink = null;
			this.Label31.Left = 0F;
			this.Label31.Name = "Label31";
			this.Label31.Style = "font-size: 9pt; font-weight: bold";
			this.Label31.Text = "Is this property located in a tax increment financing (TIF) district";
			this.Label31.Top = 0.625F;
			this.Label31.Width = 4.28125F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.1666667F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 4.28125F;
			this.Label32.Name = "Label32";
			this.Label32.Style = "font-size: 9pt; font-weight: bold";
			this.Label32.Text = "Yes";
			this.Label32.Top = 0.625F;
			this.Label32.Width = 0.28125F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.1666667F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 5.15625F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-size: 9pt; font-weight: bold";
			this.Label33.Text = "No";
			this.Label33.Top = 0.625F;
			this.Label33.Width = 0.28125F;
			// 
			// Line14
			// 
			this.Line14.Height = 0F;
			this.Line14.Left = 4.65625F;
			this.Line14.LineWeight = 1F;
			this.Line14.Name = "Line14";
			this.Line14.Top = 0.7708333F;
			this.Line14.Width = 0.2604165F;
			this.Line14.X1 = 4.65625F;
			this.Line14.X2 = 4.916667F;
			this.Line14.Y1 = 0.7708333F;
			this.Line14.Y2 = 0.7708333F;
			// 
			// Line15
			// 
			this.Line15.Height = 0F;
			this.Line15.Left = 5.53125F;
			this.Line15.LineWeight = 1F;
			this.Line15.Name = "Line15";
			this.Line15.Top = 0.7708333F;
			this.Line15.Width = 0.2604165F;
			this.Line15.X1 = 5.53125F;
			this.Line15.X2 = 5.791667F;
			this.Line15.Y1 = 0.7708333F;
			this.Line15.Y2 = 0.7708333F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.1666667F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 0.03125F;
			this.Label34.Name = "Label34";
			this.Label34.Style = "font-size: 9pt; font-weight: bold";
			this.Label34.Text = "Business";
			this.Label34.Top = 0.02083333F;
			this.Label34.Width = 0.6458333F;
			// 
			// Label35
			// 
			this.Label35.Height = 0.1666667F;
			this.Label35.HyperLink = null;
			this.Label35.Left = 0.03125F;
			this.Label35.Name = "Label35";
			this.Label35.Style = "font-size: 9pt; font-weight: bold";
			this.Label35.Text = "Owner";
			this.Label35.Top = 0.1875F;
			this.Label35.Width = 0.6458333F;
			// 
			// txtBusiness
			// 
			this.txtBusiness.DataField = "groupbinder";
			this.txtBusiness.Height = 0.1666667F;
			this.txtBusiness.Left = 0.6875F;
			this.txtBusiness.Name = "txtBusiness";
			this.txtBusiness.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left";
			this.txtBusiness.Text = null;
			this.txtBusiness.Top = 0.02083333F;
			this.txtBusiness.Width = 2.9375F;
			// 
			// txtOwner
			// 
			this.txtOwner.DataField = "groupbinder";
			this.txtOwner.Height = 0.1666667F;
			this.txtOwner.Left = 0.6875F;
			this.txtOwner.Name = "txtOwner";
			this.txtOwner.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left";
			this.txtOwner.Text = null;
			this.txtOwner.Top = 0.1875F;
			this.txtOwner.Width = 2.9375F;
			// 
			// lblMessage
			// 
			this.lblMessage.Height = 1.385417F;
			this.lblMessage.HyperLink = null;
			this.lblMessage.Left = 3.885417F;
			this.lblMessage.Name = "lblMessage";
			this.lblMessage.Style = "";
			this.lblMessage.Text = "Label37";
			this.lblMessage.Top = 0.9270833F;
			this.lblMessage.Width = 3.510417F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1666667F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 0.07291666F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-size: 8.5pt; text-align: center";
			this.Label20.Text = "[An individual who knowingly gives false information for the purpose of claiming " + "this exemption commits a Class E Crime]";
			this.Label20.Top = 0.1354167F;
			this.Label20.Width = 7.354167F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.5F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 0.05208333F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-size: 9pt";
			this.Label21.Text = resources.GetString("Label21.Text");
			this.Label21.Top = 0.3333333F;
			this.Label21.Width = 7.364583F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.1770833F;
			this.Field5.Left = 0.05208333F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-size: 9pt; font-weight: bold";
			this.Field5.Text = "Signatures";
			this.Field5.Top = 0.875F;
			this.Field5.Width = 0.9166667F;
			// 
			// Field6
			// 
			this.Field6.Height = 0.1770833F;
			this.Field6.Left = 0.25F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-size: 9pt; font-weight: bold";
			this.Field6.Text = "Applicant";
			this.Field6.Top = 1.166667F;
			this.Field6.Width = 0.9166667F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 1.010417F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.291667F;
			this.Line1.Width = 3.010417F;
			this.Line1.X1 = 1.010417F;
			this.Line1.X2 = 4.020833F;
			this.Line1.Y1 = 1.291667F;
			this.Line1.Y2 = 1.291667F;
			// 
			// Field7
			// 
			this.Field7.Height = 0.1770833F;
			this.Field7.Left = 4.0625F;
			this.Field7.Name = "Field7";
			this.Field7.Style = "font-size: 9pt; font-weight: bold";
			this.Field7.Text = "Date";
			this.Field7.Top = 1.166667F;
			this.Field7.Width = 0.4166667F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 4.510417F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 1.291667F;
			this.Line2.Width = 0.8645835F;
			this.Line2.X1 = 4.510417F;
			this.Line2.X2 = 5.375F;
			this.Line2.Y1 = 1.291667F;
			this.Line2.Y2 = 1.291667F;
			// 
			// Field8
			// 
			this.Field8.Height = 0.1770833F;
			this.Field8.Left = 5.4375F;
			this.Field8.Name = "Field8";
			this.Field8.Style = "font-size: 9pt; font-weight: bold";
			this.Field8.Text = "Telephone";
			this.Field8.Top = 1.166667F;
			this.Field8.Width = 0.9166667F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 6.197917F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 1.291667F;
			this.Line3.Width = 1.125F;
			this.Line3.X1 = 6.197917F;
			this.Line3.X2 = 7.322917F;
			this.Line3.Y1 = 1.291667F;
			this.Line3.Y2 = 1.291667F;
			// 
			// Field9
			// 
			this.Field9.Height = 0.1770833F;
			this.Field9.Left = 0.25F;
			this.Field9.Name = "Field9";
			this.Field9.Style = "font-size: 9pt; font-weight: bold";
			this.Field9.Text = "Preparer";
			this.Field9.Top = 1.59375F;
			this.Field9.Width = 0.9166667F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 1.010417F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 1.729167F;
			this.Line4.Width = 3.010417F;
			this.Line4.X1 = 1.010417F;
			this.Line4.X2 = 4.020833F;
			this.Line4.Y1 = 1.729167F;
			this.Line4.Y2 = 1.729167F;
			// 
			// Field10
			// 
			this.Field10.Height = 0.1770833F;
			this.Field10.Left = 4.0625F;
			this.Field10.Name = "Field10";
			this.Field10.Style = "font-size: 9pt; font-weight: bold";
			this.Field10.Text = "Date";
			this.Field10.Top = 1.604167F;
			this.Field10.Width = 0.4166667F;
			// 
			// Field11
			// 
			this.Field11.Height = 0.1770833F;
			this.Field11.Left = 5.4375F;
			this.Field11.Name = "Field11";
			this.Field11.Style = "font-size: 9pt; font-weight: bold";
			this.Field11.Text = "Telephone";
			this.Field11.Top = 1.604167F;
			this.Field11.Width = 0.7916667F;
			// 
			// txtSignedDate
			// 
			this.txtSignedDate.Height = 0.1770833F;
			this.txtSignedDate.Left = 4.854167F;
			this.txtSignedDate.Name = "txtSignedDate";
			this.txtSignedDate.Text = null;
			this.txtSignedDate.Top = 2.697917F;
			this.txtSignedDate.Visible = false;
			this.txtSignedDate.Width = 0.9375F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.1875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 1.760417F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center";
			this.Label22.Text = "*************** FOR ASSESSORS ONLY ***************";
			this.Label22.Top = 2.010417F;
			this.Label22.Width = 4.125F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 6.197917F;
			this.Line5.LineWeight = 1F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 1.729167F;
			this.Line5.Width = 1.125F;
			this.Line5.X1 = 6.197917F;
			this.Line5.X2 = 7.322917F;
			this.Line5.Y1 = 1.729167F;
			this.Line5.Y2 = 1.729167F;
			// 
			// Line6
			// 
			this.Line6.Height = 0F;
			this.Line6.Left = 4.510417F;
			this.Line6.LineWeight = 1F;
			this.Line6.Name = "Line6";
			this.Line6.Top = 1.729167F;
			this.Line6.Width = 0.8645835F;
			this.Line6.X1 = 4.510417F;
			this.Line6.X2 = 5.375F;
			this.Line6.Y1 = 1.729167F;
			this.Line6.Y2 = 1.729167F;
			// 
			// Field12
			// 
			this.Field12.Height = 0.1770833F;
			this.Field12.Left = 0.25F;
			this.Field12.Name = "Field12";
			this.Field12.Style = "font-size: 9pt; font-weight: bold";
			this.Field12.Text = "Approval Date";
			this.Field12.Top = 2.270833F;
			this.Field12.Width = 1.041667F;
			// 
			// Field13
			// 
			this.Field13.Height = 0.1770833F;
			this.Field13.Left = 0.25F;
			this.Field13.Name = "Field13";
			this.Field13.Style = "font-size: 9pt; font-weight: bold";
			this.Field13.Text = "Approved By";
			this.Field13.Top = 2.697917F;
			this.Field13.Width = 1.041667F;
			// 
			// Line7
			// 
			this.Line7.Height = 0F;
			this.Line7.Left = 1.197917F;
			this.Line7.LineWeight = 1F;
			this.Line7.Name = "Line7";
			this.Line7.Top = 2.875F;
			this.Line7.Width = 3.010417F;
			this.Line7.X1 = 1.197917F;
			this.Line7.X2 = 4.208333F;
			this.Line7.Y1 = 2.875F;
			this.Line7.Y2 = 2.875F;
			// 
			// Field14
			// 
			this.Field14.Height = 0.1770833F;
			this.Field14.Left = 4.375F;
			this.Field14.Name = "Field14";
			this.Field14.Style = "font-size: 9pt; font-weight: bold";
			this.Field14.Text = "Date";
			this.Field14.Top = 2.697917F;
			this.Field14.Width = 0.4166667F;
			// 
			// Line8
			// 
			this.Line8.Height = 0F;
			this.Line8.Left = 1.322917F;
			this.Line8.LineWeight = 1F;
			this.Line8.Name = "Line8";
			this.Line8.Top = 2.447917F;
			this.Line8.Width = 0.8645834F;
			this.Line8.X1 = 1.322917F;
			this.Line8.X2 = 2.1875F;
			this.Line8.Y1 = 2.447917F;
			this.Line8.Y2 = 2.447917F;
			// 
			// Field15
			// 
			this.Field15.Height = 0.1770833F;
			this.Field15.Left = 2.5F;
			this.Field15.Name = "Field15";
			this.Field15.Style = "font-size: 9pt; font-weight: bold";
			this.Field15.Text = "Effective Tax Year";
			this.Field15.Top = 2.270833F;
			this.Field15.Width = 1.375F;
			// 
			// txtEffectiveTaxYear
			// 
			this.txtEffectiveTaxYear.Height = 0.1770833F;
			this.txtEffectiveTaxYear.Left = 3.885417F;
			this.txtEffectiveTaxYear.Name = "txtEffectiveTaxYear";
			this.txtEffectiveTaxYear.Text = null;
			this.txtEffectiveTaxYear.Top = 2.270833F;
			this.txtEffectiveTaxYear.Width = 0.9375F;
			// 
			// Line16
			// 
			this.Line16.Height = 0F;
			this.Line16.Left = 4.822917F;
			this.Line16.LineWeight = 1F;
			this.Line16.Name = "Line16";
			this.Line16.Top = 2.875F;
			this.Line16.Width = 0.8645835F;
			this.Line16.X1 = 4.822917F;
			this.Line16.X2 = 5.6875F;
			this.Line16.Y1 = 2.875F;
			this.Line16.Y2 = 2.875F;
			// 
			// rptBETEApplication
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.458333F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblApplication)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddr1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddr2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddr3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddr4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBusinessType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBusiness)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSignedDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEffectiveTaxYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddr1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddr2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddr3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddr4;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBusinessType;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line9;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line10;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line11;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line12;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line14;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBusiness;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOwner;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMessage;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblApplication;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcct;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label36;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field7;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSignedDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field13;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field14;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEffectiveTaxYear;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line16;
	}
}
