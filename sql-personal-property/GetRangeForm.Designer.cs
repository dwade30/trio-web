﻿//Fecher vbPorter - Version 1.0.0.32
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmGetRangeForm.
	/// </summary>
	partial class frmGetRangeForm : BaseForm
	{
		public fecherFoundation.FCButton cmdOK;
		public fecherFoundation.FCTextBox txtStop;
		public fecherFoundation.FCTextBox txtStart;
		public fecherFoundation.FCLabel lblInformation;
		public fecherFoundation.FCLabel lblStop;
		public fecherFoundation.FCLabel lblStart;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGetRangeForm));
			this.cmdOK = new fecherFoundation.FCButton();
			this.txtStop = new fecherFoundation.FCTextBox();
			this.txtStart = new fecherFoundation.FCTextBox();
			this.lblInformation = new fecherFoundation.FCLabel();
			this.lblStop = new fecherFoundation.FCLabel();
			this.lblStart = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuContinue = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdOK);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtStop);
			this.ClientArea.Controls.Add(this.txtStart);
			this.ClientArea.Controls.Add(this.lblInformation);
			this.ClientArea.Controls.Add(this.lblStop);
			this.ClientArea.Controls.Add(this.lblStart);
			this.ClientArea.Size = new System.Drawing.Size(1078, 498);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(310, 30);
			this.HeaderText.Text = "TRIO Software Corporation";
			// 
			// cmdOK
			// 
			this.cmdOK.AppearanceKey = "acceptButton";
			this.cmdOK.Location = new System.Drawing.Point(87, 27);
			this.cmdOK.Name = "cmdOK";
			this.cmdOK.Size = new System.Drawing.Size(63, 48);
			this.cmdOK.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdOK.TabIndex = 5;
			this.cmdOK.Text = "OK";
			this.cmdOK.Visible = false;
			this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
			// 
			// txtStop
			// 
			this.txtStop.AutoSize = false;
			this.txtStop.BackColor = System.Drawing.SystemColors.Window;
			this.txtStop.LinkItem = null;
			this.txtStop.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtStop.LinkTopic = null;
			this.txtStop.Location = new System.Drawing.Point(319, 160);
			this.txtStop.Name = "txtStop";
			this.txtStop.Size = new System.Drawing.Size(218, 40);
			this.txtStop.TabIndex = 4;
			// 
			// txtStart
			// 
			this.txtStart.AutoSize = false;
			this.txtStart.BackColor = System.Drawing.SystemColors.Window;
			this.txtStart.LinkItem = null;
			this.txtStart.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtStart.LinkTopic = null;
			this.txtStart.Location = new System.Drawing.Point(319, 90);
			this.txtStart.Name = "txtStart";
			this.txtStart.Size = new System.Drawing.Size(218, 40);
			this.txtStart.TabIndex = 3;
			// 
			// lblInformation
			// 
			this.lblInformation.Location = new System.Drawing.Point(30, 30);
			this.lblInformation.Name = "lblInformation";
			this.lblInformation.Size = new System.Drawing.Size(472, 36);
			this.lblInformation.TabIndex = 2;
			this.lblInformation.Text = "PLEASE ENTER THE BEGINNING AND ENDING ENTRIES FOR THE BELOW CATEGORY.  IF BOTH/OR" + " EITHER BEGINNING AND/OR ENDING BOXES ARE LEFT EMPTY THE FIRST/LAST RECORD WILL " + "BE USED";
			// 
			// lblStop
			// 
			this.lblStop.Location = new System.Drawing.Point(30, 174);
			this.lblStop.Name = "lblStop";
			this.lblStop.Size = new System.Drawing.Size(219, 18);
			this.lblStop.TabIndex = 1;
			this.lblStop.Text = "STARTING 1234567890123456789012";
			this.lblStop.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lblStart
			// 
			this.lblStart.Location = new System.Drawing.Point(30, 104);
			this.lblStart.Name = "lblStart";
			this.lblStart.Size = new System.Drawing.Size(219, 15);
			this.lblStart.TabIndex = 0;
			this.lblStart.Text = "STARTING 1234567890123456789012";
			this.lblStart.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuContinue,
				this.mnuSepar,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuContinue
			// 
			this.mnuContinue.Index = 0;
			this.mnuContinue.Name = "mnuContinue";
			this.mnuContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuContinue.Text = "Continue";
			this.mnuContinue.Click += new System.EventHandler(this.mnuContinue_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 1;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // frmGetRangeForm
            // 
            this.AcceptButton = this.cmdOK;
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.KeyPreview = true;
			this.Name = "frmGetRangeForm";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "TRIO Software Corporation";
			this.Load += new System.EventHandler(this.frmGetRangeForm_Load);
			this.Activated += new System.EventHandler(this.frmGetRangeForm_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmGetRangeForm_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
