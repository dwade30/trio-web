﻿//Fecher vbPorter - Version 1.0.0.40
using System.Runtime.InteropServices;

namespace TWGNENTY
{
	public class modTtreupd
	{
		//=========================================================
		[StructLayout(LayoutKind.Sequential)]
		public struct UPDRECORD
		{
			public int UCTDTE;
			// 4   :'  1
			public short UCTLD;
			// 2   :'  5
			public short UCTLM;
			// 2   :'  7
			public short UMAXACCT;
			// 2   :'  9
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 4)]
			public string URSYEAR;
			// 11
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 2)]
			public string URSMONTH;
			// 15
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 4)]
			public string USRYEAR;
			// 17
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 2)]
			public string USRMONTH;
			// 21
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 4)]
			public string UCMYEAR;
			// 23
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 2)]
			public string UCMMONTH;
			// 27
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 4)]
			public string UPPYEAR;
			// 29
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 2)]
			public string UPPMONTH;
			// 33
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 4)]
			public string UBLYEAR;
			// 35
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 2)]
			public string UBLMONTH;
			// 39
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 4)]
			public string UCLYEAR;
			// 41
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 2)]
			public string UCLMONTH;
			// 45
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1)]
			public string UNETWORKFLAG;
			// 47
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1)]
			public string UMAXPP;
			// 48
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 4)]
			public string UVRYEAR;
			// 49
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 2)]
			public string UVRMONTH;
			// 53
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1)]
			public string UMAXVR;
			// 55
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 4)]
			public string UCEYEAR;
			// 56
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 2)]
			public string UCEMONTH;
			// 60
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1)]
			public string UMAXCE;
			// 62
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 4)]
			public string UBDYEAR;
			// 63
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 2)]
			public string UBDMONTH;
			// 67
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 4)]
			public string UCRYEAR;
			// 69
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 2)]
			public string UCRMONTH;
			// 73
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 4)]
			public string UPYYEAR;
			// 75
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 2)]
			public string UPYMONTH;
			// 79
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1)]
			public string UMAXPY;
			// 81
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 4)]
			public string UUTYEAR;
			// 82
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 2)]
			public string UUTMONTH;
			// 86
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1)]
			public string UMAXUT;
			// 88
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1)]
			public string UMVRAPIDRENEWAL;
			// 89
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1)]
			public string UCRMOSES;
			// 90
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 6)]
			public string UEXPANSION;
			// 91
		};
		//
		[StructLayout(LayoutKind.Sequential)]
		public struct UPDRECORD2
		{
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 2)]
			public string U2CODE;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 4)]
			public string U2MVYEAR;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 2)]
			public string U2MVMONTH;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 2)]
			public string U2MAXMV;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 2)]
			public string U2MAXRB;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 4)]
			public string U2TCYEAR;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 2)]
			public string U2TCMONTH;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 4)]
			public string U2E9YEAR;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 2)]
			public string U2E9MONTH;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 4)]
			public string U2CKYEAR;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 2)]
			public string U2CKMONTH;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1)]
			public string U2WINDRE;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1)]
			public string U2WINDPP;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1)]
			public string U2WINDBL;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1)]
			public string U2WINDCL;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1)]
			public string U2WINDCE;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1)]
			public string U2WINDBD;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1)]
			public string U2WINDCR;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1)]
			public string U2WINDPY;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1)]
			public string U2WINDUT;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1)]
			public string U2WINDMV;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1)]
			public string U2WINDTC;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1)]
			public string U2WINDRB;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1)]
			public string U2WINDVR;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 53)]
			public string U2EXPANSION;
		};

		public class StaticVariables
		{
			public UPDRECORD UPD = new UPDRECORD();
			public UPDRECORD2 UP2 = new UPDRECORD2();
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
