﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for frmBETEApplication.
	/// </summary>
	public partial class frmBETEApplication : BaseForm
	{
		public frmBETEApplication()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.txtMessage = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.txtMessage.AddControlArrayElement(txtMessage_2, 2);
			this.txtMessage.AddControlArrayElement(txtMessage_7, 7);
			this.txtMessage.AddControlArrayElement(txtMessage_6, 6);
			this.txtMessage.AddControlArrayElement(txtMessage_5, 5);
			this.txtMessage.AddControlArrayElement(txtMessage_4, 4);
			this.txtMessage.AddControlArrayElement(txtMessage_3, 3);
			this.txtMessage.AddControlArrayElement(txtMessage_0, 0);
			this.txtMessage.AddControlArrayElement(txtMessage_1, 1);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmBETEApplication InstancePtr
		{
			get
			{
				return (frmBETEApplication)Sys.GetInstance(typeof(frmBETEApplication));
			}
		}

		protected frmBETEApplication _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		/// <summary>
		/// ********************************************************
		/// </summary>
		/// <summary>
		/// Property of TRIO Software Corporation
		/// </summary>
		/// <summary>
		/// Written By
		/// </summary>
		/// <summary>
		/// Date
		/// </summary>
		/// <summary>
		/// ********************************************************
		/// </summary>
		private string strOrderBy = string.Empty;
		private string strWhereClause = string.Empty;

		public void Init(ref string strOrder, ref string strWhere)
		{
			strOrderBy = strOrder;
			strWhereClause = strWhere;
			this.Show(App.MainForm);
		}

		private void chkIncludeOther_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkIncludeOther.CheckState == CheckState.Checked)
			{
				chkPendingOnly.CheckState = Wisej.Web.CheckState.Unchecked;
			}
		}

		private void chkPendingOnly_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkPendingOnly.CheckState == CheckState.Checked)
			{
				chkIncludeOther.CheckState = Wisej.Web.CheckState.Unchecked;
			}
		}

		private void frmBETEApplication_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmBETEApplication_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmBETEApplication properties;
			//frmBETEApplication.FillStyle	= 0;
			//frmBETEApplication.ScaleWidth	= 8880;
			//frmBETEApplication.ScaleHeight	= 7530;
			//frmBETEApplication.LinkTopic	= "Form2";
			//frmBETEApplication.LockControls	= true;
			//frmBETEApplication.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			txtTaxYear.Text = FCConvert.ToString(DateTime.Today.Year);
			txtEffectiveTaxYear.Text = FCConvert.ToString(DateTime.Today.Year);
			if (Strings.Trim(modPPGN.Statics.Open1Title) != string.Empty)
			{
				if (cmbOwner.Items.Contains("Open1"))
				{
					cmbOwner.Items.Remove("Open1");
					cmbOwner.Items.Insert(1, modPPGN.Statics.Open1Title);
				}
				if (cmbBusiness.Items.Contains("Open1"))
				{
					cmbBusiness.Items.Remove("Open1");
					cmbBusiness.Items.Insert(1, modPPGN.Statics.Open1Title);
				}
			}
			if (Strings.Trim(modPPGN.Statics.Open2Title) != string.Empty)
			{
				if (cmbOwner.Items.Contains("Open 2"))
				{
					cmbOwner.Items.Remove("Open 2");
					cmbOwner.Items.Insert(2, modPPGN.Statics.Open2Title);
				}
				if (cmbBusiness.Items.Contains("Open 2"))
				{
					cmbBusiness.Items.Remove("Open 2");
					cmbBusiness.Items.Insert(2, modPPGN.Statics.Open2Title);
				}
			}
			int intTemp;
			intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("BETEAppOwnerSetting", "PP", FCConvert.ToString(0)))));
			//FC:FINAL:DDU:#i1512 - change due to invisible index in original
			if (intTemp == 4)
			{
				intTemp--;
			}
			cmbOwner.SelectedIndex = intTemp;
			//if (intTemp != 3)
			//{
			//    cmbOwner.SelectedIndex = intTemp;
			//}
			intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("BETEAppBusinessSetting", "PP", FCConvert.ToString(4)))));
			//FC:FINAL:DDU:#i1512 - change due to invisible index in original
			if (intTemp == 4)
			{
				intTemp--;
			}
			cmbBusiness.SelectedIndex = intTemp;
			//if (intTemp != 3)
			//{
			//	cmbBusiness.SelectedIndex = intTemp;
			//}
			bool boolCurrentBETEOnly;
			boolCurrentBETEOnly = FCConvert.CBool(modRegistry.GetRegistryKey("BETEAppIncludeSetting", "PP", FCConvert.ToString(false)));
			if (boolCurrentBETEOnly)
			{
				chkIncludeOther.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			else
			{
				chkIncludeOther.CheckState = CheckState.Checked;
			}
			bool boolPendingOnly;
			boolPendingOnly = FCConvert.CBool(modRegistry.GetRegistryKey("BETEAppPendingSetting", "PP", FCConvert.ToString(false)));
			if (boolPendingOnly)
			{
				chkPendingOnly.CheckState = CheckState.Checked;
			}
			else
			{
				chkPendingOnly.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			string strTemp;
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset("select betemessage from customize", modPPGN.strPPDatabase);
			strTemp = FCConvert.ToString(rsLoad.Get_Fields_String("BETEMessage"));
			string[] strAry = null;
			int x;
			int intLine;
			intLine = 0;
			strAry = Strings.Split(strTemp, "\r\n", -1, CompareConstants.vbBinaryCompare);
			for (x = 0; x <= Information.UBound(strAry, 1); x++)
			{
				if (intLine < 8)
				{
					strTemp = strAry[x];
					if (strTemp.Length <= txtMessage[FCConvert.ToInt16(intLine)].MaxLength)
					{
						txtMessage[FCConvert.ToInt16(intLine)].Text = strTemp;
						intLine += 1;
					}
					else
					{
						while (strTemp.Length > 0 && intLine < 8)
						{
							txtMessage[FCConvert.ToInt16(intLine)].Text = Strings.Mid(strTemp, 1, txtMessage[FCConvert.ToInt16(intLine)].MaxLength);
							strTemp = Strings.Mid(strTemp, txtMessage[FCConvert.ToInt16(intLine)].MaxLength + 1);
							intLine += 1;
						}
					}
				}
			}
			// x
			// txtMessage.Text = strTemp
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			int intYear;
			int intEffYear;
			intYear = FCConvert.ToInt32(Math.Round(Conversion.Val(txtTaxYear.Text)));
			intEffYear = FCConvert.ToInt32(Math.Round(Conversion.Val(txtEffectiveTaxYear.Text)));
			if (intYear < 2008)
			{
				MessageBox.Show("Invalid Tax Year", "Invalid Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (intEffYear < 2008)
			{
				MessageBox.Show("Invalid Effective Tax Year", "Invalid Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			int intOwner = 0;
			int intBusiness = 0;
			if (cmbOwner.Text == "Name")
			{
				intOwner = 0;
			}
			else if (cmbOwner.Text == "Open1" || cmbOwner.Text == modPPGN.Statics.Open1Title)
			{
				intOwner = 1;
			}
			else if (cmbOwner.Text == "Open 2" || cmbOwner.Text == modPPGN.Statics.Open2Title)
			{
				intOwner = 2;
				// ElseIf optOwner(3).Value Then
				// intOwner = 3
			}
			else if (cmbOwner.Text == "None")
			{
				intOwner = 4;
			}
			if (cmbBusiness.Text == "Name")
			{
				intBusiness = 0;
			}
			else if (cmbBusiness.Text == "Open1" || cmbBusiness.Text == modPPGN.Statics.Open1Title)
			{
				intBusiness = 1;
			}
			else if (cmbBusiness.Text == "Open 2" || cmbBusiness.Text == modPPGN.Statics.Open2Title)
			{
				intBusiness = 2;
				// ElseIf optBusiness(3).Value Then
				// intBusiness = 3
			}
			else if (cmbBusiness.Text == "None")
			{
				intBusiness = 4;
			}
			modRegistry.SaveRegistryKey("BETEAppOwnerSetting", FCConvert.ToString(intOwner), "PP");
			modRegistry.SaveRegistryKey("BETEAppBusinessSetting", FCConvert.ToString(intBusiness), "PP");
			bool boolCurrentBETEOnly = false;
			if (chkIncludeOther.CheckState == CheckState.Checked)
			{
				boolCurrentBETEOnly = false;
			}
			else
			{
				boolCurrentBETEOnly = true;
			}
			modRegistry.SaveRegistryKey("BETEAppIncludeSetting", FCConvert.ToString(boolCurrentBETEOnly), "PP");
			bool boolPending = false;
			if (chkPendingOnly.CheckState == CheckState.Checked)
			{
				boolPending = true;
			}
			else
			{
				boolPending = false;
			}
			modRegistry.SaveRegistryKey("BETEAppPendingSetting", FCConvert.ToString(boolPending), "PP");
			cBETEApplicationReport cReport = new cBETEApplicationReport();
			cReport.BusinessSource = intBusiness;
			cReport.CurrentBETEOnly = boolCurrentBETEOnly;
			cReport.EffectiveYear = intEffYear;
			cReport.OrderByClause = strOrderBy;
			cReport.OwnerSource = intOwner;
			cReport.Pending = boolPending;
			cReport.TaxYear = intYear;
			cReport.WhereClause = strWhereClause;
			cBETEApplicationController cController = new cBETEApplicationController();
			cController.LoadRange(ref cReport);
			rptNewBETEApplication.InstancePtr.Init(ref cReport);
		}

		private void txtMessage_KeyDown(short Index, object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Return:
					{
						KeyCode = (Keys)0;
						if (Index < 7)
						{
							txtMessage[FCConvert.ToInt16(Index + 1)].Focus();
						}
						break;
					}
				case Keys.Up:
					{
						KeyCode = (Keys)0;
						if (Index > 0)
						{
							txtMessage[FCConvert.ToInt16(Index - 1)].Focus();
							txtMessage[FCConvert.ToInt16(Index - 1)].SelectionStart = txtMessage[Index].SelectionStart;
						}
						break;
					}
				case Keys.Down:
					{
						KeyCode = (Keys)0;
						if (Index < 7)
						{
							txtMessage[FCConvert.ToInt16(Index + 1)].Focus();
							txtMessage[FCConvert.ToInt16(Index + 1)].SelectionStart = txtMessage[Index].SelectionStart;
						}
						break;
					}
				case Keys.Left:
					{
						if (txtMessage[Index].SelectionStart <= 1)
						{
							KeyCode = (Keys)0;
							if (Index > 0)
							{
								txtMessage[FCConvert.ToInt16(Index - 1)].Focus();
								txtMessage[FCConvert.ToInt16(Index - 1)].SelectionStart = txtMessage[FCConvert.ToInt16(Index - 1)].MaxLength;
							}
						}
						break;
					}
				case Keys.Right:
					{
						if (txtMessage[Index].SelectionStart == txtMessage[Index].MaxLength || txtMessage[Index].SelectionStart == txtMessage[Index].Text.Length)
						{
							KeyCode = (Keys)0;
							if (Index < 7)
							{
								txtMessage[FCConvert.ToInt16(Index + 1)].Focus();
								txtMessage[FCConvert.ToInt16(Index + 1)].SelectionStart = 0;
							}
						}
						break;
					}
				case Keys.Delete:
				case Keys.Back:
				case Keys.Insert:
				case Keys.Home:
				case Keys.End:
				case Keys.PageDown:
				case Keys.PageUp:
				case Keys.ShiftKey:
				case Keys.ControlKey:
				case Keys.Escape:
				case Keys.NumLock:
					{
						break;
					}
				default:
					{
						if (Index < 7)
						{
							if (txtMessage[Index].SelectionStart == txtMessage[Index].MaxLength)
							{
								txtMessage[FCConvert.ToInt16(Index + 1)].Focus();
								txtMessage[FCConvert.ToInt16(Index + 1)].SelectionStart = 0;
							}
						}
						break;
					}
			}
			//end switch
		}

		private void txtMessage_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			short index = txtMessage.GetIndex((FCTextBox)sender);
			txtMessage_KeyDown(index, sender, e);
		}
	}
}
