﻿//Fecher vbPorter - Version 1.0.0.32
namespace TWPP0000
{
	public class cBETEApplication
	{
		//=========================================================
		private string strOwner = string.Empty;
		private string strBusiness = string.Empty;
		private string strBusinessAddress = string.Empty;
		private string strTypeOfBusiness = string.Empty;
		private int lngAccount;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cBETEApplicationDetailList lstDetails = new cBETEApplicationDetailList();
		private cBETEApplicationDetailList lstDetails_AutoInitialized;

		private cBETEApplicationDetailList lstDetails
		{
			get
			{
				if (lstDetails_AutoInitialized == null)
				{
					lstDetails_AutoInitialized = new cBETEApplicationDetailList();
				}
				return lstDetails_AutoInitialized;
			}
			set
			{
				lstDetails_AutoInitialized = value;
			}
		}

		public string Owner
		{
			set
			{
				strOwner = value;
			}
			get
			{
				string Owner = "";
				Owner = strOwner;
				return Owner;
			}
		}

		public string Business
		{
			set
			{
				strBusiness = value;
			}
			get
			{
				string Business = "";
				Business = strBusiness;
				return Business;
			}
		}

		public string BusinessAddress
		{
			set
			{
				strBusinessAddress = value;
			}
			get
			{
				string BusinessAddress = "";
				BusinessAddress = strBusinessAddress;
				return BusinessAddress;
			}
		}

		public string TypeOfBusiness
		{
			set
			{
				strTypeOfBusiness = value;
			}
			get
			{
				string TypeOfBusiness = "";
				TypeOfBusiness = strTypeOfBusiness;
				return TypeOfBusiness;
			}
		}

		public int Account
		{
			set
			{
				lngAccount = value;
			}
			get
			{
				int Account = 0;
				Account = lngAccount;
				return Account;
			}
		}

		public cBETEApplicationDetailList Details
		{
			set
			{
				lstDetails = value;
			}
			get
			{
				cBETEApplicationDetailList Details = null;
				Details = lstDetails;
				return Details;
			}
		}
	}
}
