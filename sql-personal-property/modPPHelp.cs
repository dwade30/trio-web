﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWPP0000
{
	public class modPPHelp
	{
		private static void StartHelp_2(string strSQL1)
		{
			StartHelp(ref strSQL1);
		}

		private static void StartHelp(ref string strSQL1)
		{
			//! Load frmHelpCodes;
			Statics.rs.OpenRecordset(strSQL1, modPPGN.strPPDatabase);
		}

		public static void ExemptCodes()
		{
			StartHelp_2("SELECT * FROM ExemptCodes");
			// Fill Label
			Statics.strList = Strings.StrDup(42, " ");
			fecherFoundation.Strings.MidSet(ref Statics.strList, 1, 5, "CODES");
			fecherFoundation.Strings.MidSet(ref Statics.strList, 7, 22, "DESCRIPTION....");
			fecherFoundation.Strings.MidSet(ref Statics.strList, 30, 12, "DOLLAR VALUE");
			frmHelpCodes.InstancePtr.lblList.Text = Statics.strList;
			// Fill Listbox
			Statics.rs.MoveLast();
			Statics.rs.MoveFirst();
			do
			{
				Statics.strList = Strings.StrDup(42, " ");
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				fecherFoundation.Strings.MidSet(ref Statics.strList, 2, 2, Strings.Format(Statics.rs.Get_Fields("code"), "00"));
				fecherFoundation.Strings.MidSet(ref Statics.strList, 7, 22, Statics.rs.Get_Fields_String("description") + Strings.StrDup(22 - FCConvert.ToString(Statics.rs.Get_Fields_String("description")).Length, " "));
				// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
				fecherFoundation.Strings.MidSet(ref Statics.strList, 30, 12, Strings.StrDup(12 - Strings.Format(Statics.rs.Get_Fields("amount"), "#,##0").Length, " ") + Strings.Format(Statics.rs.Get_Fields_String("description"), "#,##0"));
				frmHelpCodes.InstancePtr.lstCodes.AddItem(Statics.strList);
				Statics.rs.MoveNext();
			}
			while (!Statics.rs.EndOfFile());
			//FC:FINAL:DDU:#i1645 - fix width of lstCodes
			frmHelpCodes.InstancePtr.lstCodes.Columns[0].Width = FCConvert.ToInt32(frmHelpCodes.InstancePtr.Width * 0.85);
		}

		public static void BusinessCodes()
		{
			StartHelp_2("SELECT * FROM BusinessCodes");
			// Fill Label
			Statics.strList = Strings.StrDup(42, " ");
			fecherFoundation.Strings.MidSet(ref Statics.strList, 1, 5, "CODES");
			fecherFoundation.Strings.MidSet(ref Statics.strList, 7, 22, "BUSINESS TYPE  ");
			frmHelpCodes.InstancePtr.lblList.Text = Statics.strList;
			// Fill Listbox
			if (!Statics.rs.EndOfFile())
			{
				Statics.rs.MoveLast();
				Statics.rs.MoveFirst();
				do
				{
					Statics.strList = Strings.StrDup(42, " ");
					fecherFoundation.Strings.MidSet(ref Statics.strList, 2, 3, Strings.Format(Statics.rs.Get_Fields_Int32("businesscode"), "000"));
					fecherFoundation.Strings.MidSet(ref Statics.strList, 7, 38, Statics.rs.Get_Fields_String("businesstype") + Strings.StrDup(38 - FCConvert.ToString(Statics.rs.Get_Fields_String("businesstype")).Length, " "));
					frmHelpCodes.InstancePtr.lstCodes.AddItem(Statics.strList);
					Statics.rs.MoveNext();
				}
				while (!Statics.rs.EndOfFile());
				//FC:FINAL:DDU:#i1645 - fix width of lstCodes
				frmHelpCodes.InstancePtr.lstCodes.Columns[0].Width = FCConvert.ToInt32(frmHelpCodes.InstancePtr.Width * 0.85);
			}
		}

		public static void StreetCodes()
		{
			StartHelp_2("SELECT * FROM StreetCodes");
			// Fill Label
			Statics.strList = Strings.StrDup(42, " ");
			fecherFoundation.Strings.MidSet(ref Statics.strList, 1, 5, "CODES");
			fecherFoundation.Strings.MidSet(ref Statics.strList, 7, 22, "STREET NAME    ");
			frmHelpCodes.InstancePtr.lblList.Text = Statics.strList;
			// Fill Listbox
			if (!Statics.rs.EndOfFile())
			{
				Statics.rs.MoveLast();
				Statics.rs.MoveFirst();
				do
				{
					Statics.strList = Strings.StrDup(42, " ");
					// TODO Get_Fields: Check the table for the column [streetcode] and replace with corresponding Get_Field method
					fecherFoundation.Strings.MidSet(ref Statics.strList, 2, 4, Strings.Format(Statics.rs.Get_Fields("streetcode"), "0000"));
					fecherFoundation.Strings.MidSet(ref Statics.strList, 7, 30, Statics.rs.Get_Fields_String("streetname") + Strings.StrDup(30 - FCConvert.ToString(Statics.rs.Get_Fields_String("streetname")).Length, " "));
					frmHelpCodes.InstancePtr.lstCodes.AddItem(Statics.strList);
					Statics.rs.MoveNext();
				}
				while (!Statics.rs.EndOfFile());
				//FC:FINAL:DDU:#i1645 - fix width of lstCodes
				frmHelpCodes.InstancePtr.lstCodes.Columns[0].Width = FCConvert.ToInt32(frmHelpCodes.InstancePtr.Width * 0.85);
			}
		}

		public static void HelpDescription()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			//! Load frmHelpCodes;
			////Application.DoEvents();
			clsLoad.OpenRecordset("select * from pphelpfiles where ppkey = 72", "twpp0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				frmHelpCodes.InstancePtr.txtNotes.Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("Description")));
			}
		}

		public static void HelpYear()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			//! Load frmHelpCodes;
			////Application.DoEvents();
			clsLoad.OpenRecordset("select * from pphelpfiles where ppkey = 103", "twpp0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				frmHelpCodes.InstancePtr.txtNotes.Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description")));
			}
		}

		public static void HelpLeasedReplaceCost()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			//! Load frmHelpCodes;
			////Application.DoEvents();
			clsLoad.OpenRecordset("select * from pphelpfiles where ppkey = 106", "TWPP0000.VB1");
			if (!clsLoad.EndOfFile())
			{
				frmHelpCodes.InstancePtr.txtNotes.Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description")));
			}
		}

		public static void HelpDepreciationYear()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			//! Load frmHelpCodes;
			////Application.DoEvents();
			clsLoad.OpenRecordset("select * from pphelpfiles where ppkey = 74", "twpp0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				frmHelpCodes.InstancePtr.txtNotes.Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description")));
			}
		}

		public static void HelpSRO()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			//! Load frmHelpCodes;
			////Application.DoEvents();
			clsLoad.OpenRecordset("select * from pphelpfiles where ppkey = 104", "twpp0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				frmHelpCodes.InstancePtr.txtNotes.Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description")));
			}
		}

		public static void HelpRCYear()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			//! Load frmHelpCodes;
			////Application.DoEvents();
			clsLoad.OpenRecordset("select * from pphelpfiles where ppkey = 105", "twpp0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				frmHelpCodes.InstancePtr.txtNotes.Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description")));
			}
		}

		public static void HelpReplaceCost()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			//! Load frmHelpCodes;
			////Application.DoEvents();
			clsLoad.OpenRecordset("select * from pphelpfiles where ppkey = 77", "twpp0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				frmHelpCodes.InstancePtr.txtNotes.Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description")));
			}
		}

		public static void HelpPercGood()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			//! Load frmHelpCodes;
			////Application.DoEvents();
			clsLoad.OpenRecordset("select * from pphelpfiles where ppkey = 78", "twpp0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				frmHelpCodes.InstancePtr.txtNotes.Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description")));
			}
		}

		public static void HelpFactor()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			//! Load frmHelpCodes;
			////Application.DoEvents();
			clsLoad.OpenRecordset("select * from pphelpfiles where ppkey = 79", "twpp0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				frmHelpCodes.InstancePtr.txtNotes.Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description")));
			}
		}

		public static void HelpLeaseDate()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			//! Load frmHelpCodes;
			////Application.DoEvents();
			clsLoad.OpenRecordset("select * from pphelpfiles where ppkey = 44", "twpp0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				frmHelpCodes.InstancePtr.txtNotes.Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description")));
			}
		}

		public static void HelpLeaseMonths()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			//! Load frmHelpCodes;
			////Application.DoEvents();
			clsLoad.OpenRecordset("select * from pphelpfiles where ppkey = 46", "twpp0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				frmHelpCodes.InstancePtr.txtNotes.Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description")));
			}
		}

		public static void HelpLP()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			//! Load frmHelpCodes;
			////Application.DoEvents();
			clsLoad.OpenRecordset("select * from pphelpfiles where ppkey = 47", "twpp0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				frmHelpCodes.InstancePtr.txtNotes.Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description")));
			}
		}

		public static void HelpMonthlyRent()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			//! Load frmHelpCodes;
			////Application.DoEvents();
			clsLoad.OpenRecordset("select * from pphelpfiles where ppkey = 48", "twpp0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				frmHelpCodes.InstancePtr.txtNotes.Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description")));
			}
		}

		public static void HelpCatNumbers()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			//! Load frmHelpCodes;
			////Application.DoEvents();
			Statics.strList = Strings.StrDup(42, " ");
			fecherFoundation.Strings.MidSet(ref Statics.strList, 1, 8, "Category");
			fecherFoundation.Strings.MidSet(ref Statics.strList, 10, 11, "Description");
			frmHelpCodes.InstancePtr.lblList.Text = Statics.strList;
			clsLoad.OpenRecordset("select * from RATIOTRENDS order by type", "twpp0000.vb1");
			while (!clsLoad.EndOfFile())
			{
				Statics.strList = Strings.StrDup(42, " ");
				// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
				fecherFoundation.Strings.MidSet(ref Statics.strList, 4, 1, FCConvert.ToString(clsLoad.Get_Fields("type")));
				fecherFoundation.Strings.MidSet(ref Statics.strList, 10, FCConvert.ToString(clsLoad.Get_Fields_String("Description")));
				frmHelpCodes.InstancePtr.lstCodes.AddItem(Statics.strList);
				clsLoad.MoveNext();
			}
			//FC:FINAL:DDU:#i1645 - fix width of lstCodes
			frmHelpCodes.InstancePtr.lstCodes.Columns[0].Width = FCConvert.ToInt32(frmHelpCodes.InstancePtr.Width * 0.85);
			clsLoad.OpenRecordset("select * from pphelpfiles where ppkey = 107", "twpp0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				frmHelpCodes.InstancePtr.txtNotes.Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("Description")));
			}
		}

		public class StaticVariables
		{
			//=========================================================
			//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
			//public clsDRWrapper rs = new clsDRWrapper();
			public clsDRWrapper rs_AutoInitialized;

			public clsDRWrapper rs
			{
				get
				{
					if (rs_AutoInitialized == null)
					{
						rs_AutoInitialized = new clsDRWrapper();
					}
					return rs_AutoInitialized;
				}
				set
				{
					rs_AutoInitialized = value;
				}
			}

			//public string strSQL = "";
			// vbPorter upgrade warning: strList As FixedString	OnWrite(string)
			public string strList = "";
			public int intHelpResponse;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
