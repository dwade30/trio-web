﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for srptItemizedReim.
	/// </summary>
	public partial class srptItemizedReim : FCSectionReport
	{
		public srptItemizedReim()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ActiveReport1";
		}

		public static srptItemizedReim InstancePtr
		{
			get
			{
				return (srptItemizedReim)Sys.GetInstance(typeof(srptItemizedReim));
			}
		}

		protected srptItemizedReim _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsIt?.Dispose();
                clsIt = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptItemizedReim	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsIt = new clsDRWrapper();
		float PercentGood;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsIt.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			clsIt.OpenRecordset("Select * from PPItemized where Account = " + this.UserData + " and RB = '*' order by Line", "twpp0000.vb1");
			if (clsIt.EndOfFile())
			{
				//this.Stop();
				this.Close();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (clsIt.EndOfFile())
					return;
				txtLine.Text = clsIt.GetData("line").ToString();
				txtCD.Text = clsIt.GetData("CD").ToString();
				if (Strings.Trim(clsIt.GetData("cd") + "") != "-")
				{
					if (rptReimbursement2.InstancePtr.boolPrintQTY)
					{
						txtQTY.Text = FCConvert.ToString(Conversion.Val(clsIt.GetData("quantity")));
					}
					else
					{
						txtQTY.Text = "__";
					}
					txtDesc.Text = Strings.Trim(FCConvert.ToString(clsIt.GetData("description")));
					// TODO Get_Fields: Check the table for the column [sro] and replace with corresponding Get_Field method
					if (Strings.UCase(FCConvert.ToString(clsIt.Get_Fields("sro"))) == "S")
					{
						txtMO.Text = "__";
						txtYR.Text = "____";
						if (Conversion.Val(clsIt.GetData("month")) == 0)
						{
							txtMO.Text = "__";
						}
						else
						{
							txtMO.Text = clsIt.GetData("month").ToString();
						}
						txtYR.Text = clsIt.GetData("year").ToString();
					}
					else
					{
						if (Conversion.Val(clsIt.GetData("month")) == 0)
						{
							txtMO.Text = "__";
						}
						else
						{
							txtMO.Text = clsIt.GetData("month").ToString();
						}
						txtYR.Text = clsIt.GetData("year").ToString();
					}
					if (FCConvert.ToString(clsIt.GetData("sro")) == "O")
					{
						// TODO Get_Fields: Check the table for the column [cost] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [quantity] and replace with corresponding Get_Field method
						txtOriginal.Text = Strings.Format(Conversion.Val(clsIt.Get_Fields("cost")) * Conversion.Val(clsIt.Get_Fields("quantity")), "###,###,###,##0");
					}
					else
					{
						txtOriginal.Text = "unknown";
					}
					txtAssess.Text = Strings.Format(Conversion.Val(clsIt.GetData("value")), "###,###,###,##0");
				}
				else
				{
					txtQTY.Text = "";
					txtMO.Text = "";
					txtYR.Text = "";
					txtOriginal.Text = "";
					txtAssess.Text = "";
					txtDesc.Text = Strings.Trim(FCConvert.ToString(clsIt.Get_Fields_String("description")));
				}
				clsIt.MoveNext();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Detail_format of sub report Itemized Reimbursement", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			double dblRatio;
			int x;
			clsDRWrapper clsItemized = new clsDRWrapper();
			int lngItOriginal;
			int lngItAssessment;
			int lngOrigCat;
			// vbPorter upgrade warning: lngCat As int	OnWrite(int, double)
			int lngCat;
			int intTemp = 0;
            try
            {
                // On Error GoTo ErrorHandler
                clsItemized.OpenRecordset("SELECT * FROM PPRatioOpens", "twpp0000.vb1");
                // TODO Get_Fields: Check the table for the column [Ratio] and replace with corresponding Get_Field method
                dblRatio = Conversion.Val(clsItemized.Get_Fields("Ratio")) / 100;
                // TODO Get_Fields: Check the table for the column [ratio] and replace with corresponding Get_Field method
                Label13.Text = "Itemized Total (With " +
                               FCConvert.ToString(Conversion.Val(clsItemized.Get_Fields("ratio"))) +
                               "% ratio applied to assessment and rounded by Category)";
                lngItOriginal = 0;
                lngItAssessment = 0;
                lngOrigCat = 0;
                lngCat = 0;
                clsItemized.OpenRecordset(
                    "select cd,sum(cost * quantity) as thecost,sum(value) as theValue from ppitemized where account = " +
                    this.UserData + " and rb = '*' group by cd", "twpp0000.vb1");
                while (!clsItemized.EndOfFile())
                {
                    intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(clsItemized.Get_Fields_String("cd"))));
                    if (intTemp < 10 && intTemp > 0)
                    {
                        // TODO Get_Fields: Field [thecost] not found!! (maybe it is an alias?)
                        lngOrigCat = FCConvert.ToInt32(Math.Round(Conversion.Val(clsItemized.Get_Fields("thecost"))));
                        // TODO Get_Fields: Field [thevalue] not found!! (maybe it is an alias?)
                        lngCat = FCConvert.ToInt32(Math.Round(Conversion.Val(clsItemized.Get_Fields("thevalue"))));
                        // lngOrigCat = Round(CDbl(lngOrigCat), PPRounding)
                        lngCat = FCConvert.ToInt32(modGlobal.Round(FCConvert.ToDouble(lngCat) * dblRatio,
                            modPPCalculate.Statics.PPRounding));
                        lngItOriginal += lngOrigCat;
                        lngItAssessment += lngCat;
                    }

                    clsItemized.MoveNext();
                }

                // For x = 1 To 9
                // Call GetReimItemizedForCat(lngOrigCat, lngCat, Me.Tag, x)
                // Call clsItemized.OpenRecordset("select sum(cost * quantity) as thecost,sum(value) as theValue from ppitemized where account = " & Me.Tag & " and rb = '*' and cd = '" & X & "'", "twpp0000.vb1")
                // 
                // If Not clsItemized.EndOfFile Then
                // lngOrigCat = Val(clsItemized.Fields("thecost"))
                // lngCat = Val(clsItemized.Fields("thevalue"))
                // End If
                // 
                // lngOrigCat = Round(CDbl(lngOrigCat), PPRounding)
                // lngCat = Round(CDbl(lngCat) * dblRatio, PPRounding)
                // 
                // 
                // lngItOriginal = lngItOriginal + lngOrigCat
                // lngItAssessment = lngItAssessment + lngCat
                // Next x
                txtOriginalTotal.Text = Strings.Format(lngItOriginal, "###,###,###,##0");
                txtAssessmentTotal.Text = Strings.Format(lngItAssessment, "###,###,###,##0");
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show(
                    "Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " +
                    Information.Err(ex).Description + "\r\n" + "In ReportFooter_Format", "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Hand);
            }
            finally
            {
				clsItemized.Dispose();
            }
		}

		
	}
}
