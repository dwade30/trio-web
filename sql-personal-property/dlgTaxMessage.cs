﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using Wisej.Web;
using Wisej.Core;
using System.IO;

namespace TWPP0000
{
	/// <summary>
	/// Summary description for dlgTaxMessage.
	/// </summary>
	public partial class dlgTaxMessage : BaseForm
	{
		public dlgTaxMessage()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.txtNotice = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.txtNotice.AddControlArrayElement(txtNotice_14, 14);
			this.txtNotice.AddControlArrayElement(txtNotice_13, 13);
			this.txtNotice.AddControlArrayElement(txtNotice_12, 12);
			this.txtNotice.AddControlArrayElement(txtNotice_11, 11);
			this.txtNotice.AddControlArrayElement(txtNotice_10, 10);
			this.txtNotice.AddControlArrayElement(txtNotice_9, 9);
			this.txtNotice.AddControlArrayElement(txtNotice_8, 8);
			this.txtNotice.AddControlArrayElement(txtNotice_7, 7);
			this.txtNotice.AddControlArrayElement(txtNotice_6, 6);
			this.txtNotice.AddControlArrayElement(txtNotice_5, 5);
			this.txtNotice.AddControlArrayElement(txtNotice_4, 4);
			this.txtNotice.AddControlArrayElement(txtNotice_3, 3);
			this.txtNotice.AddControlArrayElement(txtNotice_2, 2);
			this.txtNotice.AddControlArrayElement(txtNotice_1, 1);
			this.txtNotice.AddControlArrayElement(txtNotice_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static dlgTaxMessage InstancePtr
		{
			get
			{
				return (dlgTaxMessage)Sys.GetInstance(typeof(dlgTaxMessage));
			}
		}

		protected dlgTaxMessage _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int intMaxTextBox;
		bool boolIncTax;
		string strYear;
		bool boolFreeForm;
		bool boolIncExempt;
		bool boolIncTotExempts;
		bool boolUseExisting;
		string strRate;
		int intWhichAccounts;
		string strWhatOrder;
		// vbPorter upgrade warning: intARorS As short	OnWriteFCConvert.ToInt32(
		public void Init(bool boolUseFreeForm, bool boolShowEstimatedTax, bool boolShowExempt, bool boolUseExistingNotice, bool boolIncludeTotallyExempts, string strTaxYear, string strTaxRate, int intARorS, string strOrder)
		{
			// vbPorter upgrade warning: x As Variant, short --> As int
			int x = 0;
			StreamReader txtStream = null;
			boolFreeForm = boolUseFreeForm;
			boolIncTax = boolShowEstimatedTax;
			strYear = strTaxYear;
			boolIncExempt = boolShowExempt;
			boolIncTotExempts = boolIncludeTotallyExempts;
			boolUseExisting = boolUseExistingNotice;
			strRate = strTaxRate;
			intWhichAccounts = intARorS;
			strWhatOrder = strOrder;
			try
			{
				if (File.Exists(FCFileSystem.CurDir() + "\\PPNotice.txt") && boolUseExisting)
				{
					txtStream = File.OpenText(FCFileSystem.CurDir() + "\\PPNotice.txt");
					txtHeading.Text = txtStream.ReadLine();
					x = 0;
					while (!txtStream.EndOfStream)
					{
						txtNotice[FCConvert.ToInt16(x)].Text = txtStream.ReadLine();
						x += 1;
					}
				}
				if (boolFreeForm)
				{
					intMaxTextBox = 14;
				}
				else
				{
					intMaxTextBox = 6;
					for (x = 7; x <= 14; x++)
					{
						txtNotice[FCConvert.ToInt16(x)].Enabled = false;
					}
					// x
					txtNotice[7].Text = "";
					txtNotice[10].Text = "";
					txtNotice[13].Text = "";
					txtNotice[14].Text = "";
					txtNotice[7].Text = "Category 1                   0,000";
					txtNotice[8].Text = "Category 2                   0,000";
					txtNotice[9].Text = "Category 3                   0,000";
					txtNotice[10].Text = "Category 4                   0,000";
					txtNotice[11].Text = "Category 5                   0,000";
					txtNotice[12].Text = "Category 6                   0,000";
					txtNotice[13].Text = "Category 7                   0,000";
					txtNotice[14].Text = "Category 8                   0,000";
					txtCategory9.Visible = true;
					txtCategory9.Text = "Category 9                   0,000";
					txtTotal.Visible = true;
					txtTotalValue.Visible = true;
					// If boolIncExempt Then txtNotice(8).Text = txtNotice(8).Text & "     Exemption"
					// If boolIncExempt Then txtNotice(9).Text = txtNotice(9).Text & " 0,000,000,000"
					if (boolIncTax)
					{
						txtEstimated.Visible = true;
						txtEstimated.Text = "Estimated " + strYear + " Tax:";
						txtEstimatedTotal.Text = "0,000";
						txtEstimatedTotal.Visible = true;
					}
				}
				this.Show();
			}
			finally
			{
				if (txtStream != null)
				{
					txtStream.Close();
				}
			}
		}

		private void dlgTaxMessage_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuCancel_Click();
						break;
					}
			}
			//end switch
		}

		private void dlgTaxMessage_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//dlgTaxMessage properties;
			//dlgTaxMessage.ScaleWidth	= 9075;
			//dlgTaxMessage.ScaleHeight	= 7110;
			//dlgTaxMessage.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
		}

		private void mnuCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuCancel_Click()
		{
			mnuCancel_Click(mnuCancel, new System.EventArgs());
		}

		private void mnuClear_Click(object sender, System.EventArgs e)
		{
			int x;
			for (x = 0; x <= intMaxTextBox; x++)
			{
				txtNotice[FCConvert.ToInt16(x)].Text = "";
			}
			// x
		}

		private void mnuDone_Click(object sender, System.EventArgs e)
		{
			StreamWriter tsTaxNotice;
			int x;
			tsTaxNotice = File.CreateText(FCFileSystem.CurDir() + "\\PPNotice.txt");
			tsTaxNotice.WriteLine(txtHeading.Text);
			for (x = 0; x <= intMaxTextBox; x++)
			{
				tsTaxNotice.WriteLine(txtNotice[FCConvert.ToInt16(x)].Text);
			}
			// x
			tsTaxNotice.Close();
			rptTaxNotice.InstancePtr.Start(boolFreeForm, boolIncTax, boolIncExempt, boolIncTotExempts, intWhichAccounts, strWhatOrder, strYear, strRate);
		}

		public void mnuDone_Click()
		{
			mnuDone_Click(mnuDone, new System.EventArgs());
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			mnuDone_Click();
			mnuCancel_Click();
		}

		private void txtHeading_Enter(object sender, System.EventArgs e)
		{
			txtHeading.SelectionStart = 0;
			txtHeading.SelectionLength = 0;
		}

		private void txtHeading_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (txtHeading.Text.Length >= (txtHeading.MaxLength - 1))
			{
				txtNotice[0].Focus();
			}
		}

		private void txtHeading_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				txtNotice[0].Focus();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtNotice_Enter(short Index, object sender, System.EventArgs e)
		{
			txtNotice[Index].SelectionStart = 0;
			txtNotice[Index].SelectionLength = 0;
		}

		private void txtNotice_Enter(object sender, System.EventArgs e)
		{
			short index = txtNotice.GetIndex((FCTextBox)sender);
			txtNotice_Enter(index, sender, e);
		}

		private void txtNotice_KeyDown(short Index, object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (txtNotice[Index].Text.Length >= (txtNotice[Index].MaxLength - 1))
			{
				if (Index < intMaxTextBox)
				{
					txtNotice[FCConvert.ToInt16(Index + 1)].Focus();
				}
				else
				{
					txtNotice[0].Focus();
				}
			}
		}

		private void txtNotice_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			short index = txtNotice.GetIndex((FCTextBox)sender);
			txtNotice_KeyDown(index, sender, e);
		}

		private void txtNotice_KeyPress(short Index, object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				// return key
				if (Index < intMaxTextBox)
				{
					KeyAscii = (Keys)0;
					txtNotice[FCConvert.ToInt16(Index + 1)].Focus();
				}
				else
				{
					KeyAscii = (Keys)0;
					txtNotice[0].Focus();
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtNotice_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			short index = txtNotice.GetIndex((FCTextBox)sender);
			txtNotice_KeyPress(index, sender, e);
		}
	}
}
