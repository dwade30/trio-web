﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmLienPartialPayment.
	/// </summary>
	public partial class frmLienPartialPayment : BaseForm
	{
		public frmLienPartialPayment()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmLienPartialPayment InstancePtr
		{
			get
			{
				return (frmLienPartialPayment)Sys.GetInstance(typeof(frmLienPartialPayment));
			}
		}

		protected frmLienPartialPayment _InstancePtr = null;

		clsDRWrapper rsLien = new clsDRWrapper();
		clsDRWrapper rsSettings = new clsDRWrapper();
		bool boolLoaded;
		int lngLienRecordNumber;
		string strOwner = "";
		double dblPaymentAmount;
		string strSignerName;
		string strMapLot = "";
		int lngAccountNumber;
		DateTime dtPaymentDate;
		bool boolShowSignDate;
		// grid cols
		int lngColData;
		int lngColTitle;
		int lngColHidden;
		// grid rows
		int lngRowMuni;
		int lngRowState;
		int lngRowOwner1;
		int lngRowPayDate;
		int lngRowPaymentAmount;
		int lngRowSignerName;
		int lngRowMapLot;
		int lngRowSignedDate;

		public void Init(DateTime dtPassPayDate, int lngPassLRN, string strYear, int lngPassAccountNumber, double dblPassPaymentAmount, string strWitness = "", bool boolUseSignDate = false)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsInfo = new clsDRWrapper();
				bool boolRecords/*unused?*/;
				boolShowSignDate = boolUseSignDate;
				dtPaymentDate = dtPassPayDate;
				lngLienRecordNumber = lngPassLRN;
				strSignerName = strWitness;
				lngColData = 2;
				lngColHidden = 1;
				lngColTitle = 0;
				lngRowMuni = 0;
				lngRowState = 1;
				lngRowOwner1 = 2;
				lngRowPayDate = 3;
				lngRowPaymentAmount = 4;
				lngRowSignerName = 5;
				lngRowMapLot = 7;
				lngRowSignedDate = 6;
				rsInfo.OpenRecordset("SELECT * FROM BillingMaster WHERE LienRecordNumber = " + FCConvert.ToString(lngLienRecordNumber), modExtraModules.strCLDatabase);
				if (!rsInfo.EndOfFile())
				{
					strOwner = FCConvert.ToString(rsInfo.Get_Fields_String("Name1")) + " " + FCConvert.ToString(rsInfo.Get_Fields_String("Name2"));
					// strSignerName = "" 'rsInfo.Get_Fields("Name1")
					strMapLot = FCConvert.ToString(rsInfo.Get_Fields_String("MapLot"));
				}
				dblPaymentAmount = dblPassPaymentAmount;
				lngAccountNumber = lngPassAccountNumber;
				if (strYear.Length > 4)
				{
					strYear = Strings.Left(strYear, 4);
				}
				if (strYear != "")
				{
					lblYear.Text = "Year : " + strYear;
					lblYear.Visible = true;
				}
				else
				{
					lblYear.Visible = false;
				}
				rsInfo.OpenRecordset("SELECT * FROM LienRec INNER JOIN BillingMaster ON LienRec.ID = BillingMaster.LienRecordNumber WHERE LienRec.ID = " + FCConvert.ToString(lngLienRecordNumber), modExtraModules.strCLDatabase);
				if (!rsInfo.EndOfFile())
				{
					// keep going
				}
				else
				{
					FCMessageBox.Show("The lien record number " + FCConvert.ToString(lngLienRecordNumber) + " could not be found.", MsgBoxStyle.Exclamation, "Missing Lien Record");
					Close();
				}
				FormatGrid(true);
				LoadSettings();
				if (lngLienRecordNumber > 0)
				{
					this.Show(FormShowEnum.Modal);
				}
				else
				{
					FCMessageBox.Show("Cannot find a lien record for that year.", MsgBoxStyle.Exclamation, "Cannot Find Lien");
					Close();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Initializing");
			}
		}

		private void frmLienPartialPayment_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				boolLoaded = true;
			}
		}

		private void frmLienPartialPayment_Load(object sender, System.EventArgs e)
		{			
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			boolLoaded = true;
		}

		private void frmLienPartialPayment_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				Close();
			}
			else if (KeyAscii == 13)
			{
				KeyAscii = 0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		//private void mnuFileExit_Click(object sender, System.EventArgs e)
		//{
		//	Close();
		//}

		private void SaveSettings()
		{
			// this will save all of the settings for the lien discharge notice
			clsDRWrapper rsTemp = new clsDRWrapper();
			DateTime dtComExp;
			bool boolTemp/*unused?*/;
			// VBto upgrade warning: obRep As object --> As arLienPartialPayment
			arLienPartialPayment obRep = null;
			if (ValidateAnswers())
			{
				rsTemp.Execute("update partialpaymentwaiver set printed = 1 where lienkey = " + FCConvert.ToString(lngLienRecordNumber), modExtraModules.strCLDatabase);
				obRep = new arLienPartialPayment();
				if (!boolShowSignDate)
				{
					obRep.Init(vsData.TextMatrix(lngRowMuni, lngColData), vsData.TextMatrix(lngRowState, lngColData), FCConvert.ToDouble(vsData.TextMatrix(lngRowPaymentAmount, lngColData)), vsData.TextMatrix(lngRowOwner1, lngColData), Strings.Trim(Strings.Right(lblYear.Text, 6)), DateAndTime.DateValue(vsData.TextMatrix(lngRowPayDate, lngColData)), vsData.TextMatrix(lngRowSignerName, lngColData), vsData.TextMatrix(lngRowMapLot, lngColData), lngAccountNumber);
				}
				else
				{
					obRep.Init(vsData.TextMatrix(lngRowMuni, lngColData), vsData.TextMatrix(lngRowState, lngColData), FCConvert.ToDouble(vsData.TextMatrix(lngRowPaymentAmount, lngColData)), vsData.TextMatrix(lngRowOwner1, lngColData), Strings.Trim(Strings.Right(lblYear.Text, 6)), DateTime.Now, vsData.TextMatrix(lngRowSignerName, lngColData), vsData.TextMatrix(lngRowMapLot, lngColData), lngAccountNumber);
				}
				frmReportViewer.InstancePtr.Init(obRep, "", 1, this.Modal);
				frmLienPartialPayment.InstancePtr.Close();
			}
		}

		private void LoadSettings()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsCTRL = new clsDRWrapper();
				// Muni Name
				vsData.TextMatrix(lngRowMuni, lngColData, modGlobalConstants.Statics.MuniName);
				// State
				vsData.TextMatrix(lngRowState, lngColData, "Maine");
				// Name1
				vsData.TextMatrix(lngRowOwner1, lngColData, strOwner);
				// PayDate
				vsData.TextMatrix(lngRowPayDate, lngColData, Strings.Format(dtPaymentDate, "MM/dd/yyyy"));
				// Payment Amount
				vsData.TextMatrix(lngRowPaymentAmount, lngColData, Strings.Format(dblPaymentAmount, "#,##0.00"));
				// Signer Name
				vsData.TextMatrix(lngRowSignerName, lngColData, strSignerName);
				// Signer Name
				vsData.TextMatrix(lngRowSignedDate, lngColData, Strings.Format(DateTime.Today, "MM/dd/yyyy"));
				// Map Lot
				if (strMapLot != "")
				{
					vsData.TextMatrix(lngRowMapLot, lngColData, strMapLot);
				}
				else
				{
					// if the bill record does not have the map lot, then get it from the RE Master file
					rsCTRL.OpenRecordset("SELECT * FROM Master WHERE RSCard = 1 AND RSAccount = " + FCConvert.ToString(lngAccountNumber), modExtraModules.strREDatabase);
					if (!rsCTRL.EndOfFile())
					{
						vsData.TextMatrix(lngRowMapLot, lngColData, FCConvert.ToString(rsCTRL.Get_Fields_String("RSMapLot")));
					}
					else
					{
						vsData.TextMatrix(lngRowMapLot, lngColData, "");
					}
				}
                //FC:FINAL:PB: - issue #2029 fixed alignment
                vsData.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Loading Settings");
			}
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			SaveSettings();
			// this will keep the data and print the lien discharge notice
		}

		private void FormatGrid(bool boolReset = false)
		{
			int lngWid = 0;
			lngWid = vsData.WidthOriginal;
			if (boolReset)
			{
				vsData.Rows = 0;
				vsData.Rows = 8;
			}
			vsData.ColWidth(lngColTitle, FCConvert.ToInt32(lngWid * 0.37));
			// title
			//FC:FINAL:AM: hide the column
			//vsData.ColWidth(lngColHidden, 0);
			vsData.ColHidden(lngColHidden, true);
			// hidden row
			vsData.ColWidth(lngColData, FCConvert.ToInt32(lngWid * 0.61));
			// data
			vsData.TextMatrix(lngRowMuni, lngColTitle, "Municipality");
			vsData.TextMatrix(lngRowState, lngColTitle, "State");
			vsData.TextMatrix(lngRowOwner1, lngColTitle, "Owner's Name");
			vsData.TextMatrix(lngRowPayDate, lngColTitle, "Payment Date");
			vsData.TextMatrix(lngRowPaymentAmount, lngColTitle, "Payment Amount");
			vsData.TextMatrix(lngRowSignerName, lngColTitle, "Witness' Name");
			vsData.TextMatrix(lngRowSignedDate, lngColTitle, "Signed Date");
			vsData.TextMatrix(lngRowMapLot, lngColTitle, "Map Lot");
			// set the grid height
			//if ((vsData.Rows * vsData.RowHeight(0)) + 80 > (fraVariables.Height * 0.9))
			//{
			//	vsData.Height = FCConvert.ToInt32((fraVariables.Height * 0.9));
			//	vsData.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			//}
			//else
			//{
			//	vsData.Height = (vsData.Rows * vsData.RowHeight(0)) + 80;
			//	vsData.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			//}
		}

		private void vsData_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			vsData.EditMask = string.Empty;
			vsData.ComboList = "";
			if (vsData.Row == lngRowPayDate || vsData.Row == lngRowSignedDate)
			{
				vsData.EditMask = "0#/0#/####";
			}
			else if (vsData.Row == lngRowPaymentAmount)
			{
				vsData.EditMask = "#,##0.00";
			}
		}

		private void vsData_KeyPressEdit(int Row, int Col, ref Keys KeyAscii)
		{
			if (Row == lngRowPayDate || Row == lngRowSignedDate || Row == lngRowPaymentAmount)
			{
				// only allow numbers
				if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back) || (KeyAscii == Keys.Decimal) || (FCConvert.ToInt32(KeyAscii) == 46))
				{
					// do nothing
				}
				else
				{
					KeyAscii = 0;
				}
			}
			else
			{
			}
		}

		private void vsData_RowColChange(object sender, EventArgs e)
		{
			if (vsData.Col == lngColData)
			{
				if (vsData.Row == lngRowMuni || vsData.Row == lngRowState || vsData.Row == lngRowOwner1 || vsData.Row == lngRowPayDate || vsData.Row == lngRowPaymentAmount || vsData.Row == lngRowSignerName || vsData.Row == lngRowMapLot || vsData.Row == lngRowSignedDate)
				{
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
			}
			else
			{
				vsData.Editable = FCGrid.EditableSettings.flexEDNone;
				vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private bool ValidateAnswers()
		{
			bool ValidateAnswers = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				ValidateAnswers = true;
				vsData.Select(0, 1);
				// Muni Name
				if (Strings.Trim(vsData.TextMatrix(lngRowMuni, lngColData)) == "")
				{
					ValidateAnswers = false;
					FCMessageBox.Show("Please enter the Municipality name.", MsgBoxStyle.Critical, "Validation");
					vsData.Select(lngRowMuni, lngColData);
					return ValidateAnswers;
				}
				// State
				if (Strings.Trim(vsData.TextMatrix(lngRowState, lngColData)) == "")
				{
					ValidateAnswers = false;
					FCMessageBox.Show("Please enter the State name.", MsgBoxStyle.Exclamation, "Validation");
					vsData.Select(lngRowState, lngColData);
					return ValidateAnswers;
				}
				// Owner
				if (Strings.Trim(vsData.TextMatrix(lngRowOwner1, lngColData)) == "")
				{
					ValidateAnswers = false;
					FCMessageBox.Show("Please enter the Owner's name.", MsgBoxStyle.Exclamation, "Validation");
					vsData.Select(lngRowOwner1, lngColData);
					return ValidateAnswers;
				}
				// Signer Name
				if (Strings.Trim(vsData.TextMatrix(lngRowSignerName, lngColData)) == "")
				{
					ValidateAnswers = false;
					FCMessageBox.Show("Please enter the Witness' name.", MsgBoxStyle.Exclamation, "Validation");
					vsData.Select(lngRowSignerName, lngColData);
					return ValidateAnswers;
				}
				// Payment Date
				if (Strings.Trim(vsData.TextMatrix(lngRowPayDate, lngColData)) == "")
				{
					ValidateAnswers = false;
					FCMessageBox.Show("Please enter a payment date.", MsgBoxStyle.Exclamation, "Validation");
					vsData.Select(lngRowPayDate, lngColData);
					return ValidateAnswers;
				}
				else
				{
					if (!Information.IsDate(vsData.TextMatrix(lngRowPayDate, lngColData)))
					{
						ValidateAnswers = false;
						FCMessageBox.Show("Please enter a valid payment date.", MsgBoxStyle.Exclamation, "Validation");
						vsData.Select(lngRowPayDate, lngColData);
						return ValidateAnswers;
					}
				}
				// Signing Date
				if (Strings.Trim(vsData.TextMatrix(lngRowSignedDate, lngColData)) == "")
				{
					ValidateAnswers = false;
					FCMessageBox.Show("Please enter a signing date.", MsgBoxStyle.Exclamation, "Validation");
					vsData.Select(lngRowSignedDate, lngColData);
					return ValidateAnswers;
				}
				else
				{
					if (!Information.IsDate(vsData.TextMatrix(lngRowSignedDate, lngColData)))
					{
						ValidateAnswers = false;
						FCMessageBox.Show("Please enter a valid signing date.", MsgBoxStyle.Exclamation, "Validation");
						vsData.Select(lngRowSignedDate, lngColData);
						return ValidateAnswers;
					}
				}
				// Payment Amount
				if (Strings.Trim(vsData.TextMatrix(lngRowPaymentAmount, lngColData)) == "")
				{
					ValidateAnswers = false;
					FCMessageBox.Show("Please enter a payment amount.", MsgBoxStyle.Exclamation, "Validation");
					vsData.Select(lngRowPaymentAmount, lngColData);
					return ValidateAnswers;
				}
				return ValidateAnswers;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				ValidateAnswers = false;
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Validating");
			}
			return ValidateAnswers;
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFileSaveExit_Click(sender, e);
		}
	}
}
