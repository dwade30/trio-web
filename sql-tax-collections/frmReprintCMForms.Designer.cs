﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmReprintCMForm.
	/// </summary>
	partial class frmReprintCMForm : BaseForm
	{
		public fecherFoundation.FCFrame fraForm;
		public fecherFoundation.FCComboBox cmbForm;
		public fecherFoundation.FCLabel lblType;
		public fecherFoundation.FCLabel lblInstructions;
		public fecherFoundation.FCGrid vsPayments;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReprintCMForm));
            this.fraForm = new fecherFoundation.FCFrame();
            this.cmbForm = new fecherFoundation.FCComboBox();
            this.lblType = new fecherFoundation.FCLabel();
            this.lblInstructions = new fecherFoundation.FCLabel();
            this.lblPayments = new fecherFoundation.FCLabel();
            this.vsPayments = new fecherFoundation.FCGrid();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.btnProcess = new fecherFoundation.FCButton();
            this.cmdFileSelectAll = new fecherFoundation.FCButton();
            this.cmdFileSelectToBottom = new fecherFoundation.FCButton();
            this.cmdFileClearAll = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraForm)).BeginInit();
            this.fraForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsPayments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileSelectAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileSelectToBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileClearAll)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 493);
            this.BottomPanel.Size = new System.Drawing.Size(798, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.lblPayments);
            this.ClientArea.Controls.Add(this.fraForm);
            this.ClientArea.Controls.Add(this.vsPayments);
            this.ClientArea.Size = new System.Drawing.Size(798, 433);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdFileClearAll);
            this.TopPanel.Controls.Add(this.cmdFileSelectToBottom);
            this.TopPanel.Controls.Add(this.cmdFileSelectAll);
            this.TopPanel.Size = new System.Drawing.Size(798, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileSelectAll, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileSelectToBottom, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileClearAll, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(307, 30);
            this.HeaderText.Text = "Reprint Certified Mail Form";
            // 
            // fraForm
            // 
            this.fraForm.AppearanceKey = "groupBoxNoBorders";
            this.fraForm.Controls.Add(this.cmbForm);
            this.fraForm.Controls.Add(this.lblType);
            this.fraForm.Controls.Add(this.lblInstructions);
            this.fraForm.Location = new System.Drawing.Point(10, 10);
            this.fraForm.Name = "fraForm";
            this.fraForm.Size = new System.Drawing.Size(765, 179);
            this.fraForm.TabIndex = 0;
            this.fraForm.UseMnemonic = false;
            // 
            // cmbForm
            // 
            this.cmbForm.AutoSize = false;
            this.cmbForm.BackColor = System.Drawing.SystemColors.Window;
            this.cmbForm.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbForm.FormattingEnabled = true;
            this.cmbForm.Location = new System.Drawing.Point(183, 107);
            this.cmbForm.Name = "cmbForm";
            this.cmbForm.Size = new System.Drawing.Size(396, 40);
            this.cmbForm.TabIndex = 2;
            this.cmbForm.Visible = false;
            this.cmbForm.DropDown += new System.EventHandler(this.cmbForm_DropDown);
            this.cmbForm.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbForm_KeyDown);
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(20, 121);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(122, 17);
            this.lblType.TabIndex = 1;
            this.lblType.Text = "MAIL FORM TYPE";
            this.lblType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblType.Visible = false;
            // 
            // lblInstructions
            // 
            this.lblInstructions.Location = new System.Drawing.Point(20, 30);
            this.lblInstructions.Name = "lblInstructions";
            this.lblInstructions.Size = new System.Drawing.Size(559, 53);
            this.lblInstructions.TabIndex = 0;
            // 
            // lblPayments
            // 
            this.lblPayments.AutoSize = true;
            this.lblPayments.Location = new System.Drawing.Point(30, 131);
            this.lblPayments.Name = "lblPayments";
            this.lblPayments.Size = new System.Drawing.Size(238, 17);
            this.lblPayments.TabIndex = 0;
            this.lblPayments.Text = "CERTIFIED MAIL FORM ACCOUNTS";
            this.lblPayments.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblPayments.Visible = false;
            // 
            // vsPayments
            // 
            this.vsPayments.AllowSelection = false;
            this.vsPayments.AllowUserToResizeColumns = false;
            this.vsPayments.AllowUserToResizeRows = false;
            this.vsPayments.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsPayments.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsPayments.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsPayments.BackColorBkg = System.Drawing.Color.Empty;
            this.vsPayments.BackColorFixed = System.Drawing.Color.Empty;
            this.vsPayments.BackColorSel = System.Drawing.Color.Empty;
            this.vsPayments.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsPayments.Cols = 6;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsPayments.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vsPayments.ColumnHeadersHeight = 30;
            this.vsPayments.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsPayments.DefaultCellStyle = dataGridViewCellStyle2;
            this.vsPayments.DragIcon = null;
            this.vsPayments.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsPayments.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.vsPayments.FixedCols = 0;
            this.vsPayments.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsPayments.FrozenCols = 0;
            this.vsPayments.GridColor = System.Drawing.Color.Empty;
            this.vsPayments.GridColorFixed = System.Drawing.Color.Empty;
            this.vsPayments.Location = new System.Drawing.Point(30, 153);
            this.vsPayments.Name = "vsPayments";
            this.vsPayments.OutlineCol = 0;
            this.vsPayments.ReadOnly = true;
            this.vsPayments.RowHeadersVisible = false;
            this.vsPayments.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsPayments.RowHeightMin = 0;
            this.vsPayments.Rows = 1;
            this.vsPayments.ScrollTipText = null;
            this.vsPayments.ShowColumnVisibilityMenu = false;
            this.vsPayments.Size = new System.Drawing.Size(748, 243);
            this.vsPayments.StandardTab = true;
            this.vsPayments.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsPayments.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsPayments.TabIndex = 1;
            this.vsPayments.Visible = false;
            this.vsPayments.CurrentCellChanged += new System.EventHandler(this.vsPayments_RowColChange);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Location = new System.Drawing.Point(339, 30);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcess.Size = new System.Drawing.Size(120, 48);
            this.btnProcess.TabIndex = 1;
            this.btnProcess.Text = "Process";
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // cmdFileSelectAll
            // 
            this.cmdFileSelectAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileSelectAll.AppearanceKey = "toolbarButton";
            this.cmdFileSelectAll.Location = new System.Drawing.Point(699, 30);
            this.cmdFileSelectAll.Name = "cmdFileSelectAll";
            this.cmdFileSelectAll.Size = new System.Drawing.Size(76, 24);
            this.cmdFileSelectAll.TabIndex = 52;
            this.cmdFileSelectAll.Text = "Select All";
            this.cmdFileSelectAll.Click += new System.EventHandler(this.cmdFileSelectAll_Click);
            // 
            // cmdFileSelectToBottom
            // 
            this.cmdFileSelectToBottom.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileSelectToBottom.AppearanceKey = "toolbarButton";
            this.cmdFileSelectToBottom.Location = new System.Drawing.Point(486, 30);
            this.cmdFileSelectToBottom.Name = "cmdFileSelectToBottom";
            this.cmdFileSelectToBottom.Size = new System.Drawing.Size(207, 24);
            this.cmdFileSelectToBottom.TabIndex = 53;
            this.cmdFileSelectToBottom.Text = "Select Current Row To Bottom";
            this.cmdFileSelectToBottom.Click += new System.EventHandler(this.cmdFileSelectToBottom_Click);
            // 
            // cmdFileClearAll
            // 
            this.cmdFileClearAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileClearAll.AppearanceKey = "toolbarButton";
            this.cmdFileClearAll.Location = new System.Drawing.Point(375, 29);
            this.cmdFileClearAll.Name = "cmdFileClearAll";
            this.cmdFileClearAll.Size = new System.Drawing.Size(105, 24);
            this.cmdFileClearAll.TabIndex = 54;
            this.cmdFileClearAll.Text = "Clear Selection";
            this.cmdFileClearAll.Click += new System.EventHandler(this.cmdFileClearAll_Click);
            // 
            // frmReprintCMForm
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(798, 601);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmReprintCMForm";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Reprint Certified Mail Form";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmReprintCMForm_Load);
            this.Activated += new System.EventHandler(this.frmReprintCMForm_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmReprintCMForm_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmReprintCMForm_KeyPress);
            this.Resize += new System.EventHandler(this.frmReprintCMForm_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraForm)).EndInit();
            this.fraForm.ResumeLayout(false);
            this.fraForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsPayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileSelectAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileSelectToBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileClearAll)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCLabel lblPayments;
		private FCButton btnProcess;
		public FCButton cmdFileSelectAll;
		public FCButton cmdFileSelectToBottom;
		public FCButton cmdFileClearAll;
	}
}
