﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmLienMaturityFees.
	/// </summary>
	public partial class frmLienMaturityFees : BaseForm
	{
		public frmLienMaturityFees()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmLienMaturityFees InstancePtr
		{
			get
			{
				return (frmLienMaturityFees)Sys.GetInstance(typeof(frmLienMaturityFees));
			}
		}

		protected frmLienMaturityFees _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/02/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               09/07/2005              *
		// ********************************************************
		int intAction;
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsLData = new clsDRWrapper();
		clsDRWrapper rsValidate = new clsDRWrapper();
		bool boolLoaded;
		double dblFilingFee;
		double dblCertMailFee;
		bool boolChargeMort;
		bool boolChargeCert;
		bool boolChargeForNewOwner;
		// VBto upgrade warning: dtLMFDate As DateTime	OnWrite(string)
		DateTime dtLMFDate;
		int lngRK;
		bool boolDONOTACTIVATE;
		string strRateKeys;
		bool blnChargeIntParty;

		private void chkIntApplied_Click(object sender, System.EventArgs e)
		{
			if (chkIntApplied.CheckState == Wisej.Web.CheckState.Checked)
			{
				txtIntDate.Enabled = true;
			}
			else
			{
				txtIntDate.Enabled = false;
			}
		}

		private void frmLienMaturityFees_Activated(object sender, System.EventArgs e)
		{
			lngRK = frmRateRecChoice.InstancePtr.lngRateRecNumber;
			// MAL@20071001: Added support for multiple rate keys
			strRateKeys = frmRateRecChoice.InstancePtr.strRateKeyList;
			// If lngRK > 0 Then
			if (strRateKeys != "")
			{
				if (!boolLoaded)
				{
					FormatGrid();
					ShowGridFrame();
					this.Text = "Add Lien Maturity Fees";
					lblInstruction.Text = "To add lien maturity fees:" + "\r\n" + "1. Check the box beside the account." + "\r\n" + "2. Select 'Add Lien Maturity Fees' or press F12.";
					lblValidateInstruction.Text = "If these values are correct, Press F12 to advance.  If not, then please rerun your Lien Maturity Notices.";
					SetAct(0);
					FillValidateGrid();
					boolLoaded = true;
					if (!boolDONOTACTIVATE)
					{
						if (!FillDemandGrid())
						{
							boolDONOTACTIVATE = true;
							Close();
						}
						else
						{
							boolDONOTACTIVATE = false;
						}
					}
					else
					{
						// MsgBox "Stop"
					}
				}
			}
			else
			{
				// MsgBox "The rate record selected is 0.  Please choose another rate record.", MsgBoxStyle.Information, "Rate Record Violation"
				FCMessageBox.Show("No rate record selected.  Please choose a rate record.", MsgBoxStyle.Information, "Rate Record Violation");
				frmRateRecChoice.InstancePtr.Show(App.MainForm);
				Close();
				return;
			}
		}

		private void frmLienMaturityFees_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmLienMaturityFees.Icon	= "frmLienMaturityFees.frx":0000";
			//frmLienMaturityFees.FillStyle	= 0;
			//frmLienMaturityFees.ScaleWidth	= 9045;
			//frmLienMaturityFees.ScaleHeight	= 7230;
			//frmLienMaturityFees.LinkTopic	= "Form2";
			//frmLienMaturityFees.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "9.75";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//vsDemand.BackColor	= "-2147483643";
			//			//vsDemand.ForeColor	= "-2147483640";
			//vsDemand.BorderStyle	= 1;
			//vsDemand.FillStyle	= 0;
			//vsDemand.Appearance	= 1;
			//vsDemand.GridLines	= 1;
			//vsDemand.WordWrap	= 0;
			//vsDemand.ScrollBars	= 3;
			//vsDemand.RightToLeft	= 0;
			//vsDemand._cx	= 15584;
			//vsDemand._cy	= 8546;
			//vsDemand._ConvInfo	= 1;
			//vsDemand.MousePointer	= 0;
			//vsDemand.BackColorFixed	= -2147483633;
			//			//vsDemand.ForeColorFixed	= -2147483630;
			//vsDemand.BackColorSel	= -2147483635;
			//			//vsDemand.ForeColorSel	= -2147483634;
			//vsDemand.BackColorBkg	= -2147483636;
			//vsDemand.BackColorAlternate	= -2147483643;
			//vsDemand.GridColor	= -2147483633;
			//vsDemand.GridColorFixed	= -2147483632;
			//vsDemand.TreeColor	= -2147483632;
			//vsDemand.FloodColor	= 192;
			//vsDemand.SheetBorder	= -2147483642;
			//vsDemand.FocusRect	= 1;
			//vsDemand.HighLight	= 1;
			//vsDemand.AllowSelection	= -1  'True;
			//vsDemand.AllowBigSelection	= -1  'True;
			//vsDemand.AllowUserResizing	= 0;
			//vsDemand.SelectionMode	= 0;
			//vsDemand.GridLinesFixed	= 2;
			//vsDemand.GridLineWidth	= 1;
			//vsDemand.RowHeightMin	= 0;
			//vsDemand.RowHeightMax	= 0;
			//vsDemand.ColWidthMin	= 0;
			//vsDemand.ColWidthMax	= 0;
			//vsDemand.ExtendLastCol	= -1  'True;
			//vsDemand.FormatString	= "";
			//vsDemand.ScrollTrack	= -1  'True;
			//vsDemand.ScrollTips	= 0   'False;
			//vsDemand.MergeCells	= 0;
			//vsDemand.MergeCompare	= 0;
			//vsDemand.AutoResize	= -1  'True;
			//vsDemand.AutoSizeMode	= 0;
			//vsDemand.AutoSearch	= 0;
			//vsDemand.AutoSearchDelay	= 2;
			//vsDemand.MultiTotals	= -1  'True;
			//vsDemand.SubtotalPosition	= 1;
			//vsDemand.OutlineBar	= 0;
			//vsDemand.OutlineCol	= 0;
			//vsDemand.Ellipsis	= 0;
			//vsDemand.ExplorerBar	= 1;
			//vsDemand.PicturesOver	= 0   'False;
			//vsDemand.PictureType	= 0;
			//vsDemand.TabBehavior	= 0;
			//vsDemand.OwnerDraw	= 0;
			//vsDemand.ShowComboButton	= -1  'True;
			//vsDemand.TextStyle	= 0;
			//vsDemand.TextStyleFixed	= 0;
			//vsDemand.OleDragMode	= 0;
			//vsDemand.OleDropMode	= 0;
			//vsDemand.DataMode	= 0;
			//vsDemand.VirtualData	= -1  'True;
			//vsDemand.ComboSearch	= 3;
			//vsDemand.AutoSizeMouse	= -1  'True;
			//vsDemand.AllowUserFreezing	= 0;
			//vsDemand.BackColorFrozen	= 0;
			//			//vsDemand.ForeColorFrozen	= 0;
			//vsDemand.WallPaperAlignment	= 9;
			//vsValidate.BackColor	= "-2147483643";
			//			//vsValidate.ForeColor	= "-2147483640";
			//vsValidate.BorderStyle	= 1;
			//vsValidate.FillStyle	= 0;
			//vsValidate.Appearance	= 1;
			//vsValidate.GridLines	= 1;
			//vsValidate.WordWrap	= 0;
			//vsValidate.ScrollBars	= 3;
			//vsValidate.RightToLeft	= 0;
			//vsValidate._cx	= 11139;
			//vsValidate._cy	= 7881;
			//vsValidate._ConvInfo	= 1;
			//vsValidate.MousePointer	= 0;
			//vsValidate.BackColorFixed	= -2147483633;
			//			//vsValidate.ForeColorFixed	= -2147483630;
			//vsValidate.BackColorSel	= -2147483635;
			//			//vsValidate.ForeColorSel	= -2147483634;
			//vsValidate.BackColorBkg	= -2147483636;
			//vsValidate.BackColorAlternate	= -2147483643;
			//vsValidate.GridColor	= -2147483633;
			//vsValidate.GridColorFixed	= -2147483632;
			//vsValidate.TreeColor	= -2147483632;
			//vsValidate.FloodColor	= 192;
			//vsValidate.SheetBorder	= -2147483642;
			//vsValidate.FocusRect	= 1;
			//vsValidate.HighLight	= 1;
			//vsValidate.AllowSelection	= -1  'True;
			//vsValidate.AllowBigSelection	= -1  'True;
			//vsValidate.AllowUserResizing	= 0;
			//vsValidate.SelectionMode	= 0;
			//vsValidate.GridLinesFixed	= 2;
			//vsValidate.GridLineWidth	= 1;
			//vsValidate.RowHeightMin	= 0;
			//vsValidate.RowHeightMax	= 0;
			//vsValidate.ColWidthMin	= 0;
			//vsValidate.ColWidthMax	= 0;
			//vsValidate.ExtendLastCol	= -1  'True;
			//vsValidate.FormatString	= "";
			//vsValidate.ScrollTrack	= -1  'True;
			//vsValidate.ScrollTips	= 0   'False;
			//vsValidate.MergeCells	= 0;
			//vsValidate.MergeCompare	= 0;
			//vsValidate.AutoResize	= -1  'True;
			//vsValidate.AutoSizeMode	= 0;
			//vsValidate.AutoSearch	= 0;
			//vsValidate.AutoSearchDelay	= 2;
			//vsValidate.MultiTotals	= -1  'True;
			//vsValidate.SubtotalPosition	= 1;
			//vsValidate.OutlineBar	= 0;
			//vsValidate.OutlineCol	= 0;
			//vsValidate.Ellipsis	= 0;
			//vsValidate.ExplorerBar	= 0;
			//vsValidate.PicturesOver	= 0   'False;
			//vsValidate.PictureType	= 0;
			//vsValidate.TabBehavior	= 0;
			//vsValidate.OwnerDraw	= 0;
			//vsValidate.ShowComboButton	= -1  'True;
			//vsValidate.TextStyle	= 0;
			//vsValidate.TextStyleFixed	= 0;
			//vsValidate.OleDragMode	= 0;
			//vsValidate.OleDropMode	= 0;
			//vsValidate.ComboSearch	= 3;
			//vsValidate.AutoSizeMouse	= -1  'True;
			//vsValidate.AllowUserFreezing	= 0;
			//vsValidate.BackColorFrozen	= 0;
			//			//vsValidate.ForeColorFrozen	= 0;
			//vsValidate.WallPaperAlignment	= 9;
			//vsElasticLight1.OleObjectBlob	= "frmLienMaturityFees.frx":058A";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			boolLoaded = false;
			// clear the array of user defined types
			FCUtils.EraseSafe(modGlobal.Statics.arrDemand);
		}

		private void frmLienMaturityFees_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				Close();
				return;
			}
			else if (KeyAscii == 13)
			{
				KeyAscii = 0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
			boolDONOTACTIVATE = false;
		}

		private void frmLienMaturityFees_Resize(object sender, System.EventArgs e)
		{
			if (fraGrid.Visible)
			{
				ShowGridFrame();
				// this sets the height of the grid
				//FC:FINAL:AM: don't set the height; used anchoring instead
				//if ((vsDemand.Rows * vsDemand.RowHeight(0)) + 70 > fraGrid.Height - vsDemand.Top - 300)
				//{
				//	vsDemand.Height = fraGrid.Height - vsDemand.Top - 300;
				//	vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
				//}
				//else
				//{
				//	vsDemand.Height = (vsDemand.Rows * vsDemand.RowHeight(0)) + 70;
				//	vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				//}
			}
			else
			{
				ShowValidateFrame();
			}
		}

		private void mnuFileClear_Click(object sender, System.EventArgs e)
		{
			// this will clear all account checkboxes in the grid
			int lngCT;
			for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
			{
				vsDemand.TextMatrix(lngCT, 0, FCConvert.ToString(0));
			}
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void ShowValidateFrame()
		{
			// this will show/center the frame with the grid on it
			//fraValidate.Top = FCConvert.ToInt32((this.Height - fraValidate.Height) / 3.0);
			//fraValidate.Left = FCConvert.ToInt32((this.Width - fraValidate.Width) / 2.0);
			fraValidate.Visible = true;
		}

		private void ShowGridFrame()
		{
			// this will show/center the frame with the grid on it
			//fraGrid.Top = FCConvert.ToInt32((this.Height - fraGrid.Height) / 3.0);
			//fraGrid.Left = FCConvert.ToInt32((this.Width - fraGrid.Width) / 2.0);
			fraGrid.Visible = true;
		}

		private void FormatGrid()
		{
			int wid = 0;
			vsDemand.Cols = 7;
			vsDemand.Rows = 1;
			wid = vsDemand.WidthOriginal;
			vsDemand.ColWidth(0, FCConvert.ToInt32(wid * 0.1));
			// Checkbox
			vsDemand.ColWidth(1, FCConvert.ToInt32(wid * 0.15));
			// Acct
			vsDemand.ColWidth(2, FCConvert.ToInt32(wid * 0.44));
			// Name
			vsDemand.ColWidth(3, FCConvert.ToInt32(wid * 0.16));
			// Total Notices Sent
			vsDemand.ColWidth(4, FCConvert.ToInt32(wid * 0.1));
			// Total Lien Costs
			vsDemand.ColWidth(5, 0);
			// Hidden Key Field
			vsDemand.ColWidth(6, 0);
			// Hidden Code Field - 0 is ok to process...anything else is bad
			vsDemand.ColFormat(4, "#,##0.00");
			vsDemand.ColDataType(0, FCGrid.DataTypeSettings.flexDTBoolean);
			vsDemand.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsDemand.TextMatrix(0, 1, "Account");
			vsDemand.TextMatrix(0, 2, "Name");
			// MAL@20071211
			// .TextMatrix(0, 3) = "Notices"
			vsDemand.TextMatrix(0, 3, "Charged Notices");
			vsDemand.TextMatrix(0, 4, "Amount");
			vsValidate.Cols = 3;
			wid = vsValidate.WidthOriginal;
			vsValidate.ColWidth(0, FCConvert.ToInt32(wid * 0.6));
			// Question
			vsValidate.ColWidth(1, FCConvert.ToInt32(wid * 0.32));
			// Answer
			vsValidate.ColWidth(2, 0);
			// Hidden field
			vsValidate.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsValidate.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsValidate.TextMatrix(0, 1, "Parameters");
			vsValidate.TextMatrix(0, 2, "Value");
		}

		private bool FillDemandGrid()
		{
			bool FillDemandGrid = false;
			int lngError = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the grid with eligible accounts
				string strSQL = "";
				int lngIndex;
				clsDRWrapper rsMaster = new clsDRWrapper();
				double dblXInt = 0;
				clsDRWrapper rsRK = new clsDRWrapper();
				DateTime dtBillDate;
				int lngMH = 0;
				int lngCopies = 0;
				double dblAmount = 0;
				int lngIP = 0;
				clsDRWrapper rsIP = new clsDRWrapper();
				string strFields;
				FillDemandGrid = true;
				rsMaster.OpenRecordset("SELECT * FROM Master", modExtraModules.strREDatabase);
				lngError = 1;
				vsDemand.Rows = 1;
				lngError = 2;
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Info");
				lngError = 3;
				// MAL@20071001: Support for multiple rate keys
				// If frmRateRecChoice.optRange(0).Value Then
				// strSQL = "SELECT * FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.LienRecordNumber WHERE LienRec.RateKey = " & lngRK & " AND LienProcessStatus = 5 AND LienStatusEligibility = 5 ORDER BY Name1, Account"      'this will be all the records that have had Lien Maturity Notices
				// ElseIf frmRateRecChoice.optRange(1).Value Then
				// strSQL = "SELECT * FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.LienRecordNumber WHERE LienRec.RateKey = " & lngRK & " AND LienProcessStatus = 5 AND LienStatusEligibility = 5 AND Name1 > '" & frmRateRecChoice.txtRange(0).Text & "    ' AND Name1 <= '" & frmRateRecChoice.txtRange(1).Text & "ZZZZ' ORDER BY Name1, Account"
				// Else
				// strSQL = "SELECT * FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.LienRecordNumber WHERE LienRec.RateKey = " & lngRK & " AND LienProcessStatus = 5 AND LienStatusEligibility = 5 AND Account >= " & frmRateRecChoice.txtRange(0).Text & " AND Account <= " & frmRateRecChoice.txtRange(1).Text & " ORDER BY Name1, Account"
				// End If
				// kgk 04-15-2011  trocl-657  Added "AND NOT LienProcessExclusion" in case notices were printed before the account was excluded
				strFields = "BillingMaster.*, LienRec.RateKey AS LR_RateKey, LienRec.InterestAppliedThroughDate AS LR_InterestAppliedThroughDate, LienRec.Principal, LienRec.PrincipalPaid AS LR_PrincipalPaid, LienRec.InterestCharged AS LR_InterestCharged, LienRec.InterestPaid AS LR_InterestPaid, LienRec.Interest, LienRec.PLIPaid, LienRec.Costs, LienRec.MaturityFee, LienRec.CostsPaid";
				if (frmRateRecChoice.InstancePtr.cmbRange.SelectedIndex == 0)
				{
					strSQL = "SELECT " + strFields + " FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.ID WHERE LienRec.RateKey IN " + strRateKeys + " AND LienProcessStatus = 5 AND LienStatusEligibility = 5 AND LienProcessExclusion = 0 ORDER BY Name1, Account";
					// this will be all the records that have had Lien Maturity Notices
				}
				else if (frmRateRecChoice.InstancePtr.cmbRange.SelectedIndex == 1)
				{
					strSQL = "SELECT " + strFields + " FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.ID WHERE LienRec.RateKey IN " + strRateKeys + " AND LienProcessStatus = 5 AND LienStatusEligibility = 5 AND LienProcessExclusion = 0 AND Name1 > '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "    ' AND Name1 <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "ZZZZ' ORDER BY Name1, Account";
				}
				else
				{
					strSQL = "SELECT " + strFields + " FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.ID WHERE LienRec.RateKey IN " + strRateKeys + " AND LienProcessStatus = 5 AND LienStatusEligibility = 5 AND LienProcessExclusion = 0 AND Account >= " + frmRateRecChoice.InstancePtr.txtRange[0].Text + " AND Account <= " + frmRateRecChoice.InstancePtr.txtRange[1].Text + " ORDER BY Name1, Account";
				}
				lngError = 4;
				// strSQL = "SELECT * FROM BillingMaster WHERE LienProcessStatus = 5 AND LienStatusEligibility = 5 ORDER BY Name1"      'this will be all the records that have had 30 Day Notices printed
				rsData.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
				rsLData.OpenRecordset("SELECT * FROM LienRec", modExtraModules.strCLDatabase);
				// this is a recordset of all the lien records
				if (rsData.EndOfFile())
				{
					frmWait.InstancePtr.Unload();
					App.DoEvents();
					FillDemandGrid = false;
					FCMessageBox.Show("There are no accounts eligible to transfer to apply Lien Maturity Fees.", MsgBoxStyle.Information, "No Eligible Accounts - " + FCConvert.ToString(lngRK));
					return FillDemandGrid;
				}
				lngError = 5;
				while (!rsData.EndOfFile())
				{
					// MAL@20071127: Reset values
					lngMH = 0;
					lngCopies = 0;
					dblAmount = 0;
					// find the first lien record
					TryAgain:
					;
					lngError = 11;
					rsLData.FindFirstRecord("ID", rsData.Get_Fields_Int32("LienRecordNumber"));
					if (rsLData.NoMatch)
					{
						// if there is no match, then report the error
						lngError = 12;
						frmWait.InstancePtr.Unload();
						FCMessageBox.Show("Cannot find Lien Record #" + FCConvert.ToString(rsData.Get_Fields_Int32("LienRecordNumber")) + ".", MsgBoxStyle.Critical, "Missing Lien Record");
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
						if (Conversion.Val(rsLData.Get_Fields("Principal")) - Conversion.Val(rsLData.Get_Fields_Decimal("PrincipalPaid")) <= 0)
						{
							rsData.MoveNext();
							if (rsData.EndOfFile())
								break;
							goto TryAgain;
						}
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsMaster.FindFirstRecord2("RSAccount,RSCard", FCConvert.ToString(rsData.Get_Fields("Account")) + ",1", ",");
						if (rsMaster.NoMatch)
						{
							lngError = 12;
							frmWait.InstancePtr.Unload();
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							FCMessageBox.Show("Cannot find Master Record for account #" + FCConvert.ToString(rsData.Get_Fields("Account")) + ".", MsgBoxStyle.Critical, "Missing Master Record");
							// MAL@20071001: Found this error (endless loop) while testing for other issue
							rsData.MoveNext();
							if (rsData.EndOfFile())
								break;
							goto TryAgain;
						}
						else
						{
							if (FCConvert.ToBoolean(rsMaster.Get_Fields_Boolean("InBankruptcy")))
							{
								// this account is not eligible for LMFs
								goto SKIP;
							}
							else
							{
								if (modCLCalculations.CalculateAccountCLLien(rsLData, dtLMFDate, ref dblXInt) <= 0)
								{
									goto SKIP;
								}
								lngError = 13;
								vsDemand.AddItem("");
								Array.Resize(ref modGlobal.Statics.arrDemand, vsDemand.Rows - 1 + 1);
								// this will make sure that there are enough elements to use
								lngIndex = vsDemand.Rows - 2;
								//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
								modGlobal.Statics.arrDemand[lngIndex] = new modGlobal.AccountFeesAdded(0);
								modGlobal.Statics.arrDemand[lngIndex].Used = true;
								modGlobal.Statics.arrDemand[lngIndex].Processed = false;
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								vsDemand.TextMatrix(vsDemand.Rows - 1, 1, FCConvert.ToString(rsData.Get_Fields("Account")));
								// account number
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								modGlobal.Statics.arrDemand[lngIndex].Account = FCConvert.ToInt32(rsData.Get_Fields("Account"));
								vsDemand.TextMatrix(vsDemand.Rows - 1, 2, FCConvert.ToString(rsData.Get_Fields_String("Name1")));
								// name
								modGlobal.Statics.arrDemand[lngIndex].Name = FCConvert.ToString(rsData.Get_Fields_String("Name1"));
								// TODO Get_Fields: Check the table for the column [Copies] and replace with corresponding Get_Field method
								vsDemand.TextMatrix(vsDemand.Rows - 1, 3, FCConvert.ToString(rsData.Get_Fields("Copies")));
								// number of copies
								// amount
								// If rsLData.Get_Fields("Costs") = 0 Then
								// highlight the row and set the value = zero
								//								// .Cell(FCGrid.CellPropertySettings.flexcpForeColor, .rows - 1, 0, .rows - 1, .Cols - 1) = Color.Red
								// .TextMatrix(.rows - 1, 4) = 0
								// .TextMatrix(.rows - 1, 6) = -1
								// Else
								// gonna have to figure out the amount
								// MAL@20071127: Fixed flaw in logic. If Charge Mortgage Holder and no mortgage holder, it is still charging for all copies
								if (boolChargeCert)
								{
									if (boolChargeMort)
									{
										// MAL@20071127
										lngMH = CalculateMortgageHolders();
										// TODO Get_Fields: Check the table for the column [Copies] and replace with corresponding Get_Field method
										lngCopies = FCConvert.ToInt32(rsData.Get_Fields("Copies"));
										if (lngMH > 0)
										{
											if (lngCopies > lngMH)
											{
												// TODO Get_Fields: Check the table for the column [Copies] and replace with corresponding Get_Field method
												lngCopies = FCConvert.ToInt32(rsData.Get_Fields("Copies")) - lngMH;
												dblAmount = (lngMH * dblCertMailFee);
											}
										}
										else
										{
											lngCopies = lngCopies;
										}
										// Check for Interested Parties
										if (blnChargeIntParty)
										{
											// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
											lngIP = modCLCalculations.CalculateInterestedParties(FCConvert.ToInt32(rsData.Get_Fields("Account")), rsIP);
											if (lngIP > 0)
											{
												dblAmount += (lngIP * dblCertMailFee);
											}
											else
											{
												dblAmount = dblAmount;
											}
										}
										// Check for New Owner
										rsRK.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(rsData.Get_Fields_Int32("RateKey")), modExtraModules.strCLDatabase);
										if (!rsRK.EndOfFile())
										{
											dtBillDate = (DateTime)rsRK.Get_Fields_DateTime("BillingDate");
										}
										else
										{
											dtBillDate = (DateTime)rsData.Get_Fields_DateTime("TransferFromBillingDateFirst");
										}
										// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
										if (boolChargeForNewOwner && modCLCalculations.NewOwner2(FCConvert.ToString(rsData.Get_Fields_String("Name1")), FCConvert.ToInt32(rsData.Get_Fields("Account")), dtBillDate))
										{
											dblAmount += (dblCertMailFee * 2);
										}
										else
										{
											dblAmount += dblCertMailFee;
										}
										vsDemand.TextMatrix(vsDemand.Rows - 1, 4, FCConvert.ToString(dblAmount + dblFilingFee));
										// .TextMatrix(.rows - 1, 4) = (rsData.Get_Fields("Copies") * dblCertMailFee) + dblDemand
										modGlobal.Statics.arrDemand[lngIndex].CertifiedMailFee = FCConvert.ToDouble(vsDemand.TextMatrix(vsDemand.Rows - 1, 4));
										// .TextMatrix(.rows - 1, 4) = (rsData.Get_Fields("Copies") * dblCertMailFee) + dblFilingFee
										// arrDemand(lngIndex).CertifiedMailFee = CDbl(.TextMatrix(.rows - 1, 4))
									}
									else
									{
										// may have to add the new owner charge here....
										// MAL@20071126: Added check for new owner charge the same as the other processes
										// .TextMatrix(.rows - 1, 4) = dblCertMailFee + dblFilingFee
										// arrDemand(lngIndex).CertifiedMailFee = dblCertMailFee
										// Check for Interested Parties
										if (blnChargeIntParty)
										{
											// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
											lngIP = modCLCalculations.CalculateInterestedParties(FCConvert.ToInt32(rsData.Get_Fields("Account")), rsIP);
											if (lngIP > 0)
											{
												dblAmount += (lngIP * dblCertMailFee);
											}
											else
											{
												dblAmount = dblAmount;
											}
										}
										rsRK.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(rsData.Get_Fields_Int32("RateKey")), modExtraModules.strCLDatabase);
										if (!rsRK.EndOfFile())
										{
											dtBillDate = (DateTime)rsRK.Get_Fields_DateTime("BillingDate");
										}
										else
										{
											dtBillDate = (DateTime)rsData.Get_Fields_DateTime("TransferFromBillingDateFirst");
										}
										// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
										if (boolChargeForNewOwner && modCLCalculations.NewOwner2(FCConvert.ToString(rsData.Get_Fields_String("Name1")), FCConvert.ToInt32(rsData.Get_Fields("Account")),  dtBillDate))
										{
											vsDemand.TextMatrix(vsDemand.Rows - 1, 4, FCConvert.ToString((dblCertMailFee * 2) + dblFilingFee + dblAmount));
											modGlobal.Statics.arrDemand[lngIndex].CertifiedMailFee = FCConvert.ToDouble(vsDemand.TextMatrix(vsDemand.Rows - 1, 4));
										}
										else
										{
											vsDemand.TextMatrix(vsDemand.Rows - 1, 4, FCConvert.ToString(dblCertMailFee + dblFilingFee + dblAmount));
											modGlobal.Statics.arrDemand[lngIndex].CertifiedMailFee = FCConvert.ToDouble(vsDemand.TextMatrix(vsDemand.Rows - 1, 4));
										}
									}
								}
								else
								{
									vsDemand.TextMatrix(vsDemand.Rows - 1, 4, FCConvert.ToString(dblFilingFee));
									modGlobal.Statics.arrDemand[lngIndex].CertifiedMailFee = 0;
								}
								modGlobal.Statics.arrDemand[lngIndex].Fee = dblFilingFee;
								// End If
								vsDemand.TextMatrix(vsDemand.Rows - 1, 5, FCConvert.ToString(rsData.Get_Fields_Int32("ID")));
								// billkey
								lngError = 14;
							}
							SKIP:
							;
							rsData.MoveNext();
						}
					}
				}
				if (!modGlobal.Statics.arrDemand[0].Used)
				{
					// lngIndex = 0 Then
					frmWait.InstancePtr.Unload();
					App.DoEvents();
					FillDemandGrid = false;
					FCMessageBox.Show("There are no accounts eligible to transfer to apply Lien Maturity Fees.", MsgBoxStyle.Information, "No Eligible Accounts - " + FCConvert.ToString(lngRK));
					return FillDemandGrid;
				}
				lngError = 9;
				// check all of the accounts
				mnuFileSelectAll_Click();
				lngError = 10;
				// this sets the height of the grid
				//FC:FINAL:AM: don't set the height; used anchoring instead
				//if ((vsDemand.Rows * vsDemand.RowHeight(0)) + 70 > fraGrid.Height - vsDemand.Top - 300)
				//{
				//	vsDemand.Height = fraGrid.Height - vsDemand.Top - 300;
				//	vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
				//}
				//else
				//{
				//	vsDemand.Height = (vsDemand.Rows * vsDemand.RowHeight(0)) + 70;
				//	vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				//}
				lngError = 11;
				frmWait.InstancePtr.Unload();
				return FillDemandGrid;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FillDemandGrid = false;
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Filling Grid - " + FCConvert.ToString(lngError));
			}
			return FillDemandGrid;
		}

		private void FillValidateGrid()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the validate grid with the information from the Control_30DayNotice table
				// which is the information that was used to print the 30 Day Notices the last time
				vsValidate.Rows = 1;
				rsValidate.OpenRecordset("SELECT * FROM Control_LienMaturity");
				if (rsValidate.EndOfFile())
				{
					FCMessageBox.Show("Please return to and run 'Print Lien Maturity Notice'.", MsgBoxStyle.Information, "No Control Record");
					Close();
					return;
				}
				else
				{
					vsValidate.AddItem("Billing Year" + "\t" + FCConvert.ToString(rsValidate.Get_Fields_Int32("BillingYear")));
					dtLMFDate = FCConvert.ToDateTime(Strings.Format(rsValidate.Get_Fields_DateTime("MailDate"), "MM/dd/yyyy"));
					vsValidate.AddItem("Mailing Date" + "\t" + Strings.Format(dtLMFDate, "MM/dd/yyyy"));
					vsValidate.AddItem("Filing Date" + "\t" + Strings.Format(rsValidate.Get_Fields_DateTime("LienFilingDate"), "MM/dd/yyyy"));
					// vsValidate.AddItem "Certified Mail Fee" & vbTab & Format(.Get_Fields("CertMailFee"), "#,##0.00")
					dblFilingFee = Conversion.Val(rsValidate.Get_Fields_Double("ForeclosureFee"));
					vsValidate.AddItem("Foreclosure Fee" + "\t" + Strings.Format(dblFilingFee, "#,##0.00"));
					boolChargeCert = FCConvert.ToBoolean(rsValidate.Get_Fields_Boolean("PayCertMailFee"));
					boolChargeMort = FCConvert.ToBoolean(rsValidate.Get_Fields_Boolean("ChargeForMortHolder"));
					if (boolChargeCert)
					{
						vsValidate.AddItem("Certified Mail Fee" + "\t" + Strings.Format(rsValidate.Get_Fields_Double("CertMailFee"), "#,##0.00"));
						dblCertMailFee = Conversion.Val(rsValidate.Get_Fields_Double("CertMailFee"));
						vsValidate.AddItem("Charge taxpayer for Cert Mail Fee?" + "\t" + "Yes");
					}
					else
					{
						if (boolChargeMort)
						{
							vsValidate.AddItem("Certified Mail Fee" + "\t" + Strings.Format(rsValidate.Get_Fields_Double("CertMailFee"), "#,##0.00"));
						}
						else
						{
							vsValidate.AddItem("Certified Mail Fee" + "\t" + "0.00");
						}
						vsValidate.AddItem("Charge taxpayer for Cert Mail Fee?" + "\t" + "No");
						dblCertMailFee = 0;
					}
					if (FCConvert.ToBoolean(rsValidate.Get_Fields_Boolean("SendCopyToMortHolder")))
					{
						if (boolChargeMort)
						{
							vsValidate.AddItem("Charge for each mortgage holder?" + "\t" + "Yes");
						}
						else
						{
							vsValidate.AddItem("Charge for each mortgage holder?" + "\t" + "No");
						}
					}
					else
					{
						vsValidate.AddItem("Charge for each mortgage holder?" + "\t" + "No");
					}
					// MAL@20080102: Add Interested Party
					blnChargeIntParty = FCConvert.ToBoolean(rsValidate.Get_Fields_Boolean("ChargeforIntParty"));
					if (FCConvert.ToBoolean(rsValidate.Get_Fields_Boolean("SendCopyToIntParty")))
					{
						if (blnChargeIntParty)
						{
							vsValidate.AddItem("Charge for each interested party?" + "\t" + "Yes");
						}
						else
						{
							vsValidate.AddItem("Charge for each interested party?" + "\t" + "No");
						}
					}
					else
					{
						vsValidate.AddItem("Charge for each interested party?" + "\t" + "No");
					}
					if (FCConvert.ToString(rsValidate.Get_Fields_String("SendCopyToNewOwner")) != "")
					{
						if (Strings.Left(FCConvert.ToString(rsValidate.Get_Fields_String("SendCopyToNewOwner")), 2) == "Ye")
						{
							// MAL@20071126: Add to match other processes
							if (Strings.Left(FCConvert.ToString(rsValidate.Get_Fields_String("SendCopyToNewOwner")), 8) == "Yes, cha")
							{
								vsValidate.AddItem("Send to New Owner" + "\t" + "Yes, charge CMF.");
								boolChargeForNewOwner = true;
							}
							else
							{
								vsValidate.AddItem("Send to New Owner" + "\t" + "Yes, with no charge.");
								boolChargeForNewOwner = false;
							}
							// vsValidate.AddItem "Send to New Owner" & vbTab & .Get_Fields("SendCopyToNewOwner")
						}
						else
						{
							vsValidate.AddItem("Send to New Owner" + "\t" + "No");
						}
					}
					else
					{
						vsValidate.AddItem("Send to New Owner" + "\t" + "No");
					}
					// gboolUseMailDateForMaturity = CBool(.Get_Fields("UseMailDateForMaturity"))
				}
				// this will set the default date
				txtIntDate.Text = FCConvert.ToString(dtLMFDate);
				// set the height of the grid
				//if (vsValidate.Rows * vsValidate.RowHeight(0) > fraValidate.Height - 300)
				//{
				//	vsValidate.Height = fraValidate.Height - 300;
				//	vsValidate.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
				//}
				//else
				//{
				//	vsValidate.Height = vsValidate.Rows * vsValidate.RowHeight(0) + 70;
				//	vsValidate.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				//}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Filling Validation Grid");
			}
		}

		private void SetAct(short intAct)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will change all of the menu options
				switch (intAct)
				{
					case 0:
						{
							ShowValidateFrame();
							fraGrid.Visible = false;
							btnProcess.Text = "Accept Parameters";
							cmdFileSelectAll.Visible = false;
							intAction = 0;
							break;
						}
					case 1:
						{
							ShowGridFrame();
							fraValidate.Visible = false;
							cmdFileSelectAll.Visible = true;
							btnProcess.Text = "Add Maturity Fees";
							intAction = 1;
							break;
						}
				}
				//end switch
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Set Action");
			}
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			switch (intAction)
			{
				case 0:
					{
						// this will say that the user accepts the parameters shown
						FCMessageBox.Show("After you apply the maturity fees, you will not be able to reprint the notices, labels or certified mailers.", MsgBoxStyle.Exclamation, "Apply Maturity Fees");
						SetAct(1);
						break;
					}
				case 1:
					{
						// Apply the Lien Maturity Fees for the accounts selected
						if (ApplyMaturityFees())
						{
							Close();
						}
						return;
					}
			}
			//end switch
		}

		private bool ApplyMaturityFees()
		{
			bool ApplyMaturityFees = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngCT;
				int lngDemandCount = 0;
				clsDRWrapper rsRate = new clsDRWrapper();
				ApplyMaturityFees = true;
				if (chkIntApplied.CheckState == Wisej.Web.CheckState.Checked)
				{
					if (Information.IsDate(txtIntDate.Text))
					{
						if (DateAndTime.DateDiff("D", DateAndTime.DateValue(txtIntDate.Text), dtLMFDate) <= 0)
						{
							// date is after or same as the LMF date
							// is ok...keep rockin
						}
						else
						{
							// date before LMF Date
							FCMessageBox.Show("Interest Applied Through Date is before the the Lien Maturity Fee Date of " + FCConvert.ToString(dtLMFDate) + ".  Please enter a date that is the same or later than the Lien Maturity Fee Date.", MsgBoxStyle.Exclamation, "Invalid Date");
							ApplyMaturityFees = false;
							return ApplyMaturityFees;
						}
					}
					else
					{
						FCMessageBox.Show("Invalid Interest Applied Through Date.", MsgBoxStyle.Exclamation, "Invalid Date");
						ApplyMaturityFees = false;
						return ApplyMaturityFees;
					}
				}
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Applying Fees");
				for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
				{
					// for each account in the grid
					App.DoEvents();
					if (Conversion.Val(vsDemand.TextMatrix(lngCT, 0)) == -1)
					{
						// check to see if the check box if checked
						if (Conversion.Val(vsDemand.TextMatrix(lngCT, 6)) >= 0)
						{
							// then find the bill record
							rsData.FindFirstRecord("ID", vsDemand.TextMatrix(lngCT, 5));
							if (rsData.NoMatch)
							{
								FCMessageBox.Show("Error processing account " + vsDemand.TextMatrix(lngCT, 1) + ".  No lien record was found.", MsgBoxStyle.Critical, "Cannot Find BillKey - " + vsDemand.TextMatrix(lngCT, 5));
							}
							else
							{
								modGlobal.Statics.arrDemand[lngCT - 1].Processed = true;
								CreateFeeRecord_2(FCConvert.ToDouble(vsDemand.TextMatrix(lngCT, 4)));
								lngDemandCount += 1;
							}
						}
						else
						{
							// not eligible...do nothing
						}
					}
					else
					{
						// not selected...do nothing
					}
				}
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show(FCConvert.ToString(lngDemandCount) + " accounts were affected.", MsgBoxStyle.Information, "Liened Accounts Added");
				// If lngDemandCount > 0 Then
				// print the list of accounts that have been affected
				rptLienMaturityFees.InstancePtr.Init(lngDemandCount, dblFilingFee);
				frmReportViewer.InstancePtr.Init(rptLienMaturityFees.InstancePtr);
				// End If
				// rsData.OpenRecordset "SELECT Distinct BillingMaster.RateKey AS RK FROM (" & rsData.Name & ")", strCLDatabase
				if (frmRateRecChoice.InstancePtr.cmbRange.SelectedIndex == 0)
				{
					rsData.OpenRecordset("SELECT DISTINCT BillingMaster.RateKey AS RK FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.ID WHERE LienRec.RateKey IN " + strRateKeys + " AND LienProcessStatus = 5 AND LienStatusEligibility = 5 AND LienProcessExclusion = 0", modExtraModules.strCLDatabase);
					// this will be all the records that have had Lien Maturity Notices
				}
				else if (frmRateRecChoice.InstancePtr.cmbRange.SelectedIndex == 1)
				{
					rsData.OpenRecordset("SELECT DISTINCT BillingMaster.RateKey AS RK FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.ID WHERE LienRec.RateKey IN " + strRateKeys + " AND LienProcessStatus = 5 AND LienStatusEligibility = 5 AND LienProcessExclusion = 0 AND Name1 > '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "    ' AND Name1 <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "ZZZZ'", modExtraModules.strCLDatabase);
				}
				else
				{
					rsData.OpenRecordset("SELECT  DISTINCT BillingMaster.RateKey AS RK FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.ID WHERE LienRec.RateKey IN " + strRateKeys + " AND LienProcessStatus = 5 AND LienStatusEligibility = 5 AND LienProcessExclusion = 0 AND Account >= " + frmRateRecChoice.InstancePtr.txtRange[0].Text + " AND Account <= " + frmRateRecChoice.InstancePtr.txtRange[1].Text, modExtraModules.strCLDatabase);
				}
				while (!rsData.EndOfFile())
				{
					// TODO Get_Fields: Field [RK] not found!! (maybe it is an alias?)
					rsRate.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(rsData.Get_Fields("RK")), modExtraModules.strCLDatabase);
					if (!(rsRate.Get_Fields_DateTime("MaturityDate") is DateTime))
					{
						rsRate.Edit();
						rsRate.Set_Fields("MaturityDate", DateTime.Today);
						rsRate.Update();
					}
					rsData.MoveNext();
				}
				return ApplyMaturityFees;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Applying Fees");
			}
			return ApplyMaturityFees;
		}

		private void mnuFileSelectAll_Click(object sender, System.EventArgs e)
		{
			// this will select all account in the grid
			int lngCT;
			for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
			{
				vsDemand.TextMatrix(lngCT, 0, FCConvert.ToString(-1));
			}
		}

		public void mnuFileSelectAll_Click()
		{
			mnuFileSelectAll_Click(cmdFileSelectAll, new System.EventArgs());
		}

		private int CreateFeeRecord_2(double dblTotalFee)
		{
			return CreateFeeRecord(ref dblTotalFee);
		}

		private int CreateFeeRecord(ref double dblTotalFee)
		{
			int CreateFeeRecord = 0;
			// this will calculate the CHGINT line for the account and then return
			// the CHGINT number to store in the payment record
			// this will use rsData which is set to the correct record
			double dblTotalDue = 0;
			double dblInt = 0;
			double dblFee = 0;
			clsDRWrapper rsFee = new clsDRWrapper();
			clsDRWrapper rsTemp = new clsDRWrapper();
			rsLData.FindFirstRecord("ID", rsData.Get_Fields_Int32("LienRecordNumber"));
			if (rsLData.NoMatch)
			{
				// no match for the lien record...do not process the account
			}
			else
			{
				// calculate the current interest to this date
				dblTotalDue = modCLCalculations.CalculateAccountCLLien(rsLData, dtLMFDate, ref dblInt);
				if (dblTotalDue > 0)
				{
					// if there is interest due, then
					// this will actually create the payment line
					rsFee.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = 0", modExtraModules.strCLDatabase);
					rsFee.AddNew();
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					rsFee.Set_Fields("Account", rsData.Get_Fields("Account"));
					rsFee.Set_Fields("Year", rsData.Get_Fields_Int32("BillingYear"));
					rsFee.Set_Fields("BillKey", rsData.Get_Fields_Int32("LienRecordNumber"));
					rsFee.Set_Fields("CHGINTNumber", AddCHGINTForFee(ref dblInt));
					rsFee.Set_Fields("CHGINTDate", dtLMFDate);
					rsFee.Set_Fields("ActualSystemDate", DateTime.Today);
					rsFee.Set_Fields("EffectiveInterestDate", dtLMFDate);
					rsFee.Set_Fields("RecordedTransactionDate", dtLMFDate);
					rsFee.Set_Fields("Teller", Strings.UCase(Strings.Left(modGlobalConstants.Statics.gstrUserID, 3)));
					rsFee.Set_Fields("Reference", "FCFEES");
					rsFee.Set_Fields("Period", "A");
					rsFee.Set_Fields("Code", "L");
					rsFee.Set_Fields("ReceiptNumber", 0);
					rsFee.Set_Fields("Principal", 0);
					rsFee.Set_Fields("PreLienInterest", 0);
					rsFee.Set_Fields("CurrentInterest", 0);
					// calculate the fee
					// If boolChargeCert Then          'charge the certified mail fee
					// If boolChargeMort Then      'charge for each mortgage holder
					// dblFee = rsData.Get_Fields("Copies") * dblCertMailFee
					// Else
					// dblFee = dblCertMailFee
					// End If
					// Else                            'no cert mail charge
					// dblFee = 0
					// End If
					// dblFee = dblFee + dblFilingFee  'add the filing fee for the foreclosure
					dblFee = dblTotalFee;
					rsFee.Set_Fields("LienCost", dblFee * -1);
					// this has to be negative so that the status screen knows that it is charged not paid
					rsFee.Set_Fields("TransNumber", 0);
					rsFee.Set_Fields("PaidBy", "Automatic/Computer");
					rsFee.Set_Fields("Comments", "Lien Maturity Fee");
					rsFee.Set_Fields("CashDrawer", "N");
					rsFee.Set_Fields("GeneralLedger", "N");
					rsFee.Set_Fields("BudgetaryAccountNumber", "");
					rsFee.Set_Fields("BillCode", "L");
					rsFee.Set_Fields("DailyCloseOut", false);
					rsFee.Update();
					// this will edit the Lien Record
					rsLData.Edit();
					rsLData.Set_Fields("InterestCharged", (Conversion.Val(rsLData.Get_Fields_Decimal("InterestCharged")) - dblInt));
					// TODO Get_Fields: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
					rsLData.Set_Fields("MaturityFee", (Conversion.Val(rsLData.Get_Fields("MaturityFee")) - dblFee));
					if (chkIntApplied.CheckState == Wisej.Web.CheckState.Checked)
					{
						if (Information.IsDate(txtIntDate.Text))
						{
							// add a CYA entry for every Lien with a different Int PD THR date
							modGlobalFunctions.AddCYAEntry_62("CL", "Changing Interest Applied Through Date on Lien Maturity Notice.", txtIntDate.Text, FCConvert.ToString(rsLData.Get_Fields_Int32("LienRecordNumber")));
							// allow the interest date that was entered instead of just updating the InterestAppliedThroughDate
							rsLData.Set_Fields("InterestAppliedThroughDate", DateAndTime.DateValue(txtIntDate.Text));
						}
						else
						{
							rsLData.Set_Fields("InterestAppliedThroughDate", dtLMFDate);
						}
					}
					else
					{
						rsLData.Set_Fields("InterestAppliedThroughDate", dtLMFDate);
					}
					rsLData.Update();
					// this will edit the Lien Record
					// With rsData
					// .Edit
					// .Get_Fields("InterestAppliedThroughDate") = dtLMFDate
					// .Get_Fields("LienProcessStatus") = 6            'set the status to 'Lien Maturity Fees Applied'
					// .Update
					// End With
					rsTemp.Execute("UPDATE BillingMaster SET LienProcessStatus = 6 WHERE ID = " + FCConvert.ToString(rsData.Get_Fields_Int32("ID")), modExtraModules.strCLDatabase);
				}
			}
			return CreateFeeRecord;
		}

		private void vsDemand_MouseMove(object sender, DataGridViewCellMouseEventArgs e)
		{
			int lngMR;
			int lngMC;
			lngMR = vsDemand.MouseRow;
			lngMC = vsDemand.MouseCol;
			if (lngMR > 0 && lngMC > 0)
			{
				if (Conversion.Val(vsDemand.TextMatrix(lngMR, 6)) == -1)
				{
					// vsDemand.ToolTipText = "A Lien Record has already been created for this account.  This account is not eligible."
				}
				else
				{
					ToolTip1.SetToolTip(vsDemand, "");
				}
			}
		}

		private void vsDemand_RowColChange(object sender, EventArgs e)
		{
			switch (vsDemand.Col)
			{
				case 0:
					{
						vsDemand.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						break;
					}
				default:
					{
						vsDemand.Editable = FCGrid.EditableSettings.flexEDNone;
						break;
					}
			}
			//end switch
		}

		private int AddCHGINTForFee(ref double dblCurInt)
		{
			int AddCHGINTForFee = 0;
			// this will calculate the CHGINT line for the account and then return
			// the CHGINT number to store in the payment record
			// this will use rsData which is set to the correct record
			clsDRWrapper rsCHGINT = new clsDRWrapper();
			if (dblCurInt > 0)
			{
				// if there is interest due, then
				// this will actually create the payment line
				rsCHGINT.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = 0", modExtraModules.strCLDatabase);
				rsCHGINT.AddNew();
				// AddCHGINTForFee = .Get_Fields("ID")
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				rsCHGINT.Set_Fields("Account", rsData.Get_Fields("Account"));
				rsCHGINT.Set_Fields("Year", rsData.Get_Fields_Int32("BillingYear"));
				rsCHGINT.Set_Fields("BillKey", rsData.Get_Fields_Int32("LienRecordNumber"));
				rsCHGINT.Set_Fields("CHGINTNumber", 0);
				rsCHGINT.Set_Fields("CHGINTDate", rsLData.Get_Fields_DateTime("InterestAppliedThroughDate"));
				rsCHGINT.Set_Fields("ActualSystemDate", DateTime.Today);
				rsCHGINT.Set_Fields("EffectiveInterestDate", dtLMFDate);
				rsCHGINT.Set_Fields("RecordedTransactionDate", dtLMFDate);
				rsCHGINT.Set_Fields("Teller", "");
				rsCHGINT.Set_Fields("Reference", "CHGINT");
				rsCHGINT.Set_Fields("Period", "A");
				rsCHGINT.Set_Fields("Code", "I");
				rsCHGINT.Set_Fields("ReceiptNumber", 0);
				rsCHGINT.Set_Fields("Principal", 0);
				rsCHGINT.Set_Fields("PreLienInterest", 0);
				rsCHGINT.Set_Fields("CurrentInterest", modGlobal.Round(dblCurInt * -1, 2));
				rsCHGINT.Set_Fields("LienCost", 0);
				rsCHGINT.Set_Fields("TransNumber", 0);
				rsCHGINT.Set_Fields("PaidBy", 0);
				rsCHGINT.Set_Fields("Comments", "");
				rsCHGINT.Set_Fields("CashDrawer", "N");
				rsCHGINT.Set_Fields("GeneralLedger", "Y");
				rsCHGINT.Set_Fields("BudgetaryAccountNumber", "");
				rsCHGINT.Set_Fields("BillCode", "L");
				// rsPay.Get_Fields ("DailyCloseOut")
				rsCHGINT.Update();
				AddCHGINTForFee = FCConvert.ToInt32(rsCHGINT.Get_Fields_Int32("ID"));
			}
			return AddCHGINTForFee;
		}

		private int CalculateMortgageHolders()
		{
			int CalculateMortgageHolders = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will return the number of mortgage holders this account has
				clsDRWrapper rsMort = new clsDRWrapper();
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				rsMort.OpenRecordset("SELECT * FROM MortgageAssociation INNER JOIN MortgageHolders ON MortgageAssociation.MortgageHolderID = MortgageHolders.ID WHERE Account = " + FCConvert.ToString(rsData.Get_Fields("Account")) + " AND Module = 'RE'", "CentralData");
				if (rsMort.EndOfFile() != true)
				{
					CalculateMortgageHolders = rsMort.RecordCount();
				}
				else
				{
					CalculateMortgageHolders = 0;
				}
				return CalculateMortgageHolders;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				CalculateMortgageHolders = 0;
			}
			return CalculateMortgageHolders;
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFileSave_Click(sender, e);
		}

		private void cmdFileClear_Click(object sender, EventArgs e)
		{
			this.mnuFileClear_Click(sender, e);
		}

		private void cmdFileSelectAll_Click(object sender, EventArgs e)
		{
			this.mnuFileSelectAll_Click(sender, e);
		}
	}
}
