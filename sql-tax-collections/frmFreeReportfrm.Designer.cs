﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmFreeReport.
	/// </summary>
	partial class frmFreeReport : BaseForm
	{
		public fecherFoundation.FCComboBox cmbSortOrder;
		public fecherFoundation.FCComboBox cmbReport;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblPsuedoFooter;
		public fecherFoundation.FCPanel fraVariableSetup;
		public fecherFoundation.FCFrame fraSigFile;
		public fecherFoundation.FCCheckBox chkUseSig;
		public fecherFoundation.FCPictureBox imgSig;
		public fecherFoundation.FCFrame fraRecommitment;
		public fecherFoundation.FCPanel fraRecommitmentInfo;
		public fecherFoundation.FCGrid vsRecommitment;
		public fecherFoundation.FCCheckBox chkRecommitment;
		public fecherFoundation.FCGrid vsVariableSetup;
		public fecherFoundation.FCLabel lblInstructions;
		public fecherFoundation.FCPanel fraSummary;
		public fecherFoundation.FCGrid vsReturnInfo;
		public fecherFoundation.FCGrid vsSummary;
		public fecherFoundation.FCLabel lblTitle;
		public fecherFoundation.FCPanel fraEdit;
		public fecherFoundation.FCRichTextBox rtbData;
		public fecherFoundation.FCFrame fraSave;
		public fecherFoundation.FCComboBox cboSavedReport;
		public fecherFoundation.FCButton cmdAdd;
		public Wisej.Web.DataGridView lstVariable;
		public fecherFoundation.FCButton cmdAddVariable;
		public fecherFoundation.FCLabel lblPsuedoFooter_1;
		public fecherFoundation.FCLabel lblPsuedoFooter_2;
		public fecherFoundation.FCLabel lblPsuedoFooter_0;
		public Wisej.Web.TextBox lblPseudoHeader;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFreeReport));
            Wisej.Web.JavaScript.ClientEvent clientEvent1 = new Wisej.Web.JavaScript.ClientEvent();
            this.cmbSortOrder = new fecherFoundation.FCComboBox();
            this.cmbReport = new fecherFoundation.FCComboBox();
            this.fraVariableSetup = new fecherFoundation.FCPanel();
            this.lblSortOrder = new fecherFoundation.FCLabel();
            this.fraSigFile = new fecherFoundation.FCFrame();
            this.chkUseSig = new fecherFoundation.FCCheckBox();
            this.imgSig = new fecherFoundation.FCPictureBox();
            this.fraRecommitment = new fecherFoundation.FCFrame();
            this.fraRecommitmentInfo = new fecherFoundation.FCPanel();
            this.vsRecommitment = new fecherFoundation.FCGrid();
            this.chkRecommitment = new fecherFoundation.FCCheckBox();
            this.lblInstructions = new fecherFoundation.FCLabel();
            this.vsVariableSetup = new fecherFoundation.FCGrid();
            this.fraSave = new fecherFoundation.FCFrame();
            this.cboSavedReport = new fecherFoundation.FCComboBox();
            this.cmdAdd = new fecherFoundation.FCButton();
            this.fraSummary = new fecherFoundation.FCPanel();
            this.vsReturnInfo = new fecherFoundation.FCGrid();
            this.lblTitle = new fecherFoundation.FCLabel();
            this.vsSummary = new fecherFoundation.FCGrid();
            this.fraEdit = new fecherFoundation.FCPanel();
            this.lstVariable = new Wisej.Web.DataGridView();
            this.Column0 = new Wisej.Web.DataGridViewTextBoxColumn();
            this.Column1 = new Wisej.Web.DataGridViewTextBoxColumn();
            this.rtbData = new fecherFoundation.FCRichTextBox();
            this.cmdAddVariable = new fecherFoundation.FCButton();
            this.lblPsuedoFooter_1 = new fecherFoundation.FCLabel();
            this.lblPsuedoFooter_2 = new fecherFoundation.FCLabel();
            this.lblPsuedoFooter_0 = new fecherFoundation.FCLabel();
            this.lblPseudoHeader = new Wisej.Web.TextBox();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.btnProcess = new fecherFoundation.FCButton();
            this.cmdFileExclusion = new fecherFoundation.FCButton();
            this.cmdFilePrintCode = new fecherFoundation.FCButton();
            this.cmdFileRefresh = new fecherFoundation.FCButton();
            this.cmdFileChangeAct = new fecherFoundation.FCButton();
            this.javaScript1 = new Wisej.Web.JavaScript(this.components);
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraVariableSetup)).BeginInit();
            this.fraVariableSetup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSigFile)).BeginInit();
            this.fraSigFile.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseSig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraRecommitment)).BeginInit();
            this.fraRecommitment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraRecommitmentInfo)).BeginInit();
            this.fraRecommitmentInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsRecommitment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRecommitment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsVariableSetup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSave)).BeginInit();
            this.fraSave.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSummary)).BeginInit();
            this.fraSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsReturnInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraEdit)).BeginInit();
            this.fraEdit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lstVariable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtbData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddVariable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileExclusion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFilePrintCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileRefresh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileChangeAct)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 744);
            this.BottomPanel.Size = new System.Drawing.Size(1028, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraSummary);
            this.ClientArea.Controls.Add(this.fraVariableSetup);
            this.ClientArea.Controls.Add(this.fraEdit);
            this.ClientArea.Location = new System.Drawing.Point(0, 74);
            this.ClientArea.Size = new System.Drawing.Size(1048, 794);
            this.ClientArea.Controls.SetChildIndex(this.fraEdit, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraVariableSetup, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraSummary, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdFileChangeAct);
            this.TopPanel.Controls.Add(this.cmdFileRefresh);
            this.TopPanel.Controls.Add(this.cmdFilePrintCode);
            this.TopPanel.Controls.Add(this.cmdFileExclusion);
            this.TopPanel.Size = new System.Drawing.Size(1048, 74);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileExclusion, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFilePrintCode, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileRefresh, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileChangeAct, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(209, 28);
            this.HeaderText.Text = "Edit Custom Notice";
            // 
            // cmbSortOrder
            // 
            this.cmbSortOrder.Items.AddRange(new object[] {
            "Account",
            "Name"});
            this.cmbSortOrder.Location = new System.Drawing.Point(140, 489);
            this.cmbSortOrder.Name = "cmbSortOrder";
            this.cmbSortOrder.Size = new System.Drawing.Size(121, 40);
            this.cmbSortOrder.TabIndex = 5;
            // 
            // cmbReport
            // 
            this.cmbReport.Items.AddRange(new object[] {
            "Edit or Create New Report",
            "Show Saved Report",
            "Delete Saved Report"});
            this.cmbReport.Location = new System.Drawing.Point(19, 28);
            this.cmbReport.Name = "cmbReport";
            this.cmbReport.Size = new System.Drawing.Size(256, 40);
            this.cmbReport.TabIndex = 1;
            this.cmbReport.Text = "Edit or Create New Report";
            this.cmbReport.SelectedIndexChanged += new System.EventHandler(this.optReport_Click);
            // 
            // fraVariableSetup
            // 
            this.fraVariableSetup.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraVariableSetup.Controls.Add(this.lblSortOrder);
            this.fraVariableSetup.Controls.Add(this.cmbSortOrder);
            this.fraVariableSetup.Controls.Add(this.fraSigFile);
            this.fraVariableSetup.Controls.Add(this.fraRecommitment);
            this.fraVariableSetup.Controls.Add(this.lblInstructions);
            this.fraVariableSetup.Controls.Add(this.vsVariableSetup);
            this.fraVariableSetup.Name = "fraVariableSetup";
            this.fraVariableSetup.Size = new System.Drawing.Size(939, 744);
            this.fraVariableSetup.TabIndex = 1;
            this.fraVariableSetup.Visible = false;
            // 
            // lblSortOrder
            // 
            this.lblSortOrder.AutoSize = true;
            this.lblSortOrder.Location = new System.Drawing.Point(30, 502);
            this.lblSortOrder.Name = "lblSortOrder";
            this.lblSortOrder.Size = new System.Drawing.Size(91, 15);
            this.lblSortOrder.TabIndex = 4;
            this.lblSortOrder.Text = "SORT ORDER";
            this.lblSortOrder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fraSigFile
            // 
            this.fraSigFile.Controls.Add(this.chkUseSig);
            this.fraSigFile.Controls.Add(this.imgSig);
            this.fraSigFile.Location = new System.Drawing.Point(562, 549);
            this.fraSigFile.Name = "fraSigFile";
            this.fraSigFile.Size = new System.Drawing.Size(227, 88);
            this.fraSigFile.TabIndex = 6;
            this.fraSigFile.Text = "Digital Signature";
            this.fraSigFile.UseMnemonic = false;
            // 
            // chkUseSig
            // 
            this.chkUseSig.Location = new System.Drawing.Point(20, 30);
            this.chkUseSig.Name = "chkUseSig";
            this.chkUseSig.Size = new System.Drawing.Size(154, 22);
            this.chkUseSig.TabIndex = 0;
            this.chkUseSig.Text = "Use Digital Signature";
            this.chkUseSig.Click += new System.EventHandler(this.chkUseSig_Click);
            // 
            // imgSig
            // 
            this.imgSig.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgSig.Image = ((System.Drawing.Image)(resources.GetObject("imgSig.Image")));
            this.imgSig.Location = new System.Drawing.Point(21, 71);
            this.imgSig.Name = "imgSig";
            this.imgSig.Size = new System.Drawing.Size(175, 85);
            this.imgSig.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.ToolTip1.SetToolTip(this.imgSig, "This may be a distorted view.");
            // 
            // fraRecommitment
            // 
            this.fraRecommitment.Controls.Add(this.fraRecommitmentInfo);
            this.fraRecommitment.Controls.Add(this.chkRecommitment);
            this.fraRecommitment.Location = new System.Drawing.Point(30, 549);
            this.fraRecommitment.Name = "fraRecommitment";
            this.fraRecommitment.Size = new System.Drawing.Size(514, 188);
            this.fraRecommitment.TabIndex = 2;
            this.fraRecommitment.Text = "Recommitment Information";
            this.fraRecommitment.UseMnemonic = false;
            // 
            // fraRecommitmentInfo
            // 
            this.fraRecommitmentInfo.Controls.Add(this.vsRecommitment);
            this.fraRecommitmentInfo.Location = new System.Drawing.Point(5, 65);
            this.fraRecommitmentInfo.Name = "fraRecommitmentInfo";
            this.fraRecommitmentInfo.Size = new System.Drawing.Size(501, 118);
            this.fraRecommitmentInfo.TabIndex = 1;
            // 
            // vsRecommitment
            // 
            this.vsRecommitment.Cols = 4;
            this.vsRecommitment.ColumnHeadersVisible = false;
            this.vsRecommitment.FixedRows = 0;
            this.vsRecommitment.Location = new System.Drawing.Point(15, 25);
            this.vsRecommitment.Name = "vsRecommitment";
            this.vsRecommitment.Size = new System.Drawing.Size(476, 82);
            this.vsRecommitment.TabIndex = 0;
            this.vsRecommitment.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsRecommitment_AfterEdit);
            this.vsRecommitment.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsRecommitment_BeforeEdit);
            this.vsRecommitment.CurrentCellChanged += new System.EventHandler(this.vsRecommitment_RowColChange);
            this.vsRecommitment.Click += new System.EventHandler(this.vsRecommitment_Click);
            // 
            // chkRecommitment
            // 
            this.chkRecommitment.Location = new System.Drawing.Point(20, 30);
            this.chkRecommitment.Name = "chkRecommitment";
            this.chkRecommitment.Size = new System.Drawing.Size(153, 22);
            this.chkRecommitment.TabIndex = 2;
            this.chkRecommitment.Text = "This a recommitment";
            this.chkRecommitment.Click += new System.EventHandler(this.chkRecommitment_Click);
            // 
            // lblInstructions
            // 
            this.lblInstructions.Location = new System.Drawing.Point(30, 30);
            this.lblInstructions.Name = "lblInstructions";
            this.lblInstructions.Size = new System.Drawing.Size(575, 30);
            this.lblInstructions.TabIndex = 7;
            this.lblInstructions.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // vsVariableSetup
            // 
            this.vsVariableSetup.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsVariableSetup.Cols = 4;
            this.vsVariableSetup.ColumnHeadersVisible = false;
            this.vsVariableSetup.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsVariableSetup.FixedRows = 0;
            this.vsVariableSetup.Location = new System.Drawing.Point(30, 80);
            this.vsVariableSetup.Name = "vsVariableSetup";
            this.vsVariableSetup.ReadOnly = false;
            this.vsVariableSetup.Rows = 17;
            this.vsVariableSetup.Size = new System.Drawing.Size(885, 387);
            this.vsVariableSetup.TabIndex = 8;
            this.vsVariableSetup.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsVariableSetup_AfterEdit);
            this.vsVariableSetup.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.VsVariableSetup_CellFormatting);
            this.vsVariableSetup.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsVariableSetup_BeforeEdit);
            this.vsVariableSetup.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsVariableSetup_ValidateEdit);
            this.vsVariableSetup.CurrentCellChanged += new System.EventHandler(this.vsVariableSetup_RowColChange);
            // 
            // fraSave
            // 
            this.fraSave.Controls.Add(this.cboSavedReport);
            this.fraSave.Controls.Add(this.cmbReport);
            this.fraSave.Controls.Add(this.cmdAdd);
            this.fraSave.Location = new System.Drawing.Point(715, 370);
            this.fraSave.Name = "fraSave";
            this.fraSave.Size = new System.Drawing.Size(296, 201);
            this.fraSave.TabIndex = 3;
            this.fraSave.Text = "Report";
            this.fraSave.UseMnemonic = false;
            // 
            // cboSavedReport
            // 
            this.cboSavedReport.BackColor = System.Drawing.SystemColors.Window;
            this.cboSavedReport.Location = new System.Drawing.Point(19, 85);
            this.cboSavedReport.Name = "cboSavedReport";
            this.cboSavedReport.Size = new System.Drawing.Size(255, 40);
            this.cboSavedReport.TabIndex = 2;
            this.cboSavedReport.SelectedIndexChanged += new System.EventHandler(this.cboSavedReport_SelectedIndexChanged);
            // 
            // cmdAdd
            // 
            this.cmdAdd.AppearanceKey = "actionButton";
            this.cmdAdd.Location = new System.Drawing.Point(19, 144);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Size = new System.Drawing.Size(256, 40);
            this.cmdAdd.TabIndex = 3;
            this.cmdAdd.Text = "Add Custom report to Library";
            this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
            // 
            // fraSummary
            // 
            this.fraSummary.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraSummary.Controls.Add(this.vsReturnInfo);
            this.fraSummary.Controls.Add(this.lblTitle);
            this.fraSummary.Controls.Add(this.vsSummary);
            this.fraSummary.Name = "fraSummary";
            this.fraSummary.Size = new System.Drawing.Size(1028, 530);
            this.fraSummary.TabIndex = 1001;
            this.fraSummary.Text = "f";
            this.fraSummary.Visible = false;
            // 
            // vsReturnInfo
            // 
            this.vsReturnInfo.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsReturnInfo.Cols = 10;
            this.vsReturnInfo.Location = new System.Drawing.Point(30, 248);
            this.vsReturnInfo.Name = "vsReturnInfo";
            this.vsReturnInfo.Rows = 50;
            this.vsReturnInfo.Size = new System.Drawing.Size(971, 265);
            this.vsReturnInfo.TabIndex = 2;
            this.vsReturnInfo.Visible = false;
            // 
            // lblTitle
            // 
            this.lblTitle.Font = new System.Drawing.Font("Proxima Nova Regular", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblTitle.Location = new System.Drawing.Point(30, 30);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(300, 26);
            this.lblTitle.TabIndex = 3;
            this.lblTitle.Text = "ACCOUNT SUMMARY";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // vsSummary
            // 
            this.vsSummary.Cols = 4;
            this.vsSummary.ColumnHeadersVisible = false;
            this.vsSummary.FixedCols = 0;
            this.vsSummary.FixedRows = 0;
            this.vsSummary.Location = new System.Drawing.Point(30, 66);
            this.vsSummary.Name = "vsSummary";
            this.vsSummary.RowHeadersVisible = false;
            this.vsSummary.Rows = 5;
            this.vsSummary.Size = new System.Drawing.Size(771, 162);
            this.vsSummary.TabIndex = 1;
            this.vsSummary.Click += new System.EventHandler(this.vsSummary_Click);
            // 
            // fraEdit
            // 
            this.fraEdit.Controls.Add(this.lstVariable);
            this.fraEdit.Controls.Add(this.fraSave);
            this.fraEdit.Controls.Add(this.rtbData);
            this.fraEdit.Controls.Add(this.cmdAddVariable);
            this.fraEdit.Controls.Add(this.lblPsuedoFooter_1);
            this.fraEdit.Controls.Add(this.lblPsuedoFooter_2);
            this.fraEdit.Controls.Add(this.lblPsuedoFooter_0);
            this.fraEdit.Controls.Add(this.lblPseudoHeader);
            this.fraEdit.Name = "fraEdit";
            this.fraEdit.Size = new System.Drawing.Size(1021, 676);
            this.fraEdit.TabIndex = 2;
            this.fraEdit.Visible = false;
            // 
            // lstVariable
            // 
            this.lstVariable.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.lstVariable.BackColor = System.Drawing.SystemColors.Window;
            this.lstVariable.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.None;
            this.lstVariable.ColumnHeadersVisible = false;
            this.lstVariable.Columns.AddRange(new Wisej.Web.DataGridViewColumn[] {
            this.Column0,
            this.Column1});
            this.lstVariable.Location = new System.Drawing.Point(30, 370);
            this.lstVariable.MultiSelect = false;
            this.lstVariable.Name = "lstVariable";
            this.lstVariable.ReadOnly = true;
            this.lstVariable.RowHeadersVisible = false;
            this.lstVariable.RowTemplate.Height = 28;
            this.lstVariable.ShowColumnVisibilityMenu = false;
            this.lstVariable.Size = new System.Drawing.Size(666, 294);
            this.lstVariable.TabIndex = 6;
            this.lstVariable.DoubleClick += new System.EventHandler(this.lstVariable_DoubleClick);
            // 
            // Column0
            // 
            this.Column0.HeaderText = "Column0";
            this.Column0.Name = "Column0";
            this.Column0.ReadOnly = true;
            this.Column0.Width = 333;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Column1";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 300;
            // 
            // rtbData
            // 
            this.rtbData.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            clientEvent1.Event = "keydown";
            clientEvent1.JavaScript = resources.GetString("clientEvent1.JavaScript");
            this.javaScript1.GetJavaScriptEvents(this.rtbData).Add(clientEvent1);
            this.rtbData.Location = new System.Drawing.Point(30, 102);
            this.rtbData.Name = "rtbData";
            this.rtbData.Size = new System.Drawing.Size(928, 199);
            this.rtbData.TabIndex = 1;
            // 
            // cmdAddVariable
            // 
            this.cmdAddVariable.Location = new System.Drawing.Point(30, 324);
            this.cmdAddVariable.Name = "cmdAddVariable";
            this.cmdAddVariable.Size = new System.Drawing.Size(82, 25);
            this.cmdAddVariable.TabIndex = 5;
            this.cmdAddVariable.Text = "Add Variable";
            this.ToolTip1.SetToolTip(this.cmdAddVariable, "Select a variable and click this button to insert that field wherever the cursor " +
        "is below.");
            this.cmdAddVariable.Visible = false;
            this.cmdAddVariable.Click += new System.EventHandler(this.cmdAddVariable_Click);
            // 
            // lblPsuedoFooter_1
            // 
            this.lblPsuedoFooter_1.Location = new System.Drawing.Point(196, 153);
            this.lblPsuedoFooter_1.Name = "lblPsuedoFooter_1";
            this.lblPsuedoFooter_1.Size = new System.Drawing.Size(78, 101);
            this.lblPsuedoFooter_1.TabIndex = 3;
            // 
            // lblPsuedoFooter_2
            // 
            this.lblPsuedoFooter_2.Location = new System.Drawing.Point(336, 148);
            this.lblPsuedoFooter_2.Name = "lblPsuedoFooter_2";
            this.lblPsuedoFooter_2.Size = new System.Drawing.Size(252, 106);
            this.lblPsuedoFooter_2.TabIndex = 4;
            // 
            // lblPsuedoFooter_0
            // 
            this.lblPsuedoFooter_0.Location = new System.Drawing.Point(32, 153);
            this.lblPsuedoFooter_0.Name = "lblPsuedoFooter_0";
            this.lblPsuedoFooter_0.Size = new System.Drawing.Size(147, 101);
            this.lblPsuedoFooter_0.TabIndex = 2;
            // 
            // lblPseudoHeader
            // 
            this.lblPseudoHeader.BorderStyle = Wisej.Web.BorderStyle.None;
            this.lblPseudoHeader.Enabled = false;
            this.lblPseudoHeader.Font = new System.Drawing.Font("Courier New", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblPseudoHeader.ForeColor = System.Drawing.Color.Black;
            this.lblPseudoHeader.Location = new System.Drawing.Point(30, 30);
            this.lblPseudoHeader.Multiline = true;
            this.lblPseudoHeader.Name = "lblPseudoHeader";
            this.lblPseudoHeader.ReadOnly = true;
            this.lblPseudoHeader.Size = new System.Drawing.Size(1019, 51);
            this.lblPseudoHeader.TabIndex = 0;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Location = new System.Drawing.Point(476, 30);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcess.Size = new System.Drawing.Size(145, 48);
            this.btnProcess.TabIndex = 1;
            this.btnProcess.Text = "Save & Preview";
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // cmdFileExclusion
            // 
            this.cmdFileExclusion.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileExclusion.Location = new System.Drawing.Point(686, 29);
            this.cmdFileExclusion.Name = "cmdFileExclusion";
            this.cmdFileExclusion.Size = new System.Drawing.Size(148, 24);
            this.cmdFileExclusion.TabIndex = 52;
            this.cmdFileExclusion.Text = "Update Exclusion List";
            this.cmdFileExclusion.Click += new System.EventHandler(this.cmdFileExclusion_Click);
            // 
            // cmdFilePrintCode
            // 
            this.cmdFilePrintCode.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFilePrintCode.Location = new System.Drawing.Point(593, 29);
            this.cmdFilePrintCode.Name = "cmdFilePrintCode";
            this.cmdFilePrintCode.Shortcut = Wisej.Web.Shortcut.F4;
            this.cmdFilePrintCode.Size = new System.Drawing.Size(87, 24);
            this.cmdFilePrintCode.TabIndex = 53;
            this.cmdFilePrintCode.Text = "Print Code";
            this.cmdFilePrintCode.Visible = false;
            this.cmdFilePrintCode.Click += new System.EventHandler(this.cmdFilePrintCode_Click);
            // 
            // cmdFileRefresh
            // 
            this.cmdFileRefresh.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileRefresh.Location = new System.Drawing.Point(840, 29);
            this.cmdFileRefresh.Name = "cmdFileRefresh";
            this.cmdFileRefresh.Shortcut = Wisej.Web.Shortcut.F5;
            this.cmdFileRefresh.Size = new System.Drawing.Size(97, 24);
            this.cmdFileRefresh.TabIndex = 54;
            this.cmdFileRefresh.Text = "Show Codes";
            this.cmdFileRefresh.Click += new System.EventHandler(this.cmdFileRefresh_Click);
            // 
            // cmdFileChangeAct
            // 
            this.cmdFileChangeAct.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileChangeAct.Location = new System.Drawing.Point(943, 29);
            this.cmdFileChangeAct.Name = "cmdFileChangeAct";
            this.cmdFileChangeAct.Shortcut = Wisej.Web.Shortcut.F7;
            this.cmdFileChangeAct.Size = new System.Drawing.Size(78, 24);
            this.cmdFileChangeAct.TabIndex = 55;
            this.cmdFileChangeAct.Text = "Show Edit";
            this.cmdFileChangeAct.Click += new System.EventHandler(this.cmdFileChangeAct_Click);
            // 
            // frmFreeReport
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1048, 868);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmFreeReport";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Edit Custom Notice";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmFreeReport_Load);
            this.Resize += new System.EventHandler(this.frmFreeReport_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmFreeReport_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmFreeReport_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraVariableSetup)).EndInit();
            this.fraVariableSetup.ResumeLayout(false);
            this.fraVariableSetup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSigFile)).EndInit();
            this.fraSigFile.ResumeLayout(false);
            this.fraSigFile.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseSig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraRecommitment)).EndInit();
            this.fraRecommitment.ResumeLayout(false);
            this.fraRecommitment.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraRecommitmentInfo)).EndInit();
            this.fraRecommitmentInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsRecommitment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRecommitment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsVariableSetup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSave)).EndInit();
            this.fraSave.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSummary)).EndInit();
            this.fraSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsReturnInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraEdit)).EndInit();
            this.fraEdit.ResumeLayout(false);
            this.fraEdit.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lstVariable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtbData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddVariable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileExclusion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFilePrintCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileRefresh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileChangeAct)).EndInit();
            this.ResumeLayout(false);

		}

        
        #endregion

        private FCLabel lblSortOrder;
		private System.ComponentModel.IContainer components;
		private FCButton btnProcess;
		public FCButton cmdFileExclusion;
		public FCButton cmdFilePrintCode;
		public FCButton cmdFileRefresh;
		public FCButton cmdFileChangeAct;
		private DataGridViewTextBoxColumn Column0;
		private DataGridViewTextBoxColumn Column1;
		private JavaScript javaScript1;
	}
}
