﻿using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls.Expressions;
using SharedApplication;
using SharedApplication.Enums;
using SharedApplication.TaxCollections.Models;

namespace TWCL0000
{

	public partial class frmPmtActivityReport : BaseForm
	{
        private PmtActivityViewModel viewModel;
		private frmPmtActivityReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
            
		}

        public frmPmtActivityReport(PmtActivityViewModel model)
        {
            viewModel = model;
            InitializeComponent();
            InitializeComponentEx();
            AddEventHandlers();

            // CL-29: don't allow input if PP
            cmbTaxBillType.Enabled = modStatusPayments.Statics.boolRE;
            label6.Enabled = modStatusPayments.Statics.boolRE;

        }

        private void AddEventHandlers()
        {
            viewModel.SortOptionsChanged += ViewModel_SortOptionsChanged;
            viewModel.FilterChanged += ViewModel_FilterChanged;
            viewModel.SelectedOptionsChanged += ViewModel_SelectedOptionsChanged;
        }

        private void ViewModel_SelectedOptionsChanged(object sender, EventArgs e)
        {
            ShowOptions();
        }

        private void ViewModel_FilterChanged(object sender, EventArgs e)
        {
            ShowFilter();
        }

        private void ShowFilter()
        {
            
        }

        private void ViewModel_SortOptionsChanged(object sender, EventArgs e)
        {
            LoadSortList();
        }

        private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPmtActivityReport InstancePtr
		{
			get
			{
				return (frmPmtActivityReport)Sys.GetInstance(typeof(frmPmtActivityReport));
			}
		}

		protected frmPmtActivityReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		bool boolLoaded;
		
        private void cmdClear_Click(object sender, System.EventArgs e)
        {
            ClearFilters();
        }

        private void cmdExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void cmdExit_Click()
		{
			cmdExit_Click(cmdExit, new System.EventArgs());
		}

        private void cmdPrint_Click(object sender, System.EventArgs e)
        {
            //// THIS ROUTINE WORKS TO PRINT OUT THE REPORT
            try
            {
                if (!ValidateCriteriaControls())
                {
                    return;
                }

                var reportFilter = GetFilter();
                if (!ValidateFilter(reportFilter))
                {
                    return;
                }

                viewModel.SetFilter(reportFilter);
                int intCT;

                var reportOption = GetReportOption();
                SetSortOptions();

                viewModel.SetOption(reportOption);

                var paymentActivity = new rptPmtActivity();
                var reportViewer = new frmReportViewer();
                var reportConfiguration = viewModel.GetReportConfiguration();
                paymentActivity.Init(ref reportConfiguration);
                reportViewer.Init(paymentActivity);

                return;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
                FCMessageBox.Show(
                    "ERROR #:" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message,
                    MsgBoxStyle.Critical, "Print Status Lists ERROR");
            }
        }
 

        private void SetSortOptions()
        {
            for (var index = 0; index < lstSort.ListCount; index ++)
            {
                var sortItem = viewModel.SortOptions.FirstOrDefault(s => s.Item.ID == lstSort.ItemData(index));
                if (sortItem != null)
                {
                    sortItem.IsSelected = lstSort.Selected(index);
                    sortItem.Item.OrderNumber = index + 1;
                }
            }
        }

        private PmtActivityReportOption GetReportOption()
        {
            var reportOption = new PmtActivityReportOption();           
            reportOption.IncludePayments = chkIncludePayment.CheckState == Wisej.Web.CheckState.Checked;
            reportOption.IncludePrePayments = chkIncludePrePayment.CheckState == Wisej.Web.CheckState.Checked;
            reportOption.IncludeCorrections = chkIncludeCorrection.CheckState == Wisej.Web.CheckState.Checked;
            reportOption.IncludeAbatements = chkIncludeAbatement.CheckState == Wisej.Web.CheckState.Checked;
            reportOption.IncludeDiscounts = chkIncludeDiscount.CheckState == Wisej.Web.CheckState.Checked;
            reportOption.IncludeTaxClub = chkIncludeTaxClub.CheckState == Wisej.Web.CheckState.Checked;
            reportOption.IncludeRefund = chkIncludeRefund.CheckState == Wisej.Web.CheckState.Checked;
            reportOption.IncludeNonBudgetary = chkIncludeNonBudgetary.CheckState == Wisej.Web.CheckState.Checked;
            reportOption.IncludeInterest= chkIncludeInterest.CheckState == Wisej.Web.CheckState.Checked;
            reportOption.IncludeDemandFee = chkIncludeDemandFee.CheckState == Wisej.Web.CheckState.Checked;
            reportOption.IncludeMaturityFee = chkIncludeMaturityFee.CheckState == Wisej.Web.CheckState.Checked;
            return reportOption;
        }

        private bool ValidateFilter(PmtActivityReportFilter reportFilter)
        {
            if (reportFilter.AccountMax.HasValue && reportFilter.AccountMin.HasValue)
            {
                if (reportFilter.AccountMax.Value < reportFilter.AccountMin.Value)
                {
                    MessageBox.Show("That maximum account cannot be less than the minimum account", "Invalid Range", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    return false;
                }
            }

            if (reportFilter.TaxYearMax.HasValue && reportFilter.TaxYearMin.HasValue &&
                reportFilter.TaxYearMax < reportFilter.TaxYearMin)
            {
                MessageBox.Show("The tax year range is invalid", "Invalid Range", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return false;
            }
            
            if (reportFilter.PaymentDateMax.HasValue && reportFilter.PaymentDateMin.HasValue &&
                reportFilter.PaymentDateMax.Value.CompareTo(
                    reportFilter.PaymentDateMin) < 0)
            {
                MessageBox.Show("The end payment date cannot be before the start payment date", "Invalid Range", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return false;
            }
            
            return true;
        }

        private PmtActivityReportFilter GetFilter()
        {
            var reportFilter = new PmtActivityReportFilter();


            if (!String.IsNullOrWhiteSpace(txtAccountStart.Text))
            {
                reportFilter.AccountMin = Convert.ToInt32(txtAccountStart.Text);
            }

            if (!String.IsNullOrWhiteSpace(txtAccountEnd.Text))
            {
                reportFilter.AccountMax = Convert.ToInt32(txtAccountEnd.Text);
            }

            if (cmbTaxYearStart.SelectedIndex >= 0)
            {
                reportFilter.TaxYearMin =
                    Convert.ToInt32(cmbTaxYearStart.Items[cmbTaxYearStart.SelectedIndex].ToString().Replace("-", ""));
            }

            if (cmbTaxYearEnd.SelectedIndex >= 0)
            {
                reportFilter.TaxYearMax =
                    Convert.ToInt32(cmbTaxYearEnd.Items[cmbTaxYearEnd.SelectedIndex].ToString().Replace("-", ""));
            }

            reportFilter.PaymentDateMin = dtpShowPaymentStart.NullableValue;
            reportFilter.PaymentDateMax = dtpShowPaymentEnd.NullableValue;
            reportFilter.Reference = txtReference.Text;

            if (cmbTaxBillType.SelectedIndex >= 0)
            {
                DescriptionIDPair idPair = (DescriptionIDPair)cmbTaxBillType.SelectedItem;
                switch (idPair.ID)
                {
                    case 0:
                        reportFilter.TaxBillStatus = "R";
                        break;
                    case 1:
                        reportFilter.TaxBillStatus = "L";
                        break;                    
                }
            }
            if (cmbTownCode.SelectedIndex >= 0)
            {
                DescriptionIDPair idPair = (DescriptionIDPair)cmbTownCode.SelectedItem;
                int dashExists = idPair.Description.IndexOf("-", StringComparison.Ordinal);
                if (dashExists > 1)
                {
                    reportFilter.TownCode = Convert.ToInt32(idPair.Description.Substring(0,dashExists -1));
                }                            
            }

            return reportFilter;
        }
        
		public void cmdPrint_Click()
		{
			cmdPrint_Click(cmdPrint, new System.EventArgs());
		}
		
		private void frmPmtActivityReport_Activated(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
			}
			else
			{
				// put the combobox in the first question so that the user can choose
				App.DoEvents();                    
                ShowWhereQuestions();
				boolLoaded = true;
			}
		}

		private void frmPmtActivityReport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData)) / 0x10000;
			if (KeyCode == Keys.Escape)
			{
				Close();
			}
		}

		private void frmPmtActivityReport_Load(object sender, System.EventArgs e)
		{
			this.Text = "Payment Activity Report";
			this.HeaderText.Text = this.Text;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);		
			SetupControls();

            txtAccountStart.Focus();
        }

        private void SetupControls()
        {
            LoadSortList();
            FillTaxYearCombos();
            FillBillStatus();
            TownPanel.Visible = false;
            if (modGlobal.Statics.gboolMultipleTowns)
            {
                FillTownsCombo();
                TownPanel.Visible = true;
            }
            ShowOptions();                        
        }

        private void FillTownsCombo()
        {
            var townCodes = viewModel.Towns;
            foreach (var townCode in townCodes)
            {
                cmbTownCode.Items.Add(townCode);
            }
        }

        private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}
				
        private void mnuClear_Click(object sender, System.EventArgs e)
        {
            cmdClear_Click(sender,e);
        }

        private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			cmdPrint_Click();
		}		
        
		private void ShowWhereQuestions()
		{


		}

		public void LoadSortList()
		{
            lstSort.Clear();
            foreach (var selectableDescriptionIDPair in viewModel.SortOptions)
            {
                var descriptionIDPair = selectableDescriptionIDPair.Item;
                AddItemToSortList(descriptionIDPair.Description, descriptionIDPair.ID,selectableDescriptionIDPair.IsSelected);
            }
		}

        private void AddItemToSortList(string itemText, int itemData, bool selected)
        {
            lstSort.AddItem(itemText);
            var index = lstSort.Items.Count - 1;
            lstSort.ItemData(index, itemData);
            lstSort.Items[index].Selected = selected;
            lstSort.SetSelected(index, selected);
        }

	    private bool ValidateCriteriaControls()
          {
              var numericRegEx = new Regex(@"^([0-9]+)?(\.[0-9][0-9]?)?$");
              var integerRegEx = new Regex("^([0-9]+)?$");

              if (txtAccountStart.Text != "" && !integerRegEx.IsMatch(txtAccountStart.Text))
              {
                  MessageBox.Show("Invalid minimum account", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                  return false;
              }

              if (txtAccountEnd.Text != "" && !integerRegEx.IsMatch(txtAccountEnd.Text))
              {
                  MessageBox.Show("Invalid maximum account", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                  return false;
              }

              return true;
          }

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuPrint_Click(sender, e);
		}


        private void ClearFilters()
        {
            // 
            txtAccountStart.Text = string.Empty;
            txtAccountEnd.Text = string.Empty;
            cmbTaxBillType.SelectedIndex = -1;
            cmbTaxYearStart.SelectedIndex = -1;
            cmbTaxYearEnd.SelectedIndex = -1;
            dtpShowPaymentStart.Text = string.Empty;
            dtpShowPaymentEnd.Text = string.Empty;
            txtReference.Text = string.Empty;
            cmbTownCode.SelectedIndex = -1;
            
            txtAccountStart.Focus();

        }
                      
        private void FillTaxYearCombos()
        {
            var taxYears = viewModel.TaxYears;
            foreach(string taxYear in taxYears)
            {
                cmbTaxYearStart.Items.Add(taxYear);                
                cmbTaxYearEnd.Items.Add(taxYear);
            }
        }
    
        private void FillBillStatus()
        {
            var billTypes = viewModel.BillTypes;
            foreach (var billType in billTypes)
            {
                cmbTaxBillType.Items.Add(billType);
            }
        }

        private void ShowOptions()
        {
            chkIncludePayment.Value = FCCheckBox.ValueSettings.Checked;
            chkIncludePrePayment.Value = FCCheckBox.ValueSettings.Checked;
            chkIncludeCorrection.Value = FCCheckBox.ValueSettings.Checked;
            chkIncludeDiscount.Value = FCCheckBox.ValueSettings.Checked;
            chkIncludeAbatement.Value = FCCheckBox.ValueSettings.Checked;
            chkIncludeTaxClub.Value = FCCheckBox.ValueSettings.Checked;
            chkIncludeRefund.Value = FCCheckBox.ValueSettings.Checked;
            chkIncludeNonBudgetary.Value = FCCheckBox.ValueSettings.Checked;

        }

        private void chkEpmtOnly_CheckedChanged(object sender, EventArgs e)
        {
	        if (chkEpmtOnly.CheckState == Wisej.Web.CheckState.Checked)
	        {
		        txtReference.Text =  "EPYMT";
		        txtReference.Enabled = false;
	        }
	        else
	        {
		        txtReference.Text = "";
		        txtReference.Enabled = true;
            }
        }
    }
}
