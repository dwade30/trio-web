﻿using Global;
using SharedApplication;
using SharedApplication.CentralData.Extensions;
using SharedApplication.TaxCollections.Interfaces;
using SharedDataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using SharedApplication.TaxCollections.Enums;


namespace TWCL0000
{
    public enum TaxBillStatus
    {
        Regular = 0,
        Lien = 1
    }

    public enum PmtActivitySortType
    {
        Account = 0,
        TransactionDate = 1
    }

    public class PmtActivityReportOption
    {
        public bool IncludePayments { get; set; } = false;
        public bool IncludePrePayments { get; set; } = false;
        public bool IncludeCorrections { get; set; } = false;
        public bool IncludeAbatements { get; set; } = false;
        public bool IncludeDiscounts { get; set; } = false;
        public bool IncludeTaxClub { get; set; } = false;
        public bool IncludeRefund { get; set; } = false;
        public bool IncludeNonBudgetary { get; set; } = false;
        public bool IncludeInterest { get; set; } = false;
        public bool IncludeDemandFee { get; set; } = false;
        public bool IncludeMaturityFee { get; set; } = false;

        public string PaymentTypes()
        {
            string typeList = "";
            string separator = "";
            if (IncludePayments)
            {
                typeList = "'P'";
                separator = ",";
            }
            if (IncludePrePayments)
            {
                typeList += separator + "'Y'";
                separator = ",";
            }
            if (IncludeCorrections)
            {
                typeList += separator + "'C','X','S'";
                separator = ",";
            }
            if (IncludeAbatements)
            {
                typeList += separator + "'A','R'";
                separator = ",";
            }
            if (IncludeDiscounts)
            {
                typeList += separator + "'D'";
                separator = ",";
            }
            if (IncludeTaxClub)
            {
                typeList += separator + "'U'";
                separator = ",";
            }
            if (IncludeRefund)
            {
                typeList += separator + "'F'";
                separator = ",";
            }
            if (IncludeNonBudgetary)
            {
                typeList += separator + "'N'";
                separator = ",";
            }
            if (IncludeInterest)
            {
                typeList += separator + "'I'";
                separator = ",";
            }
            if (IncludeDemandFee)
            {
                typeList += separator + "'3'";
                separator = ",";
            }
            if (IncludeMaturityFee)
            {
                typeList += separator + "'L'";
            }

            return typeList;
        }
    }

    public class PmtActivityReportConfiguration
    {
        public PmtActivityReportOption Options { get; private set; }
        public PmtActivityReportFilter Filter { get; private set; }
        public string WhereClause { get; set; } = "";
        public string TownBillJoinClause { get; set; } = "";
        public string TownLienJoinClause { get; set; } = "";
        public string SortClause { get; set; } = "";
        public string SelectClause { get; set; } = "";
        public string SortListing { get; set; } = "";
        public string SQLStatement { get; set; } = "";
        public PmtActivityReportConfiguration(PmtActivityReportOption reportOptions, PmtActivityReportFilter filter)
        {
            Options = reportOptions.GetCopy();
            Filter = filter.GetCopy();
        }
    }

    public class PmtActivityReportFilter
    {
        public int? TaxYearMin { get; set; } = null;
        public int? TaxYearMax { get; set; } = null;
        public int? AccountMin { get; set; } = null;
        public int? AccountMax { get; set; } = null;
        public DateTime? PaymentDateMin { get; set; } = null;
        public DateTime? PaymentDateMax { get; set; } = null;
        public string TaxBillStatus { get; set; } = null;
        public string Reference { get; set; } = null;
        public bool AccountRangeUsed()
        {
            return AccountMin.HasValue || AccountMax.HasValue;
        }

        public bool TaxYearRangeUsed()
        {
            return TaxYearMin.HasValue || TaxYearMax.HasValue;
        }

        public bool PaymentDateRangeUsed()
        {
            return PaymentDateMin.HasValue || PaymentDateMax.HasValue;
        }

        public int TownCode { get; set; } = 0;
    }

    public static class PaymentActivityReportExtensions
    {
        public static PmtActivityReportFilter GetCopy(this PmtActivityReportFilter filter)
        {
            return new PmtActivityReportFilter()
            {
                AccountMax = filter.AccountMax,
                AccountMin = filter.AccountMin,
                TaxBillStatus = filter.TaxBillStatus,
                TaxYearMax = filter.TaxYearMax,
                TaxYearMin = filter.TaxYearMin,
                PaymentDateMax = filter.PaymentDateMax,
                PaymentDateMin = filter.PaymentDateMin,
                Reference = filter.Reference,
                TownCode = filter.TownCode
            };
        }

        public static PmtActivityReportOption GetCopy(this PmtActivityReportOption option)
        {
            return new PmtActivityReportOption()
            {
                IncludePayments = option.IncludePayments,
                IncludePrePayments = option.IncludePrePayments,
                IncludeCorrections = option.IncludeCorrections,
                IncludeAbatements = option.IncludeAbatements,
                IncludeDiscounts = option.IncludeDiscounts,
                IncludeTaxClub = option.IncludeTaxClub,
                IncludeRefund = option.IncludeRefund,
                IncludeNonBudgetary = option.IncludeNonBudgetary,
                IncludeInterest = option.IncludeInterest,
                IncludeDemandFee = option.IncludeDemandFee,
                IncludeMaturityFee = option.IncludeMaturityFee,
            };
        }
    }

    public class PmtActivityViewModel
    {

        public event EventHandler FilterChanged;
        public event EventHandler SortOptionsChanged;
        public event EventHandler SelectedOptionsChanged;
        public IEnumerable<string> TaxYears { get; private set; } = new List<string>();

        public IEnumerable<SelectableItem<OrderedDescriptionIDPair>> SortOptions { get; protected set; } =
            new List<SelectableItem<OrderedDescriptionIDPair>>();

        public IEnumerable<DescriptionIDPair> BillTypes { get; private set; } = new List<DescriptionIDPair>();
        public IEnumerable<DescriptionIDPair> Towns { get; private set; } = new List<DescriptionIDPair>();
        public PmtActivityReportOption ReportOption
        {
            get { return reportOption; }
        }

        private TrioContextFactory contextFactory;

        public PmtActivityViewModel(PropertyTaxBillType billType,
            IBillingMasterRepository billsRepository, TrioContextFactory trioContextFactory)
        {
            contextFactory = trioContextFactory;
            billingType = billType;
            billingMasterRepository = billsRepository;
            GetTaxYears();
            FillTaxBillTypes();
            FillSortOptions();
            if (modGlobal.Statics.gboolMultipleTowns)
            {
                GetTowns();
            }
        }

        private PmtActivityReportFilter reportFilter = new PmtActivityReportFilter();

        private PmtActivityReportOption reportOption = new PmtActivityReportOption();

        private IBillingMasterRepository billingMasterRepository;
        private PropertyTaxBillType billingType = PropertyTaxBillType.Real;

        public void SetFilter(PmtActivityReportFilter filter)
        {
            reportFilter = filter.GetCopy();
        }

        public PmtActivityReportFilter GetFilter()
        {
            return reportFilter.GetCopy();
        }

        public void ClearFilter()
        {
            reportFilter = new PmtActivityReportFilter();
            OnFilterChanged(new EventArgs() { });
        }

        public void SetOption(PmtActivityReportOption option)
        {
            reportOption = option.GetCopy();
        }

        protected void OnFilterChanged(EventArgs e)
        {
            EventHandler handler = FilterChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        protected void OnSortOptionsChanged(EventArgs e)
        {
            EventHandler handler = SortOptionsChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        protected void OnSelectedOptionsChanged(EventArgs e)
        {
            EventHandler handler = SelectedOptionsChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void GetTaxYears()
        {
            var taxYears = billingMasterRepository.GetTaxYears();
            TaxYears = taxYears.Select(value =>
                value.ToString().Substring(0, 4)).Distinct();
        }

        private void GetTowns()
        {
            var cdContext = contextFactory.GetCentralDataContext();
            var townRepository = new Repository<SharedApplication.CentralData.Models.Region>(cdContext);
            Towns = townRepository.GetAll().OrderBy(t => t.TownNumber).MapToDescriptionIDPairsShowingTownNumberAndTownName().ToList();
        }

        private void FillSortOptions()
        {
            SortOptions = new List<SelectableItem<OrderedDescriptionIDPair>>()
            {
                new SelectableItem<OrderedDescriptionIDPair>()
                {
                    IsSelected = false,
                    Item = new OrderedDescriptionIDPair()
                        {Description = "ACCOUNT", ID = (int) PmtActivitySortType.Account, OrderNumber = 1}

                },
                new SelectableItem<OrderedDescriptionIDPair>()
                {
                    IsSelected = true,
                    Item = new OrderedDescriptionIDPair() {Description = "TRANSACTION DATE", ID = (int) PmtActivitySortType.TransactionDate, OrderNumber = 2}
                }

            };
        }

        public PmtActivityReportConfiguration GetReportConfiguration()
        {
            var reportConfiguration = new PmtActivityReportConfiguration(reportOption, reportFilter);
            reportConfiguration.SelectClause = CreateSelectStatement();
            reportConfiguration.TownBillJoinClause = CreateTownBillJoinClause();
            reportConfiguration.TownLienJoinClause = CreateTownLienJoinClause();
            reportConfiguration.WhereClause = CreateWhereClause(reportFilter, reportOption);
            string sortOrder = CreateSortParameter();
            reportConfiguration.SortListing = "Order by: " + MakeFieldNameReadable(sortOrder);
            reportConfiguration.SortClause = " Order by " + sortOrder;
            switch (reportFilter.TaxBillStatus)
            {
                case "R":
                    reportConfiguration.SQLStatement =
                        reportConfiguration.SelectClause + reportConfiguration.TownBillJoinClause + reportConfiguration.WhereClause + reportConfiguration.SortClause;
                    break;
                case "L":
                    reportConfiguration.SQLStatement =
                        reportConfiguration.SelectClause + reportConfiguration.TownLienJoinClause + reportConfiguration.WhereClause + reportConfiguration.SortClause;
                    break;
                default:
                    reportConfiguration.SQLStatement =
                        reportConfiguration.SelectClause + reportConfiguration.TownBillJoinClause + reportConfiguration.WhereClause
                        + " Union " +
                        reportConfiguration.SelectClause + reportConfiguration.TownLienJoinClause + reportConfiguration.WhereClause + reportConfiguration.SortClause;
                    break;
            }
            return reportConfiguration;
        }

        private string MakeFieldNameReadable(string fromString)
        {
            switch (fromString.Trim())
            {
                case "":
                case "RecordedTransactionDate":
                    return "Transaction Date";

                default:
                    return fromString;
            }
        }

        private string CreateSelectStatement()
        {
            return
                "Select ID,RecordedTransactionDate,ActualSystemDate,Teller,Reference,Code,ReceiptNumber,Principal,PreLienInterest,CurrentInterest,LienCost,CashDrawer,GeneralLedger,BillCode,Account,Year FROM PaymentRec ";
        }

        private string CreateTownBillJoinClause()
        {
            return "INNER JOIN (Select ID as BillID, TranCode FROM BillingMaster) As qTmp On PaymentRec.BillKey = qTmp.BillID ";
        }

        private string CreateTownLienJoinClause()
        {
            return "INNER JOIN (Select BillingMaster.LienRecordNumber As BillID, TranCode FROM BillingMaster Inner Join LienRec On BillingMaster.LienRecordNumber = LienRec.ID) As qTmp On PaymentRec.BillKey = qTmp.BillID ";
        }

        private string CreateWhereClause(PmtActivityReportFilter filter, PmtActivityReportOption option)
        {
            string strPaymentDateRange = "";
            string whereParameter = " Where ReceiptNumber > -1 ";

            if (!string.IsNullOrWhiteSpace(filter.TaxBillStatus))
            {
                whereParameter += "and BillCode = '" + filter.TaxBillStatus + "'";
            }
            else
            {
                if (modStatusPayments.Statics.boolRE)
                {
                    whereParameter += "and (BillCode = 'R' or BillCode = 'L')";
                }
                else
                {
                    whereParameter += "and BillCode = 'P' ";
                }

            }

            if (filter.AccountMin.HasValue)
            {
                if (filter.AccountMax.HasValue && filter.AccountMax > 0)
                {
                    whereParameter += " and Account between " + filter.AccountMin + " and " +
                                      filter.AccountMax;
                }
                else
                {
                    whereParameter += " and Account >= " + filter.AccountMin;
                }
            }
            else
            {
                if (filter.AccountMax.HasValue && filter.AccountMax > 0)
                {
                    whereParameter += " and Account <= " + filter.AccountMax;
                }
            }

            if (option.PaymentTypes() != "")
            {
                whereParameter += " and Code IN (" + option.PaymentTypes() + ")";
            }
            else
            {
            }

            if (filter.TaxYearMin.HasValue && filter.TaxYearMin.Value > 0)
            {
                if (filter.TaxYearMax.HasValue && filter.TaxYearMax.Value > 0)
                {
                    whereParameter += " and (Year/10) between " + filter.TaxYearMin + " and " +
                                      filter.TaxYearMax;
                }
                else
                {
                    whereParameter += " and (Year/10) = " + filter.TaxYearMin;
                }
            }

            if (!String.IsNullOrWhiteSpace(filter.Reference))
            {
                whereParameter += " and Reference = '" + filter.Reference + "'";
            }

            if (filter.PaymentDateMin.HasValue && filter.PaymentDateMax.HasValue)
            {
                whereParameter += " and (RecordedTransactionDate >= '" +
                                      filter.PaymentDateMin.Value.ToShortDateString() +
                                      "' and RecordedTransactionDate <= '" +
                                      filter.PaymentDateMax.Value.ToShortDateString() + "')";
            }

            if (modGlobal.Statics.gboolMultipleTowns && (filter.TownCode > 0))
            {
                whereParameter += " and TranCode = " + filter.TownCode;
            }
            return whereParameter;
        }

        private string CreateSortParameter()
        {
            string sortParameter = " ";
            string separator = "";
            // GET THE FIELDS TO SORT BY
            var selectedOptions = SortOptions.Where(o => o.IsSelected).OrderBy(o => o.Item.OrderNumber);
            foreach (var sortOption in selectedOptions)
            {
                sortParameter += separator;
                separator = ", ";
                switch (sortOption.Item.ID)
                {
                    case (int)PmtActivitySortType.Account:
                        sortParameter += "Account";
                        break;
                    case (int)PmtActivitySortType.TransactionDate:
                        sortParameter += "RecordedTransactionDate";
                        break;
                }
            }

            if (String.IsNullOrWhiteSpace(sortParameter))
            {
                sortParameter = " RecordedTransactionDate";
            }

            return sortParameter;
        }

        private void FillTaxBillTypes()
        {
            BillTypes = new List<DescriptionIDPair>()
            {
                new DescriptionIDPair(){Description = "Regular", ID = 0},
                new DescriptionIDPair(){Description =  "Lien",ID = 1},
            };
        }
    }
}
