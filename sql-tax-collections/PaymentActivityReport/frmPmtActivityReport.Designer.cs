﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Collections.Generic;
using Global;

namespace TWCL0000
{
    /// <summary>
    /// Summary description for frmStatusLists.
    /// </summary>
    partial class frmPmtActivityReport : BaseForm
    {
        public fecherFoundation.FCButton cmdPrint;
        public fecherFoundation.FCButton cmdExit;
        public fecherFoundation.FCCheckBox chkIncludeAbatement;
        public fecherFoundation.FCCheckBox chkIncludePayment;
        public fecherFoundation.FCCheckBox chkIncludeCorrection;
        public fecherFoundation.FCCheckBox chkIncludePrePayment;
        public Wisej.Web.ImageList ImageList1;
        //private fecherFoundation.FCMenuStrip MainMenu1;
        public fecherFoundation.FCToolStripMenuItem mnuFile;
        public fecherFoundation.FCToolStripMenuItem mnuLayout;
        public fecherFoundation.FCToolStripMenuItem mnuAddRow;
        public fecherFoundation.FCToolStripMenuItem mnuAddColumn;
        public fecherFoundation.FCToolStripMenuItem mnuDeleteRow;
        public fecherFoundation.FCToolStripMenuItem mnuDeleteColumn;
        private Wisej.Web.ToolTip ToolTip1;

        protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
            {
                _InstancePtr = null;
                Sys.ClearInstance(this);
            }
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPmtActivityReport));
			this.cmdPrint = new fecherFoundation.FCButton();
			this.cmdExit = new fecherFoundation.FCButton();
			this.chkIncludeAbatement = new fecherFoundation.FCCheckBox();
			this.chkIncludePayment = new fecherFoundation.FCCheckBox();
			this.chkIncludeCorrection = new fecherFoundation.FCCheckBox();
			this.chkIncludePrePayment = new fecherFoundation.FCCheckBox();
			this.ImageList1 = new Wisej.Web.ImageList(this.components);
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuLayout = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddRow = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddColumn = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDeleteRow = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDeleteColumn = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.filterPanel = new Wisej.Web.FlexLayoutPanel();
			this.AccountPanel = new Wisej.Web.TableLayoutPanel();
			this.txtAccountStart = new Wisej.Web.TextBox();
			this.label3 = new Wisej.Web.Label();
			this.txtAccountEnd = new Wisej.Web.TextBox();
			this.AccountTypePanel = new Wisej.Web.TableLayoutPanel();
			this.cmbTaxBillType = new Wisej.Web.ComboBox();
			this.label6 = new Wisej.Web.Label();
			this.taxYearPanel = new Wisej.Web.TableLayoutPanel();
			this.cmbTaxYearEnd = new Wisej.Web.ComboBox();
			this.cmbTaxYearStart = new Wisej.Web.ComboBox();
			this.label4 = new Wisej.Web.Label();
			this.PaymentDateRangePanel = new Wisej.Web.TableLayoutPanel();
			this.dtpShowPaymentEnd = new Wisej.Web.DateTimePicker();
			this.dtpShowPaymentStart = new Wisej.Web.DateTimePicker();
			this.label2 = new Wisej.Web.Label();
			this.ReferencePanel = new Wisej.Web.TableLayoutPanel();
			this.txtReference = new Wisej.Web.TextBox();
			this.label10 = new Wisej.Web.Label();
			this.TownPanel = new Wisej.Web.TableLayoutPanel();
			this.cmbTownCode = new Wisej.Web.ComboBox();
			this.label1 = new Wisej.Web.Label();
			this.chkIncludeTaxClub = new fecherFoundation.FCCheckBox();
			this.chkIncludeRefund = new fecherFoundation.FCCheckBox();
			this.chkIncludeNonBudgetary = new fecherFoundation.FCCheckBox();
			this.chkIncludeInterest = new fecherFoundation.FCCheckBox();
			this.chkIncludeDemandFee = new fecherFoundation.FCCheckBox();
			this.chkIncludeMaturityFee = new fecherFoundation.FCCheckBox();
			this.label5 = new Wisej.Web.Label();
			this.label7 = new Wisej.Web.Label();
			this.label8 = new Wisej.Web.Label();
			this.label9 = new Wisej.Web.Label();
			this.label11 = new Wisej.Web.Label();
			this.label12 = new Wisej.Web.Label();
			this.label13 = new Wisej.Web.Label();
			this.label14 = new Wisej.Web.Label();
			this.label15 = new Wisej.Web.Label();
			this.label16 = new Wisej.Web.Label();
			this.label17 = new Wisej.Web.Label();
			this.btnProcess = new fecherFoundation.FCButton();
			this.cmdClear1 = new fecherFoundation.FCButton();
			this.fraQuestions2 = new fecherFoundation.FCFrame();
			this.fcFrame1 = new fecherFoundation.FCFrame();
			this.lstSort = new fecherFoundation.FCDraggableListBox();
			this.fraIncludeOptions = new fecherFoundation.FCFrame();
			this.chkIncludeDiscount = new fecherFoundation.FCCheckBox();
			this.javaScript1 = new Wisej.Web.JavaScript(this.components);
			this.chkEpmtOnly = new fecherFoundation.FCCheckBox();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdExit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIncludeAbatement)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIncludePayment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIncludeCorrection)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIncludePrePayment)).BeginInit();
			this.filterPanel.SuspendLayout();
			this.AccountPanel.SuspendLayout();
			this.AccountTypePanel.SuspendLayout();
			this.taxYearPanel.SuspendLayout();
			this.PaymentDateRangePanel.SuspendLayout();
			this.ReferencePanel.SuspendLayout();
			this.TownPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkIncludeTaxClub)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIncludeRefund)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIncludeNonBudgetary)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIncludeInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIncludeDemandFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIncludeMaturityFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraQuestions2)).BeginInit();
			this.fraQuestions2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame1)).BeginInit();
			this.fcFrame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraIncludeOptions)).BeginInit();
			this.fraIncludeOptions.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkIncludeDiscount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkEpmtOnly)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 751);
			this.BottomPanel.Size = new System.Drawing.Size(988, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraQuestions2);
			this.ClientArea.Controls.Add(this.filterPanel);
			this.ClientArea.Controls.Add(this.cmdPrint);
			this.ClientArea.Controls.Add(this.cmdExit);
			this.ClientArea.Location = new System.Drawing.Point(0, 65);
			this.ClientArea.Size = new System.Drawing.Size(1008, 693);
			this.ClientArea.Controls.SetChildIndex(this.cmdExit, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmdPrint, 0);
			this.ClientArea.Controls.SetChildIndex(this.filterPanel, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraQuestions2, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdClear1);
			this.TopPanel.Size = new System.Drawing.Size(1008, 65);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdClear1, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(177, 30);
			this.HeaderText.Text = "Custom Report";
			// 
			// cmdPrint
			// 
			this.cmdPrint.Cursor = Wisej.Web.Cursors.Default;
			this.cmdPrint.Location = new System.Drawing.Point(705, 725);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Size = new System.Drawing.Size(96, 26);
			this.cmdPrint.TabIndex = 7;
			this.cmdPrint.Text = "Print";
			this.cmdPrint.Visible = false;
			this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
			// 
			// cmdExit
			// 
			this.cmdExit.Cursor = Wisej.Web.Cursors.Default;
			this.cmdExit.Location = new System.Drawing.Point(823, 725);
			this.cmdExit.Name = "cmdExit";
			this.cmdExit.Size = new System.Drawing.Size(96, 26);
			this.cmdExit.TabIndex = 8;
			this.cmdExit.Text = "Exit";
			this.cmdExit.Visible = false;
			this.cmdExit.Click += new System.EventHandler(this.cmdExit_Click);
			// 
			// chkIncludeAbatement
			// 
			this.chkIncludeAbatement.Anchor = Wisej.Web.AnchorStyles.Left;
			this.chkIncludeAbatement.Location = new System.Drawing.Point(15, 113);
			this.chkIncludeAbatement.Name = "chkIncludeAbatement";
			this.chkIncludeAbatement.Size = new System.Drawing.Size(22, 17);
			this.chkIncludeAbatement.TabIndex = 16;
			this.ToolTip1.SetToolTip(this.chkIncludeAbatement, "This will include Abatements on the report.");
			// 
			// chkIncludePayment
			// 
			this.chkIncludePayment.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
			this.chkIncludePayment.ImageKey = "(none)";
			this.chkIncludePayment.Location = new System.Drawing.Point(15, 28);
			this.chkIncludePayment.Name = "chkIncludePayment";
			this.chkIncludePayment.Size = new System.Drawing.Size(22, 17);
			this.chkIncludePayment.TabIndex = 13;
			this.ToolTip1.SetToolTip(this.chkIncludePayment, "This will include Payments on the report..");
			// 
			// chkIncludeCorrection
			// 
			this.chkIncludeCorrection.Location = new System.Drawing.Point(15, 83);
			this.chkIncludeCorrection.Name = "chkIncludeCorrection";
			this.chkIncludeCorrection.Size = new System.Drawing.Size(22, 17);
			this.chkIncludeCorrection.TabIndex = 15;
			this.ToolTip1.SetToolTip(this.chkIncludeCorrection, "This will include Corrections on the report.");
			// 
			// chkIncludePrePayment
			// 
			this.chkIncludePrePayment.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.chkIncludePrePayment.Location = new System.Drawing.Point(15, 55);
			this.chkIncludePrePayment.Name = "chkIncludePrePayment";
			this.chkIncludePrePayment.Size = new System.Drawing.Size(22, 17);
			this.chkIncludePrePayment.TabIndex = 14;
			this.ToolTip1.SetToolTip(this.chkIncludePrePayment, "This will include Pre-Payments on the report.");
			// 
			// ImageList1
			// 
			this.ImageList1.ImageSize = new System.Drawing.Size(256, 18);
			this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(192, 192, 192);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuLayout
			// 
			this.mnuLayout.Enabled = false;
			this.mnuLayout.Index = -1;
			this.mnuLayout.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddRow,
            this.mnuAddColumn,
            this.mnuDeleteRow,
            this.mnuDeleteColumn});
			this.mnuLayout.Name = "mnuLayout";
			this.mnuLayout.Text = "Layout";
			// 
			// mnuAddRow
			// 
			this.mnuAddRow.Index = 0;
			this.mnuAddRow.Name = "mnuAddRow";
			this.mnuAddRow.Shortcut = Wisej.Web.Shortcut.F2;
			this.mnuAddRow.Text = "Add Row";
			// 
			// mnuAddColumn
			// 
			this.mnuAddColumn.Index = 1;
			this.mnuAddColumn.Name = "mnuAddColumn";
			this.mnuAddColumn.Shortcut = Wisej.Web.Shortcut.F3;
			this.mnuAddColumn.Text = "Add Column";
			// 
			// mnuDeleteRow
			// 
			this.mnuDeleteRow.Index = 2;
			this.mnuDeleteRow.Name = "mnuDeleteRow";
			this.mnuDeleteRow.Shortcut = Wisej.Web.Shortcut.F4;
			this.mnuDeleteRow.Text = "Delete Row";
			// 
			// mnuDeleteColumn
			// 
			this.mnuDeleteColumn.Index = 3;
			this.mnuDeleteColumn.Name = "mnuDeleteColumn";
			this.mnuDeleteColumn.Shortcut = Wisej.Web.Shortcut.F5;
			this.mnuDeleteColumn.Text = "Delete Column";
			// 
			// filterPanel
			// 
			this.filterPanel.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
			this.filterPanel.AutoSizeMode = Wisej.Web.AutoSizeMode.GrowAndShrink;
			this.filterPanel.Controls.Add(this.AccountPanel);
			this.filterPanel.Controls.Add(this.AccountTypePanel);
			this.filterPanel.Controls.Add(this.taxYearPanel);
			this.filterPanel.Controls.Add(this.PaymentDateRangePanel);
			this.filterPanel.Controls.Add(this.ReferencePanel);
			this.filterPanel.Controls.Add(this.TownPanel);
			this.filterPanel.LayoutStyle = Wisej.Web.FlexLayoutStyle.Vertical;
			this.filterPanel.Location = new System.Drawing.Point(30, 7);
			this.filterPanel.Name = "filterPanel";
			this.filterPanel.Size = new System.Drawing.Size(580, 629);
			this.filterPanel.TabIndex = 12;
			this.filterPanel.TabStop = true;
			// 
			// AccountPanel
			// 
			this.AccountPanel.AutoSize = true;
			this.AccountPanel.ColumnCount = 3;
			this.AccountPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 200F));
			this.AccountPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.AccountPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.AccountPanel.Controls.Add(this.txtAccountStart, 0, 0);
			this.AccountPanel.Controls.Add(this.label3, 0, 0);
			this.AccountPanel.Controls.Add(this.txtAccountEnd, 2, 0);
			this.AccountPanel.Location = new System.Drawing.Point(3, 3);
			this.AccountPanel.Name = "AccountPanel";
			this.AccountPanel.RowCount = 1;
			this.AccountPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
			this.AccountPanel.Size = new System.Drawing.Size(574, 28);
			this.AccountPanel.TabIndex = 1;
			this.AccountPanel.TabStop = true;
			// 
			// txtAccountStart
			// 
			this.txtAccountStart.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.javaScript1.SetJavaScript(this.txtAccountStart, resources.GetString("txtAccountStart.JavaScript"));
			this.txtAccountStart.Location = new System.Drawing.Point(203, 3);
			this.txtAccountStart.Name = "txtAccountStart";
			this.txtAccountStart.Size = new System.Drawing.Size(181, 22);
			this.txtAccountStart.TabIndex = 1;
			// 
			// label3
			// 
			this.label3.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Left | Wisej.Web.AnchorStyles.Right)));
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(3, 6);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(194, 16);
			this.label3.TabIndex = 2;
			this.label3.Text = "ACCOUNT";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtAccountEnd
			// 
			this.txtAccountEnd.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.javaScript1.SetJavaScript(this.txtAccountEnd, resources.GetString("txtAccountEnd.JavaScript"));
			this.txtAccountEnd.Location = new System.Drawing.Point(390, 3);
			this.txtAccountEnd.Name = "txtAccountEnd";
			this.txtAccountEnd.Size = new System.Drawing.Size(181, 22);
			this.txtAccountEnd.TabIndex = 3;
			// 
			// AccountTypePanel
			// 
			this.AccountTypePanel.AutoSize = true;
			this.AccountTypePanel.ColumnCount = 3;
			this.AccountTypePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 200F));
			this.AccountTypePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.AccountTypePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.AccountTypePanel.Controls.Add(this.cmbTaxBillType, 1, 0);
			this.AccountTypePanel.Controls.Add(this.label6, 0, 0);
			this.AccountTypePanel.Location = new System.Drawing.Point(3, 47);
			this.AccountTypePanel.Name = "AccountTypePanel";
			this.AccountTypePanel.RowCount = 1;
			this.AccountTypePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
			this.AccountTypePanel.Size = new System.Drawing.Size(574, 28);
			this.AccountTypePanel.TabIndex = 4;
			this.AccountTypePanel.TabStop = true;
			// 
			// cmbTaxBillType
			// 
			this.cmbTaxBillType.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.cmbTaxBillType.DisplayMember = "Description";
			this.cmbTaxBillType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbTaxBillType.Location = new System.Drawing.Point(203, 3);
			this.cmbTaxBillType.Name = "cmbTaxBillType";
			this.cmbTaxBillType.Size = new System.Drawing.Size(181, 22);
			this.cmbTaxBillType.TabIndex = 5;
			this.cmbTaxBillType.ValueMember = "ID";
			// 
			// label6
			// 
			this.label6.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Left | Wisej.Web.AnchorStyles.Right)));
			this.label6.Location = new System.Drawing.Point(3, 5);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(194, 17);
			this.label6.TabIndex = 5;
			this.label6.Text = "SHOW ACCOUNT TYPES";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// taxYearPanel
			// 
			this.taxYearPanel.AutoSize = true;
			this.taxYearPanel.ColumnCount = 3;
			this.taxYearPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 200F));
			this.taxYearPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.taxYearPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.taxYearPanel.Controls.Add(this.cmbTaxYearEnd, 2, 0);
			this.taxYearPanel.Controls.Add(this.cmbTaxYearStart, 1, 0);
			this.taxYearPanel.Controls.Add(this.label4, 0, 0);
			this.taxYearPanel.Location = new System.Drawing.Point(3, 91);
			this.taxYearPanel.Name = "taxYearPanel";
			this.taxYearPanel.RowCount = 1;
			this.taxYearPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
			this.taxYearPanel.Size = new System.Drawing.Size(574, 28);
			this.taxYearPanel.TabIndex = 6;
			this.taxYearPanel.TabStop = true;
			// 
			// cmbTaxYearEnd
			// 
			this.cmbTaxYearEnd.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.cmbTaxYearEnd.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbTaxYearEnd.Location = new System.Drawing.Point(390, 3);
			this.cmbTaxYearEnd.Name = "cmbTaxYearEnd";
			this.cmbTaxYearEnd.Size = new System.Drawing.Size(181, 22);
			this.cmbTaxYearEnd.TabIndex = 8;
			// 
			// cmbTaxYearStart
			// 
			this.cmbTaxYearStart.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.cmbTaxYearStart.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbTaxYearStart.Location = new System.Drawing.Point(203, 3);
			this.cmbTaxYearStart.Name = "cmbTaxYearStart";
			this.cmbTaxYearStart.Size = new System.Drawing.Size(181, 22);
			this.cmbTaxYearStart.TabIndex = 7;
			// 
			// label4
			// 
			this.label4.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Left | Wisej.Web.AnchorStyles.Right)));
			this.label4.Location = new System.Drawing.Point(3, 5);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(194, 17);
			this.label4.TabIndex = 7;
			this.label4.Text = "TAX YEAR";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// PaymentDateRangePanel
			// 
			this.PaymentDateRangePanel.AutoSize = true;
			this.PaymentDateRangePanel.AutoSizeMode = Wisej.Web.AutoSizeMode.GrowAndShrink;
			this.PaymentDateRangePanel.ColumnCount = 3;
			this.PaymentDateRangePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 200F));
			this.PaymentDateRangePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.PaymentDateRangePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.PaymentDateRangePanel.Controls.Add(this.dtpShowPaymentEnd, 2, 0);
			this.PaymentDateRangePanel.Controls.Add(this.dtpShowPaymentStart, 1, 0);
			this.PaymentDateRangePanel.Controls.Add(this.label2, 0, 0);
			this.PaymentDateRangePanel.Location = new System.Drawing.Point(3, 135);
			this.PaymentDateRangePanel.Name = "PaymentDateRangePanel";
			this.PaymentDateRangePanel.RowCount = 1;
			this.PaymentDateRangePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
			this.PaymentDateRangePanel.Size = new System.Drawing.Size(574, 46);
			this.PaymentDateRangePanel.TabIndex = 9;
			this.PaymentDateRangePanel.TabStop = true;
			// 
			// dtpShowPaymentEnd
			// 
			this.dtpShowPaymentEnd.AutoSize = false;
			this.dtpShowPaymentEnd.Checked = false;
			this.dtpShowPaymentEnd.CustomFormat = "MM/dd/yyyy";
			this.dtpShowPaymentEnd.Format = Wisej.Web.DateTimePickerFormat.Custom;
			this.dtpShowPaymentEnd.Location = new System.Drawing.Point(390, 3);
			this.dtpShowPaymentEnd.Mask = "99/99/9999";
			this.dtpShowPaymentEnd.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
			this.dtpShowPaymentEnd.Name = "dtpShowPaymentEnd";
			this.dtpShowPaymentEnd.Size = new System.Drawing.Size(181, 40);
			this.dtpShowPaymentEnd.TabIndex = 11;
			this.dtpShowPaymentEnd.Value = new System.DateTime(((long)(0)));
			// 
			// dtpShowPaymentStart
			// 
			this.dtpShowPaymentStart.AutoSize = false;
			this.dtpShowPaymentStart.Checked = false;
			this.dtpShowPaymentStart.CustomFormat = "MM/dd/yyyy";
			this.dtpShowPaymentStart.Format = Wisej.Web.DateTimePickerFormat.Custom;
			this.dtpShowPaymentStart.Location = new System.Drawing.Point(203, 3);
			this.dtpShowPaymentStart.Mask = "99/99/9999";
			this.dtpShowPaymentStart.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
			this.dtpShowPaymentStart.Name = "dtpShowPaymentStart";
			this.dtpShowPaymentStart.Size = new System.Drawing.Size(181, 40);
			this.dtpShowPaymentStart.TabIndex = 10;
			this.dtpShowPaymentStart.Value = new System.DateTime(((long)(0)));
			// 
			// label2
			// 
			this.label2.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Left | Wisej.Web.AnchorStyles.Right)));
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(3, 15);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(194, 16);
			this.label2.TabIndex = 11;
			this.label2.Text = "SHOW PAYMENTS FROM";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// ReferencePanel
			// 
			this.ReferencePanel.AutoSize = true;
			this.ReferencePanel.ColumnCount = 3;
			this.ReferencePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 200F));
			this.ReferencePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.ReferencePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.ReferencePanel.Controls.Add(this.txtReference);
			this.ReferencePanel.Controls.Add(this.label10, 0, 0);
			this.ReferencePanel.Location = new System.Drawing.Point(3, 197);
			this.ReferencePanel.Name = "ReferencePanel";
			this.ReferencePanel.RowCount = 1;
			this.ReferencePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
			this.ReferencePanel.Size = new System.Drawing.Size(574, 28);
			this.ReferencePanel.TabIndex = 12;
			this.ReferencePanel.TabStop = true;
			// 
			// txtReference
			// 
			this.txtReference.Location = new System.Drawing.Point(203, 3);
			this.txtReference.Name = "txtReference";
			this.txtReference.Size = new System.Drawing.Size(181, 22);
			this.txtReference.TabIndex = 13;
			// 
			// label10
			// 
			this.label10.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Left | Wisej.Web.AnchorStyles.Right)));
			this.label10.Location = new System.Drawing.Point(3, 5);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(194, 17);
			this.label10.TabIndex = 13;
			this.label10.Text = "REFERENCE";
			this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// TownPanel
			// 
			this.TownPanel.AutoSize = true;
			this.TownPanel.ColumnCount = 3;
			this.TownPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 200F));
			this.TownPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.TownPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
			this.TownPanel.Controls.Add(this.cmbTownCode, 1, 0);
			this.TownPanel.Controls.Add(this.label1, 0, 0);
			this.TownPanel.Location = new System.Drawing.Point(3, 241);
			this.TownPanel.Name = "TownPanel";
			this.TownPanel.RowCount = 1;
			this.TownPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
			this.TownPanel.Size = new System.Drawing.Size(574, 28);
			this.TownPanel.TabIndex = 14;
			this.TownPanel.TabStop = true;
			// 
			// cmbTownCode
			// 
			this.cmbTownCode.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.cmbTownCode.DisplayMember = "Description";
			this.cmbTownCode.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbTownCode.Location = new System.Drawing.Point(203, 3);
			this.cmbTownCode.Name = "cmbTownCode";
			this.cmbTownCode.Size = new System.Drawing.Size(181, 22);
			this.cmbTownCode.TabIndex = 15;
			this.cmbTownCode.ValueMember = "ID";
			// 
			// label1
			// 
			this.label1.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Left | Wisej.Web.AnchorStyles.Right)));
			this.label1.Location = new System.Drawing.Point(3, 5);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(194, 17);
			this.label1.TabIndex = 15;
			this.label1.Text = "TOWN CODE";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// chkIncludeTaxClub
			// 
			this.chkIncludeTaxClub.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.chkIncludeTaxClub.Location = new System.Drawing.Point(184, 28);
			this.chkIncludeTaxClub.Name = "chkIncludeTaxClub";
			this.chkIncludeTaxClub.Size = new System.Drawing.Size(22, 17);
			this.chkIncludeTaxClub.TabIndex = 17;
			this.ToolTip1.SetToolTip(this.chkIncludeTaxClub, "This will include Tax Club payments on the report.");
			// 
			// chkIncludeRefund
			// 
			this.chkIncludeRefund.Location = new System.Drawing.Point(184, 55);
			this.chkIncludeRefund.Name = "chkIncludeRefund";
			this.chkIncludeRefund.Size = new System.Drawing.Size(22, 17);
			this.chkIncludeRefund.TabIndex = 18;
			this.ToolTip1.SetToolTip(this.chkIncludeRefund, "This will include Refunds on the report.");
			// 
			// chkIncludeNonBudgetary
			// 
			this.chkIncludeNonBudgetary.Location = new System.Drawing.Point(184, 83);
			this.chkIncludeNonBudgetary.Name = "chkIncludeNonBudgetary";
			this.chkIncludeNonBudgetary.Size = new System.Drawing.Size(22, 17);
			this.chkIncludeNonBudgetary.TabIndex = 21;
			this.ToolTip1.SetToolTip(this.chkIncludeNonBudgetary, "This will include Non-Budgetary amounts on the report.");
			// 
			// chkIncludeInterest
			// 
			this.chkIncludeInterest.Location = new System.Drawing.Point(184, 112);
			this.chkIncludeInterest.Name = "chkIncludeInterest";
			this.chkIncludeInterest.Size = new System.Drawing.Size(22, 17);
			this.chkIncludeInterest.TabIndex = 22;
			this.ToolTip1.SetToolTip(this.chkIncludeInterest, "This will include Interest payments on the report.");
			// 
			// chkIncludeDemandFee
			// 
			this.chkIncludeDemandFee.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
			this.chkIncludeDemandFee.Location = new System.Drawing.Point(184, 141);
			this.chkIncludeDemandFee.Name = "chkIncludeDemandFee";
			this.chkIncludeDemandFee.Size = new System.Drawing.Size(22, 17);
			this.chkIncludeDemandFee.TabIndex = 23;
			this.ToolTip1.SetToolTip(this.chkIncludeDemandFee, "This will include Demand Fee payments on the report.");
			// 
			// chkIncludeMaturityFee
			// 
			this.chkIncludeMaturityFee.Location = new System.Drawing.Point(184, 170);
			this.chkIncludeMaturityFee.Name = "chkIncludeMaturityFee";
			this.chkIncludeMaturityFee.Size = new System.Drawing.Size(22, 17);
			this.chkIncludeMaturityFee.TabIndex = 24;
			this.ToolTip1.SetToolTip(this.chkIncludeMaturityFee, "This will include Maturity Fee payments on the report.");
			// 
			// label5
			// 
			this.label5.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Left | Wisej.Web.AnchorStyles.Right)));
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(38, 31);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(62, 31);
			this.label5.TabIndex = 25;
			this.label5.Text = "PAYMENT";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label7
			// 
			this.label7.Anchor = Wisej.Web.AnchorStyles.Left;
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(38, 59);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(105, 16);
			this.label7.TabIndex = 26;
			this.label7.Text = "PRE-PAYMENT";
			this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label8
			// 
			this.label8.Anchor = Wisej.Web.AnchorStyles.Left;
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(38, 87);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(98, 16);
			this.label8.TabIndex = 31;
			this.label8.Text = "CORRECTION";
			this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label9
			// 
			this.label9.Anchor = Wisej.Web.AnchorStyles.Left;
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(38, 116);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(90, 16);
			this.label9.TabIndex = 32;
			this.label9.Text = "ABATEMENT";
			this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label11
			// 
			this.label11.Anchor = Wisej.Web.AnchorStyles.Left;
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(38, 145);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(78, 16);
			this.label11.TabIndex = 33;
			this.label11.Text = "DISCOUNT";
			this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label12
			// 
			this.label12.Anchor = Wisej.Web.AnchorStyles.Left;
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(207, 116);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(74, 16);
			this.label12.TabIndex = 37;
			this.label12.Text = "INTEREST";
			this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label13
			// 
			this.label13.Anchor = Wisej.Web.AnchorStyles.Left;
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(207, 87);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(125, 16);
			this.label13.TabIndex = 36;
			this.label13.Text = "NON-BUDGETARY";
			this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label14
			// 
			this.label14.Anchor = Wisej.Web.AnchorStyles.Left;
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(207, 60);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(64, 16);
			this.label14.TabIndex = 35;
			this.label14.Text = "REFUND";
			this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label15
			// 
			this.label15.Anchor = Wisej.Web.AnchorStyles.Left;
			this.label15.AutoSize = true;
			this.label15.Location = new System.Drawing.Point(207, 32);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(73, 16);
			this.label15.TabIndex = 34;
			this.label15.Text = "TAX CLUB";
			this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label16
			// 
			this.label16.Anchor = Wisej.Web.AnchorStyles.Left;
			this.label16.AutoSize = true;
			this.label16.Location = new System.Drawing.Point(207, 174);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(106, 16);
			this.label16.TabIndex = 39;
			this.label16.Text = "MATURITY FEE";
			this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label17
			// 
			this.label17.Anchor = Wisej.Web.AnchorStyles.Left;
			this.label17.AutoSize = true;
			this.label17.Location = new System.Drawing.Point(207, 145);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(96, 16);
			this.label17.TabIndex = 38;
			this.label17.Text = "DEMAND FEE";
			this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Cursor = Wisej.Web.Cursors.Default;
			this.btnProcess.Location = new System.Drawing.Point(444, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(120, 48);
			this.btnProcess.TabIndex = 25;
			this.btnProcess.Text = "Process";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// cmdClear1
			// 
			this.cmdClear1.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdClear1.Cursor = Wisej.Web.Cursors.Default;
			this.cmdClear1.Location = new System.Drawing.Point(833, 29);
			this.cmdClear1.Name = "cmdClear1";
			this.cmdClear1.Size = new System.Drawing.Size(152, 24);
			this.cmdClear1.TabIndex = 26;
			this.cmdClear1.Text = "Clear Search Criteria";
			this.cmdClear1.Click += new System.EventHandler(this.cmdClear_Click);
			// 
			// fraQuestions2
			// 
			this.fraQuestions2.AppearanceKey = "groupBoxNoBorders";
			this.fraQuestions2.Controls.Add(this.chkEpmtOnly);
			this.fraQuestions2.Controls.Add(this.fcFrame1);
			this.fraQuestions2.Controls.Add(this.fraIncludeOptions);
			this.fraQuestions2.Location = new System.Drawing.Point(620, 3);
			this.fraQuestions2.Name = "fraQuestions2";
			this.fraQuestions2.Size = new System.Drawing.Size(365, 736);
			this.fraQuestions2.TabIndex = 27;
			// 
			// fcFrame1
			// 
			this.fcFrame1.Controls.Add(this.lstSort);
			this.fcFrame1.Location = new System.Drawing.Point(6, 245);
			this.fcFrame1.Name = "fcFrame1";
			this.fcFrame1.Size = new System.Drawing.Size(351, 223);
			this.fcFrame1.TabIndex = 40;
			this.fcFrame1.Text = "Fields To Sort By";
			// 
			// lstSort
			// 
			this.lstSort.BackColor = System.Drawing.SystemColors.Window;
			this.lstSort.BorderStyle = Wisej.Web.BorderStyle.None;
			this.lstSort.CheckBoxes = true;
			this.lstSort.CssClass = "alert alert-warning";
			this.lstSort.Location = new System.Drawing.Point(6, 30);
			this.lstSort.Name = "lstSort";
			this.lstSort.Size = new System.Drawing.Size(337, 187);
			this.lstSort.Style = 1;
			this.lstSort.TabIndex = 30;
			// 
			// fraIncludeOptions
			// 
			this.fraIncludeOptions.Controls.Add(this.label16);
			this.fraIncludeOptions.Controls.Add(this.label5);
			this.fraIncludeOptions.Controls.Add(this.label17);
			this.fraIncludeOptions.Controls.Add(this.chkIncludeMaturityFee);
			this.fraIncludeOptions.Controls.Add(this.label12);
			this.fraIncludeOptions.Controls.Add(this.chkIncludeDemandFee);
			this.fraIncludeOptions.Controls.Add(this.label13);
			this.fraIncludeOptions.Controls.Add(this.chkIncludeInterest);
			this.fraIncludeOptions.Controls.Add(this.label14);
			this.fraIncludeOptions.Controls.Add(this.chkIncludeNonBudgetary);
			this.fraIncludeOptions.Controls.Add(this.label15);
			this.fraIncludeOptions.Controls.Add(this.chkIncludeRefund);
			this.fraIncludeOptions.Controls.Add(this.label11);
			this.fraIncludeOptions.Controls.Add(this.chkIncludeDiscount);
			this.fraIncludeOptions.Controls.Add(this.label9);
			this.fraIncludeOptions.Controls.Add(this.chkIncludeTaxClub);
			this.fraIncludeOptions.Controls.Add(this.label8);
			this.fraIncludeOptions.Controls.Add(this.chkIncludePayment);
			this.fraIncludeOptions.Controls.Add(this.label7);
			this.fraIncludeOptions.Controls.Add(this.chkIncludeAbatement);
			this.fraIncludeOptions.Controls.Add(this.chkIncludeCorrection);
			this.fraIncludeOptions.Controls.Add(this.chkIncludePrePayment);
			this.fraIncludeOptions.Location = new System.Drawing.Point(6, 3);
			this.fraIncludeOptions.Name = "fraIncludeOptions";
			this.fraIncludeOptions.Size = new System.Drawing.Size(351, 223);
			this.fraIncludeOptions.TabIndex = 28;
			this.fraIncludeOptions.Text = "Include";
			// 
			// chkIncludeDiscount
			// 
			this.chkIncludeDiscount.Location = new System.Drawing.Point(16, 141);
			this.chkIncludeDiscount.Name = "chkIncludeDiscount";
			this.chkIncludeDiscount.Size = new System.Drawing.Size(22, 17);
			this.chkIncludeDiscount.TabIndex = 16;
			// 
			// chkEpmtOnly
			// 
			this.chkEpmtOnly.Location = new System.Drawing.Point(63, 486);
			this.chkEpmtOnly.Name = "chkEpmtOnly";
			this.chkEpmtOnly.Size = new System.Drawing.Size(196, 18);
			this.chkEpmtOnly.TabIndex = 41;
			this.chkEpmtOnly.Text = "Show Online Payments Only";
			this.chkEpmtOnly.CheckedChanged += new System.EventHandler(this.chkEpmtOnly_CheckedChanged);
			// 
			// frmPmtActivityReport
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1008, 758);
			this.Cursor = Wisej.Web.Cursors.Default;
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmPmtActivityReport";
			this.Text = "Custom Report";
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmPmtActivityReport_Load);
			this.Activated += new System.EventHandler(this.frmPmtActivityReport_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPmtActivityReport_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdExit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIncludeAbatement)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIncludePayment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIncludeCorrection)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIncludePrePayment)).EndInit();
			this.filterPanel.ResumeLayout(false);
			this.filterPanel.PerformLayout();
			this.AccountPanel.ResumeLayout(false);
			this.AccountPanel.PerformLayout();
			this.AccountTypePanel.ResumeLayout(false);
			this.AccountTypePanel.PerformLayout();
			this.taxYearPanel.ResumeLayout(false);
			this.taxYearPanel.PerformLayout();
			this.PaymentDateRangePanel.ResumeLayout(false);
			this.PaymentDateRangePanel.PerformLayout();
			this.ReferencePanel.ResumeLayout(false);
			this.ReferencePanel.PerformLayout();
			this.TownPanel.ResumeLayout(false);
			this.TownPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkIncludeTaxClub)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIncludeRefund)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIncludeNonBudgetary)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIncludeInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIncludeDemandFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIncludeMaturityFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraQuestions2)).EndInit();
			this.fraQuestions2.ResumeLayout(false);
			this.fraQuestions2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcFrame1)).EndInit();
			this.fcFrame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraIncludeOptions)).EndInit();
			this.fraIncludeOptions.ResumeLayout(false);
			this.fraIncludeOptions.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkIncludeDiscount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkEpmtOnly)).EndInit();
			this.ResumeLayout(false);

        }
        #endregion

        private System.ComponentModel.IContainer components;
        private FCButton btnProcess;
        public FCButton cmdClear1;
        private FlexLayoutPanel filterPanel;
        private TableLayoutPanel AccountPanel;
        private TextBox txtAccountStart;
        private Label label3;
        private TextBox txtAccountEnd;
        private TableLayoutPanel taxYearPanel;
        private ComboBox cmbTaxYearEnd;
        private ComboBox cmbTaxYearStart;
        private Label label4;
        private TableLayoutPanel AccountTypePanel;
        private ComboBox cmbTaxBillType;
        private Label label6;
        private TableLayoutPanel PaymentDateRangePanel;
        private DateTimePicker dtpShowPaymentEnd;
        private DateTimePicker dtpShowPaymentStart;
        private Label label2;
        private TableLayoutPanel ReferencePanel;
        private Label label10;
        private FCFrame fraQuestions2;
        public FCDraggableListBox lstSort;
        public FCCheckBox chkIncludeTaxClub;
        private JavaScript javaScript1;
        private FCCheckBox chkIncludeDiscount;
        private FCCheckBox chkIncludeMaturityFee;
        private FCCheckBox chkIncludeDemandFee;
        private FCCheckBox chkIncludeInterest;
        private FCCheckBox chkIncludeNonBudgetary;
        private FCCheckBox chkIncludeRefund;
        private TextBox txtReference;
        private TableLayoutPanel TownPanel;
        private ComboBox cmbTownCode;
        private Label label1;
        private Label label5;
        private Label label16;
        private Label label17;
        private Label label12;
        private Label label13;
        private Label label14;
        private Label label15;
        private Label label11;
        private Label label9;
        private Label label8;
        private Label label7;
        public FCFrame fraIncludeOptions;
        public FCFrame fcFrame1;
		public FCCheckBox chkEpmtOnly;
	}
}
