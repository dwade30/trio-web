﻿using fecherFoundation;

namespace TWCL0000
{
	public class modReminderNoticeSummary
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               10/28/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/02/2004              *
		// *
		// This will keep track of the notices sent out from the  *
		// reminder notices and the totals in order to create     *
		// of a summary report.                                   *
		// ********************************************************
		public struct ReminderSummaryList
		{
			public int Account;
			public string Name1;
			public string Addr1;
			public string Addr2;
			public string Addr3;
			public string Addr4;
			// VBto upgrade warning: Year As int	OnWrite(short, string)
			public int Year;
			public double Total;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public ReminderSummaryList(int unusedParam)
			{
				this.Account = 0;
				this.Name1 = string.Empty;
				this.Addr1 = string.Empty;
				this.Addr2 = string.Empty;
				this.Addr3 = string.Empty;
				this.Addr4 = string.Empty;
				this.Year = 0;
				this.Total = 0;
			}
		};
		// 0 - Labels, 1 - Post Cards, 2 - Forms, 3 - Mailers
		public class StaticVariables
		{
			public ReminderSummaryList[] arrReminderSummaryList = null;
			public int intRPTReminderSummaryType;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
