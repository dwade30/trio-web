//Fecher vbPorter - Version 1.0.0.59
using Wisej.Web;
using fecherFoundation.Extensions;
using fecherFoundation;
using Global;
using System;

namespace TWCL0000
{
	public partial class frmTaxClubOutstanding : BaseForm
    {
		public frmTaxClubOutstanding()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
            AddHandlers();
        }

		private void InitializeComponentEx()
		{

            //
            // todo: add any constructor code after initializecomponent call
            //
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
        }

        private void AddHandlers()
        {
            cmbAccounts.SelectedIndexChanged += CmbAccounts_SelectedIndexChanged;
        }

        /// <summary>
        /// default instance for form
        /// </summary>
        public static frmTaxClubOutstanding InstancePtr
        {
            get
            {
                return (frmTaxClubOutstanding)Sys.GetInstance(typeof(frmTaxClubOutstanding));
            }
        }
        protected static frmTaxClubOutstanding _InstancePtr = null;




		bool boolLoaded;
		// vbPorter upgrade warning: boolSummaryOnly As bool	OnWrite(CheckState)
		bool boolSummaryOnly; // trocl-1367 8.24.17 kjr add number of payments option
		bool boolAllAccounts;
		// vbPorter upgrade warning: intNumberOfPayments As short	OnWriteFCConvert.ToInt32(
		short intNumberOfPayments;
		string strTemp = "";
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper rsData = new clsDRWrapper();
			public clsDRWrapper rsData_AutoInitialized = null;
			public clsDRWrapper rsData
			{
				get
				{
					if ( rsData_AutoInitialized == null)
					{
						 rsData_AutoInitialized = new clsDRWrapper();
					}
					return rsData_AutoInitialized;
				}
				set
				{
					 rsData_AutoInitialized = value;
				}
			}

		public void Init()
		{
            //InstancePtr.Show(App.MainForm);
			// trocl-1367 8.25.17 kjr
			//InstancePtr.chkSummaryOnly_0.CheckState = Wisej.Web.CheckState.Unchecked; // summary only
            InstancePtr.chkSummaryOnly_0.CheckState = Wisej.Web.CheckState.Unchecked; // summary only
                                                                                      //InstancePtr.optChoice_0.Checked = true; // all accounts
            InstancePtr.cmbAccounts.SelectedIndex = 0;
            //InstancePtr.txtNumberOfPayments_3.Enabled = false;
            InstancePtr.txtNumberOfPayments_3.Enabled = false;
            //InstancePtr.chkSummaryOnly_0.Enabled = false;
            InstancePtr.chkSummaryOnly_0.Enabled = false;

            this.Show(App.MainForm);
		}

		private void frmTaxClubOutstanding_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				boolLoaded = true;
			}
			else
			{

			}
		}

		private void frmTaxClubOutstanding_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors( this);
		}

		private void frmTaxClubOutstanding_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);

			// catches the escape and enter keys
			if (KeyAscii==Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			} else if (KeyAscii==Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}

			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			boolSummaryOnly = CheckState.Unchecked != chkSummaryOnly_0.CheckState; // trocl-1367 8.25.17 kjr
			boolAllAccounts = true;
			bool boolExactPayments;
			boolExactPayments = true;
            var intChoice = cmbAccounts.SelectedIndex;
			if (intChoice != 0)
			{
				boolAllAccounts = false;
                if (String.IsNullOrWhiteSpace(txtNumberOfPayments_3.Text))
                {
                    intNumberOfPayments = 0;
                }
                else
                {
                    intNumberOfPayments = FCConvert.ToInt16(FCConvert.ToInt32(txtNumberOfPayments_3.Text));
                }
                if (intChoice == 2)
				{
					boolExactPayments = false;
				}
				rptTaxClubPaymentsOverdue tReport = new rptTaxClubPaymentsOverdue();
				tReport.Init( boolSummaryOnly,  boolAllAccounts,  intNumberOfPayments,  boolExactPayments);
			}
			else
			{
				rptTaxClubOutstanding oReport = new rptTaxClubOutstanding();
				oReport.Init();
			}

		}

		private void optChoice_0_CheckedChanged(object sender, System.EventArgs e)
		{
            txtNumberOfPayments_3.Enabled = false;
            chkSummaryOnly_0.Enabled = false;
		}

        private void optChoice_1_CheckedChanged(object sender, System.EventArgs e)
        {
            txtNumberOfPayments_3.Enabled = true;
            chkSummaryOnly_0.Enabled = true;
        }
        private void optChoice_2_CheckedChanged(object sender, System.EventArgs e)
        {
            txtNumberOfPayments_3.Enabled = true;
            chkSummaryOnly_0.Enabled = true;
        }
        //private void InitializeComponent()
        //{
        //    this.TopPanel.SuspendLayout();
        //    this.SuspendLayout();
        //    // 
        //    // BottomPanel
        //    // 
        //    this.BottomPanel.Location = new System.Drawing.Point(0, 580);
        //    // 
        //    // ClientArea
        //    // 
        //    this.ClientArea.Size = new System.Drawing.Size(1078, 520);
        //    this.ClientArea.PanelCollapsed += new System.EventHandler(this.ClientArea_PanelCollapsed);
        //    // 
        //    // HeaderText
        //    // 
        //    this.HeaderText.Location = new System.Drawing.Point(30, 26);
        //    // 
        //    // frmTaxClubOutstanding
        //    // 
        //    this.Name = "frmTaxClubOutstanding";
        //    this.TopPanel.ResumeLayout(false);
        //    this.TopPanel.PerformLayout();
        //    this.ResumeLayout(false);

        //}

        private void ClientArea_PanelCollapsed(object sender, EventArgs e)
        {

        }
        private void CmbAccounts_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch  (cmbAccounts.SelectedIndex)
            {
                case 1:
                    optChoice_1_CheckedChanged(sender, e);
                    break;
                case 2:
                optChoice_2_CheckedChanged(sender, e);
                    break;
                default:
                optChoice_0_CheckedChanged(sender, e);
                break;
            }
            
        }
        private void btnProcess_Click(object sender, EventArgs e)
        {
            this.mnuFilePrint_Click(sender, e);
        }
    }

}
