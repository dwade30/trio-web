﻿using fecherFoundation;
using fecherFoundation.VisualBasicLayer;
using Global;
using System;
using System.Collections.Generic;
using SharedApplication;
using SharedApplication.AccountsReceivable.Models;
using SharedDataAccess;
using TWCL0000.DataAccess;
using Wisej.Web;
using SharedDataAccess.TaxCollections;
using TWCL0000.TaxBillExport;
using TWCL0000.TaxBillExport.DataAccess;
using TWSharedLibrary;
using Autofac;
using GrapeCity.Viewer.Common.Model;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.AccountPayment;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Interfaces;
using SharedApplication.TaxCollections.Receipting;

namespace TWCL0000
{
	public class MDIParent
	//: fecherFoundation.FCMainForm
	{
        
		public FCCommonDialog CommonDialog1 = new FCCommonDialog();
		private int[] LabelNumber = new int[200 + 1];
		const string strTrio = "TRIO Software";
        private IContainer diContainer;
        private CommandDispatcher commandDispatcher;
		public MDIParent()
        {
            //InitializeComponent();
			InitializeComponentEx();
           
            
        }

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static MDIParent InstancePtr
		{
			get
			{
				return (MDIParent)Sys.GetInstance(typeof(MDIParent));
			}
		}

		protected MDIParent _InstancePtr = null;

        //private void MainForm_Load(object sender, System.EventArgs e)
		public void Init(IContainer container)
        {
            diContainer = container;
            commandDispatcher = diContainer.Resolve<CommandDispatcher>();
            modGlobalFunctions.LoadTRIOColors();
			App.MainForm.StatusBarText1 = StaticSettings.EnvironmentSettings.ClientName;
			App.MainForm.NavigationMenu.Owner = this;
            App.MainForm.menuTree.ImageList = CreateImageList();
			GetCollectionsMenu();
            if (modGlobalConstants.Statics.gboolTS)
            {
                CheckTSReminders();
            }
		}

		public ImageList CreateImageList()
		{
			ImageList imageList = new ImageList();
			imageList.ImageSize = new System.Drawing.Size(18, 18);
			imageList.Images.AddRange(new ImageListEntry[] {
				new ImageListEntry("menutree-file-maintenance", "file-maintenance"),
				new ImageListEntry("menutree-status", "status"),
				new ImageListEntry("menutree-mortgage-holder", "mortgage-holder"),
				new ImageListEntry("menutree-printing", "printing"),
				new ImageListEntry("menutree-tax-club-routines", "tax-club-routines"),
				new ImageListEntry("menutree-lien-process", "lien-process"),
				new ImageListEntry("menutree-interested-parties", "interested-parties"),
				new ImageListEntry("menutree-daily-audit-report", "daily-audit-report"),
				new ImageListEntry("menutree-back-information", "back-information"),
				new ImageListEntry("menutree-payments-adjustments", "payments-adjustments"),
				new ImageListEntry("menutree-file-maintenance-active", "file-maintenance-active"),
				new ImageListEntry("menutree-status-active", "status-active"),
				new ImageListEntry("menutree-mortgage-holder-active", "mortgage-holder-active"),
				new ImageListEntry("menutree-printing-active", "printing-active"),
				new ImageListEntry("menutree-tax-club-routines-active", "tax-club-routines-active"),
				new ImageListEntry("menutree-lien-process-active", "lien-process-active"),
				new ImageListEntry("menutree-interested-parties-active", "interested-parties-active"),
				new ImageListEntry("menutree-daily-audit-report-active", "daily-audit-report-active"),
				new ImageListEntry("menutree-back-information-active", "back-information-active"),
				new ImageListEntry("menutree-payments-adjustments-active", "payments-adjustments-active"),
				new ImageListEntry("menutree-street-maintenance", "street-maintenance")
			});
			return imageList;
		}

		private void GetCollectionsMenu()
		{
			string strTemp = "";
			int lngFCode = 0;
			bool boolDisable = false;
			//ClearLabels();
			string menu = "MAIN";
			string imageKey = "";
			if (modStatusPayments.Statics.boolRE)
			{
				App.MainForm.Text = strTrio + " - RE Collections   [Main]";
				App.MainForm.Caption = "REAL ESTATE COLLECTIONS";
                App.MainForm.NavigationMenu.OriginName = "Real Estate Tax Collections";
				App.MainForm.ApplicationIcon = "icon-real-estate-collections";
				string assemblyName = this.GetType().Assembly.GetName().Name;
				if (!App.MainForm.ApplicationIcons.ContainsKey(assemblyName))
				{
					App.MainForm.ApplicationIcons.Add(assemblyName, "icon-real-estate-collections");
				}
			}
			else
			{
				App.MainForm.Text = strTrio + " - PP Collections   [Main]";
				App.MainForm.Caption = "PERSONAL PROPERTY COLLECTIONS";
                App.MainForm.NavigationMenu.OriginName = "Personal Property Tax Collections";
				App.MainForm.ApplicationIcon = "icon-personal-property-collections";
				string assemblyName = this.GetType().Assembly.GetName().Name;
				if (!App.MainForm.ApplicationIcons.ContainsKey(assemblyName))
				{
					App.MainForm.ApplicationIcons.Add(assemblyName, "icon-personal-property-collections");
				}
			}
			for (int CurRow = 1; CurRow <= 10; CurRow++)
			{
				boolDisable = false;
				switch (CurRow)
				{
					case 1:
						{
							strTemp = "Status";
							imageKey = "status";
							lngFCode = modGlobal.CLSECURITYVIEWACCOUNT;
							break;
						}
					case 2:
						{
							if (modGlobalConstants.Statics.gboolCR)
							{
								// if the user has CR then do not allow then to make payments through CL w/o goin through CR
								boolDisable = true;
							}
							strTemp = "Payments & Adjustments";
							imageKey = "payments-adjustments";
							lngFCode = modGlobal.CLSECURITYPAYMENTS;
							break;
						}
					case 3:
						{
							strTemp = "Daily Audit Report";
							imageKey = "daily-audit-report";
							lngFCode = modGlobal.CLSECURITYPRINTAUDIT;
							break;
						}
					case 4:
						{
							strTemp = "Load of Back Information";
							imageKey = "back-information";
							lngFCode = modGlobal.CLSECURITYLOADBACK;
							break;
						}
					case 5:
						{
							strTemp = "Tax Club Routines";
							imageKey = "tax-club-routines";
							lngFCode = modGlobal.CLSECURITYTAXCLUB;
							break;
						}
					case 6:
						{
							if (!modStatusPayments.Statics.boolRE)
							{
								// if this is personal property, then do not allow the user in the Mortgage Holder Forms
								boolDisable = true;
							}
							strTemp = "Mortgage Holder";
							imageKey = "mortgage-holder";
							lngFCode = modGlobal.CLSECURITYMORTGAGEHOLDER;
							break;
						}
					case 7:
						{
							if (!modStatusPayments.Statics.boolRE)
							{
								boolDisable = true;
							}
							strTemp = "Interested Parties";
							imageKey = "interested-parties";
							lngFCode = modGlobal.CLSECURITYINTERESTEDPARTIES;
							break;
						}
					case 8:
						{
							strTemp = "Printing";
							imageKey = "printing";
							lngFCode = 0;
							break;
						}
					case 9:
						{
							if (!modStatusPayments.Statics.boolRE)
							{
								// if this is personal property, then do not allow the user into the Lien Process
								boolDisable = true;
							}
							strTemp = "Lien Process";
							imageKey = "lien-process";
							lngFCode = modGlobal.CLSECURITYLIENPROCESS;
							break;
						}
					case 10:
						{
							// strTemp = "X. Exit Collections"
							// lngFCode = 0
							strTemp = "File Maintenance";
							imageKey = "file-maintenance";
							lngFCode = modGlobal.CLSECURITYFILEMAINTENANCE;
							break;
						}
                    //case 11:
                    //{
                    //    strTemp = "New Status";
                    //    imageKey = "status";
                    //    lngFCode = modGlobal.CLSECURITYVIEWACCOUNT;
                    //    break;
                    //}
				}
				if (lngFCode != 0)
				{
					boolDisable |= !modSecurityValidation.ValidPermissions(this, lngFCode, false);
				}

                if (!boolDisable)
                {
                    FCMenuItem newItem =
                        App.MainForm.NavigationMenu.Add(strTemp, "Menu" + CurRow, menu, !boolDisable, 1, imageKey);
                    switch (CurRow)
                    {
                        case 8:
                        {
                            GetPrintMenu(newItem);
                            break;
                        }
                        case 9:
                        {
                            GetLienMenu(newItem);
                            break;
                        }
                        case 10:
                        {
                            GetFileMenu(newItem);
                            break;
                        }
                    }
                }

            }
            
            var customMenu = CreateCustomMenu(modGlobalConstants.Statics.MuniName);
            App.MainForm.NavigationMenu.AddMenuItem(customMenu);              
		}

        private MenuItemInformation CreateCustomMenu(string muniname)
        {
            List<MenuItemInformation> menuItems = new List<MenuItemInformation>();
            switch (muniname.ToLower())
            {
                case "york":
                    menuItems = GetCustomMenuItemsForYork();
                    break;
                case "gray":
                    menuItems = GetCustomMenuItemsForGray();
                    break;
            }

            if (menuItems.Count > 0)
            {
                return  new MenuItemInformation(menuItems)
                {
                    Name = "CUSTOM",
                    CommandCode = 0,
                    Level = 1,
                    ActionHandlerName = "ExecuteCustom",
                    Caption = "Custom Programs",
                    ImageKey = "street-maintenance"
                };
            }
            else
            {
                return null;
            }
        }
        private List<MenuItemInformation> GetCustomMenuItemsForYork()
        {
            var menuItems = new List<MenuItemInformation>();
			menuItems.Add(CreateYorkMenuItem());
			return menuItems;
        }

		private MenuItemInformation CreateYorkMenuItem()
		{
			return new MenuItemInformation(null)
			{
				Caption = "Export Unifund",
				ActionHandlerName = "ExecuteCustom",
				CommandCode = (int)CollectionCommands.UnifundExport,
				Level = 2
			};
		}

		private List<MenuItemInformation> GetCustomMenuItemsForGray()
        {
            var menuItems = new List<MenuItemInformation>();
            if (modStatusPayments.Statics.boolRE)
            {
                menuItems.Add(CreateGrayTaxBillExportMenuItem());
            }

            return menuItems;
        }
        private MenuItemInformation CreateGrayTaxBillExportMenuItem()
        {
            return new MenuItemInformation(null)
            {
                Caption = "Tax Bill Export",
                ActionHandlerName = "ExecuteCustom",
                CommandCode = (int)CollectionCommands.TaxBillExport,
                Level = 2
            };
        }
        private void Get30DayNoticeMenu(FCMenuItem parent)
		{
			string menu = "30DAYNOTICE";
            App.MainForm.Text = modStatusPayments.Statics.boolRE ? strTrio + " - RE Collections   [30 Day Process]" : strTrio + " - PP Collections   [30 Day Process]";
            string strTemp = "";
			for (int CurRow = 1; CurRow <= 6; CurRow++)
			{
				switch (CurRow)
				{
					case 1:
						{
							strTemp = "Print 30 Day Notices";
							break;
						}
					case 2:
						{
							strTemp = "Certified Mail Forms / Labels";
							break;
						}
					case 3:
						{
							strTemp = "Apply Demand Fees";
							break;
						}
					case 4:
						{
							strTemp = "Reprint Labels";
							break;
						}
					case 5:
						{
							strTemp = "Reprint Certified Mail Forms";
							break;
						}
					case 6:
						{
							strTemp = "Reprint Demand Fees List";
							break;
						}

				}
				//end switch
				parent.SubItems.Add(strTemp, "Menu" + CurRow, menu, true, 3);
			}
			LabelNumber[Strings.Asc("1")] = 1;
			LabelNumber[Strings.Asc("2")] = 2;
			LabelNumber[Strings.Asc("3")] = 3;
			LabelNumber[Strings.Asc("4")] = 4;
			LabelNumber[Strings.Asc("5")] = 5;
			LabelNumber[Strings.Asc("6")] = 6;
			// LabelNumber(Asc("7")) = 7
			// LabelNumber(Asc("X")) = 8
			LabelNumber[Strings.Asc("X")] = 7;
		}

		private void GetLienProcessMenu(FCMenuItem parent)
		{
			string menu = "LIENPROCESS";
            App.MainForm.Text = modStatusPayments.Statics.boolRE ? strTrio + " - RE Collections   [Tax Lien Process]" : strTrio + " - PP Collections   [Tax Lien Process]";
            string strTemp = "";
			for (int CurRow = 1; CurRow <= 6; CurRow++)
			{
				switch (CurRow)
				{
					case 1:
						{
							strTemp = "Print Tax Lien";
							break;
						}
					case 2:
						{
							strTemp = "Certified Mail Forms / Labels";
							break;
						}
					case 3:
						{
							strTemp = "Transfer Tax to Lien";
							break;
						}
					case 4:
						{
							strTemp = "Reprint Labels";
							break;
						}
					case 5:
						{
							strTemp = "Reprint Certified Mail Forms";
							break;
						}
					case 6:
						{
							strTemp = "Reprint Transfer Report";
							break;
						}

				}
				//end switch
				parent.SubItems.Add(strTemp, "Menu" + CurRow, menu, true, 3);
			}
			LabelNumber[Strings.Asc("1")] = 1;
			LabelNumber[Strings.Asc("2")] = 2;
			LabelNumber[Strings.Asc("3")] = 3;
			LabelNumber[Strings.Asc("4")] = 4;
			LabelNumber[Strings.Asc("5")] = 5;
			LabelNumber[Strings.Asc("6")] = 6;
			LabelNumber[Strings.Asc("X")] = 7;
		}

		private void GetLienMaturityMenu(FCMenuItem parent)
		{
			string menu = "LIENMATURITY";
            App.MainForm.Text = modStatusPayments.Statics.boolRE ? strTrio + " - RE Collections   [Lien Maturity Process]" : strTrio + " - PP Collections   [Lien Maturity Process]";
            string strTemp = "";
			for (int CurRow = 1; CurRow <= 8; CurRow++)
			{
				switch (CurRow)
				{
					case 1:
						{
							strTemp = "Print Lien Maturity Notice";
							break;
						}
					case 2:
						{
							strTemp = "Certified Mail Forms / Labels";
							break;
						}
					case 3:
						{
							strTemp = "Apply Lien Maturity Fees";
							break;
						}
					case 4:
						{
							strTemp = "Reprint Labels";
							break;
						}
					case 5:
						{
							strTemp = "Reprint Certified Mail Forms";
							break;
						}
					case 6:
						{
							strTemp = "Reprint Maturity Fees List";
							break;
						}
					case 7:
						{
							strTemp = "Reprint Bankruptcy Report";
							break;
						}
					case 8:
						{
							strTemp = "Remove Lien Maturity Fee";
							// DisableMenuOption ValidPermissions(Me, CRSECURITYREMOVELIENMATNOTICE, False), 8
							break;
						}

				}
				//end switch
				parent.SubItems.Add(strTemp, "Menu" + CurRow, menu, true, 3);
			}
			LabelNumber[Strings.Asc("1")] = 1;
			LabelNumber[Strings.Asc("2")] = 2;
			LabelNumber[Strings.Asc("3")] = 3;
			LabelNumber[Strings.Asc("4")] = 4;
			LabelNumber[Strings.Asc("5")] = 5;
			LabelNumber[Strings.Asc("6")] = 6;
			LabelNumber[Strings.Asc("7")] = 7;
			LabelNumber[Strings.Asc("8")] = 8;
			LabelNumber[Strings.Asc("X")] = 9;
		}

		private void GetFileMenu(FCMenuItem parent)
		{
			int lngFCode = 0;
			string menu = "FILE";
			bool boolDisable = false;
			if (modStatusPayments.Statics.boolRE)
			{
				App.MainForm.Text = strTrio + " - RE Collections   [File Maintenance]";
			}
			else
			{
				App.MainForm.Text = strTrio + " - PP Collections   [File Maintenance]";
			}
			string strTemp = "";
            //FC:FINAL:MSH - change number of menu items
			//for (int CurRow = 1; CurRow <= 10; CurRow++)
			for (int CurRow = 1; CurRow <= 9; CurRow++)
			{
				//PPJ:FINAL:MHO: #i108 - reset boolDisable
				boolDisable = false;
				switch (CurRow)
				{
					case 1:
						{
							strTemp = "Customize";
							lngFCode = modGlobal.CLSECURITYCUSTOMIZE;
							break;
						}
					case 2:
						{
							strTemp = "Check Database Structure";
							lngFCode = 0;
							break;
						}
					case 3:
						{
							strTemp = "Tax Service Extract";
							//PPJ:FINAL:MHO: #i108 - correct setting of boolDisable
							if (modGlobalConstants.Statics.gboolTS)
							{
								boolDisable = false;
							}
							else
							{
								boolDisable = true;
							}
							break;
						}
					case 4:
						{
							strTemp = "Database Extract";
							lngFCode = 0;
							break;
						}
					case 5:
						{
							strTemp = "Rate Record Update";
							lngFCode = modGlobal.CLSECURITYRATERECORDUPDATE;
							break;
						}
					case 6:
						{
							strTemp = "Edit Bill Information";
							lngFCode = modGlobal.CLSECURITYEDITBILLINFO;
							break;
						}
					case 7:
						{
							strTemp = "Purge Files";
							lngFCode = modGlobal.CLSECURITYPURGERECORDS;
							break;
						}
					case 8:
						{
							strTemp = "Clear Certified Mail Table";
							lngFCode = 0;
							break;
						}
					case 9:
						{
							strTemp = "Remove From Tax Acquired";
							lngFCode = modGlobal.CLSECURITYREMOVEFROMTAXACQUIRED;
							// Case 10
							// strTemp = "A. Import Payment File"
							// lngFCode = 0
							break;
						}
                    //FC:FINAL:MSH - remove an extra menu item
					//case 10:
					//	{
                    //        strTemp = "X. Return to Main Menu";
                    //        lngFCode = 0;
                    //        break;
					//	}
                }
				//end switch
				if (lngFCode != 0 && CurRow != 3 && CurRow != 4)
				{
					boolDisable |= !modSecurityValidation.ValidPermissions(this, lngFCode, false);
				}
				parent.SubItems.Add(strTemp, "Menu" + CurRow, menu, !boolDisable, 2);
                //parent.SubItems.Add(strTemp, "Execute:" + CurRow, menu, !boolDisable, 2);
            }
			LabelNumber[Strings.Asc("1")] = 1;
			LabelNumber[Strings.Asc("2")] = 2;
			LabelNumber[Strings.Asc("3")] = 3;
			LabelNumber[Strings.Asc("4")] = 4;
			LabelNumber[Strings.Asc("5")] = 5;
			LabelNumber[Strings.Asc("6")] = 6;
			LabelNumber[Strings.Asc("7")] = 7;
			LabelNumber[Strings.Asc("8")] = 8;
			LabelNumber[Strings.Asc("9")] = 9;
			LabelNumber[Strings.Asc("X")] = 10;
		}

		private void GetLienMenu(FCMenuItem parent)
		{
			int lngFCode = 0;
			string menu = "LIEN";
			bool boolDisable = false;
			if (modStatusPayments.Statics.boolRE)
			{
				App.MainForm.Text = strTrio + " - RE Collections   [Lien Process]";
			}
			else
			{
				App.MainForm.Text = strTrio + " - PP Collections   [Lien Process]";
			}
			string strTemp = "";
			for (int CurRow = 1; CurRow <= 13; CurRow++)
			{
				//PPJ:FINAL:MHO: #i108 - reset boolDisable
				boolDisable = false;
				switch (CurRow)
				{
					case 1:
						{
							strTemp = "Lien Process Edit Report";
							lngFCode = modGlobal.CLSECURITYLIENEDITREPORT;
							break;
						}
					case 2:
						{
							strTemp = "30 Day Process";
							lngFCode = modGlobal.CLSECURITY30DAYNOTICE;
							break;
						}
					case 3:
						{
							strTemp = "Tax Lien Process";
							lngFCode = modGlobal.CLSECURITYTRANSFERTAXTOLIEN;
							break;
						}
					case 4:
						{
							strTemp = "Lien Maturity Process";
							lngFCode = modGlobal.CLSECURITYLIENMATURITY;
							break;
						}
					case 5:
						{
							strTemp = "Remove From Lien Status";
							lngFCode = modGlobal.CLSECURITYREMOVELIEN;
							break;
						}
					case 6:
						{
							strTemp = "Reverse Demand Fees";
							lngFCode = modGlobal.CLSECURITYREVERSEDEMANDFEES;
							break;
						}
					case 7:
						{
							strTemp = "Edit Lien Book / Page";
							lngFCode = modGlobal.CLSECURITYEDITLIENBOOKPAGE;
							break;
						}
					case 8:
						{
							strTemp = "Edit LDN Book / Page";
							lngFCode = modGlobal.CLSECURITYEDITLDNBOOKPAGE;
							break;
						}
					case 9:
						{
							strTemp = "Lien Date Chart";
							// DisableMenuOption False, CurRow
							lngFCode = modGlobal.CLSECURITYLIENDATECHART;
							break;
						}
					case 10:
						{
							strTemp = "Updated Book Page Report";
							lngFCode = modGlobal.CLSECURITYUPDATEDBOOKPAGERPT;
							break;
						}
					case 11:
						{
							strTemp = "Tax Acquired Properties";
							// DisableMenuOption False, CurRow
							lngFCode = modGlobal.CLSECURITYTAXACQUIREDPROPERTIES;
							break;
						}
					case 12:
						{
							// trocl-565 08-08-2011 kgk  Add reprint of T/A Properties list
							strTemp = "Reprint T/A Properties List";
							// DisableMenuOption False, CurRow
							lngFCode = modGlobal.CLSECURITYTAXACQUIREDPROPERTIES;
							break;
						}
					case 13:
						{
							// trocl-565 08-08-2011  was Case 12
							strTemp = "Update Bill Addresses";
							lngFCode = modGlobal.CLSECURITYUPDATEBILLADDRESS;
							break;
						}
				//case 14:
				//    {
				//        // trocl-565 08-08-2011  was Case 13
				//        strTemp = "X. Return to Main Menu";
				//        lngFCode = 0;
				//        break;
				//    }
				//default:
				//    {
				//        strTemp = "";
				//        break;
				//    }
				}
				//end switch
				if (lngFCode != 0 && CurRow != 10)
				{
					//PPJ:FINAL:MHO: #i108 - correct setting of boolDisable
					boolDisable = !modSecurityValidation.ValidPermissions(this, lngFCode, false);
				}
				FCMenuItem item = parent.SubItems.Add(strTemp, "Menu" + CurRow, menu, !boolDisable, 2);
				switch (CurRow)
				{
					case 2:
						{
							Get30DayNoticeMenu(item);
							break;
						}
					case 3:
						{
							GetLienProcessMenu(item);
							break;
						}
					case 4:
						{
							GetLienMaturityMenu(item);
							break;
						}
				}
			}
			LabelNumber[Strings.Asc("1")] = 1;
			LabelNumber[Strings.Asc("2")] = 2;
			LabelNumber[Strings.Asc("3")] = 3;
			LabelNumber[Strings.Asc("4")] = 4;
			LabelNumber[Strings.Asc("5")] = 5;
			LabelNumber[Strings.Asc("6")] = 6;
			LabelNumber[Strings.Asc("7")] = 7;
			LabelNumber[Strings.Asc("8")] = 8;
			LabelNumber[Strings.Asc("9")] = 9;
			LabelNumber[Strings.Asc("A")] = 10;
			LabelNumber[Strings.Asc("B")] = 11;
			LabelNumber[Strings.Asc("C")] = 12;
			LabelNumber[Strings.Asc("D")] = 13;
			LabelNumber[Strings.Asc("X")] = 14;
		}

		private void GetPrintMenu(FCMenuItem parent)
		{
			string strTemp = "";
			int lngFCode = 0;
			string menu = "PRINT";
			bool boolDisable = false;
			if (modStatusPayments.Statics.boolRE)
			{
				App.MainForm.Text = strTrio + " - RE Collections   [Printing]";
			}
			else
			{
				App.MainForm.Text = strTrio + " - PP Collections   [Printing]";
			}
			for (int CurRow = 1; CurRow <= 15; CurRow++)
			{
				//PPJ:FINAL:MHO: #i108 - reset boolDisable
				boolDisable = false;
				switch (CurRow)
				{
					case 1:
						{
							strTemp = "Status Lists";
							lngFCode = modGlobal.CLSECURITYSTATUSLISTS;
							break;
						}
					case 2:
						{
							strTemp = "Redisplay Daily Audit";
							lngFCode = modGlobal.CLSECURITYREPRINTAUDIT;
							break;
						}
					case 3:
						{
							strTemp = "Reminder Notices";
							lngFCode = modGlobal.CLSECURITYREMINDERNOTICES;
							break;
						}
					case 4:
						{
							strTemp = "Tax Club Reports";
							// Activity
							lngFCode = modGlobal.CLSECURITYTAXCLUBREPORTS;
							break;
						}
					case 5:
						{
							strTemp = "Print Foreclosure Report";
							lngFCode = modGlobal.CLSECURITYFORECLOSUREREPORT;
							break;
						}
					case 6:
						{
							strTemp = "Print Lien Discharge Notice";
							lngFCode = modGlobal.CLSECUTIRYPRINTLIENDISCHARGE;
							break;
						}
					case 7:
						{
							strTemp = "Print Cert Of Settlement";
							lngFCode = modGlobal.CLSECURITYPRINTCERTOFSETTLEMENT;
							break;
						}
					case 8:
						{
							strTemp = "Print Cert Of Recommitment";
							lngFCode = modGlobal.CLSECURITYPRINTCERTOFRECOMMITMENT;
							break;
						}
					case 9:
						{
							strTemp = "Loadback Report";
							lngFCode = modGlobal.PRINTLOADBACKREPORT;
							break;
						}
					case 10:
						{
							strTemp = "Reprint Last Purge Report";
							lngFCode = modGlobal.CLSECURITYREPRINTPURGE;
							break;
						}
					case 11:
						{
							strTemp = "Reprint Last CMF Report";
							lngFCode = modGlobal.CLSECURITYREPRINTCMFREPORT;
							break;
						}
					case 12:
						{
							strTemp = "Print Book Page Report";
							lngFCode = modGlobal.CLSECURITYPRINTBOOKPAGEREPORT;
							break;
						}
					case 13:
						{
							// MAL@20080528: Added new report
							// Tracker Reference: 13784
							strTemp = "Print Tax Rate Report";
							lngFCode = modGlobal.CLSECURITYPRINTTAXRATEREPORT;
							// strTemp = "X. Return to Main Menu"
							// lngFCode = 0
							break;
						}
					case 14:
						{
							strTemp = "Partial Payment Waivers";
							lngFCode = modGlobal.CLSECURITYREMINDERNOTICES;
							break;
						}
                    case 15:
                    {
                        strTemp = "Payment Activity Report";
                        lngFCode = modGlobal.CLSECURITYPAYMENTACTIVITYREPORT;
                        break;
                    }
                        //case 15:
                        //    {
                        //        strTemp = "X. Return to Main Menu";
                        //        lngFCode = 0;
                        //        break;
                        //    }
                        //default:
                        //    {
                        //        strTemp = "";
                        //        break;
                        //    }
                }
				//end switch
				if (lngFCode != 0)
				{
					//PPJ:FINAL:MHO: #i108 - correct setting of boolDisable
					boolDisable = !modSecurityValidation.ValidPermissions(this, lngFCode, false);
				}
				FCMenuItem item = parent.SubItems.Add(strTemp, "Menu" + CurRow, menu, !boolDisable, 2);
				switch (CurRow)
				{
					case 4:
						{
							GetTaxClubPrintMenu(item);
							break;
						}
				}
			}
			LabelNumber[Strings.Asc("1")] = 1;
			LabelNumber[Strings.Asc("2")] = 2;
			LabelNumber[Strings.Asc("3")] = 3;
			LabelNumber[Strings.Asc("4")] = 4;
			LabelNumber[Strings.Asc("5")] = 5;
			LabelNumber[Strings.Asc("6")] = 6;
			LabelNumber[Strings.Asc("7")] = 7;
			LabelNumber[Strings.Asc("8")] = 8;
			LabelNumber[Strings.Asc("9")] = 9;
			LabelNumber[Strings.Asc("A")] = 10;
			LabelNumber[Strings.Asc("B")] = 11;
			LabelNumber[Strings.Asc("C")] = 12;
			LabelNumber[Strings.Asc("D")] = 13;
			LabelNumber[Strings.Asc("E")] = 14;
            LabelNumber[Strings.Asc("F")] = 15;
			LabelNumber[Strings.Asc("X")] = 16;
		}

		private void GetTaxClubPrintMenu(FCMenuItem parent)
		{
			string strTemp = "";
			int lngFCode = 0;
			string menu = "TAXCLUB";
			bool boolDisable = false;
			if (modStatusPayments.Statics.boolRE)
			{
				App.MainForm.Text = strTrio + " - RE Collections   [Tax Club Activity Printing]";
			}
			else
			{
				App.MainForm.Text = strTrio + " - PP Collections   [Tax Club Activity Printing]";
			}
			for (int CurRow = 1; CurRow <= 9; CurRow++)
			{
				//PPJ:FINAL:MHO: #i108 - reset boolDisable
				boolDisable = false;
				switch (CurRow)
				{
					case 1:
						{
							strTemp = "All Accounts";
							break;
						}
					case 2:
						{
							strTemp = "Negative Balance";
							break;
						}
					case 3:
						{
							strTemp = "Positive Balance";
							break;
						}
					case 4:
						{
							strTemp = "Zero Balance";
							break;
						}
					case 5:
						{
							strTemp = "Overdue Balances";
							break;
						}
					case 6:
						{
							strTemp = "Payment Book";
							break;
						}
					case 7:
						{
							strTemp = "Master Listing";
							break;
						}
					case 8:
						{
							strTemp = "Active Master Listing";
							break;
						}
					case 9:
						{
							strTemp = "Inactive Master Listing";
							break;
						}
				//case 10:
				//    {
				//        strTemp = "X. Return to Printing Menu";
				//        lngFCode = 0;
				//        break;
				//    }
				//default:
				//    {
				//        strTemp = "";
				//        break;
				//    }
				}
				//end switch
				if (lngFCode != 0)
				{
					boolDisable = modSecurityValidation.ValidPermissions(this, lngFCode, false);
				}
				parent.SubItems.Add(strTemp, "Menu" + CurRow, menu, !boolDisable, 3);
			}
			LabelNumber[Strings.Asc("1")] = 1;
			LabelNumber[Strings.Asc("2")] = 2;
			LabelNumber[Strings.Asc("3")] = 3;
			LabelNumber[Strings.Asc("4")] = 4;
			LabelNumber[Strings.Asc("5")] = 5;
			LabelNumber[Strings.Asc("6")] = 6;
			LabelNumber[Strings.Asc("7")] = 7;
			LabelNumber[Strings.Asc("8")] = 8;
			LabelNumber[Strings.Asc("9")] = 9;
			LabelNumber[Strings.Asc("X")] = 10;
		}

		private void GetPrintMortMenu()
		{
			string menu = "PrintMort";
			if (modStatusPayments.Statics.boolRE)
			{
				App.MainForm.Text = strTrio + " - RE Collections   [Print Mortgage Holder]";
			}
			else
			{
				App.MainForm.Text = strTrio + " - PP Collections   [Print Mortgage Holder]";
			}
			string strTemp = "";
			for (int CurRow = 1; CurRow <= 4; CurRow++)
			{
				switch (CurRow)
				{
					case 1:
						{
							strTemp = "Mortgage Holder Master";
							break;
						}
					case 2:
						{
							strTemp = "R/E List With Mortgage Holders";
							break;
						}
					case 3:
						{
							strTemp = "R/E & P/P Association List";
							break;
						}
					case 4:
						{
							strTemp = "List R/E Accounts By Mortgage Holder";
							break;
						}
				//case 5:
				//    {
				//        strTemp = "ExitTax Collections";
				//        break;
				//    }
				//default:
				//    {
				//        strTemp = "";
				//        break;
				//    }
				}
				//end switch
				App.MainForm.NavigationMenu.CurrentItem.SubItems.Add(strTemp, "Menu" + CurRow, menu);
			}
			LabelNumber[Strings.Asc("1")] = 1;
			LabelNumber[Strings.Asc("2")] = 2;
			LabelNumber[Strings.Asc("3")] = 3;
			LabelNumber[Strings.Asc("4")] = 4;
			LabelNumber[Strings.Asc("X")] = 5;
		}

		public void Menu1()
		{
            var menuItem = Strings.UCase(App.MainForm.NavigationMenu.CurrentItem.Menu);

			try
            {
                switch (menuItem)
                {
                    // On Error GoTo ERROR_HANDLER
                    case "MAIN":
                    {
                        //frmCLGetAccount.InstancePtr.txtHold.Text = "S";
                        //frmCLGetAccount.InstancePtr.Show(App.MainForm);
                        var billType = modStatusPayments.Statics.boolRE
                            ? PropertyTaxBillType.Real
                            : PropertyTaxBillType.Personal;
                        commandDispatcher.Send(new ShowTaxAccountSearch(billType));

                        break;
                    }
                    case "PRINT":
                        PrintActions(1);

                        break;
                    case "FILE":
                        // Customize
                        frmCustomize.InstancePtr.Show(App.MainForm);
                        App.DoEvents();
                        frmCustomize.InstancePtr.Focus();

                        break;
                    case "LIEN":
                        // Lien Process Edit Report
                        frmRateRecChoice.InstancePtr.Close();
                        // kk11132015 trocl-1007 / trocl-11289   Force to reload for regular bills only
                        frmRateRecChoice.InstancePtr.intRateType = 1;
                        frmRateRecChoice.InstancePtr.Show(App.MainForm);

                        break;
                    case "PRINTLIEN":
                        break;
                    case "30DAYNOTICE":
                        // print the 30 day notices
                        modCustomReport.Statics.strReportType = "30DAYNOTICE";
                        frmRateRecChoice.InstancePtr.Close();
                        // kk11132015 trocl-1007 / trocl-11289   Force to reload for regular bills only
                        frmRateRecChoice.InstancePtr.intRateType = 2;
                        frmRateRecChoice.InstancePtr.Show(App.MainForm);

                        break;
                    case "LIENPROCESS":
                        // Print Tax Lien
                        modCustomReport.Statics.strReportType = "LIENPROCESS";
                        frmRateRecChoice.InstancePtr.Close();
                        // kk11132015 trocl-1007 / trocl-11289   Force to reload to create lien rate
                        frmRateRecChoice.InstancePtr.intRateType = 3;
                        frmRateRecChoice.InstancePtr.Show(App.MainForm);

                        break;
                    case "LIENMATURITY":
                        // Print Lien Maturity Notice
                        modCustomReport.Statics.strReportType = "LIENMATURITY";
                        frmRateRecChoice.InstancePtr.Close();
                        // kk11132015 trocl-1007 / trocl-11289   Force to reload for liens only
                        frmRateRecChoice.InstancePtr.intRateType = 4;
                        frmRateRecChoice.InstancePtr.Show(App.MainForm);

                        break;
                    case "TAXCLUB":
                        // All Accounts
                        rptTaxClubActivity.InstancePtr.Init(1);

                        break;
                    default:
                        GetCollectionsMenu();

                        break;
                }

                return;
            }
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error IN Menu 1");
			}
		}

		public void Menu2()
		{
            var menuItem = Strings.UCase(App.MainForm.NavigationMenu.CurrentItem.Menu);

			try
            {
                switch (menuItem)
                {
                    // On Error GoTo ERROR_HANDLER
                    case "MAIN" when modGlobalConstants.Statics.gboolCR:
                        FCMessageBox.Show("Please use TRIO Cash Receipts to enter payments and adjustments.", MsgBoxStyle.Information, "Payments and Adjustments");

                        break;

                    //frmCLGetAccount.InstancePtr.Show(App.MainForm);
                    //frmCLGetAccount.InstancePtr.txtHold.Text = "P";
                    //App.DoEvents();
                    case "MAIN" when modStatusPayments.Statics.boolRE:
                        commandDispatcher.Send(new MakeRealEstateTransaction(Guid.NewGuid(), Guid.NewGuid(), true,0));

                        break;
                    case "MAIN":
                        commandDispatcher.Send(new MakePersonalPropertyTransaction(Guid.NewGuid(), Guid.NewGuid(), true,0));

                        break;
                    case "PRINT":
                        PrintActions(2);
                        // Redisplay Daily Audit Report
                        // MAL@20070907: Removed "Collections" from Caption
                        // frmRPTViewer.Init "LastCLDailyAudit", "Redisplay Daily Audit", 1
                        // If boolRE Then
                        // frmRPTViewer.Init "LastREDailyAudit", "Redisplay Real Estate Daily Audit"
                        // Else
                        // frmRPTViewer.Init "LastPPDailyAudit", "Redisplay Personal Property Daily Audit"
                        // End If
                        // DoEvents
                        // If frmRPTViewer.Visible And frmRPTViewer.Enabled Then
                        // frmRPTViewer.SetFocus
                        // End If
                        break;
                    case "FILE":
                        // Check Database Structure
                        frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Checking Structure");
                        // 
                        modGlobal.NewVersion(true);
                        frmWait.InstancePtr.Unload();

                        break;
                    case "LIEN":
                        // Print 30 Day Process
                        //Get30DayNoticeMenu();
                        break;
                    case "PRINTLIEN":
                        break;
                    case "30DAYNOTICE":
                        // Print Certified Mail Forms/Labels
                        frmRateRecChoice.InstancePtr.Close();
                        // kk11132015 trocl-1007 / trocl-11289   Force to reload for regular bills only
                        frmRateRecChoice.InstancePtr.intRateType = 10;
                        frmRateRecChoice.InstancePtr.Show(App.MainForm);

                        break;
                    case "LIENPROCESS":
                        // Print Certified Mail Forms/Labels
                        frmRateRecChoice.InstancePtr.Close();
                        // kk11132015 trocl-1007 / trocl-11289   Force to reload for regular bills only
                        frmRateRecChoice.InstancePtr.intRateType = 11;
                        frmRateRecChoice.InstancePtr.Show(App.MainForm);

                        break;
                    case "LIENMATURITY":
                        // Print Certified Mail Forms/Labels
                        frmRateRecChoice.InstancePtr.Close();
                        // kk11132015 trocl-1007 / trocl-11289   Force to reload for liens only
                        frmRateRecChoice.InstancePtr.intRateType = 12;
                        frmRateRecChoice.InstancePtr.Show(App.MainForm);

                        break;
                    case "TAXCLUB":
                        // Negative Balances
                        rptTaxClubActivity.InstancePtr.Init(2);

                        break;
                    default:
                        GetCollectionsMenu();

                        break;
                }

                return;
            }
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error IN Menu 2");
			}
		}

		public void Menu3()
		{
            var menuItem = Strings.UCase(App.MainForm.NavigationMenu.CurrentItem.Menu);

			try
            {
                switch (menuItem)
                {
                    // On Error GoTo ERROR_HANDLER
                    case "MAIN":
                        // R/E & P/P Daily Audit Report
                        frmCLDailyAudit.InstancePtr.Show(App.MainForm);
                        App.DoEvents();
                        frmCLDailyAudit.InstancePtr.Focus();

                        break;
                    case "PRINT":
                        PrintActions(3);
                        // Reminder Notice
                        // frmRateRecChoice.intRateType = 30
                        // frmRateRecChoice.Show , MDIParent
                        break;
                    case "FILE":
                        // Tax Service Extract
                        frmTaxService.InstancePtr.Show(App.MainForm);

                        break;
                    case "LIEN":
                        // Tax Lien Process
                        //GetLienProcessMenu();
                        break;
                    case "PRINTLIEN":
                        break;
                    case "30DAYNOTICE":
                        // Apply Demand Fees
                        frmRateRecChoice.InstancePtr.Close();
                        // kk11132015 trocl-1007 / trocl-11289   Force to reload for regular bills only
                        frmRateRecChoice.InstancePtr.intRateType = 5;
                        frmRateRecChoice.InstancePtr.Show(App.MainForm);
                        // frmApplyDemandFees.Show , MDIParent
                        break;
                    case "LIENPROCESS":
                        // Transfer Tax to Lien
                        frmRateRecChoice.InstancePtr.Close();
                        // kk11132015 trocl-1007 / trocl-11289   Force to reload for regular bills only
                        frmRateRecChoice.InstancePtr.intRateType = 6;
                        frmRateRecChoice.InstancePtr.Show(App.MainForm);

                        break;
                    case "LIENMATURITY":
                        // Apply Lien Maturity Notices
                        frmRateRecChoice.InstancePtr.Close();
                        // kk11132015 trocl-1007 / trocl-11289   Force to reload for liens only
                        frmRateRecChoice.InstancePtr.intRateType = 7;
                        frmRateRecChoice.InstancePtr.Show(App.MainForm);

                        break;
                    case "TAXCLUB":
                        // Positive Accounts
                        rptTaxClubActivity.InstancePtr.Init(3);

                        break;
                    default:
                        GetCollectionsMenu();

                        break;
                }

                return;
            }
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error IN Menu 3");
			}
		}

		public void Menu4()
		{
            var menuItem = Strings.UCase(App.MainForm.NavigationMenu.CurrentItem.Menu);

			try
            {
                switch (menuItem)
                {
                    // On Error GoTo ERROR_HANDLER
                    case "MAIN":
                        // R/E - Load Of Back Information
                        frmRateRecChoice.InstancePtr.Close();
                        // kk11132015 trocl-1007 / trocl-11289   Force to reload for regular bills only
                        frmRateRecChoice.InstancePtr.intRateType = 100;
                        frmRateRecChoice.InstancePtr.Show(App.MainForm);

                        break;
                    case "PRINT":
                        PrintActions(4);
                        // Tax Club Activity List
                        // GetTaxClubPrintMenu
                        break;
                    case "FILE":
                        // Database Extract
                        frmDatabaseExtract.InstancePtr.Show(App.MainForm);

                        break;
                    case "LIEN":
                        // Lien Maturity Process
                        //GetLienMaturityMenu();
                        break;
                    case "MORTGAGE":
                        // Print Listing
                        //GetPrintMortMenu();
                        break;
                    case "PRINTLIEN":
                        break;
                    case "30DAYNOTICE":
                        // Reprint Labels
                        frmRPTViewer.InstancePtr.Init("LastCL30DayLabels1", "Reprint 30 Day Notice Labels", 12);

                        break;
                    case "LIENPROCESS":
                        // Reprint Labels
                        frmRPTViewer.InstancePtr.Init("LastCLTransferToLienLabels1", "Reprint Transfer To Lien Labels", 12);

                        break;
                    case "LIENMATURITY":
                        // Reprint Labels
                        frmRPTViewer.InstancePtr.Init("LastCLLienMaturityLabels1", "Reprint Lien Maturity Labels", 12);

                        break;
                    case "TAXCLUB":
                        // Zero Balances
                        rptTaxClubActivity.InstancePtr.Init(4);

                        break;
                    default:
                        GetCollectionsMenu();

                        break;
                }

                return;
            }
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error IN Menu 4");
			}
		}

		public void Menu5()
		{
            var menuItem = Strings.UCase(App.MainForm.NavigationMenu.CurrentItem.Menu);

			try
            {
                switch (menuItem)
                {
                    // On Error GoTo ERROR_HANDLER
                    case "MAIN":
                        // Add Update Tax Club
                        frmTaxClub.InstancePtr.Init();

                        break;
                    case "PRINT":
                        PrintActions(5);
                        // Tax Acquired Report
                        // frmReportViewer.Init rptTaxAcquiredProperties  '.Show , MDIParent"
                        break;
                    case "FILE":
                        // Edit Rate Key Information
                        frmRateRecChoice.InstancePtr.Close();
                        // kk11132015 trocl-1007 / trocl-11289   Force to reload for regular bills only
                        frmRateRecChoice.InstancePtr.intRateType = 200;
                        frmRateRecChoice.InstancePtr.Show(App.MainForm);

                        break;
                    case "LIEN":
                        // Remove An account from lien status
                        frmRemoveLien.InstancePtr.Show(App.MainForm);

                        break;
                    case "PRINTLIEN":
                        // Apply Lien Maturity Fees to Accounts
                        FCMessageBox.Show("This option is not completed.", MsgBoxStyle.Information, "Not Complete");

                        break;
                    case "30DAYNOTICE":
                        // Reprint Certified Mail Forms
                        frmReprintCMForm.InstancePtr.Init(0);

                        break;
                    case "LIENPROCESS":
                        // Reprint Certified Mail Forms
                        frmReprintCMForm.InstancePtr.Init(1);

                        break;
                    case "LIENMATURITY":
                        // Reprint Certified Mail Forms
                        frmReprintCMForm.InstancePtr.Init(2);

                        break;
                    case "TAXCLUB":
                    {
                        // Tax Club Overdue Balances
                        string strTemp = "";
                        frmTaxClubOutstanding.InstancePtr.Init(); // trocl-1367 8.25.17 kjr Add new form with Options.

                        break;
                    }
                    default:
                        GetCollectionsMenu();

                        break;
                }

                return;
            }
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error IN Menu 5");
			}
		}

		private void PrintActions(int intChoice)
		{
			switch (intChoice)
			{
				case 1:
                {
                    ITrioContextFactory contextFactory;
                    ISavedStatusReportsRepository reportsRepository;
                    IBillingMasterRepository billsRepository;
                    IPropertyTaxStatusQueryHandler statusQueryHandler;
                    IPropertyTaxBillCalculator taxBillCalculator;
                    IDateTimeService dateTimeService;
                    var billType = modStatusPayments.Statics.boolRE
                        ? PropertyTaxBillType.Real
                        : PropertyTaxBillType.Personal;
                        using (var scope = diContainer.BeginLifetimeScope())
                    {
                        contextFactory = scope.Resolve<ITrioContextFactory>();
                        reportsRepository = scope.Resolve<ISavedStatusReportsRepository>();

                        var taxContext = contextFactory.GetTaxCollectionsContext();

                        if (billType == PropertyTaxBillType.Real)
                        {
                            billsRepository =
                                scope.Resolve<IREBillingMasterRepository
                                >();
                        }
                        else
                        {
                            billsRepository = scope.Resolve<IPPBillingMasterRepository>();
                        }

                        statusQueryHandler = scope.Resolve<IPropertyTaxStatusQueryHandler>();
                        taxBillCalculator = scope.Resolve<IPropertyTaxBillCalculator>();
                        dateTimeService = scope.Resolve<IDateTimeService>();
                    }

						
                    var statusListViewModel = new StatusListViewModel(billType, reportsRepository, billsRepository, contextFactory,statusQueryHandler,taxBillCalculator,commandDispatcher,dateTimeService);
                        var statusListsForm = new frmStatusLists(statusListViewModel);
                        statusListsForm.Show(App.MainForm);
                        App.DoEvents();
                        statusListsForm.Focus();
                        break;
                    }
				case 2:
					{
						// Redisplay Daily Audit Report
						// MAL@20070907: Removed "Collections" from Caption
						frmRPTViewer.InstancePtr.Init("LastCLDailyAudit", "Redisplay Daily Audit", 1);
						// If boolRE Then
						// frmRPTViewer.Init "LastREDailyAudit", "Redisplay Real Estate Daily Audit"
						// Else
						// frmRPTViewer.Init "LastPPDailyAudit", "Redisplay Personal Property Daily Audit"
						// End If
						App.DoEvents();
						if (frmRPTViewer.InstancePtr.Visible && frmRPTViewer.InstancePtr.Enabled)
						{
							frmRPTViewer.InstancePtr.Focus();
						}
						break;
					}
				case 3:
					{
						// Reminder Notice
						frmRateRecChoice.InstancePtr.Close();
						// kk11132015 trocl-1007 / trocl-11289   Force to reload for regular bills only
						frmRateRecChoice.InstancePtr.intRateType = 30;
						frmRateRecChoice.InstancePtr.Show(App.MainForm);
						break;
					}
				case 4:
					{
						// Tax Club Activity List
						//GetTaxClubPrintMenu();
						break;
					}
				case 5:
					{
						// Tax Acquired Report
						frmReportViewer.InstancePtr.Init(rptTaxAcquiredProperties.InstancePtr);
						// .Show , MDIParent"
						break;
					}
				case 6:
					{
						// Print Lien Discharge Notices
						frmLienDischargeGetAcct.InstancePtr.Show(App.MainForm);
						break;
					}
				case 7:
					{
						// Certificate of Settlement
						frmCertOfSettlement.InstancePtr.Init(0);
						break;
					}
				case 8:
					{
						// Certificate of Recommitment
						frmCertOfSettlement.InstancePtr.Init(1);
						break;
					}
				case 9:
					{
						// Loadback Report
						frmReportViewer.InstancePtr.Init(rptLoadbackReportMaster.InstancePtr);
						break;
					}
				case 10:
					{
						// Reprint Purge List
						frmReprintPurgeList.InstancePtr.Show(App.MainForm);
						break;
					}
				case 11:
					{
						// Reprint Last CMF Report
						frmRPTViewer.InstancePtr.Init("CLCMFList1", "Reprint of Last CMF Report", 12);
						break;
					}
				case 12:
					{
						// Book Page Report
						frmBookPageReport.InstancePtr.Init(0);
						break;
					}
				case 13:
					{
						frmTaxRateReport.InstancePtr.Show(App.MainForm);
						break;
					}
				case 14:
					{
						frmReprintPartialPaymentWaiver.InstancePtr.Show(App.MainForm);
						break;
					}
                case 15:
                    {
                        var contextFactory = GetContextFactory();
                        var taxContext = contextFactory.GetTaxCollectionsContext();
                        var billType = modStatusPayments.Statics.boolRE ? PropertyTaxBillType.Real : PropertyTaxBillType.Personal;
                        IBillingMasterRepository billsRepository;
                        if (billType == PropertyTaxBillType.Real)
                        {
                            billsRepository = new REBillingMasterRepository(taxContext);
                        }
                        else
                        {
                            billsRepository = new PPBillingMasterRepository(taxContext);
                        }
                        var pmtActivityViewModel = new PmtActivityViewModel(billType,  billsRepository, contextFactory);
                        var pmtActivityForm = new frmPmtActivityReport(pmtActivityViewModel);
                        pmtActivityForm.Show(App.MainForm);
                        App.DoEvents();
                       // pmtActivityForm.Focus();
                        break;
                    }
                case 16:
					{
						GetCollectionsMenu();
						break;
					}
			}
			//end switch
		}

        private static TrioContextFactory GetContextFactory()
        {
            var dataDetails = new SharedDataAccess.DataContextDetails();
            dataDetails.DataSource = StaticSettings.gGlobalSettings.DataSource;
            dataDetails.DataEnvironment = StaticSettings.gGlobalSettings.DataEnvironment;
            dataDetails.Password = StaticSettings.gGlobalSettings.Password;
            dataDetails.UserID = StaticSettings.gGlobalSettings.UserName;
            var contextFactory = new TrioContextFactory(dataDetails, StaticSettings.gGlobalSettings);
            return contextFactory;
        }

        public void Menu6()
		{
            var menuItem = Strings.UCase(App.MainForm.NavigationMenu.CurrentItem.Menu);

			try
            {
                switch (menuItem)
                {
                    // On Error GoTo ERROR_HANDLER
                    case "MAIN":
                        // Mortgage Holder Information
                        // If gboolBL Or gboolRE Or gboolUT Then
                        App.DoEvents();
                        frmGetMortgage.InstancePtr.Show(App.MainForm);
                        // Else
                        // MsgBox "This option is not available without TRIO Real Estate Billing or TRIO Utility Billing.", MsgBoxStyle.Information, "Mortgage Holder Information"
                        // End If
                        break;
                    case "PRINT":
                        // Print Lien Discharge Notices
                        // frmLienDischargeGetAcct.Show , MDIParent
                        PrintActions(6);

                        break;
                    case "FILE":
                        // Edit Bill Information
                        frmCLEditBillInfo.InstancePtr.Show(App.MainForm);

                        break;
                    case "LIEN":
                        // Reverse Demand Fees
                        frmRateRecChoice.InstancePtr.Close();
                        // kk11132015 trocl-1007 / trocl-11289   Force to reload for regular bills only
                        frmRateRecChoice.InstancePtr.intRateType = 60;
                        frmRateRecChoice.InstancePtr.Show(App.MainForm);

                        break;
                    case "Mortgage":
                        // Transfer Info From P/P To Here
                        FCMessageBox.Show("This option is not completed.", MsgBoxStyle.Information, "Not Complete");

                        break;
                    case "PRINTLIEN":
                        // Back to previous menu
                        //GetLienMenu();
                        break;
                    case "30DAYNOTICE":
                        // Reprint Applied Demand Fees List
                        frmRPTViewer.InstancePtr.Init("LastCLDemandFeesList", "Reprint Demand Fees List", 13);
                        App.DoEvents();
                        frmRPTViewer.InstancePtr.Focus();

                        break;
                    case "LIENPROCESS":
                        // Reprint The Last Transfer To Lien Report
                        frmRPTViewer.InstancePtr.Init("LastCLTranfserToLien", "Reprint Transfer Report", 13);
                        App.DoEvents();
                        frmRPTViewer.InstancePtr.Focus();

                        break;
                    case "LIENMATURITY":
                        // Reprint Applied Lien Maturity List
                        frmRPTViewer.InstancePtr.Init("LastCLLienMaturityList", "Reprint Maturity Fees List", 13);
                        App.DoEvents();
                        frmRPTViewer.InstancePtr.Focus();

                        break;
                    case "TAXCLUB":
                        // Tax Club Payment Book
                        modGlobal.TaxClubBooklet();

                        break;
                    default:
                        GetCollectionsMenu();

                        break;
                }

                return;
            }
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error IN Menu 6");
			}
		}

		public void Menu7()
		{
            var menuItem = Strings.UCase(App.MainForm.NavigationMenu.CurrentItem.Menu);

			try
            {
                switch (menuItem)
                {
                    // On Error GoTo ERROR_HANDLER
                    case "MAIN":
                        // MAL@20071227: Changed to add Interested Parties
                        // Call Reference: 104274
                        // Printing menu
                        // GetPrintMenu
                        App.DoEvents();
                        frmGetInterestedParties.InstancePtr.Show(App.MainForm);

                        break;
                    case "PRINT":
                        PrintActions(7);
                        // Certificate of Settlement
                        // frmCertOfSettlement.Init 0
                        break;
                    case "FILE":
                        // Purge Collection Files
                        frmPurge.InstancePtr.Show(App.MainForm);
                        App.DoEvents();
                        frmPurge.InstancePtr.Focus();

                        break;
                    case "LIEN":
                        // Edit the book and pag e of lien records
                        frmEditBookPage.InstancePtr.Init(0);

                        break;
                    case "PRINTLIEN":
                        break;
                    case "30DAYNOTICE":
                        // frmRemove30DNFee.Show , MDIParent
                        //GetLienMenu();
                        break;
                    case "LIENPROCESS":
                        // Back to previous menu
                        //GetLienMenu();
                        break;
                    case "LIENMATURITY":
                        frmRPTViewer.InstancePtr.Init("LastCLBankruptcy", "Reprint Bankruptcy Report", 11);
                        //frmRPTViewer.InstancePtr.Show(App.MainForm);
                        break;
                    case "TAXCLUB":
                        // Master Listing
                        rptTaxClubMaster.InstancePtr.Init(0, 1);

                        break;
                    default:
                        GetCollectionsMenu();

                        break;
                }

                return;
            }
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error IN Menu 7");
			}
		}

		public void Menu8()
		{
            var menuItem = Strings.UCase(App.MainForm.NavigationMenu.CurrentItem.Menu);

			try
            {
                switch (menuItem)
                {
                    // On Error GoTo ERROR_HANDLER
                    case "MAIN":
                        // MAL@20071227: 104274
                        // If boolRE Then
                        // R/E Lein Process
                        // GetLienMenu
                        // Else
                        // MsgBox "To use the Real Estate Lien Process, exit Personal Property Collections and use the Real Estate Collections option.", MsgBoxStyle.Information, "Not Available"
                        // End If
                        //GetPrintMenu();
                        break;
                    case "PRINT":
                        PrintActions(8);
                        // Certificate of Recommitment
                        // frmCertOfSettlement.Init 1
                        break;
                    case "FILE":
                        // clear the CMF numbers
                        modGlobal.ClearCMFNumbers();

                        break;
                    case "LIEN":
                        // Edit LDN Book / Page
                        frmEditBookPage.InstancePtr.Init(1);

                        break;
                    case "PRINTLIEN":
                        break;
                    case "30DAYNOTICE":
                        // Back to previous menu
                        //GetLienMenu();
                        break;
                    case "LIENPROCESS":
                        break;
                    case "LIENMATURITY":
                        // remove lien maturity fees
                        frmRemoveLienMatFee.InstancePtr.Show(App.MainForm);

                        break;
                    case "TAXCLUB":
                        // Active Master Listing
                        rptTaxClubMaster.InstancePtr.Init(0, 2);

                        break;
                    default:
                        GetCollectionsMenu();

                        break;
                }

                return;
            }
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error IN Menu 8");
			}
		}

		public void Menu9()
		{
            var menuItem = Strings.UCase(App.MainForm.NavigationMenu.CurrentItem.Menu);

			try
            {
                switch (menuItem)
                {
                    // On Error GoTo ERROR_HANDLER
                    // MAL@20071227: 104274
                    // File Maintenance
                    // GetFileMenu
                    case "MAIN" when modStatusPayments.Statics.boolRE:
                        // R/E Lein Process
                        //GetLienMenu();
                        break;
                    case "MAIN":
                        FCMessageBox.Show("To use the Real Estate Lien Process, exit Personal Property Collections and use the Real Estate Collections option.", MsgBoxStyle.Information, "Not Available");

                        break;
                    case "PRINT":
                        PrintActions(9);
                        // Loadback Report
                        // frmReportViewer.Init rptLoadbackReportMaster
                        break;
                    case "FILE":
                        // Remove From Tax Acquired
                        frmTaxAcquiredRemoval.InstancePtr.Init();

                        break;
                    case "LIEN":
                        // Lien Dates Chart
                        frmLienDates.InstancePtr.Show(App.MainForm);

                        break;
                    case "PRINTLIEN":
                        break;
                    case "30DAYNOTICE":
                        break;
                    case "LIENPROCESS":
                        break;
                    case "LIENMATURITY":
                        // this will take the user back one level
                        //GetLienMenu();
                        break;
                    case "TAXCLUB":
                        // Master Listing
                        rptTaxClubMaster.InstancePtr.Init(0, 3);

                        break;
                    default:
                        GetCollectionsMenu();

                        break;
                }

                return;
            }
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error IN Menu 9");
			}
		}

		public void Menu10()
		{
            var menuItem = Strings.UCase(App.MainForm.NavigationMenu.CurrentItem.Menu);

			try
            {
                switch (menuItem)
                {
                    // On Error GoTo ERROR_HANDLER
                    case "MAIN":
                        // MAL@20071227: 104274
                        // exit CL back to General Entry Menu
                        // gboolLeaveProject = True
                        // Close
                        // Unload Me
                        // File Maintenance
                        //GetFileMenu();
                        break;
                    case "PRINT":
                        PrintActions(10);
                        // Reprint Purge List
                        // frmReprintPurgeList.Show
                        break;
                    case "FILE":
                        GetCollectionsMenu();

                        break;
                    case "LIEN":
                        // Updated Book Page Report
                        rptUpdatedBookPage.InstancePtr.Init();

                        break;
                    case "PRINTLIEN":
                        break;
                    case "30DAYNOTICE":
                        break;
                    case "LIENPROCESS":
                        break;
                    case "LIENMATURITY":
                        break;
                }

                //else if (menuItem == "TAXCLUB")
				//{
				//    // Back to print menu
				//    GetPrintMenu();
				//}
				//else
				//{
				//    GetCollectionsMenu();
				//}
				return;
            }
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error IN Menu 10");
			}
		}

		public void Menu11()
		{
            var menuItem = Strings.UCase(App.MainForm.NavigationMenu.CurrentItem.Menu);

			try
            {
                switch (menuItem)
                {
                    // On Error GoTo ERROR_HANDLER
                    case "MAIN":
                        //var billType = modStatusPayments.Statics.boolRE
                        //    ? PropertyTaxBillType.Real
                        //    : PropertyTaxBillType.Personal;
                        //commandDispatcher.Send(new ShowTaxAccountSearch(billType));                    

                        //modGlobal.Statics.gboolLeaveProject = true;
                        //FCFileSystem.FileClose();
                        break;
                    case "PRINT":
                        PrintActions(11);
                        // Reprint Last CMF Report
                        // frmRPTViewer.Init "CLCMFList1", "Reprint of Last CMF Report", 12
                        break;
                    case "FILE":
                        GetCollectionsMenu();

                        break;
                    case "LIEN":
                        // Tax Acquired Properties
                        frmTaxAcquiredList.InstancePtr.Show(App.MainForm);

                        break;
                    case "PRINTLIEN":
                        break;
                    case "30DAYNOTICE":
                        break;
                    case "LIENPROCESS":
                        break;
                    case "LIENMATURITY":
                        break;
                    default:
                        GetCollectionsMenu();

                        break;
                }

                return;
            }
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error IN Menu 11");
			}
		}

		public void Menu12()
		{
            var menuItem = Strings.UCase(App.MainForm.NavigationMenu.CurrentItem.Menu);

			try
            {
                switch (menuItem)
                {
                    // On Error GoTo ERROR_HANDLER
                    case "MAIN":
                        modGlobal.Statics.gboolLeaveProject = true;
                        FCFileSystem.FileClose();

                        break;
                    case "FILE":
                        break;
                    case "PRINT":
                        PrintActions(12);
                        // Book Page Report
                        // frmBookPageReport.Init 0
                        break;
                    case "LIEN":
                        // trocl-565 08-08-2011 kgk   Add Reprint T/A Properties list
                        // Update Bill Addresses
                        // frmUpdateAddresses.Init
                        // Reprint Tax Acquired Properties List
                        frmRPTViewer.InstancePtr.Init("LastTAPropertiesList", "Reprint Tax Acquired Properties List", 13);
                        App.DoEvents();
                        frmRPTViewer.InstancePtr.Focus();

                        break;
                    case "PRINTLIEN":
                        break;
                    case "30DAYNOTICE":
                        break;
                    case "LIENPROCESS":
                        break;
                    case "LIENMATURITY":
                        break;
                    default:
                        GetCollectionsMenu();

                        break;
                }

                return;
            }
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error IN Menu 12");
			}
		}

		public void Menu13()
        {
            var menuItem = Strings.UCase(App.MainForm.NavigationMenu.CurrentItem.Menu);

            switch (menuItem)
            {
                case "MAIN":
                    break;
                case "FILE":
                    break;
                case "LIEN":
                    // trocl-565 08-08-2011 kgk   Add Reprint T/A Properties list - move from Menu12
                    // Back to Previous Menu
                    // GetCollectionsMenu
                    // Update Bill Addresses
                    frmUpdateAddresses.InstancePtr.Init();

                    break;
                case "PRINT":
                    PrintActions(13);
                    // MAL@20080528: Moved to Menu14 to add new report
                    // Tracker Reference: 13784
                    // Back to Previous Menu
                    // GetCollectionsMenu
                    // frmTaxRateReport.Show
                    break;
                case "PRINTLIEN":
                    break;
                default:
                    GetCollectionsMenu();

                    break;
            }
        }

		public void Menu14()
        {
            var menuItem = Strings.UCase(App.MainForm.NavigationMenu.CurrentItem.Menu);

            switch (menuItem)
            {
                case "MAIN":
                    break;
                case "FILE":
                    break;
                case "LIEN":
                    // trocl-565 08-08-2011 kgk   Add Reprint T/A Properties list - move from Menu13
                    // Back to Previous Menu
                    GetCollectionsMenu();

                    break;
                case "PRINT":
                    PrintActions(14);
                    // MAL@20080528: Moved to Menu14 to add new report
                    // Tracker Reference: 13784
                    // Back to Previous Menu
                    // GetCollectionsMenu
                    break;
                case "PRINTLIEN":
                    break;
                default:
                    GetCollectionsMenu();

                    break;
            }
        }

		public void Menu15()
        {
            var menuItem = Strings.UCase(App.MainForm.NavigationMenu.CurrentItem.Menu);

            switch (menuItem)
            {
                case "MAIN":
                    break;
                case "PRINT":
                    PrintActions(15);

                    break;
                case "FILE":
                    break;
                case "LIEN":
                    break;
                case "PRINTLIEN":
                    break;
                default:
                    GetCollectionsMenu();

                    break;
            }
        }

		public void Menu16()
        {
            var menuItem = Strings.UCase(App.MainForm.NavigationMenu.CurrentItem.Menu);

            switch (menuItem)
            {
                case "MAIN":
                    break;
                case "FILE":
                    break;
                case "LIEN":
                    break;
                case "PRINTLIEN":
                    break;
                default:
                    GetCollectionsMenu();

                    break;
            }
        }

		public void Menu17()
        {
            var menuItem = Strings.UCase(App.MainForm.NavigationMenu.CurrentItem.Menu);

            switch (menuItem)
            {
                case "MAIN":
                    break;
                case "FILE":
                    break;
                case "LIEN":
                    break;
                case "PRINTLIEN":
                    break;
                default:
                    GetCollectionsMenu();

                    break;
            }
        }

		public void Menu18()
        {
            var menuItem = Strings.UCase(App.MainForm.NavigationMenu.CurrentItem.Menu);

            switch (menuItem)
            {
                case "MAIN":
                    break;
                case "FILE":
                    break;
                case "LIEN":
                    break;
                case "PRINTLIEN":
                    break;
                default:
                    GetCollectionsMenu();

                    break;
            }
        }

		private void CheckTSReminders()
		{
			// Tracker Reference: 13727
			clsDRWrapper rsRate = new clsDRWrapper();
			clsDRWrapper rsSetting = new clsDRWrapper();
			bool blnCheck = false;
			bool blnFirstRun = false;
			bool blnPrompt = false;
			DateTime dtNewNextRemind;
			DialogResult intAnswer = 0;
			int intCount;
			int intPeriods = 0;
			int lngLastYear = 0;
			string strLastRemind = "";
			string strNextRemind = "";

            try
            {
                rsSetting.OpenRecordset("SELECT * FROM Collections", modExtraModules.strCLDatabase);
                if (rsSetting.RecordCount() > 0)
                {
                    blnCheck = FCConvert.ToBoolean(rsSetting.Get_Fields_Boolean("ShowTSReminder"));
                    if (Information.IsDate(rsSetting.Get_Fields("TSRemindLastDate")))
                    {
                        strLastRemind = FCConvert.ToString(rsSetting.Get_Fields_DateTime("TSRemindLastDate"));
                    }
                    if (Information.IsDate(rsSetting.Get_Fields("TSRemindNextDate")))
                    {
                        strNextRemind = FCConvert.ToString(rsSetting.Get_Fields_DateTime("TSRemindNextDate"));
                    }
                    if (rsSetting.IsFieldNull("TSLastRunYear"))
                    {
                        lngLastYear = FCConvert.ToInt32(rsSetting.Get_Fields_Int32("LastBilledYear"));
                        blnFirstRun = true;
                    }
                    else
                    {
                        lngLastYear = FCConvert.ToInt32(rsSetting.Get_Fields_Int32("TSLastRunYear"));
                        blnFirstRun = false;
                    }
                }
                else
                {
                    // Do Nothing - No Settings Found
                    blnCheck = false;
                }
                if (blnCheck)
                {
                    if (strNextRemind != "" && Convert.ToDateTime(strNextRemind).ToOADate() <= DateTime.Today.ToOADate() && Convert.ToDateTime(strLastRemind).ToOADate() != DateTime.Today.ToOADate())
                    {
                        blnPrompt = true;
                    }
                    else
                    {
                        // Loop through Rate Records
                        rsRate.OpenRecordset("SELECT * FROM RateRec WHERE [Year] >= " + FCConvert.ToString(lngLastYear / 10), modExtraModules.strCLDatabase);
                        if (rsRate.RecordCount() > 0)
                        {
                            rsRate.MoveFirst();
                            while (!(blnPrompt == true || rsRate.EndOfFile()))
                            {
                                if (!blnFirstRun)
                                {
                                    intPeriods = FCConvert.ToInt32(rsRate.Get_Fields_Int16("NumberOfPeriods"));
                                    for (intCount = 1; intCount <= intPeriods; intCount++)
                                    {
                                        // TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
                                        if (intCount == FCConvert.ToDouble(Strings.Right(FCConvert.ToString(lngLastYear), 1)) && ((rsRate.Get_Fields("Year"))) == (lngLastYear / 10))
                                        {
                                            // This Period has already had an extract run for it
                                            blnPrompt = false;
                                        }
                                        // TODO Get_Fields: Check the table for the column [DueDate] and replace with corresponding Get_Field method
                                        else if (Conversion.Val(rsRate.Get_Fields("DueDate" + FCConvert.ToString(intCount))) < DateTime.Today.ToOADate())
                                            {
                                                // Due Date has passed
                                                blnPrompt = false;
                                            }
                                            else
                                            {
                                                if (strLastRemind != "")
                                                {
                                                    // TODO Get_Fields: Check the table for the column [DueDate] and replace with corresponding Get_Field method
                                                    // TODO Get_Fields: Check the table for the column [DueDate] and replace with corresponding Get_Field method
                                                    if (DateAndTime.DateAdd("d", -45, (DateTime)rsRate.Get_Fields("DueDate" + FCConvert.ToString(intCount))).ToOADate() <= DateTime.Today.ToOADate() && DateAndTime.DateAdd("d", -45, (DateTime)rsRate.Get_Fields("DueDate" + FCConvert.ToString(intCount))).ToOADate() > DateAndTime.DateValue(strLastRemind).ToOADate())
                                                    {
                                                        // 45 Day Reminder
                                                        blnPrompt = true;
                                                    }
                                                    else
                                                    {
                                                        blnPrompt = false;
                                                    }
                                                }
                                                else
                                                {
                                                    // TODO Get_Fields: Check the table for the column [DueDate] and replace with corresponding Get_Field method
                                                    if (DateAndTime.DateAdd("d", -45, (DateTime)rsRate.Get_Fields("DueDate" + FCConvert.ToString(intCount))).ToOADate() <= DateTime.Today.ToOADate())
                                                    {
                                                        // 45 Day Reminder
                                                        blnPrompt = true;
                                                    }
                                                    else
                                                    {
                                                        blnPrompt = false;
                                                    }
                                                }
                                            }
                                    }
                                    // intCount
                                }
                                else
                                {
                                    intPeriods = FCConvert.ToInt32(rsRate.Get_Fields_Int16("NumberOfPeriods"));
                                    for (intCount = 1; intCount <= intPeriods; intCount++)
                                    {
                                        // TODO Get_Fields: Check the table for the column [DueDate] and replace with corresponding Get_Field method
                                        if (Conversion.Val(rsRate.Get_Fields("DueDate" + FCConvert.ToString(intCount))) < DateTime.Today.ToOADate())
                                        {
                                            // Due Date has passed
                                            blnPrompt = false;
                                        }
                                        else
                                        {
                                            var oaDate = DateAndTime.DateAdd("d", -45, (DateTime)rsRate.Get_Fields("DueDate" + FCConvert.ToString(intCount))).ToOADate();

                                            blnPrompt = strLastRemind != "" 
                                                ? oaDate <= DateTime.Today.ToOADate() && oaDate > DateAndTime.DateValue(strLastRemind).ToOADate() 
                                                : oaDate <= DateTime.Today.ToOADate();
                                        }
                                    }
                                    // intCount
                                }
                                rsRate.MoveNext();
                            }
                        }
                        else
                        {
                            blnPrompt = false;
                        }
                    }
                    if (blnPrompt)
                    {
                        intAnswer = FCMessageBox.Show("Prepare and Send an updated billing file to CoreLogic?", MsgBoxStyle.YesNo | MsgBoxStyle.Question, "Reminder: Billing File Due");
                        if (intAnswer == DialogResult.Yes)
                        {
                            // Call the Tax Extract form
                            frmTaxService.InstancePtr.Show(App.MainForm);
                        }
                        else
                        {
                            dtNewNextRemind = GetNextBusinessDay(DateAndTime.DateAdd("d", 5, DateTime.Today));
                            FCMessageBox.Show("You will be reminded again if you do not run the extract prior to " + Strings.Format(dtNewNextRemind, "MM/dd/yyyy"), MsgBoxStyle.OkOnly | MsgBoxStyle.Information, "Reminder");
                            rsSetting.Edit();
                            rsSetting.Set_Fields("TSRemindLastDate", DateTime.Today);
                            rsSetting.Set_Fields("TSRemindNextDate", dtNewNextRemind);
                            rsSetting.Update();
                        }
                    }
                }
            }
            finally
            {
                rsRate.DisposeOf();
				rsSetting.DisposeOf();
            }
        }

		private DateTime GetNextBusinessDay(DateTime dtPassDate)
		{
			DateTime GetNextBusinessDay = System.DateTime.Now;
			// Tracker Reference: 13727
			// Get Next Business Day
			bool blnIsWeekday = false;
			DateTime dtNew;
			dtNew = dtPassDate;
			while (!(blnIsWeekday == true))
			{
				if ((DateAndTime.Weekday(dtNew) == FCConvert.ToInt32(DayOfWeek.Sunday)) || (DateAndTime.Weekday(dtNew) == FCConvert.ToInt32(DayOfWeek.Saturday)))
				{
					blnIsWeekday = false;
					dtNew = DateAndTime.DateAdd("d", 1, dtNew);
				}
				else
				{
					blnIsWeekday = true;
				}
			}
			GetNextBusinessDay = dtNew;
			return GetNextBusinessDay;
		}

		private bool RanLienFeeIncr()
		{
			bool RanLienFeeIncr = false;
			// kk 09252013 trocl-1059
			clsDRWrapper rsColl = new clsDRWrapper();
			rsColl.OpenRecordset("SELECT LienFeeIncrRunOn FROM Collections", modExtraModules.strCLDatabase);
			if (!rsColl.IsFieldNull("LienFeeIncrRunOn"))
			{
				RanLienFeeIncr = FCConvert.CBool(Conversion.Val(rsColl.Get_Fields_DateTime("LienFeeIncrRunOn")) != FCConvert.ToDateTime(0).ToOADate());
			}
			else
			{
				RanLienFeeIncr = true;
			}
			return RanLienFeeIncr;
		}

        //private TaxCollectionsContext GetCollectionsContext()
        //{
        //    var dataDetails = StaticSettings.gGlobalSettings.GetContextDetails();
        //    var cf = new TrioContextFactory(dataDetails);
        //    return cf.GetTaxCollectionsContext();
        //}

        public void ExecuteCustom(int code)
        {
            switch (code)
            {
                case (int)CollectionCommands.TaxBillExport:
                    CreateGrayCustomTaxBillExport();
                    break;
                case (int)CollectionCommands.UnifundExport:
					CreateYorkCustomExport();
                    break;
            }
        }

		private void CreateYorkCustomExport()
		{
			frmUnifund.InstancePtr.Show(App.MainForm);
		}

		private void CreateGrayCustomTaxBillExport()
        {           
            var contextFactory = GetContextFactory();
            //var taxRepositoryFactory = new RepositoryFactory(contextFactory.GetTaxCollectionsContext());            
            var taxBillCalculator = diContainer.Resolve<IPropertyTaxBillCalculator>();
            var queryHandler = diContainer.Resolve<IPropertyTaxBillQueryHandler>(); //new PropertyTaxBillQueryHandler(contextFactory.GetTaxCollectionsContext());
            var exportQuery = new TaxBillExportQueryHandler(queryHandler,taxBillCalculator);
            var fileSystem = new System.IO.Abstractions.FileSystem();
            var billExporter = new SimplePropertyTaxBillExporter(fileSystem,exportQuery);
            var taxBillExportForm = new SimpleTaxBillExportForm();
            taxBillExportForm.SetDependencies(billExporter);
            taxBillExportForm.Show(App.MainForm);
        }
        private enum CollectionCommands
        {
            TaxBillExport = 1,
            UnifundExport = 2
        }


    }
}
