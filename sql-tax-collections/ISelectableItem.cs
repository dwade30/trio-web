﻿namespace TWCL0000
{
    public interface ISelectableItem
    {
        bool IsSelected { get; set; }

    }
}