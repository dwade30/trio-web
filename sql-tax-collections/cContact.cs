﻿using fecherFoundation;

namespace TWCL0000
{
	public class cContact
	{
		//=========================================================
		private int lngID;
		private string strDescription;
		private string strName;
		private string strEmail;
		private string strComment;
		private int lngPartyID;
		private FCCollection PhoneColl = new FCCollection();

		public void Clear()
		{
			PhoneColl = new FCCollection();
			lngID = 0;
			strDescription = "";
			strName = "";
			strEmail = "";
			strComment = "";
			lngPartyID = 0;
		}

		public FCCollection PhoneNumbers
		{
			get
			{
				FCCollection PhoneNumbers = null;
				PhoneNumbers = PhoneColl;
				return PhoneNumbers;
			}
		}

		public int ID
		{
			get
			{
				int ID = 0;
				ID = lngID;
				return ID;
			}
			set
			{
				lngID = value;
			}
		}

		public string Description
		{
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
			set
			{
				strDescription = value;
			}
		}

		public string Name
		{
			get
			{
				string Name = "";
				Name = strName;
				return Name;
			}
			set
			{
				strName = value;
			}
		}

		public string Email
		{
			get
			{
				string Email = "";
				Email = strEmail;
				return Email;
			}
			set
			{
				strEmail = value;
			}
		}

		public string Comment
		{
			get
			{
				string Comment = "";
				Comment = strComment;
				return Comment;
			}
			set
			{
				strComment = value;
			}
		}

		public int PartyID
		{
			get
			{
				int PartyID = 0;
				PartyID = lngPartyID;
				return PartyID;
			}
			set
			{
				lngPartyID = value;
			}
		}
	}
}
