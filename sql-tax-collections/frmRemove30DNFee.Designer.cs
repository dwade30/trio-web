﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmRemove30DNFee.
	/// </summary>
	partial class frmRemove30DNFee : BaseForm
	{
		public fecherFoundation.FCComboBox cmbYear;
		public fecherFoundation.FCTextBox txtAccount;
		public fecherFoundation.FCLabel lblyearTtitle;
		public fecherFoundation.FCLabel lblAccountTitle;
		public fecherFoundation.FCLabel lblDate;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRemove30DNFee));
			this.cmbYear = new fecherFoundation.FCComboBox();
			this.txtAccount = new fecherFoundation.FCTextBox();
			this.lblyearTtitle = new fecherFoundation.FCLabel();
			this.lblAccountTitle = new fecherFoundation.FCLabel();
			this.lblDate = new fecherFoundation.FCLabel();
			//this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.btnProcess = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 269);
			this.BottomPanel.Size = new System.Drawing.Size(359, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbYear);
			this.ClientArea.Controls.Add(this.txtAccount);
			this.ClientArea.Controls.Add(this.lblDate);
			this.ClientArea.Controls.Add(this.lblyearTtitle);
			this.ClientArea.Controls.Add(this.lblAccountTitle);
			this.ClientArea.Size = new System.Drawing.Size(359, 209);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(359, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(318, 35);
			this.HeaderText.Text = "Remove 30 Day Notice Fee";
			// 
			// cmbYear
			// 
			this.cmbYear.AutoSize = false;
			this.cmbYear.BackColor = System.Drawing.SystemColors.Window;
			this.cmbYear.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbYear.Enabled = false;
			this.cmbYear.FormattingEnabled = true;
			this.cmbYear.Location = new System.Drawing.Point(157, 149);
			this.cmbYear.Name = "cmbYear";
			this.cmbYear.Size = new System.Drawing.Size(134, 40);
			this.cmbYear.TabIndex = 4;
			this.cmbYear.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbYear_KeyDown);
			// 
			// txtAccount
			// 
			this.txtAccount.AutoSize = false;
			this.txtAccount.BackColor = System.Drawing.SystemColors.Window;
			this.txtAccount.Location = new System.Drawing.Point(157, 89);
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Size = new System.Drawing.Size(134, 40);
			this.txtAccount.TabIndex = 2;
			this.txtAccount.KeyDown += new Wisej.Web.KeyEventHandler(this.txtAccount_KeyDown);
			this.txtAccount.TextChanged += new System.EventHandler(this.txtAccount_TextChanged);
			// 
			// lblyearTtitle
			// 
			this.lblyearTtitle.AutoSize = true;
			this.lblyearTtitle.Location = new System.Drawing.Point(30, 163);
			this.lblyearTtitle.Name = "lblyearTtitle";
			this.lblyearTtitle.Size = new System.Drawing.Size(41, 16);
			this.lblyearTtitle.TabIndex = 3;
			this.lblyearTtitle.Text = "YEAR";
			this.lblyearTtitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblAccountTitle
			// 
			this.lblAccountTitle.AutoSize = true;
			this.lblAccountTitle.Location = new System.Drawing.Point(30, 103);
			this.lblAccountTitle.Name = "lblAccountTitle";
			this.lblAccountTitle.Size = new System.Drawing.Size(69, 16);
			this.lblAccountTitle.TabIndex = 1;
			this.lblAccountTitle.Text = "ACCOUNT";
			this.lblAccountTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDate
			// 
			this.lblDate.Location = new System.Drawing.Point(30, 30);
			this.lblDate.Name = "lblDate";
			this.lblDate.Size = new System.Drawing.Size(309, 39);
			this.lblDate.TabIndex = 0;
			// 
			// MainMenu1
			// 
			//this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
			//this.mnuFile});
			//this.MainMenu1.Visible = true;
			//
			// mnuFile
			// 
			this.mnuFile.Index = 0;
			this.mnuFile.Text = "File";
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(150, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(159, 48);
			this.btnProcess.TabIndex = 3;
			this.btnProcess.Text = "Save & Preview";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// frmRemove30DNFee
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(359, 377);
			this.FillColor = 0;
			this.KeyPreview = true;
			//this.Menu = this.MainMenu1;
			this.Name = "frmRemove30DNFee";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Remove 30 Day Notice Fee";
			this.Load += new System.EventHandler(this.frmRemove30DNFee_Load);
			this.Activated += new System.EventHandler(this.frmRemove30DNFee_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmRemove30DNFee_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton btnProcess;
	}
}
