﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmLienFeeIncr.
	/// </summary>
	partial class frmLienFeeIncr : BaseForm
	{
		public fecherFoundation.FCLabel lblInstr1;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileClear;
		public fecherFoundation.FCToolStripMenuItem mnuFileSelectAll;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLienFeeIncr));
			this.lblInstr1 = new fecherFoundation.FCLabel();
			this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuFileClear = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSelectAll = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.btnProcess = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 399);
			this.BottomPanel.Size = new System.Drawing.Size(557, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.lblInstr1);
			this.ClientArea.Size = new System.Drawing.Size(557, 339);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(557, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(450, 35);
			this.HeaderText.Text = "Add Lien Discharge Filing Fee Increase";
			// 
			// lblInstr1
			// 
			this.lblInstr1.Location = new System.Drawing.Point(30, 30);
			this.lblInstr1.Name = "lblInstr1";
			this.lblInstr1.Size = new System.Drawing.Size(504, 300);
			this.lblInstr1.TabIndex = 0;
			this.lblInstr1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileClear,
				this.mnuFileSelectAll,
				this.mnuFileSeperator,
				this.mnuFileSave,
				this.Seperator,
				this.mnuFileExit
			});
			this.MainMenu1.Visible = true;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = 0;
			this.mnuFile.Text = "File";
			//
			// mnuFileClear
			// 
			this.mnuFileClear.Index = 0;
			this.mnuFileClear.Text = "Clear All";
			// 
			// mnuFileSelectAll
			// 
			this.mnuFileSelectAll.Index = 1;
			this.mnuFileSelectAll.Text = "Select All";
			// 
			// mnuFileSeperator
			// 
			this.mnuFileSeperator.Index = 2;
			this.mnuFileSeperator.Text = "-";
			// 
			// mnuFileSave
			// 
			this.mnuFileSave.Index = 3;
			this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFileSave.Text = "Process";
			this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 4;
			this.Seperator.Text = "-";
			// 
			// mnuFileExit
			// 
			this.mnuFileExit.Index = 5;
			this.mnuFileExit.Text = "Exit";
			this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(238, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(120, 48);
			this.btnProcess.TabIndex = 2;
			this.btnProcess.Text = "Process";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// frmLienFeeIncr
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(557, 507);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Menu = this.MainMenu1;
			this.Name = "frmLienFeeIncr";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Add Lien Discharge Filing Fee Increase";
			this.Load += new System.EventHandler(this.frmLienFeeIncr_Load);
			this.Activated += new System.EventHandler(this.frmLienFeeIncr_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmLienFeeIncr_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton btnProcess;
	}
}
