﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmLienDischargeGetAcct.
	/// </summary>
	public partial class frmLienDischargeGetAcct : BaseForm
	{
		public frmLienDischargeGetAcct()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmLienDischargeGetAcct InstancePtr
		{
			get
			{
				return (frmLienDischargeGetAcct)Sys.GetInstance(typeof(frmLienDischargeGetAcct));
			}
		}

		protected frmLienDischargeGetAcct _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               12/05/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               04/30/2004              *
		// ********************************************************
		clsDRWrapper rsYear = new clsDRWrapper();
		bool blnFilled;

		private void chkAllNotices_Click(object sender, System.EventArgs e)
		{
			if (chkAllNotices.CheckState == Wisej.Web.CheckState.Checked)
			{
				chkReprint.CheckState = Wisej.Web.CheckState.Unchecked;
				fraSingle.Enabled = false;
			}
			else
			{
				if (chkReprint.CheckState == Wisej.Web.CheckState.Unchecked)
				{
					fraSingle.Enabled = true;
				}
			}
		}

		private void chkReprint_Click(object sender, System.EventArgs e)
		{
			if (chkReprint.CheckState == Wisej.Web.CheckState.Checked)
			{
				chkAllNotices.CheckState = Wisej.Web.CheckState.Unchecked;
				fraSingle.Enabled = false;
			}
			else
			{
				if (chkAllNotices.CheckState == Wisej.Web.CheckState.Unchecked)
				{
					fraSingle.Enabled = true;
				}
			}
		}

		private void cmbYear_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// MAL@20080212: Tracker Reference: 12300
			if (FCConvert.ToDouble(txtAcct.Text) > 0 && !blnFilled)
			{
				cmbYear.Clear();
				FillLienYearCombo();
			}
			else if (FCConvert.ToDouble(txtAcct.Text) > 0)
			{
				// Do Nothing - Year Combo Already Filled
			}
			else
			{
				FCMessageBox.Show("No Account entered.", MsgBoxStyle.OkOnly | MsgBoxStyle.Information, "Missing Data");
			}
		}

		private void cmbYear_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// If KeyAscii = 13 And cmbYear.ListIndex >= 0 Then
			// PrintDischargeNotice
			// End If
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void cmbYear_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// If cmbYear.ListIndex >= 0 Then
			// PrintDischargeNotice
			// End If
		}

		private void frmLienDischargeGetAcct_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (KeyCode == Keys.Escape)
				{
					Close();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Prepayment Keydown Error");
			}
		}

		private void frmLienDischargeGetAcct_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmLienDischargeGetAcct.Icon	= "frmLienDischargeGetAcct.frx":0000";
			//frmLienDischargeGetAcct.ScaleWidth	= 3885;
			//frmLienDischargeGetAcct.ScaleHeight	= 2475;
			//frmLienDischargeGetAcct.LinkTopic	= "Form1";
			//frmLienDischargeGetAcct.LockControls	= -1  'True;
			//Font.Size	= "8.25";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//vsElasticLight1.OleObjectBlob	= "frmLienDischargeGetAcct.frx":058A";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			this.Left = FCConvert.ToInt32((FCGlobal.Screen.Width - this.Width) / 2.0);
			this.Top = FCConvert.ToInt32((FCGlobal.Screen.Height - this.Height) / 2.0);
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			if (chkAllNotices.CheckState == Wisej.Web.CheckState.Checked)
			{
				PrintDischargeNotice(true);
			}
			else if (chkReprint.CheckState == Wisej.Web.CheckState.Checked)
			{
				PrintDischargeNotice(true, true);
			}
			else
			{
				if (Conversion.Val(txtAcct.Text) > 0)
				{
					if (cmbYear.SelectedIndex >= 0)
					{
						PrintDischargeNotice(false);
					}
				}
			}
		}

		private void txtAcct_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii == 13)
			{
                //FC:FINAL:SBE - global fix - validate event should not be triggered twice (issue #4605)
                //txtAcct_Validate(false);
                Support.SendKeys("{TAB}");
            }
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtAcct_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngAcct;
				// cmbYear.Enabled = False
				cmbYear.Clear();
				lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(txtAcct.Text)));
				if (lngAcct > 0)
				{
					// MAL@20080212: Change to call a subroutine instead of having it be in here
					// Tracker Reference: 12300
					FillLienYearCombo();
					// fill the cmbyear
					// rsYear.OpenRecordset "SELECT * FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.LienRecordNumber WHERE Account = " & lngAcct
					// If Not rsYear.EndOfFile Then
					// Do Until rsYear.EndOfFile
					// cmbYear.AddItem FormatYear(rsYear.Get_Fields("BillingYear"))
					// rsYear.MoveNext
					// Loop
					// Else
					// MsgBox "Account " & lngAcct & " does not have any valid liened years.", MsgBoxStyle.Exclamation, "No Liens"
					// Cancel = True
					// Exit Sub
					// End If
				}
				else
				{
					FCMessageBox.Show("Please enter a valid account.", MsgBoxStyle.Exclamation, "Invalid Account");
					// Cancel = True
					return;
				}
				if (cmbYear.Items.Count > 0)
				{
					// if there are valid years then
					e.Cancel = false;
					// enable the cmbyear
					cmbYear.Enabled = true;
					// set the focus to the combo box
					if (cmbYear.Visible)
					{
						cmbYear.Focus();
					}
				}
				else
				{
					// if there are no valid years then
					FCMessageBox.Show("No year eligible for a Lien Discharge Notice.", MsgBoxStyle.Exclamation, "No Liens");
					e.Cancel = true;
					return;
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Validating Account");
			}
		}

		public void txtAcct_Validate(bool Cancel)
		{
			txtAcct_Validating(txtAcct, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void PrintDischargeNotice(bool boolAll, bool boolArchive = false)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsDischarge = new clsDRWrapper();
				if (boolAll)
				{
					//FC:FINAL:DDU:#i157 set boolean variable at form initialization
					if (!boolArchive)
					{
						frmLienDischargeNotice.InstancePtr.Init(DateTime.Today, 0, "", true);
						Close();
					}
					else
					{
						frmLienDischargeNotice.InstancePtr.Init(DateTime.Today, 0, "", true, false, false, true);
						Close();
					}
				}
				else
				{
					rsYear.FindFirstRecord("BillingYear", modExtraModules.FormatYear(cmbYear.Items[cmbYear.SelectedIndex].ToString()));
					if (!rsYear.NoMatch)
					{
						// found it
						frmLienDischargeNotice.InstancePtr.Init(DateTime.FromOADate(0), FCConvert.ToInt32(rsYear.Get_Fields_Int32("LienRecordNumber")), FCConvert.ToString(rsYear.Get_Fields_Int32("BillingYear")));
						Close();
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
			}
		}

		private void FillLienYearCombo()
		{
			// Tracker Reference: 12300
			int lngAcct;
			lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(txtAcct.Text)));
			if (lngAcct > 0)
			{
				// fill the cmbyear
				rsYear.OpenRecordset("SELECT * FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.ID WHERE Account = " + FCConvert.ToString(lngAcct));
				if (!rsYear.EndOfFile())
				{
					while (!rsYear.EndOfFile())
					{
						cmbYear.AddItem(modExtraModules.FormatYear(FCConvert.ToString(rsYear.Get_Fields_Int32("BillingYear"))));
						rsYear.MoveNext();
					}
					blnFilled = true;
				}
				else
				{
					FCMessageBox.Show("Account " + FCConvert.ToString(lngAcct) + " does not have any valid liened years.", MsgBoxStyle.Exclamation, "No Liens");
					blnFilled = false;
					// Cancel = True
					return;
				}
			}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFileSave_Click(sender, e);
		}
	}
}
