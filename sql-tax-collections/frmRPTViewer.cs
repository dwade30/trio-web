﻿using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using fecherFoundation.VisualBasicLayer;
using Global;
using iTextSharp.text.pdf;
using Wisej.Core;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmRPTViewer.
	/// </summary>
	public partial class frmRPTViewer : BaseForm
	{
		public frmRPTViewer()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;

		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmRPTViewer InstancePtr
		{
			get
			{
				return (frmRPTViewer)Sys.GetInstance(typeof(frmRPTViewer));
			}
		}

		protected frmRPTViewer _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/06/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               02/14/2007              *
		// ********************************************************
		string strRPTName;
		int intType;
		string strEmailAttachmentName = "";
		// VBto upgrade warning: rptObj As object	OnWrite(rptCertMailReport, rptReminderSummary, rptLienNoticeSummary)
		public void Init(string strReportName, string strFormCaption, short intPassType, FCSectionReport rptObj = null)
		{
			strRPTName = strReportName;
			this.Text = strFormCaption;
			//FC:FINAL:DDU #i112 changed location of intType to show the error message correctly
			intType = intPassType;
			this.Show(App.MainForm);

			LoadRecreateCombo();
			LoadCombo();
			switch (intType)
			{
				case 1:
				case 2:
					{
						// this is the audits
						// ShowReport
						App.DoEvents();
						mnuFileChange_Click();
						break;
					}
				case 10:
					{
						// this is the purge list
						ShowReport();
						break;
					}
				case 11:
					{
                        // bankruptcy report
                        //FC:FINAL:BSE:#4372 - remove filemenu 
                        //mnuFileRecreate.Visible = false;
                        cmdFileRecreate.Visible = false;
                        break;
					}
				case 12:
					{
                        // Reprint of CMF Report
                        //FC:FINAL:BSE:#4372 - remove filemenu 
                        //mnuFileRecreate.Visible = false;
                        cmdFileRecreate.Visible = false;
                        break;
					}
				case 13:
					{
                        cmdFileRecreate.Visible = false;
                        break;
					}
				case 100:
					{
                        cmdFileRecreate.Visible = false;
                        cmdProcess.Visible = false;
                        cmdChangeReport.Visible = false;
                        fraNumber.Visible = false;
                        this.Menu = null;
						arView.ReportSource = rptObj;
                        arView.Visible = true;
						break;
					}
			}
		}

		private void frmRPTViewer_Activated(object sender, System.EventArgs e)
		{
			if (this.WindowState != FormWindowState.Maximized && this.WindowState != FormWindowState.Minimized)
			{
				this.Height = this.Height - 15;
			}

			LoadRecreateCombo();
			LoadCombo();
			if (intType != 1 && intType != 2)
			{
				ShowReport();
			}
            cmdChangeReport.Enabled = false;
            cmdChangeReport.Visible = true;
        }

        private void frmRPTViewer_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmRPTViewer_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// catches escape keys
			if (KeyAscii == 27)
			{
				//mnuFileExport.Enabled = false;
				//mnuFileEmail.Enabled = false;
                toolBarButtonExportExcel.Enabled = false;
                toolBarButtonExportHTML.Enabled = false;
                toolBarButtonExportPDF.Enabled = false;
                toolBarButtonExportRTF.Enabled = false;
                toolBarButtonEmailPDF.Enabled = false;
                toolBarButtonEmailRTF.Enabled = false;
				KeyAscii = 0;
				Close();
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void ShowReport(short intNum = 1)
		{
			if (modGlobalFunctions.ReportDirectoryStatus())
			{
				switch (intType)
				{
					case 1:
					case 2:
						{
							if (File.Exists(TWSharedLibrary.Variables.Statics.ReportPath + "\\" + strRPTName + FCConvert.ToString(intNum) + ".RDF"))
							{
								//FC:FINAL:SBE - #i340 - set report name instead of loading to ReportSource.Document
								//arView.ReportSource.Document.Load(TWSharedLibrary.Variables.Statics.ReportPath + "\\" + strRPTName + FCConvert.ToString(intNum) + ".RDF");
								arView.ReportName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + strRPTName + FCConvert.ToString(intNum) + ".RDF";
								strEmailAttachmentName = strRPTName + FCConvert.ToString(intNum);
								this.Refresh();
                                cmdProcess.Visible = false;
                                fraNumber.Visible = false;
								this.arView.Visible = true;
								if (arView.Visible)
								{
									arView.Focus();
								}
							}
							else
							{
								frmWait.InstancePtr.Unload();
								if (intNum == 1)
								{
									FCMessageBox.Show("To view this report, you must run the report first.", MsgBoxStyle.Information, "Run Report");
								}
								else
								{
									FCMessageBox.Show("There is not a " + strRPTName + " saved report with the number " + FCConvert.ToString(intNum) + " have been created.", MsgBoxStyle.Information, "Load Error");
								}
								if (Strings.UCase(strRPTName) != Strings.UCase("LastCLDailyAudit"))
								{
									Close();
								}
							}
							break;
						}
					case 10:
						{
							if (File.Exists(TWSharedLibrary.Variables.Statics.ReportPath + "\\" + strRPTName))
							{
								//FC:FINAL:SBE - #i340 - set report name instead of loading to ReportSource.Document
								//arView.ReportSource.Document.Load(TWSharedLibrary.Variables.Statics.ReportPath + "\\" + strRPTName);
								arView.ReportName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + strRPTName;
								strEmailAttachmentName = Path.GetFileNameWithoutExtension(strRPTName);
								this.Refresh();
                                cmdProcess.Visible = false;
                                fraNumber.Visible = false;
                                this.arView.Visible = true;
								if (arView.Visible)
								{
									arView.Focus();
								}
							}
							else
							{
								frmWait.InstancePtr.Unload();
								if (intNum == 1)
								{
									FCMessageBox.Show("To view this report, you must run the report first.", MsgBoxStyle.Information, "Run Report");
								}
								else
								{
									FCMessageBox.Show("There is not a " + strRPTName + " saved report with the number " + FCConvert.ToString(intNum) + " have been created.", MsgBoxStyle.Information, "Load Error");
								}
								Close();
							}
							break;
						}
					case 11:
						{
							// bankruptcy report
							if (File.Exists(TWSharedLibrary.Variables.Statics.ReportPath + "\\" + strRPTName + "1.RDF"))
							{
								//FC:FINAL:SBE - #i340 - set report name instead of loading to ReportSource.Document
								//arView.ReportSource.Document.Load(TWSharedLibrary.Variables.Statics.ReportPath + "\\" + strRPTName + "1.RDF");
								arView.ReportName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + strRPTName + "1.RDF";
								strEmailAttachmentName = Path.GetFileNameWithoutExtension(strRPTName);
								this.Refresh();
                                cmdProcess.Visible = false;
                                fraNumber.Visible = false;
                                this.arView.Visible = true;
								if (arView.Visible)
								{
									arView.Focus();
								}
							}
							else
							{
								frmWait.InstancePtr.Unload();
								if (intNum == 1)
								{
									// MsgBox "To view this report, you must run the report first.", MsgBoxStyle.Information, "Run Report"
									FCMessageBox.Show("This saved report has not been created.", MsgBoxStyle.Information, "Run Report");
								}
								else
								{
									FCMessageBox.Show("There is not a " + strRPTName + " saved report with the number " + FCConvert.ToString(intNum) + " have been created.", MsgBoxStyle.Information, "Load Error");
								}
								Close();
							}
							break;
						}
					case 12:
						{
							// reprint of the last CMF report
							if (File.Exists(TWSharedLibrary.Variables.Statics.ReportPath + "\\" + strRPTName + ".RDF"))
							{
								//FC:FINAL:SBE - #i340 - set report name instead of loading to ReportSource.Document
								//arView.ReportSource.Document.Load(TWSharedLibrary.Variables.Statics.ReportPath + "\\" + strRPTName + ".RDF");
								arView.ReportName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + strRPTName + ".RDF";
								strEmailAttachmentName = Path.GetFileNameWithoutExtension(strRPTName);
								this.Refresh();
                                cmdProcess.Visible = false;
                                fraNumber.Visible = false;
                                this.arView.Visible = true;
								if (arView.Visible)
								{
									arView.Focus();
								}
							}
							else
							{
								frmWait.InstancePtr.Unload();
								if (intNum == 1)
								{
									FCMessageBox.Show("To view this report, you must run the report first.", MsgBoxStyle.Information, "Run Report");
								}
								else
								{
									FCMessageBox.Show("There is not a " + strRPTName + " saved report with the number " + FCConvert.ToString(intNum) + " have been created.", MsgBoxStyle.Information, "Load Error");
								}
								Close();
							}
							break;
						}
					case 13:
						{
							// reprint of fees lists
							if (File.Exists(TWSharedLibrary.Variables.Statics.ReportPath + "\\" + strRPTName + FCConvert.ToString(intNum) + ".RDF"))
							{
								//FC:FINAL:AM: set the report
								arView.ReportSource = arLienTransferReport.InstancePtr;
								arView.ReportSource.Document.Load(TWSharedLibrary.Variables.Statics.ReportPath + "\\" + strRPTName + FCConvert.ToString(intNum) + ".RDF");
								strEmailAttachmentName = Path.GetFileNameWithoutExtension(strRPTName);
								this.Refresh();
                                cmdProcess.Visible = false;
                                fraNumber.Visible = false;
                                this.arView.Visible = true;
								if (arView.Visible)
								{
									arView.Focus();
								}
							}
							else
							{
								frmWait.InstancePtr.Unload();
								if (intNum == 1)
								{
									FCMessageBox.Show("To view this report, you must run the report first.", MsgBoxStyle.Information, "Run Report");
								}
								else
								{
									FCMessageBox.Show("There is not a " + strRPTName + " saved report with the number " + FCConvert.ToString(intNum) + " have been created.", MsgBoxStyle.Information, "Load Error");
								}
							}
							break;
						}
				}
				//end switch
			}
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			// If arView.Status = ddVStatusReadingData Then
			// cancel = true
			// End If
		}

		//private void frmRPTViewer_Resize(object sender, System.EventArgs e)
		//{
		//	arView.ReportSource.PageSettings.Margins.Top = 0;
		//	arView.ReportSource.PageSettings.PaperWidth = this.Width - 100;
		//	arView.ReportSource.PageSettings.Margins.Left = 0;
		//	arView.ReportSource.PageSettings.PaperHeight = FCConvert.ToInt32(this.Height - (400 * (this.Height / 7620.0)));
		//	// MAL@20070907: Added option so control does not drop to the bottom of the screen
		//	if (fraRecreateReport.Visible)
		//	{
		//		fraRecreateReport.Top = FCConvert.ToInt32((arView.ReportSource.PageSettings.PaperHeight / 6.0));
		//	}
		//	if (fraNumber.Visible)
		//	{
		//		fraNumber.Top = FCConvert.ToInt32((arView.ReportSource.PageSettings.PaperHeight / 6.0));
		//	}
		//}
		private void mnuEmailPDF_Click(object sender, System.EventArgs e)
		{
            try
            {
                // On Error GoTo ErrorHandler
                GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport a = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                string strList = "";
                //FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
                string fileName = string.Empty;
                FCSectionReport report = null;
                if (!Directory.Exists(TWSharedLibrary.Variables.Statics.ReportPath))
                {
                    Directory.CreateDirectory(TWSharedLibrary.Variables.Statics.ReportPath);
                }
                //string fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + ARViewer21.ReportSource.Name + ".pdf";
                //FC:FINAL:RPU:#1363 - If the ReportSource is null load the report name from frmReportViewer
                if (!(arView.ReportSource == null))
                {
                    //FC:FINAL:DSE Exception if report name contains sensitive characters
                    fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + FCUtils.FixFileName(arView.ReportSource.Name) + ".pdf";
                    report = arView.ReportSource;
                }
                else
                {
                    report = new FCSectionReport();
                    report.Document.Load(arView.ReportName);
                    //FC:FINAL:DSE Exception if report name contains sensitive characters
                    fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + FCUtils.FixFileName(frmReportViewer.InstancePtr.Text) + ".pdf";
                }
                //FC:FINAL:RPU:#1363 - Use report.Document
                //a.Export(ARViewer21.ReportSource.Document, fileName);
                a.Export(report.Document, fileName);
                // FC:FINAL: RPU:#1363 - No more need for report
                report = null;
                if (!File.Exists(fileName))
                {
                    FCMessageBox.Show("Could not create report file to send as attachment", MsgBoxStyle.Exclamation, "Cannot E-mail");
                    return;
                }
                else
                {
                    if (Strings.Trim(strEmailAttachmentName) != string.Empty)
                    {
                        strList = fileName + ";" + strEmailAttachmentName + ".pdf";
                    }
                    else
                    {
                        strList = fileName;;
                    }
                    frmEMail.InstancePtr.Init(strList, "", "", "", false, true, true, showAsModalForm: true);
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In mnuEmailPDF_Click");
            }
        }

		private void mnuEMailRTF_Click(object sender, System.EventArgs e)
		{
            try
            {
                // On Error GoTo ErrorHandler
                GrapeCity.ActiveReports.Export.Word.Section.RtfExport a = new GrapeCity.ActiveReports.Export.Word.Section.RtfExport();
                string strList = "";
                //FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
                string fileName = string.Empty;
                FCSectionReport report = null;
                if (!Directory.Exists(TWSharedLibrary.Variables.Statics.ReportPath))
                {
                    Directory.CreateDirectory(TWSharedLibrary.Variables.Statics.ReportPath);
                }
                //string fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + ARViewer21.ReportSource.Document.Name + ".rtf";
                //FC:FINAL:RPU:#1363 - If the ReportSource is null load the report name from frmReportViewer
                if (!(arView.ReportSource == null))
                {
                    //FC:FINAL:DSE:#1802 Exception if report name contains sensitive characters
                    fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + FCUtils.FixFileName(arView.ReportSource.Name) + ".rtf";
                    report = arView.ReportSource;
                }
                else
                {
                    report = new FCSectionReport();
                    report.Document.Load(arView.ReportName);
                    //FC:FINAL:DSE:#1802 Exception if report name contains sensitive characters
                    fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + FCUtils.FixFileName(frmReportViewer.InstancePtr.Text) + ".rtf";
                }
                //FC:FINAL:RPU:#1363 - Use report.Document
                //a.Export(ARViewer21.ReportSource.Document, fileName);
                a.Export(report.Document, fileName);
                // FC:FINAL: RPU:#1363 - No more need for report
                report = null;
                if (!File.Exists(fileName))
                {
                    FCMessageBox.Show("Could not create report file to send as attachment", MsgBoxStyle.Exclamation, "Cannot E-mail");
                    return;
                }
                else
                {
                    if (Strings.Trim(strEmailAttachmentName) != string.Empty)
                    {
                        strList = fileName + ";" + strEmailAttachmentName + ".rtf";
                    }
                    else
                    {
                        strList = fileName;
                    }
                    frmEMail.InstancePtr.Init(strList, "", "", "", false, true, true, showAsModalForm: true);
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In mnuEmailRTF_Click");
            }
        }

		private void mnuFile_Click(object sender, System.EventArgs e)
		{
            //FC:FINAL:BSE:#4372 - remove filemenu 
            //mnuFileSwitch.Text = "Show Report";
        }

        private void mnuFileChange_Click(object sender, System.EventArgs e)
		{
			// this will let the user switch between the numbered reports
			arView.Visible = false;
            arView.Parent = null;
            //mnuFileExport.Enabled = false;
            //mnuFileEmail.Enabled = false;
            toolBarButtonExportExcel.Enabled = false;
            toolBarButtonExportHTML.Enabled = false;
            toolBarButtonExportPDF.Enabled = false;
            toolBarButtonExportRTF.Enabled = false;
            toolBarButtonEmailPDF.Enabled = false;
            toolBarButtonEmailRTF.Enabled = false;
            fraNumber.Visible = true;
            cmbNumber.Enabled = true;
			fraNumber.Left = FCConvert.ToInt32((frmRPTViewer.InstancePtr.Width - fraNumber.Width) / 2.0);
			fraNumber.Top = FCConvert.ToInt32((frmRPTViewer.InstancePtr.Height - fraNumber.Height) / 3.0);
            //FC:FINAL:BSE:#4372 - adjustt button location when changing frame location as well 
            cmdProcess.Visible = true;
            cmdProcess.Left = fraNumber.Left + 76;
            cmdProcess.Top = fraNumber.Top + 122;

        }

		public void mnuFileChange_Click()
		{
            //FC:FINAL:BSE:#4372 - remove filemenu 
			//mnuFileChange_Click(mnuFileChange, new System.EventArgs());
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuFileExportExcel_Click(object sender, System.EventArgs e)
		{
			ExportExcel();
		}

		private void mnuFileExportHTML_Click(object sender, System.EventArgs e)
		{
			ExportHTML();
		}

		private void mnuFileExportPDF_Click(object sender, System.EventArgs e)
		{
			ExportPDF();
		}

		private void mnuFileExportRTF_Click(object sender, System.EventArgs e)
		{
			ExportRTF();
		}

		private void mnuFileRecreate_Click(object sender, System.EventArgs e)
		{
			// this will recreate the audit report
			lblRecreateInstructions.Text = "Select the report to recreate.";
			// show the recreate frame
			fraRecreateReport.Visible = true;
			// hide the rest of the frames
			fraNumber.Visible = false;
			arView.Visible = false;
            arView.Parent = null;
            //mnuFileExport.Enabled = false;
            //mnuFileEmail.Enabled = false;
            toolBarButtonExportExcel.Enabled = false;
            toolBarButtonExportHTML.Enabled = false;
            toolBarButtonExportPDF.Enabled = false;
            toolBarButtonExportRTF.Enabled = false;
            toolBarButtonEmailPDF.Enabled = false;
            toolBarButtonEmailRTF.Enabled = false;
            //FC:FINAL:DDU #i119 changed location of frame to don't overlap anymore
            fraRecreateReport.Left = FCConvert.ToInt32((frmRPTViewer.InstancePtr.Width - fraRecreateReport.Width) / 2.0);
			fraRecreateReport.Top = FCConvert.ToInt32((frmRPTViewer.InstancePtr.Height - fraRecreateReport.Height) / 3.0);
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			if (fraRecreateReport.Visible)
			{
				if (cmbRecreate.SelectedIndex != -1)
				{
					modGlobalConstants.Statics.gblnRecreate = true;
					// load the report
					ReCreateReport_2(FCConvert.ToInt32(Conversion.Val(Strings.Left(cmbRecreate.Items[cmbRecreate.SelectedIndex].ToString(), 5))));
					Close();
				}
			}
			else if (fraNumber.Visible)
			{
				if (cmbNumber.SelectedIndex != -1)
				{
					ShowReport(FCConvert.ToInt16(cmbNumber.Items[cmbNumber.SelectedIndex].ToString()));
					fraNumber.Visible = false;
					arView.Visible = true;
                    arView.Parent = this.ClientArea;
                    this.ClientArea.Controls.SetChildIndex(arView, 0);
                    //mnuFileExport.Enabled = true;
                    //mnuFileEmail.Enabled = true;
                    toolBarButtonExportExcel.Enabled = true;
                    toolBarButtonExportHTML.Enabled = true;
                    toolBarButtonExportPDF.Enabled = true;
                    toolBarButtonExportRTF.Enabled = true;
                    toolBarButtonEmailPDF.Enabled = true;
                    toolBarButtonEmailRTF.Enabled = true;
                    //FC:FINAL:BSE:#4372 - remove filemenu 
                    //mnuFileChange.Enabled = true;
                    cmdChangeReport.Enabled = true;
                    cmdChangeReport.Visible = true;
                    cmdProcess.Visible = false;
					modGlobalConstants.Statics.gblnRecreate = false;
				}
			}
		}

		private void mnuFileSwitch_Click(object sender, System.EventArgs e)
		{
			if (File.Exists(TWSharedLibrary.Variables.Statics.ReportPath + "\\" + strRPTName + "1.RDF"))
			{
				//FC:FINAL:SBE - #i340 - set report name instead of loading to ReportSource.Document
				//arView.ReportSource.Document.Load(TWSharedLibrary.Variables.Statics.ReportPath + "\\" + strRPTName + "1.RDF");
				arView.ReportName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + strRPTName + "1.RDF";
			}
			else
			{
				FCMessageBox.Show("The file (" + strRPTName + "1.RDF) being loaded does not exist.  Please create it first.", MsgBoxStyle.Information, "Load Error");
			}
            //mnuFileExport.Enabled = false;
            //mnuFileEmail.Enabled = false;
            toolBarButtonExportExcel.Enabled = false;
            toolBarButtonExportHTML.Enabled = false;
            toolBarButtonExportPDF.Enabled = false;
            toolBarButtonExportRTF.Enabled = false;
            toolBarButtonEmailPDF.Enabled = false;
            toolBarButtonEmailRTF.Enabled = false;
            LoadCombo();
		}

		private void LoadCombo()
		{
			// this will check to see which reports are saved and will show the list of numbers
			int intCT;
			cmbNumber.Clear();
			if (System.IO.Directory.Exists(TWSharedLibrary.Variables.Statics.ReportPath))
			{
				for (intCT = 1; intCT <= 9; intCT++)
				{
					if (File.Exists(TWSharedLibrary.Variables.Statics.ReportPath + "\\" + strRPTName + FCConvert.ToString(intCT) + ".RDF"))
					{
						cmbNumber.AddItem(FCConvert.ToString(intCT));
					}
				}
			}
			else
			{
				FCMessageBox.Show("You must run a report before viewing it.", MsgBoxStyle.Information, "No Report");
				modGlobalFunctions.ReportDirectoryStatus();
				// Unload Me
			}
		}
		// VBto upgrade warning: lngCloseOut As int	OnWriteFCConvert.ToDouble(
		private void ReCreateReport_2(int lngCloseOut)
		{
			ReCreateReport(ref lngCloseOut);
		}

		private void ReCreateReport(ref int lngCloseOut)
		{
			modGlobal.Statics.glngCurrentCloseOut = lngCloseOut;
			rptCLDailyAuditMaster.InstancePtr.StartUp(true, true, modRegistry.GetRegistryKey("CLAuditLandscape", "CR") == "True", true, true);
			frmReportViewer.InstancePtr.Init(rptCLDailyAuditMaster.InstancePtr);
            //mnuFileExport.Enabled = true;
            //mnuFileEmail.Enabled = true;
            toolBarButtonExportExcel.Enabled = true;
            toolBarButtonExportHTML.Enabled = true;
            toolBarButtonExportPDF.Enabled = true;
            toolBarButtonExportRTF.Enabled = true;
            toolBarButtonEmailPDF.Enabled = true;
            toolBarButtonEmailRTF.Enabled = true;
			//FC:FINAL:BSE:#4372 - enable buttons on report
            toolBarButtonPrint.Enabled = true;
            
        }

		private void LoadRecreateCombo()
		{
			// this will fill the recreation combo with all of the closeouts done in the CloseOut table
			clsDRWrapper rsCloseOut = new clsDRWrapper();
			cmbRecreate.Clear();
			rsCloseOut.OpenRecordset("SELECT * FROM CloseOut ORDER BY CloseOutDate desc", modExtraModules.strCLDatabase);
			while (!rsCloseOut.EndOfFile())
			{
				cmbRecreate.AddItem(modGlobalFunctions.PadStringWithSpaces(FCConvert.ToString(rsCloseOut.Get_Fields_Int32("ID")), 5) + " - " + FCConvert.ToString(rsCloseOut.Get_Fields_DateTime("CloseOutDate")));
				cmbRecreate.ItemData(cmbRecreate.NewIndex, FCConvert.ToInt32(rsCloseOut.Get_Fields_Int32("ID")));
				rsCloseOut.MoveNext();
			}
		}

		private void ExportHTML()
		{
            //try
            //{
            //	string strFileName = "";
            //	DialogResult intResp = 0;
            //	string strOldDir;
            //	string strFullName = "";
            //	strOldDir = Application.StartupPath;
            //	Information.Err().Clear();
            //	App.MainForm.CommonDialog1.Flags = FCConvert.ToInt32(FCCommonDialog.FileOpenConstants.cdlOFNNoChangeDir);
            //	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
            //	App.MainForm.CommonDialog1.CancelError = true;
            //	/*? On Error Resume Next  */
            //	App.MainForm.CommonDialog1.FileName = "";
            //	App.MainForm.CommonDialog1.Filter = "*.htm";
            //	App.MainForm.CommonDialog1.DefaultExt = "htm";
            //	App.MainForm.CommonDialog1.InitDir = Application.StartupPath;
            //	try
            //	{
            //		App.MainForm.CommonDialog1.ShowSave();
            //	}
            //	catch
            //	{
            //	}
            //	if (Information.Err().Number == 0)
            //	{
            //		GrapeCity.ActiveReports.Export.Html.Section.HtmlExport a = new GrapeCity.ActiveReports.Export.Html.Section.HtmlExport();
            //		strFileName = Path.GetFileNameWithoutExtension(App.MainForm.CommonDialog1.FileName);
            //		//a.FileNamePrefix = strFileName;
            //		strFullName = Path.GetFileName(App.MainForm.CommonDialog1.FileName);
            //		//a.HTMLOutputPath = Path.GetDirectoryName(App.MainForm.CommonDialog1.FileName);
            //		string HTMLOutputPath = Directory.GetParent(App.MainForm.CommonDialog1.FileName).FullName;
            //		//a.MHTOutput = false;
            //		a.MultiPage = false;
            //                 //FC:FINAL:DSE Exception when ReportSource is null
            //                 FCSectionReport report = null;
            //                 if (!(arView.ReportSource == null))
            //                 {
            //                     report = arView.ReportSource;
            //                 }
            //                 else
            //                 {
            //                     report = new FCSectionReport();
            //                     report.Document.Load(arView.ReportName);
            //                 }
            //                 //FC:FINAL:DSE Exception when ReportSource is null
            //                 //if (arView.ReportSource.Document.Pages.Count > 1)
            //		if(report.Document.Pages.Count > 1)
            //		{
            //			intResp = FCMessageBox.Show("Do you want to save all pages as one HTML page?", MsgBoxStyle.YesNo, "Single or multiple files?");
            //			if (intResp == DialogResult.No)
            //				a.MultiPage = true;
            //		}
            //		if (!Directory.Exists(HTMLOutputPath + strFileName))
            //		{
            //			Directory.CreateDirectory(HTMLOutputPath + strFileName);
            //			HTMLOutputPath = HTMLOutputPath + strFileName;
            //		}
            //                 //FC:FINAL:DSE Exception when ReportSource is null
            //                 //a.Export(arView.ReportSource.Document, HTMLOutputPath);
            //                 a.Export(report.Document, HTMLOutputPath);
            //		FCMessageBox.Show("Export to file  " + HTMLOutputPath + strFullName + "  was completed successfully." + "\r\n" + "Please note that supporting files may also have been created in the same directory.", MsgBoxStyle.Information | MsgBoxStyle.OkOnly, "TRIO Software");
            //	}
            //	else
            //	{
            //		FCMessageBox.Show("There was an error saving the file.", MsgBoxStyle.Critical, null);
            //	}
            //	FCFileSystem.ChDrive(strOldDir);
            //	//Application.StartupPath = strOldDir;
            //	return;
            //}
            //catch (Exception ex)
            //{
            //	FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In HTML export", MsgBoxStyle.Critical, "Error");
            //}
            try
            {
                string strFileName = "";
                DialogResult intResp = 0;
                string strOldDir;
                string strFullName = "";
                //FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
                FCSectionReport report = null;
                string fileName = string.Empty;
                if (!(arView.ReportSource == null))
                {
                    if (arView.ReportSource.State == GrapeCity.ActiveReports.SectionReport.ReportState.InProgress)
                    {
                        FCMessageBox.Show("Report not done", MsgBoxStyle.Exclamation, "Report Loading");
                        return;
                    }
                }
                GrapeCity.ActiveReports.Export.Html.Section.HtmlExport a = new GrapeCity.ActiveReports.Export.Html.Section.HtmlExport();
                //FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
                if (!(arView.ReportSource == null))
                {
                    report = arView.ReportSource;
                    //FC:FINAL:DSE Exception if report name contains sensitive characters
                    fileName = FCUtils.FixFileName(arView.ReportSource.Name);
                }
                else
                {
                    report = new FCSectionReport();
                    report.Document.Load(arView.ReportName);
                    //FC:FINAL:DSE Exception if report name contains sensitive characters
                    fileName = FCUtils.FixFileName(frmReportViewer.InstancePtr.Text);
                }
                //FC:FINAL:RPU:#1363 - Use the report variable
                //if (ARViewer21.ReportSource.Document.Pages.Count > 1)
                if (report.Document.Pages.Count > 1)
                {
                    DialogResult res = FCMessageBox.Show("Do you want to save all pages as one HTML page?", MsgBoxStyle.YesNo, "Single or multiple files?");
                    if (res == DialogResult.No)
                    {
                        a.MultiPage = true;
                    }
                }
                if (a.MultiPage)
                {
                    string zipFilePath = GetTempFileName(".zip");
                    //FC:FINAL:RPU:#1363 - Concatenate just extension
                    //string fileName = ARViewer21.ReportSource.Name + ".zip";
                    fileName += ".zip";
                    var zip = fecherFoundation.ZipFile.Open(zipFilePath, System.IO.Compression.ZipArchiveMode.Create);
                    //FC:FINAL:RPU:#1363 - Use the report variable
                    //for (int i = 0; i < ARViewer21.ReportSource.Document.Pages.Count; i++)
                    for (int i = 0; i < report.Document.Pages.Count; i++)
                    {
                        string currentPage = (i + 1).ToString();
                        //string htmlFileName = ARViewer21.ReportSource.Name + "_Page" + currentPage + ".html";
                        string htmlFileName = report.Name + "_Page" + currentPage + ".html";
                        string tempFilePath = GetTempFileName(".html");
                        //a.Export(ARViewer21.ReportSource.Document, tempFilePath, currentPage);
                        a.Export(report.Document, tempFilePath, currentPage);
                        zip.CreateEntryFromFile(tempFilePath, htmlFileName, System.IO.Compression.CompressionLevel.Optimal);
                    }
                    zip.Dispose();
                    //FC:FINAL:DSE use Application.DownloadAndOpen instead of Application.Download otherwise raport will get corrupted/disposed
                    //FCUtils.Download(zipFilePath, fileName);
                    FCUtils.DownloadAndOpen("_blank", zipFilePath, fileName);
                    //FC:FINAL:RPU:#1363 - No more need for report
                    report = null;
                }
                else
                {
                    //FC:FINAL:RPU:#1363 - Concatenate just extension
                    //string fileName = ARViewer21.ReportSource.Name + ".html";
                    fileName += ".html";
                    string tempFilePath = GetTempFileName(".html");
                    //FC:FINAL:RPU:#1363 - Use the report variable
                    //a.Export(ARViewer21.ReportSource.Document, tempFilePath);
                    a.Export(report.Document, tempFilePath);
                    //PPJ:FINAL:BCU - use Application.DownloadAndOpen instead of Application.Download otherwise raport will get corrupted/disposed
                    //FCUtils.Download(stream, fileName);
                    FCUtils.DownloadAndOpen("_blank", tempFilePath, fileName);
                    //FC:FINAL:RPU:#1363 - No more need for report
                    report = null;
                }
                return;
            }
            catch (Exception ex)
            {
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In HTML export", MsgBoxStyle.Critical, "Error");
            }
        }

		private void ExportRTF()
		{
            //try
            //{
            //	string strOldDir;
            //	strOldDir = Application.StartupPath;
            //	Information.Err().Clear();
            //	App.MainForm.CommonDialog1.Flags = FCConvert.ToInt32(FCCommonDialog.FileOpenConstants.cdlOFNNoChangeDir);
            //	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
            //	App.MainForm.CommonDialog1.CancelError = true;
            //	/*? On Error Resume Next  */
            //	App.MainForm.CommonDialog1.FileName = "";
            //	App.MainForm.CommonDialog1.Filter = "*.rtf";
            //	App.MainForm.CommonDialog1.DefaultExt = "rtf";
            //	App.MainForm.CommonDialog1.InitDir = Application.StartupPath;
            //	App.MainForm.CommonDialog1.ShowSave();
            //	if (Information.Err().Number == 0)
            //	{
            //		GrapeCity.ActiveReports.Export.Word.Section.RtfExport a = new GrapeCity.ActiveReports.Export.Word.Section.RtfExport();
            //		string fileName = App.MainForm.CommonDialog1.FileName;
            //                 //FC:FINAL:DSE Exception when ReportSource is Null
            //                 //a.Export(arView.ReportSource.Document, fileName);
            //                 FCSectionReport report = null;
            //                 if (!(arView.ReportSource == null))
            //                 {
            //                     report = arView.ReportSource;
            //                 }
            //                 else
            //                 {
            //                     report = new FCSectionReport();
            //                     report.Document.Load(arView.ReportName);
            //                 }
            //                 a.Export(report.Document, fileName);
            //		FCMessageBox.Show("Export to file  " + App.MainForm.CommonDialog1.FileName + "  was completed successfully.", MsgBoxStyle.Information | MsgBoxStyle.OkOnly, "TRIO Software");
            //	}
            //	else
            //	{
            //		FCMessageBox.Show("There was an error saving the file.", MsgBoxStyle.Critical, null);
            //	}
            //	FCFileSystem.ChDrive(strOldDir);
            //	//Application.StartupPath = strOldDir;
            //	return;
            //}
            //catch (Exception ex)
            //{
            //	FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In RTF export", MsgBoxStyle.Critical, "Error");
            //}
            try
            {
                string strOldDir;
                //FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
                FCSectionReport report = null;
                string fileName = string.Empty;
                if (!(arView.ReportSource == null))
                {
                    if (arView.ReportSource.State == GrapeCity.ActiveReports.SectionReport.ReportState.InProgress)
                    {
                        FCMessageBox.Show("Report not done", MsgBoxStyle.Exclamation, "Report Loading");
                        return;
                    }
                }
                strOldDir = Application.StartupPath;
                Information.Err().Clear();
                GrapeCity.ActiveReports.Export.Word.Section.RtfExport a = new GrapeCity.ActiveReports.Export.Word.Section.RtfExport();
                //string fileName = ARViewer21.ReportSource.Name + ".rtf";
                //FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
                if (!(arView.ReportSource == null))
                {
                    report = arView.ReportSource;
                    //FC:FINAL:DSE Exception if report name contains sensitive characters
                    fileName = FCUtils.FixFileName(arView.ReportSource.Name) + ".rtf";
                }
                else
                {
                    report = new FCSectionReport();
                    report.Document.Load(arView.ReportName);
                    //FC:FINAL:DSE Exception if report name contains sensitive characters
                    fileName = FCUtils.FixFileName(frmReportViewer.InstancePtr.Text) + ".rtf";
                }
                using (MemoryStream stream = new MemoryStream())
                {
                    //FC:FINAL:RPU:#1363 - Use report.Document
                    //a.Export(ARViewer21.ReportSource.Document, stream);
                    a.Export(report.Document, stream);
                    stream.Position = 0;
                    //PPJ:FINAL:BCU - use Application.DownloadAndOpen instead of Application.Download otherwise raport will get corrupted/disposed
                    //FCUtils.Download(stream, fileName);
                    FCUtils.DownloadAndOpen("_blank", stream, fileName);
                    //FC:FINAL:RPU:#1363 - No more need for report
                    report = null;
                }
                return;
            }
            catch (Exception ex)
            {
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In RTF export", MsgBoxStyle.Critical, "Error");
            }
        }

        private void ExportPDF()
        {
            //const int curOnErrorGoToLabel_Default = 0;
            //const int curOnErrorGoToLabel_ErrorHandler = 1;
            //try
            //{
            //	string strOldDir;
            //	strOldDir = Application.StartupPath;
            //	Information.Err().Clear();
            //	App.MainForm.CommonDialog1.Flags = FCConvert.ToInt32(FCCommonDialog.FileOpenConstants.cdlOFNNoChangeDir);
            //	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
            //	App.MainForm.CommonDialog1.CancelError = true;
            //	/*? On Error Resume Next  */
            //	App.MainForm.CommonDialog1.FileName = "";
            //	App.MainForm.CommonDialog1.Filter = "*.pdf";
            //	App.MainForm.CommonDialog1.DefaultExt = "pdf";
            //	App.MainForm.CommonDialog1.InitDir = Application.StartupPath;
            //	try
            //	{
            //		App.MainForm.CommonDialog1.ShowSave();
            //	}
            //	catch
            //	{
            //	}
            //	if (Information.Err().Number == 0)
            //	{
            //		GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport a = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
            //		string fileName = App.MainForm.CommonDialog1.FileName;
            //                 //FC:FINAL:DSE Exception when ReportSource is null
            //                 //a.Export(arView.ReportSource.Document, fileName);
            //                 FCSectionReport report = null;
            //                 if (!(arView.ReportSource == null))
            //                 {
            //                     report = arView.ReportSource;
            //                 }
            //                 else
            //                 {
            //                     report = new FCSectionReport();
            //                     report.Document.Load(arView.ReportName);
            //                 }
            //                 a.Export(report.Document, fileName);
            //		FCMessageBox.Show("Export to file  " + App.MainForm.CommonDialog1.FileName + "  was completed successfully.", MsgBoxStyle.Information | MsgBoxStyle.OkOnly, "TRIO Software");
            //	}
            //	else
            //	{
            //		FCMessageBox.Show("There was an error saving the file.", MsgBoxStyle.Critical, null);
            //	}
            //	/* On Error GoTo ErrorHandler */
            //	FCFileSystem.ChDrive(strOldDir);
            //	//Application.StartupPath = strOldDir;
            //	return;
            //}
            //catch (Exception ex)
            //{
            //	FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In PDF export", MsgBoxStyle.Critical, "Error");
            //}
            try
            {
                string strOldDir;
                //FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
                FCSectionReport report = null;
                string fileName = string.Empty;
                if (!(arView.ReportSource == null))
                {
                    if (arView.ReportSource.State == GrapeCity.ActiveReports.SectionReport.ReportState.InProgress)
                    {
                        FCMessageBox.Show("Report not done", MsgBoxStyle.Exclamation, "Report Loading");
                        return;
                    }
                }
                GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport a = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                //string fileName = ARViewer21.ReportSource.Name + ".pdf";
                //FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
                if (!(arView.ReportSource == null))
                {
                    report = arView.ReportSource;
                    //FC:FINAL:DSE Exception if report name contains sensitive characters
                    fileName = FCUtils.FixFileName(arView.ReportSource.Name) + ".pdf";
                }
                else
                {
                    report = new FCSectionReport();
                    report.Document.Load(arView.ReportName);
                    //FC:FINAL:DSE Exception if report name contains sensitive characters
                    fileName = FCUtils.FixFileName(frmReportViewer.InstancePtr.Text) + ".pdf";
                }
                using (MemoryStream stream = new MemoryStream())
                {
                    //FC:FINAL:RPU:#1363 - Use report.Document
                    //a.Export(ARViewer21.ReportSource.Document, stream);
                    a.Export(report.Document, stream);
                    stream.Position = 0;
                    //PPJ:FINAL:BCU - use Application.DownloadAndOpen instead of Application.Download otherwise raport will get corrupted/disposed
                    //FCUtils.Download(stream, fileName);
                    FCUtils.DownloadAndOpen("_blank", stream, fileName);
                    //FC:FINAL:RPU:#1363 - No more need for report
                    report = null;
                }
                return;
            }
            catch (Exception ex)
            {
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In PDF export", MsgBoxStyle.Critical, "Error");
            }
        }

		private void ExportExcel()
		{
            //try
            //{
            //	string strOldDir;
            //	string strFileName = "";
            //	string strFullName = "";
            //	string strPathName = "";
            //	strOldDir = Application.StartupPath;
            //	Information.Err().Clear();
            //	App.MainForm.CommonDialog1.Flags = FCConvert.ToInt32(FCCommonDialog.FileOpenConstants.cdlOFNNoChangeDir);
            //	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
            //	App.MainForm.CommonDialog1.CancelError = true;
            //	/*? On Error Resume Next  */
            //	App.MainForm.CommonDialog1.FileName = "";
            //	App.MainForm.CommonDialog1.Filter = "*.xls";
            //	App.MainForm.CommonDialog1.DefaultExt = "xls";
            //	App.MainForm.CommonDialog1.InitDir = Application.StartupPath;
            //	try
            //	{
            //		App.MainForm.CommonDialog1.ShowSave();
            //	}
            //	catch
            //	{
            //	}
            //	if (Information.Err().Number == 0)
            //	{
            //		strFileName = Path.GetFileNameWithoutExtension(App.MainForm.CommonDialog1.FileName);
            //		// name without extension
            //		strFullName = Path.GetFileName(App.MainForm.CommonDialog1.FileName);
            //		// name with extension
            //		strPathName = Directory.GetParent(App.MainForm.CommonDialog1.FileName).FullName;
            //		if (Strings.Right(strPathName, 1) != "\\")
            //			strPathName += "\\";
            //		GrapeCity.ActiveReports.Export.Excel.Section.XlsExport a = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
            //		if (!Directory.Exists(strPathName + strFileName))
            //		{
            //			Directory.CreateDirectory(strPathName + strFileName);
            //			strPathName += strFileName + "\\";
            //		}
            //                 // a.FileName = .FileName
            //                 //a.FileName = strPathName + strFullName;
            //                 //a.Version = 8;
            //                 //FC:FINAL:DSE Exception when ReportSource is null
            //                 //a.Export(arView.ReportSource.Document, strPathName + strFullName);
            //                 FCSectionReport report = null;
            //                 if (!(arView.ReportSource == null))
            //                 {
            //                     report = arView.ReportSource;
            //                 }
            //                 else
            //                 {
            //                     report = new FCSectionReport();
            //                     report.Document.Load(arView.ReportName);
            //                 }
            //		a.Export(report.Document, strPathName + strFullName);
            //		FCMessageBox.Show("Export to file  " + strPathName + strFullName + "  was completed successfully." + "\r\n" + "Please note that supporting files may also have been created in the same directory.", MsgBoxStyle.Information | MsgBoxStyle.OkOnly, "TRIO Software");
            //	}
            //	else
            //	{
            //		FCMessageBox.Show("There was an error saving the file.", MsgBoxStyle.Critical, null);
            //	}
            //	FCFileSystem.ChDrive(strOldDir);
            //	//Application.StartupPath = strOldDir;
            //	return;
            //}
            //catch (Exception ex)
            //{
            //	FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In Excel export.", MsgBoxStyle.Critical, "Error");
            //}
            try
            {
                string strOldDir;
                string strFileName = "";
                string strFullName = "";
                string strPathName = "";
                //FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
                FCSectionReport report = null;
                string fileName = string.Empty;
                if (!(arView.ReportSource == null))
                {
                    if (arView.ReportSource.State == GrapeCity.ActiveReports.SectionReport.ReportState.InProgress)
                    {
                        FCMessageBox.Show("Report not done", MsgBoxStyle.Exclamation, "Report Loading");
                        return;
                    }
                }
                GrapeCity.ActiveReports.Export.Excel.Section.XlsExport a = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                //string fileName = ARViewer21.ReportSource.Name + ".xls";
                //FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
                if (!(arView.ReportSource == null))
                {
                    report = arView.ReportSource;
                    //FC:FINAL:DSE Exception if report name contains sensitive characters
                    fileName = FCUtils.FixFileName(arView.ReportSource.Name) + ".xls";
                }
                else
                {
                    report = new FCSectionReport();
                    report.Document.Load(arView.ReportName);
                    //FC:FINAL:DSE Exception if report name contains sensitive characters
                    fileName = FCUtils.FixFileName(frmReportViewer.InstancePtr.Text) + ".xls";
                }
                using (MemoryStream stream = new MemoryStream())
                {
                    //FC:FINAL:RPU:#1363 - Use report.Document
                    //a.Export(ARViewer21.ReportSource.Document, stream);
                    a.Export(report.Document, stream);
                    stream.Position = 0;
                    //PPJ:FINAL:BCU - use Application.DownloadAndOpen instead of Application.Download otherwise raport will get corrupted/disposed
                    //FCUtils.Download(stream, fileName);
                    FCUtils.DownloadAndOpen("_blank", stream, fileName);
                    // FC:FINAL: RPU:#1363 - No more need for report
                    report = null;
                }
                return;
            }
            catch (Exception ex)
            {
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In Excel export.", MsgBoxStyle.Critical, "Error");
            }
        }

        private void toolBar1_ButtonClick(object sender, ToolBarButtonClickEventArgs e)
        {
            if (e.Button == toolBarButtonEmailPDF)
            {
                this.mnuEmailPDF_Click(e.Button, EventArgs.Empty);
            }
            else if (e.Button == toolBarButtonEmailRTF)
            {
                this.mnuEMailRTF_Click(e.Button, EventArgs.Empty);
            }
            else if (e.Button == toolBarButtonExportPDF)
            {
                ExportPDF();
            }
            else if (e.Button == toolBarButtonExportRTF)
            {
                ExportRTF();
            }
            else if (e.Button == toolBarButtonExportHTML)
            {
                ExportHTML();
            }
            else if (e.Button == toolBarButtonExportExcel)
            {
                ExportExcel();
            }else if(e.Button == toolBarButtonPrint)
            {
                Print();
            }
        }
        private void Print()
        {
            if (arView.ReportSource != null)
            {
                if (arView.ReportSource.State == GrapeCity.ActiveReports.SectionReport.ReportState.InProgress)
                {
                    FCMessageBox.Show("Report not done", MsgBoxStyle.Exclamation, "Report Loading");
                    return;
                }
            }
            try
            {
                FCSectionReport report;
                if (arView.ReportSource != null)
                {
                    report = arView.ReportSource;
                }
                else
                {
                    report = new FCSectionReport();
                    report.Document?.Load(arView.ReportName);
                }
                report.ExportToPDFAndPrint();
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In mnuPrint", MsgBoxStyle.Critical, "Error");
            }
        }
        private string GetTempFileName(string extension)
        {
            string applicationName = Path.GetFileName(Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory));
            //string tempFolder = Path.Combine(Path.GetTempPath(), "Wisej", applicationName, "Temp", "PDF");
            string tempFolder = Path.Combine(FCFileSystem.Statics.UserDataFolder, "Temp", "ReportViewer");
            if (!Directory.Exists(tempFolder))
            {
                Directory.CreateDirectory(tempFolder);
            }
            Guid guid = Guid.NewGuid();
            string tempFileName = guid.ToString() + extension;
            string tempFilePath = Path.Combine(tempFolder, tempFileName);
            while (File.Exists(tempFilePath))
            {
                guid = Guid.NewGuid();
                tempFileName = guid.ToString() + extension;
                tempFilePath = Path.Combine(tempFolder, tempFileName);
            }
            return tempFilePath;
        }
    }
}
