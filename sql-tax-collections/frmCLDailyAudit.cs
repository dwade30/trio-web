﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmCLDailyAudit.
	/// </summary>
	public partial class frmCLDailyAudit : BaseForm
	{
		public frmCLDailyAudit()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCLDailyAudit InstancePtr
		{
			get
			{
				return (frmCLDailyAudit)Sys.GetInstance(typeof(frmCLDailyAudit));
			}
		}

		protected frmCLDailyAudit _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/23/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               04/16/2004              *
		// ********************************************************
		public string auxCmbAuditItem1 = "Audit Report and Close Out";

		private void cmdDone_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// show the report modally so that the closeout will not happen until after the report is finished
				if (cmbAudit.SelectedIndex == 0)
				{
					// Audit Preview
					// this will look for payments that are not closed out yet
					modGlobal.Statics.glngCurrentCloseOut = 0;
					if (frmCLDailyAudit.InstancePtr.cmbREPP.SelectedIndex == 0)
					{
						// the user wants a real estate audit
						rptCLDailyAuditMaster.InstancePtr.StartUp(true, false, frmCLDailyAudit.InstancePtr.cmbAlignment.SelectedIndex == 1, false);
						frmReportViewer.InstancePtr.Init(rptCLDailyAuditMaster.InstancePtr);
					}
					else if (frmCLDailyAudit.InstancePtr.cmbREPP.SelectedIndex == 1)
					{
						// the user wants a personal property audit
						rptCLDailyAuditMaster.InstancePtr.StartUp(false, false, frmCLDailyAudit.InstancePtr.cmbAlignment.SelectedIndex == 1, false);
						frmReportViewer.InstancePtr.Init(rptCLDailyAuditMaster.InstancePtr);
					}
					else
					{
						// both RE and PP
						rptCLDailyAuditMaster.InstancePtr.StartUp(true, false, cmbAlignment.SelectedIndex == 1, true);
						frmReportViewer.InstancePtr.Init(rptCLDailyAuditMaster.InstancePtr);
					}
				}
				else if (cmbAudit.SelectedIndex == 1)
				{
					// Daily Audit
					// closeout the payments (only in CL)
					modGlobal.Statics.glngCurrentCloseOut = modGlobal.CLCloseOut();
					// both RE and PP and close out all of the records
					rptCLDailyAuditMaster.InstancePtr.StartUp(true, true, cmbAlignment.SelectedIndex == 1, true);
					frmReportViewer.InstancePtr.Init(rptCLDailyAuditMaster.InstancePtr);
				}
				else
				{
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Running Audit");
			}
		}

		public void cmdDone_Click()
		{
			cmdDone_Click(cmdDone, new System.EventArgs());
		}

		private void frmCLDailyAudit_Activated(object sender, System.EventArgs e)
		{
			// kk  trocrs-3 Allow Bangor to fix missing CL CloseOut
			if (modGlobalConstants.Statics.gboolCR && false)
			{
				// this will turn off the audit report if the user has CR and will only allow
				// the user to preview the CL report not actually close out the payments
				if (cmbAudit.Items[1].ToString() == auxCmbAuditItem1)
				{
					cmbAudit.Items.RemoveAt(1);
				}
				ToolTip1.SetToolTip(cmbAudit, "The audit will be run when the TRIO Cash Receipting daily audit is run.");
			}
			else
			{
				bool itemExists = false;
				//search for item
				foreach (var item in cmbAudit.Items)
				{
					if (item.ToString() == auxCmbAuditItem1)
					{
						itemExists = true;
						if (cmbAudit.Items[1].ToString() != auxCmbAuditItem1)
						{
							for (int i = 0; i < cmbAudit.Items.Count; i++)
							{
								//if not in corect index
								if (cmbAudit.Items[i].ToString() == auxCmbAuditItem1)
								{
									//set at right index
									cmbAudit.Items.RemoveAt(i);
									itemExists = false;
									break;
								}
							}
						}
						break;
					}
				}
				if (!itemExists)
				{
					cmbAudit.Items.Insert(1, auxCmbAuditItem1);
				}
				ToolTip1.SetToolTip(cmbAudit, "");
			}
			// this will default the option button to RE or PP depending on the module that they are in
			if (modStatusPayments.Statics.boolRE)
			{
				cmbREPP.SelectedIndex = 0;
			}
			else
			{
				cmbREPP.SelectedIndex = 1;
			}
			cmbAudit.Visible = true;
		}

		private void frmCLDailyAudit_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCLDailyAudit.Icon	= "frmCLDailyAudit.frx":0000";
			//frmCLDailyAudit.FillStyle	= 0;
			//frmCLDailyAudit.ScaleWidth	= 9045;
			//frmCLDailyAudit.ScaleHeight	= 6930;
			//frmCLDailyAudit.LinkTopic	= "Form2";
			//frmCLDailyAudit.LockControls	= -1  'True;
			//frmCLDailyAudit.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "9";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//txtAuditPassword.IMEMode	= 3  'DISABLE;
			txtAuditPassword.PasswordChar = '*';
            //vsElasticLight1.OleObjectBlob	= "frmCLDailyAudit.frx":058A";
            //End Unmaped Properties
            try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsPD = new clsDRWrapper();
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
				modGlobalFunctions.SetTRIOColors(this);
				if (!modGlobalConstants.Statics.gboolCR)
				{
					// if there is no CR then check the pending discounts
					rsPD.OpenRecordset("SELECT * FROM PendingDiscount", modExtraModules.strCLDatabase);
					if (!rsPD.EndOfFile())
					{
						// pending discounts
						if (FCMessageBox.Show("There are pending discounts from the BL module.  Would you like to finalize the billing amounts?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Pending Discounts") == DialogResult.Yes)
						{
							modGlobalFunctions.AddCYAEntry_8("CL", "Finalized pending discounts.");
							modCLDiscount.FinalizePendingDiscounts();
						}
						else
						{
							modGlobalFunctions.AddCYAEntry_8("CL", "Disregarded pending discounts.");
						}
					}
                    rsPD.DisposeOf();
                }
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Loading Form");
			}
		}

		private void frmCLDailyAudit_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
            FormUtilities.KeyPressHandler(e, this);
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void ShowFrame(FCFrame fraFrame)
		{
			// this will place the frame in the middle of the form
			fraFrame.Top = FCConvert.ToInt32((this.Height - fraFrame.Height) / 3.0);
			fraFrame.Left = FCConvert.ToInt32((this.Width - fraFrame.Width) / 2.0);
			fraFrame.Visible = true;
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			cmdDone_Click();
		}

		private void optAudit_Click(int Index, object sender, System.EventArgs e)
		{
			if (Index == 0)
			{
				cmbREPP.Visible = true;
				lblREPP.Visible = true;
			}
			else
			{
				cmbREPP.Visible = false;
				lblREPP.Visible = false;
			}
		}

		private void optAudit_Click(object sender, System.EventArgs e)
		{
			int index = cmbAudit.SelectedIndex;
			optAudit_Click(index, sender, e);
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFilePrint_Click(sender, e);
		}
	}
}
