﻿using System;
using System.ComponentModel;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Wisej.Web;
using System.Runtime.InteropServices;
using GrapeCity.ActiveReports;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	//FC:FINAL:AM: renamed modCollections to modGlobal
	public class modGlobal
	{
		public const string DEFAULTDATABASE = "TWCL0000.VB1";
		// kk01182017 trocl-1326  Change to user configurable tax service email, but keep default emails
		public const string gstrDefTaxServiceEmail = "eastern_daq.edg@corelogic.com,mmcfadden@CoreLogic.com";
		// Public clsSecurity                      As clsTrioSecurity
		// security constants
		public const int CLSECURITYDAILYAUDIT = 4;
		public const int CLSECURITYAUDITPREVIEW = 20;
		public const int CLSECURITYPRINTAUDIT = 23;
		public const int CLSECURITYTELLERAUDIT = 22;
		public const int CLSECURITYMAINENTRY = 1;
		public const int CLSECURITYFILEMAINTENANCE = 7;
		public const int CLSECURITYLIENPROCESS = 12;
		public const int CLSECURITY30DAYNOTICE = 14;
		public const int CLSECURITYLIENEDITREPORT = 13;
		public const int CLSECURITYLIENMATURITY = 16;
		public const int CLSECURITYREMOVELIEN = 17;
		public const int CLSECURITYTRANSFERTAXTOLIEN = 15;
		public const int CLSECURITYLOADBACK = 6;
		public const int CLSECURITYMORTGAGEHOLDER = 8;
		public const int CLSECURITYPAYMENTS = 2;
		public const int CLSECURITYPURGERECORDS = 24;
		public const int CLSECURITYREMINDERNOTICES = 10;
		public const int CLSECURITYREPRINTAUDIT = 5;
		public const int CLSECURITYREPRINTLASTRECEIPT = 9;
		public const int CLSECURITYSTATUSLISTS = 3;
		public const int CLSECURITYTAXCLUB = 11;
		public const int CLSECURITYTAXCLUBREPORTS = 25;
		public const int CLSECURITYPRINTBOOKPAGEREPORT = 33;
		public const int CLSECURITYREPRINTCMFREPORT = 32;
		public const int CLSECURITYREPRINTPURGE = 31;
		public const int PRINTLOADBACKREPORT = 30;
		public const int CLSECURITYPRINTCERTOFRECOMMITMENT = 29;
		public const int CLSECURITYPRINTCERTOFSETTLEMENT = 28;
		public const int CLSECUTIRYPRINTLIENDISCHARGE = 27;
		public const int CLSECURITYFORECLOSUREREPORT = 26;
		public const int CLSECURITYVIEWACCOUNT = 18;
		public const int CLSECURITYINTERESTEDPARTIES = 34;
		public const int CLSECURITYPRINTTAXRATEREPORT = 35;
		// MAL@20080723: Add additional security options to cover the rest of the application
		// Tracker Reference: 14565
		public const int CLSECURITYREVERSEDEMANDFEES = 36;
		public const int CLSECURITYEDITLIENBOOKPAGE = 37;
		public const int CLSECURITYEDITLDNBOOKPAGE = 38;
		public const int CLSECURITYLIENDATECHART = 39;
		public const int CLSECURITYUPDATEDBOOKPAGERPT = 40;
		public const int CLSECURITYTAXACQUIREDPROPERTIES = 41;
		public const int CLSECURITYUPDATEBILLADDRESS = 42;
		public const int CLSECURITYCUSTOMIZE = 43;
		public const int CLSECURITYTAXSERVICEEXTRACT = 44;
		public const int CLSECURITYRATERECORDUPDATE = 45;
		public const int CLSECURITYEDITBILLINFO = 46;
		public const int CLSECURITYREMOVEFROMTAXACQUIRED = 47;
		public const int CRSECURITYREMOVELIENMATNOTICE = 23;
		public const int CLSECURITYAUTOPOSTJOURNAL = 48;
        public const int CLSECURITYPAYMENTACTIVITYREPORT = 49;

        public struct AccountFeesAdded
		{
			public int Account;
			// Location                            As String
			// Principal                           As Double
			public double CertifiedMailFee;
			public double Fee;
			public string Name;
			public bool Used;
			public bool Processed;
			public int BillKey;
			public int Year;
			public string OldAddress;
			public string NewAddress;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public AccountFeesAdded(int unusedParam)
			{
				this.Account = 0;
				this.CertifiedMailFee = 0;
				this.Fee = 0;
				this.Name = String.Empty;
				this.Used = false;
				this.Processed = false;
				this.BillKey = 0;
				this.Year = 0;
				this.OldAddress = String.Empty;
				this.NewAddress = String.Empty;
			}
		};

		//[DllImport("user32")]
		//public static extern int SetWindowPos(int hwnd, int hWndInsertAfter, int x, int Y, int cx, int cy, int wFlags);

		public static void Main(Autofac.IContainer container)
		{
			int lngErrCode = 0;
            InitializeCollectionsStatics();
            clsDRWrapper rsVersion = new clsDRWrapper();
            rsVersion.OpenRecordset("SELECT * FROM Collections", modExtraModules.strCLDatabase);
            if (rsVersion.EndOfFile() != true && rsVersion.BeginningOfFile() != true)
            {
                if (FCUtils.IsNull(rsVersion.Get_Fields_String("Version")) || FCConvert.ToString(rsVersion.Get_Fields_String("Version")) != FCConvert.ToString(App.Major) + "." + FCConvert.ToString(App.Minor) + "." + FCConvert.ToString(App.Revision))
                {
                    if (rsVersion.EndOfFile() != true && rsVersion.BeginningOfFile() != true)
                    {
                        rsVersion.Edit();
                    }
                    else
                    {
                        rsVersion.AddNew();
                    }
                    rsVersion.Set_Fields("Version", FCConvert.ToString(App.Major) + "." + FCConvert.ToString(App.Minor) + "." + FCConvert.ToString(App.Revision));
                    rsVersion.Update();
                }
            }
            MDIParent.InstancePtr.Init(container);
            //         try
            //{
            //	// On Error GoTo ERROR_HANDLER
            //	// kk04082015 troges-39  Change to single config file
            //	if (!modGlobalFunctions.LoadSQLConfig("TWCL0000.VB1"))
            //	{
            //		FCMessageBox.Show("Error loading connection information", MsgBoxStyle.Critical, "Error");
            //		return;
            //	}
            //	clsDRWrapper rsVersion = new clsDRWrapper();
            //	clsDRWrapper rsCreateTable = new clsDRWrapper();

            //	Statics.strDbRE = rsCreateTable.Get_GetFullDBName(modExtraModules.strREDatabase) + ".dbo.";
            //	Statics.strDbPP = rsCreateTable.Get_GetFullDBName(modExtraModules.strPPDatabase) + ".dbo.";
            //	Statics.strDbUT = rsCreateTable.Get_GetFullDBName(modExtraModules.strUTDatabase) + ".dbo.";
            //	Statics.strDbGNC = rsCreateTable.Get_GetFullDBName("CentralData") + ".dbo.";
            //	Statics.strDbGNS = rsCreateTable.Get_GetFullDBName("SystemSettings") + ".dbo.";
            //	Statics.strDbCP = rsCreateTable.Get_GetFullDBName("CentralParties") + ".dbo.";

            //	rsVersion.OpenRecordset("SELECT * FROM Collections", modExtraModules.strCLDatabase);
            //	if (rsVersion.EndOfFile() != true && rsVersion.BeginningOfFile() != true)
            //	{
            //		if (FCUtils.IsNull(rsVersion.Get_Fields_String("Version")) || FCConvert.ToString(rsVersion.Get_Fields_String("Version")) != FCConvert.ToString(App.Major) + "." + FCConvert.ToString(App.Minor) + "." + FCConvert.ToString(App.Revision))
            //		{
            //			if (rsVersion.EndOfFile() != true && rsVersion.BeginningOfFile() != true)
            //			{
            //				rsVersion.Edit();
            //			}
            //			else
            //			{
            //				rsVersion.AddNew();
            //			}
            //			rsVersion.Set_Fields("Version", FCConvert.ToString(App.Major) + "." + FCConvert.ToString(App.Minor) + "." + FCConvert.ToString(App.Revision));
            //			rsVersion.Update();
            //		}
            //	}
            //	// use the security class to make sure that the user is allowed into each option
            //	modGlobalConstants.Statics.clsSecurityClass = new clsTrioSecurity();
            //	lngErrCode = 1;
            //	modGlobalConstants.Statics.clsSecurityClass.Init("CL");
            //	lngErrCode = 2;
            //	modGlobalConstants.Statics.gstrUserID = modGlobalConstants.Statics.clsSecurityClass.Get_OpID();
            //	// save the user name
            //	lngErrCode = 3;
            //	modGlobalFunctions.GetGlobalRegistrySetting();
            //	CLSetup2();
            //	lngErrCode = 4;
            //	frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Checking Database", true, 20);
            //	frmWait.InstancePtr.Refresh();
            //	App.DoEvents();
            //	// XXX SQL Server Conversion
            //	CheckDatabaseStructure();
            //	lngErrCode = 5;
            //	modReplaceWorkFiles.GetGlobalVariables();
            //	// Write #12, "04 - " & Now & "    - Global Variables"
            //	frmWait.InstancePtr.IncrementProgress();
            //	lngErrCode = 6;
            //	modCLCalculations.CLSetup();
            //	frmWait.InstancePtr.IncrementProgress();
            //	GetGlobalPrintAdjustments();
            //	lngErrCode = 7;
            //	modGlobalFunctions.UpdateUsedModules();
            //	// this will find all the modules that the user has
            //	if (!modGlobalConstants.Statics.gboolCL)
            //	{
            //		FCMessageBox.Show("TRIO Collections has expired.  Please call TRIO!", MsgBoxStyle.Critical, "Module Expired");
            //		Application.Exit();
            //	}
            //	frmWait.InstancePtr.IncrementProgress();
            //	lngErrCode = 8;
            //	// need to find the PP last account accessed
            //	// Call GetKeyValues(HKEY_CURRENT_USER, REGISTRYKEY, "PPLastAccountNumber", CurrentAccountPP)
            //	modNewAccountBox.GetFormats();
            //	lngErrCode = 11;
            //	Statics.gboolMultipleTowns = modRegionalTown.IsRegionalTown();
            //	SetupGlobalQueries();
            //	// kgk 03292012  Change Stored Procs to global strings
            //	frmWait.InstancePtr.Unload();
            //	MDIParent.InstancePtr.Init(container);
            //	return;
            //}
            //catch (Exception ex)
            //{
            //	// ERROR_HANDLER:
            //	// Close 12
            //	frmWait.InstancePtr.Unload();
            //	FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Main - " + FCConvert.ToString(lngErrCode));
            //}
        }

        public static void InitializeCollectionsStatics()
        {
            if (string.IsNullOrWhiteSpace(modGlobalConstants.Statics.MuniName))
            {
                try
                {
                    // On Error GoTo ERROR_HANDLER
                    // kk04082015 troges-39  Change to single config file
                    if (!modGlobalFunctions.LoadSQLConfig("TWCL0000.VB1"))
                    {
                        FCMessageBox.Show("Error loading connection information", MsgBoxStyle.Critical, "Error");
                        return;
                    }

                    clsDRWrapper rsCreateTable = new clsDRWrapper();

                    Statics.strDbRE = rsCreateTable.Get_GetFullDBName(modExtraModules.strREDatabase) + ".dbo.";
                    Statics.strDbPP = rsCreateTable.Get_GetFullDBName(modExtraModules.strPPDatabase) + ".dbo.";
                    Statics.strDbUT = rsCreateTable.Get_GetFullDBName(modExtraModules.strUTDatabase) + ".dbo.";
                    Statics.strDbGNC = rsCreateTable.Get_GetFullDBName("CentralData") + ".dbo.";
                    Statics.strDbGNS = rsCreateTable.Get_GetFullDBName("SystemSettings") + ".dbo.";
                    Statics.strDbCP = rsCreateTable.Get_GetFullDBName("CentralParties") + ".dbo.";


                    // use the security class to make sure that the user is allowed into each option
                    modGlobalConstants.Statics.clsSecurityClass = new clsTrioSecurity();

                    modGlobalConstants.Statics.clsSecurityClass.Init("CL");

                    modGlobalConstants.Statics.gstrUserID = modGlobalConstants.Statics.clsSecurityClass.Get_OpID();
                    // save the user name

                    modGlobalFunctions.GetGlobalRegistrySetting();
                    CLSetup2();
                    CheckDatabaseStructure();
                    modReplaceWorkFiles.GetGlobalVariables();
                    modCLCalculations.CLSetup();
                    GetGlobalPrintAdjustments();

                    modGlobalFunctions.UpdateUsedModules();
                    // this will find all the modules that the user has
                    //if (!modGlobalConstants.Statics.gboolCL)
                    //{
                    //    FCMessageBox.Show("TRIO Collections has expired.  Please call TRIO!", MsgBoxStyle.Critical,
                    //        "Module Expired");
                    //    Application.Exit();
                    //}

                    modNewAccountBox.GetFormats();
                    Statics.gboolMultipleTowns = modRegionalTown.IsRegionalTown();
                    //SetupGlobalQueries();
                 
                    return;
                }
                catch (Exception ex)
                {
                    // ERROR_HANDLER:
                    // Close 12
                    frmWait.InstancePtr.Unload();
                    FCMessageBox.Show(
                        "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " +
                        ex.GetBaseException().Message + ".", MsgBoxStyle.Critical,
                        "Initialize Collections");
                }
            }
        }
		public static bool NewVersion(bool boolOverride = false)
		{
			bool NewVersion = false;
			// this is no longer for new versions.  It is for db maintenance now and is only called from the file menu.  The cCollectionsDB class called when entering collections (and billing) updates new versions of the db
			int lngErrCode = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// if it is new version then check the general entry table and make sure that all the correct fields are there
				clsDRWrapper rsVersion = new clsDRWrapper();
				bool boolUpdate;
				int lngMajor/*unused?*/;
				int lngMinor/*unused?*/;
				int lngRevsion/*unused?*/;
				bool boolNoEntryError = false;
				// MAL@20080121: Add increase to file locks
				//FC:TODO:AM
				//DBEngine.SetOption(dbMaxLocksPerFile, 200000);
				boolUpdate = false;
				lngErrCode = 1;
				frmWait.InstancePtr.IncrementProgress();
				lngErrCode = 5;
				if (boolOverride)
				{
					frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Checking Database", true, 10, false);
					frmWait.InstancePtr.Refresh();
					App.DoEvents();
				}
				frmWait.InstancePtr.IncrementProgress();
				// MAL@20080410: Remove bogus CHGINT lines with -1 Receipts
				// Tracker Reference: 13228
				ClearBogusCHGINTRecords();
				App.DoEvents();
				lngErrCode = 6;
				// check the saved reports
				FillDefaultFreeReports();
				frmWait.InstancePtr.IncrementProgress();
				lngErrCode = 1122;
				// MAL@20080110: Add records to permissions tables for new menu option
				// Tracker Reference: 11808
				AddSecurityPermissions();
				App.DoEvents();
				frmWait.InstancePtr.IncrementProgress();
				lngErrCode = 1119;
				if (boolOverride)
				{
					frmWait.InstancePtr.IncrementProgress();
					lngErrCode = 10;
				}
				frmWait.InstancePtr.IncrementProgress();
				rsVersion.Reset();
				lngErrCode = 30;
				lngErrCode = 31;
				frmWait.InstancePtr.IncrementProgress();
				lngErrCode = 78;
				rsVersion.OpenRecordset("SELECT * FROM Collections");
				if (rsVersion.EndOfFile())
				{
					rsVersion.AddNew();
				}
				else
				{
					rsVersion.Edit();
				}
				frmWait.InstancePtr.IncrementProgress();
				// make sure the default reports are set correctly
				if (FCConvert.ToString(rsVersion.Get_Fields_String("30DayNotice")) == "")
				{
					rsVersion.Set_Fields("30DayNotice", "30 Day Notice");
				}
				if (FCConvert.ToString(rsVersion.Get_Fields_String("LienNotice")) == "")
				{
					rsVersion.Set_Fields("LienNotice", "Tax Lien Certificate");
				}
				if (FCConvert.ToString(rsVersion.Get_Fields_String("MaturityNotice")) == "")
				{
					rsVersion.Set_Fields("MaturityNotice", "Lien Maturity Notice");
				}
				App.DoEvents();
				if (FCConvert.ToString(rsVersion.Get_Fields_String("30DayNoticeR")) == "")
				{
					rsVersion.Set_Fields("30DayNoticeR", "30 Day Notice Recommitment");
				}
				if (FCConvert.ToString(rsVersion.Get_Fields_String("LienNoticeR")) == "")
				{
					rsVersion.Set_Fields("LienNoticeR", "Tax Lien Certificate Recommitment");
				}
				if (FCConvert.ToString(rsVersion.Get_Fields_String("MaturityNoticeR")) == "")
				{
					rsVersion.Set_Fields("MaturityNoticeR", "Lien Maturity Notice Recommitment");
				}
				if (FCConvert.ToString(rsVersion.Get_Fields_String("RNForm")) == "")
				{
					rsVersion.Set_Fields("RNForm", "Reminder Notice Form");
				}
				if (FCConvert.ToString(rsVersion.Get_Fields_String("LienAbateText")) == "")
				{
					rsVersion.Set_Fields("LienAbateText", "Tax Collector");
				}
				frmWait.InstancePtr.IncrementProgress();
				lngErrCode = 80;
				rsVersion.Update();
				rsVersion.Reset();
				lngErrCode = 81;
				// this should stay all the time
				UpdateAbatementBoolean();
				frmWait.InstancePtr.IncrementProgress();
				App.DoEvents();
				lngErrCode = 90;
				// 02/02/2005
				// This will change the currency fields to force a decimal place value of 2
				ForceDecimalPlaces();
				if (boolOverride)
				{
					lngErrCode = 200;
				}
				frmWait.InstancePtr.IncrementProgress();
				App.DoEvents();
				NewVersion = !boolNoEntryError;
				frmWait.InstancePtr.Unload();
				if (!boolNoEntryError)
				{
					if (boolOverride)
					{
						FCMessageBox.Show("Check database structure successful.", MsgBoxStyle.Information, "Check Database Structure");
					}
				}
				else
				{
					rsVersion.OpenRecordset("SELECT * FROM Collections", modExtraModules.strCLDatabase);
					// kgkdone
					if (rsVersion.EndOfFile() != true && rsVersion.BeginningOfFile() != true)
					{
						// if there is a record
						rsVersion.Edit();
						rsVersion.Set_Fields("Version", "0.0.0");
						rsVersion.Update(true);
					}
				}
				return NewVersion;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + "." + "\r\n" + "Please make sure that everyone is out of the Collections database and run File Maintenance > Check Database Structure.", MsgBoxStyle.Information, "Update Collections Table - " + FCConvert.ToString(lngErrCode));
			}
			return NewVersion;
		}
		// VBto upgrade warning: FormName As Form	OnWrite(frmApplyDemandFees, frmLienMaturityFees, frmDateChange, frmLoadValidAccounts, frmCentralPartySearch, frmEditCentralParties, frmSelectBankNumber, frmSelectPostingJournal)
		// VBto upgrade warning: 'Return' As Variant --> As bool
		public static bool FormExist(Form FormName)
		{
			bool FormExist = false;
			foreach (Form frm in FCGlobal.Statics.Forms)
			{
				if (frm.Name == FormName.Name)
				{
					if (FCConvert.ToString(frm.Tag) == "Open")
					{
						FormExist = true;
						return FormExist;
					}
				}
			}
			FormName.Tag = "Open";
			return FormExist;
		}

		//public static bool IsFormLoaded_2(string strFormName)
		//{
		//	return IsFormLoaded(ref strFormName);
		//}

		//public static bool IsFormLoaded(ref string strFormName)
		//{
		//	bool IsFormLoaded = false;
		//	foreach (Form frm in FCGlobal.Statics.Forms)
		//	{
		//		if (frm.Name == strFormName)
		//		{
		//			IsFormLoaded = true;
		//			return IsFormLoaded;
		//		}
		//	}
		//	return IsFormLoaded;
		//}
		// kk09242015 trocls-63 Moved CMF code to modGlobalModules
		// X Public Function GetNextMailFormCode(strMFCode As String) As String
		// X End Function
		// X
		// X Public Function CheckCMFBarCode(strMFCode As String) As Boolean
		// X End Function
		private static void FillDefaultFreeReports()
		{
			int intErr = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsTest = new clsDRWrapper();
				clsDRWrapper rsFill = new clsDRWrapper();
				string strBuild;
				rsTest.OpenRecordset("SELECT * FROM GlobalVariables", "SystemSettings");
				// open the globalvariables table
				intErr = 1;
				if (!rsTest.EndOfFile())
				{
					// make sure that there was a record
					modGlobalConstants.Statics.gstrCityTown = FCConvert.ToString(rsTest.Get_Fields_String("CityTown"));
				}
				else
				{
					modGlobalConstants.Statics.MuniName = string.Empty;
					modGlobalConstants.Statics.gstrArchiveYear = string.Empty;
				}
				intErr = 2;
				rsTest.OpenRecordset("SELECT * FROM SavedFreeReports", modExtraModules.strCLDatabase);
				try_again:
				;
				intErr = 3;
				rsTest.FindFirstRecord("ReportName", "DEFAULT30 Day Notice");
				// kk08122014 trocls-49   '  "'DEFAULT30 Day Notice'"
				if (rsTest.NoMatch)
				{
					// none there...keep going
				}
				else
				{
					intErr = 4;
					// delete the current record and replace it with this one
					rsTest.Delete();
					rsTest.Update();
					// kk01162015 trocls-50  Fix errors on CDBS - changes to Delete method
					rsTest.MoveFirst();
					goto try_again;
				}
				intErr = 5;
				// Create the default 30 Day Notice
				rsFill.OpenRecordset("SELECT * FROM SavedFreeReports WHERE ID = -1", DEFAULTDATABASE);
				rsFill.AddNew();
				intErr = 6;
				rsFill.Set_Fields("ReportName", "DEFAULT30 Day Notice");
				rsFill.Set_Fields("Type", "30DAYNOTICE");
				strBuild = "\r\n" + "IMPORTANT: Do not disregard this notice.  You will lose your property unless you pay your <TAXYEAR> property taxes, interest and costs.  You may apply to the municipal officers for an abatement of taxes if, because of poverty or infirmity, you cannot pay the taxes that have been assessed.";
				strBuild += "\r\n" + "\r\n" + "<NAMEADDRESS>";
				if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
				{
					strBuild += "\r\n" + "\r\n" + "I, <COLLECTOR>, collector of taxes for the <CITYTOWNOF> of <MUNI>, a municipal corporation located in the county of <COUNTY>, state of <STATE>, hereby give you notice that a tax in the amount of $<PRINCIPAL> assessed and committed to me for collection on <COMMITMENT>, <LESSPAYMENTS> remains unpaid.  The tax was assessed against real estate in said <CITYTOWNOF> of <MUNI>, and against <OWNER> as owner(s) thereof, said real estate being described as follows:";
				}
				else
				{
					strBuild += "\r\n" + "\r\n" + "I, <COLLECTOR>, collector of taxes for the <MUNI>, located in the county of <COUNTY>, state of <STATE>, hereby give you notice that a tax in the amount of $<PRINCIPAL> assessed and committed to me for collection on <COMMITMENT>, <LESSPAYMENTS> remains unpaid.  The tax was assessed against real estate in said <MUNI>, and against <OWNER> as owner(s) thereof, said real estate being described as follows:";
				}
				strBuild += "\r\n" + "\r\n" + "Real estate located at: <LOCATION>";
				strBuild += "\r\n" + "\r\n" + "<COUNTY> Registry of Deeds reference: <BOOKPAGE>";
				strBuild += "\r\n" + "\r\n" + "Map and Lot description: <MAPLOT>";
				if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
				{
					strBuild += "\r\n" + "Map and lot numbers refer to such numbers as found on tax maps of the <CITYTOWNOF> of <MUNI> prepared by: <PREPARER> and dated <PREPAREDATE>, on file at the <CITYTOWNOF> of <MUNI> municipal offices.";
				}
				else
				{
					strBuild += "\r\n" + "Map and lot numbers refer to such numbers as found on tax maps of the <MUNI> prepared by: <PREPARER> and dated <PREPAREDATE>, on file at the <MUNI> municipal offices.";
				}
				strBuild += "\r\n" + "\r\n" + "I give you further notice that said tax, together with interest in the amount of $<INTEREST>, which has been added to and has become a part of said tax, remains unpaid; that payment of the said tax, together with a tax collectors demand fee of $<DEMAND> and the certified mail, return requested fee of $<CERTTOTAL>, for the sum total of $<TOTALDUE> is hereby demanded of you within thirty (30) days from the date of these presents, which is the date of mailing this notice; and that a lien is claimed on said real estate, above described, to secure the payment of said tax.";
				strBuild += "\r\n" + "\r\n" + "Dated at said <MUNI>, this <MAILDATE>.";
				strBuild += "\r\n" + "\r\n" + "NOTICE: The municipality has a policy under Title 36, M.R.S.A. Section 906 to apply all payments to the oldest outstanding tax obligation.  If you are uncertain of the status of taxes on this property, contact the Tax Collector.";
				rsFill.Set_Fields("ReportText", strBuild);
				rsFill.Update();
				intErr = 7;
				TRY_AGAIN2:
				;
				rsTest.FindFirstRecord("ReportName", "DEFAULTTax Lien Certificate");
				// kk08122014 trocls-49   '  "'DEFAULTTax Lien Certificate'"
				if (rsTest.NoMatch)
				{
					// none there...keep going
				}
				else
				{
					intErr = 8;
					// delete the current record and replace it with this one
					rsTest.Delete();
					rsTest.Update();
					// kk01162015 trocls-50  Fix errors on CDBS - changes to Delete method
					rsTest.MoveFirst();
					goto TRY_AGAIN2;
				}
				// create the default Tax Lien
				rsFill.OpenRecordset("SELECT * FROM SavedFreeReports WHERE ID = -1", DEFAULTDATABASE);
				rsFill.AddNew();
				intErr = 9;
				rsFill.Set_Fields("ReportName", "DEFAULTTax Lien Certificate");
				rsFill.Set_Fields("Type", "LIENPROCESS");
				strBuild = "\r\n";
				strBuild += "<ADDRESSBAR>" + "\r\n";
				strBuild += "\r\n";
				// Unload frmWait
				// MsgBox "1 - '" & gstrCityTown & "'"
				if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
				{
					// MsgBox "2 - Lead In"
					strBuild += "    I, <COLLECTOR>, Collector of Taxes for the <CITYTOWNOF> of <MUNI>, a municipal corporation located in the County of <COUNTY>, State of <STATE>, hereby give you notice that a tax in the amount of <PRINCIPAL> has been assessed, and was committed to me for collection on <COMMITMENT>, against real estate in said <CITYTOWNOF> of <MUNI>, and against <OWNER> as owner(s) thereof, said real estate being described as follows:" + "\r\n";
				}
				else
				{
					// MsgBox "3 - No Lead In"
					strBuild += "    I, <COLLECTOR>, Collector of Taxes for the <MUNI>, located in the County of <COUNTY>, State of <STATE>, hereby give you notice that a tax in the amount of <PRINCIPAL> has been assessed, and was committed to me for collection on <COMMITMENT>, against real estate in said <MUNI>, and against <OWNER> as owner(s) thereof, said real estate being described as follows:" + "\r\n";
				}
				strBuild += "\r\n";
				strBuild += "Real Estate located at:      <LOCATION>" + "\r\n";
				strBuild += "\r\n";
				strBuild += "Map Lot Description:         <MAPLOT>" + "\r\n";
				strBuild += "\r\n";
				strBuild += "Registry of Deeds reference: <BOOKPAGE>" + "\r\n";
				strBuild += "\r\n";
				// MsgBox "4 - '" & gstrCityTown & "'"
				if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
				{
					// MsgBox "5 - Lead In"
					strBuild += "Map and Lot numbers refer to such numbers as found on tax maps of the <CITYTOWNOF> of <MUNI>, prepared by: <PREPARER> and dated <PREPAREDATE>, on file at the <CITYTOWNOF> of <MUNI> municipal office." + "\r\n";
				}
				else
				{
					// MsgBox "6 - No Lead In"
					strBuild += "Map and Lot numbers refer to such numbers as found on tax maps of the <MUNI>, prepared by: <PREPARER> and dated <PREPAREDATE>, on file at the <MUNI> municipal office." + "\r\n";
				}
				strBuild += "\r\n";
				strBuild += "I give you further notice that said tax, together with interest in the amount of <INTEREST>, which has been added to and has become part of said tax, remains unpaid;  That a lien is claimed on said real estate, above described, to secure the payment of said tax; that proper demand for payment of said tax has been made in accordance with Title 36, Section 942, revised statutes of 1964, as amended." + "\r\n";
				strBuild += "\r\n";
				strBuild += "<SIGNATURELINE>" + "\r\n";
				strBuild += "\r\n";
				strBuild += "NOTICE: The municipality has policy under Title 36, M.R.S.A. Section 906 to apply all payments to the oldest outstanding tax obligation.  If you are uncertain of the status on this property, contact the Tax Collector." + "\r\n";
				strBuild += "NOTICE: Partial payments do not waive a lien." + "\r\n";
				strBuild += "\r\n";
				strBuild += "<COUNTY>,  SS. State of <STATE>  <MUNI>, <STATE>  <FILINGDATE>" + "\r\n";
				strBuild += "\r\n";
				strBuild += "Then personally appeared the above named <COLLECTOR>, Collector of Taxes, and acknowledged the foregoing instrument to be his free act and deed in his said capacity." + "\r\n";
				strBuild += "\r\n";
				strBuild += "                                    Before Me,__________________" + "\r\n";
				strBuild += "                                    <SIGNER>" + "\r\n";
				strBuild += "                                    <JOP>" + "\r\n";
				strBuild += "                                    <COMMISSIONEXPIRATION>" + "\r\n";
				rsFill.Set_Fields("ReportText", strBuild);
				rsFill.Update();
				intErr = 10;
				TRY_AGAIN3:
				;
				rsTest.FindFirstRecord("ReportName", "DEFAULTLien Maturity Notice");
				// kk08122014 trocls-49   '    "'DEFAULTLien Maturity Notice'"
				if (rsTest.NoMatch)
				{
					// none there...keep going
				}
				else
				{
					intErr = 11;
					// delete the current record and replace it with this one
					rsTest.Delete();
					rsTest.Update();
					// kk01162015 trocls-50  Fix errors on CDBS - changes to Delete method
					rsTest.MoveFirst();
					goto TRY_AGAIN3;
				}
				// Create the default Lien Maturity Notice
				rsFill.OpenRecordset("SELECT * FROM SavedFreeReports WHERE ID = -1", DEFAULTDATABASE);
				rsFill.AddNew();
				intErr = 12;
				rsFill.Set_Fields("ReportName", "DEFAULTLien Maturity Notice");
				rsFill.Set_Fields("Type", "LIENMATURITY");
				strBuild = "\r\n";
				strBuild += "                                                    <MAILDATE>" + "\r\n";
				strBuild += "\r\n";
				strBuild += "IMPORTANT: DO NOT DISREGARD THIS NOTICE.  YOU WILL LOSE YOUR PROPERTY UNLESS YOU PAY YOUR <TAXYEAR> PROPERTY TAXES, INTEREST AND COSTS." + "\r\n";
				strBuild += "\r\n";
				strBuild += "<ADDRESSBAR>" + "\r\n";
				strBuild += "\r\n";
				strBuild += "You are the party named on a tax lien certificate filed on <LIENFILINGDATE>, and recorded in <BOOKPAGE> in the <COUNTY> County Registry of Deeds.  This filing has created a tax lien mortgage on the real estate described therein." + "\r\n";
				strBuild += "\r\n";
				strBuild += "                Map & Lot:  <MAPLOT>" + "\r\n";
				strBuild += "                Location:   <LOCATION>" + "\r\n";
				strBuild += "\r\n";
				strBuild += "On <FORECLOSUREDATE>, the tax lien mortgage will be foreclosed and your right to recover your property by paying the taxes, interest and costs that are owed will expire." + "\r\n";
				strBuild += "\r\n";
				strBuild += "IF THE TAX LIEN FORECLOSES, THE MUNICIPALITY WILL OWN YOUR PROPERTY." + "\r\n";
				strBuild += "\r\n";
				strBuild += "If you cannot pay the property taxes you owe, please contact me to discuss this notice." + "\r\n";
				strBuild += "\r\n";
				strBuild += "                  *************************************" + "\r\n";
				strBuild += "                  * IF YOU ARE A DEBTOR IN BANKRUPTCY,*" + "\r\n";
				strBuild += "                  * THIS NOTICE DOES NOT APPLY TO YOU.*" + "\r\n";
				strBuild += "                  *************************************" + "\r\n";
				strBuild += "\r\n";
				rsFill.Set_Fields("ReportText", strBuild);
				rsFill.Update();
				intErr = 20;
				TRY_AGAIN4:
				;
				rsTest.FindFirstRecord("ReportName", "DEFAULT30 Day Notice Recommitment");
				// kk08122014 trocls-49   '   "'DEFAULT30 Day Notice Recommitment'"
				if (rsTest.NoMatch)
				{
					// none there...keep going
				}
				else
				{
					intErr = 21;
					// delete the current record and replace it with this one
					rsTest.Delete();
					rsTest.Update();
					// kk01162015 trocls-50  Fix errors on CDBS - changes to Delete method
					rsTest.MoveFirst();
					goto TRY_AGAIN4;
				}
				intErr = 22;
				// Create the default 30 Day Notice
				rsFill.OpenRecordset("SELECT * FROM SavedFreeReports WHERE ID = -1", DEFAULTDATABASE);
				rsFill.AddNew();
				intErr = 23;
				rsFill.Set_Fields("ReportName", "DEFAULT30 Day Notice Recommitment");
				rsFill.Set_Fields("Type", "30DAYNOTICE");
				strBuild = "\r\n" + "IMPORTANT: Do not disregard this notice.  You will lose your property unless you pay your <TAXYEAR> property taxes, interest and costs.  You may apply to the municipal officers for an abatement of taxes if, because of poverty or infirmity, you cannot pay the taxes that have been assessed.";
				strBuild += "\r\n" + "\r\n" + "<NAMEADDRESS>";
				if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
				{
					// MAL@20080408: Corrected wording ; Tracker Reference: 13172
					// strBuild = strBuild & vbCrLf & vbCrLf & "I, <COLLECTOR>, collector of taxes for the <CITYTOWNOF> of <MUNI>, a municipal corporation located in the county of <COUNTY>, state of <STATE>, hereby give you notice that a tax in the amount of $<PRINCIPAL> assessed and committed to <OLDCOLLECTOR> and recommitted to me, <COLLECTOR>, on <RECOMMITMENTDATE> for collection on <COMMITMENT>, <LESSPAYMENTS> remains unpaid.  The tax was assessed against real estate in said <CITYTOWNOF> of <MUNI>, and against <OWNER> as owner(s) thereof, said real estate being described as follows:"
					strBuild += "\r\n" + "\r\n" + "I, <COLLECTOR>, collector of taxes for the <CITYTOWNOF> of <MUNI>, a municipal corporation located in the county of <COUNTY>, state of <STATE>, hereby give you notice that a tax in the amount of $<PRINCIPAL> assessed and committed to <OLDCOLLECTOR> for collection on <COMMITMENT>, and recommitted to me, <COLLECTOR>, on <RECOMMITMENTDATE>, <LESSPAYMENTS> remains unpaid.  The tax was assessed against real estate in said <CITYTOWNOF> of <MUNI>, and against <OWNER> as owner(s) thereof, said real estate being described as follows:";
				}
				else
				{
					// strBuild = strBuild & vbCrLf & vbCrLf & "I, <COLLECTOR>, collector of taxes for the <MUNI>, located in the county of <COUNTY>, state of <STATE>, hereby give you notice that a tax in the amount of $<PRINCIPAL> assessed and committed to <OLDCOLLECTOR> and recommitted to me, <COLLECTOR>, on <RECOMMITMENTDATE> for collection on <COMMITMENT>, <LESSPAYMENTS> remains unpaid.  The tax was assessed against real estate in said <MUNI>, and against <OWNER> as owner(s) thereof, said real estate being described as follows:"
					strBuild += "\r\n" + "\r\n" + "I, <COLLECTOR>, collector of taxes for the <MUNI>, located in the county of <COUNTY>, state of <STATE>, hereby give you notice that a tax in the amount of $<PRINCIPAL> assessed and committed to <OLDCOLLECTOR> for collection on <COMMITMENT>, and recommitted to me, <COLLECTOR>, on <RECOMMITMENTDATE>, <LESSPAYMENTS> remains unpaid.  The tax was assessed against real estate in said <MUNI>, and against <OWNER> as owner(s) thereof, said real estate being described as follows:";
				}
				strBuild += "\r\n" + "\r\n" + "Real estate located at: <LOCATION>";
				strBuild += "\r\n" + "\r\n" + "<COUNTY> Registry of Deeds reference: <BOOKPAGE>";
				strBuild += "\r\n" + "\r\n" + "Map and Lot description: <MAPLOT>";
				if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
				{
					strBuild += "\r\n" + "Map and lot numbers refer to such numbers as found on tax maps of the <CITYTOWNOF> of <MUNI> prepared by: <PREPARER> and dated <PREPAREDATE>, on file at the <CITYTOWNOF> of <MUNI> municipal offices.";
				}
				else
				{
					strBuild += "\r\n" + "Map and lot numbers refer to such numbers as found on tax maps of the <MUNI> prepared by: <PREPARER> and dated <PREPAREDATE>, on file at the <MUNI> municipal offices.";
				}
				strBuild += "\r\n" + "\r\n" + "I give you further notice that said tax, together with interest in the amount of $<INTEREST>, which has been added to and has become a part of said tax, remains unpaid; that payment of the said tax, together with a tax collectors demand fee of $<DEMAND> and the certified mail, return requested fee of $<CERTTOTAL>, for the sum total of $<TOTALDUE> is hereby demanded of you within thirty (30) days from the date of these presents, which is the date of mailing this notice; and that a lien is claimed on said real estate, above described, to secure the payment of said tax.";
				strBuild += "\r\n" + "\r\n" + "Dated at said <MUNI>, this <MAILDATE>.";
				strBuild += "\r\n" + "\r\n" + "NOTICE: The municipality has a policy under Title 36, M.R.S.A. Section 906 to apply all payments to the oldest outstanding tax obligation.  If you are uncertain of the status of taxes on this property, contact the Tax Collector.";
				rsFill.Set_Fields("ReportText", strBuild);
				rsFill.Update();
				intErr = 24;
				TRY_AGAIN5:
				;
				rsTest.FindFirstRecord("ReportName", "DEFAULTTax Lien Certificate Recommitment");
				// kk08122014 trocls-49   '  "'DEFAULTTax Lien Certificate Recommitment'"
				if (rsTest.NoMatch)
				{
					// none there...keep going
				}
				else
				{
					intErr = 25;
					// delete the current record and replace it with this one
					rsTest.Delete();
					rsTest.Update();
					// kk01162015 trocls-50  Fix errors on CDBS - changes to Delete method
					rsTest.MoveFirst();
					goto TRY_AGAIN5;
				}
				intErr = 26;
				// create the default Tax Lien
				rsFill.OpenRecordset("SELECT * FROM SavedFreeReports WHERE ID = -1", DEFAULTDATABASE);
				rsFill.AddNew();
				intErr = 27;
				rsFill.Set_Fields("ReportName", "DEFAULTTax Lien Certificate Recommitment");
				rsFill.Set_Fields("Type", "LIENPROCESS");
				strBuild = "\r\n";
				strBuild += "<ADDRESSBAR>" + "\r\n";
				strBuild += "\r\n";
				if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
				{
					strBuild += "    I, <COLLECTOR>, Collector of Taxes for the <CITYTOWNOF> of <MUNI>, a municipal corporation located in the County of <COUNTY>, State of <STATE>, hereby give you notice that a tax in the amount of <PRINCIPAL> has been assessed, and was committed to <OLDCOLLECTOR> and recommitted to me, <COLLECTOR>, on <RECOMMITMENTDATE> for collection on <COMMITMENT>, against real estate in said <CITYTOWNOF> of <MUNI>, and against <OWNER> as owner(s) thereof, said real estate being described as follows:" + "\r\n";
				}
				else
				{
					strBuild += "    I, <COLLECTOR>, Collector of Taxes for the <MUNI>, located in the County of <COUNTY>, State of <STATE>, hereby give you notice that a tax in the amount of <PRINCIPAL> has been assessed, and was committed to <OLDCOLLECTOR> and recommitted to me, <COLLECTOR>, on <RECOMMITMENTDATE> for collection on <COMMITMENT>, against real estate in said <MUNI>, and against <OWNER> as owner(s) thereof, said real estate being described as follows:" + "\r\n";
				}
				strBuild += "\r\n";
				strBuild += "Real Estate located at:    <LOCATION>" + "\r\n";
				strBuild += "\r\n";
				strBuild += "Map Lot Description:       <MAPLOT>" + "\r\n";
				if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
				{
					strBuild += "Map and Lot numbers refer to such numbers as found on tax maps of the <CITYTOWNOF> of <MUNI>, prepared by: <PREPARER> and dated <PREPAREDATE>, on file at the <CITYTOWNOF> of <MUNI> municipal office." + "\r\n";
				}
				else
				{
					strBuild += "Map and Lot numbers refer to such numbers as found on tax maps of the <MUNI>, prepared by: <PREPARER> and dated <PREPAREDATE>, on file at the <MUNI> municipal office." + "\r\n";
				}
				strBuild += "\r\n";
				strBuild += "I give you further notice that said tax, together with interest in the amount of <INTEREST>, which has been added to and has become part of said tax, remains unpaid;  That a lien is claimed on said real estate, above descibed, to secure the payment of said tax; that proper demand for payment of said tax has been made in accordance with Title 36, Section 942, revised statutes of 1964, as amended." + "\r\n";
				strBuild += "\r\n";
				strBuild += "<SIGNATURELINE>" + "\r\n";
				strBuild += "\r\n";
				strBuild += "NOTICE: The municipality has policy under Title 36, M.R.S.A. Section 906 to apply all payments to the oldest outstanding tax obligation.  If you are uncertain of the status on this property, contact the Tax Collector." + "\r\n";
				strBuild += "NOTICE: Partial payments do not waive a lien." + "\r\n";
				strBuild += "\r\n";
				strBuild += "<COUNTY>,  SS. State of <STATE>  <MUNI>, <STATE>  <FILINGDATE>" + "\r\n";
				strBuild += "\r\n";
				strBuild += "Then personally appeared the above named <COLLECTOR>, Collector of Taxes, and acknowledged the foregoing instrument to be his free act and deed in his said capacity." + "\r\n";
				strBuild += "\r\n";
				strBuild += "                                    Before Me,__________________" + "\r\n";
				strBuild += "                                    <SIGNER>" + "\r\n";
				strBuild += "                                    <JOP>" + "\r\n";
				strBuild += "                                    <COMMISSIONEXPIRATION>" + "\r\n";
				rsFill.Set_Fields("ReportText", strBuild);
				rsFill.Update();
				intErr = 31;
				TRY_AGAIN6:
				;
				intErr = 50;
				rsTest.FindFirstRecord("ReportName", "DEFAULTReminder Notice Form");
				// kk08122014  trocls-49   '   "'DEFAULTReminder Notice Form'"
				if (rsTest.NoMatch)
				{
					// none there...keep going
				}
				else
				{
					intErr = 51;
					// delete the current record and replace it with this one
					rsTest.Delete();
					rsTest.Update();
					// kk01162015 trocls-50  Fix errors on CDBS - changes to Delete method
					rsTest.MoveFirst();
					goto TRY_AGAIN6;
				}
				intErr = 52;
				// create the default Reminder Notice
				rsFill.OpenRecordset("SELECT * FROM SavedFreeReports WHERE ID = -1", DEFAULTDATABASE);
				rsFill.AddNew();
				intErr = 53;
				rsFill.Set_Fields("ReportName", "DEFAULTReminder Notice Form");
				rsFill.Set_Fields("Type", "RNFORM");
				strBuild = "\r\n";
				strBuild += "Be aware that these amounts are as of <INTDATE>.  Please call the <INTDEPT> at <INTDEPTPHONE> for current amount due, before sending a payment." + "\r\n";
				strBuild += "\r\n" + "If you owe additional taxes for the year <LASTYEARMAT> or earlier, you will also be required to pay an additional fee not shown on this statement.";
				strBuild += "  Please contact the <MATDEPT> at <MATDEPTPHONE> for additional information." + "\r\n";
				strBuild += "\r\n" + "If you are making payments under a valid signed workout agreement with the Town, please continue to make your regular payments." + "\r\n";
				// strBuild = strBuild & vbCrLf & "Thank You."
				rsFill.Set_Fields("ReportText", strBuild);
				rsFill.Update();
				intErr = 54;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Resetting Default Reports - " + FCConvert.ToString(intErr));
			}
		}

		//private static void FixControlTables()
		//{
		//	int lngErrNum = 0;
		//	try
		//	{
		//		// On Error GoTo ERROR_HANDLER
		//		clsDRWrapper rsFix = new clsDRWrapper();
		//		string strTableName = "";
		//		string strSQL = "";
		//		lngErrNum = 1;
		//		// need to make sure that the field SendCopyToNewOwner is changed from a boolean to a string in the three control tables
		//		// With rsFix
		//		// 
		//		// control_30daynotice
		//		// strTableName = "Control_30DayNotice"
		//		// strSQL = "ALTER TABLE " & strTableName & " ALTER COLUMN SendCopyToNewOwner VarChar(150)"
		//		// rsFix.Execute strSQL, strCLDatabase
		//		// lngErrNum = 2
		//		// make sure that the fields are allowed to have a nullstring in them
		//		// .SetAllTextFieldsToAllowNull strTableName, strCLDatabase
		//		// lngErrNum = 3
		//		// 
		//		// control_lienprocess
		//		// strTableName = "Control_LienProcess"
		//		// strSQL = "ALTER TABLE " & strTableName & " ALTER COLUMN SendCopyToNewOwner VarChar(150)"
		//		// rsFix.Execute strSQL, strCLDatabase
		//		// lngErrNum = 4
		//		// make sure that the fields are allowed to have a nullstring in them
		//		// .SetAllTextFieldsToAllowNull strTableName, strCLDatabase
		//		// lngErrNum = 5
		//		// 
		//		// control_lienmaturity
		//		// strTableName = "Control_LienMaturity"
		//		// strSQL = "ALTER TABLE " & strTableName & " ALTER COLUMN SendCopyToNewOwner VarChar(150)"
		//		// rsFix.Execute strSQL, strCLDatabase
		//		// lngErrNum = 6
		//		// make sure that the fields are allowed to have a nullstring in them
		//		// .SetAllTextFieldsToAllowNull strTableName, strCLDatabase
		//		// lngErrNum = 7
		//		// .OpenRecordset "SELECT * FROM BillingMaster", strCLDatabase
		//		// Set rsFix = Nothing
		//		// lngErrNum = 8
		//		// 
		//		// this will add the new fields to the control fields
		//		// If Not rsFix.DoesFieldExist("DateUpdated", "Control_30DayNotice", strCLDatabase) Then
		//		// lngErrNum = 10
		//		// rsFix.UpdateDatabaseTable "Control_30DayNotice", strCLDatabase
		//		// lngErrNum = 11
		//		// rsFix.AddTableField "DateUpdated", dbDate
		//		// lngErrNum = 12
		//		// rsFix.UpdateTableCreation
		//		// lngErrNum = 13
		//		// 
		//		// End If
		//		// 
		//		// If Not rsFix.DoesFieldExist("DateUpdated", "Control_LienProcess", strCLDatabase) Then
		//		// lngErrNum = 15
		//		// rsFix.UpdateDatabaseTable "Control_LienProcess", strCLDatabase
		//		// lngErrNum = 16
		//		// rsFix.AddTableField "DateUpdated", dbDate
		//		// lngErrNum = 17
		//		// rsFix.UpdateTableCreation
		//		// lngErrNum = 18
		//		// 
		//		// End If
		//		// 
		//		// If Not rsFix.DoesFieldExist("DateUpdated", "Control_LienMaturity", strCLDatabase) Then
		//		// lngErrNum = 20
		//		// rsFix.UpdateDatabaseTable "Control_LienMaturity", strCLDatabase
		//		// lngErrNum = 21
		//		// rsFix.AddTableField "DateUpdated", dbDate
		//		// lngErrNum = 22
		//		// rsFix.UpdateTableCreation
		//		// lngErrNum = 23
		//		// 
		//		// End If
		//		// End With
		//		return;
		//	}
		//	catch (Exception ex)
		//	{
		//		// ERROR_HANDLER:
		//		frmWait.InstancePtr.Unload();
		//		FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Fixing Control Tables - " + FCConvert.ToString(lngErrNum));
		//	}
		//}

		//public static string FixSQLApostrophe(ref string strSQL)
		//{
		//	string FixSQLApostrophe = "";
		//	string strTemp = "";
		//	int lngPosition;
		//	lngPosition = 1;
		//	// FIX THIS!!!      08/19/2003
		//	lngPosition = Strings.InStr(lngPosition, strSQL, "'");
		//	while (!(lngPosition == 0))
		//	{
		//		if (lngPosition < 2)
		//		{
		//			if ((Strings.Mid(strSQL, lngPosition - 2) == "=" || Strings.Mid(strSQL, lngPosition - 1) == "=") || (Strings.Mid(strSQL, lngPosition - 2) == "<" || Strings.Mid(strSQL, lngPosition - 1) == "<") || (Strings.Mid(strSQL, lngPosition - 2) == ">" || Strings.Mid(strSQL, lngPosition - 1) == ">") || (Strings.UCase(Strings.Mid(strSQL, lngPosition - 5, 4)) == "LIKE" || Strings.UCase(Strings.Mid(strSQL, lngPosition - 4, 4)) == "LIKE") || (Strings.UCase(Strings.Mid(strSQL, lngPosition - 4, 4)) == "IN(" || Strings.UCase(Strings.Mid(strSQL, lngPosition - 4, 4)) == "IN (") || (Strings.UCase(Strings.Mid(strSQL, lngPosition - 9, 9)) == "BETWEEN(" || Strings.UCase(Strings.Mid(strSQL, lngPosition - 9, 9)) == "BETWEEN ("))
		//			{
		//			}
		//			else
		//			{
		//			}
		//			FCMessageBox.Show("Stop - In Loop");
		//			lngPosition = Strings.InStr(lngPosition, strSQL, "'");
		//		}
		//	}
		//	FCMessageBox.Show("Stop");
		//	FixSQLApostrophe = strTemp;
		//	return FixSQLApostrophe;
		//}

		private static void CLSetup2()
		{
			string strTemp = "";
			// Get Last Account Accessed From RE
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "RELastAccountNumber", ref strTemp);
            StaticSettings.TaxCollectionValues.LastAccountRE = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
			// Get Last Account Accessed From PP
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "PPLastAccountNumber", ref strTemp);
            StaticSettings.TaxCollectionValues.LastAccountPP = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
			// get the default receipt length
			//modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "CLNarrowReceipt", ref strTemp);
			//if (Strings.Trim(Strings.UCase(strTemp)) != "TRUE")
			//{
			//	Statics.gboolCLNarrowReceipt = false;
			//}
			//else
			//{
			//	Statics.gboolCLNarrowReceipt = true;
			//}
			//modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "CLModule", ref strTemp);
			//if (Strings.UCase(strTemp) != "PP")
			//{
			//	modStatusPayments.Statics.boolRE = true;
			//}
			//else
			//{
			//	modStatusPayments.Statics.boolRE = false;
			//}
			//if (Strings.Right(Application.StartupPath, 1) == "\\")
			//{
			//	App.HelpFile = Application.StartupPath + "TWCL0000.HLP";
			//}
			//else
			//{
			//	App.HelpFile = FCFileSystem.Statics.UserDataFolder + "\\" + "TWCL0000.HLP";
			//}
		}

		//private static bool CheckCloseOutTable()
		//{
		//	bool CheckCloseOutTable = false;
		//	try
		//	{
		//		// On Error GoTo ERROR_HANDLER
		//		// Dim rsCreateTable As New clsDRWrapper
		//		// 
		//		// CheckCloseOutTable = True
		//		// With rsCreateTable
		//		// Create the closeout table if it does not exist
		//		// If .CreateNewDatabaseTable("CloseOut", DEFAULTDATABASE) Then
		//		// CREATE THE NEW FIELDS
		//		// .CreateTableField "Key", dbLong
		//		// .CreateTableField "Type", dbText
		//		// .CreateTableField "Date", dbDate
		//		// .CreateTableField "TellerID", dbText
		//		// 
		//		// SET THE PROPERTIES OF THE NEW FIELDS
		//		// .SetFieldAttribute "Key", dbAutoIncrField
		//		// .SetFieldDefaultValue "Date", "NOW()"
		//		// .SetFieldAllowZeroLength "Type", True
		//		// .SetFieldAllowZeroLength "TellerID", True
		//		// 
		//		// DO THE ACTUAL CREATION OF THE TABLE
		//		// .UpdateTableCreation
		//		// End If
		//		// End With
		//		return CheckCloseOutTable;
		//	}
		//	catch (Exception ex)
		//	{
		//		// ERROR_HANDLER:
		//		CheckCloseOutTable = false;
		//		FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Creating Close Out Table");
		//	}
		//	return CheckCloseOutTable;
		//}

		public static int CLCloseOut()
		{
			int CLCloseOut = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will close out all of the payments
				string strSQL;
				clsDRWrapper rsUpdate = new clsDRWrapper();
				// this will find the next close out number
				rsUpdate.OpenRecordset("SELECT * FROM CloseOut", modExtraModules.strCLDatabase);
				rsUpdate.AddNew();
				// CLCloseOut = rsUpdate.Get_Fields("Key")
				rsUpdate.Set_Fields("Type", "CL");
				rsUpdate.Set_Fields("CloseOutDate", DateTime.Now);
				rsUpdate.Update();
				CLCloseOut = FCConvert.ToInt32(rsUpdate.Get_Fields_Int32("ID"));
				// update all of the payments with this number
				strSQL = "UPDATE PaymentRec SET DailyCloseOut = " + FCConvert.ToString(CLCloseOut) + " WHERE ISNULL(DailyCloseOut,0) = 0";
				if (rsUpdate.Execute(strSQL, modExtraModules.strCLDatabase))
				{
					FCMessageBox.Show("Records Updated.", MsgBoxStyle.Information, "Update Successful");
				}
				else
				{
					FCMessageBox.Show("Records not Updated.", MsgBoxStyle.Critical, "Update Error");
				}
				return CLCloseOut;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In CL Close Out");
			}
			return CLCloseOut;
		}

		//private static void FixPositiveMaturityFee()
		//{
		//	try
		//	{
		//		// On Error GoTo ERROR_HANDLER
		//		// this function will fix the positive maturity fees in the lien records
		//		// this was an error that was fixed in the fall of '03
		//		clsDRWrapper rsFix = new clsDRWrapper();
		//		rsFix.OpenRecordset("SELECT * FROM LienRec WHERE MaturityFee > 0", modExtraModules.strCLDatabase);
		//		while (!rsFix.EndOfFile())
		//		{
		//			rsFix.Edit();
		//			// TODO Get_Fields: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
		//			rsFix.Set_Fields("MaturityFee", Math.Abs(FCConvert.ToInt16(rsFix.Get_Fields("MaturityFee"))) * -1);
		//			rsFix.Update();
		//			rsFix.MoveNext();
		//		}
		//		return;
		//	}
		//	catch (Exception ex)
		//	{
		//		// ERROR_HANDLER:
		//		FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Fixing Positive Maturity");
		//	}
		//}

		public static void TaxClubBooklet()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this routine will print a tax club booklet for any account that has a tax club setup for it
				int lngAcct/*unused?*/;
				int lngTC/*unused?*/;
				clsDRWrapper rsTC = new clsDRWrapper();
				// get the account number
				// If Not frmInput.Init(lngAcct, "Account Number", "Enter Account Number", , , idtWholeNumber) Then
				// MsgBox "Invalid Account Entered.", MsgBoxStyle.Exclamation, "Invalid Account"
				// Exit Sub
				// End If
				// 
				// If lngAcct > 0 Then
				// rsTC.OpenRecordset "SELECT * FROM TaxClubJoin WHERE Account = " & lngAcct & " ORDER BY BillingYear desc"
				// If Not rsTC.EndOfFile Then
				frmTaxClubBooklet.InstancePtr.Init(0);
				// rsTC.Get_Fields("Key")
				// Else
				// MsgBox "No Tax Club setup for account " & lngAcct & ".", MsgBoxStyle.Exclamation, "No Tax Club Setup"
				// Exit Sub
				// End If
				// Else
				// MsgBox "Invalid Account Entered.", MsgBoxStyle.Exclamation, "Invalid Account"
				// Exit Sub
				// End If
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In Tax Club Booklet");
			}
		}

		private static void UpdateAbatementBoolean()
		{
			int lngKey = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will check all bills and set the abatement boolean on any bill that has an abatement payment made to it
				// this will make more accounts be checked when doing reminder notices and status lists
				clsDRWrapper rsAbate = new clsDRWrapper();
				clsDRWrapper rsAdjust = new clsDRWrapper();
				string strTemp;
				// this will find all the regular bills that have abatement payments against it
				// this should not occur on lien accounts
				strTemp = "SELECT Distinct BillKey FROM PaymentRec WHERE Code = 'A' AND BillCode = 'R'";
				rsAbate.OpenRecordset(strTemp, modExtraModules.strCLDatabase);
				while (!rsAbate.EndOfFile())
				{
					App.DoEvents();
					lngKey = FCConvert.ToInt32(rsAbate.Get_Fields_Int32("BillKey"));
					strTemp = "UPDATE BillingMaster SET AbatementPaymentMade = 1 WHERE ID = " + FCConvert.ToString(rsAbate.Get_Fields_Int32("BillKey"));
					rsAdjust.Execute(strTemp, modExtraModules.strCLDatabase);
					rsAbate.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Updating Abatement Check - " + FCConvert.ToString(lngKey));
			}
		}

		//public static void UpdateCLAppliedThroughDate(int lngKey)
		//{
		//	// this will update the CHGINT record's receipt number so that it is not still pending
		//	clsDRWrapper rsCreatePy = new clsDRWrapper();
		//	clsDRWrapper rsBatchCheck = new clsDRWrapper();
		//	clsDRWrapper rsTemp = new clsDRWrapper();
		//	DateTime dtSwap = DateTime.FromOADate(0);
		//	int intArrInd;
		//	int I/*unused?*/;
		//	int lngArrCT;
		//	// this will open the CHGINT record from the payment record
		//	rsCreatePy.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = " + FCConvert.ToString(lngKey), modExtraModules.strCLDatabase);
		//	if (rsCreatePy.EndOfFile() != true && rsCreatePy.BeginningOfFile() != true)
		//	{
		//		rsCreatePy.Edit();
		//		if (FCConvert.ToString(rsCreatePy.Get_Fields_String("BillCode")) != "L")
		//		{
		//			// regular billing
		//			// open the billing record for this payment
		//			rsTemp.OpenRecordset("SELECT * FROM BillingMaster WHERE ID = " + FCConvert.ToString(rsCreatePy.Get_Fields_Int32("BillKey")), modExtraModules.strCLDatabase);
		//			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
		//			{
		//				// adjust all of the account totals for this billing
		//				rsTemp.Edit();
		//				if (!(rsCreatePy.Get_Fields_DateTime("CHGINTDate") is DateTime))
		//				{
		//					// if not then is the CHGINTDate field filled
		//					rsTemp.Set_Fields("InterestAppliedThroughDate", modStatusPayments.Statics.EffectiveDate);
		//				}
		//				else
		//				{
		//					if (rsCreatePy.Get_Fields_DateTime("CHGINTDate") is DateTime)
		//					{
		//						rsTemp.Set_Fields("InterestAppliedThroughDate", rsCreatePy.Get_Fields_DateTime("CHGINTDate"));
		//					}
		//					else
		//					{
		//						rsTemp.Set_Fields("InterestAppliedThroughDate", modStatusPayments.Statics.EffectiveDate);
		//						// rsCreatePy.Get_Fields("CHGINTDate")
		//					}
		//				}
		//				for (lngArrCT = 0; lngArrCT <= Information.UBound(modStatusPayments.Statics.CHGINTYearDate, 1); lngArrCT++)
		//				{
		//					if (modStatusPayments.Statics.CHGINTYearDate[lngArrCT].Reversal)
		//					{
		//						if (modStatusPayments.Statics.CHGINTYearDate[lngArrCT].BillKey == ((rsCreatePy.Get_Fields_Int32("BillKey"))))
		//						{
		//							rsTemp.Set_Fields("InterestAppliedThroughDate", modStatusPayments.Statics.CHGINTYearDate[lngArrCT].InterestAppliedThroughDate);
		//							break;
		//						}
		//					}
		//				}
		//				if (FCConvert.ToString(rsCreatePy.Get_Fields_String("Reference")) == "CHGINT" || FCConvert.ToString(rsCreatePy.Get_Fields_String("Reference")) == "EARNINT")
		//				{
		//					// set the interest applied date to the CHGINT Date so that I could track the dates backwards
		//					if (!(rsCreatePy.Get_Fields_DateTime("CHGINTDate") is DateTime))
		//					{
		//						rsCreatePy.Set_Fields("CHGINTDate", dtSwap);
		//					}
		//				}
		//				rsTemp.Update();
		//			}
		//		}
		//		else
		//		{
		//			// lien
		//			rsTemp.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(rsCreatePy.Get_Fields_Int32("BillKey")), modExtraModules.strCLDatabase);
		//			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
		//			{
		//				rsTemp.Edit();
		//				for (intArrInd = 1; intArrInd <= modStatusPayments.MAX_PAYMENTS; intArrInd++)
		//				{
		//					// this finds the correct record in the PaymentArray
		//					if (modStatusPayments.Statics.PaymentArray[intArrInd].ID == ((rsCreatePy.Get_Fields_Int32("ID"))))
		//					{
		//						if (modStatusPayments.Statics.PaymentArray[intArrInd].CHGINTDate.ToOADate() != 0)
		//							break;
		//					}
		//				}
		//				if (intArrInd > modStatusPayments.MAX_PAYMENTS)
		//				{
		//					// this finds out if there was not a matching element in the payment array
		//					if (!(rsCreatePy.Get_Fields_DateTime("CHGINTDate") is DateTime))
		//					{
		//						// if not then is the CHGINTDate field filled
		//						rsTemp.Set_Fields("InterestAppliedThroughDate", rsCreatePy.Get_Fields_DateTime("EffectiveInterestDate"));
		//					}
		//					else
		//					{
		//						if (rsCreatePy.Get_Fields_DateTime("CHGINTDate") is DateTime)
		//						{
		//							rsTemp.Set_Fields("InterestAppliedThroughDate", rsCreatePy.Get_Fields_DateTime("CHGINTDate"));
		//						}
		//						else
		//						{
		//							rsTemp.Set_Fields("InterestAppliedThroughDate", modStatusPayments.Statics.EffectiveDate);
		//							// rsCreatePy.Get_Fields("CHGINTDate")
		//						}
		//					}
		//				}
		//				else
		//				{
		//					rsTemp.Set_Fields("InterestAppliedThroughDate", modStatusPayments.Statics.PaymentArray[intArrInd].CHGINTDate);
		//				}
		//				for (lngArrCT = 0; lngArrCT <= Information.UBound(modStatusPayments.Statics.CHGINTYearDate, 1); lngArrCT++)
		//				{
		//					if (modStatusPayments.Statics.CHGINTYearDate[lngArrCT].Reversal)
		//					{
		//						if (modStatusPayments.Statics.CHGINTYearDate[lngArrCT].BillKey == ((rsCreatePy.Get_Fields_Int32("BillKey"))))
		//						{
		//							rsTemp.Set_Fields("InterestAppliedThroughDate", modStatusPayments.Statics.CHGINTYearDate[lngArrCT].InterestAppliedThroughDate);
		//							break;
		//						}
		//					}
		//				}
		//				rsTemp.Update();
		//			}
		//		}
		//		rsCreatePy.Update();
		//	}
		//}

		//public static void PrintCLReceipt()
		//{
  //          if (ClientPrintingSetup.InstancePtr.CheckConfiguration())
  //          {
  //              arCLReceipt.InstancePtr.PrintReportOnDotMatrix("RCPTPrinterName");
  //          }
  //      }

		public static void ClearCMFNumbers(bool boolOverride = false)
		{
			clsDRWrapper rsVersion = new clsDRWrapper();
			if (boolOverride)
			{
				rsVersion.Execute("DELETE FROM CMFNumbers", modExtraModules.strCLDatabase);
				modGlobalFunctions.AddCYAEntry_26("CL", "Clearing CMF Numbers Table", "Override");
			}
			else
			{
				if (FCMessageBox.Show("Are you sure that you would like to clear the Certified Mail Form Table?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Clear Table") == DialogResult.Yes)
				{
					rsVersion.Execute("DELETE FROM CMFNumbers", modExtraModules.strCLDatabase);
					modGlobalFunctions.AddCYAEntry_8("CL", "Clearing CMF Numbers Table");
					FCMessageBox.Show("Table cleared.", MsgBoxStyle.Exclamation, "Success");
				}
				else
				{
					// do nothing
				}
			}
		}

		public static string GetBulkMailerString()
		{
			string GetBulkMailerString = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsBulk = new clsDRWrapper();
				int intCT;
				rsBulk.OpenRecordset("SELECT * FROM GlobalVariables", "SystemSettings");
				if (!rsBulk.EndOfFile())
				{
					for (intCT = 1; intCT <= 5; intCT++)
					{
						// TODO Get_Fields: Field [BulkMail] not found!! (maybe it is an alias?)
						if (!rsBulk.IsFieldNull("BulkMail" + FCConvert.ToString(intCT)))
						{
							// TODO Get_Fields: Field [BulkMail] not found!! (maybe it is an alias?)
							GetBulkMailerString = GetBulkMailerString + FCConvert.ToString(rsBulk.Get_Fields("BulkMail" + FCConvert.ToString(intCT))) + "\r\n";
						}
					}
				}
				else
				{
					GetBulkMailerString = "";
				}
				return GetBulkMailerString;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Getting Bulk Mailing Text");
			}
			return GetBulkMailerString;
		}

		public static void CreateTransferTaxToLienBDEntry(double dblTotalPrin, double dblTATotalPrin, int lngYear, int lngTranCode = 0, int lngJournalNumber = 0)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this routine will create an entry in the BD database that will move monies from
				// the tax receivable account to the lien receivable account
				// kk10162104 And from the tax acquired receivable account to the tax acquired lien receivable account
				clsDRWrapper rsTax = new clsDRWrapper();
				clsDRWrapper Master = new clsDRWrapper();
				clsDRWrapper rsEntries = new clsDRWrapper();
				string strAccountNumber = "";
				string strLAccountNumber = "";
				string strTAAccountNumber = "";
				string strTALAccountNumber = "";
				string strCRAccountNumber = "";
				string strCRLAccountNumber = "";
				string strCRTAAccountNumber = "";
				string strCRTALAccountNumber = "";
				int intFund = 0;
				string strTableName = "";
				int lngJournal = 0;
				clsBudgetaryPosting tPostInfo;
				// kk01282015 trocls-51  Add automatic posting of journals
				clsPostingJournalInfo tJournalInfo;
				// Find Receivable Account Number from BD
				Master.OpenRecordset("SELECT * FROM StandardAccounts", modExtraModules.strBDDatabase);
				if (!Master.EndOfFile())
				{
					// Master.FindFirstRecord , , "Code = 'RC'"
					Master.FindFirstRecord("Code", "RR");
					// kk08112014  trocls-49   "'RR'"
					if (!Master.NoMatch)
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						strAccountNumber = FCConvert.ToString(Master.Get_Fields("Account"));
					}
					Master.FindFirstRecord("Code", "LR");
					// kk08112014  trocls-49   "'RR'"
					if (!Master.NoMatch)
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						strLAccountNumber = FCConvert.ToString(Master.Get_Fields("Account"));
					}
					// kk10162014 trocl-1192  Post to correct accounts for Tax Acquired accounts
					if (dblTATotalPrin != 0)
					{
						Master.FindFirstRecord("Code", "TR");
						if (!Master.NoMatch)
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							strTAAccountNumber = FCConvert.ToString(Master.Get_Fields("Account"));
						}
						Master.FindFirstRecord("Code", "LV");
						if (!Master.NoMatch)
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							strTALAccountNumber = FCConvert.ToString(Master.Get_Fields("Account"));
						}
					}
				}
				if (modGlobalConstants.Statics.gboolCR)
				{
					if (Statics.gboolMultipleTowns)
					{
						Master.OpenRecordset("SELECT * FROM Type WHERE Typecode = 90 AND TownKey = " + FCConvert.ToString(lngTranCode), modExtraModules.strCRDatabase);
						if (!Master.EndOfFile())
						{
							if (FCConvert.ToBoolean(Master.Get_Fields_Boolean("Year1")))
							{
								strCRAccountNumber = Strings.Left(FCConvert.ToString(Master.Get_Fields_String("Account1")), FCConvert.ToString(Master.Get_Fields_String("Account1")).Length - 2) + Strings.Format(lngYear % 100, "00");
							}
							else
							{
								strCRAccountNumber = FCConvert.ToString(Master.Get_Fields_String("Account1"));
							}
						}
						Master.OpenRecordset("SELECT * FROM Type WHERE Typecode = 91 AND TownKey = " + FCConvert.ToString(lngTranCode), modExtraModules.strCRDatabase);
						if (!Master.EndOfFile())
						{
							if (FCConvert.ToBoolean(Master.Get_Fields_Boolean("Year1")))
							{
								strCRLAccountNumber = Strings.Left(FCConvert.ToString(Master.Get_Fields_String("Account1")), FCConvert.ToString(Master.Get_Fields_String("Account1")).Length - 2) + Strings.Format(lngYear % 100, "00");
							}
							else
							{
								strCRLAccountNumber = FCConvert.ToString(Master.Get_Fields_String("Account1"));
							}
						}
						// kk10162014 trocl-1192  Post to correct accounts for Tax Acquired accounts
						if (dblTATotalPrin != 0)
						{
							Master.OpenRecordset("SELECT * FROM Type WHERE Typecode = 890 AND TownKey = " + FCConvert.ToString(lngTranCode), modExtraModules.strCRDatabase);
							if (!Master.EndOfFile())
							{
								if (FCConvert.ToBoolean(Master.Get_Fields_Boolean("Year1")))
								{
									strCRTAAccountNumber = Strings.Left(FCConvert.ToString(Master.Get_Fields_String("Account1")), FCConvert.ToString(Master.Get_Fields_String("Account1")).Length - 2) + Strings.Format(lngYear % 100, "00");
								}
								else
								{
									strCRTAAccountNumber = FCConvert.ToString(Master.Get_Fields_String("Account1"));
								}
							}
							Master.OpenRecordset("SELECT * FROM Type WHERE Typecode = 891 AND TownKey = " + FCConvert.ToString(lngTranCode), modExtraModules.strCRDatabase);
							if (!Master.EndOfFile())
							{
								if (FCConvert.ToBoolean(Master.Get_Fields_Boolean("Year1")))
								{
									strCRTALAccountNumber = Strings.Left(FCConvert.ToString(Master.Get_Fields_String("Account1")), FCConvert.ToString(Master.Get_Fields_String("Account1")).Length - 2) + Strings.Format(lngYear % 100, "00");
								}
								else
								{
									strCRTALAccountNumber = FCConvert.ToString(Master.Get_Fields_String("Account1"));
								}
							}
						}
					}
					else
					{
						Master.OpenRecordset("SELECT * FROM Type WHERE Typecode = 90", modExtraModules.strCRDatabase);
						if (!Master.EndOfFile())
						{
							if (FCConvert.ToBoolean(Master.Get_Fields_Boolean("Year1")))
							{
								strCRAccountNumber = Strings.Left(FCConvert.ToString(Master.Get_Fields_String("Account1")), FCConvert.ToString(Master.Get_Fields_String("Account1")).Length - 2) + Strings.Format(lngYear % 100, "00");
							}
							else
							{
								strCRAccountNumber = FCConvert.ToString(Master.Get_Fields_String("Account1"));
							}
						}
						Master.OpenRecordset("SELECT * FROM Type WHERE Typecode = 91", modExtraModules.strCRDatabase);
						if (!Master.EndOfFile())
						{
							if (FCConvert.ToBoolean(Master.Get_Fields_Boolean("Year1")))
							{
								strCRLAccountNumber = Strings.Left(FCConvert.ToString(Master.Get_Fields_String("Account1")), FCConvert.ToString(Master.Get_Fields_String("Account1")).Length - 2) + Strings.Format(lngYear % 100, "00");
							}
							else
							{
								strCRLAccountNumber = FCConvert.ToString(Master.Get_Fields_String("Account1"));
							}
						}
						// kk10162014 trocl-1192  Post to correct accounts for Tax Acquired accounts
						if (dblTATotalPrin != 0)
						{
							Master.OpenRecordset("SELECT * FROM Type WHERE Typecode = 890", modExtraModules.strCRDatabase);
							if (!Master.EndOfFile())
							{
								if (FCConvert.ToBoolean(Master.Get_Fields_Boolean("Year1")))
								{
									strCRTAAccountNumber = Strings.Left(FCConvert.ToString(Master.Get_Fields_String("Account1")), FCConvert.ToString(Master.Get_Fields_String("Account1")).Length - 2) + Strings.Format(lngYear % 100, "00");
								}
								else
								{
									strCRTAAccountNumber = FCConvert.ToString(Master.Get_Fields_String("Account1"));
								}
							}
							Master.OpenRecordset("SELECT * FROM Type WHERE Typecode = 891", modExtraModules.strCRDatabase);
							if (!Master.EndOfFile())
							{
								if (FCConvert.ToBoolean(Master.Get_Fields_Boolean("Year1")))
								{
									strCRTALAccountNumber = Strings.Left(FCConvert.ToString(Master.Get_Fields_String("Account1")), FCConvert.ToString(Master.Get_Fields_String("Account1")).Length - 2) + Strings.Format(lngYear % 100, "00");
								}
								else
								{
									strCRTALAccountNumber = FCConvert.ToString(Master.Get_Fields_String("Account1"));
								}
							}
						}
					}
				}
				else
				{
					// must find fund from the CL Customize screen for towns with CL+BD but not CR
					intFund = 1;
					// get the fund and account from CL and use 00 for year
					strCRAccountNumber = "G " + Strings.Format(intFund, Strings.Mid(modAccountTitle.Statics.Ledger, 1, 2)) + "-" + strAccountNumber + "-00";
					strCRLAccountNumber = "G " + Strings.Format(intFund, Strings.Mid(modAccountTitle.Statics.Ledger, 1, 2)) + "-" + strLAccountNumber + "-00";
					// kk10162014 trocl-1192  Post to correct accounts for Tax Acquired accounts
					if (dblTATotalPrin != 0)
					{
						strCRTAAccountNumber = "G " + Strings.Format(intFund, Strings.Mid(modAccountTitle.Statics.Ledger, 1, 2)) + "-" + strTAAccountNumber + "-00";
						strCRTALAccountNumber = "G " + Strings.Format(intFund, Strings.Mid(modAccountTitle.Statics.Ledger, 1, 2)) + "-" + strTALAccountNumber + "-00";
					}
				}
				if (Strings.Trim(strAccountNumber) == "")
				{
					frmWait.InstancePtr.Unload();
					FCMessageBox.Show("Please enter the Real Estate Receivable Account in Budgetary.", MsgBoxStyle.Exclamation, "Missing Receivable Account");
				}
				if (Strings.Trim(strLAccountNumber) == "")
				{
					frmWait.InstancePtr.Unload();
					FCMessageBox.Show("Please enter the Lien Receivable Account in Budgetary.", MsgBoxStyle.Exclamation, "Missing Receivable Account");
				}
				if (Strings.Trim(strCRAccountNumber) == "")
				{
					frmWait.InstancePtr.Unload();
					FCMessageBox.Show("Please enter the Real Estate Receivable Account in Cash Receipts.", MsgBoxStyle.Exclamation, "Missing Account");
				}
				if (Strings.Trim(strCRLAccountNumber) == "")
				{
					frmWait.InstancePtr.Unload();
					FCMessageBox.Show("Please enter the Lien Receivable Account in Cash Receipts.", MsgBoxStyle.Exclamation, "Missing Account");
				}
				if (Conversion.Val(Strings.Mid(strCRLAccountNumber, FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 1, 2)) + 4), FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))))) != Conversion.Val(strLAccountNumber))
				{
					frmWait.InstancePtr.Unload();
					FCMessageBox.Show("Your Lien Receivable Account in Cash Receipts and Budgetary do not match.", MsgBoxStyle.Exclamation, "Missing Account");
				}
				// kk10162014 trocl-1192  Post to correct accounts for Tax Acquired accounts
				if (dblTATotalPrin != 0)
				{
					if (Strings.Trim(strTAAccountNumber) == "")
					{
						frmWait.InstancePtr.Unload();
						FCMessageBox.Show("Please enter the Tax Acquired Receivable Account in Budgetary.", MsgBoxStyle.Exclamation, "Missing Receivable Account");
					}
					if (Strings.Trim(strTALAccountNumber) == "")
					{
						frmWait.InstancePtr.Unload();
						FCMessageBox.Show("Please enter the Tax Acquired Lien Receivable Account in Budgetary.", MsgBoxStyle.Exclamation, "Missing Receivable Account");
					}
					if (Strings.Trim(strCRTAAccountNumber) == "")
					{
						frmWait.InstancePtr.Unload();
						FCMessageBox.Show("Please enter the Tax Acquired Receivable Account in Cash Receipts.", MsgBoxStyle.Exclamation, "Missing Account");
					}
					if (Strings.Trim(strCRTALAccountNumber) == "")
					{
						frmWait.InstancePtr.Unload();
						FCMessageBox.Show("Please enter the Tax Acquired Lien Receivable Account in Cash Receipts.", MsgBoxStyle.Exclamation, "Missing Account");
					}
					if (Conversion.Val(Strings.Mid(strCRTALAccountNumber, FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 1, 2)) + 4), FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))))) != Conversion.Val(strTALAccountNumber))
					{
						frmWait.InstancePtr.Unload();
						FCMessageBox.Show("Your Tax Acquired Lien Receivable Account in Cash Receipts and Budgetary do not match.", MsgBoxStyle.Exclamation, "Missing Account");
					}
				}
				if (lngJournalNumber == 0)
				{
					// Add Journal
					// ***********************************************************************************************************
					strTableName = "";
					// get journal number
					if (modBudgetaryAccounting.LockJournal() == false)
					{
						// add entries to incomplete journals table
						strTableName = "Temp";
					}
					else
					{
						Master.OpenRecordset(("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC"), "TWBD0000.vb1");
						if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
						{
							Master.MoveLast();
							Master.MoveFirst();
							// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
							lngJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
						}
						else
						{
							lngJournal = 1;
						}
						Master.AddNew();
						Master.Set_Fields("JournalNumber", lngJournal);
						Master.Set_Fields("Status", "E");
						Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
						Master.Set_Fields("StatusChangeDate", DateTime.Today);
						Master.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " Lien");
						Master.Set_Fields("Type", "GJ");
						Master.Set_Fields("Period", DateTime.Today.Month);
						Master.Update();
						Master.Reset();
						modBudgetaryAccounting.UnlockJournal();
					}
					lngJournalNumber = lngJournal;
				}
				else
				{
					// add it to an already created journal
					lngJournal = lngJournalNumber;
				}
				// Add Journal Entries
				// ************************************************************************************************************
				rsEntries.OpenRecordset("SELECT * FROM " + strTableName + "JournalEntries WHERE ID = 0", "TWBD0000.vb1");
				// Master.OpenRecordset "SELECT * FROM UtilityBilling"
				// Add Entries
				if (dblTotalPrin != 0)
				{
					rsEntries.AddNew();
					rsEntries.Set_Fields("Type", "G");
					rsEntries.Set_Fields("JournalEntriesDate", DateTime.Today);
					rsEntries.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " Lien");
					rsEntries.Set_Fields("JournalNumber", lngJournal);
					rsEntries.Set_Fields("Account", strCRAccountNumber);
					rsEntries.Set_Fields("Amount", dblTotalPrin * -1);
					rsEntries.Set_Fields("WarrantNumber", 0);
					rsEntries.Set_Fields("Period", DateTime.Today.Month);
					rsEntries.Set_Fields("RCB", "R");
					rsEntries.Set_Fields("Status", "E");
					rsEntries.Update();
					rsEntries.AddNew();
					rsEntries.Set_Fields("Type", "G");
					rsEntries.Set_Fields("JournalEntriesDate", DateTime.Today);
					rsEntries.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " Lien");
					rsEntries.Set_Fields("JournalNumber", lngJournal);
					rsEntries.Set_Fields("Account", strCRLAccountNumber);
					rsEntries.Set_Fields("Amount", dblTotalPrin);
					rsEntries.Set_Fields("WarrantNumber", 0);
					rsEntries.Set_Fields("Period", DateTime.Today.Month);
					rsEntries.Set_Fields("RCB", "R");
					rsEntries.Set_Fields("Status", "E");
					rsEntries.Update();
				}
				if (dblTATotalPrin != 0)
				{
					rsEntries.AddNew();
					rsEntries.Set_Fields("Type", "G");
					rsEntries.Set_Fields("JournalEntriesDate", DateTime.Today);
					rsEntries.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " Lien");
					rsEntries.Set_Fields("JournalNumber", lngJournal);
					rsEntries.Set_Fields("Account", strCRTAAccountNumber);
					rsEntries.Set_Fields("Amount", dblTATotalPrin * -1);
					rsEntries.Set_Fields("WarrantNumber", 0);
					rsEntries.Set_Fields("Period", DateTime.Today.Month);
					rsEntries.Set_Fields("RCB", "R");
					rsEntries.Set_Fields("Status", "E");
					rsEntries.Update();
					rsEntries.AddNew();
					rsEntries.Set_Fields("Type", "G");
					rsEntries.Set_Fields("JournalEntriesDate", DateTime.Today);
					rsEntries.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " Lien");
					rsEntries.Set_Fields("JournalNumber", lngJournal);
					rsEntries.Set_Fields("Account", strCRTALAccountNumber);
					rsEntries.Set_Fields("Amount", dblTATotalPrin);
					rsEntries.Set_Fields("WarrantNumber", 0);
					rsEntries.Set_Fields("Period", DateTime.Today.Month);
					rsEntries.Set_Fields("RCB", "R");
					rsEntries.Set_Fields("Status", "E");
					rsEntries.Update();
				}
				frmWait.InstancePtr.Unload();
				// kk01282015 trocls-51  Allow Automatic posting of journals
				if (lngJournal == 0)
				{
					FCMessageBox.Show("Unable to complete the journal.  You must go into the Budgetary System > Data Entry > Build Incomplete Journals to finish creating the journal.", MsgBoxStyle.Exclamation, "Unable to Save");
				}
				else
				{
					if (modSecurityValidation.ValidPermissions(null, CLSECURITYAUTOPOSTJOURNAL, false) && modBudgetaryAccounting.AutoPostAllowed(modBudgetaryAccounting.AutoPostType.aptCLLiens))
					{
						if (FCMessageBox.Show("The entries have been saved into journal " + Strings.Format(lngJournal, "0000") + ".  Would you like to post the journal?", MsgBoxStyle.Question | MsgBoxStyle.YesNo, "Post Entries") == DialogResult.Yes)
						{
							tJournalInfo = new clsPostingJournalInfo();
							tJournalInfo.JournalNumber = lngJournal;
							tJournalInfo.JournalType = "GJ";
							tJournalInfo.Period = FCConvert.ToString(DateTime.Today.Month);
							tJournalInfo.CheckDate = "";
							tPostInfo = new clsBudgetaryPosting();
							tPostInfo.AddJournal(tJournalInfo);
							tPostInfo.AllowPreview = true;
							tPostInfo.PageBreakBetweenJournalsOnReport = true;
							tPostInfo.WaitForReportToEnd = true;
							tPostInfo.PostJournals();
							tJournalInfo = null;
							tPostInfo = null;
						}
					}
					else
					{
						FCMessageBox.Show("The Budgetary journal entries were created in journal #" + FCConvert.ToString(lngJournal) + ".", MsgBoxStyle.Exclamation, "Journal Created");
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Creating Journal Entries");
			}
		}
		// Public Sub CreateTransferTaxToLienBDEntry(dblTotalPrin As Double, lngYear As Long)
		// On Error GoTo ERROR_HANDLER
		// this routine will create an entry in the BD database that will move monies from
		// the taxes receivable account to the lien receivable account
		// Dim rsTax                   As New clsDRWrapper
		//
		// rsTax.OpenRecordset "SELECT * FROM TaxCommitment WHERE Key = 0", strBDDatabase
		// rsTax.AddNew
		// If dblTotalPrin >= 0 Then
		// rsTax.Get_Fields("TaxType") = "L"
		// Else
		// rsTax.Get_Fields("TaxType") = "M"
		// End If
		// rsTax.Get_Fields("RE") = dblTotalPrin
		// rsTax.Get_Fields("Year") = lngYear
		// rsTax.Get_Fields("ActivityDate") = Date
		// rsTax.Update
		//
		// Unload frmWait
		// MsgBox "An automatic entry has been created in TRIO Budgetary.  To create the appropriate journals go to the Data Entry menu and select Create Automatic Journals.", MsgBoxStyle.Exclamation, "Journal Entries"
		//
		// Exit Sub
		// ERROR_HANDLER:
		// Unload frmWait
		// MsgBox "Error #" & Err.Number & " - " & Err.Description & ".", MsgBoxStyle.Critical
		// End Sub
		//private static void SetPLIPaidField()
		//{
		//	try
		//	{
		//		// On Error GoTo ERROR_HANDLER
		//		// this routine will search through each lien record and find records that have
		//		// paid more interest than their charged interest field, then it will take the
		//		// excess paid interest out of the InterestPaid field and put it in the PLIPaid fiel
		//		// in order to track PLI better from the LienRec itself
		//		clsDRWrapper rsLN = new clsDRWrapper();
		//		double dblPLIPaid = 0;
		//		double dblIntPaid = 0;
		//		double dblIntCharged = 0;
		//		rsLN.OpenRecordset("SELECT * FROM LienRec", modExtraModules.strCLDatabase);
		//		// WHERE InterestPaid > (InterestCharged * -1)", strCLDatabase
		//		while (!rsLN.EndOfFile())
		//		{
		//			App.DoEvents();
		//			// If rsLN.Get_Fields("LienRecordNumber") = 659 Then Stop
		//			dblIntCharged = FCConvert.ToDouble(rsLN.Get_Fields_Decimal("InterestCharged") * -1);
		//			dblIntPaid = Conversion.Val(rsLN.Get_Fields_Decimal("InterestPaid"));
		//			if (dblIntPaid > dblIntCharged)
		//			{
		//				dblPLIPaid = (dblIntPaid - dblIntCharged);
		//				dblIntPaid -= (dblIntPaid - dblIntCharged);
		//				rsLN.Edit();
		//				rsLN.Set_Fields("InterestPaid", dblIntPaid);
		//				rsLN.Set_Fields("PLIPaid", dblPLIPaid);
		//				rsLN.Update();
		//			}
		//			else
		//			{
		//				rsLN.Edit();
		//				rsLN.Set_Fields("PLIPaid", 0);
		//				rsLN.Update();
		//			}
		//			rsLN.MoveNext();
		//		}
		//		return;
		//	}
		//	catch (Exception ex)
		//	{
		//		// ERROR_HANDLER:
		//		frmWait.InstancePtr.Unload();
		//		FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Setting PLI Paid");
		//	}
		//}

		//private static void SetPLIPaidFieldAndPayments()
		//{
		//	try
		//	{
		//		// On Error GoTo ERROR_HANDLER
		//		// this routine will search through each lien record and find records that have
		//		// paid more interest than their charged interest field, then it will take the
		//		// excess paid interest out of the InterestPaid field and put it in the PLIPaid fiel
		//		// in order to track PLI better from the LienRec itself
		//		clsDRWrapper rsLN = new clsDRWrapper();
		//		clsDRWrapper rsPay = new clsDRWrapper();
		//		double dblPLIPaid = 0;
		//		double dblIntPaid = 0;
		//		double dblIntCharged = 0;
		//		double dblTotPaymentInt = 0;
		//		rsLN.OpenRecordset("SELECT * FROM LienRec", modExtraModules.strCLDatabase);
		//		// WHERE InterestPaid > (InterestCharged * -1)", strCLDatabase
		//		while (!rsLN.EndOfFile())
		//		{
		//			App.DoEvents();
		//			dblIntCharged = FCConvert.ToDouble(rsLN.Get_Fields_Decimal("InterestCharged") * -1);
		//			// TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
		//			dblIntPaid = Conversion.Val(rsLN.Get_Fields_Decimal("InterestPaid")) + Conversion.Val(rsLN.Get_Fields("PLIPaid"));
		//			// If rsLN.Get_Fields("LienRecordNumber") = 48 Then Stop
		//			if (dblIntPaid > dblIntCharged)
		//			{
		//				dblPLIPaid = (dblIntPaid - dblIntCharged);
		//				dblIntPaid -= (dblIntPaid - dblIntCharged);
		//				if (dblPLIPaid >= 0)
		//				{
		//					rsLN.Edit();
		//					rsLN.Set_Fields("InterestPaid", dblIntPaid);
		//					rsLN.Set_Fields("PLIPaid", dblPLIPaid);
		//					rsLN.Update();
		//					// set the payment amounts
		//					rsPay.OpenRecordset("SELECT * FROM PaymentRec WHERE Code <> 'I' AND BillKey = " + FCConvert.ToString(rsLN.Get_Fields_Int32("LienRecordNumber")) + " AND BillCode = 'L'", modExtraModules.strCLDatabase);
		//					while (!rsPay.EndOfFile())
		//					{
		//						dblTotPaymentInt = Conversion.Val(rsPay.Get_Fields_Decimal("PreLienInterest")) + Conversion.Val(rsPay.Get_Fields_Decimal("CurrentInterest"));
		//						if (dblTotPaymentInt != 0)
		//						{
		//							rsPay.Edit();
		//							if (dblPLIPaid > 0)
		//							{
		//								if (dblPLIPaid > dblTotPaymentInt)
		//								{
		//									rsPay.Set_Fields("PreLienInterest", dblTotPaymentInt);
		//									rsPay.Set_Fields("CurrentInterest", 0);
		//									dblPLIPaid -= dblTotPaymentInt;
		//								}
		//								else
		//								{
		//									rsPay.Set_Fields("PreLienInterest", dblPLIPaid);
		//									rsPay.Set_Fields("CurrentInterest", dblTotPaymentInt - dblPLIPaid);
		//									dblPLIPaid = 0;
		//									dblIntPaid -= (dblTotPaymentInt - dblPLIPaid);
		//								}
		//							}
		//							else
		//							{
		//								rsPay.Set_Fields("PreLienInterest", 0);
		//								rsPay.Set_Fields("CurrentInterest", dblTotPaymentInt);
		//							}
		//							rsPay.Update();
		//						}
		//						rsPay.MoveNext();
		//					}
		//				}
		//			}
		//			else
		//			{
		//				rsLN.Edit();
		//				rsLN.Set_Fields("PLIPaid", 0);
		//				rsLN.Update();
		//				// set the payment amounts
		//				rsPay.OpenRecordset("SELECT * FROM PaymentRec WHERE Code <> 'I' AND BillKey = " + FCConvert.ToString(rsLN.Get_Fields_Int32("LienRecordNumber")) + " AND BillCode = 'L'", modExtraModules.strCLDatabase);
		//				while (!rsPay.EndOfFile())
		//				{
		//					rsPay.Edit();
		//					if (((rsPay.Get_Fields_Decimal("PreLienInterest"))) != 0 || ((rsPay.Get_Fields_Decimal("CurrentInterest"))) != 0)
		//					{
		//						dblTotPaymentInt = Conversion.Val(rsPay.Get_Fields_Decimal("PreLienInterest")) + Conversion.Val(rsPay.Get_Fields_Decimal("CurrentInterest"));
		//						rsPay.Set_Fields("PreLienInterest", 0);
		//						rsPay.Set_Fields("CurrentInterest", dblTotPaymentInt);
		//						rsPay.Update();
		//					}
		//					rsPay.MoveNext();
		//				}
		//			}
		//			rsLN.MoveNext();
		//		}
		//		return;
		//	}
		//	catch (Exception ex)
		//	{
		//		// ERROR_HANDLER:
		//		frmWait.InstancePtr.Unload();
		//		FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Setting PLI Payment Amounts");
		//	}
		//}

		private static void ForceDecimalPlaces()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsTemp = new clsDRWrapper();
				rsTemp.OpenRecordset("SELECT * FROM BillingMaster WHERE TaxDue1 < 2100000 AND ((TaxDue1 * 1000) % 10) <> 0", modExtraModules.strCLDatabase);
				while (!rsTemp.EndOfFile())
				{
					App.DoEvents();
					rsTemp.Edit();
					rsTemp.Set_Fields("TaxDue1", modGlobal.Round(Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue1")), 2));
					rsTemp.Update();
					rsTemp.MoveNext();
				}
				// Dim rs As DAO.Recordset
				// Dim tdf As DAO.TableDef
				// Dim ff As DAO.Field
				// Dim pp As DAO.Property
				// Dim db As DAO.Database
				// 
				// Set db = OpenDatabase("TWCL0000.VB1", False, False, ";PWD=" & DATABASEPASSWORD)
				// Set rs = db.OpenRecordset("SELECT * FROM BillingMaster")
				// 
				// Set tdf = db.TableDefs("BillingMaster")
				// Set ff = tdf.CreateField("MyField", dbLong)
				// tdf.Get_Fields.Append ff
				// 
				// For Each ff In rs.Get_Fields
				// If ff.Name = "TaxDue1" Then Exit For
				// Debug.Print ff.Name & " - " & ff.Type
				// Select Case ff.Type
				// Case 5
				// ff.Properties("DecimalPlaces") = 2
				// End Select
				// 
				// Next
				// 
				// Set pp = ff.CreateProperty("DecimalPlaces", dbCurrency, "2")
				// ff.Properties.Append pp
				// ff.Properties.Refresh
				// tdf.Get_Fields.Refresh
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Forcing double Places");
			}
		}

		//private static void AdjustIntPaidDate()
		//{
		//	try
		//	{
		//		// On Error GoTo ERROR_HANDLER
		//		clsDRWrapper rsAdj = new clsDRWrapper();
		//		clsDRWrapper rsP = new clsDRWrapper();
		//		if (Strings.Trim(Statics.gstrLastYearBilled) == "")
		//		{
		//			rsAdj.OpenRecordset("SELECT BillingYear FROM BillingMaster WHERE TaxDue1 > 0 ORDER BY BillingYear desc");
		//			if (!rsAdj.EndOfFile())
		//			{
		//				Statics.gstrLastYearBilled = FCConvert.ToString(rsAdj.Get_Fields_Int32("BillingYear"));
		//			}
		//			else
		//			{
		//				return;
		//			}
		//		}
		//		rsAdj.OpenRecordset("SELECT * FROM BillingMaster INNER JOIN RateRec ON BillingMaster.RateKey = RateRec.RateKey WHERE BillingYear / 10 = " + Strings.Left(Statics.gstrLastYearBilled, 4) + " AND LienRecordNumber = 0 AND InterestAppliedThroughDate = InterestStartDate1", modExtraModules.strCLDatabase);
		//		frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Adjusting Int Paid Date");
		//		// , True, rsAdj.RecordCount, True
		//		while (!rsAdj.EndOfFile())
		//		{
		//			App.DoEvents();
		//			// check to make sure that this bill does not have any payments
		//			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
		//			rsP.OpenRecordset("SELECT ID FROM PaymentRec WHERE Account = " + FCConvert.ToString(rsAdj.Get_Fields("Account")) + " AND BillKey = " + FCConvert.ToString(rsAdj.Get_Fields_Int32("BillKey")) + " AND BillCode = '" + Strings.Left(FCConvert.ToString(rsAdj.Get_Fields_String("BillingType")), 1) + "' AND EffectiveInterestDate = '" + FCConvert.ToString(rsAdj.Get_Fields_DateTime("InterestStartDate1")) + "'", modExtraModules.strCLDatabase);
		//			if (rsP.EndOfFile())
		//			{
		//				// if no payments then change the date
		//				rsAdj.Edit();
		//				rsAdj.Set_Fields("InterestAppliedThroughDate", rsAdj.Get_Fields_DateTime("DueDate1"));
		//				rsAdj.Update();
		//			}
		//			// frmWait.IncrementProgress
		//			rsAdj.MoveNext();
		//		}
		//		frmWait.InstancePtr.Unload();
		//		return;
		//	}
		//	catch (Exception ex)
		//	{
		//		// ERROR_HANDLER:
		//		frmWait.InstancePtr.Unload();
		//		FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Adjusting Interest Paid Through Date");
		//	}
		//}

		//public static void AddExtraLienCost()
		//{
		//	// This routine will check to see if the extra lien fees will need to be added to outstanding lien accounts
		//	try
		//	{
		//		// On Error GoTo ERROR_HANDLER
		//		clsDRWrapper rsLN = new clsDRWrapper();
		//		clsDRWrapper rsPayment = new clsDRWrapper();
		//		int lngCount = 0;
		//		int lngAdjusted = 0;
		//		int[] lngLRN = null;
		//		DateTime dtDate;
		//		double dblXtraInt = 0;
		//		int lngExcludeYear = 0;
		//		dtDate = DateAndTime.DateValue("09/17/2005 12:01:00 AM");
		//		modGlobalFunctions.AddCYAEntry_8("CL", "Add new lien fees attempt.");
		//		if (DateAndTime.DateDiff("D", dtDate, DateTime.Now) >= 0)
		//		{
		//			// if the date/time is after midnight on the 17th then this will run
		//			// check to see if this code has run before
		//			if (!rsLN.DoesFieldExist("AddLienCharge", "Collections"))
		//			{
		//				lngExcludeYear = FindExcludeYear();
		//				modGlobalFunctions.AddCYAEntry_8("CL", "Adding new lien fees.");
		//				if (lngExcludeYear > 0)
		//				{
		//					rsLN.OpenRecordset("SELECT * FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.LienRecordNumber WHERE BillingYear / 10 <> " + FCConvert.ToString(lngExcludeYear));
		//				}
		//				else
		//				{
		//					rsLN.OpenRecordset("SELECT * FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.LienRecordNumber");
		//				}
		//				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Adding New Lien Charges");
		//				// , True, rsLN.RecordCount, True
		//				while (!rsLN.EndOfFile())
		//				{
		//					// are there any liens?
		//					App.DoEvents();
		//					// frmWait.IncrementProgress
		//					if (modCLCalculations.CalculateAccountCLLien(rsLN, DateTime.Today, ref dblXtraInt, true) != 0)
		//					{
		//						lngCount += 1;
		//						rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(rsLN.Get_Fields_Int32("LienRecordNumber")) + " AND BillCode = 'L' AND LienCost = -5 AND Reference = 'STATE LC'", modExtraModules.strCLDatabase);
		//						if (rsPayment.EndOfFile())
		//						{
		//							lngAdjusted += 1;
		//							Array.Resize(ref lngLRN, lngAdjusted + 1);
		//							lngLRN[lngAdjusted] = FCConvert.ToInt32(rsLN.Get_Fields_Int32("LienRecordNumber"));
		//							rsPayment.AddNew();
		//							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
		//							rsPayment.Set_Fields("Account", rsLN.Get_Fields("Account"));
		//							rsPayment.Set_Fields("Year", rsLN.Get_Fields_Int32("BillingYear"));
		//							rsPayment.Set_Fields("BillKey", rsLN.Get_Fields_Int32("LienRecordNumber"));
		//							rsPayment.Set_Fields("ActualSystemDate", DateAndTime.DateValue("09/17/2005"));
		//							rsPayment.Set_Fields("EffectiveInterestDate", DateAndTime.DateValue("09/17/2005"));
		//							rsPayment.Set_Fields("RecordedTransactionDate", DateAndTime.DateValue("09/17/2005"));
		//							rsPayment.Set_Fields("Reference", "STATE LC");
		//							rsPayment.Set_Fields("Period", "A");
		//							rsPayment.Set_Fields("Code", "L");
		//							rsPayment.Set_Fields("LienCost", -5);
		//							rsPayment.Set_Fields("CashDrawer", "Y");
		//							rsPayment.Set_Fields("GeneralLedger", "Y");
		//							rsPayment.Set_Fields("BillCode", "L");
		//							rsPayment.Set_Fields("DailyCloseOut", 1);
		//							rsPayment.Update();
		//							rsLN.Edit();
		//							rsLN.Set_Fields("CostsPaid", FCConvert.ToInt16(rsLN.Get_Fields_Decimal("CostsPaid")) - 5);
		//							rsLN.Update();
		//						}
		//						else
		//						{
		//							// This lien has already been affected
		//						}
		//					}
		//					rsLN.MoveNext();
		//				}
		//				frmWait.InstancePtr.Unload();
		//				if (lngAdjusted > 0)
		//				{
		//					// show the report with all the liens that were affected
		//					rptAddExtraLienFees.InstancePtr.Init(lngLRN);
		//				}
		//				// add the field so I know not to run this code again
		//				// rsLN.UpdateDatabaseTable "Collections", strCLDatabase
		//				// If Not rsLN.AddTableField("AddLienCharge", dbBoolean) Then
		//				// 
		//				// End If
		//				// rsLN.UpdateTableCreation
		//				FCMessageBox.Show("New Lien charges have been added due to an increase in the fees for recording legal documents." + "\r\n" + FCConvert.ToString(lngAdjusted) + " Liens were adjusted.", MsgBoxStyle.Exclamation, "New Fees Added");
		//			}
		//			else
		//			{
		//				FCMessageBox.Show("New Lien charges have not been added, These fees have already been added to your system.", MsgBoxStyle.Exclamation, "New Fees Not Added");
		//			}
		//		}
		//		return;
		//	}
		//	catch (Exception ex)
		//	{
		//		// ERROR_HANDLER:
		//		frmWait.InstancePtr.Unload();
		//		FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Adding Extra Lien Costs");
		//	}
		//}

		public static short ValidateTellerID(string strID, short intSecLvl = 0, string strPass = "")
		{
			short ValidateTellerID = 0;
			// this is a dummy function that will let all tellers go through
			ValidateTellerID = 0;
			return ValidateTellerID;
		}

		//private static int FindExcludeYear()
		//{
		//	int FindExcludeYear = 0;
		//	try
		//	{
		//		// On Error GoTo ERROR_HANDLER
		//		// VBto upgrade warning: lngTemp As int	OnWrite(string)
		//		int lngTemp = 0;
		//		frmWait.InstancePtr.Unload();
		//		TryAgain:
		//		;
		//		DialogResult messageBoxResult = FCMessageBox.Show("Have you manually applied the new registry of deeds fee to your current year liens?", MsgBoxStyle.Exclamation | MsgBoxStyle.YesNo, "Adding Lien Fee");
		//		if (messageBoxResult == DialogResult.Yes)
		//		{
		//			// find out what year
		//			// lngtemp = InputBox("Please enter the year of the liens that have had the new lien fee applied.", "Year To Exclude From Update")
		//			lngTemp = FCConvert.ToInt32(Interaction.InputBox("What year were these fees applied to?", "Year To Exclude From Update"));
		//			if (lngTemp > 2000 && lngTemp < 2006)
		//			{
		//				// return the lear for the SQL statement
		//				FindExcludeYear = lngTemp;
		//				modGlobalFunctions.AddCYAEntry_26("CL", "Adding New Lien Fees", "User did apply new fees for the year " + FCConvert.ToString(lngTemp) + " .");
		//			}
		//			else
		//			{
		//				FCMessageBox.Show("This year is not your most current liened year.", MsgBoxStyle.Exclamation, "Try again.");
		//				goto TryAgain;
		//			}
		//		}
		//		else if (messageBoxResult == DialogResult.No)
		//		{
		//			// let this rip
		//			modGlobalFunctions.AddCYAEntry_26("CL", "Adding New Lien Fees", "User did not apply new fees yet.");
		//			FindExcludeYear = 0;
		//		}
		//		return FindExcludeYear;
		//	}
		//	catch (Exception ex)
		//	{
		//		// ERROR_HANDLER:
		//		FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Adding Extra Lien Costs");
		//	}
		//	return FindExcludeYear;
		//}

		//public static void GeneralFixRoutine()
		//{
		//	clsDRWrapper rsLien = new clsDRWrapper();
		//	clsDRWrapper rsPayment = new clsDRWrapper();
		//	decimal curPreLienIntTotal;
		//	// VBto upgrade warning: curCurrentIntTotal As double	OnWriteFCConvert.ToInt16(
		//	decimal curCurrentIntTotal;
		//	decimal curSCurrentIntTotal;
		//	// VBto upgrade warning: curPreLienIntRemaining As double	OnWrite(double, short)
		//	decimal curPreLienIntRemaining;
		//	decimal curCurrentIntRemaining;
		//	decimal curSCurrentIntRemaining;
		//	// VBto upgrade warning: curPreLienIntPaidPaymentTotal As double	OnWrite(short, double)
		//	decimal curPreLienIntPaidPaymentTotal;
		//	// VBto upgrade warning: curCurrentIntPaidPaymentTotal As double	OnWrite(short, double)
		//	decimal curCurrentIntPaidPaymentTotal;
		//	decimal curSCurrentIntPaidPaymentTotal;
		//	decimal curPreLienIntPaidLienTotal;
		//	decimal curCurrentIntPaidLienTotal;
		//	decimal curSCurrentIntPaidLienTotal;
		//	// VBto upgrade warning: curTotalIntPayment As double	OnWrite(double, double)
		//	decimal curTotalIntPayment;
		//	// VBto upgrade warning: curCorrectPreLienIntPaidTotal As double	OnWrite(short, double)
		//	decimal curCorrectPreLienIntPaidTotal;
		//	// VBto upgrade warning: curCorrectCurrentIntPaidTotal As double	OnWrite(short, double)
		//	decimal curCorrectCurrentIntPaidTotal;
		//	decimal curSCorrectCurrentIntPaidTotal;
		//	clsDRWrapper rsAccountInfo = new clsDRWrapper();
		//	decimal curSCurrentIntPaymentTotal;
		//	// VBto upgrade warning: curCurrentIntPaymentTotal As double	OnWrite(short, double)
		//	decimal curCurrentIntPaymentTotal;
		//	clsDRWrapper rsBillYear = new clsDRWrapper();
		//	clsDRWrapper rsCheck = new clsDRWrapper();
		//	string strAccount = "";
		//	rsCheck.OpenRecordset("SELECT * FROM Collections", "TWCL0000.vb1");
		//	if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
		//	{
		//		// do nothing
		//	}
		//	else
		//	{
		//		return;
		//	}
		//	if (rsCheck.Get_Fields_Boolean("RanGeneralFix") != true)
		//	{
		//		FCFileSystem.FileOpen(1, "CLLienFix.txt", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
		//		FCFileSystem.WriteLine(1, "Fix perfomed on " + FCConvert.ToString(DateTime.Now));
		//		frmWait.InstancePtr.lblMessage.Text = "Checking Liens" + "\r\n" + "Please Wait...";
		//		frmWait.InstancePtr.prgProgress.Value = 0;
  //              frmWait.InstancePtr.Refresh();
  //              // frmWait.Refresh
  //              rsLien.OpenRecordset("SELECT * FROM LienRec", "TWCL0000.vb1");
		//		if (rsLien.EndOfFile() != true && rsLien.BeginningOfFile() != true)
		//		{
		//			// frmWait.prgProgress.Max = rsLien.RecordCount
		//			do
		//			{
		//				App.DoEvents();
		//				// TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
		//				curPreLienIntTotal = FCConvert.ToDecimal(rsLien.Get_Fields("Interest"));
		//				curCurrentIntTotal = FCConvert.ToDecimal(rsLien.Get_Fields_Decimal("InterestCharged")) * -1;
		//				// TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
		//				curPreLienIntPaidLienTotal = FCConvert.ToDecimal(rsLien.Get_Fields("PLIPaid"));
		//				curCurrentIntPaidLienTotal = FCConvert.ToDecimal(rsLien.Get_Fields_Decimal("InterestPaid"));
		//				curPreLienIntPaidPaymentTotal = 0;
		//				curCurrentIntPaidPaymentTotal = 0;
		//				curCurrentIntPaymentTotal = 0;
		//				rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(rsLien.Get_Fields_Int32("LienRecordNumber")) + " AND BillCode = 'L' ORDER BY ActualSystemDate", "TWCL0000.vb1");
		//				if (rsPayment.EndOfFile() != true && rsPayment.BeginningOfFile() != true)
		//				{
		//					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
		//					strAccount = FCConvert.ToString(rsPayment.Get_Fields("Account"));
		//					rsBillYear.OpenRecordset("SELECT * FROM BillingMaster WHERE LienRecordNumber = " + FCConvert.ToString(rsLien.Get_Fields_Int32("LienRecordNumber")), "TWCL0000.vb1");
		//					if (rsBillYear.EndOfFile() != true && rsBillYear.BeginningOfFile() != true)
		//					{
		//						// do nothing
		//					}
		//					else
		//					{
		//						FCFileSystem.WriteLine(1, "No bills tied to lien key " + FCConvert.ToString(rsLien.Get_Fields_Int32("LienRecordNumber")));
		//						goto CheckNextLien;
		//					}
		//					do
		//					{
		//						App.DoEvents();
		//						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
		//						if (FCConvert.ToString(rsPayment.Get_Fields("Code")) != "I")
		//						{
		//							curPreLienIntPaidPaymentTotal += FCConvert.ToDecimal(rsPayment.Get_Fields_Decimal("PreLienInterest"));
		//							curCurrentIntPaidPaymentTotal += FCConvert.ToDecimal(rsPayment.Get_Fields_Decimal("CurrentInterest"));
		//						}
		//						else
		//						{
		//							curCurrentIntPaymentTotal += FCConvert.ToDecimal(rsPayment.Get_Fields_Decimal("CurrentInterest"));
		//						}
		//						rsPayment.MoveNext();
		//					}
		//					while (rsPayment.EndOfFile() != true);
		//					if (curPreLienIntTotal < curPreLienIntPaidLienTotal || curCurrentIntTotal < curCurrentIntPaidLienTotal || curPreLienIntPaidLienTotal != curPreLienIntPaidPaymentTotal || curCurrentIntPaidLienTotal != curCurrentIntPaidPaymentTotal)
		//					{
		//						curPreLienIntRemaining = curPreLienIntTotal;
		//						curCurrentIntRemaining = curCurrentIntTotal;
		//						curCorrectPreLienIntPaidTotal = 0;
		//						curCorrectCurrentIntPaidTotal = 0;
		//						rsPayment.MoveFirst();
		//						do
		//						{
		//							App.DoEvents();
		//							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
		//							if (FCConvert.ToString(rsPayment.Get_Fields("Code")) != "I")
		//							{
		//								curTotalIntPayment = FCConvert.ToDecimal(Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")) + Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")));
		//								if (curPreLienIntRemaining >= 0)
		//								{
		//									if (curTotalIntPayment > 0)
		//									{
		//										if (curTotalIntPayment < curPreLienIntRemaining)
		//										{
		//											rsPayment.Edit();
		//											if (Conversion.Val(rsPayment.Get_Fields("PreLienInterest")) != FCConvert.ToDouble(curTotalIntPayment) || ((rsPayment.Get_Fields_Decimal("CurrentInterest"))) != 0)
		//											{
		//												FCFileSystem.WriteLine(1, "Adjusted Payment for Account: " + strAccount + "   Year: " + FCConvert.ToString(rsBillYear.Get_Fields_Int32("BillingYear")) + "   Old PLI Paid: " + FCConvert.ToString(rsPayment.Get_Fields_Decimal("PreLienInterest")) + "   New PLI Paid: " + FCConvert.ToString(curTotalIntPayment) + "   Old Cur Int Paid: " + FCConvert.ToString(rsPayment.Get_Fields_Decimal("CurrentInterest")) + "   New Cur Int Paid: 0.00");
		//											}
		//											rsPayment.Set_Fields("PreLienInterest", curTotalIntPayment);
		//											rsPayment.Set_Fields("CurrentInterest", 0);
		//											rsPayment.Update();
		//											curPreLienIntRemaining -= curTotalIntPayment;
		//											curCorrectPreLienIntPaidTotal += curTotalIntPayment;
		//										}
		//										else
		//										{
		//											if (curPreLienIntRemaining > 0)
		//											{
		//												rsPayment.Edit();
		//												if (FCConvert.ToDecimal(Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest"))) != curPreLienIntRemaining || FCConvert.ToDecimal(Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest"))) != (curTotalIntPayment - curPreLienIntRemaining))
		//												{
		//													FCFileSystem.WriteLine(1, "Adjusted Payment for Account: " + strAccount + "   Year: " + FCConvert.ToString(rsBillYear.Get_Fields_Int32("BillingYear")) + "   Old PLI Paid: " + FCConvert.ToString(rsPayment.Get_Fields_Decimal("PreLienInterest")) + "   New PLI Paid: " + FCConvert.ToString(curPreLienIntRemaining) + "   Old Cur Int Paid: " + FCConvert.ToString(rsPayment.Get_Fields_Decimal("CurrentInterest")) + "   New Cur Int Paid: " + FCConvert.ToString(curTotalIntPayment - curPreLienIntRemaining));
		//												}
		//												rsPayment.Set_Fields("PreLienInterest", curPreLienIntRemaining);
		//												curTotalIntPayment -= curPreLienIntRemaining;
		//												curCorrectPreLienIntPaidTotal += curPreLienIntRemaining;
		//												curPreLienIntRemaining = 0;
		//												rsPayment.Set_Fields("CurrentInterest", curTotalIntPayment);
		//												rsPayment.Update();
		//												curCurrentIntRemaining -= curTotalIntPayment;
		//												curCorrectCurrentIntPaidTotal += curTotalIntPayment;
		//											}
		//											else
		//											{
		//												rsPayment.Edit();
		//												if (((rsPayment.Get_Fields_Decimal("PreLienInterest"))) != 0 || Conversion.Val(rsPayment.Get_Fields("CurrentInterest")) != FCConvert.ToDouble(curTotalIntPayment))
		//												{
		//													FCFileSystem.WriteLine(1, "Adjusted Payment for Account: " + strAccount + "   Year: " + FCConvert.ToString(rsBillYear.Get_Fields_Int32("BillingYear")) + "   Old PLI Paid: " + FCConvert.ToString(rsPayment.Get_Fields_Decimal("PreLienInterest")) + "   New PLI Paid: " + "0.00" + "   Old Cur Int Paid: " + FCConvert.ToString(rsPayment.Get_Fields_Decimal("CurrentInterest")) + "   New Cur Int Paid: " + FCConvert.ToString(curTotalIntPayment));
		//												}
		//												rsPayment.Set_Fields("PreLienInterest", 0);
		//												rsPayment.Set_Fields("CurrentInterest", curTotalIntPayment);
		//												rsPayment.Update();
		//												curCurrentIntRemaining -= curTotalIntPayment;
		//												curCorrectCurrentIntPaidTotal += curTotalIntPayment;
		//											}
		//										}
		//									}
		//									else if (curTotalIntPayment == 0)
		//									{
		//										if (((rsPayment.Get_Fields_Decimal("PreLienInterest"))) != 0 && ((rsPayment.Get_Fields_Decimal("CurrentInterest"))) != 0)
		//										{
		//											rsPayment.Edit();
		//											FCFileSystem.WriteLine(1, "Adjusted Payment for Account: " + strAccount + "   Year: " + FCConvert.ToString(rsBillYear.Get_Fields_Int32("BillingYear")) + "   Old PLI Paid: " + FCConvert.ToString(rsPayment.Get_Fields_Decimal("PreLienInterest")) + "   New PLI Paid: " + "0.00" + "   Old Cur Int Paid: " + FCConvert.ToString(rsPayment.Get_Fields_Decimal("CurrentInterest")) + "   New Cur Int Paid: " + "0.00");
		//											rsPayment.Set_Fields("PreLienInterest", 0);
		//											rsPayment.Set_Fields("CurrentInterest", 0);
		//											rsPayment.Update();
		//										}
		//									}
		//									else
		//									{
		//										rsPayment.Edit();
		//										if (((rsPayment.Get_Fields_Decimal("PreLienInterest"))) != 0 || Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")) != FCConvert.ToDouble(curTotalIntPayment))
		//										{
		//											FCFileSystem.WriteLine(1, "Adjusted Payment for Account: " + strAccount + "   Year: " + FCConvert.ToString(rsBillYear.Get_Fields_Int32("BillingYear")) + "   Old PLI Paid: " + FCConvert.ToString(rsPayment.Get_Fields_Decimal("PreLienInterest")) + "   New PLI Paid: " + "0.00" + "   Old Cur Int Paid: " + FCConvert.ToString(rsPayment.Get_Fields_Decimal("CurrentInterest")) + "   New Cur Int Paid: " + FCConvert.ToString(curTotalIntPayment));
		//										}
		//										rsPayment.Set_Fields("PreLienInterest", 0);
		//										rsPayment.Set_Fields("CurrentInterest", curTotalIntPayment);
		//										curCorrectCurrentIntPaidTotal += curTotalIntPayment;
		//										rsPayment.Update();
		//									}
		//								}
		//								else
		//								{
		//									if (curTotalIntPayment < 0)
		//									{
		//										if (curTotalIntPayment > curPreLienIntRemaining)
		//										{
		//											rsPayment.Edit();
		//											if (Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")) != FCConvert.ToDouble(curTotalIntPayment) || ((rsPayment.Get_Fields_Decimal("CurrentInterest"))) != 0)
		//											{
		//												FCFileSystem.WriteLine(1, "Adjusted Payment for Account: " + strAccount + "   Year: " + FCConvert.ToString(rsBillYear.Get_Fields_Int32("BillingYear")) + "   Old PLI Paid: " + FCConvert.ToString(rsPayment.Get_Fields_Decimal("PreLienInterest")) + "   New PLI Paid: " + FCConvert.ToString(curTotalIntPayment) + "   Old Cur Int Paid: " + FCConvert.ToString(rsPayment.Get_Fields_Decimal("CurrentInterest")) + "   New Cur Int Paid: 0.00");
		//											}
		//											rsPayment.Set_Fields("PreLienInterest", curTotalIntPayment);
		//											rsPayment.Set_Fields("CurrentInterest", 0);
		//											rsPayment.Update();
		//											curPreLienIntRemaining -= curTotalIntPayment;
		//											curCorrectPreLienIntPaidTotal += curTotalIntPayment;
		//										}
		//										else
		//										{
		//											rsPayment.Edit();
		//											if (FCConvert.ToDecimal(Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest"))) != curPreLienIntRemaining || FCConvert.ToDecimal(Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest"))) != (curTotalIntPayment - curPreLienIntRemaining))
		//											{
		//												FCFileSystem.WriteLine(1, "Adjusted Payment for Account: " + strAccount + "   Year: " + FCConvert.ToString(rsBillYear.Get_Fields_Int32("BillingYear")) + "   Old PLI Paid: " + FCConvert.ToString(rsPayment.Get_Fields_Decimal("PreLienInterest")) + "   New PLI Paid: " + FCConvert.ToString(curPreLienIntRemaining) + "   Old Cur Int Paid: " + FCConvert.ToString(rsPayment.Get_Fields_Decimal("CurrentInterest")) + "   New Cur Int Paid: " + FCConvert.ToString(curTotalIntPayment - curPreLienIntRemaining));
		//											}
		//											rsPayment.Set_Fields("PreLienInterest", curPreLienIntRemaining);
		//											curTotalIntPayment -= curPreLienIntRemaining;
		//											curCorrectPreLienIntPaidTotal += curPreLienIntRemaining;
		//											curPreLienIntRemaining = 0;
		//											rsPayment.Set_Fields("CurrentInterest", curTotalIntPayment);
		//											rsPayment.Update();
		//											curCurrentIntRemaining -= curTotalIntPayment;
		//											curCorrectCurrentIntPaidTotal += curTotalIntPayment;
		//										}
		//									}
		//									else if (curTotalIntPayment == 0)
		//									{
		//										if (((rsPayment.Get_Fields_Decimal("PreLienInterest"))) != 0 && ((rsPayment.Get_Fields_Decimal("CurrentInterest"))) != 0)
		//										{
		//											rsPayment.Edit();
		//											FCFileSystem.WriteLine(1, "Adjusted Payment for Account: " + strAccount + "   Year: " + FCConvert.ToString(rsBillYear.Get_Fields_Int32("BillingYear")) + "   Old PLI Paid: " + FCConvert.ToString(rsPayment.Get_Fields_Decimal("PreLienInterest")) + "   New PLI Paid: " + "0.00" + "   Old Cur Int Paid: " + FCConvert.ToString(rsPayment.Get_Fields_Decimal("CurrentInterest")) + "   New Cur Int Paid: " + "0.00");
		//											rsPayment.Set_Fields("PreLienInterest", 0);
		//											rsPayment.Set_Fields("CurrentInterest", 0);
		//											rsPayment.Update();
		//										}
		//									}
		//									else
		//									{
		//										rsPayment.Edit();
		//										if (((rsPayment.Get_Fields_Decimal("PreLienInterest"))) != 0 || (Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest"))) != FCConvert.ToDouble(curTotalIntPayment))
		//										{
		//											FCFileSystem.WriteLine(1, "Adjusted Payment for Account: " + strAccount + "   Year: " + FCConvert.ToString(rsBillYear.Get_Fields_Int32("BillingYear")) + "   Old PLI Paid: " + FCConvert.ToString(rsPayment.Get_Fields_Decimal("PreLienInterest")) + "   New PLI Paid: " + "0.00" + "   Old Cur Int Paid: " + FCConvert.ToString(rsPayment.Get_Fields_Decimal("CurrentInterest")) + "   New Cur Int Paid: " + FCConvert.ToString(curTotalIntPayment));
		//										}
		//										rsPayment.Set_Fields("PreLienInterest", 0);
		//										rsPayment.Set_Fields("CurrentInterest", curTotalIntPayment);
		//										curCorrectCurrentIntPaidTotal += curTotalIntPayment;
		//										rsPayment.Update();
		//									}
		//								}
		//							}
		//							rsPayment.MoveNext();
		//						}
		//						while (rsPayment.EndOfFile() != true);
		//						// TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
		//						FCFileSystem.WriteLine(1, "Account: " + strAccount + "   Year: " + FCConvert.ToString(rsBillYear.Get_Fields_Int32("BillingYear")) + "   Old PLI Paid: " + FCConvert.ToString(rsLien.Get_Fields("PLIPaid")) + "   New PLI Paid: " + FCConvert.ToString(curCorrectPreLienIntPaidTotal) + "   Old Cur Int Paid: " + FCConvert.ToString(rsLien.Get_Fields_Decimal("InterestPaid")) + "   New Cur Int Paid: " + FCConvert.ToString(curCorrectCurrentIntPaidTotal));
		//						rsLien.Edit();
		//						rsLien.Set_Fields("PLIPaid", curCorrectPreLienIntPaidTotal);
		//						rsLien.Set_Fields("InterestPaid", curCorrectCurrentIntPaidTotal);
		//						if (curCurrentIntPaymentTotal != (curCurrentIntTotal * -1))
		//						{
		//							FCFileSystem.WriteLine(1, "Adjusted Current Int Owed on Account: " + strAccount + "   Year: " + FCConvert.ToString(rsBillYear.Get_Fields_Int32("BillingYear")) + "   Old Cur Int Owed: " + FCConvert.ToString(rsLien.Get_Fields_Decimal("InterestCharged")) + "   New Cur Int Owed: " + FCConvert.ToString(curCurrentIntPaymentTotal));
		//							rsLien.Set_Fields("InterestCharged", curCurrentIntPaymentTotal);
		//						}
		//						rsLien.Update();
		//					}
		//				}
		//				CheckNextLien:
		//				;
		//				rsLien.MoveNext();
		//				frmWait.InstancePtr.IncrementProgress();
		//			}
		//			while (rsLien.EndOfFile() != true);
		//		}
		//		FCFileSystem.FileClose(1);
		//		// Bill Portion
		//		FCFileSystem.FileOpen(1, "CLBillFix.txt", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
		//		FCFileSystem.WriteLine(1, "Fix perfomed on " + FCConvert.ToString(DateTime.Now));
		//		frmWait.InstancePtr.lblMessage.Text = "Checking Bills" + "\r\n" + "Please Wait...";
		//		frmWait.InstancePtr.prgProgress.Value = 0;
  //              frmWait.InstancePtr.Refresh();
  //              // frmWait.Refresh
  //              rsLien.OpenRecordset("SELECT * FROM BillingMaster", "TWCL0000.vb1");
		//		if (rsLien.EndOfFile() != true && rsLien.BeginningOfFile() != true)
		//		{
		//			// frmWait.prgProgress.Max = rsLien.RecordCount
		//			do
		//			{
		//				App.DoEvents();
		//				curCurrentIntTotal = FCConvert.ToDecimal(rsLien.Get_Fields_Decimal("InterestCharged")) * -1;
		//				curCurrentIntPaidLienTotal = FCConvert.ToDecimal(rsLien.Get_Fields_Decimal("InterestPaid"));
		//				curCurrentIntPaidPaymentTotal = 0;
		//				curCurrentIntPaymentTotal = 0;
		//				rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(rsLien.Get_Fields_Int32("BillKey")) + " AND BillCode <> 'L' AND ReceiptNumber >= 0 ORDER BY ActualSystemDate", "TWCL0000.vb1");
		//				if (rsPayment.EndOfFile() != true && rsPayment.BeginningOfFile() != true)
		//				{
		//					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
		//					strAccount = FCConvert.ToString(rsPayment.Get_Fields("Account"));
		//					do
		//					{
		//						App.DoEvents();
		//						if (((rsPayment.Get_Fields_Decimal("PreLienInterest"))) != 0)
		//						{
		//							FCFileSystem.WriteLine(1, "Pre Lien Interest Moved to Current Interest on Payment for Account: " + strAccount + "   Year: " + FCConvert.ToString(rsLien.Get_Fields_Int32("BillingYear")) + "   PLI Paid: " + FCConvert.ToString(rsPayment.Get_Fields_Decimal("PreLienInterest")));
		//							rsPayment.Edit();
		//							rsPayment.Set_Fields("CurrentInterest", Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")) + Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")));
		//							rsPayment.Set_Fields("PreLienInterest", 0);
		//							rsPayment.Update();
		//						}
		//						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
		//						if (FCConvert.ToString(rsPayment.Get_Fields("Code")) == "I")
		//						{
		//							curCurrentIntPaymentTotal += FCConvert.ToDecimal(rsPayment.Get_Fields_Decimal("CurrentInterest"));
		//						}
		//						else
		//						{
		//							curCurrentIntPaidPaymentTotal += FCConvert.ToDecimal(rsPayment.Get_Fields_Decimal("CurrentInterest"));
		//						}
		//						rsPayment.MoveNext();
		//					}
		//					while (rsPayment.EndOfFile() != true);
		//					if (curCurrentIntPaidLienTotal != curCurrentIntPaidPaymentTotal)
		//					{
		//						FCFileSystem.WriteLine(1, "Adjusted Int Paid for Account: " + strAccount + "   Year: " + FCConvert.ToString(rsLien.Get_Fields_Int32("BillingYear")) + "   Old Int Paid: " + FCConvert.ToString(rsLien.Get_Fields_Decimal("InterestPaid")) + "   New Int Paid: " + FCConvert.ToString(curCurrentIntPaidPaymentTotal));
		//						rsLien.Edit();
		//						rsLien.Set_Fields("InterestPaid", curCurrentIntPaidPaymentTotal);
		//						rsLien.Update();
		//					}
		//					else
		//					{
		//					}
		//					if (FCConvert.ToDecimal(Conversion.Val(rsLien.Get_Fields_Decimal("InterestCharged"))) != curCurrentIntPaymentTotal)
		//					{
		//						FCFileSystem.WriteLine(1, "Adjusted Int Owed for Account: " + strAccount + "   Year: " + FCConvert.ToString(rsLien.Get_Fields_Int32("BillingYear")) + "   Old Int Owed: " + FCConvert.ToString(rsLien.Get_Fields_Decimal("InterestCharged")) + "   New Int Owed: " + FCConvert.ToString(curCurrentIntPaymentTotal * -1));
		//						rsLien.Edit();
		//						rsLien.Set_Fields("InterestCharged", curCurrentIntPaymentTotal);
		//						rsLien.Update();
		//					}
		//					else
		//					{
		//					}
		//				}
		//				rsLien.MoveNext();
		//				frmWait.InstancePtr.IncrementProgress();
		//			}
		//			while (rsLien.EndOfFile() != true);
		//		}
		//		FCFileSystem.FileClose(1);
		//		rsCheck.Edit();
		//		rsCheck.Set_Fields("RanGeneralFix", true);
		//		rsCheck.Update();
		//		frmWait.InstancePtr.lblMessage.Text = "Checking Database" + "\r\n" + "Please Wait...";
		//		frmWait.InstancePtr.prgProgress.Value = 0;
		//		frmWait.InstancePtr.prgProgress.Maximum = 10;
		//		frmWait.InstancePtr.prgProgress.Value = 1;
		//		frmWait.InstancePtr.Refresh();
		//	}
		//}

		//public static void AddCopiesFieldtoOwners()
		//{

		//}

		private static void AddSecurityPermissions()
		{
			// Tracker Reference: 11808
			clsDRWrapper rsTable = new clsDRWrapper();
			clsDRWrapper rsPerm = new clsDRWrapper();
			int lngID/*unused?*/;
			string strPerm = "";
			rsTable.OpenRecordset("SELECT * FROM PermissionSetup WHERE ModuleName = 'CL' AND FunctionID = 34", "SystemSettings");
			if (rsTable.RecordCount() == 0)
			{
				rsTable.AddNew();
				rsTable.Set_Fields("ModuleName", "CL");
				rsTable.Set_Fields("ParentFunction", "Interested Parties");
				rsTable.Set_Fields("ChildFunction", "");
				rsTable.Set_Fields("FunctionID", 34);
				rsTable.Set_Fields("UseViewOnly", false);
				rsTable.Set_Fields("Constant", "CLSECURITYINTERESTEDPARTIES");
				rsTable.Update();
			}
			rsTable.OpenRecordset("SELECT UserID, Permission FROM PermissionsTable WHERE ModuleName = 'CL' AND FunctionID = 8", "SystemSettings");
			if (rsTable.RecordCount() > 0)
			{
				rsTable.MoveFirst();
				while (!rsTable.EndOfFile())
				{
					App.DoEvents();
					strPerm = FCConvert.ToString(rsTable.Get_Fields_String("Permission"));
					// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
					rsPerm.OpenRecordset("SELECT * FROM PermissionsTable WHERE UserID = " + FCConvert.ToString(rsTable.Get_Fields("UserID")) + " AND ModuleName = 'CL' AND FunctionID = 34", "SystemSettings");
					if (rsPerm.RecordCount() == 0)
					{
						rsPerm.AddNew();
						// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
						rsPerm.Set_Fields("UserID", rsTable.Get_Fields("UserID"));
						rsPerm.Set_Fields("ModuleName", "CL");
						rsPerm.Set_Fields("FunctionID", 34);
						rsPerm.Set_Fields("Permission", strPerm);
						rsPerm.Update();
					}
					rsTable.MoveNext();
				}
			}
			// MAL@20080528: Add security for new report
			// Tracker Reference: 13784
			rsTable.OpenRecordset("SELECT * FROM PermissionSetup WHERE ModuleName = 'CL' AND FunctionID = 35", "SystemSettings");
			if (rsTable.RecordCount() == 0)
			{
				rsTable.AddNew();
				rsTable.Set_Fields("ModuleName", "CL");
				rsTable.Set_Fields("ParentFunction", "Tax Rate Report");
				rsTable.Set_Fields("ChildFunction", "");
				rsTable.Set_Fields("FunctionID", 35);
				rsTable.Set_Fields("UseViewOnly", false);
				rsTable.Set_Fields("Constant", "CLSECURITYPRINTTAXRATEREPORT");
				rsTable.Update();
			}
			rsTable.OpenRecordset("SELECT UserID, Permission FROM PermissionsTable WHERE ModuleName = 'CL' AND FunctionID = 33", "SystemSettings");
			// Get Permissions for Book/Page Report and use that as basis for Tax Rate report
			if (rsTable.RecordCount() > 0)
			{
				rsTable.MoveFirst();
				while (!rsTable.EndOfFile())
				{
					App.DoEvents();
					strPerm = FCConvert.ToString(rsTable.Get_Fields_String("Permission"));
					// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
					rsPerm.OpenRecordset("SELECT * FROM PermissionsTable WHERE UserID = " + FCConvert.ToString(rsTable.Get_Fields("UserID")) + " AND ModuleName = 'CL' AND FunctionID = 35", "SystemSettings");
					if (rsPerm.RecordCount() == 0)
					{
						rsPerm.AddNew();
						// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
						rsPerm.Set_Fields("UserID", rsTable.Get_Fields("UserID"));
						rsPerm.Set_Fields("ModuleName", "CL");
						rsPerm.Set_Fields("FunctionID", 35);
						rsPerm.Set_Fields("Permission", strPerm);
						rsPerm.Update();
					}
					rsTable.MoveNext();
				}
			}
			// MAL@20080723: Add security to cover the rest of the application options
			// Tracker Reference: 14565
			rsTable.OpenRecordset("SELECT * FROM PermissionSetup WHERE ModuleName = 'CL' AND FunctionID = 36", "SystemSettings");
			if (rsTable.RecordCount() == 0)
			{
				rsTable.AddNew();
				rsTable.Set_Fields("ModuleName", "CL");
				rsTable.Set_Fields("ParentFunction", "Lien Process");
				rsTable.Set_Fields("ChildFunction", "Reverse Demand Fees");
				rsTable.Set_Fields("FunctionID", 36);
				rsTable.Set_Fields("UseViewOnly", false);
				rsTable.Set_Fields("Constant", "CLSECURITYREVERSEDEMANDFEES");
				rsTable.Update();
			}
			rsTable.OpenRecordset("SELECT UserID, Permission FROM PermissionsTable WHERE ModuleName = 'CL' AND FunctionID = 12", "SystemSettings");
			// Get Permissions for Lien Process and use that as basis
			if (rsTable.RecordCount() > 0)
			{
				rsTable.MoveFirst();
				while (!rsTable.EndOfFile())
				{
					strPerm = FCConvert.ToString(rsTable.Get_Fields_String("Permission"));
					// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
					rsPerm.OpenRecordset("SELECT * FROM PermissionsTable WHERE UserID = " + FCConvert.ToString(rsTable.Get_Fields("UserID")) + " AND ModuleName = 'CL' AND FunctionID = 36", "SystemSettings");
					if (rsPerm.RecordCount() == 0)
					{
						rsPerm.AddNew();
						// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
						rsPerm.Set_Fields("UserID", rsTable.Get_Fields("UserID"));
						rsPerm.Set_Fields("ModuleName", "CL");
						rsPerm.Set_Fields("FunctionID", 36);
						rsPerm.Set_Fields("Permission", strPerm);
						rsPerm.Update();
					}
					rsTable.MoveNext();
				}
			}
			rsTable.OpenRecordset("SELECT * FROM PermissionSetup WHERE ModuleName = 'CL' AND FunctionID = 37", "SystemSettings");
			if (rsTable.RecordCount() == 0)
			{
				rsTable.AddNew();
				rsTable.Set_Fields("ModuleName", "CL");
				rsTable.Set_Fields("ParentFunction", "Lien Process");
				rsTable.Set_Fields("ChildFunction", "Edit Lien Book/Page");
				rsTable.Set_Fields("FunctionID", 37);
				rsTable.Set_Fields("UseViewOnly", false);
				rsTable.Set_Fields("Constant", "CLSECURITYEDITLIENBOOKPAGE");
				rsTable.Update();
			}
			rsTable.OpenRecordset("SELECT UserID, Permission FROM PermissionsTable WHERE ModuleName = 'CL' AND FunctionID = 12", "SystemSettings");
			// Get Permissions for Lien Process and use that as basis
			if (rsTable.RecordCount() > 0)
			{
				rsTable.MoveFirst();
				while (!rsTable.EndOfFile())
				{
					strPerm = FCConvert.ToString(rsTable.Get_Fields_String("Permission"));
					// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
					rsPerm.OpenRecordset("SELECT * FROM PermissionsTable WHERE UserID = " + FCConvert.ToString(rsTable.Get_Fields("UserID")) + " AND ModuleName = 'CL' AND FunctionID = 37", "SystemSettings");
					if (rsPerm.RecordCount() == 0)
					{
						rsPerm.AddNew();
						// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
						rsPerm.Set_Fields("UserID", rsTable.Get_Fields("UserID"));
						rsPerm.Set_Fields("ModuleName", "CL");
						rsPerm.Set_Fields("FunctionID", 37);
						rsPerm.Set_Fields("Permission", strPerm);
						rsPerm.Update();
					}
					rsTable.MoveNext();
				}
			}
			rsTable.OpenRecordset("SELECT * FROM PermissionSetup WHERE ModuleName = 'CL' AND FunctionID = 38", "SystemSettings");
			if (rsTable.RecordCount() == 0)
			{
				rsTable.AddNew();
				rsTable.Set_Fields("ModuleName", "CL");
				rsTable.Set_Fields("ParentFunction", "Lien Process");
				rsTable.Set_Fields("ChildFunction", "Edit LDN Book/Page");
				rsTable.Set_Fields("FunctionID", 38);
				rsTable.Set_Fields("UseViewOnly", false);
				rsTable.Set_Fields("Constant", "CLSECURITYEDITLDNBOOKPAGE");
				rsTable.Update();
			}
			rsTable.OpenRecordset("SELECT UserID, Permission FROM PermissionsTable WHERE ModuleName = 'CL' AND FunctionID = 12", "SystemSettings");
			// Get Permissions for Lien Process and use that as basis
			if (rsTable.RecordCount() > 0)
			{
				rsTable.MoveFirst();
				while (!rsTable.EndOfFile())
				{
					strPerm = FCConvert.ToString(rsTable.Get_Fields_String("Permission"));
					// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
					rsPerm.OpenRecordset("SELECT * FROM PermissionsTable WHERE UserID = " + FCConvert.ToString(rsTable.Get_Fields("UserID")) + " AND ModuleName = 'CL' AND FunctionID = 38", "SystemSettings");
					if (rsPerm.RecordCount() == 0)
					{
						rsPerm.AddNew();
						// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
						rsPerm.Set_Fields("UserID", rsTable.Get_Fields("UserID"));
						rsPerm.Set_Fields("ModuleName", "CL");
						rsPerm.Set_Fields("FunctionID", 38);
						rsPerm.Set_Fields("Permission", strPerm);
						rsPerm.Update();
					}
					rsTable.MoveNext();
				}
			}
			rsTable.OpenRecordset("SELECT * FROM PermissionSetup WHERE ModuleName = 'CL' AND FunctionID = 39", "SystemSettings");
			if (rsTable.RecordCount() == 0)
			{
				rsTable.AddNew();
				rsTable.Set_Fields("ModuleName", "CL");
				rsTable.Set_Fields("ParentFunction", "Lien Process");
				rsTable.Set_Fields("ChildFunction", "Lien Date Chart");
				rsTable.Set_Fields("FunctionID", 39);
				rsTable.Set_Fields("UseViewOnly", false);
				rsTable.Set_Fields("Constant", "CLSECURITYLIENDATECHART");
				rsTable.Update();
			}
			rsTable.OpenRecordset("SELECT UserID, Permission FROM PermissionsTable WHERE ModuleName = 'CL' AND FunctionID = 12", "SystemSettings");
			// Get Permissions for Lien Process and use that as basis
			if (rsTable.RecordCount() > 0)
			{
				rsTable.MoveFirst();
				while (!rsTable.EndOfFile())
				{
					strPerm = FCConvert.ToString(rsTable.Get_Fields_String("Permission"));
					// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
					rsPerm.OpenRecordset("SELECT * FROM PermissionsTable WHERE UserID = " + FCConvert.ToString(rsTable.Get_Fields("UserID")) + " AND ModuleName = 'CL' AND FunctionID = 39", "SystemSettings");
					if (rsPerm.RecordCount() == 0)
					{
						rsPerm.AddNew();
						// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
						rsPerm.Set_Fields("UserID", rsTable.Get_Fields("UserID"));
						rsPerm.Set_Fields("ModuleName", "CL");
						rsPerm.Set_Fields("FunctionID", 39);
						rsPerm.Set_Fields("Permission", strPerm);
						rsPerm.Update();
					}
					rsTable.MoveNext();
				}
			}
			rsTable.OpenRecordset("SELECT * FROM PermissionSetup WHERE ModuleName = 'CL' AND FunctionID = 40", "SystemSettings");
			if (rsTable.RecordCount() == 0)
			{
				rsTable.AddNew();
				rsTable.Set_Fields("ModuleName", "CL");
				rsTable.Set_Fields("ParentFunction", "Lien Process");
				rsTable.Set_Fields("ChildFunction", "Updated Book Page Report");
				rsTable.Set_Fields("FunctionID", 40);
				rsTable.Set_Fields("UseViewOnly", false);
				rsTable.Set_Fields("Constant", "CLSECURITYUPDATEDBOOKPAGERPT");
				rsTable.Update();
			}
			rsTable.OpenRecordset("SELECT UserID, Permission FROM PermissionsTable WHERE ModuleName = 'CL' AND FunctionID = 12", "SystemSettings");
			// Get Permissions for Lien Process and use that as basis
			if (rsTable.RecordCount() > 0)
			{
				rsTable.MoveFirst();
				while (!rsTable.EndOfFile())
				{
					strPerm = FCConvert.ToString(rsTable.Get_Fields_String("Permission"));
					// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
					rsPerm.OpenRecordset("SELECT * FROM PermissionsTable WHERE UserID = " + FCConvert.ToString(rsTable.Get_Fields("UserID")) + " AND ModuleName = 'CL' AND FunctionID = 40", "SystemSettings");
					if (rsPerm.RecordCount() == 0)
					{
						rsPerm.AddNew();
						// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
						rsPerm.Set_Fields("UserID", rsTable.Get_Fields("UserID"));
						rsPerm.Set_Fields("ModuleName", "CL");
						rsPerm.Set_Fields("FunctionID", 40);
						rsPerm.Set_Fields("Permission", strPerm);
						rsPerm.Update();
					}
					rsTable.MoveNext();
				}
			}
			rsTable.OpenRecordset("SELECT * FROM PermissionSetup WHERE ModuleName = 'CL' AND FunctionID = 41", "SystemSettings");
			if (rsTable.RecordCount() == 0)
			{
				rsTable.AddNew();
				rsTable.Set_Fields("ModuleName", "CL");
				rsTable.Set_Fields("ParentFunction", "Lien Process");
				rsTable.Set_Fields("ChildFunction", "Tax Acquired Properties");
				rsTable.Set_Fields("FunctionID", 41);
				rsTable.Set_Fields("UseViewOnly", false);
				rsTable.Set_Fields("Constant", "CLSECURITYTAXACQUIREDPROPERTIES");
				rsTable.Update();
			}
			rsTable.OpenRecordset("SELECT UserID, Permission FROM PermissionsTable WHERE ModuleName = 'CL' AND FunctionID = 12", "SystemSettings");
			// Get Permissions for Lien Process and use that as basis
			if (rsTable.RecordCount() > 0)
			{
				rsTable.MoveFirst();
				while (!rsTable.EndOfFile())
				{
					strPerm = FCConvert.ToString(rsTable.Get_Fields_String("Permission"));
					// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
					rsPerm.OpenRecordset("SELECT * FROM PermissionsTable WHERE UserID = " + FCConvert.ToString(rsTable.Get_Fields("UserID")) + " AND ModuleName = 'CL' AND FunctionID = 41", "SystemSettings");
					if (rsPerm.RecordCount() == 0)
					{
						rsPerm.AddNew();
						// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
						rsPerm.Set_Fields("UserID", rsTable.Get_Fields("UserID"));
						rsPerm.Set_Fields("ModuleName", "CL");
						rsPerm.Set_Fields("FunctionID", 41);
						rsPerm.Set_Fields("Permission", strPerm);
						rsPerm.Update();
					}
					rsTable.MoveNext();
				}
			}
			rsTable.OpenRecordset("SELECT * FROM PermissionSetup WHERE ModuleName = 'CL' AND FunctionID = 42", "SystemSettings");
			if (rsTable.RecordCount() == 0)
			{
				rsTable.AddNew();
				rsTable.Set_Fields("ModuleName", "CL");
				rsTable.Set_Fields("ParentFunction", "Lien Process");
				rsTable.Set_Fields("ChildFunction", "Update Bill Addresses");
				rsTable.Set_Fields("FunctionID", 42);
				rsTable.Set_Fields("UseViewOnly", false);
				rsTable.Set_Fields("Constant", "CLSECURITYUPDATEBILLADDRESS");
				rsTable.Update();
			}
			rsTable.OpenRecordset("SELECT UserID, Permission FROM PermissionsTable WHERE ModuleName = 'CL' AND FunctionID = 12", "SystemSettings");
			// Get Permissions for Lien Process and use that as basis
			if (rsTable.RecordCount() > 0)
			{
				rsTable.MoveFirst();
				while (!rsTable.EndOfFile())
				{
					strPerm = FCConvert.ToString(rsTable.Get_Fields_String("Permission"));
					// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
					rsPerm.OpenRecordset("SELECT * FROM PermissionsTable WHERE UserID = " + FCConvert.ToString(rsTable.Get_Fields("UserID")) + " AND ModuleName = 'CL' AND FunctionID = 42", "SystemSettings");
					if (rsPerm.RecordCount() == 0)
					{
						rsPerm.AddNew();
						// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
						rsPerm.Set_Fields("UserID", rsTable.Get_Fields("UserID"));
						rsPerm.Set_Fields("ModuleName", "CL");
						rsPerm.Set_Fields("FunctionID", 42);
						rsPerm.Set_Fields("Permission", strPerm);
						rsPerm.Update();
					}
					rsTable.MoveNext();
				}
			}
			rsTable.OpenRecordset("SELECT * FROM PermissionSetup WHERE ModuleName = 'CL' AND FunctionID = 43", "SystemSettings");
			if (rsTable.RecordCount() == 0)
			{
				rsTable.AddNew();
				rsTable.Set_Fields("ModuleName", "CL");
				rsTable.Set_Fields("ParentFunction", "File Maintenance");
				rsTable.Set_Fields("ChildFunction", "Customize");
				rsTable.Set_Fields("FunctionID", 43);
				rsTable.Set_Fields("UseViewOnly", false);
				rsTable.Set_Fields("Constant", "CLSECURITYCUSTOMIZE");
				rsTable.Update();
			}
			rsTable.OpenRecordset("SELECT UserID, Permission FROM PermissionsTable WHERE ModuleName = 'CL' AND FunctionID = 7", "SystemSettings");
			// Get Permissions for File Maintenance and use that as basis
			if (rsTable.RecordCount() > 0)
			{
				rsTable.MoveFirst();
				while (!rsTable.EndOfFile())
				{
					strPerm = FCConvert.ToString(rsTable.Get_Fields_String("Permission"));
					// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
					rsPerm.OpenRecordset("SELECT * FROM PermissionsTable WHERE UserID = " + FCConvert.ToString(rsTable.Get_Fields("UserID")) + " AND ModuleName = 'CL' AND FunctionID = 43", "SystemSettings");
					if (rsPerm.RecordCount() == 0)
					{
						rsPerm.AddNew();
						// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
						rsPerm.Set_Fields("UserID", rsTable.Get_Fields("UserID"));
						rsPerm.Set_Fields("ModuleName", "CL");
						rsPerm.Set_Fields("FunctionID", 43);
						rsPerm.Set_Fields("Permission", strPerm);
						rsPerm.Update();
					}
					rsTable.MoveNext();
				}
			}
			rsTable.OpenRecordset("SELECT * FROM PermissionSetup WHERE ModuleName = 'CL' AND FunctionID = 44", "SystemSettings");
			if (rsTable.RecordCount() == 0)
			{
				rsTable.AddNew();
				rsTable.Set_Fields("ModuleName", "CL");
				rsTable.Set_Fields("ParentFunction", "File Maintenance");
				rsTable.Set_Fields("ChildFunction", "Tax Service Extract");
				rsTable.Set_Fields("FunctionID", 44);
				rsTable.Set_Fields("UseViewOnly", false);
				rsTable.Set_Fields("Constant", "CLSECURITYTAXSERVICEEXTRACT");
				rsTable.Update();
			}
			rsTable.OpenRecordset("SELECT UserID, Permission FROM PermissionsTable WHERE ModuleName = 'CL' AND FunctionID = 7", "SystemSettings");
			// Get Permissions for File Maintenance and use that as basis
			if (rsTable.RecordCount() > 0)
			{
				rsTable.MoveFirst();
				while (!rsTable.EndOfFile())
				{
					strPerm = FCConvert.ToString(rsTable.Get_Fields_String("Permission"));
					// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
					rsPerm.OpenRecordset("SELECT * FROM PermissionsTable WHERE UserID = " + FCConvert.ToString(rsTable.Get_Fields("UserID")) + " AND ModuleName = 'CL' AND FunctionID = 44", "SystemSettings");
					if (rsPerm.RecordCount() == 0)
					{
						rsPerm.AddNew();
						// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
						rsPerm.Set_Fields("UserID", rsTable.Get_Fields("UserID"));
						rsPerm.Set_Fields("ModuleName", "CL");
						rsPerm.Set_Fields("FunctionID", 44);
						rsPerm.Set_Fields("Permission", strPerm);
						rsPerm.Update();
					}
					rsTable.MoveNext();
				}
			}
			rsTable.OpenRecordset("SELECT * FROM PermissionSetup WHERE ModuleName = 'CL' AND FunctionID = 45", "SystemSettings");
			if (rsTable.RecordCount() == 0)
			{
				rsTable.AddNew();
				rsTable.Set_Fields("ModuleName", "CL");
				rsTable.Set_Fields("ParentFunction", "File Maintenance");
				rsTable.Set_Fields("ChildFunction", "Rate Record Update");
				rsTable.Set_Fields("FunctionID", 45);
				rsTable.Set_Fields("UseViewOnly", false);
				rsTable.Set_Fields("Constant", "CLSECURITYRATERECORDUPDATE");
				rsTable.Update();
			}
			rsTable.OpenRecordset("SELECT UserID, Permission FROM PermissionsTable WHERE ModuleName = 'CL' AND FunctionID = 7", "SystemSettings");
			// Get Permissions for File Maintenance and use that as basis
			if (rsTable.RecordCount() > 0)
			{
				rsTable.MoveFirst();
				while (!rsTable.EndOfFile())
				{
					strPerm = FCConvert.ToString(rsTable.Get_Fields_String("Permission"));
					// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
					rsPerm.OpenRecordset("SELECT * FROM PermissionsTable WHERE UserID = " + FCConvert.ToString(rsTable.Get_Fields("UserID")) + " AND ModuleName = 'CL' AND FunctionID = 45", "SystemSettings");
					if (rsPerm.RecordCount() == 0)
					{
						rsPerm.AddNew();
						// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
						rsPerm.Set_Fields("UserID", rsTable.Get_Fields("UserID"));
						rsPerm.Set_Fields("ModuleName", "CL");
						rsPerm.Set_Fields("FunctionID", 45);
						rsPerm.Set_Fields("Permission", strPerm);
						rsPerm.Update();
					}
					rsTable.MoveNext();
				}
			}
			rsTable.OpenRecordset("SELECT * FROM PermissionSetup WHERE ModuleName = 'CL' AND FunctionID = 46", "SystemSettings");
			if (rsTable.RecordCount() == 0)
			{
				rsTable.AddNew();
				rsTable.Set_Fields("ModuleName", "CL");
				rsTable.Set_Fields("ParentFunction", "File Maintenance");
				rsTable.Set_Fields("ChildFunction", "Edit Bill Information");
				rsTable.Set_Fields("FunctionID", 46);
				rsTable.Set_Fields("UseViewOnly", false);
				rsTable.Set_Fields("Constant", "CLSECURITYEDITBILLINFO");
				rsTable.Update();
			}
			rsTable.OpenRecordset("SELECT UserID, Permission FROM PermissionsTable WHERE ModuleName = 'CL' AND FunctionID = 7", "SystemSettings");
			// Get Permissions for File Maintenance and use that as basis
			if (rsTable.RecordCount() > 0)
			{
				rsTable.MoveFirst();
				while (!rsTable.EndOfFile())
				{
					strPerm = FCConvert.ToString(rsTable.Get_Fields_String("Permission"));
					// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
					rsPerm.OpenRecordset("SELECT * FROM PermissionsTable WHERE UserID = " + FCConvert.ToString(rsTable.Get_Fields("UserID")) + " AND ModuleName = 'CL' AND FunctionID = 46", "SystemSettings");
					if (rsPerm.RecordCount() == 0)
					{
						rsPerm.AddNew();
						// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
						rsPerm.Set_Fields("UserID", rsTable.Get_Fields("UserID"));
						rsPerm.Set_Fields("ModuleName", "CL");
						rsPerm.Set_Fields("FunctionID", 46);
						rsPerm.Set_Fields("Permission", strPerm);
						rsPerm.Update();
					}
					rsTable.MoveNext();
				}
			}
			rsTable.OpenRecordset("SELECT * FROM PermissionSetup WHERE ModuleName = 'CL' AND FunctionID = 47", "SystemSettings");
			if (rsTable.RecordCount() == 0)
			{
				rsTable.AddNew();
				rsTable.Set_Fields("ModuleName", "CL");
				rsTable.Set_Fields("ParentFunction", "File Maintenance");
				rsTable.Set_Fields("ChildFunction", "Remove From Tax Acquired");
				rsTable.Set_Fields("FunctionID", 47);
				rsTable.Set_Fields("UseViewOnly", false);
				rsTable.Set_Fields("Constant", "CLSECURITYREMOVEFROMTAXACQUIRED");
				rsTable.Update();
			}
			rsTable.OpenRecordset("SELECT UserID, Permission FROM PermissionsTable WHERE ModuleName = 'CL' AND FunctionID = 7", "SystemSettings");
			// Get Permissions for File Maintenance and use that as basis
			if (rsTable.RecordCount() > 0)
			{
				rsTable.MoveFirst();
				while (!rsTable.EndOfFile())
				{
					strPerm = FCConvert.ToString(rsTable.Get_Fields_String("Permission"));
					// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
					rsPerm.OpenRecordset("SELECT * FROM PermissionsTable WHERE UserID = " + FCConvert.ToString(rsTable.Get_Fields("UserID")) + " AND ModuleName = 'CL' AND FunctionID = 47", "SystemSettings");
					if (rsPerm.RecordCount() == 0)
					{
						rsPerm.AddNew();
						// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
						rsPerm.Set_Fields("UserID", rsTable.Get_Fields("UserID"));
						rsPerm.Set_Fields("ModuleName", "CL");
						rsPerm.Set_Fields("FunctionID", 47);
						rsPerm.Set_Fields("Permission", strPerm);
						rsPerm.Update();
					}
					rsTable.MoveNext();
				}
			}
		}

		//private static void GeneralFixPastDueAmounts()
		//{
		//	// Tracker Reference: 12355
		//	bool blnContinue = false;
		//	clsDRWrapper rsBill = new clsDRWrapper();
		//	clsDRWrapper rsLien = new clsDRWrapper();
		//	clsDRWrapper rsPayment = new clsDRWrapper();
		//	clsDRWrapper rsTemp = new clsDRWrapper();
		//	int lngAccountKey = 0;
		//	int lngBillKey = 0;
		//	int lngLienKey = 0;
		//	string strYear = "";
		//	double dblPrinOwed_Bill = 0;
		//	double dblTaxOwed_Bill = 0;
		//	double dblIntOwed_Bill = 0;
		//	double dblCostOwed_Bill = 0;
		//	double dblPLIOwed_Bill = 0;
		//	double dblMatOwed_Bill = 0;
		//	double dblIntAdded_Bill = 0;
		//	double dblCostAdded_Bill/*unused?*/;
		//	double dblPrinPaid_Bill = 0;
		//	double dblTaxPaid_Bill = 0;
		//	double dblIntPaid_Bill = 0;
		//	double dblCostPaid_Bill = 0;
		//	double dblPLIPaid_Bill = 0;
		//	double dblMatPaid_Bill/*unused?*/;
		//	double dblPrinPaid_Pymt = 0;
		//	double dblTaxPaid_Pymt;
		//	double dblIntPaid_Pymt = 0;
		//	double dblIntChg_Pymt = 0;
		//	double dblCostChg_Pymt = 0;
		//	double dblCostPaid_Pymt = 0;
		//	double dblPLIPaid_Pymt = 0;
		//	double dblMatPaid_Pymt;
		//	double dblMatChg_Pymt = 0;
		//	double dblTotalOwed = 0;
		//	double dblTotalPaid = 0;
		//	double dblTotal;
		//	rsTemp.OpenRecordset("SELECT * FROM Collections", modExtraModules.strCLDatabase);
		//	blnContinue = !FCConvert.ToBoolean(rsTemp.Get_Fields_Boolean("GeneralFixPastDueAmts"));
		//	frmWait.InstancePtr.Init("Checking Payment Amounts");
		//	// , True
		//	if (blnContinue)
		//	{
		//		frmWait.InstancePtr.lblMessage.Text = "Checking Non-Lien Payments" + "\r\n" + "Please Wait...";
		//		frmWait.InstancePtr.prgProgress.Value = 0;
  //              frmWait.InstancePtr.Refresh();
  //              // frmWait.Refresh
  //              // Non-Liened Records
  //              rsBill.OpenRecordset("SELECT * FROM BillingMaster WHERE LienRecordNumber = 0 ORDER BY Account, BillKey", modExtraModules.strCLDatabase);
		//		if (rsBill.RecordCount() > 0)
		//		{
		//			// frmWait.prgProgress.Max = .RecordCount
		//			rsBill.MoveFirst();
		//			while (!rsBill.EndOfFile())
		//			{
		//				App.DoEvents();
		//				lngBillKey = FCConvert.ToInt32(rsBill.Get_Fields_Int32("BillKey"));
		//				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
		//				lngAccountKey = FCConvert.ToInt32(rsBill.Get_Fields("Account"));
		//				strYear = FCConvert.ToString(rsBill.Get_Fields_Int32("BillingYear"));
		//				// Reset Values
		//				dblPrinPaid_Pymt = 0;
		//				dblTaxPaid_Pymt = 0;
		//				dblIntPaid_Pymt = 0;
		//				dblCostPaid_Pymt = 0;
		//				dblIntChg_Pymt = 0;
		//				dblCostChg_Pymt = 0;
		//				dblPrinOwed_Bill = Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue4"));
		//				dblIntOwed_Bill = 0;
		//				dblIntAdded_Bill = Conversion.Val(rsBill.Get_Fields_Decimal("InterestCharged"));
		//				dblCostOwed_Bill = Conversion.Val(rsBill.Get_Fields_Decimal("DemandFees"));
		//				dblTotalOwed = dblPrinOwed_Bill + dblTaxOwed_Bill + dblIntOwed_Bill + dblCostOwed_Bill - dblIntAdded_Bill;
		//				dblPrinPaid_Bill = Conversion.Val(rsBill.Get_Fields_Decimal("PrincipalPaid"));
		//				dblTaxPaid_Bill = 0;
		//				dblIntPaid_Bill = Conversion.Val(rsBill.Get_Fields_Decimal("InterestPaid"));
		//				dblCostPaid_Bill = Conversion.Val(rsBill.Get_Fields_Decimal("DemandFeesPaid"));
		//				dblTotalPaid = dblPrinPaid_Bill + dblTaxPaid_Bill + dblIntPaid_Bill + dblCostPaid_Bill;
		//				dblTotal = dblTotalOwed - dblTotalPaid;
		//				// Get Charged Interest to Compare
		//				rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE Account = " + FCConvert.ToString(lngAccountKey) + " AND BillKey = " + FCConvert.ToString(lngBillKey) + " AND [Year] = " + strYear + " AND BillCode <> 'L' AND (Reference = 'CHGINT' OR Reference = 'INTEREST')", modExtraModules.strCLDatabase);
		//				if (rsPayment.RecordCount() > 0)
		//				{
		//					if (rsPayment.RecordCount() > 1)
		//					{
		//						while (!rsPayment.EndOfFile())
		//						{
		//							dblIntChg_Pymt += Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest"));
		//							rsPayment.MoveNext();
		//						}
		//					}
		//					else
		//					{
		//						dblIntChg_Pymt = Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest"));
		//					}
		//				}
		//				// Get Charged Costs to Compare
		//				rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE Account = " + FCConvert.ToString(lngAccountKey) + " AND BillKey = " + FCConvert.ToString(lngBillKey) + " AND [Year] = " + strYear + " AND BillCode <> 'L' AND (Code = 'L' OR Code = '3')", modExtraModules.strCLDatabase);
		//				if (rsPayment.RecordCount() > 0)
		//				{
		//					if (rsPayment.RecordCount() > 1)
		//					{
		//						while (!rsPayment.EndOfFile())
		//						{
		//							dblCostChg_Pymt += Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost"));
		//							rsPayment.MoveNext();
		//						}
		//					}
		//					else
		//					{
		//						dblCostChg_Pymt = Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost"));
		//					}
		//				}
		//				rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE Account = " + FCConvert.ToString(lngAccountKey) + " AND BillKey = " + FCConvert.ToString(lngBillKey) + " AND [Year] = " + strYear + " AND BillCode <> 'L' AND Code <> 'I' AND Code <> 'L' AND Code <> '3'", modExtraModules.strCLDatabase);
		//				if (rsPayment.RecordCount() > 0)
		//				{
		//					rsPayment.MoveFirst();
		//					while (!rsPayment.EndOfFile())
		//					{
		//						App.DoEvents();
		//						// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
		//						dblPrinPaid_Pymt += Conversion.Val(rsPayment.Get_Fields("Principal"));
		//						dblIntPaid_Pymt += Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest"));
		//						dblCostPaid_Pymt += Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost"));
		//						rsPayment.MoveNext();
		//					}
		//				}
		//				else
		//				{
		//					// Do Nothing - No Payments, Nothing to Fix
		//				}
		//				if (dblPrinPaid_Bill == dblPrinPaid_Pymt)
		//				{
		//					// Principal Payments Match - Move On
		//				}
		//				else
		//				{
		//					rsBill.Edit();
		//					rsBill.Set_Fields("PrincipalPaid", dblPrinPaid_Pymt);
		//					rsBill.Update();
		//				}
		//				// Costs
		//				if (dblCostOwed_Bill == (dblCostChg_Pymt * -1))
		//				{
		//					// Do Nothing - They're Equal
		//				}
		//				else
		//				{
		//					rsBill.Edit();
		//					rsBill.Set_Fields("DemandFees", (dblCostChg_Pymt * -1));
		//					rsBill.Update();
		//				}
		//				if (dblCostPaid_Bill == dblCostPaid_Pymt)
		//				{
		//					// Cost Payments Match - Move On
		//				}
		//				else
		//				{
		//					rsBill.Edit();
		//					rsBill.Set_Fields("DemandFeesPaid", dblCostPaid_Pymt);
		//					rsBill.Update();
		//				}
		//				// Charged Interest ; Check All Pieces (Paid and Charged)
		//				if (dblIntAdded_Bill != dblIntChg_Pymt)
		//				{
		//					rsBill.Edit();
		//					rsBill.Set_Fields("InterestCharged", dblIntChg_Pymt);
		//					rsBill.Update();
		//				}
		//				// Check Payments
		//				if (dblIntPaid_Bill == dblIntPaid_Pymt)
		//				{
		//					// Equal - No Changes Needed
		//				}
		//				else
		//				{
		//					rsBill.Edit();
		//					rsBill.Set_Fields("InterestPaid", dblIntPaid_Pymt);
		//					rsBill.Update();
		//				}
		//				frmWait.InstancePtr.IncrementProgress();
		//				rsBill.MoveNext();
		//			}
		//		}
		//		// Liened Bills
		//		frmWait.InstancePtr.lblMessage.Text = "Checking Lien Payments" + "\r\n" + "Please Wait...";
		//		frmWait.InstancePtr.prgProgress.Value = 0;
		//		frmWait.InstancePtr.Refresh();
		//		rsBill.OpenRecordset("SELECT * FROM BillingMaster WHERE LienRecordNumber > 0 ORDER BY Account, BillKey", modExtraModules.strCLDatabase);
		//		if (rsBill.RecordCount() > 0)
		//		{
		//			// frmWait.prgProgress.Max = .RecordCount
		//			rsBill.MoveFirst();
		//			while (!rsBill.EndOfFile())
		//			{
		//				App.DoEvents();
		//				lngBillKey = FCConvert.ToInt32(rsBill.Get_Fields_Int32("BillKey"));
		//				lngLienKey = FCConvert.ToInt32(rsBill.Get_Fields_Int32("LienRecordNumber"));
		//				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
		//				lngAccountKey = FCConvert.ToInt32(rsBill.Get_Fields("Account"));
		//				strYear = FCConvert.ToString(rsBill.Get_Fields_Int32("BillingYear"));
		//				// Reset Values
		//				dblPrinPaid_Pymt = 0;
		//				dblTaxPaid_Pymt = 0;
		//				dblIntPaid_Pymt = 0;
		//				dblCostPaid_Pymt = 0;
		//				dblPLIPaid_Pymt = 0;
		//				dblIntChg_Pymt = 0;
		//				dblCostChg_Pymt = 0;
		//				dblMatPaid_Pymt = 0;
		//				dblMatChg_Pymt = 0;
		//				rsLien.OpenRecordset("SELECT * FROM LienRec WHERE LienRecordNumber = " + FCConvert.ToString(lngLienKey), modExtraModules.strCLDatabase);
		//				if (rsLien.RecordCount() > 0)
		//				{
		//					// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
		//					dblPrinOwed_Bill = Conversion.Val(rsLien.Get_Fields("Principal"));
		//					dblIntOwed_Bill = FCConvert.ToDouble(rsLien.Get_Fields_Decimal("InterestCharged") * -1);
		//					// TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
		//					dblPLIOwed_Bill = Conversion.Val(rsLien.Get_Fields("Interest"));
		//					// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
		//					dblCostOwed_Bill = Conversion.Val(rsLien.Get_Fields("Costs"));
		//					// TODO Get_Fields: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
		//					dblMatOwed_Bill = FCConvert.ToDouble(rsLien.Get_Fields("MaturityFee")) * -1;
		//					dblTotalOwed = dblPrinOwed_Bill + dblTaxOwed_Bill + dblIntOwed_Bill + dblPLIOwed_Bill + dblCostOwed_Bill + dblMatOwed_Bill;
		//					dblPrinPaid_Bill = Conversion.Val(rsLien.Get_Fields_Decimal("PrincipalPaid"));
		//					dblIntPaid_Bill = Conversion.Val(rsLien.Get_Fields_Decimal("InterestPaid"));
		//					// TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
		//					dblPLIPaid_Bill = Conversion.Val(rsLien.Get_Fields("PLIPaid"));
		//					dblCostPaid_Bill = Conversion.Val(rsLien.Get_Fields_Decimal("CostsPaid"));
		//					dblTotalPaid = dblPrinPaid_Bill + dblTaxPaid_Bill + dblIntPaid_Bill + dblPLIPaid_Bill + dblCostPaid_Bill;
		//					dblTotal = dblTotalOwed - dblTotalPaid;
		//					rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE Account = " + FCConvert.ToString(lngAccountKey) + " AND BillKey = " + FCConvert.ToString(lngLienKey) + " AND [Year] = " + strYear + " AND BillCode = 'L' AND (Reference = 'CHGINT' Or Reference = 'INTEREST')", modExtraModules.strCLDatabase);
		//					if (rsPayment.RecordCount() > 0)
		//					{
		//						if (rsPayment.RecordCount() > 1)
		//						{
		//							while (!rsPayment.EndOfFile())
		//							{
		//								App.DoEvents();
		//								dblIntChg_Pymt += Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest"));
		//								rsPayment.MoveNext();
		//							}
		//						}
		//						else
		//						{
		//							dblIntChg_Pymt = Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest"));
		//						}
		//					}
		//					// Get Added Maturity Fees to Compare
		//					rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE Account = " + FCConvert.ToString(lngAccountKey) + " AND BillKey = " + FCConvert.ToString(lngLienKey) + " AND [Year] = " + strYear + " AND (Code = 'L' OR Code = '3') AND BillCode = 'L'", modExtraModules.strCLDatabase);
		//					if (rsPayment.RecordCount() > 0)
		//					{
		//						if (rsPayment.RecordCount() > 1)
		//						{
		//							while (!rsPayment.EndOfFile())
		//							{
		//								App.DoEvents();
		//								dblMatChg_Pymt += Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost"));
		//								rsPayment.MoveNext();
		//							}
		//						}
		//						else
		//						{
		//							dblMatChg_Pymt = Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost"));
		//						}
		//					}
		//					rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE Account = " + FCConvert.ToString(lngAccountKey) + " AND BillKey = " + FCConvert.ToString(lngLienKey) + "AND [Year] = " + strYear + " AND BillCode = 'L' AND Code <> 'I' AND Code <> 'L' AND Code <> '3'", modExtraModules.strCLDatabase);
		//					if (rsPayment.RecordCount() > 0)
		//					{
		//						rsPayment.MoveFirst();
		//						while (!rsPayment.EndOfFile())
		//						{
		//							App.DoEvents();
		//							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
		//							dblPrinPaid_Pymt += Conversion.Val(rsPayment.Get_Fields("Principal"));
		//							dblIntPaid_Pymt += Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest"));
		//							dblPLIPaid_Pymt += Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest"));
		//							dblCostPaid_Pymt += Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost"));
		//							rsPayment.MoveNext();
		//						}
		//					}
		//					else
		//					{
		//						// Do Nothing - No Payments
		//					}
		//					// Principal ; Only Change the Paid Amount, Should Not Change the Charged Amount
		//					if (dblPrinPaid_Bill == dblPrinPaid_Pymt)
		//					{
		//						// Principal Payments Match - Move On
		//					}
		//					else
		//					{
		//						rsLien.Edit();
		//						rsLien.Set_Fields("PrincipalPaid", dblPrinPaid_Pymt);
		//						rsLien.Update();
		//					}
		//					// Maturity Fees
		//					if (dblMatOwed_Bill != (dblMatChg_Pymt * -1))
		//					{
		//						rsLien.Edit();
		//						rsLien.Set_Fields("MaturityFee", dblMatChg_Pymt);
		//						rsLien.Update();
		//					}
		//					// Costs ; Check All Pieces (Paid and Charged)
		//					if (dblCostPaid_Bill == dblCostPaid_Pymt)
		//					{
		//						// Cost Payments Match - Move On
		//					}
		//					else
		//					{
		//						rsLien.Edit();
		//						rsLien.Set_Fields("CostsPaid", dblCostPaid_Pymt);
		//						rsLien.Update();
		//					}
		//					// Charged Interest ; Check All Pieces (Paid and Charged)
		//					if (dblIntOwed_Bill != (dblIntChg_Pymt * -1))
		//					{
		//						rsLien.Edit();
		//						rsLien.Set_Fields("InterestCharged", dblIntChg_Pymt);
		//						rsLien.Update();
		//					}
		//					// Check Payments
		//					if (dblIntPaid_Bill == dblIntPaid_Pymt)
		//					{
		//						// Equal - No Changes Needed
		//					}
		//					else
		//					{
		//						rsLien.Edit();
		//						rsLien.Set_Fields("InterestPaid", dblIntPaid_Pymt);
		//						rsLien.Update();
		//					}
		//					// Pre-Lien Interest ; Check All Pieces (Paid and Charged)
		//					if (dblPLIPaid_Bill == dblPLIPaid_Pymt)
		//					{
		//						// PLI Payments Match - Move On
		//					}
		//					else
		//					{
		//						rsLien.Edit();
		//						rsLien.Set_Fields("PLIPaid", dblPLIPaid_Pymt);
		//						rsLien.Update();
		//					}
		//				}
		//				else
		//				{
		//					// Do Nothing - Lien Record Does Not Exist
		//				}
		//				frmWait.InstancePtr.IncrementProgress();
		//				rsBill.MoveNext();
		//			}
		//		}
		//		rsTemp.Edit();
		//		rsTemp.Set_Fields("GeneralFixPastDueAmts", true);
		//		rsTemp.Update();
		//		frmWait.InstancePtr.lblMessage.Text = "Checking Database" + "\r\n" + "Please Wait...";
		//		frmWait.InstancePtr.prgProgress.Value = 0;
		//		frmWait.InstancePtr.prgProgress.Maximum = 10;
		//		frmWait.InstancePtr.prgProgress.Value = 2;
  //              frmWait.InstancePtr.Refresh();
  //              // frmWait.Refresh
  //          }
		//	else
		//	{
		//		// This Has Already Been Run - Don't Run It Again
		//	}
		//}

		public static void ClearBogusCHGINTRecords()
		{
			// Tracker Reference: 13228
			clsDRWrapper rsPymt = new clsDRWrapper();
			rsPymt.OpenRecordset("SELECT * FROM PaymentRec WHERE Reference = 'CHGINT' AND ReceiptNumber = -1", modExtraModules.strCLDatabase);
			if (rsPymt.RecordCount() > 0)
			{
				rsPymt.MoveFirst();
				while (!rsPymt.EndOfFile())
				{
					rsPymt.Delete();
					rsPymt.Update();
					// kk08112014 trocls-49
					rsPymt.MoveNext();
				}
			}
		}

		//public static void FixReverseDemandFees_PostGF()
		//{
		//	// MAL@20080514: Post global fix accounts with reverse demand fees had incorrect right-click amounts
		//	// Tracker Reference: 13653
		//	clsDRWrapper rsBill = new clsDRWrapper();
		//	clsDRWrapper rsLien = new clsDRWrapper();
		//	clsDRWrapper rsPymt = new clsDRWrapper();
		//	double dblCost = 0;
		//	double dblCostPay = 0;
		//	int lngKey = 0;
		//	int lngLienKey = 0;
		//	int intCount/*unused?*/;
		//	rsBill.OpenRecordset("SELECT * FROM BillingMaster WHERE DemandFees <> 0 AND BillingType = 'RE'", modExtraModules.strCLDatabase);
		//	if (rsBill.RecordCount() > 0)
		//	{
		//		while (!rsBill.EndOfFile())
		//		{
		//			dblCost = 0;
		//			dblCostPay = 0;
		//			lngKey = FCConvert.ToInt32(rsBill.Get_Fields_Int32("BillKey"));
		//			lngLienKey = FCConvert.ToInt32(rsBill.Get_Fields_Int32("LienRecordNumber"));
		//			if (lngLienKey == 0)
		//			{
		//				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
		//				rsPymt.OpenRecordset("SELECT * FROM PaymentRec WHERE Account = " + FCConvert.ToString(rsBill.Get_Fields("Account")) + " AND BillKey = " + FCConvert.ToString(lngKey) + " AND BillCode = 'R'", modExtraModules.strCLDatabase);
		//				if (rsPymt.RecordCount() > 0)
		//				{
		//					while (!rsPymt.EndOfFile())
		//					{
		//						if (rsPymt.Get_Fields_Decimal("LienCost") > 0)
		//						{
		//							dblCostPay += Conversion.Val(rsPymt.Get_Fields_Decimal("LienCost"));
		//						}
		//						else if (Conversion.Val(rsPymt.Get_Fields_Decimal("LienCost")) < 0)
		//						{
		//							dblCost -= Conversion.Val(rsPymt.Get_Fields_Decimal("LienCost"));
		//						}
		//						else
		//						{
		//							dblCostPay = dblCostPay;
		//							dblCost = dblCost;
		//						}
		//						rsPymt.MoveNext();
		//					}
		//					if (Conversion.Val(rsBill.Get_Fields_Decimal("DemandFeesPaid")) != dblCostPay || Conversion.Val(rsBill.Get_Fields_Decimal("DemandFees")) != dblCost)
		//					{
		//						rsBill.Edit();
		//						rsBill.Set_Fields("DemandFeesPaid", dblCostPay);
		//						rsBill.Set_Fields("DemandFees", dblCost);
		//						rsBill.Update();
		//					}
		//				}
		//			}
		//			else
		//			{
		//				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
		//				rsPymt.OpenRecordset("SELECT * FROM PaymentRec WHERE Account = " + FCConvert.ToString(rsBill.Get_Fields("Account")) + " AND BillKey = " + FCConvert.ToString(lngLienKey) + " AND Code <> 'L'", modExtraModules.strCLDatabase);
		//				if (rsPymt.RecordCount() > 0)
		//				{
		//					while (!rsPymt.EndOfFile())
		//					{
		//						// If rsPymt.Get_Fields("LienCost") > 0 Then
		//						// dblCostPay = dblCostPay + rsPymt.Get_Fields("LienCost")
		//						// ElseIf rsPymt.Get_Fields("LienCost") < 0 Then
		//						// dblCost = dblCost - rsPymt.Get_Fields("LienCost")
		//						// Else
		//						// dblCostPay = dblCostPay
		//						// dblCost = dblCost
		//						// End If
		//						dblCostPay += Conversion.Val(rsPymt.Get_Fields_Decimal("LienCost"));
		//						// dblCost = dblCost
		//						rsPymt.MoveNext();
		//					}
		//					rsLien.OpenRecordset("SELECT * FROM LienRec WHERE LienRecordNumber = " + FCConvert.ToString(lngLienKey), "TWCL0000.vb1");
		//					if (rsLien.RecordCount() > 0)
		//					{
		//						// If rsLien.Get_Fields("Costs") < dblCost And rsLien.Get_Fields("Costs") <> dblCost Then
		//						// rsLien.Edit
		//						// rsLien.Get_Fields("Costs") = dblCost
		//						// rsLien.Update
		//						// End If
		//						// If rsLien.Get_Fields("CostsPaid") < dblCostPay And rsLien.Get_Fields("CostsPaid") <> dblCostPay Then
		//						if (Conversion.Val(rsLien.Get_Fields_Decimal("CostsPaid")) != dblCostPay)
		//						{
		//							rsLien.Edit();
		//							rsLien.Set_Fields("CostsPaid", dblCostPay);
		//							rsLien.Update();
		//						}
		//					}
		//				}
		//			}
		//			rsBill.MoveNext();
		//		}
		//	}
		//}

		//public static void FixConversionIRecords_PostGF()
		//{
		//	// MAL@20080514: Adapt fix program to run for all towns
		//	// Tracker Reference: 13686
		//	clsDRWrapper rsBill = new clsDRWrapper();
		//	clsDRWrapper rsInt = new clsDRWrapper();
		//	clsDRWrapper rsPymt = new clsDRWrapper();
		//	double dblAmt = 0;
		//	double dblChg = 0;
		//	double dblInt = 0;
		//	double dblNew = 0;
		//	int intCnt = 0;
		//	int lngAcct = 0;
		//	int lngBill = 0;
		//	int lngYear;
		//	string strType = "";
		//	// Loop Through All Records
		//	rsBill.OpenRecordset("SELECT * FROM BillingMaster WHERE ID IN (SELECT BillKey FROM PaymentRec WHERE Reference = 'CNVRSN' AND Code = 'I') ORDER BY BillingType, Account, BillingYear", modExtraModules.strCLDatabase);
		//	if (rsBill.RecordCount() > 0)
		//	{
		//		rsBill.MoveFirst();
		//		while (!rsBill.EndOfFile())
		//		{
		//			dblNew = 0;
		//			dblChg = 0;
		//			dblInt = 0;
		//			lngBill = FCConvert.ToInt32(rsBill.Get_Fields_Int32("ID"));
		//			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
		//			lngAcct = FCConvert.ToInt32(rsBill.Get_Fields("Account"));
		//			lngYear = FCConvert.ToInt32(rsBill.Get_Fields_Int32("BillingYear"));
		//			strType = FCConvert.ToString(rsBill.Get_Fields_String("BillingType"));
		//			dblAmt = Conversion.Val(rsBill.Get_Fields_Decimal("InterestCharged"));
		//			rsPymt.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngBill) + " AND Account = " + FCConvert.ToString(lngAcct) + " AND Reference = 'CNVRSN' AND Code = 'I'", modExtraModules.strCLDatabase);
		//			if (rsPymt.RecordCount() > 0)
		//			{
		//				rsPymt.MoveFirst();
		//				while (!rsPymt.EndOfFile())
		//				{
		//					dblInt += Conversion.Val(rsPymt.Get_Fields_Decimal("CurrentInterest"));
		//					rsPymt.MoveNext();
		//				}
		//				rsInt.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngBill) + " AND Account = " + FCConvert.ToString(lngAcct) + " AND Reference = 'CHGINT' AND Code = 'I'", modExtraModules.strCLDatabase);
		//				if (rsInt.RecordCount() > 0)
		//				{
		//					rsInt.MoveFirst();
		//					while (!rsInt.EndOfFile())
		//					{
		//						dblChg += Conversion.Val(rsInt.Get_Fields_Decimal("CurrentInterest"));
		//						rsInt.MoveNext();
		//					}
		//				}
		//				if ((dblInt + dblChg) == dblAmt)
		//				{
		//					// Do Nothing - Account Matches
		//					dblNew = dblAmt;
		//				}
		//				else
		//				{
		//					dblNew = dblInt + dblChg;
		//				}
		//				// If dblAmt <> dblInt Then
		//				// dblNew = dblAmt + dblInt
		//				// Else
		//				// dblNew = dblAmt
		//				// End If
		//				rsBill.Edit();
		//				rsBill.Set_Fields("InterestCharged", dblNew);
		//				rsBill.Update();
		//				intCnt += 1;
		//			}
		//			else
		//			{
		//				// Do Nothing - No Conversion Records
		//			}
		//			rsBill.MoveNext();
		//		}
		//	}
		//}

		//public static void FixEarnInterest_PostGF()
		//{
		//	// MAL@20080514: Adapted fix program that Corrects Bill Records Where Payments for EARNINT exist
		//	clsDRWrapper rsBill = new clsDRWrapper();
		//	clsDRWrapper rsInt = new clsDRWrapper();
		//	clsDRWrapper rsPymt = new clsDRWrapper();
		//	double dblAmt = 0;
		//	double dblChg = 0;
		//	double dblInt = 0;
		//	double dblNew = 0;
		//	int intCnt = 0;
		//	int lngAcct = 0;
		//	int lngBill = 0;
		//	int lngYear;
		//	string strType = "";
		//	// Loop Through All Records
		//	rsBill.OpenRecordset("SELECT * FROM BillingMaster WHERE ID IN (SELECT BillKey FROM PaymentRec WHERE Reference = 'CNVRSN' AND Code = 'I') ORDER BY BillingType, Account, BillingYear", modExtraModules.strCLDatabase);
		//	if (rsBill.RecordCount() > 0)
		//	{
		//		rsBill.MoveFirst();
		//		while (!rsBill.EndOfFile())
		//		{
		//			dblNew = 0;
		//			dblChg = 0;
		//			dblInt = 0;
		//			lngBill = FCConvert.ToInt32(rsBill.Get_Fields_Int32("ID"));
		//			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
		//			lngAcct = FCConvert.ToInt32(rsBill.Get_Fields("Account"));
		//			lngYear = FCConvert.ToInt32(rsBill.Get_Fields_Int32("BillingYear"));
		//			strType = FCConvert.ToString(rsBill.Get_Fields_String("BillingType"));
		//			dblAmt = Conversion.Val(rsBill.Get_Fields_Decimal("InterestCharged"));
		//			rsPymt.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngBill) + " AND Account = " + FCConvert.ToString(lngAcct) + " AND Reference = 'EARNINT' AND Code = 'I'", modExtraModules.strCLDatabase);
		//			if (rsPymt.RecordCount() > 0)
		//			{
		//				rsPymt.MoveFirst();
		//				while (!rsPymt.EndOfFile())
		//				{
		//					dblInt += Conversion.Val(rsPymt.Get_Fields_Decimal("CurrentInterest"));
		//					rsPymt.MoveNext();
		//				}
		//				rsInt.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngBill) + " AND Account = " + FCConvert.ToString(lngAcct) + " AND Reference = 'CHGINT' AND Code = 'I'", modExtraModules.strCLDatabase);
		//				if (rsInt.RecordCount() > 0)
		//				{
		//					rsInt.MoveFirst();
		//					while (!rsInt.EndOfFile())
		//					{
		//						dblChg += Conversion.Val(rsInt.Get_Fields_Decimal("CurrentInterest"));
		//						rsInt.MoveNext();
		//					}
		//				}
		//				if ((dblInt + dblChg) == dblAmt)
		//				{
		//					// Do Nothing - Account Matches
		//					dblNew = dblAmt;
		//				}
		//				else
		//				{
		//					dblNew = dblInt + dblChg;
		//				}
		//				// If dblAmt <> dblInt Then
		//				// dblNew = dblAmt + dblInt
		//				// Else
		//				// dblNew = dblAmt
		//				// End If
		//				rsBill.Edit();
		//				rsBill.Set_Fields("InterestCharged", dblNew);
		//				rsBill.Update();
		//				intCnt += 1;
		//			}
		//			else
		//			{
		//				// Do Nothing - No Conversion Records
		//			}
		//			rsBill.MoveNext();
		//		}
		//	}
		//}

		private static void GetGlobalPrintAdjustments()
		{
			// Tracker Reference: 14279
			string strTemp = "";
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL", "AdjustmentCMFLaser", ref strTemp);
			if (Conversion.Val(strTemp) != 0)
			{
				Statics.gdblCMFAdjustment = FCConvert.ToDouble(strTemp);
			}
			else
			{
				Statics.gdblCMFAdjustment = 0;
			}
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL", "AdjustmentLabels", ref strTemp);
			if (Conversion.Val(strTemp) != 0)
			{
				Statics.gdblLabelsAdjustment = FCConvert.ToDouble(strTemp);
			}
			else
			{
				Statics.gdblLabelsAdjustment = 0;
			}
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL", "AdjustmentLabelsDMH", ref strTemp);
			if (Conversion.Val(strTemp) != 0)
			{
				Statics.gdblLabelsAdjustmentDMH = FCConvert.ToDouble(strTemp);
			}
			else
			{
				Statics.gdblLabelsAdjustmentDMH = 0;
			}
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL", "AdjustmentLabelsDMV", ref strTemp);
			if (Conversion.Val(strTemp) != 0)
			{
				Statics.gdblLabelsAdjustmentDMV = FCConvert.ToDouble(strTemp);
			}
			else
			{
				Statics.gdblLabelsAdjustmentDMV = 0;
			}
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL", "gdblLienAdjustmentTop", ref strTemp);
			if (Conversion.Val(strTemp) != 0)
			{
				Statics.gdblLienAdjustmentTop = FCConvert.ToDouble(strTemp);
			}
			else
			{
				Statics.gdblLienAdjustmentTop = 0;
			}
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL", "gdblLienAdjustmentBottom", ref strTemp);
			// kk04212014 trocl-1156
			if (Conversion.Val(strTemp) != 0)
			{
				Statics.gdblLienAdjustmentBottom = FCConvert.ToDouble(strTemp);
			}
			else
			{
				Statics.gdblLienAdjustmentBottom = 0;
			}
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL", "gdblLienAdjustmentSide", ref strTemp);
			// kk02082016 trout-1197  Added side margin adjustment and removed the top right box
			if (Conversion.Val(strTemp) != 0)
			{
				Statics.gdblLienAdjustmentSide = FCConvert.ToDouble(strTemp);
			}
			else
			{
				Statics.gdblLienAdjustmentSide = 0;
			}
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL", "gdblLDNAdjustmentTop", ref strTemp);
			if (Conversion.Val(strTemp) != 0)
			{
				Statics.gdblLDNAdjustmentTop = FCConvert.ToDouble(strTemp);
			}
			else
			{
				Statics.gdblLDNAdjustmentTop = 0;
			}
			// kk04212014 trocl-1156
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL", "gdblLDNAdjustmentBottom", ref strTemp);
			if (Conversion.Val(strTemp) != 0)
			{
				Statics.gdblLDNAdjustmentBottom = FCConvert.ToDouble(strTemp);
			}
			else
			{
				Statics.gdblLDNAdjustmentBottom = 0;
			}
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL", "gdblLDNAdjustmentSide", ref strTemp);
			// kk02082016 trout-1197  Added side margin adjustment
			if (Conversion.Val(strTemp) != 0)
			{
				Statics.gdblLDNAdjustmentSide = FCConvert.ToDouble(strTemp);
			}
			else
			{
				Statics.gdblLDNAdjustmentSide = 0;
			}
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL", "ReminderFormAdjustH", ref strTemp);
			if (Conversion.Val(strTemp) != 0)
			{
				Statics.gdblReminderFormAdjustH = FCConvert.ToDouble(strTemp);
			}
			else
			{
				Statics.gdblReminderFormAdjustH = 0;
			}
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL", "ReminderFormAdjustV", ref strTemp);
			if (Conversion.Val(strTemp) != 0)
			{
				Statics.gdblReminderFormAdjustV = FCConvert.ToDouble(strTemp);
			}
			else
			{
				Statics.gdblReminderFormAdjustV = 0;
			}
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL", "AdjustmentLienSignature", ref strTemp);
			if (Conversion.Val(strTemp) != 0)
			{
				Statics.gdblLienSigAdjust = FCConvert.ToDouble(strTemp);
			}
			else
			{
				Statics.gdblLienSigAdjust = 0;
			}
		}

		//public static void ImportPaymentFile()
		//{
		//	try
		//	{
		//		// On Error GoTo ErrorHandler
		//		string strImportFile = "";
		//		return;
		//	}
		//	catch (Exception ex)
		//	{
		//		// ErrorHandler:
		//		FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In ImportPaymentFile", MsgBoxStyle.Critical, "Error");
		//	}
		//}

		private static void SetupGlobalQueries()
		{
			// kgk 03292012  These were stored procedures in NewVersion()
			// Since these can appear as subqueries, we can't allow duplicate column names to appear in the result set,
			// so will no longer use SELECT * with joined tables
			// I don't see a reason why gstrOSBalQuery is fetching Bills with (LRN = 0 AND PrinDue <> 0) OR DemandFeesDue <> 0 OR IntDue <> 0
			// I looked at the call tracker 13187, but I still think the query should be LRN = 0 AND (PrinDue <> 0 OR DemandFeesDue <> 0 OR IntDue <> 0)
			// so I am changing the WHERE clause in gstrODLienBalQuery
			// gstrOSBalQuery = "SELECT * FROM BillingMaster WHERE (  (([BillingMaster].[LienRecordNumber])=0) And (([TaxDue1]+[TaxDue2]+[TaxDue3]+[TaxDue4])<>[PrincipalPaid]) OR DemandFees <> DemandFeesPaid OR InterestCharged <> ([InterestPaid]*-1)  )"
			// kk 01092012 trocl-17  Added checks for NULL values below
			//Statics.gstrOSBalQuery = "SELECT * FROM BillingMaster WHERE ISNULL(LienRecordNumber,0) = 0 And ( (ISNULL(TaxDue1,0)+ISNULL(TaxDue2,0)+ISNULL(TaxDue3,0)+ISNULL(TaxDue4,0)) <> ISNULL(PrincipalPaid,0) OR ISNULL(DemandFees,0) <> ISNULL(DemandFeesPaid,0) OR ISNULL(InterestCharged,0) <> (ISNULL(InterestPaid,0) * -1) )";
			//Statics.gstrOSLienBalQuery = "SELECT LienRec.*,b.BillingYear,b.RateKey as BRateKey,b.BillingType,b.LienRecordNumber AS LRN,b.Account,b.Name1 " + "FROM BillingMaster b INNER JOIN LienRec ON b.LienRecordNumber = LienRec.ID " + "WHERE ISNULL(LienRec.Principal,0) <> ISNULL(LienRec.PrincipalPaid,0)";
			//Statics.gstrZeroBalQuery = "SELECT * FROM BillingMaster WHERE ISNULL(LienRecordNumber,0) = 0 And (ISNULL(TaxDue1,0)+ISNULL(TaxDue2,0)+ISNULL(TaxDue3,0)+ISNULL(TaxDue4,0)) = ISNULL(PrincipalPaid,0)";
			//Statics.gstrZeroLienBalQuery = "SELECT LienRec.*,b.BillingYear,b.RateKey as BRateKey,b.BillingType,b.LienRecordNumber AS LRN,b.Account,b.Name1 " + "FROM BillingMaster b INNER JOIN LienRec ON b.LienRecordNumber = LienRec.ID " + "WHERE ISNULL(LienRec.Principal,0) = ISNULL(LienRec.PrincipalPaid,0)";
		}

		public static string GetMasterJoin( bool boolRE)
		{
			string GetMasterJoin = "";
			// select master.*, cpo.FullNameLastFirst as RSName,cpso.FullNameLastFirst as RSSecOwner,cpo.address1 as rsaddr1,cpo.address2 as rsaddr2,cpo.address3 as rsaddress3,cpo.city as rsaddr3, cpo.state as rsstate,cpo.zip as rszip, '' as rszip4 from TRIO_Test_Live_RealEstate.dbo.master left join TRIO_Test_Live_CentralParties.dbo.PartyAndAddressView as cpo on (master.ownerpartyid = cpo.PartyID) left join TRIO_Test_Live_CentralParties.dbo.PartyAndAddressView as cpso on (master.SecOwnerPartyID = cpso.PartyID)
			string strReturn = "";
			if (boolRE)
			{
				strReturn = "select master.*, DeedName1 as Own1FullName,DeedName2 as Own2FullName,cpo.address1 as Own1Address1,cpo.address2 as Own1Address2,cpo.city as Own1City, cpo.state as Own1State,cpo.zip as Own1Zip from " + Statics.strDbRE + "master left join ";
				strReturn += Statics.strDbCP + "PartyAndAddressView cpo on master.ownerpartyid = cpo.ID left join ";
				strReturn += Statics.strDbCP + "PartyAndAddressView cpso on master.SecOwnerPartyID = cpso.ID";
				// strReturn = "select * from (" & strReturn & ") AS qTmp"
			}
			else
			{
				strReturn = "select ppmaster.*, cpo.FullNameLF as Own1FullName,cpo.address1 as Own1Address1,cpo.address2 as Own1Address2,cpo.city as Own1City, cpo.state as Own1State,cpo.zip as Own1Zip" + Statics.strDbPP + "ppmaster left join ";
				strReturn += Statics.strDbCP + "PartyAndAddressView as cpo on ppmaster.partyid = cpo.PartyID";
				// strReturn = "select * from (" & strReturn & ") AS qTmp"
			}
			GetMasterJoin = strReturn;
			return GetMasterJoin;
		}

		//private static void UpdateRateEntryAndStorage()
		//{
		//	// kk09302014 trocl-1197  Change the Discount, Prepayment and Abatement Interest Rates
		//	// to be entered as Percentages and stored as decimal (7.00% -> 0.07)
		//	clsDRWrapper rsColl = new clsDRWrapper();
		//	double dblDiscRate = 0;
		//	double dblPrepayRate = 0;
		//	double dblAbateRate = 0;
		//	rsColl.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'RateEntryUpdateDone' AND TABLE_NAME = 'Collections'", modExtraModules.strCLDatabase);
		//	if (rsColl.EndOfFile())
		//	{
		//		rsColl.Execute("ALTER TABLE.[dbo].[Collections] ADD [RateEntryUpdateDone] bit NULL", modExtraModules.strCLDatabase);
		//	}
		//	rsColl.OpenRecordset("SELECT RateEntryUpdateDone FROM Collections", modExtraModules.strCLDatabase);
		//	if (!FCConvert.ToBoolean(rsColl.Get_Fields_Boolean("RateEntryUpdateDone")))
		//	{
		//		rsColl.OpenRecordset("SELECT DiscountPercent, OverPayInterestRate, AbatementInterestRate FROM Collections", modExtraModules.strCLDatabase);
		//		if (!rsColl.EndOfFile())
		//		{
		//			// TODO Get_Fields: Check the table for the column [DiscountPercent] and replace with corresponding Get_Field method
		//			dblDiscRate = Conversion.Val(rsColl.Get_Fields("DiscountPercent"));
		//			dblPrepayRate = Conversion.Val(rsColl.Get_Fields_Double("OverPayInterestRate"));
		//			dblAbateRate = Conversion.Val(rsColl.Get_Fields_Double("AbatementInterestRate"));
		//			rsColl.Execute("ALTER TABLE [dbo].[Collections] DROP COLUMN [DiscountPercent]", modExtraModules.strCLDatabase);
		//			rsColl.Execute("ALTER TABLE dbo.Collections ADD DiscountPercent float(53) NULL", modExtraModules.strCLDatabase);
		//			rsColl.OpenRecordset("SELECT * FROM Collections", modExtraModules.strCLDatabase);
		//			rsColl.Edit();
		//			rsColl.Set_Fields("RateEntryUpdateDone", true);
		//			rsColl.Set_Fields("DiscountPercent", dblDiscRate / 10000);
		//			rsColl.Set_Fields("OverPayInterestRate", dblPrepayRate / 100);
		//			rsColl.Set_Fields("AbatementInterestRate", dblAbateRate / 10000);
		//			rsColl.Update();
		//			if (dblDiscRate != 0 || dblPrepayRate != 0 || dblAbateRate != 0)
		//			{
		//				frmWait.InstancePtr.Visible = false;
		//				FCMessageBox.Show("Please verify the rates for discounts and interest in File Maintenance -> Customize are entered as percentages and not decimals.");
		//				frmWait.InstancePtr.Visible = true;
		//			}
		//		}
		//	}
		//}

		public static bool CheckDatabaseStructure()
		{
			bool CheckDatabaseStructure = false;
			bool boolReturn;
            var reDb = new cRealEstateDB();
            reDb.CheckVersion();
            var ppDb = new cPPDatabase();
            ppDb.CheckVersion();
            cCollectionsDB collDB = new cCollectionsDB();
			boolReturn = collDB.CheckVersion();
			CheckDatabaseStructure = boolReturn;
			return CheckDatabaseStructure;
		}
		// Private Sub CheckDatabaseStructure()
		// Dim rsDB As New clsDRWrapper
		// DoEvents
		// Make sure Version is in the Collections table
		// rsDB.OpenRecordset "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'Version' AND TABLE_NAME = 'Collections'", strCLDatabase
		// If rsDB.EndOfFile Then
		// rsDB.Execute "ALTER TABLE.[dbo].[Collections] ADD [Version] nvarchar(50) NULL", strCLDatabase
		// End If
		// DoEvents
		// kk 09252013 trocl-30  Lien Discharge Filing Fee Increase
		// rsDB.OpenRecordset "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'LienFeeIncrRunOn' AND TABLE_NAME = 'Collections'", strCLDatabase
		// If rsDB.EndOfFile Then
		// rsDB.Execute "ALTER TABLE.[dbo].[Collections] ADD [LienFeeIncrRunOn] datetime NULL", strCLDatabase
		// rsDB.Execute "UPDATE Collections SET LienFeeIncrRunOn = '" & Format(CDate(0), "MM/dd/yyyy") & "'", strCLDatabase
		// End If
		// DoEvents
		// kk03162015 trocrs-36  Add option to disable auto-fill payment amount on collection screen
		// rsDB.OpenRecordset "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'DisableAutoPmtFill' AND TABLE_NAME = 'Collections'", strCLDatabase
		// If rsDB.EndOfFile Then
		// rsDB.Execute "ALTER TABLE.[dbo].[Collections] ADD [DisableAutoPmtFill] bit NULL", strCLDatabase
		// rsDB.Execute "UPDATE Collections SET DisableAutoPmtFill = 0", strCLDatabase
		// End If
		// DoEvents
		// Make sure BillingMaster is indexed on Account
		// rsDB.OpenRecordset "SELECT name FROM sys.indexes WHERE Name = 'IX_BillingMaster_Account'"
		// If rsDB.EndOfFile Then
		// rsDB.Execute "CREATE NONCLUSTERED INDEX [IX_BillingMaster_Account] ON [dbo].[BillingMaster] " &
		// "([Account] ASC, [BillingType] Asc) " &
		// "INCLUDE ([LienRecordNumber]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]", strCLDatabase
		// End If
		// DoEvents
		// Make sure BillingMaster is indexed on Name1
		// rsDB.OpenRecordset "SELECT name FROM sys.indexes WHERE Name = 'IX_BillingMaster_Name1'"
		// If rsDB.EndOfFile Then
		// rsDB.Execute "CREATE NONCLUSTERED INDEX [IX_BillingMaster_Name1] ON [dbo].[BillingMaster] " &
		// "([Name1] ASC, [BillingType] Asc) " &
		// "INCLUDE ([LienRecordNumber]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]", strCLDatabase
		// End If
		// DoEvents
		// Make sure PaymentRec is indexed on Account
		// rsDB.OpenRecordset "SELECT name FROM sys.indexes WHERE Name = 'IX_PaymentRec_Account'"
		// If rsDB.EndOfFile Then
		// rsDB.Execute "CREATE NONCLUSTERED INDEX [IX_PaymentRec_Account] ON [dbo].[PaymentRec] " &
		// "([Account] Asc)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]", strCLDatabase
		// End If
		// DoEvents
		// Make sure PaymentRec is indexed on BillKey
		// rsDB.OpenRecordset "SELECT name FROM sys.indexes WHERE Name = 'IX_PaymentRec_BillKey'"
		// If rsDB.EndOfFile Then
		// rsDB.Execute "CREATE NONCLUSTERED INDEX [IX_PaymentRec_BillKey] ON [dbo].[PaymentRec] " &
		// "([BillKey] Asc, [BillCode] ASC) " &
		// "INCLUDE ([Code],[Principal],[PreLienInterest],[CurrentInterest],[LienCost],[ReceiptNumber],[RecordedTransactionDate]) " &
		// "WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]", strCLDatabase
		// End If
		// DoEvents
		// Make sure RE Master is indexed on RSAccount
		// rsDB.OpenRecordset "SELECT name FROM sys.indexes WHERE Name = 'IX_Master_RSAccount'", strREDatabase
		// If rsDB.EndOfFile Then
		// rsDB.Execute "CREATE NONCLUSTERED INDEX [IX_Master_RSAccount] ON [dbo].[Master] " &
		// "([RSAccount] Asc)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]", strREDatabase
		// End If
		// DoEvents
		// Make sure PP PPMaster is indexed on Account
		// rsDB.OpenRecordset "SELECT name FROM sys.indexes WHERE Name = 'IX_PPMaster_Account'", strPPDatabase
		// If rsDB.EndOfFile Then
		// rsDB.Execute "CREATE NONCLUSTERED INDEX [IX_PPMaster_Account] ON [dbo].[PPMaster] " &
		// "([Account] Asc)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]", strPPDatabase
		// End If
		// DoEvents
		// kk09302014 trocl-1197  Fix for Discount, Prepayment and Abatement Interest Rates
		// UpdateRateEntryAndStorage
		//
		// kk09242015 trocls-63  Add IMPB Tracking Number to CMFNumbers and BillingMaster tables
		// rsDB.OpenRecordset "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'IMPBTrackingNumber' AND TABLE_NAME = 'CMFNumbers'", strCLDatabase
		// If rsDB.EndOfFile Then
		// rsDB.Execute "ALTER TABLE.[dbo].[CMFNumbers] ADD [IMPBTrackingNumber] nvarchar(30) NULL", strCLDatabase
		// End If
		// rsDB.OpenRecordset "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'IMPBTrackingNumber' AND TABLE_NAME = 'BillingMaster'", strCLDatabase
		// If rsDB.EndOfFile Then
		// rsDB.Execute "ALTER TABLE.[dbo].[BillingMaster] ADD [IMPBTrackingNumber] nvarchar(30) NULL", strCLDatabase
		// End If
		// DoEvents
		// trocls-68
		// AlterBookPageFields
		//
		// End Sub
		//private static string GetDataType(string strTableName, string strFieldName, string strDB)
		//{
		//	string GetDataType = "";
		//	string strSQL;
		//	clsDRWrapper rsLoad = new clsDRWrapper();
		//	strSQL = "select data_type from information_schema.columns where table_name = '" + strTableName + "' and column_name = '" + strFieldName + "'";
		//	rsLoad.OpenRecordset(strSQL, strDB);
		//	if (!rsLoad.EndOfFile())
		//	{
		//		// TODO Get_Fields: Field [data_type] not found!! (maybe it is an alias?)
		//		GetDataType = FCConvert.ToString(rsLoad.Get_Fields("data_type"));
		//	}
		//	return GetDataType;
		//}

		public static double Round(double dValue, short iDigits)
		{
			double Round = 0;
			// Round = Int(dValue * (10 ^ iDigits) + 0.5) / (10 ^ iDigits)
			Round = Conversion.Int(FCConvert.ToDouble(dValue * (Math.Pow(10, iDigits)) + 0.5)) / (Math.Pow(10, iDigits));
			return Round;
		}
		// Private Sub AlterBookPageFields()
		// Dim boolBook As Boolean
		// Dim boolPage As Boolean
		// Dim boolBookDischargeNeededArchive As Boolean
		// Dim boolPageDischargeNeededArchive As Boolean
		// Dim boolBookLien As Boolean
		// Dim boolPageLien As Boolean
		// Dim strSQL As String
		// Dim rsSave As New clsDRWrapper
		//
		// If GetDataType("DischargeNeeded", "Book", strCLDatabase) = "int" Then
		// boolBook = True
		// End If
		// If GetDataType("DischargeNeeded", "Page", strCLDatabase) = "int" Then
		// boolPage = True
		// End If
		// If GetDataType("DischargeNeededArchive", "Book", strCLDatabase) = "int" Then
		// boolBookDischargeNeededArchive = True
		// End If
		// If GetDataType("DischargeNeededArchive", "Page", strCLDatabase) = "int" Then
		// boolPageDischargeNeededArchive = True
		// End If
		// If GetDataType("LienRec", "Book", strCLDatabase) = "int" Then
		// boolBookLien = True
		// End If
		// If GetDataType("LienRec", "Page", strCLDatabase) = "int" Then
		// boolPageLien = True
		// End If
		//
		// If boolBook Then
		// strSQL = "Alter table DischargeNeeded alter column Book varchar(255) null"
		// Call rsSave.Execute(strSQL, strCLDatabase)
		// End If
		// If boolPage Then
		// strSQL = "Alter table DischargeNeeded alter column Page varchar(255) null"
		// Call rsSave.Execute(strSQL, strCLDatabase)
		// End If
		// If boolBookDischargeNeededArchive Then
		// strSQL = "Alter table DischargeNeededArchive alter column Book varchar(255) null"
		// Call rsSave.Execute(strSQL, strCLDatabase)
		// End If
		// If boolPageDischargeNeededArchive Then
		// strSQL = "Alter table DischargeNeededArchive alter column Page varchar(255) null"
		// Call rsSave.Execute(strSQL, strCLDatabase)
		// End If
		// If boolBookLien Then
		// strSQL = "Alter table LienRec alter column Book varchar(255) null"
		// Call rsSave.Execute(strSQL, strCLDatabase)
		// End If
		// If boolPageLien Then
		// strSQL = "Alter table LienRec alter column Page varchar(255) null"
		// Call rsSave.Execute(strSQL, strCLDatabase)
		// End If
		// End Sub
		// These Declares are used to get the block cursor
		//[DllImport("user32")]
		//public static extern int CreateCaret(int hwnd, int hBitmap, int nWidth, int nHeight);

		//[DllImport("user32")]
		//public static extern int ShowCaret(int hwnd);

		//[DllImport("user32")]
		//public static extern int GetFocus();

		//[DllImport("user32")]
		//public static extern object SetCursorPos(int x, int y);

		//[DllImport("user32")]
		//public static extern int GetForegroundWindow();

		public const int WM_COMMAND = 0x111;
		// File Editting copying, moving, deleting
		// screen setup items
		const string Logo = "TRIO Software Corporation";
		public const string SearchInputBoxTitle = "Real Estate Assessment Search";

		//[DllImport("kernel32")]
		//public static extern void Sleep(int dwMilliseconds);

		//[DllImport("kernel32")]
		//public static extern int WinExec(string lpCmdLine, int nCmdShow);

		//[DllImport("kernel32")]
		//public static extern int GlobalAlloc(int wFlags, int dwBytes);

		//public struct ControlProportions
		//{
		//	public float WidthProportions;
		//	public float HeightProportions;
		//	public float TopProportions;
		//	public float LeftProportions;
		//};

		//public static void GetPaths()
		//{
		//	Statics.PPDatabase = "P:\\";
		//	Statics.TrioEXE = "\\T2K\\";
		//	Statics.TrioDATA = "\\VBTRIO\\VBTEST\\";
		//	Statics.PictureFormat = ".BMP";
		//	Statics.SketchLocation = "\\VBTRIO\\VBTEST\\";
		//	Statics.Display = "TRIOTEMP.TXT";
		//}

		//public static void G9996_LOCK()
		//{
		//	if (Statics.HighLockNumber == 0)
		//		Statics.HighLockNumber = Statics.LowLockNumber;
		//	FCFileSystem.Lock(Statics.LockFileNumber, Statics.LowLockNumber, Statics.HighLockNumber);
		//	if (Statics.Bbort != "Y")
		//	{
		//		Statics.LocksHeld[Statics.LockFileNumber] += 1;
		//		Statics.LowLock[Statics.LockFileNumber, Statics.LocksHeld[Statics.LockFileNumber]] = Statics.LowLockNumber;
		//		Statics.HighLock[Statics.LockFileNumber, Statics.LocksHeld[Statics.LockFileNumber]] = Statics.HighLockNumber;
		//	}
		//	Statics.HighLockNumber = 0;
		//}

		//public static void G9997_UNLOCK()
		//{
		//	int x;
		//	int y/*unused?*/;
		//	if (Statics.LocksHeld[Statics.LockFileNumber] != 0)
		//	{
		//		for (x = Statics.LocksHeld[Statics.LockFileNumber]; x >= 1; x--)
		//		{
		//			if (Statics.LowLock[Statics.LockFileNumber, x] != 0)
		//			{
		//				FCFileSystem.Unlock(Statics.LockFileNumber, Statics.LowLock[Statics.LockFileNumber, x], Statics.HighLock[Statics.LockFileNumber, x]);
		//				Statics.LowLock[Statics.LockFileNumber, x] = 0;
		//				Statics.HighLock[Statics.LockFileNumber, x] = 0;
		//			}
		//		}
		//		// x
		//		Statics.LocksHeld[Statics.LockFileNumber] = 0;
		//	}
		//}

		//public static void G9998_ABORT()
		//{
		//	int x;
		//	int y;
		//	/*? On Error Resume Next  */// to be removed later
		//	for (x = 1; x <= 50; x++)
		//	{
		//		if (Statics.LocksHeld[x] > 0)
		//		{
		//			for (y = Statics.LocksHeld[x]; y >= 1; y--)
		//			{
		//				if (Statics.LowLock[x, y] != 0)
		//				{
		//					FCFileSystem.Unlock(x, Statics.LowLock[x, y], Statics.HighLock[x, y]);
		//					Statics.LowLock[x, y] = 0;
		//					Statics.HighLock[x, y] = 0;
		//				}
		//			}
		//			// y
		//		}
		//		Statics.LocksHeld[x] = 0;
		//	}
		//	// x
		//}

		//public static void ConvertToString()
		//{
		//	Statics.IntVarTemp = Conversion.Str(Statics.IntvarNumeric);
		//	Statics.INTVARSTRING = "";
		//	Statics.IntVarTemp = Strings.LTrim(Statics.IntVarTemp);
		//	Statics.INTVARSTRING = Strings.StrDup(Statics.intLen - Statics.IntVarTemp.Length, "0") + Statics.IntVarTemp;
		//}

		//public static void ConvertEdited(ref string newfld)
		//{
		//	int x;
		//	int y;
		//	for (x = 1; x <= newfld.Length; x++)
		//	{
		//		if (Strings.Mid(newfld, x, 1) == "," || Strings.Mid(newfld, x, 1) == "." || Strings.Mid(newfld, x, 1) == "$")
		//		{
		//			for (y = x; y >= 2; y--)
		//			{
		//				Strings.MidSet(ref newfld, y, 1, Strings.Mid(newfld, y - 1, 1));
		//			}
		//			// y
		//			Strings.MidSet(ref newfld, 1, 1, " ");
		//		}
		//	}
		//	// x
		//	newfld = Strings.Trim(newfld);
		//	// IntLen = 9
		//	Statics.IntvarNumeric = Conversion.Val(newfld);
		//	ConvertToString();
		//	if (Statics.Bbort == "Y")
		//		return;
		//}

		//public static void MakeCaption()
		//{
		//	Statics.TitleCaption = "TRIO Software Corporation";
		//	Statics.TitleTime = Strings.Format(DateAndTime.TimeOfDay, "h:mm");
		//	if (Strings.Mid(Statics.TitleTime, 1, 1) == "0")
		//		Strings.MidSet(ref Statics.TitleTime, 1, 1, " ");
		//}

		//public static void PrintFormat()
		//{
		//	int fmtextraspc;
		//	// VBto upgrade warning: Formatted As Variant --> As string
		//	string Formatted = "";
		//	if (Statics.fmttype == "Whole")
		//	{
		//		Formatted = Strings.Format(Statics.fmtval, "###0");
		//	}
		//	else if (Statics.fmttype == "WholeC")
		//	{
		//		Formatted = Strings.Format(Statics.fmtval, "#,##0");
		//	}
		//	else if (Statics.fmttype == "Dollar")
		//	{
		//		Formatted = Strings.Format(Statics.fmtval / 100.0, "0.00");
		//	}
		//	else if (Statics.fmttype == "DollarC")
		//	{
		//		Formatted = Strings.Format(Statics.fmtval / 100.0, "#,##0.00");
		//	}
		//	else if (Statics.fmttype == "WholeS")
		//	{
		//		if (Statics.fmtval < 0)
		//		{
		//			Formatted = Strings.Format((Statics.fmtval * -1) / 100.0, "###0-");
		//		}
		//		else
		//		{
		//			Formatted = Strings.Format(Statics.fmtval / 100.0, "###0+");
		//		}
		//	}
		//	else if (Statics.fmttype == "WholeSC")
		//	{
		//		if (Statics.fmtval < 0)
		//		{
		//			Formatted = Strings.Format((Statics.fmtval * -1) / 100.0, "#,##0-");
		//		}
		//		else
		//		{
		//			Formatted = Strings.Format(Statics.fmtval / 100.0, "#,##0+");
		//		}
		//	}
		//	else if (Statics.fmttype == "DollarS")
		//	{
		//		if (Statics.fmtval < 0)
		//		{
		//			Formatted = Strings.Format((Statics.fmtval * -1) / 100.0, "0.00-");
		//		}
		//		else
		//		{
		//			Formatted = Strings.Format(Statics.fmtval / 100.0, "0.00+");
		//		}
		//	}
		//	else if (Statics.fmttype == "DollarSC")
		//	{
		//		if (Statics.fmtval < 0)
		//		{
		//			Formatted = Strings.Format((Statics.fmtval * -1) / 100.0, "#,##0.00-");
		//		}
		//		else
		//		{
		//			Formatted = Strings.Format(Statics.fmtval / 100.0, "#,##0.00+");
		//		}
		//	}
		//	fmtextraspc = 0;
		//	if (FCConvert.ToString(Formatted).Length < Statics.fmtlgth)
		//		fmtextraspc = Statics.fmtlgth - FCConvert.ToString(Formatted).Length;
		//	if (fmtextraspc != 0)
		//		FCFileSystem.Print(40, FCFileSystem.Spc(fmtextraspc));
		//	if (Statics.fmtspcbef != 0)
		//		FCFileSystem.Print(40, FCFileSystem.Spc(Statics.fmtspcbef));
		//	FCFileSystem.Print(40, Formatted);
		//	if (Statics.fmtspcaft != 0)
		//		FCFileSystem.Print(40, FCFileSystem.Spc(Statics.fmtspcaft));
		//	if (Statics.fmtcolon != "Y")
		//		FCFileSystem.PrintLine(40, " ");
		//}

		//public static void CheckNumeric()
		//{
		//	for (Statics.x = 1; Statics.x <= FCConvert.ToString(Statics.Resp1).Length; Statics.x++)
		//	{
		//		Statics.Resp2 = Convert.ToByte(Strings.Mid(FCConvert.ToString(Statics.Resp1), Statics.x, 1)[0]);
		//		if (Statics.Resp2 < 48 || Statics.Resp2 > 57)
		//		{
		//			string stringResp1 = FCConvert.ToString(Statics.Resp1);
		//			Strings.MidSet(ref stringResp1, Statics.x, Strings.Len(Statics.Resp1) - Statics.x, Strings.Mid(FCConvert.ToString(Statics.Resp1), Statics.x + 1, FCConvert.ToString(Statics.Resp1).Length - 1));
		//			Strings.MidSet(ref stringResp1, Strings.Len(Statics.Resp1), 1, " ");
		//			break;
		//		}
		//	}
		//	// x
		//}

		public static string PadToString(int intValue, int intLength)
		{
            if (intValue == null || intLength == null || intLength<= 0) Console.WriteLine($"bad call to PadToString: {intValue}{intLength}");
			string PadToString = "";
			// converts an integer to string
			// this routine is faster than ConvertToString
			// to use this function pass it a value and then length of string
			// Example:  XYZStr = PadToString(XYZInt, 6)
			// if XYZInt is equal to 123 then XYZStr is equal to "000123"
			// 
			if (FCUtils.IsNull(intValue) == true)
				intValue = 0;
			if (FCConvert.ToInt32(intValue) >= 0)
            {
                var targetLength = intLength - FCConvert.ToString(intValue).Length;

                if (targetLength > 0)
				{
					PadToString = Strings.StrDup(targetLength, "0") + FCConvert.ToString(intValue);
				}
				else
				{
					PadToString = intValue.ToString();
				}
            }
			else
			{
				// VBto upgrade warning: intTemp As Variant, short --> As int	OnWriteFCConvert.ToInt16(
				int intTemp = 0;
				intTemp = -1 * FCConvert.ToInt32(intValue);
				string strReturn = "";
				if (intLength - FCConvert.ToString(intTemp).Length > 0)
				{
					strReturn = Strings.StrDup(intLength - FCConvert.ToString(intTemp).Length, "0") + FCConvert.ToString(intTemp);
				}
				else
				{
					strReturn = intTemp.ToString();
				}
				if (Strings.Left(strReturn, 1) == "0")
				{
					Strings.MidSet(ref strReturn, 1, "-");
				}
				else
				{
					strReturn = "-" + strReturn;
				}
				PadToString = strReturn;
			}
			return PadToString;
		}

		//public static object SetInsertMode(ref short InsertMode)
		//{
		//	object SetInsertMode = null;
		//	// 0 = Insert Mode        = SelLength of 0
		//	// 1 = OverType Mode      = SelLength of 1
		//	// The Variable "OverType" returns either 0 or 1 for SelLength
		//	// 
		//	if ((FCGlobal.Screen.ActiveControl is TextBoxBase) && (FCGlobal.Screen.ActiveControl as TextBoxBase).SelectionLength == 1)
		//	{
		//		Statics.OverType = 0;
		//	}
		//	else
		//	{
		//		Statics.OverType = 1;
		//	}
		//	return SetInsertMode;
		//}

		//public static void GetValue()
		//{
		//	int y;
		//	int yy;
		//	Statics.NewData = Strings.Trim(Statics.NewData);
		//	for (y = 1; y <= Statics.NewData.Length; y++)
		//	{
		//		if (Strings.Mid(Statics.NewData, y, 1) == "," || Strings.Mid(Statics.NewData, y, 1) == ".")
		//		{
		//			for (yy = y; yy <= Statics.NewData.Length - 1; yy++)
		//			{
		//				Strings.MidSet(ref Statics.NewData, yy, 1, Strings.Mid(Statics.NewData, yy + 1, 1));
		//			}
		//			// yy
		//			Strings.MidSet(ref Statics.NewData, Strings.Len(Statics.NewData), 1, " ");
		//		}
		//		else if (Strings.UCase(Strings.Mid(Statics.NewData, y, 1)) == "C")
		//		{
		//			Statics.NewData = "0";
		//			break;
		//		}
		//	}
		//	// y
		//	Statics.NewValue = FCConvert.ToInt32(Math.Round(Conversion.Val(Statics.NewData)));
		//}

		//public static string Quotes(ref string strValue)
		//{
		//	string Quotes = "";
		//	Quotes = FCConvert.ToString(Convert.ToChar(34)) + strValue + FCConvert.ToString(Convert.ToChar(34));
		//	return Quotes;
		//}

		//public static void LockRecord(ref int lngLowNumber, ref int lngHighNumber, ref short intFileNumber)
		//{
		//	/*? On Error Resume Next  */
		//	if (lngHighNumber == 0)
		//		lngHighNumber = lngLowNumber;
		//	FCFileSystem.Lock(intFileNumber, lngLowNumber, lngHighNumber);
		//	Statics.LocksHeld[intFileNumber] += 1;
		//	Statics.LowLock[intFileNumber, Statics.LocksHeld[intFileNumber]] = lngLowNumber;
		//	Statics.HighLock[intFileNumber, Statics.LocksHeld[intFileNumber]] = lngHighNumber;
		//	Information.Err().Clear();
		//}

		//public static void UnLockRecord(ref int lngLowNumber, ref int lngHighNumber, ref short intFileNumber)
		//{
		//	/*? On Error Resume Next  */
		//	int x;
		//	int y/*unused?*/;
		//	if (Statics.LocksHeld[intFileNumber] != 0)
		//	{
		//		for (x = Statics.LocksHeld[intFileNumber]; x >= 1; x--)
		//		{
		//			if (Statics.LowLock[intFileNumber, x] != 0)
		//			{
		//				FCFileSystem.Unlock(intFileNumber, Statics.LowLock[intFileNumber, x], Statics.HighLock[(intFileNumber), x]);
		//				Statics.LowLock[intFileNumber, x] = 0;
		//				Statics.HighLock[intFileNumber, x] = 0;
		//			}
		//		}
		//		// x
		//		Statics.LocksHeld[intFileNumber] = 0;
		//	}
		//	Information.Err().Clear();
		//}

		//public static void UnlockALLRecords()
		//{
		//	/*? On Error Resume Next  */
		//	int x;
		//	int y;
		//	for (x = 1; x <= 50; x++)
		//	{
		//		if (Statics.LocksHeld[x] > 0)
		//		{
		//			for (y = Statics.LocksHeld[x]; y >= 1; y--)
		//			{
		//				if (Statics.LowLock[x, y] != 0)
		//				{
		//					FCFileSystem.Unlock(x, Statics.LowLock[x, y], Statics.HighLock[(x), y]);
		//					Statics.LowLock[x, y] = 0;
		//					Statics.HighLock[x, y] = 0;
		//				}
		//			}
		//			// y
		//		}
		//		Statics.LocksHeld[x] = 0;
		//	}
		//	// x
		//	Information.Err().Clear();
		//}

		//public static bool ExitTrioNow()
		//{
		//	bool ExitTrioNow = false;
		//	// VBto upgrade warning: Ans As Variant --> As 
		//	DialogResult Ans;
		//	// msgbox '260' = vbyesno with the 'NO' the default button.
		//	Ans = FCMessageBox.Show("Would you like to exit TRIO?", MsgBoxStyle.OkCancel | MsgBoxStyle.Question, "Exit TRIO");
		//	if (Ans == DialogResult.Yes)
		//	{
		//		ExitTrioNow = true;
		//		FCFileSystem.FileClose();
		//		Wisej.Web.Application.Exit();
		//	}
		//	else
		//	{
		//		ExitTrioNow = false;
		//	}
		//	return ExitTrioNow;
		//}

		//public static void SaveWindowSize(ref Form frm)
		//{
		//	Interaction.SaveSetting("TWGNENTY", "GENERAL_SETTINGS", "MainLeft", FCConvert.ToString(frm.Left));
		//	Interaction.SaveSetting("TWGNENTY", "GENERAL_SETTINGS", "MainTop", FCConvert.ToString(frm.Top));
		//	Interaction.SaveSetting("TWGNENTY", "GENERAL_SETTINGS", "MainWidth", FCConvert.ToString(frm.Width));
		//	Interaction.SaveSetting("TWGNENTY", "GENERAL_SETTINGS", "MainHeight", FCConvert.ToString(frm.Height));
		//}

		//public static void GetWindowSize(ref Form frm)
		//{
		//	int lngHeight;
		//	int lngWidth;
		//	int lngTop;
		//	int lngLeft;
		//	// 
		//	lngHeight = FCGlobal.Screen.Height;
		//	lngLeft = 0;
		//	lngTop = 0;
		//	lngWidth = FCGlobal.Screen.Width;
		//	// lngHeight = GetSetting("TWGNENTY", "GENERAL_SETTINGS", "MainHeight", 6500)
		//	// lngLeft = GetSetting("TWGNENTY", "GENERAL_SETTINGS", "MainLeft", 1000)
		//	// lngTop = GetSetting("TWGNENTY", "GENERAL_SETTINGS", "MainTop", 1000)
		//	// lngWidth = GetSetting("TWGNENTY", "GENERAL_SETTINGS", "MainWidth", 6500)
		//	if (lngHeight != frm.Height)
		//	{
		//		if (lngWidth != frm.Width)
		//		{
		//			if (frm.WindowState != FormWindowState.Normal)
		//			{
		//				frm.WindowState = FormWindowState.Normal;
		//			}
		//			frm.Height = lngHeight;
		//			frm.Left = lngLeft;
		//			frm.Top = lngTop;
		//			frm.Width = lngWidth;
		//			if (FCGlobal.Screen.Height - frm.Height <= 100)
		//			{
		//				if (FCGlobal.Screen.Width - frm.Width <= 100)
		//				{
		//					frm.WindowState = FormWindowState.Maximized;
		//				}
		//			}
		//		}
		//	}
		//}

		//public static void CloseChildForms()
		//{
		//	foreach (Form frm in FCGlobal.Statics.Forms)
		//	{
		//		if (frm.Name != "MDIParent")
		//		{
		//			if (!Statics.boolLoadHide)
		//				frm.Close();
		//		}
		//	}
		//}

		public class StaticVariables
		{
            public bool moduleInitialized = false;
			public int gintBasis;
			// this is a value of either 360 or 365 depending on how the department calculates interest
			public bool boolSubReport;
			public string gstrLastYearBilled = "";
			public bool gboolUseSigFile;

			public double gdblCMFAdjustment;
			public double gdblLabelsAdjustment;
			public double gdblLabelsAdjustmentDMH;
			public double gdblLabelsAdjustmentDMV;
			public double gdblLienAdjustmentTop;
			public double gdblLienAdjustmentBottom;
			// kk04212014 trocl-1156 Allow user to adjust bottom margin
			public double gdblLienAdjustmentSide;
			public double gdblLDNAdjustmentTop;
			public double gdblLDNAdjustmentBottom;
			// kk04212014 trocl-1156 Allow user to adjust bottom margin
			public double gdblLDNAdjustmentSide;
			// kk02082016 trout-1197 Allow user to adjust side margins
			public double gdblReminderFormAdjustH;
			public double gdblReminderFormAdjustV;
			// Tracker Reference: 14279
			// MAL@20080616: Add new adjustments for signature line
			public double gdblLienSigAdjust;
			public int glngCurrentCloseOut;
			public bool gboolShowLastCLAccountInCR;
			public bool gboolUseMailDateForMaturity;
			public int gintPassQuestion;
			
            public bool gboolSLDateRange;

			public bool gboolLeaveProject;
			public bool gboolMultipleTowns;
			
			public double[,] arrTAPrincipal = new double[1000 + 1, 10 + 1];
			// These are for the Tax Acquired Listing
			public double[,] arrTALienPrincipal = new double[1000 + 1, 10 + 1];
			
			public string gstrBillYrQuery = "";
			//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
			//public cPartyController gCPCtlr = new cPartyController();
			public cPartyController gCPCtlr_AutoInitialized;

			public cPartyController gCPCtlr
			{
				get
				{
					if (this.gCPCtlr_AutoInitialized == null)
					{
						this.gCPCtlr_AutoInitialized = new cPartyController();
					}
					return this.gCPCtlr_AutoInitialized;
				}
				set
				{
					this.gCPCtlr_AutoInitialized = value;
				}
			}
			// kgk 02-27-2012  Add full db paths for joins
			// kk01282015 trocls-51  Add automatic posting of journals
			public string strDbRE = string.Empty;
			public string strDbPP = string.Empty;
			public string strDbUT = string.Empty;
			public string strDbGNC = string.Empty;
			public string strDbGNS = string.Empty;
			public string strDbCP = string.Empty;
	
			public AccountFeesAdded[] arrDemand = null;
			//FC:FINAL:AM
			public cSettingsController setCont = new cSettingsController();
            // VBto upgrade warning: gobjFormName As object	OnRead(Form)
			public Form gobjFormName;
			// VBto upgrade warning: gobjLabel As object --> As Label
			public FCLabel gobjLabel;
			
        }

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
