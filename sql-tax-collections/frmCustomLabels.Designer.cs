﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmCustomLabels.
	/// </summary>
	partial class frmCustomLabels : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> chkFormPrintOptions;
		public fecherFoundation.FCTextBox txtExtraText;
		public fecherFoundation.FCFrame fraWhere;
		public fecherFoundation.FCGrid vsWhere;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCDraggableListBox lstSort;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCFrame fraFields;
		public fecherFoundation.FCCheckBox chkFormPrintOptions_3;
		public fecherFoundation.FCCheckBox chkFormPrintOptions_0;
		public fecherFoundation.FCCheckBox chkFormPrintOptions_1;
		public fecherFoundation.FCCheckBox chkFormPrintOptions_2;
		public fecherFoundation.FCDraggableListBox lstFields;
		public fecherFoundation.FCLabel lblFormPrint;
		public fecherFoundation.FCButton cmdExit;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCComboBox cmbFormType;
		public fecherFoundation.FCGrid vsLayout;
		public fecherFoundation.FCFrame fraType;
		public fecherFoundation.FCCheckBox chkCondensed;
		public fecherFoundation.FCComboBox cmbLabelType;
		public fecherFoundation.FCLabel lblDescription;
		public fecherFoundation.FCFrame fraMessage;
		public fecherFoundation.FCLabel Label3;
		public Wisej.Web.ImageList ImageList1;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomLabels));
            this.txtExtraText = new fecherFoundation.FCTextBox();
            this.fraWhere = new fecherFoundation.FCFrame();
            this.vsWhere = new fecherFoundation.FCGrid();
            this.cmdClear = new fecherFoundation.FCButton();
            this.lstSort = new fecherFoundation.FCDraggableListBox();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.fraFields = new fecherFoundation.FCFrame();
            this.chkFormPrintOptions_3 = new fecherFoundation.FCCheckBox();
            this.chkFormPrintOptions_0 = new fecherFoundation.FCCheckBox();
            this.chkFormPrintOptions_1 = new fecherFoundation.FCCheckBox();
            this.chkFormPrintOptions_2 = new fecherFoundation.FCCheckBox();
            this.lstFields = new fecherFoundation.FCDraggableListBox();
            this.lblFormPrint = new fecherFoundation.FCLabel();
            this.cmdExit = new fecherFoundation.FCButton();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.cmbFormType = new fecherFoundation.FCComboBox();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.vsLayout = new fecherFoundation.FCGrid();
            this.fraType = new fecherFoundation.FCFrame();
            this.chkCondensed = new fecherFoundation.FCCheckBox();
            this.cmbLabelType = new fecherFoundation.FCComboBox();
            this.lblDescription = new fecherFoundation.FCLabel();
            this.fraMessage = new fecherFoundation.FCFrame();
            this.Label3 = new fecherFoundation.FCLabel();
            this.ImageList1 = new Wisej.Web.ImageList(this.components);
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.fcLabel2 = new fecherFoundation.FCLabel();
            this.fcLabel3 = new fecherFoundation.FCLabel();
            this.btnProcess = new fecherFoundation.FCButton();
            this.cmdClear1 = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraWhere)).BeginInit();
            this.fraWhere.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsWhere)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraFields)).BeginInit();
            this.fraFields.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkFormPrintOptions_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFormPrintOptions_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFormPrintOptions_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFormPrintOptions_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsLayout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraType)).BeginInit();
            this.fraType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCondensed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMessage)).BeginInit();
            this.fraMessage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear1)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 678);
            this.BottomPanel.Size = new System.Drawing.Size(941, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraFields);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.fcLabel3);
            this.ClientArea.Controls.Add(this.txtExtraText);
            this.ClientArea.Controls.Add(this.fcLabel2);
            this.ClientArea.Controls.Add(this.lstSort);
            this.ClientArea.Controls.Add(this.cmdExit);
            this.ClientArea.Controls.Add(this.cmdPrint);
            this.ClientArea.Controls.Add(this.fraWhere);
            this.ClientArea.Size = new System.Drawing.Size(961, 682);
            this.ClientArea.Controls.SetChildIndex(this.fraWhere, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdPrint, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdExit, 0);
            this.ClientArea.Controls.SetChildIndex(this.lstSort, 0);
            this.ClientArea.Controls.SetChildIndex(this.fcLabel2, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtExtraText, 0);
            this.ClientArea.Controls.SetChildIndex(this.fcLabel3, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraFields, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdClear1);
            this.TopPanel.Size = new System.Drawing.Size(961, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdClear1, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(75, 28);
            this.HeaderText.Text = "Labels";
            // 
            // txtExtraText
            // 
            this.txtExtraText.BackColor = System.Drawing.SystemColors.Window;
            this.txtExtraText.Location = new System.Drawing.Point(175, 412);
            this.txtExtraText.MaxLength = 255;
            this.txtExtraText.Name = "txtExtraText";
            this.txtExtraText.Size = new System.Drawing.Size(379, 40);
            this.txtExtraText.TabIndex = 7;
            // 
            // fraWhere
            // 
            this.fraWhere.AppearanceKey = "groupBoxNoBorders";
            this.fraWhere.Controls.Add(this.vsWhere);
            this.fraWhere.Controls.Add(this.cmdClear);
            this.fraWhere.Location = new System.Drawing.Point(30, 475);
            this.fraWhere.Name = "fraWhere";
            this.fraWhere.Size = new System.Drawing.Size(878, 203);
            this.fraWhere.TabIndex = 8;
            this.fraWhere.Text = "Select Search Criteria";
            this.fraWhere.UseMnemonic = false;
            this.fraWhere.DoubleClick += new System.EventHandler(this.fraWhere_DoubleClick);
            // 
            // vsWhere
            // 
            this.vsWhere.Cols = 10;
            this.vsWhere.ColumnHeadersVisible = false;
            this.vsWhere.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsWhere.FixedRows = 0;
            this.vsWhere.Location = new System.Drawing.Point(0, 30);
            this.vsWhere.Name = "vsWhere";
            this.vsWhere.ReadOnly = false;
            this.vsWhere.Rows = 0;
            this.vsWhere.Size = new System.Drawing.Size(872, 120);
            this.vsWhere.TabIndex = 0;
            this.vsWhere.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsWhere_AfterEdit);
            this.vsWhere.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsWhere_BeforeEdit);
            this.vsWhere.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsWhere_ValidateEdit);
            this.vsWhere.CurrentCellChanged += new System.EventHandler(this.vsWhere_RowColChange);
            // 
            // cmdClear
            // 
            this.cmdClear.Location = new System.Drawing.Point(383, 160);
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Size = new System.Drawing.Size(94, 23);
            this.cmdClear.TabIndex = 1;
            this.cmdClear.Text = "Clear";
            this.cmdClear.Visible = false;
            this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
            // 
            // lstSort
            // 
            this.lstSort.BackColor = System.Drawing.SystemColors.Window;
            this.lstSort.CheckBoxes = true;
            this.lstSort.Location = new System.Drawing.Point(175, 257);
            this.lstSort.Name = "lstSort";
            this.lstSort.Size = new System.Drawing.Size(226, 135);
            this.lstSort.Style = 1;
            this.lstSort.TabIndex = 3;
            // 
            // cmdPrint
            // 
            this.cmdPrint.Location = new System.Drawing.Point(498, 458);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(73, 26);
            this.cmdPrint.TabIndex = 4;
            this.cmdPrint.Text = "Print";
            this.cmdPrint.Visible = false;
            this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
            // 
            // fraFields
            // 
            this.fraFields.Controls.Add(this.chkFormPrintOptions_3);
            this.fraFields.Controls.Add(this.chkFormPrintOptions_0);
            this.fraFields.Controls.Add(this.chkFormPrintOptions_1);
            this.fraFields.Controls.Add(this.chkFormPrintOptions_2);
            this.fraFields.Controls.Add(this.lstFields);
            this.fraFields.Controls.Add(this.lblFormPrint);
            this.fraFields.Location = new System.Drawing.Point(580, 247);
            this.fraFields.Name = "fraFields";
            this.fraFields.Size = new System.Drawing.Size(199, 192);
            this.fraFields.TabIndex = 1;
            this.fraFields.Text = "Print Specific";
            this.fraFields.UseMnemonic = false;
            this.fraFields.Visible = false;
            this.fraFields.DoubleClick += new System.EventHandler(this.fraFields_DoubleClick);
            // 
            // chkFormPrintOptions_3
            // 
            this.chkFormPrintOptions_3.Checked = true;
            this.chkFormPrintOptions_3.CheckState = Wisej.Web.CheckState.Checked;
            this.chkFormPrintOptions_3.Location = new System.Drawing.Point(18, 144);
            this.chkFormPrintOptions_3.Name = "chkFormPrintOptions_3";
            this.chkFormPrintOptions_3.Size = new System.Drawing.Size(124, 22);
            this.chkFormPrintOptions_3.TabIndex = 4;
            this.chkFormPrintOptions_3.Text = "Interested Party";
            // 
            // chkFormPrintOptions_0
            // 
            this.chkFormPrintOptions_0.Checked = true;
            this.chkFormPrintOptions_0.CheckState = Wisej.Web.CheckState.Checked;
            this.chkFormPrintOptions_0.Location = new System.Drawing.Point(18, 30);
            this.chkFormPrintOptions_0.Name = "chkFormPrintOptions_0";
            this.chkFormPrintOptions_0.Size = new System.Drawing.Size(70, 22);
            this.chkFormPrintOptions_0.TabIndex = 1;
            this.chkFormPrintOptions_0.Text = "Owner";
            // 
            // chkFormPrintOptions_1
            // 
            this.chkFormPrintOptions_1.Checked = true;
            this.chkFormPrintOptions_1.CheckState = Wisej.Web.CheckState.Checked;
            this.chkFormPrintOptions_1.Location = new System.Drawing.Point(18, 68);
            this.chkFormPrintOptions_1.Name = "chkFormPrintOptions_1";
            this.chkFormPrintOptions_1.Size = new System.Drawing.Size(129, 22);
            this.chkFormPrintOptions_1.TabIndex = 2;
            this.chkFormPrintOptions_1.Text = "Mortgage Holder";
            // 
            // chkFormPrintOptions_2
            // 
            this.chkFormPrintOptions_2.Checked = true;
            this.chkFormPrintOptions_2.CheckState = Wisej.Web.CheckState.Checked;
            this.chkFormPrintOptions_2.Location = new System.Drawing.Point(18, 106);
            this.chkFormPrintOptions_2.Name = "chkFormPrintOptions_2";
            this.chkFormPrintOptions_2.TabIndex = 3;
            this.chkFormPrintOptions_2.Text = "New Owner";
            // 
            // lstFields
            // 
            this.lstFields.BackColor = System.Drawing.SystemColors.Window;
            this.lstFields.Location = new System.Drawing.Point(20, 108);
            this.lstFields.Name = "lstFields";
            this.lstFields.Size = new System.Drawing.Size(169, 18);
            this.lstFields.TabIndex = 5;
            this.lstFields.Visible = false;
            this.lstFields.SelectedIndexChanged += new System.EventHandler(this.lstFields_SelectedIndexChanged);
            // 
            // lblFormPrint
            // 
            this.lblFormPrint.AutoSize = true;
            this.lblFormPrint.Location = new System.Drawing.Point(20, 30);
            this.lblFormPrint.Name = "lblFormPrint";
            this.lblFormPrint.Size = new System.Drawing.Size(43, 15);
            this.lblFormPrint.TabIndex = 6;
            this.lblFormPrint.Text = "PRINT";
            this.lblFormPrint.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblFormPrint.Visible = false;
            // 
            // cmdExit
            // 
            this.cmdExit.Location = new System.Drawing.Point(572, 458);
            this.cmdExit.Name = "cmdExit";
            this.cmdExit.Size = new System.Drawing.Size(73, 26);
            this.cmdExit.TabIndex = 5;
            this.cmdExit.Text = "Exit";
            this.cmdExit.Visible = false;
            this.cmdExit.Click += new System.EventHandler(this.cmdExit_Click);
            // 
            // Frame1
            // 
            this.Frame1.AppearanceKey = "groupBoxNoBorders";
            this.Frame1.Controls.Add(this.cmbFormType);
            this.Frame1.Controls.Add(this.fcLabel1);
            this.Frame1.Controls.Add(this.vsLayout);
            this.Frame1.Controls.Add(this.fraType);
            this.Frame1.Controls.Add(this.fraMessage);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(941, 245);
            this.Frame1.TabIndex = 1001;
            this.Frame1.UseMnemonic = false;
            // 
            // cmbFormType
            // 
            this.cmbFormType.BackColor = System.Drawing.SystemColors.Window;
            this.cmbFormType.Location = new System.Drawing.Point(175, 198);
            this.cmbFormType.Name = "cmbFormType";
            this.cmbFormType.Size = new System.Drawing.Size(379, 40);
            this.cmbFormType.TabIndex = 4;
            // 
            // fcLabel1
            // 
            this.fcLabel1.AutoSize = true;
            this.fcLabel1.Location = new System.Drawing.Point(30, 212);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(114, 15);
            this.fcLabel1.TabIndex = 3;
            this.fcLabel1.Text = "MAIL FORM TYPE";
            this.fcLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // vsLayout
            // 
            this.vsLayout.AllowUserToOrderColumns = true;
            this.vsLayout.Cols = 1;
            this.vsLayout.ColumnHeadersVisible = false;
            this.vsLayout.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExMove;
            this.vsLayout.FixedCols = 0;
            this.vsLayout.FixedRows = 0;
            this.vsLayout.Location = new System.Drawing.Point(30, 20);
            this.vsLayout.Name = "vsLayout";
            this.vsLayout.RowHeadersVisible = false;
            this.vsLayout.Rows = 4;
            this.vsLayout.Size = new System.Drawing.Size(524, 162);
            this.vsLayout.TabIndex = 5;
            this.vsLayout.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsLayout_AfterEdit);
            // 
            // fraType
            // 
            this.fraType.Controls.Add(this.chkCondensed);
            this.fraType.Controls.Add(this.cmbLabelType);
            this.fraType.Controls.Add(this.lblDescription);
            this.fraType.Location = new System.Drawing.Point(580, 12);
            this.fraType.Name = "fraType";
            this.fraType.Size = new System.Drawing.Size(333, 168);
            this.fraType.TabIndex = 2;
            this.fraType.Text = "Label Type";
            this.fraType.UseMnemonic = false;
            this.fraType.Visible = false;
            // 
            // chkCondensed
            // 
            this.chkCondensed.Location = new System.Drawing.Point(20, 128);
            this.chkCondensed.Name = "chkCondensed";
            this.chkCondensed.Size = new System.Drawing.Size(140, 22);
            this.chkCondensed.TabIndex = 2;
            this.chkCondensed.Text = "Condensed Labels";
            // 
            // cmbLabelType
            // 
            this.cmbLabelType.BackColor = System.Drawing.SystemColors.Window;
            this.cmbLabelType.Location = new System.Drawing.Point(20, 30);
            this.cmbLabelType.Name = "cmbLabelType";
            this.cmbLabelType.Size = new System.Drawing.Size(293, 40);
            this.cmbLabelType.TabIndex = 3;
            this.cmbLabelType.SelectedIndexChanged += new System.EventHandler(this.cmbLabelType_SelectedIndexChanged);
            // 
            // lblDescription
            // 
            this.lblDescription.Location = new System.Drawing.Point(20, 80);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(293, 42);
            this.lblDescription.TabIndex = 1;
            // 
            // fraMessage
            // 
            this.fraMessage.Controls.Add(this.Label3);
            this.fraMessage.Location = new System.Drawing.Point(30, 20);
            this.fraMessage.Name = "fraMessage";
            this.fraMessage.Size = new System.Drawing.Size(371, 88);
            this.fraMessage.TabIndex = 1;
            this.fraMessage.UseMnemonic = false;
            this.fraMessage.Visible = false;
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(30, 30);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(321, 39);
            this.Label3.TabIndex = 0;
            this.Label3.Text = "THIS IS A PRE-DEFINED REPORT MAKING SOME SECTIONS ON THIS FORM INACCESSIBLE";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // ImageList1
            // 
            this.ImageList1.ImageSize = new System.Drawing.Size(256, 18);
            this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(192, 192, 192);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // fcLabel2
            // 
            this.fcLabel2.AutoSize = true;
            this.fcLabel2.Location = new System.Drawing.Point(31, 271);
            this.fcLabel2.Name = "fcLabel2";
            this.fcLabel2.Size = new System.Drawing.Size(132, 15);
            this.fcLabel2.TabIndex = 2;
            this.fcLabel2.Text = "FIELDS TO SORT BY";
            this.fcLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fcLabel3
            // 
            this.fcLabel3.AutoSize = true;
            this.fcLabel3.Location = new System.Drawing.Point(31, 426);
            this.fcLabel3.Name = "fcLabel3";
            this.fcLabel3.Size = new System.Drawing.Size(84, 15);
            this.fcLabel3.TabIndex = 6;
            this.fcLabel3.Text = "EXTRA TEXT";
            this.fcLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Location = new System.Drawing.Point(413, 30);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcess.Size = new System.Drawing.Size(120, 48);
            this.btnProcess.TabIndex = 1;
            this.btnProcess.Text = "Process";
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // cmdClear1
            // 
            this.cmdClear1.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdClear1.Location = new System.Drawing.Point(787, 29);
            this.cmdClear1.Name = "cmdClear1";
            this.cmdClear1.Size = new System.Drawing.Size(145, 24);
            this.cmdClear1.TabIndex = 52;
            this.cmdClear1.Text = "Clear Search Criteria";
            this.cmdClear1.Click += new System.EventHandler(this.cmdSear_Click);
            // 
            // frmCustomLabels
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(961, 742);
            this.Cursor = Wisej.Web.Cursors.Default;
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmCustomLabels";
            this.Text = "Labels";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmCustomLabels_Load);
            this.Activated += new System.EventHandler(this.frmCustomLabels_Activated);
            this.Resize += new System.EventHandler(this.frmCustomLabels_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCustomLabels_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraWhere)).EndInit();
            this.fraWhere.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsWhere)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraFields)).EndInit();
            this.fraFields.ResumeLayout(false);
            this.fraFields.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkFormPrintOptions_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFormPrintOptions_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFormPrintOptions_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFormPrintOptions_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsLayout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraType)).EndInit();
            this.fraType.ResumeLayout(false);
            this.fraType.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCondensed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMessage)).EndInit();
            this.fraMessage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear1)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCLabel fcLabel1;
		private FCLabel fcLabel2;
		private FCLabel fcLabel3;
		private FCButton btnProcess;
		public FCButton cmdClear1;
	}
}
