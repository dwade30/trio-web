﻿using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmReprintPurgeList.
	/// </summary>
	public partial class frmReprintPurgeList : BaseForm
	{
		public frmReprintPurgeList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmReprintPurgeList InstancePtr
		{
			get
			{
				return (frmReprintPurgeList)Sys.GetInstance(typeof(frmReprintPurgeList));
			}
		}

		protected frmReprintPurgeList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               07/26/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               07/26/2004              *
		// ********************************************************
		private void File1_Click()
		{
		}

		private void frmReprintPurgeList_Activated(object sender, System.EventArgs e)
		{
			// lblDate.Text = "Select a report to view."
			// ShowFrame fraDate
			cmbREPP.Visible = true;
		}

		private void frmReprintPurgeList_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmReprintPurgeList.Icon	= "frmReprintPurgeList.frx":0000";
			//frmReprintPurgeList.FillStyle	= 0;
			//frmReprintPurgeList.ScaleWidth	= 3885;
			//frmReprintPurgeList.ScaleHeight	= 2520;
			//frmReprintPurgeList.LinkTopic	= "Form2";
			//frmReprintPurgeList.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "9.75";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//vsElasticLight1.OleObjectBlob	= "frmReprintPurgeList.frx":058A";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			this.Text = "Reprint Purge Report";
			if (modStatusPayments.Statics.boolRE)
			{
				cmbREPP.SelectedIndex = 0;
			}
			else
			{
				cmbREPP.SelectedIndex = 1;
			}
			// fileBox.Path = CurDir & "\RPT\"
			// fileBox.Pattern = "CLRP*.RDF"
		}

		private void frmReprintPurgeList_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				Close();
			}
			else if (KeyAscii == 13)
			{
				KeyAscii = 0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void frmReprintPurgeList_Resize(object sender, System.EventArgs e)
		{
			// ShowFrame fraDate
			cmbREPP.Visible = true;
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void ShowFrame(FCFrame fra)
		{
			string VBtoVar = fra.GetName();
			if (VBtoVar == "fraDate")
			{
				fra.Top = (cmbREPP.Top + cmbREPP.Height);
			}
			else if (VBtoVar == "fraREPP")
			{
				fra.Top = 0;
			}
			else
			{
			}
			fra.Left = FCConvert.ToInt32((this.Width - fra.Width) / 2.0);
			fra.Visible = true;
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			// this is where I will show the report in the viewer
			// If fileBox.FileName <> "" Then
			// frmRPTViewer.Init fileBox.FileName, "Reprint Purge List", 10
			// Else
			// MsgBox "Select a report.", MsgBoxStyle.Exclamation, "Report Name"
			// End If
			if (cmbREPP.SelectedIndex == 0)
			{
				if (File.Exists(TWSharedLibrary.Variables.Statics.ReportPath + "\\CLRP1.RDF"))
				{
					frmRPTViewer.InstancePtr.Init("CLRP1.RDF", "Reprint Purge List", 10);
				}
				else
				{
					FCMessageBox.Show("Last purge list report not found", MsgBoxStyle.Information, "File Not Found");
				}
			}
			else
			{
				if (File.Exists(TWSharedLibrary.Variables.Statics.ReportPath + "\\CLPP1.rdf"))
				{
					frmRPTViewer.InstancePtr.Init("CLPP1.RDF", "Reprint Purge List", 10);
				}
				else
				{
					FCMessageBox.Show("Last purge list report not found", MsgBoxStyle.Information, "File Not Found");
				}
			}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFilePrint_Click(sender, e);
		}
		// Private Sub optREPP_Click(Index As Integer)
		// If Index = 0 Then
		// fileBox.Pattern = "CLRP*.RDF"
		// Else
		// fileBox.Pattern = "CLPP*.RDF"
		// End If
		// fileBox.Refresh
		// fileBox.Path = CurDir & "\RPT\"
		// fileBox.Height = 300
		// End Sub
	}
}
