﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmUnifund.
	/// </summary>
	partial class frmUnifund : BaseForm
	{
		public fecherFoundation.FCComboBox cmbBillType;
		public fecherFoundation.FCComboBox cmbEnd;
		public fecherFoundation.FCComboBox cmbStart;
		public fecherFoundation.FCButton cmdBrowse;
		public fecherFoundation.FCTextBox txtFile;
		public fecherFoundation.FCLabel lblType;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblYear;
		public fecherFoundation.FCLabel lblAccount;
		public fecherFoundation.FCLabel Label1;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUnifund));
			this.cmbBillType = new fecherFoundation.FCComboBox();
			this.cmbEnd = new fecherFoundation.FCComboBox();
			this.cmbStart = new fecherFoundation.FCComboBox();
			this.cmdBrowse = new fecherFoundation.FCButton();
			this.txtFile = new fecherFoundation.FCTextBox();
			this.lblType = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.lblYear = new fecherFoundation.FCLabel();
			this.lblAccount = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.lblBillType = new fecherFoundation.FCLabel();
			this.lblYearRange = new fecherFoundation.FCLabel();
			this.btnProcess = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 302);
			this.BottomPanel.Size = new System.Drawing.Size(624, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.lblYearRange);
			this.ClientArea.Controls.Add(this.cmbEnd);
			this.ClientArea.Controls.Add(this.cmbStart);
			this.ClientArea.Controls.Add(this.lblBillType);
			this.ClientArea.Controls.Add(this.cmbBillType);
			this.ClientArea.Controls.Add(this.cmdBrowse);
			this.ClientArea.Controls.Add(this.txtFile);
			this.ClientArea.Controls.Add(this.lblType);
			this.ClientArea.Controls.Add(this.Label4);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.lblYear);
			this.ClientArea.Controls.Add(this.lblAccount);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(624, 242);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(624, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(175, 30);
			this.HeaderText.Text = "Unifund Export";
			// 
			// cmbBillType
			// 
			this.cmbBillType.BackColor = System.Drawing.SystemColors.Window;
			this.cmbBillType.Items.AddRange(new object[] {
            "Real Estate",
            "Personal Property"});
			this.cmbBillType.Location = new System.Drawing.Point(170, 126);
			this.cmbBillType.Name = "cmbBillType";
			this.cmbBillType.Size = new System.Drawing.Size(140, 40);
			this.cmbBillType.TabIndex = 10;
			// 
			// cmbEnd
			// 
			this.cmbEnd.BackColor = System.Drawing.SystemColors.Window;
			this.cmbEnd.Location = new System.Drawing.Point(329, 186);
			this.cmbEnd.Name = "cmbEnd";
			this.cmbEnd.Size = new System.Drawing.Size(140, 40);
			this.cmbEnd.TabIndex = 13;
			// 
			// cmbStart
			// 
			this.cmbStart.BackColor = System.Drawing.SystemColors.Window;
			this.cmbStart.Location = new System.Drawing.Point(170, 186);
			this.cmbStart.Name = "cmbStart";
			this.cmbStart.Size = new System.Drawing.Size(140, 40);
			this.cmbStart.TabIndex = 12;
			// 
			// cmdBrowse
			// 
			this.cmdBrowse.AppearanceKey = "actionButton";
			this.cmdBrowse.Location = new System.Drawing.Point(508, 66);
			this.cmdBrowse.Name = "cmdBrowse";
			this.cmdBrowse.Size = new System.Drawing.Size(89, 40);
			this.cmdBrowse.TabIndex = 8;
			this.cmdBrowse.Text = "Browse";
			this.cmdBrowse.Click += new System.EventHandler(this.cmdBrowse_Click);
			// 
			// txtFile
			// 
			this.txtFile.BackColor = System.Drawing.SystemColors.Window;
			this.txtFile.Location = new System.Drawing.Point(170, 66);
			this.txtFile.Name = "txtFile";
			this.txtFile.Size = new System.Drawing.Size(317, 40);
			this.txtFile.TabIndex = 7;
			// 
			// lblType
			// 
			this.lblType.Location = new System.Drawing.Point(448, 30);
			this.lblType.Name = "lblType";
			this.lblType.Size = new System.Drawing.Size(40, 21);
			this.lblType.TabIndex = 5;
			this.lblType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label4
			// 
			this.Label4.AutoSize = true;
			this.Label4.Location = new System.Drawing.Point(400, 30);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(40, 15);
			this.Label4.TabIndex = 4;
			this.Label4.Text = "TYPE";
			this.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label3
			// 
			this.Label3.AutoSize = true;
			this.Label3.Location = new System.Drawing.Point(280, 30);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(41, 15);
			this.Label3.TabIndex = 2;
			this.Label3.Text = "YEAR";
			this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label2
			// 
			this.Label2.AutoSize = true;
			this.Label2.Location = new System.Drawing.Point(30, 30);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(69, 15);
			this.Label2.TabIndex = 14;
			this.Label2.Text = "ACCOUNT";
			this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblYear
			// 
			this.lblYear.Location = new System.Drawing.Point(330, 30);
			this.lblYear.Name = "lblYear";
			this.lblYear.Size = new System.Drawing.Size(50, 21);
			this.lblYear.TabIndex = 3;
			this.lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblAccount
			// 
			this.lblAccount.Location = new System.Drawing.Point(135, 30);
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Size = new System.Drawing.Size(124, 21);
			this.lblAccount.TabIndex = 1;
			this.lblAccount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label1
			// 
			this.Label1.AutoSize = true;
			this.Label1.Location = new System.Drawing.Point(30, 72);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(69, 15);
			this.Label1.TabIndex = 6;
			this.Label1.Text = "FILENAME";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// lblBillType
			// 
			this.lblBillType.AutoSize = true;
			this.lblBillType.Location = new System.Drawing.Point(30, 132);
			this.lblBillType.Name = "lblBillType";
			this.lblBillType.Size = new System.Drawing.Size(68, 15);
			this.lblBillType.TabIndex = 9;
			this.lblBillType.Text = "BILL TYPE";
			this.lblBillType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblYearRange
			// 
			this.lblYearRange.AutoSize = true;
			this.lblYearRange.Location = new System.Drawing.Point(30, 192);
			this.lblYearRange.Name = "lblYearRange";
			this.lblYearRange.Size = new System.Drawing.Size(88, 15);
			this.lblYearRange.TabIndex = 11;
			this.lblYearRange.Text = "YEAR RANGE";
			this.lblYearRange.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(227, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(98, 48);
			this.btnProcess.TabIndex = 1;
			this.btnProcess.Text = "Export";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// frmUnifund
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(624, 410);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmUnifund";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Unifund Export";
			this.Load += new System.EventHandler(this.frmUnifund_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmUnifund_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private FCLabel lblBillType;
		private FCLabel lblYearRange;
		private FCButton btnProcess;
	}
}
