﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmReprintPartialPaymentWaiver.
	/// </summary>
	partial class frmReprintPartialPaymentWaiver : BaseForm
	{
		public fecherFoundation.FCComboBox cmbShow;
		public fecherFoundation.FCTextBox txtWitness;
		public fecherFoundation.FCGrid Grid;
		public fecherFoundation.FCLabel lblWitness;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReprintPartialPaymentWaiver));
			this.cmbShow = new fecherFoundation.FCComboBox();
			this.txtWitness = new fecherFoundation.FCTextBox();
			this.Grid = new fecherFoundation.FCGrid();
			this.lblWitness = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.lblShow = new fecherFoundation.FCLabel();
			this.btnProcess = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 502);
			this.BottomPanel.Size = new System.Drawing.Size(789, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.lblShow);
			this.ClientArea.Controls.Add(this.cmbShow);
			this.ClientArea.Controls.Add(this.txtWitness);
			this.ClientArea.Controls.Add(this.Grid);
			this.ClientArea.Controls.Add(this.lblWitness);
			this.ClientArea.Size = new System.Drawing.Size(789, 442);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(789, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(352, 35);
			this.HeaderText.Text = "Print Partial Payment Waivers";
			// 
			// cmbShow
			// 
			this.cmbShow.AutoSize = false;
			this.cmbShow.FormattingEnabled = true;
			this.cmbShow.Items.AddRange(new object[] {
				"Unprinted waivers",
				"All waivers"
			});
			this.cmbShow.Location = new System.Drawing.Point(98, 30);
			this.cmbShow.Name = "cmbShow";
			this.cmbShow.Size = new System.Drawing.Size(181, 40);
			this.cmbShow.TabIndex = 1;
			this.cmbShow.Text = "Unprinted waivers";
			this.cmbShow.SelectedIndexChanged += new System.EventHandler(this.optShow_Click);
			// 
			// txtWitness
			// 
			this.txtWitness.AutoSize = false;
			this.txtWitness.BackColor = System.Drawing.SystemColors.Window;
			this.txtWitness.Location = new System.Drawing.Point(395, 30);
			this.txtWitness.Name = "txtWitness";
			this.txtWitness.Size = new System.Drawing.Size(376, 40);
			this.txtWitness.TabIndex = 3;
			// 
			// Grid
			// 
			this.Grid.AllowSelection = false;
			this.Grid.AllowUserToResizeColumns = false;
			this.Grid.AllowUserToResizeRows = false;
			this.Grid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.Grid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.Grid.BackColorAlternate = System.Drawing.Color.Empty;
			this.Grid.BackColorBkg = System.Drawing.Color.Empty;
			this.Grid.BackColorFixed = System.Drawing.Color.Empty;
			this.Grid.BackColorSel = System.Drawing.Color.Empty;
			this.Grid.Cols = 8;
			this.Grid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle1.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.Grid.DefaultCellStyle = dataGridViewCellStyle1;
			this.Grid.ExtendLastCol = true;
			this.Grid.FixedCols = 0;
			this.Grid.ForeColorFixed = System.Drawing.Color.Empty;
			this.Grid.FrozenCols = 0;
			this.Grid.FrozenRows = 0;
			this.Grid.GridColor = System.Drawing.Color.Empty;
			this.Grid.Location = new System.Drawing.Point(30, 90);
			this.Grid.Name = "Grid";
			this.Grid.ReadOnly = true;
			this.Grid.RowHeadersVisible = false;
			this.Grid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.Grid.RowHeightMin = 0;
			this.Grid.Rows = 1;
			this.Grid.ShowColumnVisibilityMenu = false;
			this.Grid.Size = new System.Drawing.Size(741, 332);
			this.Grid.StandardTab = true;
			this.Grid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.Grid.TabIndex = 4;
			this.Grid.DoubleClick += new System.EventHandler(this.Grid_DblClick);
			// 
			// lblWitness
			// 
			this.lblWitness.AutoSize = true;
			this.lblWitness.Location = new System.Drawing.Point(313, 44);
			this.lblWitness.Name = "lblWitness";
			this.lblWitness.Size = new System.Drawing.Size(64, 16);
			this.lblWitness.TabIndex = 2;
			this.lblWitness.Text = "WITNESS";
			this.lblWitness.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.Text = "File";
			// 
			// lblShow
			// 
			this.lblShow.AutoSize = true;
			this.lblShow.Location = new System.Drawing.Point(30, 44);
			this.lblShow.Name = "lblShow";
			this.lblShow.Size = new System.Drawing.Size(46, 16);
			this.lblShow.TabIndex = 0;
			this.lblShow.Text = "SHOW";
			this.lblShow.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(258, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(257, 48);
			this.btnProcess.TabIndex = 1;
			this.btnProcess.Text = "Print all unprinted waivers";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// frmReprintPartialPaymentWaiver
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(789, 610);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmReprintPartialPaymentWaiver";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Print Partial Payment Waivers";
			this.Load += new System.EventHandler(this.frmReprintPartialPaymentWaiver_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmReprintPartialPaymentWaiver_KeyDown);
			this.Resize += new System.EventHandler(this.frmReprintPartialPaymentWaiver_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCLabel lblShow;
		private FCButton btnProcess;
	}
}
