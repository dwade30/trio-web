﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmTaxAcquiredList.
	/// </summary>
	public partial class frmTaxAcquiredList : BaseForm
	{
		public frmTaxAcquiredList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmTaxAcquiredList InstancePtr
		{
			get
			{
				return (frmTaxAcquiredList)Sys.GetInstance(typeof(frmTaxAcquiredList));
			}
		}

		protected frmTaxAcquiredList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/09/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/07/2006              *
		// ********************************************************
		int intAction;
		clsDRWrapper rsData = new clsDRWrapper();
		bool boolLoaded;
		double dblDemand;
		double dblCertMailFee;
		bool boolChargeMort;
		bool boolChargeCert;
		bool boolChargeForNewOwner;
		DateTime dtMailDate;
		clsDRWrapper rsRE = new clsDRWrapper();
		// VBto upgrade warning: lngBillingYear As int	OnWrite(string)
		int lngBillingYear;
		string strYearList = "";
		int lngValidateRowYear;
		int lngGridColTaxAcquired;
		int lngGridColAccount;
		int lngGridColName;
		int lngGridColLocation;
		int lngGridColOSTaxYear;
		int lngGridColOSTaxTotal;
		int lngGridColBillKey;

		private void frmTaxAcquiredList_Activated(object sender, System.EventArgs e)
		{
		}

		private void frmTaxAcquiredList_Load(object sender, System.EventArgs e)
		{
			lngValidateRowYear = 0;
			lngGridColTaxAcquired = 0;
			lngGridColBillKey = 1;
			lngGridColAccount = 2;
			lngGridColName = 3;
			lngGridColLocation = 4;
			lngGridColOSTaxYear = 5;
			lngGridColOSTaxTotal = 6;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			FCUtils.EraseSafe(modGlobal.Statics.arrDemand);
			FCUtils.EraseSafe(modGlobal.Statics.arrTALienPrincipal);
			FCUtils.EraseSafe(modGlobal.Statics.arrTAPrincipal);
			FormatGrid(true);
			if (fraGrid.Visible)
			{
				ShowGridFrame();
			}
			else
			{
				ShowValidateFrame();
			}
			//FC:FINAL:AM: moved code from Activate
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			this.Text = "Tax Acquired Properties";
			lblInstructionHeader.Visible = true;
			// False 'commented back in from a bug fix #4854
			lblInstruction.Visible = true;
			lblValidateInstruction.Visible = true;
			lblInstruction.Text = "To Set Account To Tax Aquired:" + "\r\n" + "     1. Enter the Tax Acquired date." + "\r\n" + "     2. Check the box beside the account." + "\r\n" + "     3. Select 'Save' or press F12.";
			lblValidateInstruction.Text = "Select a year.";
			lblValidateInstruction.Alignment = HorizontalAlignment.Center;
			App.DoEvents();
			FillBillingYearCombo();
			if (modGlobalConstants.Statics.gboolBD)
			{
				modValidateAccount.SetBDFormats();
			}
			ShowGridFrame();
			SetAct(0);
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
		}

		private void frmTaxAcquiredList_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				Close();
			}
			else if (KeyAscii == 13)
			{
				KeyAscii = 0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		//private void frmTaxAcquiredList_Resize(object sender, System.EventArgs e)
		//{
		//	FormatGrid(true);
		//	if (fraGrid.Visible)
		//	{
		//		ShowGridFrame();
		//	}
		//	else
		//	{
		//		ShowValidateFrame();
		//	}
		//}
		private void mnuFileClear_Click(object sender, System.EventArgs e)
		{
			ClearCheckBoxes();
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
			App.DoEvents();
			if (App.MainForm.Enabled && App.MainForm.Visible)
			{
				App.MainForm.Focus();
			}
		}

		private void ShowValidateFrame()
		{
			// this will show/center the frame with the grid on it
			fraValidate.Top = FCConvert.ToInt32((this.Height - fraValidate.Height) / 53.0);
			fraValidate.Left = FCConvert.ToInt32((this.Width - fraValidate.Width) / 42.0);
			fraValidate.Visible = true;
			// this will set the height of the grid
			//FC:FINAL:AM: don't set the height; used anchoring instead
			//if (vsValidate.Rows > 0)
			//{
			//	vsValidate.HeightOriginal = ((vsValidate.Rows) * vsValidate.RowHeight(0)) + 70;
			//}
		}

		private void ShowGridFrame()
		{
			// this will show/center the frame with the grid on it
			//fraGrid.Top = FCConvert.ToInt32((this.Height - fraGrid.Height) / 3.0);
			//fraGrid.Left = FCConvert.ToInt32((this.Width - fraGrid.Width) / 2.0);
			fraGrid.Visible = true;
			//if (vsDemand.Rows * vsDemand.RowHeight(0) > fraGrid.HeightOriginal - lblInstruction.HeightOriginal - 500)
			//{
			//	vsDemand.HeightOriginal = fraGrid.HeightOriginal - lblInstruction.HeightOriginal  - 500;
			//	vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			//}
			//else
			//{
			//	vsDemand.HeightOriginal = vsDemand.Rows * vsDemand.RowHeight(0) + 70;
			//	vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			//}
		}

		private void FormatGrid(bool boolResize = false)
		{
			int wid = 0;
			if (!boolResize)
			{
				vsDemand.Rows = 1;
			}
			vsDemand.Cols = 7;
			wid = vsDemand.WidthOriginal;
			vsDemand.ColWidth(lngGridColTaxAcquired, FCConvert.ToInt32(wid * 0.04));
			// checkbox
			vsDemand.ColWidth(lngGridColBillKey, 0);
			// BillKey
			vsDemand.ColWidth(lngGridColAccount, FCConvert.ToInt32(wid * 0.08));
			// Acct
			vsDemand.ColWidth(lngGridColName, FCConvert.ToInt32(wid * 0.45));
			// Account
			vsDemand.ColWidth(lngGridColLocation, FCConvert.ToInt32(wid * 0.2));
			// Location
			vsDemand.ColWidth(lngGridColOSTaxYear, FCConvert.ToInt32(wid * 0.1));
			vsDemand.ColWidth(lngGridColOSTaxTotal, FCConvert.ToInt32(wid * 0.1));
			vsDemand.ColFormat(lngGridColOSTaxYear, "#,##0.00");
			vsDemand.ColFormat(lngGridColOSTaxTotal, "#,##0.00");
			vsDemand.ColDataType(lngGridColTaxAcquired, FCGrid.DataTypeSettings.flexDTBoolean);
			vsDemand.ColAlignment(lngGridColAccount, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(lngGridColName, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(lngGridColLocation, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(lngGridColOSTaxYear, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsDemand.ColAlignment(lngGridColOSTaxTotal, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsDemand.TextMatrix(0, lngGridColTaxAcquired, "TA");
			vsDemand.TextMatrix(0, lngGridColAccount, "Acct");
			vsDemand.TextMatrix(0, lngGridColName, "Name");
			vsDemand.TextMatrix(0, lngGridColLocation, "Location");
			vsDemand.TextMatrix(0, lngGridColOSTaxTotal, "Total Due");
			//FC:FINAL:AM:#86 - set first the FixedCols property
		}

		private void FillDemandGrid()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the grid with eligible accounts
				clsDRWrapper rsRK = new clsDRWrapper();
				DateTime dtBillDate;
				string strSQL;
				int lngIndex;
				double dblXInt = 0;
				double dblBillAmount = 0;
				double dblTotalAmount = 0;
				clsDRWrapper rsLN = new clsDRWrapper();
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Info");
				vsDemand.Rows = 1;
				lngBillingYear = FCConvert.ToInt32(modExtraModules.FormatYear(cmbBillingYear.Text));
				vsDemand.TextMatrix(0, lngGridColOSTaxYear, cmbBillingYear.Text);
				strSQL = "SELECT * FROM BillingMaster WHERE BillingYear = " + FCConvert.ToString(lngBillingYear) + " AND BillingType = 'RE' ORDER BY Name1, Account";
				rsData.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
				rsRE.OpenRecordset("SELECT * FROM Master WHERE RSCard = 1 AND TaxAcquired = 0 AND InBankruptcy = 0 ORDER BY RSAccount", modExtraModules.strREDatabase);
				if (rsRE.EndOfFile())
				{
					frmWait.InstancePtr.Unload();
					FCMessageBox.Show("There are no accounts set to Tax Acquired.", MsgBoxStyle.Information, "No Eligible Accounts");
					Close();
				}
				lngIndex = -1;
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Accounts", true, rsRE.RecordCount(), true);
				while (!rsRE.EndOfFile())
				{
					frmWait.InstancePtr.IncrementProgress();
					App.DoEvents();
					rsData.FindFirstRecord2("BillingYear,Account", FCConvert.ToString(lngBillingYear) + "," + FCConvert.ToString(rsRE.Get_Fields_Int32("RSACCOUNT")), ",");
					if (rsData.NoMatch)
					{
						// skip this account because there are no eligible bills
						goto SKIP;
					}
					if (((rsData.Get_Fields_Int32("LienRecordNumber"))) != 0)
					{
						rsLN.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(rsData.Get_Fields_Int32("LienRecordNumber")), modExtraModules.strCLDatabase);
						if (!rsLN.EndOfFile())
						{
							dblBillAmount = modCLCalculations.CalculateAccountCLLien(rsLN, dtMailDate, ref dblXInt);
						}
						else
						{
							dblBillAmount = 0;
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						dblBillAmount = modCLCalculations.CalculateAccountCL(ref rsData, FCConvert.ToInt32(rsData.Get_Fields("Account")), dtMailDate, ref dblXInt);
					}
					if (dblBillAmount <= 0)
					{
						// skip this account because this account has already been paid
						goto SKIP;
					}
					// add a row/element
					vsDemand.AddItem("");
					Array.Resize(ref modGlobal.Statics.arrDemand, vsDemand.Rows - 1 + 1);
					// this will make sure that there are enough elements to use
					lngIndex = vsDemand.Rows - 2;
					//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
					modGlobal.Statics.arrDemand[lngIndex] = new modGlobal.AccountFeesAdded(0);
					modGlobal.Statics.arrDemand[lngIndex].Used = true;
					modGlobal.Statics.arrDemand[lngIndex].Processed = false;
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					vsDemand.TextMatrix(vsDemand.Rows - 1, lngGridColAccount, FCConvert.ToString(rsData.Get_Fields("Account")));
					// account number
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					modGlobal.Statics.arrDemand[lngIndex].Account = FCConvert.ToInt32(rsData.Get_Fields("Account"));
					vsDemand.TextMatrix(vsDemand.Rows - 1, lngGridColName, FCConvert.ToString(rsData.Get_Fields_String("Name1")));
					// name
					modGlobal.Statics.arrDemand[lngIndex].Name = FCConvert.ToString(rsData.Get_Fields_String("Name1"));
					vsDemand.TextMatrix(vsDemand.Rows - 1, lngGridColBillKey, FCConvert.ToString(rsData.Get_Fields_Int32("ID")));
					// billkey
					modGlobal.Statics.arrDemand[lngIndex].BillKey = FCConvert.ToInt32(rsData.Get_Fields_Int32("ID"));
					// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
					if (Conversion.Val(rsData.Get_Fields("StreetNumber")) != 0)
					{
						// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
						vsDemand.TextMatrix(vsDemand.Rows - 1, lngGridColLocation, Strings.Trim(FCConvert.ToString(rsData.Get_Fields("StreetNumber")) + " " + FCConvert.ToString(rsData.Get_Fields_String("StreetName"))));
						// location
					}
					else if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("StreetName"))) != "")
					{
						vsDemand.TextMatrix(vsDemand.Rows - 1, lngGridColLocation, Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("StreetName"))));
						// location
					}
					else
					{
						// if there is nothing in the bill then show the RE Master info
						if (Conversion.Val(rsRE.Get_Fields_String("RSLOCNUMALPH")) != 0)
						{
							vsDemand.TextMatrix(vsDemand.Rows - 1, lngGridColLocation, Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("RSLOCNUMALPH")) + " " + FCConvert.ToString(rsRE.Get_Fields_String("RSLOCSTREET"))));
							// location
						}
						else
						{
							vsDemand.TextMatrix(vsDemand.Rows - 1, lngGridColLocation, Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("RSLOCSTREET"))));
							// location
						}
					}
					// amounts
					vsDemand.TextMatrix(vsDemand.Rows - 1, lngGridColOSTaxYear, FCConvert.ToString(dblBillAmount));
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					dblTotalAmount = modCLCalculations.CalculateAccountTotal1(FCConvert.ToInt32(rsData.Get_Fields("Account")), true);
					vsDemand.TextMatrix(vsDemand.Rows - 1, lngGridColOSTaxTotal, FCConvert.ToString(dblTotalAmount));
					SKIP:
					;
					rsRE.MoveNext();
				}
				if (lngIndex < 0)
				{
					frmWait.InstancePtr.Unload();
					FCMessageBox.Show("There are no " + modExtraModules.FormatYear(lngBillingYear.ToString()) + " bills eligible for Tax Acquired Status.", MsgBoxStyle.Information, "No Eligible Bills");
					Close();
				}
				// check all of the accounts
				mnuFileSelectAll_Click();
				//if (vsDemand.Rows * vsDemand.RowHeight(0) > fraGrid.Height - lblInstruction.Height - 500)
				//{
				//	vsDemand.Height = fraGrid.Height - lblInstruction.Height - 500;
				//	vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
				//}
				//else
				//{
				//	vsDemand.Height = vsDemand.Rows * vsDemand.RowHeight(0) + 70;
				//	vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				//}
				frmWait.InstancePtr.Unload();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Filling Grid");
			}
		}

		private void FillBillingYearCombo()
		{
			// this will fill the validate grid with the information from the Control_30DayNotice table
			// which is the information that was used to print the 30 Day Notices the last time
			clsDRWrapper rsValidate = new clsDRWrapper();
			rsValidate.OpenRecordset("SELECT DISTINCT BillingYear FROM BillingMaster WHERE LienRecordNumber <> 0 ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
			strYearList = "";
            cmbBillingYear.Items.Clear();
			while (!rsValidate.EndOfFile())
			{
                cmbBillingYear.AddItem(modExtraModules.FormatYear(FCConvert.ToString(rsValidate.Get_Fields_Int32("BillingYear"))));
				//strYearList += modExtraModules.FormatYear(FCConvert.ToString(rsValidate.Get_Fields_Int32("BillingYear"))) + "|";
				rsValidate.MoveNext();
			}
			//vsValidate.Rows = 1;
			//vsValidate.TextMatrix(0, 0, "Billing Year");
		}

		private void SetAct(short intAct)
		{
			// this will change all of the menu options
			switch (intAct)
			{
				case 0:
					{
						ShowValidateFrame();
						fraGrid.Visible = false;
						btnProcess.Text = "Accept Parameters";
						btnProcess.Size = new System.Drawing.Size(210, 48);
						cmdFileSelectAll.Visible = false;
						intAction = 0;
						break;
					}
				case 1:
					{
						FillDemandGrid();
						//FC:FINAL:MSH - issue #1774: form can be closed in FillDemandGrid()
						if (!this.IsDisposed)
						{
							ShowGridFrame();
							fraValidate.Visible = false;
							cmdFileSelectAll.Visible = true;
							btnProcess.Text = "Save";
							btnProcess.Size = new System.Drawing.Size(120, 48);
						}
						intAction = 1;
						break;
					}
			}
			//end switch
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			switch (intAction)
			{
				case 0:
					{
						// this will say that the user accepts the parameters shown
						if (cmbBillingYear.Text != "")
						{
							// MsgBox "After demand fees are applied, it will not be possible to reprint the notices, labels or certified mailers.", MsgBoxStyle.Exclamation, "Apply Demand Fees"
							SetAct(1);
						}
						else
						{
							FCMessageBox.Show("Please enter a year.", MsgBoxStyle.Exclamation, "Missing Information");
							// MAL@20071011:Changed Message Caption from 'Missing Paremeters'
						}
						break;
					}
				case 1:
					{
						// MAL@20080812 ; Tracker Reference: 13868
						if (txtTADate.Text == "")
						{
							FCMessageBox.Show("You must enter a Tax Acquired Date", MsgBoxStyle.OkOnly | MsgBoxStyle.Exclamation, "Required");
							return;
						}
						else if (!Information.IsDate(txtTADate.Text))
						{
							FCMessageBox.Show("You must enter a Tax Acquired Date", MsgBoxStyle.OkOnly | MsgBoxStyle.Exclamation, "Required");
							return;
						}
						else
						{
							AffectTaxAcquiredProperties();
							Close();
							App.DoEvents();
							if (App.MainForm.Enabled && App.MainForm.Visible)
							{
								App.MainForm.Focus();
							}
						}
						break;
					}
			}
			//end switch
		}

		private void AffectTaxAcquiredProperties()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngCT;
				int lngCount = 0;
				clsDRWrapper rsRate = new clsDRWrapper();
				// Start fresh
				FCUtils.EraseSafe(modGlobal.Statics.arrTALienPrincipal);
				FCUtils.EraseSafe(modGlobal.Statics.arrTAPrincipal);
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Updating Tax Acquired");
				for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
				{
					// for each account in the grid
					if (Conversion.Val(vsDemand.TextMatrix(lngCT, 0)) == -1)
					{
						// check to see if the check box if checked
						modGlobal.Statics.arrDemand[FindDemandIndex(FCConvert.ToInt32(vsDemand.TextMatrix(lngCT, lngGridColBillKey)))].Processed = true;
						CalculateTAJournalEntries(FCConvert.ToInt32(Conversion.Val(vsDemand.TextMatrix(lngCT, lngGridColAccount))));
						lngCount += 1;
					}
					else
					{
						// not selected...do nothing
					}
				}
				frmWait.InstancePtr.Unload();
				if (CreateTAJournalEntry())
				{
					frmWait.InstancePtr.Unload();
					if (lngCount != 1)
					{
						FCMessageBox.Show(FCConvert.ToString(lngCount) + " accounts were affected.", MsgBoxStyle.Information, "Tax Acquired Status");
					}
					else
					{
						FCMessageBox.Show(FCConvert.ToString(lngCount) + " account was affected.", MsgBoxStyle.Information, "Tax Acquired Status");
					}
					// update RE
					if (!UpdateTaxAcquiredAccounts())
					{
						FCMessageBox.Show("Errors setting tax acquired status.", MsgBoxStyle.Critical, "Error");
					}
					// print the list of accounts that have been affected
					rptTaxAcquiredList.InstancePtr.Init(lngCount);
					frmReportViewer.InstancePtr.Init(rptTaxAcquiredList.InstancePtr);
				}
				else
				{
					// Failed the journal entry
					// do not set the acquired field and do not show any report
					FCMessageBox.Show("The Budgetary journal entries were not created and the Tax Acquired field has not been updated.", MsgBoxStyle.Exclamation, "Error Updating");
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Affecting Status");
			}
		}

		private bool UpdateTaxAcquiredAccounts()
		{
			bool UpdateTaxAcquiredAccounts = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngCT;
				UpdateTaxAcquiredAccounts = true;
				for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
				{
					// for each account in the grid
					if (Conversion.Val(vsDemand.TextMatrix(lngCT, 0)) == -1)
					{
						// check to see if the check box if checked
						rsRE.FindFirstRecord("RSAccount", vsDemand.TextMatrix(lngCT, lngGridColAccount));
						if (!rsRE.NoMatch)
						{
							rsRE.Edit();
							rsRE.Set_Fields("TaxAcquired", true);
							// MAL@20080812: Changed to use the entered date
							// Tracker Reference: 13868
							// rsRE.Get_Fields("TADate") = Now
							rsRE.Set_Fields("TADate", txtTADate.Text);
							rsRE.Update();
						}
					}
				}
				return UpdateTaxAcquiredAccounts;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				UpdateTaxAcquiredAccounts = false;
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Updating TA Accounts");
			}
			return UpdateTaxAcquiredAccounts;
		}
		// VBto upgrade warning: lngBillKey As int	OnWrite(string)
		private int FindDemandIndex(int lngBillKey)
		{
			int FindDemandIndex = 0;
			int lngCT;
			for (lngCT = 0; lngCT <= Information.UBound(modGlobal.Statics.arrDemand, 1); lngCT++)
			{
				if (lngBillKey == modGlobal.Statics.arrDemand[lngCT].BillKey)
				{
					FindDemandIndex = lngCT;
					break;
				}
			}
			return FindDemandIndex;
		}

		private void mnuFileSelectAll_Click(object sender, System.EventArgs e)
		{
			// this will select all account in the grid
			int lngCT;
			for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
			{
				vsDemand.TextMatrix(lngCT, 0, FCConvert.ToString(-1));
			}
		}

		public void mnuFileSelectAll_Click()
		{
			mnuFileSelectAll_Click(cmdFileSelectAll, new System.EventArgs());
		}

		private void vsDemand_DblClick(object sender, EventArgs e)
		{
			int lngMR;
			int lngMC;
			lngMR = vsDemand.MouseRow;
			lngMC = vsDemand.MouseCol;
			if (lngMR == 0 && lngMC == 0)
			{
				mnuFileSelectAll_Click();
			}
		}

		private void vsDemand_RowColChange(object sender, EventArgs e)
		{
			if (vsDemand.Col == lngGridColTaxAcquired)
			{
				vsDemand.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else
			{
				vsDemand.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void ClearCheckBoxes()
		{
			// this will set all of the textboxes to unchecked
			int lngCT;
			for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
			{
				vsDemand.TextMatrix(lngCT, lngGridColTaxAcquired, FCConvert.ToString(0));
			}
		}
		//FC:FINAL:AM:#86 - no need for this code
		//private void vsValidate_RowColChange(object sender, EventArgs e)
		//{
		//          vsValidate.ComboList = strYearList;
		//          vsValidate.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
		//      }
		private void CalculateTAJournalEntries(int lngAcct)
		{
			try
			{
				// On Error GoTo ERROR_HANLDER
				// This function will add records to the journal for the whole account
				// arrJournalEntry(Year - DEFAULTSUBTRACTIONVALUE) = the journal amount   'this will hold the principal amount
				clsDRWrapper rsCL = new clsDRWrapper();
				clsDRWrapper rsLN = new clsDRWrapper();
				double dblAmount = 0;
				clsDRWrapper rsMaster = new clsDRWrapper();
				int lngRes = 0;
				rsCL.OpenRecordset("SELECT * FROM BillingMaster INNER JOIN " + modGlobal.Statics.strDbRE + "Master ON BillingMaster.Account = Master.RSAccount WHERE RSCard = 1 AND Account = " + FCConvert.ToString(lngAcct) + " AND BillingType = 'RE'", modExtraModules.strCLDatabase);
				while (!rsCL.EndOfFile())
				{
					if (modGlobal.Statics.gboolMultipleTowns)
					{
						lngRes = FCConvert.ToInt32(rsCL.Get_Fields_Int32("RITranCode"));
					}
					else
					{
						lngRes = 0;
					}
					if (((rsCL.Get_Fields_Int32("LienRecordNumber"))) != 0)
					{
						// Lien
						rsLN.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(rsCL.Get_Fields_Int32("LienRecordNumber")), modExtraModules.strCLDatabase);
						if (!rsLN.EndOfFile())
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							dblAmount = Conversion.Val(rsLN.Get_Fields("Principal")) - Conversion.Val(rsLN.Get_Fields_Decimal("PrincipalPaid"));
							modGlobal.Statics.arrTALienPrincipal[rsCL.Get_Fields_Int32("BillingYear") - modExtraModules.DEFAULTSUBTRACTIONVALUE, lngRes] = modGlobal.Statics.arrTALienPrincipal[FCConvert.ToInt16(rsCL.Get_Fields_Int32("BillingYear")) - modExtraModules.DEFAULTSUBTRACTIONVALUE, lngRes] + dblAmount;
						}
						else
						{
							// do nothing
						}
					}
					else
					{
						// Non Lien
						dblAmount = (Conversion.Val(rsCL.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsCL.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsCL.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsCL.Get_Fields_Decimal("TaxDue4"))) - Conversion.Val(rsCL.Get_Fields_Decimal("PrincipalPaid"));
						modGlobal.Statics.arrTAPrincipal[rsCL.Get_Fields_Int32("BillingYear") - modExtraModules.DEFAULTSUBTRACTIONVALUE, lngRes] = modGlobal.Statics.arrTAPrincipal[FCConvert.ToInt16(rsCL.Get_Fields_Int32("BillingYear")) - modExtraModules.DEFAULTSUBTRACTIONVALUE, lngRes] + dblAmount;
					}
					rsCL.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANLDER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Adding Journal Entries");
			}
		}

		private bool CreateTAJournalEntry()
		{
			bool CreateTAJournalEntry = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsEntries = new clsDRWrapper();
				clsDRWrapper rsBillInfo = new clsDRWrapper();
				clsDRWrapper Master = new clsDRWrapper();
				clsDRWrapper rsTown = new clsDRWrapper();
				string strTableName;
				int lngJournal = 0;
				string strLAccountNumber = "";
				string strRAccountNumber = "";
				string strLTAAccountNumber = "";
				string strRTAAccountNumber = "";
				string strSQL = "";
				double[] dblRFundAmount = new double[99 + 1];
				double[] dblLFundAmount = new double[99 + 1];
				int lngYear = 0;
				int intFund = 0;
				int intCT;
				bool boolUseYear = false;
				bool boolUseLYear = false;
				bool boolTAUseYear = false;
				bool boolTAUseLYear = false;
				int intResCode;
				string strMulti = "";
				string strRealEstateCommitmentAcct = "";
				clsBudgetaryPosting tPostInfo;
				// kk01282015 trocls-51  Add automatic posting of journals
				clsPostingJournalInfo tJournalInfo;
				CreateTAJournalEntry = true;
				if (!modGlobalConstants.Statics.gboolBD)
				{
					// if the user does not have BD then exit and return back true
					return CreateTAJournalEntry;
				}
				// Find Receivable Account Number
				Master.OpenRecordset("SELECT * FROM StandardAccounts", modExtraModules.strBDDatabase);
				if (!Master.EndOfFile())
				{
					Master.FindFirstRecord("Code", "RR");
					// kk08122014 trocls-49   ' "'RR'"
					if (!Master.NoMatch)
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						strRAccountNumber = FCConvert.ToString(Master.Get_Fields("Account"));
					}
					Master.FindFirstRecord("Code", "LR");
					// kk08122014 trocls-49   ' "'LR'"
					if (!Master.NoMatch)
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						strLAccountNumber = FCConvert.ToString(Master.Get_Fields("Account"));
					}
					Master.FindFirstRecord("Code", "TR");
					// kk08122014 trocls-49   ' "'TR'"
					if (!Master.NoMatch)
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						strRTAAccountNumber = FCConvert.ToString(Master.Get_Fields("Account"));
					}
					Master.FindFirstRecord("Code", "LV");
					// kk08122014 trocls-49   ' "'LV'"
					if (!Master.NoMatch)
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						strLTAAccountNumber = FCConvert.ToString(Master.Get_Fields("Account"));
					}
					Master.FindFirstRecord("Code", "RM");
					// kk08122014 trocls-49   ' "'RM'"
					if (!Master.NoMatch)
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						strRealEstateCommitmentAcct = FCConvert.ToString(Master.Get_Fields("Account"));
					}
				}
				// Check to make sure the accounts are all there
				if (Strings.Trim(strRAccountNumber) == "")
				{
					FCMessageBox.Show("Please enter the Real Estate Tax Receivable Account in Budgetary.", MsgBoxStyle.Exclamation, "Missing Receivable Account");
					CreateTAJournalEntry = false;
					return CreateTAJournalEntry;
				}
				if (Strings.Trim(strLAccountNumber) == "")
				{
					FCMessageBox.Show("Please enter the Real Estate Lien Receivable Account in Budgetary.", MsgBoxStyle.Exclamation, "Missing Receivable Account");
					CreateTAJournalEntry = false;
					return CreateTAJournalEntry;
				}
				if (Strings.Trim(strRTAAccountNumber) == "")
				{
					FCMessageBox.Show("Please enter the Tax Acquired Tax Receivable Account in Budgetary.", MsgBoxStyle.Exclamation, "Missing Receivable Account");
					CreateTAJournalEntry = false;
					return CreateTAJournalEntry;
				}
				if (Strings.Trim(strLTAAccountNumber) == "")
				{
					FCMessageBox.Show("Please enter the Tax Acquired Lien Receivable Account in Budgetary.", MsgBoxStyle.Exclamation, "Missing Receivable Account");
					CreateTAJournalEntry = false;
					return CreateTAJournalEntry;
				}
				if (Strings.Trim(strRealEstateCommitmentAcct) == "")
				{
					FCMessageBox.Show("Please enter the Real Estate Commitment Account in Budgetary.", MsgBoxStyle.Exclamation, "Missing Receivable Account");
					CreateTAJournalEntry = false;
					return CreateTAJournalEntry;
				}
				strTableName = "";
				// get journal number
				if (modBudgetaryAccounting.LockJournal() == false)
				{
					// add entries to incomplete journals table
					strTableName = "Temp";
				}
				else
				{
					Master.OpenRecordset(("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC"), "TWBD0000.vb1");
					if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
					{
						Master.MoveLast();
						Master.MoveFirst();
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						lngJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
					}
					else
					{
						lngJournal = 1;
					}
					Master.AddNew();
					Master.Set_Fields("JournalNumber", lngJournal);
					Master.Set_Fields("Status", "E");
					Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
					Master.Set_Fields("StatusChangeDate", DateTime.Today);
					Master.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " TA Status");
					Master.Set_Fields("Type", "GJ");
					Master.Set_Fields("Period", DateTime.Today.Month);
					Master.Update();
					Master.Reset();
					modBudgetaryAccounting.UnlockJournal();
				}
				if (modGlobalConstants.Statics.gboolCR)
				{
					if (modGlobal.Statics.gboolMultipleTowns)
					{
						strMulti = " AND TownKey = 1";
					}
					rsEntries.OpenRecordset("SELECT * FROM Type WHERE Typecode = 90" + strMulti, modExtraModules.strCRDatabase);
					if (!rsEntries.EndOfFile())
					{
						boolUseYear = FCConvert.ToBoolean(rsEntries.Get_Fields_Boolean("Year1"));
					}
					else
					{
						boolUseYear = false;
					}
					rsEntries.OpenRecordset("SELECT * FROM Type WHERE Typecode = 91" + strMulti, modExtraModules.strCRDatabase);
					if (!rsEntries.EndOfFile())
					{
						boolUseLYear = FCConvert.ToBoolean(rsEntries.Get_Fields_Boolean("Year1"));
					}
					else
					{
						boolUseLYear = false;
					}
					rsEntries.OpenRecordset("SELECT * FROM Type WHERE Typecode = 890" + strMulti, modExtraModules.strCRDatabase);
					if (!rsEntries.EndOfFile())
					{
						boolTAUseYear = FCConvert.ToBoolean(rsEntries.Get_Fields_Boolean("Year1"));
					}
					else
					{
						boolTAUseYear = false;
					}
					rsEntries.OpenRecordset("SELECT * FROM Type WHERE Typecode = 891" + strMulti, modExtraModules.strCRDatabase);
					if (!rsEntries.EndOfFile())
					{
						boolTAUseLYear = FCConvert.ToBoolean(rsEntries.Get_Fields_Boolean("Year1"));
					}
					else
					{
						boolTAUseLYear = false;
					}
				}
				else
				{
					boolUseYear = false;
					boolUseLYear = false;
					boolTAUseYear = false;
					boolTAUseLYear = false;
				}
				rsEntries.OpenRecordset("SELECT * FROM " + strTableName + "JournalEntries WHERE ID = 0", "TWBD0000.vb1");
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Adding Journal Entries", true, Information.UBound(modGlobal.Statics.arrTAPrincipal, 1));
				// Add Regular Tax Entries and recievable entry
				for (intCT = 0; intCT <= Information.UBound(modGlobal.Statics.arrTAPrincipal, 1); intCT++)
				{
					frmWait.InstancePtr.IncrementProgress();
					for (intResCode = 0; intResCode <= 9; intResCode++)
					{
						App.DoEvents();
						if ((modGlobal.Statics.gboolMultipleTowns && intResCode > 0) || (!modGlobal.Statics.gboolMultipleTowns && intResCode == 0))
						{
							if (modGlobal.Statics.gboolMultipleTowns)
							{
								intFund = 1;
								rsTown.OpenRecordset("SELECT TownNumber, Fund FROM tblRegions WHERE TownNumber = " + FCConvert.ToString(intResCode), "CentralData");
								if (!rsTown.EndOfFile())
								{
									// TODO Get_Fields: Check the table for the column [TownNumber] and replace with corresponding Get_Field method
									intFund = FCConvert.ToInt32(Math.Round(Conversion.Val("0" + FCConvert.ToString(rsTown.Get_Fields("TownNumber")))));
								}
							}
							else
							{
								intFund = modBudgetaryAccounting.GetFundFromAccount(strRealEstateCommitmentAcct);
							}
							if (modGlobal.Statics.arrTAPrincipal[intCT, intResCode] != 0)
							{
								// Regular Receivable
								rsEntries.AddNew();
								rsEntries.Set_Fields("Type", "G");
								rsEntries.Set_Fields("JournalEntriesDate", DateTime.Today);
								rsEntries.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " Tax Acquired");
								rsEntries.Set_Fields("JournalNumber", lngJournal);
								lngYear = intCT + modExtraModules.DEFAULTSUBTRACTIONVALUE;
								if (boolUseYear)
								{
									rsEntries.Set_Fields("Account", "G " + modGlobal.PadToString(intFund, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modGlobal.PadToString(FCConvert.ToInt32(strRAccountNumber), FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))) + "-" + modGlobal.PadToString(((lngYear / 10) % 100), FCConvert.ToInt16(Conversion.Val(Strings.Right(modAccountTitle.Statics.Ledger, 2)))));
								}
								else
								{
									rsEntries.Set_Fields("Account", "G " + modGlobal.PadToString(intFund, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modGlobal.PadToString(FCConvert.ToInt32(strRAccountNumber), FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))) + "-" + modGlobal.PadToString(0, FCConvert.ToInt16(Conversion.Val(Strings.Right(modAccountTitle.Statics.Ledger, 2)))));
								}
								rsEntries.Set_Fields("Amount", modGlobal.Statics.arrTAPrincipal[intCT, intResCode] * -1);
								rsEntries.Set_Fields("WarrantNumber", 0);
								rsEntries.Set_Fields("Period", DateTime.Today.Month);
								rsEntries.Set_Fields("RCB", "R");
								rsEntries.Set_Fields("Status", "E");
								rsEntries.Update();
								// Tax Acquired Receivable
								rsEntries.AddNew();
								rsEntries.Set_Fields("Type", "G");
								rsEntries.Set_Fields("JournalEntriesDate", DateTime.Today);
								rsEntries.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " Tax Acquired");
								rsEntries.Set_Fields("JournalNumber", lngJournal);
								lngYear = intCT + modExtraModules.DEFAULTSUBTRACTIONVALUE;
								// intFund = 1  'How do I get the fund?     'GetFundFromAccount(strRAccountNumber)
								if (boolTAUseYear)
								{
									rsEntries.Set_Fields("Account", "G " + modGlobal.PadToString(intFund, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modGlobal.PadToString(FCConvert.ToInt32(strRTAAccountNumber), FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))) + "-" + modGlobal.PadToString(((lngYear / 10) % 100), FCConvert.ToInt16(Conversion.Val(Strings.Right(modAccountTitle.Statics.Ledger, 2)))));
								}
								else
								{
									rsEntries.Set_Fields("Account", "G " + modGlobal.PadToString(intFund, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modGlobal.PadToString(FCConvert.ToInt32(strRTAAccountNumber), FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))) + "-" + modGlobal.PadToString(0, FCConvert.ToInt16(Conversion.Val(Strings.Right(modAccountTitle.Statics.Ledger, 2)))));
								}
								rsEntries.Set_Fields("Amount", modGlobal.Statics.arrTAPrincipal[intCT, intResCode]);
								rsEntries.Set_Fields("WarrantNumber", 0);
								rsEntries.Set_Fields("Period", DateTime.Today.Month);
								rsEntries.Set_Fields("RCB", "R");
								rsEntries.Set_Fields("Status", "E");
								rsEntries.Update();
							}
							if (modGlobal.Statics.arrTALienPrincipal[intCT, intResCode] != 0)
							{
								// Lien Receivable
								rsEntries.AddNew();
								rsEntries.Set_Fields("Type", "G");
								rsEntries.Set_Fields("JournalEntriesDate", DateTime.Today);
								rsEntries.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " Tax Acquired");
								rsEntries.Set_Fields("JournalNumber", lngJournal);
								lngYear = intCT + modExtraModules.DEFAULTSUBTRACTIONVALUE;
								// intFund = 1  'How do I get the fund?     'GetFundFromAccount(strRAccountNumber)
								if (boolUseLYear)
								{
									rsEntries.Set_Fields("Account", "G " + modGlobal.PadToString(intFund, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modGlobal.PadToString(FCConvert.ToInt32(strLAccountNumber), FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))) + "-" + modGlobal.PadToString(((lngYear / 10) % 100), FCConvert.ToInt16(Conversion.Val(Strings.Right(modAccountTitle.Statics.Ledger, 2)))));
								}
								else
								{
									rsEntries.Set_Fields("Account", "G " + modGlobal.PadToString(intFund, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modGlobal.PadToString(FCConvert.ToInt32(strLAccountNumber), FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))) + "-" + modGlobal.PadToString(0, FCConvert.ToInt16(Conversion.Val(Strings.Right(modAccountTitle.Statics.Ledger, 2)))));
								}
								rsEntries.Set_Fields("Amount", modGlobal.Statics.arrTALienPrincipal[intCT, intResCode] * -1);
								rsEntries.Set_Fields("WarrantNumber", 0);
								rsEntries.Set_Fields("Period", DateTime.Today.Month);
								rsEntries.Set_Fields("RCB", "R");
								rsEntries.Set_Fields("Status", "E");
								rsEntries.Update();
								// Tax Acquired Lien Receivable
								rsEntries.AddNew();
								rsEntries.Set_Fields("Type", "G");
								rsEntries.Set_Fields("JournalEntriesDate", DateTime.Today);
								rsEntries.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " Tax Acquired");
								rsEntries.Set_Fields("JournalNumber", lngJournal);
								lngYear = intCT + modExtraModules.DEFAULTSUBTRACTIONVALUE;
								// intFund = 1  'How do I get the fund?     'GetFundFromAccount(strRAccountNumber)
								if (boolTAUseLYear)
								{
									rsEntries.Set_Fields("Account", "G " + modGlobal.PadToString(intFund, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modGlobal.PadToString(FCConvert.ToInt32(strLTAAccountNumber), FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))) + "-" + modGlobal.PadToString(((lngYear / 10) % 100), FCConvert.ToInt16(Conversion.Val(Strings.Right(modAccountTitle.Statics.Ledger, 2)))));
								}
								else
								{
									rsEntries.Set_Fields("Account", "G " + modGlobal.PadToString(intFund, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modGlobal.PadToString(FCConvert.ToInt32(strLTAAccountNumber), FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))) + "-" + modGlobal.PadToString(0, FCConvert.ToInt16(Conversion.Val(Strings.Right(modAccountTitle.Statics.Ledger, 2)))));
								}
								rsEntries.Set_Fields("Amount", modGlobal.Statics.arrTALienPrincipal[intCT, intResCode]);
								rsEntries.Set_Fields("WarrantNumber", 0);
								rsEntries.Set_Fields("Period", DateTime.Today.Month);
								rsEntries.Set_Fields("RCB", "R");
								rsEntries.Set_Fields("Status", "E");
								rsEntries.Update();
							}
						}
					}
				}
				frmWait.InstancePtr.Unload();
				// kk01282015 trocls-51  Allow Automatic posting of journals
				if (lngJournal == 0)
				{
					FCMessageBox.Show("Unable to complete the journal.  You must go into the Budgetary System > Data Entry > Build Incomplete Journals to finish creating the journal.", MsgBoxStyle.Exclamation, "Unable to Save");
				}
				else
				{
					if (modSecurityValidation.ValidPermissions(null, modGlobal.CLSECURITYAUTOPOSTJOURNAL, false) && modBudgetaryAccounting.AutoPostAllowed(modBudgetaryAccounting.AutoPostType.aptCLTaxAcquired))
					{
						if (FCMessageBox.Show("The entries have been saved into journal " + Strings.Format(lngJournal, "0000") + ".  Would you like to post the journal?", MsgBoxStyle.Question | MsgBoxStyle.YesNo, "Post Entries") == DialogResult.Yes)
						{
							tJournalInfo = new clsPostingJournalInfo();
							tJournalInfo.JournalNumber = lngJournal;
							tJournalInfo.JournalType = "GJ";
							tJournalInfo.Period = FCConvert.ToString(DateTime.Today.Month);
							tJournalInfo.CheckDate = "";
							tPostInfo = new clsBudgetaryPosting();
							tPostInfo.AddJournal(tJournalInfo);
							tPostInfo.AllowPreview = true;
							tPostInfo.PageBreakBetweenJournalsOnReport = true;
							tPostInfo.WaitForReportToEnd = true;
							tPostInfo.PostJournals();
							tJournalInfo = null;
							tPostInfo = null;
						}
					}
					else
					{
						FCMessageBox.Show("The Budgetary journal entries were created in journal #" + FCConvert.ToString(lngJournal) + ".", MsgBoxStyle.Exclamation, "Journal Created");
					}
				}
				return CreateTAJournalEntry;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				CreateTAJournalEntry = false;
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Creating Journal");
			}
			return CreateTAJournalEntry;
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFilePrint_Click(sender, e);
		}

		private void cmdFileSelectAll_Click(object sender, EventArgs e)
		{
			this.mnuFileSelectAll_Click(sender, e);
		}

		private void cmdFileClear_Click(object sender, EventArgs e)
		{
			this.mnuFileClear_Click(sender, e);
		}
	}
}
