﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmLienDates.
	/// </summary>
	public partial class frmLienDates : BaseForm
	{
		public frmLienDates()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmLienDates InstancePtr
		{
			get
			{
				return (frmLienDates)Sys.GetInstance(typeof(frmLienDates));
			}
		}

		protected frmLienDates _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               03/29/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               06/16/2004              *
		// ********************************************************
		clsDRWrapper rsSettings = new clsDRWrapper();
		bool boolLoaded;
		int lngColRateKey;
		int lngColYear;
		int lngColHiddenDueNow;
		int lngColHiddenDue30Days;
		int lngColType;
		int lngColCommitmentDate;
		int lngCol30DNDate1;
		int lngCol30DNDate2;
		int lngCOl30DNDate;
		int lngColLien1;
		int lngColLien2;
		int lngColLienDate;
		int lngColMaturityDate;
		int lngColMaturityNotice1;
		int lngColMaturityNotice2;
		int lngColDescription;
		public string strReportHeader = "";
		// this will be the header for the report
		private void frmLienDates_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				LoadSettings();
				boolLoaded = true;
			}
			else
			{
			}
		}

		private void frmLienDates_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmLienDates.Icon	= "frmLienDates.frx":0000";
			//frmLienDates.FillStyle	= 0;
			//frmLienDates.ScaleWidth	= 9300;
			//frmLienDates.ScaleHeight	= 7650;
			//frmLienDates.LinkTopic	= "Form2";
			//frmLienDates.LockControls	= -1  'True;
			//frmLienDates.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "9.75";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//vsRate.BackColor	= "-2147483643";
			//			//vsRate.ForeColor	= "-2147483640";
			//vsRate.BorderStyle	= 1;
			//vsRate.FillStyle	= 0;
			//vsRate.Appearance	= 1;
			//vsRate.GridLines	= 1;
			//vsRate.WordWrap	= 0;
			//vsRate.ScrollBars	= 3;
			//vsRate.RightToLeft	= 0;
			//vsRate._cx	= 15796;
			//vsRate._cy	= 11834;
			//vsRate._ConvInfo	= 1;
			//vsRate.MousePointer	= 0;
			//vsRate.BackColorFixed	= -2147483633;
			//			//vsRate.ForeColorFixed	= -2147483630;
			//vsRate.BackColorSel	= -2147483635;
			//			//vsRate.ForeColorSel	= -2147483634;
			//vsRate.BackColorBkg	= -2147483636;
			//vsRate.BackColorAlternate	= -2147483643;
			//vsRate.GridColor	= -2147483633;
			//vsRate.GridColorFixed	= -2147483632;
			//vsRate.TreeColor	= -2147483632;
			//vsRate.FloodColor	= 192;
			//vsRate.SheetBorder	= -2147483642;
			//vsRate.FocusRect	= 1;
			//vsRate.HighLight	= 1;
			//vsRate.AllowSelection	= -1  'True;
			//vsRate.AllowBigSelection	= -1  'True;
			//vsRate.AllowUserResizing	= 1;
			//vsRate.SelectionMode	= 0;
			//vsRate.GridLinesFixed	= 2;
			//vsRate.GridLineWidth	= 1;
			//vsRate.RowHeightMin	= 0;
			//vsRate.RowHeightMax	= 0;
			//vsRate.ColWidthMin	= 0;
			//vsRate.ColWidthMax	= 0;
			//vsRate.ExtendLastCol	= -1  'True;
			//vsRate.FormatString	= "";
			//vsRate.ScrollTrack	= -1  'True;
			//vsRate.ScrollTips	= 0   'False;
			//vsRate.MergeCells	= 0;
			//vsRate.MergeCompare	= 0;
			//vsRate.AutoResize	= -1  'True;
			//vsRate.AutoSizeMode	= 0;
			//vsRate.AutoSearch	= 0;
			//vsRate.AutoSearchDelay	= 2;
			//vsRate.MultiTotals	= -1  'True;
			//vsRate.SubtotalPosition	= 1;
			//vsRate.OutlineBar	= 0;
			//vsRate.OutlineCol	= 0;
			//vsRate.Ellipsis	= 0;
			//vsRate.ExplorerBar	= 5;
			//vsRate.PicturesOver	= 0   'False;
			//vsRate.PictureType	= 0;
			//vsRate.TabBehavior	= 0;
			//vsRate.OwnerDraw	= 0;
			//vsRate.ShowComboButton	= -1  'True;
			//vsRate.TextStyle	= 0;
			//vsRate.TextStyleFixed	= 0;
			//vsRate.OleDragMode	= 0;
			//vsRate.OleDropMode	= 0;
			//vsRate.ComboSearch	= 3;
			//vsRate.AutoSizeMouse	= -1  'True;
			//vsRate.AllowUserFreezing	= 0;
			//vsRate.BackColorFrozen	= 0;
			//			//vsRate.ForeColorFrozen	= 0;
			//vsRate.WallPaperAlignment	= 9;
			//vsElasticLight1.OleObjectBlob	= "frmLienDates.frx":058A";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			// set the columns
			lngColRateKey = 0;
			// Rate Key, Year and the two hidden fields should stay the same
			lngColYear = 1;
			// because the reports use them w/o the variables, the rest can be shuffled
			lngColHiddenDueNow = 2;
			lngColHiddenDue30Days = 3;
			// ******** Changeable *********
			lngColType = 4;
			lngColDescription = 5;
			lngColCommitmentDate = 6;
			lngCol30DNDate1 = 7;
			lngCol30DNDate2 = 8;
			lngCOl30DNDate = 9;
			lngColLien1 = 10;
			lngColLien2 = 11;
			lngColLienDate = 12;
			lngColMaturityNotice1 = 13;
			lngColMaturityNotice2 = 14;
			lngColMaturityDate = 15;
		}

		private void FormatGrid(bool boolReset)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngWid = 0;
				if (boolReset)
				{
					vsRate.Rows = 1;
				}
				vsRate.Cols = 16;
				lngWid = vsRate.WidthOriginal;
				vsRate.ColWidth(lngColRateKey, FCConvert.ToInt32(lngWid * 0.07));
				vsRate.ColWidth(lngColYear, FCConvert.ToInt32(lngWid * 0.08));
				vsRate.ColWidth(lngColHiddenDueNow, 0);
				vsRate.ColWidth(lngColHiddenDue30Days, 0);
				vsRate.ColWidth(lngColType, FCConvert.ToInt32(lngWid * 0.06));
				vsRate.ColWidth(lngColDescription, FCConvert.ToInt32(lngWid * 0.23));
				vsRate.ColWidth(lngColCommitmentDate, FCConvert.ToInt32(lngWid * 0.13));
				vsRate.ColWidth(lngCol30DNDate1, FCConvert.ToInt32(lngWid * 0.13));
				vsRate.ColWidth(lngCol30DNDate2, FCConvert.ToInt32(lngWid * 0.13));
				vsRate.ColWidth(lngCOl30DNDate, FCConvert.ToInt32(lngWid * 0.13));
				vsRate.ColWidth(lngColLien1, FCConvert.ToInt32(lngWid * 0.13));
				vsRate.ColWidth(lngColLien2, FCConvert.ToInt32(lngWid * 0.13));
				vsRate.ColWidth(lngColLienDate, FCConvert.ToInt32(lngWid * 0.13));
				vsRate.ColWidth(lngColMaturityNotice1, FCConvert.ToInt32(lngWid * 0.13));
				vsRate.ColWidth(lngColMaturityNotice2, FCConvert.ToInt32(lngWid * 0.13));
				vsRate.ColWidth(lngColMaturityDate, FCConvert.ToInt32(lngWid * 0.13));
				vsRate.TextMatrix(0, lngColRateKey, "Key");
				vsRate.TextMatrix(0, lngColYear, "Year");
				vsRate.TextMatrix(0, lngColType, "Type");
				vsRate.TextMatrix(0, lngColDescription, "Description");
				vsRate.TextMatrix(0, lngColCommitmentDate, "Commitment");
				vsRate.TextMatrix(0, lngCol30DNDate1, "Start 30DN");
				vsRate.TextMatrix(0, lngCol30DNDate2, "End 30DN");
				vsRate.TextMatrix(0, lngCOl30DNDate, "30DN Date");
				vsRate.TextMatrix(0, lngColLien1, "Start Lien");
				vsRate.TextMatrix(0, lngColLien2, "End Lien");
				vsRate.TextMatrix(0, lngColLienDate, "Lien Date");
				vsRate.TextMatrix(0, lngColMaturityNotice1, "Start Mat");
				vsRate.TextMatrix(0, lngColMaturityNotice2, "End Mat");
				vsRate.TextMatrix(0, lngColMaturityDate, "Mat Date");
				// formatting
				vsRate.ColAlignment(lngColYear, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsRate.ColAlignment(lngColType, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				SetGridHeight();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Formatting Grid");
			}
		}

		private void SetGridHeight()
		{
			// this will set the correct grid height compared to how many rows are included in the grid
			//if ((((vsRate.Rows + 1) * vsRate.RowHeight(0)) + vsRate.Top) > frmLienDates.InstancePtr.Height)
			//{
			//	vsRate.Height = FCConvert.ToInt32(((frmLienDates.InstancePtr.Height - vsRate.Top) * 0.95) - vsRate.Top);
			//	// vsRate.Height = (frmLienDates.Height * 0.92) - vsRate.Top
			//}
			//else
			//{
			//	vsRate.Height = ((vsRate.Rows + 1) * vsRate.RowHeight(0));
			//}
		}

		private void frmLienDates_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
            FormUtilities.KeyPressHandler(e, this);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		private void frmLienDates_Resize(object sender, System.EventArgs e)
		{
			SetGridHeight();
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			frmPrintLienDates.InstancePtr.Show(App.MainForm);
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			SaveSettings();
			RefreshGrid();
		}

		private void SaveSettings()
		{
			int intError = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngRW;
				bool boolUpdate = false;
				// go through each record and check to make sure each date has not changed...if it has, then update it
				// just update each one
				// force the end of the edit if there is any
				vsRate.Select(0, 0);
				rsSettings.OpenRecordset("SELECT * FROM RateRec", modExtraModules.strCLDatabase);
				for (lngRW = 1; lngRW <= vsRate.Rows - 1; lngRW++)
				{
					intError = 1;
					boolUpdate = false;
					rsSettings.FindFirstRecord("ID", Conversion.Val(vsRate.TextMatrix(lngRW, lngColRateKey)));
					if (!rsSettings.NoMatch)
					{
						intError = 2;
						rsSettings.Edit();
						if (Strings.Trim(vsRate.TextMatrix(lngRW, lngCOl30DNDate)) != "")
						{
							if (Information.IsDate(vsRate.TextMatrix(lngRW, lngCOl30DNDate)))
							{
								if (Conversion.Val(rsSettings.Get_Fields_DateTime("30DNDate")) != DateAndTime.DateValue(vsRate.TextMatrix(lngRW, lngCOl30DNDate)).ToOADate())
								{
									boolUpdate = true;
									rsSettings.Set_Fields("30DNDate", DateAndTime.DateValue(vsRate.TextMatrix(lngRW, lngCOl30DNDate)));
								}
							}
							else
							{
								rsSettings.Set_Fields("30DNDate", 0);
								// set these to zero after
							}
						}
						else
						{
							rsSettings.Set_Fields("30DNDate", 0);
						}
						if (Strings.Trim(vsRate.TextMatrix(lngRW, lngColLienDate)) != "")
						{
							if (Information.IsDate(vsRate.TextMatrix(lngRW, lngColLienDate)))
							{
								if (Conversion.Val(rsSettings.Get_Fields_DateTime("LienDate")) != DateAndTime.DateValue(vsRate.TextMatrix(lngRW, lngColLienDate)).ToOADate())
								{
									boolUpdate = true;
									rsSettings.Set_Fields("LienDate", DateAndTime.DateValue(vsRate.TextMatrix(lngRW, lngColLienDate)));
								}
							}
							else
							{
								rsSettings.Set_Fields("LienDate", 0);
								// set these to zero after
							}
						}
						else
						{
							rsSettings.Set_Fields("LienDate", 0);
						}
						if (Strings.Trim(vsRate.TextMatrix(lngRW, lngColMaturityDate)) != "")
						{
							if (Information.IsDate(vsRate.TextMatrix(lngRW, lngColMaturityDate)))
							{
								if (Conversion.Val(rsSettings.Get_Fields_DateTime("MaturityDate")) != DateAndTime.DateValue(vsRate.TextMatrix(lngRW, lngColMaturityDate)).ToOADate())
								{
									boolUpdate = true;
									rsSettings.Set_Fields("MaturityDate", DateAndTime.DateValue(vsRate.TextMatrix(lngRW, lngColMaturityDate)));
								}
							}
							else
							{
								rsSettings.Set_Fields("MaturityDate", 0);
								// set these to zero after
							}
						}
						else
						{
							rsSettings.Set_Fields("MaturityDate", 0);
						}
						rsSettings.Update(true);
					}
					if (boolUpdate)
					{
						// add a CYA entry
						modGlobalFunctions.AddCYAEntry_242("CL", "Edit Lien Dates", "30DN = " + vsRate.TextMatrix(lngRW, lngCOl30DNDate), "Lien = " + vsRate.TextMatrix(lngRW, lngColLienDate), "Maturity = " + vsRate.TextMatrix(lngRW, lngColMaturityDate));
					}
				}
				intError = 20;
				FCMessageBox.Show("Save Successful.", MsgBoxStyle.Information, "Save Complete");
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Saving Settings - " + FCConvert.ToString(intError));
			}
		}

		private void LoadSettings()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will load the settings from the database
				// VBto upgrade warning: dt30DN As DateTime	OnWriteFCConvert.ToInt16(
				DateTime dt30DN;
				// VBto upgrade warning: dtLien As DateTime	OnWriteFCConvert.ToInt16(
				DateTime dtLien;
				// VBto upgrade warning: dtMat As DateTime	OnWriteFCConvert.ToInt16(
				DateTime dtMat;
                DateTime dtComm;

                FormatGrid(true);
				// this will get all the Regular Rate Records
				rsSettings.OpenRecordset("SELECT ID AS RK, * FROM RateRec WHERE RateType <> 'L' ORDER BY RateType, [Year], RK", modExtraModules.strCLDatabase);
				while (!rsSettings.EndOfFile())
				{
					vsRate.AddItem("");
					// TODO Get_Fields: Field [RK] not found!! (maybe it is an alias?)
					vsRate.TextMatrix(vsRate.Rows - 1, lngColRateKey, FCConvert.ToString(rsSettings.Get_Fields("RK")));
					// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
					vsRate.TextMatrix(vsRate.Rows - 1, lngColYear, FCConvert.ToString(rsSettings.Get_Fields("Year")));
					vsRate.TextMatrix(vsRate.Rows - 1, lngColType, FCConvert.ToString(rsSettings.Get_Fields_String("RateType")));
					vsRate.TextMatrix(vsRate.Rows - 1, lngColDescription, FCConvert.ToString(rsSettings.Get_Fields_String("Description")));
					if (!rsSettings.IsFieldNull("30DNDate"))
					{
						dt30DN = (DateTime)rsSettings.Get_Fields_DateTime("30DNDate");
					}
					else
					{
						dt30DN = DateTime.FromOADate(0);
					}
					if (!rsSettings.IsFieldNull("LienDate"))
					{
						dtLien = (DateTime)rsSettings.Get_Fields_DateTime("LienDate");
					}
					else
					{
						dtLien = DateTime.FromOADate(0);
					}
					if (!rsSettings.IsFieldNull("MaturityDate"))
					{
						dtMat = (DateTime)rsSettings.Get_Fields_DateTime("MaturityDate");
					}
					else
					{
						dtMat = DateTime.FromOADate(0);
					}
                    if (!rsSettings.IsFieldNull("CommitmentDate"))
                    {
                        dtComm = (DateTime)rsSettings.Get_Fields_DateTime("CommitmentDate");
                    }
                    else
                    {
                        dtComm = DateTime.FromOADate(0);
                    }
                    CalculateLienProcessDates_26(vsRate.Rows - 1, rsSettings.Get_Fields_String("RateType") == "L", FCConvert.ToDateTime(rsSettings.Get_Fields_DateTime("CommitmentDate")), dt30DN, dtLien, dtMat);
					rsSettings.MoveNext();
				}
				// this will get all the LoadBack/Lien Rate Records
				rsSettings.OpenRecordset("SELECT * FROM LoadBackOriginal", modExtraModules.strCLDatabase);
				if (!rsSettings.EndOfFile())
				{
					rsSettings.OpenRecordset("SELECT RateRec.ID AS RK, * FROM RateRec INNER JOIN (BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.ID) ON RateRec.ID = LienRec.RateKey WHERE BillingMaster.RateKey = LienRec.RateKey ORDER BY RateType, [Year], RK", modExtraModules.strCLDatabase);
					while (!rsSettings.EndOfFile())
					{
						// TODO Get_Fields: Field [RK] not found!! (maybe it is an alias?)
						if (vsRate.FindRow(rsSettings.Get_Fields("RK"), -1, lngColRateKey, false, false) < 0)
						{
							vsRate.AddItem("");
							// TODO Get_Fields: Field [RK] not found!! (maybe it is an alias?)
							vsRate.TextMatrix(vsRate.Rows - 1, lngColRateKey, FCConvert.ToString(rsSettings.Get_Fields("RK")));
							// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
							vsRate.TextMatrix(vsRate.Rows - 1, lngColYear, FCConvert.ToString(rsSettings.Get_Fields("Year")));
							vsRate.TextMatrix(vsRate.Rows - 1, lngColType, FCConvert.ToString(rsSettings.Get_Fields_String("RateType")));
							vsRate.TextMatrix(vsRate.Rows - 1, lngColDescription, FCConvert.ToString(rsSettings.Get_Fields_String("Description")));
							if (!rsSettings.IsFieldNull("30DNDate"))
							{
								dt30DN = (DateTime)rsSettings.Get_Fields_DateTime("30DNDate");
							}
							else
							{
								dt30DN = DateTime.FromOADate(0);
							}
							if (!rsSettings.IsFieldNull("LienDate"))
							{
								dtLien = (DateTime)rsSettings.Get_Fields_DateTime("LienDate");
							}
							else
							{
								dtLien = DateTime.FromOADate(0);
							}
							if (!rsSettings.IsFieldNull("MaturityDate"))
							{
								dtMat = (DateTime)rsSettings.Get_Fields_DateTime("MaturityDate");
							}
							else
							{
								dtMat = DateTime.FromOADate(0);
							}
                            if (!rsSettings.IsFieldNull("commitmentdate"))
                            {
                                dtComm = (DateTime)rsSettings.Get_Fields_DateTime("CommitmentDate");
                            }
                            else
                            {
                                dtComm = DateTime.FromOADate(0);
                            }
                            // 30DNDate, LienDate, MaturityDate
                            CalculateLienProcessDates_26(vsRate.Rows - 1, true, dtComm, dt30DN, dtLien, dtMat);
						}
						rsSettings.MoveNext();
					}
				}
				SetGridHeight();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Loading Default Information");
			}
		}

		private void RefreshGrid()
		{
			// this will refresht he grid contents
			FormatGrid(true);
			LoadSettings();
		}

		private void CalculateLienProcessDates_26(int lngRow, bool boolLien, DateTime dtComDate, DateTime dt30DNDate, DateTime dtLienDate, DateTime dtMatDate)
		{
			CalculateLienProcessDates(ref lngRow, ref boolLien, ref dtComDate, ref dt30DNDate, ref dtLienDate, ref dtMatDate);
		}

		private void CalculateLienProcessDates(ref int lngRow, ref bool boolLien, ref DateTime dtComDate, ref DateTime dt30DNDate, ref DateTime dtLienDate, ref DateTime dtMatDate)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// calculate these values from the dates that are in the records
				DateTime dt30DN1;
				DateTime dt30DN2;
				DateTime dtLien1;
				DateTime dtLien2;
				DateTime dtMatNot1;
				DateTime dtMatNot2;
				bool boolDue;
				bool bool30Days;
				boolDue = false;
				bool30Days = false;
				if (!boolLien)
				{
					if (dtComDate.ToOADate() != 0)
					{
						// Commitment Date
						// 30 Day Notice should be send 8 months after commitment
						dt30DN1 = DateAndTime.DateAdd("M", 8, dtComDate);
						dt30DN1 = DateAndTime.DateAdd("D", 1, dt30DN1);
						// End 30 DN time 1 year after the commitment
						dt30DN2 = DateAndTime.DateAdd("yyyy", 1, dtComDate);
						vsRate.TextMatrix(lngRow, lngColCommitmentDate, Strings.Format(dtComDate, "MM/dd/yyyy"));
						vsRate.TextMatrix(lngRow, lngCol30DNDate1, Strings.Format(dt30DN1, "MM/dd/yyyy"));
						vsRate.TextMatrix(lngRow, lngCol30DNDate2, Strings.Format(dt30DN2, "MM/dd/yyyy"));
						if (DateAndTime.DateDiff("d", DateTime.Today, dt30DN1) <= 0 && DateAndTime.DateDiff("d", DateTime.Today, dt30DN2) >= 0)
						{
							// color the dates so the user can see that they are in affect
							boolDue = true;
							//							vsRate.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRow, lngCol30DNDate1, lngRow, lngCol30DNDate2, modGlobalConstants.Statics.TRIOCOLORBLUE);
						}
						if ((DateAndTime.DateDiff("d", DateTime.Today, dt30DN1) > 0 && DateAndTime.DateDiff("d", DateTime.Today, dt30DN1) < 30) && DateAndTime.DateDiff("d", DateAndTime.DateAdd("D", 30, DateTime.Today), dt30DN2) >= 0)
						{
							bool30Days = true;
						}
					}
					else
					{
						vsRate.TextMatrix(vsRate.Rows - 1, lngColCommitmentDate, "");
						vsRate.TextMatrix(vsRate.Rows - 1, lngCol30DNDate1, "");
						vsRate.TextMatrix(vsRate.Rows - 1, lngCol30DNDate2, "");
					}
					// 30 DN Date
					if (dt30DNDate.ToOADate() != 0)
					{
						// Begin Lien Window  30 Days after notice date
						dtLien1 = DateAndTime.DateAdd("d", 30, dt30DNDate);
						// End Lien Window (10 Days)
						dtLien2 = DateAndTime.DateAdd("d", 10, dtLien1);
						vsRate.TextMatrix(lngRow, lngCOl30DNDate, Strings.Format(dt30DNDate, "MM/dd/yyyy"));
						vsRate.TextMatrix(lngRow, lngColLien1, Strings.Format(dtLien1, "MM/dd/yyyy"));
						vsRate.TextMatrix(lngRow, lngColLien2, Strings.Format(dtLien2, "MM/dd/yyyy"));
						if (DateAndTime.DateDiff("d", DateTime.Today, dtLien1) <= 0 && DateAndTime.DateDiff("d", DateTime.Today, dtLien2) >= 0)
						{
							// color the dates so the user can see that they are in affect
							boolDue = true;
							//							vsRate.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRow, lngColLien1, lngRow, lngColLien2, modGlobalConstants.Statics.TRIOCOLORBLUE);
						}
						if ((DateAndTime.DateDiff("d", DateTime.Today, dtLien1) > 0 && DateAndTime.DateDiff("d", DateTime.Today, dtLien1) < 30) && DateAndTime.DateDiff("d", DateAndTime.DateAdd("D", 30, DateTime.Today), dtLien2) >= 0)
						{
							bool30Days = true;
						}
					}
					else
					{
						vsRate.TextMatrix(vsRate.Rows - 1, lngCOl30DNDate, "");
						vsRate.TextMatrix(vsRate.Rows - 1, lngColLien1, "");
						vsRate.TextMatrix(vsRate.Rows - 1, lngColLien2, "");
					}
					// Lien Date
					if (dtLienDate.ToOADate() != 0)
					{
						// Maturity Notice Start
						dtMatNot1 = DateAndTime.DateAdd("M", 18, dtLienDate);
						dtMatNot1 = DateAndTime.DateAdd("d", -45, dtMatNot1);
						// Maturity Notice End
						dtMatNot2 = DateAndTime.DateAdd("d", 15, dtMatNot1);
						vsRate.TextMatrix(lngRow, lngColLienDate, Strings.Format(dtLienDate, "MM/dd/yyyy"));
						vsRate.TextMatrix(lngRow, lngColMaturityNotice1, Strings.Format(dtMatNot1, "MM/dd/yyyy"));
						vsRate.TextMatrix(lngRow, lngColMaturityNotice2, Strings.Format(dtMatNot2, "MM/dd/yyyy"));
						if (DateAndTime.DateDiff("d", DateTime.Today, dtMatNot1) <= 0 && DateAndTime.DateDiff("d", DateTime.Today, dtMatNot2) >= 0)
						{
							// color the dates so the user can see that they are in affect
							boolDue = true;
							//							vsRate.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRow, lngColMaturityNotice1, lngRow, lngColMaturityNotice2, modGlobalConstants.Statics.TRIOCOLORBLUE);
						}
						if ((DateAndTime.DateDiff("d", DateTime.Today, dtMatNot1) > 0 && DateAndTime.DateDiff("d", DateTime.Today, dtMatNot1) < 30) && DateAndTime.DateDiff("d", DateAndTime.DateAdd("D", 30, DateTime.Today), dtMatNot2) >= 0)
						{
							bool30Days = true;
						}
					}
					else
					{
						vsRate.TextMatrix(vsRate.Rows - 1, lngColLienDate, "");
						vsRate.TextMatrix(vsRate.Rows - 1, lngColMaturityNotice1, "");
						vsRate.TextMatrix(vsRate.Rows - 1, lngColMaturityNotice2, "");
					}
					// Maturity Date
					if (dtMatDate.ToOADate() != 0)
					{
						vsRate.TextMatrix(lngRow, lngColMaturityDate, Strings.Format(dtMatDate, "MM/dd/yyyy"));
					}
					else
					{
						vsRate.TextMatrix(lngRow, lngColMaturityDate, "");
					}
				}
				else
				{
					// Lien Date
					if (dtLienDate.ToOADate() != 0)
					{
						vsRate.TextMatrix(lngRow, lngColCommitmentDate, Strings.Format(dtLienDate, "MM/dd/yyyy"));
						// Maturity Notice Start
						dtMatNot1 = DateAndTime.DateAdd("M", 18, dtLienDate);
						dtMatNot1 = DateAndTime.DateAdd("d", -45, dtMatNot1);
						// Maturity Notice End
						dtMatNot2 = DateAndTime.DateAdd("d", 15, dtMatNot1);
						vsRate.TextMatrix(lngRow, lngColLienDate, Strings.Format(dtLienDate, "MM/dd/yyyy"));
						vsRate.TextMatrix(lngRow, lngColMaturityNotice1, Strings.Format(dtMatNot1, "MM/dd/yyyy"));
						vsRate.TextMatrix(lngRow, lngColMaturityNotice2, Strings.Format(dtMatNot2, "MM/dd/yyyy"));
						if (DateAndTime.DateDiff("d", DateTime.Today, dtMatNot1) <= 0 && DateAndTime.DateDiff("d", DateTime.Today, dtMatNot2) >= 0)
						{
							// color the dates so the user can see that they are in affect
							boolDue = true;
							//							vsRate.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRow, lngColMaturityNotice1, lngRow, lngColMaturityNotice2, modGlobalConstants.Statics.TRIOCOLORBLUE);
						}
						if ((DateAndTime.DateDiff("d", DateTime.Today, dtMatNot1) > 0 && DateAndTime.DateDiff("d", DateTime.Today, dtMatNot1) < 30) && DateAndTime.DateDiff("d", DateAndTime.DateAdd("D", 30, DateTime.Today), dtMatNot2) >= 0)
						{
							bool30Days = true;
						}
					}
					else
					{
						vsRate.TextMatrix(vsRate.Rows - 1, lngColLienDate, "");
						vsRate.TextMatrix(vsRate.Rows - 1, lngColMaturityNotice1, "");
						vsRate.TextMatrix(vsRate.Rows - 1, lngColMaturityNotice2, "");
					}
					// Maturity Date
					if (dtMatDate.ToOADate() != 0)
					{
						vsRate.TextMatrix(lngRow, lngColMaturityDate, Strings.Format(dtMatDate, "MM/dd/yyyy"));
					}
					else
					{
						vsRate.TextMatrix(lngRow, lngColMaturityDate, "");
					}
				}
				if (boolDue)
				{
					vsRate.TextMatrix(lngRow, lngColHiddenDueNow, FCConvert.ToString(-1));
				}
				else
				{
					vsRate.TextMatrix(lngRow, lngColHiddenDueNow, FCConvert.ToString(0));
				}
				if (bool30Days)
				{
					vsRate.TextMatrix(lngRow, lngColHiddenDue30Days, FCConvert.ToString(-1));
				}
				else
				{
					vsRate.TextMatrix(lngRow, lngColHiddenDue30Days, FCConvert.ToString(0));
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Calculating Dates");
			}
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			SaveSettings();
			Close();
		}

		private void vsRate_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (vsRate.Col == lngCOl30DNDate || vsRate.Col == lngColLienDate || vsRate.Col == lngColMaturityDate)
			{
				if (Strings.Trim(vsRate.TextMatrix(vsRate.Row, vsRate.Col)) == "__/__/____")
				{
					vsRate.TextMatrix(vsRate.Row, vsRate.Col, string.Empty);
				}
			}
		}

		private void vsRate_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (vsRate.Row > 0)
			{
				if (vsRate.Col == lngCOl30DNDate || vsRate.Col == lngColLienDate || vsRate.Col == lngColMaturityDate)
				{
					// allow editing
					vsRate.EditMask = "##/##/####";
				}
				else
				{
					// do not allow editing
					vsRate.EditMask = "";
				}
			}
		}

		private void vsRate_DblClick(object sender, EventArgs e)
		{
			// this will show the rate key that is clicked on
			ShowChart_2(vsRate.TextMatrix(vsRate.Row, lngColRateKey) + ",");
		}

		private void vsRate_RowColChange(object sender, EventArgs e)
		{
			if (vsRate.Row > 0)
			{
				if (vsRate.Col == lngCOl30DNDate || vsRate.Col == lngColLienDate || vsRate.Col == lngColMaturityDate)
				{
					// allow editing
					vsRate.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsRate.EditCell();
				}
				else
				{
					// do not allow editing
					vsRate.Editable = FCGrid.EditableSettings.flexEDNone;
				}
			}
		}

		private void vsRate_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (vsRate.CurrentCell.IsInEditMode)
			{
				if (Information.IsDate(vsRate.EditText) && Strings.Trim(vsRate.EditText).Length == 10)
				{
					// let the value stand and only update at the end
					// SaveNewDateValue vsRate.TextMatrix(Row, lngColRateKey), CDate(vsRate.EditText), Col
				}
				else if (vsRate.EditText == "__/__/____" || Strings.Trim(vsRate.EditText) == "/  /")
				{
					vsRate.EditText = "";
				}
				else
				{
					//FC:FINAL:DDU prevent FCMessageBox to show up twice
					DialogResult dialogBoxResult = FCMessageBox.Show("Invalid date. Would you like to continue?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Invalid Date");
					if (dialogBoxResult == DialogResult.Yes)
					{
						vsRate.EditText = "";
					}
					else if (dialogBoxResult == DialogResult.Cancel)
					{
						e.Cancel = true;
					}
				}
			}
		}

		private void SaveNewDateValue(ref DateTime dtDate, ref int lngRK, ref int lngCol)
		{
			clsDRWrapper rsSave = new clsDRWrapper();
			rsSave.OpenRecordset("SELECT * FROM RateRec WHERE RateKey = " + FCConvert.ToString(lngRK));
			if (!rsSave.EndOfFile())
			{
				rsSave.Edit();
				if (lngCol == lngCOl30DNDate)
				{
					rsSave.Set_Fields("30DNDate", dtDate);
				}
				else if (lngCol == lngColLienDate)
				{
					rsSave.Set_Fields("LienDate", dtDate);
				}
				else if (lngCol == lngColMaturityDate)
				{
					rsSave.Set_Fields("MaturityDate", dtDate);
				}
				rsSave.Update();
			}
		}

		private void ShowChart_2(string strString)
		{
			ShowChart(ref strString);
		}

		private void ShowChart(ref string strString)
		{
			// show them in the viewer
			frmLienDates.InstancePtr.strReportHeader = "Lien Dates Timeline";
			rptLienDateLine.InstancePtr.Init(strString);
			frmReportViewer.InstancePtr.Init(rptLienDateLine.InstancePtr);
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFileSave_Click(sender, e);
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			this.mnuFileSave_Click(sender, e);
		}

		private void cmdFilePrint_Click(object sender, EventArgs e)
		{
			this.mnuFilePrint_Click(sender, e);
		}
	}
}
