﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmRemove30DNFee.
	/// </summary>
	public partial class frmRemove30DNFee : BaseForm
	{
		public frmRemove30DNFee()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               12/01/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/01/2003              *
		// ********************************************************
		int lngRE;
		int lngPP;
		int lngBL;
		DateTime dtDate;
		int lngAcct;

		private void cmbYear_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == (Keys)13)
			{
				// return
				mnuFilePrint_Click();
			}
		}

		private void frmRemove30DNFee_Activated(object sender, System.EventArgs e)
		{
			lblDate.Text = "Select the account to remove the 30 Day Notice fees from";
		}

		private void frmRemove30DNFee_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmRemove30DNFee.Icon	= "frmRemove30DNFee.frx":0000";
			//frmRemove30DNFee.FillStyle	= 0;
			//frmRemove30DNFee.ScaleWidth	= 3885;
			//frmRemove30DNFee.ScaleHeight	= 2385;
			//frmRemove30DNFee.LinkTopic	= "Form2";
			//frmRemove30DNFee.LockControls	= -1  'True;
			//frmRemove30DNFee.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "9.75";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//vsElasticLight1.OleObjectBlob	= "frmRemove30DNFee.frx":058A";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			this.Text = "Remove 30 Day Notice Fee";
		}

		private void frmRemove30DNFee_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				Close();
			}
			else if (KeyAscii == 13)
			{
				KeyAscii = 0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			int intErr = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsPay = new clsDRWrapper();
				clsDRWrapper rsBill = new clsDRWrapper();
				// save routine
				if (Conversion.Val(txtAccount.Text) != 0)
				{
					intErr = 1;
					if (cmbYear.SelectedIndex != -1)
					{
						intErr = 2;
						if (FCMessageBox.Show("Are you sure that you would want to remove the demand fees from this account?", MsgBoxStyle.YesNoCancel, "Remove 30 Day Notice Fees") != DialogResult.Yes)
						{
							return;
						}
						// CYA entry
						modGlobalFunctions.AddCYAEntry_80("CL", "Removing 30 DN fees.", "Account: " + FCConvert.ToString(lngAcct), "Year: " + cmbYear.Items[cmbYear.SelectedIndex].ToString());
						intErr = 3;
						// remove the fees
						// find the billing record
						rsBill.OpenRecordset("SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAcct) + " AND BillingYear = " + modExtraModules.FormatYear(cmbYear.Items[cmbYear.SelectedIndex].ToString()), modExtraModules.strCLDatabase);
						if (!rsBill.EndOfFile())
						{
							// this will find the fee from the payment rec to delete
							intErr = 4;
							rsPay.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(rsBill.Get_Fields_Int32("BillKey")) + " AND Code = '3'", modExtraModules.strCLDatabase);
							if (!rsPay.EndOfFile())
							{
								intErr = 5;
								modGlobalFunctions.AddCYAEntry_728("CL", "Removing 30 DN Record.", "EID: " + FCConvert.ToString(rsPay.Get_Fields_DateTime("EffectiveInterestDate")), "RTD: " + FCConvert.ToString(rsPay.Get_Fields_DateTime("RecordedTransactionDate")), Strings.Format(rsPay.Get_Fields_Decimal("LienCost"), "$#,##0.00"), "Bill Key = " + FCConvert.ToString(rsPay.Get_Fields_Int32("BillKey")));
								// found ... delete it
								rsPay.Delete();
                                rsPay.Update();
							}
							else
							{
								// not found...
								FCMessageBox.Show("Payment for this was not found and not deleted.", MsgBoxStyle.Exclamation, "Payment Not Found");
							}
							intErr = 6;
							// set the eligibility back to be able to run the notices/apply fees again if needed
							rsBill.Edit();
							rsBill.Set_Fields("LienStatusEligibility", 1);
							// Changed because these did not show up under 30 day notices list
							rsBill.Set_Fields("LienProcessStatus", 0);
							rsBill.Set_Fields("DemandFees", 0);
							rsBill.Update(true);
							FCMessageBox.Show("Account " + FCConvert.ToString(lngAcct) + " and year " + cmbYear.Items[cmbYear.SelectedIndex].ToString() + " has been updated.", MsgBoxStyle.Exclamation, "Finished");
							Close();
						}
						else
						{
							intErr = 7;
							FCMessageBox.Show("Error finding bill record, please try again.", MsgBoxStyle.Critical, "Missing Bill Record");
							cmbYear.Enabled = false;
							if (txtAccount.Enabled)
							{
								txtAccount.Focus();
							}
						}
					}
					else
					{
						if (cmbYear.Enabled)
						{
							FCMessageBox.Show("Please select a year.", MsgBoxStyle.Critical, "Data Input");
						}
						intErr = 8;
						// fill the grid
						FillYearCombo();
						intErr = 9;
						if (cmbYear.Items.Count > 0)
						{
							intErr = 10;
							cmbYear.Enabled = true;
							cmbYear.Focus();
						}
						else
						{
							intErr = 11;
							cmbYear.Enabled = false;
							if (txtAccount.Enabled)
							{
								txtAccount.Focus();
							}
						}
					}
				}
				else
				{
					// enter an account
					FCMessageBox.Show("Please enter a valid account.", MsgBoxStyle.Exclamation, "Valid Account");
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Removing 30 DN Fees - " + FCConvert.ToString(intErr));
			}
		}

		public void mnuFilePrint_Click()
		{
			mnuFilePrint_Click(btnProcess, new System.EventArgs());
		}

		private void txtAccount_TextChanged(object sender, System.EventArgs e)
		{
			// turn this off so that the user will have to select the year each time
			// and I will have time to fill it for each account selected
			cmbYear.SelectedIndex = -1;
			cmbYear.Enabled = false;
		}

		private void FillYearCombo()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsYear = new clsDRWrapper();
				lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(txtAccount.Text)));
				cmbYear.Clear();
				if (lngAcct > 0)
				{
					rsYear.OpenRecordset("SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAcct) + " AND DemandFees <> 0 ORDER BY BillingYear desc");
					if (!rsYear.EndOfFile())
					{
						while (!rsYear.EndOfFile())
						{
							cmbYear.AddItem(modExtraModules.FormatYear(FCConvert.ToString(rsYear.Get_Fields_Int32("BillingYear"))));
							rsYear.MoveNext();
						}
					}
					else
					{
						FCMessageBox.Show("This account does not have any 30 Day Notice Fees applied.", MsgBoxStyle.Exclamation, "Valid Account");
						cmbYear.Enabled = false;
						if (txtAccount.Enabled)
						{
							txtAccount.Focus();
						}
					}
				}
				else
				{
					FCMessageBox.Show("Please enter a valid account.", MsgBoxStyle.Exclamation, "Valid Account");
					cmbYear.Enabled = false;
					if (txtAccount.Enabled)
					{
						txtAccount.Focus();
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Filling Combo");
			}
		}

		private void txtAccount_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == (Keys)13)
			{
				// return
				mnuFilePrint_Click();
			}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFilePrint_Click(sender, e);
		}
	}
}
