﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmAuditInfo.
	/// </summary>
	partial class frmAuditInfo : BaseForm
	{
		public fecherFoundation.FCComboBox cmbRateRecType;
		public System.Collections.Generic.List<fecherFoundation.FCFrame> frPeriod;
		public System.Collections.Generic.List<T2KDateBox> t2kInterestDate;
		public System.Collections.Generic.List<T2KDateBox> t2kDueDate;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblDDate;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblIDate;
		public T2KDateBox t2kBilldate;
		public fecherFoundation.FCTextBox txtDescription;
		public fecherFoundation.FCTextBox txtInterestRate;
		public fecherFoundation.FCFrame frPeriod_3;
		public T2KDateBox t2kInterestDate_3;
		public T2KDateBox t2kDueDate_3;
		public fecherFoundation.FCLabel lblDDate_3;
		public fecherFoundation.FCLabel lblIDate_3;
		public fecherFoundation.FCFrame frPeriod_2;
		public T2KDateBox t2kInterestDate_2;
		public T2KDateBox t2kDueDate_2;
		public fecherFoundation.FCLabel lblDDate_2;
		public fecherFoundation.FCLabel lblIDate_2;
		public fecherFoundation.FCFrame frPeriod_1;
		public T2KDateBox t2kInterestDate_1;
		public T2KDateBox t2kDueDate_1;
		public fecherFoundation.FCLabel lblDDate_1;
		public fecherFoundation.FCLabel lblIDate_1;
		public fecherFoundation.FCFrame frPeriod_0;
		public T2KDateBox t2kInterestDate_0;
		public T2KDateBox t2kDueDate_0;
		public fecherFoundation.FCLabel lblDDate_0;
		public fecherFoundation.FCLabel lblIDate_0;
		public fecherFoundation.FCTextBox txtTaxYear;
		public fecherFoundation.FCTextBox txtTaxRate;
		public fecherFoundation.FCComboBox cmbPeriods;
		public T2KDateBox t2kCommitment;
		public fecherFoundation.FCLabel lblCommitmentDate;
		public fecherFoundation.FCLabel Label24;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel lblBillDate;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAuditInfo));
			this.cmbRateRecType = new fecherFoundation.FCComboBox();
			this.t2kBilldate = new Global.T2KDateBox();
			this.txtDescription = new fecherFoundation.FCTextBox();
			this.txtInterestRate = new fecherFoundation.FCTextBox();
			this.frPeriod_3 = new fecherFoundation.FCFrame();
			this.t2kInterestDate_3 = new Global.T2KDateBox();
			this.t2kDueDate_3 = new Global.T2KDateBox();
			this.lblDDate_3 = new fecherFoundation.FCLabel();
			this.lblIDate_3 = new fecherFoundation.FCLabel();
			this.frPeriod_2 = new fecherFoundation.FCFrame();
			this.t2kInterestDate_2 = new Global.T2KDateBox();
			this.t2kDueDate_2 = new Global.T2KDateBox();
			this.lblDDate_2 = new fecherFoundation.FCLabel();
			this.lblIDate_2 = new fecherFoundation.FCLabel();
			this.frPeriod_1 = new fecherFoundation.FCFrame();
			this.t2kInterestDate_1 = new Global.T2KDateBox();
			this.t2kDueDate_1 = new Global.T2KDateBox();
			this.lblDDate_1 = new fecherFoundation.FCLabel();
			this.lblIDate_1 = new fecherFoundation.FCLabel();
			this.frPeriod_0 = new fecherFoundation.FCFrame();
			this.t2kInterestDate_0 = new Global.T2KDateBox();
			this.t2kDueDate_0 = new Global.T2KDateBox();
			this.lblDDate_0 = new fecherFoundation.FCLabel();
			this.lblIDate_0 = new fecherFoundation.FCLabel();
			this.txtTaxYear = new fecherFoundation.FCTextBox();
			this.txtTaxRate = new fecherFoundation.FCTextBox();
			this.cmbPeriods = new fecherFoundation.FCComboBox();
			this.t2kCommitment = new Global.T2KDateBox();
			this.lblCommitmentDate = new fecherFoundation.FCLabel();
			this.Label24 = new fecherFoundation.FCLabel();
			this.Label7 = new fecherFoundation.FCLabel();
			this.Label6 = new fecherFoundation.FCLabel();
			this.lblBillDate = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.fcLabel1 = new fecherFoundation.FCLabel();
			this.btnProcess = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.t2kBilldate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.frPeriod_3)).BeginInit();
			this.frPeriod_3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.t2kInterestDate_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.t2kDueDate_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.frPeriod_2)).BeginInit();
			this.frPeriod_2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.t2kInterestDate_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.t2kDueDate_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.frPeriod_1)).BeginInit();
			this.frPeriod_1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.t2kInterestDate_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.t2kDueDate_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.frPeriod_0)).BeginInit();
			this.frPeriod_0.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.t2kInterestDate_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.t2kDueDate_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.t2kCommitment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 670);
			this.BottomPanel.Size = new System.Drawing.Size(608, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fcLabel1);
			this.ClientArea.Controls.Add(this.cmbRateRecType);
			this.ClientArea.Controls.Add(this.t2kBilldate);
			this.ClientArea.Controls.Add(this.txtDescription);
			this.ClientArea.Controls.Add(this.txtInterestRate);
			this.ClientArea.Controls.Add(this.frPeriod_3);
			this.ClientArea.Controls.Add(this.frPeriod_2);
			this.ClientArea.Controls.Add(this.frPeriod_1);
			this.ClientArea.Controls.Add(this.frPeriod_0);
			this.ClientArea.Controls.Add(this.txtTaxYear);
			this.ClientArea.Controls.Add(this.txtTaxRate);
			this.ClientArea.Controls.Add(this.cmbPeriods);
			this.ClientArea.Controls.Add(this.t2kCommitment);
			this.ClientArea.Controls.Add(this.lblCommitmentDate);
			this.ClientArea.Controls.Add(this.Label24);
			this.ClientArea.Controls.Add(this.Label7);
			this.ClientArea.Controls.Add(this.Label6);
			this.ClientArea.Controls.Add(this.lblBillDate);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(722, 610);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(722, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(224, 30);
			this.HeaderText.Text = "Add A Rate Record";
			// 
			// cmbRateRecType
			// 
			this.cmbRateRecType.AutoSize = false;
			this.cmbRateRecType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbRateRecType.FormattingEnabled = true;
			this.cmbRateRecType.Items.AddRange(new object[] {
				"Regular",
				"Lien",
				"Supplemental"
			});
			this.cmbRateRecType.Location = new System.Drawing.Point(179, 25);
			this.cmbRateRecType.Name = "cmbRateRecType";
			this.cmbRateRecType.Size = new System.Drawing.Size(165, 40);
			this.cmbRateRecType.TabIndex = 1;
			this.cmbRateRecType.SelectedIndexChanged += new System.EventHandler(this.optRateRecType_Click);
			// 
			// t2kBilldate
			// 
			this.t2kBilldate.MaxLength = 10;
			this.t2kBilldate.Location = new System.Drawing.Point(179, 176);
			this.t2kBilldate.Mask = "##/##/####";
			this.t2kBilldate.Name = "t2kBilldate";
			this.t2kBilldate.Size = new System.Drawing.Size(121, 40);
			this.t2kBilldate.TabIndex = 12;
			this.t2kBilldate.Text = "  /  /";
			// 
			// txtDescription
			// 
			this.txtDescription.AutoSize = false;
			this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
			this.txtDescription.Location = new System.Drawing.Point(179, 76);
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Size = new System.Drawing.Size(419, 40);
			this.txtDescription.TabIndex = 5;
			// 
			// txtInterestRate
			// 
			this.txtInterestRate.AutoSize = false;
			this.txtInterestRate.BackColor = System.Drawing.SystemColors.Window;
			this.txtInterestRate.Location = new System.Drawing.Point(477, 126);
			this.txtInterestRate.Name = "txtInterestRate";
			this.txtInterestRate.Size = new System.Drawing.Size(121, 40);
			this.txtInterestRate.TabIndex = 9;
			this.txtInterestRate.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtInterestRate_KeyPress);
			// 
			// frPeriod_3
			// 
			this.frPeriod_3.Controls.Add(this.t2kInterestDate_3);
			this.frPeriod_3.Controls.Add(this.t2kDueDate_3);
			this.frPeriod_3.Controls.Add(this.lblDDate_3);
			this.frPeriod_3.Controls.Add(this.lblIDate_3);
			this.frPeriod_3.Enabled = false;
			this.frPeriod_3.Location = new System.Drawing.Point(343, 454);
			this.frPeriod_3.Name = "frPeriod_3";
			this.frPeriod_3.Size = new System.Drawing.Size(275, 138);
			this.frPeriod_3.TabIndex = 0;
			this.frPeriod_3.Text = "Period 4";
			this.frPeriod_3.UseMnemonic = false;
			// 
			// t2kInterestDate_3
			// 
			this.t2kInterestDate_3.MaxLength = 10;
			this.t2kInterestDate_3.Location = new System.Drawing.Point(135, 30);
			this.t2kInterestDate_3.Mask = "##/##/####";
			this.t2kInterestDate_3.Name = "t2kInterestDate_3";
			this.t2kInterestDate_3.Size = new System.Drawing.Size(121, 40);
			this.t2kInterestDate_3.TabIndex = 1;
			this.t2kInterestDate_3.Text = "  /  /";
			// 
			// t2kDueDate_3
			// 
			this.t2kDueDate_3.MaxLength = 10;
			this.t2kDueDate_3.Location = new System.Drawing.Point(135, 80);
			this.t2kDueDate_3.Mask = "##/##/####";
			this.t2kDueDate_3.Name = "t2kDueDate_3";
			this.t2kDueDate_3.Size = new System.Drawing.Size(121, 40);
			this.t2kDueDate_3.TabIndex = 3;
			this.t2kDueDate_3.Text = "  /  /";
			// 
			// lblDDate_3
			// 
			this.lblDDate_3.AutoSize = true;
			this.lblDDate_3.Location = new System.Drawing.Point(20, 94);
			this.lblDDate_3.Name = "lblDDate_3";
			this.lblDDate_3.Size = new System.Drawing.Size(70, 16);
			this.lblDDate_3.TabIndex = 2;
			this.lblDDate_3.Text = "DUE DATE";
			this.lblDDate_3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblIDate_3
			// 
			this.lblIDate_3.AutoSize = true;
			this.lblIDate_3.Location = new System.Drawing.Point(20, 44);
			this.lblIDate_3.Name = "lblIDate_3";
			this.lblIDate_3.Size = new System.Drawing.Size(105, 16);
			this.lblIDate_3.TabIndex = 0;
			this.lblIDate_3.Text = "INTEREST DATE";
			this.lblIDate_3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// frPeriod_2
			// 
			this.frPeriod_2.Controls.Add(this.t2kInterestDate_2);
			this.frPeriod_2.Controls.Add(this.t2kDueDate_2);
			this.frPeriod_2.Controls.Add(this.lblDDate_2);
			this.frPeriod_2.Controls.Add(this.lblIDate_2);
			this.frPeriod_2.Enabled = false;
			this.frPeriod_2.Location = new System.Drawing.Point(30, 454);
			this.frPeriod_2.Name = "frPeriod_2";
			this.frPeriod_2.Size = new System.Drawing.Size(275, 138);
			this.frPeriod_2.TabIndex = 19;
			this.frPeriod_2.Text = "Period 3";
			this.frPeriod_2.UseMnemonic = false;
			// 
			// t2kInterestDate_2
			// 
			this.t2kInterestDate_2.MaxLength = 10;
			this.t2kInterestDate_2.Location = new System.Drawing.Point(135, 30);
			this.t2kInterestDate_2.Mask = "##/##/####";
			this.t2kInterestDate_2.Name = "t2kInterestDate_2";
			this.t2kInterestDate_2.Size = new System.Drawing.Size(121, 40);
			this.t2kInterestDate_2.TabIndex = 1;
			this.t2kInterestDate_2.Text = "  /  /";
			// 
			// t2kDueDate_2
			// 
			this.t2kDueDate_2.MaxLength = 10;
			this.t2kDueDate_2.Location = new System.Drawing.Point(135, 80);
			this.t2kDueDate_2.Mask = "##/##/####";
			this.t2kDueDate_2.Name = "t2kDueDate_2";
			this.t2kDueDate_2.Size = new System.Drawing.Size(121, 40);
			this.t2kDueDate_2.TabIndex = 3;
			this.t2kDueDate_2.Text = "  /  /";
			// 
			// lblDDate_2
			// 
			this.lblDDate_2.AutoSize = true;
			this.lblDDate_2.Location = new System.Drawing.Point(20, 94);
			this.lblDDate_2.Name = "lblDDate_2";
			this.lblDDate_2.Size = new System.Drawing.Size(70, 16);
			this.lblDDate_2.TabIndex = 2;
			this.lblDDate_2.Text = "DUE DATE";
			this.lblDDate_2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblIDate_2
			// 
			this.lblIDate_2.AutoSize = true;
			this.lblIDate_2.Location = new System.Drawing.Point(20, 44);
			this.lblIDate_2.Name = "lblIDate_2";
			this.lblIDate_2.Size = new System.Drawing.Size(105, 16);
			this.lblIDate_2.TabIndex = 0;
			this.lblIDate_2.Text = "INTEREST DATE";
			this.lblIDate_2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// frPeriod_1
			// 
			this.frPeriod_1.Controls.Add(this.t2kInterestDate_1);
			this.frPeriod_1.Controls.Add(this.t2kDueDate_1);
			this.frPeriod_1.Controls.Add(this.lblDDate_1);
			this.frPeriod_1.Controls.Add(this.lblIDate_1);
			this.frPeriod_1.Enabled = false;
			this.frPeriod_1.Location = new System.Drawing.Point(343, 291);
			this.frPeriod_1.Name = "frPeriod_1";
			this.frPeriod_1.Size = new System.Drawing.Size(275, 138);
			this.frPeriod_1.TabIndex = 18;
			this.frPeriod_1.Text = "Period 2";
			this.frPeriod_1.UseMnemonic = false;
			// 
			// t2kInterestDate_1
			// 
			this.t2kInterestDate_1.MaxLength = 10;
			this.t2kInterestDate_1.Location = new System.Drawing.Point(135, 30);
			this.t2kInterestDate_1.Mask = "##/##/####";
			this.t2kInterestDate_1.Name = "t2kInterestDate_1";
			this.t2kInterestDate_1.Size = new System.Drawing.Size(121, 40);
			this.t2kInterestDate_1.TabIndex = 1;
			this.t2kInterestDate_1.Text = "  /  /";
			// 
			// t2kDueDate_1
			// 
			this.t2kDueDate_1.MaxLength = 10;
			this.t2kDueDate_1.Location = new System.Drawing.Point(135, 80);
			this.t2kDueDate_1.Mask = "##/##/####";
			this.t2kDueDate_1.Name = "t2kDueDate_1";
			this.t2kDueDate_1.Size = new System.Drawing.Size(121, 40);
			this.t2kDueDate_1.TabIndex = 3;
			this.t2kDueDate_1.Text = "  /  /";
			// 
			// lblDDate_1
			// 
			this.lblDDate_1.AutoSize = true;
			this.lblDDate_1.Location = new System.Drawing.Point(20, 94);
			this.lblDDate_1.Name = "lblDDate_1";
			this.lblDDate_1.Size = new System.Drawing.Size(70, 16);
			this.lblDDate_1.TabIndex = 2;
			this.lblDDate_1.Text = "DUE DATE";
			this.lblDDate_1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblIDate_1
			// 
			this.lblIDate_1.AutoSize = true;
			this.lblIDate_1.Location = new System.Drawing.Point(20, 44);
			this.lblIDate_1.Name = "lblIDate_1";
			this.lblIDate_1.Size = new System.Drawing.Size(105, 16);
			this.lblIDate_1.TabIndex = 0;
			this.lblIDate_1.Text = "INTEREST DATE";
			this.lblIDate_1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// frPeriod_0
			// 
			this.frPeriod_0.Controls.Add(this.t2kInterestDate_0);
			this.frPeriod_0.Controls.Add(this.t2kDueDate_0);
			this.frPeriod_0.Controls.Add(this.lblDDate_0);
			this.frPeriod_0.Controls.Add(this.lblIDate_0);
			this.frPeriod_0.Location = new System.Drawing.Point(30, 291);
			this.frPeriod_0.Name = "frPeriod_0";
			this.frPeriod_0.Size = new System.Drawing.Size(275, 138);
			this.frPeriod_0.TabIndex = 17;
			this.frPeriod_0.Text = "Period 1";
			this.frPeriod_0.UseMnemonic = false;
			// 
			// t2kInterestDate_0
			// 
			this.t2kInterestDate_0.MaxLength = 10;
			this.t2kInterestDate_0.Location = new System.Drawing.Point(135, 30);
			this.t2kInterestDate_0.Mask = "##/##/####";
			this.t2kInterestDate_0.Name = "t2kInterestDate_0";
			this.t2kInterestDate_0.Size = new System.Drawing.Size(121, 40);
			this.t2kInterestDate_0.TabIndex = 1;
			this.t2kInterestDate_0.Text = "  /  /";
			// 
			// t2kDueDate_0
			// 
			this.t2kDueDate_0.MaxLength = 10;
			this.t2kDueDate_0.Location = new System.Drawing.Point(135, 80);
			this.t2kDueDate_0.Mask = "##/##/####";
			this.t2kDueDate_0.Name = "t2kDueDate_0";
			this.t2kDueDate_0.Size = new System.Drawing.Size(121, 40);
			this.t2kDueDate_0.TabIndex = 3;
			this.t2kDueDate_0.Text = "  /  /";
			// 
			// lblDDate_0
			// 
			this.lblDDate_0.AutoSize = true;
			this.lblDDate_0.Location = new System.Drawing.Point(20, 94);
			this.lblDDate_0.Name = "lblDDate_0";
			this.lblDDate_0.Size = new System.Drawing.Size(70, 16);
			this.lblDDate_0.TabIndex = 2;
			this.lblDDate_0.Text = "DUE DATE";
			this.lblDDate_0.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblIDate_0
			// 
			this.lblIDate_0.AutoSize = true;
			this.lblIDate_0.Location = new System.Drawing.Point(20, 44);
			this.lblIDate_0.Name = "lblIDate_0";
			this.lblIDate_0.Size = new System.Drawing.Size(105, 16);
			this.lblIDate_0.TabIndex = 0;
			this.lblIDate_0.Text = "INTEREST DATE";
			this.lblIDate_0.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtTaxYear
			// 
			this.txtTaxYear.MaxLength = 4;
			this.txtTaxYear.AutoSize = false;
			this.txtTaxYear.BackColor = System.Drawing.SystemColors.Window;
			this.txtTaxYear.Location = new System.Drawing.Point(477, 25);
			this.txtTaxYear.Name = "txtTaxYear";
			this.txtTaxYear.Size = new System.Drawing.Size(121, 40);
			this.txtTaxYear.TabIndex = 3;
			this.txtTaxYear.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtTaxYear_KeyPress);
			this.txtTaxYear.Enter += new System.EventHandler(this.txtTaxYear_Enter);
			// 
			// txtTaxRate
			// 
			this.txtTaxRate.AutoSize = false;
			this.txtTaxRate.BackColor = System.Drawing.SystemColors.Window;
			this.txtTaxRate.Location = new System.Drawing.Point(179, 126);
			this.txtTaxRate.Name = "txtTaxRate";
			this.txtTaxRate.Size = new System.Drawing.Size(121, 40);
			this.txtTaxRate.TabIndex = 7;
			this.txtTaxRate.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtTaxRate_KeyPress);
			this.txtTaxRate.Validating += new System.ComponentModel.CancelEventHandler(this.txtTaxRate_Validating);
			// 
			// cmbPeriods
			// 
			this.cmbPeriods.AutoSize = false;
			this.cmbPeriods.BackColor = System.Drawing.SystemColors.Window;
			this.cmbPeriods.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbPeriods.FormattingEnabled = true;
			this.cmbPeriods.Location = new System.Drawing.Point(229, 228);
			this.cmbPeriods.Name = "cmbPeriods";
			this.cmbPeriods.Size = new System.Drawing.Size(71, 40);
			this.cmbPeriods.Sorted = true;
			this.cmbPeriods.TabIndex = 16;
			this.cmbPeriods.SelectedIndexChanged += new System.EventHandler(this.cmbPeriods_SelectedIndexChanged);
			// 
			// t2kCommitment
			// 
			this.t2kCommitment.MaxLength = 10;
			this.t2kCommitment.Location = new System.Drawing.Point(477, 176);
			this.t2kCommitment.Mask = "##/##/####";
			this.t2kCommitment.Name = "t2kCommitment";
			this.t2kCommitment.Size = new System.Drawing.Size(121, 40);
			this.t2kCommitment.TabIndex = 14;
			this.t2kCommitment.Text = "  /  /";
			// 
			// lblCommitmentDate
			// 
			this.lblCommitmentDate.AutoSize = true;
			this.lblCommitmentDate.Location = new System.Drawing.Point(344, 190);
			this.lblCommitmentDate.Name = "lblCommitmentDate";
			this.lblCommitmentDate.Size = new System.Drawing.Size(129, 16);
			this.lblCommitmentDate.TabIndex = 13;
			this.lblCommitmentDate.Text = "COMMITMENT DATE";
			this.lblCommitmentDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label24
			// 
			this.Label24.AutoSize = true;
			this.Label24.Location = new System.Drawing.Point(30, 90);
			this.Label24.Name = "Label24";
			this.Label24.Size = new System.Drawing.Size(136, 16);
			this.Label24.TabIndex = 4;
			this.Label24.Text = "DESCRIPTION / TITLE";
			this.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label7
			// 
			this.Label7.AutoSize = true;
			this.Label7.Location = new System.Drawing.Point(344, 140);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(105, 16);
			this.Label7.TabIndex = 8;
			this.Label7.Text = "INTEREST RATE";
			this.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label6
			// 
			this.Label6.AutoSize = true;
			this.Label6.Location = new System.Drawing.Point(397, 39);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(68, 16);
			this.Label6.TabIndex = 2;
			this.Label6.Text = "TAX YEAR";
			this.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblBillDate
			// 
			this.lblBillDate.AutoSize = true;
			this.lblBillDate.Location = new System.Drawing.Point(30, 190);
			this.lblBillDate.Name = "lblBillDate";
			this.lblBillDate.Size = new System.Drawing.Size(69, 16);
			this.lblBillDate.TabIndex = 10;
			this.lblBillDate.Text = "BILL DATE";
			this.lblBillDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label2
			// 
			this.Label2.AutoSize = true;
			this.Label2.Location = new System.Drawing.Point(30, 242);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(168, 16);
			this.Label2.TabIndex = 15;
			this.Label2.Text = "NUMBER OF PAY PERIODS";
			this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label1
			// 
			this.Label1.AutoSize = true;
			this.Label1.Location = new System.Drawing.Point(30, 140);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(68, 16);
			this.Label1.TabIndex = 6;
			this.Label1.Text = "TAX RATE";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.Text = "File";
			// 
			// fcLabel1
			// 
			this.fcLabel1.AutoSize = true;
			this.fcLabel1.Location = new System.Drawing.Point(30, 39);
			this.fcLabel1.Name = "fcLabel1";
			this.fcLabel1.Size = new System.Drawing.Size(105, 16);
			this.fcLabel1.TabIndex = 0;
			this.fcLabel1.Text = "RATE REC TYPE";
			this.fcLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(243, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(121, 48);
			this.btnProcess.TabIndex = 2;
			this.btnProcess.Text = "Save";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// frmAuditInfo
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(725, 778);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmAuditInfo";
			this.Text = "Add A Rate Record";
			this.Load += new System.EventHandler(this.frmAuditInfo_Load);
			this.Activated += new System.EventHandler(this.frmAuditInfo_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmAuditInfo_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmAuditInfo_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.t2kBilldate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.frPeriod_3)).EndInit();
			this.frPeriod_3.ResumeLayout(false);
			this.frPeriod_3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.t2kInterestDate_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.t2kDueDate_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.frPeriod_2)).EndInit();
			this.frPeriod_2.ResumeLayout(false);
			this.frPeriod_2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.t2kInterestDate_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.t2kDueDate_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.frPeriod_1)).EndInit();
			this.frPeriod_1.ResumeLayout(false);
			this.frPeriod_1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.t2kInterestDate_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.t2kDueDate_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.frPeriod_0)).EndInit();
			this.frPeriod_0.ResumeLayout(false);
			this.frPeriod_0.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.t2kInterestDate_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.t2kDueDate_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.t2kCommitment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		public FCLabel fcLabel1;
		private FCButton btnProcess;
	}
}
