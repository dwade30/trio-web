//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmTaxClubOutstanding.
	/// </summary>
	partial class frmTaxClubOutstanding : BaseForm
    {
		//public System.Collections.Generic.List<fecherFoundation.FCRadioButton> optChoice;
		//public System.Collections.Generic.List<fecherFoundation.FCCheckBox> chkSummaryOnly;
		//public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtNumberOfPayments;
		//public fecherFoundation.FCFrame fraChoice;
     
        public fecherFoundation.FCCheckBox chkSummaryOnly_0;
		public fecherFoundation.FCTextBox txtNumberOfPayments_3;
		//public fecherFoundation.FCPictureBox vsElasticLight1;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		//public fecherFoundation.FCToolStripMenuItem mnuFile;
		//public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		//public fecherFoundation.FCToolStripMenuItem Seperator;
		//public fecherFoundation.FCToolStripMenuItem mnuFileExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				Sys.ClearInstance(this);
				_InstancePtr = null;
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbAccounts = new fecherFoundation.FCComboBox();
            this.chkSummaryOnly_0 = new fecherFoundation.FCCheckBox();
            this.txtNumberOfPayments_3 = new fecherFoundation.FCTextBox();
            this.btnProcess = new fecherFoundation.FCButton();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSummaryOnly_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 303);
            this.BottomPanel.Size = new System.Drawing.Size(480, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.btnProcess);
            this.ClientArea.Controls.Add(this.cmbAccounts);
            this.ClientArea.Controls.Add(this.chkSummaryOnly_0);
            this.ClientArea.Controls.Add(this.txtNumberOfPayments_3);
            this.ClientArea.Size = new System.Drawing.Size(480, 243);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(480, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(350, 30);
            this.HeaderText.Text = "Print Tax Club Overdue Report";
            // 
            // cmbAccounts
            // 
            this.cmbAccounts.Items.AddRange(new object[] {
            "All Accounts",
            "Number of Payments in Arrears",
            "Number of Payments or More in Arrears"});
            this.cmbAccounts.Location = new System.Drawing.Point(30, 30);
            this.cmbAccounts.Name = "cmbAccounts";
            this.cmbAccounts.Size = new System.Drawing.Size(384, 40);
            this.cmbAccounts.TabIndex = 7;
            // 
            // chkSummaryOnly_0
            // 
            this.chkSummaryOnly_0.ForeColor = System.Drawing.SystemColors.ControlText;
            this.chkSummaryOnly_0.Location = new System.Drawing.Point(140, 97);
            this.chkSummaryOnly_0.Name = "chkSummaryOnly_0";
            this.chkSummaryOnly_0.Size = new System.Drawing.Size(160, 24);
            this.chkSummaryOnly_0.TabIndex = 6;
            this.chkSummaryOnly_0.Text = "Show Summary Only";
            // 
            // txtNumberOfPayments_3
            // 
            this.txtNumberOfPayments_3.BackColor = System.Drawing.SystemColors.Window;
            this.txtNumberOfPayments_3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtNumberOfPayments_3.Location = new System.Drawing.Point(30, 90);
            this.txtNumberOfPayments_3.Name = "txtNumberOfPayments_3";
            this.txtNumberOfPayments_3.Size = new System.Drawing.Size(63, 40);
            this.txtNumberOfPayments_3.TabIndex = 5;
            this.txtNumberOfPayments_3.Text = "1";
            this.txtNumberOfPayments_3.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Location = new System.Drawing.Point(30, 150);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcess.Size = new System.Drawing.Size(120, 48);
            this.btnProcess.TabIndex = 2;
            this.btnProcess.Text = "Print";
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // frmTaxClubOutstanding
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(480, 411);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmTaxClubOutstanding";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Print Tax Club Overdue Report";
            this.Load += new System.EventHandler(this.frmTaxClubOutstanding_Load);
            this.Activated += new System.EventHandler(this.frmTaxClubOutstanding_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmTaxClubOutstanding_KeyPress);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSummaryOnly_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
            this.ResumeLayout(false);

		}


        #endregion

        private FCComboBox cmbAccounts;
        private FCButton btnProcess;
    }
}