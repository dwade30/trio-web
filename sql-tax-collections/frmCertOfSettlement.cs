﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmCertOfSettlement.
	/// </summary>
	public partial class frmCertOfSettlement : BaseForm
	{
		public frmCertOfSettlement()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCertOfSettlement InstancePtr
		{
			get
			{
				return (frmCertOfSettlement)Sys.GetInstance(typeof(frmCertOfSettlement));
			}
		}

		protected frmCertOfSettlement _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               12/31/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               02/13/2006              *
		// ********************************************************
		bool boolLoaded;
		int lngLienRecordNumber;
		int intReportType;
		// grid cols
		int lngColData;
		int lngColTitle;
		int lngColHidden;
		// grid rows
		int lngRowCollector;
		// Row - 0  'Collector Name
		// Dim lngRowTName                 As Long 'Row - 1  'Treasurer Name
		int lngRowMuni;
		// Row - 2  'Muni Name
		int lngRowCounty;
		// Row - 3  'County
		int lngRowRegistryCounty;
		// Row - 4  'County Of Registry
		int lngRowLegalDescription;
		// Row - 5  'Legal Description
		int lngRowTaxYear;
		// Row - 6  'Tax Year
		// Dim lngRowInterestRate          As Long 'Row - 7  'Interest Rate
		int lngRowDateSigned;
		// Row - 8  'Date Signed
		// Dim lngRowEndingDate            As Long 'Row - 9  'Ending Date
		public void Init(short intType)
		{
			intReportType = intType;
			lngColData = 2;
			lngColHidden = 1;
			lngColTitle = 0;
			lngRowCollector = 0;
			lngRowMuni = 1;
			// Muni Name
			lngRowCounty = 2;
			// County
			lngRowRegistryCounty = 3;
			// County Of Registry
			// lngRowLegalDescription = 4  'Legal Description
			switch (intType)
			{
				case 0:
					{
						lngRowTaxYear = 4;
						// Tax Year
						lngRowDateSigned = 5;
						// Date Signed
						this.Text = "Certificate Of Settlement";
                        this.HeaderText.Text = "Certificate Of Settlement";

                        break;
					}
				case 1:
					{
						lngRowTaxYear = 5;
						// Tax Year
						lngRowDateSigned = 4;
						// Date Signed
						this.Text = "Certificate Of Recommitment";
                        this.HeaderText.Text = "Certificate Of Recommitment";

                        break;
					}
			}
			//end switch
			// lngRowEndingDate = 9        'Ending Date
			// lngRowTName = 1             'Treasurer Name
			// lngRowInterestRate = 7      'Interest Date
			this.Show(App.MainForm);
			FormatGrid(true);
			LoadSettings();
		}

		private void frmCertOfSettlement_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				boolLoaded = true;
			}
		}

		private void frmCertOfSettlement_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCertOfSettlement.Icon	= "frmCertOfSettlement.frx":0000";
			//frmCertOfSettlement.FillStyle	= 0;
			//frmCertOfSettlement.ScaleWidth	= 5880;
			//frmCertOfSettlement.ScaleHeight	= 4065;
			//frmCertOfSettlement.LinkTopic	= "Form2";
			//frmCertOfSettlement.LockControls	= -1  'True;
			//frmCertOfSettlement.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "9.75";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//vsData.BackColor	= "-2147483643";
			//			//vsData.ForeColor	= "-2147483640";
			//vsData.BorderStyle	= 1;
			//vsData.FillStyle	= 0;
			//vsData.Appearance	= 1;
			//vsData.GridLines	= 1;
			//vsData.WordWrap	= 0;
			//vsData.ScrollBars	= 2;
			//vsData.RightToLeft	= 0;
			//vsData._cx	= 9551;
			//vsData._cy	= 4941;
			//vsData._ConvInfo	= 1;
			//vsData.MousePointer	= 0;
			//vsData.BackColorFixed	= -2147483633;
			//			//vsData.ForeColorFixed	= -2147483630;
			//vsData.BackColorSel	= -2147483635;
			//			//vsData.ForeColorSel	= -2147483634;
			//vsData.BackColorBkg	= -2147483636;
			//vsData.BackColorAlternate	= -2147483643;
			//vsData.GridColor	= -2147483633;
			//vsData.GridColorFixed	= -2147483632;
			//vsData.TreeColor	= -2147483632;
			//vsData.FloodColor	= 192;
			//vsData.SheetBorder	= -2147483642;
			//vsData.FocusRect	= 1;
			//vsData.HighLight	= 1;
			//vsData.AllowSelection	= -1  'True;
			//vsData.AllowBigSelection	= -1  'True;
			//vsData.AllowUserResizing	= 0;
			//vsData.SelectionMode	= 1;
			//vsData.GridLinesFixed	= 2;
			//vsData.GridLineWidth	= 1;
			//vsData.RowHeightMin	= 0;
			//vsData.RowHeightMax	= 0;
			//vsData.ColWidthMin	= 0;
			//vsData.ColWidthMax	= 0;
			//vsData.ExtendLastCol	= -1  'True;
			//vsData.FormatString	= "";
			//vsData.ScrollTrack	= -1  'True;
			//vsData.ScrollTips	= 0   'False;
			//vsData.MergeCells	= 0;
			//vsData.MergeCompare	= 0;
			//vsData.AutoResize	= -1  'True;
			//vsData.AutoSizeMode	= 0;
			//vsData.AutoSearch	= 0;
			//vsData.AutoSearchDelay	= 2;
			//vsData.MultiTotals	= -1  'True;
			//vsData.SubtotalPosition	= 1;
			//vsData.OutlineBar	= 0;
			//vsData.OutlineCol	= 0;
			//vsData.Ellipsis	= 0;
			//vsData.ExplorerBar	= 0;
			//vsData.PicturesOver	= 0   'False;
			//vsData.PictureType	= 0;
			//vsData.TabBehavior	= 0;
			//vsData.OwnerDraw	= 0;
			//vsData.ShowComboButton	= -1  'True;
			//vsData.TextStyle	= 0;
			//vsData.TextStyleFixed	= 0;
			//vsData.OleDragMode	= 0;
			//vsData.OleDropMode	= 0;
			//vsData.DataMode	= 0;
			//vsData.VirtualData	= -1  'True;
			//vsData.ComboSearch	= 3;
			//vsData.AutoSizeMouse	= -1  'True;
			//vsData.AllowUserFreezing	= 0;
			//vsData.BackColorFrozen	= 0;
			//			//vsData.ForeColorFrozen	= 0;
			//vsData.WallPaperAlignment	= 9;
			//vsElasticLight1.OleObjectBlob	= "frmCertOfSettlement.frx":058A";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			boolLoaded = true;
			//FC:FINAL:AM: method called from Init too
			//FormatGrid(true);
		}

		private void frmCertOfSettlement_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
            FormUtilities.KeyPressHandler(e, this);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		//private void frmCertOfSettlement_Resize(object sender, System.EventArgs e)
		//{
		//	if (boolLoaded)
		//	{
		//		FormatGrid();
		//	}
		//}
		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void SaveSettings()
		{
			// this will save all of the settings for the lien discharge notice
			// VBto upgrade warning: dtTemp As DateTime	OnWrite(short, DateTime)
			DateTime dtTemp;
			if (ValidateAnswers())
			{
				// loads the certificate of settlement
				vsData.Select(0, 0);
				// add a CYA entry
				// AddCYAEntry "CL", "Certificate Of Settlement", vsData.TextMatrix(lngRowTName, lngColData), vsData.TextMatrix(lngRowDateSigned, lngColData), vsData.TextMatrix(lngRowMuni, lngColData)
				if (Strings.Trim(vsData.TextMatrix(lngRowDateSigned, lngColData)) == "" || Strings.Trim(vsData.TextMatrix(lngRowDateSigned, lngColData)) == "/  /")
				{
					dtTemp = DateTime.FromOADate(0);
				}
				else
				{
					if (Information.IsDate(vsData.TextMatrix(lngRowDateSigned, lngColData)))
					{
						dtTemp = DateAndTime.DateValue(vsData.TextMatrix(lngRowDateSigned, lngColData));
					}
					else
					{
						dtTemp = DateTime.FromOADate(0);
					}
				}
				switch (intReportType)
				{
					case 0:
						{
							// run the report
							rptCertificateOfSettlement.InstancePtr.Init(Strings.Trim(vsData.TextMatrix(lngRowCollector, lngColData)), Strings.Trim(vsData.TextMatrix(lngRowMuni, lngColData)), Strings.Trim(vsData.TextMatrix(lngRowCounty, lngColData)), Strings.Trim(vsData.TextMatrix(lngRowRegistryCounty, lngColData)), "", vsData.TextMatrix(lngRowTaxYear, lngColData), dtTemp);
							// , Trim(.TextMatrix(lngRowEndingDate, lngColData)), Trim(.TextMatrix(lngRowTName, lngColData)), Trim(.TextMatrix(lngRowInterestRate, lngColData)), Trim(.TextMatrix(lngRowLegalDescription, lngColData))
							frmReportViewer.InstancePtr.Init(rptCertificateOfSettlement.InstancePtr);
							break;
						}
					case 1:
						{
							// run the report
							rptCertificateOfRecommitment.InstancePtr.Init(Strings.Trim(vsData.TextMatrix(lngRowCollector, lngColData)), Strings.Trim(vsData.TextMatrix(lngRowMuni, lngColData)), Strings.Trim(vsData.TextMatrix(lngRowCounty, lngColData)), Strings.Trim(vsData.TextMatrix(lngRowRegistryCounty, lngColData)), "", "", dtTemp);
							frmReportViewer.InstancePtr.Init(rptCertificateOfRecommitment.InstancePtr);
							break;
						}
				}
				//end switch
			}
		}

		private void LoadSettings()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will load the settings from the database
				// Tax Collector
				vsData.TextMatrix(lngRowMuni, lngColData, "");
				// Muni Name
				vsData.TextMatrix(lngRowMuni, lngColData, modGlobalConstants.Statics.MuniName);
				// County
				vsData.TextMatrix(lngRowCounty, lngColData, "");
				// Registry County
				vsData.TextMatrix(lngRowRegistryCounty, lngColData, "");
				if (intReportType == 0)
				{
					// Tax Year
					vsData.TextMatrix(lngRowTaxYear, lngColData, "");
				}
				// Date Signed
				vsData.TextMatrix(lngRowDateSigned, lngColData, Strings.Format(DateTime.Today, "MM/dd/yyyy"));
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Loading Settings");
			}
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			SaveSettings();
			// this will keep the data and print the lien discharge notice
		}

		private void FormatGrid(bool boolReset = false)
		{
			int lngWid = 0;
			lngWid = vsData.WidthOriginal;
			if (boolReset)
			{
				vsData.Rows = 0;
				switch (intReportType)
				{
					case 0:
						{
							vsData.Rows = 6;
							break;
						}
					case 1:
						{
							vsData.Rows = 5;
							break;
						}
				}
				//end switch
			}
			vsData.ColWidth(lngColTitle, FCConvert.ToInt32(lngWid * 0.38));
			// title
			vsData.ColWidth(lngColHidden, 0);
			// hidden row
			vsData.ColWidth(lngColData, FCConvert.ToInt32(lngWid * 0.62));
			// data
			vsData.TextMatrix(lngRowCollector, lngColTitle, "Collector Name");
			vsData.TextMatrix(lngRowMuni, lngColTitle, "Municipality");
			vsData.TextMatrix(lngRowCounty, lngColTitle, "County");
			vsData.TextMatrix(lngRowRegistryCounty, lngColTitle, "County Of Registry");
           if (intReportType == 0)
			{
				vsData.TextMatrix(lngRowTaxYear, lngColTitle, "Tax Year");
			}
			vsData.TextMatrix(lngRowDateSigned, lngColTitle, "Date To Be Signed");
            // set the grid height
            //if ((vsData.Rows * vsData.RowHeight(0)) + 80 > (fraVariables.Height * 0.9))
            //{
            //	vsData.Height = FCConvert.ToInt32((fraVariables.Height * 0.9));
            //	vsData.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
            //}
            //else
            //{
            //	vsData.Height = (vsData.Rows * vsData.RowHeight(0)) + 80;
            //	vsData.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
            //}
            vsData.Height = vsData.Rows * 40 + 2;
        }

		private void vsData_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			// this will empty the grid if this is left blank
			string strTemp;
			strTemp = Strings.Trim(vsData.TextMatrix(vsData.Row, lngColData));
			if (vsData.Row == lngRowTaxYear)
			{
				if (strTemp == "____")
				{
					vsData.TextMatrix(vsData.Row, lngColData, string.Empty);
				}
			}
			if (vsData.Row == lngRowDateSigned)
			{
				if (strTemp == "__/__/____")
				{
					vsData.TextMatrix(vsData.Row, lngColData, string.Empty);
				}
			}
		}

		private void vsData_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			vsData.EditMask = string.Empty;
			if (vsData.Row == lngRowTaxYear)
			{
				//FC:FINAL:CHN: Fix format to not allow to input Space, plus and minus symbols.
				// vsData.EditMask = "####";
				vsData.EditMask = "0000";
			}
			else if (vsData.Row == lngRowDateSigned)
			{
				// , lngRowEndingDate
				//FC:FINAL:CHN: Fix date format to not allow to input Space, plus and minus symbols.
				// vsData.EditMask = "##/##/####";
				vsData.EditMask = "00/00/0000";
			}
		}

		private void vsData_KeyPressEdit(object sender, KeyEventArgs e)
		{
			Keys KeyAscii = e.KeyCode;
			if (vsData.Row == lngRowTaxYear || vsData.Row == lngRowDateSigned)
			{
				// , lngRowEndingDate, lngRowInterestRate
				// only allow numbers
				if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back) || (KeyAscii == Keys.Decimal) || (FCConvert.ToInt32(KeyAscii) == 46))
				{
					// do nothing
				}
				else
				{
					KeyAscii = 0;
				}
			}
			else
			{
			}
		}

        //private void vsData_MouseMove(object sender, DataGridViewCellMouseEventArgs e)
        //{
        //	int lngMR;
        //	lngMR = vsData.MouseRow;
        //	if (lngMR == lngRowCollector)
        //	{
        //		ToolTip1.SetToolTip(vsData, "Name of the Tax Collector.");
        //		// Case lngRowTName
        //		// vsData.ToolTipText = "Name of the treasurer or the successor in office."
        //	}
        //	else if (lngMR == lngRowMuni)
        //	{

        //		ToolTip1.SetToolTip(vsData, "");
        //	}
        //	else if (lngMR == lngRowCounty)
        //	{
        //		ToolTip1.SetToolTip(vsData, "County that your municipality is located.");
        //	}
        //	else if (lngMR == lngRowRegistryCounty)
        //	{
        //		ToolTip1.SetToolTip(vsData, "County in which your liens are filed.");
        //		// Case lngRowLegalDescription
        //		// vsData.ToolTipText = ""
        //	}
        //	else if (lngMR == lngRowTaxYear)
        //	{
        //		ToolTip1.SetToolTip(vsData, "Tax year that is being settled.");
        //		// Case lngRowInterestRate
        //		// vsData.ToolTipText = ""
        //	}
        //	else if (lngMR == lngRowDateSigned)
        //	{
        //		ToolTip1.SetToolTip(vsData, "Date that this certificate will be signed.");
        //		// Case lngRowEndingDate
        //		// vsData.ToolTipText = "Date when the treasurer must complete an account of collections."
        //	}
        //	else
        //	{
        //		ToolTip1.SetToolTip(vsData, "");
        //	}
        //}

        private void VsData_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            int lngMR;
            lngMR = vsData.GetFlexRowIndex(e.RowIndex);
            if (vsData.IsValidCell(e.ColumnIndex, e.RowIndex))
            {
                DataGridViewCell cell = vsData[e.ColumnIndex, e.RowIndex];
                if (lngMR == lngRowCollector)
                {
                    cell.ToolTipText = "Name of the Tax Collector.";

                    // Case lngRowTName
                    // vsData.ToolTipText = "Name of the treasurer or the successor in office."
                }
                else if (lngMR == lngRowMuni)
                {
                    cell.ToolTipText = "";
                }
                else if (lngMR == lngRowCounty)
                {
                    cell.ToolTipText = "County that your municipality is located.";
                }
                else if (lngMR == lngRowRegistryCounty)
                {
                    cell.ToolTipText = "County in which your liens are filed.";
                    // Case lngRowLegalDescription
                    // vsData.ToolTipText = ""
                }
                else if (lngMR == lngRowTaxYear)
                {
                    cell.ToolTipText = "Tax year that is being settled.";
                    // Case lngRowInterestRate
                    // vsData.ToolTipText = ""
                }
                else if (lngMR == lngRowDateSigned)
                {
                    cell.ToolTipText = "Date that this certificate will be signed.";
                    // Case lngRowEndingDate
                    // vsData.ToolTipText = "Date when the treasurer must complete an account of collections."
                }
                else
                {
                    cell.ToolTipText = "";
                }
            }
        }

        private void vsData_RowColChange(object sender, EventArgs e)
		{
			if (vsData.Col == vsData.Cols - 1)
			{
				if (vsData.Row == vsData.Rows - 1)
				{
					// this is the last row and will need to tab to the next control rather than the next cell
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
				}
				else
				{
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
			}
			else
			{
				vsData.Editable = FCGrid.EditableSettings.flexEDNone;
				vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private bool ValidateAnswers()
		{
			bool ValidateAnswers = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				ValidateAnswers = true;
				vsData.Select(0, 1);
				// MAL@20070907: Changed Message Captions from "Validation"
				// Collector Name
				if (Strings.Trim(vsData.TextMatrix(lngRowCollector, lngColData)) == "")
				{
					ValidateAnswers = false;
					FCMessageBox.Show("Please enter the Tax Collector's name.", MsgBoxStyle.Exclamation, "Missing Information");
					vsData.Select(lngRowCollector, lngColData);
					return ValidateAnswers;
				}
				// Muni Name
				if (Strings.Trim(vsData.TextMatrix(lngRowMuni, lngColData)) == "")
				{
					ValidateAnswers = false;
					FCMessageBox.Show("Please enter the Municipality name.", MsgBoxStyle.Exclamation, "Missing Information");
					vsData.Select(lngRowMuni, lngColData);
					return ValidateAnswers;
				}
				// County
				if (Strings.Trim(vsData.TextMatrix(lngRowCounty, lngColData)) == "")
				{
					ValidateAnswers = false;
					FCMessageBox.Show("Please enter the County name.", MsgBoxStyle.Exclamation, "Missing Information");
					vsData.Select(lngRowCounty, lngColData);
					return ValidateAnswers;
				}
				// Registry County
				if (Strings.Trim(vsData.TextMatrix(lngRowRegistryCounty, lngColData)) == "")
				{
					ValidateAnswers = false;
					FCMessageBox.Show("Please enter the County where the registry of deeds is that liens are files with.", MsgBoxStyle.Exclamation, "Missing Information");
					vsData.Select(lngRowRegistryCounty, lngColData);
					return ValidateAnswers;
				}
				if (intReportType == 0)
				{
					// Tax Year
					if (Strings.Trim(vsData.TextMatrix(lngRowTaxYear, lngColData)) == "")
					{
						ValidateAnswers = false;
						FCMessageBox.Show("Please enter the Tax Year.", MsgBoxStyle.Exclamation, "Missing Information");
						vsData.Select(lngRowTaxYear, lngColData);
						return ValidateAnswers;
					}
					else
					{
						if (Conversion.Val(vsData.TextMatrix(lngRowTaxYear, lngColData)) > 1980 && Conversion.Val(vsData.TextMatrix(lngRowTaxYear, lngColData)) < 3000)
						{
							// this is ok
						}
						else
						{
							ValidateAnswers = false;
							FCMessageBox.Show("Please enter a valid Tax Year.", MsgBoxStyle.Exclamation, "Missing Information");
							vsData.Select(lngRowTaxYear, lngColData);
							return ValidateAnswers;
						}
					}
				}
				// Date to be signed  - let a zero by blank lines so the user can write the date in
				if (Strings.Trim(vsData.TextMatrix(lngRowDateSigned, lngColData)) == "")
				{
					ValidateAnswers = false;
					FCMessageBox.Show("Please enter a signing / interest date.", MsgBoxStyle.Exclamation, "Missing Information");
					vsData.Select(lngRowDateSigned, lngColData);
					return ValidateAnswers;
				}
				else
				{
					if (!Information.IsDate(vsData.TextMatrix(lngRowDateSigned, lngColData)))
					{
						ValidateAnswers = false;
						FCMessageBox.Show("Please enter a valid signing / interest date.", MsgBoxStyle.Exclamation, "Missing Information");
						vsData.Select(lngRowDateSigned, lngColData);
						return ValidateAnswers;
					}
				}
				return ValidateAnswers;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Validating Answers");
			}
			return ValidateAnswers;
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFileSaveExit_Click(sender, e);
		}
	}
}
