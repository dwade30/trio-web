﻿using fecherFoundation;
using Global;
using System;
using System.IO;
using Wisej.Web;

namespace TWCL0000
{
    /// <summary>
    /// Summary description for frmProcessBatch.
    /// </summary>
    public partial class frmProcessBatch : BaseForm
    {
        public frmProcessBatch()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        //=========================================================
        // ********************************************************
        // Property of TRIO Software Corporation
        // Written By
        // Date
        // ********************************************************
        const int CNSTCOLYEAR = 0;
        const int CNSTCOLPRINT = 1;
        const int CNSTCOLACCOUNT = 2;
        const int CNSTCOLNAME = 3;
        const int CNSTCOLPRINCIPAL = 4;
        const int CNSTCOLINTEREST = 5;
        const int CNSTCOLCOST = 6;
        const int CNSTCOLTOTAL = 7;
        const int CNSTCOLPLIPAID = 8;
        const int CNSTCOLEFFDATE = 9;
        const int CNSTCOLPAYMENTDATE = 10;
        const int CNSTCOLPAIDBY = 11;
        const int CNSTCOLCHGINT = 12;
        const int CNSTCOLPAYTYPE = 13;
        const int CNSTCOLBILLKEY = 14;
        const int CNSTCOLLASTINTDATE = 15;
        private DateTime dtDefaultEffDate;
        private DateTime dtDefaultTranDate;
        private string strDefaultPaidBy = string.Empty;
        private int intDefaultYear;
        private string strTellerID = string.Empty;
        private bool boolUseREBills;
        private bool boolLoaded;
        private string strFileToLoad = "";
        private bool boolDefaultPrint;

        private void frmProcessBatch_Activated(object sender, EventArgs e)
        {
            if (!boolLoaded)
            {
                boolLoaded = true;
                if (!LoadFile(strFileToLoad))
                {
                    FCMessageBox.Show("Could not load batch payments", MsgBoxStyle.Critical, "Load Failed");
                    btnProcess.Enabled = false;
                }
            }
        }

        private void frmProcessBatch_KeyDown(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (KeyCode == Keys.Escape)
            {
                KeyCode = 0;
                mnuExit_Click();
            }
        }

        public void Init(string strImportFile, int intYear, DateTime dtEffectiveDate, DateTime dtPaymentDate, string strPaidBy, string strTeller, bool boolREBills, bool boolPrint)
        {
            boolLoaded = false;
            boolDefaultPrint = boolPrint;
            dtDefaultEffDate = dtEffectiveDate;
            dtDefaultTranDate = dtPaymentDate;
            strDefaultPaidBy = strPaidBy;
            intDefaultYear = intYear;
            strTellerID = strTeller;
            boolUseREBills = boolREBills;
            strFileToLoad = strImportFile;
            Show(FormShowEnum.Modal);
        }

        private void ParseFirstAmerican(string strLine)
        {
            int lngAccount = 0;
            string strMapLot = "";
            // VBto upgrade warning: crPayment As double	OnWriteFCConvert.ToDouble(
            decimal crPayment;
            string strPaidBy = "";
            if (strLine.Length >= 51)
            {
                lngAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(strLine, 1, 10))));
                if (lngAccount > 0)
                {
                    strMapLot = Strings.Trim(Strings.Mid(strLine, 11, 20));
                    crPayment = FCConvert.ToDecimal((Conversion.Val(Strings.Trim(Strings.Mid(strLine, 31, 11))) / 100));
                    if (crPayment != 0)
                    {
                        strPaidBy = Strings.Trim(Strings.Mid(strLine, 42, 10));
                        if (strPaidBy == "" || modExtraModules.IsThisCR())
                            strPaidBy = strDefaultPaidBy;
                        AddPaymentLine(lngAccount, intDefaultYear, dtDefaultEffDate, dtDefaultTranDate, strPaidBy, crPayment);
                    }
                }
            }
        }
        // VBto upgrade warning: crAmount As double	OnReadFCConvert.ToDouble(
        private void AddPaymentLine(int lngAcct, int lngYear, DateTime dtEffective, DateTime dtTranDate, string strPaidBy, decimal crAmount)
        {
            clsDRWrapper rsLoad = new clsDRWrapper();
            double dblTotalOwed;
            double dblPrinOwed = 0;
            double dblInterestOwed = 0;
            double dblXInt = 0;
            double[] dblAbate = new double[4 + 1];
            double dblCost = 0;
            string strREPP = "";
            double dblIntPaid = 0;
            double dblCostsPaid = 0;
            double dblPrinPaid = 0;
            string strPayType = "";
            // VBto upgrade warning: dblPayLeft As double	OnWrite(double, short, double)
            double dblPayLeft = 0;
            int lngRow;
            strREPP = boolUseREBills ? "RE" : "PP";

            try
            {
                rsLoad.OpenRecordset($"select * from billingmaster where account = {FCConvert.ToString(lngAcct)} and billingtype = '{strREPP}' and billingyear = {FCConvert.ToString(lngYear)}1", modExtraModules.strCLDatabase);
                if (!rsLoad.EndOfFile())
                {
                    if (((rsLoad.Get_Fields_Int32("lienrecordnumber"))) == 0)
                    {
                        modCLCalculations.CheckForAbatementPayments(FCConvert.ToInt32(rsLoad.Get_Fields_Int32("billkey")), FCConvert.ToInt32(rsLoad.Get_Fields_Int32("RateKey")), boolUseREBills, ref dblAbate[0], ref dblAbate[1], ref dblAbate[2], ref dblAbate[3], Conversion.Val(rsLoad.Get_Fields_Decimal("TaxDue1")), Conversion.Val(rsLoad.Get_Fields_Decimal("TaxDue2")), Conversion.Val(rsLoad.Get_Fields_Decimal("TaxDue3")), Conversion.Val(rsLoad.Get_Fields_Decimal("TaxDue4")));
                        dblTotalOwed = modCLCalculations.CalculateAccountCL(ref rsLoad, lngAcct, dtEffective, ref dblXInt, ref dblPrinOwed, ref dblInterestOwed, ref dblCost, ref dblAbate[0], ref dblAbate[1], ref dblAbate[2], ref dblAbate[3]);
                        strPayType = boolUseREBills ? "R" : "P";
                        dblPayLeft = FCConvert.ToDouble(crAmount);
                        if (dblInterestOwed > 0)
                        {
                            if (dblInterestOwed > dblPayLeft)
                            {
                                dblIntPaid = dblPayLeft;
                                dblPayLeft = 0;
                            }
                            else
                            {
                                dblIntPaid = dblInterestOwed;
                                dblPayLeft -= dblIntPaid;
                            }
                        }
                        if (dblCost > 0 && dblPayLeft > 0)
                        {
                            if (dblCost > dblPayLeft)
                            {
                                dblCostsPaid = dblPayLeft;
                                dblPayLeft = 0;
                            }
                            else
                            {
                                dblCostsPaid = dblCost;
                                dblPayLeft -= dblCostsPaid;
                            }
                        }
                        dblPrinPaid = dblPayLeft;
                        Grid.Rows += 1;
                        lngRow = Grid.Rows - 1;
                        Grid.TextMatrix(lngRow, CNSTCOLACCOUNT, FCConvert.ToString(lngAcct));
                        Grid.TextMatrix(lngRow, CNSTCOLCHGINT, Strings.Format(-1 * dblXInt, "0.00"));
                        Grid.TextMatrix(lngRow, CNSTCOLCOST, Strings.Format(dblCostsPaid, "0.00"));
                        Grid.TextMatrix(lngRow, CNSTCOLEFFDATE, FCConvert.ToString(dtEffective));
                        Grid.TextMatrix(lngRow, CNSTCOLINTEREST, Strings.Format(dblIntPaid, "0.00"));
                        Grid.TextMatrix(lngRow, CNSTCOLNAME, FCConvert.ToString(rsLoad.Get_Fields_String("name1")));
                        Grid.TextMatrix(lngRow, CNSTCOLPAIDBY, strPaidBy);
                        Grid.TextMatrix(lngRow, CNSTCOLPAYMENTDATE, FCConvert.ToString(dtTranDate));
                        Grid.TextMatrix(lngRow, CNSTCOLPAYTYPE, strPayType);
                        Grid.TextMatrix(lngRow, CNSTCOLPLIPAID, "0.00");
                        Grid.TextMatrix(lngRow, CNSTCOLPRINCIPAL, Strings.Format(dblPrinPaid, "0.00"));
                        Grid.TextMatrix(lngRow, CNSTCOLTOTAL, Strings.Format(crAmount, "0.00"));
                        Grid.TextMatrix(lngRow, CNSTCOLYEAR, FCConvert.ToString(lngYear));
                        Grid.TextMatrix(lngRow, CNSTCOLBILLKEY, FCConvert.ToString(rsLoad.Get_Fields_Int32("billkey")));
                        Grid.TextMatrix(lngRow, CNSTCOLPRINT, FCConvert.ToString(boolDefaultPrint));
                        Grid.TextMatrix(lngRow, CNSTCOLLASTINTDATE, FCConvert.ToString(rsLoad.Get_Fields_DateTime("interestappliedthroughdate")));
                    }
                    else
                    {
                        strPayType = "L";
                    }
                }
            }
            finally
            {
                rsLoad.DisposeOf();
            }
        }

        private bool LoadFile(string strFileName)
        {
            bool LoadFile = false;
            StreamReader ts = null;
            bool boolOpen = false;
            try
            {
                // On Error GoTo ErrorHandler
                boolOpen = false;
                string strLine = "";
                if (File.Exists(strFileName))
                {
                    ts = new StreamReader(strFileName);
                    boolOpen = true;
                    while ((strLine = ts.ReadLine()) != null)
                    {
                        App.DoEvents();
                        ParseFirstAmerican(strLine);
                    }
                    ts.Close();
                    boolOpen = false;
                    btnProcess.Enabled = true;
                    LoadFile = true;
                }
                else
                {
                    FCMessageBox.Show("File " + strFileName + " not found", MsgBoxStyle.Critical, "File Not Found");
                    LoadFile = false;
                }
                return LoadFile;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                if (boolOpen)
                {
                    ts.Close();
                }
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In LoadFile", MsgBoxStyle.Critical, "Error");
            }
            return LoadFile;
        }

        private void frmProcessBatch_Load(object sender, EventArgs e)
        {
            //Begin Unmaped Properties
            //frmProcessBatch.Icon	= "frmProcessBatch.frx":0000";
            //frmProcessBatch.FillStyle	= 0;
            //frmProcessBatch.ScaleWidth	= 9300;
            //frmProcessBatch.ScaleHeight	= 7530;
            //frmProcessBatch.LinkTopic	= "Form2";
            //frmProcessBatch.PaletteMode	= 1  'UseZOrder;
            //Font.Size	= "9";
            //Font.Name	= "Tahoma";
            //Font.Weight	= 400;
            //Font.Italic	= 0;
            //Font.Underline	= 0;
            //Font.Strikethrough	= 0;
            //Font.Charset	= 0;
            //Grid.BackColor	= "-2147483643";
            //			//Grid.ForeColor	= "-2147483640";
            //Grid.BorderStyle	= 1;
            //Grid.FillStyle	= 0;
            //Grid.Appearance	= 1;
            //Grid.GridLines	= 1;
            //Grid.WordWrap	= 0;
            //Grid.ScrollBars	= 2;
            //Grid.RightToLeft	= 0;
            //Grid._cx	= 15849;
            //Grid._cy	= 12303;
            //Grid._ConvInfo	= 1;
            //Grid.MousePointer	= 0;
            //Grid.BackColorFixed	= -2147483633;
            //			//Grid.ForeColorFixed	= -2147483630;
            //Grid.BackColorSel	= -2147483635;
            //			//Grid.ForeColorSel	= -2147483634;
            //Grid.BackColorBkg	= -2147483636;
            //Grid.BackColorAlternate	= -2147483643;
            //Grid.GridColor	= -2147483633;
            //Grid.GridColorFixed	= -2147483632;
            //Grid.TreeColor	= -2147483632;
            //Grid.FloodColor	= 192;
            //Grid.SheetBorder	= -2147483642;
            //Grid.FocusRect	= 1;
            //Grid.HighLight	= 0;
            //Grid.AllowSelection	= -1  'True;
            //Grid.AllowBigSelection	= -1  'True;
            //Grid.AllowUserResizing	= 0;
            //Grid.SelectionMode	= 0;
            //Grid.GridLinesFixed	= 2;
            //Grid.GridLineWidth	= 1;
            //Grid.RowHeightMin	= 0;
            //Grid.RowHeightMax	= 0;
            //Grid.ColWidthMin	= 0;
            //Grid.ColWidthMax	= 0;
            //Grid.ExtendLastCol	= -1  'True;
            //Grid.FormatString	= "";
            //Grid.ScrollTrack	= -1  'True;
            //Grid.ScrollTips	= 0   'False;
            //Grid.MergeCells	= 0;
            //Grid.MergeCompare	= 0;
            //Grid.AutoResize	= -1  'True;
            //Grid.AutoSizeMode	= 0;
            //Grid.AutoSearch	= 0;
            //Grid.AutoSearchDelay	= 2;
            //Grid.MultiTotals	= -1  'True;
            //Grid.SubtotalPosition	= 1;
            //Grid.OutlineBar	= 0;
            //Grid.OutlineCol	= 0;
            //Grid.Ellipsis	= 0;
            //Grid.ExplorerBar	= 0;
            //Grid.PicturesOver	= 0   'False;
            //Grid.PictureType	= 0;
            //Grid.TabBehavior	= 0;
            //Grid.OwnerDraw	= 0;
            //Grid.ShowComboButton	= -1  'True;
            //Grid.TextStyle	= 0;
            //Grid.TextStyleFixed	= 0;
            //Grid.OleDragMode	= 0;
            //Grid.OleDropMode	= 0;
            //Grid.ComboSearch	= 3;
            //Grid.AutoSizeMouse	= -1  'True;
            //Grid.AllowUserFreezing	= 0;
            //Grid.BackColorFrozen	= 0;
            //			//Grid.ForeColorFrozen	= 0;
            //Grid.WallPaperAlignment	= 9;
            //vsElasticLight1.OleObjectBlob	= "frmProcessBatch.frx":058A";
            //End Unmaped Properties
            modGlobalFunctions.SetFixedSize(this);
            modGlobalFunctions.SetTRIOColors(this);
            SetupGrid();
        }

        private void frmProcessBatch_Resize(object sender, EventArgs e)
        {
            ResizeGrid();
        }

        private void mnuExit_Click(object sender, EventArgs e)
        {
            int x/*unused?*/;
        }

        public void mnuExit_Click()
        {
            //mnuExit_Click(mnuExit, new System.EventArgs());
        }

        private void ResizeGrid()
        {
            int lngGridWidth = 0;
            lngGridWidth = Grid.WidthOriginal;
            Grid.ColWidth(CNSTCOLPRINT, FCConvert.ToInt32(0.06 * lngGridWidth));
            Grid.ColWidth(CNSTCOLACCOUNT, FCConvert.ToInt32(0.09 * lngGridWidth));
            Grid.ColWidth(CNSTCOLNAME, FCConvert.ToInt32(0.42 * lngGridWidth));
            Grid.ColWidth(CNSTCOLPRINCIPAL, FCConvert.ToInt32(0.1 * lngGridWidth));
            Grid.ColWidth(CNSTCOLINTEREST, FCConvert.ToInt32(0.1 * lngGridWidth));
            Grid.ColWidth(CNSTCOLCOST, FCConvert.ToInt32(0.1 * lngGridWidth));
        }

        private void SetupGrid()
        {
            Grid.ColHidden(CNSTCOLYEAR, true);
            Grid.TextMatrix(0, CNSTCOLACCOUNT, "Account");
            Grid.TextMatrix(0, CNSTCOLNAME, "Name");
            Grid.TextMatrix(0, CNSTCOLPRINCIPAL, "Principal");
            Grid.TextMatrix(0, CNSTCOLINTEREST, "Interest");
            Grid.TextMatrix(0, CNSTCOLCOST, "Cost");
            Grid.TextMatrix(0, CNSTCOLTOTAL, "Total");
            Grid.TextMatrix(0, CNSTCOLPLIPAID, "PLI Paid");
            Grid.TextMatrix(0, CNSTCOLEFFDATE, "Eff Date");
            Grid.TextMatrix(0, CNSTCOLPAIDBY, "Paid By");
            Grid.TextMatrix(0, CNSTCOLPAYMENTDATE, "Tran Date");
            Grid.TextMatrix(0, CNSTCOLPRINT, "Print");
            Grid.ColHidden(CNSTCOLPLIPAID, true);
            Grid.ColHidden(CNSTCOLEFFDATE, true);
            Grid.ColHidden(CNSTCOLPAIDBY, true);
            Grid.ColHidden(CNSTCOLPAYMENTDATE, true);
            Grid.ColHidden(CNSTCOLCHGINT, true);
            Grid.ColHidden(CNSTCOLPAYTYPE, true);
            Grid.ColHidden(CNSTCOLBILLKEY, true);
            Grid.ColHidden(CNSTCOLLASTINTDATE, true);
            Grid.ColDataType(CNSTCOLPRINT, FCGrid.DataTypeSettings.flexDTBoolean);
        }

        private void mnuSaveExit_Click(object sender, EventArgs e)
        {
            if (Processpayments())
            {
            }
        }

        private bool Processpayments()
        {
            bool Processpayments = false;
            try
            {
                // On Error GoTo ErrorHandler
                modGlobalFunctions.AddCYAEntry_728("CL", "Batch Payment From File", "Year " + FCConvert.ToString(intDefaultYear), "EffectiveDate " + FCConvert.ToString(dtDefaultEffDate), "Teller " + strTellerID, "Paid By " + strDefaultPaidBy);
                int lngRow;
                clsDRWrapper rsPay = new clsDRWrapper();
                clsDRWrapper rsCR = new clsDRWrapper();
                int lngKey/*unused?*/;
                rsPay.OpenRecordset("select * from paymentrec where id = -1", modExtraModules.strCLDatabase);
                if (modExtraModules.IsThisCR())
                {
                    rsCR.OpenRecordset("select * from batchrecover where [KEY] = -1 ", modExtraModules.strCLDatabase);
                }
                int lngPayID = 0;
                for (lngRow = 1; lngRow <= Grid.Rows - 1; lngRow++)
                {
                    lngPayID = 0;
                    if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTCOLCHGINT)) != 0)
                    {
                        rsPay.AddNew();
                        // lngPayID = rsPay.Get_Fields("ID")
                        rsPay.Set_Fields("account", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTCOLACCOUNT))));
                        rsPay.Set_Fields("Year", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTCOLYEAR))) + "1");
                        rsPay.Set_Fields("billkey", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTCOLBILLKEY))));
                        rsPay.Set_Fields("chgintdate", Grid.TextMatrix(0, CNSTCOLEFFDATE));
                        rsPay.Set_Fields("ActualSystemDate", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
                        rsPay.Set_Fields("effectiveinterestdate", Grid.TextMatrix(0, CNSTCOLEFFDATE));
                        rsPay.Set_Fields("RecordedTransactionDate", Grid.TextMatrix(0, CNSTCOLPAYMENTDATE));
                        rsPay.Set_Fields("Reference", "CHGINT");
                        rsPay.Set_Fields("Period", "1");
                        rsPay.Set_Fields("Code", "I");
                        if (!modExtraModules.IsThisCR())
                        {
                            rsPay.Set_Fields("Receiptnumber", 0);
                        }
                        else
                        {
                            rsPay.Set_Fields("receiptnumber", -1);
                        }
                        rsPay.Set_Fields("CurrentInterest", Strings.Format(-1 * FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTCOLCHGINT)))), "0.00"));
                        rsPay.Set_Fields("CashDrawer", "Y");
                        rsPay.Set_Fields("GeneralLedger", "Y");
                        rsPay.Set_Fields("Billcode", Grid.TextMatrix(lngRow, CNSTCOLPAYTYPE));
                        rsPay.Set_Fields("OldInterestDate", Grid.TextMatrix(lngRow, CNSTCOLLASTINTDATE));
                        rsPay.Update();
                        lngPayID = FCConvert.ToInt32(rsPay.Get_Fields_Int32("ID"));
                        if (modExtraModules.IsThisCR())
                        {
                            // add to batch
                            rsCR.AddNew();
                            rsCR.Update();
                        }
                    }
                    rsPay.AddNew();
                    rsPay.Set_Fields("chgintnumber", lngPayID);
                    rsPay.Set_Fields("account", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTCOLACCOUNT))));
                    rsPay.Set_Fields("Year", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTCOLYEAR))) + "1");
                    rsPay.Set_Fields("billkey", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTCOLBILLKEY))));
                    rsPay.Set_Fields("chgintdate", Grid.TextMatrix(0, CNSTCOLEFFDATE));
                    rsPay.Set_Fields("ActualSystemDate", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
                    rsPay.Set_Fields("effectiveinterestdate", Grid.TextMatrix(0, CNSTCOLEFFDATE));
                    rsPay.Set_Fields("RecordedTransactionDate", Grid.TextMatrix(0, CNSTCOLPAYMENTDATE));
                    rsPay.Set_Fields("Period", "A");
                    rsPay.Set_Fields("Code", "P");
                    rsPay.Set_Fields("Teller", strTellerID);
                    if (!modExtraModules.IsThisCR())
                    {
                        rsPay.Set_Fields("ReceiptNumber", 0);
                    }
                    else
                    {
                        rsPay.Set_Fields("receiptnumber", -1);
                    }
                    rsPay.Set_Fields("principal", Strings.Format(FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTCOLPRINCIPAL))), "0.00"));
                    rsPay.Set_Fields("PreLienInterest", Strings.Format(FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTCOLPLIPAID))), "0.00"));
                    rsPay.Set_Fields("CurrentInterest", Strings.Format(FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTCOLINTEREST))), "0.00"));
                    rsPay.Set_Fields("LienCost", Strings.Format(FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTCOLCOST))), "0.00"));
                    rsPay.Set_Fields("PaidBy", Grid.TextMatrix(lngRow, CNSTCOLPAIDBY));
                    rsPay.Set_Fields("CashDrawer", "Y");
                    rsPay.Set_Fields("GeneralLedger", "Y");
                    rsPay.Set_Fields("BillCode", Grid.TextMatrix(lngRow, CNSTCOLPAYTYPE));
                    rsPay.Update();
                    if (modExtraModules.IsThisCR())
                    {
                        // also put in the batch table
                    }
                }
                // lngRow
                Processpayments = true;
                return Processpayments;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In ProcessPayments", MsgBoxStyle.Critical, "Error");
            }
            return Processpayments;
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            mnuSaveExit_Click(sender, e);
        }
    }
}
