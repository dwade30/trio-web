﻿using System;
using System.Linq;
using SharedApplication;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.Models;

namespace TWCL0000
{
    public class PropertyTaxBillCalculator : IPropertyTaxBillCalculator
    {
        private IBillPaymentUtility billPaymentUtility;
        private decimal overPayRate;
        private int periodsInYear;

        public PropertyTaxBillCalculator(IBillPaymentUtility paymentUtility,GlobalTaxCollectionSettings taxCollectionSettings)
        {
            billPaymentUtility = paymentUtility;
            periodsInYear = taxCollectionSettings.DaysInAYear;
            overPayRate = taxCollectionSettings.OverPaymentInterestRate;
        }
        public TaxBillCalculationSummary CalculateBill(PropertyTaxBill bill,DateTime asOfDate)
        {
            if (bill == null)
            {
                return null;
            }
            if (bill.HasLien())
            {
                return CalculateLien(bill,asOfDate);
            }
            else
            {
                return CalculateRegularBill(bill, asOfDate);
            }
        }

        private TaxBillCalculationSummary CalculateRegularBill(PropertyTaxBill bill, DateTime asOfDate)
        {
            try
            {
                var interestPaidDate = GetMaxOfDates(DateTime.MinValue, bill.InterestAppliedThroughDate);

                Decimal newInterest = 0;

                if (interestPaidDate.IsEarlierThan(asOfDate))
                {

                    var principalDue = bill.TaxInstallments.GetEffectiveDueAsOf(asOfDate);
                    var currentPrincipalDue = principalDue - bill.BillSummary.PrincipalPaid;
                    var lastPayment = bill.Payments.GetAll().Where(p => p.Code != "I")
                        .OrderByDescending(p => p.EffectiveInterestDate).FirstOrDefault();
                    var calculateInterest = true;
                    if (lastPayment != null)
                    {
                        if (lastPayment.Code == "U")
                        {
                            if (bill.HasTaxClub())
                            {
                                if (!bill.TaxClub.NonActive ?? true)
                                {
                                    calculateInterest = false;
                                }
                            }
                        }
                    }

                    if (calculateInterest)
                    {
                        decimal interestRate = 0;
                        if (bill.RateRecord != null)
                        {
                            interestRate = bill.RateRecord.InterestRate;
                        }
                        newInterest = billPaymentUtility.CalculateInterest(principalDue, interestPaidDate, asOfDate,
                            interestRate, overPayRate, periodsInYear);
                    }
                }

                return new TaxBillCalculationSummary()
                {
                    PrincipalPaid = bill.BillSummary.PrincipalPaid,
                    Balance = bill.BillSummary.GetNetOwed() + newInterest,
                    Charged = bill.BillSummary.PrincipalCharged,
                    ChargedInterest = bill.BillSummary.InterestCharged,
                    Interest = newInterest,
                    InterestPaid = bill.BillSummary.InterestPaid,
                    CostsCharged = bill.BillSummary.DemandFeesCharged,
                    CostsPaid = bill.BillSummary.DemandFeesPaid
                };
            }
            catch (Exception ex)
            {
                //return null;
            }

            return null;
        }

        protected DateTime GetMaxOfDates(DateTime? Date1, DateTime? Date2)
        {
            if (!Date1.HasValue)
            {
                return GetMaxOfDates(DateTime.MinValue, Date2);
            }
            if (!Date2.HasValue)
            {
                return GetMaxOfDates(DateTime.MinValue, Date1);
            }
            if (DateTime.Compare(Date1.Value, Date2.Value) < 0)
            {
                return Date2.Value;
            }
            else
            {
                return Date1.Value;
            }
        }
      
        private TaxBillCalculationSummary CalculateLien(PropertyTaxBill bill, DateTime asOfDate)
        {
            var lien = bill.Lien;
            var interestPaidDate = GetMaxOfDates(DateTime.MinValue, lien.InterestAppliedThroughDate);
            var paymentsSummary = lien.Payments.GetPaymentsSummary();
            var calculationSummary = new TaxBillCalculationSummary()
            {
                PrincipalPaid = lien.LienSummary.PrincipalPaid,
                Charged = lien.LienSummary.PrincipalCharged,
                ChargedInterest = lien.LienSummary.GetTotalInterestCharged(),
                InterestPaid = lien.LienSummary.GetTotalInterestPaid(),
                CostsCharged = lien.LienSummary.GetTotalCostsCharged(),
                CostsPaid = lien.LienSummary.CostsPaid,
            };
            if (paymentsSummary.Principal > 0 && interestPaidDate.IsEarlierThan(asOfDate))
            {
                var rateRecord = lien.RateRecord;
                var interestStartDate =
                    GetMaxOfDates(interestPaidDate, rateRecord.Installments[0].InterestStartDate);
                calculationSummary.Interest = billPaymentUtility.CalculateInterest(paymentsSummary.Principal,
                    interestStartDate, asOfDate, rateRecord.InterestRate, overPayRate, periodsInYear);
            }

            calculationSummary.Balance = lien.LienSummary.GetNetOwed() + calculationSummary.Interest;
            return calculationSummary;
        }
    }
}