﻿using System;

namespace TWCL0000.TaxBillExport
{
    public interface ISimplePropertyTaxBillExporter
    {
        int RecordCount();
        void CreateExport(DateTime asOfDate, int minimumTaxYear, string fileName);
    }
}