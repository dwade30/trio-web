﻿using System;
using System.Collections.Generic;
using System.IO.Abstractions;
using System.Linq;
using System.Net;
//using Chilkat;
using SharedApplication;
using SharedApplication.Extensions;
using Wisej.Web;
using StringBuilder = System.Text.StringBuilder;

namespace TWCL0000.TaxBillExport
{
    public class SimplePropertyTaxBillExporter : ISimplePropertyTaxBillExporter
    {
        private int recordCount;
        private IFileSystem _fileSystem;
        private IQueryHandler<TaxBillExportQuery, IEnumerable<PropertyTaxBillForExport>> exportQueryHandler;
        public SimplePropertyTaxBillExporter(IFileSystem fileSystem, IQueryHandler<TaxBillExportQuery, IEnumerable<PropertyTaxBillForExport>> queryHandler)
        {
            _fileSystem = fileSystem;
            exportQueryHandler = queryHandler;
        }
        public int RecordCount()
        {
            return RecordCount();
        }

        public void CreateExport(DateTime asOfDate, int minimumTaxYear, string fileName)
        {
            recordCount = 0;
           
            var taxBills = GetBillsForExport(asOfDate,minimumTaxYear).ToList();
            if (taxBills.Any())
            {
                recordCount = taxBills.Count();
                WriteBillsToFile(taxBills,fileName);
            }
        }

       
        private IEnumerable<PropertyTaxBillForExport> GetBillsForExport(DateTime asOfDate,int minimumTaxYear)
        {
            var query = new TaxBillExportQuery(){ AsOfDate = asOfDate,MinimumTaxYear = minimumTaxYear};
            return exportQueryHandler.ExecuteQuery(query);
        }
        private void WriteBillsToFile(IEnumerable<PropertyTaxBillForExport> taxBills,string defaultFileName)
        {
            var tempFileName = _fileSystem.Path.GetRandomFileName();
            var filePath = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;
            var fullFilePath = _fileSystem.Path.Combine(filePath, tempFileName);            
            using (var fileStream = _fileSystem.File.CreateText(fullFilePath))
            {
                fileStream.WriteLine(GetHeaderLine());
                foreach (var bill in taxBills)
                {
                    fileStream.WriteLine(GetBillLine(bill));
                }
                fileStream.Flush();
                fileStream.Close();
            }
            fecherFoundation.FCUtils.DownloadAndOpen("_blank", fullFilePath,defaultFileName);
        }

        private string GetHeaderLine()
        {
            var line = new StringBuilder();
            line.Append( "\"Acct\"");
            line.Append(",\"Bill Type\"");
            line.Append(",\"Name\"");
            line.Append(",\"Fiscal Year\"");
            line.Append(",\"Original Tax\"");
            line.Append(",\"Payments\"");
            line.Append(",\"Subtotal\"");
            line.Append(",\"Interest\"");
            line.Append(",\"Costs\"");
            line.Append(",\"Balance Due\"");
            line.Append(",\"Acct-Key\"");
            line.Append(",\"Import Date\"");
            return line.ToString();
        }

        private string GetBillLine(PropertyTaxBillForExport bill)
        {
            var line = new StringBuilder();
            line.Append( bill.Account.ToString());
            line.Append(",\"" + bill.BillCode + "\"");
            line.Append(",\"" + bill.Name + "\"");
            line.Append("," + bill.BillYear.ToString());
            line.Append("," + bill.OriginalTax.ToString());
            line.Append("," + bill.PaymentsMade.ToString());
            line.Append("," + bill.PrincipalDue().ToString());
            line.Append("," + bill.Interest.ToString());
            line.Append("," + bill.Costs.ToString());
            line.Append("," + bill.BalanceDue.ToString());
            line.Append(",\"" + bill.BillYear.ToString() + "-" + bill.Account.ToString());
            line.Append("," + bill.AsOfDate.FormatAndPadShortDate());
            return line.ToString();
        }

      

    }
}

