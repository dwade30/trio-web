﻿using System;
using System.Collections.Generic;
using System.Linq;
using SharedApplication;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.Interfaces;
using SharedApplication.TaxCollections.Models;

namespace TWCL0000.TaxBillExport.DataAccess
{
    public class TaxBillExportQueryHandler : IQueryHandler<TaxBillExportQuery,IEnumerable<PropertyTaxBillForExport>>
    {
        private IPropertyTaxBillQueryHandler taxBillQueryHandler;
        private IPropertyTaxBillCalculator taxBillCalculator;
        public TaxBillExportQueryHandler(IPropertyTaxBillQueryHandler queryHandler,IPropertyTaxBillCalculator billCalculator)
        {
            taxBillQueryHandler = queryHandler;
            taxBillCalculator = billCalculator;
        }
        public IEnumerable<PropertyTaxBillForExport> ExecuteQuery(TaxBillExportQuery query)
        {
            var taxBills = taxBillQueryHandler.GetAll().Where(b => b.TaxYear > 2009);
            return GetComputedTaxBillsForExport(taxBills, query.AsOfDate);
        }

        private IEnumerable<PropertyTaxBillForExport> GetComputedTaxBillsForExport(
            IEnumerable<PropertyTaxBill> propertyTaxBills, DateTime asOfDate)
        {
            var currentBill = new PropertyTaxBillForExport();
            var taxBills = new List<PropertyTaxBillForExport>();
            int billYear = 0;
            if (propertyTaxBills == null)
            {
                return taxBills;
            }
            foreach (var bill in propertyTaxBills)
            {
                if (currentBill.Account != bill.Account || currentBill.BillYear != bill.TaxYear ||
                    currentBill.BillType != bill.BillType)
                {
                    if (currentBill.Account > 0 )
                    {                        
                        taxBills.Add(currentBill);
                    }
                    currentBill = new PropertyTaxBillForExport()
                    {
                        Account = bill.Account ?? 0,
                        BillType = bill.BillType,
                        BillYear = bill.TaxYear,
                        Name = bill.Name1,
                        Name2 = bill.Name2,
                        AsOfDate = asOfDate
                        
                    };
                    var calcSummary = taxBillCalculator.CalculateBill(bill, asOfDate);
                    AddCalcSummaryToBill(calcSummary,ref currentBill);
                }
            }

            if (currentBill.Account > 0)
            {
                taxBills.Add(currentBill);
            }

            return taxBills;
        }

        private void AddCalcSummaryToBill(TaxBillCalculationSummary calcSummary,ref PropertyTaxBillForExport taxBill)
        {
            if (calcSummary == null || taxBill == null)
            {
                return;
            }
            taxBill.BalanceDue += calcSummary.Balance;
            taxBill.Costs += calcSummary.CostsDue();
            taxBill.Interest += calcSummary.InterestDue();
            taxBill.OriginalTax += calcSummary.Charged;
            taxBill.PaymentsMade += calcSummary.PrincipalPaid;
        }
    }
}
