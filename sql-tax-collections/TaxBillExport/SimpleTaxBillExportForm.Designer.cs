﻿using Global;

namespace TWCL0000.TaxBillExport
{
    partial class SimpleTaxBillExportForm : BaseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
		{
            fecherFoundation.Sys.ClearInstance(this);
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.btnProcess = new fecherFoundation.FCButton();
			this.dtpAsOfDate = new Wisej.Web.DateTimePicker();
			this.fcLabel3 = new fecherFoundation.FCLabel();
			this.fcLabel1 = new fecherFoundation.FCLabel();
			this.cmbOldestTaxYear = new Wisej.Web.ComboBox();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbOldestTaxYear);
			this.ClientArea.Controls.Add(this.fcLabel1);
			this.ClientArea.Controls.Add(this.fcLabel3);
			this.ClientArea.Controls.Add(this.dtpAsOfDate);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(171, 30);
			this.HeaderText.Text = "Tax Bill Export";
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Cursor = Wisej.Web.Cursors.Default;
			this.btnProcess.Location = new System.Drawing.Point(447, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(120, 48);
			this.btnProcess.TabIndex = 2;
			this.btnProcess.Text = "Export";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// dtpAsOfDate
			// 
			this.dtpAsOfDate.Format = Wisej.Web.DateTimePickerFormat.Short;
			this.dtpAsOfDate.Location = new System.Drawing.Point(21, 74);
			this.dtpAsOfDate.Name = "dtpAsOfDate";
			this.dtpAsOfDate.Size = new System.Drawing.Size(119, 21);
			this.dtpAsOfDate.TabIndex = 0;
			this.dtpAsOfDate.Value = new System.DateTime(2019, 3, 19, 10, 34, 55, 818);
			// 
			// fcLabel3
			// 
			this.fcLabel3.Location = new System.Drawing.Point(21, 51);
			this.fcLabel3.Name = "fcLabel3";
			this.fcLabel3.Size = new System.Drawing.Size(89, 17);
			this.fcLabel3.TabIndex = 23;
			this.fcLabel3.Text = "AS OF DATE";
			this.fcLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fcLabel1
			// 
			this.fcLabel1.Location = new System.Drawing.Point(21, 135);
			this.fcLabel1.Name = "fcLabel1";
			this.fcLabel1.Size = new System.Drawing.Size(134, 17);
			this.fcLabel1.TabIndex = 24;
			this.fcLabel1.Text = "OLDEST TAX YEAR";
			this.fcLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cmbOldestTaxYear
			// 
			this.cmbOldestTaxYear.Anchor = Wisej.Web.AnchorStyles.Left;
			this.cmbOldestTaxYear.Location = new System.Drawing.Point(21, 167);
			this.cmbOldestTaxYear.Name = "cmbOldestTaxYear";
			this.cmbOldestTaxYear.Size = new System.Drawing.Size(119, 40);
			this.cmbOldestTaxYear.TabIndex = 25;
			// 
			// SimpleTaxBillExportForm
			// 
			this.Name = "SimpleTaxBillExportForm";
			this.Text = "Tax Bill Export";
			this.Load += new System.EventHandler(this.SimpleTaxBillExportForm_Load);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private fecherFoundation.FCButton btnProcess;
        private Wisej.Web.DateTimePicker dtpAsOfDate;
        private fecherFoundation.FCLabel fcLabel3;
        private Wisej.Web.ComboBox cmbOldestTaxYear;
        private fecherFoundation.FCLabel fcLabel1;
    }
}