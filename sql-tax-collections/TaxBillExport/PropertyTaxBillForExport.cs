﻿using System;

namespace TWCL0000.TaxBillExport
{
    public class PropertyTaxBillForExport
    {
        public DateTime AsOfDate { get; set; }
        public int Account { get; set; }
        public SharedApplication.TaxCollections.Enums.PropertyTaxBillType BillType { get; set; } = SharedApplication.TaxCollections.Enums.PropertyTaxBillType.Real;
        public PropertyTaxBillCode BillCode { get; set; } = PropertyTaxBillCode.Regular;
        public string Name { get; set; } = "";
        public string Name2 { get; set; } = "";
        public int BillYear { get; set; }
        public Decimal OriginalTax { get; set; }
        public Decimal PaymentsMade { get; set; }
        public Decimal Interest { get; set; }
        public Decimal Costs { get; set; }
        public Decimal BalanceDue { get; set; }

        public Decimal PrincipalDue()
        {
            return OriginalTax - PaymentsMade;
        }
    }
}