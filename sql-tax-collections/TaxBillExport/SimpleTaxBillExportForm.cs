﻿using System;
using fecherFoundation;
using Global;
using Microsoft.EntityFrameworkCore.Metadata.Conventions.Internal;
using SharedApplication.Extensions;
using Wisej.Web;

namespace TWCL0000.TaxBillExport
{
    public partial class SimpleTaxBillExportForm : BaseForm
    {
        private ISimplePropertyTaxBillExporter billExporter;
        public SimpleTaxBillExportForm()
        {
            InitializeComponent();
        }

        public void SetDependencies(ISimplePropertyTaxBillExporter taxBillExporter)
        {
            billExporter = taxBillExporter;
        }

        private void SimpleTaxBillExportForm_Load(object sender, EventArgs e)
        {
            FillTaxYearCombo();
            cmbOldestTaxYear.SelectedIndex = 0;
        }

        private void FillTaxYearCombo()
        {
            cmbOldestTaxYear.Items.Add(2016);
            cmbOldestTaxYear.Items.Add(2017);
            cmbOldestTaxYear.Items.Add(2018);
        }
        private void btnProcess_Click(object sender, EventArgs e)
        {
            if (ValidateOptions())
            {
                DateTime asOfDate = dtpAsOfDate.Value;
                var minimumTaxYear = (int) cmbOldestTaxYear.SelectedItem;
                billExporter.CreateExport(asOfDate,minimumTaxYear,GetExportFileName());
            }
        }

        private bool ValidateOptions()
        {
            if (!dtpAsOfDate.NullableValue.HasValue)
            {
                FCMessageBox.Show("You must select a valid As Of Date", MsgBoxStyle.Exclamation, "Invalid Date");
                return false;
            }

            return true;
        }

        private string GetExportFileName()
        {
            return "TaxBillExport_" + DateTime.Now.FormatAndPadShortDate("-") + ".csv";
        }
    }
}
