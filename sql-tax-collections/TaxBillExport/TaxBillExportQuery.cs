﻿using System;

namespace TWCL0000.TaxBillExport
{
    public class TaxBillExportQuery
    {
        public DateTime AsOfDate { get; set; }
        public int MinimumTaxYear { get; set; }
    }
}