﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmLienDischargeGetAcct.
	/// </summary>
	partial class frmLienDischargeGetAcct : BaseForm
	{
		public fecherFoundation.FCCheckBox chkReprint;
		public fecherFoundation.FCCheckBox chkAllNotices;
		public fecherFoundation.FCFrame fraSingle;
		public fecherFoundation.FCComboBox cmbYear;
		public fecherFoundation.FCTextBox txtAcct;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLienDischargeGetAcct));
            this.chkReprint = new fecherFoundation.FCCheckBox();
            this.chkAllNotices = new fecherFoundation.FCCheckBox();
            this.fraSingle = new fecherFoundation.FCFrame();
            this.cmbYear = new fecherFoundation.FCComboBox();
            this.txtAcct = new fecherFoundation.FCTextBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.btnProcess = new fecherFoundation.FCButton();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkReprint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAllNotices)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSingle)).BeginInit();
            this.fraSingle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 441);
            this.BottomPanel.Size = new System.Drawing.Size(374, 10);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.btnProcess);
            this.ClientArea.Controls.Add(this.chkReprint);
            this.ClientArea.Controls.Add(this.chkAllNotices);
            this.ClientArea.Controls.Add(this.fraSingle);
            this.ClientArea.Size = new System.Drawing.Size(374, 381);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(374, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(234, 30);
            this.HeaderText.Text = "Print Lien Discharge";
            // 
            // chkReprint
            // 
            this.chkReprint.Location = new System.Drawing.Point(30, 78);
            this.chkReprint.Name = "chkReprint";
            this.chkReprint.Size = new System.Drawing.Size(162, 27);
            this.chkReprint.TabIndex = 1;
            this.chkReprint.Text = "Reprint Last Batch";
            this.chkReprint.Click += new System.EventHandler(this.chkReprint_Click);
            // 
            // chkAllNotices
            // 
            this.chkAllNotices.Checked = true;
            this.chkAllNotices.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
            this.chkAllNotices.Location = new System.Drawing.Point(30, 30);
            this.chkAllNotices.Name = "chkAllNotices";
            this.chkAllNotices.Size = new System.Drawing.Size(194, 27);
            this.chkAllNotices.Text = "Print All Saved Notices";
            this.chkAllNotices.Click += new System.EventHandler(this.chkAllNotices_Click);
            // 
            // fraSingle
            // 
            this.fraSingle.Controls.Add(this.cmbYear);
            this.fraSingle.Controls.Add(this.txtAcct);
            this.fraSingle.Controls.Add(this.Label1);
            this.fraSingle.Controls.Add(this.Label2);
            this.fraSingle.Enabled = false;
            this.fraSingle.Location = new System.Drawing.Point(30, 137);
            this.fraSingle.Name = "fraSingle";
            this.fraSingle.Size = new System.Drawing.Size(296, 150);
            this.fraSingle.TabIndex = 2;
            this.fraSingle.Text = "Single Account";
            this.fraSingle.UseMnemonic = false;
            // 
            // cmbYear
            // 
            this.cmbYear.BackColor = System.Drawing.SystemColors.Window;
            this.cmbYear.Location = new System.Drawing.Point(146, 90);
            this.cmbYear.Name = "cmbYear";
            this.cmbYear.Size = new System.Drawing.Size(130, 40);
            this.cmbYear.TabIndex = 3;
            this.cmbYear.SelectedIndexChanged += new System.EventHandler(this.cmbYear_SelectedIndexChanged);
            this.cmbYear.Validating += new System.ComponentModel.CancelEventHandler(this.cmbYear_Validating);
            this.cmbYear.KeyPress += new Wisej.Web.KeyPressEventHandler(this.cmbYear_KeyPress);
            // 
            // txtAcct
            // 
            this.txtAcct.BackColor = System.Drawing.SystemColors.Window;
            this.txtAcct.Location = new System.Drawing.Point(146, 30);
            this.txtAcct.Name = "txtAcct";
            this.txtAcct.Size = new System.Drawing.Size(130, 40);
            this.txtAcct.TabIndex = 1;
            this.txtAcct.Validating += new System.ComponentModel.CancelEventHandler(this.txtAcct_Validating);
            this.txtAcct.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtAcct_KeyPress);
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(20, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(90, 20);
            this.Label1.Text = "ACCOUNT";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(20, 104);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(54, 20);
            this.Label2.TabIndex = 2;
            this.Label2.Text = "YEAR";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Location = new System.Drawing.Point(30, 317);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcess.Size = new System.Drawing.Size(159, 48);
            this.btnProcess.TabIndex = 2;
            this.btnProcess.Text = "Save & Continue";
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // frmLienDischargeGetAcct
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(374, 451);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmLienDischargeGetAcct";
            this.Text = "Print Lien Discharge";
            this.Load += new System.EventHandler(this.frmLienDischargeGetAcct_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmLienDischargeGetAcct_KeyDown);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkReprint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAllNotices)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSingle)).EndInit();
            this.fraSingle.ResumeLayout(false);
            this.fraSingle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton btnProcess;
	}
}
