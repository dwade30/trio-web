﻿using Global;
using SharedApplication;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Commands;
using SharedApplication.TaxCollections.Comment;
using SharedApplication.TaxCollections.Enums;

namespace TWCL0000.Comments
{
    public class EditCollectionsCommentHandler : CommandHandler<EditCollectionsComment>
    {
        private IModalView<ICollectionsCommentViewModel> view;
        public EditCollectionsCommentHandler(IModalView<ICollectionsCommentViewModel> view)
        {
            this.view = view;
        }
        protected override void Handle(EditCollectionsComment command)
        {
            view.ViewModel.Account = command.Account;
            view.ViewModel.BillType = command.BillType;
            view.ViewModel.LoadComment();
            view.ShowModal();            
        }
    }
}