﻿using System;
using SharedApplication.TaxCollections.Models;

namespace TWCL0000
{
    public interface IPropertyTaxBillCalculator
    {
        TaxBillCalculationSummary CalculateBill(PropertyTaxBill bill,DateTime asOfDate);
    }
}