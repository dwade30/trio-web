﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	public class modRhymalReporting
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               06/08/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/18/2005              *
		// ********************************************************
		public struct FreeReportFormat
		{
			// VBto upgrade warning: DatabaseFieldName As string	OnWrite(string, int)
			public string DatabaseFieldName;
			// Dynamic Variable - this will be the field name inside the database
			// Question - Not Used
			// Calculated Field - this is the variable name in the scope of the report
			// or blank if just a text field...this will not be processed automatically in any way
			// it is just a way for you to get more info from the user
			public string NameToShowUser;
			// Variable - this will be how the user sees the tag ex. "Owner Name"
			// Question - it is the actual question
			public string Tag;
			// Variable - all UPPERCASE, this is the actual word(s) that will be used as the tag ex.<NAME>
			// Question - Not Used
			public bool Required;
			// Variable - if this is true for any variable, the report will not run until that variable is used
			// Question - if it is true for and question then the user cannot move to the next screen until the question is answered
			public int VariableType;
			// 0 - Static Variable, 1 - Dynamic Variable, 2 - Questions, 3 - Calculated
			public string ComboList;
			// Variable - it is the list of answers in the combo box
			// Question - it is the list of answers in the combo box
			public short Type;
			// This is the type of variable. ex(Date, Text, Number) using the Custom Report
			// 0 = Text, 1 = Number, 2 = Date, 3 = Do not ask for default (comes from DB), 4 = Formatted Year
			public int RowNumber;
			// this will be set when loaded into the grid, all questions and Static Variables will be assigned a row number, this is the row of the answer in the grid
			public string ToolTip;
			// this is what the user will see when they hover over the row
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public FreeReportFormat(int unusedParam)
			{
				this.DatabaseFieldName = String.Empty;
				this.NameToShowUser = String.Empty;
				this.Tag = String.Empty;
				this.Required = false;
				this.VariableType = 0;
				this.ComboList = String.Empty;
				this.Type = 0;
				this.RowNumber = 0;
				this.ToolTip = String.Empty;
			}
		};

		public const int MAXFREEVARIABLES = 37;
		// this will be used to find out which report is being processed and use this in multiple modules
		// VBto upgrade warning: 'Return' As Variant --> As string
		public static string ReturnLocationString(ref short intType, ref int lngAcct)
		{
			string ReturnLocationString = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				// This function will get the location from the card(s) of an account and return
				// nothing, a string of all locations or just one location
				// If intType = 0 then it will return a string with all locations
				// If intType = 1 then it will return a string with only 1 location and nothing if the account has multiple locations
				// If intType = 2 then it will return an empty string
				clsDRWrapper rsL = new clsDRWrapper();
				string strTemp = "";
				string strOrigLoc = "";
				bool boolMultiple = false;
				string[] strLocations = new string[99 + 1];
				int intCT;
				bool boolMatch = false;
				if (intType != 2)
				{
					rsL.OpenRecordset("SELECT * FROM Master WHERE RSAccount = " + FCConvert.ToString(lngAcct), modExtraModules.strREDatabase);
					if (!rsL.EndOfFile())
					{
						if (Conversion.Val(rsL.Get_Fields_String("RSLOCNUMALPH")) != 0)
						{
							strOrigLoc = FCConvert.ToString(rsL.Get_Fields_String("RSLOCNUMALPH")) + " " + Strings.Trim(Strings.UCase(FCConvert.ToString(rsL.Get_Fields_String("RSLocStreet"))));
							strLocations[0] = FCConvert.ToString(rsL.Get_Fields_String("RSLOCNUMALPH")) + " " + Strings.Trim(FCConvert.ToString(rsL.Get_Fields_String("RSLocStreet")));
						}
						else
						{
							strOrigLoc = Strings.Trim(Strings.UCase(FCConvert.ToString(rsL.Get_Fields_String("RSLocStreet"))));
							strLocations[0] = Strings.Trim(FCConvert.ToString(rsL.Get_Fields_String("RSLocStreet")));
						}
						if (rsL.RecordCount() == 1)
						{
							boolMultiple = false;
						}
						else
						{
							rsL.MoveNext();
							// this will start the check on the second record
							while (!rsL.EndOfFile())
							{
								if (Conversion.Val(rsL.Get_Fields_String("RSLOCNUMALPH")) != 0)
								{
									strTemp = FCConvert.ToString(rsL.Get_Fields_String("RSLOCNUMALPH")) + " " + Strings.Trim(FCConvert.ToString(rsL.Get_Fields_String("RSLocStreet")));
								}
								else
								{
									strTemp = Strings.Trim(FCConvert.ToString(rsL.Get_Fields_String("RSLocStreet")));
								}
								if (strTemp != "" && Strings.UCase(strTemp) != strOrigLoc)
								{
									// if a non blank location that is different than the original, than this account
									// has multiple locations on one account and has to be dealt with
									boolMultiple = true;
									boolMatch = false;
									// check all the other locations to make sure that do not match
									for (intCT = 0; intCT <= 99; intCT++)
									{
										if (strLocations[intCT] == "")
										{
											break;
										}
										else if (Strings.UCase(strLocations[intCT]) == strTemp)
										{
											boolMatch = true;
										}
									}
									if (intCT < 99 && !boolMatch)
									{
										// fill the array with the new location
										strLocations[intCT] = strTemp;
									}
								}
								rsL.MoveNext();
							}
						}
						if (boolMultiple)
						{
							if (intType == 0)
							{
								strTemp = strLocations[0];
								for (intCT = 1; intCT <= 99; intCT++)
								{
									if (strLocations[intCT] != "")
									{
										strTemp += ", " + strLocations[intCT];
									}
									else
									{
										break;
									}
								}
								ReturnLocationString = strTemp;
							}
							else
							{
								// Multiple Locations so return none
								ReturnLocationString = "";
							}
						}
						else
						{
							// if this is not a multiple then print the one location
							ReturnLocationString = strOrigLoc;
						}
					}
					else
					{
						ReturnLocationString = "";
					}
				}
				else
				{
					ReturnLocationString = "";
				}
				return ReturnLocationString;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Returning Location String - " + FCConvert.ToString(lngAcct));
			}
			return ReturnLocationString;
		}

		public class StaticVariables
		{
			// this should be the same or less then the max element number in frfFreeReport array
			public FreeReportFormat[] frfFreeReport = new FreeReportFormat[MAXFREEVARIABLES + 1];
			// this is the array to store them
			public string strFreeReportType = "";
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
