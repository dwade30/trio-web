﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmImportBatch.
	/// </summary>
	partial class frmImportBatch : BaseForm
	{
		public fecherFoundation.FCFrame fraBatchQuestions;
		public fecherFoundation.FCButton btnBrowse;
		public fecherFoundation.FCTextBox txtFileName;
		public fecherFoundation.FCComboBox cmbTaxYear;
		public fecherFoundation.FCTextBox txtPaidBy;
		public fecherFoundation.FCTextBox txtTellerID;
		public fecherFoundation.FCCheckBox chkReceipt;
		public T2KDateBox txtPaymentDate2;
		public T2KDateBox txtEffectiveDate;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel lblPaymentDate;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel lblEffectiveDate;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmImportBatch));
			this.fraBatchQuestions = new fecherFoundation.FCFrame();
			this.btnBrowse = new fecherFoundation.FCButton();
			this.txtFileName = new fecherFoundation.FCTextBox();
			this.cmbTaxYear = new fecherFoundation.FCComboBox();
			this.txtPaidBy = new fecherFoundation.FCTextBox();
			this.txtTellerID = new fecherFoundation.FCTextBox();
			this.chkReceipt = new fecherFoundation.FCCheckBox();
			this.txtPaymentDate2 = new Global.T2KDateBox();
			this.txtEffectiveDate = new Global.T2KDateBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.lblPaymentDate = new fecherFoundation.FCLabel();
			this.Label6 = new fecherFoundation.FCLabel();
			this.Label8 = new fecherFoundation.FCLabel();
			this.lblEffectiveDate = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.btnProcess = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraBatchQuestions)).BeginInit();
			this.fraBatchQuestions.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnBrowse)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkReceipt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 542);
			this.BottomPanel.Size = new System.Drawing.Size(651, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraBatchQuestions);
			this.ClientArea.Size = new System.Drawing.Size(651, 482);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(651, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(26, 26);
			this.HeaderText.Size = new System.Drawing.Size(198, 30);
			this.HeaderText.Text = "Import Batch File";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// fraBatchQuestions
			// 
			this.fraBatchQuestions.Controls.Add(this.btnBrowse);
			this.fraBatchQuestions.Controls.Add(this.txtFileName);
			this.fraBatchQuestions.Controls.Add(this.cmbTaxYear);
			this.fraBatchQuestions.Controls.Add(this.txtPaidBy);
			this.fraBatchQuestions.Controls.Add(this.txtTellerID);
			this.fraBatchQuestions.Controls.Add(this.chkReceipt);
			this.fraBatchQuestions.Controls.Add(this.txtPaymentDate2);
			this.fraBatchQuestions.Controls.Add(this.txtEffectiveDate);
			this.fraBatchQuestions.Controls.Add(this.Label1);
			this.fraBatchQuestions.Controls.Add(this.Label3);
			this.fraBatchQuestions.Controls.Add(this.lblPaymentDate);
			this.fraBatchQuestions.Controls.Add(this.Label6);
			this.fraBatchQuestions.Controls.Add(this.Label8);
			this.fraBatchQuestions.Controls.Add(this.lblEffectiveDate);
			this.fraBatchQuestions.Location = new System.Drawing.Point(0, 0);
			this.fraBatchQuestions.Name = "fraBatchQuestions";
			this.fraBatchQuestions.Size = new System.Drawing.Size(603, 438);
			this.fraBatchQuestions.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.fraBatchQuestions, null);
			this.fraBatchQuestions.UseMnemonic = false;
			this.fraBatchQuestions.Visible = false;
			// 
			// btnBrowse
			// 
			this.btnBrowse.AppearanceKey = "actionButton";
			this.btnBrowse.Location = new System.Drawing.Point(517, 378);
			this.btnBrowse.Name = "btnBrowse";
			this.btnBrowse.Size = new System.Drawing.Size(80, 40);
			this.btnBrowse.TabIndex = 13;
			this.btnBrowse.Text = "Browse";
			this.ToolTip1.SetToolTip(this.btnBrowse, null);
			// 
			// txtFileName
			// 
			this.txtFileName.AutoSize = false;
			this.txtFileName.BackColor = System.Drawing.SystemColors.Window;
			this.txtFileName.Location = new System.Drawing.Point(190, 378);
			this.txtFileName.Name = "txtFileName";
			this.txtFileName.Size = new System.Drawing.Size(307, 40);
			this.txtFileName.TabIndex = 12;
			this.txtFileName.Text = "Text1";
			this.ToolTip1.SetToolTip(this.txtFileName, null);
			// 
			// cmbTaxYear
			// 
			this.cmbTaxYear.AutoSize = false;
			this.cmbTaxYear.BackColor = System.Drawing.SystemColors.Window;
			this.cmbTaxYear.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbTaxYear.FormattingEnabled = true;
			this.cmbTaxYear.Location = new System.Drawing.Point(190, 138);
			this.cmbTaxYear.Name = "cmbTaxYear";
			this.cmbTaxYear.Size = new System.Drawing.Size(159, 40);
			this.cmbTaxYear.TabIndex = 4;
			this.cmbTaxYear.Text = "Combo1";
			this.ToolTip1.SetToolTip(this.cmbTaxYear, null);
			// 
			// txtPaidBy
			// 
			this.txtPaidBy.MaxLength = 25;
			this.txtPaidBy.AutoSize = false;
			this.txtPaidBy.BackColor = System.Drawing.SystemColors.Window;
			this.txtPaidBy.Location = new System.Drawing.Point(190, 318);
			this.txtPaidBy.Name = "txtPaidBy";
			this.txtPaidBy.Size = new System.Drawing.Size(159, 40);
			this.txtPaidBy.TabIndex = 10;
			this.ToolTip1.SetToolTip(this.txtPaidBy, null);
			// 
			// txtTellerID
			// 
			this.txtTellerID.MaxLength = 3;
			this.txtTellerID.AutoSize = false;
			this.txtTellerID.BackColor = System.Drawing.SystemColors.Window;
			this.txtTellerID.Location = new System.Drawing.Point(190, 78);
			this.txtTellerID.Name = "txtTellerID";
			this.txtTellerID.Size = new System.Drawing.Size(159, 40);
			this.txtTellerID.TabIndex = 2;
			this.ToolTip1.SetToolTip(this.txtTellerID, null);
			// 
			// chkReceipt
			// 
			this.chkReceipt.Location = new System.Drawing.Point(20, 30);
			this.chkReceipt.Name = "chkReceipt";
			this.chkReceipt.Size = new System.Drawing.Size(250, 28);
			this.chkReceipt.TabIndex = 0;
			this.chkReceipt.Text = "Default Receipt Option to \'Yes\'";
			this.ToolTip1.SetToolTip(this.chkReceipt, null);
			// 
			// txtPaymentDate2
			// 
			this.txtPaymentDate2.MaxLength = 10;
			this.txtPaymentDate2.Location = new System.Drawing.Point(190, 198);
			this.txtPaymentDate2.Mask = "##/##/####";
			this.txtPaymentDate2.Name = "txtPaymentDate2";
			this.txtPaymentDate2.Size = new System.Drawing.Size(159, 40);
			this.txtPaymentDate2.TabIndex = 6;
			this.txtPaymentDate2.Text = "  /  /";
			this.ToolTip1.SetToolTip(this.txtPaymentDate2, null);
			// 
			// txtEffectiveDate
			// 
			this.txtEffectiveDate.MaxLength = 10;
			this.txtEffectiveDate.Location = new System.Drawing.Point(190, 258);
			this.txtEffectiveDate.Mask = "##/##/####";
			this.txtEffectiveDate.Name = "txtEffectiveDate";
			this.txtEffectiveDate.Size = new System.Drawing.Size(159, 40);
			this.txtEffectiveDate.TabIndex = 8;
			this.txtEffectiveDate.Text = "  /  /";
			this.ToolTip1.SetToolTip(this.txtEffectiveDate, null);
			// 
			// Label1
			// 
			this.Label1.AutoSize = true;
			this.Label1.Location = new System.Drawing.Point(30, 392);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(34, 16);
			this.Label1.TabIndex = 11;
			this.Label1.Text = "FILE";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.Label1, null);
			// 
			// Label3
			// 
			this.Label3.AutoSize = true;
			this.Label3.Location = new System.Drawing.Point(30, 152);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(68, 16);
			this.Label3.TabIndex = 3;
			this.Label3.Text = "TAX YEAR";
			this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.Label3, null);
			// 
			// lblPaymentDate
			// 
			this.lblPaymentDate.AutoSize = true;
			this.lblPaymentDate.Location = new System.Drawing.Point(30, 212);
			this.lblPaymentDate.Name = "lblPaymentDate";
			this.lblPaymentDate.Size = new System.Drawing.Size(103, 16);
			this.lblPaymentDate.TabIndex = 5;
			this.lblPaymentDate.Text = "PAYMENT DATE";
			this.lblPaymentDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblPaymentDate, "Recorded Transaction Date");
			// 
			// Label6
			// 
			this.Label6.AutoSize = true;
			this.Label6.Location = new System.Drawing.Point(30, 332);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(56, 16);
			this.Label6.TabIndex = 9;
			this.Label6.Text = "PAID BY";
			this.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.Label6, null);
			// 
			// Label8
			// 
			this.Label8.AutoSize = true;
			this.Label8.Location = new System.Drawing.Point(30, 92);
			this.Label8.Name = "Label8";
			this.Label8.Size = new System.Drawing.Size(70, 16);
			this.Label8.TabIndex = 1;
			this.Label8.Text = "TELLER ID";
			this.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.Label8, null);
			// 
			// lblEffectiveDate
			// 
			this.lblEffectiveDate.AutoSize = true;
			this.lblEffectiveDate.Location = new System.Drawing.Point(30, 272);
			this.lblEffectiveDate.Name = "lblEffectiveDate";
			this.lblEffectiveDate.Size = new System.Drawing.Size(112, 16);
			this.lblEffectiveDate.TabIndex = 7;
			this.lblEffectiveDate.Text = "EFFECTIVE DATE";
			this.lblEffectiveDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblEffectiveDate, "Effective Transaction Date");
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.Text = "File";
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(234, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(160, 48);
			this.btnProcess.TabIndex = 1;
			this.btnProcess.Text = "Save & Continue";
			this.ToolTip1.SetToolTip(this.btnProcess, null);
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// frmImportBatch
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(651, 650);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmImportBatch";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Import Batch File";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmImportBatch_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmImportBatch_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraBatchQuestions)).EndInit();
			this.fraBatchQuestions.ResumeLayout(false);
			this.fraBatchQuestions.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnBrowse)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkReceipt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton btnProcess;
	}
}
