﻿using fecherFoundation;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmReceiptInput.
	/// </summary>
	public partial class frmReceiptInput : fecherFoundation.FCForm
	{
		public frmReceiptInput()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmReceiptInput InstancePtr
		{
			get
			{
				return (frmReceiptInput)Sys.GetInstance(typeof(frmReceiptInput));
			}
		}

		protected frmReceiptInput _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// This is a dummy form to allow the fix for bucksport when the focus was not on
		// the getmasteraccount screen when coming through CR
		public bool boolLoadingUTsearch;

		private void frmReceiptInput_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmReceiptInput.Icon	= "frmCLReceiptInput.frx":0000";
			//frmReceiptInput.ScaleWidth	= 4680;
			//frmReceiptInput.ScaleHeight	= 3090;
			//frmReceiptInput.LinkTopic	= "Form1";
			//End Unmaped Properties
		}
	}
}
