﻿using Wisej.Web;

namespace TWCL0000.AccountPayment
{
    partial class frmTaxAccountStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTaxAccountStatus));
            this.lblAssocAcct = new fecherFoundation.FCLabel();
            this.lblGrpInfo = new fecherFoundation.FCLabel();
            this.lblAssocAcct_Pmt = new fecherFoundation.FCLabel();
            this.lblAcctComment = new fecherFoundation.FCLabel();
            this.lblTaxAcquired = new fecherFoundation.FCLabel();
            this.lblGrpInfo_Pmt = new fecherFoundation.FCLabel();
            this.lblAccount = new fecherFoundation.FCHeader();
            this.imgNote = new fecherFoundation.FCPictureBox();
            this.AccountInfoPanel = new Wisej.Web.TableLayoutPanel();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuFileInterestedParty = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePreviousAccount = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileNextAccount = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileAccountInfo = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileTaxInfoSheet = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePrintRateInfo = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileComment = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileREComment = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileEditNote = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePriority = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileReprintReceipt = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPayment = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileOptionLoadValidAccount = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPaymentClearPayment = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPaymentClearList = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileOptionsBlankBill = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileDischarge = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePrintAltLDN = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePrintBPReport = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePrintGroupInfo = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRedistributeCredits = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.toolTip1 = new Wisej.Web.ToolTip(this.components);
            this.lblTaxClub = new fecherFoundation.FCLabel();
            this.Label1_9 = new fecherFoundation.FCLabel();
            this.Label1_8 = new fecherFoundation.FCLabel();
            this.flowLayoutPanel1 = new Wisej.Web.FlowLayoutPanel();
            this.panel2 = new Wisej.Web.Panel();
            this.cmbYear = new Wisej.Web.ComboBox();
            this.Label1_3 = new fecherFoundation.FCLabel();
            this.panel3 = new Wisej.Web.Panel();
            this.txtTransactionDate = new Global.T2KDateBox();
            this.Label1_1 = new fecherFoundation.FCLabel();
            this.panel4 = new Wisej.Web.Panel();
            this.Label1_10 = new fecherFoundation.FCLabel();
            this.txtReference = new fecherFoundation.FCTextBox();
            this.panel5 = new Wisej.Web.Panel();
            this.Label1_2 = new fecherFoundation.FCLabel();
            this.cmbPeriod = new fecherFoundation.FCComboBox();
            this.panel6 = new Wisej.Web.Panel();
            this.fcLabel2 = new fecherFoundation.FCLabel();
            this.cmbCode = new Wisej.Web.ComboBox();
            this.panel7 = new Wisej.Web.Panel();
            this.txtCD = new fecherFoundation.FCTextBox();
            this.panel8 = new Wisej.Web.Panel();
            this.txtCash = new fecherFoundation.FCTextBox();
            this.panel9 = new Wisej.Web.Panel();
            this.txtAcctNumber = new fecherFoundation.FCGrid();
            this.Label1_11 = new fecherFoundation.FCLabel();
            this.AmountPanel = new Wisej.Web.Panel();
            this.txtAmount = new fecherFoundation.FCTextBox();
            this.lblAmount = new fecherFoundation.FCLabel();
            this.PrincipalPanel = new Wisej.Web.Panel();
            this.txtPrincipal = new fecherFoundation.FCTextBox();
            this.lblPrincipal = new fecherFoundation.FCLabel();
            this.InterestPanel = new Wisej.Web.Panel();
            this.txtInterest = new fecherFoundation.FCTextBox();
            this.lblInterest = new fecherFoundation.FCLabel();
            this.CostsPanel = new Wisej.Web.Panel();
            this.txtCosts = new fecherFoundation.FCTextBox();
            this.lblCosts = new fecherFoundation.FCLabel();
            this.PaymentsGrid = new fecherFoundation.FCGrid();
            this.cmdProcessEffective = new fecherFoundation.FCButton();
            this.fraPayment = new fecherFoundation.FCPanel();
            this.txtTotalPendingDue = new fecherFoundation.FCTextBox();
            this.PendingGrid = new fecherFoundation.FCGrid();
            this.txtTransactionDate2 = new fecherFoundation.FCTextBox();
            this.txtComments = new fecherFoundation.FCTextBox();
            this.txtBLID = new fecherFoundation.FCTextBox();
            this.PeriodGrid = new fecherFoundation.FCGrid();
            this.lblTotalPendingDue = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label1_12 = new fecherFoundation.FCLabel();
            this.btnAddPayment = new fecherFoundation.FCButton();
            this.txtAccountTotalsAsOf = new fecherFoundation.FCTextBox();
            this.btnSaveExit = new fecherFoundation.FCButton();
            this.panel10 = new Wisej.Web.Panel();
            this.txtTotalTotal = new fecherFoundation.FCLabel();
            this.txtTotalCosts = new fecherFoundation.FCLabel();
            this.txtTotalPrincipal = new fecherFoundation.FCLabel();
            this.txtTotalInterest = new fecherFoundation.FCLabel();
            this.GridPanel = new Wisej.Web.TableLayoutPanel();
            this.cmdPrintReceipt = new fecherFoundation.FCButton();
            this.javaScript1 = new Wisej.Web.JavaScript(this.components);
            this.btnChangeAccount = new fecherFoundation.FCButton();
            this.btnPrintAccountDetail = new fecherFoundation.FCButton();
            this.btnMortgageHolderInfo = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgNote)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransactionDate)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAcctNumber)).BeginInit();
            this.AmountPanel.SuspendLayout();
            this.PrincipalPanel.SuspendLayout();
            this.InterestPanel.SuspendLayout();
            this.CostsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PaymentsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessEffective)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraPayment)).BeginInit();
            this.fraPayment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PendingGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PeriodGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddPayment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSaveExit)).BeginInit();
            this.panel10.SuspendLayout();
            this.GridPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintReceipt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnChangeAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrintAccountDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMortgageHolderInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnSaveExit);
            this.BottomPanel.Location = new System.Drawing.Point(0, 831);
            this.BottomPanel.Size = new System.Drawing.Size(1040, 82);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.GridPanel);
            this.ClientArea.Controls.Add(this.fraPayment);
            this.ClientArea.Controls.Add(this.AccountInfoPanel);
            this.ClientArea.Size = new System.Drawing.Size(1060, 653);
            this.ClientArea.Controls.SetChildIndex(this.AccountInfoPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraPayment, 0);
            this.ClientArea.Controls.SetChildIndex(this.GridPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.btnPrintAccountDetail);
            this.TopPanel.Controls.Add(this.btnMortgageHolderInfo);
            this.TopPanel.Controls.Add(this.btnChangeAccount);
            this.TopPanel.Controls.Add(this.cmdPrintReceipt);
            this.TopPanel.Controls.Add(this.cmdProcessEffective);
            this.TopPanel.Controls.Add(this.btnAddPayment);
            this.TopPanel.Controls.Add(this.lblAccount);
            this.TopPanel.Controls.Add(this.lblAssocAcct);
            this.TopPanel.Controls.Add(this.lblGrpInfo);
            this.TopPanel.Controls.Add(this.lblAssocAcct_Pmt);
            this.TopPanel.Controls.Add(this.lblAcctComment);
            this.TopPanel.Controls.Add(this.lblTaxAcquired);
            this.TopPanel.Controls.Add(this.lblGrpInfo_Pmt);
            this.TopPanel.Controls.Add(this.imgNote);
            this.TopPanel.Size = new System.Drawing.Size(1060, 60);
            this.TopPanel.Controls.SetChildIndex(this.imgNote, 0);
            this.TopPanel.Controls.SetChildIndex(this.lblGrpInfo_Pmt, 0);
            this.TopPanel.Controls.SetChildIndex(this.lblTaxAcquired, 0);
            this.TopPanel.Controls.SetChildIndex(this.lblAcctComment, 0);
            this.TopPanel.Controls.SetChildIndex(this.lblAssocAcct_Pmt, 0);
            this.TopPanel.Controls.SetChildIndex(this.lblGrpInfo, 0);
            this.TopPanel.Controls.SetChildIndex(this.lblAssocAcct, 0);
            this.TopPanel.Controls.SetChildIndex(this.lblAccount, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnAddPayment, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdProcessEffective, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrintReceipt, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnChangeAccount, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnMortgageHolderInfo, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnPrintAccountDetail, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(20, 26);
            this.HeaderText.Size = new System.Drawing.Size(91, 28);
            this.HeaderText.Text = "Account";
            // 
            // lblAssocAcct
            // 
            this.lblAssocAcct.AutoSize = true;
            this.lblAssocAcct.Font = new System.Drawing.Font("default", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblAssocAcct.ForeColor = System.Drawing.Color.Red;
            this.lblAssocAcct.Location = new System.Drawing.Point(195, 4);
            this.lblAssocAcct.Name = "lblAssocAcct";
            this.lblAssocAcct.Size = new System.Drawing.Size(32, 23);
            this.lblAssocAcct.TabIndex = 122;
            this.lblAssocAcct.Text = "RE";
            this.lblAssocAcct.Visible = false;
            // 
            // lblGrpInfo
            // 
            this.lblGrpInfo.AutoSize = true;
            this.lblGrpInfo.Font = new System.Drawing.Font("default", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblGrpInfo.ForeColor = System.Drawing.Color.Red;
            this.lblGrpInfo.Location = new System.Drawing.Point(155, 3);
            this.lblGrpInfo.Name = "lblGrpInfo";
            this.lblGrpInfo.Size = new System.Drawing.Size(20, 23);
            this.lblGrpInfo.TabIndex = 123;
            this.lblGrpInfo.Text = "G";
            this.lblGrpInfo.Visible = false;
            this.lblGrpInfo.Click += new System.EventHandler(this.lblGrpInfo_Click);
            // 
            // lblAssocAcct_Pmt
            // 
            this.lblAssocAcct_Pmt.AutoSize = true;
            this.lblAssocAcct_Pmt.BackColor = System.Drawing.Color.FromName("@window");
            this.lblAssocAcct_Pmt.Font = new System.Drawing.Font("default", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblAssocAcct_Pmt.ForeColor = System.Drawing.Color.Red;
            this.lblAssocAcct_Pmt.Location = new System.Drawing.Point(195, 4);
            this.lblAssocAcct_Pmt.Name = "lblAssocAcct_Pmt";
            this.lblAssocAcct_Pmt.Size = new System.Drawing.Size(32, 23);
            this.lblAssocAcct_Pmt.TabIndex = 120;
            this.lblAssocAcct_Pmt.Text = "RE";
            this.lblAssocAcct_Pmt.Visible = false;
            // 
            // lblAcctComment
            // 
            this.lblAcctComment.AutoSize = true;
            this.lblAcctComment.Font = new System.Drawing.Font("default", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblAcctComment.ForeColor = System.Drawing.Color.Red;
            this.lblAcctComment.Location = new System.Drawing.Point(77, 3);
            this.lblAcctComment.Name = "lblAcctComment";
            this.lblAcctComment.Size = new System.Drawing.Size(18, 23);
            this.lblAcctComment.TabIndex = 119;
            this.lblAcctComment.Text = "C";
            this.lblAcctComment.Visible = false;
            // 
            // lblTaxAcquired
            // 
            this.lblTaxAcquired.AutoSize = true;
            this.lblTaxAcquired.Font = new System.Drawing.Font("default", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblTaxAcquired.ForeColor = System.Drawing.Color.Red;
            this.lblTaxAcquired.Location = new System.Drawing.Point(112, 3);
            this.lblTaxAcquired.Name = "lblTaxAcquired";
            this.lblTaxAcquired.Size = new System.Drawing.Size(31, 23);
            this.lblTaxAcquired.TabIndex = 118;
            this.lblTaxAcquired.Text = "TA";
            this.lblTaxAcquired.Visible = false;
            // 
            // lblGrpInfo_Pmt
            // 
            this.lblGrpInfo_Pmt.BackColor = System.Drawing.Color.Transparent;
            this.lblGrpInfo_Pmt.Font = new System.Drawing.Font("default", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblGrpInfo_Pmt.ForeColor = System.Drawing.Color.Red;
            this.lblGrpInfo_Pmt.Location = new System.Drawing.Point(130, 3);
            this.lblGrpInfo_Pmt.Name = "lblGrpInfo_Pmt";
            this.lblGrpInfo_Pmt.Size = new System.Drawing.Size(27, 25);
            this.lblGrpInfo_Pmt.TabIndex = 121;
            // 
            // lblAccount
            // 
            this.lblAccount.BackColor = System.Drawing.Color.Transparent;
            this.lblAccount.Location = new System.Drawing.Point(127, 26);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(132, 30);
            this.lblAccount.TabIndex = 125;
            // 
            // imgNote
            // 
            this.imgNote.AllowDrop = false;
            this.imgNote.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgNote.Cursor = Wisej.Web.Cursors.Default;
            this.imgNote.Image = ((System.Drawing.Image)(resources.GetObject("imgNote.Image")));
            this.imgNote.Location = new System.Drawing.Point(30, 3);
            this.imgNote.Name = "imgNote";
            this.imgNote.Size = new System.Drawing.Size(26, 23);
            this.imgNote.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgNote.Visible = false;
            // 
            // AccountInfoPanel
            // 
            this.AccountInfoPanel.ColumnCount = 2;
            this.AccountInfoPanel.ColumnStyles.Clear();
            this.AccountInfoPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 100F));
            this.AccountInfoPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 331F));
            this.AccountInfoPanel.Location = new System.Drawing.Point(3, 4);
            this.AccountInfoPanel.Margin = new Wisej.Web.Padding(3, 3, 0, 3);
            this.AccountInfoPanel.Name = "AccountInfoPanel";
            this.AccountInfoPanel.RowCount = 1;
            this.AccountInfoPanel.RowStyles.Clear();
            this.AccountInfoPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.AccountInfoPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.AccountInfoPanel.Size = new System.Drawing.Size(1036, 120);
            this.AccountInfoPanel.TabIndex = 1;
            this.AccountInfoPanel.TabStop = true;
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileInterestedParty,
            this.mnuFilePreviousAccount,
            this.mnuFileNextAccount,
            this.mnuFileAccountInfo,
            this.mnuFileTaxInfoSheet,
            this.mnuFilePrintRateInfo,
            this.mnuFileComment,
            this.mnuFileREComment,
            this.mnuFileEditNote,
            this.mnuFilePriority,
            this.mnuFileReprintReceipt,
            this.mnuPayment,
            this.mnuFileDischarge,
            this.mnuFilePrintAltLDN,
            this.mnuFilePrintBPReport,
            this.mnuFilePrintGroupInfo,
            this.mnuRedistributeCredits});
            this.MainMenu1.Name = null;
            // 
            // mnuFileInterestedParty
            // 
            this.mnuFileInterestedParty.Index = 0;
            this.mnuFileInterestedParty.Name = "mnuFileInterestedParty";
            this.mnuFileInterestedParty.Shortcut = Wisej.Web.Shortcut.ShiftF4;
            this.mnuFileInterestedParty.Text = "Interested Party Information";
            // 
            // mnuFilePreviousAccount
            // 
            this.mnuFilePreviousAccount.Index = 1;
            this.mnuFilePreviousAccount.Name = "mnuFilePreviousAccount";
            this.mnuFilePreviousAccount.Shortcut = Wisej.Web.Shortcut.F7;
            this.mnuFilePreviousAccount.Text = "Previous Account";
            this.mnuFilePreviousAccount.Visible = false;
            // 
            // mnuFileNextAccount
            // 
            this.mnuFileNextAccount.Index = 2;
            this.mnuFileNextAccount.Name = "mnuFileNextAccount";
            this.mnuFileNextAccount.Shortcut = Wisej.Web.Shortcut.F8;
            this.mnuFileNextAccount.Text = "Next Account";
            this.mnuFileNextAccount.Visible = false;
            // 
            // mnuFileAccountInfo
            // 
            this.mnuFileAccountInfo.Index = 3;
            this.mnuFileAccountInfo.Name = "mnuFileAccountInfo";
            this.mnuFileAccountInfo.Text = "Show Account Info";
            // 
            // mnuFileTaxInfoSheet
            // 
            this.mnuFileTaxInfoSheet.Index = 4;
            this.mnuFileTaxInfoSheet.Name = "mnuFileTaxInfoSheet";
            this.mnuFileTaxInfoSheet.Text = "Print Tax Information Sheet";
            // 
            // mnuFilePrintRateInfo
            // 
            this.mnuFilePrintRateInfo.Index = 5;
            this.mnuFilePrintRateInfo.Name = "mnuFilePrintRateInfo";
            this.mnuFilePrintRateInfo.Text = "Print Rate/Payment Info";
            this.mnuFilePrintRateInfo.Visible = false;
            // 
            // mnuFileComment
            // 
            this.mnuFileComment.Index = 6;
            this.mnuFileComment.Name = "mnuFileComment";
            this.mnuFileComment.Text = "Account Comment";
            // 
            // mnuFileREComment
            // 
            this.mnuFileREComment.Index = 7;
            this.mnuFileREComment.Name = "mnuFileREComment";
            this.mnuFileREComment.Text = "Add RE / CL Comment";
            // 
            // mnuFileEditNote
            // 
            this.mnuFileEditNote.Index = 8;
            this.mnuFileEditNote.Name = "mnuFileEditNote";
            this.mnuFileEditNote.Text = "Add/Edit Note";
            // 
            // mnuFilePriority
            // 
            this.mnuFilePriority.Enabled = false;
            this.mnuFilePriority.Index = 9;
            this.mnuFilePriority.Name = "mnuFilePriority";
            this.mnuFilePriority.Text = "Set Note Priority";
            this.mnuFilePriority.Visible = false;
            // 
            // mnuFileReprintReceipt
            // 
            this.mnuFileReprintReceipt.Index = 10;
            this.mnuFileReprintReceipt.Name = "mnuFileReprintReceipt";
            this.mnuFileReprintReceipt.Text = "Reprint Receipt";
            this.mnuFileReprintReceipt.Visible = false;
            // 
            // mnuPayment
            // 
            this.mnuPayment.Index = 11;
            this.mnuPayment.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileOptionLoadValidAccount,
            this.mnuPaymentClearPayment,
            this.mnuPaymentClearList,
            this.mnuFileOptionsBlankBill});
            this.mnuPayment.Name = "mnuPayment";
            this.mnuPayment.Text = "Options";
            this.mnuPayment.Visible = false;
            // 
            // mnuFileOptionLoadValidAccount
            // 
            this.mnuFileOptionLoadValidAccount.Index = 0;
            this.mnuFileOptionLoadValidAccount.Name = "mnuFileOptionLoadValidAccount";
            this.mnuFileOptionLoadValidAccount.Text = "Load Valid Account";
            // 
            // mnuPaymentClearPayment
            // 
            this.mnuPaymentClearPayment.Index = 1;
            this.mnuPaymentClearPayment.Name = "mnuPaymentClearPayment";
            this.mnuPaymentClearPayment.Text = "Clear Payment Boxes";
            // 
            // mnuPaymentClearList
            // 
            this.mnuPaymentClearList.Index = 2;
            this.mnuPaymentClearList.Name = "mnuPaymentClearList";
            this.mnuPaymentClearList.Text = "Clear Pending Transactions";
            // 
            // mnuFileOptionsBlankBill
            // 
            this.mnuFileOptionsBlankBill.Enabled = false;
            this.mnuFileOptionsBlankBill.Index = 3;
            this.mnuFileOptionsBlankBill.Name = "mnuFileOptionsBlankBill";
            this.mnuFileOptionsBlankBill.Text = "Create Blank Billing Record";
            this.mnuFileOptionsBlankBill.Visible = false;
            // 
            // mnuFileDischarge
            // 
            this.mnuFileDischarge.Index = 12;
            this.mnuFileDischarge.Name = "mnuFileDischarge";
            this.mnuFileDischarge.Text = "Print Lien Discharge Notice";
            this.mnuFileDischarge.Visible = false;
            // 
            // mnuFilePrintAltLDN
            // 
            this.mnuFilePrintAltLDN.Index = 13;
            this.mnuFilePrintAltLDN.Name = "mnuFilePrintAltLDN";
            this.mnuFilePrintAltLDN.Text = "Print Lien Discharge Report";
            this.mnuFilePrintAltLDN.Visible = false;
            // 
            // mnuFilePrintBPReport
            // 
            this.mnuFilePrintBPReport.Index = 14;
            this.mnuFilePrintBPReport.Name = "mnuFilePrintBPReport";
            this.mnuFilePrintBPReport.Text = "Print Book Page Report";
            // 
            // mnuFilePrintGroupInfo
            // 
            this.mnuFilePrintGroupInfo.Index = 15;
            this.mnuFilePrintGroupInfo.Name = "mnuFilePrintGroupInfo";
            this.mnuFilePrintGroupInfo.Text = "Print Group Listing";
            this.mnuFilePrintGroupInfo.Visible = false;
            // 
            // mnuRedistributeCredits
            // 
            this.mnuRedistributeCredits.Index = 16;
            this.mnuRedistributeCredits.Name = "mnuRedistributeCredits";
            this.mnuRedistributeCredits.Text = "Apply Credits to Balances";
            this.mnuRedistributeCredits.Visible = false;
            // 
            // mnuProcess
            // 
            this.mnuProcess.Enabled = false;
            this.mnuProcess.Index = -1;
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // lblTaxClub
            // 
            this.lblTaxClub.AutoSize = true;
            this.lblTaxClub.Location = new System.Drawing.Point(205, 148);
            this.lblTaxClub.Name = "lblTaxClub";
            this.lblTaxClub.Size = new System.Drawing.Size(21, 15);
            this.lblTaxClub.TabIndex = 97;
            this.lblTaxClub.Text = "TC";
            this.toolTip1.SetToolTip(this.lblTaxClub, "Tax Club Payment");
            this.lblTaxClub.Visible = false;
            // 
            // Label1_9
            // 
            this.Label1_9.AutoSize = true;
            this.Label1_9.Name = "Label1_9";
            this.Label1_9.Size = new System.Drawing.Size(23, 15);
            this.Label1_9.TabIndex = 76;
            this.Label1_9.Text = "CD";
            this.Label1_9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.toolTip1.SetToolTip(this.Label1_9, "Cash Drawer");
            // 
            // Label1_8
            // 
            this.Label1_8.AutoSize = true;
            this.Label1_8.Name = "Label1_8";
            this.Label1_8.Size = new System.Drawing.Size(22, 15);
            this.Label1_8.TabIndex = 75;
            this.Label1_8.Text = "AC";
            this.Label1_8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.toolTip1.SetToolTip(this.Label1_8, "Affect Cash");
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.panel2);
            this.flowLayoutPanel1.Controls.Add(this.panel3);
            this.flowLayoutPanel1.Controls.Add(this.panel4);
            this.flowLayoutPanel1.Controls.Add(this.panel5);
            this.flowLayoutPanel1.Controls.Add(this.panel6);
            this.flowLayoutPanel1.Controls.Add(this.panel7);
            this.flowLayoutPanel1.Controls.Add(this.panel8);
            this.flowLayoutPanel1.Controls.Add(this.panel9);
            this.flowLayoutPanel1.Controls.Add(this.AmountPanel);
            this.flowLayoutPanel1.Controls.Add(this.PrincipalPanel);
            this.flowLayoutPanel1.Controls.Add(this.InterestPanel);
            this.flowLayoutPanel1.Controls.Add(this.CostsPanel);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 53);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1028, 78);
            this.flowLayoutPanel1.TabIndex = 1;
            this.flowLayoutPanel1.TabStop = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.cmbYear);
            this.panel2.Controls.Add(this.Label1_3);
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(96, 72);
            this.panel2.TabIndex = 0;
            this.panel2.TabStop = true;
            // 
            // cmbYear
            // 
            this.cmbYear.AutoSize = false;
            this.cmbYear.BackColor = System.Drawing.SystemColors.Window;
            this.cmbYear.DisplayMember = "Description";
            this.cmbYear.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbYear.Location = new System.Drawing.Point(0, 18);
            this.cmbYear.Name = "cmbYear";
            this.cmbYear.Size = new System.Drawing.Size(96, 40);
            this.cmbYear.Sorted = true;
            this.cmbYear.TabIndex = 86;
            this.cmbYear.ValueMember = "ID";
            // 
            // Label1_3
            // 
            this.Label1_3.AutoSize = true;
            this.Label1_3.Name = "Label1_3";
            this.Label1_3.Size = new System.Drawing.Size(39, 15);
            this.Label1_3.TabIndex = 85;
            this.Label1_3.Text = "YEAR";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.txtTransactionDate);
            this.panel3.Controls.Add(this.Label1_1);
            this.panel3.Location = new System.Drawing.Point(105, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(113, 72);
            this.panel3.TabIndex = 1;
            this.panel3.TabStop = true;
            // 
            // txtTransactionDate
            // 
            this.txtTransactionDate.Location = new System.Drawing.Point(0, 18);
            this.txtTransactionDate.Name = "txtTransactionDate";
            this.txtTransactionDate.Size = new System.Drawing.Size(113, 22);
            this.txtTransactionDate.TabIndex = 87;
            // 
            // Label1_1
            // 
            this.Label1_1.AutoSize = true;
            this.Label1_1.Name = "Label1_1";
            this.Label1_1.Size = new System.Drawing.Size(39, 15);
            this.Label1_1.TabIndex = 86;
            this.Label1_1.Text = "DATE";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.Label1_10);
            this.panel4.Controls.Add(this.txtReference);
            this.panel4.Location = new System.Drawing.Point(224, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(115, 72);
            this.panel4.TabIndex = 2;
            this.panel4.TabStop = true;
            // 
            // Label1_10
            // 
            this.Label1_10.AutoSize = true;
            this.Label1_10.Name = "Label1_10";
            this.Label1_10.Size = new System.Drawing.Size(30, 15);
            this.Label1_10.TabIndex = 82;
            this.Label1_10.Text = "REF";
            // 
            // txtReference
            // 
            this.txtReference.BackColor = System.Drawing.SystemColors.Window;
            this.txtReference.Location = new System.Drawing.Point(0, 18);
            this.txtReference.MaxLength = 6;
            this.txtReference.Name = "txtReference";
            this.txtReference.Size = new System.Drawing.Size(115, 40);
            this.txtReference.TabIndex = 86;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.Label1_2);
            this.panel5.Controls.Add(this.cmbPeriod);
            this.panel5.Location = new System.Drawing.Point(345, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(57, 72);
            this.panel5.TabIndex = 3;
            this.panel5.TabStop = true;
            // 
            // Label1_2
            // 
            this.Label1_2.AutoSize = true;
            this.Label1_2.Name = "Label1_2";
            this.Label1_2.Size = new System.Drawing.Size(31, 15);
            this.Label1_2.TabIndex = 74;
            this.Label1_2.Text = "PER";
            // 
            // cmbPeriod
            // 
            this.cmbPeriod.BackColor = System.Drawing.SystemColors.Window;
            this.cmbPeriod.Location = new System.Drawing.Point(0, 18);
            this.cmbPeriod.Name = "cmbPeriod";
            this.cmbPeriod.Size = new System.Drawing.Size(57, 40);
            this.cmbPeriod.Sorted = true;
            this.cmbPeriod.TabIndex = 87;
            this.cmbPeriod.TabStop = false;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.fcLabel2);
            this.panel6.Controls.Add(this.cmbCode);
            this.panel6.Location = new System.Drawing.Point(408, 3);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(57, 72);
            this.panel6.TabIndex = 4;
            this.panel6.TabStop = true;
            // 
            // fcLabel2
            // 
            this.fcLabel2.Name = "fcLabel2";
            this.fcLabel2.Size = new System.Drawing.Size(54, 17);
            this.fcLabel2.TabIndex = 102;
            this.fcLabel2.Text = "CODE";
            // 
            // cmbCode
            // 
            this.cmbCode.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.cmbCode.AutoSize = false;
            this.cmbCode.BackColor = System.Drawing.SystemColors.Window;
            this.cmbCode.DisplayMember = "Description";
            this.cmbCode.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbCode.Location = new System.Drawing.Point(0, 18);
            this.cmbCode.Name = "cmbCode";
            this.cmbCode.Size = new System.Drawing.Size(57, 40);
            this.cmbCode.Sorted = true;
            this.cmbCode.TabIndex = 88;
            this.cmbCode.TabStop = false;
            this.cmbCode.ValueMember = "ID";
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.Label1_9);
            this.panel7.Controls.Add(this.txtCD);
            this.panel7.Location = new System.Drawing.Point(471, 3);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(42, 72);
            this.panel7.TabIndex = 5;
            this.panel7.TabStop = true;
            // 
            // txtCD
            // 
            this.txtCD.BackColor = System.Drawing.Color.FromName("@window");
            this.txtCD.Location = new System.Drawing.Point(0, 18);
            this.txtCD.MaxLength = 1;
            this.txtCD.Name = "txtCD";
            this.txtCD.Size = new System.Drawing.Size(42, 40);
            this.txtCD.TabIndex = 89;
            this.txtCD.TabStop = false;
            this.txtCD.Text = "Y";
            this.txtCD.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.Label1_8);
            this.panel8.Controls.Add(this.txtCash);
            this.panel8.Location = new System.Drawing.Point(519, 3);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(42, 72);
            this.panel8.TabIndex = 6;
            this.panel8.TabStop = true;
            // 
            // txtCash
            // 
            this.txtCash.BackColor = System.Drawing.Color.FromName("@window");
            this.txtCash.Enabled = false;
            this.txtCash.Location = new System.Drawing.Point(0, 18);
            this.txtCash.MaxLength = 1;
            this.txtCash.Name = "txtCash";
            this.txtCash.Size = new System.Drawing.Size(42, 40);
            this.txtCash.TabIndex = 95;
            this.txtCash.TabStop = false;
            this.txtCash.Text = "Y";
            this.txtCash.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.txtAcctNumber);
            this.panel9.Controls.Add(this.Label1_11);
            this.panel9.Location = new System.Drawing.Point(567, 3);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(143, 72);
            this.panel9.TabIndex = 7;
            this.panel9.TabStop = true;
            // 
            // txtAcctNumber
            // 
            this.txtAcctNumber.Cols = 1;
            this.txtAcctNumber.ColumnHeadersVisible = false;
            this.txtAcctNumber.ExtendLastCol = true;
            this.txtAcctNumber.FixedCols = 0;
            this.txtAcctNumber.FixedRows = 0;
            this.txtAcctNumber.Location = new System.Drawing.Point(0, 18);
            this.txtAcctNumber.Name = "txtAcctNumber";
            this.txtAcctNumber.RowHeadersVisible = false;
            this.txtAcctNumber.Rows = 1;
            this.txtAcctNumber.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollNone;
            this.txtAcctNumber.ShowFocusCell = false;
            this.txtAcctNumber.Size = new System.Drawing.Size(140, 42);
            this.txtAcctNumber.TabIndex = 92;
            // 
            // Label1_11
            // 
            this.Label1_11.AutoSize = true;
            this.Label1_11.Name = "Label1_11";
            this.Label1_11.Size = new System.Drawing.Size(50, 15);
            this.Label1_11.TabIndex = 91;
            this.Label1_11.Text = "ACCT #";
            // 
            // AmountPanel
            // 
            this.AmountPanel.Anchor = Wisej.Web.AnchorStyles.Left;
            this.AmountPanel.Controls.Add(this.txtAmount);
            this.AmountPanel.Controls.Add(this.lblAmount);
            this.AmountPanel.Location = new System.Drawing.Point(716, 3);
            this.AmountPanel.Name = "AmountPanel";
            this.AmountPanel.Size = new System.Drawing.Size(98, 72);
            this.AmountPanel.TabIndex = 11;
            this.AmountPanel.TabStop = true;
            // 
            // txtAmount
            // 
            this.txtAmount.BackColor = System.Drawing.SystemColors.Window;
            this.javaScript1.SetJavaScript(this.txtAmount, resources.GetString("txtAmount.JavaScript"));
            this.txtAmount.Location = new System.Drawing.Point(0, 18);
            this.txtAmount.MaxLength = 17;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(98, 40);
            this.txtAmount.TabIndex = 12;
            this.txtAmount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // lblAmount
            // 
            this.lblAmount.AutoSize = true;
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(60, 15);
            this.lblAmount.TabIndex = 92;
            this.lblAmount.Text = "AMOUNT";
            this.lblAmount.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // PrincipalPanel
            // 
            this.PrincipalPanel.Controls.Add(this.txtPrincipal);
            this.PrincipalPanel.Controls.Add(this.lblPrincipal);
            this.PrincipalPanel.Location = new System.Drawing.Point(820, 3);
            this.PrincipalPanel.Name = "PrincipalPanel";
            this.PrincipalPanel.Size = new System.Drawing.Size(98, 72);
            this.PrincipalPanel.TabIndex = 13;
            this.PrincipalPanel.TabStop = true;
            this.PrincipalPanel.Visible = false;
            // 
            // txtPrincipal
            // 
            this.txtPrincipal.BackColor = System.Drawing.SystemColors.Window;
            this.javaScript1.SetJavaScript(this.txtPrincipal, resources.GetString("txtPrincipal.JavaScript"));
            this.txtPrincipal.Location = new System.Drawing.Point(0, 18);
            this.txtPrincipal.MaxLength = 17;
            this.txtPrincipal.Name = "txtPrincipal";
            this.txtPrincipal.Size = new System.Drawing.Size(98, 40);
            this.txtPrincipal.TabIndex = 14;
            this.txtPrincipal.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // lblPrincipal
            // 
            this.lblPrincipal.AutoSize = true;
            this.lblPrincipal.Name = "lblPrincipal";
            this.lblPrincipal.Size = new System.Drawing.Size(73, 15);
            this.lblPrincipal.TabIndex = 92;
            this.lblPrincipal.Text = "PRINCIPAL";
            this.lblPrincipal.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // InterestPanel
            // 
            this.InterestPanel.Anchor = Wisej.Web.AnchorStyles.Left;
            this.InterestPanel.Controls.Add(this.txtInterest);
            this.InterestPanel.Controls.Add(this.lblInterest);
            this.InterestPanel.Location = new System.Drawing.Point(924, 3);
            this.InterestPanel.Name = "InterestPanel";
            this.InterestPanel.Size = new System.Drawing.Size(98, 72);
            this.InterestPanel.TabIndex = 15;
            this.InterestPanel.TabStop = true;
            this.InterestPanel.Visible = false;
            // 
            // txtInterest
            // 
            this.txtInterest.Anchor = Wisej.Web.AnchorStyles.Left;
            this.txtInterest.BackColor = System.Drawing.SystemColors.Window;
            this.javaScript1.SetJavaScript(this.txtInterest, resources.GetString("txtInterest.JavaScript"));
            this.txtInterest.Location = new System.Drawing.Point(0, 18);
            this.txtInterest.MaxLength = 12;
            this.txtInterest.Name = "txtInterest";
            this.txtInterest.Size = new System.Drawing.Size(98, 40);
            this.txtInterest.TabIndex = 16;
            this.txtInterest.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // lblInterest
            // 
            this.lblInterest.AutoSize = true;
            this.lblInterest.Name = "lblInterest";
            this.lblInterest.Size = new System.Drawing.Size(68, 15);
            this.lblInterest.TabIndex = 93;
            this.lblInterest.Text = "INTEREST";
            this.lblInterest.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // CostsPanel
            // 
            this.CostsPanel.Anchor = Wisej.Web.AnchorStyles.Left;
            this.CostsPanel.Controls.Add(this.txtCosts);
            this.CostsPanel.Controls.Add(this.lblCosts);
            this.CostsPanel.Location = new System.Drawing.Point(3, 81);
            this.CostsPanel.Name = "CostsPanel";
            this.CostsPanel.Size = new System.Drawing.Size(98, 72);
            this.CostsPanel.TabIndex = 18;
            this.CostsPanel.TabStop = true;
            this.CostsPanel.Visible = false;
            // 
            // txtCosts
            // 
            this.txtCosts.BackColor = System.Drawing.SystemColors.Window;
            this.javaScript1.SetJavaScript(this.txtCosts, resources.GetString("txtCosts.JavaScript"));
            this.txtCosts.Location = new System.Drawing.Point(0, 18);
            this.txtCosts.MaxLength = 11;
            this.txtCosts.Name = "txtCosts";
            this.txtCosts.Size = new System.Drawing.Size(98, 40);
            this.txtCosts.TabIndex = 19;
            this.txtCosts.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // lblCosts
            // 
            this.lblCosts.AutoSize = true;
            this.lblCosts.Name = "lblCosts";
            this.lblCosts.Size = new System.Drawing.Size(49, 15);
            this.lblCosts.TabIndex = 94;
            this.lblCosts.Text = "COSTS";
            this.lblCosts.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // PaymentsGrid
            // 
            this.PaymentsGrid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.PaymentsGrid.Cols = 1;
            this.PaymentsGrid.Location = new System.Drawing.Point(3, 3);
            this.PaymentsGrid.Name = "PaymentsGrid";
            this.PaymentsGrid.RowHeadersVisible = false;
            this.PaymentsGrid.Rows = 1;
            this.PaymentsGrid.Size = new System.Drawing.Size(1030, 243);
            this.PaymentsGrid.TabIndex = 54;
            this.PaymentsGrid.Visible = false;
            // 
            // cmdProcessEffective
            // 
            this.cmdProcessEffective.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdProcessEffective.Cursor = Wisej.Web.Cursors.Default;
            this.cmdProcessEffective.Location = new System.Drawing.Point(798, 29);
            this.cmdProcessEffective.Name = "cmdProcessEffective";
            this.cmdProcessEffective.Shortcut = Wisej.Web.Shortcut.F2;
            this.cmdProcessEffective.Size = new System.Drawing.Size(97, 24);
            this.cmdProcessEffective.TabIndex = 127;
            this.cmdProcessEffective.Text = "Effective Date";
            // 
            // fraPayment
            // 
            this.fraPayment.Controls.Add(this.flowLayoutPanel1);
            this.fraPayment.Controls.Add(this.txtTotalPendingDue);
            this.fraPayment.Controls.Add(this.PendingGrid);
            this.fraPayment.Controls.Add(this.txtTransactionDate2);
            this.fraPayment.Controls.Add(this.txtComments);
            this.fraPayment.Controls.Add(this.txtBLID);
            this.fraPayment.Controls.Add(this.PeriodGrid);
            this.fraPayment.Controls.Add(this.lblTaxClub);
            this.fraPayment.Controls.Add(this.lblTotalPendingDue);
            this.fraPayment.Controls.Add(this.Label8);
            this.fraPayment.Controls.Add(this.Label1_12);
            this.fraPayment.Location = new System.Drawing.Point(20, 431);
            this.fraPayment.Margin = new Wisej.Web.Padding(3, 3, 0, 3);
            this.fraPayment.MaximumSize = new System.Drawing.Size(2000, 400);
            this.fraPayment.MinimumSize = new System.Drawing.Size(800, 320);
            this.fraPayment.Name = "fraPayment";
            this.fraPayment.Size = new System.Drawing.Size(1036, 400);
            this.fraPayment.TabIndex = 70;
            // 
            // txtTotalPendingDue
            // 
            this.txtTotalPendingDue.BackColor = System.Drawing.SystemColors.Window;
            this.txtTotalPendingDue.Location = new System.Drawing.Point(827, 4);
            this.txtTotalPendingDue.LockedOriginal = true;
            this.txtTotalPendingDue.Name = "txtTotalPendingDue";
            this.txtTotalPendingDue.ReadOnly = true;
            this.txtTotalPendingDue.Size = new System.Drawing.Size(205, 40);
            this.txtTotalPendingDue.TabIndex = 73;
            this.txtTotalPendingDue.Text = "0.00";
            this.txtTotalPendingDue.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // PendingGrid
            // 
            this.PendingGrid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.PendingGrid.Cols = 13;
            this.PendingGrid.FixedCols = 0;
            this.PendingGrid.Location = new System.Drawing.Point(3, 186);
            this.PendingGrid.MinimumSize = new System.Drawing.Size(800, 128);
            this.PendingGrid.Name = "PendingGrid";
            this.PendingGrid.RowHeadersVisible = false;
            this.PendingGrid.Rows = 1;
            this.PendingGrid.Size = new System.Drawing.Size(1022, 208);
            this.PendingGrid.TabIndex = 21;
            // 
            // txtTransactionDate2
            // 
            this.txtTransactionDate2.Appearance = 0;
            this.txtTransactionDate2.BackColor = System.Drawing.SystemColors.Window;
            this.txtTransactionDate2.BorderStyle = Wisej.Web.BorderStyle.None;
            this.txtTransactionDate2.Location = new System.Drawing.Point(240, 135);
            this.txtTransactionDate2.MaxLength = 10;
            this.txtTransactionDate2.Name = "txtTransactionDate2";
            this.txtTransactionDate2.Size = new System.Drawing.Size(57, 40);
            this.txtTransactionDate2.TabIndex = 72;
            this.txtTransactionDate2.Visible = false;
            // 
            // txtComments
            // 
            this.txtComments.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.txtComments.BackColor = System.Drawing.SystemColors.Window;
            this.txtComments.Location = new System.Drawing.Point(415, 137);
            this.txtComments.MaxLength = 255;
            this.txtComments.Name = "txtComments";
            this.txtComments.Size = new System.Drawing.Size(615, 40);
            this.txtComments.TabIndex = 20;
            // 
            // txtBLID
            // 
            this.txtBLID.BackColor = System.Drawing.SystemColors.Window;
            this.txtBLID.Location = new System.Drawing.Point(1116, 86);
            this.txtBLID.Name = "txtBLID";
            this.txtBLID.Size = new System.Drawing.Size(34, 40);
            this.txtBLID.TabIndex = 101;
            this.txtBLID.Visible = false;
            // 
            // PeriodGrid
            // 
            this.PeriodGrid.Cols = 5;
            this.PeriodGrid.ColumnHeadersVisible = false;
            this.PeriodGrid.FixedCols = 0;
            this.PeriodGrid.FixedRows = 0;
            this.PeriodGrid.Location = new System.Drawing.Point(3, 4);
            this.PeriodGrid.Name = "PeriodGrid";
            this.PeriodGrid.RowHeadersVisible = false;
            this.PeriodGrid.Rows = 1;
            this.PeriodGrid.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollNone;
            this.PeriodGrid.Size = new System.Drawing.Size(679, 40);
            this.PeriodGrid.TabIndex = 70;
            this.PeriodGrid.TabStop = false;
            // 
            // lblTotalPendingDue
            // 
            this.lblTotalPendingDue.AutoSize = true;
            this.lblTotalPendingDue.Location = new System.Drawing.Point(701, 20);
            this.lblTotalPendingDue.Name = "lblTotalPendingDue";
            this.lblTotalPendingDue.Size = new System.Drawing.Size(109, 15);
            this.lblTotalPendingDue.TabIndex = 71;
            this.lblTotalPendingDue.Text = "TOTAL PENDING";
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(3, 152);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(169, 15);
            this.Label8.TabIndex = 98;
            this.Label8.Text = "PENDING TRANSACTIONS";
            // 
            // Label1_12
            // 
            this.Label1_12.AutoSize = true;
            this.Label1_12.Location = new System.Drawing.Point(344, 150);
            this.Label1_12.Name = "Label1_12";
            this.Label1_12.Size = new System.Drawing.Size(71, 15);
            this.Label1_12.TabIndex = 96;
            this.Label1_12.Text = "COMMENT";
            // 
            // btnAddPayment
            // 
            this.btnAddPayment.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnAddPayment.Location = new System.Drawing.Point(666, 29);
            this.btnAddPayment.Name = "btnAddPayment";
            this.btnAddPayment.Shortcut = Wisej.Web.Shortcut.F11;
            this.btnAddPayment.Size = new System.Drawing.Size(110, 24);
            this.btnAddPayment.TabIndex = 2;
            this.btnAddPayment.Text = "Add Payment";
            this.btnAddPayment.Visible = false;
            // 
            // txtAccountTotalsAsOf
            // 
            this.txtAccountTotalsAsOf.Appearance = 0;
            this.txtAccountTotalsAsOf.BackColor = System.Drawing.SystemColors.Window;
            this.txtAccountTotalsAsOf.BorderStyle = Wisej.Web.BorderStyle.None;
            this.txtAccountTotalsAsOf.Font = new System.Drawing.Font("@defaultBold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.txtAccountTotalsAsOf.Location = new System.Drawing.Point(0, 5);
            this.txtAccountTotalsAsOf.LockedOriginal = true;
            this.txtAccountTotalsAsOf.Margin = new Wisej.Web.Padding(0);
            this.txtAccountTotalsAsOf.Name = "txtAccountTotalsAsOf";
            this.txtAccountTotalsAsOf.ReadOnly = true;
            this.txtAccountTotalsAsOf.Size = new System.Drawing.Size(275, 40);
            this.txtAccountTotalsAsOf.TabIndex = 104;
            this.txtAccountTotalsAsOf.Text = "Account Totals As Of 00/00/0000";
            // 
            // btnSaveExit
            // 
            this.btnSaveExit.AppearanceKey = "acceptButton";
            this.btnSaveExit.Location = new System.Drawing.Point(409, 12);
            this.btnSaveExit.Name = "btnSaveExit";
            this.btnSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnSaveExit.Size = new System.Drawing.Size(211, 48);
            this.btnSaveExit.TabIndex = 3;
            this.btnSaveExit.Text = "Add Payment & Exit";
            // 
            // panel10
            // 
            this.panel10.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.panel10.Controls.Add(this.txtTotalTotal);
            this.panel10.Controls.Add(this.txtTotalCosts);
            this.panel10.Controls.Add(this.txtTotalPrincipal);
            this.panel10.Controls.Add(this.txtTotalInterest);
            this.panel10.Controls.Add(this.txtAccountTotalsAsOf);
            this.panel10.Location = new System.Drawing.Point(3, 252);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(1030, 44);
            this.panel10.TabIndex = 105;
            this.panel10.TabStop = true;
            // 
            // txtTotalTotal
            // 
            this.txtTotalTotal.Font = new System.Drawing.Font("@defaultBold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.txtTotalTotal.ForeColor = System.Drawing.Color.FromArgb(5, 204, 71);
            this.txtTotalTotal.Location = new System.Drawing.Point(874, 5);
            this.txtTotalTotal.Name = "txtTotalTotal";
            this.txtTotalTotal.Size = new System.Drawing.Size(154, 40);
            this.txtTotalTotal.TabIndex = 103;
            this.txtTotalTotal.Text = "0.00";
            this.txtTotalTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTotalCosts
            // 
            this.txtTotalCosts.Font = new System.Drawing.Font("@defaultBold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.txtTotalCosts.ForeColor = System.Drawing.Color.FromArgb(5, 204, 71);
            this.txtTotalCosts.Location = new System.Drawing.Point(735, 5);
            this.txtTotalCosts.Name = "txtTotalCosts";
            this.txtTotalCosts.Size = new System.Drawing.Size(130, 40);
            this.txtTotalCosts.TabIndex = 102;
            this.txtTotalCosts.Text = "0.00";
            this.txtTotalCosts.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTotalPrincipal
            // 
            this.txtTotalPrincipal.Font = new System.Drawing.Font("@defaultBold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.txtTotalPrincipal.ForeColor = System.Drawing.Color.FromArgb(5, 204, 71);
            this.txtTotalPrincipal.Location = new System.Drawing.Point(514, 5);
            this.txtTotalPrincipal.Name = "txtTotalPrincipal";
            this.txtTotalPrincipal.Size = new System.Drawing.Size(103, 40);
            this.txtTotalPrincipal.TabIndex = 100;
            this.txtTotalPrincipal.Text = "0.00";
            this.txtTotalPrincipal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTotalInterest
            // 
            this.txtTotalInterest.Font = new System.Drawing.Font("@defaultBold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.txtTotalInterest.ForeColor = System.Drawing.Color.FromArgb(5, 204, 71);
            this.txtTotalInterest.Location = new System.Drawing.Point(623, 5);
            this.txtTotalInterest.Name = "txtTotalInterest";
            this.txtTotalInterest.Size = new System.Drawing.Size(106, 40);
            this.txtTotalInterest.TabIndex = 101;
            this.txtTotalInterest.Text = "0.00";
            this.txtTotalInterest.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // GridPanel
            // 
            this.GridPanel.ColumnCount = 1;
            this.GridPanel.ColumnStyles.Clear();
            this.GridPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 100F));
            this.GridPanel.Controls.Add(this.panel10, 0, 1);
            this.GridPanel.Controls.Add(this.PaymentsGrid, 0, 0);
            this.GridPanel.Location = new System.Drawing.Point(20, 126);
            this.GridPanel.Margin = new Wisej.Web.Padding(3, 3, 0, 3);
            this.GridPanel.Name = "GridPanel";
            this.GridPanel.RowCount = 2;
            this.GridPanel.RowStyles.Clear();
            this.GridPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 100F));
            this.GridPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Absolute, 50F));
            this.GridPanel.Size = new System.Drawing.Size(1036, 299);
            this.GridPanel.TabIndex = 106;
            this.GridPanel.TabStop = true;
            // 
            // cmdPrintReceipt
            // 
            this.cmdPrintReceipt.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintReceipt.Enabled = false;
            this.cmdPrintReceipt.Location = new System.Drawing.Point(158, 30);
            this.cmdPrintReceipt.Name = "cmdPrintReceipt";
            this.cmdPrintReceipt.Shortcut = Wisej.Web.Shortcut.F9;
            this.cmdPrintReceipt.Size = new System.Drawing.Size(154, 24);
            this.cmdPrintReceipt.TabIndex = 129;
            this.cmdPrintReceipt.Text = "Print Receipt And Save";
            // 
            // btnChangeAccount
            // 
            this.btnChangeAccount.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnChangeAccount.Location = new System.Drawing.Point(922, 29);
            this.btnChangeAccount.Name = "btnChangeAccount";
            this.btnChangeAccount.Shortcut = Wisej.Web.Shortcut.F6;
            this.btnChangeAccount.Size = new System.Drawing.Size(117, 24);
            this.btnChangeAccount.TabIndex = 131;
            this.btnChangeAccount.Text = "Change Account";
            // 
            // btnPrintAccountDetail
            // 
            this.btnPrintAccountDetail.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnPrintAccountDetail.Location = new System.Drawing.Point(513, 29);
            this.btnPrintAccountDetail.Name = "btnPrintAccountDetail";
            this.btnPrintAccountDetail.Shortcut = Wisej.Web.Shortcut.F4;
            this.btnPrintAccountDetail.Size = new System.Drawing.Size(138, 24);
            this.btnPrintAccountDetail.TabIndex = 133;
            this.btnPrintAccountDetail.Text = "Print Account Detail";
            // 
            // btnMortgageHolderInfo
            // 
            this.btnMortgageHolderInfo.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnMortgageHolderInfo.Location = new System.Drawing.Point(313, 29);
            this.btnMortgageHolderInfo.Name = "btnMortgageHolderInfo";
            this.btnMortgageHolderInfo.Shortcut = Wisej.Web.Shortcut.F3;
            this.btnMortgageHolderInfo.Size = new System.Drawing.Size(194, 24);
            this.btnMortgageHolderInfo.TabIndex = 134;
            this.btnMortgageHolderInfo.Text = "Mortgage Holder Information";
            // 
            // frmTaxAccountStatus
            // 
            this.ClientSize = new System.Drawing.Size(1060, 713);
            this.Cursor = Wisej.Web.Cursors.Default;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmTaxAccountStatus";
            this.Text = "Collections Status";
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgNote)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransactionDate)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAcctNumber)).EndInit();
            this.AmountPanel.ResumeLayout(false);
            this.AmountPanel.PerformLayout();
            this.PrincipalPanel.ResumeLayout(false);
            this.PrincipalPanel.PerformLayout();
            this.InterestPanel.ResumeLayout(false);
            this.InterestPanel.PerformLayout();
            this.CostsPanel.ResumeLayout(false);
            this.CostsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PaymentsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessEffective)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraPayment)).EndInit();
            this.fraPayment.ResumeLayout(false);
            this.fraPayment.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PendingGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PeriodGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddPayment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSaveExit)).EndInit();
            this.panel10.ResumeLayout(false);
            this.GridPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintReceipt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnChangeAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrintAccountDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMortgageHolderInfo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public fecherFoundation.FCLabel lblAssocAcct;
        public fecherFoundation.FCLabel lblGrpInfo;
        public fecherFoundation.FCLabel lblAssocAcct_Pmt;
        public fecherFoundation.FCLabel lblAcctComment;
        public fecherFoundation.FCLabel lblTaxAcquired;
        public fecherFoundation.FCLabel lblGrpInfo_Pmt;
        public fecherFoundation.FCPictureBox imgNote;
        public fecherFoundation.FCHeader lblAccount;
        private Wisej.Web.TableLayoutPanel AccountInfoPanel;
        private Wisej.Web.ToolTip toolTip1;
        private fecherFoundation.FCMenuStrip MainMenu1;
        public fecherFoundation.FCToolStripMenuItem mnuProcess;
        //public fecherFoundation.FCToolStripMenuItem mnuProcessGoTo;
        public fecherFoundation.FCToolStripMenuItem mnuFileInterestedParty;
        public fecherFoundation.FCToolStripMenuItem mnuFilePreviousAccount;
        public fecherFoundation.FCToolStripMenuItem mnuFileNextAccount;
        public fecherFoundation.FCToolStripMenuItem mnuFileAccountInfo;
        public fecherFoundation.FCToolStripMenuItem mnuFileTaxInfoSheet;
        public fecherFoundation.FCToolStripMenuItem mnuFilePrintRateInfo;
        public fecherFoundation.FCToolStripMenuItem mnuFileComment;
        public fecherFoundation.FCToolStripMenuItem mnuFileREComment;
        public fecherFoundation.FCToolStripMenuItem mnuFileEditNote;
        public fecherFoundation.FCToolStripMenuItem mnuFilePriority;
        public fecherFoundation.FCToolStripMenuItem mnuFileReprintReceipt;
        public fecherFoundation.FCToolStripMenuItem mnuPayment;
        public fecherFoundation.FCToolStripMenuItem mnuFileOptionLoadValidAccount;
        public fecherFoundation.FCToolStripMenuItem mnuPaymentClearPayment;
        public fecherFoundation.FCToolStripMenuItem mnuPaymentClearList;
        public fecherFoundation.FCToolStripMenuItem mnuFileOptionsBlankBill;
        public fecherFoundation.FCToolStripMenuItem mnuFileDischarge;
        public fecherFoundation.FCToolStripMenuItem mnuFilePrintAltLDN;
        public fecherFoundation.FCToolStripMenuItem mnuFilePrintBPReport;
        public fecherFoundation.FCToolStripMenuItem mnuFilePrintGroupInfo;
        public fecherFoundation.FCToolStripMenuItem mnuRedistributeCredits;
        public fecherFoundation.FCButton cmdProcessEffective;
        public fecherFoundation.FCGrid PaymentsGrid;
        public fecherFoundation.FCPanel fraPayment;
        public fecherFoundation.FCLabel fcLabel2;
        public fecherFoundation.FCTextBox txtTotalPendingDue;
        public fecherFoundation.FCTextBox txtCash;
        public fecherFoundation.FCGrid PendingGrid;
        public fecherFoundation.FCTextBox txtTransactionDate2;
        public fecherFoundation.FCTextBox txtComments;
        public fecherFoundation.FCTextBox txtBLID;
        public fecherFoundation.FCGrid PeriodGrid;        
        public fecherFoundation.FCComboBox cmbPeriod;
        public fecherFoundation.FCTextBox txtReference;
        public fecherFoundation.FCTextBox txtCD;
        public fecherFoundation.FCLabel lblTaxClub;
        public fecherFoundation.FCLabel Label1_9;
        public fecherFoundation.FCLabel lblTotalPendingDue;
        public fecherFoundation.FCLabel Label1_2;
        public fecherFoundation.FCLabel Label1_10;
        public fecherFoundation.FCLabel Label1_8;
        public fecherFoundation.FCLabel Label8;
        public fecherFoundation.FCLabel Label1_12;
        private Wisej.Web.FlowLayoutPanel flowLayoutPanel1;
        private Wisej.Web.Panel panel2;
        public fecherFoundation.FCLabel Label1_3;
        private Wisej.Web.Panel panel3;
        public Global.T2KDateBox txtTransactionDate;
        //public Wisej.Web.DateTimePicker txtTransactionDate;
        
        public fecherFoundation.FCLabel Label1_1;
        private Wisej.Web.Panel panel4;
        private Wisej.Web.Panel panel5;
        private Wisej.Web.Panel panel6;
        private Wisej.Web.Panel panel7;
        private Wisej.Web.Panel panel8;
        private Wisej.Web.Panel panel9;
        public fecherFoundation.FCLabel Label1_11;
        private Wisej.Web.Panel PrincipalPanel;
        public fecherFoundation.FCTextBox txtPrincipal;
        public fecherFoundation.FCLabel lblPrincipal;
        private Wisej.Web.Panel InterestPanel;
        public fecherFoundation.FCTextBox txtInterest;
        public fecherFoundation.FCLabel lblInterest;
        private Wisej.Web.Panel CostsPanel;
        public fecherFoundation.FCTextBox txtCosts;
        public fecherFoundation.FCLabel lblCosts;
        private Wisej.Web.Panel AmountPanel;
        public fecherFoundation.FCTextBox txtAmount;
        public fecherFoundation.FCLabel lblAmount;
        private fecherFoundation.FCButton btnAddPayment;
        public fecherFoundation.FCTextBox txtAccountTotalsAsOf;
        public Wisej.Web.ComboBox cmbCode;
        public Wisej.Web.ComboBox cmbYear;
        private fecherFoundation.FCButton btnSaveExit;
        private TableLayoutPanel GridPanel;
        private Panel panel10;
        public fecherFoundation.FCButton cmdPrintReceipt;
        private JavaScript javaScript1;
        public fecherFoundation.FCButton btnChangeAccount;
        public fecherFoundation.FCButton btnPrintAccountDetail;
        public fecherFoundation.FCButton btnMortgageHolderInfo;
        public fecherFoundation.FCGrid txtAcctNumber;
        public fecherFoundation.FCLabel txtTotalPrincipal;
        public fecherFoundation.FCLabel txtTotalInterest;
        public fecherFoundation.FCLabel txtTotalTotal;
        public fecherFoundation.FCLabel txtTotalCosts;
    }
}