﻿namespace TWCL0000.AccountPayment
{
    partial class PPAccountIdentifyingInfoView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.lblName = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.lblLocation = new fecherFoundation.FCLabel();
            this.SuspendLayout();
            // 
            // fcLabel1
            // 
            this.fcLabel1.AutoSize = true;
            this.fcLabel1.Location = new System.Drawing.Point(12, 15);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(47, 17);
            this.fcLabel1.TabIndex = 60;
            this.fcLabel1.Text = "NAME";
            this.fcLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblName
            // 
            this.lblName.BackColor = System.Drawing.Color.Transparent;
            this.lblName.Font = new System.Drawing.Font("default", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblName.ForeColor = System.Drawing.Color.FromArgb(53, 64, 82);
            this.lblName.Location = new System.Drawing.Point(90, 10);
            this.lblName.MinimumSize = new System.Drawing.Size(200, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(538, 28);
            this.lblName.TabIndex = 59;
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(12, 54);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(78, 17);
            this.Label3.TabIndex = 61;
            this.Label3.Text = "LOCATION";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLocation
            // 
            this.lblLocation.BackColor = System.Drawing.Color.Transparent;
            this.lblLocation.Font = new System.Drawing.Font("default", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblLocation.ForeColor = System.Drawing.Color.FromArgb(53, 64, 82);
            this.lblLocation.Location = new System.Drawing.Point(90, 49);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(538, 28);
            this.lblLocation.TabIndex = 62;
            this.lblLocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PPAccountIdentifyingInfoView
            // 
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.lblLocation);
            this.Controls.Add(this.fcLabel1);
            this.Controls.Add(this.lblName);
            this.Name = "PPAccountIdentifyingInfoView";
            this.Size = new System.Drawing.Size(726, 120);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public fecherFoundation.FCLabel fcLabel1;
        public fecherFoundation.FCLabel lblName;
        public fecherFoundation.FCLabel Label3;
        public fecherFoundation.FCLabel lblLocation;
    }
}
