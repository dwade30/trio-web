﻿namespace TWCL0000.AccountPayment
{
    partial class REAccountIdentifyingInfoView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.lblName = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.lblAcreage = new fecherFoundation.FCLabel();
            this.lblLocation = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.lblMapLot = new fecherFoundation.FCLabel();
            this.lblReference = new fecherFoundation.FCLabel();
            this.fcLabel4 = new fecherFoundation.FCLabel();
            this.SuspendLayout();
            // 
            // fcLabel1
            // 
            this.fcLabel1.Location = new System.Drawing.Point(13, 15);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(57, 19);
            this.fcLabel1.TabIndex = 58;
            this.fcLabel1.Text = "NAME";
            this.fcLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(13, 91);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(84, 19);
            this.Label6.TabIndex = 56;
            this.Label6.Text = "ACREAGE";
            this.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblName
            // 
            this.lblName.BackColor = System.Drawing.Color.Transparent;
            this.lblName.Location = new System.Drawing.Point(91, 15);
            this.lblName.MinimumSize = new System.Drawing.Size(200, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(538, 19);
            this.lblName.TabIndex = 53;
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(13, 54);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(88, 19);
            this.Label3.TabIndex = 54;
            this.Label3.Text = "LOCATION";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAcreage
            // 
            this.lblAcreage.BackColor = System.Drawing.Color.Transparent;
            this.lblAcreage.Location = new System.Drawing.Point(91, 91);
            this.lblAcreage.Name = "lblAcreage";
            this.lblAcreage.Size = new System.Drawing.Size(69, 19);
            this.lblAcreage.TabIndex = 57;
            this.lblAcreage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLocation
            // 
            this.lblLocation.BackColor = System.Drawing.Color.Transparent;
            this.lblLocation.Location = new System.Drawing.Point(91, 54);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(538, 19);
            this.lblLocation.TabIndex = 55;
            this.lblLocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(172, 91);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(76, 19);
            this.Label4.TabIndex = 59;
            this.Label4.Text = "MAP LOT";
            this.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMapLot
            // 
            this.lblMapLot.BackColor = System.Drawing.Color.Transparent;
            this.lblMapLot.Location = new System.Drawing.Point(246, 91);
            this.lblMapLot.Name = "lblMapLot";
            this.lblMapLot.Size = new System.Drawing.Size(127, 19);
            this.lblMapLot.TabIndex = 60;
            this.lblMapLot.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblReference
            // 
            this.lblReference.BackColor = System.Drawing.Color.Transparent;
            this.lblReference.Location = new System.Drawing.Point(466, 91);
            this.lblReference.Name = "lblReference";
            this.lblReference.Size = new System.Drawing.Size(299, 19);
            this.lblReference.TabIndex = 62;
            this.lblReference.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fcLabel4
            // 
            this.fcLabel4.Location = new System.Drawing.Point(379, 91);
            this.fcLabel4.Name = "fcLabel4";
            this.fcLabel4.Size = new System.Drawing.Size(105, 19);
            this.fcLabel4.TabIndex = 61;
            this.fcLabel4.Text = "REFERENCE";
            this.fcLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // REAccountIdentifyingInfoView
            // 
            this.Controls.Add(this.lblReference);
            this.Controls.Add(this.fcLabel4);
            this.Controls.Add(this.lblMapLot);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.fcLabel1);
            this.Controls.Add(this.Label6);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.lblAcreage);
            this.Controls.Add(this.lblLocation);
            this.Name = "REAccountIdentifyingInfoView";
            this.Size = new System.Drawing.Size(777, 120);
            this.ResumeLayout(false);

        }

        #endregion

        public fecherFoundation.FCLabel fcLabel1;
        public fecherFoundation.FCLabel Label6;
        public fecherFoundation.FCLabel lblName;
        public fecherFoundation.FCLabel Label3;
        public fecherFoundation.FCLabel lblAcreage;
        public fecherFoundation.FCLabel lblLocation;
        public fecherFoundation.FCLabel Label4;
        public fecherFoundation.FCLabel lblMapLot;
        public fecherFoundation.FCLabel lblReference;
        public fecherFoundation.FCLabel fcLabel4;
    }
}
