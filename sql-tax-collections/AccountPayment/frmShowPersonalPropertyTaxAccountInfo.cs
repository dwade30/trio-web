﻿using System;
using fecherFoundation;
using Global;
using SharedApplication;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections.AccountPayment;
using Wisej.Web;

namespace TWCL0000.AccountPayment
{
    public partial class frmShowPersonalPropertyTaxAccountInfo : BaseForm, IView<IShowPersonalPropertyTaxAccountInfoViewModel>
    {
        public frmShowPersonalPropertyTaxAccountInfo()
        {
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            this.Load += FrmShowPersonalPropertyTaxAccountInfo_Load;
        }

        private void FrmShowPersonalPropertyTaxAccountInfo_Load(object sender, EventArgs e)
        {
            ShowInfo();
        }

        public frmShowPersonalPropertyTaxAccountInfo(IShowPersonalPropertyTaxAccountInfoViewModel viewModel):this()
        {
            ViewModel = viewModel;
        }

        public new void Show()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            if (!this.Visible)
            {
                this.BringToFront();
                base.Show(FormShowEnum.Modeless, "", null);
            }
        }

        public IShowPersonalPropertyTaxAccountInfoViewModel ViewModel { get; set; }

        private void ShowInfo()
        {
            var accountInfo = ViewModel.AccountInfo;
            AddControl("Current Owner", accountInfo.Name);
            if (!string.IsNullOrWhiteSpace(accountInfo.Location))
            {
                AddControl("Location", accountInfo.Location);
            }
            var mailingAddress = accountInfo.MailingAddress;
            var description = "Mailing Address";
            if (!string.IsNullOrWhiteSpace(mailingAddress.Address1))
            {
                AddControl(description, mailingAddress.Address1);
                description = "";
            }

            if (!string.IsNullOrWhiteSpace(mailingAddress.Address2))
            {
                AddControl(description, mailingAddress.Address2);
                description = "";
            }

            if (!string.IsNullOrWhiteSpace(mailingAddress.Address3))
            {
                AddControl(description, mailingAddress.Address3);
                description = "";
            }

            var csz = GetCityStateZip(mailingAddress.City, mailingAddress.State, mailingAddress.Zip);
            if (!string.IsNullOrWhiteSpace(csz))
            {
                AddControl(description, csz);
            }

            if (!string.IsNullOrWhiteSpace(accountInfo.Open1))
            {
                AddControl(ViewModel.Open1Description,accountInfo.Open1);
            }

            if (!string.IsNullOrWhiteSpace(accountInfo.Open2))
            {
                AddControl(ViewModel.Open2Description, accountInfo.Open2);
            }
            AddControl("","");
            if (accountInfo.AssociatedREAccount > 0)
            {
                AddControl("Associated RE Account", accountInfo.AssociatedREAccount.ToString());
                AddControl("Outstanding RE Amount", accountInfo.OutstandingREAmount.FormatAsCurrencyNoSymbol());
            }


        }

        private string GetCityStateZip(string city, string state, string zip)
        {
            return city + ", " + state + " " + zip;
        }
        private void AddControl(string description, string value)
        {
            LayoutPanel.Controls.Add(GetControl(description, value));
        }
        private TableLayoutPanel GetControl(string description, string value)
        {
            var panel = new TableLayoutPanel();
            panel.Margin = new Padding(1);
            panel.ColumnCount = 2;
            panel.RowCount = 1;
            panel.AutoSize = true;
            panel.Dock = DockStyle.Left & DockStyle.Right;
            panel.Padding = new Padding(1);

            panel.Controls.Add(new FCLabel() { Text = description, AutoSize = false, Dock = DockStyle.Fill, Width = 165, Margin = Padding.Empty });
            panel.Controls.Add(new Label() { Text = value, AutoSize = true, Dock = DockStyle.Fill, Margin = Padding.Empty });

            return panel;
        }

    }
}
