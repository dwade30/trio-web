﻿using SharedApplication;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Lien;

namespace TWCL0000.AccountPayment
{
    public class ShowLienDischargeAlternateReportHandler : CommandHandler<ShowLienDischargeAlternateReport>
    {
        private IView<ITaxLienDischargeAlternateNoticeReportModel> reportView;
        public ShowLienDischargeAlternateReportHandler(IView<ITaxLienDischargeAlternateNoticeReportModel> reportView)
        {
            this.reportView = reportView;
        }
        protected override void Handle(ShowLienDischargeAlternateReport command)
        {
            reportView.ViewModel.TaxAccount = command.TaxAccount;
            reportView.ViewModel.PayDate = command.PayDate;
            reportView.ViewModel.DischargeNotice = command.DischargeNotice;
            reportView.Show();
        }
    }
}