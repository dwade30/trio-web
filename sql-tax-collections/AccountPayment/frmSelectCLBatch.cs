﻿using System;
using System.Linq;
using Global;
using SharedApplication;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections.AccountPayment;
using Wisej.Web;

namespace TWCL0000.AccountPayment
{
    public partial class frmSelectCLBatch : BaseForm, IModalView<ISelectCLBatchViewModel>
    {
        public frmSelectCLBatch()
        {
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            this.Load += FrmSelectCLBatch_Load;
            this.btnProcess.Click += BtnProcess_Click;
        }


        private void BtnProcess_Click(object sender, EventArgs e)
        {
            SelectBatch();
        }

        private void FrmSelectCLBatch_Load(object sender, EventArgs e)
        {
            SetupCombo();
        }

        public frmSelectCLBatch(ISelectCLBatchViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }
        public ISelectCLBatchViewModel ViewModel { get; set; }
        public void ShowModal()
        {
            this.Show(FormShowEnum.Modal);
        }

        private void SetupCombo()
        {
            var choices = ViewModel.BatchChoices.OrderByDescending(b => b.BatchDate).ToList();
            foreach (var choice in choices)
            {
                var item = new Wisej.Web.ListViewItem();
                item.Text = choice.BatchDate.FormatAndPadShortDate();
                var subItem = new Wisej.Web.ListViewItem.ListViewSubItem();
                subItem.Text = choice.TellerId;
                item.SubItems.Add(subItem);
                subItem = new Wisej.Web.ListViewItem.ListViewSubItem();
                subItem.Text = choice.PaidBy;
                item.SubItems.Add(subItem);
                item.Tag = choice;
                cmbBatchChoice.Items.Add(item);
            }
           
        }

        private void SelectBatch()
        {
            if (cmbBatchChoice.SelectedItem != null && cmbBatchChoice.SelectedItem.Selected)
            {
                var choice = (CLBatchChoice)(cmbBatchChoice.SelectedItem.Tag);
                ViewModel.SelectedBatch = choice;
                ViewModel.Cancelled = false;
                Close();
            }
            else
            {
                MessageBox.Show("Please select a batch", "No Selection", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
