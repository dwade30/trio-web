﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using SharedApplication;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections.AccountPayment;
using TWSharedLibrary;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptBatchListing : BaseSectionReport, IView<IPropertyTaxBatchListingViewModel>
    {

		int lngRow;
		string strLastYearText = "";
		bool boolDone;
		double dblSumPrin;
		double dblSumInt;
		double dblSumCost;
		double dblSumTotal;
        private DataEnvironmentSettings envSettings;
        private IEnumerator<PropertyTaxBatchItemGroup> iterator;
		public rptBatchListing()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        public rptBatchListing(DataEnvironmentSettings envSettings) : this()
        {
            this.envSettings = envSettings;
        }
		private void InitializeComponentEx()
		{
			this.Name = "Account Detail";
		}




		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
        {
            eArgs.EOF = !iterator.MoveNext();
        }

		private void ActiveReport_ReportStart(object sender, EventArgs e)
        {
            iterator = ViewModel.TaxBatch.BatchItems.GetEnumerator();
            fldYear.Text = ViewModel.TaxBatch.DefaultTaxYear.ToString();
            fldDate.Text = ViewModel.TaxBatch.BatchRecoverDate.FormatAndPadShortDate();
            fldIntDate.Text = ViewModel.TaxBatch.EffectiveDate.FormatAndPadShortDate();
            lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblMuniName.Text = envSettings.ClientName;
			lngRow = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
            if (iterator.Current != null)
            {
                BindFields(iterator.Current);
            }
        }

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void BindFields(PropertyTaxBatchItemGroup itemGroup)
		{
			double dblTemp = 0;
			// this will print the row from the grid
			if (itemGroup != null)
			{
				// Account Number
				fldAcct.Text = itemGroup.BatchRecover.AccountNumber.GetValueOrDefault().ToString();
				// Name
				fldName.Text = itemGroup.BatchRecover.Name;
				// Principal
				dblTemp = itemGroup.BatchRecover.Prin.GetValueOrDefault();
				fldPrincipal.Text = Strings.Format(dblTemp, "#,##0.00");
				dblSumPrin += dblTemp;
				// Interest
				dblTemp = itemGroup.BatchRecover.Int.GetValueOrDefault();
				fldInterest.Text = Strings.Format(dblTemp, "#,##0.00");
				dblSumInt += dblTemp;
				// Costs
				dblTemp = itemGroup.BatchRecover.Cost.GetValueOrDefault();
				fldCosts.Text = Strings.Format(dblTemp, "#,##0.00");
				dblSumCost += dblTemp;
				// Total
				dblTemp = itemGroup.BatchRecover.Prin.GetValueOrDefault() + itemGroup.BatchRecover.Int.GetValueOrDefault() + itemGroup.BatchRecover.Cost.GetValueOrDefault();
				fldTotal.Text = Strings.Format(dblTemp, "#,##0.00");
				dblSumTotal += dblTemp;
			}

		}




		private void ReportFooter_Format(object sender, EventArgs e)
        {
            var count = ViewModel.TaxBatch.BatchItems.Count;
			if (count <= 1)
			{
				lblTotalTitle.Text = "Total for " + count + " account:";
			}
			else
			{
				lblTotalTitle.Text = "Total for " + count + " accounts:";
			}
			fldTotalPrin.Text = Strings.Format(dblSumPrin, "#,##0.00");
			fldTotalInt.Text = Strings.Format(dblSumInt, "#,##0.00");
			fldTotalCost.Text = Strings.Format(dblSumCost, "#,##0.00");
			fldTotalTotal.Text = Strings.Format(dblSumTotal, "#,##0.00");
		}


        public void Show()
        {
            frmReportViewer.InstancePtr.Init(this);
        }

        public IPropertyTaxBatchListingViewModel ViewModel { get; set; }
    }
}
