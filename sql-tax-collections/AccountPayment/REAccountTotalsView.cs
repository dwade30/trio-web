﻿using System;
using SharedApplication.Extensions;
using SharedApplication.RealEstate;
using Wisej.Web;

namespace TWCL0000.AccountPayment
{
    public partial class REAccountTotalsView : Wisej.Web.UserControl
    {
        private RealEstateAccountTotals accountTotals;
        private RealEstateAccountTotals billedTotals;
        private int billedYear;
        public REAccountTotalsView()
        {
            InitializeComponent();
        }

        public REAccountTotalsView(RealEstateAccountTotals accountTotals,RealEstateAccountTotals billedTotals,int billedYear) : this()
        {
            this.accountTotals = accountTotals;
            this.billedTotals = billedTotals;
            this.billedYear = billedYear;
            ShowInfo();
        }

        private void ShowInfo()
        {
            if (accountTotals != null)
            {
                lblBuildingValue.Text = accountTotals.Building.FormatAsNumber();
                lblLandValue.Text = accountTotals.Land.FormatAsNumber();
                lblExemptValue.Text = accountTotals.Exemptions.FormatAsNumber();
                lblTotalValue.Text = accountTotals.NetTaxable().FormatAsNumber();
            }
            else
            {
                lblBuildingValue.Text = "";
                lblLandValue.Text = "";
                lblExemptValue.Text = "";
                lblTotalValue.Text = "";
            }

            if (billedTotals != null && billedYear > 0)
            {
                lblBuildingValue2.Text = billedTotals.Building.FormatAsNumber();
                lblLandValue2.Text = billedTotals.Land.FormatAsNumber();
                lblExemptValue2.Text = billedTotals.Exemptions.FormatAsNumber();
                lblTotalValue2.Text = billedTotals.NetTaxable().FormatAsNumber();
                lblYear.Text = billedYear.ToString();
            }
            else
            {
                lblBuildingValue2.Text = "";
                lblLandValue2.Text = "";
                lblExemptValue2.Text = "";
                lblTotalValue2.Text = "";
                lblYear.Text = "";
            }

        }
    }
}
