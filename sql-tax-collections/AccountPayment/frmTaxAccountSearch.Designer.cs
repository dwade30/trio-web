﻿using Wisej.Web;

namespace TWCL0000.AccountPayment
{
    partial class frmTaxAccountSearch 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
            this.txtSearch = new fecherFoundation.FCTextBox();
            this.chkShowPreviousOwnerInfo = new fecherFoundation.FCCheckBox();
            this.chkShowDeletedAccountInfo = new fecherFoundation.FCCheckBox();
            this.lblSearchType = new fecherFoundation.FCLabel();
            this.txtGetAccountNumber = new fecherFoundation.FCTextBox();
            this.cmbSearchType = new Wisej.Web.ComboBox();
            this.cmbMatchType = new Wisej.Web.ComboBox();
            this.lblInstructions1 = new fecherFoundation.FCLabel();
            this.cmdProcessClearSearch = new fecherFoundation.FCButton();
            this.cmdSearch = new fecherFoundation.FCButton();
            this.btnProcess = new fecherFoundation.FCButton();
            this.SearchGrid = new fecherFoundation.FCGrid();
            this.fcMenuStrip1 = new fecherFoundation.FCMenuStrip();
            this.mnuStartBatch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPurgeBatch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRecoverBatch = new fecherFoundation.FCToolStripMenuItem();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowPreviousOwnerInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowDeletedAccountInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessClearSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SearchGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 534);
            this.BottomPanel.Size = new System.Drawing.Size(1042, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.SearchGrid);
            this.ClientArea.Controls.Add(this.txtSearch);
            this.ClientArea.Controls.Add(this.chkShowPreviousOwnerInfo);
            this.ClientArea.Controls.Add(this.chkShowDeletedAccountInfo);
            this.ClientArea.Controls.Add(this.lblSearchType);
            this.ClientArea.Controls.Add(this.txtGetAccountNumber);
            this.ClientArea.Controls.Add(this.cmbSearchType);
            this.ClientArea.Controls.Add(this.cmbMatchType);
            this.ClientArea.Controls.Add(this.lblInstructions1);
            this.ClientArea.Size = new System.Drawing.Size(1062, 643);
            this.ClientArea.Controls.SetChildIndex(this.lblInstructions1, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbMatchType, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbSearchType, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtGetAccountNumber, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblSearchType, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkShowDeletedAccountInfo, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkShowPreviousOwnerInfo, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtSearch, 0);
            this.ClientArea.Controls.SetChildIndex(this.SearchGrid, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdProcessClearSearch);
            this.TopPanel.Controls.Add(this.cmdSearch);
            this.TopPanel.Size = new System.Drawing.Size(1062, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdSearch, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdProcessClearSearch, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(164, 28);
            this.HeaderText.Text = "Select Account";
            // 
            // txtSearch
            // 
            this.txtSearch.BackColor = System.Drawing.SystemColors.Window;
            this.txtSearch.Location = new System.Drawing.Point(455, 81);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(181, 40);
            this.txtSearch.TabIndex = 81;
            this.txtSearch.KeyDown += new Wisej.Web.KeyEventHandler(this.txtSearch_KeyDown);
            // 
            // chkShowPreviousOwnerInfo
            // 
            this.chkShowPreviousOwnerInfo.Location = new System.Drawing.Point(866, 87);
            this.chkShowPreviousOwnerInfo.Name = "chkShowPreviousOwnerInfo";
            this.chkShowPreviousOwnerInfo.Size = new System.Drawing.Size(167, 22);
            this.chkShowPreviousOwnerInfo.TabIndex = 82;
            this.chkShowPreviousOwnerInfo.Text = "Show Previous Owners";
            // 
            // chkShowDeletedAccountInfo
            // 
            this.chkShowDeletedAccountInfo.Location = new System.Drawing.Point(656, 87);
            this.chkShowDeletedAccountInfo.Name = "chkShowDeletedAccountInfo";
            this.chkShowDeletedAccountInfo.Size = new System.Drawing.Size(170, 22);
            this.chkShowDeletedAccountInfo.TabIndex = 83;
            this.chkShowDeletedAccountInfo.Text = "Show Deleted Accounts";
            // 
            // lblSearchType
            // 
            this.lblSearchType.AutoSize = true;
            this.lblSearchType.Location = new System.Drawing.Point(22, 95);
            this.lblSearchType.Name = "lblSearchType";
            this.lblSearchType.Size = new System.Drawing.Size(79, 15);
            this.lblSearchType.TabIndex = 86;
            this.lblSearchType.Text = "SEARCH BY";
            // 
            // txtGetAccountNumber
            // 
            this.txtGetAccountNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtGetAccountNumber.Location = new System.Drawing.Point(134, 21);
            this.txtGetAccountNumber.Name = "txtGetAccountNumber";
            this.txtGetAccountNumber.Size = new System.Drawing.Size(136, 40);
            this.txtGetAccountNumber.TabIndex = 79;
            // 
            // cmbSearchType
            // 
            this.cmbSearchType.AutoSize = false;
            this.cmbSearchType.DisplayMember = "Description";
            this.cmbSearchType.Location = new System.Drawing.Point(134, 81);
            this.cmbSearchType.Name = "cmbSearchType";
            this.cmbSearchType.Size = new System.Drawing.Size(136, 40);
            this.cmbSearchType.TabIndex = 85;
            this.cmbSearchType.Text = "Name";
            this.cmbSearchType.ValueMember = "ID";
            // 
            // cmbMatchType
            // 
            this.cmbMatchType.AutoSize = false;
            this.cmbMatchType.DisplayMember = "Description";
            this.cmbMatchType.Location = new System.Drawing.Point(290, 81);
            this.cmbMatchType.Name = "cmbMatchType";
            this.cmbMatchType.Size = new System.Drawing.Size(145, 40);
            this.cmbMatchType.TabIndex = 84;
            this.cmbMatchType.Text = "Starts With";
            this.cmbMatchType.ValueMember = "ID";
            // 
            // lblInstructions1
            // 
            this.lblInstructions1.AutoSize = true;
            this.lblInstructions1.Location = new System.Drawing.Point(22, 35);
            this.lblInstructions1.Name = "lblInstructions1";
            this.lblInstructions1.Size = new System.Drawing.Size(68, 15);
            this.lblInstructions1.TabIndex = 80;
            this.lblInstructions1.Text = "ACCOUNT";
            // 
            // cmdProcessClearSearch
            // 
            this.cmdProcessClearSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdProcessClearSearch.Location = new System.Drawing.Point(848, 31);
            this.cmdProcessClearSearch.Name = "cmdProcessClearSearch";
            this.cmdProcessClearSearch.Size = new System.Drawing.Size(95, 24);
            this.cmdProcessClearSearch.TabIndex = 54;
            this.cmdProcessClearSearch.Text = "Clear Search";
            // 
            // cmdSearch
            // 
            this.cmdSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSearch.ImageSource = "button-search";
            this.cmdSearch.Location = new System.Drawing.Point(949, 31);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Size = new System.Drawing.Size(81, 24);
            this.cmdSearch.TabIndex = 53;
            this.cmdSearch.Text = "Search";
            this.cmdSearch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Location = new System.Drawing.Point(476, 30);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcess.Size = new System.Drawing.Size(120, 48);
            this.btnProcess.TabIndex = 1;
            this.btnProcess.Text = "Process";
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // SearchGrid
            // 
            this.SearchGrid.Cols = 5;
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SearchGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.SearchGrid.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.SearchGrid.FixedCols = 0;
            this.SearchGrid.ForeColorFixed = System.Drawing.SystemColors.ControlText;
            this.SearchGrid.Location = new System.Drawing.Point(22, 168);
            this.SearchGrid.MinimumSize = new System.Drawing.Size(500, 200);
            this.SearchGrid.Name = "SearchGrid";
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SearchGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.SearchGrid.RowHeadersVisible = false;
            this.SearchGrid.Rows = 1;
            this.SearchGrid.ShowFocusCell = false;
            this.SearchGrid.Size = new System.Drawing.Size(1019, 366);
            this.SearchGrid.TabIndex = 87;
            // 
            // fcMenuStrip1
            // 
            this.fcMenuStrip1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuStartBatch,
            this.mnuPurgeBatch,
            this.mnuRecoverBatch});
            this.fcMenuStrip1.Name = null;
            // 
            // mnuStartBatch
            // 
            this.mnuStartBatch.Enabled = false;
            this.mnuStartBatch.Index = 0;
            this.mnuStartBatch.Name = "mnuStartBatch";
            this.mnuStartBatch.Text = "Start Batch Update";
            this.mnuStartBatch.Visible = false;
            // 
            // mnuPurgeBatch
            // 
            this.mnuPurgeBatch.Enabled = false;
            this.mnuPurgeBatch.Index = 1;
            this.mnuPurgeBatch.Name = "mnuPurgeBatch";
            this.mnuPurgeBatch.Text = "Purge Batch";
            this.mnuPurgeBatch.Visible = false;
            // 
            // mnuRecoverBatch
            // 
            this.mnuRecoverBatch.Enabled = false;
            this.mnuRecoverBatch.Index = 2;
            this.mnuRecoverBatch.Name = "mnuRecoverBatch";
            this.mnuRecoverBatch.Text = "Recover Batch";
            this.mnuRecoverBatch.Visible = false;
            // 
            // frmTaxAccountSearch
            // 
            this.ClientSize = new System.Drawing.Size(1062, 703);
            this.Menu = this.fcMenuStrip1;
            this.Name = "frmTaxAccountSearch";
            this.Text = "Select Account";
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowPreviousOwnerInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowDeletedAccountInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessClearSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SearchGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public fecherFoundation.FCTextBox txtSearch;
        public fecherFoundation.FCCheckBox chkShowPreviousOwnerInfo;
        public fecherFoundation.FCCheckBox chkShowDeletedAccountInfo;
        private fecherFoundation.FCLabel lblSearchType;
        public fecherFoundation.FCTextBox txtGetAccountNumber;
        public Wisej.Web.ComboBox cmbSearchType;
        public Wisej.Web.ComboBox cmbMatchType;
        public fecherFoundation.FCLabel lblInstructions1;
        public fecherFoundation.FCButton cmdProcessClearSearch;
        public fecherFoundation.FCButton cmdSearch;
        public fecherFoundation.FCGrid SearchGrid;
        private fecherFoundation.FCButton btnProcess;
        private fecherFoundation.FCMenuStrip fcMenuStrip1;
        private fecherFoundation.FCToolStripMenuItem mnuPurgeBatch;
        private fecherFoundation.FCToolStripMenuItem mnuRecoverBatch;
        private fecherFoundation.FCToolStripMenuItem mnuStartBatch;
    }
}