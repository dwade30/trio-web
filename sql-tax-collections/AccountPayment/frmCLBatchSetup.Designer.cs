﻿using System.Windows.Forms;
using Wisej.Web;

namespace TWCL0000.AccountPayment
{
    partial class frmCLBatchSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPaymentDate = new Wisej.Web.DateTimePicker();
            this.chkReceipt = new fecherFoundation.FCCheckBox();
            this.txtTellerID = new fecherFoundation.FCTextBox();
            this.cmbPeriod = new fecherFoundation.FCComboBox();
            this.txtTaxYear = new fecherFoundation.FCTextBox();
            this.txtPaidBy = new fecherFoundation.FCTextBox();
            this.txtEffectiveDate = new Wisej.Web.DateTimePicker();
            this.lblEffectiveDate = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.lblPaymentDate = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.btnSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkReceipt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 368);
            this.BottomPanel.Size = new System.Drawing.Size(690, 103);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtPaidBy);
            this.ClientArea.Controls.Add(this.txtPaymentDate);
            this.ClientArea.Controls.Add(this.chkReceipt);
            this.ClientArea.Controls.Add(this.txtTellerID);
            this.ClientArea.Controls.Add(this.cmbPeriod);
            this.ClientArea.Controls.Add(this.txtTaxYear);
            this.ClientArea.Controls.Add(this.txtEffectiveDate);
            this.ClientArea.Controls.Add(this.lblEffectiveDate);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.Label4);
            this.ClientArea.Controls.Add(this.Label6);
            this.ClientArea.Controls.Add(this.Label8);
            this.ClientArea.Controls.Add(this.lblPaymentDate);
            this.ClientArea.Location = new System.Drawing.Point(0, 0);
            this.ClientArea.Size = new System.Drawing.Size(690, 368);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(690, 0);
            // 
            // txtPaymentDate
            // 
            this.txtPaymentDate.AutoSize = false;
            this.txtPaymentDate.Checked = false;
            this.txtPaymentDate.Format = Wisej.Web.DateTimePickerFormat.Short;
            this.txtPaymentDate.Location = new System.Drawing.Point(232, 140);
            this.txtPaymentDate.Mask = "00/00/0000";
            this.txtPaymentDate.Name = "txtPaymentDate";
            this.txtPaymentDate.Size = new System.Drawing.Size(159, 40);
            this.txtPaymentDate.TabIndex = 6;
            this.txtPaymentDate.Value = new System.DateTime(2019, 12, 9, 11, 21, 14, 794);
            // 
            // chkReceipt
            // 
            this.chkReceipt.Location = new System.Drawing.Point(21, 3);
            this.chkReceipt.Name = "chkReceipt";
            this.chkReceipt.Size = new System.Drawing.Size(220, 24);
            this.chkReceipt.TabIndex = 11;
            this.chkReceipt.Text = "Default Receipt Option to \'Yes\'";
            this.chkReceipt.Visible = false;
            // 
            // txtTellerID
            // 
            this.txtTellerID.BackColor = System.Drawing.SystemColors.Window;
            this.txtTellerID.Location = new System.Drawing.Point(232, 37);
            this.txtTellerID.MaxLength = 3;
            this.txtTellerID.Name = "txtTellerID";
            this.txtTellerID.Size = new System.Drawing.Size(159, 40);
            this.txtTellerID.TabIndex = 4;
            // 
            // cmbPeriod
            // 
            this.cmbPeriod.BackColor = System.Drawing.SystemColors.Window;
            this.cmbPeriod.Location = new System.Drawing.Point(232, 297);
            this.cmbPeriod.Name = "cmbPeriod";
            this.cmbPeriod.Size = new System.Drawing.Size(159, 40);
            this.cmbPeriod.Sorted = true;
            this.cmbPeriod.TabIndex = 15;
            this.cmbPeriod.Visible = false;
            // 
            // txtTaxYear
            // 
            this.txtTaxYear.BackColor = System.Drawing.SystemColors.Window;
            this.txtTaxYear.Location = new System.Drawing.Point(232, 87);
            this.txtTaxYear.MaxLength = 4;
            this.txtTaxYear.Name = "txtTaxYear";
            this.txtTaxYear.Size = new System.Drawing.Size(159, 40);
            this.txtTaxYear.TabIndex = 5;
            // 
            // txtPaidBy
            // 
            this.txtPaidBy.BackColor = System.Drawing.SystemColors.Window;
            this.txtPaidBy.Location = new System.Drawing.Point(232, 246);
            this.txtPaidBy.MaxLength = 25;
            this.txtPaidBy.Name = "txtPaidBy";
            this.txtPaidBy.Size = new System.Drawing.Size(159, 40);
            this.txtPaidBy.TabIndex = 9;
            // 
            // txtEffectiveDate
            // 
            this.txtEffectiveDate.AutoSize = false;
            this.txtEffectiveDate.Checked = false;
            this.txtEffectiveDate.Format = Wisej.Web.DateTimePickerFormat.Short;
            this.txtEffectiveDate.Location = new System.Drawing.Point(232, 194);
            this.txtEffectiveDate.Mask = "00/00/0000";
            this.txtEffectiveDate.Name = "txtEffectiveDate";
            this.txtEffectiveDate.Size = new System.Drawing.Size(159, 40);
            this.txtEffectiveDate.TabIndex = 7;
            this.txtEffectiveDate.Value = new System.DateTime(2019, 12, 9, 11, 21, 14, 893);
            // 
            // lblEffectiveDate
            // 
            this.lblEffectiveDate.AutoSize = true;
            this.lblEffectiveDate.Location = new System.Drawing.Point(21, 208);
            this.lblEffectiveDate.Name = "lblEffectiveDate";
            this.lblEffectiveDate.Size = new System.Drawing.Size(122, 17);
            this.lblEffectiveDate.TabIndex = 74;
            this.lblEffectiveDate.Text = "EFFECTIVE DATE";
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(21, 51);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(77, 17);
            this.Label8.TabIndex = 75;
            this.Label8.Text = "TELLER ID";
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(21, 264);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(62, 17);
            this.Label6.TabIndex = 25;
            this.Label6.Text = "PAID BY";
            // 
            // lblPaymentDate
            // 
            this.lblPaymentDate.AutoSize = true;
            this.lblPaymentDate.Location = new System.Drawing.Point(21, 154);
            this.lblPaymentDate.Name = "lblPaymentDate";
            this.lblPaymentDate.Size = new System.Drawing.Size(113, 17);
            this.lblPaymentDate.TabIndex = 24;
            this.lblPaymentDate.Text = "PAYMENT DATE";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(21, 311);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(59, 17);
            this.Label4.TabIndex = 23;
            this.Label4.Text = "PERIOD";
            this.Label4.Visible = false;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(21, 101);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(142, 17);
            this.Label3.TabIndex = 22;
            this.Label3.Text = "DEFAULT TAX YEAR";
            // 
            // btnSave
            // 
            this.btnSave.AppearanceKey = "acceptButton";
            this.btnSave.Location = new System.Drawing.Point(284, 32);
            this.btnSave.Name = "btnSave";
            this.btnSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnSave.Size = new System.Drawing.Size(120, 48);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "OK";
            // 
            // frmCLBatchSetup
            // 
            this.ClientSize = new System.Drawing.Size(690, 471);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCLBatchSetup";
            this.Text = "Batch Process";
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkReceipt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        public fecherFoundation.FCCheckBox chkReceipt;
        public fecherFoundation.FCTextBox txtTellerID;
        public fecherFoundation.FCComboBox cmbPeriod;
        public fecherFoundation.FCTextBox txtTaxYear;
        public Wisej.Web.DateTimePicker txtPaymentDate;
        public fecherFoundation.FCTextBox txtPaidBy;
        public Wisej.Web.DateTimePicker txtEffectiveDate;
        public fecherFoundation.FCLabel lblEffectiveDate;
        public fecherFoundation.FCLabel Label8;
        public fecherFoundation.FCLabel Label6;
        public fecherFoundation.FCLabel lblPaymentDate;
        public fecherFoundation.FCLabel Label4;
        public fecherFoundation.FCLabel Label3;
        private fecherFoundation.FCButton btnSave;
    }
}