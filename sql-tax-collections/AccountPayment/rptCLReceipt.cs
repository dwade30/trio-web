﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Global;
using SharedApplication;
using SharedApplication.Enums;
using SharedApplication.Extensions;
using SharedApplication.Receipting;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Extensions;
using SharedApplication.TaxCollections.Models;
using SharedApplication.TaxCollections.Receipting;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptCLReceipt : BaseSectionReport, IView<ICLReceiptViewModel>
	{
		float lngTotalWidth;
		// total width of the report
		int intPaymentNumber;
		// this is the payment number in the grid
		bool boolTextExport;
		//FC:FINAL:AM: variable not used
		//FCPrinter prn = new FCPrinter();
		bool boolDone;
		// VBto upgrade warning: dblReceiptTotal As double	OnWriteFCConvert.ToDouble(
		decimal dblReceiptTotal;
		bool boolAsterisk;
		
		bool boolFirstPass;
		string strPrinterName = "";

        private IEnumerator<PropertyTaxTransaction> transactionEnumerator;
		public rptCLReceipt()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        public rptCLReceipt(ICLReceiptViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }

		private void InitializeComponentEx()
		{
			this.Name = "Receipt";
		}



		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				eArgs.EOF = !transactionEnumerator.MoveNext();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In Fetch Data");
			}
		}

		private void SetArraySizes()
		{
			try
			{
                //if (!modGlobal.Statics.gboolCLNarrowReceipt)
                if (!ViewModel.UseNarrowReceipt)
                {
					// narrow
					// Me.Printer.PaperSize = 255
					PageSettings.Margins.Left = 360 / 1440F;
					PageSettings.Margins.Right = 360 / 1440F;
					PageSettings.Margins.Top = 360 / 1440F;
					PageSettings.Margins.Bottom = 360 / 1440F;
					// Me.Printer.PaperWidth = 10000
				}
				else
				{
					this.Document.Printer.PaperSize = new System.Drawing.Printing.PaperSize("Size", 312, 1000);
                    //this.Document.Printer.PaperHeight = 1440 * 10; // 22
                    //this.Document.Printer.PaperWidth = 4500;
                    PageSettings.PaperHeight = 10;
                    PageSettings.PaperWidth = 4500 / 1440f;
					PageSettings.Margins.Top = 0;
					PageSettings.Margins.Bottom = 0;
					PageSettings.Margins.Left = 0;
					PageSettings.Margins.Right = 0;
					this.PrintWidth = 4102 / 1440F;
				}
				lngTotalWidth = this.PrintWidth;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Setting Array Size");
			}
		}

		private void SetupReceiptFormat()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// set the lefts and widths
				lblMuniName.Width = lngTotalWidth;
				lblHeader.Width = lngTotalWidth;
				lblComment.Width = lngTotalWidth;
				lblReprint.Width = lngTotalWidth;
				lblOtherAccounts.Width = lngTotalWidth;
				if (ViewModel.UseNarrowReceipt)
				{
					// narrow
					// move all the header fields to the left and move then on top of one another
					lblDate.Left = 0;
					lblTime.Left = 0;
					lblAccount.Left = 0;
					lblTime.Top = lblDate.Top + lblDate.Height;
					// + 100
					lblAccount.Top = lblTime.Top + lblTime.Height;
					// + 100
					lblHeaderAmt.Left = 0;
					// (lngTotalWidth - lblHeaderAmt.Width) - 300
					lblHeaderAmt.Width = lngTotalWidth;
					lblHeaderAmt.Top = lblAccount.Top + lblAccount.Height;
					// + 100
					lblHeaderAmt.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
					lnHeader.Y1 = lblHeaderAmt.Top + lblHeaderAmt.Height;
					lnHeader.Y2 = lnHeader.Y1;
					lnHeader.Visible = false;
				}
				else
				{
					// wide
					lblDate.Left = 0;
					lblTime.Left = lblDate.Left + lblDate.Width;
					lblAccount.Left = lblTime.Left + lblTime.Width;
					lblTime.Top = lblDate.Top;
					lblAccount.Top = lblDate.Top;
					lblHeaderAmt.Top = lblAccount.Top + lblAccount.Height;
					// set the line
					lnHeader.Y1 = lblHeaderAmt.Top + lblHeaderAmt.Height;
					lnHeader.Y2 = lnHeader.Y1;
					lnHeader.X1 = lblHeaderAmt.Left + 100 / 1440F;
					lnHeader.X2 = lblHeaderAmt.Left + lblHeaderAmt.Width - 100 / 1440F;
				}
				ReportHeader.Height = lnHeader.Y1 + 100 / 1440F;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Setting Receipt Format", "Error Setting Receipt Format");
			}
		}

		private void SetMainLabels()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will put in all of the information on the top and bottom of the receipt..ie comments, date/time, and teller
				DateTime dtReceiptDate;
				dtReceiptDate = DateTime.Today;
				// 1st line
				if (!string.IsNullOrWhiteSpace(ViewModel.CityTown))
				{
					lblMuniName.Text = ViewModel.CityTown + " of " + ViewModel.MuniName;
				}
				else
				{
					lblMuniName.Text = ViewModel.MuniName;
				}
				// 2nd line
				lblHeader.Text = "-----  R e c e i p t  -----";
				// 3nd line
				lblDate.Text = dtReceiptDate.FormatAndPadShortDate();
				lblTime.Text = dtReceiptDate.FormatAndPadTime();

                lblAccount.Text = "Account " + ViewModel.Account;

                // 4rd line
                if (ViewModel.UseNarrowReceipt)
                {
                    lblHeaderAmt.Text = "Amount".PadLeft(28);
				}
				else
				{
					lblHeaderAmt.Text = "Amount";
				}
				if (ViewModel.IsReprint)
				{
					lblReprint.Visible = true;
					lblOtherAccounts.Text = " ";
					lblOtherAccounts.Visible = false;
				}
				else
                {
                    //var summary = ViewModel.GetGroupSummary();
					lblOtherAccounts.Text = GetRemainingCLBalance();
					lblOtherAccounts.Visible = true;
					lblReprint.Visible = false;
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Setting Main Labels", "Error Setting Main Labels");
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			//Application.DoEvents();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
            {
                transactionEnumerator = ViewModel.Transactions.GetEnumerator();
				// On Error GoTo ERROR_HANDLER
				strPrinterName = StaticSettings.GlobalSettingService
					                 .GetSettingValue(SettingOwnerType.Machine, "RCPTPrinterName")?.SettingValue ?? "";

				//modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "RCPTPrinterName", ref strPrinterName);
				if (strPrinterName != "")
				{
					// check to see if the printer that is setup is a file rather than a printer
					if (SetupAsPrinterFile(strPrinterName))
					{
						// if it is then export it to a file
						boolTextExport = true;
					}
					else
					{
						boolTextExport = false;
						this.Document.Printer.PrinterName = strPrinterName;
					}
				}
				
				boolFirstPass = true;
				// this is for reprints
				// use the printers collection to set the printer settings
				SetArraySizes();
				SetupReceiptFormat();
				intPaymentNumber = 1;
				SetMainLabels();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Starting Receipt");
			}
		}

		private bool SetupAsPrinterFile(string strName)
		{
			bool SetupAsPrinterFile = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will return true if the string passed int is "PRNT.txt"
				if (Strings.UCase(strName) == Strings.UCase("PRNT.TXT"))
				{
					SetupAsPrinterFile = true;
				}
				else
				{
					SetupAsPrinterFile = false;
				}
				return SetupAsPrinterFile;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Printer File");
			}
			return SetupAsPrinterFile;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				BindFields();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In Detail Format");
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// strTemp = GetInfoAboutOtherAccounts(intTag, lngNumOfRows)
				if (ViewModel.UseNarrowReceipt)
				{
					fldTotal.Width = 0;
					fldTotal.Left = 0;
					fldTotal.Visible = false;
					fldTotal.Text = "";
					lblTotal.Top = 0;
					lblTotal.Left = 0;
					lblTotal.Width = lngTotalWidth;
					if (!boolAsterisk)
					{
						lblTotal.Text = "Total:".PadRight(15)  + dblReceiptTotal.ToString("$#,##0.00").PadLeft(13);
					}
					else
					{
						lblTotal.Text = "Total:".PadRight(15) + (dblReceiptTotal.ToString("$#,##0.00") + "*").PadLeft(13);
					}
				}
				else
				{
					fldTotal.Width = lblHeaderAmt.Width;
					fldTotal.Left = lblHeaderAmt.Left;
					lblTotal.Left = fldTotal.Left - lblTotal.Width;
					if (!boolAsterisk)
					{
						fldTotal.Text = dblReceiptTotal.ToString("$#,##0.00");
					}
					else
					{
						fldTotal.Text = dblReceiptTotal.ToString("$#,##0.00") + "*";
					}
					lblTotal.Text = "Total:";
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In Footer Format");
			}
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intRow = 0;
				float lngStartingPoint = 0;
				float lngRowHeight;
				string strPrinType = "";
				bool boolAddPayment = false;
				lngRowHeight = 270 / 1440F;

                var currentTransaction = transactionEnumerator.Current;
                if (currentTransaction == null)
                {
                    ClearFields();
                    return;
                }
               
                var paymentSummary = currentTransaction.Payments.GetAll().SummarizeAsReceiptTotals();
                var payment = currentTransaction.Payments.GetAll().Where(p => !p.IsChargedInterest()).FirstOrDefault();
				lblLocation.Text = "";
				lblComment.Text = "";
                lblName.Text = !string.IsNullOrWhiteSpace(currentTransaction.PayerName) ? currentTransaction.PayerName : ViewModel.Name;
                lblLocation.Text = ViewModel.Location;
                lblComment.Text = payment.Comments;
                var payCode = payment.GetPayCode();
                if (payCode == PropertyTaxPaymentCode.Abatement)
                {
                    strPrinType = "Abatement";
                    boolAddPayment = false;
                }
                else if (payCode == PropertyTaxPaymentCode.Discount)
                {
                    strPrinType = "Discount";
                    boolAddPayment = false;
                }
                else if (payCode == PropertyTaxPaymentCode.Interest)
                {
                    strPrinType = "Interest";
                    boolAddPayment = false;
                    intPaymentNumber += 1;
                 //   goto NEXTPAYMENT;
                }
                else
                {
                    strPrinType = "Principal";
                    boolAddPayment = true;
                }
                if (!ViewModel.IsReprint)
				{
					
					if (ViewModel.UseNarrowReceipt)
					{
						// this will set the size of the labels
						lblPrincipal.Width = lngTotalWidth;
						lblPLI.Width = lngTotalWidth;
						lblInterest.Width = lngTotalWidth;
						lblCost.Width = lngTotalWidth;
						lblPrincipalAmount.Width = 0;
						lblPLIAmount.Width = 0;
						lblInterestAmount.Width = 0;
						lblCostAmount.Width = 0;
					}

					// this is where the payment lines will be set to
					lngStartingPoint = lblLocation.Top + lblLocation.Height;

					if (paymentSummary.Principal != 0)
					{
						// show it
						lblPrincipal.Visible = true;
						if (ViewModel.UseNarrowReceipt)
						{
							lblPrincipal.Top = lngStartingPoint + (lngRowHeight * intRow);
							intRow += 1;
							lblPrincipalAmount.Visible = false;
							lblPrincipalAmount.Left = 0;
							if (boolAddPayment || !(payment.CashDrawer == "N" && payment.GeneralLedger == "N"))
                            {
                                lblPrincipal.Text = strPrinType.PadRight(15) +
                                                    paymentSummary.Principal.ToString("$#,##0.00").PadLeft(13);
							}
							else
							{
								boolAsterisk = true;
								lblPrincipal.Text = strPrinType.PadRight(15) + (paymentSummary.Principal.ToString("$#,##0.00") + "*").PadLeft(13);
							}
							lblPrincipalAmount.Text = "";
						}
						else
						{
							lblPrincipal.Top = lngStartingPoint + (lngRowHeight * intRow);
							lblPrincipalAmount.Top = lngStartingPoint + (lngRowHeight * intRow);
							lblPrincipalAmount.Visible = true;
							lblPrincipalAmount.Left = lblHeaderAmt.Left;
							lblPrincipal.Text = strPrinType;
							if (boolAddPayment || !(payment.CashDrawer == "N" && payment.GeneralLedger == "N"))
							{
								lblPrincipalAmount.Text = paymentSummary.Principal.ToString("$#,##0.00");
							}
							else
							{
								boolAsterisk = true;
								lblPrincipalAmount.Text = paymentSummary.Principal.ToString("$#,##0.00") + "*";
							}
							intRow += 1;
						}
					}
					else
					{
						// hide it
						lblPrincipal.Visible = false;
						lblPrincipalAmount.Visible = false;
					}
					if ( paymentSummary.PreLienInterest != 0)
					{
						// show it
						lblPLI.Visible = true;
						lblPLI.Visible = true;
						lblPLIAmount.Left = lblHeaderAmt.Left;
						if (ViewModel.UseNarrowReceipt)
						{
							lblPLI.Top = lngStartingPoint + (lngRowHeight * intRow);
							intRow += 1;
							lblPLIAmount.Visible = false;
							lblPLIAmount.Left = 0;
							lblPLIAmount.Top = 0;
							lblPLI.Text = "Pre Lien Interest".PadRight(15) + paymentSummary.PreLienInterest.ToString( "$#,##0.00").PadLeft(13);
							lblPLIAmount.Text = "";
						}
						else
						{
							lblPLI.Text = "Pre Lien Interest";
							lblPLIAmount.Text = paymentSummary.PreLienInterest.ToString("$#,##0.00");
							lblPLI.Top = lngStartingPoint + (lngRowHeight * intRow);
							lblPLIAmount.Top = lngStartingPoint + (lngRowHeight * intRow);
							intRow += 1;
						}
					}
					else
					{
						// hide it
						lblPLI.Visible = false;
						lblPLIAmount.Visible = false;
					}
					if (paymentSummary.CurrentInterest != 0)
					{
						// show it
						lblInterest.Visible = true;
						lblInterestAmount.Visible = true;
						lblInterestAmount.Left = lblHeaderAmt.Left;
						if (ViewModel.UseNarrowReceipt)
						{
							lblInterest.Top = lngStartingPoint + (lngRowHeight * intRow);
							intRow += 1;
							lblInterest.Text = "Interest".PadRight(15) + paymentSummary.CurrentInterest.ToString("$#,##0.00").PadLeft(13);
							lblInterestAmount.Text = "";
							lblInterestAmount.Top = 0;
							lblInterestAmount.Visible = false;
							lblInterestAmount.Left = 0;
						}
						else
						{
							lblInterestAmount.Text = paymentSummary.CurrentInterest.ToString("$#,##0.00");
							lblInterest.Text = "Interest";
							lblInterest.Top = lngStartingPoint + (lngRowHeight * intRow);
							lblInterestAmount.Top = lngStartingPoint + (lngRowHeight * intRow);
							intRow += 1;
						}
					}
					else
					{
						// hide it
						lblInterest.Visible = false;
						lblInterestAmount.Visible = false;
					}
					if (paymentSummary.LienCost != 0)
					{
						// show it
						lblCost.Visible = true;
						lblCostAmount.Visible = true;
						lblCostAmount.Left = lblHeaderAmt.Left;
						if (ViewModel.UseNarrowReceipt)
						{
							lblCost.Top = lngStartingPoint + (lngRowHeight * intRow);
							intRow += 1;
							lblCost.Text = "Cost".PadRight(15) + paymentSummary.LienCost.ToString("$#,##0.00").PadLeft(13);
							lblCostAmount.Top = 0;
							lblCostAmount.Visible = false;
							lblCostAmount.Text = "";
							lblCostAmount.Left = 0;
						}
						else
						{
							lblCost.Top = lngStartingPoint + (lngRowHeight * intRow);
							lblCostAmount.Top = lngStartingPoint + (lngRowHeight * intRow);
							lblCost.Text = "Cost";
							lblCostAmount.Text = paymentSummary.LienCost.ToString("$#,##0.00");
							intRow += 1;
						}
					}
					else
					{
						// hide it
						lblCost.Visible = false;
						lblCostAmount.Visible = false;
					}
					if (boolAddPayment)
					{
						dblReceiptTotal += paymentSummary.Principal + paymentSummary.PreLienInterest + paymentSummary.CurrentInterest + paymentSummary.LienCost;
					}
				}
				else
				{

					if (ViewModel.UseNarrowReceipt)
					{
						// this will set the size of the labels
						lblPrincipal.Width = lngTotalWidth;
						lblPLI.Width = lngTotalWidth;
						lblInterest.Width = lngTotalWidth;
						lblCost.Width = lngTotalWidth;
						lblPrincipalAmount.Width = 0;
						// lblHeaderAmt.Width
						lblPLIAmount.Width = 0;
						// lblHeaderAmt.Width
						lblInterestAmount.Width = 0;
						// lblHeaderAmt.Width
						lblCostAmount.Width = 0;
						// lblHeaderAmt.Width
					}
					
     //               var payCode = payment.GetPayCode();
					//if (payCode == PropertyTaxPaymentCode.Abatement)
					//{
					//	strPrinType = "Abatement";
					//	boolAddPayment = false;
					//}
					//else if (payCode == PropertyTaxPaymentCode.Discount)
					//{
					//	strPrinType = "Discount";
					//	boolAddPayment = false;
					//}
					//else if (payCode == PropertyTaxPaymentCode.Interest)
					//{
					//	strPrinType = "Interest";
					//	boolAddPayment = false;
					//	intPaymentNumber += 1;
					//	//goto NEXTPAYMENT;
					//}
					//else
					//{
					//	strPrinType = "Principal";
					//	boolAddPayment = true;
					//}
					// this is where the payment lines will be set to
					lngStartingPoint = lblLocation.Top + lblLocation.Height;

					if (paymentSummary.Principal != 0)
					{
						// show it
						lblPrincipal.Visible = true;
						if (ViewModel.UseNarrowReceipt)
						{
							lblPrincipal.Top = lngStartingPoint + (lngRowHeight * intRow);
							intRow += 1;
							lblPrincipalAmount.Visible = false;
							lblPrincipalAmount.Left = 0;
							if (boolAddPayment || !(payment.CashDrawer == "N" && payment.GeneralLedger == "N"))
							{
								lblPrincipal.Text = strPrinType.PadRight(15)  + paymentSummary.Principal.ToString("$#,##0.00").PadLeft(13);
							}
							else
							{
								boolAsterisk = true;
								lblPrincipal.Text = strPrinType.PadRight(15) + (paymentSummary.Principal.ToString( "$#,##0.00") + "*").PadLeft(13);
							}
							lblPrincipalAmount.Text = "";
						}
						else
						{
							lblPrincipal.Top = lngStartingPoint + (lngRowHeight * intRow);
							lblPrincipalAmount.Top = lngStartingPoint + (lngRowHeight * intRow);
							lblPrincipalAmount.Visible = true;
							lblPrincipalAmount.Left = lblHeaderAmt.Left;
							lblPrincipal.Text = strPrinType;
							if (boolAddPayment || !(payment.CashDrawer == "N" && payment.GeneralLedger == "N"))
							{
								lblPrincipalAmount.Text = paymentSummary.Principal.ToString( "$#,##0.00");
							}
							else
							{
								boolAsterisk = true;
								lblPrincipalAmount.Text = paymentSummary.Principal.ToString("$#,##0.00") + "*";
							}
							intRow += 1;
						}
					}
					else
					{
						// hide it
						lblPrincipal.Visible = false;
						lblPrincipalAmount.Visible = false;
					}
					if (paymentSummary.PreLienInterest != 0)
					{
						// show it
						lblPLI.Visible = true;
						lblPLI.Visible = true;
						lblPLIAmount.Left = lblHeaderAmt.Left;
						if (ViewModel.UseNarrowReceipt)
						{
							lblPLI.Top = lngStartingPoint + (lngRowHeight * intRow);
							intRow += 1;
							lblPLIAmount.Visible = false;
							lblPLIAmount.Left = 0;
							lblPLIAmount.Top = 0;
                            lblPLI.Text = "Pre Lien Interest".PadRight(15) +
                                          paymentSummary.PreLienInterest.ToString("$#,##0.00").PadLeft(13);
							lblPLIAmount.Text = "";
						}
						else
						{
							lblPLI.Text = "Pre Lien Interest";
							lblPLIAmount.Text = paymentSummary.PreLienInterest.ToString( "$#,##0.00");
							lblPLI.Top = lngStartingPoint + (lngRowHeight * intRow);
							lblPLIAmount.Top = lngStartingPoint + (lngRowHeight * intRow);
							intRow += 1;
						}
					}
					else
					{
						// hide it
						lblPLI.Visible = false;
						lblPLIAmount.Visible = false;
					}
					if (paymentSummary.CurrentInterest != 0)
					{
						// show it
						lblInterest.Visible = true;
						lblInterestAmount.Visible = true;
						lblInterestAmount.Left = lblHeaderAmt.Left;
						if (ViewModel.UseNarrowReceipt)
						{
							lblInterest.Top = lngStartingPoint + (lngRowHeight * intRow);
							intRow += 1;
							lblInterest.Text = "Interest".PadRight(15) + paymentSummary.CurrentInterest.ToString( "$#,##0.00").PadLeft(13);
							lblInterestAmount.Text = "";
							lblInterestAmount.Top = 0;
							lblInterestAmount.Visible = false;
							lblInterestAmount.Left = 0;
						}
						else
						{
							lblInterestAmount.Text = paymentSummary.CurrentInterest.ToString( "$#,##0.00");
							lblInterest.Text = "Interest";
							lblInterest.Top = lngStartingPoint + (lngRowHeight * intRow);
							lblInterestAmount.Top = lngStartingPoint + (lngRowHeight * intRow);
							intRow += 1;
						}
					}
					else
					{
						// hide it
						lblInterest.Visible = false;
						lblInterestAmount.Visible = false;
					}
					if (paymentSummary.LienCost != 0)
					{
						// show it
						lblCost.Visible = true;
						lblCostAmount.Visible = true;
						lblCostAmount.Left = lblHeaderAmt.Left;
						if (ViewModel.UseNarrowReceipt)
						{
							lblCost.Top = lngStartingPoint + (lngRowHeight * intRow);
							intRow += 1;
							lblCost.Text = "Cost";
							lblCost.Text = "Cost".PadRight(15) + paymentSummary.LienCost.ToString("$#,##0.00").PadLeft(13);
							lblCostAmount.Top = 0;
							lblCostAmount.Visible = false;
							lblCostAmount.Text = "";
							lblCostAmount.Left = 0;
						}
						else
						{
							lblCost.Top = lngStartingPoint + (lngRowHeight * intRow);
							lblCostAmount.Top = lngStartingPoint + (lngRowHeight * intRow);
							lblCost.Text = "Cost";
							lblCostAmount.Text = paymentSummary.LienCost.ToString("$#.##0.00");
							intRow += 1;
						}
					}
					else
					{
						// hide it
						lblCost.Visible = false;
						lblCostAmount.Visible = false;
					}
					if (boolAddPayment)
					{
						dblReceiptTotal += paymentSummary.Principal + paymentSummary.PreLienInterest + paymentSummary.CurrentInterest + paymentSummary.LienCost;
					}
				}
				// set the height of the detail section
				Detail.Height = lngStartingPoint + ((intRow) * lngRowHeight);
				intPaymentNumber += 1;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In Detail Format");
			}
		}

		private void ClearFields()
		{
			Detail.Height = 0;
			lblPrincipal.Text = "";
			lblPrincipalAmount.Text = "";
			lblPLI.Text = "";
			lblPLIAmount.Text = "";
			lblInterest.Text = "";
			lblInterestAmount.Text = "";
			lblCost.Text = "";
			lblCostAmount.Text = "";
		}

		private string GetRemainingCLBalance()
		{
			try
			{			
                var groupSummaryResult = ViewModel.GetGroupSummary();
                if (groupSummaryResult.groupCount > 1)
                {
                    return "Remaining Balance : " + "(" + groupSummaryResult.groupCount + ") " + groupSummaryResult.groupSummary.Balance.ToString("$#,##0.00");
                }
                return "Remaining Balance : " + groupSummaryResult.groupSummary.Balance.ToString("$#,##0.00");
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Remaining Balance Error");
			}
			return "";
		}
      	
        public void Show()
        {
            if (!ViewModel.PrintWithoutPreview || ViewModel.ReceiptPrinterSetting.IsNullOrWhiteSpace())
            {
                frmReportViewer.InstancePtr.Init(this);
            }
            else
            {
                if (SetupAsPrinterFile(ViewModel.ReceiptPrinterSetting))
                {
                    var ARTemp = new GrapeCity.ActiveReports.Export.Word.Section.RtfExport();
                    this.Run(false);
                    string fileName = "PRNT1.TXT";
                    ARTemp.Export(this.Document, fileName);
                }
                else
                {

                    this.PrintReportOnDotMatrix("RCPTPrinterName");
                }
            }
        }

        public ICLReceiptViewModel ViewModel { get; set; }
    }
}
