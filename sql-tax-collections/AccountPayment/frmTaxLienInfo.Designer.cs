﻿namespace TWCL0000.AccountPayment
{
    partial class frmTaxLienInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flexLayoutPanel1 = new Wisej.Web.FlexLayoutPanel();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 557);
            this.BottomPanel.Size = new System.Drawing.Size(433, 0);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.flexLayoutPanel1);
            this.ClientArea.Location = new System.Drawing.Point(0, 0);
            this.ClientArea.Size = new System.Drawing.Size(433, 557);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(433, 0);
            // 
            // flexLayoutPanel1
            // 
            this.flexLayoutPanel1.LayoutStyle = Wisej.Web.FlexLayoutStyle.Vertical;
            this.flexLayoutPanel1.Location = new System.Drawing.Point(40, 26);
            this.flexLayoutPanel1.Name = "flexLayoutPanel1";
            this.flexLayoutPanel1.Size = new System.Drawing.Size(349, 528);
            this.flexLayoutPanel1.TabIndex = 1;
            this.flexLayoutPanel1.TabStop = true;
            // 
            // frmTaxLienInfo
            // 
            this.ClientSize = new System.Drawing.Size(433, 557);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmTaxLienInfo";
            this.Text = "Lien Information";
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private Wisej.Web.FlexLayoutPanel flexLayoutPanel1;
    }
}