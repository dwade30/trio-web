﻿using System;
using System.Linq;
using fecherFoundation;
using fecherFoundation.VisualBasicLayer;
using Global;
using SharedApplication;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections.AccountPayment;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Models;
using Wisej.Web;

namespace TWCL0000.AccountPayment
{
    public partial class frmCLBatchProcess : BaseForm, IView<ITaxCollectionsBatchViewModel>
    {
        private bool isImporting = false;
        private bool validatingAccount = false;
        private bool cancelling = true;
        public frmCLBatchProcess()
        {
            InitializeComponent();
            InitializeComponentEx();
        }

        public frmCLBatchProcess(ITaxCollectionsBatchViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }

        private void InitializeComponentEx()
        {
            this.Load += FrmCLBatchProcess_Load;
            this.vsBatch.CurrentCellChanged += VsBatch_CurrentCellChanged;
            this.vsBatch.KeyDown += VsBatch_KeyDown;
            txtAmount.KeyDown += TxtAmount_KeyDown;
            txtBatchAccount.KeyDown += TxtBatchAccount_KeyDown;
            txtBatchAccount.Validating += TxtBatchAccount_Validating;
            btnSaveExit.Click += BtnSaveExit_Click;
            this.Closing += FrmCLBatchProcess_Closing;
            mnuImportFirstAmericanBatch.Click += MnuImportFirstAmericanBatch_Click;
            mnuFileImport.Click += MnuFileImport_Click;
            mnuBatchPrint.Click += MnuBatchPrint_Click;
            btnSearch.Click += BtnSearch_Click;
        }

        private void BtnSearch_Click(object sender, EventArgs e)
        {
            Search();
        }

        private void Search()
        {
            var account = ViewModel.Search();
            if (account > 0)
            {
                if (txtBatchAccount.Text.ToIntegerValue() != account)
                {
                    txtAmount.Text = "";
                    txtBatchAccount.Text = account.ToString();
                    ValidateAccount();
                }
            }

        }

        private void MnuBatchPrint_Click(object sender, EventArgs e)
        {
            ViewModel.ShowBatchListing();
        }

        private void MnuFileImport_Click(object sender, EventArgs e)
        {
            ViewModel.ImportBatch();
            ShowBatch();
        }

        private void MnuImportFirstAmericanBatch_Click(object sender, EventArgs e)
        {
            ViewModel.ImportFirstAmerican();
            ShowBatch();
        }

        private void FrmCLBatchProcess_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (cancelling)
            {
                ViewModel.Cancel();
            }
        }

        private void BtnSaveExit_Click(object sender, EventArgs e)
        {
            CompleteTransaction();
        }

        private void TxtBatchAccount_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {            
            ValidateAccount();
            validatingAccount = false;
        }

        private void TxtBatchAccount_KeyDown(object sender, KeyEventArgs e)
        {
            Keys keyCode = e.KeyCode;
            if (keyCode == Keys.Return)
            {
                
                if (ValidateAccount())
                {
                    txtAmount.Focus();
                }

                validatingAccount = false;
            }
        }

        private bool ValidateAccount()
        {
            if (validatingAccount)
            {
                return true;
            }

            validatingAccount = true;
            var account = txtBatchAccount.Text.ToIntegerValue();
            if (account <= 0)
            {
                return false;
            }

            var period = cmbPeriod1.SelectedIndex + 1;
            if (period == 0)
            {
                period = 4;
            }

            var calculatedAccount = ViewModel.LoadTaxAccount(account, ViewModel.BillType);
            if (calculatedAccount == null)
            {
                txtBatchAccount.Text = "";
                txtAmount.Text = "";
                lblName.Text = "";
                lblLocation.Text = "";
                if (!isImporting)
                {
                    MessageBox.Show("Please enter a valid account number", "Invalid Account", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                }

                return false;
            }

            var bill = calculatedAccount.TaxAccount.Bills.FirstOrDefault(b =>
                b.TaxYear == ViewModel.TaxBatch.DefaultTaxYear);
            if (bill == null)
            {
                txtBatchAccount.Text = "";
                txtAmount.Text = "";
                lblName.Text = "";
                lblLocation.Text = "";
                if (!isImporting)
                {
                    MessageBox.Show(
                        "No bill for account " + account + " and year " + ViewModel.TaxBatch.DefaultTaxYear + " found",
                        "Missing Bill Record", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                return false;
            }

            var previousUnpaidBillsExist = false;
            if (!isImporting)
            {
                var previousBills =
                    calculatedAccount.TaxAccount.Bills.Where(b => b.TaxYear < ViewModel.TaxBatch.DefaultTaxYear);
                if (previousBills.Any())
                {
                    foreach (var previousBill in previousBills)
                    {
                        if (calculatedAccount.CalculationSummaries.Any(s =>
                            s.Key == previousBill.ID && s.Value.Balance != 0))
                        {
                            previousUnpaidBillsExist = true;
                            break;
                        }
                    }
                }

                if (previousUnpaidBillsExist)
                {
                    if (MessageBox.Show(
                            "There are previous year(s) with account balances.  Would you like to continue adding this payment?",
                            "Previous Account Balance", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) !=
                        DialogResult.Yes)
                    {
                        return false;
                    }
                    
                    ViewModel.AddCYAEntry("CL Batch payment with previous balances. - Acct: " + account);
                }

                lblName.Text = "Name: " + bill.OwnerNames();

                if (bill.StreetNumber > 0)
                {
                    lblLocation.Text = bill.StreetNumber + " " +
                                       bill.StreetName;
                }
                else
                {
                    lblLocation.Text = bill.StreetName;
                }

                var summary = calculatedAccount.CalculationSummaries.FirstOrDefault(s => s.Key == 99999).Value;
                var due = 0M;
                if (summary != null)
                {
                    due = summary.Balance;
                }

                txtAmount.Text = due.FormatAsMoney();
            }

           

            return true;
        }
        private void TxtAmount_KeyDown(object sender, KeyEventArgs e)
        {
            Keys keyCode = e.KeyCode;
            if (keyCode == Keys.Return)
            {
                if (txtAmount.Text.ToIntegerValue() != 0)
                {
                    if (txtBatchAccount.Text.ToIntegerValue() != 0)
                    {
                        if (MessageBox.Show("Would you like to save this payment?", "Save Payment?", MessageBoxButtons.YesNo , MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            if (GetCurrentDue() < txtAmount.Text.ToDecimalValue())
                            {
                                if (MessageBox.Show("This payment is more than owed, would you like to continue?", "Overpayment", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) !=
                                    DialogResult.Yes)
                                {
                                    return;
                                }
                            }

                            // End If
                            AddPaymentToBatch(txtBatchAccount.Text.ToIntegerValue(),txtAmount.Text.ToIntegerValue(),ViewModel.BillType);
                            
                           ShowBatch();
                        }
                        else
                        {
                            txtBatchAccount.Focus();
                        }
                    }
                    else
                    {
                        MessageBox.Show("You didn't enter an account", "Invalid Account", MessageBoxButtons.OK,
                            MessageBoxIcon.Warning);                        
                        txtBatchAccount.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("The payment entered cannot be zero", "Invalid Payment Amount", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtAmount.Focus();
                }
            }
        }

        private void AddPaymentToBatch(int account,  decimal amount, PropertyTaxBillType billType)
        {
            ViewModel.AddPaymentToBatch(account, amount, billType);
        }
        private decimal GetCurrentDue()
        {
            var taxAccount = ViewModel.CurrentTaxAccount();
            if (taxAccount == null)
            {
                return 0;
            }

            var bill = taxAccount.TaxAccount.Bills.FirstOrDefault(b => b.TaxYear == ViewModel.TaxBatch.DefaultTaxYear);
            if (bill == null)
            {
                return 0;
            }

            var summary = taxAccount.CalculationSummaries.FirstOrDefault(s => s.Key == bill.ID).Value;
            return summary.Balance;
        }
        private void VsBatch_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Delete)
            {
                // delete a row from the grid
                DeletePaymentFromBatch(vsBatch.Row);
                //if (vsBatch.Rows == 1)
                //{
                //    mnuFileImport.Enabled = true;
                //    mnuImportFirstAmericanBatch.Enabled = true;
                //}
            }
        }

        private void DeletePaymentFromBatch(int row)
        {
            if (MessageBox.Show("Are you sure that you would like to delete this payment line?", "Delete Payment?",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                var rowData = (BatchRecover)vsBatch.RowData(row);
                if (rowData != null)
                {
                    if (rowData.Id > 0)
                    {
                        ViewModel.DeleteBatchItem(rowData.Id);
                    }
                    vsBatch.RemoveItem(row);
                    ShowTotal();
                    if (vsBatch.Rows == 1)
                    {
                        //mnuBatchRecover.Enabled = true;
                    }
                }
            }
        }

        private void VsBatch_CurrentCellChanged(object sender, EventArgs e)
        {

        }

        private void FrmCLBatchProcess_Load(object sender, EventArgs e)
        {
            SetupGrid();
            ShowBatch();
        }

        private void ShowBatch()
        {
            vsBatch.Rows = 1;
            var batch = ViewModel.TaxBatch;
            foreach (var item in batch.BatchItems)
            {
                ShowItem(item.BatchRecover);
            }

            if (vsBatch.Rows > 1)
            {
                mnuImportFirstAmericanBatch.Enabled = false;
                mnuFileImport.Enabled = false;
            }
            else
            {
                mnuImportFirstAmericanBatch.Enabled = true;
                mnuFileImport.Enabled = true;
            }
            ShowTotal();
        }

        private void ShowTotal()
        {
            var total =ViewModel.TaxBatch.BatchItems.Sum(b =>
                b.BatchRecover.Cost.GetValueOrDefault() + b.BatchRecover.Prin.GetValueOrDefault() + b.BatchRecover.Int.GetValueOrDefault());
            txtTotal.Text = total.FormatAsCurrencyNoSymbol();
        }

        private void ShowItem(BatchRecover item)
        {
            if (item != null)
            {
                vsBatch.Rows += 1;
                var row = vsBatch.Rows - 1;
                vsBatch.RowData(row, item);
                vsBatch.TextMatrix(row, (int) BatchColumns.Account, item.AccountNumber.GetValueOrDefault());
                vsBatch.TextMatrix(row, (int) BatchColumns.Name,item.Name);
                vsBatch.TextMatrix(row, (int)BatchColumns.TaxYear,item.Year.GetValueOrDefault());
                vsBatch.TextMatrix(row, (int)BatchColumns.Reference,item.Ref);
                vsBatch.TextMatrix(row, (int)BatchColumns.Principal,item.Prin.GetValueOrDefault());
                vsBatch.TextMatrix(row, (int)BatchColumns.Interest,item.Int.GetValueOrDefault());
                vsBatch.TextMatrix(row, (int)BatchColumns.Cost,item.Cost.GetValueOrDefault());
                var total = item.Prin.GetValueOrDefault() + item.Int.GetValueOrDefault() +
                            item.Cost.GetValueOrDefault();
                vsBatch.TextMatrix(row, (int)BatchColumns.Total,total);
            }
        }

        private void SetupGrid()
        {
            int gridWidth = vsBatch.WidthOriginal;
            vsBatch.Cols = 9;
           
            vsBatch.ColWidth((int) BatchColumns.BillType, (gridWidth * .04).ToInteger());
            vsBatch.ColWidth((int) BatchColumns.Account, (gridWidth * .055).ToInteger());
            vsBatch.ColWidth((int) BatchColumns.Name, (gridWidth * .3).ToInteger());
            vsBatch.ColWidth((int) BatchColumns.TaxYear, (gridWidth * .06).ToInteger());
            vsBatch.ColWidth((int) BatchColumns.Reference, (gridWidth * .08).ToInteger());
            vsBatch.ColWidth((int) BatchColumns.Principal, (gridWidth * .1).ToInteger());
            vsBatch.ColWidth((int) BatchColumns.Interest, (gridWidth * .1).ToInteger());
            vsBatch.ColWidth((int) BatchColumns.Cost, (gridWidth * .1).ToInteger());
            vsBatch.ColWidth((int) BatchColumns.Total, (gridWidth * .13).ToInteger());
            vsBatch.TextMatrix(0, (int) BatchColumns.Account, "Account");
            vsBatch.TextMatrix(0, (int) BatchColumns.Name, "Name");
            vsBatch.TextMatrix(0, (int) BatchColumns.TaxYear, "Year");
            vsBatch.TextMatrix(0, (int) BatchColumns.Reference, "Reference");
            vsBatch.TextMatrix(0, (int) BatchColumns.Principal, "Principal");
            vsBatch.TextMatrix(0, (int) BatchColumns.Interest, "Interest");
            vsBatch.TextMatrix(0, (int) BatchColumns.Cost, "Cost");
            vsBatch.TextMatrix(0,(int)BatchColumns.Total,"Total");            

            vsBatch.ColFormat((int) BatchColumns.Principal, "#,##0.00");
            vsBatch.ColFormat((int) BatchColumns.Interest, "#,##0.00");
            vsBatch.ColFormat((int)BatchColumns.Cost,"#,##0.00");
            vsBatch.ColFormat((int) BatchColumns.Total, "#,##0.00");

            vsBatch.ColAlignment((int)BatchColumns.Account, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsBatch.ColAlignment((int)BatchColumns.TaxYear, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsBatch.ColAlignment((int)BatchColumns.Principal, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vsBatch.ColAlignment((int)BatchColumns.Interest, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vsBatch.ColAlignment((int)BatchColumns.Cost, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vsBatch.ColAlignment((int)BatchColumns.Total, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vsBatch.ColAlignment((int)BatchColumns.Reference, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            //vsBatch.ColAlignment(lngColBatchCorrectAmount, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsBatch.ColAlignment((int)BatchColumns.Name, FCGrid.AlignmentSettings.flexAlignLeftCenter);
        }
        public ITaxCollectionsBatchViewModel ViewModel { get; set; }

        private void CloseWithoutCancelling()
        {
            cancelling = false;
            Close();
        }

        private void CompleteTransaction()
        {
            if (vsBatch.Rows <= 1)
            {
                MessageBox.Show("There are no entries to process", "No Entries", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return;
            }

            if (MessageBox.Show("Are you sure you want to process the batch with total " + txtTotal.Text + "?",
                    "Process Batch", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
            {
                return;
            }

            ViewModel.CompleteTransaction();
            CloseWithoutCancelling();
        }
        private enum BatchColumns
        {           
            BillType = 0,
            Account = 1,
            Name = 2,
            TaxYear =3,
            Reference =4,
            Principal = 5,
            Interest = 6,
            Cost = 7,
            Total = 8
        }
    }
}
