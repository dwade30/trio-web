﻿using System;
using Global;
using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections.AccountPayment;
using Wisej.Web;

namespace TWCL0000.AccountPayment
{
    public partial class frmTaxLienInfo : BaseForm, IView<ITaxLienInfoViewModel>
    {
        public frmTaxLienInfo()
        {
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            this.Load += FrmTaxLienInfo_Load;
        }

        private void FrmTaxLienInfo_Load(object sender, EventArgs e)
        {
            ShowDetail();
        }

        public frmTaxLienInfo(ITaxLienInfoViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }
        public ITaxLienInfoViewModel ViewModel { get; set; }

        public new void Show()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            if (!this.Visible)
            {
                //FC:FINAL:MSH - i.issue #1839: sometimes new modal window can be overlapped by already opened modal window
                this.BringToFront();
                base.Show(FormShowEnum.Modeless, "", null);
            }
        }

        private void ShowDetail()
        {
            flexLayoutPanel1.Controls.Add(GetControl("Lien Id", ViewModel.LienId.ToString()));
            flexLayoutPanel1.Controls.Add(GetControl("Rate Record", ViewModel.RateId.ToString()));

            if (ViewModel.DateCreated != DateTime.MinValue)
            {
                flexLayoutPanel1.Controls.Add(GetControl("Date Created",ViewModel.DateCreated.FormatAndPadShortDate()));
            }

            flexLayoutPanel1.Controls.Add(GetControl("Status", ViewModel.Status));
            if (ViewModel.InterestAppliedThroughDate != DateTime.MinValue)
            {
                flexLayoutPanel1.Controls.Add(GetControl("Interest Applied Through", ViewModel.DateCreated.FormatAndPadShortDate()));
            }

            if (!string.IsNullOrWhiteSpace(ViewModel.Book) && !string.IsNullOrWhiteSpace(ViewModel.Page))
            {
                flexLayoutPanel1.Controls.Add(GetControl(@"Book / Page", "B" + ViewModel.Book + "P" + ViewModel.Page));
            }

            flexLayoutPanel1.Controls.Add(GetControl("Principal",ViewModel.LienSummary.OriginalPrincipalCharged.FormatAsCurrencyNoSymbol()));
            flexLayoutPanel1.Controls.Add(GetControl("Pre-Lien Interest",ViewModel.LienSummary.OriginalPreLienInterestCharged.FormatAsCurrencyNoSymbol()));
            flexLayoutPanel1.Controls.Add(GetControl("Costs", ViewModel.LienSummary.OriginalCostsCharged.FormatAsCurrencyNoSymbol()));
            flexLayoutPanel1.Controls.Add(GetControl("Interest Charged", ViewModel.LienSummary.OriginalInterestCharged.FormatAsCurrencyNoSymbol()));
            flexLayoutPanel1.Controls.Add(GetControl("Maturity Fee", ViewModel.LienSummary.MaturityFee.FormatAsCurrencyNoSymbol()));
            flexLayoutPanel1.Controls.Add(GetControl("Lien Total", ViewModel.LienSummary.GetTotalCharged().FormatAsCurrencyNoSymbol()));
            flexLayoutPanel1.Controls.Add(GetControl("",""));
            flexLayoutPanel1.Controls.Add(GetControl("Principal Paid", ViewModel.LienSummary.PrincipalPaid.FormatAsCurrencyNoSymbol()));
            flexLayoutPanel1.Controls.Add(GetControl("Interest Paid", ViewModel.LienSummary.GetTotalInterestPaid().FormatAsCurrencyNoSymbol()));
            flexLayoutPanel1.Controls.Add(GetControl("Costs Paid", ViewModel.LienSummary.CostsPaid.FormatAsCurrencyNoSymbol()));
            flexLayoutPanel1.Controls.Add(GetControl("Total Paid", ViewModel.LienSummary.GetTotalPaid().FormatAsCurrencyNoSymbol()));
            flexLayoutPanel1.Controls.Add(GetControl("",""));
            flexLayoutPanel1.Controls.Add(GetControl("Total Due", ViewModel.LienSummary.GetNetOwed().FormatAsCurrencyNoSymbol()));
        }

        private TableLayoutPanel GetControl(string description, string value)
        {
            var panel = new TableLayoutPanel();
            panel.Margin = new Padding(1);
            panel.ColumnCount = 2;
            panel.RowCount = 1;
            panel.AutoSize = true;
            panel.Dock = DockStyle.Left & DockStyle.Right;
            panel.Padding = new Padding(1);

            panel.Controls.Add(new Label() { Text = description, AutoSize = false, Dock = DockStyle.Fill, Width = 165, Margin = Padding.Empty });
            panel.Controls.Add(new Label() { Text = value, AutoSize = true, Dock = DockStyle.Fill, Margin = Padding.Empty });

            return panel;
        }
    }
}
