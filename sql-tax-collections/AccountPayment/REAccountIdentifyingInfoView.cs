﻿using System;
using SharedApplication.Extensions;
using SharedApplication.RealEstate;
using Wisej.Web;

namespace TWCL0000.AccountPayment
{
    public partial class REAccountIdentifyingInfoView : Wisej.Web.UserControl
    {
        private RealEstateAccountBriefInfo accountInfo;
        private decimal acreage;
        public REAccountIdentifyingInfoView()
        {
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            
        }

        public REAccountIdentifyingInfoView(RealEstateAccountBriefInfo info, decimal acreage) : this()
        {
            accountInfo = info;
            this.acreage = acreage;
            ShowInfo();
        }

        private void ShowInfo()
        {
            lblName.Text = BuildName(accountInfo.DeedName1, accountInfo.DeedName2);
            lblLocation.Text = BuildLocation(accountInfo.LocationNumber, accountInfo.LocationStreet);
            lblMapLot.Text = accountInfo.MapLot;
            lblReference.Text = accountInfo.Reference1;
            lblAcreage.Text = acreage.ToString("0.##");
        }

        private string BuildName(string name1, string name2)
        {
            if (!string.IsNullOrWhiteSpace(name2))
            {
                return name1.Trim() + " & " + name2.Trim();
            }

            if (!string.IsNullOrWhiteSpace(name1))
            {
                return name1.Trim();
            }

            return "";
        }

        private string BuildLocation(int locationNumber, string locationStreet)
        {
            if (locationNumber >= 0)
            {
                return (locationNumber.ToString() + " " + locationStreet.Trim()).Trim();
            }

            return locationStreet.Trim();
        }
    }
}
