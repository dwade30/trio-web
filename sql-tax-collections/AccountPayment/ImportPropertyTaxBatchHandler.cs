﻿using System.Collections.Generic;
using System.IO;
using SharedApplication;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.AccountPayment;

namespace TWCL0000.AccountPayment
{
    public class ImportPropertyTaxBatchHandler : CommandHandler<ImportPropertyTaxBatch, (bool Success, IEnumerable<(int Year, int Account, decimal Amount)> Items)>
    {
        private CommandDispatcher commandDispatcher;

        public ImportPropertyTaxBatchHandler(CommandDispatcher commandDispatcher)
        {
            this.commandDispatcher = commandDispatcher;
        }
        protected override (bool Success, IEnumerable<(int Year, int Account, decimal Amount)> Items) Handle(ImportPropertyTaxBatch command)
        {
            var filePath = commandDispatcher.Send(new UploadFile
            {
                DialogTitle = "Select file to import from"
            }).Result;
            if (string.IsNullOrWhiteSpace(filePath))
            {
                return (Success: false, Items: new List<(int Year, int Account, decimal Amount)>());
            }

            if (!File.Exists(filePath))
            {
                return (Success: false, Items: new List<(int Year, int Account, decimal Amount)>());
            }

            var ts = new StreamReader(filePath);
            var line = "";
            var items = new List<(int Year, int Account, decimal Amount)>();
            while ((line = ts.ReadLine()) != null)
            {
                var parsedLine = ParseLine(line);
                if (parsedLine.Account > 0 && parsedLine.Amount != 0)
                {
                    items.Add((Year: parsedLine.Year, Account: parsedLine.Account, Amount: parsedLine.Amount));
                }
            }
            ts.Close();
            return (Success: true, Items: items);
        }

        private (int Year, int Account, decimal Amount) ParseLine(string line)
        {
            var account = line.Substring(0, 8).ToIntegerValue();
            var year = line.Substring(41, 4).ToIntegerValue();
            var amount = line.Substring(45, 14).ToDecimalValue();
            return (Year: year, Account: account, Amount: amount);
        }
    }
}