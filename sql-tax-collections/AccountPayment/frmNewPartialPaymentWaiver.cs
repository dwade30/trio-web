﻿using System;
using Global;
using SharedApplication;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections.Lien;
using Wisej.Web;

namespace TWCL0000.AccountPayment
{
    public partial class frmNewPartialPaymentWaiver : BaseForm, IModalView<IPartialPaymentWaiverViewModel>
    { 
        public frmNewPartialPaymentWaiver()
        {
            InitializeComponent();
            InitializeComponentEx();
        }

        public frmNewPartialPaymentWaiver(IPartialPaymentWaiverViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }

        private void InitializeComponentEx()
        {
            this.Load += FrmNewPartialPaymentWaiver_Load;
            this.btnProcess.Click += BtnProcess_Click;
        }

        private void BtnProcess_Click(object sender, EventArgs e)
        {
            ViewModel.PaymentAmount = txtPayment.Text.ToDecimalValue();
            ViewModel.PaymentDate = dtPaymentDate.Value;
            ViewModel.SignedDate = dtSigned.Value;
            ViewModel.State = txtState.Text;
            ViewModel.Witness = txtWitness.Text;
            ViewModel.MapLot = txtMapLot.Text;
            ViewModel.Owner = txtOwner.Text;
            ViewModel.PrintWaiver();
            Close();
        }

        private void FrmNewPartialPaymentWaiver_Load(object sender, EventArgs e)
        {
            ShowData();
        }

        private void ShowData()
        {
            txtMapLot.Text = ViewModel.MapLot;
            txtMunicipality.Text = ViewModel.Municipality;
            txtOwner.Text = ViewModel.Owner;
            txtState.Text = ViewModel.State;
            txtWitness.Text = ViewModel.Witness;
            dtPaymentDate.Value = ViewModel.PaymentDate;
            dtSigned.Value = ViewModel.SignedDate;
            txtPayment.Text = ViewModel.PaymentAmount.FormatAsMoney();
            if (ViewModel.ShowSignDate)
            {
                lblSignedDate.Visible = true;
                dtSigned.Visible = true;
            }
            else
            {
                lblSignedDate.Visible = false;
                dtSigned.Visible = false;
            }
        }

        public IPartialPaymentWaiverViewModel ViewModel { get; set; }
        public void ShowModal()
        {
            this.Show(FormShowEnum.Modal);
        }
    }
}
