﻿namespace TWCL0000.AccountPayment
{
    partial class frmNonBudgetaryPayment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmNonBudgetaryPayment));
            this.btnProcess = new fecherFoundation.FCButton();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.fcLabel2 = new fecherFoundation.FCLabel();
            this.fcLabel3 = new fecherFoundation.FCLabel();
            this.fcLabel4 = new fecherFoundation.FCLabel();
            this.fcLabel5 = new fecherFoundation.FCLabel();
            this.txtPrincipal = new fecherFoundation.FCTextBox();
            this.javaScript1 = new Wisej.Web.JavaScript(this.components);
            this.txtInterest = new fecherFoundation.FCTextBox();
            this.txtPreLienInterest = new fecherFoundation.FCTextBox();
            this.txtCosts = new fecherFoundation.FCTextBox();
            this.lblTotal = new fecherFoundation.FCLabel();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 262);
            this.BottomPanel.Size = new System.Drawing.Size(351, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.lblTotal);
            this.ClientArea.Controls.Add(this.txtCosts);
            this.ClientArea.Controls.Add(this.txtPreLienInterest);
            this.ClientArea.Controls.Add(this.txtInterest);
            this.ClientArea.Controls.Add(this.txtPrincipal);
            this.ClientArea.Controls.Add(this.fcLabel5);
            this.ClientArea.Controls.Add(this.fcLabel4);
            this.ClientArea.Controls.Add(this.fcLabel3);
            this.ClientArea.Controls.Add(this.fcLabel2);
            this.ClientArea.Controls.Add(this.fcLabel1);
            this.ClientArea.Size = new System.Drawing.Size(371, 415);
            this.ClientArea.Controls.SetChildIndex(this.fcLabel1, 0);
            this.ClientArea.Controls.SetChildIndex(this.fcLabel2, 0);
            this.ClientArea.Controls.SetChildIndex(this.fcLabel3, 0);
            this.ClientArea.Controls.SetChildIndex(this.fcLabel4, 0);
            this.ClientArea.Controls.SetChildIndex(this.fcLabel5, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtPrincipal, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtInterest, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtPreLienInterest, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtCosts, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblTotal, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(371, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.AutoSize = false;
            this.HeaderText.Size = new System.Drawing.Size(282, 28);
            this.HeaderText.Text = "Amounts to Correct";
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Location = new System.Drawing.Point(120, 26);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcess.Size = new System.Drawing.Size(120, 48);
            this.btnProcess.TabIndex = 5;
            this.btnProcess.Text = "Process";
            // 
            // fcLabel1
            // 
            this.fcLabel1.AutoSize = true;
            this.fcLabel1.Location = new System.Drawing.Point(30, 45);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(80, 17);
            this.fcLabel1.TabIndex = 14;
            this.fcLabel1.Text = "PRINCIPAL";
            // 
            // fcLabel2
            // 
            this.fcLabel2.AutoSize = true;
            this.fcLabel2.Location = new System.Drawing.Point(30, 95);
            this.fcLabel2.Name = "fcLabel2";
            this.fcLabel2.Size = new System.Drawing.Size(74, 17);
            this.fcLabel2.TabIndex = 2;
            this.fcLabel2.Text = "INTEREST";
            // 
            // fcLabel3
            // 
            this.fcLabel3.AutoSize = true;
            this.fcLabel3.Location = new System.Drawing.Point(30, 145);
            this.fcLabel3.Name = "fcLabel3";
            this.fcLabel3.Size = new System.Drawing.Size(140, 17);
            this.fcLabel3.TabIndex = 4;
            this.fcLabel3.Text = "PRE-LIEN INTEREST";
            // 
            // fcLabel4
            // 
            this.fcLabel4.AutoSize = true;
            this.fcLabel4.Location = new System.Drawing.Point(30, 195);
            this.fcLabel4.Name = "fcLabel4";
            this.fcLabel4.Size = new System.Drawing.Size(54, 17);
            this.fcLabel4.TabIndex = 6;
            this.fcLabel4.Text = "COSTS";
            // 
            // fcLabel5
            // 
            this.fcLabel5.AutoSize = true;
            this.fcLabel5.Location = new System.Drawing.Point(30, 245);
            this.fcLabel5.Name = "fcLabel5";
            this.fcLabel5.Size = new System.Drawing.Size(52, 17);
            this.fcLabel5.TabIndex = 8;
            this.fcLabel5.Text = "TOTAL";
            // 
            // txtPrincipal
            // 
            this.javaScript1.SetJavaScript(this.txtPrincipal, resources.GetString("txtPrincipal.JavaScript"));
            this.txtPrincipal.Location = new System.Drawing.Point(191, 31);
            this.txtPrincipal.Name = "txtPrincipal";
            this.txtPrincipal.Size = new System.Drawing.Size(140, 40);
            this.txtPrincipal.TabIndex = 1;
            // 
            // txtInterest
            // 
            this.javaScript1.SetJavaScript(this.txtInterest, resources.GetString("txtInterest.JavaScript"));
            this.txtInterest.Location = new System.Drawing.Point(191, 81);
            this.txtInterest.Name = "txtInterest";
            this.txtInterest.Size = new System.Drawing.Size(140, 40);
            this.txtInterest.TabIndex = 2;
            // 
            // txtPreLienInterest
            // 
            this.javaScript1.SetJavaScript(this.txtPreLienInterest, resources.GetString("txtPreLienInterest.JavaScript"));
            this.txtPreLienInterest.Location = new System.Drawing.Point(191, 131);
            this.txtPreLienInterest.Name = "txtPreLienInterest";
            this.txtPreLienInterest.Size = new System.Drawing.Size(140, 40);
            this.txtPreLienInterest.TabIndex = 3;
            // 
            // txtCosts
            // 
            this.javaScript1.SetJavaScript(this.txtCosts, resources.GetString("txtCosts.JavaScript"));
            this.txtCosts.Location = new System.Drawing.Point(191, 181);
            this.txtCosts.Name = "txtCosts";
            this.txtCosts.Size = new System.Drawing.Size(140, 40);
            this.txtCosts.TabIndex = 4;
            // 
            // lblTotal
            // 
            this.lblTotal.Location = new System.Drawing.Point(191, 242);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(121, 20);
            this.lblTotal.TabIndex = 13;
            // 
            // frmNonBudgetaryPayment
            // 
            this.ClientSize = new System.Drawing.Size(371, 475);
            this.Name = "frmNonBudgetaryPayment";
            this.Text = "Non-Budgetary Payment";
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private fecherFoundation.FCButton btnProcess;
        private fecherFoundation.FCLabel fcLabel5;
        private fecherFoundation.FCLabel fcLabel4;
        private fecherFoundation.FCLabel fcLabel3;
        private fecherFoundation.FCLabel fcLabel2;
        private fecherFoundation.FCLabel fcLabel1;
        private fecherFoundation.FCTextBox txtPrincipal;
        private Wisej.Web.JavaScript javaScript1;
        private fecherFoundation.FCTextBox txtCosts;
        private fecherFoundation.FCTextBox txtPreLienInterest;
        private fecherFoundation.FCTextBox txtInterest;
        public fecherFoundation.FCLabel lblTotal;
    }
}