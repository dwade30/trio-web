﻿namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	partial class rptTaxAccountDetail
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptTaxAccountDetail));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.fldYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldRef = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCosts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lnTotals = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtComments = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.fldPerDiem1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPerDiem1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPerDiemHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.fldEx1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblEx = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldEx2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldEx3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblWarningFooter = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalPrin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalCosts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtAsOf = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAcct = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDateHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblRef = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPer = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCode = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPrincipal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblInterest = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCosts = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblLocation = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMapLot = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAcreage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblLand = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBuilding = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblExempt = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblValuationTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblLandValue = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBuildingValue = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblExemptValue = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblValualtionValue = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblBookPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblRef1Val = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblRef1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldPerDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblMailingAddressTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMailingAddress = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAssessmentYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.fldYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRef)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCosts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtComments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPerDiem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPerDiem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPerDiemHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldEx1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEx)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldEx2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldEx3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWarningFooter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalPrin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalCosts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAsOf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAcct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDateHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRef)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCosts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMapLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAcreage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLand)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBuilding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExempt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblValuationTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLandValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBuildingValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExemptValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblValualtionValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBookPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRef1Val)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRef1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPerDue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMailingAddressTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanShrink = true;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldYear,
            this.fldDate,
            this.fldPer,
            this.fldCode,
            this.fldRef,
            this.fldTotal,
            this.fldName,
            this.fldPrincipal,
            this.fldInterest,
            this.fldCosts,
            this.lnTotals,
            this.txtComments});
            this.Detail.Height = 0.24F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // fldYear
            // 
            this.fldYear.Height = 0.1875F;
            this.fldYear.Left = 0F;
            this.fldYear.Name = "fldYear";
            this.fldYear.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldYear.Text = null;
            this.fldYear.Top = 0F;
            this.fldYear.Width = 1F;
            // 
            // fldDate
            // 
            this.fldDate.Height = 0.1875F;
            this.fldDate.Left = 1.0625F;
            this.fldDate.Name = "fldDate";
            this.fldDate.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.fldDate.Text = null;
            this.fldDate.Top = 0F;
            this.fldDate.Width = 0.875F;
            // 
            // fldPer
            // 
            this.fldPer.Height = 0.1875F;
            this.fldPer.Left = 3.0625F;
            this.fldPer.Name = "fldPer";
            this.fldPer.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.fldPer.Text = null;
            this.fldPer.Top = 0F;
            this.fldPer.Width = 0.1875F;
            // 
            // fldCode
            // 
            this.fldCode.Height = 0.1875F;
            this.fldCode.Left = 3.25F;
            this.fldCode.Name = "fldCode";
            this.fldCode.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.fldCode.Text = null;
            this.fldCode.Top = 0F;
            this.fldCode.Width = 0.1875F;
            // 
            // fldRef
            // 
            this.fldRef.Height = 0.1875F;
            this.fldRef.Left = 1.9375F;
            this.fldRef.Name = "fldRef";
            this.fldRef.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.fldRef.Text = null;
            this.fldRef.Top = 0F;
            this.fldRef.Width = 1.125F;
            // 
            // fldTotal
            // 
            this.fldTotal.Height = 0.1875F;
            this.fldTotal.Left = 6.4375F;
            this.fldTotal.Name = "fldTotal";
            this.fldTotal.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldTotal.Text = null;
            this.fldTotal.Top = 0F;
            this.fldTotal.Width = 1.0625F;
            // 
            // fldName
            // 
            this.fldName.Height = 0.1875F;
            this.fldName.Left = 3.062F;
            this.fldName.Name = "fldName";
            this.fldName.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.fldName.Text = null;
            this.fldName.Top = 0F;
            this.fldName.Width = 4.2505F;
            // 
            // fldPrincipal
            // 
            this.fldPrincipal.Height = 0.1875F;
            this.fldPrincipal.Left = 3.4375F;
            this.fldPrincipal.Name = "fldPrincipal";
            this.fldPrincipal.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldPrincipal.Text = null;
            this.fldPrincipal.Top = 0F;
            this.fldPrincipal.Width = 1F;
            // 
            // fldInterest
            // 
            this.fldInterest.Height = 0.1875F;
            this.fldInterest.Left = 4.4375F;
            this.fldInterest.Name = "fldInterest";
            this.fldInterest.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldInterest.Text = null;
            this.fldInterest.Top = 0F;
            this.fldInterest.Width = 1F;
            // 
            // fldCosts
            // 
            this.fldCosts.Height = 0.1875F;
            this.fldCosts.Left = 5.4375F;
            this.fldCosts.Name = "fldCosts";
            this.fldCosts.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldCosts.Text = null;
            this.fldCosts.Top = 0F;
            this.fldCosts.Width = 1F;
            // 
            // lnTotals
            // 
            this.lnTotals.Height = 0F;
            this.lnTotals.Left = 3.4375F;
            this.lnTotals.LineWeight = 1F;
            this.lnTotals.Name = "lnTotals";
            this.lnTotals.Top = 0F;
            this.lnTotals.Visible = false;
            this.lnTotals.Width = 4.0625F;
            this.lnTotals.X1 = 3.4375F;
            this.lnTotals.X2 = 7.5F;
            this.lnTotals.Y1 = 0F;
            this.lnTotals.Y2 = 0F;
            // 
            // txtComments
            // 
            this.txtComments.Height = 0.1875F;
            this.txtComments.Left = 3.063F;
            this.txtComments.Name = "txtComments";
            this.txtComments.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.txtComments.Text = null;
            this.txtComments.Top = 0.188F;
            this.txtComments.Width = 4.437F;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldPerDiem1,
            this.lblPerDiem1,
            this.lblPerDiemHeader,
            this.Line3,
            this.fldEx1,
            this.lblEx,
            this.fldEx2,
            this.fldEx3,
            this.lblWarningFooter,
            this.txtTotalTotal,
            this.txtTotalPrin,
            this.txtTotalInterest,
            this.txtTotalCosts,
            this.line4,
            this.txtAsOf});
            this.ReportFooter.Height = 3.177083F;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
            // 
            // fldPerDiem1
            // 
            this.fldPerDiem1.Height = 0.1875F;
            this.fldPerDiem1.Left = 2F;
            this.fldPerDiem1.Name = "fldPerDiem1";
            this.fldPerDiem1.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.fldPerDiem1.Text = null;
            this.fldPerDiem1.Top = 0.7180001F;
            this.fldPerDiem1.Width = 1F;
            // 
            // lblPerDiem1
            // 
            this.lblPerDiem1.Height = 0.1875F;
            this.lblPerDiem1.HyperLink = null;
            this.lblPerDiem1.Left = 0.375F;
            this.lblPerDiem1.Name = "lblPerDiem1";
            this.lblPerDiem1.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblPerDiem1.Text = null;
            this.lblPerDiem1.Top = 0.7180001F;
            this.lblPerDiem1.Width = 1.625F;
            // 
            // lblPerDiemHeader
            // 
            this.lblPerDiemHeader.Height = 0.1875F;
            this.lblPerDiemHeader.HyperLink = null;
            this.lblPerDiemHeader.Left = 0.375F;
            this.lblPerDiemHeader.Name = "lblPerDiemHeader";
            this.lblPerDiemHeader.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; ddo-char-set: 0";
            this.lblPerDiemHeader.Text = "Per Diem";
            this.lblPerDiemHeader.Top = 0.5305001F;
            this.lblPerDiemHeader.Width = 2.625F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 0.375F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0.7180001F;
            this.Line3.Width = 2.625F;
            this.Line3.X1 = 0.375F;
            this.Line3.X2 = 3F;
            this.Line3.Y1 = 0.7180001F;
            this.Line3.Y2 = 0.7180001F;
            // 
            // fldEx1
            // 
            this.fldEx1.Height = 0.1875F;
            this.fldEx1.Left = 4.9375F;
            this.fldEx1.Name = "fldEx1";
            this.fldEx1.Style = "font-family: \'Tahoma\'";
            this.fldEx1.Text = null;
            this.fldEx1.Top = 0.5305001F;
            this.fldEx1.Width = 2.125F;
            // 
            // lblEx
            // 
            this.lblEx.Height = 0.1875F;
            this.lblEx.HyperLink = null;
            this.lblEx.Left = 3.875F;
            this.lblEx.Name = "lblEx";
            this.lblEx.Style = "font-family: \'Tahoma\'";
            this.lblEx.Text = "Exempt Codes:";
            this.lblEx.Top = 0.5305001F;
            this.lblEx.Width = 1.125F;
            // 
            // fldEx2
            // 
            this.fldEx2.Height = 0.1875F;
            this.fldEx2.Left = 4.9375F;
            this.fldEx2.Name = "fldEx2";
            this.fldEx2.Style = "font-family: \'Tahoma\'";
            this.fldEx2.Text = null;
            this.fldEx2.Top = 0.7180001F;
            this.fldEx2.Width = 2.125F;
            // 
            // fldEx3
            // 
            this.fldEx3.Height = 0.1875F;
            this.fldEx3.Left = 4.9375F;
            this.fldEx3.Name = "fldEx3";
            this.fldEx3.Style = "font-family: \'Tahoma\'";
            this.fldEx3.Text = null;
            this.fldEx3.Top = 0.9054998F;
            this.fldEx3.Width = 2.125F;
            // 
            // lblWarningFooter
            // 
            this.lblWarningFooter.Height = 0.5625F;
            this.lblWarningFooter.HyperLink = null;
            this.lblWarningFooter.Left = 3.875F;
            this.lblWarningFooter.Name = "lblWarningFooter";
            this.lblWarningFooter.Style = "font-family: \'Tahoma\'";
            this.lblWarningFooter.Text = "Note:  Payments will be reflected as positive values and charges to the account w" +
    "ill be represented as negative values.";
            this.lblWarningFooter.Top = 1.1555F;
            this.lblWarningFooter.Width = 3.125F;
            // 
            // txtTotalTotal
            // 
            this.txtTotalTotal.Height = 0.1875F;
            this.txtTotalTotal.Left = 6.4365F;
            this.txtTotalTotal.Name = "txtTotalTotal";
            this.txtTotalTotal.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.txtTotalTotal.Text = null;
            this.txtTotalTotal.Top = 0.06700003F;
            this.txtTotalTotal.Width = 1.0625F;
            // 
            // txtTotalPrin
            // 
            this.txtTotalPrin.Height = 0.1875F;
            this.txtTotalPrin.Left = 3.4365F;
            this.txtTotalPrin.Name = "txtTotalPrin";
            this.txtTotalPrin.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.txtTotalPrin.Text = null;
            this.txtTotalPrin.Top = 0.06700003F;
            this.txtTotalPrin.Width = 1F;
            // 
            // txtTotalInterest
            // 
            this.txtTotalInterest.Height = 0.1875F;
            this.txtTotalInterest.Left = 4.436501F;
            this.txtTotalInterest.Name = "txtTotalInterest";
            this.txtTotalInterest.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.txtTotalInterest.Text = null;
            this.txtTotalInterest.Top = 0.06700003F;
            this.txtTotalInterest.Width = 1F;
            // 
            // txtTotalCosts
            // 
            this.txtTotalCosts.Height = 0.1875F;
            this.txtTotalCosts.Left = 5.436501F;
            this.txtTotalCosts.Name = "txtTotalCosts";
            this.txtTotalCosts.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.txtTotalCosts.Text = null;
            this.txtTotalCosts.Top = 0.06700003F;
            this.txtTotalCosts.Width = 1F;
            // 
            // line4
            // 
            this.line4.Height = 0F;
            this.line4.Left = 3.4365F;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Top = 0.06700003F;
            this.line4.Width = 4.0625F;
            this.line4.X1 = 3.4365F;
            this.line4.X2 = 7.499F;
            this.line4.Y1 = 0.06700003F;
            this.line4.Y2 = 0.06700003F;
            // 
            // txtAsOf
            // 
            this.txtAsOf.Height = 0.1875F;
            this.txtAsOf.Left = 0F;
            this.txtAsOf.Name = "txtAsOf";
            this.txtAsOf.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.txtAsOf.Text = null;
            this.txtAsOf.Top = 0.067F;
            this.txtAsOf.Width = 3.25F;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblHeader,
            this.lblMuniName,
            this.lblTime,
            this.lblDate,
            this.lblPage,
            this.lblAcct,
            this.lblDateHeader,
            this.lblRef,
            this.lblPer,
            this.lblCode,
            this.lblPrincipal,
            this.lblInterest,
            this.lblCosts,
            this.lblTotal,
            this.Line1,
            this.lblName,
            this.lblLocation,
            this.lblMapLot,
            this.lblAcreage,
            this.lblLand,
            this.lblBuilding,
            this.lblExempt,
            this.lblValuationTotal,
            this.lblLandValue,
            this.lblBuildingValue,
            this.lblExemptValue,
            this.lblValualtionValue,
            this.Line2,
            this.lblBookPage,
            this.lblRef1Val,
            this.lblRef1,
            this.fldPerDue,
            this.lblMailingAddressTitle,
            this.lblMailingAddress,
            this.lblAssessmentYear});
            this.PageHeader.Height = 2.490083F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            // 
            // lblHeader
            // 
            this.lblHeader.Height = 0.4F;
            this.lblHeader.HyperLink = null;
            this.lblHeader.Left = 0F;
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" +
    "o-char-set: 0";
            this.lblHeader.Text = "Account Detail";
            this.lblHeader.Top = 0F;
            this.lblHeader.Width = 7.5F;
            // 
            // lblMuniName
            // 
            this.lblMuniName.Height = 0.1875F;
            this.lblMuniName.HyperLink = null;
            this.lblMuniName.Left = 0F;
            this.lblMuniName.Name = "lblMuniName";
            this.lblMuniName.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblMuniName.Text = null;
            this.lblMuniName.Top = 0F;
            this.lblMuniName.Width = 2.75F;
            // 
            // lblTime
            // 
            this.lblTime.Height = 0.1875F;
            this.lblTime.HyperLink = null;
            this.lblTime.Left = 0F;
            this.lblTime.Name = "lblTime";
            this.lblTime.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblTime.Text = null;
            this.lblTime.Top = 0.1875F;
            this.lblTime.Width = 1.25F;
            // 
            // lblDate
            // 
            this.lblDate.Height = 0.1875F;
            this.lblDate.HyperLink = null;
            this.lblDate.Left = 5.8125F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.lblDate.Text = null;
            this.lblDate.Top = 0F;
            this.lblDate.Width = 1.6875F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1875F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 5.8125F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.lblPage.Text = null;
            this.lblPage.Top = 0.1875F;
            this.lblPage.Width = 1.6875F;
            // 
            // lblAcct
            // 
            this.lblAcct.Height = 0.1875F;
            this.lblAcct.HyperLink = null;
            this.lblAcct.Left = 0F;
            this.lblAcct.Name = "lblAcct";
            this.lblAcct.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.lblAcct.Text = "Year";
            this.lblAcct.Top = 2.25F;
            this.lblAcct.Width = 1F;
            // 
            // lblDateHeader
            // 
            this.lblDateHeader.Height = 0.1875F;
            this.lblDateHeader.HyperLink = null;
            this.lblDateHeader.Left = 1.0625F;
            this.lblDateHeader.Name = "lblDateHeader";
            this.lblDateHeader.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
            this.lblDateHeader.Text = "Date";
            this.lblDateHeader.Top = 2.25F;
            this.lblDateHeader.Width = 0.8125F;
            // 
            // lblRef
            // 
            this.lblRef.Height = 0.1875F;
            this.lblRef.HyperLink = null;
            this.lblRef.Left = 1.9375F;
            this.lblRef.Name = "lblRef";
            this.lblRef.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
            this.lblRef.Text = "Reference";
            this.lblRef.Top = 2.25F;
            this.lblRef.Width = 1.125F;
            // 
            // lblPer
            // 
            this.lblPer.Height = 0.1875F;
            this.lblPer.HyperLink = null;
            this.lblPer.Left = 3.0625F;
            this.lblPer.Name = "lblPer";
            this.lblPer.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
            this.lblPer.Text = "P";
            this.lblPer.Top = 2.25F;
            this.lblPer.Width = 0.1875F;
            // 
            // lblCode
            // 
            this.lblCode.Height = 0.1875F;
            this.lblCode.HyperLink = null;
            this.lblCode.Left = 3.25F;
            this.lblCode.Name = "lblCode";
            this.lblCode.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
            this.lblCode.Text = "C";
            this.lblCode.Top = 2.25F;
            this.lblCode.Width = 0.1875F;
            // 
            // lblPrincipal
            // 
            this.lblPrincipal.Height = 0.1875F;
            this.lblPrincipal.HyperLink = null;
            this.lblPrincipal.Left = 3.4375F;
            this.lblPrincipal.Name = "lblPrincipal";
            this.lblPrincipal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.lblPrincipal.Text = "Principal";
            this.lblPrincipal.Top = 2.25F;
            this.lblPrincipal.Width = 1F;
            // 
            // lblInterest
            // 
            this.lblInterest.Height = 0.1875F;
            this.lblInterest.HyperLink = null;
            this.lblInterest.Left = 4.4375F;
            this.lblInterest.Name = "lblInterest";
            this.lblInterest.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.lblInterest.Text = "Interest";
            this.lblInterest.Top = 2.25F;
            this.lblInterest.Width = 1F;
            // 
            // lblCosts
            // 
            this.lblCosts.Height = 0.1875F;
            this.lblCosts.HyperLink = null;
            this.lblCosts.Left = 5.4375F;
            this.lblCosts.Name = "lblCosts";
            this.lblCosts.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.lblCosts.Text = "Costs";
            this.lblCosts.Top = 2.25F;
            this.lblCosts.Width = 1F;
            // 
            // lblTotal
            // 
            this.lblTotal.Height = 0.1875F;
            this.lblTotal.HyperLink = null;
            this.lblTotal.Left = 6.4375F;
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.lblTotal.Text = "Total";
            this.lblTotal.Top = 2.25F;
            this.lblTotal.Width = 1.0625F;
            // 
            // Line1
            // 
            this.Line1.Height = 0.0004999638F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 2.437F;
            this.Line1.Width = 7.5F;
            this.Line1.X1 = 7.5F;
            this.Line1.X2 = 0F;
            this.Line1.Y1 = 2.437F;
            this.Line1.Y2 = 2.4375F;
            // 
            // lblName
            // 
            this.lblName.Height = 0.3125F;
            this.lblName.HyperLink = null;
            this.lblName.Left = 0.4375F;
            this.lblName.Name = "lblName";
            this.lblName.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblName.Text = null;
            this.lblName.Top = 0.4375F;
            this.lblName.Width = 3.75F;
            // 
            // lblLocation
            // 
            this.lblLocation.Height = 0.1875F;
            this.lblLocation.HyperLink = null;
            this.lblLocation.Left = 0.4375F;
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblLocation.Text = null;
            this.lblLocation.Top = 0.75F;
            this.lblLocation.Width = 3.75F;
            // 
            // lblMapLot
            // 
            this.lblMapLot.Height = 0.1875F;
            this.lblMapLot.HyperLink = null;
            this.lblMapLot.Left = 1.4375F;
            this.lblMapLot.Name = "lblMapLot";
            this.lblMapLot.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblMapLot.Text = null;
            this.lblMapLot.Top = 0.9375F;
            this.lblMapLot.Width = 2.75F;
            // 
            // lblAcreage
            // 
            this.lblAcreage.Height = 0.1875F;
            this.lblAcreage.HyperLink = null;
            this.lblAcreage.Left = 0.4375F;
            this.lblAcreage.Name = "lblAcreage";
            this.lblAcreage.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblAcreage.Text = null;
            this.lblAcreage.Top = 0.9375F;
            this.lblAcreage.Width = 1F;
            // 
            // lblLand
            // 
            this.lblLand.Height = 0.1875F;
            this.lblLand.HyperLink = null;
            this.lblLand.Left = 4.625F;
            this.lblLand.Name = "lblLand";
            this.lblLand.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblLand.Text = "Land:";
            this.lblLand.Top = 0.4375F;
            this.lblLand.Width = 1F;
            // 
            // lblBuilding
            // 
            this.lblBuilding.Height = 0.1875F;
            this.lblBuilding.HyperLink = null;
            this.lblBuilding.Left = 4.625F;
            this.lblBuilding.Name = "lblBuilding";
            this.lblBuilding.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblBuilding.Text = "Building:";
            this.lblBuilding.Top = 0.625F;
            this.lblBuilding.Width = 1F;
            // 
            // lblExempt
            // 
            this.lblExempt.Height = 0.1875F;
            this.lblExempt.HyperLink = null;
            this.lblExempt.Left = 4.625F;
            this.lblExempt.Name = "lblExempt";
            this.lblExempt.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblExempt.Text = "Exempt";
            this.lblExempt.Top = 0.8125F;
            this.lblExempt.Width = 1F;
            // 
            // lblValuationTotal
            // 
            this.lblValuationTotal.Height = 0.1875F;
            this.lblValuationTotal.HyperLink = null;
            this.lblValuationTotal.Left = 4.625F;
            this.lblValuationTotal.Name = "lblValuationTotal";
            this.lblValuationTotal.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblValuationTotal.Text = "Total:";
            this.lblValuationTotal.Top = 1F;
            this.lblValuationTotal.Width = 1F;
            // 
            // lblLandValue
            // 
            this.lblLandValue.Height = 0.1875F;
            this.lblLandValue.HyperLink = null;
            this.lblLandValue.Left = 5.625F;
            this.lblLandValue.Name = "lblLandValue";
            this.lblLandValue.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.lblLandValue.Text = null;
            this.lblLandValue.Top = 0.4375F;
            this.lblLandValue.Width = 1F;
            // 
            // lblBuildingValue
            // 
            this.lblBuildingValue.Height = 0.1875F;
            this.lblBuildingValue.HyperLink = null;
            this.lblBuildingValue.Left = 5.625F;
            this.lblBuildingValue.Name = "lblBuildingValue";
            this.lblBuildingValue.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.lblBuildingValue.Text = null;
            this.lblBuildingValue.Top = 0.625F;
            this.lblBuildingValue.Width = 1F;
            // 
            // lblExemptValue
            // 
            this.lblExemptValue.Height = 0.1875F;
            this.lblExemptValue.HyperLink = null;
            this.lblExemptValue.Left = 5.625F;
            this.lblExemptValue.Name = "lblExemptValue";
            this.lblExemptValue.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.lblExemptValue.Text = null;
            this.lblExemptValue.Top = 0.8125F;
            this.lblExemptValue.Width = 1F;
            // 
            // lblValualtionValue
            // 
            this.lblValualtionValue.Height = 0.1875F;
            this.lblValualtionValue.HyperLink = null;
            this.lblValualtionValue.Left = 5.625F;
            this.lblValualtionValue.Name = "lblValualtionValue";
            this.lblValualtionValue.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.lblValualtionValue.Text = null;
            this.lblValualtionValue.Top = 1F;
            this.lblValualtionValue.Width = 1F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 4.625F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 1F;
            this.Line2.Width = 2F;
            this.Line2.X1 = 4.625F;
            this.Line2.X2 = 6.625F;
            this.Line2.Y1 = 1F;
            this.Line2.Y2 = 1F;
            // 
            // lblBookPage
            // 
            this.lblBookPage.Height = 0.3125F;
            this.lblBookPage.HyperLink = null;
            this.lblBookPage.Left = 0.4375F;
            this.lblBookPage.Name = "lblBookPage";
            this.lblBookPage.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblBookPage.Text = null;
            this.lblBookPage.Top = 1.125F;
            this.lblBookPage.Width = 3.75F;
            // 
            // lblRef1Val
            // 
            this.lblRef1Val.Height = 0.1875F;
            this.lblRef1Val.HyperLink = null;
            this.lblRef1Val.Left = 5F;
            this.lblRef1Val.Name = "lblRef1Val";
            this.lblRef1Val.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblRef1Val.Text = null;
            this.lblRef1Val.Top = 1.25F;
            this.lblRef1Val.Width = 1.75F;
            // 
            // lblRef1
            // 
            this.lblRef1.Height = 0.1875F;
            this.lblRef1.HyperLink = null;
            this.lblRef1.Left = 4.3125F;
            this.lblRef1.Name = "lblRef1";
            this.lblRef1.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblRef1.Text = null;
            this.lblRef1.Top = 1.25F;
            this.lblRef1.Width = 0.6875F;
            // 
            // fldPerDue
            // 
            this.fldPerDue.Height = 0.875F;
            this.fldPerDue.Left = 0.4375F;
            this.fldPerDue.Name = "fldPerDue";
            this.fldPerDue.Style = "font-family: \'Tahoma\'";
            this.fldPerDue.Text = null;
            this.fldPerDue.Top = 1.4375F;
            this.fldPerDue.Width = 3.75F;
            // 
            // lblMailingAddressTitle
            // 
            this.lblMailingAddressTitle.Height = 0.375F;
            this.lblMailingAddressTitle.HyperLink = null;
            this.lblMailingAddressTitle.Left = 4.3125F;
            this.lblMailingAddressTitle.Name = "lblMailingAddressTitle";
            this.lblMailingAddressTitle.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblMailingAddressTitle.Text = null;
            this.lblMailingAddressTitle.Top = 1.4375F;
            this.lblMailingAddressTitle.Width = 0.6875F;
            // 
            // lblMailingAddress
            // 
            this.lblMailingAddress.Height = 0.8125F;
            this.lblMailingAddress.HyperLink = null;
            this.lblMailingAddress.Left = 5F;
            this.lblMailingAddress.Name = "lblMailingAddress";
            this.lblMailingAddress.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblMailingAddress.Text = null;
            this.lblMailingAddress.Top = 1.4375F;
            this.lblMailingAddress.Width = 2.5F;
            // 
            // lblAssessmentYear
            // 
            this.lblAssessmentYear.Height = 0.1875F;
            this.lblAssessmentYear.HyperLink = null;
            this.lblAssessmentYear.Left = 6.625F;
            this.lblAssessmentYear.Name = "lblAssessmentYear";
            this.lblAssessmentYear.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.lblAssessmentYear.Text = null;
            this.lblAssessmentYear.Top = 0.4375F;
            this.lblAssessmentYear.Width = 0.75F;
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // rptTaxAccountDetail
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.25F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.25F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.5F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.fldYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRef)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCosts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtComments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPerDiem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPerDiem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPerDiemHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldEx1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEx)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldEx2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldEx3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWarningFooter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalPrin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalCosts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAsOf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAcct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDateHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRef)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCosts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMapLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAcreage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLand)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBuilding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExempt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblValuationTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLandValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBuildingValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExemptValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblValualtionValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBookPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRef1Val)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRef1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPerDue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMailingAddressTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAssessmentYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPer;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRef;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInterest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCosts;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnTotals;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPerDiem1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPerDiem1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPerDiemHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEx1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblEx;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEx2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEx3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblWarningFooter;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAcct;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDateHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRef;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPer;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblInterest;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCosts;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLocation;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAcreage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLand;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBuilding;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblExempt;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblValuationTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLandValue;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBuildingValue;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblExemptValue;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblValualtionValue;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBookPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRef1Val;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRef1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPerDue;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMailingAddressTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMailingAddress;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAssessmentYear;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtComments;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalTotal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalPrin;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalInterest;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalCosts;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAsOf;
    }
}
