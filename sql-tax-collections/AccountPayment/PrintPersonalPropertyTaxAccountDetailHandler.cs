﻿using System.Linq;
using SharedApplication;
using SharedApplication.Messaging;
using SharedApplication.PersonalProperty;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.AccountPayment;
using SharedApplication.TaxCollections.Enums;

namespace TWCL0000.AccountPayment
{
    public class PrintPersonalPropertyTaxAccountDetailHandler : CommandHandler<PrintPersonalPropertyTaxAccountDetail>
    {
        private IView<ITaxAccountDetailReportModel> reportView;
        private GlobalTaxCollectionSettings taxSettings;
        private DataEnvironmentSettings environmentSettings;
        private IPersonalPropertyContext ppContext;
        public PrintPersonalPropertyTaxAccountDetailHandler(IView<ITaxAccountDetailReportModel> reportView,
            GlobalTaxCollectionSettings taxSettings, DataEnvironmentSettings environmentSettings, IPersonalPropertyContext ppContext)
        {
            this.reportView = reportView;
            this.taxSettings = taxSettings;
            this.environmentSettings = environmentSettings;
            this.ppContext = ppContext;
        }

        protected override void Handle(PrintPersonalPropertyTaxAccountDetail command)
        {
            var accountView = ppContext.PPAccountPartyAddressViews.FirstOrDefault(p => p.Account == command.Account);
            if (accountView != null)
            {
                reportView.ViewModel.MailingAddress.Address1 = accountView.Address1;
                reportView.ViewModel.MailingAddress.Address2 = accountView.Address2;
                reportView.ViewModel.MailingAddress.Address3 = accountView.Address3;
                reportView.ViewModel.MailingAddress.City = accountView.City;
                reportView.ViewModel.MailingAddress.State = accountView.State;
                reportView.ViewModel.MailingAddress.Zip = accountView.Zip;

                if (accountView.ExemptCode1.GetValueOrDefault() > 0)
                {
                    reportView.ViewModel.ExemptionCodes.Add(accountView.ExemptCode1.GetValueOrDefault().ToString());
                }

                if (accountView.ExemptCode2.GetValueOrDefault() > 0)
                {
                    reportView.ViewModel.ExemptionCodes.Add(accountView.ExemptCode2.GetValueOrDefault().ToString());
                }
            }
            reportView.ViewModel.Account = command.Account;
            foreach (var keyPair in command.CalculationSummaries)
            {
                reportView.ViewModel.CalculationSummaries.Add(keyPair.Key, keyPair.Value);
            }

            reportView.ViewModel.MuniName = environmentSettings.ClientName;
            reportView.ViewModel.AccountDetailAssessmentSetting = taxSettings.AccountDetailAssessmentSetting;
            reportView.ViewModel.ShowReceiptNumber = taxSettings.ShowReceiptNumberOnDetail;
            reportView.ViewModel.AccountSummary = new TaxBillCalculationSummary(command.Account, command.BillType);
            reportView.ViewModel.AccountSummary.AddTo(command.AccountSummary);
            reportView.ViewModel.BillType = command.BillType;
            reportView.ViewModel.EffectiveDate = command.EffectiveDate;
            reportView.ViewModel.PPTaxAccount = command.PPTaxAccount;
            reportView.ViewModel.RETaxAccount = null;
            reportView.ViewModel.ExpandedYearBills = command.ExpandedYearBills;
            reportView.Show();
        }
    }
}