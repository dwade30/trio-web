﻿namespace TWCL0000.AccountPayment
{
    partial class frmTaxPaymentInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RightPanel = new Wisej.Web.FlexLayoutPanel();
            this.LeftPanel = new Wisej.Web.FlexLayoutPanel();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 512);
            this.BottomPanel.Size = new System.Drawing.Size(686, 0);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.RightPanel);
            this.ClientArea.Controls.Add(this.LeftPanel);
            this.ClientArea.Location = new System.Drawing.Point(0, 0);
            this.ClientArea.Size = new System.Drawing.Size(706, 526);
            this.ClientArea.Controls.SetChildIndex(this.LeftPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.RightPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(706, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // RightPanel
            // 
            this.RightPanel.LayoutStyle = Wisej.Web.FlexLayoutStyle.Vertical;
            this.RightPanel.Location = new System.Drawing.Point(361, 26);
            this.RightPanel.Name = "RightPanel";
            this.RightPanel.Size = new System.Drawing.Size(320, 486);
            this.RightPanel.TabIndex = 6;
            this.RightPanel.TabStop = true;
            // 
            // LeftPanel
            // 
            this.LeftPanel.LayoutStyle = Wisej.Web.FlexLayoutStyle.Vertical;
            this.LeftPanel.Location = new System.Drawing.Point(21, 26);
            this.LeftPanel.Name = "LeftPanel";
            this.LeftPanel.Size = new System.Drawing.Size(320, 486);
            this.LeftPanel.TabIndex = 5;
            this.LeftPanel.TabStop = true;
            // 
            // frmTaxPaymentInfo
            // 
            this.ClientSize = new System.Drawing.Size(706, 526);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmTaxPaymentInfo";
            this.Text = "Payment Information";
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Wisej.Web.FlexLayoutPanel RightPanel;
        private Wisej.Web.FlexLayoutPanel LeftPanel;
    }
}