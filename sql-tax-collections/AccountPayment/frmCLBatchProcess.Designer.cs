﻿namespace TWCL0000.AccountPayment
{
    partial class frmCLBatchProcess
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private fecherFoundation.FCMenuStrip MainMenu1;
        public fecherFoundation.FCToolStripMenuItem mnuProcess;
        public fecherFoundation.FCToolStripMenuItem mnuBatch;

        public fecherFoundation.FCToolStripMenuItem mnuBatchPrint;
        public fecherFoundation.FCToolStripMenuItem mnuFileImport;
        public fecherFoundation.FCToolStripMenuItem mnuImportFirstAmericanBatch;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCLBatchProcess));
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuBatchPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileImport = new fecherFoundation.FCToolStripMenuItem();
            this.mnuImportFirstAmericanBatch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuBatch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.lblPeriod1 = new fecherFoundation.FCLabel();
            this.cmbPeriod1 = new fecherFoundation.FCComboBox();
            this.txtAmount = new fecherFoundation.FCTextBox();
            this.txtBatchAccount = new fecherFoundation.FCTextBox();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.vsBatch = new fecherFoundation.FCGrid();
            this.txtTotal = new fecherFoundation.FCTextBox();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.javaScript1 = new Wisej.Web.JavaScript(this.components);
            this.lblName = new fecherFoundation.FCLabel();
            this.lblLocation = new fecherFoundation.FCLabel();
            this.btnSaveExit = new fecherFoundation.FCButton();
            this.btnSave = new fecherFoundation.FCButton();
            this.btnSearch = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsBatch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSaveExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearch)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnSaveExit);
            this.BottomPanel.Location = new System.Drawing.Point(0, 490);
            this.BottomPanel.Size = new System.Drawing.Size(1058, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.lblName);
            this.ClientArea.Controls.Add(this.lblLocation);
            this.ClientArea.Controls.Add(this.txtTotal);
            this.ClientArea.Controls.Add(this.fcLabel1);
            this.ClientArea.Controls.Add(this.vsBatch);
            this.ClientArea.Controls.Add(this.lblPeriod1);
            this.ClientArea.Controls.Add(this.cmbPeriod1);
            this.ClientArea.Controls.Add(this.txtAmount);
            this.ClientArea.Controls.Add(this.txtBatchAccount);
            this.ClientArea.Controls.Add(this.Label10);
            this.ClientArea.Controls.Add(this.Label9);
            this.ClientArea.Size = new System.Drawing.Size(1058, 430);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.btnSearch);
            this.TopPanel.Controls.Add(this.btnSave);
            this.TopPanel.Size = new System.Drawing.Size(1058, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnSave, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnSearch, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(136, 30);
            this.HeaderText.Text = "Batch Input";
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuBatchPrint,
            this.mnuFileImport,
            this.mnuImportFirstAmericanBatch});
            this.MainMenu1.Name = null;
            // 
            // mnuBatchPrint
            // 
            this.mnuBatchPrint.Index = 1;
            this.mnuBatchPrint.Name = "mnuBatchPrint";
            this.mnuBatchPrint.Text = "Print Current Batch Listing";
            // 
            // mnuFileImport
            // 
            this.mnuFileImport.Enabled = false;
            this.mnuFileImport.Index = 2;
            this.mnuFileImport.Name = "mnuFileImport";
            this.mnuFileImport.Text = "Import Batch File";
            // 
            // mnuImportFirstAmericanBatch
            // 
            this.mnuImportFirstAmericanBatch.Enabled = false;
            this.mnuImportFirstAmericanBatch.Index = 3;
            this.mnuImportFirstAmericanBatch.Name = "mnuImportFirstAmericanBatch";
            this.mnuImportFirstAmericanBatch.Text = "Import CoreLogic Batch File";
            // 
            // mnuBatch
            // 
            this.mnuBatch.Index = -1;
            this.mnuBatch.Name = "mnuBatch";
            this.mnuBatch.Text = "";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "";
            // 
            // lblPeriod1
            // 
            this.lblPeriod1.AutoSize = true;
            this.lblPeriod1.Location = new System.Drawing.Point(489, 35);
            this.lblPeriod1.Name = "lblPeriod1";
            this.lblPeriod1.Size = new System.Drawing.Size(59, 17);
            this.lblPeriod1.TabIndex = 33;
            this.lblPeriod1.Text = "PERIOD";
            this.lblPeriod1.Visible = false;
            // 
            // cmbPeriod1
            // 
            this.cmbPeriod1.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
            this.cmbPeriod1.Location = new System.Drawing.Point(568, 21);
            this.cmbPeriod1.Name = "cmbPeriod1";
            this.cmbPeriod1.Size = new System.Drawing.Size(121, 40);
            this.cmbPeriod1.TabIndex = 34;
            this.cmbPeriod1.Visible = false;
            // 
            // txtAmount
            // 
            this.txtAmount.BackColor = System.Drawing.SystemColors.Window;
            this.javaScript1.SetJavaScript(this.txtAmount, resources.GetString("txtAmount.JavaScript"));
            this.txtAmount.Location = new System.Drawing.Point(340, 21);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(121, 40);
            this.txtAmount.TabIndex = 32;
            // 
            // txtBatchAccount
            // 
            this.txtBatchAccount.BackColor = System.Drawing.SystemColors.Window;
            this.txtBatchAccount.Location = new System.Drawing.Point(116, 21);
            this.txtBatchAccount.Name = "txtBatchAccount";
            this.txtBatchAccount.Size = new System.Drawing.Size(121, 40);
            this.txtBatchAccount.TabIndex = 30;
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(255, 35);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(62, 20);
            this.Label10.TabIndex = 31;
            this.Label10.Text = "AMOUNT";
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(22, 35);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(69, 20);
            this.Label9.TabIndex = 35;
            this.Label9.Text = "ACCOUNT";
            // 
            // vsBatch
            // 
            this.vsBatch.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsBatch.Cols = 10;
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.vsBatch.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vsBatch.FixedCols = 0;
            this.vsBatch.ForeColorFixed = System.Drawing.SystemColors.ControlText;
            this.vsBatch.Location = new System.Drawing.Point(21, 83);
            this.vsBatch.Name = "vsBatch";
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.vsBatch.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.vsBatch.RowHeadersVisible = false;
            this.vsBatch.Rows = 1;
            this.vsBatch.ShowFocusCell = false;
            this.vsBatch.Size = new System.Drawing.Size(988, 339);
            this.vsBatch.TabIndex = 36;
            // 
            // txtTotal
            // 
            this.txtTotal.BackColor = System.Drawing.SystemColors.Window;
            this.txtTotal.Location = new System.Drawing.Point(813, 445);
            this.txtTotal.LockedOriginal = true;
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.ReadOnly = true;
            this.txtTotal.Size = new System.Drawing.Size(121, 40);
            this.txtTotal.TabIndex = 38;
            // 
            // fcLabel1
            // 
            this.fcLabel1.Location = new System.Drawing.Point(728, 459);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(62, 20);
            this.fcLabel1.TabIndex = 37;
            this.fcLabel1.Text = "TOTAL";
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(489, 21);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(418, 18);
            this.lblName.TabIndex = 39;
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLocation
            // 
            this.lblLocation.Location = new System.Drawing.Point(489, 49);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(418, 18);
            this.lblLocation.TabIndex = 40;
            this.lblLocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnSaveExit
            // 
            this.btnSaveExit.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.btnSaveExit.AppearanceKey = "acceptButton";
            this.btnSaveExit.Location = new System.Drawing.Point(424, 30);
            this.btnSaveExit.Name = "btnSaveExit";
            this.btnSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnSaveExit.Size = new System.Drawing.Size(211, 48);
            this.btnSaveExit.TabIndex = 4;
            this.btnSaveExit.Text = "Save & Continue";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(927, 26);
            this.btnSave.Name = "btnSave";
            this.btnSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.btnSave.Size = new System.Drawing.Size(110, 24);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Save Batch";
            this.btnSave.Visible = false;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(785, 26);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Shortcut = Wisej.Web.Shortcut.F6;
            this.btnSearch.Size = new System.Drawing.Size(110, 24);
            this.btnSearch.TabIndex = 4;
            this.btnSearch.Text = "Search";
            // 
            // frmCLBatchProcess
            // 
            this.ClientSize = new System.Drawing.Size(1058, 598);
            this.Cursor = Wisej.Web.Cursors.Default;
            this.Menu = this.MainMenu1;
            this.Name = "frmCLBatchProcess";
            this.Text = "Batch Input";
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsBatch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSaveExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearch)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private fecherFoundation.FCLabel lblPeriod1;
        public fecherFoundation.FCComboBox cmbPeriod1;
        public fecherFoundation.FCTextBox txtAmount;
        public fecherFoundation.FCTextBox txtBatchAccount;
        public fecherFoundation.FCLabel Label10;
        public fecherFoundation.FCLabel Label9;
        public fecherFoundation.FCGrid vsBatch;
        public fecherFoundation.FCTextBox txtTotal;
        public fecherFoundation.FCLabel fcLabel1;
        private Wisej.Web.JavaScript javaScript1;
        public fecherFoundation.FCLabel lblName;
        public fecherFoundation.FCLabel lblLocation;
        private fecherFoundation.FCButton btnSaveExit;
        private fecherFoundation.FCButton btnSave;
        private fecherFoundation.FCButton btnSearch;
    }
}