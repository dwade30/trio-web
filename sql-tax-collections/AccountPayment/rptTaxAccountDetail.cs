﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using fecherFoundation;
using SharedApplication;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.AccountPayment;
using SharedApplication.TaxCollections.Enums;
using TWSharedLibrary;
using Wisej.Web;

#if TWCL0000
using TWCL0000;


#elif TWBD0000
using TWBD0000;


#else
using TWCR0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptTaxAccountDetail : BaseSectionReport, IView<ITaxAccountDetailReportModel>
	{
		int lngRow;
		int lngPDNumber;
		int lngPDTop;
		decimal dblPDTotal;
		bool boolPerDiem;
		string strLastYearText = "";
		int lngAcctNum;
		bool boolShowCurrentAssessment;
      
        private IEnumerable<TaxAccountDetailLine> detailLines;
		//clsDRWrapper rsRK = new clsDRWrapper();
        private IEnumerator<TaxAccountDetailLine> detailEnumerator;
		public rptTaxAccountDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();           
        }

        public rptTaxAccountDetail(ITaxAccountDetailReportModel viewModel) : this()
        {
            ViewModel = viewModel;
        }
		private void InitializeComponentEx()
		{
			this.Name = "Account Detail";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
            if (!detailEnumerator.MoveNext())
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
            {
                detailLines = ViewModel.CreateDetailLines();
                detailEnumerator = detailLines.GetEnumerator();

				if (ViewModel.BillType == PropertyTaxBillType.Real)
				{ 
                    switch (ViewModel.AccountDetailAssessmentSetting)
                    {
                        case "C":
                            boolShowCurrentAssessment = true;
                            break;
                        case "B":
                            boolShowCurrentAssessment = false;
                            break;
                        default:
                            boolShowCurrentAssessment = FCMessageBox.Show("Would you like to show the current assessment as it is in the Real Estate Database?", MsgBoxStyle.YesNo | MsgBoxStyle.Question, "Show Assessments?") == DialogResult.Yes;
                            break;
                    }					
				}
				else
				{
					boolShowCurrentAssessment = FCMessageBox.Show("Would you like to show the current assessment as it is in the Personal Property database?", MsgBoxStyle.YesNo | MsgBoxStyle.Question, "Show Current?") == DialogResult.Yes;
				}
				SetHeaderString();

				boolPerDiem = false;
				// MAL@20080918 ; Tracker Reference: 11810
				if (ViewModel.ShowReceiptNumber)
				{
					lblAcct.Text = "Year/Rec #";
				}
				else
				{
					lblAcct.Text = "Year";
				}
				lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
				lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				lblMuniName.Text = ViewModel.MuniName;
				CreatePerDiemTable();
				CreatePerDiemField();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MsgBoxStyle.Critical, "Error Starting Account Detail");
			}
		}

		private void Detail_Format(object sender, EventArgs e)
        { 
			BindFields();            
        }

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
			// this will print the row from the grid
            var currentLine = detailEnumerator.Current;
            if (currentLine != null)
            {
                lnTotals.Visible = false;
                fldYear.Text = "";
                fldRef.Text = "";
                fldDate.Text = "";
                fldName.Text = "";
                fldPrincipal.Text = "";
                fldPer.Text = "";
                fldCosts.Text = "";
                fldCode.Text = "";
                fldTotal.Text = "";
                fldInterest.Text = "";
                txtComments.Text = "";
                txtComments.Visible = false;
                Detail.Height = .24F;
                switch (currentLine.LineType)
                {
                    case TaxAccountDetailLineType.Header:
                        fldYear.Text = FormatYearBill(currentLine.YearBill) + " " + currentLine.BillCategory;
                        fldRef.Text = currentLine.Reference;
                        if (currentLine.RateId > 0)
                        {
                            fldRef.Text = "Original";
                            fldDate.Text = currentLine.EffectiveDate.FormatAndPadShortDate();
                        }
                        else
                        {
                            fldDate.Text = "No RK";
                        }

                        fldPrincipal.Text = currentLine.Principal.FormatAsCurrencyNoSymbol();
                        fldTotal.Text = currentLine.Total.FormatAsCurrencyNoSymbol();
                        fldCosts.Text = currentLine.Costs.FormatAsCurrencyNoSymbol();
                        fldInterest.Text = currentLine.Interest.FormatAsCurrencyNoSymbol();
                        fldName.Text = "";
                        break;
                    case TaxAccountDetailLineType.LienHeader:
                        fldName.Text = "";
                        fldRef.Text = currentLine.Reference;
                        fldPrincipal.Text = currentLine.Principal.FormatAsCurrencyNoSymbol();
                        fldCosts.Text = currentLine.Costs.FormatAsCurrencyNoSymbol();
                        fldInterest.Text = currentLine.Interest.FormatAsCurrencyNoSymbol();
                        fldDate.Text = currentLine.EffectiveDate.FormatAndPadShortDate();
                        fldCode.Text = "";
                        fldPer.Text = "";
                        fldYear.Text = "";
                        fldTotal.Text = currentLine.Total.FormatAsCurrencyNoSymbol();
                        break;
                    case TaxAccountDetailLineType.Name:
                        fldName.Text = currentLine.Name;
                        fldPrincipal.Text = "";
                        fldTotal.Text = "";
                        fldCosts.Text = "";
                        fldInterest.Text = "";
                        fldDate.Text = "";
                        fldYear.Text = "";
                        fldRef.Text = currentLine.Reference;
                        fldCode.Text = "";
                        fldPer.Text = "";
                        break;
                    case TaxAccountDetailLineType.NewInterest:
                        fldYear.Text = "";
                        fldDate.Text = "";
                        fldRef.Text = currentLine.Reference;
                        fldInterest.Text = currentLine.Interest.FormatAsCurrencyNoSymbol();
                        fldTotal.Text = currentLine.Total.FormatAsCurrencyNoSymbol();
                        fldPrincipal.Text = currentLine.Principal.FormatAsCurrencyNoSymbol();
                        fldCosts.Text = currentLine.Costs.FormatAsCurrencyNoSymbol();
                        fldName.Text = "";
                        fldCode.Text = "";
                        fldPer.Text = "";
                        break;
                    case TaxAccountDetailLineType.Payment:
                        if (ViewModel.ShowReceiptNumber)
                        {
                            fldYear.Text = currentLine.Receipt.ToString();
                        }
                        else
                        {
                            fldYear.Text = "";
                        }

                        fldDate.Text = currentLine.EffectiveDate.FormatAndPadShortDate();
                        fldRef.Text = currentLine.Reference;
                        fldPer.Text = currentLine.Period;
                        fldCode.Text = currentLine.PayCode;
                        fldPrincipal.Text = currentLine.Principal.FormatAsCurrencyNoSymbol();
                        fldInterest.Text = currentLine.Interest.FormatAsCurrencyNoSymbol();
                        fldCosts.Text = currentLine.Costs.FormatAsCurrencyNoSymbol();
                        fldTotal.Text = currentLine.Total.FormatAsCurrencyNoSymbol();
                        fldName.Text = "";
                        if (!string.IsNullOrWhiteSpace(currentLine.Comment))
                        {
                            txtComments.Visible = true;
                            txtComments.Text = currentLine.Comment;
                            Detail.Height = .428F;
                        }
                        break;
                    case TaxAccountDetailLineType.Subtotal:
                        lnTotals.Visible = true;
                        fldName.Text = "";
                        fldCode.Text = "";
                        fldRef.Text = "Total";
                        fldCosts.Text = currentLine.Costs.FormatAsCurrencyNoSymbol();
                        fldInterest.Text = currentLine.Interest.FormatAsCurrencyNoSymbol();
                        fldPrincipal.Text = currentLine.Principal.FormatAsCurrencyNoSymbol();
                        fldTotal.Text = currentLine.Total.FormatAsCurrencyNoSymbol();
                        fldYear.Text = "";
                        break;
                    case TaxAccountDetailLineType.SummaryHeader:
                        fldName.Text = "";
                        fldCode.Text = "";
                        fldRef.Text = "";
                        fldCosts.Text = currentLine.Costs.FormatAsCurrencyNoSymbol();
                        fldInterest.Text = currentLine.Interest.FormatAsCurrencyNoSymbol();
                        fldPrincipal.Text = currentLine.Principal.FormatAsCurrencyNoSymbol();
                        fldTotal.Text = currentLine.Total.FormatAsCurrencyNoSymbol();
                        fldYear.Text = FormatYearBill(currentLine.YearBill) + " " + currentLine.BillCategory;
                        if (currentLine.BillCategory == "L")
                        {
                            fldDate.Text = "*";
                        }
                        break;
                }
            }
		}

        private string FormatYearBill(int yearBill)
        {
            var tempString = yearBill.ToString();
            return tempString.Left(4) + "-" + tempString.Right(1);
        }
	
		private void SetHeaderString()
		{
			// this will set the correct header for this account
			string strTemp = "";
			if (ViewModel.BillType == PropertyTaxBillType.Real)
			{
				// determine if this is a Real Estate or Personal Property Account
				strTemp = "RE";
				lngAcctNum = ViewModel.Account;
			}
			else
			{
				strTemp = "PP";
				lngAcctNum = ViewModel.Account;
				// hide the header labels/line
				lblLand.Visible = false;
				lblLandValue.Visible = false;
				lblBuilding.Visible = false;
				lblBuildingValue.Visible = false;
				lblExempt.Visible = false;
				lblExemptValue.Visible = false;
				lblMapLot.Visible = false;
				lblAcreage.Visible = false;
				lblBookPage.Visible = false;
				Line2.Visible = false;
			}
			strTemp += " Account " + FCConvert.ToString(lngAcctNum) + " Detail";
            strTemp += "\r\n" + "as of " + ViewModel.EffectiveDate.FormatAndPadShortDate();
			lblHeader.Text = strTemp;
			lblName.Text = "Name: " + ViewModel.CurrentOwnerName;
            
			lblLocation.Text = "Location: " + ViewModel.Location;
			if (ViewModel.BillType == PropertyTaxBillType.Real)
			{
				lblValuationTotal.Text = "Total:";
				lblMapLot.Text = "Map/Lot: " + ViewModel.RETaxAccount.AccountBriefInfo.MapLot;
				lblAcreage.Text = "Acreage: " + ViewModel.RETaxAccount.AccountTotals.Acreage.ToString("0.00") ;
				lblBookPage.Text = "Book Page: " + ViewModel.BookPages;
				lblRef1.Text = "Ref1:";
				lblRef1Val.Text = ViewModel.RETaxAccount.AccountBriefInfo.Reference1;
				if (boolShowCurrentAssessment)
                {
                    lblLandValue.Text = ViewModel.RETaxAccount.AccountTotals.Land.FormatAsNumber();
					lblBuildingValue.Text = ViewModel.RETaxAccount.AccountTotals.Building.FormatAsNumber();
					lblExemptValue.Text = ViewModel.RETaxAccount.AccountTotals.Exemptions.FormatAsNumber();
					lblValualtionValue.Text = ViewModel.RETaxAccount.AccountTotals.NetTaxable().FormatAsNumber();
				}
				else
				{
                     var bill = ViewModel.TaxAccount.Bills.FirstOrDefault(b => b.RateId > 0);
                     if (bill != null)
                     {
                         lblAssessmentYear.Text = "As of " + bill.TaxYear.ToString();
                     }
                     else
                     {
                         lblAssessmentYear.Text = "";
                     }
                    
					lblLandValue.Text = ViewModel.RETaxAccount.LatestBillTotals.Land.FormatAsNumber();
					lblBuildingValue.Text = ViewModel.RETaxAccount.LatestBillTotals.Building.FormatAsNumber();
					lblExemptValue.Text = ViewModel.RETaxAccount.LatestBillTotals.Exemptions.FormatAsNumber();
					lblValualtionValue.Text = ViewModel.RETaxAccount.LatestBillTotals.NetTaxable().FormatAsNumber();
				}
			}
			else
			{
				lblMapLot.Text = "";
				lblAcreage.Text = "";
				lblBookPage.Text = "";
				lblRef1.Text = "";
				lblRef1Val.Text = "";
				lblValuationTotal.Text = "Assessment:";
				if (boolShowCurrentAssessment)
				{
					lblValualtionValue.Text = ViewModel.PPTaxAccount.AccountTotals.Valuations.Totals().totalAmount.ToString("N");
				}
				else
				{
					lblAssessmentYear.Text = "As of " + ViewModel.EffectiveDate;
                    lblValualtionValue.Text = ViewModel.PPTaxAccount.LatestBillTotals.Valuations.Totals().totalAmount
                        .ToString("N");
				}
			}
			// fill the period due field
			CalculatePeriodDue();
			FillMailingAddress();
		}

		private void CreatePerDiemTable()
        {
            var summaryList = ViewModel.CalculationSummaries.ToList();
            var bills = ViewModel.TaxAccount.Bills;
            foreach (var bill in bills)
            {
                if (ViewModel.CalculationSummaries.ContainsKey(bill.ID))
                {
                    AddPerDiem(bill.YearBill(),ViewModel.CalculationSummaries[bill.ID].PerDiem);
                }
            }			
		}

		private void AddPerDiem(int yearBill, decimal perDiem)
		{
            if (perDiem == 0)
            {
                return;
            }
			GrapeCity.ActiveReports.SectionReportModel.TextBox obNew/*unused?*/;
			string strTemp = "";
            // this will add another per diem line in the report footer
            var yearBillString = yearBill.ToString();
            dblPDTotal += perDiem;
            if (lngPDNumber == 0)
			{
				lngPDNumber = 1;
				fldPerDiem1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
                fldPerDiem1.Text = perDiem.ToString("0.0000");
               
                lblPerDiem1.Text = yearBillString.Left(4) + "-" + yearBillString.Right(1);				
               
            }
			else
			{
				// increment the number of fields
				lngPDNumber += 1;
				// add a field
				obNew = ReportFooter.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.TextBox>("fldPerDiem" + FCConvert.ToString(lngPDNumber));
				obNew.Top = fldPerDiem1.Top + ((lngPDNumber - 1) * fldPerDiem1.Height);
				obNew.Left = fldPerDiem1.Left;
				obNew.Width = fldPerDiem1.Width;
				obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				strTemp = obNew.Font.ToString();
				obNew.Font = fldPerDiem1.Font;
				// this sets the font to the same as the field that is already created				
			    obNew.Text = perDiem.ToString("0.0000");
                GrapeCity.ActiveReports.SectionReportModel.Label obLabel = ReportFooter.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>("lblPerDiem" + FCConvert.ToString(lngPDNumber));

				obLabel.Top = lblPerDiem1.Top + ((lngPDNumber - 1) * lblPerDiem1.Height);
				obLabel.Left = lblPerDiem1.Left;
				obLabel.Font = fldPerDiem1.Font;

				obLabel.Text = yearBillString.Left(4) + "-" + yearBillString.Right(1);
            }
			boolPerDiem = true;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
            var counter = 0;
            foreach (var exemptionCode in ViewModel.ExemptionCodes)
            {
                counter++;
                switch (counter)
                {
                    case 1:
                        fldEx1.Text = exemptionCode;
                        break;
                    case 2:
                        fldEx2.Text = exemptionCode;
                        break;
                    case 3:
                        fldEx3.Text = exemptionCode;
                        break;
                }
                lblEx.Visible = true;
            }
            txtAsOf.Text = "Account Totals as of " + ViewModel.EffectiveDate.FormatAndPadShortDate();
            txtTotalTotal.Text = ViewModel.AccountSummary.Balance.FormatAsMoney();
            txtTotalPrin.Text = ViewModel.AccountSummary.PrincipalDue().FormatAsMoney();
            txtTotalInterest.Text = ViewModel.AccountSummary.InterestDue().FormatAsMoney();
            txtTotalCosts.Text = ViewModel.AccountSummary.CostsDue().FormatAsMoney();

		}

		private void CreatePerDiemField()
		{
			if (boolPerDiem)
			{
				// add a field
				GrapeCity.ActiveReports.SectionReportModel.TextBox obNew = ReportFooter.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.TextBox>("fldPerDiemTotal");
				//obNew.Name = "fldPerDiemTotal";
				obNew.Top = fldPerDiem1.Top + (lngPDNumber * fldPerDiem1.Height);
				obNew.Left = fldPerDiem1.Left;
				obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				obNew.Width = fldPerDiem1.Width;
				obNew.Text = Strings.Format(dblPDTotal, "0.0000");
				//ReportFooter.Controls.Add(obNew);
				// add a label
				GrapeCity.ActiveReports.SectionReportModel.Label obLabel = ReportFooter.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>("lblPerDiemTotal");
				//obLabel.Name = "lblPerDiemTotal";
				obLabel.Top = lblPerDiem1.Top + (lngPDNumber * lblPerDiem1.Height);
				obLabel.Left = lblPerDiem1.Left;
				obLabel.Text = "Total";
				//ReportFooter.Controls.Add(obLabel);
				// add a line
				GrapeCity.ActiveReports.SectionReportModel.Line obLine = ReportFooter.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Line>("lnPerDiemTotal");
				//obLine.Name = "lnPerDiemTotal";
				obLine.X1 = fldPerDiem1.Left;
				obLine.X2 = fldPerDiem1.Left + fldPerDiem1.Width;
				obLine.Y1 = lblPerDiem1.Top + (lngPDNumber * lblPerDiem1.Height);
				obLine.Y2 = obLine.Y1;
				//ReportFooter.Controls.Add(obLine);
				// kk10222014 trocls-35  Adding controls here doesn't trigger the footer to grow
				if (ReportFooter.Height < (fldPerDiem1.Top + (lngPDNumber + 1) * fldPerDiem1.Height))
				{
					ReportFooter.Height = fldPerDiem1.Top + (lngPDNumber + 1) * fldPerDiem1.Height;
				}
			}
			else
			{
				lblPerDiem1.Visible = false;
				lblPerDiemHeader.Visible = false;
				fldPerDiem1.Visible = false;
				Line3.Visible = false;
			}
		}

		private void CalculatePeriodDue()
        {
            var bill = ViewModel.TaxAccount.Bills.FirstOrDefault(b => !b.HasLien() && b.RateId > 0);
            if (bill != null)
            {
                decimal period1 = 0;
                decimal period2 = 0;
                decimal period3 = 0;
                decimal period4 = 0;
                decimal extraInterest = 0;
                if (ViewModel.CalculationSummaries.ContainsKey(bill.ID))
                {
                    var summary = ViewModel.CalculationSummaries[bill.ID];
                    extraInterest = summary.Interest;
                }
                var installmentsDue = bill.GetInstallmentsDue(extraInterest);
                var strTemp = FormatYearBill(bill.YearBill()) + " Period Due: ";
                var period = 0;
                foreach (var installment in installmentsDue)
                {
                    period += 1;
                    if (installment != 0)
                    {
                        strTemp += "\n" + "     " + period.ToString() +") " + installment.FormatAsCurrencyNoSymbol() + "  ";
                    }
                }
              
                fldPerDue.Text = strTemp;
            }
            else
            {
                fldPerDue.Text = "";
            }			
		}

		private void FillMailingAddress()
		{
			try
            {
                var strTemp = "";
                lblMailingAddressTitle.Text = "Mailing Address:";
                
                var separ = "";
                if (!string.IsNullOrWhiteSpace( ViewModel.MailingAddress.Address1 ))
                {
                    strTemp += separ + ViewModel.MailingAddress.Address1;
                    separ = "\n";
                }

                if (!string.IsNullOrWhiteSpace(ViewModel.MailingAddress.Address2))
                {
                    strTemp += separ + ViewModel.MailingAddress.Address2;
                    separ = "\n";
                }

                if (!string.IsNullOrWhiteSpace(ViewModel.MailingAddress.Address3))
                {
                    strTemp += separ + ViewModel.MailingAddress.Address3;
                    separ = "\n";
                }

                var csz = ViewModel.MailingAddress.City + ", " + ViewModel.MailingAddress.State +
                          " " + ViewModel.MailingAddress.Zip;
                strTemp += separ + csz;
                lblMailingAddress.Text = strTemp;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Adding Mailing Address");
			}
		}

        public void Show()
        {
            var reportViewer = new frmReportViewer();
            reportViewer.Init(this);
        }

        public ITaxAccountDetailReportModel ViewModel { get; set; }
    }
}
