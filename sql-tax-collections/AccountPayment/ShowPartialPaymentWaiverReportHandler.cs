﻿using Global;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Lien;

namespace TWCL0000.AccountPayment
{
    public class ShowPartialPaymentWaiverReportHandler : CommandHandler<ShowPartialPaymentWaiverReport>
    {
        public ShowPartialPaymentWaiverReportHandler()
        {

        }
        protected override void Handle(ShowPartialPaymentWaiverReport command)
        {
            var obRep = new arLienPartialPayment();
            obRep.Init(command.Municipality,command.State,command.PaymentAmount.ToDouble(),command.Owner,command.TaxYear.ToString(),command.PaymentDate,command.Witness,command.MapLot,command.Account);
            frmReportViewer.InstancePtr.Init(obRep,"",1,false,false,"Pages",false,"","TRIO Software",false,true,"",false,false,command.ShowModal,false);
        }
    }
}


