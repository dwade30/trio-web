﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Global;
using SharedApplication;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections.Lien;
using SharedApplication.TaxCollections.Models;
using TWSharedLibrary;

namespace TWCL0000.AccountPayment
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptLienDischarge : BaseSectionReport, IView<ITaxLienDischargeAlternateNoticeReportModel>
    {
        private IEnumerable<RealEstateTaxBill> lienedBills;
        private IEnumerator<RealEstateTaxBill> reportEnumerator;

		public rptLienDischarge()
		{
			InitializeComponent();
			InitializeComponentEx();
		}

        public rptLienDischarge(ITaxLienDischargeAlternateNoticeReportModel viewModel) : this()
        {
            ViewModel = viewModel;
        }
		private void InitializeComponentEx()
		{

			this.Name = "30 Day Notice";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
            if (reportEnumerator.MoveNext())
            {
                eArgs.EOF = false;
            }
            else
            {
                eArgs.EOF = true;
            }
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
            {
                lienedBills = ViewModel.PaidLiens();
                reportEnumerator = lienedBills.GetEnumerator();
				// On Error GoTo ERROR_HANDLER
				fldDate.Text = ViewModel.PayDate.FormatAndPadShortDate();
				fldHeading.Text = "Dear " + ViewModel.TaxAccount.Name1() + ",";
				fldTopText.Text = ViewModel.DischargeNotice.AltTopText;
				lblYear.Text = "Year";
				lblMapLot.Text = "Map Lot";
				lblLienDate.Text = "Lien Date";
				lblLienBook.Text = "Book";
				lblLienPage.Text = "Page";
				lblDischargeDate.Text = "Discharge Date";
				lblLDNBook.Text = "Book";
				lblLDNPage.Text = "Page";
				fldBottomText.Text = ViewModel.DischargeNotice.AltBottomText;
				fldEnding.Text = "Sincerely," + "\r\n" + "\r\n";
				fldEnding.Text = fldEnding.Text + ViewModel.DischargeNotice.AltName1 + "\r\n";
				fldEnding.Text = fldEnding.Text + ViewModel.DischargeNotice.AltName2 + "\r\n";
				fldEnding.Text = fldEnding.Text + ViewModel.DischargeNotice.AltName3 + "\r\n";
				fldEnding.Text = fldEnding.Text + ViewModel.DischargeNotice.AltName4 + "\r\n";
                
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Starting Report");
			}
		}

		private void BindFields()
		{
			try
            {
                var bill = reportEnumerator.Current;
                if (bill != null)
                {
                    fldYear.Text = bill.TaxYear + "-" + bill.BillNumber;
                    fldMapLot.Text = bill.MapLot;
                    fldLienBook.Text = bill.Lien.Book;
                    fldLienPage.Text = bill.Lien.Page;
                    if (bill.Lien.RateRecord != null)
                    {
                        fldLienDate.Text = bill.Lien.RateRecord.BillingDate.FormatAndPadShortDate();
                    }
                    else
                    {
                        fldLienDate.Text = "";
                    }
                }

                var dischargeNeeded = ViewModel.GetDischargeNeeded(bill.LienID);
                if (dischargeNeeded != null)
                {
                    fldDischargeDate.Text = dischargeNeeded.DatePaid.GetValueOrDefault().FormatAndPadShortDate();
                    fldLDNBook.Text = dischargeNeeded.Book;
                    fldLDNPage.Text = dischargeNeeded.Page;
                }				
				else
				{
					fldDischargeDate.Text = "";
					fldLDNBook.Text = "";
					fldLDNPage.Text = "";
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Binding Fields");
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}


        public void Show()
        {
            frmReportViewer.InstancePtr.Init(this);
        }

        public ITaxLienDischargeAlternateNoticeReportModel ViewModel { get; set; }
    }
}
