﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedApplication;
using SharedApplication.CentralParties;
using SharedApplication.Messaging;
using SharedApplication.RealEstate;
using SharedApplication.RealEstate.Models;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.AccountPayment;

namespace TWCL0000.AccountPayment
{
    public class PrintRealEstateTaxAccountDetailHandler : CommandHandler<PrintRealEstateTaxAccountDetail>
    {
        private IView<ITaxAccountDetailReportModel> reportView;
        private GlobalTaxCollectionSettings taxSettings;
        private DataEnvironmentSettings environmentSettings;
        private IQueryHandler<RealEstateBookPageQuery, IEnumerable<BookPage>> bookPageQueryHandler;
        private IRealEstateContext reContext;
        public PrintRealEstateTaxAccountDetailHandler(IView<ITaxAccountDetailReportModel> reportView, IRealEstateContext reContext, GlobalTaxCollectionSettings taxSettings, DataEnvironmentSettings environmentSettings, IQueryHandler<RealEstateBookPageQuery, IEnumerable<BookPage>> bookPageQueryHandler)
        {
            this.reportView = reportView;
            this.taxSettings = taxSettings;
            this.environmentSettings = environmentSettings;
            this.bookPageQueryHandler = bookPageQueryHandler;
            this.reContext = reContext;
        }
        protected override void Handle(PrintRealEstateTaxAccountDetail command)
        {
            reportView.ViewModel.Account = command.Account;
            foreach (var keyPair in command.CalculationSummaries)
            {
                reportView.ViewModel.CalculationSummaries.Add(keyPair.Key, keyPair.Value);
            }
            var accountView = reContext.REAccountPartyAddressViews.Where(r => r.Rsaccount == command.Account && r.Rscard == 1).FirstOrDefault();
            
            if (accountView != null)
            {
                reportView.ViewModel.MailingAddress.Address1 = accountView.Address1;
                reportView.ViewModel.MailingAddress.Address2 = accountView.Address2;
                reportView.ViewModel.MailingAddress.Address3 = accountView.Address3;
                reportView.ViewModel.MailingAddress.City = accountView.City;
                reportView.ViewModel.MailingAddress.State = accountView.State;
                reportView.ViewModel.MailingAddress.Zip = accountView.Zip;

                if (accountView.RiexemptCd1.GetValueOrDefault() > 0)
                {
                    var code = reContext.ExemptCodes.FirstOrDefault(e => e.Code.GetValueOrDefault() == accountView.RiexemptCd1.GetValueOrDefault());
                    if (code != null)
                    {
                        reportView.ViewModel.ExemptionCodes.Add(code.Code.GetValueOrDefault().ToString() + " " + code.Description);
                    }
                }

                if (accountView.RiexemptCd2.GetValueOrDefault() > 0)
                {
                    var code = reContext.ExemptCodes.FirstOrDefault(e => e.Code.GetValueOrDefault() == accountView.RiexemptCd2.GetValueOrDefault());
                    if (code != null)
                    {
                        reportView.ViewModel.ExemptionCodes.Add(code.Code.GetValueOrDefault().ToString() + " " + code.Description);
                    }
                }

                if (accountView.RiexemptCd3.GetValueOrDefault() > 0)
                {
                    var code = reContext.ExemptCodes.FirstOrDefault(e => e.Code.GetValueOrDefault() == accountView.RiexemptCd3.GetValueOrDefault());
                    if (code != null)
                    {
                        reportView.ViewModel.ExemptionCodes.Add(code.Code.GetValueOrDefault().ToString() + " " + code.Description);
                    }
                }
            }
            reportView.ViewModel.MuniName = environmentSettings.ClientName;
            reportView.ViewModel.AccountDetailAssessmentSetting = taxSettings.AccountDetailAssessmentSetting;
            reportView.ViewModel.ShowReceiptNumber = taxSettings.ShowReceiptNumberOnDetail;
            reportView.ViewModel.AccountSummary = new TaxBillCalculationSummary(command.Account,command.BillType);
            reportView.ViewModel.AccountSummary.AddTo(command.AccountSummary);
            reportView.ViewModel.BillType = command.BillType;
            reportView.ViewModel.EffectiveDate = command.EffectiveDate;
            reportView.ViewModel.PPTaxAccount = null;
            reportView.ViewModel.RETaxAccount = command.RETaxAccount;
            reportView.ViewModel.ExpandedYearBills = command.ExpandedYearBills;
            var bookPages = bookPageQueryHandler.ExecuteQuery(new RealEstateBookPageQuery(command.Account, true));
            reportView.ViewModel.BookPages = MakeBookPageString(bookPages);

            reportView.Show();
        }

        private string MakeBookPageString(IEnumerable<BookPage> bookPages)
        {
            if (bookPages == null)
            {
                return "";
            }

            var bookPageString = new StringBuilder();
            var spacer = "";
            foreach (var bookPage in bookPages)
            {
                bookPageString.Append(spacer + "B" + bookPage.Book + "P" + bookPage.Page);
                spacer = ", ";
            }

            return bookPageString.ToString();
        }
    }
}