﻿using System;
using System.Linq;
using fecherFoundation;
using Global;
using SharedApplication;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections.AccountPayment;
using Wisej.Web;

namespace TWCL0000.AccountPayment
{
    public partial class frmShowRealEstateTaxAccountInfo : BaseForm, IView<IShowRealEstateTaxAccountInfoViewModel>
    {
        public frmShowRealEstateTaxAccountInfo()
        {
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            this.Load += FrmShowRealEstateTaxAccountInfo_Load;
        }

        private void FrmShowRealEstateTaxAccountInfo_Load(object sender, EventArgs e)
        {
            ShowInfo();
        }

        public frmShowRealEstateTaxAccountInfo(IShowRealEstateTaxAccountInfoViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }

        public new void Show()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            if (!this.Visible)
            {
                this.BringToFront();
                base.Show(FormShowEnum.Modeless, "", null);
            }
        }

        private void ShowInfo()
        {
            var accountInfo = ViewModel.AccountInfo;
            AddControl("Current Owner",accountInfo.Name1);
            if (!string.IsNullOrWhiteSpace(accountInfo.Name2))
            {
                AddControl("Second Owner",accountInfo.Name2);
            }

            if (!string.IsNullOrWhiteSpace(accountInfo.Location))
            {
                AddControl("Location", accountInfo.Location);
            }

            var mailingAddress = accountInfo.MailingAddress;
            var description = "Mailing Address";
            if (!string.IsNullOrWhiteSpace(mailingAddress.Address1))
            {
                AddControl(description,mailingAddress.Address1);
                description = "";
            }

            if (!string.IsNullOrWhiteSpace(mailingAddress.Address2))
            {
                AddControl(description,mailingAddress.Address2);
                description = "";
            }

            if (!string.IsNullOrWhiteSpace(mailingAddress.Address3))
            {
                AddControl(description,mailingAddress.Address3);
                description = "";
            }

            var csz = GetCityStateZip(mailingAddress.City, mailingAddress.State, mailingAddress.Zip);
            if (!string.IsNullOrWhiteSpace(csz))
            {
                AddControl(description,csz);
            }

            if (!string.IsNullOrWhiteSpace(accountInfo.MapLot))
            {
                AddControl("Maplot", accountInfo.MapLot);
            }

            if (!string.IsNullOrWhiteSpace(accountInfo.BookPage))
            {
                AddControl("Book Page(s)", accountInfo.BookPage);
            }

            if (accountInfo.Acreage > 0)
            {
                AddControl("Acres", accountInfo.Acreage.ToString("0.###"));
            }
            AddControl("Tax Acquired", accountInfo.IsTaxAcquired ? "Yes" : "No");
            if (accountInfo.AssociatedPPAccount > 0)
            {
                AddControl("Associated PP Account", accountInfo.AssociatedPPAccount.ToString());
                AddControl("Outstanding PP Amount", accountInfo.OutstandingPPAmount.FormatAsCurrencyNoSymbol());
            }

            if (accountInfo.AccountGroup > 0)
            {
                AddControl("Account Group", accountInfo.AccountGroup.ToString());
            }

            if (accountInfo.ExemptionCodes.Any())
            {
                description = "Exempt Codes";
                foreach (var exemption in accountInfo.ExemptionCodes)
                {
                    AddControl(description,exemption);
                    description = "";
                }
            }
        }

        private string GetCityStateZip(string city, string state, string zip)
        {
            return city + ", " + state + " " + zip;
        }
        private void AddControl(string description, string value)
        {
            LayoutPanel.Controls.Add(GetControl(description,value));
        }
        private TableLayoutPanel GetControl(string description, string value)
        {
            var panel = new TableLayoutPanel();
            panel.Margin = new Padding(1);
            panel.ColumnCount = 2;
            panel.RowCount = 1;
            panel.AutoSize = true;
            panel.Dock = DockStyle.Left & DockStyle.Right;
            panel.Padding = new Padding(1);

            panel.Controls.Add(new FCLabel() { Text = description, AutoSize = false, Dock = DockStyle.Fill, Width = 165, Margin = Padding.Empty });
            panel.Controls.Add(new Label() { Text = value, AutoSize = true, Dock = DockStyle.Fill, Margin = Padding.Empty });

            return panel;
        }

        public IShowRealEstateTaxAccountInfoViewModel ViewModel { get; set; }
    }
}
