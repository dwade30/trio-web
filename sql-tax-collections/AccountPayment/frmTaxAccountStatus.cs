﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using fecherFoundation;
using Global;
using iTextSharp.text;
using SharedApplication;
using SharedApplication.AccountGroups;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.RealEstate;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.AccountPayment;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Models;
using TWSharedLibrary;
using AnchorStyles = Wisej.Web.AnchorStyles;
using Color = System.Drawing.Color;
using DataGridViewCellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle;
using DialogResult = Wisej.Web.DialogResult;
using Keys = Wisej.Web.Keys;
using MessageBox = Wisej.Web.MessageBox;
using MessageBoxButtons = Wisej.Web.MessageBoxButtons;
using MessageBoxIcon = Wisej.Web.MessageBoxIcon;
using MouseButtons = Wisej.Web.MouseButtons;

namespace TWCL0000.AccountPayment
{
    public partial class frmTaxAccountStatus : BaseForm, IView<ITaxAccountStatusViewModel>
    {
        private bool changingPayCode = false;
        private bool cancelling = true;
        private List<string> periodToolTips = new List<string>();
        private GridAccountUtility clsAcct = new GridAccountUtility();
       // private clsGridAccount clsAcct = new clsGridAccount();
        public frmTaxAccountStatus()
        {
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            PeriodGrid.RowHeight(-1, 550);
            PeriodGrid.CellBorderStyle = DataGridViewCellBorderStyle.None;
            this.Load += FrmTaxAccountStatus_Load;
            toolTip1.SetToolTip(lblAcctComment, "This account has a comment");
            lblAcctComment.DoubleClick += LblAcctComment_DoubleClick;
            mnuFileComment.Click += MnuFileComment_Click;
            mnuFileREComment.Click += MnuFileREComment_Click;
            lblAcctComment.Click += LblAcctComment_Click;
            imgNote.DoubleClick += ImgNote_DoubleClick;
            mnuFileEditNote.Click += MnuFileEditNote_Click;
            this.imgNote.ImageSource = "imgnote?color=#707884";
            imgNote.Click += ImgNote_Click;
            this.cmdProcessEffective.Click += CmdProcessEffective_Click;
            this.Resize += FrmTaxAccountStatus_Resize;
            PaymentsGrid.RowCollapsed += PaymentsGrid_RowCollapsed;
            PaymentsGrid.RowExpanded += PaymentsGrid_RowExpanded;
            PaymentsGrid.Click += PaymentsGrid_Click;
            PaymentsGrid.DoubleClick += PaymentsGrid_DoubleClick;
            this.Closing += FrmTaxAccountStatus_Closing;
            PendingGrid.KeyDown += PendingGrid_KeyDown;
            PaymentsGrid.MouseDown += PaymentsGrid_MouseDown;
            this.mnuFileAccountInfo.Click += MnuFileAccountInfo_Click;
            this.mnuFileInterestedParty.Click += MnuFileInterestedParty_Click;
            this.mnuFileTaxInfoSheet.Click += MnuFileTaxInfoSheet_Click;
            this.mnuFilePrintBPReport.Click += MnuFilePrintBPReport_Click;
            this.mnuFileOptionLoadValidAccount.Click += MnuFileOptionLoadValidAccount_Click;
            this.mnuFileDischarge.Click += MnuFileDischarge_Click;
            this.mnuFilePrintAltLDN.Click += MnuFilePrintAltLDN_Click;
            txtCD.DoubleClick += TxtCD_DoubleClick;
            txtCash.DoubleClick += TxtCash_DoubleClick;
            this.Shown += FrmTaxAccountStatus_Shown;
            mnuPaymentClearList.Click += MnuPaymentClearList_Click;
            txtCD.TextChanged += TxtCD_TextChanged;
            txtCash.TextChanged += TxtCash_TextChanged;
            btnChangeAccount.Click += BtnChangeAccount_Click;
            btnPrintAccountDetail.Click += BtnPrintAccountDetail_Click;
            btnMortgageHolderInfo.Click += BtnMortgageHolderInfo_Click;
            clsAcct.GRID7Light = txtAcctNumber;
            clsAcct.DefaultAccountType = "G";
            clsAcct.ToolTipText = "Hit Shift-F2 to get a valid accounts list.";
            cmbPeriod.Validating += CmbPeriod_Validating;
            this.KeyDown += FrmTaxAccountStatus_KeyDown;
          
            txtTotalTotal.DoubleClick += TxtTotalTotal_DoubleClick;
            mnuFilePreviousAccount.Click += MnuFilePreviousAccount_Click;
            mnuFileNextAccount.Click += MnuFileNextAccount_Click;
        }

        private void MnuFileNextAccount_Click(object sender, EventArgs e)
        {
            if (ViewModel.HasPendingPayments())
            {
                if (FCMessageBox.Show("Any pending changes you have made will be lost.\nDo you want to exit without saving?", MsgBoxStyle.Exclamation | MsgBoxStyle.YesNo, "Exit Without Saving?") != DialogResult.Yes)
                {
                    return;
                }
            }
            if (ViewModel.ShowNextAccount())
            {
                CloseWithoutCancel();
            }
        }

        private void MnuFilePreviousAccount_Click(object sender, EventArgs e)
        {
            if (ViewModel.HasPendingPayments())
            {
                if (FCMessageBox.Show("Any pending changes you have made will be lost.\nDo you want to exit without saving?", MsgBoxStyle.Exclamation | MsgBoxStyle.YesNo, "Exit Without Saving?") != DialogResult.Yes)
                {
                    return;
                }
            }
            if (ViewModel.ShowPreviousAccount())
            {
                CloseWithoutCancel();
            }
        }

        private void MnuFileREComment_Click(object sender, EventArgs e)
        {
            ViewModel.AddRECLComment();
            CheckForComment();
        }

        private void TxtTotalTotal_DoubleClick(object sender, EventArgs e)
        {
            if (ViewModel.AllowPayments)
            {
                SetPaymentsForAuto();
                InsertAutoPayment();
            }
        }

        private void FrmTaxAccountStatus_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;

            if (ViewModel.AllowPayments && KeyCode == Keys.Insert)
            {
                e.Handled = true;
                InsertAutoPayment();
            }
        }

        private void CmbPeriod_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //if (cmbCode.SelectedItem == null)
            //{
            //    return;
            //}
            //var payCodePair = (DescriptionEnumPair<PropertyTaxPaymentCode>)cmbCode.SelectedItem;
            //if (payCodePair.ID != PropertyTaxPaymentCode.Abatement)
            //{
            //    if (cmbPeriod.Text.Left(1) != "A")
            //    {
            //        SetPaymentPeriod("Auto");
            //        //e.Cancel = true;
            //        return;
            //    }
            //}
        }

        private void BtnMortgageHolderInfo_Click(object sender, EventArgs e)
        {
            ViewModel.ShowMortgageHolder();
        }

        private void BtnPrintAccountDetail_Click(object sender, EventArgs e)
        {
            PrintAccountDetail();
        }

        private void BtnChangeAccount_Click(object sender, EventArgs e)
        {
            if (ViewModel.HasPendingPayments())
            {
                if (FCMessageBox.Show("Any pending changes you have made will be lost.\nDo you want to exit without saving?", MsgBoxStyle.Exclamation | MsgBoxStyle.YesNo, "Exit Without Saving?") != DialogResult.Yes)
                {
                    return;
                }
            }
            ViewModel.SearchForAccount();
            CloseWithoutCancel();
        }

        private void TxtCash_TextChanged(object sender, EventArgs e)
        {
            if (txtCash.Text.ToUpper() != "Y" && txtCash.Text.ToUpper() != "N")
            {
                txtCash.Text = "Y";
                return;
            }

            if (txtCash.Text != txtCash.Text.ToUpper())
            {
                txtCash.Text = txtCash.Text.ToUpper();
                return;
            }
            CheckCash();
        }

        private void TxtCD_TextChanged(object sender, EventArgs e)
        {
            if (txtCD.Text.ToUpper() != "Y" && txtCD.Text.ToUpper() != "N")
            {
                txtCD.Text = "Y";
                return;
            }

            if (txtCD.Text != txtCD.Text.ToUpper())
            {
                txtCD.Text = txtCD.Text.ToUpper();
                return;
            }
            CheckCD();
        }

        private void MnuPaymentClearList_Click(object sender, EventArgs e)
        {
            ViewModel.RemoveAllPendingPayments();
            if (ViewModel.BillType == PropertyTaxBillType.Personal)
            {
                ShowBills(ViewModel.PPTaxAccount);
            }
            else
            {
                ShowBills(ViewModel.RETaxAccount);
            }
        }

        private void FrmTaxAccountStatus_Shown(object sender, EventArgs e)
        {
            if (ViewModel.AutoShowNote)
            {
                FCMessageBox.Show("Account " + ViewModel.Account + " note." + "\r\n" + ViewModel.Note,
                    MsgBoxStyle.Information | MsgBoxStyle.OkOnly, "Account Note");
                //MessageBox.Show("Account " + ViewModel.Account + " note." + "\r\n" + ViewModel.Note,
                    // "Account Note", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            if (ViewModel.AllowPayments)
            {
                txtReference.Focus();
                if (ViewModel.HasCreditsOnOlderYears())
                {
                    if (FCMessageBox.Show(
                            "You have some years with negative balances.\nWould you like to apply those to other balances now?\n You can always choose 'Apply Credits to Balances' from the menu to apply the appropriate entries later.",
                            MsgBoxStyle.YesNo | MsgBoxStyle.Question, "Apply Credits To Balances") == DialogResult.Yes)
                    {
                        ApplyCredits();
                    }
                }
            }
        }

        private void TxtCash_DoubleClick(object sender, EventArgs e)
        {
            if (txtCash.Enabled)
            {
                if (txtCash.Text == "Y")
                {
                    txtCash.Text = "N";
                }
                else
                {
                    txtCash.Text = "Y";
                }
            }
        }

        private void TxtCD_DoubleClick(object sender, EventArgs e)
        {
            if (txtCD.Enabled)
            {
                if (txtCD.Text == "Y")
                {
                    txtCD.Text = "N";
                }
                else
                {
                    txtCD.Text = "Y";
                }
            }
        }

        private void MnuFilePrintAltLDN_Click(object sender, EventArgs e)
        {
            ShowAlternateDischargeNotice();
        }

        private void MnuFileDischarge_Click(object sender, EventArgs e)
        {
            ShowDischargeNotice();
        }

        private void MnuFileOptionLoadValidAccount_Click(object sender, EventArgs e)
        {
            LoadValidAccount();
        }

        private void LoadValidAccount()
        {
            var account = ViewModel.SelectValidAccount(txtAcctNumber.TextMatrix(0, 0));
            txtAcctNumber.TextMatrix(0, 0, account);
            txtAcctNumber.EditText = account;
        }

        private void MnuFilePrintBPReport_Click(object sender, EventArgs e)
        {
            ViewModel.ShowBookPageReport();
        }

        private void MnuFileTaxInfoSheet_Click(object sender, EventArgs e)
        {
            ViewModel.ShowTaxInformationSheet();
        }

        private void MnuFileInterestedParty_Click(object sender, EventArgs e)
        {
            ViewModel.ShowInterestedParties();
        }

        private void PrintAccountDetail()
        {
            var expandedBills = new List<int>();
            for (int row = 1; row < PaymentsGrid.Rows; row++)
            {
                var rowData = (PaymentGridRowInfo) PaymentsGrid.RowData(row);
                if (rowData != null)
                {
                    if (rowData.RowType == PaymentGridRowType.Header)
                    {
                        if (PaymentsGrid.IsCollapsed(row) != FCGrid.CollapsedSettings.flexOutlineCollapsed)
                        {
                            expandedBills.Add(rowData.YearBill);
                        }
                    }
                }
            }

            ViewModel.ShowAccountDetail(expandedBills);
        }

        private void MnuFileAccountInfo_Click(object sender, EventArgs e)
        {
            ShowAccountInfo();
        }

        private void ShowAccountInfo()
        {
            ViewModel.ShowAccountInfo();
        }

        private void PaymentsGrid_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
        {
            var mouseRow = PaymentsGrid.MouseRow;

            if (e.Button == MouseButtons.Right && mouseRow > 0)
            {
                var rowData = (PaymentGridRowInfo) PaymentsGrid.RowData(mouseRow);
                ShowDetail(rowData);
            }
        }

        private void ShowDetail(PaymentGridRowInfo rowData)
        {
            switch (rowData.RowType)
            {
                case PaymentGridRowType.Header:
                    ViewModel.ShowRateInfo(rowData.YearBill);
                    break;
                case PaymentGridRowType.Payment:
                    ViewModel.ShowPaymentInfo(rowData.Payment);
                    break;
                case PaymentGridRowType.LienHeader:
                    ViewModel.ShowLienInfo(rowData.YearBill);
                    break;
                default:
                    // ViewModel.ShowPaymentInfo(rowData.Payment);
                    break;
            }
        }

        private void PendingGrid_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            int keyCode = e.KeyValue;
            if (e.KeyCode == Keys.Delete)
            {
                var row = PendingGrid.Row;
                if (row < 1)
                {
                    return;
                }

                if (!ViewModel.AllowPayments)
                {
                    return;
                }

                if (FCMessageBox.Show("Are you sure you want to delete this payment?",MsgBoxStyle.YesNo | MsgBoxStyle.Question, "Delete Payment?") == DialogResult.Yes)
                {
                    RemovePendingRow(row);
                }
            }
        }

        private void RemovePendingRow(int row)
        {
            if (row < 1)
            {
                return;
            }

            if (row >= PendingGrid.Rows)
            {
                return;
            }

            var payment = (PaymentRec) PendingGrid.RowData(row);
            if (payment == null)
            {
                return;
            }

            if (payment.IsCreatedInterest())
            {
                return;
            }

            ReversePayment(payment);
        }

        private void FrmTaxAccountStatus_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (cancelling)
            {
                if (ViewModel.HasPendingPayments())
                {
                    if (FCMessageBox.Show("Any pending changes you have made will be lost.\nDo you want to exit without saving?", MsgBoxStyle.Exclamation | MsgBoxStyle.YesNo, "Exit Without Saving?") != DialogResult.Yes)
                    {
                        e.Cancel = true;
                        return;
                    }
                }
                ViewModel.Cancel();
            }
        }

        private void PaymentsGrid_DoubleClick(object sender, EventArgs e)
        {

            DoubleClickRow(PaymentsGrid.Row);
        }

        private void DoubleClickRow(int row)
        {
            if (row < 1)
            {
                return;
            }

            if (!ViewModel.AllowPayments)
            {
                return;
            }

            var rowInfo = (PaymentGridRowInfo) PaymentsGrid.RowData(row);
            if (rowInfo == null)
            {
                return;
            }

            switch (rowInfo.RowType)
            {
                case PaymentGridRowType.Header:
                    DoubleClickHeader(row, rowInfo.YearBill);
                    return;
                    break;
                case PaymentGridRowType.LienHeader:
                    break;
                case PaymentGridRowType.Payment:
                    if (rowInfo.Payment.IsCreatedInterest())
                    {
                        return;
                    }

                    if (rowInfo.Payment.Id == 0)
                    {
                        ReversePayment(rowInfo.Payment);
                        return;
                    }

                    if (!rowInfo.Payment.IsChargedInterest() && !rowInfo.Payment.IsEarnedInterest() &&
                        !rowInfo.Payment.IsConversionRecord() && !rowInfo.Payment.IsReversal.GetValueOrDefault())
                    {
                        if (rowInfo.Payment.IsTypeOfAbatement())
                        {
                            FCMessageBox.Show(
                                "Please use a correction or negative abatement to manually reverse an abatement", MsgBoxStyle.OkOnly |  MsgBoxStyle.Exclamation,
                                "Abatement Reversal");
                            return;
                        }

                        if (rowInfo.Payment.IsDiscount())
                        {
                            if (FCMessageBox.Show("Are you sure that you would like to reverse this discount?", MsgBoxStyle.YesNo | MsgBoxStyle.Question,
                                    "Reverse Payment") ==
                                DialogResult.Yes)
                            {
                                ReversePayment(rowInfo.Payment);
                            }
                        }
                        else
                        {
                            if (FCMessageBox.Show("Are you sure that you would like to reverse this payment?", MsgBoxStyle.YesNo| MsgBoxStyle.Question,
                                    "Reverse Payment") == DialogResult.Yes)
                            {
                                ReversePayment(rowInfo.Payment);
                            }
                        }
                    }

                    break;
                case PaymentGridRowType.Subtotal:
                    FillPaymentControlsFromSubtotal(rowInfo.BillSummary, rowInfo.YearBill);
                    break;
                default:
                    return;
                    break;
            }
        }

        private void DoubleClickHeader(int row, int yearBill)
        {
            if (row < 1)
            {
                return;
            }

            SetYearCombo(yearBill);
            PaymentsGrid.IsCollapsed(row,
                PaymentsGrid.IsCollapsed(row) == FCGrid.CollapsedSettings.flexOutlineCollapsed
                    ? FCGrid.CollapsedSettings.flexOutlineExpanded
                    : FCGrid.CollapsedSettings.flexOutlineCollapsed);
        }

        private void FillPaymentControlsFromSubtotal(TaxBillCalculationSummary billSummary, int yearBill)
        {
            if (ViewModel.AllowAutoPaymentFill)
            {
                if (yearBill > 0)
                {
                    SetPaymentBoxesVisibility(true);
                    SelectBillYear(yearBill);
                    SetPaymentPeriod(0);
                    SetPayCode(PropertyTaxPaymentCode.RegularPayment);
                    txtCD.Text = "Y";
                    txtCash.Text = "Y";
                    txtAcctNumber.Text = "";
                    txtAmount.Text = billSummary.Balance.FormatAsMoney();
                }
            }
        }

        private void ReversePayment(PaymentRec payment)
        {
            var outcome = ViewModel.ReversePayment(payment);
            if (!outcome.success)
            {
                FCMessageBox.Show(outcome.message, MsgBoxStyle.OkOnly | MsgBoxStyle.Exclamation, outcome.caption);
                return;
            }
            else
            {
                if (ViewModel.BillType == PropertyTaxBillType.Personal)
                {
                    ShowBills(ViewModel.PPTaxAccount);
                }
                else
                {
                    ShowBills(ViewModel.RETaxAccount);
                }
                PaymentsGrid.Outline(1);
                ExpandYearsWithPending();
            }

        }

        private void AddPayment(PaymentSetupInfo paymentInfo)
        {
            if (paymentInfo != null)
            {
                var result = ViewModel.AddPendingPayment(paymentInfo);
                if (!result.success)
                {
                    FCMessageBox.Show(result.message, MsgBoxStyle.OkOnly | MsgBoxStyle.Critical, result.caption);
                    return;
                }
                else
                {
                    if (ViewModel.BillType == PropertyTaxBillType.Personal)
                    {
                        ShowBills(ViewModel.PPTaxAccount);
                    }
                    else
                    {
                        ShowBills(ViewModel.RETaxAccount);
                    }
                    PaymentsGrid.Outline(1);
                    ExpandYearsWithPending();
                }

            }
        }

        private void SetPaymentControlsFromInfo(PaymentSetupInfo paymentInfo)
        {
            SelectBillYear(paymentInfo.YearBill);
            SetPayCode(paymentInfo.PayCode);
            SetPaymentPeriod(paymentInfo.Period);
            txtReference.Text = paymentInfo.Reference;
            txtTransactionDate.Text = paymentInfo.TransactionDate.FormatAndPadShortDate();
            txtCD.Text = paymentInfo.AffectCashDrawer;
            txtCash.Text = paymentInfo.AffectCash;
            txtAcctNumber.TextMatrix(0, 0, paymentInfo.Account);
            txtPrincipal.Text = paymentInfo.Principal.FormatAsMoney();
            txtInterest.Text = paymentInfo.Interest.FormatAsMoney();
            txtCosts.Text = paymentInfo.Costs.FormatAsMoney();
        }

        private void SetPaymentPeriod(string paymentPeriod)
        {
            var period = paymentPeriod;
            if (paymentPeriod == "A")
            {
                period = "Auto";
            }

            for (int x = 0; x < cmbPeriod.Items.Count; x++)
            {
                if (cmbPeriod.Items[x] == period)
                {
                    cmbPeriod.SelectedIndex = x;
                    break;
                }
            }
        }

        private void SetPaymentPeriod(int period)
        {
            if (period == 0)
            {
                SetPaymentPeriod("Auto");
            }
            else
            {
                SetPaymentPeriod(period.ToString());
            }
        }

        private void PaymentsGrid_Click(object sender, EventArgs e)
        {
            SelectPaymentsGridRow();
        }

        private void SelectPaymentsGridRow()
        {
            var row = PaymentsGrid.Row;
            var col = PaymentsGrid.Col;
            mnuFileReprintReceipt.Visible = false;
            if ( ViewModel.AllowPayments)
            {
                var rowInfo = (PaymentGridRowInfo) PaymentsGrid.RowData(row);
                if (rowInfo != null)
                {
                    if (rowInfo.RowType == PaymentGridRowType.Payment && ViewModel.OriginIsTaxCollections)
                    {
                        mnuFileReprintReceipt.Visible = true;
                    }

                    if (col == (int) PaymentColumns.TaxYear)
                    {
                        if ( rowInfo.RowType == PaymentGridRowType.Header)
                        {
                            if (rowInfo.YearBill > 0)
                            {
                                SelectBillYear(rowInfo.YearBill);
                            }
                        }
                    }
                }
                
            }
        }

        private void SelectBillYear(int yearBill)
        {
            foreach (DescriptionIDPair yearPair in cmbYear.Items)
            {
                if (yearPair.ID == yearBill)
                {
                    cmbYear.SelectedItem = yearPair;
                }
            }
        }

        private void PaymentsGrid_RowExpanded(object sender, Wisej.Web.DataGridViewRowEventArgs e)
        {
            var row = PaymentsGrid.GetFlexRowIndex(e.RowIndex);
            var rowInfo = (PaymentGridRowInfo) PaymentsGrid.RowData(row);
            if (rowInfo != null && rowInfo.RowType == PaymentGridRowType.Header)
            {
                ReShowHeader(row);
            }
        }

        private void PaymentsGrid_RowCollapsed(object sender, Wisej.Web.DataGridViewRowEventArgs e)
        {
            var row = PaymentsGrid.GetFlexRowIndex(e.RowIndex);
            var rowInfo = (PaymentGridRowInfo) PaymentsGrid.RowData(row);
            if (rowInfo != null && rowInfo.RowType == PaymentGridRowType.Header)
            {
                ReShowHeader(row);
            }
        }

        private void WireUpPaymentHandlers()
        {
            this.cmbYear.SelectedIndexChanged += CmbYear_SelectedIndexChanged;
            this.cmbCode.SelectedIndexChanged += CmbCode_SelectedIndexChanged;
            this.cmbCode.Validating += CmbCode_Validating;
            btnAddPayment.Click += BtnAddPayment_Click;
            btnSaveExit.Click += BtnSaveExit_Click;
            this.mnuPaymentClearPayment.Click += MnuPaymentClearPayment_Click;
            cmdPrintReceipt.Click += CmdPrintReceipt_Click;
            mnuFileReprintReceipt.Click += MnuFileReprintReceipt_Click;
            lblTaxClub.DoubleClick += LblTaxClub_DoubleClick;
            this.mnuRedistributeCredits.Click += MnuRedistributeCredits_Click;
            PeriodGrid.DoubleClick += PeriodGrid_DoubleClick;
        }

        private void PeriodGrid_DoubleClick(object sender, EventArgs e)
        {
            InsertAutoPayment();
        }

        private void InsertAutoPayment()
        {
            if (ViewModel.AllowPayments)
            {
                var yearPair = (DescriptionIDPair)cmbYear.SelectedItem;
                if (yearPair != null)
                {
                    if (yearPair.Description.ToLower() == "auto")
                    {
                        if (ViewModel.AccountSummary.Balance > 0)
                        {
                            SetPayCode(PropertyTaxPaymentCode.RegularPayment);
                            txtAmount.Text = ViewModel.AccountSummary.Balance.FormatAsMoney();
                        }
                    }
                    else
                    {
                        var bill = ViewModel.GetBill(yearPair.ID);
                       // TaxBillCalculationSummary bSum;
                       if (ViewModel.CalculationSummaries.TryGetValue(bill.ID, out var bSum))
                       {
                           FillPaymentControlsFromSubtotal(bSum, bill.YearBill());
                       }
                    }
                }
            }
        }

        private void MnuRedistributeCredits_Click(object sender, EventArgs e)
        {

            ApplyCredits();
        }

        private void ApplyCredits()
        {
            //if (FCMessageBox.Show(
            //        "This will apply payments to redistribute negative balances to other bills.\nAre you sure you want to continue?",
            //        MsgBoxStyle.YesNo | MsgBoxStyle.Question, "Apply Negative Balances?") != DialogResult.Yes)
            //{
            //    return;
            //}
            var creditInfo = ViewModel.GetCreditsOnOlderYears();
            if (!creditInfo.HasCreditsOnOlderYears)
            {
                FCMessageBox.Show("There are no credits to redistribute to other tax years", MsgBoxStyle.OkOnly,
                    "No Credits");
                return;
            }

            bool addPrepay = false;
            if (!creditInfo.PrePayExists && creditInfo.OlderCredits > creditInfo.TotalPositive)
            {
                addPrepay = FCMessageBox.Show(
                    "This will leave an overpayment.\nWould you like to create a new billing year to apply the overpayment to?", MsgBoxStyle.YesNo | MsgBoxStyle.Question,"Overpayment") == DialogResult.Yes;
            }

            bool applyOverpayToLastBill = !creditInfo.PrePayExists && !addPrepay;
            ViewModel.ApplyCredits(applyOverpayToLastBill);
            if (ViewModel.BillType == PropertyTaxBillType.Personal)
            {
                ShowBills(ViewModel.PPTaxAccount);
            }
            else
            {
                ShowBills(ViewModel.RETaxAccount);
            }
            PaymentsGrid.Outline(1);
            ExpandYearsWithPending();
        }

        private void LblTaxClub_DoubleClick(object sender, EventArgs e)
        {
            if (lblTaxClub.Tag == null)
            {
                return;
            }
            var taxClub = (ValueTuple<int ,double >)lblTaxClub.Tag;
            var yearBill = taxClub.Item1;
            if (yearBill == 0)
            {
                return;
            }

            var payment = taxClub.Item2;
            if (payment == 0)
            {
                return;
            }
            txtReference.Text = "TAXCLU";
            SetPayCode(PropertyTaxPaymentCode.TaxClubPayment);
            txtInterest.Text = "0.00";
            txtCosts.Text = "0.00";
            txtAmount.Text = "0.00";
            txtPrincipal.Text = payment.FormatAsMoney();
        }

        private void CmbCode_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (cmbCode.SelectedItem == null)
            {
                return;
            }
            if (cmbYear.SelectedItem != null)
            {
                var yearPair = (DescriptionIDPair)cmbYear.SelectedItem;
                
                var payCodePair = (GenericDescriptionPair<PropertyTaxPaymentCode>)cmbCode.SelectedItem;
                if (yearPair.Description.ToLower() == "auto")
                {
                    if (payCodePair.ID != PropertyTaxPaymentCode.RegularPayment)
                    {
                        e.Cancel = true;
                        return;
                    }
                }
                else if (yearPair.Description.Contains("*"))
                {
                    if (payCodePair.ID != PropertyTaxPaymentCode.PrePayment &&
                        payCodePair.ID != PropertyTaxPaymentCode.Discount &&
                        payCodePair.ID != PropertyTaxPaymentCode.TaxClubPayment)
                    {
                        e.Cancel = true;
                        return;
                    }
                }
                else
                {
                    if (payCodePair.ID == PropertyTaxPaymentCode.PrePayment)
                    {
                        e.Cancel = true;
                        return;
                    }
                }
                
            }
        }

        private void MnuFileReprintReceipt_Click(object sender, EventArgs e)
        {
            if (ViewModel.OriginIsTaxCollections)
            {
                ReprintReceipt();
            }
        }

        private void ReprintReceipt()
        {
            int row = PaymentsGrid.Row;
            if (row > 0)
            {
                var rowInfo = (PaymentGridRowInfo)PaymentsGrid.RowData(row);
                if (rowInfo.RowType == PaymentGridRowType.Payment)
                {
                    var payment = rowInfo?.Payment;
                    if (payment != null)
                    {
                        if (payment.IsCreatedInterest())
                        {
                            return;
                        }
                    
                        ViewModel.ReprintReceipt(payment);
                    }
                }
            }
        }

        private void CmdPrintReceipt_Click(object sender, EventArgs e)
        {
            SaveAndPrintReceipt();

        }

        private void SaveAndPrintReceipt()
        {
            if (CompleteTransaction(true))
            {
                ViewModel.LoadAccount();
                ShowAccount();
            }
        }


        private void MnuPaymentClearPayment_Click(object sender, EventArgs e)
        {
            ClearPaymentBoxes();
        }

        private void BtnSaveExit_Click(object sender, EventArgs e)
        {
            if (CompleteTransaction(false))
            {
                CloseWithoutCancel();
            }
        }

        private bool CompleteTransaction(bool printReceipt)
        {
            if (AddPendingPayment())
            {
                ViewModel.CompleteTransaction(printReceipt);
                return true;
            }
            else
            {
                return false;
            }
        }

        private void BtnAddPayment_Click(object sender, EventArgs e)
        {           
            AddPendingPayment();
        }

        private void CmbCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!PayCodeSelectionChanged())
            {
                
            }
        }

        private bool PayCodeSelectionChanged()
        {
            if (cmbCode.SelectedIndex == -1)
            {
                return false;
            }

            if (cmbCode.SelectedItem == null)
            {
                return false;
            }

            if (cmbYear.SelectedItem == null)
            {
                return false;
            }

            var yearPair = (DescriptionIDPair)cmbYear.SelectedItem;
            var payCodePair = (GenericDescriptionPair<PropertyTaxPaymentCode>)cmbCode.SelectedItem;
            var paymentVisibility = true;
            if (yearPair.ID != 0) //not Auto
            {
                var bill = ViewModel.GetBill(yearPair.ID);
                if (bill != null)
                {
                    if (ViewModel.ValidateYearAndCode(yearPair.ID, (PropertyTaxPaymentCode) payCodePair.ID))
                    {
                        if (payCodePair.ID == PropertyTaxPaymentCode.Abatement ||
                            payCodePair.ID == PropertyTaxPaymentCode.RefundedAbatement)
                        {
                            txtCD.Text = "N";
                            txtCash.Text = "N";                            
                            var acctNumber = ViewModel.GetCLAccount(payCodePair.ID, bill.TaxYear, bill.HasLien(), ViewModel.BillType == PropertyTaxBillType.Personal ? false : ViewModel.RETaxAccount.AccountBriefInfo.IsTaxAcquired);

                            txtAcctNumber.TextMatrix(0, 0, acctNumber);

                            paymentVisibility = false;
                        }
                        else if (payCodePair.ID == PropertyTaxPaymentCode.Discount)
                        {
                            //if (cmbPeriod.Text == "A")
                            //{
                                // PopUpDiscount
                                decimal interestCharged = 0;
                                decimal interestAndCostsOwed = 0;
                                if (bill.HasLien())
                                {
                                    interestCharged = bill.Lien.LienSummary.AdjustedInterestCharged;
                                    interestAndCostsOwed =
                                        bill.Lien.LienSummary.GetNetCosts() + bill.Lien.LienSummary.GetNetInterest();
                                }
                                else
                                {
                                    interestCharged = bill.BillSummary.InterestCharged;
                                    interestAndCostsOwed =
                                        bill.BillSummary.GetNetCosts() + bill.BillSummary.GetNetInterest();
                                }

                                if (interestCharged != 0)
                                {
                                    if (interestAndCostsOwed > 0)
                                    {
                                        if (FCMessageBox.Show(
                                                @"There is outstanding interest and/or costs. Are you sure this is a valid discount?",
                                                MsgBoxStyle.YesNo | MsgBoxStyle.Exclamation, "Interest And Cost Warning") !=
                                            DialogResult.Yes)
                                        {
                                            SetPayCode(PropertyTaxPaymentCode.RegularPayment);
                                            return false;
                                        }
                                }
                                    else
                                    {
                                        if (FCMessageBox.Show(
                                                "There is interest on this account. Are you sure that you would like to continue?",
                                                MsgBoxStyle.YesNo | MsgBoxStyle.Exclamation, "Account Accruing Interest") !=
                                            DialogResult.Yes)
                                        {
                                            SetPayCode(PropertyTaxPaymentCode.RegularPayment);
                                            return false;
                                        }
                                    }
                                    
                                }
                                var result = ViewModel.GetDiscount(bill.ID);
                                if (!result.Cancelled && result.Principal + result.Discount != 0)
                                {                                    
                                    AddDiscountPayment(result.Principal, result.Discount,bill.RateId == 0,bill.HasLien());
                                    return true;
                                }
                                else
                                {
                                    if (bill.RateId > 0)
                                    {
                                        SetPayCode(PropertyTaxPaymentCode.RegularPayment);
                                    }
                                    else
                                    {
                                        SetPayCode(PropertyTaxPaymentCode.PrePayment);
                                    }
                                }
                            //var acctNumber = "";
                            //    acctNumber = ViewModel.GetCLAccount(payCodePair.ID, bill.TaxYear, false);
                            //    txtAcctNumber.TextMatrix(0, 0, acctNumber);

                            //txtAcctNumber.TextMatrix(0, 0,
                            //    ViewModel.GetCLAccount(PropertyTaxPaymentCode.Discount, bill.TaxYear, false));
                        }
                        else if (payCodePair.ID == PropertyTaxPaymentCode.RegularPayment)
                        {
                            //select period
                            cmbPeriod.SelectedIndex = cmbPeriod.ListCount - 1;
                            txtCash.Text = "Y";
                            txtCD.Text = "Y";
                            txtAcctNumber.TextMatrix(0, 0, "");
                            txtAcctNumber.Enabled = false;
                        }
                        else if (payCodePair.ID == PropertyTaxPaymentCode.TaxClubPayment)
                        {
                            txtCash.Text = "Y";
                            txtCD.Text = "Y";
                            txtAcctNumber.TextMatrix(0, 0, "");
                            txtAcctNumber.Enabled = false;
                            paymentVisibility = false;
                        }
                        else if (payCodePair.ID == PropertyTaxPaymentCode.PrePayment)
                        {
                            changingPayCode = true;
                            cmbPeriod.SelectedIndex = cmbPeriod.ListCount - 1;
                            txtCash.Text = "Y";
                            txtCD.Text = "Y";
                            txtAcctNumber.TextMatrix(0, 0, "");
                            txtAcctNumber.Enabled = false;
                            changingPayCode = false;
                        }
                        else if (payCodePair.ID == PropertyTaxPaymentCode.Correction)
                        {
                            txtCash.Text = "Y";
                            txtCD.Text = "Y";
                            txtAcctNumber.TextMatrix(0, 0, "");
                            txtAcctNumber.Enabled = false;
                            cmbPeriod.SelectedIndex = cmbPeriod.ListCount - 1;
                            paymentVisibility = false;
                        }
                        else if (payCodePair.ID == PropertyTaxPaymentCode.NonBudgetary)
                        {
                            var result = ViewModel.GetNonBudgetaryPayment(bill.ID);

                            if (!result.Cancelled && result.Amounts.IsNonZero())
                            {
                                AddNonBudgetaryPayment(result.Amounts,bill.HasLien());
                                return true;
                            }
                            else
                            {
                                SetPayCode(PropertyTaxPaymentCode.RegularPayment);
                            }

                        }
                        else if (payCodePair.ID == PropertyTaxPaymentCode.OverpayRefund)
                        {
                            var refundValidation = ValidateRefund(bill);
                            if (refundValidation.Valid)
                            {

                                txtAcctNumber.TextMatrix(0, 0,
                                    ViewModel.GetCLAccount(PropertyTaxPaymentCode.OverpayRefund, bill.YearBill(),
                                        bill.HasLien(), ViewModel.BillType == PropertyTaxBillType.Personal ? false : ViewModel.RETaxAccount.AccountBriefInfo.IsTaxAcquired));

                                txtCash.Text = "N";
                                txtCD.Text = "N";
                                txtReference.Text = "Refund";
                                txtAmount.Text = refundValidation.Principal.FormatAsMoney();
                            }
                            else
                            {
                                FCMessageBox.Show(refundValidation.Message,
                                    MsgBoxStyle.OkOnly | MsgBoxStyle.Exclamation, refundValidation.Title);
                            }
                        }
                        else
                        {
                            cmbPeriod.SelectedIndex = cmbPeriod.ListCount - 1;
                            txtCash.Text = "Y";
                            txtCD.Text = "Y";
                            txtAcctNumber.TextMatrix(0, 0, "");
                            txtAcctNumber.Enabled = false;
                        }
                    }
                    else
                    {
                        changingPayCode = true;
                        SetPayCode(PropertyTaxPaymentCode.RegularPayment);
                        changingPayCode = false;
                    }
                }
            }
            SetPaymentBoxesVisibility(paymentVisibility);
            CheckCD();
            CheckCash();
            CheckPeriod();
            return true;
        }

        private (Boolean Valid,decimal Principal,string Message,string Title) ValidateRefund(PropertyTaxBill bill)
        {
            decimal prinOwed = 0;
            if (bill.HasLien())
            {
                prinOwed = bill.Lien.LienSummary.GetNetPrincipal();
            }
            else
            {
                prinOwed = bill.BillSummary.GetNetPrincipal();
            }

            if (prinOwed == 0)
            {
                return (Valid: false, prinOwed, "Principal balance is zero. Refund can only be made on principal.","Zero Balance");
            }

            if (prinOwed > 0)
            {
                return (Valid: false, prinOwed, "Credit balance does not exist for this bill.","Balance Due");
            }

            return (Valid: true, prinOwed, "","");
        }

        private void AddDiscountPayment(decimal principal, decimal discount, bool prepay, bool isLien)
        {
            var paymentInfo = new PaymentSetupInfo()
            {
                Account = "",
                PayCode = prepay ? PropertyTaxPaymentCode.PrePayment : PropertyTaxPaymentCode.RegularPayment,
                AffectCashDrawer = txtCD.Text,
                Comment = txtComments.Text,
                AffectCash = "Y",
                Principal = principal,
                PrelienInterest = 0,
                Interest = 0,
                Period = 0,
                Costs = 0,
                Reference = txtReference.Text,
                TransactionDate = txtTransactionDate.Text.ToDate(),
                YearBill = ((DescriptionIDPair) cmbYear.SelectedItem).ID,
                ForcePaymentDistributionAsIs = true
            };
            var discountPayment = new PaymentSetupInfo()
            {

                PayCode = PropertyTaxPaymentCode.Discount,
                AffectCashDrawer = "N",
                Account = ViewModel.GetCLAccount(PropertyTaxPaymentCode.Discount,
                    ((DescriptionIDPair) cmbYear.SelectedItem).ID, isLien, ViewModel.BillType == PropertyTaxBillType.Personal ? false : ViewModel.RETaxAccount.AccountBriefInfo.IsTaxAcquired),
                Comment = txtComments.Text,
                AffectCash = "N",
                Period = 0,
                PrelienInterest = 0,
                Principal = discount,
                Reference = "DISCOUNT",
                TransactionDate = txtTransactionDate.Text.ToDate(),
                YearBill = ((DescriptionIDPair) cmbYear.SelectedItem).ID
            };


            var result = ViewModel.AddPendingPayment(paymentInfo);
            if (!result.success)
            {
                FCMessageBox.Show(result.message, MsgBoxStyle.OkOnly | MsgBoxStyle.Critical, result.caption);
                return;
            }
            result = ViewModel.AddPendingPayment(discountPayment);
            if (!result.success)
            {
                FCMessageBox.Show(result.message, MsgBoxStyle.OkOnly | MsgBoxStyle.Critical, result.caption);
                return;
            }
            if (ViewModel.BillType == PropertyTaxBillType.Personal)
            {
                ShowBills(ViewModel.PPTaxAccount);
            }
            else
            {
                ShowBills(ViewModel.RETaxAccount);
            }
            PaymentsGrid.Outline(1);
            ExpandYearsWithPending();
        }
        private void AddNonBudgetaryPayment(PropertyTaxAmounts amounts, bool isLien)
        {
            if (amounts.Principal != 0)
            {
                var paymentInfo = new PaymentSetupInfo()
                {
                    Account = ViewModel.GetCLAccount(PropertyTaxPaymentCode.NonBudgetary,
                        ((DescriptionIDPair)cmbYear.SelectedItem).ID, isLien, ViewModel.BillType == PropertyTaxBillType.Personal ? false : ViewModel.RETaxAccount.AccountBriefInfo.IsTaxAcquired, PropertyTaxNonBudgetaryPaymentType.Principal),
                    Principal = amounts.Principal,
                    Costs = 0,
                    Interest = 0,
                    PrelienInterest = 0,
                    AffectCash = "N",
                    AffectCashDrawer = "N",
                    Comment = txtComments.Text,
                    PayCode = PropertyTaxPaymentCode.NonBudgetary,
                    Period = 0,
                    Reference = "NB",
                    TransactionDate = txtTransactionDate.Text.ToDate(),
                    YearBill = ((DescriptionIDPair)cmbYear.SelectedItem).ID
                };
                AddPayment(paymentInfo);
            }

            if (amounts.Costs != 0)
            {
                var paymentInfo = new PaymentSetupInfo()
                {
                    Account = ViewModel.GetCLAccount(PropertyTaxPaymentCode.NonBudgetary,
                        ((DescriptionIDPair)cmbYear.SelectedItem).ID, isLien, ViewModel.BillType == PropertyTaxBillType.Personal ? false : ViewModel.RETaxAccount.AccountBriefInfo.IsTaxAcquired, PropertyTaxNonBudgetaryPaymentType.Cost),
                    Principal = 0,
                    Costs = amounts.Costs,
                    Interest = 0,
                    PrelienInterest = 0,
                    AffectCash = "N",
                    AffectCashDrawer = "N",
                    Comment = txtComments.Text,
                    PayCode = PropertyTaxPaymentCode.NonBudgetary,
                    Period = 0,
                    Reference = "NB",
                    TransactionDate = txtTransactionDate.Text.ToDate(),
                    YearBill = ((DescriptionIDPair)cmbYear.SelectedItem).ID
                };
                AddPayment(paymentInfo);
            }

            if (amounts.Interest != 0)
            {
                var paymentInfo = new PaymentSetupInfo()
                {
                    Account = ViewModel.GetCLAccount(PropertyTaxPaymentCode.NonBudgetary,
                        ((DescriptionIDPair)cmbYear.SelectedItem).ID, isLien, ViewModel.BillType == PropertyTaxBillType.Personal ? false : ViewModel.RETaxAccount.AccountBriefInfo.IsTaxAcquired, PropertyTaxNonBudgetaryPaymentType.Interest),
                    Principal = 0,
                    Costs = 0,
                    Interest = amounts.Interest,
                    PrelienInterest = 0,
                    AffectCash = "N",
                    AffectCashDrawer = "N",
                    Comment = txtComments.Text,
                    PayCode = PropertyTaxPaymentCode.NonBudgetary,
                    Period = 0,
                    Reference = "NB",
                    TransactionDate = txtTransactionDate.Text.ToDate(),
                    YearBill = ((DescriptionIDPair)cmbYear.SelectedItem).ID
                };
                AddPayment(paymentInfo);
            }

            if (amounts.PreLienInterest != 0)
            {
                var paymentInfo = new PaymentSetupInfo()
                {
                    Account = ViewModel.GetCLAccount(PropertyTaxPaymentCode.NonBudgetary,
                        ((DescriptionIDPair)cmbYear.SelectedItem).ID, isLien, ViewModel.BillType == PropertyTaxBillType.Personal ? false : ViewModel.RETaxAccount.AccountBriefInfo.IsTaxAcquired, PropertyTaxNonBudgetaryPaymentType.PreLienInterest),
                    Principal = 0,
                    Costs = 0,
                    Interest = 0,
                    PrelienInterest = amounts.PreLienInterest,
                    AffectCash = "N",
                    AffectCashDrawer = "N",
                    Comment = txtComments.Text,
                    PayCode = PropertyTaxPaymentCode.NonBudgetary,
                    Period = 0,
                    Reference = "NB",
                    TransactionDate = txtTransactionDate.Text.ToDate(),
                    YearBill = ((DescriptionIDPair)cmbYear.SelectedItem).ID
                };
                AddPayment(paymentInfo);
            }
            SetPaymentDefaults();
        }

        private void SetPaymentBoxesVisibility(bool showAmountOnly)
        {
            PrincipalPanel.Visible = !showAmountOnly;
            InterestPanel.Visible = !showAmountOnly;
            CostsPanel.Visible = !showAmountOnly;
            AmountPanel.Visible = showAmountOnly;
            
            if (showAmountOnly)
            {
                var total = txtPrincipal.Text.ToDecimalValue() + txtCosts.Text.ToDecimalValue();
                txtAmount.Text = total.ToString();
            }

        }

        private void CmbYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            YearComboSelectionChanged();
        }


        private void YearComboSelectionChanged()
        {
            if (cmbYear.SelectedIndex == -1)
            {
                return;
            }

            if (cmbYear.SelectedItem == null)
            {
                return;
            }

            var yearPair = (DescriptionIDPair)cmbYear.SelectedItem;
            //var payCodePair = (DescriptionEnumPair<PropertyTaxPaymentCode>)cmbCode.SelectedItem;
            ClearPeriodToolTips();
            if (yearPair.ID == 0) //If set to Auto
            {
                FillPayCodeCombo(PaymentYearType.Auto);
                SetPayCode(PropertyTaxPaymentCode.RegularPayment);
                var payCodePair = (GenericDescriptionPair<PropertyTaxPaymentCode>)cmbCode.SelectedItem;
                FillInPeriodGrid(ViewModel.AccountSummary.Balance, 0, 0, 0, true, 1);
            }
            else
            {
                if (cmbCode.SelectedItem == null)
                {
                    return;
                }
                var payCodePair = (GenericDescriptionPair<PropertyTaxPaymentCode>)cmbCode.SelectedItem;
                if (yearPair.Description.Contains("*"))
                {
                    FillPayCodeCombo(PaymentYearType.Prepay);
                    if (!IsValidPayCode(payCodePair.ID))
                    {
                        SetPayCode(PropertyTaxPaymentCode.PrePayment);
                        payCodePair = (GenericDescriptionPair<PropertyTaxPaymentCode>)cmbCode.SelectedItem;
                    }
                    else
                    {
                        SetPayCode(payCodePair.ID);
                        payCodePair = (GenericDescriptionPair<PropertyTaxPaymentCode>)cmbCode.SelectedItem;
                    }
                }
                else
                {
                    FillPayCodeCombo(PaymentYearType.Regular);
                    if (!IsValidPayCode(payCodePair.ID))
                    {
                        SetPayCode(PropertyTaxPaymentCode.RegularPayment);
                        payCodePair = (GenericDescriptionPair<PropertyTaxPaymentCode>)cmbCode.SelectedItem;
                    }
                    else
                    {
                        SetPayCode(payCodePair.ID);
                        payCodePair = (GenericDescriptionPair<PropertyTaxPaymentCode>)cmbCode.SelectedItem;
                    }
                }
                var bill = ViewModel.GetBill(yearPair.ID);
                if (bill != null)
                {
                    CheckTaxClubs(bill);
                    if (bill.RateRecord != null)
                    {
                        var rateRec = bill.RateRecord;
                        foreach (var installment in rateRec.Installments)
                        {
                            periodToolTips[installment.InstallmentNumber - 1] =
                                installment.DueDate.FormatAndPadShortDate();
                        }
                    }

                    var acctNumber = "";
                    FillPeriodCombo(0);
                    if (bill.HasLien())
                    {
                        if (payCodePair.ID == PropertyTaxPaymentCode.Abatement)
                        {
                            acctNumber = ViewModel.GetCLAccount(payCodePair.ID, bill.TaxYear, true, ViewModel.BillType == PropertyTaxBillType.Personal ? false : ViewModel.RETaxAccount.AccountBriefInfo.IsTaxAcquired);
                        }
                        decimal extraInterest = 0;
                        if (ViewModel.CalculationSummaries.ContainsKey(bill.ID))
                        {
                            var summary = ViewModel.CalculationSummaries[bill.ID];
                            extraInterest = summary.Interest;
                        }
                        FillInPeriodGrid(bill.Lien.LienSummary.GetNetOwed() + extraInterest, 0, 0, 0, false, 1);
                        
                    }
                    else
                    {
                        acctNumber = ViewModel.GetCLAccount(payCodePair.ID, bill.TaxYear, false, ViewModel.BillType == PropertyTaxBillType.Personal ? false : ViewModel.RETaxAccount.AccountBriefInfo.IsTaxAcquired);
                        if (bill.HasTaxClub())
                        {
                            SetPayCode(PropertyTaxPaymentCode.TaxClubPayment);
                        }
                        else if (payCodePair.ID == PropertyTaxPaymentCode.TaxClubPayment)
                        {
                            SetPayCode(PropertyTaxPaymentCode.RegularPayment);
                        }

                        decimal extraInterest = 0;
                        if (ViewModel.CalculationSummaries.ContainsKey(bill.ID))
                        {
                            var summary = ViewModel.CalculationSummaries[bill.ID];
                            extraInterest = summary.Interest;
                        }
                        var amountsDue = bill.GetInstallmentsDue(extraInterest);
                        decimal[] amounts = new decimal[4] { 0, 0, 0, 0 };
                        var amountIndex = 0;
                        foreach (var amountDue in amountsDue)
                        {
                            amounts[amountIndex] = amountDue;
                            amountIndex += 1;
                        }

                        FillInPeriodGrid(amounts[0], amounts[1], amounts[2], amounts[3], false, amountsDue.Count());
                    }

                    txtAcctNumber.TextMatrix(0, 0, acctNumber);
                }
            }
        }

        private bool IsValidPayCode(PropertyTaxPaymentCode payCode)
        {
            foreach (GenericDescriptionPair<PropertyTaxPaymentCode> pair in cmbCode.Items)
            {
                if (pair.ID == payCode)
                {
                    return true;
                }
            }

            return false;
        }
        private void CheckTaxClubs(PropertyTaxBill bill)
        {
            if (bill != null && bill.HasTaxClub() && bill.TaxClub != null)
            {
                var netLeft = bill.TaxClub.TotalPayments.GetValueOrDefault() -
                              bill.TaxClub.TotalPaid.GetValueOrDefault();
                if (netLeft > bill.TaxClub.PaymentAmount.GetValueOrDefault())
                {
                    lblTaxClub.Text =
                        "TC: " + bill.TaxClub.PaymentAmount.GetValueOrDefault().FormatAsCurrencyNoSymbol();
                    lblTaxClub.Tag = (yearBill: bill.YearBill(), payment: bill.TaxClub.PaymentAmount.GetValueOrDefault());
                }
                else
                {
                    lblTaxClub.Text = "TC: " + netLeft.FormatAsCurrencyNoSymbol();
                    lblTaxClub.Tag = (yearBill: bill.YearBill(), payment: netLeft);
                }

                // bill.TaxClub.Year.GetValueOrDefault();
                toolTip1.SetToolTip(lblTaxClub,"Tax Club: " + bill.TaxYear + "-" + bill.BillNumber);
                lblTaxClub.Visible = true;
            }
            else
            {
                lblTaxClub.Text = "";
                lblTaxClub.Tag = (yearBill: 0, payment: 0);
                lblTaxClub.Visible = false;
            }
        }

        private void ClearPeriodToolTips()
        {
            periodToolTips.Clear();
            periodToolTips.AddRange(new string[]{"", "", "", ""});
        }

        private void FillInPeriodGrid(decimal periodOne, decimal periodTwo, decimal periodThree, decimal periodFour, bool asTotalDue,int numberOfPeriods)
        {
            var prefix = "";
            if (asTotalDue)
            {
                PeriodGrid.TextMatrix(0, 0, "Total Due:");
            }
            else
            {
                PeriodGrid.TextMatrix(0, 0, "Period Due:");
            }
            if (periodOne != 0 || !asTotalDue)
            {
                if (!asTotalDue)
                {
                    prefix = "1) ";
                }
                PeriodGrid.TextMatrix(0, 1, prefix + periodOne.FormatAsCurrencyNoSymbol());
            }
            else
            {
                PeriodGrid.TextMatrix(0, 1, "");
            }
            if (numberOfPeriods > 1 && (periodTwo != 0 || !asTotalDue))
            {
                if (!asTotalDue)
                {
                    prefix = "2) ";
                }
                PeriodGrid.TextMatrix(0, 2, prefix + periodTwo.FormatAsCurrencyNoSymbol());
            }
            else
            {
                PeriodGrid.TextMatrix(0, 2, "");
            }
            if (numberOfPeriods > 2 && (periodThree != 0 || !asTotalDue))
            {
                if (!asTotalDue)
                {
                    prefix = "3) ";
                }
                PeriodGrid.TextMatrix(0, 1, prefix + periodThree.FormatAsCurrencyNoSymbol());
            }
            else
            {
                PeriodGrid.TextMatrix(0, 3, "");
            }
            if (numberOfPeriods > 3 && (periodFour != 0 || !asTotalDue))
            {
                if (!asTotalDue)
                {
                    prefix = "4) ";
                }
                PeriodGrid.TextMatrix(0, 1, prefix + periodFour.FormatAsCurrencyNoSymbol());
            }
            else
            {
                PeriodGrid.TextMatrix(0, 4, "");
            }
        }

        private void FrmTaxAccountStatus_Resize(object sender, EventArgs e)
        {
            ResizePaymentsGrid();
            if (ViewModel.AllowPayments)
            {
                ResizePendingGrid();
                ResizePeriodGrid();
            }
        }

        private void CmdProcessEffective_Click(object sender, EventArgs e)
        {
           ViewModel.ChangeEffectiveDate();
        }

        private void ImgNote_Click(object sender, EventArgs e)
        {
            this.imgNote.ImageSource = "imgnote?color=#707884";
        }

        private void MnuFileEditNote_Click(object sender, EventArgs e)
        {
           ShowNote();
        }

        private void ImgNote_DoubleClick(object sender, EventArgs e)
        {
            ShowNote();
        }

        private void ShowNote()
        {
            ViewModel.EditNote();
            CheckForNote();
        }

        private void CheckForNote()
        {
            if (ViewModel.HasNote())
            {
                imgNote.Visible = true;
            }
            else
            {
                imgNote.Visible = false;
            }
        }

        private void LblAcctComment_Click(object sender, EventArgs e)
        {
            EditComment();
        }

        private void MnuFileComment_Click(object sender, EventArgs e)
        {
            EditComment();
        }

        private void LblAcctComment_DoubleClick(object sender, EventArgs e)
        {
            EditComment();
        }

        private void EditComment()
        {
            ViewModel.EditComment();
            CheckForComment();
        }

        private void FrmTaxAccountStatus_Load(object sender, EventArgs e)
        {
           ViewModel.EffectiveDateChanged += ViewModel_EffectiveDateChanged;
           ClearPeriodToolTips();
           ShowControls();
           SetupPaymentsGrid();
           ResizePaymentsGrid();
           if (ViewModel.BillType == PropertyTaxBillType.Personal)
           {
               btnMortgageHolderInfo.Visible = false;
               btnMortgageHolderInfo.Enabled = false;
               mnuFileInterestedParty.Enabled = false;
               mnuFileInterestedParty.Visible = false;
               mnuFileDischarge.Visible = false;
               mnuFilePrintAltLDN.Visible = false;
               mnuFileREComment.Visible = false;
           }
           if (ViewModel.AllowPayments)
           {
               WireUpPaymentHandlers();
               SetupPendingGrid();
               ResizePendingGrid();
               FillPaymentControls();
               mnuPayment.Visible = true;
               if (ViewModel.OriginIsTaxCollections)
               {
                   cmdPrintReceipt.Visible = true;
                   cmdPrintReceipt.Enabled = true;
               }
           }
           else
           {
               cmdPrintReceipt.Enabled = false;
               cmdPrintReceipt.Visible = false;
               mnuPayment.Visible = false;
               BottomPanel.Height = 0;
           }

           ShowAccount();
           //if (ViewModel.HasCreditsOnOlderYears())
           //{
           //    if (FCMessageBox.Show(
           //            "You have some years with negative balances.\nWould you like to apply those to other balances now?\n You can always choose 'Apply Credits to Balances' from the menu to apply the appropriate entries later.",
           //            MsgBoxStyle.YesNo | MsgBoxStyle.Question, "Apply Credits To Balances") == DialogResult.Yes)
           //    {
           //        ApplyCredits();
           //    }
           //}
        }

        private void SetPaymentDefaults()
        {
            if (!ViewModel.AllowPayments)
            {
                return;
            }
            SetPaymentBoxesVisibility(true);
            SetYearCombo(0);
            txtTransactionDate.Text = ViewModel.RecordedTransactionDate.FormatAndPadShortDate();
            txtReference.Text = "";
            SetPaymentPeriod(0);
            SetPayCode(PropertyTaxPaymentCode.RegularPayment);
            txtCash.Text = "Y";
            txtCD.Text = "Y";
            txtInterest.Text = "0.00";
            txtPrincipal.Text = "0.00";
            txtCosts.Text = "0.00";
            txtAmount.Text = "0.00";
            txtComments.Text = "";            
            var defaultYear = ViewModel.GetDefaultTaxClubYear();
            if (defaultYear > 0)
            {
                SetYearCombo(defaultYear);                
            }
        }

        private void SetPaymentsForAuto()
        {
            if (!ViewModel.AllowPayments)
            {
                return;
            }
            SetPaymentBoxesVisibility(true);
            SetYearCombo(0);
            txtReference.Text = "";
            SetPaymentPeriod(0);
            SetPayCode(PropertyTaxPaymentCode.RegularPayment);
            txtCash.Text = "Y";
            txtCD.Text = "Y";
            txtInterest.Text = "0.00";
            txtPrincipal.Text = "0.00";
            txtCosts.Text = "0.00";
            txtAmount.Text = "0.00";
            txtComments.Text = "";
        }

        private void FillPaymentControls()
        {
            FillPeriodCombo();
            //FillPayCodeCombo();
            FillPayCodeCombo(PaymentYearType.None);
            FillYearCombo();
            SetupPeriodGrid();
            ResizePeriodGrid();
        }

        private void FillPeriodCombo()
        {
            cmbPeriod.Clear();
            cmbPeriod.AddItem("Auto");
            cmbPeriod.AddItem("1");
            cmbPeriod.AddItem("2");
            cmbPeriod.AddItem("3");
            cmbPeriod.AddItem("4");
        }

        private void FillPeriodCombo(int periods)
        {
            cmbPeriod.Clear();
            cmbPeriod.AddItem("Auto");
            if (periods > 1)
            {
                for (int x = 1; x <= periods; x++)
                {
                    cmbPeriod.AddItem(x.ToString());
                }
            }

            SetPaymentPeriod("Auto");
        }

        private void CheckPeriod()
        {
            var payCodePair = (GenericDescriptionPair<PropertyTaxPaymentCode>)cmbCode.SelectedItem;
            if (payCodePair != null)
            {
                if (payCodePair.ID == PropertyTaxPaymentCode.Abatement)
                {
                    FillPeriodComboByBill();
                    return;
                }
            }
            FillPeriodCombo(0);
        }
        private void FillPeriodComboByBill()
        {
            if (cmbYear.SelectedItem != null)
            {
                var periods = 0;
                var yearPair = (DescriptionIDPair)cmbYear.SelectedItem;
                if (yearPair.ID != 0)
                {
                    var bill = ViewModel.GetBill(yearPair.ID);
                    if (bill != null)
                    {
                        if (!bill.HasLien())
                        {
                            if (bill.RateRecord != null)
                            {
                                periods = bill.RateRecord.Installments.Count;
                            }
                        }
                    }
                }
                FillPeriodCombo(periods);               
            }
            else
            {
                FillPeriodCombo(0);
            }
            SetPaymentPeriod("Auto");
        }

        private void FillPayCodeCombo()
        {
            cmbCode.Items.Clear();
            cmbCode.DisplayMember = "Description";
            cmbCode.ValueMember = "ID";
            cmbCode.Items.Add(PropertyTaxPaymentCode.PrePayment.ToDescriptionEnumPair());
            cmbCode.Items.Add(PropertyTaxPaymentCode.Abatement.ToDescriptionEnumPair());
            cmbCode.Items.Add(PropertyTaxPaymentCode.Correction.ToDescriptionEnumPair());
            cmbCode.Items.Add(PropertyTaxPaymentCode.Discount.ToDescriptionEnumPair());
            cmbCode.Items.Add(PropertyTaxPaymentCode.RegularPayment.ToDescriptionEnumPair());
            cmbCode.Items.Add(PropertyTaxPaymentCode.RefundedAbatement.ToDescriptionEnumPair());
            cmbCode.Items.Add(PropertyTaxPaymentCode.TaxClubPayment.ToDescriptionEnumPair());
            if (!ViewModel.OriginIsTaxCollections)
            {
                cmbCode.Items.Add(PropertyTaxPaymentCode.NonBudgetary.ToDescriptionEnumPair());
                cmbCode.Items.Add(PropertyTaxPaymentCode.OverpayRefund.ToDescriptionEnumPair());
            }
        }

        private void FillPayCodeCombo(PaymentYearType yearType)
        {
            cmbCode.Items.Clear();
            cmbCode.DisplayMember = "Description";
            cmbCode.ValueMember = "ID";
            switch (yearType)
            {
                case PaymentYearType.Auto:
                    cmbCode.Items.Add(PropertyTaxPaymentCode.RegularPayment.ToDescriptionEnumPair());
                    break;
                case PaymentYearType.Prepay:
                    cmbCode.Items.Add(PropertyTaxPaymentCode.PrePayment.ToDescriptionEnumPair());
                    cmbCode.Items.Add(PropertyTaxPaymentCode.Discount.ToDescriptionEnumPair());
                    cmbCode.Items.Add(PropertyTaxPaymentCode.TaxClubPayment.ToDescriptionEnumPair());
                    break;
                case PaymentYearType.Regular:
                    cmbCode.Items.Add(PropertyTaxPaymentCode.Abatement.ToDescriptionEnumPair());
                    cmbCode.Items.Add(PropertyTaxPaymentCode.Correction.ToDescriptionEnumPair());
                    cmbCode.Items.Add(PropertyTaxPaymentCode.Discount.ToDescriptionEnumPair());
                    cmbCode.Items.Add(PropertyTaxPaymentCode.RegularPayment.ToDescriptionEnumPair());
                    cmbCode.Items.Add(PropertyTaxPaymentCode.RefundedAbatement.ToDescriptionEnumPair());
                    cmbCode.Items.Add(PropertyTaxPaymentCode.TaxClubPayment.ToDescriptionEnumPair());
                    if (!ViewModel.OriginIsTaxCollections)
                    {
                        cmbCode.Items.Add(PropertyTaxPaymentCode.NonBudgetary.ToDescriptionEnumPair());
                        cmbCode.Items.Add(PropertyTaxPaymentCode.OverpayRefund.ToDescriptionEnumPair());
                    }

                    break;
                default:
                    cmbCode.Items.Add(PropertyTaxPaymentCode.RegularPayment.ToDescriptionEnumPair());
                    break;
            }
        }

        private void FillYearCombo()
        {
           cmbYear.Items.Clear();
           var yearPairs = ViewModel.TaxYears;
           foreach (var yearPair in yearPairs)
           {
               cmbYear.Items.Add(yearPair);
           }

        }
        private void ViewModel_EffectiveDateChanged(object sender, EventArgs e)
        {
            ShowAccount();
        }

        public frmTaxAccountStatus(ITaxAccountStatusViewModel viewModel) : this()
        {
            ViewModel = viewModel;           
        }

        public ITaxAccountStatusViewModel ViewModel { get; set; }

        private void ShowAccount()
        {
            lblAccount.Text = ViewModel.Account.ToString();
            txtAccountTotalsAsOf.Text = "Account Totals As Of " + ViewModel.EffectiveDate.FormatAndPadShortDate();
            CheckIndicationIcons();
            AccountInfoPanel.Controls.Clear();
            if (ViewModel.ArePreviousAccountsInList())
            {
                mnuFilePreviousAccount.Visible = true;
                mnuFilePreviousAccount.Enabled = true;
            }
            else
            {
                mnuFilePreviousAccount.Visible = false;
                mnuFilePreviousAccount.Enabled = false;
            }

            if (ViewModel.AreFollowingAccountsInList())
            {
                mnuFileNextAccount.Visible = true;
                mnuFileNextAccount.Enabled = true;
            }
            else
            {
                mnuFileNextAccount.Visible = false;
                mnuFileNextAccount.Enabled = false;
            }

            switch (ViewModel.BillType)
            {
                case PropertyTaxBillType.Real:
                    AccountInfoPanel.Controls.Add(new REAccountIdentifyingInfoView(
                        ViewModel.RETaxAccount.AccountBriefInfo, ViewModel.RETaxAccount.AccountTotals.Acreage));
                    var totalsView = new REAccountTotalsView(ViewModel.RETaxAccount.AccountTotals,
                        ViewModel.RETaxAccount.LatestBillTotals, ViewModel.LatestBilledYear);
                    totalsView.Anchor = AnchorStyles.Top & AnchorStyles.Right;
                    AccountInfoPanel.Controls.Add(totalsView);
                    ShowBills(ViewModel.RETaxAccount);
                    break;
                case PropertyTaxBillType.Personal:
                    AccountInfoPanel.Controls.Add(
                        new PPAccountIdentifyingInfoView(ViewModel.PPTaxAccount.AccountBriefInfo));
                    var ppTotalsView = new PPAccountTotalsView(ViewModel.PPTaxAccount.AccountTotals,
                        ViewModel.PPTaxAccount.LatestBillTotals, ViewModel.LatestBilledYear);
                    ppTotalsView.Anchor = AnchorStyles.Top & AnchorStyles.Right;
                    AccountInfoPanel.Controls.Add(ppTotalsView);
                    ShowBills(ViewModel.PPTaxAccount);
                    break;
            }

            ColorGridPayments();
            PaymentsGrid.Outline(1);
            ExpandYearsWithPending();
            SetPaymentDefaults();

            lblGrpInfo.ToolTipText = "";
            if (ViewModel.GroupInfo != null)
            {
                lblGrpInfo.Visible = true;
                var groupSummary = ViewModel.GetGroupSummary(ViewModel.EffectiveDate);
                var comma = "";
                if (groupSummary != null && groupSummary.AccountSummaries.Any())
                {
                    if (groupSummary.AccountSummaries.Any())
                    {
                        var groupToolTipText = "Group Balance (";
                        foreach (var accountSummary in groupSummary.AccountSummaries.OrderBy(x => x.AccountType.ToAbbreviation()).ThenBy(y => y.Account))
                        {
                            groupToolTipText += comma + accountSummary.AccountType.ToAbbreviation() + " #" +
                                                accountSummary.Account;
                            comma = ", ";

                            if (ViewModel.ModuleAssociationInfo != null)
                            {
                                if (ViewModel.BillType == PropertyTaxBillType.Real)
                                {
                                    if (accountSummary.Account == ViewModel.ModuleAssociationInfo.Account)
                                    {
                                        lblAssocAcct.ToolTipText =
                                            "Associated PP Account: " + accountSummary.Account +
                                            "  Amount Outstanding: " + accountSummary.Balance.ToString("$#,##0.00");
                                    }
                                }
                                else
                                {
                                    if (accountSummary.Account == ViewModel.ModuleAssociationInfo.REMasterAcct)
                                    {
                                        lblAssocAcct.ToolTipText =
                                            "Associated RE Account: " + accountSummary.Account +
                                            "  Amount Outstanding: " + accountSummary.Balance.ToString("$#,##0.00");
                                    }
                                }
                                
                            }
                        }
                        groupToolTipText += ") is " + groupSummary.Balance.FormatAsCurrencyNoSymbol();
                        lblGrpInfo.ToolTipText = groupToolTipText;
                    }
                }
            }
            else
            {
                lblGrpInfo.Visible = false;
            }

            if (ViewModel.ModuleAssociationInfo != null)
            {
                lblAssocAcct.Text = ViewModel.BillType == PropertyTaxBillType.Real ? "PP" : "RE";
                lblAssocAcct.Visible = true;
            }
            else
            {
                lblAssocAcct.Visible = false;
            }
        }

        private void ColorGridPayments()
        {
            GridColorUtility.ColorGrid(PaymentsGrid,1,PaymentsGrid.Rows - 1,0, PaymentsGrid.Cols - 1,false);
        }
        private void CheckIndicationIcons()
        {
            CheckForComment();
            CheckForNote();
            CheckForTaxAcquired();
        }
        private void CheckForComment()
        {
            if (ViewModel.HasComment())
            {
                lblAcctComment.Visible = true;
            }
            else
            {
                lblAcctComment.Visible = false;
            }
        }

        private void CheckForTaxAcquired()
        {
            if (ViewModel.BillType == PropertyTaxBillType.Real)
            {
                lblTaxAcquired.Visible = ViewModel.RETaxAccount.AccountBriefInfo.IsTaxAcquired;
            }
            else
            {
                lblTaxAcquired.Visible = false;
            }
        }

        private void SetupPaymentsGrid()
        {
            PaymentsGrid.Visible = true;
            PaymentsGrid.Cols = 11;
            PaymentsGrid.Rows = 1;
            PaymentsGrid.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
            PaymentsGrid.TextMatrix(0, (int) PaymentColumns.TaxYear, "Year");
            PaymentsGrid.TextMatrix(0, (int) PaymentColumns.Date, "Date");
            PaymentsGrid.TextMatrix(0, (int) PaymentColumns.PayCode, "C");
            PaymentsGrid.TextMatrix(0, (int) PaymentColumns.Period, "P");
            PaymentsGrid.TextMatrix(0, (int) PaymentColumns.Reference, "Ref");
            PaymentsGrid.TextMatrix(0, (int) PaymentColumns.Principal, "Principal");
            PaymentsGrid.TextMatrix(0, (int) PaymentColumns.Interest, "Interest");
            PaymentsGrid.TextMatrix(0, (int) PaymentColumns.Costs, "Costs");
            PaymentsGrid.TextMatrix(0, (int) PaymentColumns.Total, "Total");
            PaymentsGrid.ColFormat((int)PaymentColumns.Date, "MM/dd/yyyy");
            PaymentsGrid.ColFormat((int)PaymentColumns.Principal, "#,##0.00");
            PaymentsGrid.ColFormat((int)PaymentColumns.Interest, "#,##0.00");
            PaymentsGrid.ColFormat((int)PaymentColumns.Costs, "#,##0.00");
            PaymentsGrid.ColFormat((int)PaymentColumns.Total, "#,##0.00");
            PaymentsGrid.ColAlignment((int)PaymentColumns.Principal, FCGrid.AlignmentSettings.flexAlignRightCenter);
            PaymentsGrid.ColAlignment((int)PaymentColumns.Interest, FCGrid.AlignmentSettings.flexAlignRightCenter);
            PaymentsGrid.ColAlignment((int)PaymentColumns.Costs, FCGrid.AlignmentSettings.flexAlignRightCenter);
            PaymentsGrid.ColAlignment((int)PaymentColumns.Total, FCGrid.AlignmentSettings.flexAlignRightCenter);
            PaymentsGrid.ColAlignment((int)PaymentColumns.Date, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            PaymentsGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, (int)PaymentColumns.Principal, 0, (int)PaymentColumns.Total, FCGrid.AlignmentSettings.flexAlignRightCenter);
            PaymentsGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, (int)PaymentColumns.Period, 0, (int)PaymentColumns.PayCode, FCGrid.AlignmentSettings.flexAlignRightCenter);
            PaymentsGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, (int)PaymentColumns.Date, 0, (int)PaymentColumns.Date, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            PaymentsGrid.AddExpandButton();
        }

        private void ResizePaymentsGrid()
        {
            int gridWidth = PaymentsGrid.WidthOriginal;
            int yearWidth = (gridWidth * .08).ToInteger();
            int refWidth = (gridWidth * .12).ToInteger();
            int dateWidth = (gridWidth * .11).ToInteger();
            int periodWidth = (gridWidth * .03).ToInteger();
            int payCodeWidth = (gridWidth * .03).ToInteger();
            int principalWidth = (gridWidth * .15).ToInteger();
            int interestWidth = (gridWidth * .14).ToInteger();
            int costsWidth = (gridWidth * .14).ToInteger();
            int totalWidth = (gridWidth * .15).ToInteger();
            int pendingWidth = (gridWidth * .03).ToInteger();
            PaymentsGrid.ColWidth((int)PaymentColumns.TaxYear, yearWidth);
            PaymentsGrid.ColWidth((int)PaymentColumns.Date, dateWidth);
            PaymentsGrid.ColWidth((int)PaymentColumns.Reference, refWidth);
            PaymentsGrid.ColWidth((int)PaymentColumns.Period, periodWidth);
            PaymentsGrid.ColWidth((int)PaymentColumns.PayCode, payCodeWidth);
            PaymentsGrid.ColWidth((int)PaymentColumns.Principal, principalWidth);
            PaymentsGrid.ColWidth((int)PaymentColumns.Interest, interestWidth);
            PaymentsGrid.ColWidth((int)PaymentColumns.Costs, costsWidth);
            PaymentsGrid.ColWidth((int)PaymentColumns.Total, totalWidth);
            PaymentsGrid.ColWidth((int) PaymentColumns.Pending, pendingWidth);
            yearWidth = yearWidth / 15;
             refWidth = refWidth / 15;
             dateWidth = dateWidth / 15;
             periodWidth = periodWidth / 15;
             payCodeWidth = payCodeWidth / 15;
             principalWidth = principalWidth / 15;
             interestWidth = interestWidth / 15;
             costsWidth = costsWidth / 15;
             totalWidth = totalWidth / 15;
            txtTotalPrincipal.Width = principalWidth;
            txtTotalInterest.Width = interestWidth;
            txtTotalCosts.Width = costsWidth;
            txtTotalTotal.Width = totalWidth;
            int offset = 14;
            txtTotalPrincipal.Left =
            PaymentsGrid.Left + yearWidth + dateWidth + refWidth + periodWidth + payCodeWidth + offset;
            txtTotalInterest.Left = txtTotalPrincipal.Left + principalWidth;
            txtTotalCosts.Left = txtTotalInterest.Left + interestWidth;
            txtTotalTotal.Left = txtTotalCosts.Left + costsWidth;
        }

        private void SetupPendingGrid()
        {
            PendingGrid.Visible = true;
            PendingGrid.Cols = 10;
            PendingGrid.Rows = 1;
            PendingGrid.TextMatrix(0, (int)PendingColumns.TaxYear, "Year");
            PendingGrid.TextMatrix(0, (int)PendingColumns.Date, "Date");
            PendingGrid.TextMatrix(0, (int)PendingColumns.PayCode, "Code");
            PendingGrid.TextMatrix(0, (int)PendingColumns.Period, "Per");
            PendingGrid.TextMatrix(0, (int)PendingColumns.Reference, "Ref");
            PendingGrid.TextMatrix(0, (int)PendingColumns.Principal, "Principal");
            PendingGrid.TextMatrix(0, (int)PendingColumns.Interest, "Interest");
            PendingGrid.TextMatrix(0, (int)PendingColumns.Costs, "Costs");
            PendingGrid.TextMatrix(0, (int)PendingColumns.Total, "Total");
            PendingGrid.TextMatrix(0, (int) PendingColumns.Cash, "Cash");
            PendingGrid.ColFormat((int)PendingColumns.Date, "MM/dd/yyyy");
            PendingGrid.ColFormat((int)PendingColumns.Principal, "#,##0.00");
            PendingGrid.ColFormat((int)PendingColumns.Interest, "#,##0.00");
            PendingGrid.ColFormat((int)PendingColumns.Costs, "#,##0.00");
            PendingGrid.ColFormat((int)PendingColumns.Total, "#,##0.00");
            PendingGrid.ColAlignment((int)PendingColumns.Principal, FCGrid.AlignmentSettings.flexAlignRightCenter);
            PendingGrid.ColAlignment((int)PendingColumns.Interest, FCGrid.AlignmentSettings.flexAlignRightCenter);
            PendingGrid.ColAlignment((int)PendingColumns.Costs, FCGrid.AlignmentSettings.flexAlignRightCenter);
            PendingGrid.ColAlignment((int)PendingColumns.Total, FCGrid.AlignmentSettings.flexAlignRightCenter);
            PendingGrid.ColAlignment((int)PendingColumns.Date, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            PendingGrid.ColAlignment((int) PendingColumns.Period, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            PendingGrid.ColAlignment((int) PendingColumns.PayCode, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            //PendingGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, (int)PendingColumns.Principal, 0, (int)PendingColumns.Total, FCGrid.AlignmentSettings.flexAlignRightCenter);
            //PendingGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, (int)PendingColumns.Period, 0, (int)PendingColumns.PayCode, FCGrid.AlignmentSettings.flexAlignRightCenter);
            //PendingGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, (int)PendingColumns.Date, 0, (int)PendingColumns.Date, FCGrid.AlignmentSettings.flexAlignLeftCenter);
        }

        private void ResizePendingGrid()
        {
            int gridWidth = PendingGrid.WidthOriginal;
            int yearWidth = (gridWidth * .08).ToInteger();
            int refWidth = (gridWidth * .11).ToInteger();
            int dateWidth = (gridWidth * .10).ToInteger();
            int periodWidth = (gridWidth * .03).ToInteger();
            int payCodeWidth = (gridWidth * .04).ToInteger();
            int cashWidth = (gridWidth * .04).ToInteger();
            int principalWidth = (gridWidth * .15).ToInteger();
            int interestWidth = (gridWidth * .14).ToInteger();
            int costsWidth = (gridWidth * .14).ToInteger();
            int totalWidth = (gridWidth * .15).ToInteger();
            PendingGrid.ColWidth((int)PendingColumns.TaxYear, yearWidth);
            PendingGrid.ColWidth((int)PendingColumns.Date, dateWidth);
            PendingGrid.ColWidth((int)PendingColumns.Reference, refWidth);
            PendingGrid.ColWidth((int)PendingColumns.Period, periodWidth);
            PendingGrid.ColWidth((int)PendingColumns.PayCode, payCodeWidth);
            PendingGrid.ColWidth((int)PendingColumns.Cash, cashWidth);
            PendingGrid.ColWidth((int)PendingColumns.Principal, principalWidth);
            PendingGrid.ColWidth((int)PendingColumns.Interest, interestWidth);
            PendingGrid.ColWidth((int)PendingColumns.Costs, costsWidth);
            PendingGrid.ColWidth((int)PendingColumns.Total, totalWidth);

        }
        private enum PaymentColumns
        {
            TaxYear = 1,
            Date = 2,
            Reference = 3,
            Period = 4,
            PayCode = 5,
            Principal = 6,
            Interest = 7,
            Costs = 8,
            Total = 9,
            Pending = 10
        }

        private enum PendingColumns
        {
            TaxYear = 0,
            Date = 1,
            Reference = 2,
            Period = 3,
            PayCode = 4,
            Cash = 5,
            Principal = 6,
            Interest = 7,
            Costs = 8,
            Total = 9
        }

        
        private void ShowBills(PropertyTaxAccount taxAccount)
        {
           // var billSummary = new PropertyTaxBillSummary();
           
           if (ViewModel.AllowPayments && ViewModel.HasCreditsOnOlderYears())
           {
               mnuRedistributeCredits.Visible = true;
           }
           else
           {
               mnuRedistributeCredits.Visible = false;
           }
            PaymentsGrid.Rows = 1;
            ClearPendingGrid();
            foreach (var bill in taxAccount.Bills)
            {
                ShowBill(bill);  
               // billSummary.Add(bill.BillSummary);
            }

            foreach (var bill in taxAccount.Bills.OrderBy(b => b.YearBill()))
            {
                IEnumerable<PaymentRec> payments;
                if (!bill.HasLien())
                {
                    payments = bill.Payments.GetAll().Where(p => p.Id == 0);
                }
                else
                {
                    payments = bill.Lien.Payments.GetAll().Where(p => p.Id == 0);
                    if (bill.Lien.LienSummary.GetNetOwed() <= 0)
                    {
                        mnuFilePrintAltLDN.Visible = true;
                        mnuFileDischarge.Visible = true;
                    }
                }

                foreach (var payment in payments.Where(x => x.Code != "I"))
                {
                    AddPaymentToPendingGrid(payment);
                }
            }
            var billSummary = new TaxBillCalculationSummary(ViewModel.Account,ViewModel.BillType);
            foreach (var calcSummary in ViewModel.CalculationSummaries.Values)
            {
                billSummary.AddTo(calcSummary);
            }

            txtTotalPrincipal.Text = billSummary.PrincipalDue().FormatAsCurrencyNoSymbol();
            txtTotalTotal.Text = billSummary.Balance.FormatAsCurrencyNoSymbol();
            txtTotalInterest.Text = billSummary.InterestDue().FormatAsCurrencyNoSymbol();
            txtTotalCosts.Text = billSummary.CostsDue().FormatAsCurrencyNoSymbol();
        }

        private void AddPaymentToPendingGrid(PaymentRec payment)
        {
            PendingGrid.Rows += 1;
            int row = PendingGrid.Rows - 1;
            PendingGrid.TextMatrix(row, (int) PendingColumns.TaxYear, payment.TaxYear().ToString() + "-" + payment.BillNumber().ToString());
            PendingGrid.TextMatrix(row, (int) PendingColumns.Date,
                payment.RecordedTransactionDate.GetValueOrDefault().ToShortDateString());
            PendingGrid.TextMatrix(row, (int) PendingColumns.Reference, payment.Reference);
            PendingGrid.TextMatrix(row, (int) PendingColumns.Period, payment.Period);
            PendingGrid.TextMatrix(row, (int) PendingColumns.PayCode, payment.Code);
            PendingGrid.TextMatrix(row, (int) PendingColumns.Cash, payment.CashDrawer + " " + payment.GeneralLedger);
            PendingGrid.TextMatrix(row,(int)PendingColumns.Principal,payment.Principal.GetValueOrDefault().FormatAsCurrencyNoSymbol());
            PendingGrid.TextMatrix(row, (int) PendingColumns.Interest,
                (payment.CurrentInterest.GetValueOrDefault() + payment.PreLienInterest.GetValueOrDefault())
                .FormatAsCurrencyNoSymbol());
            PendingGrid.TextMatrix(row, (int) PendingColumns.Costs,
                payment.LienCost.GetValueOrDefault().FormatAsCurrencyNoSymbol());
            PendingGrid.TextMatrix(row, (int) PendingColumns.Total, payment.Total());
            PendingGrid.RowData(row, payment);
        }

        private string FormatBillYear(int billYear)
        {
            var tempYear = billYear.ToString();
            return tempYear.Left(4) + "-" + tempYear.Substring(4);
        }
        private void ShowBill(PropertyTaxBill bill)
        {
            if (bill != null)
            {
                var headerRow = ShowHeader(bill);
               ShowPayments(bill,headerRow);               
            }
        }
       

        private int ShowHeader(PropertyTaxBill bill)
        {
            if (bill != null)
            {               
                var curRow = AddRow(0);
                PaymentsGrid.RowData(curRow,
                    new PaymentGridRowInfo() { BillId = bill.ID, Payment = null, RowType = PaymentGridRowType.Header, YearBill = bill.YearBill() });
                PaymentsGrid.TextMatrix(curRow, (int) PaymentColumns.TaxYear, bill.TaxYear + "-" + bill.BillNumber);
                ShowHeaderCollapsed(bill,curRow);                              
                return curRow;
            }

            return 0;
        }

        private int AddRow(int level)
        {
            PaymentsGrid.Rows = PaymentsGrid.Rows + 1;
            var curRow = PaymentsGrid.Rows - 1;
            if (level == 0)
            {
                PaymentsGrid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, curRow, (int)PaymentColumns.Total, Color.FromArgb(5, 204, 71));
            }
            else if (level == 1)
            {
                PaymentsGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, curRow,  (int)PaymentColumns.Reference, curRow, PaymentsGrid.Cols - 1, FCGrid.AlignmentSettings.flexAlignRightCenter);               
                PaymentsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, curRow,(int)PaymentColumns.TaxYear, curRow, PaymentsGrid.Cols - 1, ColorTranslator.ToOle(Color.FromArgb(244, 247, 249)));
            }

            PaymentsGrid.RowOutlineLevel(curRow, level + 1);
            if (level <= 0)
            {
                PaymentsGrid.IsSubtotal(curRow, true);
            }
            return curRow;
        }

        private void ShowPayments(PropertyTaxBill bill, int headerRow)
        {
            if (bill.TaxYear != 0)
            {
                int curRow;
                if (bill.OwnerNames().Trim() != ViewModel.CurrentOwnerName)
                {
                    curRow = AddRow(1);
                    PaymentsGrid.TextMatrix(curRow, (int) PaymentColumns.Reference, "Billed To:");
                    PaymentsGrid.MergeRow(curRow,true,(int)PaymentColumns.PayCode,(int)PaymentColumns.Costs);
                    PaymentsGrid.TextMatrix(curRow, (int) PaymentColumns.PayCode, bill.OwnerNames());
                    PaymentsGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, curRow,(int)PaymentColumns.PayCode, curRow,(int)PaymentColumns.Costs, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                    PaymentsGrid.RowData(curRow, new PaymentGridRowInfo() {  RowType = PaymentGridRowType.None, YearBill = bill.YearBill() });
                    //PaymentsGrid.TextMatrix(curRow, lngColPending, " ");
                }

                var payments = bill.Payments.GetAll().OrderBy(p => p.RecordedTransactionDate)
                    .ThenBy(p => p.ActualSystemDate);
                foreach (var payment in payments)
                {
                    if (payment.ReceiptNumber != -1)
                    {
                        ShowPayment(payment);
                    }
                }

                if (bill.HasLien())
                {
                    ShowLienPayments(bill.Lien,bill.YearBill(),bill.ID);
                }

                if (ViewModel.CalculationSummaries.ContainsKey(bill.ID))
                {
                    var summary = ViewModel.CalculationSummaries[bill.ID];
                    if (summary.Interest != 0)
                    {
                        AddInterestLine(summary.Interest,bill.YearBill());
                    }

                    AddTotalLine(summary,bill.YearBill());
                }

            }

           PaymentsGrid.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 1,(int)PaymentColumns.Total, PaymentsGrid.Rows - 1, (int)PaymentColumns.Total,true);
            ColorGridPayments();
        }

        private void AddTotalLine(TaxBillCalculationSummary summary,int yearBill)
        {
            var curRow = AddRow(1);
            AddRow(1);
            PaymentsGrid.TextMatrix(curRow, (int) PaymentColumns.Reference, "Total");
            PaymentsGrid.TextMatrix(curRow, (int) PaymentColumns.PayCode, "");
            PaymentsGrid.TextMatrix(curRow, (int) PaymentColumns.Period, "");
            PaymentsGrid.TextMatrix(curRow, (int) PaymentColumns.Principal,
                summary.PrincipalDue().FormatAsCurrencyNoSymbol());
            PaymentsGrid.TextMatrix(curRow, (int) PaymentColumns.Interest,
                (summary.InterestDue() ).FormatAsCurrencyNoSymbol());
            PaymentsGrid.TextMatrix(curRow, (int) PaymentColumns.Costs, summary.CostsDue().FormatAsCurrencyNoSymbol());
            PaymentsGrid.TextMatrix(curRow, (int) PaymentColumns.Total, summary.Balance.FormatAsCurrencyNoSymbol());            
            PaymentsGrid.Cell(FCGrid.CellPropertySettings.flexcpForeColor,curRow,(int)PaymentColumns.Principal, curRow, (int)PaymentColumns.Total, Color.FromArgb(5, 204, 71));
            PaymentsGrid.Cell(FCGrid.CellPropertySettings.flexcpFontBold,curRow, 0,curRow, (int)PaymentColumns.Total, true);
            PaymentsGrid.RowData(curRow,
                new PaymentGridRowInfo() {RowType = PaymentGridRowType.Subtotal, Payment = null,YearBill = yearBill,BillSummary = summary});
        }

        private void ShowPayment(PaymentRec payment)
        {
            if (payment != null)
            {
                var curRow = AddRow(1);
                PaymentsGrid.TextMatrix(curRow, (int) PaymentColumns.Date,
                    payment.RecordedTransactionDate.GetValueOrDefault().FormatAndPadShortDate());
                PaymentsGrid.TextMatrix(curRow, (int) PaymentColumns.Reference, payment.Reference?.Trim());
                PaymentsGrid.TextMatrix(curRow, (int) PaymentColumns.Period, payment.Period);
                PaymentsGrid.TextMatrix(curRow, (int) PaymentColumns.PayCode, payment.Code);
                PaymentsGrid.TextMatrix(curRow, (int) PaymentColumns.Principal, payment.Principal.GetValueOrDefault().FormatAsCurrencyNoSymbol());
                PaymentsGrid.TextMatrix(curRow, (int) PaymentColumns.Interest,
                    (payment.PreLienInterest.GetValueOrDefault() + payment.CurrentInterest.GetValueOrDefault()).FormatAsCurrencyNoSymbol());
                PaymentsGrid.TextMatrix(curRow, (int) PaymentColumns.Costs, payment.LienCost.GetValueOrDefault().FormatAsCurrencyNoSymbol());
                PaymentsGrid.TextMatrix(curRow, (int) PaymentColumns.Total,
                    (payment.LienCost.GetValueOrDefault() + payment.CurrentInterest.GetValueOrDefault() + payment.PreLienInterest.GetValueOrDefault() + payment.Principal.GetValueOrDefault()).FormatAsCurrencyNoSymbol());
                //PaymentsGrid.TextMatrix(curRow, lngColGridCode, "-");                
                PaymentsGrid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, curRow, (int)PaymentColumns.Total, Color.FromArgb(5, 204, 71));
                if (payment.Id == 0)
                {
                    PaymentsGrid.TextMatrix(curRow, (int) PaymentColumns.Pending, "*");
                }
                PaymentsGrid.RowData(curRow, new PaymentGridRowInfo(){Payment = payment,RowType = PaymentGridRowType.Payment, YearBill = payment.Year.GetValueOrDefault()});
            }
        }

        private void ShowLienPayments(PropertyTaxLien billLien, int yearBill, int billId)
        {
            if (billLien != null)
            {
                var curRow = AddRow(1);
                if (billLien.RateRecord != null)
                {
                    PaymentsGrid.TextMatrix(curRow, (int) PaymentColumns.Date,
                        billLien.RateRecord.BillingDate.FormatAndPadShortDate());
                }

                PaymentsGrid.TextMatrix(curRow, (int) PaymentColumns.Reference, "Liened");
                PaymentsGrid.Cell(FCGrid.CellPropertySettings.flexcpFontBold, curRow, (int) PaymentColumns.Reference,
                    true);
                PaymentsGrid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, curRow, (int) PaymentColumns.Reference,
                    Color.Red);
                PaymentsGrid.Select(curRow,(int)PaymentColumns.Principal,curRow,(int)PaymentColumns.Total);
                PaymentsGrid.CellBorder(Color.Black,0,1,0,0,0,0);
                //PaymentsGrid.TextMatrix(curRow, (int) PaymentColumns.Principal, billLien.LienSummary.PrincipalCharged);
                PaymentsGrid.TextMatrix(curRow, (int)PaymentColumns.Principal, billLien.LienSummary.OriginalPrincipalCharged);
                PaymentsGrid.TextMatrix(curRow, (int) PaymentColumns.Interest,
                    billLien.LienSummary.OriginalPreLienInterestCharged);
                PaymentsGrid.TextMatrix(curRow, (int) PaymentColumns.Costs, billLien.LienSummary.OriginalCostsCharged);
                PaymentsGrid.RowData(curRow,
                    new PaymentGridRowInfo() {RowType = PaymentGridRowType.LienHeader, Payment = null,YearBill = yearBill, BillId = billId});
               // PaymentsGrid.TextMatrix(curRow, (int)PaymentColumns.Total,billLien.LienSummary.)
               var payments = billLien.Payments.GetAll().OrderBy(p => p.RecordedTransactionDate)
                   .ThenBy(p => p.ActualSystemDate);
               foreach (var payment in payments)
               {
                   if (payment.ReceiptNumber != -1)
                   {
                        ShowPayment(payment);
                   }
               }
            }
        }

        private void AddInterestLine(decimal interest, int yearBill)
        {
            var curInterest = interest * -1;
            if (interest != 0)
            {
                var curRow = AddRow(1);
                PaymentsGrid.TextMatrix(curRow,(int)PaymentColumns.Principal,0);
                PaymentsGrid.TextMatrix(curRow, (int) PaymentColumns.Interest, curInterest.FormatAsCurrencyNoSymbol());
                PaymentsGrid.TextMatrix(curRow, (int) PaymentColumns.Costs, 0);
                PaymentsGrid.TextMatrix(curRow, (int) PaymentColumns.Total, curInterest.FormatAsCurrencyNoSymbol());
                PaymentsGrid.TextMatrix(curRow, (int) PaymentColumns.TaxYear, "");
                PaymentsGrid.TextMatrix(curRow, (int) PaymentColumns.Date, "");
                if (curInterest < 0)
                {
                    PaymentsGrid.TextMatrix(curRow, (int) PaymentColumns.Reference, "CURINT");
                }
                else
                {
                    PaymentsGrid.TextMatrix(curRow, (int) PaymentColumns.Reference, "EARNINT");
                }
                PaymentsGrid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, curRow, (int)PaymentColumns.Total, Color.FromArgb(5, 204, 71));
                PaymentsGrid.RowData(curRow,
                    new PaymentGridRowInfo() {RowType = PaymentGridRowType.NewInterest, Payment = null,YearBill = yearBill});
            }
        }


        private void ShowControls()
        {
            if (ViewModel.AllowPayments)
            {
                ShowPaymentControls();
            }
            else
            {
                ShowOnlyStatus();
            }
        }
        private void ShowOnlyStatus()
        {
            fraPayment.Visible = false;
            btnAddPayment.Visible = false;
            //PaymentsGrid.HeightOriginal = (ClientArea.Height - PaymentsGrid.Top) * 15;
            GridPanel.Height = ClientArea.Height - GridPanel.Top - BottomPanel.Height;
        }
        private void ShowPaymentControls()
        {
            fraPayment.Visible = true;
            btnAddPayment.Visible = true;
        }

        private void SetupPeriodGrid()
        {
            PeriodGrid.TextMatrix(0, 0, "Total Due:");
            PeriodGrid.TextMatrix(0, 1, "");
            PeriodGrid.TextMatrix(0, 2, "");
            PeriodGrid.TextMatrix(0, 3, "");
            PeriodGrid.TextMatrix(0, 4, "");
        }

        private void ResizePeriodGrid()
        {
            var gridWidth = PeriodGrid.WidthOriginal;
            PeriodGrid.ColWidth(0, (gridWidth * .2).ToInteger());
            PeriodGrid.ColWidth(1, (gridWidth * .19).ToInteger());
            PeriodGrid.ColWidth(2, (gridWidth * .19).ToInteger());
            PeriodGrid.ColWidth(3, (gridWidth * .19).ToInteger());
            PeriodGrid.ColWidth(4, (gridWidth * .19).ToInteger());
            PeriodGrid.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            PeriodGrid.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            PeriodGrid.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            PeriodGrid.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            PeriodGrid.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
        }

        private void SetPayCode(PropertyTaxPaymentCode payCode)
        {
            if (cmbCode.SelectedItem != null)
            {
                if (((GenericDescriptionPair<PropertyTaxPaymentCode>) cmbCode.SelectedItem).ID == payCode)
                {
                    return;
                }
            }
            changingPayCode = true;
            foreach (GenericDescriptionPair<PropertyTaxPaymentCode> pair in cmbCode.Items)
            {
                if (pair.ID == payCode)
                {
                    cmbCode.SelectedItem = pair;
                }
            }

            if (payCode == PropertyTaxPaymentCode.Abatement)
            {
                FillPeriodComboByBill();
            }
            else
            {
                FillPeriodCombo(0);                
            }
            changingPayCode = false;
        }

        private void SetYearCombo(int yearBill)
        {
            if (cmbYear.SelectedItem != null)
            {
                if (((DescriptionIDPair) cmbYear.SelectedItem).ID == yearBill)
                {
                    return;
                }
            }
            foreach (DescriptionIDPair pair in cmbYear.Items)
            {
                if (pair.ID == yearBill)
                {
                    cmbYear.SelectedItem = pair;
                }
            }
        }

        private void ReShowHeader(int row)
        {
            var rowInfo = (PaymentGridRowInfo) PaymentsGrid.RowData(row);
            var bill = ViewModel.GetBill(rowInfo.YearBill);

            if (bill != null)
            {
                if (PaymentsGrid.IsCollapsed(row) == FCGrid.CollapsedSettings.flexOutlineExpanded)
                {
                    ShowHeaderExpanded(bill,row);
                }
                else
                {
                    ShowHeaderCollapsed(bill,row);

                }

            }
        }

        private void ShowHeaderCollapsed(PropertyTaxBill bill, int row)
        {
            PaymentsGrid.TextMatrix(row, (int)PaymentColumns.Reference, "");
            if (bill.HasLien())
            {
                PaymentsGrid.TextMatrix(row, (int)PaymentColumns.Date, "*");
            }
            else
            {
                PaymentsGrid.TextMatrix(row, (int)PaymentColumns.Date, "");
            }

            if (ViewModel.CalculationSummaries.ContainsKey(bill.ID))
            {
                var summary = ViewModel.CalculationSummaries[bill.ID];
                PaymentsGrid.TextMatrix(row, (int)PaymentColumns.Principal,
                    summary.PrincipalDue().FormatAsCurrencyNoSymbol());
                PaymentsGrid.TextMatrix(row, (int)PaymentColumns.Interest,
                    summary.InterestDue().FormatAsCurrencyNoSymbol());
                PaymentsGrid.TextMatrix(row, (int)PaymentColumns.Costs,
                    summary.CostsDue().FormatAsCurrencyNoSymbol());
                PaymentsGrid.TextMatrix(row, (int)PaymentColumns.Total,
                    summary.Balance.FormatAsCurrencyNoSymbol());
            }
        }

        private void ShowHeaderExpanded(PropertyTaxBill bill, int row)
        {
            
            PaymentsGrid.TextMatrix(row, (int)PaymentColumns.TaxYear,
                bill.TaxYear + "-" + bill.BillNumber);
            if (bill.RateRecord != null)
            {
                var originalBillDate = bill.RateRecord.BillingDate;
                PaymentsGrid.TextMatrix(row, (int)PaymentColumns.Date,
                    originalBillDate.FormatAndPadShortDate());
            }
            else
            {
                PaymentsGrid.TextMatrix(row, (int)PaymentColumns.Date,
                    DateTime.Now.FormatAndPadShortDate());
                PaymentsGrid.TextMatrix(row, (int)PaymentColumns.Date, "No RK");
            }

            if (bill.TaxYear > 0)
            {
                PaymentsGrid.TextMatrix(row, (int)PaymentColumns.Reference, "Original");

                PaymentsGrid.TextMatrix(row, (int)PaymentColumns.Principal,
                    bill.TaxInstallments.GetOriginalCharged());
                PaymentsGrid.TextMatrix(row, (int)PaymentColumns.Total, PaymentsGrid.TextMatrix(row, (int)PaymentColumns.Principal));
            }

            PaymentsGrid.TextMatrix(row, (int) PaymentColumns.Interest, "");
            PaymentsGrid.TextMatrix(row, (int) PaymentColumns.Costs, "");
        }



        private bool AddPendingPayment()
        {
            if (!ViewModel.AllowPayments)
            {
                return true;
            }

            if (cmbYear.SelectedItem == null)
            {
                return true;
            }

            if (!txtTransactionDate.Text.IsDate())
            {
                return true;
            }

            if (cmbPeriod.SelectedIndex < 0)
            {
                return true;
            }

            if (cmbCode.SelectedItem == null)
            {
                return true;
            }

            if (txtCD.Text != "N" && txtCD.Text != "Y")
            {
                return true;
            }

            if (txtCash.Text != "N" && txtCash.Text != "Y")
            {
                return true;
            }

            var totalPaid = txtPrincipal.Visible ? txtPrincipal.Text.ToDecimalValue() : 0;
            totalPaid += txtInterest.Visible ? txtInterest.Text.ToDecimalValue() : 0;
            totalPaid += txtCosts.Visible ? txtCosts.Text.ToDecimalValue() : 0;
            totalPaid += txtAmount.Visible ? txtAmount.Text.ToDecimalValue() : 0;
            
            if (totalPaid == 0)
            {
                //throw new NotImplementedException();
                return true;
            }

            if (cmbYear.Text.ToUpper() == "AUTO")
            {
                if (ViewModel.HasPendingPayments())
                {
                    MessageBox.Show("You may not make an auto payment because pending payments exist.", "Existing Payments", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
            }
            else
            {
                if (ViewModel.HasPendingPaymentsOnBill(((DescriptionIDPair)cmbYear.SelectedItem).ID))
                {
                    MessageBox.Show("You may not make a payment on this bill because pending payments exist.", "Existing Payments", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
            }

            if (txtCash.Text == "N" && txtCD.Text == "N")
            {
                var ledgerAccount = txtAcctNumber.TextMatrix(0, 0);
                if (string.IsNullOrEmpty(ledgerAccount) || (clsAcct.AllowFormats && ledgerAccount.Contains("_")))
                {
                    FCMessageBox.Show("Please enter a valid account number",
                        MsgBoxStyle.Exclamation | MsgBoxStyle.OkOnly, "Account Number Missing");
                    return false;
                }
            }


            var paymentInfo = new PaymentSetupInfo()
            {
                Account = txtAcctNumber.TextMatrix(0,0),
                Reference = txtReference.Text,
                PayCode = ((GenericDescriptionPair<PropertyTaxPaymentCode>)cmbCode.SelectedItem).ID,
                Principal = txtPrincipal.Visible ? txtPrincipal.Text.ToDecimalValue() : 0,
                Interest = txtInterest.Visible ? txtInterest.Text.ToDecimalValue() : 0,
                Costs = txtCosts.Visible ? txtCosts.Text.ToDecimalValue() : 0
            };

            paymentInfo.TransactionDate = txtTransactionDate.Text.ToDate();
            paymentInfo.AffectCash = txtCash.Text;
            paymentInfo.AffectCashDrawer = txtCD.Text;
            paymentInfo.YearBill = ((DescriptionIDPair) cmbYear.SelectedItem).ID;
            paymentInfo.Comment = txtComments.Text;            
            if (AmountPanel.Visible)
            {
                paymentInfo.Principal = txtAmount.Text.ToDecimalValue();
            }


            if (paymentInfo.PayCode == PropertyTaxPaymentCode.RegularPayment ||
                paymentInfo.PayCode == PropertyTaxPaymentCode.Correction)
            {
                var defaultPaymentYear = ViewModel.GetDefaultPaymentYear();
                if (defaultPaymentYear > 0 && paymentInfo.YearBill != defaultPaymentYear && paymentInfo.YearBill > 0)
                {
                    if (FCMessageBox.Show(
                            "You are not paying the default year. Would you like to continue with this transaction?",
                            MsgBoxStyle.Question | MsgBoxStyle.YesNo, "Not The Default Year") != DialogResult.Yes)
                    {
                        return false;
                    }
                }
                decimal totalOwed = 0;
                if (paymentInfo.YearBill == 0)
                {
                    totalOwed = ViewModel.AccountSummary.Balance;
                }
                else
                {
                    var bill = ViewModel.GetBill(paymentInfo.YearBill);
                    if (bill != null)
                    {
                        if (ViewModel.CalculationSummaries.ContainsKey(bill.ID))
                        {
                            var summary = ViewModel.CalculationSummaries[bill.ID];
                            totalOwed = summary.Balance;
                        }
                    }
                    
                }

                if (totalOwed < paymentInfo.GetTotal())
                {
                    if (FCMessageBox.Show("The payment is greater than owed, would you like to continue?", MsgBoxStyle.YesNo |  MsgBoxStyle.Question,"Continue with over payment?") != DialogResult.Yes)
                    {
                        return false;
                    }                     
                }
            }
                

            
            AddPayment(paymentInfo);
            SetPaymentDefaults();
            return true;
        }

        private void CloseWithoutCancel()
        {
            cancelling = false;
            Unload();
        }

        private void ClearPendingGrid()
        {
            PendingGrid.Rows = 1;
        }

        private void ShowDischargeNotice()
        {
            var row = PaymentsGrid.Row;
            if (row < 1)
            {
                return;
            }
            var rowInfo = (PaymentGridRowInfo)PaymentsGrid.RowData(row);
            if (rowInfo != null)
            {
                var yearBill = rowInfo.YearBill;
                var bill = ViewModel.GetBill(yearBill);
                if (bill.HasLien())
                {
                    ViewModel.ShowLienDischargeNotice(bill.ID);
                }
            }            
        }

        private void ShowAlternateDischargeNotice()
        {
            ViewModel.ShowAlternateDischargeNotice();
        }

        private void ClearPaymentBoxes()
        {
            txtReference.Text = "";
            txtPrincipal.Text = "0.00";
            txtInterest.Text = "0.00";
            txtCosts.Text = "0.00";
            txtComments.Text = "";
            txtAmount.Text = "0.00";
        }

        private void CheckCD()
        {
            if (txtCD.Text == "N")
            {
                txtCash.Enabled = true;
                txtCash.TabStop = true;
            }
            else
            {
                if (txtCash.Text == "N")
                    txtCash.Text = "Y";
                txtCash.TabStop = false;
                txtCash.Enabled = false;
                if (cmbCode.SelectedItem != null)
                {
                    var payCodePair = (GenericDescriptionPair<PropertyTaxPaymentCode>)cmbCode.SelectedItem;
                    if (payCodePair.ID != PropertyTaxPaymentCode.Discount)                    
                    {
                        txtAcctNumber.TextMatrix(0, 0, "");
                    }
                }
                txtAcctNumber.Enabled = false;
                txtAcctNumber.TabStop = false;
            }
        }

        private void CheckCash()
        {
            if (txtCD.Text == "N")
            {
                if (txtCash.Text == "N")
                {
                    txtAcctNumber.Enabled = true;
                    txtAcctNumber.TabStop = true;
                }
                else
                {
                    txtAcctNumber.TextMatrix(0, 0, "");
                    txtAcctNumber.Enabled = false;
                    txtAcctNumber.TabStop = false;
                }
            }
            else
            {
                if (txtCash.Text == "N")
                    txtCash.Text = "Y";
            }
        }

        private void ExpandYearsWithPending()
        {
            var billIds = ViewModel.GetBillIdsWithPending();
            //foreach (var id in billIds)
            //{
                for (int row = 1; row < PaymentsGrid.Rows; row++)
                {
                    var rowInfo = (PaymentGridRowInfo)PaymentsGrid.RowData(row);
                    if (rowInfo != null)
                    {
                        if (rowInfo.RowType == PaymentGridRowType.Header)
                        {
                            if (billIds.Contains(rowInfo.BillId))
                            {
                                PaymentsGrid.IsCollapsed(row,FCGrid.CollapsedSettings.flexOutlineExpanded);
                            }
                        }
                    }
                }
            //}
        }

        private class PaymentGridRowInfo
        {
            public PaymentGridRowType RowType { get; set; } = PaymentGridRowType.None;
            public int BillId { get; set; }
            public int YearBill { get; set; }
            public PaymentRec Payment { get; set; }
            public TaxBillCalculationSummary BillSummary {get; set;}
        }

        private enum PaymentGridRowType
        {
            Header = 0,
            Payment = 1,
            NewInterest = 2,
            LienHeader = 3,
            Subtotal = 4,
            None = 5
        }

        private enum PaymentYearType
        {
            None = 0,
            Auto = 1,
            Prepay = 2,
            Regular = 3
        }

        private void lblGrpInfo_Click(object sender, EventArgs e)
        {
            frmGroupBalance.InstancePtr.Init(ViewModel.GroupInfo.ID, ViewModel.EffectiveDate);
        }
    }
}
