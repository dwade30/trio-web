﻿namespace TWCL0000.AccountPayment
{
    partial class PPAccountTotalsView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CategoryGrid = new fecherFoundation.FCGrid();
            ((System.ComponentModel.ISupportInitialize)(this.CategoryGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // CategoryGrid
            // 
            this.CategoryGrid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.CategoryGrid.Cols = 3;
            this.CategoryGrid.FixedCols = 0;
            this.CategoryGrid.Location = new System.Drawing.Point(0, 3);
            this.CategoryGrid.MinimumSize = new System.Drawing.Size(200, 110);
            this.CategoryGrid.Name = "CategoryGrid";
            this.CategoryGrid.RowHeadersVisible = false;
            this.CategoryGrid.Rows = 1;
            this.CategoryGrid.Size = new System.Drawing.Size(349, 110);
            this.CategoryGrid.StandardTab = false;
            this.CategoryGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.CategoryGrid.TabIndex = 25;
            // 
            // PPAccountTotalsView
            // 
            this.Controls.Add(this.CategoryGrid);
            this.Name = "PPAccountTotalsView";
            this.Size = new System.Drawing.Size(350, 120);
            ((System.ComponentModel.ISupportInitialize)(this.CategoryGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public fecherFoundation.FCGrid CategoryGrid;
    }
}
