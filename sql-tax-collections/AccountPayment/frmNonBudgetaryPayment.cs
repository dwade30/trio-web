﻿using System;
using Global;
using SharedApplication;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections.AccountPayment;
using Wisej.Web;

namespace TWCL0000.AccountPayment
{
    public partial class frmNonBudgetaryPayment : BaseForm, IModalView<INonBudgetaryPaymentViewModel>
    {
        private bool cancelling = true;
        public frmNonBudgetaryPayment()
        {
            InitializeComponent();
            InitializeComponentEx();
        }

        public frmNonBudgetaryPayment(INonBudgetaryPaymentViewModel viewModel) : this()
        {
            ViewModel = viewModel;
            
        }

        private void InitializeComponentEx()
        {
            txtPrincipal.Validating += TxtPrincipal_Validating;
            txtPreLienInterest.Validating += TxtPreLienInterest_Validating;
            txtCosts.Validating += TxtCosts_Validating;
            txtInterest.Validating += TxtInterest_Validating;
            btnProcess.Click += btnProcess_Click;
            this.Closing += FrmNonBudgetaryPayment_Closing;
            this.Load += FrmNonBudgetaryPayment_Load;
        }

        private void FrmNonBudgetaryPayment_Load(object sender, EventArgs e)
        {
            txtPreLienInterest.Text = ViewModel.IsLien ? ViewModel.PreLienInterest.ToString("0.00") : "0.00";
            txtPreLienInterest.Enabled = ViewModel.IsLien;
          
            txtPrincipal.Text = ViewModel.Principal.ToString("0.00");
            txtCosts.Text = ViewModel.Costs.ToString("0.00");
            txtInterest.Text = ViewModel.Interest.ToString("0.00");
            lblTotal.Text = (ViewModel.Principal + ViewModel.Interest + ViewModel.PreLienInterest + ViewModel.Costs)
                .ToString("0.00");
        }

        private void FrmNonBudgetaryPayment_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (cancelling)
            {
                ViewModel.Cancel();
            }
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            ViewModel.SetPayment(txtPrincipal.Text.ToDecimalValue(), txtInterest.Text.ToDecimalValue(),txtPreLienInterest.Text.ToDecimalValue(), txtCosts.Text.ToDecimalValue());
            CloseWithoutCancelling();
        }

        private void CloseWithoutCancelling()
        {
            cancelling = false;
            Close();
        }

        private void TxtInterest_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            RecalcTotal();
        }

        private void TxtCosts_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            RecalcTotal();
        }

        private void TxtPreLienInterest_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            RecalcTotal();
        }

        private void TxtPrincipal_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            RecalcTotal();
        }

        private void RecalcTotal()
        {
            var principal = txtPrincipal.Text.ToDecimalValue();
            var preLienInterest = txtPreLienInterest.Text.ToDecimalValue();
            var interest = txtInterest.Text.ToDecimalValue();
            var costs = txtCosts.Text.ToDecimalValue();
            var total = principal + interest + preLienInterest + costs;
            lblTotal.Text = total.ToString("0.00");
        }
        public INonBudgetaryPaymentViewModel ViewModel { get; set; }
        public void ShowModal()
        {            
            this.Show(FormShowEnum.Modal);
        }
    }
}
