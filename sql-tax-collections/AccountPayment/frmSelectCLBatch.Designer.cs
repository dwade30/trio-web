﻿namespace TWCL0000.AccountPayment
{
    partial class frmSelectCLBatch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnProcess = new fecherFoundation.FCButton();
            this.cmbBatchChoice = new Wisej.Web.ListViewComboBox();
            this.columnHeader1 = new Wisej.Web.ColumnHeader();
            this.columnHeader2 = new Wisej.Web.ColumnHeader();
            this.columnHeader3 = new Wisej.Web.ColumnHeader();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 87);
            this.BottomPanel.Size = new System.Drawing.Size(424, 80);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbBatchChoice);
            this.ClientArea.Location = new System.Drawing.Point(0, 0);
            this.ClientArea.Size = new System.Drawing.Size(424, 87);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(424, 0);
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Location = new System.Drawing.Point(152, 16);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcess.Size = new System.Drawing.Size(120, 48);
            this.btnProcess.TabIndex = 2;
            this.btnProcess.Text = "Select";
            // 
            // cmbBatchChoice
            // 
            this.cmbBatchChoice.AutoSize = false;
            this.cmbBatchChoice.Columns.AddRange(new Wisej.Web.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.cmbBatchChoice.Location = new System.Drawing.Point(21, 26);
            this.cmbBatchChoice.Name = "cmbBatchChoice";
            this.cmbBatchChoice.Size = new System.Drawing.Size(380, 38);
            this.cmbBatchChoice.TabIndex = 2;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Image = null;
            this.columnHeader1.Name = "columnHeader1";
            this.columnHeader1.Text = "Batch Date";
            this.columnHeader1.Width = 110;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Image = null;
            this.columnHeader2.Name = "columnHeader2";
            this.columnHeader2.Text = "Teller";
            // 
            // columnHeader3
            // 
            this.columnHeader3.Image = null;
            this.columnHeader3.Name = "columnHeader3";
            this.columnHeader3.Text = "Paid By";
            this.columnHeader3.Width = 210;
            // 
            // frmSelectCLBatch
            // 
            this.ClientSize = new System.Drawing.Size(424, 167);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSelectCLBatch";
            this.Text = "Collections Batch";
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private fecherFoundation.FCButton btnProcess;
        private Wisej.Web.ListViewComboBox cmbBatchChoice;
        private Wisej.Web.ColumnHeader columnHeader1;
        private Wisej.Web.ColumnHeader columnHeader2;
        private Wisej.Web.ColumnHeader columnHeader3;
    }
}