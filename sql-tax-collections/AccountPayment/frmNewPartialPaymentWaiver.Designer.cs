﻿using Global;

namespace TWCL0000.AccountPayment
{
    partial class frmNewPartialPaymentWaiver : BaseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmNewPartialPaymentWaiver));
            this.btnProcess = new fecherFoundation.FCButton();
            this.txtMunicipality = new fecherFoundation.FCTextBox();
            this.lblAccount = new fecherFoundation.FCLabel();
            this.txtState = new fecherFoundation.FCTextBox();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.txtOwner = new fecherFoundation.FCTextBox();
            this.fcLabel2 = new fecherFoundation.FCLabel();
            this.fcLabel3 = new fecherFoundation.FCLabel();
            this.txtPayment = new fecherFoundation.FCTextBox();
            this.fcLabel4 = new fecherFoundation.FCLabel();
            this.txtWitness = new fecherFoundation.FCTextBox();
            this.fcLabel5 = new fecherFoundation.FCLabel();
            this.fcLabel6 = new fecherFoundation.FCLabel();
            this.txtMapLot = new fecherFoundation.FCTextBox();
            this.lblSignedDate = new fecherFoundation.FCLabel();
            this.dtPaymentDate = new fecherFoundation.FCDateTimePicker();
            this.dtSigned = new fecherFoundation.FCDateTimePicker();
            this.javaScript1 = new Wisej.Web.JavaScript(this.components);
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 473);
            this.BottomPanel.Size = new System.Drawing.Size(663, 103);
            // 
            // ClientArea
            // 
            this.ClientArea.Anchor = Wisej.Web.AnchorStyles.Left;
            this.ClientArea.Controls.Add(this.dtSigned);
            this.ClientArea.Controls.Add(this.dtPaymentDate);
            this.ClientArea.Controls.Add(this.txtMapLot);
            this.ClientArea.Controls.Add(this.lblSignedDate);
            this.ClientArea.Controls.Add(this.fcLabel6);
            this.ClientArea.Controls.Add(this.txtWitness);
            this.ClientArea.Controls.Add(this.fcLabel5);
            this.ClientArea.Controls.Add(this.txtPayment);
            this.ClientArea.Controls.Add(this.fcLabel4);
            this.ClientArea.Controls.Add(this.fcLabel3);
            this.ClientArea.Controls.Add(this.txtOwner);
            this.ClientArea.Controls.Add(this.fcLabel2);
            this.ClientArea.Controls.Add(this.txtState);
            this.ClientArea.Controls.Add(this.fcLabel1);
            this.ClientArea.Controls.Add(this.txtMunicipality);
            this.ClientArea.Controls.Add(this.lblAccount);
            this.ClientArea.Dock = Wisej.Web.DockStyle.None;
            this.ClientArea.Size = new System.Drawing.Size(683, 613);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblAccount, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtMunicipality, 0);
            this.ClientArea.Controls.SetChildIndex(this.fcLabel1, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtState, 0);
            this.ClientArea.Controls.SetChildIndex(this.fcLabel2, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtOwner, 0);
            this.ClientArea.Controls.SetChildIndex(this.fcLabel3, 0);
            this.ClientArea.Controls.SetChildIndex(this.fcLabel4, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtPayment, 0);
            this.ClientArea.Controls.SetChildIndex(this.fcLabel5, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtWitness, 0);
            this.ClientArea.Controls.SetChildIndex(this.fcLabel6, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblSignedDate, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtMapLot, 0);
            this.ClientArea.Controls.SetChildIndex(this.dtPaymentDate, 0);
            this.ClientArea.Controls.SetChildIndex(this.dtSigned, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(683, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(157, 30);
            this.HeaderText.Text = "Confirm Data";
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Location = new System.Drawing.Point(237, 31);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcess.Size = new System.Drawing.Size(156, 48);
            this.btnProcess.TabIndex = 3;
            this.btnProcess.Text = "Save & Continue";
            // 
            // txtMunicipality
            // 
            this.txtMunicipality.BackColor = System.Drawing.SystemColors.Window;
            this.txtMunicipality.Location = new System.Drawing.Point(161, 13);
            this.txtMunicipality.Name = "txtMunicipality";
            this.txtMunicipality.Size = new System.Drawing.Size(220, 40);
            this.txtMunicipality.TabIndex = 1;
            // 
            // lblAccount
            // 
            this.lblAccount.AutoSize = true;
            this.lblAccount.Location = new System.Drawing.Point(25, 27);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(103, 17);
            this.lblAccount.TabIndex = 1003;
            this.lblAccount.Text = "MUNICIPALITY";
            this.lblAccount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtState
            // 
            this.txtState.BackColor = System.Drawing.SystemColors.Window;
            this.txtState.Location = new System.Drawing.Point(161, 73);
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(175, 40);
            this.txtState.TabIndex = 2;
            // 
            // fcLabel1
            // 
            this.fcLabel1.AutoSize = true;
            this.fcLabel1.Location = new System.Drawing.Point(25, 87);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(52, 17);
            this.fcLabel1.TabIndex = 1005;
            this.fcLabel1.Text = "STATE";
            this.fcLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtOwner
            // 
            this.txtOwner.BackColor = System.Drawing.SystemColors.Window;
            this.txtOwner.Location = new System.Drawing.Point(161, 133);
            this.txtOwner.Name = "txtOwner";
            this.txtOwner.Size = new System.Drawing.Size(502, 40);
            this.txtOwner.TabIndex = 3;
            // 
            // fcLabel2
            // 
            this.fcLabel2.AutoSize = true;
            this.fcLabel2.Location = new System.Drawing.Point(25, 147);
            this.fcLabel2.Name = "fcLabel2";
            this.fcLabel2.Size = new System.Drawing.Size(114, 17);
            this.fcLabel2.TabIndex = 1007;
            this.fcLabel2.Text = "OWNER\'S NAME";
            this.fcLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fcLabel3
            // 
            this.fcLabel3.AutoSize = true;
            this.fcLabel3.Location = new System.Drawing.Point(25, 207);
            this.fcLabel3.Name = "fcLabel3";
            this.fcLabel3.Size = new System.Drawing.Size(113, 17);
            this.fcLabel3.TabIndex = 1009;
            this.fcLabel3.Text = "PAYMENT DATE";
            this.fcLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtPayment
            // 
            this.txtPayment.BackColor = System.Drawing.SystemColors.Window;
            this.javaScript1.SetJavaScript(this.txtPayment, resources.GetString("txtPayment.JavaScript"));
            this.txtPayment.Location = new System.Drawing.Point(161, 253);
            this.txtPayment.Name = "txtPayment";
            this.txtPayment.Size = new System.Drawing.Size(175, 40);
            this.txtPayment.TabIndex = 5;
            // 
            // fcLabel4
            // 
            this.fcLabel4.AutoSize = true;
            this.fcLabel4.Location = new System.Drawing.Point(25, 267);
            this.fcLabel4.Name = "fcLabel4";
            this.fcLabel4.Size = new System.Drawing.Size(136, 17);
            this.fcLabel4.TabIndex = 1011;
            this.fcLabel4.Text = "PAYMENT AMOUNT";
            this.fcLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtWitness
            // 
            this.txtWitness.BackColor = System.Drawing.SystemColors.Window;
            this.txtWitness.Location = new System.Drawing.Point(161, 313);
            this.txtWitness.Name = "txtWitness";
            this.txtWitness.Size = new System.Drawing.Size(502, 40);
            this.txtWitness.TabIndex = 6;
            // 
            // fcLabel5
            // 
            this.fcLabel5.AutoSize = true;
            this.fcLabel5.Location = new System.Drawing.Point(25, 327);
            this.fcLabel5.Name = "fcLabel5";
            this.fcLabel5.Size = new System.Drawing.Size(115, 17);
            this.fcLabel5.TabIndex = 1013;
            this.fcLabel5.Text = "WITNESS\' NAME";
            this.fcLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fcLabel6
            // 
            this.fcLabel6.AutoSize = true;
            this.fcLabel6.Location = new System.Drawing.Point(25, 387);
            this.fcLabel6.Name = "fcLabel6";
            this.fcLabel6.Size = new System.Drawing.Size(68, 17);
            this.fcLabel6.TabIndex = 1015;
            this.fcLabel6.Text = "MAP LOT";
            this.fcLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMapLot
            // 
            this.txtMapLot.BackColor = System.Drawing.SystemColors.Window;
            this.txtMapLot.Location = new System.Drawing.Point(161, 373);
            this.txtMapLot.Name = "txtMapLot";
            this.txtMapLot.Size = new System.Drawing.Size(175, 40);
            this.txtMapLot.TabIndex = 7;
            // 
            // lblSignedDate
            // 
            this.lblSignedDate.AutoSize = true;
            this.lblSignedDate.Location = new System.Drawing.Point(25, 447);
            this.lblSignedDate.Name = "lblSignedDate";
            this.lblSignedDate.Size = new System.Drawing.Size(99, 17);
            this.lblSignedDate.TabIndex = 1017;
            this.lblSignedDate.Text = "SIGNED DATE";
            this.lblSignedDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dtPaymentDate
            // 
            this.dtPaymentDate.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.dtPaymentDate.AutoSize = false;
            this.dtPaymentDate.Format = Wisej.Web.DateTimePickerFormat.Custom;
            this.dtPaymentDate.Location = new System.Drawing.Point(161, 193);
            this.dtPaymentDate.Mask = "00/00/0000";
            this.dtPaymentDate.Name = "dtPaymentDate";
            this.dtPaymentDate.Size = new System.Drawing.Size(175, 40);
            this.dtPaymentDate.TabIndex = 4;
            // 
            // dtSigned
            // 
            this.dtSigned.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.dtSigned.AutoSize = false;
            this.dtSigned.Format = Wisej.Web.DateTimePickerFormat.Custom;
            this.dtSigned.Location = new System.Drawing.Point(161, 433);
            this.dtSigned.Mask = "00/00/0000";
            this.dtSigned.Name = "dtSigned";
            this.dtSigned.Size = new System.Drawing.Size(175, 40);
            this.dtSigned.TabIndex = 8;
            // 
            // frmNewPartialPaymentWaiver
            // 
            this.ClientSize = new System.Drawing.Size(683, 673);
            this.Name = "frmNewPartialPaymentWaiver";
            this.Text = "Partial Payment";
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private fecherFoundation.FCButton btnProcess;
        public fecherFoundation.FCTextBox txtMapLot;
        public fecherFoundation.FCLabel lblSignedDate;
        public fecherFoundation.FCLabel fcLabel6;
        public fecherFoundation.FCTextBox txtWitness;
        public fecherFoundation.FCLabel fcLabel5;
        public fecherFoundation.FCTextBox txtPayment;
        public fecherFoundation.FCLabel fcLabel4;
        public fecherFoundation.FCLabel fcLabel3;
        public fecherFoundation.FCTextBox txtOwner;
        public fecherFoundation.FCLabel fcLabel2;
        public fecherFoundation.FCTextBox txtState;
        public fecherFoundation.FCLabel fcLabel1;
        public fecherFoundation.FCTextBox txtMunicipality;
        public fecherFoundation.FCLabel lblAccount;
        private fecherFoundation.FCDateTimePicker dtSigned;
        private fecherFoundation.FCDateTimePicker dtPaymentDate;
        private Wisej.Web.JavaScript javaScript1;
    }
}