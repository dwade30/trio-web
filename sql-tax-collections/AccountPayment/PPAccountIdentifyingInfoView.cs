﻿using SharedApplication.PersonalProperty;

namespace TWCL0000.AccountPayment
{
    public partial class PPAccountIdentifyingInfoView : Wisej.Web.UserControl
    {
        private PersonalPropertyAccountBriefInfo accountInfo;
        public PPAccountIdentifyingInfoView()
        {
            InitializeComponent();
        }
        public PPAccountIdentifyingInfoView(PersonalPropertyAccountBriefInfo info) : this()
        {
            accountInfo = info;
            ShowInfo();
        }

        private void ShowInfo()
        {
            if (accountInfo == null) return;

            lblName.Text = accountInfo.Name;
            lblLocation.Text = BuildLocation(accountInfo.LocationNumber, accountInfo.LocationStreet);
        }

        private string BuildLocation(int locationNumber, string locationStreet)
        {
            var locNumStr = "";
            if (locationNumber > 0) locNumStr = locationNumber.ToString();
            return $"{locNumStr} {locationStreet ?? ""}".Trim();
        }
    }
}
