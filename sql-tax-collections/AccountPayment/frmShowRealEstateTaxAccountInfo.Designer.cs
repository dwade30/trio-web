﻿namespace TWCL0000.AccountPayment
{
    partial class frmShowRealEstateTaxAccountInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LayoutPanel = new Wisej.Web.FlexLayoutPanel();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 547);
            this.BottomPanel.Size = new System.Drawing.Size(451, 0);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.LayoutPanel);
            this.ClientArea.Location = new System.Drawing.Point(0, 0);
            this.ClientArea.Size = new System.Drawing.Size(471, 557);
            this.ClientArea.Controls.SetChildIndex(this.LayoutPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(471, 0);
            // 
            // LayoutPanel
            // 
            this.LayoutPanel.LayoutStyle = Wisej.Web.FlexLayoutStyle.Vertical;
            this.LayoutPanel.Location = new System.Drawing.Point(40, 26);
            this.LayoutPanel.Name = "LayoutPanel";
            this.LayoutPanel.Size = new System.Drawing.Size(406, 521);
            this.LayoutPanel.TabIndex = 4;
            this.LayoutPanel.TabStop = true;
            // 
            // frmShowRealEstateTaxAccountInfo
            // 
            this.ClientSize = new System.Drawing.Size(471, 557);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmShowRealEstateTaxAccountInfo";
            this.Text = "Account Information";
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Wisej.Web.FlexLayoutPanel LayoutPanel;
    }
}