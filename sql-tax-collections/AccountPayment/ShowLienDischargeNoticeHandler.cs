﻿using Global;
using SharedApplication;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Lien;

namespace TWCL0000.AccountPayment
{
    public class ShowLienDischargeNoticeHandler : CommandHandler<ShowLienDischargeNotice>
    {
        private IModalView<ITaxLienDischargeNoticeViewModel> dischargeView;
        public ShowLienDischargeNoticeHandler(IModalView<ITaxLienDischargeNoticeViewModel> dischargeView)
        {
            this.dischargeView = dischargeView;
        }
        protected override void Handle(ShowLienDischargeNotice command)
        {
            dischargeView.ViewModel.PayDate = command.DatePaid;
            dischargeView.ViewModel.TaxBill = command.TaxBill;
            dischargeView.ShowModal();
        }
    }
}