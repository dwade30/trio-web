﻿using System;
using fecherFoundation;
using Global;
using SharedApplication;
using SharedApplication.Clerk.Dogs;
using SharedApplication.Enums;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections.AccountPayment;
using SharedApplication.TaxCollections.Enums;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCL0000.AccountPayment
{
    public partial class frmTaxAccountSearch : BaseForm, IView<ITaxAccountSearchViewModel>
    {
        private bool cancelling = true;
        private GlobalColorSettings colorSettings;
        public frmTaxAccountSearch()
        {
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            this.Closing += FrmTaxAccountSearch_Closing;
            this.Resize += FrmTaxAccountSearch_Resize;
            this.cmdSearch.Click += CmdSearch_Click;
            txtGetAccountNumber.KeyDown += txtGetAccountNumber_KeyDown;
            txtGetAccountNumber.KeyPress += txtGetAccountNumber_KeyPress;
            SearchGrid.DoubleClick += SearchGrid_DoubleClick;
            SearchGrid.Click += SearchGrid_Click;
            SearchGrid.KeyDown += SearchGrid_KeyDown;
            SearchGrid.SelectionChanged += SearchGrid_SelectionChanged;
            this.Load += FrmTaxAccountSearch_Load;

            this.mnuPurgeBatch.Click += MnuPurgeBatch_Click;
            this.mnuRecoverBatch.Click += MnuRecoverBatch_Click;
            this.mnuStartBatch.Click += MnuStartBatch_Click;
        }

        private void MnuStartBatch_Click(object sender, EventArgs e)
        {
            StartBatch();            
        }

        private void StartBatch()
        {
            if (ViewModel.AllowPayments && !ViewModel.OriginIsTaxCollections)
            {
                ViewModel.StartBatch();
            }
        }

        private void MnuRecoverBatch_Click(object sender, EventArgs e)
        {
            if (ViewModel.AllowPayments && !ViewModel.OriginIsTaxCollections)
            {
                ViewModel.RecoverBatch();
            }
        }

        private void MnuPurgeBatch_Click(object sender, EventArgs e)
        {
            if (ViewModel.AllowPayments && !ViewModel.OriginIsTaxCollections)
            {
                ViewModel.PurgeBatch();
            }
        }      

        private void FrmTaxAccountSearch_Load(object sender, EventArgs e)
        {
            ViewModel.Closing += ViewModel_Closing;
            SetCriteriaChoices();
            SetSearchByOptions();
            SetupSearchGrid();
            ShowHideControls();
            GetDefaultAccount();
        }

        private void ViewModel_Closing(object sender, EventArgs e)
        {
            cancelling = false;
            Close();
        }

        private void SearchGrid_SelectionChanged(object sender, EventArgs e)
        {
            SetCurrentAccount();
        }

        private void SearchGrid_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                GetCurrentAccount();
            }
        }

        private void SearchGrid_Click(object sender, EventArgs e)
        {
            SetCurrentAccount();
        }

        private void SetCurrentAccount()
        {
            if (SearchGrid.Row > 0)
            {
                txtGetAccountNumber.Text = SearchGrid.TextMatrix(SearchGrid.Row, (int) GridColumns.Account);
            }
        }


        private void SearchGrid_DoubleClick(object sender, EventArgs e)
        {
           GetCurrentAccount();
        }

        private void GetCurrentAccount()
        {
            if (SearchGrid.Row > 0)
            {
                txtGetAccountNumber.Text = SearchGrid.TextMatrix(SearchGrid.Row, (int)GridColumns.Account);
                EnteredAccountNumber(true);
            }
        }

        private void CmdSearch_Click(object sender, EventArgs e)
        {
            Search();
        }

        private void FrmTaxAccountSearch_Resize(object sender, EventArgs e)
        {
            ResizeSearchGrid();
        }

        private void FrmTaxAccountSearch_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (cancelling)
            {
                ViewModel.Cancel();
            }
        }

        public frmTaxAccountSearch(ITaxAccountSearchViewModel viewModel,GlobalColorSettings colorSettings) : this()
        {
            this.colorSettings = colorSettings;
            ViewModel = viewModel;
        }

        private void GetDefaultAccount()
        {
            var account = ViewModel.GetLastAccountAccessed();
            if (account > 0)
            {
                txtGetAccountNumber.Text = account.ToString();
            }
        }

        private void ShowHideControls()
        {
            switch (ViewModel.BillType)
            {
                case PropertyTaxBillType.Real:
                    chkShowPreviousOwnerInfo.Visible = true;
                    break;
                case PropertyTaxBillType.Personal:
                    chkShowPreviousOwnerInfo.Visible = false;
                    break;
            }

            if (ViewModel.AllowPayments && !ViewModel.OriginIsTaxCollections && ViewModel.BillType == PropertyTaxBillType.Real)
            {
                mnuPurgeBatch.Visible = true;
                mnuPurgeBatch.Enabled = true;
                mnuRecoverBatch.Visible = true;
                mnuRecoverBatch.Enabled = true;
                mnuStartBatch.Visible = true;
                mnuStartBatch.Enabled = true;
            }
            else
            {
                mnuPurgeBatch.Visible = false;
                mnuPurgeBatch.Enabled = false;
                mnuRecoverBatch.Visible = false;
                mnuRecoverBatch.Enabled = false;
                mnuStartBatch.Visible = false;
                mnuStartBatch.Enabled = false;
            }
        }

        private void SetupSearchGrid()
        {
            SearchGrid.Rows = 1;
            SearchGrid.Cols = 5;
            if (ViewModel.BillType == PropertyTaxBillType.Personal)
            {
                SearchGrid.ColHidden((int)GridColumns.MapLot, true);
            }

            SearchGrid.TextMatrix(0, (int) GridColumns.Account, "Account");
            SearchGrid.TextMatrix(0, (int) GridColumns.Name, "Name");
            SearchGrid.TextMatrix(0, (int) GridColumns.StreetNumber, "#");
            SearchGrid.TextMatrix(0, (int) GridColumns.StreetName, "Street Name");
            SearchGrid.TextMatrix(0, (int) GridColumns.MapLot, "Map / Lot");
            SearchGrid.ColAlignment((int) GridColumns.Account, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            SearchGrid.ColAlignment((int) GridColumns.MapLot, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            SearchGrid.ColAlignment((int)GridColumns.Name, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            SearchGrid.ColAlignment((int)GridColumns.StreetName, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            SearchGrid.ColAlignment((int)GridColumns.StreetNumber, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            ResizeSearchGrid();
        }

        private void ResizeSearchGrid()
        {
            var gridWidth = SearchGrid.WidthOriginal;
            var colWidth = .25;
            if (ViewModel.BillType != PropertyTaxBillType.Real)
            {
                colWidth = .33;
               // SearchGrid.ColWidth((int)GridColumns.MapLot, ((colWidth + colWidth * .33) * gridWidth).ToInteger());
            }
            SearchGrid.ColWidth((int)GridColumns.MapLot, ((colWidth + colWidth * .13) * gridWidth).ToInteger());
            SearchGrid.ColWidth((int) GridColumns.Account, (.3 * colWidth * gridWidth).ToInteger());
            SearchGrid.ColWidth((int) GridColumns.StreetNumber, (.3 * colWidth * gridWidth).ToInteger());
            SearchGrid.ColWidth((int) GridColumns.Name, ((colWidth + colWidth * .13) * gridWidth ).ToInteger());
            SearchGrid.ColWidth((int) GridColumns.StreetName, ((colWidth + colWidth * .13) * gridWidth).ToInteger());
        }
        public ITaxAccountSearchViewModel ViewModel { get; set; }

        private void SetCriteriaChoices()
        {
            cmbMatchType.Items.Clear();
            foreach (var matchOption in ViewModel.CriteriaMatchOptions)
            {
                cmbMatchType.Items.Add(matchOption);
            }

            cmbMatchType.SelectedItem = cmbMatchType.Items[0];

        }

        private void SetSearchByOptions()
        {
            cmbSearchType.Items.Clear();
            foreach (var searchOption in ViewModel.SearchByOptions)
            {
                cmbSearchType.Items.Add(searchOption);
            }

            cmbSearchType.SelectedItem = cmbSearchType.Items[0];
        }

        private enum GridColumns
        {
            Account = 0,
            Name = 1,
            StreetNumber = 2,
            StreetName = 3,
            MapLot = 4
        }

        private void Search()
        {
            if (ValidateSearch())
            {
                ViewModel.Search(GetMatchCriteria(),GetSearchByOption(),txtSearch.Text, chkShowPreviousOwnerInfo.Checked, chkShowDeletedAccountInfo);
                ShowSearchResults();
            }
        }

        private void ShowSearchResults()
        {
            SearchGrid.Rows = 1;
            foreach (var result in ViewModel.SearchResults())
            {
                AddResultToGrid(result);
            }
        }

        private void AddResultToGrid(TaxAccountSearchResult result)
        {
            SearchGrid.Rows += 1;
            var row = SearchGrid.Rows - 1;
            SearchGrid.TextMatrix(row, (int) GridColumns.Account, result.Account);
            SearchGrid.TextMatrix(row, (int) GridColumns.MapLot, result.MapLot);
            SearchGrid.TextMatrix(row, (int) GridColumns.Name, result.Name);
            SearchGrid.TextMatrix(row, (int) GridColumns.StreetName, result.StreetName);
            if (result.StreetNumber > 0)
            {
                SearchGrid.TextMatrix(row, (int) GridColumns.StreetNumber, result.StreetNumber);
            }
            else
            {
                SearchGrid.TextMatrix(row, (int)GridColumns.StreetNumber, "");
            }

            if (result.IsPreviousOwner)
            {
                SearchGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, row, 0, row, SearchGrid.Cols - 1,colorSettings.TRIOCOLORHIGHLIGHT);
            }

            if (result.IsDeleted)
            {
                SearchGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, row, 0, row, SearchGrid.Cols - 1, colorSettings.TRIOCOLORHIGHLIGHT);
            }
        }

        private TaxAccountSearchByOption GetSearchByOption()
        {
            if (cmbSearchType.SelectedItem != null)
            {
                return ((GenericDescriptionPair<TaxAccountSearchByOption>) cmbSearchType.SelectedItem).ID;
            }
            else
            {
                return TaxAccountSearchByOption.Name;
            }
        }

        private SearchCriteriaMatchType GetMatchCriteria()
        {
            if (cmbMatchType.SelectedItem != null)
            {
                return ((GenericDescriptionPair<SearchCriteriaMatchType>) cmbMatchType.SelectedItem).ID;
            }
            else
            {
                return SearchCriteriaMatchType.StartsWith;
            }
        }

        private bool ValidateSearch()
        {
            if (cmbSearchType.SelectedItem == null)
            {
                MessageBox.Show("You must select what to search by", "Missing search by", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return false;
            }

            if (cmbMatchType.SelectedItem == null)
            {
                MessageBox.Show("You must select how to match search criteria", "Missing match choice",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (String.IsNullOrWhiteSpace(txtSearch.Text))
            {
                MessageBox.Show("You must enter something to search by", "Missing Search Criteria",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            EnteredAccountNumber(false);
        }

        private void EnteredAccountNumber(bool includeSearchList)
        {
            if (txtGetAccountNumber.Text.ToIntegerValue() > 0)
            {
                SelectAccount(txtGetAccountNumber.Text.ToIntegerValue(),includeSearchList);
            }
        }

        private void SelectAccount(int accountNumber,bool includeSearchList)
        {
            if (ViewModel.SelectAccount(accountNumber,includeSearchList))
            {
                CloseWithoutCancel();
            }
        }

        private void txtGetAccountNumber_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (KeyCode == Keys.Return)
            {
                KeyCode = 0;
                EnteredAccountNumber(false);
            }
        }

        private void txtGetAccountNumber_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            int KeyAscii = Strings.Asc(e.KeyChar);
            if (KeyAscii < 48 || KeyAscii > 57)
            {
                if (KeyAscii == 8)
                {
                }
                else
                {
                    KeyAscii = 0;
                }
            }
            e.KeyChar = Strings.Chr(KeyAscii);
        }

        private void CloseWithoutCancel()
        {
            cancelling = false;
            Unload();
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            int KeyCode = FCConvert.ToInt32(e.KeyCode);
            if (KeyCode == FCConvert.ToInt32(Keys.Return))
            {
                Search();
            }
        }
    }
}
