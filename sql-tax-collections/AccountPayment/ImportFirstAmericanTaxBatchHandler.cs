﻿using System.Collections.Generic;
using System.IO;
using System.Web.Management;
using SharedApplication;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.AccountPayment;
using Wisej.Web;

namespace TWCL0000.AccountPayment
{
    public class ImportFirstAmericanTaxBatchHandler : CommandHandler<ImportFirstAmericanTaxBatch,(bool Success, IEnumerable<(int Year, int Account, decimal Amount)> Items)>
    {
        private CommandDispatcher commandDispatcher;
        public ImportFirstAmericanTaxBatchHandler(CommandDispatcher commandDispatcher)
        {
            this.commandDispatcher = commandDispatcher;
        }
        protected override (bool Success,IEnumerable<(int Year, int Account, decimal Amount)> Items) Handle(ImportFirstAmericanTaxBatch command)
        {
            var filePath = commandDispatcher.Send(new UploadFile
            {
                DialogTitle = "Select file to import from"
            }).Result;
            if (string.IsNullOrWhiteSpace(filePath))
            {
                return (Success: false,Items: new List<(int Year, int Account, decimal Amount)>());
            }

            if (!File.Exists(filePath))
            {
                return (Success: false, Items: new List<(int Year, int Account, decimal Amount)>());
            }


            var ts = new StreamReader(filePath);
            var line = "";
            var total = 0M;
            while ((line = ts.ReadLine()) != null)
            {
                total += TestFirstAmerican(line);
            }
            ts.Close();
            if (Wisej.Web.MessageBox.Show(
                    "The total file amount is " + total.FormatAsCurrencyNoSymbol() + ", is this correct?",
                    "Confirm Amount", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
            {
                return (Success: false, Items: new List<(int Year, int Account, decimal Amount)>());
            }

            var items = new List<(int Year, int Account, decimal Amount)>();
            ts = new StreamReader(filePath);
            while ((line = ts.ReadLine()) != null)
            {
                var parsedLine = ParseFirstAmerican(line);
                if (parsedLine.Account > 0 && parsedLine.Amount != 0)
                {
                    items.Add((Year: parsedLine.Year, Account: parsedLine.Account, Amount: parsedLine.Amount));
                }
            }
            ts.Close();
            return (Success: true, Items: items);
        }

        private (int Year, int Account, decimal Amount) ParseFirstAmerican(string line)
        {
            if (line.Length < 51)
            {
                return (Year: 0, Account: 0, Amount: 0);
            }

            var year = line.Substring(0, 4).ToIntegerValue();
            var account = line.Substring(4, 10).ToIntegerValue();
            var amount = line.Substring(34, 11).ToDecimalValue() / 100;

            return (Year: year, Account: account, Amount: amount);
        }

        private decimal TestFirstAmerican(string line)
        {
            if (line.Length < 47)
            {
                return 0;
            }

            if (line.Substring(4, 10).ToIntegerValue() == 0)
            {
                return 0;
            }
            
            return (line.Substring(34, 11).ToDecimalValue()) / 100;
        }
    }
}