﻿namespace TWCL0000.AccountPayment
{
    partial class REAccountTotalsView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblExemptValue = new fecherFoundation.FCLabel();
            this.lblBuildingValue = new fecherFoundation.FCLabel();
            this.lblExempt = new fecherFoundation.FCLabel();
            this.lblLandValue = new fecherFoundation.FCLabel();
            this.lblBuilding = new fecherFoundation.FCLabel();
            this.lblLand = new fecherFoundation.FCLabel();
            this.lblTotal = new fecherFoundation.FCLabel();
            this.lblTotalValue = new fecherFoundation.FCLabel();
            this.lblLandValue2 = new fecherFoundation.FCLabel();
            this.lblExemptValue2 = new fecherFoundation.FCLabel();
            this.lblBuildingValue2 = new fecherFoundation.FCLabel();
            this.lblYear = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.lblTotalValue2 = new fecherFoundation.FCLabel();
            this.SuspendLayout();
            // 
            // lblExemptValue
            // 
            this.lblExemptValue.Location = new System.Drawing.Point(93, 67);
            this.lblExemptValue.Name = "lblExemptValue";
            this.lblExemptValue.Size = new System.Drawing.Size(82, 18);
            this.lblExemptValue.TabIndex = 57;
            this.lblExemptValue.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblBuildingValue
            // 
            this.lblBuildingValue.Location = new System.Drawing.Point(93, 45);
            this.lblBuildingValue.Name = "lblBuildingValue";
            this.lblBuildingValue.Size = new System.Drawing.Size(82, 18);
            this.lblBuildingValue.TabIndex = 55;
            this.lblBuildingValue.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblExempt
            // 
            this.lblExempt.Location = new System.Drawing.Point(19, 67);
            this.lblExempt.Name = "lblExempt";
            this.lblExempt.Size = new System.Drawing.Size(63, 17);
            this.lblExempt.TabIndex = 56;
            this.lblExempt.Text = "EXEMPT";
            // 
            // lblLandValue
            // 
            this.lblLandValue.Location = new System.Drawing.Point(93, 23);
            this.lblLandValue.Name = "lblLandValue";
            this.lblLandValue.Size = new System.Drawing.Size(82, 18);
            this.lblLandValue.TabIndex = 53;
            this.lblLandValue.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblBuilding
            // 
            this.lblBuilding.Location = new System.Drawing.Point(19, 45);
            this.lblBuilding.Name = "lblBuilding";
            this.lblBuilding.Size = new System.Drawing.Size(78, 17);
            this.lblBuilding.TabIndex = 54;
            this.lblBuilding.Text = "BUILDING";
            // 
            // lblLand
            // 
            this.lblLand.Location = new System.Drawing.Point(19, 23);
            this.lblLand.Name = "lblLand";
            this.lblLand.Size = new System.Drawing.Size(45, 17);
            this.lblLand.TabIndex = 52;
            this.lblLand.Text = "LAND";
            // 
            // lblTotal
            // 
            this.lblTotal.Location = new System.Drawing.Point(20, 95);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(52, 17);
            this.lblTotal.TabIndex = 59;
            this.lblTotal.Text = "TOTAL";
            // 
            // lblTotalValue
            // 
            this.lblTotalValue.Location = new System.Drawing.Point(93, 95);
            this.lblTotalValue.Name = "lblTotalValue";
            this.lblTotalValue.Size = new System.Drawing.Size(82, 18);
            this.lblTotalValue.TabIndex = 58;
            this.lblTotalValue.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblLandValue2
            // 
            this.lblLandValue2.Location = new System.Drawing.Point(193, 23);
            this.lblLandValue2.Name = "lblLandValue2";
            this.lblLandValue2.Size = new System.Drawing.Size(82, 18);
            this.lblLandValue2.TabIndex = 62;
            this.lblLandValue2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblExemptValue2
            // 
            this.lblExemptValue2.Location = new System.Drawing.Point(193, 67);
            this.lblExemptValue2.Name = "lblExemptValue2";
            this.lblExemptValue2.Size = new System.Drawing.Size(82, 18);
            this.lblExemptValue2.TabIndex = 64;
            this.lblExemptValue2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblBuildingValue2
            // 
            this.lblBuildingValue2.Location = new System.Drawing.Point(193, 45);
            this.lblBuildingValue2.Name = "lblBuildingValue2";
            this.lblBuildingValue2.Size = new System.Drawing.Size(82, 18);
            this.lblBuildingValue2.TabIndex = 63;
            this.lblBuildingValue2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblYear
            // 
            this.lblYear.Alignment = Wisej.Web.HorizontalAlignment.Right;
            this.lblYear.AutoSize = true;
            this.lblYear.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblYear.Location = new System.Drawing.Point(230, 3);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(45, 17);
            this.lblYear.TabIndex = 61;
            this.lblYear.Text = "YEAR";
            this.lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label5
            // 
            this.Label5.Alignment = Wisej.Web.HorizontalAlignment.Right;
            this.Label5.AutoSize = true;
            this.Label5.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Label5.Location = new System.Drawing.Point(102, 3);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(73, 17);
            this.Label5.TabIndex = 60;
            this.Label5.Text = "CURRENT";
            this.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblTotalValue2
            // 
            this.lblTotalValue2.Location = new System.Drawing.Point(193, 95);
            this.lblTotalValue2.Name = "lblTotalValue2";
            this.lblTotalValue2.Size = new System.Drawing.Size(82, 18);
            this.lblTotalValue2.TabIndex = 65;
            this.lblTotalValue2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // REAccountTotalsView
            // 
            this.Controls.Add(this.lblTotalValue2);
            this.Controls.Add(this.lblLandValue2);
            this.Controls.Add(this.lblExemptValue2);
            this.Controls.Add(this.lblBuildingValue2);
            this.Controls.Add(this.lblYear);
            this.Controls.Add(this.Label5);
            this.Controls.Add(this.lblExemptValue);
            this.Controls.Add(this.lblBuildingValue);
            this.Controls.Add(this.lblExempt);
            this.Controls.Add(this.lblLandValue);
            this.Controls.Add(this.lblBuilding);
            this.Controls.Add(this.lblLand);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.lblTotalValue);
            this.Name = "REAccountTotalsView";
            this.Size = new System.Drawing.Size(284, 120);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public fecherFoundation.FCLabel lblExemptValue;
        public fecherFoundation.FCLabel lblBuildingValue;
        public fecherFoundation.FCLabel lblExempt;
        public fecherFoundation.FCLabel lblLandValue;
        public fecherFoundation.FCLabel lblBuilding;
        public fecherFoundation.FCLabel lblLand;
        public fecherFoundation.FCLabel lblTotal;
        public fecherFoundation.FCLabel lblTotalValue;
        public fecherFoundation.FCLabel lblLandValue2;
        public fecherFoundation.FCLabel lblExemptValue2;
        public fecherFoundation.FCLabel lblBuildingValue2;
        public fecherFoundation.FCLabel lblYear;
        public fecherFoundation.FCLabel Label5;
        public fecherFoundation.FCLabel lblTotalValue2;
    }
}
