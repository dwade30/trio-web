﻿using System;
using Global;
using SharedApplication;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections.AccountPayment;
using Wisej.Web;

namespace TWCL0000.AccountPayment
{
    public partial class frmTaxPaymentInfo : BaseForm, IView<ITaxPaymentInfoViewModel>
    {
        public frmTaxPaymentInfo()
        {
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            this.Load += FrmTaxPaymentInfo_Load;
        }

        private void FrmTaxPaymentInfo_Load(object sender, EventArgs e)
        {
            ShowDetails();
        }

        public  frmTaxPaymentInfo(ITaxPaymentInfoViewModel viewModel): this()
        {
            ViewModel = viewModel;
        }

        public new void Show()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            if (!this.Visible)
            {
                //FC:FINAL:MSH - i.issue #1839: sometimes new modal window can be overlapped by already opened modal window
                this.BringToFront();
                base.Show(FormShowEnum.Modeless, "", null);
            }
        }

        private void ShowDetails()
        {
            ShowLeftPanel();
            ShowRightPanel();
        }

        private void ShowLeftPanel()
        {
            var payment = ViewModel.Payment;
            LeftPanel.Controls.Add(GetControl("Account",payment.Account.GetValueOrDefault().ToString()));
            LeftPanel.Controls.Add(GetControl("Tax Year", payment.TaxYear() + "-" + payment.BillNumber()));
            if (payment.IsLien())
            {
                LeftPanel.Controls.Add(GetControl("Lien Id",payment.BillId.GetValueOrDefault().ToString()));
            }
            else
            {
                LeftPanel.Controls.Add(GetControl("Bill Id", payment.BillId.GetValueOrDefault().ToString()));
            }

            if (ViewModel.ReceiptNumber > 0)
            {
                LeftPanel.Controls.Add(GetControl("Receipt Number", ViewModel.ReceiptNumber.ToString()));
            }

            LeftPanel.Controls.Add(GetControl("",""));
            if (payment.RecordedTransactionDate.GetValueOrDefault() != DateTime.MinValue)
            {
                LeftPanel.Controls.Add(GetControl("Recorded Transaction Date", payment.RecordedTransactionDate.Value.FormatAndPadShortDate()));
            }

            if (payment.EffectiveInterestDate.GetValueOrDefault() != DateTime.MinValue)
            {
                LeftPanel.Controls.Add(GetControl("Effective Interest Date", payment.EffectiveInterestDate.Value.FormatAndPadShortDate()));
            }

            if (payment.ActualSystemDate.GetValueOrDefault() != DateTime.MinValue)
            {
                LeftPanel.Controls.Add(GetControl("Actual Transaction Date", payment.ActualSystemDate.Value.FormatAndPadShortDate()));
            }

            LeftPanel.Controls.Add(GetControl("",""));
            LeftPanel.Controls.Add(GetControl("Teller Id", payment.Teller));
            LeftPanel.Controls.Add(GetControl("", ""));
            LeftPanel.Controls.Add(GetControl("Cash Drawer", payment.CashDrawer == "Y" ? "Yes" : "No"));
            LeftPanel.Controls.Add(GetControl("Affect Cash", payment.GeneralLedger == "Y" ? "Yes" : "No"));
            if (!string.IsNullOrWhiteSpace(payment.BudgetaryAccountNumber))
            {
                LeftPanel.Controls.Add(GetControl("Budgetary Account",payment.BudgetaryAccountNumber));
            }

            if (!string.IsNullOrWhiteSpace(payment.Comments))
            {
                LeftPanel.Controls.Add(GetControl("", ""));
                LeftPanel.Controls.Add(GetControl("Comments",payment.Comments));
            }
        }

        private void ShowRightPanel()
        {
            var payment = ViewModel.Payment;
            RightPanel.Controls.Add(GetControl("Id",payment.Id.ToString()));
            RightPanel.Controls.Add(GetControl("",""));
            if (!string.IsNullOrWhiteSpace(payment.Reference))
            {
                RightPanel.Controls.Add(GetControl("Reference", payment.Reference));
            }

            if (!string.IsNullOrWhiteSpace(payment.PaidBy))
            {
                RightPanel.Controls.Add(GetControl("Paid By", payment.PaidBy));
            }

            RightPanel.Controls.Add(GetControl("Daily Closeout", payment.DailyCloseOut > 0 ? "True" : "False"));

            RightPanel.Controls.Add(GetControl("",""));
            if (payment.Principal.GetValueOrDefault() > 0)
            {
                RightPanel.Controls.Add(GetControl("Principal", payment.Principal.GetValueOrDefault().FormatAsCurrencyNoSymbol()));
            }

            if (payment.CurrentInterest.GetValueOrDefault() > 0)
            {
                RightPanel.Controls.Add(GetControl("Interest", payment.CurrentInterest.GetValueOrDefault().FormatAsCurrencyNoSymbol()));
            }

            if (payment.PreLienInterest.GetValueOrDefault() > 0)
            {
                RightPanel.Controls.Add(GetControl("Pre-Lien Interest", payment.PreLienInterest.GetValueOrDefault().FormatAsCurrencyNoSymbol()));
            }

            if (payment.LienCost.GetValueOrDefault() > 0)
            {
                RightPanel.Controls.Add(GetControl("Lien Costs", payment.LienCost.GetValueOrDefault().FormatAsCurrencyNoSymbol()));
            }

            RightPanel.Controls.Add(GetControl("",""));
            RightPanel.Controls.Add(GetControl("Total", (payment.Principal.GetValueOrDefault() + payment.CurrentInterest.GetValueOrDefault() + payment.PreLienInterest.GetValueOrDefault() + payment.LienCost.GetValueOrDefault()).FormatAsCurrencyNoSymbol()));
        }

        private TableLayoutPanel GetControl(string description, string value)
        {
            var panel = new TableLayoutPanel();
            panel.Margin = new Padding(1);
            panel.ColumnCount = 2;
            panel.RowCount = 1;
            panel.AutoSize = true;
            panel.Dock = DockStyle.Left & DockStyle.Right;
            panel.Padding = new Padding(1);

            panel.Controls.Add(new Label() { Text = description, AutoSize = false, Dock = DockStyle.Fill, Width = 165, Margin = Padding.Empty });
            panel.Controls.Add(new Label() { Text = value, AutoSize = true, Dock = DockStyle.Fill, Margin = Padding.Empty });

            return panel;
        }

        public ITaxPaymentInfoViewModel ViewModel { get; set; }
    }
}
