﻿using System;
using System.Linq;
using fecherFoundation;
using SharedApplication.Extensions;
using SharedApplication.PersonalProperty;
using Wisej.Web;

namespace TWCL0000.AccountPayment
{
    public partial class PPAccountTotalsView : Wisej.Web.UserControl
    {
        private PersonalPropertyAccountTotals accountTotals;
        private PersonalPropertyAccountTotals billedTotals;
        private int billedYear;
        public PPAccountTotalsView()
        {
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            this.Resize += PPAccountTotalsView_Resize;
        }

        private void PPAccountTotalsView_Resize(object sender, EventArgs e)
        {
            ResizeGrid();
        }

        public PPAccountTotalsView(PersonalPropertyAccountTotals accountTotals, PersonalPropertyAccountTotals billedTotals, int billedYear) : this()
        {
            this.accountTotals = accountTotals;
            this.billedTotals = billedTotals;
            this.billedYear = billedYear;
            SetupGrid();
            ShowInfo();
        }

        private void SetupGrid()
        {
            CategoryGrid.Rows = 1;
            CategoryGrid.RowHeight(-1, 300);
            CategoryGrid.ExtendLastCol = true;
            CategoryGrid.BorderStyle = BorderStyle.None;
            CategoryGrid.TextMatrix(0, 0, "Category");
            CategoryGrid.TextMatrix(0, 1, "Current");
            CategoryGrid.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
            CategoryGrid.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
        }

        private void ResizeGrid()
        {
            var origWidth = CategoryGrid.WidthOriginal;
            CategoryGrid.ColWidth(0, (origWidth * .45).ToInteger());
            CategoryGrid.ColWidth(1, (origWidth * .26).ToInteger());
            CategoryGrid.ColWidth(2, (origWidth * .255).ToInteger());
            CategoryGrid.RowHeight(-1, 300);
        }

        private void ShowInfo()
        {
            CategoryGrid.Rows = 1;
            int totalCurrent = 0;
            int totalBilled = 0;
            if (billedTotals != null)
            {
                CategoryGrid.TextMatrix(0, 2, billedYear.ToString());
                totalBilled = billedTotals.Valuations.Totals().totalAmount;
            }
            else
            {
                CategoryGrid.TextMatrix(0, 2, "");
            }

            if (accountTotals != null)
            {
                int row = 0;
                totalCurrent = accountTotals.Valuations.Totals().totalAmount;
                foreach (var valuation in accountTotals.Valuations.Valuations)
                {
                    row = 0;
                    int currentAmount = 0;
                    int billedAmount = 0;
                    if (valuation.Amount > 0)
                    {
                        currentAmount = valuation.Amount;
                    }

                    var billedValuation = billedTotals?.Valuations.Valuations.FirstOrDefault(v => v.CategoryNumber == valuation.CategoryNumber);
                    if (billedValuation != null)
                    {
                        if (billedValuation.Amount > 0)
                        {
                            billedAmount = billedValuation.Amount;
                        }
                    }

                    if (currentAmount > 0 || billedAmount > 0)
                    {
                        CategoryGrid.Rows += 1;
                        row = CategoryGrid.Rows - 1;
                        CategoryGrid.TextMatrix(row, 0, valuation.CategoryName);
                        CategoryGrid.TextMatrix(row, 1, currentAmount.FormatAsNumber());
                        CategoryGrid.TextMatrix(row, 2, billedAmount.FormatAsNumber());
                    }
                }
                CategoryGrid.Rows += 1;
                row = CategoryGrid.Rows - 1;
                CategoryGrid.TextMatrix(row, 0, "Total");
                CategoryGrid.TextMatrix(row, 1, totalCurrent.FormatAsNumber());
                CategoryGrid.TextMatrix(row, 2, totalBilled.FormatAsNumber());
                CategoryGrid.Select(row - 1, 1, row - 1, 2);
                CategoryGrid.CellBorder(System.Drawing.Color.Black, 0, 0, 0, 1, 0, 1);
            }
           
            CategoryGrid.RowHeight(-1, 300);

        }
    }
}
