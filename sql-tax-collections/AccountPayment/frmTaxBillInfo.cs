﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication;
using SharedApplication.Extensions;
using SharedApplication.PersonalProperty;
using SharedApplication.RealEstate;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.AccountPayment;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Models;
using Wisej.Web;

namespace TWCL0000.AccountPayment
{
    public partial class frmTaxBillInfo : BaseForm, IView<ITaxBillInfoViewModel>
    {
        public frmTaxBillInfo()
        {
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            this.Load += FrmTaxBillInfo_Load;
            cmdPrint.Click += CmdPrint_Click;
        }

        private void CmdPrint_Click(object sender, EventArgs e)
        {
            PrintBillInfo();
        }

        private void PrintBillInfo()
        {
            ViewModel.Print();
            Close();
        }

        public frmTaxBillInfo(ITaxBillInfoViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }
        public ITaxBillInfoViewModel ViewModel { get; set; }

        public new void Show()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            if (!this.Visible)
            {
                //FC:FINAL:MSH - i.issue #1839: sometimes new modal window can be overlapped by already opened modal window
                this.BringToFront();
                base.Show(FormShowEnum.Modeless, "", null);
            }
        }

        private void FrmTaxBillInfo_Load(object sender, EventArgs e)
        {
            ShowDetails();
        }

        private void ShowDetails()
        {
            ShowRatePanel();
            ShowBillPanel();
        }

        private void ShowRatePanel()
        {
	        RateLayout.Controls.Add(GetHeaderControl("Bill Information"));
            if (ViewModel.TaxBill.RateId > 0)
            {
                if (ViewModel.TaxBill.HasLien())
                {
                    ShowLienRate();
                }
                else
                {
                    ShowRegularRate();
                }
                
            }
            else
            {
                RateLayout.Controls.Add(GetSingleControl("No Tax Rate Assigned"));
            }
        }

        private void ShowRegularRate()
        {
            RateLayout.Controls.Add(GetControl("Rate Id", ViewModel.TaxBill.RateId.ToString()));
            var rate = ViewModel.TaxBill.RateRecord;
            ShowRateInfo(rate,ViewModel.TaxBill.GetInstallmentsDue(0),ViewModel.TaxBill.ID);            
        }
        private void ShowLienRate()
        {
            var lien = ViewModel.TaxBill.Lien;
            RateLayout.Controls.Add(GetControl("Rate Id", lien.RateId.ToString()));
            var rate = lien.RateRecord;
            ShowRateInfo(rate, new List<decimal>() {lien.LienSummary.GetNetOwed()}, lien.Id);          
        }

        private void ShowRateInfo(TaxRateInfo rate, IEnumerable<decimal> installments,int billOrLienId)
        {
            if (rate != null)
            {
                if (rate.RateType != PropertyTaxRateType.Lien)
                {
                    RateLayout.Controls.Add(GetControl("Tax Rate", (rate.TaxRate * 1000).ToString()));
                }

                RateLayout.Controls.Add(GetControl("# of Periods", rate.Installments.Count.ToString()));
                string rateTypeDescription;
                switch (rate.RateType)
                {
                    case PropertyTaxRateType.Lien:
                        rateTypeDescription = "Lien";
                        break;
                    case PropertyTaxRateType.Supplemental:
                        rateTypeDescription = "Supplemental";
                        break;
                    default:
                        rateTypeDescription = "Regular";
                        break;
                }

                RateLayout.Controls.Add(GetControl("Rate Type", rateTypeDescription));
                if (rate.RateType != PropertyTaxRateType.Lien)
                {
                    RateLayout.Controls.Add(GetControl("Bill Id", billOrLienId.ToString()));
                }
                else
                {
                    RateLayout.Controls.Add(GetControl("Lien Id", billOrLienId.ToString()));
                }
                RateLayout.Controls.Add(GetControl("", ""));
                foreach (var installment in rate.Installments)
                {
                    RateLayout.Controls.Add(GetControl("Interest Start Period " + installment.InstallmentNumber.ToString(),
                        installment.InterestStartDate.FormatAndPadShortDate()));
                }

                foreach (var installment in rate.Installments)
                {
                    RateLayout.Controls.Add(GetControl("Due Date Period " + installment.InstallmentNumber.ToString(),
                        installment.DueDate.FormatAndPadShortDate()));
                }
                RateLayout.Controls.Add(GetControl("Interest Rate", (rate.InterestRate * 100).ToString() + "%"));

                RateLayout.Controls.Add(GetControl("",""));
                int counter = 0;
                foreach (var installment in installments)
                {
                    counter += 1;
                    decimal extra = 0;
                    if (counter == 1)
                    {
                        extra = ViewModel.CalculationSummary.Interest;
                    }
                    RateLayout.Controls.Add(GetControl("Period " + counter + " Due", (installment + extra).FormatAsMoney()));
                }

                if (ViewModel.CalculationSummary.PerDiem != 0)
                {
                    RateLayout.Controls.Add(GetControl("", ""));
                    RateLayout.Controls.Add(GetControl("Per Diem",
                        ViewModel.CalculationSummary.PerDiem.ToString("N4")));
                }
            }
        }
        private void ShowBillPanel()
        {
            if (ViewModel.TaxBill.BillType == PropertyTaxBillType.Personal)
            {
                ShowBilledPPInfo();
            }
            else
            {
                ShowBilledREInfo();
            }

        }

        private void ShowBilledREInfo()
        {
            var reBill = (RealEstateTaxBill) ViewModel.TaxBill;
            BillLayout.Controls.Add(GetHeaderControl(reBill.TaxYear + "-" + reBill.BillNumber + " Information"));
            BillLayout.Controls.Add(GetControl("Land Value",reBill.LandValue.ToString("#,##0")));
            BillLayout.Controls.Add(GetControl("Building Value",reBill.BuildingValue.ToString("#,##0")));
            BillLayout.Controls.Add(GetControl("Exempt Value",reBill.ExemptValue.ToString("##,##0")));
            BillLayout.Controls.Add(GetControl("Net Value", (reBill.LandValue + reBill.BuildingValue - reBill.ExemptValue).ToString("#,##0")));
            BillLayout.Controls.Add(GetHeaderControl("Current Information"));

            if (ViewModel.AccountTotals != null)
            {
                var accountTotals = (RealEstateAccountTotals)ViewModel.AccountTotals;
                BillLayout.Controls.Add(GetControl("Land Value", accountTotals.Land.ToString("#,##0")));
                BillLayout.Controls.Add(GetControl("Building Value", accountTotals.Building.ToString("#,##0")));
                BillLayout.Controls.Add(GetControl("Exempt Value",accountTotals.Exemptions.ToString("#,##0")));
                BillLayout.Controls.Add(GetControl("Net Value",accountTotals.NetTaxable().ToString("#,##0")));
                BillLayout.Controls.Add(GetControl("", ""));
            }

            if (reBill.HasLien())
            {
	            BillLayout.Controls.Add(GetControl("Interest Paid Date", reBill.Lien.InterestAppliedThroughDate != null ? reBill.Lien.InterestAppliedThroughDate.Value.FormatAndPadShortDate() : ""));
	            if (ViewModel.TotalPerDiem != 0)
	            {
		            BillLayout.Controls.Add(GetControl("Total Per Diem", ViewModel.TotalPerDiem.ToString("N4")));
	            }
	            BillLayout.Controls.Add(GetControl("", ""));
                ShowRateDates(reBill.Lien.RateRecord);
            }
            else
            {
	            BillLayout.Controls.Add(GetControl("Interest Paid Date", reBill.InterestAppliedThroughDate != null ? reBill.InterestAppliedThroughDate.Value.FormatAndPadShortDate() : ""));
	            if (ViewModel.TotalPerDiem != 0)
	            {
		            BillLayout.Controls.Add(GetControl("Total Per Diem", ViewModel.TotalPerDiem.ToString("N4")));
	            }
	            BillLayout.Controls.Add(GetControl("", ""));
                ShowRateDates(reBill.RateRecord);
            }
            BillLayout.Controls.Add(GetControl("Map Lot", reBill.MapLot));
            BillLayout.Controls.Add(GetControl("Book Page",reBill.BookPage));
            if (reBill.HasLien() && (!string.IsNullOrWhiteSpace(reBill.Lien.Book) || !string.IsNullOrWhiteSpace(reBill.Lien.Page)))
            {
                BillLayout.Controls.Add(GetControl("Lien Book Page", "B" + reBill.Lien.Book + "P" + reBill.Lien.Page));
            }
            if (reBill.HasLien() && ViewModel.LienDischargeBookPage != "")
            {
	            BillLayout.Controls.Add(GetControl("Lien Book Page", ViewModel.LienDischargeBookPage));
            }
        }

        private void ShowRateDates(TaxRateInfo rate)
        {
            if (rate == null)
            {
                return;
            }
            if (rate.RateType != PropertyTaxRateType.Lien)
            {
                BillLayout.Controls.Add(GetControl("Billing Date",rate.BillingDate.FormatAndPadShortDate()));
            }
            else
            {
                BillLayout.Controls.Add(GetControl("Lien Date", rate.BillingDate.FormatAndPadShortDate()));
            }

            if (rate.CreationDate != DateTime.MinValue)
            {
                BillLayout.Controls.Add(GetControl("Creation Date", rate.CreationDate.FormatAndPadShortDate()));
            }

            if (rate.CommitmentDate != DateTime.MinValue)
            {
                BillLayout.Controls.Add(GetControl("Commitment Date", rate.CommitmentDate.FormatAndPadShortDate()));
            }
        }
        private void ShowBilledPPInfo()
        {
            var ppBill = (PersonalPropertyTaxBill) ViewModel.TaxBill;
            //BillLayout.Controls.Add(GetHeaderControl(ppBill.TaxYear + "-" + ppBill.BillNumber + " Information"));
            BillLayout.Controls.Add(GetDoubleControl("","Current", ppBill.TaxYear + "-" + ppBill.BillNumber));
            var billValuations = ppBill.PersonalPropertyValuations.Valuations.OrderBy(v => v.CategoryNumber).ToArray();
            var totals = (PersonalPropertyAccountTotals) ViewModel.AccountTotals;
            var currentValuations = totals.Valuations.Valuations.OrderBy(v => v.CategoryNumber).ToArray();
            for (int i = 0;i < 9; i++)
            {
                BillLayout.Controls.Add(GetDoubleControl("Category " + (i + 1).ToString(),currentValuations[i].Amount.ToString("#,##0"),billValuations[i].Amount.ToString("#,##0")));
            }
            BillLayout.Controls.Add(GetControl("",""));
            BillLayout.Controls.Add(GetControl("Interest Paid Date", ppBill.InterestAppliedThroughDate != null ? ppBill.InterestAppliedThroughDate.Value.FormatAndPadShortDate() : ""));
            if (ViewModel.TotalPerDiem != 0)
            {
                BillLayout.Controls.Add(GetControl("Total Per Diem", ViewModel.TotalPerDiem.ToString("N4")));
            }
            BillLayout.Controls.Add(GetControl("", ""));
            ShowRateDates(ppBill.RateRecord);
        }

        private TableLayoutPanel GetControl(string description, string value)
        {
            var panel = new TableLayoutPanel();
            panel.Margin = new Padding(1);
            panel.ColumnCount = 2;
            panel.RowCount = 1;
            panel.AutoSize = true;
            panel.Dock = DockStyle.Left & DockStyle.Right;
            panel.Padding = new Padding(1);
            
            panel.Controls.Add(new Label() { Text = description, AutoSize = false, Dock = DockStyle.Fill, Width = 135, Margin = Padding.Empty});
            panel.Controls.Add(new Label() { Text = value, AutoSize = true, Dock = DockStyle.Fill , Margin = Padding.Empty});
            
            return panel;
        }

        private TableLayoutPanel GetDoubleControl(string description, string value1, string value2)
        {
            var panel = new TableLayoutPanel();
            panel.Margin = new Padding(1);
            panel.ColumnCount = 3;
            panel.RowCount = 1;
            panel.AutoSize = true;
            panel.Dock = DockStyle.Left & DockStyle.Right;
            panel.Padding = new Padding(1);
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute,135));
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50));
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50));
            panel.Controls.Add(new Label() { Text = description, AutoSize = false, Dock = DockStyle.Fill, Margin = Padding.Empty });
            panel.Controls.Add(new Label() { Text = value1, AutoSize = false, Dock = DockStyle.Fill, Margin = Padding.Empty });
            panel.Controls.Add(new Label() { Text = value2, AutoSize = false, Dock = DockStyle.Fill, Margin = Padding.Empty });
            return panel;
        }

        private Label GetSingleControl(string description)
        {
            var label = new Label();
            label.Dock = DockStyle.Left & DockStyle.Right;
            label.Text = description;
            return label;
        }
        
        private Label GetHeaderControl(string description)
        {
            var label = new Label();
            label.Dock = DockStyle.Left & DockStyle.Right;
            label.Text = description;
            label.TextAlign = ContentAlignment.TopLeft;                        
            //label.SetFontBold(true);
            label.SetFontSize(14);
            label.AutoSize = true;
            return label;
        }
    }
}
