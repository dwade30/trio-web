﻿using System;
using fecherFoundation.Extensions;
using Global;
using SharedApplication;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections.AccountPayment;
using Wisej.Web;

namespace TWCL0000.AccountPayment
{
    public partial class frmChooseDiscount : BaseForm, IModalView<IGetPropertyTaxDiscountViewModel>
    {
        private bool cancelling = true;
        public frmChooseDiscount()
        {
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            this.Load += FrmChooseDiscount_Load;
            this.btnRecalc.Click += BtnRecalc_Click;
            txtPrincipal.Validating += TxtPrincipal_Validating;
            this.btnProcess.Click += BtnProcess_Click;
            this.FormClosing += FrmChooseDiscount_FormClosing;
            txtPrincipal.KeyPress += TxtPrincipal_KeyPress;
        }

        private void TxtPrincipal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                if (txtPrincipal.Text.ToDecimalValue() < 0)
                {
                    return;
                }
               ValidatePrincipal(); 
            }
        }

        private void FrmChooseDiscount_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (cancelling)
            {
                ViewModel.Cancel();
            }
        }

        private void BtnProcess_Click(object sender, EventArgs e)
        {
            ViewModel.SetPrincipal(txtPrincipal.Text.ToDecimalValue());
            ViewModel.SetPayment(ViewModel.Principal,ViewModel.Discount);
            CloseWithoutCancelling();
        }

        private void CloseWithoutCancelling()
        {
            cancelling = false;
            Close();
        }
        private void TxtPrincipal_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (txtPrincipal.Text.ToDecimalValue() < 0)
            {
                e.Cancel = true;
                return;
            }

            ValidatePrincipal();
            
        }

        private void ValidatePrincipal()
        {
            ViewModel.SetPrincipal(txtPrincipal.Text.ToDecimalValue());
            Reshow();
        }


        private void BtnRecalc_Click(object sender, EventArgs e)
        {
            Recalculate();
        }

        private void Recalculate()
        {
            ViewModel.Recalculate();
            Reshow();
        }

        private void FrmChooseDiscount_Load(object sender, EventArgs e)
        {
            lblOriginalTax.Text = ViewModel.OriginalPrincipal.FormatAsCurrencyNoSymbol();
            Reshow();
        }

        private void Reshow()
        {
            txtPrincipal.Text = ViewModel.Principal.FormatAsMoney();
            txtDiscount.Text = ViewModel.Discount.FormatAsMoney();
            lblTotal.Text = (ViewModel.Principal + ViewModel.Discount).FormatAsCurrencyNoSymbol();
        }
        public frmChooseDiscount(IGetPropertyTaxDiscountViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }
        public IGetPropertyTaxDiscountViewModel ViewModel { get; set; }
        public void ShowModal()
        {
            //this.Show();
            this.Show(FormShowEnum.Modal);
        }
    }
}
