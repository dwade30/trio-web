﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication;
using SharedApplication.TaxCollections.Lien;
using SharedApplication.TaxCollections.Models;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCL0000.AccountPayment
{
	/// <summary>
	/// Summary description for frmLienDischargeNoticeBP.
	/// </summary>
	public partial class frmLienDischargeNoticeReport : BaseForm, IView<ITaxLienDischargeNoticeReportViewModel>
	{
		public frmLienDischargeNoticeReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        public frmLienDischargeNoticeReport(ITaxLienDischargeNoticeReportViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }

		private void InitializeComponentEx()
		{
			
		}
		

	

		private void frmLienDischargeNoticeBP_Load(object sender, System.EventArgs e)
		{
            LoadSettings();
		}

		private void frmLienDischargeNoticeBP_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
            FormUtilities.KeyPressHandler(e, this);
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void SaveSettings()
		{
			try
			{

                var dischargeNotice = ViewModel.GetControlDischargeNotice() ?? new ControlDischargeNotice();

                dischargeNotice.AltTopText = txtTop.Text;
                dischargeNotice.AltBottomText = txtBottom.Text;
                dischargeNotice.AltName1 = txtName1.Text;
                dischargeNotice.AltName2 = txtName2.Text;
                dischargeNotice.AltName3 = txtName3.Text;
                dischargeNotice.AltName4 = txtName4.Text;
				ViewModel.SaveNotice(dischargeNotice);
				// show the report for this account
                ViewModel.ShowReport();				
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Saving Settings");
			}
		}

		private void LoadSettings()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will load the settings from the database
				lblStart.Text = "Dear, Taxpayer" + "\r\n" + "     ";
				lblBKList.Text = "Year      MapLot    Date           Book Page    Discharge Date   Book Page" + "\r\n";
				lblBKList.Text = lblBKList.Text + FCConvert.ToString(DateTime.Today.Year - 1) + "-1   000-000   00/00/0000   0000 0000    00/00/0000         0000 0000";
				txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
                var dischargeNotice = ViewModel.GetControlDischargeNotice();
				
				if (dischargeNotice != null)
				{
					if (!string.IsNullOrWhiteSpace(dischargeNotice.AltTopText))
                    {
                        txtTop.Text = dischargeNotice.AltTopText;
                        txtBottom.Text = dischargeNotice.AltBottomText;
                        txtName1.Text = dischargeNotice.AltName1;
						txtName2.Text = dischargeNotice.AltName2;
                        txtName3.Text = dischargeNotice.AltName3;
						txtName4.Text = dischargeNotice.AltName4;
                    }
					else
					{
						// start from scratch
						txtTop.Text = "This letter is to confirm the tax liens on the above referenced property through the municipality of " + ViewModel.TownName() + ", have been satisfied.  Below is a list of said liens along with the lien discharge information.";
						txtBottom.Text = "This should be sufficient information to provide to bank institutions etc.  Should you have any questions, please to contact me.";
						txtName1.Text = "";
						txtName2.Text = "";
						txtName3.Text = "";
						txtName4.Text = "";
					}
				}
				else
				{
					// start from scratch
					txtTop.Text = "This letter is to confirm the tax liens on the above referenced property through the municipality of " + ViewModel.TownName() + ", have been satisfied.  Below is a list of said liens along with the lien discharge information.";
					txtBottom.Text = "This should be sufficient information to provide to bank institutions etc.  Should you have any questions, please to contact me.";
					txtName1.Text = "";
					txtName2.Text = "";
					txtName3.Text = "";
					txtName4.Text = "";
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Loading Settings");
			}
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			SaveSettings();
		}	

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFileSaveExit_Click(sender, e);
		}

        public ITaxLienDischargeNoticeReportViewModel ViewModel { get; set; }
    }
}
