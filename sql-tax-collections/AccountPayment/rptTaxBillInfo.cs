﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Global;
using GrapeCity.ActiveReports.Document.Section;
using SharedApplication;
using SharedApplication.Extensions;
using SharedApplication.PersonalProperty;
using SharedApplication.RealEstate;
using SharedApplication.TaxCollections.AccountPayment;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Models;
using TWSharedLibrary;

#if TWCL0000
using TWCL0000;


#elif TWBD0000
using TWBD0000;


#else
using TWCR0000;
#endif

namespace TWCL0000.AccountPayment
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptTaxBillInfo : BaseSectionReport, IView<ITaxBillInfoReportModel>
	{

		bool boolFirstTime;

		public rptTaxBillInfo()
		{
			InitializeComponent();
			InitializeComponentEx();
		}

        public rptTaxBillInfo(ITaxBillInfoReportModel viewModel) : this()
        {
            ViewModel = viewModel;
        }

		private void InitializeComponentEx()
		{
			this.Name = "Account Information";
		}


		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (boolFirstTime)
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
				boolFirstTime = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			SetHeaderString();
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
            lblMuniName.Text = ViewModel.ClientName;
        }

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
            fldOut10.Text = "";
            
            fldOut14.Text = "Bill Id";
            fldOut24.Text = ViewModel.TaxBill.ID.ToString();
            fldOut30.Text = ViewModel.TaxBill.TaxYear + "-" + ViewModel.TaxBill.BillNumber + " Information";
            if (ViewModel != null)
            {
                ShowRateInfo();
                if (ViewModel.TaxBill.BillType == PropertyTaxBillType.Personal)
                {
                    ShowPPINfo();
                }
                else
                {
                    ShowREInfo();
                }
            }
		}

        private void ShowREInfo()
        {

            var bill = (RealEstateTaxBill)ViewModel.TaxBill;
            fldOut31.Text = "Land Value";
            fldOut41.Text = bill.LandValue.ToString("#,##0");

            fldOut32.Text = "BuildingValue";
            fldOut42.Text = bill.BuildingValue.ToString("#,##0");

            fldOut33.Text = "Exempt Value";
            fldOut43.Text = bill.ExemptValue.ToString("#,##0");

            fldOut34.Text = "Net Value";
            fldOut44.Text = (bill.LandValue + bill.BuildingValue - bill.ExemptValue).ToString("#,##0");

            fldOut35.Text = "Current Information";
            fldOut36.Text = "Land Value";
            fldOut37.Text = "Building Value";
            fldOut38.Text = "ExemptValue";
            fldOut39.Text = "Net Value";

            var accountTotals = (RealEstateAccountTotals)ViewModel.AccountTotals;
            fldOut46.Text = accountTotals.Land.ToString("#,##0");
            fldOut47.Text = accountTotals.Building.ToString("#,##0");
            fldOut48.Text = accountTotals.Exemptions.ToString("#,##0");
            fldOut49.Text = accountTotals.NetTaxable().ToString("#,##0");

            fldOut311.Text = "Interest Paid Date";
            fldOut411.Text = bill.InterestAppliedThroughDate != null
                ? bill.InterestAppliedThroughDate.Value.FormatAndPadShortDate()
                : "";
            if (ViewModel.TotalPerDiem != 0)
            {
                fldOut312.Text = "Total Per Diem";
                fldOut412.Text = ViewModel.TotalPerDiem.ToString("N4");
            }

            ShowRateDates(bill.RateRecord);

            fldOut318.Text = "Map Lot";
            fldOut418.Text = bill.MapLot;
            fldOut319.Text = "Book Page";
            fldOut419.Text = bill.BookPage;
        }

        private void ShowRateDates(TaxRateInfo rate)
        {
            if (rate == null)
            {
                return;
            }

            if (rate.RateType != PropertyTaxRateType.Lien)
            {
                fldOut314.Text = "Billing Date";                
            }
            else
            {
                fldOut314.Text = "LienDate";
            }
            fldOut414.Text = rate.BillingDate.FormatAndPadShortDate();
            if (rate.CreationDate != DateTime.MinValue)
            {
                fldOut315.Text = "Creation Date";
                fldOut415.Text = rate.CreationDate.FormatAndPadShortDate();
            }

            if (rate.CommitmentDate != DateTime.MinValue)
            {
                fldOut316.Text = "Commitment Date";
                fldOut416.Text = rate.CommitmentDate.FormatAndPadShortDate();
            }
        }

        private void ShowRateInfo()
        {
            if (ViewModel.TaxBill.RateRecord != null)
            {
                fldOut11.Text = "Tax Rate";
                fldOut12.Text = "# of Periods";
                fldOut13.Text = "Rate Type";

                var rateRecord = ViewModel.TaxBill.RateRecord;
                var numPeriods = rateRecord.Installments.Count;

                fldOut21.Text = rateRecord.TaxRate.ToString();
                fldOut22.Text = numPeriods.ToString();
                switch (rateRecord.RateType)
                {
                    case PropertyTaxRateType.Lien:
                        fldOut23.Text = "Lien";
                        break;
                    case PropertyTaxRateType.Supplemental:
                        fldOut23.Text = "Supplemental";
                        break;
                    default:
                        fldOut23.Text = "Regular";
                        break;
                }

                var billInstallments = ViewModel.TaxBill.GetInstallmentsDue(0).ToArray();
                var installments = rateRecord.Installments.OrderBy(i => i.InstallmentNumber).ToArray();

                fldOut16.Text = "Interest Start Period 1";
                fldOut110.Text = "Due Date Period 1";
                fldOut114.Text = "Period 1 Due";
                fldOut26.Text = installments[0].InterestStartDate.FormatAndPadShortDate();
                fldOut210.Text = installments[0].DueDate.FormatAndPadShortDate();
                fldOut214.Text = billInstallments[0].FormatAsMoney();
                
                if (numPeriods > 1)
                {
                    fldOut17.Text = "Interest Start Period 2";
                    fldOut111.Text = "Due Date Period 2";
                    fldOut115.Text = "Period 2 Due";
                    fldOut27.Text = installments[1].InterestStartDate.FormatAndPadShortDate();
                    fldOut211.Text = installments[1].DueDate.FormatAndPadShortDate();
                    fldOut215.Text = billInstallments[1].FormatAsMoney();
                }

                if (numPeriods > 2)
                {
                    fldOut18.Text = "Interest Start Period 3";
                    fldOut112.Text = "Due Date Period 3";
                    fldOut116.Text = "Period 3 Due";
                    fldOut28.Text = installments[2].InterestStartDate.FormatAndPadShortDate();
                    fldOut212.Text = installments[2].DueDate.FormatAndPadShortDate();
                    fldOut216.Text = billInstallments[2].FormatAsMoney();
                }

                if (numPeriods > 3)
                {
                    fldOut19.Text = "Interest Start Period 4";
                    fldOut113.Text = "Due Date Period 4";
                    fldOut117.Text = "Period 4 Due";
                    fldOut29.Text = installments[4].InterestStartDate.FormatAndPadShortDate();
                    fldOut213.Text = installments[4].DueDate.FormatAndPadShortDate();
                    fldOut217.Text = billInstallments[4].FormatAsMoney();
                }

                if (ViewModel.CalculationSummary.PerDiem != 0)
                {
                    fldOut119.Text = "Per Diem";
                    fldOut219.Text = ViewModel.CalculationSummary.PerDiem.ToString("N4");
                }
            }
        }

        private void ShowPPINfo()
        {
            var ppBill = (PersonalPropertyTaxBill)ViewModel.TaxBill;
            fldOut30.Text = "Current";
            fldOut30.Alignment = TextAlignment.Right;
            fldOut40.Alignment = TextAlignment.Right;
            fldOut40.Text = ppBill.TaxYear + "-" + ppBill.BillNumber;
            txtCat1.Visible = true;
            txtCat2.Visible = true;
            txtCat3.Visible = true;
            txtCat4.Visible = true;
            txtCat5.Visible = true;
            txtCat6.Visible = true;
            txtCat7.Visible = true;
            txtCat8.Visible = true;
            txtCat9.Visible = true;
            var billValuations = ppBill.PersonalPropertyValuations.Valuations.OrderBy(v => v.CategoryNumber).ToArray();
            var totals = (PersonalPropertyAccountTotals)ViewModel.AccountTotals;
            var currentValuations = totals.Valuations.Valuations.OrderBy(v => v.CategoryNumber).ToArray();
            for (int i = 0; i < 9; i++)
            {
                if (currentValuations.Length > i)
                {
                    var box = (GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[
                        "fldOut3" + (i + 1).ToString()];
                    box.Text = currentValuations[i].Amount.ToString("#,##0");
                    box.Alignment = TextAlignment.Right;
                }
                if (billValuations.Length > i)
                {
                    var box =
                        (GrapeCity.ActiveReports.SectionReportModel.TextBox) Detail.Controls[
                            "fldOut4" + (i + 1).ToString()];
                    box.Text = billValuations[i].Amount.ToString("#,##0");
                    box.Alignment = TextAlignment.Right;
                }
            }

            fldOut311.Text = "Interest Paid Date";
            if (ppBill.InterestAppliedThroughDate != null &&
                ppBill.InterestAppliedThroughDate.GetValueOrDefault() != DateTime.MinValue)
            {
                fldOut411.Text = ppBill.InterestAppliedThroughDate.GetValueOrDefault().FormatAndPadShortDate();
            }
            if (ViewModel.TotalPerDiem != 0)
            {
                fldOut312.Text = "Total Per Diem";
                fldOut412.Text = ViewModel.TotalPerDiem.ToString("N4");
            }

            ShowRateDates(ppBill.RateRecord);
        }

		private void SetHeaderString()
		{
			// this will set the correct header for this account
			string strTemp = "";
			int lngAcctNum = 0;
            lngAcctNum = ViewModel.TaxBill.Account.GetValueOrDefault();
            if (ViewModel.TaxBill.BillType == PropertyTaxBillType.Real)
			{
				strTemp = "RE";
                lblRef1.Text = "Ref1:";
                lblRef1Val.Text = ((RealEstateTaxBill) ViewModel.TaxBill).Reference1;
            }
			else
			{
				strTemp = "PP";			
			}

            strTemp += " Account " + FCConvert.ToString(lngAcctNum);//+ " " +  frmRECLStatus.InstancePtr.fraRateInfo.Text;
            lblAsOf.Text = "as of " + ViewModel.EffectiveDate.FormatAndPadShortDate();
			lblHeader.Text = strTemp;
			lblName.Text = "Name: " + ViewModel.TaxBill.OwnerNames() ;
			lblLocation.Text = "Location: " + MakeLocationString(ViewModel.TaxBill.StreetNumber, ViewModel.TaxBill.StreetName);

		}

        private string MakeLocationString(int streetNumber, string streetName)
        {
            if (streetNumber > 0)
            {
                return streetNumber + " " + streetName;
            }

            return streetName;
        }

        public void Show()
        {
            var reportViewer = new frmReportViewer();
            reportViewer.Init(this);
        }

        public ITaxBillInfoReportModel ViewModel { get; set; }
    }
}
