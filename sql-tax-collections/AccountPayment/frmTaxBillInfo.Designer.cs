﻿namespace TWCL0000.AccountPayment
{
    partial class frmTaxBillInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.BillLayout = new Wisej.Web.FlexLayoutPanel();
			this.RateLayout = new Wisej.Web.FlexLayoutPanel();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 636);
			this.BottomPanel.Size = new System.Drawing.Size(675, 0);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.BillLayout);
			this.ClientArea.Controls.Add(this.RateLayout);
			this.ClientArea.Size = new System.Drawing.Size(695, 649);
			this.ClientArea.Controls.SetChildIndex(this.RateLayout, 0);
			this.ClientArea.Controls.SetChildIndex(this.BillLayout, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdPrint);
			this.TopPanel.Size = new System.Drawing.Size(695, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
			// 
			// BillLayout
			// 
			this.BillLayout.LayoutStyle = Wisej.Web.FlexLayoutStyle.Vertical;
			this.BillLayout.Location = new System.Drawing.Point(361, 26);
			this.BillLayout.Name = "BillLayout";
			this.BillLayout.Size = new System.Drawing.Size(320, 610);
			this.BillLayout.TabIndex = 4;
			this.BillLayout.TabStop = true;
			// 
			// RateLayout
			// 
			this.RateLayout.LayoutStyle = Wisej.Web.FlexLayoutStyle.Vertical;
			this.RateLayout.Location = new System.Drawing.Point(21, 26);
			this.RateLayout.Name = "RateLayout";
			this.RateLayout.Size = new System.Drawing.Size(320, 610);
			this.RateLayout.TabIndex = 3;
			this.RateLayout.TabStop = true;
			// 
			// cmdPrint
			// 
			this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrint.Cursor = Wisej.Web.Cursors.Default;
			this.cmdPrint.Location = new System.Drawing.Point(615, 26);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Size = new System.Drawing.Size(66, 24);
			this.cmdPrint.TabIndex = 128;
			this.cmdPrint.Text = "Print";
			// 
			// frmTaxBillInfo
			// 
			this.ClientSize = new System.Drawing.Size(695, 709);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmTaxBillInfo";
			this.Text = "Bill Information";
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private Wisej.Web.FlexLayoutPanel BillLayout;
        private Wisej.Web.FlexLayoutPanel RateLayout;
        public fecherFoundation.FCButton cmdPrint;
    }
}