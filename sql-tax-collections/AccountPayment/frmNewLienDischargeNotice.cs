﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using SharedApplication;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections.Lien;
using SharedApplication.TaxCollections.Models;
using TWSharedLibrary;
using Wisej.Web;

#if TWCL0000
using TWCL0000;


#elif TWCR0000
using TWCR0000;


#elif TWBD0000
using TWBD0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmLienDischargeNotice.
	/// </summary>
	public partial class frmNewLienDischargeNotice : BaseForm, IModalView<ITaxLienDischargeNoticeViewModel>
	{
		public frmNewLienDischargeNotice()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{

		}

        private GlobalColorSettings colorSettings;
        public frmNewLienDischargeNotice(ITaxLienDischargeNoticeViewModel viewModel,GlobalColorSettings colorSettings) : this()
        {
            ViewModel = viewModel;
            this.colorSettings = colorSettings;
        }
		
		bool boolLoaded;
	
		bool boolPrintAllSaved;
		bool boolPrintArchive;
		bool boolAbateDefault;
		string strAbate;
		string strPayment;
		bool boolDefaultAppearedDate;
		int lngTownKey;
		// grid cols
		int lngColData;
		int lngColTitle;
		int lngColHidden;
		// grid rows
		int lngRowTName;
		int lngRowMuni;
		int lngRowCounty;
		int lngRowOwner1;
		int lngRowOwner2;
		int lngRowPayDate;
		int lngRowFileDate;
		int lngRowBook;
		int lngRowPage;
		int lngRowHisHer;
		int lngRowSignerName;
		int lngRowTitle;
		int lngRowSignerDesignation;
		int lngRowCommissionExpiration;
		int lngRowMapLot;
		int lngRowSignedDate;
		int lngRowAbatementDefault;
		int lngRowAppearedDate;
		int lngTempAccountNumber;
		DateTime dtPaymentDate;

     
		
		private void Init( )
		{

			try
            {	
				bool boolRecords = false;
				double dblYearTot = 0;
				double dblXInt = 0;
				string strFields = "";
				dtPaymentDate = ViewModel.PayDate;
				boolPrintAllSaved = false;
				boolPrintArchive = false;

					lngColData = 2;
					lngColHidden = 1;
					lngColTitle = 0;
					lngRowTName = 0;
					lngRowTitle = 1;
					lngRowMuni = 2;
					lngRowCounty = 3;
					lngRowOwner1 = 4;
					lngRowOwner2 = 5;
					lngRowPayDate = 6;
					lngRowFileDate = 7;
					lngRowSignedDate = 8;
					lngRowBook = 9;
					lngRowPage = 10;
					lngRowMapLot = 11;
					lngRowAbatementDefault = 12;
					lngRowHisHer = 13;
					lngRowSignerName = 14;
					lngRowSignerDesignation = 15;
					lngRowCommissionExpiration = 16;
					lngRowAppearedDate = 17;
					if (ViewModel.TaxBill.TaxYear > 0)
					{
						lblYear.Text = "Year : " + ViewModel.TaxBill.TaxYear.ToString() + "-" + ViewModel.TaxBill.BillNumber.ToString();
						lblYear.Visible = true;
					}
					else
					{
						lblYear.Visible = false;
					}
					strFields = "Account,Name1,Name2,BillingYear,Book,Page,MapLot,TranCode,LienRec.ID,LienRec.RateKey as LienRateKey,LienRec.InterestAppliedThroughDate,PrintedLDN";
                    lngTownKey = ViewModel.TaxBill.TranCode;

				FormatGrid(true);

                
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Initializing");
			}
		}

		private void frmLienDischargeNotice_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				boolLoaded = true;
			}
		}

		private void frmLienDischargeNotice_Load(object sender, System.EventArgs e)
		{

            Init();
			boolLoaded = true;
			FormatGrid(true);

			LoadSettings();
			strAbate = "Paid With Abatement";
			strPayment = "Paid With Payment";
		}

		private void frmLienDischargeNotice_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
            FormUtilities.KeyPressHandler(e, this);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		private void frmLienDischargeNotice_Resize(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
				FormatGrid();
			}
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private bool SaveSettings()
		{

			DateTime dtComExp;
			bool boolTemp/*unused?*/;
			// VBto upgrade warning: dtAppear As DateTime	OnWrite(DateTime, short)
			DateTime dtAppear;
			clsDRWrapper rsArchive = new clsDRWrapper();
			if (ValidateAnswers())
            {
                var dischargeNotice = ViewModel.GetControlDischargeNotice() ?? new ControlDischargeNotice();

                dischargeNotice.County = vsData.TextMatrix(lngRowCounty, lngColData);
                dischargeNotice.Treasurer = vsData.TextMatrix(lngRowTName, lngColData);
                
				if (Strings.Trim(vsData.TextMatrix(lngRowTitle, lngColData)) != "")
                {
                    dischargeNotice.TreasurerTitle = vsData.TextMatrix(lngRowTitle, lngColData);

                }
				else
                {
                    dischargeNotice.TreasurerTitle = " ";
                }
				// his/her
                if (vsData.TextMatrix(lngRowHisHer, lngColData).ToUpper().StartsWith("HIS"))
                {
                    dischargeNotice.Male = true;
                }
				else
                {
                    dischargeNotice.Male = false;
                }

                dischargeNotice.SignerName = vsData.TextMatrix(lngRowSignerName, lngColData).Trim();
                dischargeNotice.SignerDesignation = vsData.TextMatrix(lngRowSignerDesignation, lngColData).Trim();

				if (Strings.Trim(vsData.TextMatrix(lngRowCommissionExpiration, lngColData)) != "")
				{
					if (vsData.TextMatrix(lngRowCommissionExpiration, lngColData).IsDate())
                    {
                        dischargeNotice.CommissionExpiration =
                            vsData.TextMatrix(lngRowCommissionExpiration, lngColData).ToDate();
					}
					else
                    {
                        dischargeNotice.CommissionExpiration = null;
                    }
				}
				else
				{
                    dischargeNotice.CommissionExpiration = null;
                }
				ViewModel.SaveNotice(dischargeNotice);
                vsData.Select(0, 0);
                if (Strings.Trim(vsData.TextMatrix(lngRowCommissionExpiration, lngColData)) != "")
                {
                    dtComExp = DateAndTime.DateValue(vsData.TextMatrix(lngRowCommissionExpiration, lngColData));
                }
                else
                {
                    dtComExp = DateTime.FromOADate(0);
                }
                if (Strings.InStr(1, vsData.TextMatrix(lngRowAppearedDate, lngColData), "_") == 0 && Strings.Trim(vsData.TextMatrix(lngRowAppearedDate, lngColData)) != "")
                {
                    dtAppear = DateAndTime.DateValue(vsData.TextMatrix(lngRowAppearedDate, lngColData));
                }
                else
                {
                    dtAppear = DateTime.FromOADate(0);
                }
                if (Strings.UCase(Strings.Trim(vsData.TextMatrix(lngRowAbatementDefault, lngColData))) == Strings.UCase(Strings.Trim(strAbate)))
                {
                    boolAbateDefault = true;
                }
                else
                {
                    boolAbateDefault = false;
                }
                // Doevents
                arLienDischargeNotice.InstancePtr.Init(vsData.TextMatrix(lngRowTName, lngColData), vsData.TextMatrix(lngRowMuni, lngColData), vsData.TextMatrix(lngRowCounty, lngColData), vsData.TextMatrix(lngRowOwner1, lngColData), Strings.Trim(vsData.TextMatrix(lngRowOwner2, lngColData)), DateAndTime.DateValue(vsData.TextMatrix(lngRowPayDate, lngColData)), DateAndTime.DateValue(vsData.TextMatrix(lngRowFileDate, lngColData)), vsData.TextMatrix(lngRowBook, lngColData), vsData.TextMatrix(lngRowPage, lngColData), vsData.TextMatrix(lngRowHisHer, lngColData), vsData.TextMatrix(lngRowSignerName, lngColData), vsData.TextMatrix(lngRowSignerDesignation, lngColData), dtComExp, vsData.TextMatrix(lngRowMapLot, lngColData), vsData.TextMatrix(lngRowTitle, lngColData), lngTempAccountNumber, DateAndTime.DateValue(vsData.TextMatrix(lngRowSignedDate, lngColData)), boolAbateDefault, dtAppear, Conversion.Val(Strings.Right(lblYear.Text, 6)).ToString());
                // frmReportViewer.Init arLienDischargeNotice
                ViewModel.SetDischargeNeededToPrinted();
                ViewModel.SetLienDischargePrinted();
                return true;
            }

            return false;
        }

		private void LoadSettings()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will load the settings from the database
                var dischargeNotice = ViewModel.GetControlDischargeNotice();
				
				if (lngRowAbatementDefault > 0)
				{
					if (boolAbateDefault)
					{
						vsData.TextMatrix(lngRowAbatementDefault, lngColData, strAbate);
					}
					else
					{
						vsData.TextMatrix(lngRowAbatementDefault, lngColData, strPayment);
					}
				}
				if (dischargeNotice != null)
				{
					// load all of the rows
					// Row - 0  'titles
					vsData.TextMatrix(lngRowTitle, lngColData, dischargeNotice.TreasurerTitle);
					// Row - 1  'Treasurer Name
					vsData.TextMatrix(lngRowTName, lngColData, dischargeNotice.Treasurer);
					// Row - 3  'County
					// TODO: Check the table for the column [County] and replace with corresponding Get_Field method
					vsData.TextMatrix(lngRowCounty, lngColData, dischargeNotice.County);
					// Row - 10 'His/Her
					if (dischargeNotice.Male.GetValueOrDefault())
					{
						vsData.TextMatrix(lngRowHisHer, lngColData, "His");
					}
					else
					{
						vsData.TextMatrix(lngRowHisHer, lngColData, "Her");
					}
					// Signer's name - 2
					vsData.TextMatrix(lngRowSignerName, lngColData,dischargeNotice.SignerName);
					// Signer's Designation
					vsData.TextMatrix(lngRowSignerDesignation, lngColData,dischargeNotice.SignerDesignation);
					// Commission Expiration Date
					vsData.TextMatrix(lngRowCommissionExpiration, lngColData, dischargeNotice.CommissionExpiration.GetValueOrDefault().FormatAndPadShortDate());
				}
				else
                {
                    var lienControl = ViewModel.GetControlLienProcess();

					if (lienControl != null)
					{
						// Row - 0  'titles
						vsData.TextMatrix(lngRowTitle, lngColData, "Treasurer");
						// Row - 1  'Treasurer Name
						vsData.TextMatrix(lngColTitle, lngColData, lienControl.CollectorName);
						// Row - 3  'County
						// TODO: Check the table for the column [County] and replace with corresponding Get_Field method
						vsData.TextMatrix(lngRowCounty, lngColData, lienControl.County);
						// Row - 10 'His/Her
						vsData.TextMatrix(lngRowHisHer, lngColData, "Her");
						// Signer's name
						vsData.TextMatrix(lngRowSignerName, lngColData, lienControl.Signer);
						// Signer's Designation
						vsData.TextMatrix(lngRowSignerDesignation, lngColData, lienControl.Designation);
						// Commission Expiration Date
						vsData.TextMatrix(lngRowCommissionExpiration, lngColData, "");
					}
					else
					{
						// Row - 0  'titles
						vsData.TextMatrix(lngRowTitle, lngColData, "Treasurer");
						// Row - 1  'Treasurer Name
						vsData.TextMatrix(lngRowTName, lngColData, "");
						// Row - 3  'County
						vsData.TextMatrix(lngRowCounty, lngColData, "");
						// Row - 10 'His/Her
						vsData.TextMatrix(lngRowHisHer, lngColData, "Her");
						// Signer's name
						vsData.TextMatrix(lngRowSignerName, lngColData, "");
						// Signer's Designation
						vsData.TextMatrix(lngRowSignerDesignation, lngColData, "");
						// Commission Expiration Date
						vsData.TextMatrix(lngRowCommissionExpiration, lngColData, "");
					}
				}
				// Row - 2  'Muni Name
				vsData.TextMatrix(lngRowMuni, lngColData, ViewModel.TownName());
                    var lien = ViewModel.TaxBill.Lien;
					vsData.TextMatrix(lngRowOwner1, lngColData,  ViewModel.TaxBill.Name1);
					vsData.TextMatrix(lngRowOwner2, lngColData, ViewModel.TaxBill.Name2);
                    var lastPayment = lien.Payments.LastRecordedPayment();
                    dtPaymentDate = lastPayment?.RecordedTransactionDate ?? DateTime.Now;
					vsData.TextMatrix(lngRowPayDate, lngColData, dtPaymentDate.FormatAndPadShortDate());
					vsData.TextMatrix(lngRowSignedDate, lngColData, dtPaymentDate.FormatAndPadShortDate());
					vsData.TextMatrix(lngRowAppearedDate, lngColData, "");
                    lien.RateRecord.BillingDate.FormatAndPadShortDate();
                    vsData.TextMatrix(lngRowFileDate, lngColData, lien.RateRecord.BillingDate.FormatAndPadShortDate());
                    
                    vsData.TextMatrix(lngRowBook, lngColData, lien.Book);
					
					vsData.TextMatrix(lngRowPage, lngColData, lien.Page);
                // Row - Map Lot
                vsData.TextMatrix(lngRowMapLot, lngColData, ViewModel.TaxBill.MapLot);
                lngTempAccountNumber = ViewModel.TaxBill.Account.GetValueOrDefault();
                
				
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Loading Settings");
			}
		}

		private void mnuFilePrintBlankForm_Click(object sender, System.EventArgs e)
		{
			arLienDischargeNotice.InstancePtr.Init("", modGlobalConstants.Statics.MuniName, "", "", "", DateTime.FromOADate(0), DateTime.FromOADate(0), "", "", "", "", "", DateTime.FromOADate(0), null, null, 0, default(DateTime), false, default(DateTime), null, true);
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
            if (SaveSettings())
            {
                Close();
            }
			// this will keep the data and print the lien discharge notice
		}

		private void FormatGrid(bool boolReset = false)
		{
			// MAL@20071011: Added highlight backcolor to all required rows
			// Program Bug Reference: 6343
			int lngWid = 0;
			lngWid = vsData.WidthOriginal;
			if (boolPrintAllSaved)
			{
				if (boolReset)
				{
					vsData.Rows = 0;
					vsData.Rows = 10;
				}
				vsData.ColWidth(lngColTitle, FCConvert.ToInt32(lngWid * 0.38));
				// title
				vsData.ColWidth(lngColHidden, 0);
				// hidden row
				vsData.ColWidth(lngColData, FCConvert.ToInt32(lngWid * 0.58));
				// data
				vsData.TextMatrix(lngRowTName, lngColTitle, "Treasurer Name");
				vsData.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowTName, 2, lngRowTName, 2, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				vsData.TextMatrix(lngRowMuni, lngColTitle, "Municipality");
				vsData.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowMuni, 2, lngRowMuni, 2, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				vsData.TextMatrix(lngRowCounty, lngColTitle, "County");
				vsData.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowCounty, 2, lngRowCounty, 2, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				vsData.TextMatrix(lngRowSignedDate, lngColTitle, "Treasurer Signing Date");
				vsData.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowSignedDate, 2, lngRowSignedDate, 2, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				vsData.TextMatrix(lngRowHisHer, lngColTitle, "His / Her  (Treasurer)");
				vsData.TextMatrix(lngRowSignerName, lngColTitle, "Signer's Name");
				vsData.TextMatrix(lngRowTitle, lngColTitle, "Treasurer Title");
				vsData.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowTitle, 2, lngRowTitle, 2, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				vsData.TextMatrix(lngRowSignerDesignation, lngColTitle, "Signer's Designation");
				vsData.TextMatrix(lngRowCommissionExpiration, lngColTitle, "Commission Expiration");
				vsData.TextMatrix(lngRowAppearedDate, lngColTitle, "Date Appeared");
			}
			else
			{
				if (boolReset)
				{
					vsData.Rows = 0;
					vsData.Rows = 18;
				}
				vsData.ColWidth(lngColTitle, FCConvert.ToInt32(lngWid * 0.38));
				// title
				vsData.ColWidth(lngColHidden, 0);
				// hidden row
				vsData.ColWidth(lngColData, FCConvert.ToInt32(lngWid * 0.58));
				// data
				vsData.TextMatrix(lngRowTName, lngColTitle, "Treasurer Name");
				vsData.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowTName, 2, lngRowTName, 2, colorSettings.TRIOCOLORHIGHLIGHT);
				vsData.TextMatrix(lngRowMuni, lngColTitle, "Municipality");
				vsData.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowMuni, 2, lngRowMuni, 2, colorSettings.TRIOCOLORHIGHLIGHT);
				vsData.TextMatrix(lngRowCounty, lngColTitle, "County");
				vsData.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowCounty, 2, lngRowCounty, 2, colorSettings.TRIOCOLORHIGHLIGHT);
				vsData.TextMatrix(lngRowOwner1, lngColTitle, "Owner Name");
				vsData.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowOwner1, 2, lngRowOwner1, 2, colorSettings.TRIOCOLORHIGHLIGHT);
				vsData.TextMatrix(lngRowOwner2, lngColTitle, "Second Owner");
				vsData.TextMatrix(lngRowPayDate, lngColTitle, "Payment Date");
				vsData.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowPayDate, 2, lngRowPayDate, 2, colorSettings.TRIOCOLORHIGHLIGHT);
				vsData.TextMatrix(lngRowFileDate, lngColTitle, "Filing Date");
				vsData.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowFileDate, 2, lngRowFileDate, 2, colorSettings.TRIOCOLORHIGHLIGHT);
				vsData.TextMatrix(lngRowSignedDate, lngColTitle, "Treasurer Signing Date");
				vsData.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowSignedDate, 2, lngRowSignedDate, 2, colorSettings.TRIOCOLORHIGHLIGHT);
				vsData.TextMatrix(lngRowBook, lngColTitle, "Book");
				vsData.TextMatrix(lngRowPage, lngColTitle, "Page");
				vsData.TextMatrix(lngRowMapLot, lngColTitle, "Map Lot");
				vsData.TextMatrix(lngRowAbatementDefault, lngColTitle, "How was the lien paid?");
				vsData.TextMatrix(lngRowHisHer, lngColTitle, "His / Her  (Treasurer)");
				vsData.TextMatrix(lngRowSignerName, lngColTitle, "Signer's Name");
				vsData.TextMatrix(lngRowTitle, lngColTitle, "Treasurer Title");
				vsData.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowTitle, 2, 1, 2, colorSettings.TRIOCOLORHIGHLIGHT);
				vsData.TextMatrix(lngRowSignerDesignation, lngColTitle, "Signer's Designation");
				vsData.TextMatrix(lngRowCommissionExpiration, lngColTitle, "Commission Expiration");
				vsData.TextMatrix(lngRowAppearedDate, lngColTitle, "Date Appeared");
			}

		}

		private void vsData_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			vsData.EditMask = string.Empty;
			vsData.ComboList = "";
			if (vsData.Row == lngRowPayDate || vsData.Row == lngRowFileDate || vsData.Row == lngRowCommissionExpiration || vsData.Row == lngRowSignedDate || vsData.Row == lngRowAppearedDate)
			{
				vsData.EditMask = "##/##/####";
			}
			else if (vsData.Row == lngRowAbatementDefault)
			{
				vsData.ComboList = "0;" + strPayment + "|1;" + strAbate;
			}
		}

		private void vsData_KeyPressEdit(int Row, int Col, ref Keys KeyAscii)
		{
			if (Row == lngRowPayDate || Row == lngRowFileDate || Row == lngRowBook || Row == lngRowPage || Row == lngRowCommissionExpiration || Row == lngRowAppearedDate)
			{
				// only allow numbers
				if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back) || (KeyAscii == Keys.Decimal) || (FCConvert.ToInt32(KeyAscii) == 46))
				{
					// do nothing
				}
				else
				{
					KeyAscii = 0;
				}
			}
			else
			{
			}
		}

		private void vsData_RowColChange(object sender, EventArgs e)
		{
			if (vsData.Col == lngColData)
			{
				if (vsData.Row == lngRowTName)
				{
					// Treasurer Name
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowMuni)
				{
					// Muni Name
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowCounty)
				{
					// County
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowOwner1)
				{
					// Name1
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowOwner2)
				{
					// Name2
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowPayDate)
				{
					// Payment Date
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowSignedDate)
				{
					// Treas Signing Date
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowFileDate)
				{
					// Filing Date
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowBook)
				{
					// Book
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowPage)
				{
					// Page
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowHisHer)
				{
					// His/Her
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowSignerName || vsData.Row == lngRowTitle)
				{
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowSignerDesignation)
				{
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowCommissionExpiration)
				{
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
				}
				else if (vsData.Row == lngRowAbatementDefault)
				{
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
				}
				else if (vsData.Row == lngRowAppearedDate)
				{
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
			}
			else
			{
				vsData.Editable = FCGrid.EditableSettings.flexEDNone;
				vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private bool ValidateAnswers()
		{
			bool ValidateAnswers = false;
			ValidateAnswers = true;
			vsData.Select(0, 1);
			// Row - 0  'Treasurer Name
			if (Strings.Trim(vsData.TextMatrix(lngRowTName, lngColData)) == "")
			{
				ValidateAnswers = false;
				FCMessageBox.Show("Please enter the Treasurer's name.", MsgBoxStyle.Exclamation, "Validation");
				vsData.Select(lngRowTName, lngColData);
				return ValidateAnswers;
			}
			// Row - 0  'Treasurer Title
			if (Strings.Trim(vsData.TextMatrix(lngRowTitle, lngColData)) == "")
			{
				ValidateAnswers = false;
				FCMessageBox.Show("Please enter the Treasurer's title.", MsgBoxStyle.Exclamation, "Validation");
				vsData.Select(lngRowTitle, lngColData);
				return ValidateAnswers;
			}
			// Row - 1  'Muni Name
			if (Strings.Trim(vsData.TextMatrix(lngRowMuni, lngColData)) == "")
			{
				ValidateAnswers = false;
				FCMessageBox.Show("Please enter the Municipality name.", MsgBoxStyle.Exclamation, "Validation");
				vsData.Select(lngRowMuni, lngColData);
				return ValidateAnswers;
			}
			// Row - 2  'County
			if (Strings.Trim(vsData.TextMatrix(lngRowCounty, lngColData)) == "")
			{
				ValidateAnswers = false;
				FCMessageBox.Show("Please enter the County name.", MsgBoxStyle.Exclamation, "Validation");
				vsData.Select(lngRowCounty, lngColData);
				return ValidateAnswers;
			}
			// Row - 5  'Payment Date
			if (Strings.Trim(vsData.TextMatrix(lngRowSignedDate, lngColData)) == "")
			{
				ValidateAnswers = false;
				FCMessageBox.Show("Please enter a treasurer signing date.", MsgBoxStyle.Exclamation, "Validation");
				vsData.Select(lngRowSignedDate, lngColData);
				return ValidateAnswers;
			}
			else
			{
				if (!Information.IsDate(vsData.TextMatrix(lngRowSignedDate, lngColData)))
				{
					ValidateAnswers = false;
					FCMessageBox.Show("Please enter a valid treasurer signing date.", MsgBoxStyle.Exclamation, "Validation");
					vsData.Select(lngRowSignedDate, lngColData);
					return ValidateAnswers;
				}
			}
			if (!boolPrintAllSaved)
			{
				// Row - 3  'Name1
				if (Strings.Trim(vsData.TextMatrix(lngRowOwner1, lngColData)) == "")
				{
					ValidateAnswers = false;
					FCMessageBox.Show("Please enter the Owner's name.", MsgBoxStyle.Exclamation, "Validation");
					vsData.Select(lngRowOwner1, lngColData);
					return ValidateAnswers;
				}
				// Row - 4  'Name2
				// no validations
				// Row - 5  'Payment Date
				if (Strings.Trim(vsData.TextMatrix(lngRowPayDate, lngColData)) == "")
				{
					ValidateAnswers = false;
					FCMessageBox.Show("Please enter a payment date.", MsgBoxStyle.Exclamation, "Validation");
					vsData.Select(lngRowPayDate, lngColData);
					return ValidateAnswers;
				}
				else
				{
					if (!Information.IsDate(vsData.TextMatrix(lngRowPayDate, lngColData)))
					{
						ValidateAnswers = false;
						FCMessageBox.Show("Please enter a valid payment date.", MsgBoxStyle.Exclamation, "Validation");
						vsData.Select(lngRowPayDate, lngColData);
						return ValidateAnswers;
					}
				}
				// Row - 6  'Filing Date
				if (Strings.Trim(vsData.TextMatrix(lngRowFileDate, lngColData)) == "")
				{
					ValidateAnswers = false;
					FCMessageBox.Show("Please enter a lien filing date.", MsgBoxStyle.Exclamation, "Validation");
					vsData.Select(lngRowFileDate, lngColData);
					return ValidateAnswers;
				}
				else
				{
					if (!Information.IsDate(vsData.TextMatrix(lngRowFileDate, lngColData)))
					{
						ValidateAnswers = false;
						FCMessageBox.Show("Please enter a valid lien filing date.", MsgBoxStyle.Exclamation, "Validation");
						vsData.Select(lngRowFileDate, lngColData);
						return ValidateAnswers;
					}
				}
				// Row - 7  'Book
				// Row - 8  'Page
			}
			else
			{
				// Appeared Date
				if (Strings.Trim(vsData.TextMatrix(lngRowAppearedDate, lngColData)) == "" || Strings.Trim(vsData.TextMatrix(lngRowAppearedDate, lngColData)) == "__/__/____" || Strings.Trim(vsData.TextMatrix(lngRowAppearedDate, lngColData)) == "/  /")
				{
					//Prevented FCMessageBox to appear twice
					DialogResult messageBoxResult = FCMessageBox.Show("Would you like the appeared date to default to the payment date?", MsgBoxStyle.Question | MsgBoxStyle.YesNoCancel, null);
					if (messageBoxResult == DialogResult.Yes)
					{
						boolDefaultAppearedDate = true;
					}
					else if (messageBoxResult == DialogResult.No)
					{
						boolDefaultAppearedDate = false;
					}
					else if (messageBoxResult == DialogResult.Cancel)
					{
						boolDefaultAppearedDate = false;
						ValidateAnswers = false;
					}
				}
				else
				{
					// force this date
				}
			}
			
			if (Strings.Trim(vsData.TextMatrix(lngRowCommissionExpiration, lngColData)) == "")
			{

			}
			else
			{
				if (Strings.Trim(vsData.TextMatrix(lngRowCommissionExpiration, lngColData)) == "/  /")
				{
					vsData.TextMatrix(lngRowCommissionExpiration, lngColData, "");
					// change it back to blank
				}
				else
				{
					if (!Information.IsDate(vsData.TextMatrix(lngRowCommissionExpiration, lngColData)))
					{
						if (Conversion.Val(vsData.TextMatrix(lngRowCommissionExpiration, lngColData)) == 0)
						{
							vsData.TextMatrix(lngRowCommissionExpiration, lngColData, "");
							// change it back to blank
						}
						else
						{
							ValidateAnswers = false;
							FCMessageBox.Show("Please enter a valid Commission Expiration date.", MsgBoxStyle.Exclamation, "Validation");
							vsData.Select(lngRowCommissionExpiration, lngColData);
							return ValidateAnswers;
						}
					}
				}
			}
			return ValidateAnswers;
		}

		private void vsData_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (vsData.CurrentCell.IsInEditMode)
			{
				//FC:FINAL:MSH - save and use correct indexes of the cell
				int row = vsData.GetFlexRowIndex(e.RowIndex);
				int col = vsData.GetFlexColIndex(e.ColumnIndex);
				if (row == lngRowCommissionExpiration || row == lngRowAppearedDate || row == lngRowFileDate || row == lngRowPayDate || row == lngRowSignedDate)
				{
					if (vsData.EditText == "  /  /    " || vsData.EditText == "__/__/____" || Strings.Trim(vsData.EditText) == "/  /")
					{
						vsData.EditText = "";
						vsData.TextMatrix(row, col, "");
						vsData.EditMask = "";
					}
				}
			}
		}
	

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFileSaveExit_Click(sender, e);
		}

		private void cmdFilePrintBlankForm_Click(object sender, EventArgs e)
		{
			this.mnuFilePrintBlankForm_Click(sender, e);
		}

        public ITaxLienDischargeNoticeViewModel ViewModel { get; set; }
        public void ShowModal()
        {
            this.Show(FormShowEnum.Modal);
        }
    }
}
