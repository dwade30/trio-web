﻿namespace TWCL0000.AccountPayment
{
    partial class frmChooseDiscount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRecalc = new fecherFoundation.FCButton();
            this.txtDiscount = new fecherFoundation.FCTextBox();
            this.txtPrincipal = new fecherFoundation.FCTextBox();
            this.lblOriginalTax = new fecherFoundation.FCLabel();
            this.lblOriginalTaxLabel = new fecherFoundation.FCLabel();
            this.lblDiscHiddenTotal = new fecherFoundation.FCLabel();
            this.lblTotal = new fecherFoundation.FCLabel();
            this.Label11 = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.btnProcess = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnRecalc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 356);
            this.BottomPanel.Size = new System.Drawing.Size(366, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.btnRecalc);
            this.ClientArea.Controls.Add(this.txtDiscount);
            this.ClientArea.Controls.Add(this.txtPrincipal);
            this.ClientArea.Controls.Add(this.lblOriginalTax);
            this.ClientArea.Controls.Add(this.lblOriginalTaxLabel);
            this.ClientArea.Controls.Add(this.lblDiscHiddenTotal);
            this.ClientArea.Controls.Add(this.lblTotal);
            this.ClientArea.Controls.Add(this.Label11);
            this.ClientArea.Controls.Add(this.Label10);
            this.ClientArea.Controls.Add(this.Label9);
            this.ClientArea.Size = new System.Drawing.Size(366, 296);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(366, 60);
            // 
            // btnRecalc
            // 
            this.btnRecalc.Location = new System.Drawing.Point(139, 253);
            this.btnRecalc.Name = "btnRecalc";
            this.btnRecalc.Size = new System.Drawing.Size(62, 27);
            this.btnRecalc.TabIndex = 82;
            this.btnRecalc.Text = "Recalc";
            // 
            // txtDiscount
            // 
            this.txtDiscount.BackColor = System.Drawing.SystemColors.Window;
            this.txtDiscount.Enabled = false;
            this.txtDiscount.Location = new System.Drawing.Point(151, 137);
            this.txtDiscount.MaxLength = 12;
            this.txtDiscount.Name = "txtDiscount";
            this.txtDiscount.Size = new System.Drawing.Size(115, 40);
            this.txtDiscount.TabIndex = 74;
            // 
            // txtPrincipal
            // 
            this.txtPrincipal.BackColor = System.Drawing.SystemColors.Window;
            this.txtPrincipal.Location = new System.Drawing.Point(151, 82);
            this.txtPrincipal.MaxLength = 12;
            this.txtPrincipal.Name = "txtPrincipal";
            this.txtPrincipal.Size = new System.Drawing.Size(115, 40);
            this.txtPrincipal.TabIndex = 72;
            // 
            // lblOriginalTax
            // 
            this.lblOriginalTax.Location = new System.Drawing.Point(151, 21);
            this.lblOriginalTax.Name = "lblOriginalTax";
            this.lblOriginalTax.Size = new System.Drawing.Size(88, 40);
            this.lblOriginalTax.TabIndex = 71;
            this.lblOriginalTax.Text = "0.00";
            this.lblOriginalTax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblOriginalTaxLabel
            // 
            this.lblOriginalTaxLabel.Location = new System.Drawing.Point(56, 34);
            this.lblOriginalTaxLabel.Name = "lblOriginalTaxLabel";
            this.lblOriginalTaxLabel.Size = new System.Drawing.Size(94, 19);
            this.lblOriginalTaxLabel.TabIndex = 70;
            this.lblOriginalTaxLabel.Text = "ORIGINAL TAX";
            // 
            // lblDiscHiddenTotal
            // 
            this.lblDiscHiddenTotal.Location = new System.Drawing.Point(277, 152);
            this.lblDiscHiddenTotal.Name = "lblDiscHiddenTotal";
            this.lblDiscHiddenTotal.Size = new System.Drawing.Size(59, 25);
            this.lblDiscHiddenTotal.TabIndex = 78;
            this.lblDiscHiddenTotal.Visible = false;
            // 
            // lblTotal
            // 
            this.lblTotal.Location = new System.Drawing.Point(151, 197);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(88, 40);
            this.lblTotal.TabIndex = 77;
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label11
            // 
            this.Label11.Location = new System.Drawing.Point(56, 204);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(72, 19);
            this.Label11.TabIndex = 76;
            this.Label11.Text = "TOTAL";
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(56, 152);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(72, 19);
            this.Label10.TabIndex = 75;
            this.Label10.Text = "DISCOUNT";
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(56, 94);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(72, 19);
            this.Label9.TabIndex = 73;
            this.Label9.Text = "PRINCIPAL";
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Location = new System.Drawing.Point(109, 24);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcess.Size = new System.Drawing.Size(120, 48);
            this.btnProcess.TabIndex = 6;
            this.btnProcess.Text = "Process";
            // 
            // frmChooseDiscount
            // 
            this.ClientSize = new System.Drawing.Size(366, 464);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmChooseDiscount";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
            this.Text = "Discount";
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnRecalc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public fecherFoundation.FCButton btnRecalc;
        public fecherFoundation.FCTextBox txtDiscount;
        public fecherFoundation.FCTextBox txtPrincipal;
        public fecherFoundation.FCLabel lblOriginalTax;
        public fecherFoundation.FCLabel lblOriginalTaxLabel;
        public fecherFoundation.FCLabel lblDiscHiddenTotal;
        public fecherFoundation.FCLabel lblTotal;
        public fecherFoundation.FCLabel Label11;
        public fecherFoundation.FCLabel Label10;
        public fecherFoundation.FCLabel Label9;
        private fecherFoundation.FCButton btnProcess;
    }
}