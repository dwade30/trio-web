﻿using System;
using fecherFoundation;
using Global;
using SharedApplication;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections.AccountPayment;
using Wisej.Web;

namespace TWCL0000.AccountPayment
{
    public partial class frmCLBatchSetup : BaseForm, IModalView<ICLBatchInputSetupViewModel>
    {
        public frmCLBatchSetup()
        {
            InitializeComponent();
            InitializeComponentEx();
        }

        public frmCLBatchSetup(ICLBatchInputSetupViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }

        private void InitializeComponentEx()
        {
            this.Load += FrmCLBatchSetup_Load;
            this.btnSave.Click += BtnSave_Click;
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void Save()
        {
            if (!Validate())
            {
                return;
            }
            var inputSetup = new CLBatchInputSetup();
            inputSetup.DefaultTaxYear = txtTaxYear.Text.ToIntegerValue();
            inputSetup.PaidBy = txtPaidBy.Text;
            inputSetup.TellerID = txtTellerID.Text;
            if (txtEffectiveDate.NullableValue.HasValue && txtEffectiveDate.NullableValue.Value != DateTime.MinValue)
            {
                inputSetup.EffectiveDate = txtEffectiveDate.NullableValue.Value;
            }

            if (txtPaymentDate.NullableValue.HasValue && txtPaymentDate.NullableValue.Value != DateTime.MinValue)
            {
                inputSetup.BatchRecoverDate = txtPaymentDate.NullableValue.Value;
            }
            ViewModel.Save(inputSetup);
            Close();
        }

        private void FrmCLBatchSetup_Load(object sender, EventArgs e)
        {
            LoadSetup();
        }

        private void LoadSetup()
        {
            txtPaidBy.Text = ViewModel.BatchSetup.PaidBy;
            txtTellerID.Text = ViewModel.BatchSetup.TellerID;
            txtEffectiveDate.Text = ViewModel.BatchSetup.EffectiveDate.FormatAndPadShortDate();
            txtPaymentDate.Text = ViewModel.BatchSetup.BatchRecoverDate.FormatAndPadShortDate();
            if (ViewModel.BatchSetup.Period > 0)
            {
                cmbPeriod.ListIndex = ViewModel.BatchSetup.Period - 1;
            }
        }

        public ICLBatchInputSetupViewModel ViewModel { get; set; }
        public void ShowModal()
        {
            this.Show(FormShowEnum.Modal);
        }

        private bool Validate()
        {
            if (string.IsNullOrWhiteSpace(txtTellerID.Text))
            {
                MessageBox.Show("Please enter a valid teller ID", "Invalid teller ID", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
            }
            var result = ViewModel.ValidateTeller(txtTellerID.Text);
            if (result == false)
            {
                MessageBox.Show("Please enter a valid teller ID", "Invalid teller ID", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return false;
            }

            var taxYear = txtTaxYear.Text.ToIntegerValue();
            if (taxYear == 0)
            {
                MessageBox.Show("Please enter a valid tax year", "Invalid Tax Year", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return false;
            }

            if (!ViewModel.ValidateTaxYear(taxYear))
            {
                MessageBox.Show("There is no rate record for the year " + taxYear, "Invalid Tax Year", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return false;
            }

            if (!txtEffectiveDate.NullableValue.HasValue ||
                txtEffectiveDate.NullableValue.GetValueOrDefault() == DateTime.MinValue)
            {
                MessageBox.Show("Please enter a valid effective date", "Invalid Effective Date", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return false;
            }

            if (!txtPaymentDate.NullableValue.HasValue ||
                txtPaymentDate.NullableValue.GetValueOrDefault() == DateTime.MinValue)
            {
                MessageBox.Show("Please enter a valid payment date", "Invalid Payment Date", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return false;
            }

            if (string.IsNullOrWhiteSpace(txtPaidBy.Text))
            {
                MessageBox.Show("Please enter a paid by name", "Invalid Paid By Name", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }
    }
}
