﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmReprintPartialPaymentWaiver.
	/// </summary>
	public partial class frmReprintPartialPaymentWaiver : BaseForm
	{
		public frmReprintPartialPaymentWaiver()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmReprintPartialPaymentWaiver InstancePtr
		{
			get
			{
				return (frmReprintPartialPaymentWaiver)Sys.GetInstance(typeof(frmReprintPartialPaymentWaiver));
			}
		}

		protected frmReprintPartialPaymentWaiver _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		const int CNSTGRIDCOLID = 0;
		const int CNSTGRIDCOLLIENKEY = 1;
		const int CNSTGRIDCOLPAYMENT = 2;
		const int CNSTGRIDCOLEFFECTIVEDATE = 3;
		const int CNSTGRIDCOLPRINTED = 4;
		const int CNSTGRIDCOLACCOUNT = 5;
		const int CNSTGRIDCOLYEAR = 6;
		const int CNSTGRIDCOLNAME = 7;

		private void frmReprintPartialPaymentWaiver_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = 0;
				mnuExit_Click();
			}
		}

		private void frmReprintPartialPaymentWaiver_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmReprintPartialPaymentWaiver.Icon	= "frmReprintPartialPaymentWaiver.frx":0000";
			//frmReprintPartialPaymentWaiver.FillStyle	= 0;
			//frmReprintPartialPaymentWaiver.ScaleWidth	= 5880;
			//frmReprintPartialPaymentWaiver.ScaleHeight	= 4155;
			//frmReprintPartialPaymentWaiver.LinkTopic	= "Form2";
			//frmReprintPartialPaymentWaiver.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "9";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//Grid.BackColor	= "-2147483643";
			//			//Grid.ForeColor	= "-2147483640";
			//Grid.BorderStyle	= 1;
			//Grid.FillStyle	= 0;
			//Grid.Appearance	= 1;
			//Grid.GridLines	= 1;
			//Grid.WordWrap	= 0;
			//Grid.ScrollBars	= 2;
			//Grid.RightToLeft	= 0;
			//Grid._cx	= 9922;
			//Grid._cy	= 4623;
			//Grid._ConvInfo	= 1;
			//Grid.MousePointer	= 0;
			//Grid.BackColorFixed	= -2147483633;
			//			//Grid.ForeColorFixed	= -2147483630;
			//Grid.BackColorSel	= -2147483635;
			//			//Grid.ForeColorSel	= -2147483634;
			//Grid.BackColorBkg	= -2147483636;
			//Grid.BackColorAlternate	= -2147483643;
			//Grid.GridColor	= -2147483633;
			//Grid.GridColorFixed	= -2147483632;
			//Grid.TreeColor	= -2147483632;
			//Grid.FloodColor	= 192;
			//Grid.SheetBorder	= -2147483642;
			//Grid.FocusRect	= 0;
			//Grid.HighLight	= 1;
			//Grid.AllowSelection	= -1  'True;
			//Grid.AllowBigSelection	= 0   'False;
			//Grid.AllowUserResizing	= 0;
			//Grid.SelectionMode	= 1;
			//Grid.GridLinesFixed	= 2;
			//Grid.GridLineWidth	= 1;
			//Grid.RowHeightMin	= 0;
			//Grid.RowHeightMax	= 0;
			//Grid.ColWidthMin	= 0;
			//Grid.ColWidthMax	= 0;
			//Grid.ExtendLastCol	= -1  'True;
			//Grid.FormatString	= "";
			//Grid.ScrollTrack	= -1  'True;
			//Grid.ScrollTips	= 0   'False;
			//Grid.MergeCells	= 0;
			//Grid.MergeCompare	= 0;
			//Grid.AutoResize	= -1  'True;
			//Grid.AutoSizeMode	= 0;
			//Grid.AutoSearch	= 0;
			//Grid.AutoSearchDelay	= 2;
			//Grid.MultiTotals	= -1  'True;
			//Grid.SubtotalPosition	= 1;
			//Grid.OutlineBar	= 0;
			//Grid.OutlineCol	= 0;
			//Grid.Ellipsis	= 0;
			//Grid.ExplorerBar	= 0;
			//Grid.PicturesOver	= 0   'False;
			//Grid.PictureType	= 0;
			//Grid.TabBehavior	= 0;
			//Grid.OwnerDraw	= 0;
			//Grid.ShowComboButton	= -1  'True;
			//Grid.TextStyle	= 0;
			//Grid.TextStyleFixed	= 0;
			//Grid.OleDragMode	= 0;
			//Grid.OleDropMode	= 0;
			//Grid.ComboSearch	= 3;
			//Grid.AutoSizeMouse	= -1  'True;
			//Grid.AllowUserFreezing	= 0;
			//Grid.BackColorFrozen	= 0;
			//			//Grid.ForeColorFrozen	= 0;
			//Grid.WallPaperAlignment	= 9;
			//vsElasticLight1.OleObjectBlob	= "frmReprintPartialPaymentWaiver.frx":058A";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
			LoadGrid();
		}

		private void SetupGrid()
		{
			Grid.ColDataType(CNSTGRIDCOLPRINTED, FCGrid.DataTypeSettings.flexDTBoolean);
			Grid.ColHidden(CNSTGRIDCOLID, true);
			Grid.ColHidden(CNSTGRIDCOLPRINTED, true);
			Grid.ColHidden(CNSTGRIDCOLLIENKEY, true);
			Grid.ColHidden(CNSTGRIDCOLPAYMENT, true);
			Grid.ColHidden(CNSTGRIDCOLEFFECTIVEDATE, true);
			Grid.TextMatrix(0, CNSTGRIDCOLACCOUNT, "Account");
			Grid.TextMatrix(0, CNSTGRIDCOLYEAR, "Year");
			Grid.TextMatrix(0, CNSTGRIDCOLNAME, "Name");
			//FC:FINAL:DDU:#2028 - align all columns to left
			Grid.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.ColAlignment(7, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		}

		private void ResizeGrid()
		{
			int GridWidth;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTGRIDCOLACCOUNT, FCConvert.ToInt32(GridWidth * 0.2));
			Grid.ColWidth(CNSTGRIDCOLYEAR, FCConvert.ToInt32(GridWidth * 0.2));
			//Resize Name column too, to see it well
			Grid.ColWidth(CNSTGRIDCOLNAME, FCConvert.ToInt32(GridWidth * 0.5));
		}

		private void frmReprintPartialPaymentWaiver_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Grid_DblClick(object sender, EventArgs e)
		{
			if (Grid.Row > 0)
			{
				int lngRow = 0;
				lngRow = Grid.Row;
				frmLienPartialPayment.InstancePtr.Init(Convert.ToDateTime(Grid.TextMatrix(lngRow, CNSTGRIDCOLEFFECTIVEDATE)), FCConvert.ToInt32(Grid.TextMatrix(lngRow, CNSTGRIDCOLLIENKEY)), Grid.TextMatrix(lngRow, CNSTGRIDCOLYEAR), FCConvert.ToInt32(Grid.TextMatrix(lngRow, CNSTGRIDCOLACCOUNT)), FCConvert.ToDouble(Grid.TextMatrix(lngRow, CNSTGRIDCOLPAYMENT)), txtWitness.Text, true);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void LoadGrid()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strSQL;
			strSQL = "select * from PartialPaymentWaiver order by billyear desc,account";
			rsLoad.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
			int lngRow;
			while (!rsLoad.EndOfFile())
			{
				rsTemp.OpenRecordset("select * from billingmaster where lienrecordnumber = " + FCConvert.ToString(rsLoad.Get_Fields_Int32("LienKey")), modExtraModules.strCLDatabase);
				if (!rsTemp.EndOfFile())
				{
					Grid.Rows += 1;
					lngRow = Grid.Rows - 1;
					Grid.TextMatrix(lngRow, CNSTGRIDCOLID, FCConvert.ToString(rsLoad.Get_Fields_Int32("ID")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLPRINTED, FCConvert.ToString(rsLoad.Get_Fields_Boolean("Printed")));
					// TODO Get_Fields: Check the table for the column [billyear] and replace with corresponding Get_Field method
					Grid.TextMatrix(lngRow, CNSTGRIDCOLYEAR, FCConvert.ToString(rsLoad.Get_Fields("billyear")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLLIENKEY, FCConvert.ToString(rsLoad.Get_Fields_Int32("Lienkey")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLPAYMENT, FCConvert.ToString(rsLoad.Get_Fields_Double("Payment")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLEFFECTIVEDATE, FCConvert.ToString(rsLoad.Get_Fields_DateTime("effectivedate")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLNAME, FCConvert.ToString(rsTemp.Get_Fields_String("name1")));
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					Grid.TextMatrix(lngRow, CNSTGRIDCOLACCOUNT, FCConvert.ToString(rsTemp.Get_Fields("account")));
					if (FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("printed")))
						Grid.RowHidden(lngRow, true);
				}
				rsLoad.MoveNext();
			}
		}

		private void ReshowGrid(bool boolShowAll)
		{
			int x;
			for (x = 1; x <= Grid.Rows - 1; x++)
			{
				if (Grid.TextMatrix(x, CNSTGRIDCOLPRINTED) == "1" && !boolShowAll)
				{
					Grid.RowHidden(x, true);
				}
				else
				{
					Grid.RowHidden(x, false);
				}
			}
			// x
		}

		private void mnuSaveContinue_Click()
		{
		}

		private void mnuPrintall_Click(object sender, System.EventArgs e)
		{
			rptPrintUnprintedPartialPaymentWaivers.InstancePtr.Init(modGlobalConstants.Statics.MuniName, "Maine", txtWitness.Text);
		}

		private void optShow_Click(int Index, object sender, System.EventArgs e)
		{
			if (Index == 0)
			{
				ReshowGrid(false);
			}
			else
			{
				ReshowGrid(true);
			}
		}

		private void optShow_Click(object sender, System.EventArgs e)
		{
			int index = cmbShow.SelectedIndex;
			optShow_Click(index, sender, e);
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuPrintall_Click(sender, e);
		}
	}
}
