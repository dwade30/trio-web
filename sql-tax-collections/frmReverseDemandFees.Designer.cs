﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmReverseDemandFees.
	/// </summary>
	partial class frmReverseDemandFees : BaseForm
	{
		public fecherFoundation.FCGrid vsDemand;
		public fecherFoundation.FCLabel lblInstructionHeader;
		public fecherFoundation.FCLabel lblInstruction;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReverseDemandFees));
			this.vsDemand = new fecherFoundation.FCGrid();
			this.lblInstructionHeader = new fecherFoundation.FCLabel();
			this.lblInstruction = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.btnProcess = new fecherFoundation.FCButton();
			this.cmdFileSelectAll = new fecherFoundation.FCButton();
			this.cmdFileClear = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsDemand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSelectAll)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileClear)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 459);
			this.BottomPanel.Size = new System.Drawing.Size(673, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.vsDemand);
			this.ClientArea.Controls.Add(this.lblInstructionHeader);
			this.ClientArea.Controls.Add(this.lblInstruction);
			this.ClientArea.Size = new System.Drawing.Size(673, 399);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFileClear);
			this.TopPanel.Controls.Add(this.cmdFileSelectAll);
			this.TopPanel.Size = new System.Drawing.Size(673, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileSelectAll, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileClear, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(265, 30);
			this.HeaderText.Text = "Reverse Demand Fees";
			// 
			// vsDemand
			// 
			this.vsDemand.AllowSelection = false;
			this.vsDemand.AllowUserToResizeColumns = false;
			this.vsDemand.AllowUserToResizeRows = false;
			this.vsDemand.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsDemand.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsDemand.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsDemand.BackColorBkg = System.Drawing.Color.Empty;
			this.vsDemand.BackColorFixed = System.Drawing.Color.Empty;
			this.vsDemand.BackColorSel = System.Drawing.Color.Empty;
			this.vsDemand.Cols = 5;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsDemand.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsDemand.ColumnHeadersHeight = 30;
			this.vsDemand.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsDemand.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsDemand.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsDemand.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vsDemand.FixedCols = 0;
			this.vsDemand.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsDemand.FrozenCols = 0;
			this.vsDemand.FrozenRows = 0;
			this.vsDemand.GridColor = System.Drawing.Color.Empty;
			this.vsDemand.Location = new System.Drawing.Point(30, 143);
			this.vsDemand.Name = "vsDemand";
			this.vsDemand.ReadOnly = true;
			this.vsDemand.RowHeadersVisible = false;
			this.vsDemand.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsDemand.RowHeightMin = 0;
			this.vsDemand.Rows = 1;
			this.vsDemand.ShowColumnVisibilityMenu = false;
			this.vsDemand.Size = new System.Drawing.Size(623, 236);
			this.vsDemand.StandardTab = true;
			this.vsDemand.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsDemand.TabIndex = 2;
			this.vsDemand.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsDemand_AfterEdit);
			this.vsDemand.CurrentCellChanged += new System.EventHandler(this.vsDemand_RowColChange);
			this.vsDemand.DoubleClick += new System.EventHandler(this.vsDemand_DblClick);
            this.vsDemand.CellFormatting += new DataGridViewCellFormattingEventHandler(vsDemand_CellFormatting);
			// 
			// lblInstructionHeader
			// 
			this.lblInstructionHeader.Location = new System.Drawing.Point(30, 30);
			this.lblInstructionHeader.Name = "lblInstructionHeader";
			this.lblInstructionHeader.Size = new System.Drawing.Size(623, 17);
			this.lblInstructionHeader.TabIndex = 0;
			this.lblInstructionHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblInstruction
			// 
			this.lblInstruction.Location = new System.Drawing.Point(30, 67);
			this.lblInstruction.Name = "lblInstruction";
			this.lblInstruction.Size = new System.Drawing.Size(623, 66);
			this.lblInstruction.TabIndex = 1;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.Text = "File";
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(238, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(196, 48);
			this.btnProcess.TabIndex = 1;
			this.btnProcess.Text = "Apply Demand Fees";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// cmdFileSelectAll
			// 
			this.cmdFileSelectAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileSelectAll.AppearanceKey = "toolbarButton";
			this.cmdFileSelectAll.Location = new System.Drawing.Point(576, 30);
			this.cmdFileSelectAll.Name = "cmdFileSelectAll";
			this.cmdFileSelectAll.Size = new System.Drawing.Size(75, 24);
			this.cmdFileSelectAll.TabIndex = 52;
			this.cmdFileSelectAll.Text = "Select All";
			this.cmdFileSelectAll.Click += new System.EventHandler(this.cmdFileSelectAll_Click);
			// 
			// cmdFileClear
			// 
			this.cmdFileClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileClear.AppearanceKey = "toolbarButton";
			this.cmdFileClear.Location = new System.Drawing.Point(458, 30);
			this.cmdFileClear.Name = "cmdFileClear";
			this.cmdFileClear.Size = new System.Drawing.Size(111, 24);
			this.cmdFileClear.TabIndex = 53;
			this.cmdFileClear.Text = "Clear Selection";
			this.cmdFileClear.Click += new System.EventHandler(this.cmdFileClear_Click);
			// 
			// frmReverseDemandFees
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(673, 567);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmReverseDemandFees";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Reverse Demand Fees";
			this.Load += new System.EventHandler(this.frmReverseDemandFees_Load);
			this.Activated += new System.EventHandler(this.frmReverseDemandFees_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmReverseDemandFees_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsDemand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSelectAll)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileClear)).EndInit();
			this.ResumeLayout(false);
		}

        
        #endregion

        private System.ComponentModel.IContainer components;
		private FCButton btnProcess;
		public FCButton cmdFileSelectAll;
		public FCButton cmdFileClear;
	}
}
