﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmPrintLienDates.
	/// </summary>
	public partial class frmPrintLienDates : BaseForm
	{
		public frmPrintLienDates()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
            WireEvents();
		}

        private void WireEvents()
        {
            cmbPrint.SelectedIndexChanged += CmbPrintOnSelectedIndexChanged;
        }

        private void CmbPrintOnSelectedIndexChanged(object sender, EventArgs e)
        {
            RateKeyLayout.Visible = false;
            YearLayout.Visible = false;
            switch (cmbPrint.SelectedIndex)
            {
                case (int)PrintLienDateOptions.OneRate:
                    RateKeyLayout.Visible = true;
                    break;
                case (int)PrintLienDateOptions.OneYear:
                    YearLayout.Visible = true;
                    break;
            }
        }

        /// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPrintLienDates InstancePtr
		{
			get
			{
				return (frmPrintLienDates)Sys.GetInstance(typeof(frmPrintLienDates));
			}
		}

		protected frmPrintLienDates _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>

		clsDRWrapper rsRate = new clsDRWrapper();
		bool boolDirty;
		bool boolLoaded;

		private void frmPrintLienDates_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				FillComboBoxes();
				cmbPrint.Items.Clear();
                cmbPrint.Items.Add("All Eligible Rate Keys");
                cmbPrint.Items.Add("One Rate Key");
                cmbPrint.Items.Add("One Year");
                cmbPrint.Items.Add("Anything Currently Pending");
                cmbPrint.Items.Add("Anything Pending within 30 Days");
                cmbPrint.SelectedIndex = 0;
				boolLoaded = true;
			}
		}

		private void frmPrintLienDates_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = 0;
			}
		}

		private void frmPrintLienDates_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			// this will set the size of the form to the smaller TRIO size (1/2 normal)
			modGlobalFunctions.SetTRIOColors(this);
			// this will set all the TRIO colors
		}

		private void frmPrintLienDates_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				Close();
			}
			else if (KeyAscii == 13)
			{
				KeyAscii = 0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			SaveInfo();
			Close();
		}

		private void FillComboBoxes()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the year combo with all of the years that this account has liens
				// and it will clear out the book and page fields
				int lngRW;
				rsRate.OpenRecordset("SELECT * FROM RateRec order by [year] desc", modExtraModules.strCLDatabase);
				if (rsRate.EndOfFile())
				{
					FCMessageBox.Show("There are no rate keys in the database.", MsgBoxStyle.Information, "No Rate Keys");
					return;
				}
				cmbRK.Clear();
				if (frmLienDates.InstancePtr.vsRate.Rows > 1)
				{
					for (lngRW = 1; lngRW <= frmLienDates.InstancePtr.vsRate.Rows - 1; lngRW++)
					{
						rsRate.FindFirstRecord("ID", frmLienDates.InstancePtr.vsRate.TextMatrix(lngRW, 0));
						if (rsRate.NoMatch)
						{
							cmbRK.AddItem(frmLienDates.InstancePtr.vsRate.TextMatrix(lngRW, 0));
							cmbRK.ItemData(cmbRK.NewIndex, FCConvert.ToInt32(frmLienDates.InstancePtr.vsRate.TextMatrix(lngRW, 0)));
						}
						else
						{
							cmbRK.AddItem(FCConvert.ToString(rsRate.Get_Fields_Int32("ID")) + " - " + FCConvert.ToString(rsRate.Get_Fields_String("Description")));
							cmbRK.ItemData(cmbRK.NewIndex, FCConvert.ToInt32(rsRate.Get_Fields_Int32("ID")));
						}
					}

                    cmbRK.SelectedIndex = 0;
                }
				cmbYear.Clear();
				rsRate.OpenRecordset("SELECT Distinct [Year] FROM RateRec ORDER BY [Year] desc", modExtraModules.strCLDatabase);
				if (rsRate.RecordCount() > 0)
				{
					while (!rsRate.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
						cmbYear.AddItem(FCConvert.ToString(rsRate.Get_Fields("Year")));
						rsRate.MoveNext();
					}

                    cmbYear.SelectedIndex = 0;
                }
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Loading Account");
			}
		}

		private void SaveInfo()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strString = "";
				int lngRW;
				// create the string of RK to show
				if (cmbPrint.SelectedIndex == 0)
				{
					// all
					for (lngRW = 1; lngRW <= frmLienDates.InstancePtr.vsRate.Rows - 1; lngRW++)
					{
						strString += frmLienDates.InstancePtr.vsRate.TextMatrix(lngRW, 0) + ",";
					}
				}
				else if (cmbPrint.SelectedIndex == 1)
				{
					// one rate record
					strString = cmbRK.Text;
				}
				else if (cmbPrint.SelectedIndex == 2)
				{
					//FC:FINAL:AM: add check
					if (cmbYear.SelectedIndex != -1)
					{
						// one year
						for (lngRW = 1; lngRW <= frmLienDates.InstancePtr.vsRate.Rows - 1; lngRW++)
						{
							if (frmLienDates.InstancePtr.vsRate.TextMatrix(lngRW, 1) == cmbYear.Items[cmbYear.SelectedIndex].ToString())
							{
								strString += frmLienDates.InstancePtr.vsRate.TextMatrix(lngRW, 0) + ",";
							}
						}
					}
				}
				else if (cmbPrint.SelectedIndex == 3)
				{
					// all due
					for (lngRW = 1; lngRW <= frmLienDates.InstancePtr.vsRate.Rows - 1; lngRW++)
					{
						if (FCConvert.ToDouble(frmLienDates.InstancePtr.vsRate.TextMatrix(lngRW, 2)) == -1)
						{
							strString += frmLienDates.InstancePtr.vsRate.TextMatrix(lngRW, 0) + ",";
						}
					}
				}
				else
				{
					// all due within 30 days
					for (lngRW = 1; lngRW <= frmLienDates.InstancePtr.vsRate.Rows - 1; lngRW++)
					{
						if (FCConvert.ToDouble(frmLienDates.InstancePtr.vsRate.TextMatrix(lngRW, 3)) == -1)
						{
							strString += frmLienDates.InstancePtr.vsRate.TextMatrix(lngRW, 0) + ",";
						}
					}
				}
				if (strString != "")
				{
					// show them in the viewer
					frmLienDates.InstancePtr.strReportHeader = "Lien Dates Timeline";
					rptLienDateLine.InstancePtr.Init(strString);
					frmReportViewer.InstancePtr.Init(rptLienDateLine.InstancePtr);
				}
				else
				{
					FCMessageBox.Show("There are no eligible Rate Keys", MsgBoxStyle.Exclamation, "No Rate Key Eligible");
				}
				Close();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Saving Lien");
			}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFileSaveExit_Click(sender, e);
		}

        private enum PrintLienDateOptions
        {
            AllEligible = 0,
            OneRate = 1,
            OneYear = 2,
            AnythingPending = 3,
            AnythingPendingIn30Days = 4
        }
	}
}
