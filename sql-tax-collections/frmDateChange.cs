﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using SharedApplication;
using SharedApplication.Extensions;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmDateChange.
	/// </summary>
	public partial class frmDateChange : fecherFoundation.FCForm, IModalView<IEffectiveDateChangeViewModel>
    {
        private bool cancelling = true;
		public frmDateChange()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        public frmDateChange(IEffectiveDateChangeViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }

		private void InitializeComponentEx()
		{
            this.Closing += FrmDateChange_Closing;
		}

        private void FrmDateChange_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (cancelling)
            {
                ViewModel.Cancelled = true;
            }
            else
            {
                ViewModel.Cancelled = false;
            }
        }

        /// <summary>
        /// Default instance for Form
        /// </summary>
        //public static frmDateChange InstancePtr
        //{
        //	get
        //	{
        //		return (frmDateChange)Sys.GetInstance(typeof(frmDateChange));
        //	}
        //}

        //protected frmDateChange _InstancePtr = null;
  //      DateTime dtNew;
		//int intFormType;
		//bool boolEffectChecked;

		//public void Init(string strText, short intType, DateTime dtEffectiveDate)
		//{
		//	// strText is the actual sting that will be shown above the date box
		//	// intType will tell me which place in my code called this form
		//	string strTemp = "";
		//	if (intType == 1)
		//	{
		//		chkEffect.Visible = true;
		//	}

		//	lblInstructions.Text = strText;
		//	txtDate.Text = Strings.Format(dtEffectiveDate, "MM/dd/yyyy");
		//	intFormType = intType;
		//	this.Show(FormShowEnum.Modal);
		//}

		private void cmdOk_Click(object sender, System.EventArgs e)
		{
			DialogResult intAns = 0;
			DateTime datLowDate;
			DateTime datHighDate;
			//switch (intFormType)
			//{
				//case 1:
				//	{
						// Effective Interest Date from frmRECLStatus
						if (txtDate.Text.IsDate())
						{
							datLowDate = DateAndTime.DateAdd("yyyy", -1, DateTime.Today);
							datHighDate = DateAndTime.DateAdd("yyyy", 1, DateTime.Today);
							if (DateAndTime.DateValue(txtDate.Text).ToOADate() < datLowDate.ToOADate() || DateAndTime.DateValue(txtDate.Text).ToOADate() > datHighDate.ToOADate())
							{
								intAns = FCMessageBox.Show("The date you are entering is different from today's date by more than a year.  Are you sure you wish to use this date?", MsgBoxStyle.Question | MsgBoxStyle.YesNo, "Change Date?");
								if (intAns == DialogResult.No)
                                {
                                    return;
                                }
							}
                            var selectedDate = txtDate.Text.ToDate();
                            ViewModel.SetEffectiveDate(new EffectiveDateChoice()
                            {
                                EffectiveDate = selectedDate,
                                UpdateRecordedDate = ViewModel.AllowChangingRecordedDate && chkEffect.Checked
                            });
							//dtNew = DateAndTime.DateValue(txtDate.Text);
							//boolEffectChecked = FCConvert.CBool(chkEffect.CheckState == Wisej.Web.CheckState.Checked);
							//this.Hide();

							//frmRECLStatus.InstancePtr.GetNewEffectiveInterestDate(ref dtNew, boolEffectChecked);
							////FC:FINAL:AM:#i178 - refresh the grid
							//frmRECLStatus.InstancePtr.FormReset = true;
							//frmRECLStatus.InstancePtr.Form_Activate();
							//frmRECLStatus.InstancePtr.FormReset = false;
                            CloseWithoutCancelling();
                            return;
                         }
            //break;
            //		}
            //}
            //end switch
        }


        private void CloseWithoutCancelling()
        {
            cancelling = false;
            Close();
        }
		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void frmDateChange_Activated(object sender, System.EventArgs e)
		{
			
		}

		private void frmDateChange_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Return)
			{
				Support.SendKeys("{tab}", false);
			}
		}

		private void frmDateChange_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii == 27)
				Close();
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void frmDateChange_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
            ShowEffectiveDate();
            //if (modCLCalculations.Statics.gboolDefaultRTDToAutoChange)
            //{
            //	chkEffect.CheckState = Wisej.Web.CheckState.Checked;
            //}
            //else
            //{
            //	chkEffect.CheckState = Wisej.Web.CheckState.Unchecked;
            //}
            txtDate.Select(0, 0);
		}

        private void ShowEffectiveDate()
        {
            chkEffect.Visible = ViewModel.AllowChangingRecordedDate;
            chkEffect.Checked = ViewModel.AllowChangingRecordedDate && ViewModel.UpdateRecordedDate;
            txtDate.Text = ViewModel.EffectiveDate.FormatAndPadShortDate();
        }
		private void txtDate_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Return)
			{
				// if the user hits the enter ID on the textbox when they are done editing the date
				cmdOk_Click(cmdOk, EventArgs.Empty);
			}
		}

		private void txtDate_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(Strings.Left(txtDate.Text, 2)) > 12)
			{
				e.Cancel = true;
				FCMessageBox.Show("Please use the date format MM/dd/yyyy.", MsgBoxStyle.Information, "Invalid Date Format");
			}
		}

        public IEffectiveDateChangeViewModel ViewModel { get; set; }
        public void ShowModal()
        {
            this.Show(FormShowEnum.Modal);
        }
    }
}
