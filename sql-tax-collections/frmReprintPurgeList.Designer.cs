﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmReprintPurgeList.
	/// </summary>
	partial class frmReprintPurgeList : BaseForm
	{
		public fecherFoundation.FCComboBox cmbREPP;
		public fecherFoundation.FCFrame fraDate;
		public fecherFoundation.FCFileListBox fileBox;
		public fecherFoundation.FCLabel lblDateTitle;
		public fecherFoundation.FCLabel lblDate;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReprintPurgeList));
			this.cmbREPP = new fecherFoundation.FCComboBox();
			this.fraDate = new fecherFoundation.FCFrame();
			this.fileBox = new fecherFoundation.FCFileListBox();
			this.lblDateTitle = new fecherFoundation.FCLabel();
			this.lblDate = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.lblREPP = new fecherFoundation.FCLabel();
			this.btnProcess = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraDate)).BeginInit();
			this.fraDate.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 453);
			this.BottomPanel.Size = new System.Drawing.Size(474, 27);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.btnProcess);
			this.ClientArea.Controls.Add(this.lblREPP);
			this.ClientArea.Controls.Add(this.cmbREPP);
			this.ClientArea.Controls.Add(this.fraDate);
			this.ClientArea.Size = new System.Drawing.Size(474, 393);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(474, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(245, 30);
			this.HeaderText.Text = "Reprint Purge Report";
			// 
			// cmbREPP
			// 
			this.cmbREPP.AutoSize = false;
			this.cmbREPP.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbREPP.FormattingEnabled = true;
			this.cmbREPP.Items.AddRange(new object[] {
				"Real Estate",
				"Personal Property"
			});
			this.cmbREPP.Location = new System.Drawing.Point(157, 30);
			this.cmbREPP.Name = "cmbREPP";
			this.cmbREPP.Size = new System.Drawing.Size(279, 40);
			this.cmbREPP.TabIndex = 1;
			// 
			// fraDate
			// 
			this.fraDate.AppearanceKey = "groupBoxNoBorders";
			this.fraDate.Controls.Add(this.fileBox);
			this.fraDate.Controls.Add(this.lblDateTitle);
			this.fraDate.Controls.Add(this.lblDate);
			this.fraDate.Location = new System.Drawing.Point(0, 173);
			this.fraDate.Name = "fraDate";
			this.fraDate.Size = new System.Drawing.Size(456, 213);
			this.fraDate.TabIndex = 2;
			this.fraDate.UseMnemonic = false;
			this.fraDate.Visible = false;
			// 
			// fileBox
			// 
			this.fileBox.Items.AddRange(new object[] {
				"Default.rdp",
				"desktop.ini",
				"Default.rdp",
				"desktop.ini",
				"Default.rdp",
				"desktop.ini",
				"Default.rdp",
				"desktop.ini",
				"Default.rdp",
				"desktop.ini",
				"Default.rdp",
				"desktop.ini"
			});
			this.fileBox.Location = new System.Drawing.Point(157, 66);
			this.fileBox.Name = "fileBox";
			this.fileBox.Size = new System.Drawing.Size(279, 132);
			this.fileBox.TabIndex = 2;
			// 
			// lblDateTitle
			// 
			this.lblDateTitle.AutoSize = true;
			this.lblDateTitle.Location = new System.Drawing.Point(30, 80);
			this.lblDateTitle.Name = "lblDateTitle";
			this.lblDateTitle.Size = new System.Drawing.Size(40, 16);
			this.lblDateTitle.TabIndex = 1;
			this.lblDateTitle.Text = "DATE";
			this.lblDateTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDate
			// 
			this.lblDate.Location = new System.Drawing.Point(30, 30);
			this.lblDate.Name = "lblDate";
			this.lblDate.Size = new System.Drawing.Size(307, 16);
			this.lblDate.TabIndex = 0;
			this.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.Text = "File";
			// 
			// lblREPP
			// 
			this.lblREPP.AutoSize = true;
			this.lblREPP.Location = new System.Drawing.Point(30, 44);
			this.lblREPP.Name = "lblREPP";
			this.lblREPP.Size = new System.Drawing.Size(75, 16);
			this.lblREPP.TabIndex = 0;
			this.lblREPP.Text = "PROPERTY";
			this.lblREPP.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(30, 107);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(158, 48);
			this.btnProcess.TabIndex = 1;
			this.btnProcess.Text = "Save & Preview";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// frmReprintPurgeList
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(474, 480);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmReprintPurgeList";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Reprint Purge Report";
			this.Load += new System.EventHandler(this.frmReprintPurgeList_Load);
			this.Activated += new System.EventHandler(this.frmReprintPurgeList_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmReprintPurgeList_KeyPress);
			this.Resize += new System.EventHandler(this.frmReprintPurgeList_Resize);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraDate)).EndInit();
			this.fraDate.ResumeLayout(false);
			this.fraDate.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCLabel lblREPP;
		private FCButton btnProcess;
	}
}
