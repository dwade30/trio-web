﻿using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using TWCL0000.DataAccess;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmPurge.
	/// </summary>
	public partial class frmPurge : BaseForm
	{
		public frmPurge()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPurge InstancePtr
		{
			get
			{
				return (frmPurge)Sys.GetInstance(typeof(frmPurge));
			}
		}

		protected frmPurge _InstancePtr = null;

		int lngRE;
		int lngPP;
		int lngLN;
		public DateTime dtDate;
        private bool isRealEstate;
        private bool isPersonalProperty;

        private void cmdPurge_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strSQL = "";
				clsDRWrapper rsPurge = new clsDRWrapper();
				clsDRWrapper rsBill = new clsDRWrapper();
				string strList = "";
				string strOut = "";
				int lngErrorNum = 0;
				string strError = "";
				int intError/*unused?*/;
				bool boolPurged/*unused?*/;

				// check to see if there is any selection made
                isRealEstate = cmbREPP.SelectedIndex == 0;
                isPersonalProperty = cmbREPP.SelectedIndex == 1;

                if (!isRealEstate && !isPersonalProperty)
				{
					FCMessageBox.Show("Please make a selection.", MsgBoxStyle.Information, "No Selection Made");
					cmbREPP.Focus();
					return;
				}

                if (cmbYear.SelectedIndex >= 0)
				{
                    var selectedYear = Strings.Left(Strings.Trim(cmbYear.Items[cmbYear.SelectedIndex].ToString()),4);
                    switch (cmbREPP.SelectedIndex)
                    {
                        // RE 
                        case 0 when FCMessageBox.Show($"Are you sure that you would like to Purge your Real Estate Collections file of all bills paid for the {selectedYear} billing year?" + "\r\n" + "This may take a few minutes.", MsgBoxStyle.Question | MsgBoxStyle.YesNo, "Purge Confirmation") == DialogResult.Yes:
                        {
                            if (rptPurge.InstancePtr.Init(true, dtDate, this.Modal, lngErrorNum, strError))
                            {
                                // this will select all the non lien accounts that have been paid before the data passed in...
                            }
                            else
                            {
                                FCMessageBox.Show("Error #" + FCConvert.ToString(lngErrorNum) + " - " + Information.Err().Description + " while running report.  Purge Aborted", MsgBoxStyle.Information, "TRIO Software");
                                txtDate.Focus();
                                return;
                            }

                            break;
                        }
                        case 0:
                            NotifyAbortPurge();
                            return;

                        // PP Records
                        case 1 when FCMessageBox.Show($"Are you sure that you would like to Purge your Personal Property Collections file of all bills paid for the {selectedYear} billing year?" + "\r\n" + "This may take a few minutes.", MsgBoxStyle.Question | MsgBoxStyle.YesNo, "Purge Confirmation") == DialogResult.Yes:
                        {
                            if (!rptPurge.InstancePtr.Init(false, dtDate, this.Modal, lngErrorNum, strError))
                            {
                                FCMessageBox.Show(
                                    "Error #" + FCConvert.ToString(lngErrorNum) + " - " +
                                    Information.Err().Description + " while running report.  Purge Aborted",
                                    MsgBoxStyle.Information, "TRIO Software");
                                txtDate.Focus();
                                return;
                            }

                            break;
                        }
                        case 1:
                            NotifyAbortPurge();
                            return;
                    }

                    App.DoEvents();
                    
					if (isRealEstate)
					{
                        var repo = new REBillingMasterRepository_Legacy();
                        repo.Purge(new List<string> { selectedYear });
                    }
					else
                    {
                       
                        var repo = new PPBillingMasterRepository_Legacy();
                        repo.Purge(new List<string> { selectedYear });
                    }
                    
                    // purge the CYA Table
                    var CYA_repo = new CYABillingMasterRepository_Legacy();
                    CYA_repo.Purge(new List<string> { selectedYear });

                    Close();
				}
				else
				{
					FCMessageBox.Show("Please select a year.", MsgBoxStyle.Critical, "Invalid year");
					cmbYear.Focus();
				}
            }
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MsgBoxStyle.Critical, "Purge Error");
			}
		}

        private void NotifyAbortPurge()
        {
            FCMessageBox.Show("Purge Aborted", MsgBoxStyle.Information, "TRIO Software");
            txtDate.Focus();
        }

        public void cmdPurge_Click()
		{
			cmdPurge_Click(cmdPurge, new System.EventArgs());
		}

		private void frmPurge_Activated(object sender, System.EventArgs e)
		{
			lblDate.Text = "All bills paid from the billing year entered will be purged.";
			ShowFrame(fraDate);
			cmbREPP.Visible = true;
		}

		private void frmPurge_Load(object sender, System.EventArgs e)
		{
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			this.Text = "Purge Collection Files";

            cmbREPP.SelectedIndex = modStatusPayments.Statics.boolRE ? 0 : 1;
			FillYearCombo();

            var strMessage = "Please make sure that you have a current permanent backup of the database before proceeding." + "\r\n";
			strMessage += "Make sure that everyone is out of TRIO when running this function." + "\r\n";
			strMessage += "This operation is memory intensive so please be patient.";
			FCMessageBox.Show(strMessage, MsgBoxStyle.Exclamation, "Backup!");
		}

		private void frmPurge_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
            FormUtilities.KeyPressHandler(e, this);
		}

		private void frmPurge_Resize(object sender, System.EventArgs e)
		{
			ShowFrame(fraDate);
			cmbREPP.Visible = true;
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void ShowFrame(FCFrame fra)
		{
			fra.Visible = true;
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
        {
            var maxToPurge = Conversion.Val(txtMaxAccounts.Text);

            modCLPurge.Statics.lngMaxPurgeAccounts = maxToPurge > 0 && maxToPurge <= 9999
                    ? FCConvert.ToInt32(Math.Round(maxToPurge))
                    : 1000;
            cmdPurge_Click();
        }

		private void txtDate_GotFocus()
		{
			txtDate.SelStart = 0;
			txtDate.SelLength = txtDate.Text.Length;
		}

		private void txtDate_KeyDown(ref Keys KeyCode, ref short Shift)
		{
			if (KeyCode == Keys.Return)
			{
				cmdPurge_Click();
			}
		}

		private void FillYearCombo()
		{
			try
			{
				clsDRWrapper rsYear = new clsDRWrapper();
				int lngCount/*unused?*/;
				cmbYear.Clear();
				rsYear.OpenRecordset("SELECT DISTINCT BillingYear FROM BillingMaster ORDER BY BillingYear DESC", modExtraModules.strCLDatabase);
				while (!rsYear.EndOfFile())
				{
					cmbYear.AddItem(modExtraModules.FormatYear(FCConvert.ToString(rsYear.Get_Fields_Int32("BillingYear"))));
					rsYear.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MsgBoxStyle.Critical, "Error Filling Combo");
			}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFilePrint_Click(sender, e);
		}
	}

}
