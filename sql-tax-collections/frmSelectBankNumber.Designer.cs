﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmSelectBankNumber.
	/// </summary>
	partial class frmSelectBankNumber : BaseForm
	{
		public fecherFoundation.FCGrid vsBankNumber;
		public fecherFoundation.FCLabel lblBankNumber;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSelectBankNumber));
			this.vsBankNumber = new fecherFoundation.FCGrid();
			this.lblBankNumber = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.btnProcess = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsBankNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 265);
			this.BottomPanel.Size = new System.Drawing.Size(648, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.vsBankNumber);
			this.ClientArea.Controls.Add(this.lblBankNumber);
			this.ClientArea.Size = new System.Drawing.Size(648, 205);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(648, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(252, 35);
			this.HeaderText.Text = "Enter Bank Numbers";
			// 
			// vsBankNumber
			// 
			this.vsBankNumber.AllowSelection = false;
			this.vsBankNumber.AllowUserToResizeColumns = false;
			this.vsBankNumber.AllowUserToResizeRows = false;
			this.vsBankNumber.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsBankNumber.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsBankNumber.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsBankNumber.BackColorBkg = System.Drawing.Color.Empty;
			this.vsBankNumber.BackColorFixed = System.Drawing.Color.Empty;
			this.vsBankNumber.BackColorSel = System.Drawing.Color.Empty;
			this.vsBankNumber.Cols = 3;
			this.vsBankNumber.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle1.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsBankNumber.DefaultCellStyle = dataGridViewCellStyle1;
			this.vsBankNumber.FixedCols = 2;
			this.vsBankNumber.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsBankNumber.FrozenCols = 1;
			this.vsBankNumber.FrozenRows = 0;
			this.vsBankNumber.GridColor = System.Drawing.Color.Empty;
			this.vsBankNumber.Location = new System.Drawing.Point(30, 66);
			this.vsBankNumber.Name = "vsBankNumber";
			this.vsBankNumber.ReadOnly = true;
			this.vsBankNumber.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsBankNumber.RowHeightMin = 0;
			this.vsBankNumber.Rows = 1;
			this.vsBankNumber.ShowColumnVisibilityMenu = false;
			this.vsBankNumber.Size = new System.Drawing.Size(600, 119);
			this.vsBankNumber.StandardTab = true;
			this.vsBankNumber.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsBankNumber.TabIndex = 1;
			this.vsBankNumber.CurrentCellChanged += new System.EventHandler(this.vsBankNumber_RowColChange);
			this.vsBankNumber.Click += new System.EventHandler(this.vsBankNumber_Click);
			// 
			// lblBankNumber
			// 
			this.lblBankNumber.AutoSize = true;
			this.lblBankNumber.Location = new System.Drawing.Point(30, 30);
			this.lblBankNumber.Name = "lblBankNumber";
			this.lblBankNumber.Size = new System.Drawing.Size(505, 16);
			this.lblBankNumber.TabIndex = 0;
			this.lblBankNumber.Text = "PLEASE SELECT THE BANK NUMBER EACH CASH ACCOUNT IS ASSOCIATED WITH";
			this.lblBankNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.Text = "File";
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(268, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(120, 48);
			this.btnProcess.TabIndex = 1;
			this.btnProcess.Text = "Process";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// frmSelectBankNumber
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(648, 373);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmSelectBankNumber";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Enter Bank Numbers";
			this.Load += new System.EventHandler(this.frmSelectBankNumber_Load);
			this.Activated += new System.EventHandler(this.frmSelectBankNumber_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmSelectBankNumber_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsBankNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton btnProcess;
	}
}
