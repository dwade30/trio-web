﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmTaxClubBooklet.
	/// </summary>
	public partial class frmTaxClubBooklet : BaseForm
	{
		public frmTaxClubBooklet()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.txtAddress = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.txtAddress.AddControlArrayElement(txtAddress_3, 3);
			this.txtAddress.AddControlArrayElement(txtAddress_2, 2);
			this.txtAddress.AddControlArrayElement(txtAddress_1, 1);
			this.txtAddress.AddControlArrayElement(txtAddress_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmTaxClubBooklet InstancePtr
		{
			get
			{
				return (frmTaxClubBooklet)Sys.GetInstance(typeof(frmTaxClubBooklet));
			}
		}

		protected frmTaxClubBooklet _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               02/22/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               10/11/2005              *
		// ********************************************************
		clsDRWrapper rsSettings = new clsDRWrapper();
		bool boolLoaded;
		int lngTCNumber;
		public int lngColCheck;
		public int lngColName;
		public int lngColYear;
		public int lngColAccount;
		public int lngColTC;
		//FC:FINAL:DSE Auto-initialize members declared with the "As New" declaration
		//public clsDRWrapper rsData = new clsDRWrapper();
		public clsDRWrapper rsData_AutoInitialized;

		public clsDRWrapper rsData
		{
			get
			{
				if (rsData_AutoInitialized == null)
				{
					rsData = new clsDRWrapper();
				}
				//FC:FINAL:CHN: Fix StackOverflow on recursion.
				// return rsData;
				return rsData_AutoInitialized;
			}
			set
			{
				rsData_AutoInitialized = value;
			}
		}
		// VBto upgrade warning: lngTaxClubNumber As int	OnWrite(short, string)
		public void Init(int lngTaxClubNumber)
		{
			//PPJ:FINAL:MHO:IIT#77 - move setting of default values from Load() to Init()
			lngColCheck = 0;
			lngColAccount = 1;
			lngColTC = 2;
			lngColYear = 3;
			lngColName = 4;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			FormatGrid();
			lngTCNumber = lngTaxClubNumber;
			LoadDefaultSettings();
			SetAccountList();
			this.Show(App.MainForm);
		}

		private void frmTaxClubBooklet_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				boolLoaded = true;
			}
			else
			{
			}
		}

		private void frmTaxClubBooklet_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTaxClubBooklet.Icon	= "frmTaxClubBooklet.frx":0000";
			//frmTaxClubBooklet.FillStyle	= 0;
			//frmTaxClubBooklet.ScaleWidth	= 9300;
			//frmTaxClubBooklet.ScaleHeight	= 7695;
			//frmTaxClubBooklet.LinkTopic	= "Form2";
			//frmTaxClubBooklet.LockControls	= -1  'True;
			//frmTaxClubBooklet.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "9.75";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//vsTC.BackColor	= "-2147483643";
			//			//vsTC.ForeColor	= "-2147483640";
			//vsTC.BorderStyle	= 1;
			//vsTC.FillStyle	= 0;
			//vsTC.Appearance	= 1;
			//vsTC.GridLines	= 1;
			//vsTC.WordWrap	= 0;
			//vsTC.ScrollBars	= 3;
			//vsTC.RightToLeft	= 0;
			//vsTC._cx	= 14737;
			//vsTC._cy	= 6978;
			//vsTC._ConvInfo	= 1;
			//vsTC.MousePointer	= 0;
			//vsTC.BackColorFixed	= -2147483633;
			//			//vsTC.ForeColorFixed	= -2147483630;
			//vsTC.BackColorSel	= -2147483635;
			//			//vsTC.ForeColorSel	= -2147483634;
			//vsTC.BackColorBkg	= -2147483636;
			//vsTC.BackColorAlternate	= -2147483643;
			//vsTC.GridColor	= -2147483633;
			//vsTC.GridColorFixed	= -2147483632;
			//vsTC.TreeColor	= -2147483632;
			//vsTC.FloodColor	= 192;
			//vsTC.SheetBorder	= -2147483642;
			//vsTC.FocusRect	= 1;
			//vsTC.HighLight	= 0;
			//vsTC.AllowSelection	= 0   'False;
			//vsTC.AllowBigSelection	= 0   'False;
			//vsTC.AllowUserResizing	= 1;
			//vsTC.SelectionMode	= 0;
			//vsTC.GridLinesFixed	= 2;
			//vsTC.GridLineWidth	= 1;
			//vsTC.RowHeightMin	= 0;
			//vsTC.RowHeightMax	= 0;
			//vsTC.ColWidthMin	= 0;
			//vsTC.ColWidthMax	= 0;
			//vsTC.ExtendLastCol	= -1  'True;
			//vsTC.FormatString	= "";
			//vsTC.ScrollTrack	= -1  'True;
			//vsTC.ScrollTips	= 0   'False;
			//vsTC.MergeCells	= 0;
			//vsTC.MergeCompare	= 0;
			//vsTC.AutoResize	= -1  'True;
			//vsTC.AutoSizeMode	= 0;
			//vsTC.AutoSearch	= 0;
			//vsTC.AutoSearchDelay	= 2;
			//vsTC.MultiTotals	= -1  'True;
			//vsTC.SubtotalPosition	= 1;
			//vsTC.OutlineBar	= 0;
			//vsTC.OutlineCol	= 0;
			//vsTC.Ellipsis	= 0;
			//vsTC.ExplorerBar	= 1;
			//vsTC.PicturesOver	= 0   'False;
			//vsTC.PictureType	= 0;
			//vsTC.TabBehavior	= 0;
			//vsTC.OwnerDraw	= 0;
			//vsTC.ShowComboButton	= -1  'True;
			//vsTC.TextStyle	= 0;
			//vsTC.TextStyleFixed	= 0;
			//vsTC.OleDragMode	= 0;
			//vsTC.OleDropMode	= 0;
			//vsTC.ComboSearch	= 3;
			//vsTC.AutoSizeMouse	= -1  'True;
			//vsTC.AllowUserFreezing	= 0;
			//vsTC.BackColorFrozen	= 0;
			//			//vsTC.ForeColorFrozen	= 0;
			//vsTC.WallPaperAlignment	= 9;
			//txtAddress_3.MaxLength	= 50;
			//txtAddress_2.MaxLength	= 50;
			//txtAddress_1.MaxLength	= 50;
			//txtAddress_0.MaxLength	= 50;
			//vsElasticLight1.OleObjectBlob	= "frmTaxClubBooklet.frx":058A";
			//End Unmaped Properties
			//PPJ:FINAL:MHO:IIT#77 - move setting of default values from Load() to Init()
			//lngColCheck = 0;
			//lngColAccount = 1;
			//lngColTC = 2;
			//lngColYear = 3;
			//lngColName = 4;
			//modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			//modGlobalFunctions.SetTRIOColors(this);
			//FormatGrid();
		}

		private void frmTaxClubBooklet_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				Close();
			}
			else if (KeyAscii == 13)
			{
				KeyAscii = 0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		//private void frmTaxClubBooklet_Resize(object sender, System.EventArgs e)
		//{
		//	FormatGrid();
		//}
		private void mnuFileClear_Click(object sender, System.EventArgs e)
		{
			int lngCT;
			for (lngCT = 1; lngCT <= vsTC.Rows - 1; lngCT++)
			{
				vsTC.TextMatrix(lngCT, 0, FCConvert.ToString(0));
			}
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void PrintBooklet()
		{
			// this will proceed to printing the booklet
			rptTaxClubBooklet.InstancePtr.Init(lngTCNumber);
			modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "DefaultReturnAddress1", txtAddress[1].Text);
			modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "DefaultReturnAddress2", txtAddress[2].Text);
			modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "DefaultReturnAddress3", txtAddress[3].Text);
		}

		private void LoadDefaultSettings()
		{
			string strTemp = "";
			// this will load default information into the fields
			if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
			{
				txtAddress[0].Text = modGlobalConstants.Statics.gstrCityTown + " of " + modGlobalConstants.Statics.MuniName;
			}
			else
			{
				txtAddress[0].Text = modGlobalConstants.Statics.MuniName;
			}
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "DefaultReturnAddress1", ref strTemp);
			txtAddress[1].Text = Strings.Trim(strTemp);
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "DefaultReturnAddress2", ref strTemp);
			txtAddress[2].Text = Strings.Trim(strTemp);
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "DefaultReturnAddress3", ref strTemp);
			txtAddress[3].Text = Strings.Trim(strTemp);
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			PrintBooklet();
			// Unload Me
		}

		private void SetAccountList()
		{
			if (lngTCNumber > 0)
			{
				// This is a single TC to print
				vsTC.Enabled = false;
				vsTC.Rows = 1;
				FillGrid_2(true);
			}
			else
			{
				vsTC.Enabled = true;
				FillGrid_2(false);
			}
		}

		private void FillGrid_2(bool boolUseTCNumber)
		{
			FillGrid(ref boolUseTCNumber);
		}

		private void FillGrid(ref bool boolUseTCNumber)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strNum = "";
				if (boolUseTCNumber)
				{
					strNum = " AND ID = " + FCConvert.ToString(lngTCNumber);
				}
				vsTC.Rows = 1;
				if (modStatusPayments.Statics.boolRE)
				{
					rsData.OpenRecordset("SELECT * FROM TaxClub WHERE Not (IsNull(NonActive, 0) = 1) AND Type = 'RE'" + strNum);
				}
				else
				{
					rsData.OpenRecordset("SELECT * FROM TaxClub WHERE Not (IsNull(NonActive, 0) = 1) AND Type = 'PP'" + strNum);
				}
				while (!rsData.EndOfFile())
				{
					vsTC.AddItem("");
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					vsTC.TextMatrix(vsTC.Rows - 1, lngColAccount, FCConvert.ToString(rsData.Get_Fields("Account")));
					// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
					vsTC.TextMatrix(vsTC.Rows - 1, lngColYear, FCConvert.ToString(rsData.Get_Fields("Year")));
					vsTC.TextMatrix(vsTC.Rows - 1, lngColTC, FCConvert.ToString(rsData.Get_Fields_Int32("ID")));
					vsTC.TextMatrix(vsTC.Rows - 1, lngColName, FCConvert.ToString(rsData.Get_Fields_String("Name")));
					if (boolUseTCNumber)
					{
						vsTC.TextMatrix(vsTC.Rows - 1, lngColCheck, FCConvert.ToString(-1));
					}
					rsData.MoveNext();
				}
				if (vsTC.Rows > 1)
				{
					lblTC.Visible = true;
					vsTC.Visible = true;
				}
				else
				{
					lblTC.Visible = false;
					vsTC.Visible = false;
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Filling Grid");
			}
		}

		private void FormatGrid()
		{
			int lngWid = 0;
			lngWid = vsTC.WidthOriginal;
			vsTC.ColDataType(lngColCheck, FCGrid.DataTypeSettings.flexDTBoolean);
			vsTC.ColWidth(lngColCheck, FCConvert.ToInt32(lngWid * 0.1));
			vsTC.ColWidth(lngColTC, 0);
			vsTC.ColWidth(lngColName, FCConvert.ToInt32(lngWid * 0.6));
			vsTC.ColWidth(lngColAccount, FCConvert.ToInt32(lngWid * 0.1));
			vsTC.ColWidth(lngColYear, FCConvert.ToInt32(lngWid * 0.1));
		}

		private void mnuFileSelectAll_Click(object sender, System.EventArgs e)
		{
			int lngCT;
			for (lngCT = 1; lngCT <= vsTC.Rows - 1; lngCT++)
			{
				vsTC.TextMatrix(lngCT, 0, FCConvert.ToString(-1));
			}
		}

		private void vsTC_Click(object sender, EventArgs e)
		{
			int lngMR;
			int lngMC;
			lngMR = vsTC.MouseRow;
			lngMC = vsTC.MouseCol;
			if (lngMC == lngColCheck)
			{
				if (Conversion.Val(vsTC.TextMatrix(lngMR, lngMC)) == -1)
				{
					vsTC.TextMatrix(lngMR, lngMC, FCConvert.ToString(0));
				}
				else
				{
					vsTC.TextMatrix(lngMR, lngMC, FCConvert.ToString(-1));
				}
			}
		}

		private void vsTC_DblClick(object sender, EventArgs e)
		{
			int lngCT;
			for (lngCT = 1; lngCT <= vsTC.Rows - 1; lngCT++)
			{
				vsTC.TextMatrix(lngCT, 0, FCConvert.ToString(-1));
			}
		}

		private void vsTC_KeyPress(object sender, KeyPressEventArgs e)
		{
			int lngMR;
			int lngMC;
			lngMR = vsTC.Row;
			lngMC = vsTC.Col;
			if (lngMC == lngColCheck)
			{
				if (Conversion.Val(vsTC.TextMatrix(lngMR, lngMC)) == -1)
				{
					vsTC.TextMatrix(lngMR, lngMC, FCConvert.ToString(0));
				}
				else
				{
					vsTC.TextMatrix(lngMR, lngMC, FCConvert.ToString(-1));
				}
			}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFileSaveExit_Click(sender, e);
		}

		private void cmdFileClear_Click(object sender, EventArgs e)
		{
			this.mnuFileClear_Click(sender, e);
		}

		private void cmdFileSelectAll_Click(object sender, EventArgs e)
		{
			this.mnuFileSelectAll_Click(sender, e);
		}
	}
}
