﻿namespace TWCL0000
{
    partial class frmRealEstateTaxAccountSearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            this.btnSaveExit = new fecherFoundation.FCButton();
            this.SearchGrid = new fecherFoundation.FCGrid();
            this.txtMapLot = new fecherFoundation.FCTextBox();
            this.txtName = new fecherFoundation.FCTextBox();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.btnSearch = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSaveExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SearchGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearch)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnSaveExit);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.SearchGrid);
            this.ClientArea.Controls.Add(this.txtMapLot);
            this.ClientArea.Controls.Add(this.txtName);
            this.ClientArea.Controls.Add(this.Label10);
            this.ClientArea.Controls.Add(this.Label9);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.btnSearch);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnSearch, 0);
            // 
            // btnSaveExit
            // 
            this.btnSaveExit.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.btnSaveExit.AppearanceKey = "acceptButton";
            this.btnSaveExit.Location = new System.Drawing.Point(402, 30);
            this.btnSaveExit.Name = "btnSaveExit";
            this.btnSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnSaveExit.Size = new System.Drawing.Size(211, 48);
            this.btnSaveExit.TabIndex = 5;
            this.btnSaveExit.Text = "Select Account";
            // 
            // SearchGrid
            // 
            this.SearchGrid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.SearchGrid.Cols = 10;
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SearchGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.SearchGrid.FixedCols = 0;
            this.SearchGrid.ForeColorFixed = System.Drawing.SystemColors.ControlText;
            this.SearchGrid.Location = new System.Drawing.Point(30, 83);
            this.SearchGrid.Name = "SearchGrid";
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SearchGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.SearchGrid.RowHeadersVisible = false;
            this.SearchGrid.Rows = 1;
            this.SearchGrid.ShowFocusCell = false;
            this.SearchGrid.Size = new System.Drawing.Size(935, 339);
            this.SearchGrid.TabIndex = 41;
            // 
            // txtMapLot
            // 
            this.txtMapLot.BackColor = System.Drawing.SystemColors.Window;
            this.txtMapLot.Location = new System.Drawing.Point(504, 21);
            this.txtMapLot.Name = "txtMapLot";
            this.txtMapLot.Size = new System.Drawing.Size(158, 40);
            this.txtMapLot.TabIndex = 39;
            // 
            // txtName
            // 
            this.txtName.BackColor = System.Drawing.SystemColors.Window;
            this.txtName.Location = new System.Drawing.Point(92, 21);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(259, 40);
            this.txtName.TabIndex = 37;
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(419, 35);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(62, 20);
            this.Label10.TabIndex = 38;
            this.Label10.Text = "MAP LOT";
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(31, 35);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(69, 20);
            this.Label9.TabIndex = 40;
            this.Label9.Text = "NAME";
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(863, 26);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Shortcut = Wisej.Web.Shortcut.F6;
            this.btnSearch.Size = new System.Drawing.Size(110, 24);
            this.btnSearch.TabIndex = 5;
            this.btnSearch.Text = "Search";
            // 
            // frmRealEstateTaxAccountSearch
            // 
            this.Name = "frmRealEstateTaxAccountSearch";
            this.Text = "Select Account";
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSaveExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SearchGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearch)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private fecherFoundation.FCButton btnSaveExit;
        public fecherFoundation.FCGrid SearchGrid;
        public fecherFoundation.FCTextBox txtMapLot;
        public fecherFoundation.FCTextBox txtName;
        public fecherFoundation.FCLabel Label10;
        public fecherFoundation.FCLabel Label9;
        private fecherFoundation.FCButton btnSearch;
    }
}