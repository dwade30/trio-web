﻿namespace TWCL0000
{
        public enum CLBillingAddressType
        {
            AddressAtBilling = 0,
            CareOfCurrentOwner = 1,
            LastRecordedAddress = 2
        }
}