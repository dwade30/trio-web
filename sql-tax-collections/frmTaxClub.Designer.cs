﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmTaxClub.
	/// </summary>
	partial class frmTaxClub : BaseForm
	{
		public fecherFoundation.FCComboBox cmbFreq;
		public fecherFoundation.FCTextBox txtAccount;
		public fecherFoundation.FCLabel lblAccount;
		public fecherFoundation.FCGrid vsTaxClubSummary;
		public fecherFoundation.FCFrame fraTaxClubDetails;
		public fecherFoundation.FCGrid vsTaxClub;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrintMaster;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrintBooklet;
		private Wisej.Web.ToolTip ToolTip1;
		private FCLabel lblFreq;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTaxClub));
            this.cmbFreq = new fecherFoundation.FCComboBox();
            this.txtAccount = new fecherFoundation.FCTextBox();
            this.lblAccount = new fecherFoundation.FCLabel();
            this.vsTaxClubSummary = new fecherFoundation.FCGrid();
            this.fraTaxClubDetails = new fecherFoundation.FCFrame();
            this.vsTaxClub = new fecherFoundation.FCGrid();
            this.lblFreq = new fecherFoundation.FCLabel();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuFilePrintMaster = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePrintBooklet = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.fraTaxClubSummary = new fecherFoundation.FCFrame();
            this.lblTaxClubSummary = new fecherFoundation.FCLabel();
            this.btnProcess = new fecherFoundation.FCButton();
            this.cmdFileDelete = new fecherFoundation.FCButton();
            this.cmdFileNew = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsTaxClubSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraTaxClubDetails)).BeginInit();
            this.fraTaxClubDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsTaxClub)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraTaxClubSummary)).BeginInit();
            this.fraTaxClubSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileNew)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 447);
            this.BottomPanel.Size = new System.Drawing.Size(717, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.lblTaxClubSummary);
            this.ClientArea.Controls.Add(this.txtAccount);
            this.ClientArea.Controls.Add(this.lblAccount);
            this.ClientArea.Controls.Add(this.fraTaxClubDetails);
            this.ClientArea.Controls.Add(this.fraTaxClubSummary);
            this.ClientArea.Size = new System.Drawing.Size(717, 387);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdFileNew);
            this.TopPanel.Controls.Add(this.cmdFileDelete);
            this.TopPanel.Size = new System.Drawing.Size(717, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileDelete, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileNew, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(26, 26);
            this.HeaderText.Size = new System.Drawing.Size(110, 30);
            this.HeaderText.Text = "Tax Club";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbFreq
            // 
            this.cmbFreq.Items.AddRange(new object[] {
            "Monthly",
            "Weekly",
            "Bi Weekly"});
            this.cmbFreq.Location = new System.Drawing.Point(136, 43);
            this.cmbFreq.Name = "cmbFreq";
            this.cmbFreq.Size = new System.Drawing.Size(147, 40);
            this.cmbFreq.TabIndex = 1;
            this.cmbFreq.Text = "Monthly";
            this.ToolTip1.SetToolTip(this.cmbFreq, null);
            // 
            // txtAccount
            // 
            this.txtAccount.BackColor = System.Drawing.SystemColors.Window;
            this.txtAccount.Location = new System.Drawing.Point(166, 23);
            this.txtAccount.Name = "txtAccount";
            this.txtAccount.Size = new System.Drawing.Size(147, 40);
            this.txtAccount.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.txtAccount, null);
            this.txtAccount.Enter += new System.EventHandler(this.txtAccount_Enter);
            this.txtAccount.Validating += new System.ComponentModel.CancelEventHandler(this.txtAccount_Validating);
            this.txtAccount.KeyDown += new Wisej.Web.KeyEventHandler(this.txtAccount_KeyDown);
            // 
            // lblAccount
            // 
            this.lblAccount.AutoSize = true;
            this.lblAccount.Location = new System.Drawing.Point(30, 37);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(131, 17);
            this.lblAccount.Text = "SELECT ACCOUNT";
            this.lblAccount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblAccount, null);
            // 
            // vsTaxClubSummary
            // 
            this.vsTaxClubSummary.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsTaxClubSummary.Cols = 3;
            this.vsTaxClubSummary.ColumnHeadersVisible = false;
            this.vsTaxClubSummary.FixedRows = 0;
            this.vsTaxClubSummary.Location = new System.Drawing.Point(30, 33);
            this.vsTaxClubSummary.Name = "vsTaxClubSummary";
            this.vsTaxClubSummary.Rows = 1;
            this.vsTaxClubSummary.Size = new System.Drawing.Size(672, 223);
            this.vsTaxClubSummary.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.vsTaxClubSummary, null);
            this.vsTaxClubSummary.CurrentCellChanged += new System.EventHandler(this.vsTaxClubSummary_RowColChange);
            this.vsTaxClubSummary.DoubleClick += new System.EventHandler(this.vsTaxClubSummary_DblClick);
            // 
            // fraTaxClubDetails
            // 
            this.fraTaxClubDetails.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraTaxClubDetails.AppearanceKey = "groupBoxNoBorders";
            this.fraTaxClubDetails.Controls.Add(this.vsTaxClub);
            this.fraTaxClubDetails.Controls.Add(this.cmbFreq);
            this.fraTaxClubDetails.Controls.Add(this.lblFreq);
            this.fraTaxClubDetails.Location = new System.Drawing.Point(30, 81);
            this.fraTaxClubDetails.Name = "fraTaxClubDetails";
            this.fraTaxClubDetails.Size = new System.Drawing.Size(673, 297);
            this.fraTaxClubDetails.TabIndex = 2;
            this.fraTaxClubDetails.Text = "Account Information";
            this.ToolTip1.SetToolTip(this.fraTaxClubDetails, null);
            this.fraTaxClubDetails.UseMnemonic = false;
            this.fraTaxClubDetails.Visible = false;
            // 
            // vsTaxClub
            // 
            this.vsTaxClub.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsTaxClub.Cols = 3;
            this.vsTaxClub.ColumnHeadersVisible = false;
            this.vsTaxClub.FixedRows = 0;
            this.vsTaxClub.Location = new System.Drawing.Point(0, 94);
            this.vsTaxClub.Name = "vsTaxClub";
            this.vsTaxClub.Rows = 1;
            this.vsTaxClub.Size = new System.Drawing.Size(663, 194);
            this.vsTaxClub.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.vsTaxClub, null);
            this.vsTaxClub.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsTaxClub_BeforeEdit);
            this.vsTaxClub.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsTaxClub_ValidateEdit);
            this.vsTaxClub.CurrentCellChanged += new System.EventHandler(this.vsTaxClub_RowColChange);
            // 
            // lblFreq
            // 
            this.lblFreq.AutoSize = true;
            this.lblFreq.Location = new System.Drawing.Point(0, 57);
            this.lblFreq.Name = "lblFreq";
            this.lblFreq.Size = new System.Drawing.Size(92, 17);
            this.lblFreq.Text = "FREQUENCY";
            this.lblFreq.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblFreq, null);
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFilePrintMaster,
            this.mnuFilePrintBooklet});
            this.MainMenu1.Name = null;
            // 
            // mnuFilePrintMaster
            // 
            this.mnuFilePrintMaster.Index = 0;
            this.mnuFilePrintMaster.Name = "mnuFilePrintMaster";
            this.mnuFilePrintMaster.Text = "Print Tax Club Master";
            this.mnuFilePrintMaster.Click += new System.EventHandler(this.mnuFilePrintMaster_Click);
            // 
            // mnuFilePrintBooklet
            // 
            this.mnuFilePrintBooklet.Index = 1;
            this.mnuFilePrintBooklet.Name = "mnuFilePrintBooklet";
            this.mnuFilePrintBooklet.Text = "Print Tax Club Payment Book";
            this.mnuFilePrintBooklet.Click += new System.EventHandler(this.mnuFilePrintBooklet_Click);
            // 
            // mnuFilePrint
            // 
            this.mnuFilePrint.Enabled = false;
            this.mnuFilePrint.Index = -1;
            this.mnuFilePrint.Name = "mnuFilePrint";
            this.mnuFilePrint.Text = "Print";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // fraTaxClubSummary
            // 
            this.fraTaxClubSummary.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraTaxClubSummary.AppearanceKey = "groupBoxNoBorders";
            this.fraTaxClubSummary.Controls.Add(this.vsTaxClubSummary);
            this.fraTaxClubSummary.Location = new System.Drawing.Point(0, 110);
            this.fraTaxClubSummary.Name = "fraTaxClubSummary";
            this.fraTaxClubSummary.Size = new System.Drawing.Size(703, 274);
            this.fraTaxClubSummary.TabIndex = 3;
            this.fraTaxClubSummary.UseMnemonic = false;
            this.fraTaxClubSummary.Visible = false;
            // 
            // lblTaxClubSummary
            // 
            this.lblTaxClubSummary.AutoSize = true;
            this.lblTaxClubSummary.Location = new System.Drawing.Point(30, 81);
            this.lblTaxClubSummary.Name = "lblTaxClubSummary";
            this.lblTaxClubSummary.Size = new System.Drawing.Size(77, 17);
            this.lblTaxClubSummary.TabIndex = 12;
            this.lblTaxClubSummary.Text = "SUMMARY";
            this.lblTaxClubSummary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTaxClubSummary.Visible = false;
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Location = new System.Drawing.Point(292, 30);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcess.Size = new System.Drawing.Size(120, 48);
            this.btnProcess.TabIndex = 1;
            this.btnProcess.Text = "Save";
            this.btnProcess.Visible = false;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // cmdFileDelete
            // 
            this.cmdFileDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileDelete.Enabled = false;
            this.cmdFileDelete.Location = new System.Drawing.Point(579, 30);
            this.cmdFileDelete.Name = "cmdFileDelete";
            this.cmdFileDelete.Size = new System.Drawing.Size(114, 24);
            this.cmdFileDelete.TabIndex = 57;
            this.cmdFileDelete.Text = "Delete Tax Club";
            this.cmdFileDelete.Click += new System.EventHandler(this.cmdFileDelete_Click);
            // 
            // cmdFileNew
            // 
            this.cmdFileNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileNew.Location = new System.Drawing.Point(473, 30);
            this.cmdFileNew.Name = "cmdFileNew";
            this.cmdFileNew.Shortcut = Wisej.Web.Shortcut.CtrlShiftN;
            this.cmdFileNew.Size = new System.Drawing.Size(99, 24);
            this.cmdFileNew.TabIndex = 58;
            this.cmdFileNew.Text = "New Tax Club";
            this.cmdFileNew.Click += new System.EventHandler(this.cmdFileNew_Click);
            // 
            // frmTaxClub
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(717, 555);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmTaxClub";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Tax Club";
            this.ToolTip1.SetToolTip(this, null);
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmTaxClub_Load);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmTaxClub_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsTaxClubSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraTaxClubDetails)).EndInit();
            this.fraTaxClubDetails.ResumeLayout(false);
            this.fraTaxClubDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsTaxClub)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraTaxClubSummary)).EndInit();
            this.fraTaxClubSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileNew)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		public FCFrame fraTaxClubSummary;
		private FCButton btnProcess;
		public FCButton cmdFileDelete;
		public FCButton cmdFileNew;
		public FCLabel lblTaxClubSummary;
	}
}
