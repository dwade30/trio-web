﻿using Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedDataAccess;
namespace TWCL0000
{
    public static class CLAppSpecificExtensions
    {
        public static DataContextDetails GetContextDetails(this cGlobalSettings settings)
        {
            var dataDetails = new SharedDataAccess.DataContextDetails();
            dataDetails.DataSource = settings.DataSource;
            dataDetails.DataEnvironment = settings.DataEnvironment;
            dataDetails.Password = settings.Password;
            dataDetails.UserID = settings.UserName;
            return dataDetails;
        }
    }
}
