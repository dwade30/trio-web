﻿using Global;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections;

namespace TWCL0000
{
    public class ShowRETaxInformationSheetInCollectionsHandler : CommandHandler<ShowRETaxInformationSheetInCollections>
    {
        protected override void Handle(ShowRETaxInformationSheetInCollections command)
        {
            rptAccountInformationSheet.InstancePtr.Init(command.Account,command.EffectiveDate);
        }
    }
}