﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using Microsoft.Ajax.Utilities;
using SharedApplication.Extensions;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmCLEditBillInfo.
	/// </summary>
	public partial class frmCLEditBillInfo : BaseForm
	{
		public frmCLEditBillInfo()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCLEditBillInfo InstancePtr
		{
			get
			{
				return (frmCLEditBillInfo)Sys.GetInstance(typeof(frmCLEditBillInfo));
			}
		}

		protected frmCLEditBillInfo _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               04/27/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/14/2006              *
		// ********************************************************
		public string auxCmbSearchTypeItem2 = "Map / Lot";
		int lngYear;
		bool boolResultsScreen;
		// this is true when the result grid is being shown
		bool boolSearch;
		string strSearchString;
		bool boolDirty;
		int lngLastRow;
		string strCopyName1;
		string strCopyName2;
		string strCopyAddr1;
		string strCopyAddr2;
		string strCopyAddr3;
		string strCopyMailingAddr3;
		string strCopyMapLot;
		string strCopyBookPage;
		string strCopyStreetNumber;
		string strCopyStreetName;
		string strCopyRef1;
		bool boolCopyUseRef1;
		private string strCopyInterestPaidDate;

		// these constants are for the grid columns
		// edit grid
		int lngColEDYear;
		int lngColEDName1;
		int lngColEDName2;
		int lngColEDBillkey;
		// Dim lngColMapLot                As Long
		// search grid
		int lngColAcct;
		int lngColName;
		int lngcolLocationNumber;
		int lngColLocation;
		int lngColMapLot;
		int lngColBookPage;
		int lngColStreetNumber;
		int lngColStreetName;
		int lngRowOwner;
		int lngRowSecondOwner;
		int lngRowAddress1;
		int lngRowAddress2;
		int lngRowAddress3;
		int lngRowMailingAddress3;
		int lngRowMapLot;
		int lngRowBookPage;
		int lngRowStreetNumber;
		int lngRowStreetName;
		int lngRowRef1;
		int lngRowUseRef1OnLien;
		private int lngRowInterestPaidDate;

		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			if (boolResultsScreen)
			{
				//FC:FINAL:DDU #i274 disable save button
				//cmdFileSave.Enabled = false;
				vsSearch.Visible = false;
				lblSearchListInstruction.Visible = false;
				fraSearchCriteria.Visible = true;
				fraSearch.Visible = true;
				fraGetAccount.Visible = true;
				boolResultsScreen = false;
				btnProcess.Enabled = true;
				cmdProcessSearch.Enabled = true;
			}
			else
			{
				cmbSearchType.SelectedIndex = 0;
				txtSearch.Text = "";
			}
		}

		public void cmdClear_Click()
		{
			cmdClear_Click(cmdClear, new System.EventArgs());
		}

		private void cmdGetAccountNumber_Click(object sender, System.EventArgs e)
		{
			int intError = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int holdaccount = 0;
				clsDRWrapper rsCL = new clsDRWrapper();
				clsDRWrapper rsRE = new clsDRWrapper();
				string strSQL = "";
				int intAns/*unused?*/;
				lblSearchListInstruction.Visible = false;
				TryAgain:
				;
				if (Conversion.Val(txtGetAccountNumber.Text) != 0)
				{
					holdaccount = FCConvert.ToInt32(FCConvert.ToDouble(txtGetAccountNumber.Text));
					intError = 1;
					if (modStatusPayments.Statics.boolRE)
					{
                        StaticSettings.TaxCollectionValues.LastAccountRE = holdaccount;
						strSQL = "SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + " AND BillingType = 'RE'";
					}
					else
					{
                        StaticSettings.TaxCollectionValues.LastAccountPP = holdaccount;
						strSQL = "SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP) + " AND BillingType = 'PP'";
					}
					intError = 2;
					rsCL.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
					intError = 3;
					if (rsCL.EndOfFile() != true && rsCL.BeginningOfFile() != true)
					{
						// Create Queries on the database for the next screen to use
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						ShowEditAccount_2(FCConvert.ToInt32(rsCL.Get_Fields("Account")));
					}
					else
					{
						FCMessageBox.Show("The account selected does not have any billing records.", MsgBoxStyle.Information, "Error");
						return;
					}
					intError = 50;
					// set the last account field in the registry to the current account
					if (cmbRE.SelectedIndex == 0)
					{
						intError = 51;
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "RELastAccountNumber", modGlobal.PadToString(StaticSettings.TaxCollectionValues.LastAccountRE, 6));
					}
					else
					{
						intError = 52;
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "PPLastAccountNumber", modGlobal.PadToString(StaticSettings.TaxCollectionValues.LastAccountPP, 6));
					}
					intError = 53;
					//cmdFileSave.Enabled = true;
					btnProcess.Enabled = true;
					cmdProcessSearch.Enabled = true;
				}
				else
				{
					frmWait.InstancePtr.Unload();
					FCMessageBox.Show("Please enter a valid account number.", MsgBoxStyle.Information, "Invalid Account");
					return;
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("ERROR #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Get Account Error - " + FCConvert.ToString(intError));
				Close();
			}
		}

		public void cmdGetAccountNumber_Click()
		{
			cmdGetAccountNumber_Click(cmdGetAccountNumber, new System.EventArgs());
		}

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
            clsDRWrapper rs = new clsDRWrapper();
            clsDRWrapper rsBook = new clsDRWrapper();
            clsDRWrapper rsCLAccounts = new clsDRWrapper();

            try
            {
                // On Error GoTo ERROR_HANDLER
                int lngSelection = 0;
                string strAcctList = "";
                string strLeftOverAccts = "";
                string SQL = "";

                string strList = "";
                string strBook = "";
                string strLastName = "";
                string strDoubleSearch = "";

                switch (cmbSearchType.SelectedIndex)
                {
                    // find out which search option has been checked
                    case 0:
                        lngSelection = 0;

                        break;
                    case 1:
                        lngSelection = 1;

                        break;
                    case 2:
                        lngSelection = 2;

                        break;
                    case 3:
                        lngSelection = 3;

                        break;
                    default:
                        FCMessageBox.Show("You must select a field to search by from the 'Search By' box.", MsgBoxStyle.Information, "Search By");

                        return;
                }

                // make sure that the user typed search criteria in
                if (Strings.Trim(txtSearch.Text) == "")
                {
                    FCMessageBox.Show("You must type a search criteria in the white box.", MsgBoxStyle.Information, "Search Criteria");

                    return;
                }

                frmWait.InstancePtr.Init("Please Wait...." + "\r\n" + "Searching");
                App.DoEvents();

                switch (lngSelection)
                {
                    case 0:
                    {
                        // Name
                        // check for the type of account to look for Real Estate or Personal Property
                        if (modStatusPayments.Statics.boolRE)
                        {
                            SQL = "SELECT * FROM BillingMaster INNER JOIN (SELECT ID AS BK FROM BillingMaster WHERE BillingType = 'RE' AND ((Name1 >= '" + txtSearch.Text + "  ' AND Name1 < '" + txtSearch.Text + "ZZZ') OR (Name2 >= '" + txtSearch.Text + "  ' AND Name2 < '" + txtSearch.Text + "ZZZ'))) AS List ON BillingMaster.ID = List.BK ORDER BY Name1";
                        }
                        else
                        {
                            SQL = "SELECT * FROM BillingMaster INNER JOIN (SELECT ID AS BK FROM BillingMaster WHERE BillingType = 'PP' AND ((Name1 >= '" + txtSearch.Text + "  ' AND Name1 < '" + txtSearch.Text + "ZZZ') OR (Name2 >= '" + txtSearch.Text + "  ' AND Name2 < '" + txtSearch.Text + "ZZZ'))) AS List ON BillingMaster.ID = List.BK ORDER BY Name1";
                        }

                        // this will create a string of accounts that will be added in the grid as well as the RE matches (for old collection files with that name)
                        rsCLAccounts.OpenRecordset(SQL, modExtraModules.strCLDatabase);
                        strAcctList = ",";

                        while (!rsCLAccounts.EndOfFile())
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            strAcctList += FCConvert.ToString(rsCLAccounts.Get_Fields("Account")) + ",";
                            rsCLAccounts.MoveNext();
                        }

                        if (modStatusPayments.Statics.boolRE)
                        {
                            // If strAcctList = "" Then
                            // kgk SQL = "SELECT *, RSAccount AS Account, RSName AS Name FROM Master WHERE (RSName >= '" & txtSearch.Text & "' AND RSName < '" & txtSearch.Text & "ZZZ') OR (RSSecOwner >= '" & txtSearch.Text & "' AND RSSecOwner < '" & txtSearch.Text & "ZZZ') ORDER BY RSName, RSAccount"
                            SQL = "SELECT OwnerPartyID, SecOwnerPartyID, DeedName1 as Own1FullName, DeedName2 as Own2FullName, RSLocStreet, RSLocNumAlph, RSMapLot, RSAccount AS Account FROM Master";
                            strDoubleSearch =
                                "SELECT RSAccount AS Account, DeedName1 AS Name, DeedName2 FROM Master ";
                            strDoubleSearch = strDoubleSearch + "WHERE (DeedName1 >= '" + txtSearch.Text + "' AND DeedName1 < '" + txtSearch.Text + "ZZZ') OR (DeedName2 >= '" + txtSearch.Text + "' AND DeedName2 < '" + txtSearch.Text + "ZZZ')";
                            strDoubleSearch += "UNION ALL (SELECT Account, Name1 AS Name FROM BillingMaster WHERE BillingType = 'RE' AND Name1 >= '" + txtSearch.Text + "' AND Name1 < '" + txtSearch.Text + "ZZZ')";
                            strDoubleSearch = "SELECT DISTINCT Account, Name FROM (" + strDoubleSearch + ") As qNew ORDER BY Name";

                            rs.OpenRecordset(SQL, modExtraModules.strREDatabase);

                            rs.MoveFirst();
                        }
                        else
                        {
                            // If strAcctList = "" Then
                            SQL = "SELECT PartyID, Street, StreetNumber, Account FROM PPMaster";
                            strDoubleSearch = "(SELECT Account, p.FullNameLF AS Name " + "FROM PPMaster CROSS APPLY " + modGlobal.Statics.strDbCP + "GetPartyNameAndAddress(PPMaster.PartyID,NULL,'PP',PPMaster.Account) AS p1 " + "WHERE (p1.FullNameLF >= '" + txtSearch.Text + "' AND p1.FullNameLF < '" + txtSearch.Text + "ZZZ') ";
                            strDoubleSearch += "UNION ALL (SELECT Account, Name1 AS Name FROM BillingMaster WHERE BillingType = 'PP' AND Name1 >= '" + txtSearch.Text + "' AND Name1 < '" + txtSearch.Text + "ZZZ')";
                            strDoubleSearch = "SELECT DISTINCT Account, Name FROM (" + strDoubleSearch + ")) As qNew ORDER BY Name";

                            rs.OpenRecordset(SQL, modExtraModules.strREDatabase);
                            rs.InsertName("PartyID", "Own1", false, true, true, "", false, "Own1FullName,Account", true, "(Own1FullName >= '" + txtSearch.Text + "' AND Own1FullName < '" + txtSearch.Text + "ZZZ')");
                            rs.MoveFirst();
                        }

                        strSearchString = strDoubleSearch;

                        break;
                    }
                    case 1:
                    {
                        // Street Name
                        rsCLAccounts.OpenRecordset("SELECT * FROM BillingMaster WHERE ID = 0", modExtraModules.strCLDatabase);

                        if (modStatusPayments.Statics.boolRE)
                        {
                            // XXXXXXXX this was ORDER BY Val(RSLocNumAlph & '')
                            SQL = "SELECT OwnerPartyID, SecOwnerPartyID, DeedName1 as Own1FullName, DeedName2 as Own2FullName, RSLocStreet, RSLocNumAlph, RSMapLot, RSAccount AS Account FROM Master WHERE (RSLOCSTREET >= '" + txtSearch.Text + "' AND RSLOCSTREET < '" + txtSearch.Text + "ZZZ') ORDER BY RSLOCSTREET, RSLOCNUMALPH, RSAccount";
                            rs.OpenRecordset(SQL, modExtraModules.strREDatabase);
                        }
                        else
                        {
                            SQL = "SELECT PartyID, Street, StreetNumber, Account FROM PPMaster WHERE (STREET >= '" + txtSearch.Text + "' AND STREET < '" + txtSearch.Text + "ZZZ') ORDER BY STREET, CONVERT(int, STREETNUMBER), Account";
                            rs.OpenRecordset(SQL, modExtraModules.strREDatabase);
                            rs.InsertName("PartyID", "Own1", false, true, true, "", false, "", true, "");
                        }

                        strSearchString = SQL;

                        break;
                    }
                    case 2:
                    {
                        // Map Lot
                        rsCLAccounts.OpenRecordset("SELECT * FROM BillingMaster WHERE ID = 0", modExtraModules.strCLDatabase);

                        if (modStatusPayments.Statics.boolRE)
                        {
                            SQL = "SELECT OwnerPartyID, SecOwnerPartyID, DeedName1 as Own1FullName, DeedName2 as Own2FullName, RSLocStreet, RSLocNumAlph, RSMapLot, RSAccount AS Account FROM Master WHERE (RSMAPLOT >= '" + txtSearch.Text + "' AND RSMAPLOT < '" + txtSearch.Text + "ZZZ') ORDER BY RSMAPLOT, RSAccount";
                            rs.OpenRecordset(SQL, modExtraModules.strREDatabase);
                        }
                        else
                        {
                            // SQL = "SELECT * FROM Master WHERE (RSMAPLOT >= '" & txtSearch.Text & "' AND RSMAPLOT < '" & txtSearch.Text & "ZZZ') ORDER BY RSMAPLOT, Account"
                            return;
                        }

                        strSearchString = SQL;

                        break;
                    }
                }

                //end switch
                if (modStatusPayments.Statics.boolRE)
                {
                    // RE
                    // moved up   'rs.OpenRecordset SQL, strREDatabase
                    if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
                    {
                        frmWait.InstancePtr.Init("Please Wait...." + "\r\n" + "Searching", true, rs.RecordCount(), true);
                        this.Refresh();
                        rs.MoveLast();
                        rs.MoveFirst();

                        // clear the listbox
                        vsSearch.Rows = 1;
                        FormatSearchGrid();
                        rsBook.OpenRecordset("SELECT * FROM BookPage WHERE [Current] = 1 ORDER BY Line desc", modExtraModules.strREDatabase);

                        // fill the listbox with all the records returned
                        while (!rs.EndOfFile())
                        {
                            frmWait.InstancePtr.IncrementProgress();
                            App.DoEvents();

                            // get any book page info from
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            rsBook.FindFirstRecord2("Account,Card", FCConvert.ToString(rs.Get_Fields("Account")) + ",1", ",");

                            if (rsBook.NoMatch)
                            {
                                strBook = "";
                            }
                            else
                            {
                                strBook = FCConvert.ToString(rsBook.Get_Fields("Book")) + " " + FCConvert.ToString(rsBook.Get_Fields("Page"));
                            }

                            if (lngSelection == 0)
                            {
                                // is this from CL and RE
                                if (Conversion.Val(rs.Get_Fields("Account")) != Conversion.Val(Strings.Left(vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct), 6)))
                                {
                                    vsSearch.AddItem("");

                                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct, FCConvert.ToString(rs.Get_Fields("Account")));
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBookPage, strBook);
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocation, FCConvert.ToString(rs.Get_Fields_String("RSLOCSTREET")));
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngcolLocationNumber, Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("RSLOCNUMALPH"))));
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColMapLot, Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("RSMAPLOT"))));

                                    if ((Strings.CompareString(Strings.UCase(FCConvert.ToString(rs.Get_Fields("Own1FullName"))), ">=", Strings.UCase(txtSearch.Text) + "   ") && Strings.CompareString(Strings.UCase(FCConvert.ToString(rs.Get_Fields("Own1FullName"))), "<=", Strings.UCase(txtSearch.Text + "ZZZ"))) || lngSelection != 0)
                                    {
                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rs.Get_Fields("Own1FullName")) + " & " + FCConvert.ToString(rs.Get_Fields("Own2FullName")));
                                    }
                                    else
                                    {
                                        // TODO Get_Fields: Field [Own2FullName] not found!! (maybe it is an alias?)
                                        // TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rs.Get_Fields("Own2FullName")) + ", " + FCConvert.ToString(rs.Get_Fields("Own1FullName")));
                                    }
                                }
                            }
                            else
                            {
                                // data from RE
                                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                if (Conversion.Val(rs.Get_Fields("Account")) != Conversion.Val(Strings.Left(vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct), 6)))
                                {
                                    strList = FCConvert.ToString(rs.Get_Fields("Account")) + "\t" + FCConvert.ToString(rs.Get_Fields("Own1FullName")) + "\t" + Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("RSLOCNUMALPH"))) + "\t" + FCConvert.ToString(rs.Get_Fields_String("RSLOCSTREET")) + "\t" + strBook + "\t" + Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("RSMAPLOT")));
                                    vsSearch.AddItem("");

                                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct, FCConvert.ToString(rs.Get_Fields("Account")));
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBookPage, strBook);
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocation, FCConvert.ToString(rs.Get_Fields_String("RSLOCSTREET")));
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngcolLocationNumber, Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("RSLOCNUMALPH"))));
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColMapLot, Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("RSMAPLOT"))));

                                    if (lngSelection == 0)
                                    {
                                        if ((Strings.CompareString(Strings.UCase(FCConvert.ToString(rsCLAccounts.Get_Fields("Own1FullName"))), ">=", Strings.UCase(txtSearch.Text) + "   ") && Strings.CompareString(Strings.UCase(FCConvert.ToString(rsCLAccounts.Get_Fields("Own1FullName"))), "<=", Strings.UCase(txtSearch.Text + "ZZZ"))) || lngSelection != 0)
                                        {
                                            vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rsCLAccounts.Get_Fields("Own1FullName")) + " & " + FCConvert.ToString(rsCLAccounts.Get_Fields("Own2FullName")));
                                        }
                                        else
                                        {
                                            // TODO Get_Fields: Field [Own2FullName] not found!! (maybe it is an alias?)
                                            // TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
                                            vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rsCLAccounts.Get_Fields("Own2FullName")) + ", " + FCConvert.ToString(rsCLAccounts.Get_Fields("Own1FullName")));
                                        }
                                    }
                                    else
                                    {
                                        if ((Strings.CompareString(Strings.UCase(FCConvert.ToString(rs.Get_Fields("Own1FullName"))), ">=", Strings.UCase(txtSearch.Text) + "   ") && Strings.CompareString(Strings.UCase(FCConvert.ToString(rs.Get_Fields("Own1FullName"))), "<=", Strings.UCase(txtSearch.Text + "ZZZ"))) || lngSelection != 0)
                                        {
                                            vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rs.Get_Fields("Own1FullName")) + " & " + FCConvert.ToString(rs.Get_Fields("Own2FullName")));
                                        }
                                        else
                                        {
                                            vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rs.Get_Fields("Own2FullName")) + ", " + FCConvert.ToString(rs.Get_Fields("Own1FullName")));
                                        }
                                    }
                                }
                            }

                            rs.MoveNext();
                        }

                        // now check all of the billing records to see if any did not match the master record,
                        // if they did not, then add a row for that bill as well
                        rsCLAccounts.MoveFirst();

                        while (!rsCLAccounts.EndOfFile())
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            rs.FindFirstRecord("Account", rsCLAccounts.Get_Fields("Account"));

                            if (!rs.NoMatch)
                            {
                                // TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
                                if (Strings.Trim(Strings.UCase(FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")))) == Strings.Trim(Strings.UCase(FCConvert.ToString(rs.Get_Fields("Own1FullName")))) || Strings.Trim(Strings.UCase(FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")))) == Strings.Trim(Strings.UCase(strLastName)))
                                {
                                    // if this bill has the same name as its master account or has the
                                    // same name as the last bill then do nothing
                                }
                                else
                                {
                                    // add the row
                                    vsSearch.AddItem("");

                                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct, FCConvert.ToString(rsCLAccounts.Get_Fields("Account")));
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBookPage, FCConvert.ToString(rsCLAccounts.Get_Fields_String("BookPage")));
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocation, FCConvert.ToString(rsCLAccounts.Get_Fields_String("StreetName")));

                                    // TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngcolLocationNumber, FCConvert.ToString(rsCLAccounts.Get_Fields("StreetNumber")));
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColMapLot, Strings.Trim(FCConvert.ToString(rsCLAccounts.Get_Fields_String("MapLot"))));

                                    if ((Strings.CompareString(Strings.UCase(FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1"))), ">=", Strings.UCase(txtSearch.Text) + "   ") && Strings.CompareString(Strings.UCase(FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1"))), "<=", Strings.UCase(txtSearch.Text + "ZZZ"))) || lngSelection != 0)
                                    {
                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")) + " & " + FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name2")));
                                    }
                                    else
                                    {
                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name2")) + ", " + FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")));
                                    }

                                    // highlight the added rows that are only past owners
                                    vsSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsSearch.Rows - 1, 0, vsSearch.Rows - 1, vsSearch.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
                                    strLastName = FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1"));
                                }
                            }
                            else
                                if (Strings.Trim(Strings.UCase(FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")))) != Strings.Trim(Strings.UCase(strLastName)))
                                {
                                    // add the row
                                    vsSearch.AddItem("");

                                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct, FCConvert.ToString(rsCLAccounts.Get_Fields("Account")));
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBookPage, FCConvert.ToString(rsCLAccounts.Get_Fields_String("BookPage")));
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocation, FCConvert.ToString(rsCLAccounts.Get_Fields_String("StreetName")));

                                    // TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngcolLocationNumber, FCConvert.ToString(rsCLAccounts.Get_Fields("StreetNumber")));
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColMapLot, Strings.Trim(FCConvert.ToString(rsCLAccounts.Get_Fields_String("MapLot"))));

                                    if ((Strings.CompareString(Strings.UCase(FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1"))), ">=", Strings.UCase(txtSearch.Text) + "   ") && Strings.CompareString(Strings.UCase(FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1"))), "<=", Strings.UCase(txtSearch.Text + "ZZZ"))) || lngSelection != 0)
                                    {
                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")) + " & " + FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name2")));
                                    }
                                    else
                                    {
                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name2")) + ", " + FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")));
                                    }

                                    // highlight the added rows that are only past owners
                                    vsSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsSearch.Rows - 1, 0, vsSearch.Rows - 1, vsSearch.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
                                    strLastName = FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1"));
                                }

                            rsCLAccounts.MoveNext();
                        }

                        vsSearch.Visible = true;
                        fraSearch.Visible = false;
                        fraGetAccount.Visible = false;
                        fraSearchCriteria.Visible = false;
                        boolResultsScreen = true;
                        btnProcess.Enabled = false;
                        cmdProcessSearch.Enabled = false;

                        if (lngSelection == 0)
                        {
                            // force the name order
                            vsSearch.Col = lngColName;
                            vsSearch.Sort = FCGrid.SortSettings.flexSortGenericAscending;
                        }

                        frmWait.InstancePtr.Unload();
                    }
                    else
                    {
                        frmWait.InstancePtr.Unload();
                        FCMessageBox.Show("No results were matched to the criteria.  Please try again.", MsgBoxStyle.Information, "No RE Results");
                    }
                }
                else
                {
                    // PP
                    FormatSearchGrid();
                    rs.OpenRecordset(SQL, modExtraModules.strPPDatabase);

                    if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
                    {
                        frmWait.InstancePtr.Init("Please Wait...." + "\r\n" + "Searching", true, rs.RecordCount(), true);
                        this.Refresh();
                        rs.MoveLast();
                        rs.MoveFirst();

                        // clear the listbox
                        vsSearch.Rows = 1;

                        // fill the listbox with all the records returned
                        while (!rs.EndOfFile())
                        {
                            frmWait.InstancePtr.IncrementProgress();
                            App.DoEvents();

                            if (lngSelection == 0)
                            {
                                // is this from CL and PP
                                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                if (Conversion.Val(rs.Get_Fields("Account")) != Conversion.Val(Strings.Left(vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct), 6)))
                                {
                                    vsSearch.AddItem("");

                                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct, FCConvert.ToString(rs.Get_Fields("Account")));
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBookPage, "");

                                    // rsCLAccounts.Get_Fields("BookPage")
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocation, FCConvert.ToString(rs.Get_Fields_String("Street")));

                                    // TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngcolLocationNumber, FCConvert.ToString(rs.Get_Fields("StreetNumber")));
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColMapLot, "");

                                    // Trim(rsCLAccounts.Get_Fields("MapLot"))
                                    // TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rs.Get_Fields("Own1FullName")));
                                }
                            }

                            rs.MoveNext();
                        }

                        // now check all of the billing records to see if any did not match the master record,
                        // if they did not, then add a row for that bill as well
                        rsCLAccounts.MoveFirst();

                        while (!rsCLAccounts.EndOfFile())
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            rs.FindFirstRecord("Account", rsCLAccounts.Get_Fields("Account"));

                            if (!rs.NoMatch)
                            {
                                // TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
                                if (Strings.Trim(Strings.UCase(FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")))) == Strings.Trim(Strings.UCase(FCConvert.ToString(rs.Get_Fields("Own1FullName")))) || Strings.Trim(Strings.UCase(FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")))) == Strings.Trim(Strings.UCase(strLastName)))
                                {
                                    // if this bill has the same name as its master account or has the
                                    // same name as the last bill then do nothing
                                }
                                else
                                {
                                    // add the row
                                    vsSearch.AddItem("");

                                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct, FCConvert.ToString(rsCLAccounts.Get_Fields("Account")));
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBookPage, FCConvert.ToString(rsCLAccounts.Get_Fields_String("BookPage")));
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocation, FCConvert.ToString(rsCLAccounts.Get_Fields_String("StreetName")));

                                    // TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngcolLocationNumber, FCConvert.ToString(rsCLAccounts.Get_Fields("StreetNumber")));
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColMapLot, Strings.Trim(FCConvert.ToString(rsCLAccounts.Get_Fields_String("MapLot"))));
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")) + " " + FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name2")));

                                    // highlight the added rows that are only past owners
                                    vsSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsSearch.Rows - 1, 0, vsSearch.Rows - 1, vsSearch.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
                                }
                            }
                            else
                                if (Strings.Trim(Strings.UCase(FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")))) != Strings.Trim(Strings.UCase(strLastName)))
                                {
                                    // add the row
                                    vsSearch.AddItem("");

                                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct, FCConvert.ToString(rsCLAccounts.Get_Fields("Account")));
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBookPage, "");

                                    // rsCLAccounts.Get_Fields("BookPage")
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocation, FCConvert.ToString(rsCLAccounts.Get_Fields_String("StreetName")));

                                    // TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngcolLocationNumber, FCConvert.ToString(rsCLAccounts.Get_Fields("StreetNumber")));
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColMapLot, "");

                                    // Trim(rsCLAccounts.Get_Fields("MapLot"))
                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")) + " " + FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name2")));

                                    // highlight the added rows that are only past owners
                                    vsSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsSearch.Rows - 1, 0, vsSearch.Rows - 1, vsSearch.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
                                }

                            strLastName = FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1"));
                            rsCLAccounts.MoveNext();
                        }

                        // set the height of the grid
                        //FC:FINAL:AM: don't set the height; used anchoring instead
                        //if ((vsSearch.Rows * vsSearch.RowHeight(0)) + 70 < (this.Height - vsSearch.Top) - 1000)
                        //{
                        //	vsSearch.Height = (vsSearch.Rows * vsSearch.RowHeight(0)) + 70;
                        //	vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
                        //}
                        //else
                        //{
                        //	vsSearch.Height = (this.Height - vsSearch.Top) - 1000;
                        //	vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
                        //}
                        vsSearch.Visible = true;

                        //FC:FINAL:DDU #i274 hide search button
                        cmdProcessSearch.Enabled = false;
                        boolResultsScreen = true;
                        btnProcess.Enabled = false;
                        fraSearch.Visible = false;
                        frmWait.InstancePtr.Unload();
                    }
                    else
                    {
                        frmWait.InstancePtr.Unload();
                        FCMessageBox.Show("No results were matched to the criteria.  Please try again.", MsgBoxStyle.Information, "No PP Results");
                    }
                }

                return;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                frmWait.InstancePtr.Unload();
                FCMessageBox.Show("ERROR #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Open Recordset ERROR");
                Close();
            }
            finally
            {
                rs.DisposeOf();
                rsBook.DisposeOf();
                rsCLAccounts.DisposeOf();
            }
        }

		public void cmdSearch_Click()
		{
			cmdSearch_Click(cmdSearch, new System.EventArgs());
		}

		private void frmCLEditBillInfo_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (fraEditInfo.Visible)
			{
				if (KeyCode == Keys.C)
				{
					if (Shift == 2)
					{
						CopyInformation();
					}
				}
				else if (KeyCode == Keys.V)
				{
					if (Shift == 2)
					{
						PasteInformation();
					}
				}
			}
		}

		private void frmCLEditBillInfo_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii == 27)
			{
				if (vsSearch.Visible == true)
				{
					vsSearch.Visible = false;
					fraSearchCriteria.Visible = true;
					fraSearch.Visible = true;
					fraGetAccount.Visible = true;
					lblSearchListInstruction.Visible = false;
					boolSearch = false;
				}
				else if (fraEditInfo.Visible)
				{
					fraEditInfo.Visible = false;
					fraSearch.Visible = true;
					fraSearchCriteria.Visible = true;
					fraGetAccount.Visible = true;
					//cmdFileSave.Visible = false;
					cmdProcessSearch.Visible = true;
					btnProcess.Text = "Process";
					lblSearchListInstruction.Visible = false;
					boolSearch = false;
				}
				else
				{
					KeyAscii = 0;
					Close();
				}
			}
			else if (KeyAscii == 13)
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void frmCLEditBillInfo_Load(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strTemp = "";
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
				// this just sets the columns constant values
				// edit grid
				lngColEDBillkey = 0;
				lngColEDYear = 1;
				lngColEDName1 = 2;
				lngColEDName2 = 3;
				// search grid
				lngColAcct = 0;
				lngColName = 1;
				lngcolLocationNumber = 2;
				lngColLocation = 3;
				lngColBookPage = 4;
				lngColMapLot = 5;
				lngRowOwner = 0;
				lngRowSecondOwner = 1;
				lngRowAddress1 = 2;
				lngRowAddress2 = 3;
				lngRowMailingAddress3 = 4;
				lngRowAddress3 = 5;
				lngRowMapLot = 6;
				lngRowBookPage = 7;
				lngRowStreetNumber = 8;
				lngRowStreetName = 9;
				lngRowRef1 = 10;
				lngRowUseRef1OnLien = 11;
				lngRowInterestPaidDate = 12;

				strSearchString = "";
				if (modStatusPayments.Statics.boolRE)
				{
					if (cmbSearchType.Items[2].ToString() != auxCmbSearchTypeItem2)
					{
						cmbSearchType.Items.Insert(2, auxCmbSearchTypeItem2);
					}
				}
				else
				{
					cmbSearchType.Items.RemoveAt(2);
				}
				modGlobalFunctions.SetTRIOColors(this);

				// On Error GoTo ErrorTag
				// If FormExist(Me) Then Exit Sub
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				if (modStatusPayments.Statics.boolRE)
				{
					// set the menu options on for RE
					if (modExtraModules.IsThisCR())
					{
						if (modGlobal.Statics.gboolShowLastCLAccountInCR)
						{
							txtGetAccountNumber.Text = FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE);
						}
					}
					else
					{
						txtGetAccountNumber.Text = FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE);
					}
					cmbRE.SelectedIndex = 0;
				}
				else
				{
					if (modExtraModules.IsThisCR())
					{
						if (modGlobal.Statics.gboolShowLastCLAccountInCR)
						{
							txtGetAccountNumber.Text = FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP);
						}
					}
					else
					{
						txtGetAccountNumber.Text = FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP);
					}
					cmbRE.SelectedIndex = 1;
				}
				ShowSearch();
				txtGetAccountNumber.SelectionStart = 0;
				txtGetAccountNumber.SelectionLength = txtGetAccountNumber.Text.Length;
				txtGetAccountNumber.Focus();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				App.DoEvents();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Form Load Error");
			}
		}
		
		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			SaveInformation();
		}

		private void vsEditInfo_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			boolDirty = true;
		}

		private void vsEditInfo_GotFocus(object sender, EventArgs e)
		{
			if (vsEditInfo.Col > 0)
			{
				vsEditInfo.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsEditInfo.EditCell();
			}
			else
			{
				vsEditInfo.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			vsEditInfo.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
		}

		private void VsEditInfo_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            int lngMR;
            lngMR = vsEditInfo.GetFlexRowIndex(e.RowIndex);

            if (vsEditInfo.IsValidCell(e.ColumnIndex, e.RowIndex))
            {
                DataGridViewCell cell = vsEditInfo[e.ColumnIndex, e.RowIndex];

                if (lngMR == lngRowAddress1 || lngMR == lngRowAddress2 || lngMR == lngRowAddress3 || lngMR == lngRowMailingAddress3)
                {
                    cell.ToolTipText = "";
                }
                else if (lngMR == lngRowBookPage)
                {
                    cell.ToolTipText = "Book and Page";
                }
                else if (lngMR == lngRowMapLot)
                {
                    cell.ToolTipText = "Map and Lot";
                }
                else if (lngMR == lngRowOwner)
                {
                    cell.ToolTipText = "";
                }
                else if (lngMR == lngRowRef1)
                {
                    cell.ToolTipText = "Reference 1";
                }
                else if (lngMR == lngRowSecondOwner)
                {
                    cell.ToolTipText = "";
                }
                else if (lngMR == lngRowStreetName)
                {
                    cell.ToolTipText = "";
                }
                else if (lngMR == lngRowStreetNumber)
                {
                    cell.ToolTipText = "";
                }
                else if (lngMR == lngRowUseRef1OnLien)
                {
                    cell.ToolTipText = "Use the Ref 1 field instead of the Book and Page fields on 30 DN and Liens.";
                }
                else
                {
	                cell.ToolTipText = "";
                }
            }
        }

		private void vsEditInfo_RowColChange(object sender, EventArgs e)
		{
			if (vsEditInfo.Col > 0)
			{
				if (vsEditInfo.Row == lngRowUseRef1OnLien)
				{
					vsEditInfo.EditMask = "";
					vsEditInfo.ComboList = "1;Yes|2;No";
				}
				else if (vsEditInfo.Row == lngRowInterestPaidDate)
				{
					vsEditInfo.ComboList = "";
					vsEditInfo.EditMask = "##/##/####";
				}
				else
				{
					vsEditInfo.EditMask = "";
					vsEditInfo.ComboList = "";
				}
				vsEditInfo.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsEditInfo.EditCell();
			}
			else
			{
				vsEditInfo.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			if (vsEditInfo.Row == vsEditInfo.Rows - 1)
			{
				vsEditInfo.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				vsEditInfo.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void vsSearch_Click(object sender, EventArgs e)
		{
			txtGetAccountNumber.Text = Strings.Left(vsSearch.TextMatrix(vsSearch.Row, lngColAcct), 6);
		}

		private void vsSearch_DblClick(object sender, EventArgs e)
		{
			if (vsSearch.Row > 0)
			{
				txtGetAccountNumber.Text = Strings.Left(vsSearch.TextMatrix(vsSearch.Row, lngColAcct), 6);
				cmdGetAccountNumber_Click();
			}
		}

		private void vsSearch_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			// captures the return key to accept the account highlighted in the listbox
			if (e.KeyCode == Keys.Return)
			{
				cmdGetAccountNumber_Click();
			}
		}

		private void mnuProcessClearSearch_Click(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}

		private void mnuProcessGetAccount_Click(object sender, System.EventArgs e)
		{
			if (fraEditInfo.Visible)
			{
				// this is a save and exit
				SaveInformation();
				Close();
			}
			else
			{
				if (Strings.Trim(txtSearch.Text) != "")
				{
					cmdSearch_Click();
				}
				else
				{
					cmdGetAccountNumber_Click();
				}
			}
		}

		private void mnuProcessSearch_Click(object sender, System.EventArgs e)
		{
			if (fraGetAccount.Visible)
			{
				cmdSearch_Click();
			}
			else
			{
				// go back to that screen
				if (boolDirty)
				{
					DialogResult messageBoxResult = FCMessageBox.Show("Changes have been made.  Would you like to save the information?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Save Changes");
					if (messageBoxResult == DialogResult.Yes)
					{
						SaveInformation();
					}
					else if (messageBoxResult == DialogResult.No)
					{
						// do nothing
					}
					else if (messageBoxResult == DialogResult.Cancel)
					{
						return;
					}
				}
				btnProcess.Text = "Process";
				cmdProcessClearSearch.Visible = true;
				fraSearch.Visible = true;
				fraGetAccount.Visible = true;
				fraSearchCriteria.Visible = true;
				fraEditInfo.Visible = false;
			}
		}

		private void optPP_Click(object sender, System.EventArgs e)
		{
			if (modExtraModules.IsThisCR() && !modGlobal.Statics.gboolShowLastCLAccountInCR)
			{
			}
			else
			{
				txtGetAccountNumber.Text = FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP);
			}
		}

		private void optRE_Click(object sender, System.EventArgs e)
		{
			if (modExtraModules.IsThisCR() && !modGlobal.Statics.gboolShowLastCLAccountInCR)
			{
			}
			else
			{
				txtGetAccountNumber.Text = FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE);
			}
		}

		private void txtGetAccountNumber_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Return)
			{
				KeyCode = 0;
				cmdGetAccountNumber_Click();
			}
		}

		private void txtGetAccountNumber_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii < 48 || KeyAscii > 57)
			{
				if (KeyAscii == 8)
				{
				}
				else
				{
					KeyAscii = 0;
				}
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtSearch_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Return)
			{
				KeyCode = 0;
				cmdSearch_Click();
			}
		}

		private void ShowSearch()
		{
			// this will align the Search Screen and Show it
			boolSearch = true;
			//fraSearch.Left = FCConvert.ToInt32((this.Width - fraSearch.Width) / 2.0);
			//fraSearch.Top = FCConvert.ToInt32((this.Height - fraSearch.Height) / 2.0);
			fraSearch.Visible = true;
		}

		private void vsSearch_MouseMove(object sender, DataGridViewCellMouseEventArgs e)
		{
			int lngR;
			int lngC;
			lngC = vsSearch.MouseCol;
			lngR = vsSearch.MouseRow;
			// vssearch.ToolTipText =
		}

		private void vsSearch_RowColChange(object sender, EventArgs e)
		{
			if (Conversion.Val(Strings.Left(vsSearch.TextMatrix(vsSearch.Row, lngColAcct), 6)) != 0)
			{
				txtGetAccountNumber.Text = Strings.Left(vsSearch.TextMatrix(vsSearch.Row, lngColAcct), 6);
			}
		}

		private void FormatSearchGrid()
		{
			int lngWid;
			vsSearch.Cols = 6;
			// set the height of the grid
			vsSearch.Height = FCConvert.ToInt32(this.Height * 0.72);
			vsSearch.Width = FCConvert.ToInt32(this.Width * 0.9);
			vsSearch.Left = FCConvert.ToInt32(((this.Width - vsSearch.Width) / 2.0));
			vsSearch.Top = FCConvert.ToInt32(this.Height * 0.05);
			vsSearch.ExtendLastCol = true;
			lngWid = vsSearch.WidthOriginal;
			// set the column widths
			if (modStatusPayments.Statics.boolRE)
			{
				vsSearch.ColWidth(lngColAcct, FCConvert.ToInt32(lngWid * 0.09));
				vsSearch.ColWidth(lngColName, FCConvert.ToInt32(lngWid * 0.32));
				vsSearch.ColWidth(lngcolLocationNumber, FCConvert.ToInt32(lngWid * 0.06));
				vsSearch.ColWidth(lngColLocation, FCConvert.ToInt32(lngWid * 0.31));
				vsSearch.ColWidth(lngColBookPage, 0);
				vsSearch.ColWidth(lngColMapLot, FCConvert.ToInt32(lngWid * 0.18));
			}
			else
			{
				vsSearch.ColWidth(lngColAcct, FCConvert.ToInt32(lngWid * 0.09));
				vsSearch.ColWidth(lngColName, FCConvert.ToInt32(lngWid * 0.4));
				vsSearch.ColWidth(lngcolLocationNumber, FCConvert.ToInt32(lngWid * 0.06));
				vsSearch.ColWidth(lngColLocation, FCConvert.ToInt32(lngWid * 0.43));
				vsSearch.ColWidth(lngColBookPage, 0);
				vsSearch.ColWidth(lngColMapLot, 0);
			}
			vsSearch.ColAlignment(lngColAcct, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSearch.ColAlignment(lngColName, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSearch.ColAlignment(lngcolLocationNumber, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSearch.ColAlignment(lngColLocation, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSearch.ColAlignment(lngColBookPage, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSearch.ColAlignment(lngColMapLot, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSearch.TextMatrix(0, lngColAcct, "Account");
			vsSearch.TextMatrix(0, lngColName, "Name");
			vsSearch.TextMatrix(0, lngcolLocationNumber, "#");
			vsSearch.TextMatrix(0, lngColLocation, "Street");
			vsSearch.TextMatrix(0, lngColBookPage, "Book/Page");
			vsSearch.TextMatrix(0, lngColMapLot, "Map/Lot");
			lblSearchListInstruction.Text = "Select an account by double clicking or pressing 'Enter' while the account is highlighted.";
			lblSearchListInstruction.Top = 0;
			lblSearchListInstruction.Width = lngWid;
			lblSearchListInstruction.Left = vsSearch.Left;
			lblSearchListInstruction.Visible = true;
		}

		private void ShowEditAccount_2(int lngShowAccount)
		{
			ShowEditAccount(ref lngShowAccount);
		}

		private void ShowEditAccount(ref int lngShowAccount)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this routine will show the years that this account has been billed
				// and let the user edit the name and address information
				clsDRWrapper rsAcct = new clsDRWrapper();
				vsYearInfo.Rows = 1;
				//cmdFileSave.Visible = true;
				btnProcess.Text = "Save";
				cmdProcessClearSearch.Visible = false;
				FormatGrids();
				if (modStatusPayments.Statics.boolRE)
				{
					rsAcct.OpenRecordset("SELECT BillingMaster.ID, BillingYear, Name1, Name2 FROM BillingMaster INNER JOIN " + modGlobal.Statics.strDbRE + "Master ON BillingMaster.Account = Master.RSAccount WHERE RSCard = 1 AND BillingMaster.Account = " + FCConvert.ToString(lngShowAccount) + " AND BillingType = 'RE' ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
				}
				else
				{
					rsAcct.OpenRecordset("SELECT BillingMaster.ID, BillingYear, Name1, Name2 FROM BillingMaster INNER JOIN " + modGlobal.Statics.strDbPP + "PPMaster ON BillingMaster.Account = PPMaster.Account WHERE BillingMaster.Account = " + FCConvert.ToString(lngShowAccount) + " AND BillingType = 'PP' ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
				}
				if (!rsAcct.EndOfFile())
				{
					while (!rsAcct.EndOfFile())
					{
						vsYearInfo.AddItem("");
						vsYearInfo.TextMatrix(vsYearInfo.Rows - 1, lngColEDBillkey, FCConvert.ToString(rsAcct.Get_Fields_Int32("ID")));
						vsYearInfo.TextMatrix(vsYearInfo.Rows - 1, lngColEDYear, modExtraModules.FormatYear(FCConvert.ToString(rsAcct.Get_Fields_Int32("BillingYear"))));
						vsYearInfo.TextMatrix(vsYearInfo.Rows - 1, lngColEDName1, FCConvert.ToString(rsAcct.Get_Fields_String("Name1")));
						vsYearInfo.TextMatrix(vsYearInfo.Rows - 1, lngColEDName2, FCConvert.ToString(rsAcct.Get_Fields_String("Name2")));
						rsAcct.MoveNext();
					}
					vsYearInfo.Select(1, 0, 1, vsYearInfo.Cols - 1);
					lngLastRow = vsYearInfo.Row;
					EditBillInformation(FCConvert.ToInt32(vsYearInfo.TextMatrix(vsYearInfo.Row, lngColEDBillkey)));
					fraEditInfo.Visible = true;
					fraSearch.Visible = false;
					vsSearch.Visible = false;
					fraEditInfo.Text = "Edit Account " + FCConvert.ToString(lngShowAccount) + " Information";
				}
				else
				{
					FCMessageBox.Show("Cannot load account #" + FCConvert.ToString(lngShowAccount) + ".", MsgBoxStyle.Exclamation, "Missing Account");
					return;
				}
				AdjustGridHeight();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - ", MsgBoxStyle.Critical, "Error Showing Account");
			}
		}

		private void FormatGrids()
		{
			// this will format the two grids vsYearInfo and vsEditinfo
			int wid = 0;
			wid = vsYearInfo.WidthOriginal;
			vsYearInfo.ExtendLastCol = true;
			vsYearInfo.ColWidth(lngColEDBillkey, 0);
			vsYearInfo.ColWidth(lngColEDYear, FCConvert.ToInt32(wid * 0.15));
			vsYearInfo.ColWidth(lngColEDName1, FCConvert.ToInt32(wid * 0.4));
			vsYearInfo.ColWidth(lngColEDName2, FCConvert.ToInt32(wid * 0.4));
			vsYearInfo.TextMatrix(0, lngColEDYear, "Year");
			vsYearInfo.TextMatrix(0, lngColEDName1, "Owner");
			vsYearInfo.TextMatrix(0, lngColEDName2, "Second Owner");
			vsEditInfo.Cols = 2;
			vsEditInfo.Rows = 13;
			vsEditInfo.ColWidth(lngColEDBillkey, FCConvert.ToInt32(wid * 0.315));
			vsEditInfo.ColWidth(lngColEDYear, FCConvert.ToInt32(wid * 0.68));
			vsEditInfo.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsEditInfo.TextMatrix(lngRowOwner, 0, "Owner");
			vsEditInfo.TextMatrix(lngRowSecondOwner, 0, "Second Owner");
			vsEditInfo.TextMatrix(lngRowAddress1, 0, "Address 1");
			vsEditInfo.TextMatrix(lngRowAddress2, 0, "Address 2");
			vsEditInfo.TextMatrix(lngRowAddress3, 0, "City St Zip");
			vsEditInfo.TextMatrix(lngRowMailingAddress3, 0, "Address 3");
			vsEditInfo.TextMatrix(lngRowMapLot, 0, "Map Lot");
			vsEditInfo.TextMatrix(lngRowBookPage, 0, "Book Page");
			vsEditInfo.TextMatrix(lngRowStreetNumber, 0, "Street Number");
			vsEditInfo.TextMatrix(lngRowStreetName, 0, "Street Name");
			vsEditInfo.TextMatrix(lngRowRef1, 0, "Ref 1");
			vsEditInfo.TextMatrix(lngRowUseRef1OnLien, 0, "Use Ref 1 on Liens");
			vsEditInfo.TextMatrix(lngRowInterestPaidDate, 0, "Interest Paid Date");
		}
		// VBto upgrade warning: lngBK As int	OnWrite(string)
		private void EditBillInformation(int lngBK)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will show the information for the bill that has been selected
				clsDRWrapper rsAcct = new clsDRWrapper();
				rsAcct.OpenRecordset("SELECT * FROM BillingMaster INNER JOIN " + modGlobal.Statics.strDbRE + "Master ON BillingMaster.Account = Master.RSAccount WHERE RSCard = 1 AND BillingMaster.ID = " + FCConvert.ToString(lngBK) + " ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
				if (!rsAcct.EndOfFile())
				{
					vsEditInfo.TextMatrix(lngRowOwner, 1, FCConvert.ToString(rsAcct.Get_Fields_String("Name1")));
					vsEditInfo.TextMatrix(lngRowSecondOwner, 1, FCConvert.ToString(rsAcct.Get_Fields_String("Name2")));
					vsEditInfo.TextMatrix(lngRowAddress1, 1, FCConvert.ToString(rsAcct.Get_Fields_String("Address1")));
					vsEditInfo.TextMatrix(lngRowAddress2, 1, FCConvert.ToString(rsAcct.Get_Fields_String("Address2")));
					vsEditInfo.TextMatrix(lngRowAddress3, 1, FCConvert.ToString(rsAcct.Get_Fields_String("Address3")));
					vsEditInfo.TextMatrix(lngRowMailingAddress3, 1, FCConvert.ToString(rsAcct.Get_Fields_String("MailingAddress3")));
					vsEditInfo.TextMatrix(lngRowMapLot, 1, FCConvert.ToString(rsAcct.Get_Fields_String("MapLot")));
					vsEditInfo.TextMatrix(lngRowBookPage, 1, FCConvert.ToString(rsAcct.Get_Fields_String("BookPage")));
					// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
					vsEditInfo.TextMatrix(lngRowStreetNumber, 1, FCConvert.ToString(rsAcct.Get_Fields("StreetNumber")));
					vsEditInfo.TextMatrix(lngRowStreetName, 1, FCConvert.ToString(rsAcct.Get_Fields_String("StreetName")));
					vsEditInfo.TextMatrix(lngRowRef1, 1, FCConvert.ToString(rsAcct.Get_Fields_String("Ref1")));
					if (FCConvert.ToBoolean(rsAcct.Get_Fields_Boolean("ShowRef1")))
					{
						vsEditInfo.TextMatrix(lngRowUseRef1OnLien, 1, "Yes");
					}
					else
					{
						vsEditInfo.TextMatrix(lngRowUseRef1OnLien, 1, "No");
					}

					vsEditInfo.TextMatrix(lngRowInterestPaidDate, 1,
						rsAcct.Get_Fields_DateTime("InterestAppliedThroughDate").FormatAndPadShortDate());
				}
				else
				{
					vsEditInfo.TextMatrix(lngRowOwner, 1, "");
					vsEditInfo.TextMatrix(lngRowSecondOwner, 1, "");
					vsEditInfo.TextMatrix(lngRowAddress1, 1, "");
					vsEditInfo.TextMatrix(lngRowAddress2, 1, "");
					vsEditInfo.TextMatrix(lngRowMailingAddress3, 1, "");
					vsEditInfo.TextMatrix(lngRowAddress3, 1, "");
					vsEditInfo.TextMatrix(lngRowMapLot, 1, "");
					vsEditInfo.TextMatrix(lngRowBookPage, 1, "");
					vsEditInfo.TextMatrix(lngRowStreetNumber, 1, "");
					vsEditInfo.TextMatrix(lngRowStreetName, 1, "");
					vsEditInfo.TextMatrix(lngRowRef1, 1, "");
					vsEditInfo.TextMatrix(lngRowUseRef1OnLien, 1, "");
					vsEditInfo.TextMatrix(lngRowInterestPaidDate, 1, "");
				}
				boolDirty = false;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Editing Bill Information");
			}
		}

		private void vsYearInfo_GotFocus()
		{
			vsYearInfo.Row = 1;
			vsYearInfo.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
		}

		private void vsYearInfo_RowColChange(object sender, EventArgs e)
		{
			if (vsYearInfo.Row > 0 && lngLastRow != vsYearInfo.Row)
			{
				if (boolDirty)
				{
					DialogResult messageBoxResult = FCMessageBox.Show("Changes have been made.  Would you like to save the information?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Save Changes");
					if (messageBoxResult == DialogResult.Yes)
					{
						SaveInformation();
						EditBillInformation(FCConvert.ToInt32(vsYearInfo.TextMatrix(vsYearInfo.Row, lngColEDBillkey)));
					}
					else if (messageBoxResult == DialogResult.No)
					{
						EditBillInformation(FCConvert.ToInt32(vsYearInfo.TextMatrix(vsYearInfo.Row, lngColEDBillkey)));
					}
					else if (messageBoxResult == DialogResult.Cancel)
					{
						// do nothing
					}
				}
				else
				{
					EditBillInformation(FCConvert.ToInt32(vsYearInfo.TextMatrix(vsYearInfo.Row, lngColEDBillkey)));
				}
				lngLastRow = vsYearInfo.Row;
			}
			if (vsYearInfo.Row == vsYearInfo.Rows - 1)
			{
				vsYearInfo.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				vsYearInfo.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void SaveInformation()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strName1 = "";
				string strName2 = "";
				string strAddr1 = "";
				string strAddr2 = "";
				string strAddr3 = "";
				string strMailingAddr3 = "";
				string strMapLot = "";
				string strBookPage = "";
				int lngStreetNumber = 0;
				string strStreetName = "";
				string strRef1 = "";
				bool boolUseRef1 = false;
				string interestAppliedDate = "";

				clsDRWrapper rsAcct = new clsDRWrapper();
				vsEditInfo.Select(0, 0);
				// use lngLastRow to get the from from the grid, this is the last row selected and is the way that I will find the billkey
				if (lngLastRow > 0)
				{
					rsAcct.OpenRecordset("SELECT * FROM BillingMaster WHERE ID = " + vsYearInfo.TextMatrix(lngLastRow, lngColEDBillkey), modExtraModules.strCLDatabase);
					if (!rsAcct.EndOfFile())
					{
						// this will save the information into the correct bill
						strName1 = Strings.Trim(vsEditInfo.TextMatrix(lngRowOwner, 1));
						strName2 = Strings.Trim(vsEditInfo.TextMatrix(lngRowSecondOwner, 1));
						strAddr1 = Strings.Trim(vsEditInfo.TextMatrix(lngRowAddress1, 1));
						strAddr2 = Strings.Trim(vsEditInfo.TextMatrix(lngRowAddress2, 1));
						strAddr3 = Strings.Trim(vsEditInfo.TextMatrix(lngRowAddress3, 1));
						strMailingAddr3 = Strings.Trim(vsEditInfo.TextMatrix(lngRowMailingAddress3, 1));
						strMapLot = Strings.Trim(vsEditInfo.TextMatrix(lngRowMapLot, 1));
						strBookPage = Strings.Trim(vsEditInfo.TextMatrix(lngRowBookPage, 1));
						lngStreetNumber = FCConvert.ToInt32(Math.Round(Conversion.Val("0" + Strings.Trim(vsEditInfo.TextMatrix(lngRowStreetNumber, 1)))));
						strStreetName = Strings.Trim(vsEditInfo.TextMatrix(lngRowStreetName, 1));
						strRef1 = Strings.Trim(vsEditInfo.TextMatrix(lngRowRef1, 1));
						boolUseRef1 = FCConvert.CBool(Strings.Left(vsEditInfo.TextMatrix(lngRowUseRef1OnLien, 1), 1) == "Y");
						interestAppliedDate = vsEditInfo.TextMatrix(lngRowInterestPaidDate, 1);

						rsAcct.Edit();
						rsAcct.Set_Fields("Name1", strName1);
						rsAcct.Set_Fields("Name2", strName2);
						rsAcct.Set_Fields("Address1", strAddr1);
						rsAcct.Set_Fields("Address2", strAddr2);
						rsAcct.Set_Fields("Address3", strAddr3);
						rsAcct.Set_Fields("MailingAddress3", strMailingAddr3);
						rsAcct.Set_Fields("MapLot", strMapLot);
						rsAcct.Set_Fields("BookPage", strBookPage);
						rsAcct.Set_Fields("StreetNumber", lngStreetNumber);
						rsAcct.Set_Fields("StreetName", strStreetName);
						rsAcct.Set_Fields("Ref1", strRef1);
						rsAcct.Set_Fields("ShowRef1", boolUseRef1);
						if (Information.IsDate(interestAppliedDate))
						{
							if (interestAppliedDate == "12/30/1899")
							{
								rsAcct.Set_Fields("InterestAppliedThroughDate", null);
							}
							else
							{
								rsAcct.Set_Fields("InterestAppliedThroughDate", interestAppliedDate);
							}
							rsAcct.Set_Fields("InterestAppliedThroughDate", interestAppliedDate);
						}
						else
						{
							rsAcct.Set_Fields("InterestAppliedThroughDate", null);
						}
							
						rsAcct.Update();
						vsYearInfo.TextMatrix(lngLastRow, lngColEDName1, strName1);
						vsYearInfo.TextMatrix(lngLastRow, lngColEDName2, strName2);
						modGlobalFunctions.AddCYAEntry_26("CL", "Editing Bill information.", "Billkey = " + vsYearInfo.TextMatrix(lngLastRow, lngColEDBillkey));
					}
					boolDirty = false;
				}
				FCMessageBox.Show("Save Successful.", MsgBoxStyle.Exclamation, "Success");
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Saving Bill");
			}
		}

		private void CopyInformation(int lngRow = -1)
		{
			// this will copy the information into the virtual clipboard from the grid
			// Select Case lngRow
			// Case -1
			strCopyName1 = Strings.Trim(vsEditInfo.TextMatrix(lngRowOwner, 1));
			strCopyName2 = Strings.Trim(vsEditInfo.TextMatrix(lngRowSecondOwner, 1));
			strCopyAddr1 = Strings.Trim(vsEditInfo.TextMatrix(lngRowAddress1, 1));
			strCopyAddr2 = Strings.Trim(vsEditInfo.TextMatrix(lngRowAddress2, 1));
			strCopyAddr3 = Strings.Trim(vsEditInfo.TextMatrix(lngRowAddress3, 1));
			strCopyMailingAddr3 = Strings.Trim(vsEditInfo.TextMatrix(lngRowMailingAddress3, 1));
			strCopyMapLot = Strings.Trim(vsEditInfo.TextMatrix(lngRowMapLot, 1));
			strCopyBookPage = Strings.Trim(vsEditInfo.TextMatrix(lngRowBookPage, 1));
			strCopyStreetNumber = FCConvert.ToString(Conversion.Val("0" + Strings.Trim(vsEditInfo.TextMatrix(lngRowStreetNumber, 1))));
			strCopyStreetName = Strings.Trim(vsEditInfo.TextMatrix(lngRowStreetName, 1));
			strCopyRef1 = Strings.Trim(vsEditInfo.TextMatrix(lngRowRef1, 1));
			boolCopyUseRef1 = FCConvert.CBool(Strings.Left(vsEditInfo.TextMatrix(lngRowUseRef1OnLien, 1), 1) == "Y");
			strCopyInterestPaidDate = Strings.Trim(vsEditInfo.TextMatrix(lngRowInterestPaidDate, 1));
			// Case 0
			// strCopyName1 = Trim(vsEditInfo.EditSelText)
			// Case 1
			// strCopyName2 = Trim(vsEditInfo.EditSelText)
			// Case 2
			// strCopyAddr1 = Trim(vsEditInfo.EditSelText)
			// Case 3
			// strCopyAddr2 = Trim(vsEditInfo.EditSelText)
			// Case 4
			// strCopyAddr3 = Trim(vsEditInfo.EditSelText)
			// End Select
		}

		private void PasteInformation()
		{
			// this will paste the information into the grid from the virtual clipboard
			if (strCopyName1 != "" && (strCopyName2 != "" || strCopyAddr1 != "" || strCopyAddr2 != "" || strCopyAddr3 != ""))
			{
				vsEditInfo.TextMatrix(lngRowOwner, 1, strCopyName1);
				vsEditInfo.TextMatrix(lngRowSecondOwner, 1, strCopyName2);
				vsEditInfo.TextMatrix(lngRowAddress1, 1, strCopyAddr1);
				vsEditInfo.TextMatrix(lngRowAddress2, 1, strCopyAddr2);
				vsEditInfo.TextMatrix(lngRowAddress3, 1, strCopyAddr3);
				vsEditInfo.TextMatrix(lngRowMailingAddress3, 1, strCopyMailingAddr3);
				vsEditInfo.TextMatrix(lngRowMapLot, 1, strCopyMapLot);
				vsEditInfo.TextMatrix(lngRowBookPage, 1, strCopyBookPage);
				vsEditInfo.TextMatrix(lngRowStreetNumber, 1, strCopyStreetNumber);
				vsEditInfo.TextMatrix(lngRowStreetName, 1, strCopyStreetName);
				vsEditInfo.TextMatrix(lngRowRef1, 1, strCopyRef1);
				if (boolCopyUseRef1)
				{
					vsEditInfo.TextMatrix(lngRowUseRef1OnLien, 1, "Yes");
				}
				else
				{
					vsEditInfo.TextMatrix(lngRowUseRef1OnLien, 1, "No");
				}
				vsEditInfo.TextMatrix(lngRowInterestPaidDate, 1, strCopyInterestPaidDate);
			}
		}

		private void AdjustGridHeight()
		{
			//FC:FINAL:AM: don't set the height; used anchoring instead
			//vsEditInfo.HeightOriginal = FCConvert.ToInt32((vsEditInfo.Rows * vsEditInfo.RowHeight(0)) + (vsEditInfo.RowHeight(0) * 0.15));
			//vsYearInfo.HeightOriginal = FCConvert.ToInt32((vsYearInfo.Rows * vsYearInfo.RowHeight(0)) + (vsYearInfo.RowHeight(0) * 0.15));
			//FC:FINAL:AM: adjust the location of the detail grid
			vsEditInfo.Top = vsYearInfo.Bottom + 30;
            //TODO: check if this height adjustment needs to be converted
            //if (((vsYearInfo.Rows * vsYearInfo.RowHeight(0)) + 70 + vsYearInfo.TopOriginal) < (vsEditInfo.TopOriginal - 500))
            //{
            //    vsYearInfo.Text.HeightOriginal = ((vsYearInfo.Rows * vsYearInfo.RowHeight(0)) + 70);
            //    // vsyearinfo.ScrollBars = flexScrollBarNone
            //}
            //else
            //{
            //    vsYearInfo.Text.HeightOriginal = (vsEditInfo.TopOriginal - 500 - vsYearInfo.TopOriginal);
            //    vsYearInfo.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
            //}
        }

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuProcessGetAccount_Click(sender, e);
		}

		private void fcButton1_Click(object sender, EventArgs e)
		{
			this.mnuProcessSearch_Click(sender, e);
		}

		private void cmdProcessClearSearch_Click(object sender, EventArgs e)
		{
			this.mnuProcessClearSearch_Click(sender, e);
		}

		private void cmdFileSave_Click(object sender, EventArgs e)
		{
			this.mnuFileSave_Click(sender, e);
		}
	}
}
