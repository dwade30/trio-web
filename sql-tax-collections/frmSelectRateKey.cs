﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmSelectRateKey.
	/// </summary>
	public partial class frmSelectRateKey : BaseForm
	{
		public frmSelectRateKey()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmSelectRateKey InstancePtr
		{
			get
			{
				return (frmSelectRateKey)Sys.GetInstance(typeof(frmSelectRateKey));
			}
		}

		protected frmSelectRateKey _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               02/25/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               02/25/2004              *
		// ********************************************************
		int lngColCheck;
		int lngColRateKey;
		int lngColDescription;
		int lngColPeriods;
		int lngColHidden;
		int lngColDue1;
		int lngColDue2;
		int lngColDue3;
		int lngColDue4;
		int lngFinalRK;

		public int Init(ref clsDRWrapper rsRK, string strMessage = "")
		{
			int Init = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will accept a recordset with rate keys
				// if there are no ratekeys then it will return a 0
				// if there is only one key then it will return that number
				// if there are multiple rate keys then the user will select
				// on and the function will return the number selected
				// if there is an error then return a negative
				bool boolFound/*unused?*/;
				int intCT/*unused?*/;
				if (Strings.Trim(strMessage + " ") != "")
				{
					HeaderText.Text = strMessage;
				}
				Init = 0;
				if (rsRK.RecordCount() == 0)
				{
					Init = 0;
				}
				else
				{
					if (rsRK.RecordCount() == 1)
					{
						Init = rsRK.Get_Fields_Int32("ID");
					}
					else
					{
						//FC:FINAL:BCU #i169 - move on Init otherwise it`s too late
						modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
						modGlobalFunctions.SetTRIOColors(this);
						// these are the column numbers
						lngColCheck = 0;
						lngColRateKey = 1;
						lngColDescription = 2;
						lngColPeriods = 3;
						lngColHidden = 4;
						lngColDue1 = 5;
						lngColDue2 = 6;
						lngColDue3 = 7;
						lngColDue4 = 8;
						FormatGrid();
						FillGrid(ref rsRK);
						this.Show(FormShowEnum.Modal);
					}
					Init = lngFinalRK;
				}
				return Init;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				Init = -1;
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Init Error");
			}
			return Init;
		}

		private void frmSelectRateKey_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
           TWSharedLibrary.FormUtilities.KeyPressHandler(e, this);
        }


        private void frmSelectRateKey_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSelectRateKey.Icon	= "frmSelectRateKey.frx":0000";
			//frmSelectRateKey.FillStyle	= 0;
			//frmSelectRateKey.ScaleWidth	= 5880;
			//frmSelectRateKey.ScaleHeight	= 4320;
			//frmSelectRateKey.LinkTopic	= "Form2";
			//frmSelectRateKey.LockControls	= -1  'True;
			//frmSelectRateKey.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "9.75";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//vsRate.BackColor	= "-2147483643";
			//			//vsRate.ForeColor	= "-2147483640";
			//vsRate.Bindings	= "frmSelectRateKey.frx":058A";
			//vsRate.BorderStyle	= 1;
			//vsRate.FillStyle	= 0;
			//vsRate.Appearance	= 1;
			//vsRate.GridLines	= 1;
			//vsRate.WordWrap	= 0;
			//vsRate.ScrollBars	= 3;
			//vsRate.RightToLeft	= 0;
			//vsRate._cx	= 9234;
			//vsRate._cy	= 5773;
			//vsRate._ConvInfo	= 1;
			//vsRate.MousePointer	= 0;
			//vsRate.BackColorFixed	= -2147483633;
			//			//vsRate.ForeColorFixed	= -2147483630;
			//vsRate.BackColorSel	= -2147483635;
			//			//vsRate.ForeColorSel	= -2147483634;
			//vsRate.BackColorBkg	= -2147483636;
			//vsRate.BackColorAlternate	= -2147483643;
			//vsRate.GridColor	= -2147483633;
			//vsRate.GridColorFixed	= -2147483632;
			//vsRate.TreeColor	= -2147483632;
			//vsRate.FloodColor	= 192;
			//vsRate.SheetBorder	= -2147483642;
			//vsRate.FocusRect	= 1;
			//vsRate.HighLight	= 1;
			//vsRate.AllowSelection	= -1  'True;
			//vsRate.AllowBigSelection	= -1  'True;
			//vsRate.AllowUserResizing	= 0;
			//vsRate.SelectionMode	= 0;
			//vsRate.GridLinesFixed	= 2;
			//vsRate.GridLineWidth	= 1;
			//vsRate.RowHeightMin	= 0;
			//vsRate.RowHeightMax	= 0;
			//vsRate.ColWidthMin	= 0;
			//vsRate.ColWidthMax	= 0;
			//vsRate.ExtendLastCol	= 0   'False;
			//vsRate.FormatString	= "";
			//vsRate.ScrollTrack	= 0   'False;
			//vsRate.ScrollTips	= 0   'False;
			//vsRate.MergeCells	= 0;
			//vsRate.MergeCompare	= 0;
			//vsRate.AutoResize	= -1  'True;
			//vsRate.AutoSizeMode	= 0;
			//vsRate.AutoSearch	= 0;
			//vsRate.AutoSearchDelay	= 2;
			//vsRate.MultiTotals	= -1  'True;
			//vsRate.SubtotalPosition	= 1;
			//vsRate.OutlineBar	= 0;
			//vsRate.OutlineCol	= 0;
			//vsRate.Ellipsis	= 0;
			//vsRate.ExplorerBar	= 0;
			//vsRate.PicturesOver	= 0   'False;
			//vsRate.PictureType	= 0;
			//vsRate.TabBehavior	= 0;
			//vsRate.OwnerDraw	= 0;
			//vsRate.ShowComboButton	= -1  'True;
			//vsRate.TextStyle	= 0;
			//vsRate.TextStyleFixed	= 0;
			//vsRate.OleDragMode	= 0;
			//vsRate.OleDropMode	= 0;
			//vsRate.DataMode	= 0;
			//vsRate.VirtualData	= -1  'True;
			//vsRate.ComboSearch	= 3;
			//vsRate.AutoSizeMouse	= -1  'True;
			//vsRate.AllowUserFreezing	= 0;
			//vsRate.BackColorFrozen	= 0;
			//			//vsRate.ForeColorFrozen	= 0;
			//vsRate.WallPaperAlignment	= 9;
			//vsElasticLight1.OleObjectBlob	= "frmSelectRateKey.frx":059A";
			//End Unmaped Properties
			////FC:FINAL:BCU #i169 - move on Init otherwise it`s too late
			//modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			//modGlobalFunctions.SetTRIOColors(this);
			//// these are the column numbers
			//lngColCheck = 0;
			//lngColRateKey = 1;
			//lngColDescription = 2;
			//lngColPeriods = 3;
			//lngColHidden = 4;
			//lngColDue1 = 5;
			//lngColDue2 = 6;
			//lngColDue3 = 7;
			//lngColDue4 = 8;
		}

		private void FormatGrid()
		{
			int lngWid = 0;
			lngWid = vsRate.WidthOriginal;
			vsRate.Cols = 9;
			vsRate.ExplorerBar = FCGrid.ExplorerBarSettings.flexExSortShow;
			vsRate.AllowUserResizing = FCGrid.AllowUserResizeSettings.flexResizeColumns;
			vsRate.FrozenCols = 2;
			vsRate.ColDataType(lngColCheck, FCGrid.DataTypeSettings.flexDTBoolean);
			vsRate.ColWidth(lngColCheck, FCConvert.ToInt32(lngWid * 0.05));
			vsRate.ColWidth(lngColRateKey, FCConvert.ToInt32(lngWid * 0.05));
			vsRate.ColWidth(lngColDescription, FCConvert.ToInt32(lngWid * 0.35));
			vsRate.ColWidth(lngColPeriods, FCConvert.ToInt32(lngWid * 0.06));
			vsRate.ColWidth(lngColHidden, 0);
			vsRate.ColWidth(lngColDue1, FCConvert.ToInt32(lngWid * 0.12));
			vsRate.ColWidth(lngColDue2, FCConvert.ToInt32(lngWid * 0.12));
			vsRate.ColWidth(lngColDue3, FCConvert.ToInt32(lngWid * 0.12));
			vsRate.ColWidth(lngColDue4, FCConvert.ToInt32(lngWid * 0.12));
			vsRate.TextMatrix(0, lngColCheck, "");
			vsRate.TextMatrix(0, lngColRateKey, "Rate");
			vsRate.TextMatrix(0, lngColDescription, "Description");
			vsRate.TextMatrix(0, lngColPeriods, "Periods");
			vsRate.TextMatrix(0, lngColDue1, "Due 1");
			vsRate.TextMatrix(0, lngColDue2, "Due 2");
			vsRate.TextMatrix(0, lngColDue3, "Due 3");
			vsRate.TextMatrix(0, lngColDue4, "Due 4");
            vsRate.ColAlignment(lngColRateKey, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsRate.ColAlignment(lngColDescription, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsRate.ColAlignment(lngColPeriods, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsRate.ColAlignment(lngColDue1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsRate.ColAlignment(lngColDue2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsRate.ColAlignment(lngColDue3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsRate.ColAlignment(lngColDue4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
        }

		private void FillGrid(ref clsDRWrapper rsFill)
		{
			vsRate.Rows = 1;
			while (!rsFill.EndOfFile())
			{
				vsRate.AddItem("");
				vsRate.TextMatrix(vsRate.Rows - 1, lngColRateKey, rsFill.Get_Fields_Int32("ID"));
				vsRate.TextMatrix(vsRate.Rows - 1, lngColDescription, rsFill.Get_Fields_String("Description"));
				vsRate.TextMatrix(vsRate.Rows - 1, lngColPeriods, rsFill.Get_Fields_Int16("NumberOfPeriods"));
				vsRate.TextMatrix(vsRate.Rows - 1, lngColDue1, Strings.Format(rsFill.Get_Fields("DueDate1"), "MM/dd/yyyy"));
				vsRate.TextMatrix(vsRate.Rows - 1, lngColDue2, Strings.Format(rsFill.Get_Fields("DueDate2"), "MM/dd/yyyy"));
				vsRate.TextMatrix(vsRate.Rows - 1, lngColDue3, Strings.Format(rsFill.Get_Fields("DueDate3"), "MM/dd/yyyy"));
				vsRate.TextMatrix(vsRate.Rows - 1, lngColDue4, Strings.Format(rsFill.Get_Fields("DueDate4"), "MM/dd/yyyy"));
				rsFill.MoveNext();
			}
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			bool boolFound;
			int intCT;
			boolFound = false;
			// return the ratekey selection
			for (intCT = 1; intCT <= vsRate.Rows - 1; intCT++)
			{
				if (Conversion.Val(vsRate.TextMatrix(intCT, lngColCheck)) == -1)
				{
					boolFound = true;
					break;
				}
			}
			if (boolFound)
			{
				lngFinalRK = FCConvert.ToInt32(Math.Round(Conversion.Val(vsRate.TextMatrix(intCT, lngColRateKey))));
			}
			else
			{
				lngFinalRK = 0;
				FCMessageBox.Show("No rate record selected.", MsgBoxStyle.Exclamation, "No Rate Record");
			}
		}

		private void frmSelectRateKey_Resize(object sender, System.EventArgs e)
		{
			//FC:FINAL:BCU #i169 - do not format grid on reisze or data will be lost
			//FormatGrid();
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void vsRate_RowColChange(object sender, EventArgs e)
		{
			if (vsRate.Col == lngColCheck)
			{
				vsRate.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else
			{
				vsRate.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void vsRate_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (vsRate.CurrentCell.IsInEditMode)
			{
				//FC:FINAL:MSH - save and use correct indexes of the cell
				int row = vsRate.GetFlexRowIndex(e.RowIndex);
				int col = vsRate.GetFlexColIndex(e.ColumnIndex);
				int intCT;
				if (col == lngColCheck)
				{
					for (intCT = 1; intCT <= vsRate.Rows - 1; intCT++)
					{
						if (intCT != row)
						{
							vsRate.TextMatrix(intCT, lngColCheck, FCConvert.ToString(0));
						}
					}
				}
			}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFileSaveExit_Click(sender, e);
		}
	}
}
