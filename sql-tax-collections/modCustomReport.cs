﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	public class modCustomReport
	{
		// this will tell the subreport to change the format of the report and show the payments in Status List format rather than account detail format
		public const int GRIDNONE = 0;
		// VBto upgrade warning: GRIDTEXT As short --> As int	OnRead(string)
		public const int GRIDTEXT = 1;
		// VBto upgrade warning: GRIDDATE As short --> As int	OnRead(string)
		public const int GRIDDATE = 2;
		// VBto upgrade warning: GRIDCOMBOIDTEXT As short --> As int	OnRead(string)
		public const int GRIDCOMBOIDTEXT = 3;
		// VBto upgrade warning: GRIDNUMRANGE As short --> As int	OnRead(string)
		public const int GRIDNUMRANGE = 4;
		public const int GRIDCOMBOIDNUM = 5;
		// VBto upgrade warning: GRIDCOMBOTEXT As short --> As int	OnRead(string)
		public const int GRIDCOMBOTEXT = 6;
		// VBto upgrade warning: GRIDTEXTRANGE As short --> As int	OnRead(string)
		public const int GRIDTEXTRANGE = 7;
		// VBto upgrade warning: GRIDCOMBOANDNUM As short --> As int	OnRead(string)
		public const int GRIDCOMBOANDNUM = 8;
		// THIS HAS A COMBO BOX AND NUMERIC FIELD
		// VBto upgrade warning: GRIDCOMBORANGE As short --> As int	OnRead(string)
		public const int GRIDCOMBORANGE = 9;
		// VBto upgrade warning: GRIDDATERANGE As short --> As int	OnRead(string)
		public const int GRIDDATERANGE = 10;
		public const string CUSTOMREPORTDATABASE = "TWCL0000.VB1";
		// VBto upgrade warning: 'Return' As Variant --> As string
		public static string FixQuotes(string strValue)
		{
			string FixQuotes = "";
			FixQuotes = strValue.Replace("'", "''");
			return FixQuotes;
		}

		public static void SetColumnCaptions(dynamic FormName)
		{
			// THIS ROUTINE GETS THE CAPTIONS THAT ARE TO BE DISPLAYED ON
			// THE REPORT ITSELF
			// THIS ARRAY (strFieldCaptions) HOLDS ONLY THE FIELD NAMES THAT ARE
			// TO BE DISPLAYED WHERE THE ARRAY (strCaptions) HOLDS ALL POSSIBLE
			// FIELD NAMES
			int intCounter;
			int intCount;
			intCount = 0;
			for (intCounter = 0; intCounter <= FormName.lstFields.ListCount - 1; intCounter++)
			{
				if (FormName.lstFields.Selected(intCounter))
				{
					Statics.strFieldCaptions[intCount] = Statics.strCaptions[FormName.lstFields.ItemData(intCounter)];
					intCount += 1;
				}
			}
		}

		//public static void GetNumberOfFields(string strSQL)
		//{
		//	// THIS ROUTINE GETS THE NUMBER OF FIELDS TO REPORT ON.
		//	// ALSO...THIS FILLS THE ARRAY (strFieldNames) WITH ALL OF THE FIELDS
		//	// TO SHOW IN THE REPORT. THESE ARE NOT THE DISPLAY CAPTIONS BUT THE
		//	// ACTUAL DATABASE FIELD NAMES TO BE USED IN THE SQL STATEMENT
		//	int intCT;
		//	Statics.intNumberOfSQLFields = 0;
		//	for (intCT = 0; intCT <= frmCustomReport.InstancePtr.lstFields.Items.Count - 1; intCT++)
		//	{
		//		if (frmCustomReport.InstancePtr.lstFields.Selected(intCT))
		//		{
		//			Statics.intNumberOfSQLFields += 1;
		//		}
		//	}
		//	// Dim strTemp
		//	// Dim strTemp2
		//	// Dim intCount As Integer
		//	// Dim intCounter As Integer
		//	// 
		//	// If Trim(strSQL) = vbNullString Then
		//	// intNumberOfSQLFields = -1
		//	// Exit Sub
		//	// End If
		//	// 
		//	// strTemp = Split(strSQL, "from")
		//	// strSQL = Mid(strTemp(0), 7, Len(strTemp(0)) - 6)
		//	// strTemp = Split(strSQL, ",")
		//	// intNumberOfSQLFields = UBound(strTemp)
		//	// 
		//	// For intCounter = 0 To intNumberOfSQLFields
		//	// If Trim(strTemp(intCounter)) <> vbNullString Then
		//	// CHECK TO SEE IF AN ALIAS WAS USED IN THE SQL STATEMENT
		//	// strFieldNames(intCounter) = CheckForAS(strTemp(intCounter))
		//	// 
		//	// NEED TO SET THE CAPTIONS ARRAY SO THAT IF SHOWING A SAVED
		//	// REPORT THE CORRECT CAPTION WILL SHOW ON THE REPORT
		//	// For intCount = 0 To intNumberOfSQLFields
		//	// If strFields(intCount) = Trim(strTemp(intCounter)) Then
		//	// strFieldCaptions(intCounter) = strCaptions(intCount)
		//	// Exit For
		//	// End If
		//	// Next
		//	// End If
		//	// Next
		//}
		// VBto upgrade warning: 'Return' As object	OnWrite(object, string)
		public static object CheckForAS(string strFieldName, short intSegment = 2)
		{
			object CheckForAS = null;
			// THIS FUNCTION WILL RETURN THE SQL REFERENCE FIELD NAME.
			// IF AN ALIAS WAS USED THEN IT GETS THE ALIAS NAME AND NOT THE
			// DATABASE FIELD NAME
			// VBto upgrade warning: strTemp As Variant --> As string()
			string[] strTemp = null;
			if (Strings.InStr(1, strFieldName, " AS ", CompareConstants.vbTextCompare) != 0)
			{
				// SEE IF THE WORD AS IS USED TO INDICATE AN ALIAS
				strTemp = Strings.Split(Strings.Trim(strFieldName), " ", -1, CompareConstants.vbBinaryCompare);
				if (Strings.UCase(strTemp[1]) == "AS")
				{
					// RETURN THE SQL REFERENCE FIELD NAME
					if (intSegment == 1)
					{
						CheckForAS = strTemp[0];
					}
					else
					{
						CheckForAS = strTemp[2];
					}
				}
				else
				{
					// RETURN THE ACTUAL DATABASE FIELD NAME
					CheckForAS = strTemp[0];
				}
			}
			else
			{
				// AN ALIAS WAS NOT USED SO JUST USE THE DATABASE FIELD NAME
				strTemp = Strings.Split(Strings.Trim(strFieldName), " ", -1, CompareConstants.vbBinaryCompare);
				CheckForAS = Strings.Trim(strTemp[0]);
			}
			return CheckForAS;
		}

		public static void ClearComboListArray()
		{
			// CLEAR THE COMBO LIST ARRAY.
			// THIS IS THE ARRAY THAT HOLDS THE COMBO LIST IN THE WHERE GRID
			int intCount;
			for (intCount = 0; intCount <= 20; intCount++)
			{
				Statics.strComboList[intCount, 0] = string.Empty;
				Statics.strComboList[intCount, 1] = string.Empty;
			}
		}

		public static void SetHeaderGridProperties(ref object GridName)
		{
			// THIS ROUTINE CAN BE USED IF YOU WANT SPECIFIC GRID FORMATTING DONE
			// THIS IS FORMATTING FOR THE GRID THAT DISPLAYS THE HEADINGS
			if (Strings.UCase(Statics.strReportType) == "ACCOUNT")
			{
			}
			else if (Strings.UCase(Statics.strReportType) == "PREPAYMENTS")
			{
			}
			else if (Strings.UCase(Statics.strReportType) == "TAXCLUB")
			{
			}
			else if (Strings.UCase(Statics.strReportType) == "TAXLIEN")
			{
			}
			else
			{
				// GridName.BackColor = vbBlue
				//				// GridName.ForeColor = System.Drawing.Color.White
			}
			// GridName.Select 0, 0, 0, GridName.Cols - 1
			// GridName.CellFontBold = True
		}

		public static void SetDataGridProperties(ref object GridName)
		{
			// THIS ROUTINE CAN BE USED IF YOU WANT SPECIFIC GRID FORMATTING DONE
			// THIS IS FORMATTING FOR THE GRID THAT DISPLAYS THE REPORTS DATA
			if (Strings.UCase(Statics.strReportType) == "ACCOUNT")
			{
			}
			else if (Strings.UCase(Statics.strReportType) == "PREPAYMENTS")
			{
			}
			else if (Strings.UCase(Statics.strReportType) == "TAXCLUB")
			{
			}
			else if (Strings.UCase(Statics.strReportType) == "TAXLIEN")
			{
			}
		}

		public static void LoadWhereGrid(dynamic FormName)
		{
			// LOAD THE WHERE GRID WITH FIELDS FROM THE FIELD LIST BOX
			int intCounter;
			int lngWid;
			FormName.vsWhere.Rows = 0;
			for (intCounter = 0; intCounter <= FormName.lstFields.ListCount - 1; intCounter++)
			{
				FormName.vsWhere.AddItem(FormName.lstFields.List(intCounter));
				// IF THE TYPE OF THE FIELD IS A DATE THEN WE WANT TO ALLOW
				// THE USER TO PUT IN A RANGE OF DATES SO WE NEED TO ALLOW
				// THEM TWO FIELDS UNLIKE ALL OTHER TYPES WHERE WE ONLY WANT ONE
				if (FCConvert.ToDouble(Statics.strWhereType[intCounter]) != GRIDDATE && FCConvert.ToDouble(Statics.strWhereType[intCounter]) != GRIDNUMRANGE && FCConvert.ToDouble(Statics.strWhereType[intCounter]) != GRIDTEXTRANGE)
				{
					FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, FormName.vsWhere.Rows - 1, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				}
			}
			// SET THE NUMBER OF COLUMNS AND THE WIDTH OF THOSE COLUMNS
			// THE LAST COLUMN IS THERE TO HOLD ID VALUES IF THE TYPE IS OF
			// A COMBO BOX BECAUSE THE LIST DOES NOT STAY WITH THAT CELL ONCE
			// THE USER MOVES TO A NEW ONE.
			lngWid = FormName.vsWhere.WidthOriginal;
			FormName.vsWhere.Cols = 4;
			FormName.vsWhere.ColWidth(0, lngWid * 0.35);
			FormName.vsWhere.ColWidth(1, lngWid * 0.3);
			FormName.vsWhere.ColWidth(2, lngWid * 0.3);
			FormName.vsWhere.ColWidth(3, 0);
		}
		// VBto upgrade warning: FormName As Form	OnWrite(Form)
		private static void LoadGridCellAsCombo(dynamic FormName, int intRowNumber, string strSQL, string FieldName = "", string IDField = "", string DatabaseFieldName = "", string strDatabaseToUse = modGlobal.DEFAULTDATABASE)
		{
			// THIS ROUTINE BUILDS A COMBO LIST THAT MAKE THE CELL WORK LIKE A
			// COMBO BOX ONCE THE USER CLICKS ON IT. THE LIST ARE GOVERNED BY THE
			// ROW THAT IT IS IN THE GIRD.
			// THE SECOND ELEMENT IN THE ARRAY HOLDS THE DATABASE FIELD NAME SO
			// WHEN THE WHERE CLAUSE IS BUILT IT WILL KNOW WHAT FIELD NAME TO USE
			if (Information.IsNothing(FieldName) || FieldName == string.Empty)
			{
				Statics.strComboList[intRowNumber, 0] = strSQL;
				// ElseIf UCase(FieldName) = "LISTTAB" Then
			}
			else
			{
				int intCounter/*unused?*/;
				clsDRWrapper rsCombo = new clsDRWrapper();
				if (Strings.UCase(FieldName) == "BILLINGYEAR")
				{
					rsCombo.OpenRecordset(strSQL, strDatabaseToUse);
					while (!rsCombo.EndOfFile())
					{
						Statics.strComboList[intRowNumber, 0] += "|";
						Statics.strComboList[intRowNumber, 0] += "#" + FCConvert.ToString(rsCombo.Get_Fields(IDField)) + ";" + modExtraModules.FormatYear(FCConvert.ToString(rsCombo.Get_Fields(FieldName)));
						Statics.strComboList[intRowNumber, 1] = DatabaseFieldName;
						rsCombo.MoveNext();
					}
				}
				else if (Strings.UCase(FieldName) == "CODE")
				{
					rsCombo.OpenRecordset(strSQL, strDatabaseToUse);
					while (!rsCombo.EndOfFile())
					{
						Statics.strComboList[intRowNumber, 0] += "|";
						Statics.strComboList[intRowNumber, 0] += "#" + FCConvert.ToString(rsCombo.Get_Fields(IDField)) + ";" + FCConvert.ToString(rsCombo.Get_Fields(FieldName)) + "\t" + FCConvert.ToString(rsCombo.Get_Fields_String("ShortDescription"));
						Statics.strComboList[intRowNumber, 1] = FCConvert.ToString(rsCombo.Get_Fields(IDField));
						rsCombo.MoveNext();
					}
				}
				else if (Strings.UCase(FieldName) == "ID")
				{
					// "RATEKEY" Then
					rsCombo.OpenRecordset(strSQL, strDatabaseToUse);
					while (!rsCombo.EndOfFile())
					{
						Statics.strComboList[intRowNumber, 0] += "|";
						Statics.strComboList[intRowNumber, 0] += "#" + FCConvert.ToString(rsCombo.Get_Fields(IDField)) + ";" + FCConvert.ToString(rsCombo.Get_Fields(FieldName)) + "\t" + FCConvert.ToString(rsCombo.Get_Fields_String("RateType")) + "\t" + Strings.Format(rsCombo.Get_Fields_DateTime("BillingDate"), "MM/dd/yyyy");
						Statics.strComboList[intRowNumber, 1] = FCConvert.ToString(rsCombo.Get_Fields(IDField));
						rsCombo.MoveNext();
					}
				}
				else
				{
					rsCombo.OpenRecordset(strSQL, strDatabaseToUse);
					while (!rsCombo.EndOfFile())
					{
						Statics.strComboList[intRowNumber, 0] += "|";
						Statics.strComboList[intRowNumber, 0] += "#" + FCConvert.ToString(rsCombo.Get_Fields(IDField)) + ";" + FCConvert.ToString(rsCombo.Get_Fields(FieldName));
						Statics.strComboList[intRowNumber, 1] = DatabaseFieldName;
						rsCombo.MoveNext();
					}
				}
			}
		}
		// VBto upgrade warning: 'Return' As Variant --> As bool
		public static bool IsValidDate(string DateToCheck)
		{
			bool IsValidDate = false;
			string strAnnTemp;
			// VBto upgrade warning: intMonth As short --> As int	OnWrite(string)
			int intMonth;
			// hold month
			// VBto upgrade warning: intDay As short --> As int	OnWrite(string)
			int intDay;
			// hold day
			int intYear/*unused?*/;
			// hold year
			int intCheckFormat/*unused?*/;
			// make sure date is format 'mm/dd'
			strAnnTemp = DateToCheck;
			intMonth = FCConvert.ToInt32(Strings.Left(strAnnTemp, 2));
			intDay = FCConvert.ToInt32(Strings.Mid(strAnnTemp, 4, 2));
			switch (intMonth)
			{
				case 2:
					{
						// feb can't be more then 29 days
						if (intDay > 29)
						{
							FCMessageBox.Show("Invalid Month on Date", MsgBoxStyle.OkOnly, "TRIO Software");
							return IsValidDate;
						}
						break;
					}
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
				case 12:
					{
						// these months no more then 31 days
						if (intDay > 31)
						{
							FCMessageBox.Show("Invalid day on Date", MsgBoxStyle.OkOnly, "TRIO Software");
							return IsValidDate;
						}
						break;
					}
				case 4:
				case 6:
				case 9:
				case 11:
					{
						// these months no more then 30 days
						if (intDay > 30)
						{
							FCMessageBox.Show("Invalid day on Date", MsgBoxStyle.OkOnly, "ErTRIO Softwarevror");
							return IsValidDate;
						}
						break;
					}
				default:
					{
						// not even a month
						FCMessageBox.Show("Invalid Month on Date", MsgBoxStyle.OkOnly, "TRIO Software");
						return IsValidDate;
					}
			}
			//end switch
			IsValidDate = true;
			return IsValidDate;
		}
		// VBto upgrade warning: FormName As Form	OnWrite(frmCustomReport)
		//public static void SetFormFieldCaptions(Form FormName, string ReportType)
		//{
		//	// THIS IS THE MAIN ROUTINE THAT FILLS THE CUSTOM REPORT FORM.
		//	// 
		//	// ****************************************************************
		//	// THIS IS THE ONLY ROUTINE THAT NEEDS TO BE ALTERED WHEN YOU WANT
		//	// TO ADD A NEW REPORT TYPE.
		//	// ****************************************************************
		//	Statics.strReportType = Strings.UCase(ReportType);
		//	// CLEAR THE COMBO LIST ARRAY
		//	ClearComboListArray();
		//	if ((Strings.UCase(ReportType) == "ACCOUNT") || (Strings.UCase(ReportType) == "ABATE"))
		//	{
		//		SetAccountLists(FormName);
		//	}
		//	else if (Strings.UCase(ReportType) == "PREPAYMENTS")
		//	{
		//		SetPrepayment(FormName);
		//	}
		//	else if (Strings.UCase(ReportType) == "TAXCLUB")
		//	{
		//		SetTaxClub(FormName);
		//	}
		//	else if (Strings.UCase(ReportType) == "TAXLIEN")
		//	{
		//		SetOutStandingLien(FormName);
		//	}
		//	// POPULATE THE SORT LIST WITH THE SAME FIELDS AS THE FIELD LIST BOX
		//	frmCustomReport.InstancePtr.LoadSortList();
		//	// POPULATE THE WHERE GRID WITH THE FIELDS FROM THE FIELD LIST BOX
		//	// Call LoadWhereGrid(FormName)
		//	// LOAD THE SAVED REPORT COMBO ON THE FORM
		//	FCUtils.CallByName(FormName, "LoadCombo", CallType.Method);
		//}
		// VBto upgrade warning: FormName As Form	OnWrite(Form)
		public static void SetAccountLists(dynamic FormName)
		{
			// WHAT IS THE CAPTION TO SHOW ON THE TOP OF THE REPORT
			Statics.strCustomTitle = "Complete Listing";
			FormName.lstFields.Clear();
			// GIVE THE NAME TO SHOW IN THE LIST BOX
			FormName.lstFields.AddItem("Location");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 0);
			FormName.lstFields.AddItem("Map / Lot");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 1);
			FormName.lstFields.AddItem("Mailing Address");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 2);
			// FormName.lstFields.AddItem "Tax Year"
			// FormName.lstFields.ItemData(FormName.lstFields.NewIndex) = 3
			// FormName.lstFields.AddItem "Book / Page"
			// FormName.lstFields.ItemData(FormName.lstFields.NewIndex) = 4
			// FormName.lstFields.AddItem "Land Value"
			// FormName.lstFields.ItemData(FormName.lstFields.NewIndex) = 5
			// FormName.lstFields.AddItem "Building Value"
			// FormName.lstFields.ItemData(FormName.lstFields.NewIndex) = 6
			// FormName.lstFields.AddItem "Exemption"
			// FormName.lstFields.ItemData(FormName.lstFields.NewIndex) = 7
			// THIS IS THE ACTUAL DATABASE FIELD NAME
			// this is for the selection list
			Statics.strFields[0] = "Location";
			Statics.strFields[1] = "MapLot";
			// strFields(2) = "Address"
			// strFields(3) = "BillingYear"
			// strFields(4) = "BookPage"
			// strFields(5) = "LandValue"
			// strFields(6) = "BuildingValue"
			// strFields(7) = "ExemptValue"
			// THESE ARE THE FIELD NAMES AFTER ALL OF THE
			// this is for the sort list
			Statics.strCaptions[0] = "Account";
			Statics.strCaptions[1] = "Name";
			Statics.strCaptions[2] = "Year";
			Statics.strCaptions[3] = "";
			Statics.strCaptions[4] = "";
			Statics.strCaptions[5] = "";
			Statics.strCaptions[6] = "";
			Statics.strCaptions[7] = "";
			// MARK WHAT TYPE THIS FIELD IS SO THAT THE WHERE GRID
			// WILL KNOW WHAT TYPE OF DATA TO DISPLAY.
			// 
			// THE SEQUENCE THAT THESE APPEAR IN THE ARRAY SHOULD MATCH
			// THAT SEQUENCE OF THE strFields ARRAY.
			// this is the type of the question
			Statics.strWhereType[0] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[1] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[2] = FCConvert.ToString(GRIDCOMBORANGE);
			Statics.strWhereType[3] = FCConvert.ToString(GRIDCOMBOANDNUM);
			Statics.strWhereType[4] = FCConvert.ToString(GRIDCOMBOTEXT);
			Statics.strWhereType[5] = FCConvert.ToString(GRIDDATE);
			Statics.strWhereType[6] = FCConvert.ToString(GRIDDATERANGE);
			Statics.strWhereType[7] = FCConvert.ToString(GRIDCOMBORANGE);
			Statics.strWhereType[8] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[9] = FCConvert.ToString(GRIDCOMBORANGE);
			Statics.strWhereType[10] = FCConvert.ToString(GRIDCOMBOTEXT);
			// this is the text that will be shown on the grid
			Statics.strWhereNames[0] = "Account";
			Statics.strWhereNames[1] = "Name";
			Statics.strWhereNames[2] = "Tax Year";
			Statics.strWhereNames[3] = "Balance Due";
			Statics.strWhereNames[4] = "Only Accounts with Type";
			Statics.strWhereNames[5] = "As Of Date";
			Statics.strWhereNames[6] = "Show Payment From";
			Statics.strWhereNames[7] = "Tran Code";
			Statics.strWhereNames[8] = "Tax Acquired";
			Statics.strWhereNames[9] = "Rate Record";
			Statics.strWhereNames[10] = "Bill Status";
			// this is the field that will be used
			Statics.strWhereFields[0] = "Account";
			Statics.strWhereFields[1] = "Name1";
			Statics.strWhereFields[2] = "BillingYear";
			Statics.strWhereFields[3] = "Total";
			Statics.strWhereFields[4] = "Code";
			Statics.strWhereFields[5] = "";
			Statics.strWhereFields[6] = "";
			Statics.strWhereFields[7] = "TranCode";
			Statics.strWhereFields[8] = "";
			Statics.strWhereFields[9] = "RateKey";
			// IF ONE OF THE FIELDS IS OF TYPE GRIDCOMBOIDTEXT THEN YOU NEED
			// TO CREATE A LIST TO SHOW WHEN THE USER CLICKS ON THAT CELL
			// IN THE GRID
			LoadGridCellAsCombo(FormName, 2, "SELECT DISTINCT BillingYear FROM BillingMaster ORDER BY BillingYear", "BillingYear", "BillingYear");
			LoadGridCellAsCombo(FormName, 3, "#0;<" + "\t" + "Less Than|#1;>" + "\t" + "Greater Than|#2;=" + "\t" + "Equals|#3;<>" + "\t" + "Not Equal");
			LoadGridCellAsCombo(FormName, 4, "#0;Abatements|#1;Discounts|#2;Refunded Abatements|#3;Tax Clubs|#4;Pre Payments|#5;Lien / 30 Day Costs|#6;Payments|#7;Corrections");
			if (modStatusPayments.Statics.boolRE)
			{
				LoadGridCellAsCombo(FormName, 7, "SELECT Code, ShortDescription FROM tblTranCode ORDER BY Code", "Code", "Code", "", modExtraModules.strREDatabase);
			}
			else
			{
				LoadGridCellAsCombo(FormName, 7, "SELECT Trancode, TranType FROM TranCodes ORDER BY TranCode", "Trancode", "Trancode", "", modExtraModules.strPPDatabase);
			}
			// MAL@20071011: Changed to remove the 'Both' option
			// Program Bug Reference: 6533
			// Call LoadGridCellAsCombo(FormName, 8, "#0;Both|#1;Tax Acquired|#2;Non Tax Acquired")
			LoadGridCellAsCombo(FormName, 8, "#0;Tax Acquired|#1;Non Tax Acquired");
			LoadGridCellAsCombo(FormName, 9, "SELECT DISTINCT ID, RateType, BillingDate FROM RateRec ORDER BY ID", "ID", "ID");
			// "RateKey", "RateKey")
			LoadGridCellAsCombo(FormName, 10, "#0;Both|#1;Regular|#2;Lien|#3;PreLien Information");
			// FINISH THE MID SECTION OF THE SQL STATEMENT
			// FormName.fraFields.Tag = "from (DogInfo INNER JOIN DefaultColors on val(DogInfo.DogColor) = DefaultColors.ID) INNER JOIN DefaultBreeds on val(DogInfo.DogBreed) = DefaultBreeds.ID"
		}
		// VBto upgrade warning: FormName As Form	OnWrite(Form)
		public static void SetPrepayment(dynamic FormName)
		{
			Statics.strCustomTitle = "Prepayment Report";
			FormName.lstFields.Clear();
			FormName.lstFields.AddItem("Account");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 0);
			FormName.lstFields.AddItem("Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 1);
			FormName.lstFields.AddItem("Date");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 2);
			FormName.lstFields.AddItem("Amount");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 3);
			FormName.lstFields.AddItem("Period");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 4);
			FormName.lstFields.AddItem("Reference");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 5);
			Statics.strFields[0] = "Account";
			Statics.strFields[1] = "Name";
			Statics.strFields[2] = "Date";
			Statics.strFields[3] = "Amount";
			Statics.strFields[4] = "Period";
			Statics.strFields[5] = "Reference";
			Statics.strCaptions[0] = "Account";
			Statics.strCaptions[1] = "Name";
			Statics.strCaptions[2] = "Date";
			Statics.strCaptions[3] = "Amount";
			Statics.strCaptions[4] = "Period";
			Statics.strCaptions[5] = "Reference";
			Statics.strWhereType[0] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[1] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[2] = FCConvert.ToString(GRIDDATE);
			Statics.strWhereType[3] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[4] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[5] = FCConvert.ToString(GRIDTEXT);
		}
		// VBto upgrade warning: FormName As Form	OnWrite(Form)
		public static void SetTaxClub(dynamic FormName)
		{
			Statics.strCustomTitle = "Tax Club Report";
			FormName.lstFields.Clear();
			FormName.lstFields.AddItem("Account");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 0);
			FormName.lstFields.AddItem("Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 1);
			FormName.lstFields.AddItem("Date");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 2);
			FormName.lstFields.AddItem("Amount");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 3);
			FormName.lstFields.AddItem("Period");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 4);
			FormName.lstFields.AddItem("Reference");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 5);
			Statics.strFields[0] = "Account";
			Statics.strFields[1] = "Name";
			Statics.strFields[2] = "Date";
			Statics.strFields[3] = "Amount";
			Statics.strFields[4] = "Period";
			Statics.strFields[5] = "Reference";
			Statics.strCaptions[0] = "Account";
			Statics.strCaptions[1] = "Name";
			Statics.strCaptions[2] = "Date";
			Statics.strCaptions[3] = "Amount";
			Statics.strCaptions[4] = "Period";
			Statics.strCaptions[5] = "Reference";
			Statics.strWhereType[0] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[1] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[2] = FCConvert.ToString(GRIDDATE);
			Statics.strWhereType[3] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[4] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[5] = FCConvert.ToString(GRIDTEXT);
		}
		// VBto upgrade warning: FormName As Form	OnWrite(Form)
		public static void SetOutStandingLien(dynamic FormName)
		{
			Statics.strCustomTitle = "Outstanding Tax Lien Report";
			FormName.lstFields.Clear();
			FormName.lstFields.AddItem("Account");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 0);
			FormName.lstFields.AddItem("Name");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 1);
			FormName.lstFields.AddItem("Principal");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 2);
			FormName.lstFields.AddItem("Pre Lien Interest");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 3);
			FormName.lstFields.AddItem("Costs");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 4);
			FormName.lstFields.AddItem("Current Interest");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 5);
			FormName.lstFields.AddItem("Total");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 6);
			Statics.strFields[0] = "Account";
			Statics.strFields[1] = "Name";
			Statics.strFields[2] = "Date";
			Statics.strFields[3] = "Amount";
			Statics.strFields[4] = "Period";
			Statics.strFields[5] = "Reference";
			Statics.strFields[6] = "Total";
			Statics.strCaptions[0] = "Account";
			Statics.strCaptions[1] = "Name";
			Statics.strCaptions[2] = "Date";
			Statics.strCaptions[3] = "Amount";
			Statics.strCaptions[4] = "Period";
			Statics.strCaptions[5] = "Reference";
			Statics.strCaptions[6] = "Total";
			Statics.strWhereType[0] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[1] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[2] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[3] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[4] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[5] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[6] = FCConvert.ToString(GRIDNUMRANGE);
		}

		public static void LoadSortList(dynamic FormName)
		{
			// LOAD THE SORT LIST BOX WITH THE FIELDS FROM THE FIELDS LIST BOX
			int intCounter;
			FormName.lstSort.Clear();
			for (intCounter = 0; intCounter <= FormName.vsWhere.Rows - 1; intCounter++)
			{
				FormName.lstSort.AddItem(FormName.vsWhere.TextMatrix(intCounter, 0));
				FormName.lstSort.ItemData(FormName.lstSort.NewIndex, intCounter);
			}
		}

		//public static string GetStatusName(clsDRWrapper rsD, bool boolDelAccount = false)
		//{
		//	string GetStatusName = "";
		//	try
		//	{

		//		string strDel = "";
		//		// VBto upgrade warning: dtTransferFromBillDate As DateTime	OnWriteFCConvert.ToInt16(
		//		DateTime dtTransferFromBillDate;

		//		// TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
		//		if (Strings.Trim(rsD.Get_Fields("Own1FullName")) != "")
		//		{
		//			// Not rsRE.EndOfFile
		//			// if this is true then get the info from the RE Master, else get it from the bill record
		//			if (modStatusPayments.Statics.boolRE)
		//			{
		//				boolDelAccount = rsD.Get_Fields_Boolean("RSDeleted");
		//			}
		//			else
		//			{
		//				boolDelAccount = rsD.Get_Fields_Boolean("Deleted");
		//			}
		//			if (boolDelAccount)
		//			{
		//				strDel = "!";
		//			}
		//			else
		//			{
		//				strDel = "";
		//			}
		//			if (Information.IsDate(rsD.Get_Fields("TransferFromBillingDateFirst")))
		//			{
		//				dtTransferFromBillDate = rsD.Get_Fields_DateTime("TransferFromBillingDateFirst");
		//			}
		//			else
		//			{
		//				dtTransferFromBillDate = DateTime.FromOADate(0);
		//			}
		//			switch (frmCustomReport.InstancePtr.intShowOwnerType)
		//			{
		//				case 0:
		//					{
		//						if (modStatusPayments.Statics.boolRE)
		//						{
		//							// TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
		//							// TODO Get_Fields: Field [Own2FullName] not found!! (maybe it is an alias?)
		//							GetStatusName = Strings.Trim(rsD.Get_Fields("Own1FullName")) + " " + rsD.Get_Fields("Own2FullName");
		//						}
		//						else
		//						{
		//							// TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
		//							GetStatusName = Strings.Trim(rsD.Get_Fields("Own1FullName"));
		//						}
		//						break;
		//					}
		//				case 1:
		//					{
		//						// TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
		//						if (Strings.UCase(rsD.Get_Fields_String("Name1")) != Strings.UCase(rsD.Get_Fields("Own1FullName")))
		//						{
		//							// rsSR.OpenRecordset "SELECT * FROM SRMaster WHERE RSAccount = " & .Get_Fields("Account") & " AND RSCard = 1 AND SaleDate > #" & .Get_Fields("TransferFromBillingDateLast") & "#", strREDatabase
		//							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
		//							if (!modCLCalculations.NewOwner(rsD.Get_Fields_String("Name1"), rsD.Get_Fields("Account"), ref dtTransferFromBillDate))
		//							{
		//								GetStatusName = rsD.Get_Fields_String("Name1") + " " + rsD.Get_Fields_String("Name2");
		//							}
		//							else
		//							{
		//								GetStatusName = rsD.Get_Fields_String("Name1") + " " + rsD.Get_Fields_String("Name2");
		//								// & " C\O " & rsRE.Get_Fields("Name")
		//							}
		//						}
		//						else
		//						{
		//							GetStatusName = rsD.Get_Fields_String("Name1") + " " + rsD.Get_Fields_String("Name2");
		//						}
		//						break;
		//					}
		//				case 2:
		//					{
		//						// TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
		//						if (Strings.UCase(rsD.Get_Fields_String("Name1")) != Strings.UCase(rsD.Get_Fields("Own1FullName")))
		//						{
		//							// rsSR.OpenRecordset "SELECT * FROM SRMaster WHERE RSAccount = " & .Get_Fields("Account") & " AND RSCard = 1 AND SaleDate > #" & .Get_Fields("TransferFromBillingDateLast") & "#", strREDatabase
		//							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
		//							if (!modCLCalculations.NewOwner(rsD.Get_Fields_String("Name1"), rsD.Get_Fields("Account"), ref dtTransferFromBillDate))
		//							{
		//								GetStatusName = Strings.Trim(Strings.Trim(rsD.Get_Fields_String("Name1")) + " " + rsD.Get_Fields_String("Name2"));
		//							}
		//							else
		//							{
		//								GetStatusName = Strings.Trim(Strings.Trim(rsD.Get_Fields_String("Name1")) + " " + rsD.Get_Fields_String("Name2")) + " C\\O " + Strings.Trim(rsD.Get_Fields_String("Name"));
		//							}
		//						}
		//						else
		//						{
		//							GetStatusName = Strings.Trim(Strings.Trim(rsD.Get_Fields_String("Name1")) + " " + rsD.Get_Fields_String("Name2"));
		//						}
		//						break;
		//					}
		//				case 3:
		//					{
		//						// TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
		//						if (Strings.UCase(rsD.Get_Fields_String("Name1")) != Strings.UCase(rsD.Get_Fields("Own1FullName")))
		//						{
		//							// rsSR.OpenRecordset "SELECT * FROM SRMaster WHERE RSAccount = " & .Get_Fields("Account") & " AND RSCard = 1 AND SaleDate > #" & .Get_Fields("TransferFromBillingDateLast") & "#", strREDatabase
		//							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
		//							if (!modCLCalculations.NewOwner(rsD.Get_Fields_String("Name1"), rsD.Get_Fields("Account"), ref dtTransferFromBillDate))
		//							{
		//								if (modStatusPayments.Statics.boolRE)
		//								{
		//									// TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
		//									// TODO Get_Fields: Field [Own2FullName] not found!! (maybe it is an alias?)
		//									GetStatusName = rsD.Get_Fields("Own1FullName") + " " + rsD.Get_Fields("Own2FullName");
		//								}
		//								else
		//								{
		//									// TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
		//									GetStatusName = rsD.Get_Fields("Own1FullName");
		//								}
		//							}
		//							else
		//							{
		//								// TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
		//								GetStatusName = Strings.Trim(rsD.Get_Fields("Own1FullName")) + " Bill = " + rsD.Get_Fields_String("Name1") + " " + rsD.Get_Fields_String("Name2");
		//							}
		//						}
		//						else
		//						{
		//							if (modStatusPayments.Statics.boolRE)
		//							{
		//								// TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
		//								// TODO Get_Fields: Field [Own2FullName] not found!! (maybe it is an alias?)
		//								GetStatusName = rsD.Get_Fields("Own1FullName") + " " + rsD.Get_Fields("Own2FullName");
		//							}
		//							else
		//							{
		//								// TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
		//								GetStatusName = Strings.Trim(rsD.Get_Fields("Own1FullName"));
		//							}
		//						}
		//						break;
		//					}
		//			}
		//			//end switch
		//		}
		//		else
		//		{
		//			GetStatusName = rsD.Get_Fields_String("Name1") + " " + rsD.Get_Fields_String("Name2");
		//		}
		//		GetStatusName = strDel + GetStatusName;
		//		return GetStatusName;
		//	}
		//	catch (Exception ex)
		//	{
		//		// ERROR_HANDLER:
		//		FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In Get Status Name");
		//	}
		//	return GetStatusName;
		//}

		public class StaticVariables
		{
			// ********************************************************
			// LAST UPDATED   :               04/28/2006              *
			// MODIFIED BY    :               Jim Bertolino           *
			// *
			// DATE           :               10/12/2003              *
			// WRITTEN BY     :               Jim Bertolino           *
			// *
			// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
			// ********************************************************
			//=========================================================
			public int intNumberOfSQLFields;
			public int[] strFieldWidth = new int[20 + 1];
			public string strCustomSQL = "";
			public string strColumnCaptions = "";
			public string[] strFields = new string[20 + 1];
			public string[] strFieldCaptions = new string[20 + 1];
			public string[] strFieldNames = new string[50 + 1];
			// this is for the mortgage labels
			public string[] strCaptions = new string[20 + 1];
			public string strCustomTitle = string.Empty;
			public string strReportType = string.Empty;
			public string[,] strComboList = new string[20 + 1, 20 + 1];
			// VBto upgrade warning: strWhereType As string	OnWriteFCConvert.ToInt32(
			public string[] strWhereType = new string[20 + 1];
			public string[] strWhereNames = new string[20 + 1];
			public string[] strWhereFields = new string[20 + 1];
			public bool gboolShowPaymentsOnSL;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
