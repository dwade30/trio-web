﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmPurge.
	/// </summary>
	partial class frmPurge : BaseForm
	{
		public fecherFoundation.FCComboBox cmbREPP;
		public fecherFoundation.FCTextBox txtMaxAccounts;
		public fecherFoundation.FCFrame fraDate;
		public fecherFoundation.FCComboBox cmbYear;
		public T2KDateBox txtDate;
		public fecherFoundation.FCButton cmdPurge;
		public fecherFoundation.FCLabel lblYear;
		public fecherFoundation.FCLabel lblDateTitle;
		public fecherFoundation.FCLabel lblDate;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPurge));
            this.cmbREPP = new fecherFoundation.FCComboBox();
            this.txtMaxAccounts = new fecherFoundation.FCTextBox();
            this.fraDate = new fecherFoundation.FCFrame();
            this.cmbYear = new fecherFoundation.FCComboBox();
            this.txtDate = new Global.T2KDateBox();
            this.cmdPurge = new fecherFoundation.FCButton();
            this.lblYear = new fecherFoundation.FCLabel();
            this.lblDateTitle = new fecherFoundation.FCLabel();
            this.lblDate = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.lblREPP = new fecherFoundation.FCLabel();
            this.btnProcess = new fecherFoundation.FCButton();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraDate)).BeginInit();
            this.fraDate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPurge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 447);
            this.BottomPanel.Size = new System.Drawing.Size(460, 10);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.btnProcess);
            this.ClientArea.Controls.Add(this.lblREPP);
            this.ClientArea.Controls.Add(this.cmbREPP);
            this.ClientArea.Controls.Add(this.txtMaxAccounts);
            this.ClientArea.Controls.Add(this.fraDate);
            this.ClientArea.Size = new System.Drawing.Size(460, 387);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(460, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(137, 30);
            this.HeaderText.Text = "Purge Files";
            // 
            // cmbREPP
            // 
            this.cmbREPP.Items.AddRange(new object[] {
            "Real Estate Bills",
            "Personal Property Bills"});
            this.cmbREPP.Location = new System.Drawing.Point(158, 30);
            this.cmbREPP.Name = "cmbREPP";
            this.cmbREPP.Size = new System.Drawing.Size(210, 40);
            this.cmbREPP.TabIndex = 1;
            // 
            // txtMaxAccounts
            // 
            this.txtMaxAccounts.BackColor = System.Drawing.SystemColors.Window;
            this.txtMaxAccounts.Location = new System.Drawing.Point(385, 30);
            this.txtMaxAccounts.Name = "txtMaxAccounts";
            this.txtMaxAccounts.Size = new System.Drawing.Size(54, 40);
            this.txtMaxAccounts.TabIndex = 2;
            this.txtMaxAccounts.Text = "9999";
            this.ToolTip1.SetToolTip(this.txtMaxAccounts, "Maximum number of accounts to attempt to purge at once.");
            this.txtMaxAccounts.Visible = false;
            // 
            // fraDate
            // 
            this.fraDate.AppearanceKey = "groupBoxNoBorders";
            this.fraDate.Controls.Add(this.cmbYear);
            this.fraDate.Controls.Add(this.txtDate);
            this.fraDate.Controls.Add(this.cmdPurge);
            this.fraDate.Controls.Add(this.lblYear);
            this.fraDate.Controls.Add(this.lblDateTitle);
            this.fraDate.Controls.Add(this.lblDate);
            this.fraDate.Location = new System.Drawing.Point(0, 70);
            this.fraDate.Name = "fraDate";
            this.fraDate.Size = new System.Drawing.Size(457, 192);
            this.fraDate.TabIndex = 3;
            this.fraDate.UseMnemonic = false;
            this.fraDate.Visible = false;
            // 
            // cmbYear
            // 
            this.cmbYear.BackColor = System.Drawing.SystemColors.Window;
            this.cmbYear.Location = new System.Drawing.Point(158, 73);
            this.cmbYear.Name = "cmbYear";
            this.cmbYear.Size = new System.Drawing.Size(122, 40);
            this.cmbYear.TabIndex = 2;
            // 
            // txtDate
            // 
            this.txtDate.Location = new System.Drawing.Point(158, 133);
            this.txtDate.Mask = "##/##/####";
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new System.Drawing.Size(122, 22);
            this.txtDate.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.txtDate, "MM/dd/yyyy");
            this.txtDate.Visible = false;
            // 
            // cmdPurge
            // 
            this.cmdPurge.AppearanceKey = "actionButton";
            this.cmdPurge.Location = new System.Drawing.Point(302, 73);
            this.cmdPurge.Name = "cmdPurge";
            this.cmdPurge.Size = new System.Drawing.Size(94, 40);
            this.cmdPurge.TabIndex = 5;
            this.cmdPurge.Text = "Purge";
            this.cmdPurge.Visible = false;
            this.cmdPurge.Click += new System.EventHandler(this.cmdPurge_Click);
            // 
            // lblYear
            // 
            this.lblYear.AutoSize = true;
            this.lblYear.Location = new System.Drawing.Point(30, 87);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(45, 17);
            this.lblYear.TabIndex = 1;
            this.lblYear.Text = "YEAR";
            this.lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateTitle
            // 
            this.lblDateTitle.AutoSize = true;
            this.lblDateTitle.Location = new System.Drawing.Point(30, 147);
            this.lblDateTitle.Name = "lblDateTitle";
            this.lblDateTitle.Size = new System.Drawing.Size(44, 17);
            this.lblDateTitle.TabIndex = 3;
            this.lblDateTitle.Text = "DATE";
            this.lblDateTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblDateTitle.Visible = false;
            // 
            // lblDate
            // 
            this.lblDate.Location = new System.Drawing.Point(30, 30);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(409, 23);
            this.lblDate.TabIndex = 6;
            this.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // lblREPP
            // 
            this.lblREPP.AutoSize = true;
            this.lblREPP.Location = new System.Drawing.Point(30, 44);
            this.lblREPP.Name = "lblREPP";
            this.lblREPP.Size = new System.Drawing.Size(81, 17);
            this.lblREPP.TabIndex = 2;
            this.lblREPP.Text = "PROPERTY";
            this.lblREPP.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Location = new System.Drawing.Point(30, 304);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcess.Size = new System.Drawing.Size(154, 48);
            this.btnProcess.TabIndex = 1;
            this.btnProcess.Text = "Save & Preview";
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // frmPurge
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(460, 457);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmPurge";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Purge Files";
            this.Load += new System.EventHandler(this.frmPurge_Load);
            this.Activated += new System.EventHandler(this.frmPurge_Activated);
            this.Resize += new System.EventHandler(this.frmPurge_Resize);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPurge_KeyPress);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraDate)).EndInit();
            this.fraDate.ResumeLayout(false);
            this.fraDate.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPurge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCLabel lblREPP;
		private System.ComponentModel.IContainer components;
		private FCButton btnProcess;
	}
}
