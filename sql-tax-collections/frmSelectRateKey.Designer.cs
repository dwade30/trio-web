﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmSelectRateKey.
	/// </summary>
	partial class frmSelectRateKey : BaseForm
	{
		public fecherFoundation.FCGrid vsRate;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSelectRateKey));
            this.vsRate = new fecherFoundation.FCGrid();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.btnProcess = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 319);
            this.BottomPanel.Size = new System.Drawing.Size(1054, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.vsRate);
            this.ClientArea.Size = new System.Drawing.Size(1054, 259);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(1054, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(335, 30);
            this.HeaderText.Text = "MULTIPLE RATE RECORDS";
            // 
            // vsRate
            // 
            this.vsRate.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsRate.Cols = 7;
            this.vsRate.FixedCols = 0;
            this.vsRate.Location = new System.Drawing.Point(30, 30);
            this.vsRate.Name = "vsRate";
            this.vsRate.RowHeadersVisible = false;
            this.vsRate.Rows = 1;
            this.vsRate.Size = new System.Drawing.Size(1004, 209);
            this.vsRate.TabIndex = 1;
            this.vsRate.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsRate_ValidateEdit);
            this.vsRate.CurrentCellChanged += new System.EventHandler(this.vsRate_RowColChange);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Location = new System.Drawing.Point(434, 30);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcess.Size = new System.Drawing.Size(187, 48);
            this.btnProcess.TabIndex = 1;
            this.btnProcess.Text = "Save and Continue";
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // frmSelectRateKey
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1054, 427);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmSelectRateKey";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Create Tax Service Export";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmSelectRateKey_Load);
            this.Resize += new System.EventHandler(this.frmSelectRateKey_Resize);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmSelectRateKey_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton btnProcess;
	}
}
