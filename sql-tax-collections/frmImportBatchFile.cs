﻿using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmImportBatch.
	/// </summary>
	public partial class frmImportBatch : BaseForm
	{
		public frmImportBatch()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void frmImportBatch_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = 0;
				mnuExit_Click();
			}
		}

		private void frmImportBatch_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmImportBatch.Icon	= "frmImportBatchFile.frx":0000";
			//frmImportBatch.FillStyle	= 0;
			//frmImportBatch.ScaleWidth	= 5790;
			//frmImportBatch.ScaleHeight	= 4050;
			//frmImportBatch.LinkTopic	= "Form2";
			//frmImportBatch.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "9";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//txtPaidBy.MaxLength	= 25;
			//txtTellerID.MaxLength	= 3;
			//txtPaymentDate2.BorderStyle	= 1;
			//txtPaymentDate2.Appearance	= 1;
			//txtPaymentDate2.MaxLength	= 10;
			//txtEffectiveDate.BorderStyle	= 1;
			//txtEffectiveDate.Appearance	= 1;
			//txtEffectiveDate.MaxLength	= 10;
			//vsElasticLight1.OleObjectBlob	= "frmImportBatchFile.frx":058A";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			FillYearCmb();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			string strTellerID;
			string strPaymentDate = "";
			strTellerID = "";
			string strEffectiveDate = "";
			string strFileName;
			if (Strings.Trim(txtTellerID.Text) != "")
			{
				if (modGlobal.ValidateTellerID(Strings.Trim(txtTellerID.Text)) <= 1)
				{
					strTellerID = Strings.Trim(txtTellerID.Text);
				}
				else
				{
					FCMessageBox.Show("Invalid teller id entered", MsgBoxStyle.Exclamation, "Invalid Teller");
					return;
				}
			}
			else
			{
				FCMessageBox.Show("You must enter a valid teller id", MsgBoxStyle.Exclamation, "Invalid Teller");
				return;
			}
			if (Strings.Trim(txtEffectiveDate.Text) != "")
			{
				if (Information.IsDate(txtEffectiveDate.Text))
				{
					strEffectiveDate = txtEffectiveDate.Text;
				}
				else
				{
					FCMessageBox.Show("You must enter a valid effective interest date", MsgBoxStyle.Exclamation, "Invalid Date");
					return;
				}
			}
			else
			{
				FCMessageBox.Show("You must enter an effective interest date", MsgBoxStyle.Exclamation, "Invalid Date");
				return;
			}
			if (Strings.Trim(txtPaymentDate2.Text) != "")
			{
				if (Information.IsDate(txtPaymentDate2.Text))
				{
					strPaymentDate = txtPaymentDate2.Text;
				}
				else
				{
					FCMessageBox.Show("You must enter a valid payment date", MsgBoxStyle.Exclamation, "Invalid Date");
				}
			}
			else
			{
				FCMessageBox.Show("You must enter a payment date", MsgBoxStyle.Exclamation, "Invalid Date");
				return;
			}
			if (Strings.Trim(txtFileName.Text) == "")
			{
				FCMessageBox.Show("You must select a file", MsgBoxStyle.Exclamation, "Invalid Filename");
				return;
			}
			if (Strings.Trim(txtPaidBy.Text) == "" && modExtraModules.IsThisCR())
			{
				FCMessageBox.Show("You must enter a paid by name", MsgBoxStyle.Exclamation, "Invalid Name");
				return;
			}
			if (!File.Exists(Strings.Trim(txtFileName.Text)))
			{
				FCMessageBox.Show("Could not find file " + Strings.Trim(txtFileName.Text), MsgBoxStyle.Critical, "File Not Found");
				return;
			}
			strFileName = Strings.Trim(txtFileName.Text);
			string strPayer;
			strPayer = Strings.Trim(txtPaidBy.Text);
		}

		private void FillYearCmb()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset("select [YEAR] from raterec GROUP by [year] order by [year] desc", modExtraModules.strCLDatabase);
			cmbTaxYear.Clear();
			while (!rsLoad.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [year] and replace with corresponding Get_Field method
				cmbTaxYear.AddItem(FCConvert.ToString(rsLoad.Get_Fields("year")));
				rsLoad.MoveNext();
			}
		}

		private void ImportCLBatchPaymentFile(string strImportFile, int intYear, DateTime dtEffectiveDate, DateTime dtPaymentDate, string strPaidBy, string strTeller)
		{
			try
			{
				// On Error GoTo ErrorHandler
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In ImportCLBatchPaymentFile", MsgBoxStyle.Critical, "Error");
			}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuSaveContinue_Click(sender, e);
		}
	}
}
