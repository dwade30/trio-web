﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmDateChange.
	/// </summary>
	partial class frmDateChange : fecherFoundation.FCForm
	{
		public fecherFoundation.FCCheckBox chkEffect;
		public T2KDateBox txtDate;
		public fecherFoundation.FCButton cmdQuit;
		public fecherFoundation.FCButton cmdOk;
		public fecherFoundation.FCLabel lblInstructions;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			//if (_InstancePtr == this)
			//{
			//	_InstancePtr = null;
			//	Sys.ClearInstance(this);
			//}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDateChange));
            this.chkEffect = new fecherFoundation.FCCheckBox();
            this.cmdQuit = new fecherFoundation.FCButton();
            this.cmdOk = new fecherFoundation.FCButton();
            this.lblInstructions = new fecherFoundation.FCLabel();
            this.txtDate = new Global.T2KDateBox();
            ((System.ComponentModel.ISupportInitialize)(this.chkEffect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            this.SuspendLayout();
            // 
            // chkEffect
            // 
            this.chkEffect.Checked = true;
            this.chkEffect.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
            this.chkEffect.Location = new System.Drawing.Point(20, 130);
            this.chkEffect.Name = "chkEffect";
            this.chkEffect.Size = new System.Drawing.Size(242, 24);
            this.chkEffect.TabIndex = 2;
            this.chkEffect.Text = "Affect Recorded Transaction Date";
            this.chkEffect.Visible = false;
            // 
            // cmdQuit
            // 
            this.cmdQuit.AppearanceKey = "actionButton";
            this.cmdQuit.Location = new System.Drawing.Point(157, 178);
            this.cmdQuit.Name = "cmdQuit";
            this.cmdQuit.Size = new System.Drawing.Size(87, 40);
            this.cmdQuit.TabIndex = 4;
            this.cmdQuit.Text = "Cancel";
            this.cmdQuit.Click += new System.EventHandler(this.cmdQuit_Click);
            // 
            // cmdOk
            // 
            this.cmdOk.AppearanceKey = "actionButton";
            this.cmdOk.DialogResult = Wisej.Web.DialogResult.OK;
            this.cmdOk.Location = new System.Drawing.Point(50, 178);
            this.cmdOk.Name = "cmdOk";
            this.cmdOk.Size = new System.Drawing.Size(87, 40);
            this.cmdOk.TabIndex = 3;
            this.cmdOk.Text = "Process";
            this.cmdOk.Click += new System.EventHandler(this.cmdOk_Click);
            // 
            // lblInstructions
            // 
            this.lblInstructions.Location = new System.Drawing.Point(38, 25);
            this.lblInstructions.Name = "lblInstructions";
            this.lblInstructions.Size = new System.Drawing.Size(229, 40);
            this.lblInstructions.Text = "PLEASE ENTER DATE TO USE FOR PROCESSING THIS TRANSACTION";
            this.lblInstructions.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // txtDate
            // 
            this.txtDate.Location = new System.Drawing.Point(90, 72);
            this.txtDate.Mask = "##/##/####";
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new System.Drawing.Size(122, 22);
            this.txtDate.TabIndex = 1;
            this.txtDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtDate_Validating);
            // 
            // frmDateChange
            // 
            this.AcceptButton = this.cmdOk;
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(318, 245);
            this.Controls.Add(this.chkEffect);
            this.Controls.Add(this.txtDate);
            this.Controls.Add(this.cmdQuit);
            this.Controls.Add(this.cmdOk);
            this.Controls.Add(this.lblInstructions);
            this.FillColor = 16777215;
            this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDateChange";
            this.ShowInTaskbar = false;
            this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
            this.Text = "Date Change";
            this.Load += new System.EventHandler(this.frmDateChange_Load);
            this.Activated += new System.EventHandler(this.frmDateChange_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmDateChange_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmDateChange_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.chkEffect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion
	}
}
