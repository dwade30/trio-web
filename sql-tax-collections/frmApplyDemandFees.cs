﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmApplyDemandFees.
	/// </summary>
	public partial class frmApplyDemandFees : BaseForm
	{
		public frmApplyDemandFees()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmApplyDemandFees InstancePtr
		{
			get
			{
				return (frmApplyDemandFees)Sys.GetInstance(typeof(frmApplyDemandFees));
			}
		}

		protected frmApplyDemandFees _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/26/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/16/2006              *
		// ********************************************************
		int intAction;
		clsDRWrapper rsData = new clsDRWrapper();
		bool boolLoaded;
		double dblDemand;
		double dblCertMailFee;
		bool boolChargeMort;
		bool boolChargeCert;
		bool boolChargeForNewOwner;
		DateTime dtMailDate;
		clsDRWrapper rsRE = new clsDRWrapper();
		int lngBillingYear;
		string strRateKey;
		bool blnChargeIntParty;

		public void Init(string strRateKeyList)
		{
			strRateKey = strRateKeyList;
			//FC:FINAL:KS: #133:The form does not open 
			//this.Show();
			this.Show(App.MainForm);
		}

		private void frmApplyDemandFees_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded && !this.IsDisposed)
			{
				boolLoaded = true;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				this.Text = "Apply Demand Fees";
				lblInstructionHeader.Visible = true;
				// False 'commented back in from a bug fix #4854
				lblInstruction.Visible = true;
				lblValidateInstruction.Visible = true;
				lblInstruction.Text = "To Apply Demand Fees:" + "\r\n" + "     1. Check the box beside the account." + "\r\n" + "     2. Select 'Apply Demand Fees' or press F12.";
				lblValidateInstruction.Text = "If these values are correct, Press F12 to advance.  If not, then please rerun your 30 Day Notices.";
				App.DoEvents();
				FillValidateGrid();
				FillDemandGrid();
				//FC:FINAL:AM: the form might be closed in FillDemandGrid
				if (!this.IsDisposed)
				{
					ShowGridFrame();
					SetAct(0);
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				}
			}
		}

		private void frmApplyDemandFees_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmApplyDemandFees.Icon	= "frmApplyDemandFees.frx":0000";
			//frmApplyDemandFees.FillStyle	= 0;
			//frmApplyDemandFees.ScaleWidth	= 9195;
			//frmApplyDemandFees.ScaleHeight	= 7875;
			//frmApplyDemandFees.LinkTopic	= "Form2";
			//frmApplyDemandFees.LockControls	= -1  'True;
			//frmApplyDemandFees.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "9.75";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//vsValidate.BackColor	= "-2147483643";
			//			//vsValidate.ForeColor	= "-2147483640";
			//vsValidate.BorderStyle	= 1;
			//vsValidate.FillStyle	= 0;
			//vsValidate.Appearance	= 1;
			//vsValidate.GridLines	= 1;
			//vsValidate.WordWrap	= 0;
			//vsValidate.ScrollBars	= 3;
			//vsValidate.RightToLeft	= 0;
			//vsValidate._cx	= 11139;
			//vsValidate._cy	= 7971;
			//vsValidate._ConvInfo	= 1;
			//vsValidate.MousePointer	= 0;
			//vsValidate.BackColorFixed	= -2147483633;
			//			//vsValidate.ForeColorFixed	= -2147483630;
			//vsValidate.BackColorSel	= -2147483635;
			//			//vsValidate.ForeColorSel	= -2147483634;
			//vsValidate.BackColorBkg	= -2147483636;
			//vsValidate.BackColorAlternate	= -2147483643;
			//vsValidate.GridColor	= -2147483633;
			//vsValidate.GridColorFixed	= -2147483632;
			//vsValidate.TreeColor	= -2147483632;
			//vsValidate.FloodColor	= 192;
			//vsValidate.SheetBorder	= -2147483642;
			//vsValidate.FocusRect	= 1;
			//vsValidate.HighLight	= 1;
			//vsValidate.AllowSelection	= -1  'True;
			//vsValidate.AllowBigSelection	= -1  'True;
			//vsValidate.AllowUserResizing	= 0;
			//vsValidate.SelectionMode	= 0;
			//vsValidate.GridLinesFixed	= 2;
			//vsValidate.GridLineWidth	= 1;
			//vsValidate.RowHeightMin	= 0;
			//vsValidate.RowHeightMax	= 0;
			//vsValidate.ColWidthMin	= 0;
			//vsValidate.ColWidthMax	= 0;
			//vsValidate.ExtendLastCol	= -1  'True;
			//vsValidate.FormatString	= "";
			//vsValidate.ScrollTrack	= -1  'True;
			//vsValidate.ScrollTips	= 0   'False;
			//vsValidate.MergeCells	= 0;
			//vsValidate.MergeCompare	= 0;
			//vsValidate.AutoResize	= -1  'True;
			//vsValidate.AutoSizeMode	= 0;
			//vsValidate.AutoSearch	= 0;
			//vsValidate.AutoSearchDelay	= 2;
			//vsValidate.MultiTotals	= -1  'True;
			//vsValidate.SubtotalPosition	= 1;
			//vsValidate.OutlineBar	= 0;
			//vsValidate.OutlineCol	= 0;
			//vsValidate.Ellipsis	= 0;
			//vsValidate.ExplorerBar	= 0;
			//vsValidate.PicturesOver	= 0   'False;
			//vsValidate.PictureType	= 0;
			//vsValidate.TabBehavior	= 0;
			//vsValidate.OwnerDraw	= 0;
			//vsValidate.ShowComboButton	= -1  'True;
			//vsValidate.TextStyle	= 0;
			//vsValidate.TextStyleFixed	= 0;
			//vsValidate.OleDragMode	= 0;
			//vsValidate.OleDropMode	= 0;
			//vsValidate.ComboSearch	= 3;
			//vsValidate.AutoSizeMouse	= -1  'True;
			//vsValidate.AllowUserFreezing	= 0;
			//vsValidate.BackColorFrozen	= 0;
			//			//vsValidate.ForeColorFrozen	= 0;
			//vsValidate.WallPaperAlignment	= 9;
			//vsDemand.BackColor	= "-2147483643";
			//			//vsDemand.ForeColor	= "-2147483640";
			//vsDemand.BorderStyle	= 1;
			//vsDemand.FillStyle	= 0;
			//vsDemand.Appearance	= 1;
			//vsDemand.GridLines	= 1;
			//vsDemand.WordWrap	= 0;
			//vsDemand.ScrollBars	= 3;
			//vsDemand.RightToLeft	= 0;
			//vsDemand._cx	= 15584;
			//vsDemand._cy	= 8744;
			//vsDemand._ConvInfo	= 1;
			//vsDemand.MousePointer	= 0;
			//vsDemand.BackColorFixed	= -2147483633;
			//			//vsDemand.ForeColorFixed	= -2147483630;
			//vsDemand.BackColorSel	= -2147483635;
			//			//vsDemand.ForeColorSel	= -2147483634;
			//vsDemand.BackColorBkg	= -2147483636;
			//vsDemand.BackColorAlternate	= -2147483643;
			//vsDemand.GridColor	= -2147483633;
			//vsDemand.GridColorFixed	= -2147483632;
			//vsDemand.TreeColor	= -2147483632;
			//vsDemand.FloodColor	= 192;
			//vsDemand.SheetBorder	= -2147483642;
			//vsDemand.FocusRect	= 1;
			//vsDemand.HighLight	= 1;
			//vsDemand.AllowSelection	= -1  'True;
			//vsDemand.AllowBigSelection	= -1  'True;
			//vsDemand.AllowUserResizing	= 0;
			//vsDemand.SelectionMode	= 0;
			//vsDemand.GridLinesFixed	= 2;
			//vsDemand.GridLineWidth	= 1;
			//vsDemand.Rows	= 1;
			//vsDemand.Cols	= 5;
			//vsDemand.FixedRows	= 1;
			//vsDemand.FixedCols	= 0;
			//vsDemand.RowHeightMin	= 0;
			//vsDemand.RowHeightMax	= 0;
			//vsDemand.ColWidthMin	= 0;
			//vsDemand.ColWidthMax	= 0;
			//vsDemand.ExtendLastCol	= -1  'True;
			//vsDemand.FormatString	= "";
			//vsDemand.ScrollTrack	= -1  'True;
			//vsDemand.ScrollTips	= 0   'False;
			//vsDemand.MergeCells	= 0;
			//vsDemand.MergeCompare	= 0;
			//vsDemand.AutoResize	= -1  'True;
			//vsDemand.AutoSizeMode	= 0;
			//vsDemand.AutoSearch	= 0;
			//vsDemand.AutoSearchDelay	= 2;
			//vsDemand.MultiTotals	= -1  'True;
			//vsDemand.SubtotalPosition	= 1;
			//vsDemand.OutlineBar	= 0;
			//vsDemand.OutlineCol	= 0;
			//vsDemand.Ellipsis	= 0;
			//vsDemand.ExplorerBar	= 1;
			//vsDemand.PicturesOver	= 0   'False;
			//vsDemand.PictureType	= 0;
			//vsDemand.TabBehavior	= 0;
			//vsDemand.OwnerDraw	= 0;
			//vsDemand.Editable	= 0;
			//vsDemand.ShowComboButton	= -1  'True;
			//vsDemand.TextStyle	= 0;
			//vsDemand.TextStyleFixed	= 0;
			//vsDemand.OleDragMode	= 0;
			//vsDemand.OleDropMode	= 0;
			//vsDemand.DataMode	= 0;
			//vsDemand.VirtualData	= -1  'True;
			//vsDemand.ComboSearch	= 3;
			//vsDemand.AutoSizeMouse	= -1  'True;
			//vsDemand.FrozenRows	= 0;
			//vsDemand.FrozenCols	= 0;
			//vsDemand.AllowUserFreezing	= 0;
			//vsDemand.BackColorFrozen	= 0;
			//			//vsDemand.ForeColorFrozen	= 0;
			//vsDemand.WallPaperAlignment	= 9;
			//vsElasticLight1.OleObjectBlob	= "frmApplyDemandFees.frx":058A";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			FCUtils.EraseSafe(modGlobal.Statics.arrDemand);
		}

		private void frmApplyDemandFees_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
            FormUtilities.KeyPressHandler(e, this);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		private void frmApplyDemandFees_Resize(object sender, System.EventArgs e)
		{
			FormatGrid(true);
			if (fraGrid.Visible)
			{
				ShowGridFrame();
			}
			else
			{
				ShowValidateFrame();
			}
		}

		private void mnuFileClear_Click(object sender, System.EventArgs e)
		{
			ClearCheckBoxes();
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void ShowValidateFrame()
		{
			//FC:FINAL:BCU #i191 - no need to adjust size in code, fixed in designer
			// this will show/center the frame with the grid on it
			//fraValidate.Top = FCConvert.ToInt32((this.Height - fraValidate.Height) / 3.0);
			//fraValidate.Left = FCConvert.ToInt32((this.Width - fraValidate.Width) / 2.0);
			fraValidate.Visible = true;
			//FC:FINAL:BCU #i191 - no need to adjust size in code, fixed in designer
			// this will set the height of the grid
			//if (vsValidate.Rows > 1)
			//{
			//    vsValidate.Height = ((vsValidate.Rows - 1) * vsValidate.RowHeight(1)) + 70;
			//}
		}

		private void ShowGridFrame()
		{
			// this will show/center the frame with the grid on it
			//FC:FINAL:RPU- no need to adjust size in code, fixed in designer
			//fraGrid.Top = FCConvert.ToInt32((this.Height - fraGrid.Height) / 3.0);
			//fraGrid.Left = FCConvert.ToInt32((this.Width - fraGrid.Width) / 2.0);
			fraGrid.Visible = true;
			//if (vsDemand.Rows * vsDemand.RowHeight(0) > fraGrid.Height - lblInstruction.Height - 500)
			//{
			//	vsDemand.Height = fraGrid.Height - lblInstruction.Height - 500;
			//	vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			//}
			//else
			//{
			//	vsDemand.Height = vsDemand.Rows * vsDemand.RowHeight(0) + 70;
			//	vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			//}
		}

		private void FormatGrid(bool boolResize = false)
		{
			int wid = 0;
			if (!boolResize)
			{
				vsDemand.Rows = 1;
			}
			vsDemand.Cols = 7;
			wid = vsDemand.WidthOriginal;
			vsDemand.ColWidth(0, FCConvert.ToInt32(wid * 0.1));
			// checkbox
			vsDemand.ColWidth(1, FCConvert.ToInt32(wid * 0.15));
			// Acct
			vsDemand.ColWidth(2, FCConvert.ToInt32(wid * 0.43));
			// Name
			vsDemand.ColWidth(3, FCConvert.ToInt32(wid * 0.17));
			// Total Notices Sent
			vsDemand.ColWidth(4, FCConvert.ToInt32(wid * 0.1));
			// Total Demand Fees
			vsDemand.ColWidth(5, 0);
			// Hidden Key Field
			vsDemand.ColWidth(6, 0);
			// Hidden Code Field - 0 is ok to process...anything else is bad
			vsDemand.ColFormat(4, "#,##0.00");
			vsDemand.ColDataType(0, FCGrid.DataTypeSettings.flexDTBoolean);
			vsDemand.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsDemand.TextMatrix(0, 1, "Account");
			vsDemand.TextMatrix(0, 2, "Name");
			vsDemand.TextMatrix(0, 3, "Charged Notices");
			vsDemand.TextMatrix(0, 4, "Amount");
			vsValidate.Cols = 3;
			wid = vsValidate.WidthOriginal;
			vsValidate.ColWidth(0, FCConvert.ToInt32(wid * 0.7));
			// Question
			vsValidate.ColWidth(1, FCConvert.ToInt32(wid * 0.28));
			// Answer
			vsValidate.ColWidth(2, 0);
			// Hidden field
			vsValidate.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsValidate.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsValidate.TextMatrix(0, 1, "Parameters");
			vsValidate.TextMatrix(0, 2, "Value");
		}

		private void FillDemandGrid()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the grid with eligible accounts
				clsDRWrapper rsRK = new clsDRWrapper();
				DateTime dtBillDate;
				string strSQL = "";
				int lngIndex;
				double dblXInt = 0;
				string strWhereClause = "";
				int lngMH = 0;
				int lngCopies = 0;
				double dblAmount = 0;
				int lngIP = 0;
				clsDRWrapper rsIP = new clsDRWrapper();
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Info");
				vsDemand.Rows = 1;
				if (frmRateRecChoice.InstancePtr.cmbRange.SelectedIndex == 2)
				{
					// range of accounts
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[0].Text) != "")
					{
						if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
						{
							// both full
							strWhereClause = " AND Account >= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text)) + " AND Account <= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
						}
						else
						{
							// first full second empty
							strWhereClause = " AND Account >= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text));
						}
					}
					else
					{
						if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
						{
							// first empty second full
							strWhereClause = " AND Account <= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
						}
						else
						{
							// both empty
							strWhereClause = "";
						}
					}
				}
				else if (frmRateRecChoice.InstancePtr.cmbRange.SelectedIndex == 1)
				{
					// range of names
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[0].Text) != "")
					{
						if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
						{
							// both full
							strWhereClause = " AND Name1 >= '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "   ' AND Name1 <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "ZZZ'";
						}
						else
						{
							// first full second empty
							strWhereClause = " AND Name1 >= '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "   '";
						}
					}
					else
					{
						if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
						{
							// first empty second full
							strWhereClause = " AND Name1 <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "ZZZ'";
						}
						else
						{
							// both empty
							strWhereClause = "";
						}
					}
				}
				else
				{
				}
				if (Strings.Trim(strRateKey) == "")
				{
					strSQL = "SELECT * FROM BillingMaster WHERE LienProcessStatus = 1 AND LienStatusEligibility >= 2 AND BillingYear / 10 = " + FCConvert.ToString(lngBillingYear) + strWhereClause + " AND BillingType = 'RE' ORDER BY Name1, Account";
					// this will be all the records that have had 30 Day Notices printed
				}
				else
				{
					strSQL = "SELECT * FROM BillingMaster WHERE LienProcessStatus = 1 AND LienStatusEligibility >= 2 AND BillingYear / 10 = " + FCConvert.ToString(lngBillingYear) + strWhereClause + " AND BillingType = 'RE' AND RateKey IN " + strRateKey + " ORDER BY Name1, Account";
					// this will be all the records that have had 30 Day Notices printed
				}
				// strSQL = "SELECT * FROM BillingMaster WHERE LienProcessStatus = 1 AND LienStatusEligibility >= 2  AND BillingType = 'RE' ORDER BY Name1"     'this will be all the records that have had 30 Day Notices printed
				rsData.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
				rsRE.OpenRecordset("SELECT * FROM " + modGlobal.Statics.strDbRE + " Master");
				if (rsData.EndOfFile())
				{
					frmWait.InstancePtr.Unload();
					FCMessageBox.Show("There are no accounts eligible to have demand fees applied.", MsgBoxStyle.Information, "No Eligible Accounts");
					Close();
					return;
				}
				lngIndex = -1;
				while (!rsData.EndOfFile())
				{
					App.DoEvents();
					// MAL@20080408: Correct to reset amount to 0
					// Tracker Reference: 13117
					// Reset Values
					dblAmount = 0;
					dblXInt = 0;
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (modCLCalculations.CalculateAccountCL(ref rsData, FCConvert.ToInt32(rsData.Get_Fields("Account")), dtMailDate, ref dblXInt) <= 0)
					{
						// skip this account
						goto SKIP;
					}
					// add a row/element
					vsDemand.AddItem("");
					Array.Resize(ref modGlobal.Statics.arrDemand, vsDemand.Rows - 1 + 1);
					// this will make sure that there are enough elements to use
					lngIndex = vsDemand.Rows - 2;
					//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
					modGlobal.Statics.arrDemand[lngIndex] = new modGlobal.AccountFeesAdded(0);
					modGlobal.Statics.arrDemand[lngIndex].Used = true;
					modGlobal.Statics.arrDemand[lngIndex].Processed = false;
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					vsDemand.TextMatrix(vsDemand.Rows - 1, 1, FCConvert.ToString(rsData.Get_Fields("Account")));
					// account number
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					modGlobal.Statics.arrDemand[lngIndex].Account = FCConvert.ToInt32(rsData.Get_Fields("Account"));
					vsDemand.TextMatrix(vsDemand.Rows - 1, 2, FCConvert.ToString(rsData.Get_Fields_String("Name1")));
					// name
					modGlobal.Statics.arrDemand[lngIndex].Name = FCConvert.ToString(rsData.Get_Fields_String("Name1"));
					// TODO Get_Fields: Check the table for the column [Copies] and replace with corresponding Get_Field method
					vsDemand.TextMatrix(vsDemand.Rows - 1, 3, FCConvert.ToString(rsData.Get_Fields("Copies")));
					// number of copies
					// amount
					if (rsData.Get_Fields_Decimal("DemandFees") > 0)
					{
						// highlight the row and set the value = zero
						//						vsDemand.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vsDemand.Rows - 1, 0, vsDemand.Rows - 1, vsDemand.Cols - 1, Color.Red);
						vsDemand.TextMatrix(vsDemand.Rows - 1, 4, FCConvert.ToString(0));
						vsDemand.TextMatrix(vsDemand.Rows - 1, 6, FCConvert.ToString(-1));
					}
					else
					{
						// gonna have to figure out the amount
						modGlobal.Statics.arrDemand[lngIndex].Fee = dblDemand;
						if (boolChargeCert)
						{
							if (boolChargeMort)
							{
								// MAL@20071127
								lngMH = CalculateMortgageHolders();
								// TODO Get_Fields: Check the table for the column [Copies] and replace with corresponding Get_Field method
								lngCopies = FCConvert.ToInt32(rsData.Get_Fields("Copies"));
								if (lngMH > 0)
								{
									if (lngCopies > lngMH)
									{
										// TODO Get_Fields: Check the table for the column [Copies] and replace with corresponding Get_Field method
										lngCopies = FCConvert.ToInt32(rsData.Get_Fields("Copies")) - lngMH;
										dblAmount = (lngMH * dblCertMailFee);
									}
								}
								else
								{
									lngCopies = lngCopies;
								}
								// Check for Interested Parties
								if (blnChargeIntParty)
								{
									// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
									lngIP = modCLCalculations.CalculateInterestedParties(FCConvert.ToInt32(rsData.Get_Fields("Account")), rsIP);
									if (lngIP > 0)
									{
										dblAmount += (lngIP * dblCertMailFee);
									}
									else
									{
										dblAmount = dblAmount;
									}
								}
								// Check for New Owner
								rsRK.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(rsData.Get_Fields_Int32("RateKey")), modExtraModules.strCLDatabase);
								if (!rsRK.EndOfFile())
								{
									dtBillDate = (DateTime)rsRK.Get_Fields_DateTime("BillingDate");
								}
								else
								{
									dtBillDate = (DateTime)rsData.Get_Fields_DateTime("TransferFromBillingDateFirst");
								}
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								if (boolChargeForNewOwner && modCLCalculations.NewOwner2(FCConvert.ToString(rsData.Get_Fields_String("Name1")), FCConvert.ToInt32(rsData.Get_Fields("Account")), dtBillDate))
								{
									dblAmount += (dblCertMailFee * 2);
								}
								else
								{
									dblAmount += dblCertMailFee;
								}
								vsDemand.TextMatrix(vsDemand.Rows - 1, 4, FCConvert.ToString(dblAmount + dblDemand));
								// .TextMatrix(.rows - 1, 4) = (rsData.Get_Fields("Copies") * dblCertMailFee) + dblDemand
								modGlobal.Statics.arrDemand[lngIndex].CertifiedMailFee = FCConvert.ToDouble(vsDemand.TextMatrix(vsDemand.Rows - 1, 4));
							}
							else
							{
								// Interested Party
								if (blnChargeIntParty)
								{
									// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
									lngIP = modCLCalculations.CalculateInterestedParties(FCConvert.ToInt32(rsData.Get_Fields("Account")), rsIP);
									if (lngIP > 0)
									{
										dblAmount += (lngIP * dblCertMailFee);
									}
									else
									{
										dblAmount = dblAmount;
									}
								}
								// may have to add the new owner charge here...
								rsRK.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(rsData.Get_Fields_Int32("RateKey")), modExtraModules.strCLDatabase);
								if (!rsRK.EndOfFile())
								{
									dtBillDate = (DateTime)rsRK.Get_Fields_DateTime("BillingDate");
								}
								else
								{
									dtBillDate = (DateTime)rsData.Get_Fields_DateTime("TransferFromBillingDateFirst");
								}
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								if (boolChargeForNewOwner && modCLCalculations.NewOwner2(FCConvert.ToString(rsData.Get_Fields_String("Name1")), FCConvert.ToInt32(rsData.Get_Fields("Account")),  dtBillDate))
								{
									vsDemand.TextMatrix(vsDemand.Rows - 1, 4, FCConvert.ToString((dblCertMailFee * 2) + dblDemand + dblAmount));
									modGlobal.Statics.arrDemand[lngIndex].CertifiedMailFee = FCConvert.ToDouble(vsDemand.TextMatrix(vsDemand.Rows - 1, 4));
								}
								else
								{
									vsDemand.TextMatrix(vsDemand.Rows - 1, 4, FCConvert.ToString(dblCertMailFee + dblDemand + dblAmount));
									modGlobal.Statics.arrDemand[lngIndex].CertifiedMailFee = FCConvert.ToDouble(vsDemand.TextMatrix(vsDemand.Rows - 1, 4));
								}
							}
						}
						else
						{
							vsDemand.TextMatrix(vsDemand.Rows - 1, 4, FCConvert.ToString(dblDemand));
							modGlobal.Statics.arrDemand[lngIndex].CertifiedMailFee = 0;
						}
					}
					vsDemand.TextMatrix(vsDemand.Rows - 1, 5, FCConvert.ToString(rsData.Get_Fields_Int32("ID")));
					// billkey
					SKIP:
					;
					rsData.MoveNext();
				}
				if (lngIndex < 0)
				{
					frmWait.InstancePtr.Unload();
					FCMessageBox.Show("There are no accounts eligible to have demand fees applied.", MsgBoxStyle.Information, "No Eligible Accounts");
					Close();
				}
				// check all of the accounts
				mnuFileSelectAll_Click();
				//FC:FINAL:AM: don't set the height; used anchoring instead
				//if (vsDemand.Rows * vsDemand.RowHeight(0) > fraGrid.Height - lblInstruction.Height - 500)
				//{
				//	vsDemand.Height = fraGrid.Height - lblInstruction.Height - 500;
				//	vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
				//}
				//else
				//{
				//	vsDemand.Height = vsDemand.Rows * vsDemand.RowHeight(0) + 70;
				//	vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				//}
				frmWait.InstancePtr.Unload();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Filling Grid");
			}
		}

		private void FillValidateGrid()
		{
			// this will fill the validate grid with the information from the Control_30DayNotice table
			// which is the information that was used to print the 30 Day Notices the last time
			clsDRWrapper rsValidate = new clsDRWrapper();
			vsValidate.Rows = 1;
			rsValidate.OpenRecordset("SELECT * FROM Control_30DayNotice");
			if (rsValidate.EndOfFile())
			{
				FCMessageBox.Show("Please return to and run 'Print 30 Day Notices'.", MsgBoxStyle.Information, "No Control Record");
				Close();
			}
			else
			{
				lngBillingYear = FCConvert.ToInt32(rsValidate.Get_Fields_Int32("BillingYear"));
				vsValidate.AddItem("Billing Year" + "\t" + FCConvert.ToString(rsValidate.Get_Fields_Int32("BillingYear")));
				// vsValidate.AddItem "Recorded Date" & vbTab & .Get_Fields("DateCreated")
				dtMailDate = (DateTime)rsValidate.Get_Fields_DateTime("MailDate");
				vsValidate.AddItem("Interest/Mailing Date" + "\t" + FCConvert.ToString(rsValidate.Get_Fields_DateTime("MailDate")));
				vsValidate.AddItem("Demand Fee" + "\t" + Strings.Format(rsValidate.Get_Fields_Double("Demand"), "#,##0.00"));
				dblDemand = Conversion.Val(rsValidate.Get_Fields_Double("Demand"));
				boolChargeMort = FCConvert.ToBoolean(rsValidate.Get_Fields_Boolean("ChargeForMortHolder"));
				boolChargeCert = FCConvert.ToBoolean(rsValidate.Get_Fields_Boolean("PayCertMailFee"));
				if (boolChargeCert)
				{
					vsValidate.AddItem("Cert Mail Fee" + "\t" + Strings.Format(rsValidate.Get_Fields_Double("CertMailFee"), "#,##0.00"));
					dblCertMailFee = Conversion.Val(rsValidate.Get_Fields_Double("CertMailFee"));
					vsValidate.AddItem("Charge taxpayer for Cert Mail Fee?" + "\t" + "Yes");
				}
				else
				{
					if (boolChargeMort)
					{
						vsValidate.AddItem("Cert Mail Fee" + "\t" + FCConvert.ToString(rsValidate.Get_Fields_Double("CertMailFee")));
					}
					else
					{
						vsValidate.AddItem("Cert Mail Fee" + "\t" + "0.00");
					}
					vsValidate.AddItem("Charge taxpayer for Cert Mail Fee?" + "\t" + "No");
					dblCertMailFee = 0;
				}
				if (FCConvert.ToBoolean(rsValidate.Get_Fields_Boolean("SendCopyToMortHolder")))
				{
					if (boolChargeMort)
					{
						vsValidate.AddItem("Charge for each mortgage holder?" + "\t" + "Yes");
					}
					else
					{
						vsValidate.AddItem("Charge for each mortgage holder?" + "\t" + "No");
					}
				}
				else
				{
					vsValidate.AddItem("Charge for each mortgage holder?" + "\t" + "No");
				}
				// MAL@20080102: Add Interested Party
				blnChargeIntParty = FCConvert.ToBoolean(rsValidate.Get_Fields_Boolean("ChargeforIntParty"));
				if (FCConvert.ToBoolean(rsValidate.Get_Fields_Boolean("SendCopyToIntParty")))
				{
					if (blnChargeIntParty)
					{
						vsValidate.AddItem("Charge for each interested party?" + "\t" + "Yes");
					}
					else
					{
						vsValidate.AddItem("Charge for each interested party?" + "\t" + "No");
					}
				}
				else
				{
					vsValidate.AddItem("Charge for each interested party?" + "\t" + "No");
				}
				if (FCConvert.ToString(rsValidate.Get_Fields_String("SendCopyToNewOwner")) != "")
				{
					if (Strings.Left(FCConvert.ToString(rsValidate.Get_Fields_String("SendCopyToNewOwner")), 2) == "Ye")
					{
						if (Strings.Left(FCConvert.ToString(rsValidate.Get_Fields_String("SendCopyToNewOwner")), 8) == "Yes, cha")
						{
							vsValidate.AddItem("Send to New Owner" + "\t" + "Yes, charge CMF.");
							boolChargeForNewOwner = true;
						}
						else
						{
							vsValidate.AddItem("Send to New Owner" + "\t" + "Yes, with no charge.");
							boolChargeForNewOwner = false;
						}
					}
					else
					{
						vsValidate.AddItem("Send to New Owner" + "\t" + "No");
					}
				}
				else
				{
					vsValidate.AddItem("Send to New Owner" + "\t" + "No");
				}
			}
			//FC:FINAL:MHO:#i182 - Hide the row instead of setting its height to 0
			//vsValidate.RowHeight(0, 0);
			vsValidate.RowHidden(0, true);
			// set the height of the grid
			//FC:FINAL:BCU #i191 - no need to adjust size in code, fixed in designer
			//if ((vsValidate.Rows - 1) * vsValidate.RowHeight(1) > fraValidate.Height - 300)
			//{
			//    vsValidate.Height = fraValidate.Height - 300;
			//    vsValidate.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			//}
			//else
			//{
			//    vsValidate.Height = (vsValidate.Rows - 1) * vsValidate.RowHeight(1) + 70;
			//    vsValidate.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			//}
		}

		private void SetAct(short intAct)
		{
			// this will change all of the menu options
			switch (intAct)
			{
				case 0:
					{
						ShowValidateFrame();
						fraGrid.Visible = false;
						btnProcess.Text = "Accept Parameters";
						cmdSelectAll.Visible = false;
						intAction = 0;
						break;
					}
				case 1:
					{
						ShowGridFrame();
						fraValidate.Visible = false;
						cmdSelectAll.Visible = true;
						btnProcess.Text = "Apply Demand Fees";
						intAction = 1;
						break;
					}
			}
			//end switch
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			switch (intAction)
			{
				case 0:
					{
						// this will say that the user accepts the parameters shown
						FCMessageBox.Show("After demand fees are applied, it will not be possible to reprint the notices, labels or certified mailers.", MsgBoxStyle.Exclamation, "Apply Demand Fees");
						SetAct(1);
						break;
					}
				case 1:
					{
						// apply demand fees
						ApplyDemandFees();
						Close();
						break;
					}
			}
			//end switch
		}

		private void ApplyDemandFees()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngCT;
				int lngDemandCount = 0;
				clsDRWrapper rsRate = new clsDRWrapper();
				string strTemp = "";
				frmWait.InstancePtr.Init("Processing..." + "\r\n" + "Adding Fees");
				for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
				{
					// for each account in the grid
					App.DoEvents();
					if (Conversion.Val(vsDemand.TextMatrix(lngCT, 0)) == -1)
					{
						// check to see if the check box if checked
						if (Conversion.Val(vsDemand.TextMatrix(lngCT, 6)) >= 0)
						{
							// then what?
							rsData.FindFirstRecord("ID", vsDemand.TextMatrix(lngCT, 5));
							if (rsData.NoMatch)
							{
								FCMessageBox.Show("Error processing account " + vsDemand.TextMatrix(lngCT, 1) + ".  No demand fees were applied.", MsgBoxStyle.Critical, "Cannot Find BillKey - " + vsDemand.TextMatrix(lngCT, 5));
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								modGlobal.Statics.arrDemand[FindDemandIndex_2(FCConvert.ToInt32(rsData.Get_Fields("Account")))].Processed = true;
								AddDemandPayment_2(FCConvert.ToDouble(vsDemand.TextMatrix(lngCT, 4)));
								lngDemandCount += 1;
							}
						}
						else
						{
							// not eligible...do nothing
						}
					}
					else
					{
						// not selected...do nothing
					}
				}
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show(FCConvert.ToString(lngDemandCount) + " accounts were affected.", MsgBoxStyle.Information, "Demand Fees Added");
				int lngStrPos;
				lngStrPos = Strings.InStr(1, Strings.UCase(rsData.Name()), "ORDER BY");
				if (lngStrPos > 0)
				{
					strTemp = Strings.Left(rsData.Name(), lngStrPos - 1);
				}
				else
				{
					strTemp = rsData.Name();
				}
				rsData.OpenRecordset("SELECT Distinct RateKey AS RK FROM (" + strTemp + ") AS qRK", modExtraModules.strCLDatabase);
				while (!rsData.EndOfFile())
				{
					App.DoEvents();
					// TODO Get_Fields: Field [RK] not found!! (maybe it is an alias?)
					rsRate.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(rsData.Get_Fields("RK")), modExtraModules.strCLDatabase);
					if (!(rsRate.Get_Fields_DateTime("30DNDate") is DateTime))
					{
						rsRate.Edit();
						rsRate.Set_Fields("30DNDate", DateTime.Today);
						rsRate.Update();
					}
					rsData.MoveNext();
				}
				// print the list of accounts that have been affected
				//FC:FINAL:DSE:#1999 Report not displayed if current form is not closed first
				this.Unload();
				rptDemandFeeList.InstancePtr.Init(lngDemandCount, dblDemand);
				frmReportViewer.InstancePtr.Init(rptDemandFeeList.InstancePtr);
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Applying Fees");
			}
		}

		private int FindDemandIndex_2(int lngAcct)
		{
			return FindDemandIndex(ref lngAcct);
		}

		private int FindDemandIndex(ref int lngAcct)
		{
			int FindDemandIndex = 0;
			int lngCT;
			for (lngCT = 0; lngCT <= Information.UBound(modGlobal.Statics.arrDemand, 1); lngCT++)
			{
				if (lngAcct == modGlobal.Statics.arrDemand[lngCT].Account)
				{
					FindDemandIndex = lngCT;
					break;
				}
			}
			return FindDemandIndex;
		}

		private void mnuFileSelectAll_Click(object sender, System.EventArgs e)
		{
			// this will select all account in the grid
			int lngCT;
			for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
			{
				vsDemand.TextMatrix(lngCT, 0, FCConvert.ToString(-1));
			}
		}

		public void mnuFileSelectAll_Click()
		{
			mnuFileSelectAll_Click(cmdSelectAll, new System.EventArgs());
		}

		private void AddDemandPayment_2(double dblFeeAmount)
		{
			AddDemandPayment(ref dblFeeAmount);
		}

		private void AddDemandPayment(ref double dblFeeAmount)
		{
			double dblTotalCertMailFee/*unused?*/;
			double dblTotalChange = 0;
			double dblCurInterest = 0;
			clsDRWrapper rsPay = new clsDRWrapper();
			// this will actually create the payment line
			rsPay.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = 0", modExtraModules.strCLDatabase);
			rsPay.AddNew();
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			rsPay.Set_Fields("Account", rsData.Get_Fields("Account"));
			rsPay.Set_Fields("Year", rsData.Get_Fields_Int32("BillingYear"));
			rsPay.Set_Fields("BillKey", rsData.Get_Fields_Int32("ID"));
			// .Get_Fields("CHGINTNumber") = AddCHGINTForDemandFee    'do not charge interest (ron said so, to make it easier to reverse interest)
			rsPay.Set_Fields("CHGINTDate", DateTime.Today);
			rsPay.Set_Fields("ActualSystemDate", DateTime.Today);
			// MAL@20080603: Change to be more accurate
			// Tracker Reference: 14041
			// .Get_Fields("EffectiveInterestDate") = Date
			// .Get_Fields("RecordedTransactionDate") = Date
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			rsPay.Set_Fields("EffectiveInterestDate", modStatusPayments.GetLastInterestDate_CL(FCConvert.ToInt32(rsData.Get_Fields("Account")), FCConvert.ToInt32(rsData.Get_Fields_Int32("BillingYear")), dtMailDate, FCConvert.ToInt32(rsData.Get_Fields_Int32("ID"))));
			rsPay.Set_Fields("RecordedTransactionDate", dtMailDate);
			rsPay.Set_Fields("Teller", "");
			rsPay.Set_Fields("Reference", "DEMAND");
			rsPay.Set_Fields("Period", "A");
			rsPay.Set_Fields("Code", "3");
			rsPay.Set_Fields("ReceiptNumber", 0);
			rsPay.Set_Fields("Principal", 0);
			rsPay.Set_Fields("PreLienInterest", 0);
			rsPay.Set_Fields("CurrentInterest", 0);
			// calculate the lien costs
			// dblTotalCertMailFee = dblCertMailFee * rsData.Get_Fields("Copies")
			// dblTotalChange = dblTotalCertMailFee + dblDemand
			dblTotalChange = dblFeeAmount;
			rsPay.Set_Fields("LienCost", modGlobal.Round(dblTotalChange * -1, 2));
			rsPay.Set_Fields("TransNumber", 0);
			rsPay.Set_Fields("PaidBy", "Automatic/Computer");
			rsPay.Set_Fields("Comments", "Demand Fees");
			rsPay.Set_Fields("CashDrawer", "N");
			rsPay.Set_Fields("GeneralLedger", "Y");
			rsPay.Set_Fields("BudgetaryAccountNumber", "");
			rsPay.Set_Fields("BillCode", "R");
			// rsPay.Get_Fields ("DailyCloseOut")
			rsPay.Update();
			// this will edit the bill record
			// .OpenRecordset "SELECT * FROM CurrentAccountBills WHERE Billkey = " & .BillKey, strCLDATABASE
			// If .RecordCount <> 0 Then
			rsData.Edit();
			rsData.Set_Fields("InterestCharged", (Conversion.Val(rsData.Get_Fields_Decimal("InterestCharged")) - dblCurInterest));
			rsData.Set_Fields("DemandFees", (Conversion.Val(rsData.Get_Fields_Decimal("DemandFees")) + dblTotalChange));
			// .Get_Fields("InterestAppliedThroughDate") = Date   'do not change the interest date (ron said so)
			rsData.Set_Fields("LienProcessStatus", 2);
			// set the status to 'Demand Fees Applied'
			rsData.Set_Fields("LienStatusEligibility", 3);
			rsData.Update();
			// End If
		}

		//private int AddCHGINTForDemandFee()
		//{
		//	int AddCHGINTForDemandFee = 0;
		//	// this will calculate the CHGINT line for the account and then return
		//	// the CHGINT number to store in the payment record
		//	// this will use rsData which is set to the correct record
		//	double dblCurInt = 0;
		//	double dblTotalDue;
		//	clsDRWrapper rsCHGINT = new clsDRWrapper();
		//	// calculate the current interest to this date
		//	// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
		//	dblTotalDue = modCLCalculations.CalculateAccountCL(ref rsData, FCConvert.ToInt32(rsData.Get_Fields("Account")), DateTime.Today, ref dblCurInt);
		//	if (dblCurInt > 0)
		//	{
		//		// if there is interest due, then
		//		// this will actually create the payment line
		//		rsCHGINT.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = 0", modExtraModules.strCLDatabase);
		//		rsCHGINT.AddNew();
		//		// AddCHGINTForDemandFee = .Get_Fields("ID")
		//		// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
		//		rsCHGINT.Set_Fields("Account", rsData.Get_Fields("Account"));
		//		rsCHGINT.Set_Fields("Year", rsData.Get_Fields_Int32("BillingYear"));
		//		rsCHGINT.Set_Fields("BillKey", rsData.Get_Fields_Int32("BillKey"));
		//		rsCHGINT.Set_Fields("CHGINTNumber", 0);
		//		rsCHGINT.Set_Fields("CHGINTDate", DateTime.Today);
		//		rsCHGINT.Set_Fields("ActualSystemDate", DateTime.Today);
		//		rsCHGINT.Set_Fields("EffectiveInterestDate", DateTime.Today);
		//		rsCHGINT.Set_Fields("RecordedTransactionDate", DateTime.Today);
		//		rsCHGINT.Set_Fields("Teller", "");
		//		rsCHGINT.Set_Fields("Reference", "CHGINT");
		//		rsCHGINT.Set_Fields("Period", "A");
		//		rsCHGINT.Set_Fields("Code", "I");
		//		rsCHGINT.Set_Fields("ReceiptNumber", 0);
		//		rsCHGINT.Set_Fields("Principal", 0);
		//		rsCHGINT.Set_Fields("PreLienInterest", 0);
		//		rsCHGINT.Set_Fields("CurrentInterest", modGlobal.Round(dblCurInt, 2));
		//		rsCHGINT.Set_Fields("LienCost", 0);
		//		rsCHGINT.Set_Fields("TransNumber", 0);
		//		rsCHGINT.Set_Fields("PaidBy", 0);
		//		rsCHGINT.Set_Fields("Comments", "Demand Fees");
		//		rsCHGINT.Set_Fields("CashDrawer", "N");
		//		rsCHGINT.Set_Fields("GeneralLedger", "Y");
		//		rsCHGINT.Set_Fields("BudgetaryAccountNumber", "");
		//		rsCHGINT.Set_Fields("BillCode", "R");
  //              rsCHGINT.Set_Fields("ChargedInterestIdentifier",Guid.Empty);
  //              rsCHGINT.Set_Fields("PaymentIdentifier",Guid.NewGuid());
		//		// rsPay.Get_Fields ("DailyCloseOut")
		//		rsCHGINT.Update();
		//		AddCHGINTForDemandFee = FCConvert.ToInt32(rsCHGINT.Get_Fields_Int32("ID"));
		//	}
		//	return AddCHGINTForDemandFee;
		//}

		private void vsDemand_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			// this should not allow an account that already has demand fees to be checked
			if (vsDemand.Col == 0)
			{
				if (Conversion.Val(vsDemand.TextMatrix(vsDemand.Row, 6)) != 0)
				{
					vsDemand.TextMatrix(vsDemand.Row, 0, FCConvert.ToString(0));
				}
			}
		}

		private void vsDemand_DblClick(object sender, EventArgs e)
		{
			int lngMR = 0;
			int lngMC = 0;
			lngMR = vsDemand.MouseRow;
			lngMC = vsDemand.MouseCol;
			if (lngMR == 0 && lngMC == 0)
			{
				mnuFileSelectAll_Click();
			}
		}

        //FC:FINAL:AM: set the tooltip on each cell
        //private void vsDemand_MouseMove(object sender, DataGridViewCellMouseEventArgs e)
        //{
        //	int lngMR = 0;
        //	int lngMC = 0;
        //	lngMR = vsDemand.MouseRow;
        //	lngMC = vsDemand.MouseCol;
        //	if (lngMR > 0 && lngMC > 0)
        //	{
        //		if (Conversion.Val(vsDemand.TextMatrix(lngMR, 6)) == -1)
        //		{
        //			ToolTip1.SetToolTip(vsDemand, "Demand Fees have already been added to this account.  This account is not eligible.");
        //		}
        //		else
        //		{
        //			ToolTip1.SetToolTip(vsDemand, "");
        //		}
        //	}
        //}

        private void VsDemand_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            int lngMR = 0;
            int lngMC = 0;
            lngMR = vsDemand.GetFlexRowIndex(e.RowIndex);
            lngMC = vsDemand.GetFlexColIndex(e.ColumnIndex);

            if (vsDemand.IsValidCell(e.ColumnIndex, e.RowIndex))
            {
                DataGridViewCell cell = vsDemand[e.ColumnIndex, e.RowIndex];

                if (Conversion.Val(vsDemand.TextMatrix(lngMR, 6)) == -1)
                {
                   cell.ToolTipText = "Demand Fees have already been added to this account.  This account is not eligible.";
                }
                else
                {
                    cell.ToolTipText = "";
                }
            }
        }

        private void vsDemand_RowColChange(object sender, EventArgs e)
		{
			switch (vsDemand.Col)
			{
				case 0:
					{
						vsDemand.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						break;
					}
				default:
					{
						vsDemand.Editable = FCGrid.EditableSettings.flexEDNone;
						break;
					}
			}
			//end switch
		}

		private void ClearCheckBoxes()
		{
			// this will set all of the textboxes to unchecked
			int lngCT;
			for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
			{
				vsDemand.TextMatrix(lngCT, 0, FCConvert.ToString(0));
			}
		}

		private int CalculateMortgageHolders()
		{
			int CalculateMortgageHolders = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will return the number of mortgage holders this account has
				clsDRWrapper rsMort = new clsDRWrapper();
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				rsMort.OpenRecordset("SELECT * FROM MortgageAssociation INNER JOIN MortgageHolders ON MortgageAssociation.MortgageHolderID = MortgageHolders.ID WHERE Account = " + FCConvert.ToString(rsData.Get_Fields("Account")) + " AND Module = 'RE'", "CentralData");
				if (rsMort.EndOfFile() != true)
				{
					CalculateMortgageHolders = rsMort.RecordCount();
				}
				else
				{
					CalculateMortgageHolders = 0;
				}
				return CalculateMortgageHolders;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				CalculateMortgageHolders = 0;
			}
			return CalculateMortgageHolders;
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFilePrint_Click(sender, e);
		}

		private void cmdClearSelection_Click(object sender, EventArgs e)
		{
			this.mnuFileClear_Click(sender, e);
		}

		private void cmdSelectAll_Click(object sender, EventArgs e)
		{
			this.mnuFileSelectAll_Click(sender, e);
		}
		//
		// Private Function NewOwner() As Boolean
		// On Error GoTo ERROR_HANDLER
		// this function will find out if there has been a new owner since the billing date
		//
		// NewOwner = False
		// If gboolRE Then
		// rsRE.FindFirstRecord , , "RSAccount = " & rsData.Get_Fields("Account")
		// If Not rsRE.NoMatch Then
		// If rsRE.Get_Fields("RSName") <> rsData.Get_Fields("Name1") Then
		// NewOwner = True
		// End If
		// boolREMatch = True
		// Else
		// set this to end of file
		// it will make sure no incorrect infromation it added to the notices
		// boolREMatch = False
		// End If
		// End If
		// Exit Function
		// ERROR_HANDLER:
		// NewOwner = False
		// End Function
	}
}
