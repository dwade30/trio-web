using System;
using fecherFoundation;
using fecherFoundation.Extensions;

namespace TWCL0000
{
	public class cLogin
	{
		//=========================================================
		public bool ValidLogin(string strUser, string strPassword)
		{
			bool ValidLogin = false;
			ValidLogin = false;
			try
			{
				// On Error GoTo ErrorHandler
				if (!(Strings.Trim(strUser) == "") && !(Strings.Trim(strPassword) == ""))
				{
					cSecurityUser tUser;
					tUser = GetUserRecord(strUser);
					if (!(tUser == null))
					{
						string strPassHash = "";
						string strCalcHash = "";
						strPassHash = tUser.PasswordHash;
						// now compute password hash and compare
						Chilkat.Crypt2 tCrypt = new Chilkat.Crypt2();
						tCrypt.UnlockComponent("HARRISCrypt_vEoVPOQtXRFY");
						tCrypt.HashAlgorithm = "sha512";
						strCalcHash = tCrypt.HashStringENC(strPassword);
						if (strCalcHash == strPassHash)
						{
							ValidLogin = true;
						}
						else
						{
							ValidLogin = false;
						}
					}
				}
				return ValidLogin;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				ValidLogin = false;
			}
			return ValidLogin;
		}

		public cSecurityUser GetUserByID(int lngUserID)
		{
			cSecurityUser GetUserByID = null;
			GetUserByID = null;
			clsDRWrapper rs = new clsDRWrapper();
			rs.OpenRecordset("select * from security where id = " + Convert.ToString(lngUserID), "SystemSettings");
			cSecurityUser tUser = new cSecurityUser();
			if (!rs.EndOfFile())
			{
				tUser.ID = Convert.ToInt32(rs.Get_Fields_Int32("ID"));
				// TODO Get_Fields: Field [datechange] not found!! (maybe it is an alias?)
				if (Information.IsDate(rs.Get_Fields("datechange")))
				{
					tUser.DateChanged = (DateTime)rs.Get_Fields_DateTime("DateChanged");
				}
				tUser.DefaultAdvancedSearch = Convert.ToBoolean(rs.Get_Fields_Boolean("DefaultAdvancedSearch"));
				// TODO Get_Fields: Check the table for the column [Frequency] and replace with corresponding Get_Field method
				tUser.Frequency = Convert.ToInt32(rs.Get_Fields("Frequency"));
				tUser.OpID = Convert.ToString(rs.Get_Fields_String("opid"));
				tUser.PasswordHash = Convert.ToString(rs.Get_Fields_String("Password"));
				if (Information.IsDate(rs.Get_Fields_DateTime("updatedate")))
				{
					tUser.UpdateDate = (DateTime)rs.Get_Fields_DateTime("updatedate");
				}
				// TODO Get_Fields: Check the table for the column [userid] and replace with corresponding Get_Field method
				tUser.User = Convert.ToString(rs.Get_Fields("userid"));
				tUser.UserName = Convert.ToString(rs.Get_Fields_String("username"));
				// TODO Get_Fields: Check the table for the column [usesecurity] and replace with corresponding Get_Field method
				tUser.UseSecurity = Convert.ToBoolean(rs.Get_Fields("usesecurity"));
				GetUserByID = tUser;
			}
			else
			{
				if (lngUserID == -1)
				{
					tUser = new cSecurityUser();
					tUser.ID = -1;
					tUser.User = "SuperUser";
					tUser.UserName = "SuperUser";
					tUser.UseSecurity = false;
					GetUserByID = tUser;
				}
			}
			return GetUserByID;
		}

		public string GetUserList()
		{
			string GetUserList = "";
			string strReturn = "";
			clsDRWrapper rs = new clsDRWrapper();
			string strComma;
			rs.OpenRecordset("select * from security order by userid", "SystemSettings");
			strComma = "";
			while (!rs.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [userid] and replace with corresponding Get_Field method
				strReturn += strComma + FCConvert.ToString(rs.Get_Fields("userid")) + ";" + FCConvert.ToString(rs.Get_Fields_Int32("ID"));
				strComma = ",";
				rs.MoveNext();
			}
			GetUserList = strReturn;
			return GetUserList;
		}

		private cSecurityUser GetUserRecord(string strUser)
		{
			cSecurityUser GetUserRecord = null;
			GetUserRecord = null;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				string strTemp;
				strTemp = modGlobalFunctions.EscapeQuotes(strUser);
				rs.OpenRecordset("select * from security where userid = '" + strTemp + "'", "SystemSettings");
				if (!rs.EndOfFile())
				{
					cSecurityUser tUser = new cSecurityUser();
					tUser.ID = Convert.ToInt32(rs.Get_Fields_Int32("ID"));
					if (Information.IsDate(rs.Get_Fields_DateTime("datechanged")))
					{
						tUser.DateChanged = (DateTime)rs.Get_Fields_DateTime("DateChanged");
					}
					tUser.DefaultAdvancedSearch = Convert.ToBoolean(rs.Get_Fields_Boolean("DefaultAdvancedSearch"));
					// TODO Get_Fields: Check the table for the column [Frequency] and replace with corresponding Get_Field method
					tUser.Frequency = Convert.ToInt32(rs.Get_Fields("Frequency"));
					tUser.OpID = Convert.ToString(rs.Get_Fields_String("opid"));
					tUser.PasswordHash = Convert.ToString(rs.Get_Fields_String("Password"));
					if (Information.IsDate(rs.Get_Fields_DateTime("updatedate")))
					{
						tUser.UpdateDate = (DateTime)rs.Get_Fields_DateTime("updatedate");
					}
					// TODO Get_Fields: Check the table for the column [userid] and replace with corresponding Get_Field method
					tUser.User = Convert.ToString(rs.Get_Fields("userid"));
					tUser.UserName = Convert.ToString(rs.Get_Fields_String("username"));
					// TODO Get_Fields: Check the table for the column [usesecurity] and replace with corresponding Get_Field method
					tUser.UseSecurity = Convert.ToBoolean(rs.Get_Fields("usesecurity"));
					GetUserRecord = tUser;
				}
				return GetUserRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return GetUserRecord;
		}

		public cSecurityUser GetUser(string strUser, string strPassword)
		{
			cSecurityUser GetUser = null;
			GetUser = null;
			try
			{
				// On Error GoTo ErrorHandler
				if (!(Strings.Trim(strUser) == "") && !(Strings.Trim(strPassword) == ""))
				{
					cSecurityUser tUser;
					tUser = GetUserRecord(strUser);
					if (!(tUser == null))
					{
						string strPassHash = "";
						string strCalcHash = "";
						strPassHash = tUser.PasswordHash;
						// now compute password hash and compare
						Chilkat.Crypt2 tCrypt = new Chilkat.Crypt2();
						tCrypt.UnlockComponent("HARRISCrypt_vEoVPOQtXRFY");
						tCrypt.HashAlgorithm = "sha512";
						strCalcHash = tCrypt.HashStringENC(strPassword);
						if (strCalcHash == strPassHash)
						{
							GetUser = tUser;
						}
						else
						{
							GetUser = null;
						}
					}
				}
				return GetUser;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				GetUser = null;
			}
			return GetUser;
		}

		public bool SaveUser(ref cSecurityUser tUser)
		{
			bool SaveUser = false;
			SaveUser = false;
			try
			{
				// On Error GoTo ErrorHandler
				if (!(tUser == null))
				{
					if (!(tUser.ID == -1))
					{
						clsDRWrapper rsSave = new clsDRWrapper();
						string strTemp = "";
						strTemp = tUser.User;
						strTemp = modGlobalFunctions.EscapeQuotes(strTemp);
						rsSave.OpenRecordset("select * from security where userid = '" + strTemp + "'", "SystemSettings");
						if (!rsSave.EndOfFile())
						{
							rsSave.Edit();
						}
						else
						{
							rsSave.AddNew();
						}
						rsSave.Set_Fields("UserID", tUser.User);
						rsSave.Set_Fields("OpID", tUser.OpID);
						rsSave.Set_Fields("UserName", tUser.UserName);
						rsSave.Set_Fields("Frequency", tUser.Frequency);
						rsSave.Set_Fields("DateChanged", tUser.DateChanged);
						rsSave.Set_Fields("UseSecurity", tUser.UseSecurity);
						rsSave.Set_Fields("UpdateDate", tUser.UpdateDate);
						rsSave.Set_Fields("Password", tUser.PasswordHash);
						rsSave.Set_Fields("DefaultAdvancedSearch", tUser.DefaultAdvancedSearch);
						rsSave.Update();
						tUser.ID = rsSave.Get_Fields_Int32("id");
					}
				}
				SaveUser = true;
				return SaveUser;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return SaveUser;
		}
	}
}
