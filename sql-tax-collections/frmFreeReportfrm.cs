﻿using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using Global;
using fecherFoundation.VisualBasicLayer;
using SharedApplication.TaxCollections.Interfaces;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmFreeReport.
	/// </summary>
	public partial class frmFreeReport : BaseForm
	{
		public frmFreeReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.lblPsuedoFooter = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.lblPsuedoFooter.AddControlArrayElement(this.lblPsuedoFooter_0, 0);
			this.lblPsuedoFooter.AddControlArrayElement(this.lblPsuedoFooter_1, 1);
			this.lblPsuedoFooter.AddControlArrayElement(this.lblPsuedoFooter_2, 2);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmFreeReport InstancePtr
		{
			get
			{
				return (frmFreeReport)Sys.GetInstance(typeof(frmFreeReport));
			}
		}

		protected frmFreeReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               06/08/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/21/2006              *
		// ********************************************************
		public bool boolUseQuestions;
		public bool boolInitial;
		bool boolLoaded;
		int intAct;
		bool boolDontCheck;
		string strCurrentReportName = "";
		int lngCurrentSummaryRow;
		bool boolResize;
		bool boolUnloadingReport;
		// VBto upgrade warning: dblMinimumAmount As double	OnWrite(double, short, string)
		public double dblMinimumAmount;
		// this will hold the lowest amount of principal that should be liened
		public string strSigPassword = "";
		public string strCollectorSigPassword = "";
		bool boolUnloading;
		public string strRK = string.Empty;
        private ITaxCollectionNoticeOptions noticeOptions;

        private FCSectionReport reportObject;

        public ITaxCollectionNoticeOptions NoticeOptions
        {
            get { return noticeOptions; }
            set { noticeOptions = value; }
        }

        private void cboSavedReport_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// THIS CODE WILL ALLOW THE USER TO DELETE A CUSTOM REPORT THAT
			// WAS PREVIOUSELY SAVED
			string strTemp = "";
			clsDRWrapper rs = new clsDRWrapper();
			int intCT/*unused?*/;
			int intStart/*unused?*/;
			int lngRW/*unused?*/;
			if (cmbReport.SelectedIndex == 2)
			{
				if (cboSavedReport.SelectedIndex < 0)
					return;
				if (FCMessageBox.Show("This will delete the custom report " + cboSavedReport.Items[cboSavedReport.SelectedIndex].ToString() + ". Continue?", MsgBoxStyle.Question | MsgBoxStyle.YesNoCancel, "TRIO Software") == DialogResult.Yes)
				{
					clsDRWrapper rsDelete = new clsDRWrapper();
					rsDelete.OpenRecordset("SELECT * FROM SavedFreeReports WHERE ID = " + FCConvert.ToString(cboSavedReport.ItemData(cboSavedReport.SelectedIndex)), modGlobal.DEFAULTDATABASE);
					if (rsDelete.EndOfFile() != true)
					{
						rsDelete.Edit();
						// sql conv True
						rsDelete.Set_Fields("ReportName", "ARCHIVE" + FCConvert.ToString(rsDelete.Get_Fields_String("ReportName")));
						rsDelete.Update();
						LoadCombo();
						FCMessageBox.Show("Custom report deleted successfully.", MsgBoxStyle.Information, "TRIO Software");
					}
					else
					{
						FCMessageBox.Show("Custom report could not be found.", MsgBoxStyle.Information, "TRIO Software");
					}
				}
			}
			else if (cmbReport.SelectedIndex == 1)
			{
				// check to make sure that a report was selected
				if (cboSavedReport.SelectedIndex < 0)
					return;
				// show the report
				rs.OpenRecordset("SELECT * FROM SavedFreeReports WHERE ID = " + FCConvert.ToString(cboSavedReport.ItemData(cboSavedReport.SelectedIndex)), modGlobal.DEFAULTDATABASE);
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rtbData.Text = FCConvert.ToString(rs.Get_Fields_String("ReportText"));
					strCurrentReportName = cboSavedReport.Items[cboSavedReport.SelectedIndex].ToString();
					// this will save the name
					// load the contraints
					// lngRw = 0
					// For intCT = 0 To MAXFREEVARIABLES
					// If intCT < 30 And lngRw < vsVariableSetup.rows Then
					// If frfFreeReport(intCT).VariableType <> 1 And frfFreeReport(intCT).VariableType <> 3 Then
					// vsVariableSetup.TextMatrix(lngRw, 1) = .Get_Fields("FieldDefault" & intCT)
					// frfFreeReport(intCT).RowNumber = lngRw
					// lngRw = lngRw + 1
					// Else
					// vsVariableSetup.TextMatrix(intCT, 1) = ""
					// End If
					// Else
					// Exit For
					// End If
					// Next
				}
				else
				{
					FCMessageBox.Show("There was an error while opening this file.", MsgBoxStyle.Critical, "Load Error");
				}
				cmbReport.SelectedIndex = 0;
				// set it back to create
			}
		}

		private void chkRecommitment_Click(object sender, System.EventArgs e)
		{
			fraRecommitmentInfo.Enabled = chkRecommitment.CheckState == Wisej.Web.CheckState.Checked;
			if (chkRecommitment.CheckState == Wisej.Web.CheckState.Checked)
			{
				//				vsRecommitment.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 0, 1, 1, modGlobalConstants.Statics.TRIOCOLORBLACK);
			}
			else
			{
				//				vsRecommitment.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 0, 1, 1, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT);
			}
		}

		private void chkUseSig_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				bool boolPasswordAccept/*unused?*/;
				int lngAnswer = 0;
				if (chkUseSig.CheckState == Wisej.Web.CheckState.Checked)
				{
					if ((Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "30DAYNOTICE") || (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "LIENPROCESS"))
					{
						// set the default year from the last time this was entered
						// have the user enter the sig password
						COLLECTORTRYAGAIN:
						;
						lngAnswer = frmSignaturePassword.InstancePtr.Init(modSignatureFile.COLLECTORSIG);
						if (lngAnswer > 0)
						{
							// is the password correct
							modSignatureFile.Statics.gstrCollectorSigPath = Path.Combine(FCFileSystem.Statics.UserDataFolder, modSignatureFile.GetSigFileFromID(ref lngAnswer));
							// yes the password is correct
							if (File.Exists(modSignatureFile.Statics.gstrCollectorSigPath))
							{
								// rock on
								modGlobal.Statics.gboolUseSigFile = true;
								imgSig.Image = FCUtils.LoadPicture(modSignatureFile.Statics.gstrCollectorSigPath);
								imgSig.Visible = true;
								fraSigFile.Height = 188;
								modGlobalFunctions.AddCYAEntry_8("CL", "Use signature file for lien process.", modRhymalReporting.Statics.strFreeReportType);
							}
							else
							{
								modGlobal.Statics.gboolUseSigFile = false;
								chkUseSig.CheckState = Wisej.Web.CheckState.Unchecked;
								if (Strings.Trim(modSignatureFile.Statics.gstrCollectorSigPath) == "")
								{
									FCMessageBox.Show("No path setup.  Please set it up in General Entry > File Maintenance > Customize.", MsgBoxStyle.Exclamation, "Invalid Path");
								}
								else
								{
									FCMessageBox.Show(modSignatureFile.Statics.gstrCollectorSigPath + " is not a valid path.  Please set it up in General Entry > File Maintenance > Customize.", MsgBoxStyle.Exclamation, "Invalid Path");
								}
								imgSig.Visible = false;
								fraSigFile.Height = 88;
							}
						}
						else
						{
							modGlobal.Statics.gboolUseSigFile = false;
							imgSig.Visible = false;
							fraSigFile.Height = 88;
							if (FCMessageBox.Show("The password entered is invalid.  Would you like to try again?", MsgBoxStyle.Question | MsgBoxStyle.YesNoCancel, "Invalid Password") == DialogResult.Yes)
							{
								goto COLLECTORTRYAGAIN;
							}
							else
							{
								chkUseSig.CheckState = Wisej.Web.CheckState.Unchecked;
								// this will set it opposite then it will be unset
								modGlobal.Statics.gboolUseSigFile = false;
								imgSig.Visible = false;
								fraSigFile.Height = 88;
							}
						}
					}
					else if ((Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "LIENMATURITY") || (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "LIENPROCESS"))
					{
						lngAnswer = frmSignaturePassword.InstancePtr.Init(modSignatureFile.TREASURERSIG);
						TryAgain:
						;
						if (lngAnswer > 0)
						{
							// yes the password is correct
							modSignatureFile.Statics.gstrTreasSigPath = Path.Combine(FCFileSystem.Statics.UserDataFolder, modSignatureFile.GetSigFileFromID(ref lngAnswer));
							if (File.Exists(modSignatureFile.Statics.gstrTreasSigPath))
							{
								// rock on
								modGlobal.Statics.gboolUseSigFile = true;
								imgSig.Image = FCUtils.LoadPicture(modSignatureFile.Statics.gstrTreasSigPath);
								imgSig.Visible = true;
								fraSigFile.Height = 188;
								modGlobalFunctions.AddCYAEntry_8("CL", "Use signature file for lien process.", modRhymalReporting.Statics.strFreeReportType);
							}
							else
							{
								modGlobal.Statics.gboolUseSigFile = false;
								chkUseSig.CheckState = Wisej.Web.CheckState.Unchecked;
								if (Strings.Trim(modSignatureFile.Statics.gstrTreasSigPath) == "")
								{
									FCMessageBox.Show("No path setup.  Please set it up in File Maintenance - Customize.", MsgBoxStyle.Exclamation, "Invalid Path");
								}
								else
								{
									FCMessageBox.Show(modSignatureFile.Statics.gstrTreasSigPath + " is not a valid path.  Please set it up in File Maintenance - Customize.", MsgBoxStyle.Exclamation, "Invalid Path");
								}
								imgSig.Visible = false;
								fraSigFile.Height = 88;
							}
						}
						else
						{
							modGlobal.Statics.gboolUseSigFile = false;
							imgSig.Visible = false;
							fraSigFile.Height = 88;
							if (FCMessageBox.Show("The password entered is invalid.  Would you like to try again?", MsgBoxStyle.Question | MsgBoxStyle.YesNoCancel, "Invalid Password") == DialogResult.Yes)
							{
								goto TryAgain;
							}
							else
							{
								chkUseSig.CheckState = Wisej.Web.CheckState.Unchecked;
								// this will set it opposite then it will be unset
								modGlobal.Statics.gboolUseSigFile = false;
								imgSig.Visible = false;
								fraSigFile.Height = 88;
							}
						}
					}
				}
				else
				{
					modGlobal.Statics.gboolUseSigFile = false;
					imgSig.Visible = false;
					fraSigFile.Height = 88;
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				chkUseSig.CheckState = Wisej.Web.CheckState.Unchecked;
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Password Error");
			}
		}

		private bool CheckSigPassword(ref string strPW)
		{
			bool CheckSigPassword = false;
			// this will validate the password signature
			if (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "30DAYNOTICE")
			{
				if (Strings.UCase(Strings.Trim(modSignatureFile.Statics.gstrCollectorSigPassword)) == Strings.UCase(Strings.Trim(strPW)))
				{
					CheckSigPassword = true;
				}
				else
				{
					CheckSigPassword = false;
				}
			}
			else
			{
				if (Strings.UCase(Strings.Trim(modSignatureFile.Statics.gstrTreasurerSigPassword)) == Strings.UCase(Strings.Trim(strPW)))
				{
					CheckSigPassword = true;
				}
				else
				{
					CheckSigPassword = false;
				}
			}
			return CheckSigPassword;
		}

		private void cmdAdd_Click(object sender, System.EventArgs e)
		{
			// THIS ALLOWS THE USER TO SAVE THE REPORT THAT WAS JUST GENERATED
			// THIS ONLY SAVES THE SQL STATEMENT THAT IS GENERATED. THE USER
			// WILL NOT SEE THE LIST BOXES AND WHERE GRID FILLED IN FOR THEM IF
			// THEY DISPLAY A SAVED REPORT SO NAMING EACH REPORT IS ***VERY***
			// IMPORTANT
			string strReturn;
			clsDRWrapper rsSave = new clsDRWrapper();
			int intRow;
			int intCol/*unused?*/;
			string strTemp = "";
			int intCT/*unused?*/;
			DialogResult intAns = 0;
			TryAgain:
			;
			strReturn = Interaction.InputBox("Enter name for new report", "New Custom Report");
			strReturn = modGlobalFunctions.RemoveApostrophe(strReturn);
			if (strReturn == string.Empty)
			{
				// DO NOT SAVE REPORT
			}
			else
			{
				// THIS ALLOWS FOR THE BUILDING OF THE SQL STATEMENT BUT DOES
				// NOT SHOW IT
				// SAVE THE REPORT
				SAVETHIS:
				;
				rsSave.OpenRecordset("SELECT * FROM SavedFreeReports WHERE (ReportName = '" + strReturn + "' OR ReportName = 'DEFAULT" + strReturn + "') AND Type = '" + Strings.UCase(modCustomReport.Statics.strReportType) + "'", modGlobal.DEFAULTDATABASE);
				if (!rsSave.EndOfFile())
				{
					intAns = FCMessageBox.Show("A report by that name already exists. Would you like to save it anyway?", MsgBoxStyle.Question | MsgBoxStyle.YesNoCancel, "TRIO Software");
					if (intAns == DialogResult.Yes)
					{
						// Yes
						// save the old report as an archive
						rsSave.Edit();
						rsSave.Set_Fields("ReportName", "ARCHIVE" + FCConvert.ToString(rsSave.Get_Fields_String("ReportName")));
						rsSave.Set_Fields("LastUpdated", DateTime.Today);
						rsSave.Update();
						goto SAVETHIS;
					}
					else if (intAns == DialogResult.Cancel)
					{
						// Cancel
						return;
					}
					else
					{
						// No
						goto TryAgain;
					}
				}
				else
				{
					rsSave.AddNew();
					rsSave.Set_Fields("ReportName", strReturn);
					rsSave.Set_Fields("ReportText", rtbData.Text);
					rsSave.Set_Fields("LastUpdated", DateTime.Today);
					rsSave.Set_Fields("Type", modCustomReport.Statics.strReportType);
					intRow = 0;
					// For intCT = 0 To MAXFREEVARIABLES
					// If intCT < 30 And intRow < vsVariableSetup.rows Then
					// If frfFreeReport(intCT).VariableType <> 1 And frfFreeReport(intCT).VariableType <> 3 Then
					// .Get_Fields("FieldDefault" & intCT) = vsVariableSetup.TextMatrix(intRow, 1)
					// intRow = intRow + 1
					// Else
					// 
					// End If
					// Else
					// Exit For
					// End If
					// Next
					if (rsSave.Update())
					{
						FCMessageBox.Show("Custom Report saved as " + strReturn, MsgBoxStyle.Information, "TRIO Software");
					}
					LoadCombo();
				}
			}
		}

		private void cmdAddVariable_Click(object sender, System.EventArgs e)
		{
			if (lstVariable.SelectedRows.Count > 0 && lstVariable.SelectedRows[0].Index != -1)
			{
				InsertTagAtCursor_2(FCConvert.ToInt16(lstVariable.SelectedRows[0].Tag));
				rtbData.Focus();
			}
			else
			{
				FCMessageBox.Show("Please choose a variable to insert before using the button.", MsgBoxStyle.Information, "Select Variable");
				lstVariable.Focus();
			}
		}
		//private void frmFreeReport_Activated(object sender, System.EventArgs e)
		//{
		//	if (boolLoaded)
		//	{
		//		// do nothing
		//	}
		//	else
		//	{
		//		// kk 01082013 trocls-12  This causes a Resource error in SQL Server
		//		// MAL@20080121: Add increase to file locks
		//		// DBEngine.SetOption dbMaxLocksPerFile, 200000
		//		StartUp();
		//	}
		//}
		private void StartUp()
		{
			string strDefaultReport = "";
			string strTemp = "";
			int intCT;
			//SetPositions();
			//			vsRecommitment.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 0, 1, 1, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT);
			ResetVariables();
			LoadReport();
			LoadVariableCombo();
			LoadFreeVariablesGRID();
			LoadDefaultInformation();
			ShowFrame(fraVariableSetup);
			// load the saved report combo box
			// LoadCombo
			cmbReport.SelectedIndex = 1;
			// set the default report
			if (cboSavedReport.Items.Count > 0)
			{
				if (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "30DAYNOTICE")
				{
					// set the default year from the last time this was entered
					// GetKeyValues HKEY_CURRENT_USER, REGISTRYKEY & "CL\", "LASTFREEREPORT30DN", strTemp
					strTemp = GetDefaultNotice_2(1);
				}
				else if (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "LIENPROCESS")
				{
					// set the default year from the last time this was entered
					// GetKeyValues HKEY_CURRENT_USER, REGISTRYKEY & "CL\", "LASTFREEREPORTLIEN", strTemp
					strTemp = GetDefaultNotice_2(2);
				}
				else if (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "LIENMATURITY")
				{
					// set the default year from the last time this was entered
					// GetKeyValues HKEY_CURRENT_USER, REGISTRYKEY & "CL\", "LASTFREEREPORTMATURITY", strTemp
					strTemp = GetDefaultNotice_2(3);
				}
				else if (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "RNFORM")
				{
					strTemp = GetDefaultNotice_2(10);
					cmdFileExclusion.Visible = false;
				}
				if (Strings.Trim(strTemp) != "")
				{
					for (intCT = 0; intCT <= cboSavedReport.Items.Count - 1; intCT++)
					{
						if (Strings.Trim(cboSavedReport.Items[intCT].ToString()) == Strings.Trim(strTemp))
						{
							cboSavedReport.SelectedIndex = intCT;
							break;
						}
					}
					// if it cannot find the report then set the default to the first one
					if (intCT == cboSavedReport.Items.Count)
					{
						cboSavedReport.SelectedIndex = 0;
					}
				}
				else
				{
					cboSavedReport.SelectedIndex = 0;
				}
			}
			fraEdit.Visible = false;
			SetAct(0);
			boolLoaded = true;
		}

		private void SetAct(short intActNum)
		{
			// this will change all of the settings that are needed to change between actions
			switch (intActNum)
			{
				case 0:
					{
						// beginning screen (shows the variables that the user can change)
						// change the menu option caption
						cmdFileRefresh.Visible = false;
						btnProcess.Visible = true;
						cmdFileChangeAct.Caption = "Save and Continue";
						cmdFileChangeAct.Visible = false;
						// show the correct frames
						ShowFrame(fraVariableSetup);
						//FC:FINAL:AM:#74 - hide the edit frame
						fraEdit.Visible = false;
						if ((modCustomReport.Statics.strReportType == "LIENPROCESS") || (modCustomReport.Statics.strReportType == "30DAYNOTICE"))
						{
							fraRecommitment.Visible = true;
							// MAL@20071011: Corrected so signature frame shows when this page is recalled
							// Program Bug Reference: 6509
							fraSigFile.Visible = true;
							// fraRecommitment.Top = fraVariableSetup.Top + vsVariableSetup.Top + vsVariableSetup.Height + 100
							// fraSigFile.Top = fraRecommitment.Top
							intAct = 0;
							cmdFileExclusion.Visible = true;
						}
						else if (modCustomReport.Statics.strReportType == "RNFORM")
						{
							// move to the next step, variables will already be filled
							SetAct(1);
							cmdFileExclusion.Visible = false;
						}
						else
						{
							fraRecommitment.Visible = false;
							// fraSigFile.Top = fraVariableSetup.Top + vsVariableSetup.Top + vsVariableSetup.Height + 100
							// hide the others
							fraEdit.Visible = false;
							fraSummary.Visible = false;
							fraSigFile.Visible = true;
							intAct = 0;
							cmdFileExclusion.Visible = true;
						}
						break;
					}
				case 1:
					{
						// show the report screen (large white box with the save report and variables at the bottom)
						// change the menu option caption
						cmdFileRefresh.Visible = true;
						btnProcess.Visible = true;
						cmdFileChangeAct.Text = "Previous";
						cmdFileChangeAct.Visible = true;
						if (modCustomReport.Statics.strReportType == "RNFORM")
						{
							cmdFileExclusion.Visible = false;
						}
						else
						{
							cmdFileExclusion.Visible = true;
						}
						// show the correct frames
						ShowFrame(fraEdit);
						// set the labels on the next frame to look like the report
						SetPsuedoReportLabel();
						// hide the others
						fraVariableSetup.Visible = false;
						fraSummary.Visible = false;
						fraSigFile.Visible = false;
						intAct = 1;
						break;
					}
				case 2:
					{
						// this will set the actions to the summary screen for the user to choose if
						// they want to see the report or go back and fix any accounts
						cmdFileRefresh.Visible = false;
						cmdFileChangeAct.Visible = false;
						fraVariableSetup.Visible = false;
						cmdFileExclusion.Visible = false;
						fraEdit.Visible = false;
						fraSigFile.Visible = false;
						ShowFrame(fraSummary);
						intAct = 2;
						break;
					}
			}
			//end switch
		}

		private bool ValidateVariables()
		{
			bool ValidateVariables = false;
			// this will check to make sure that all of the variables that are needed to be filled in by
			// the user are, only then are they allowed to print the report
			int intCT;
			ValidateVariables = true;
			for (intCT = 0; intCT <= modRhymalReporting.MAXFREEVARIABLES; intCT++)
			{
				if (modRhymalReporting.Statics.frfFreeReport[intCT].Required && modRhymalReporting.Statics.frfFreeReport[intCT].VariableType == 2)
				{
					// is this a required question?
					if (Strings.Trim(vsVariableSetup.TextMatrix(intCT, 1)) == "")
					{
						ValidateVariables = false;
						FCMessageBox.Show(vsVariableSetup.TextMatrix(intCT, 1) + " is a required field.", MsgBoxStyle.Critical, "Question Validation");
						vsVariableSetup.Select(intCT, 1);
						vsVariableSetup.Focus();
						break;
					}
				}
			}
			return ValidateVariables;
		}

		private void frmFreeReport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F7)
			{
				// previous
				KeyCode = 0;
				if (intAct > 0)
				{
					SetAct(FCConvert.ToInt16(intAct - 1));
				}
			}
			else if (KeyCode == Keys.F8)
			{
				// next
				KeyCode = 0;
				if (intAct == 0)
				{
					SetAct(1);
				}
			}
			else if (KeyCode == Keys.F10)
			{
				// print preview
				KeyCode = 0;
			}
			else if (KeyCode == Keys.F5)
			{
				KeyCode = 0;
				if (intAct == 1)
				{
					RefreshHardCodes();
				}
			}
		}

		private void frmFreeReport_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetTRIOColors(this);
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			CheckReportTable();
			boolInitial = frmRateRecChoice.InstancePtr.cmbRePrint.SelectedIndex == 0;
			intAct = 0;
			modGlobal.Statics.gboolUseSigFile = false;
			strRK = frmRateRecChoice.InstancePtr.strRateKeyList;
			// this will change the caption on the
			if (Strings.UCase(modCustomReport.Statics.strReportType) == "30DAYNOTICE")
			{
				this.Text = "30 Day Notice";
			}
			else if (Strings.UCase(modCustomReport.Statics.strReportType) == "LIENPROCESS")
			{
				this.Text = "Lien Certificate";
			}
			else if (Strings.UCase(modCustomReport.Statics.strReportType) == "LIENMATURITY")
			{
				this.Text = "Lien Maturity Notice";
			}
			else if (Strings.UCase(modCustomReport.Statics.strReportType) == "RNFORM")
			{
				this.Text = "Reminder Notice Form";
			}
			StartUp();
		}

		private void frmFreeReport_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				Close();
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}
		// VBto upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (boolUnloadingReport)
			{
				e.Cancel = true;
			}
			else
			{
				if (modGlobal.Statics.gboolLeaveProject)
				{
					return;
				}
				frmRateRecChoice.InstancePtr.Unload();
				if (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "30DAYNOTICE")
				{
					frmReportViewer.InstancePtr.Unload();
					//ar30DayNotice.InstancePtr.Hide();
				}
				else if (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "LIENPROCESS")
				{
					//arLienProcess.InstancePtr.Hide();
				}
				else if (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "LIENMATURITY")
				{
					//arLienMaturity.InstancePtr.Hide();
				}
				else if (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "RNFORM")
				{
					if (!boolUnloading)
					{
						boolUnloading = true;
						rptReminderForm.InstancePtr.boolNoSummary = true;
						//rptReminderForm.InstancePtr.Hide();
					}
				}
				ResetVariables();
				boolLoaded = false;
			}
		}

		private void frmFreeReport_Resize(object sender, System.EventArgs e)
		{
			int intCT;
			int intTotal;
			boolResize = true;
			FormatVariableGrid();
			FormatRecommitmentGrid();
			FormatSummaryGrid();
			if (fraSummary.Visible && fraSummary.Enabled)
			{
				SetupAccountList(vsSummary.Row);
			}

			boolResize = false;

			this.lblPseudoHeader.Left = (this.fraEdit.Width - this.lblPseudoHeader.Width) / 2;
		}

		private void lstVariable_DoubleClick(object sender, System.EventArgs e)
		{
			if (lstVariable.SelectedRows.Count > 0 && lstVariable.SelectedRows[0].Index != -1)
			{
				InsertTagAtCursor_2(FCConvert.ToInt16(lstVariable.SelectedRows[0].Tag));
				rtbData.Focus();
			}
		}

		private void mnuFileChangeAct_Click(object sender, System.EventArgs e)
		{
			switch (intAct)
			{
				case 0:
					{
						// forward to edit
						break;
					}
				case 1:
					{
						// back to questions
						if (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "RNFORM")
						{
							frmReminderNotices.InstancePtr.Show(App.MainForm);
							Close();
						}
						else
						{
							SetAct(0);
						}
						break;
					}
			}
			//end switch
		}

		private void mnuFileExclusion_Click(object sender, System.EventArgs e)
		{
			if (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "30DAYNOTICE")
			{
				frmLienExclusion.InstancePtr.Init(1, ref frmRateRecChoice.InstancePtr.lngRateRecNumber);
			}
			else if (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "LIENPROCESS")
			{
				frmLienExclusion.InstancePtr.Init(2, ref frmRateRecChoice.InstancePtr.lngRateRecNumber);
			}
			else if (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "LIENMATURITY")
			{
				frmLienExclusion.InstancePtr.Init(3, ref frmRateRecChoice.InstancePtr.lngRateRecNumber);
			}
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void LoadVariableCombo()
		{
			// this will load the combobox of variables
			int intCT;
			lstVariable.Rows.Clear();
          for (intCT = 0; intCT <= modRhymalReporting.MAXFREEVARIABLES; intCT++)
			{
				// make sure that no empty variables get in or any questions
				if (modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser != "" && modRhymalReporting.Statics.frfFreeReport[intCT].VariableType != 2)
				{
					switch (modRhymalReporting.Statics.frfFreeReport[intCT].Tag)
					{
					// Case "ADDRESS1", "ADDRESS2", "ADDRESS3"
					// this will weed out the mailing address lines
						default:
							{
								// this will show the title of the tag and the actual tag that will show on the report
								int rowIndex = lstVariable.Rows.Add(modGlobalFunctions.PadStringWithSpaces(modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser, 30, false), modRhymalReporting.Statics.frfFreeReport[intCT].Tag);
								lstVariable.Rows[rowIndex].Tag = intCT;
								break;
							}
					}
					//end switch
				}
				else
				{
				}
			}
            //FC:FINAL:BSE #2111 items should be ordered alphabetically 
            lstVariable.Sort(lstVariable.Columns[0], System.ComponentModel.ListSortDirection.Ascending);

        }

		private void LoadReport()
		{
			// This sub will load the variables
			// Each index is considered a variable that is allowed to be selected by the
			// user unless it has VariableType = 2 which is a question.  This is for your use only.
			if (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "30DAYNOTICE")
			{
				modRhymalReporting.Statics.frfFreeReport[0].DatabaseFieldName = "MailDate";
				modRhymalReporting.Statics.frfFreeReport[0].NameToShowUser = "Mail Date (MM/DD/YYYY)";
				modRhymalReporting.Statics.frfFreeReport[0].Tag = "MAILDATE";
				modRhymalReporting.Statics.frfFreeReport[0].Required = true;
				modRhymalReporting.Statics.frfFreeReport[0].VariableType = 0;
				// 0 - Variable (Question Grid), 1 - Dynamic Variable (Recordset), 2 - Static
				modRhymalReporting.Statics.frfFreeReport[0].Type = 2;
				modRhymalReporting.Statics.frfFreeReport[0].ToolTip = "Date that this notice will be mailed.";
				modRhymalReporting.Statics.frfFreeReport[1].DatabaseFieldName = "BillingYear";
				modRhymalReporting.Statics.frfFreeReport[1].NameToShowUser = "Tax Year";
				modRhymalReporting.Statics.frfFreeReport[1].Tag = "TAXYEAR";
				modRhymalReporting.Statics.frfFreeReport[1].Required = false;
				modRhymalReporting.Statics.frfFreeReport[1].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[1].Type = 4;
				modRhymalReporting.Statics.frfFreeReport[1].ToolTip = "Billing year that this notice is being created for.";
				modRhymalReporting.Statics.frfFreeReport[2].DatabaseFieldName = "MapLot";
				modRhymalReporting.Statics.frfFreeReport[2].NameToShowUser = "Map Lot";
				modRhymalReporting.Statics.frfFreeReport[2].Tag = "MAPLOT";
				modRhymalReporting.Statics.frfFreeReport[2].Required = false;
				modRhymalReporting.Statics.frfFreeReport[2].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[2].Type = 0;
				// frfFreeReport(2).ToolTip = "Map and Lot number."
				modRhymalReporting.Statics.frfFreeReport[3].DatabaseFieldName = "BookPage";
				modRhymalReporting.Statics.frfFreeReport[3].NameToShowUser = "Book and Page";
				modRhymalReporting.Statics.frfFreeReport[3].Tag = "BOOKPAGE";
				modRhymalReporting.Statics.frfFreeReport[3].Required = false;
				modRhymalReporting.Statics.frfFreeReport[3].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[3].Type = 1;
				// frfFreeReport(3).ToolTip = "Book and Page number."
				modRhymalReporting.Statics.frfFreeReport[4].DatabaseFieldName = "Collector";
				modRhymalReporting.Statics.frfFreeReport[4].NameToShowUser = "Current Collector";
				modRhymalReporting.Statics.frfFreeReport[4].Tag = "COLLECTOR";
				modRhymalReporting.Statics.frfFreeReport[4].Required = true;
				modRhymalReporting.Statics.frfFreeReport[4].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[4].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[4].ToolTip = "This is the name of the Tax Collector.";
				modRhymalReporting.Statics.frfFreeReport[5].DatabaseFieldName = "MuniName";
				modRhymalReporting.Statics.frfFreeReport[5].NameToShowUser = "Municipality";
				modRhymalReporting.Statics.frfFreeReport[5].Tag = "MUNI";
				modRhymalReporting.Statics.frfFreeReport[5].Required = true;
				modRhymalReporting.Statics.frfFreeReport[5].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[5].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[5].ToolTip = "Municipality's name the way that it should be shown.";
				modRhymalReporting.Statics.frfFreeReport[6].DatabaseFieldName = "County";
				modRhymalReporting.Statics.frfFreeReport[6].NameToShowUser = "County";
				modRhymalReporting.Statics.frfFreeReport[6].Tag = "COUNTY";
				modRhymalReporting.Statics.frfFreeReport[6].Required = true;
				modRhymalReporting.Statics.frfFreeReport[6].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[6].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[6].ToolTip = "County that the municipality is located in.";
				modRhymalReporting.Statics.frfFreeReport[7].DatabaseFieldName = "State";
				modRhymalReporting.Statics.frfFreeReport[7].NameToShowUser = "State (ie. Maine)";
				modRhymalReporting.Statics.frfFreeReport[7].Tag = "STATE";
				modRhymalReporting.Statics.frfFreeReport[7].Required = true;
				modRhymalReporting.Statics.frfFreeReport[7].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[7].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[7].ToolTip = "State that the municipality is located in.";
				modRhymalReporting.Statics.frfFreeReport[8].DatabaseFieldName = "Zip";
				modRhymalReporting.Statics.frfFreeReport[8].NameToShowUser = "Zip Code";
				modRhymalReporting.Statics.frfFreeReport[8].Tag = "ZIP";
				modRhymalReporting.Statics.frfFreeReport[8].Required = true;
				modRhymalReporting.Statics.frfFreeReport[8].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[8].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[8].ToolTip = "Zip code of the municipality.";
				modRhymalReporting.Statics.frfFreeReport[9].DatabaseFieldName = "CommitmentDate";
				modRhymalReporting.Statics.frfFreeReport[9].NameToShowUser = "Commitment Date";
				modRhymalReporting.Statics.frfFreeReport[9].Tag = "COMMITMENT";
				modRhymalReporting.Statics.frfFreeReport[9].Required = true;
				modRhymalReporting.Statics.frfFreeReport[9].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[9].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[9].ToolTip = "Date on which the bills were commited.";
				modRhymalReporting.Statics.frfFreeReport[10].DatabaseFieldName = "DemandFee";
				modRhymalReporting.Statics.frfFreeReport[10].NameToShowUser = "Demand Fee";
				modRhymalReporting.Statics.frfFreeReport[10].Tag = "DEMAND";
				modRhymalReporting.Statics.frfFreeReport[10].Required = true;
				modRhymalReporting.Statics.frfFreeReport[10].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[10].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[10].ToolTip = "Amount of the demand fee applied to the account.";
				modRhymalReporting.Statics.frfFreeReport[11].DatabaseFieldName = "MailFee";
				modRhymalReporting.Statics.frfFreeReport[11].NameToShowUser = "Certified Mail Fee";
				modRhymalReporting.Statics.frfFreeReport[11].Tag = "MAILFEE";
				modRhymalReporting.Statics.frfFreeReport[11].Required = true;
				modRhymalReporting.Statics.frfFreeReport[11].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[11].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[11].ToolTip = "Amount of the Certified Mail Fee charged.";
				modRhymalReporting.Statics.frfFreeReport[12].DatabaseFieldName = "DescriptionCode";
				modRhymalReporting.Statics.frfFreeReport[12].NameToShowUser = "";
				modRhymalReporting.Statics.frfFreeReport[12].Tag = "DESCRIPTION";
				modRhymalReporting.Statics.frfFreeReport[12].Required = true;
				modRhymalReporting.Statics.frfFreeReport[12].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[12].Type = 3;
				modRhymalReporting.Statics.frfFreeReport[12].ToolTip = "This is not used.";
				modRhymalReporting.Statics.frfFreeReport[13].DatabaseFieldName = "MapPreparer";
				modRhymalReporting.Statics.frfFreeReport[13].NameToShowUser = "Map Preparer Name";
				modRhymalReporting.Statics.frfFreeReport[13].Tag = "PREPARER";
				modRhymalReporting.Statics.frfFreeReport[13].Required = true;
				modRhymalReporting.Statics.frfFreeReport[13].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[13].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[13].ToolTip = "This is the name of the Map Preparer.";
				modRhymalReporting.Statics.frfFreeReport[14].DatabaseFieldName = "MapPrepareDate";
				modRhymalReporting.Statics.frfFreeReport[14].NameToShowUser = "Map Prepared Date";
				modRhymalReporting.Statics.frfFreeReport[14].Tag = "PREPAREDATE";
				modRhymalReporting.Statics.frfFreeReport[14].Required = true;
				modRhymalReporting.Statics.frfFreeReport[14].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[14].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[14].ToolTip = "Date on which the map was prepared.";
				boolUseQuestions = true;
				// this will fill/show the questions to be asked in the same grid as the variables
				modRhymalReporting.Statics.frfFreeReport[15].ComboList = "0;Owner/address at time of billing.|1;Owner at time of billing, C/O Current owner/address.|2;Owner at time of billing at last recorded address.";
				modRhymalReporting.Statics.frfFreeReport[15].NameToShowUser = "Mail To:";
				modRhymalReporting.Statics.frfFreeReport[15].Tag = "";
				modRhymalReporting.Statics.frfFreeReport[15].Required = true;
				modRhymalReporting.Statics.frfFreeReport[15].VariableType = 2;
				modRhymalReporting.Statics.frfFreeReport[15].ToolTip = "How would you like it addressed.";
				modRhymalReporting.Statics.frfFreeReport[16].ComboList = "0;No|1;Yes";
				modRhymalReporting.Statics.frfFreeReport[16].NameToShowUser = "Do you want each taxpayer to pay the Certified Mail Fee?";
				modRhymalReporting.Statics.frfFreeReport[16].Tag = "";
				modRhymalReporting.Statics.frfFreeReport[16].Required = true;
				modRhymalReporting.Statics.frfFreeReport[16].VariableType = 2;
				modRhymalReporting.Statics.frfFreeReport[16].ToolTip = "";
				modRhymalReporting.Statics.frfFreeReport[17].DatabaseFieldName = "dblPrincipal";
				modRhymalReporting.Statics.frfFreeReport[17].NameToShowUser = "Original Principal";
				modRhymalReporting.Statics.frfFreeReport[17].Tag = "PRINCIPAL";
				modRhymalReporting.Statics.frfFreeReport[17].Required = true;
				modRhymalReporting.Statics.frfFreeReport[17].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[17].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[18].DatabaseFieldName = "dblInterest";
				modRhymalReporting.Statics.frfFreeReport[18].NameToShowUser = "Interest Due";
				modRhymalReporting.Statics.frfFreeReport[18].Tag = "INTEREST";
				modRhymalReporting.Statics.frfFreeReport[18].Required = true;
				modRhymalReporting.Statics.frfFreeReport[18].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[18].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[19].DatabaseFieldName = "dblLessPayments";
				modRhymalReporting.Statics.frfFreeReport[19].NameToShowUser = "Less Payments Line";
				modRhymalReporting.Statics.frfFreeReport[19].Tag = "LESSPAYMENTS";
				modRhymalReporting.Statics.frfFreeReport[19].Required = true;
				modRhymalReporting.Statics.frfFreeReport[19].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[19].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[20].DatabaseFieldName = "CityTownOf";
				modRhymalReporting.Statics.frfFreeReport[20].NameToShowUser = "City or Town Of";
				modRhymalReporting.Statics.frfFreeReport[20].Tag = "CITYTOWNOF";
				modRhymalReporting.Statics.frfFreeReport[20].Required = true;
				modRhymalReporting.Statics.frfFreeReport[20].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[20].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[21].DatabaseFieldName = "Name1";
				modRhymalReporting.Statics.frfFreeReport[21].NameToShowUser = "Owner Names (on bill)";
				modRhymalReporting.Statics.frfFreeReport[21].Tag = "OWNER";
				modRhymalReporting.Statics.frfFreeReport[21].Required = true;
				modRhymalReporting.Statics.frfFreeReport[21].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[21].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[22].DatabaseFieldName = "Address1";
				modRhymalReporting.Statics.frfFreeReport[22].NameToShowUser = "Mailing Address Line 1";
				modRhymalReporting.Statics.frfFreeReport[22].Tag = "ADDRESS1";
				modRhymalReporting.Statics.frfFreeReport[22].Required = true;
				modRhymalReporting.Statics.frfFreeReport[22].VariableType = 1;
				modRhymalReporting.Statics.frfFreeReport[22].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[23].DatabaseFieldName = "Address2";
				modRhymalReporting.Statics.frfFreeReport[23].NameToShowUser = "Mailing Address Line 2";
				modRhymalReporting.Statics.frfFreeReport[23].Tag = "ADDRESS2";
				modRhymalReporting.Statics.frfFreeReport[23].Required = true;
				modRhymalReporting.Statics.frfFreeReport[23].VariableType = 1;
				modRhymalReporting.Statics.frfFreeReport[23].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[24].DatabaseFieldName = "Address3";
				modRhymalReporting.Statics.frfFreeReport[24].NameToShowUser = "Mailing Address Line 3";
				modRhymalReporting.Statics.frfFreeReport[24].Tag = "ADDRESS3";
				modRhymalReporting.Statics.frfFreeReport[24].Required = true;
				modRhymalReporting.Statics.frfFreeReport[24].VariableType = 1;
				modRhymalReporting.Statics.frfFreeReport[24].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[25].DatabaseFieldName = "TotalDue";
				modRhymalReporting.Statics.frfFreeReport[25].NameToShowUser = "Total Due";
				modRhymalReporting.Statics.frfFreeReport[25].Tag = "TOTALDUE";
				modRhymalReporting.Statics.frfFreeReport[25].Required = true;
				modRhymalReporting.Statics.frfFreeReport[25].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[25].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[26].DatabaseFieldName = "Location";
				modRhymalReporting.Statics.frfFreeReport[26].NameToShowUser = "Location";
				modRhymalReporting.Statics.frfFreeReport[26].Tag = "LOCATION";
				modRhymalReporting.Statics.frfFreeReport[26].Required = true;
				modRhymalReporting.Statics.frfFreeReport[26].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[26].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[27].DatabaseFieldName = "NameandAddressBar";
				modRhymalReporting.Statics.frfFreeReport[27].NameToShowUser = "Names and Address Bar (2-3 lines)";
				modRhymalReporting.Statics.frfFreeReport[27].Tag = "NAMEADDRESS";
				modRhymalReporting.Statics.frfFreeReport[27].Required = true;
				modRhymalReporting.Statics.frfFreeReport[27].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[27].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[28].DatabaseFieldName = "0.00";
				modRhymalReporting.Statics.frfFreeReport[28].NameToShowUser = "Certified Mail Fee Total";
				modRhymalReporting.Statics.frfFreeReport[28].Tag = "CERTTOTAL";
				modRhymalReporting.Statics.frfFreeReport[28].Required = true;
				modRhymalReporting.Statics.frfFreeReport[28].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[28].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[29].ComboList = "0;No|1;Yes, without Certified Mail Fee.|2;Yes, charge Certified Mail Fee.";
				modRhymalReporting.Statics.frfFreeReport[29].NameToShowUser = "Would you like to send a copy to each Mortgage Holder?";
				modRhymalReporting.Statics.frfFreeReport[29].Tag = "";
				modRhymalReporting.Statics.frfFreeReport[29].Required = true;
				modRhymalReporting.Statics.frfFreeReport[29].VariableType = 2;
				modRhymalReporting.Statics.frfFreeReport[29].ToolTip = "";
				modRhymalReporting.Statics.frfFreeReport[30].DatabaseFieldName = "OldCollector";
				modRhymalReporting.Statics.frfFreeReport[30].NameToShowUser = "Recommitment Collector";
				modRhymalReporting.Statics.frfFreeReport[30].Tag = "OLDCOLLECTOR";
				modRhymalReporting.Statics.frfFreeReport[30].Required = false;
				modRhymalReporting.Statics.frfFreeReport[30].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[30].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[31].DatabaseFieldName = "RecommitmentDate";
				modRhymalReporting.Statics.frfFreeReport[31].NameToShowUser = "Recommitment Date";
				modRhymalReporting.Statics.frfFreeReport[31].Tag = "RECOMMITMENTDATE";
				modRhymalReporting.Statics.frfFreeReport[31].Required = false;
				modRhymalReporting.Statics.frfFreeReport[31].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[31].Type = 2;
				modRhymalReporting.Statics.frfFreeReport[32].ComboList = "0;No|1;Yes, without Certified Mail Fee.|2;Yes, charge Certified Mail Fee.";
				modRhymalReporting.Statics.frfFreeReport[32].NameToShowUser = "Send Copy To New Owner? (if different)";
				modRhymalReporting.Statics.frfFreeReport[32].Tag = "COPYTONEWOWNER";
				modRhymalReporting.Statics.frfFreeReport[32].Required = true;
				modRhymalReporting.Statics.frfFreeReport[32].VariableType = 2;
				modRhymalReporting.Statics.frfFreeReport[32].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[32].ToolTip = "Send a copy of the notice to the new owner also.";
				// as well as the person that the bill was committed to."
				// MAL@20071231: Interested Party
				// Call Reference: 104274
				modRhymalReporting.Statics.frfFreeReport[33].ComboList = "0;No|1;Yes, without Certified Mail Fee.|2;Yes, charge Certified Mail Fee.";
				modRhymalReporting.Statics.frfFreeReport[33].NameToShowUser = "Would you like to send a copy to each Interested Party?";
				modRhymalReporting.Statics.frfFreeReport[33].Tag = "";
				modRhymalReporting.Statics.frfFreeReport[33].Required = true;
				modRhymalReporting.Statics.frfFreeReport[33].VariableType = 2;
				modRhymalReporting.Statics.frfFreeReport[33].ToolTip = "";
				modRhymalReporting.Statics.frfFreeReport[34].ComboList = "0;Show all locations.|1;Show location if only one.|2;Do not show location.";
				modRhymalReporting.Statics.frfFreeReport[34].NameToShowUser = "Show Location?";
				modRhymalReporting.Statics.frfFreeReport[34].Tag = "SHOWLOCATION";
				modRhymalReporting.Statics.frfFreeReport[34].Required = true;
				modRhymalReporting.Statics.frfFreeReport[34].VariableType = 2;
				modRhymalReporting.Statics.frfFreeReport[34].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[34].ToolTip = "Show multiple location with this rule.";
			}
			else if (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "LIENPROCESS")
			{
				modRhymalReporting.Statics.frfFreeReport[0].DatabaseFieldName = "Filing Date";
				modRhymalReporting.Statics.frfFreeReport[0].NameToShowUser = "Filing Date (MM/dd/yyyy)";
				modRhymalReporting.Statics.frfFreeReport[0].Tag = "FILINGDATE";
				modRhymalReporting.Statics.frfFreeReport[0].Required = true;
				modRhymalReporting.Statics.frfFreeReport[0].VariableType = 0;
				// 0 - Variable (Question Grid), 1 - Dynamic Variable (Recordset), 2 - Static
				modRhymalReporting.Statics.frfFreeReport[0].Type = 2;
				modRhymalReporting.Statics.frfFreeReport[0].ToolTip = "Date that the Lien will be filed.";
				modRhymalReporting.Statics.frfFreeReport[1].DatabaseFieldName = "BillingYear";
				modRhymalReporting.Statics.frfFreeReport[1].NameToShowUser = "Tax Year";
				modRhymalReporting.Statics.frfFreeReport[1].Tag = "TAXYEAR";
				modRhymalReporting.Statics.frfFreeReport[1].Required = false;
				modRhymalReporting.Statics.frfFreeReport[1].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[1].Type = 4;
				modRhymalReporting.Statics.frfFreeReport[1].ToolTip = "Billing year that this lien is being created for.";
				modRhymalReporting.Statics.frfFreeReport[2].DatabaseFieldName = "MapLot";
				modRhymalReporting.Statics.frfFreeReport[2].NameToShowUser = "Map Lot";
				modRhymalReporting.Statics.frfFreeReport[2].Tag = "MAPLOT";
				modRhymalReporting.Statics.frfFreeReport[2].Required = false;
				modRhymalReporting.Statics.frfFreeReport[2].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[2].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[3].DatabaseFieldName = "BookPage";
				modRhymalReporting.Statics.frfFreeReport[3].NameToShowUser = "Book and Page";
				modRhymalReporting.Statics.frfFreeReport[3].Tag = "BOOKPAGE";
				modRhymalReporting.Statics.frfFreeReport[3].Required = false;
				modRhymalReporting.Statics.frfFreeReport[3].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[3].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[4].DatabaseFieldName = "Collector";
				modRhymalReporting.Statics.frfFreeReport[4].NameToShowUser = "Current Collector";
				modRhymalReporting.Statics.frfFreeReport[4].Tag = "COLLECTOR";
				modRhymalReporting.Statics.frfFreeReport[4].Required = true;
				modRhymalReporting.Statics.frfFreeReport[4].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[4].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[4].ToolTip = "This is the name of the Tax Collector.";
				modRhymalReporting.Statics.frfFreeReport[5].DatabaseFieldName = "MuniName";
				modRhymalReporting.Statics.frfFreeReport[5].NameToShowUser = "Municipality";
				modRhymalReporting.Statics.frfFreeReport[5].Tag = "MUNI";
				modRhymalReporting.Statics.frfFreeReport[5].Required = true;
				modRhymalReporting.Statics.frfFreeReport[5].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[5].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[5].ToolTip = "Municipality's name the way that it should be shown.";
				modRhymalReporting.Statics.frfFreeReport[6].DatabaseFieldName = "County";
				modRhymalReporting.Statics.frfFreeReport[6].NameToShowUser = "County";
				modRhymalReporting.Statics.frfFreeReport[6].Tag = "COUNTY";
				modRhymalReporting.Statics.frfFreeReport[6].Required = true;
				modRhymalReporting.Statics.frfFreeReport[6].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[6].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[6].ToolTip = "County that the municipality is located in.";
				modRhymalReporting.Statics.frfFreeReport[7].DatabaseFieldName = "State";
				modRhymalReporting.Statics.frfFreeReport[7].NameToShowUser = "State (ie. Maine)";
				modRhymalReporting.Statics.frfFreeReport[7].Tag = "STATE";
				modRhymalReporting.Statics.frfFreeReport[7].Required = true;
				modRhymalReporting.Statics.frfFreeReport[7].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[7].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[7].ToolTip = "State that the municipality is located in.";
				modRhymalReporting.Statics.frfFreeReport[8].DatabaseFieldName = "JOP";
				modRhymalReporting.Statics.frfFreeReport[8].NameToShowUser = "Signer's Designation";
				modRhymalReporting.Statics.frfFreeReport[8].Tag = "JOP";
				modRhymalReporting.Statics.frfFreeReport[8].Required = false;
				modRhymalReporting.Statics.frfFreeReport[8].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[8].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[8].ToolTip = "Signer's title.  (ie. Justice of the Peace)";
				modRhymalReporting.Statics.frfFreeReport[9].DatabaseFieldName = "Signer";
				modRhymalReporting.Statics.frfFreeReport[9].NameToShowUser = "Signer";
				modRhymalReporting.Statics.frfFreeReport[9].Tag = "SIGNER";
				modRhymalReporting.Statics.frfFreeReport[9].Required = false;
				modRhymalReporting.Statics.frfFreeReport[9].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[9].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[9].ToolTip = "Signer's name.";
				modRhymalReporting.Statics.frfFreeReport[10].DatabaseFieldName = "CommissionExpiration";
				modRhymalReporting.Statics.frfFreeReport[10].NameToShowUser = "Commission Expiration Date";
				modRhymalReporting.Statics.frfFreeReport[10].Tag = "COMMISSIONEXPIRATION";
				modRhymalReporting.Statics.frfFreeReport[10].Required = false;
				modRhymalReporting.Statics.frfFreeReport[10].VariableType = 0;
				// MAL@20071109: Changed type to Date
				// frfFreeReport(10).Type = 0
				modRhymalReporting.Statics.frfFreeReport[10].Type = 2;
				modRhymalReporting.Statics.frfFreeReport[10].ToolTip = "Date in which the signer's commission expires.";
				modRhymalReporting.Statics.frfFreeReport[11].DatabaseFieldName = "FilingFee";
				// MAL@20080718: Changed wording to eliminate some confusion
				// Tracker Reference: 14195
				// frfFreeReport(11).NameToShowUser = "Filing Fee"
				modRhymalReporting.Statics.frfFreeReport[11].NameToShowUser = "Lien Filing, Municipal, and Recording/Discharge Fees";
				modRhymalReporting.Statics.frfFreeReport[11].Tag = "FILINGFEE";
				modRhymalReporting.Statics.frfFreeReport[11].Required = true;
				modRhymalReporting.Statics.frfFreeReport[11].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[11].Type = 1;
				// frfFreeReport(11).ToolTip = "Amount of the filing fee applied to the account."
				modRhymalReporting.Statics.frfFreeReport[11].ToolTip = "Amount of the fees applied to the account (Do NOT include 30 Day demand fees).";
				modRhymalReporting.Statics.frfFreeReport[12].DatabaseFieldName = "MailFee";
				modRhymalReporting.Statics.frfFreeReport[12].NameToShowUser = "Certified Mail Fee";
				modRhymalReporting.Statics.frfFreeReport[12].Tag = "MAILFEE";
				modRhymalReporting.Statics.frfFreeReport[12].Required = true;
				modRhymalReporting.Statics.frfFreeReport[12].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[12].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[12].ToolTip = "Amount of the Certified Mail Fee charged.";
				modRhymalReporting.Statics.frfFreeReport[13].DatabaseFieldName = "Address Bar";
				modRhymalReporting.Statics.frfFreeReport[13].NameToShowUser = "Address Bar (2-3 Lines)";
				modRhymalReporting.Statics.frfFreeReport[13].Tag = "ADDRESSBAR";
				modRhymalReporting.Statics.frfFreeReport[13].Required = true;
				modRhymalReporting.Statics.frfFreeReport[13].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[13].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[14].DatabaseFieldName = "MapPreparer";
				modRhymalReporting.Statics.frfFreeReport[14].NameToShowUser = "Map Preparer Name";
				modRhymalReporting.Statics.frfFreeReport[14].Tag = "PREPARER";
				modRhymalReporting.Statics.frfFreeReport[14].Required = true;
				modRhymalReporting.Statics.frfFreeReport[14].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[14].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[14].ToolTip = "The name of the map preparer.";
				modRhymalReporting.Statics.frfFreeReport[15].DatabaseFieldName = "MapPrepareDate";
				modRhymalReporting.Statics.frfFreeReport[15].NameToShowUser = "Map Prepared Date";
				modRhymalReporting.Statics.frfFreeReport[15].Tag = "PREPAREDATE";
				modRhymalReporting.Statics.frfFreeReport[15].Required = true;
				modRhymalReporting.Statics.frfFreeReport[15].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[15].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[15].ToolTip = "Date on which the map was prepared.";
				boolUseQuestions = true;
				// this will fill/show the questions to be asked in the same grid as the variables
				modRhymalReporting.Statics.frfFreeReport[16].ComboList = "0;Owner at the time of billing.|1;Owner at time of billing, C/O Current owner/address.|2;Owner at time of billing at last recorded address.";
				modRhymalReporting.Statics.frfFreeReport[16].NameToShowUser = "Mail To:";
				modRhymalReporting.Statics.frfFreeReport[16].Tag = "";
				modRhymalReporting.Statics.frfFreeReport[16].Required = true;
				modRhymalReporting.Statics.frfFreeReport[16].VariableType = 2;
				modRhymalReporting.Statics.frfFreeReport[16].ToolTip = "How would you like it addressed.";
				modRhymalReporting.Statics.frfFreeReport[17].ComboList = "0;No|1;Yes";
				modRhymalReporting.Statics.frfFreeReport[17].NameToShowUser = "Do you want each taxpayer to pay the Certified Mail Fee?";
				modRhymalReporting.Statics.frfFreeReport[17].Tag = "";
				modRhymalReporting.Statics.frfFreeReport[17].Required = true;
				modRhymalReporting.Statics.frfFreeReport[17].VariableType = 2;
				modRhymalReporting.Statics.frfFreeReport[17].ToolTip = "";
				modRhymalReporting.Statics.frfFreeReport[18].DatabaseFieldName = "dblPrincipal";
				modRhymalReporting.Statics.frfFreeReport[18].NameToShowUser = "Original Principal";
				modRhymalReporting.Statics.frfFreeReport[18].Tag = "PRINCIPAL";
				modRhymalReporting.Statics.frfFreeReport[18].Required = true;
				modRhymalReporting.Statics.frfFreeReport[18].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[18].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[19].DatabaseFieldName = "dblInterest";
				modRhymalReporting.Statics.frfFreeReport[19].NameToShowUser = "Interest Due";
				modRhymalReporting.Statics.frfFreeReport[19].Tag = "INTEREST";
				modRhymalReporting.Statics.frfFreeReport[19].Required = true;
				modRhymalReporting.Statics.frfFreeReport[19].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[19].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[20].DatabaseFieldName = "dblLessPayments";
				modRhymalReporting.Statics.frfFreeReport[20].NameToShowUser = "Less Payments Line";
				modRhymalReporting.Statics.frfFreeReport[20].Tag = "LESSPAYMENTS";
				modRhymalReporting.Statics.frfFreeReport[20].Required = true;
				modRhymalReporting.Statics.frfFreeReport[20].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[20].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[21].DatabaseFieldName = "CityTownOf";
				modRhymalReporting.Statics.frfFreeReport[21].NameToShowUser = "City or Town Of";
				modRhymalReporting.Statics.frfFreeReport[21].Tag = "CITYTOWNOF";
				modRhymalReporting.Statics.frfFreeReport[21].Required = true;
				modRhymalReporting.Statics.frfFreeReport[21].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[21].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[22].DatabaseFieldName = "Name1";
				modRhymalReporting.Statics.frfFreeReport[22].NameToShowUser = "Owner Names (on bill)";
				modRhymalReporting.Statics.frfFreeReport[22].Tag = "OWNER";
				modRhymalReporting.Statics.frfFreeReport[22].Required = true;
				modRhymalReporting.Statics.frfFreeReport[22].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[22].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[23].DatabaseFieldName = "Address1";
				modRhymalReporting.Statics.frfFreeReport[23].NameToShowUser = "Mailing Address Line 1";
				modRhymalReporting.Statics.frfFreeReport[23].Tag = "ADDRESS1";
				modRhymalReporting.Statics.frfFreeReport[23].Required = true;
				modRhymalReporting.Statics.frfFreeReport[23].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[23].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[24].DatabaseFieldName = "Address2";
				modRhymalReporting.Statics.frfFreeReport[24].NameToShowUser = "Mailing Address Line 2";
				modRhymalReporting.Statics.frfFreeReport[24].Tag = "ADDRESS2";
				modRhymalReporting.Statics.frfFreeReport[24].Required = true;
				modRhymalReporting.Statics.frfFreeReport[24].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[24].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[25].DatabaseFieldName = "Address3";
				modRhymalReporting.Statics.frfFreeReport[25].NameToShowUser = "Mailing Address Line 3";
				modRhymalReporting.Statics.frfFreeReport[25].Tag = "ADDRESS3";
				modRhymalReporting.Statics.frfFreeReport[25].Required = true;
				modRhymalReporting.Statics.frfFreeReport[25].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[25].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[26].DatabaseFieldName = "TotalDue";
				modRhymalReporting.Statics.frfFreeReport[26].NameToShowUser = "Total Due";
				modRhymalReporting.Statics.frfFreeReport[26].Tag = "TOTALDUE";
				modRhymalReporting.Statics.frfFreeReport[26].Required = true;
				modRhymalReporting.Statics.frfFreeReport[26].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[26].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[27].DatabaseFieldName = "Location";
				modRhymalReporting.Statics.frfFreeReport[27].NameToShowUser = "Location";
				modRhymalReporting.Statics.frfFreeReport[27].Tag = "LOCATION";
				modRhymalReporting.Statics.frfFreeReport[27].Required = true;
				modRhymalReporting.Statics.frfFreeReport[27].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[27].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[28].DatabaseFieldName = "CostAndSignature";
				modRhymalReporting.Statics.frfFreeReport[28].NameToShowUser = "Cost and Signature (6 lines)";
				modRhymalReporting.Statics.frfFreeReport[28].Tag = "SIGNATURELINE";
				modRhymalReporting.Statics.frfFreeReport[28].Required = true;
				modRhymalReporting.Statics.frfFreeReport[28].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[28].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[29].DatabaseFieldName = "0.00";
				modRhymalReporting.Statics.frfFreeReport[29].NameToShowUser = "Certified Mail Fee Total";
				modRhymalReporting.Statics.frfFreeReport[29].Tag = "CERTTOTAL";
				modRhymalReporting.Statics.frfFreeReport[29].Required = true;
				modRhymalReporting.Statics.frfFreeReport[29].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[29].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[30].ComboList = "0;No|1;Yes, without Certified Mail Fee.|2;Yes, charge Certified Mail Fee.";
				modRhymalReporting.Statics.frfFreeReport[30].NameToShowUser = "Would you like to send a copy to each Mortgage Holder?";
				modRhymalReporting.Statics.frfFreeReport[30].Tag = "";
				modRhymalReporting.Statics.frfFreeReport[30].Required = true;
				modRhymalReporting.Statics.frfFreeReport[30].VariableType = 2;
				modRhymalReporting.Statics.frfFreeReport[30].ToolTip = "";
				modRhymalReporting.Statics.frfFreeReport[31].DatabaseFieldName = "OldCollector";
				modRhymalReporting.Statics.frfFreeReport[31].NameToShowUser = "Recommitment Collector";
				modRhymalReporting.Statics.frfFreeReport[31].Tag = "OLDCOLLECTOR";
				modRhymalReporting.Statics.frfFreeReport[31].Required = false;
				modRhymalReporting.Statics.frfFreeReport[31].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[31].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[32].DatabaseFieldName = "RecommitmentDate";
				modRhymalReporting.Statics.frfFreeReport[32].NameToShowUser = "Recommitment Date";
				modRhymalReporting.Statics.frfFreeReport[32].Tag = "RECOMMITMENTDATE";
				modRhymalReporting.Statics.frfFreeReport[32].Required = false;
				modRhymalReporting.Statics.frfFreeReport[32].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[32].Type = 2;
				modRhymalReporting.Statics.frfFreeReport[33].DatabaseFieldName = "DemandFees";
				modRhymalReporting.Statics.frfFreeReport[33].NameToShowUser = "Demand Fees";
				modRhymalReporting.Statics.frfFreeReport[33].Tag = "DEMAND";
				modRhymalReporting.Statics.frfFreeReport[33].Required = false;
				modRhymalReporting.Statics.frfFreeReport[33].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[33].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[34].ComboList = "0;No|1;Yes, without Certified Mail Fee.|2;Yes, charge Certified Mail Fee.";
				modRhymalReporting.Statics.frfFreeReport[34].NameToShowUser = "Send Copy To New Owner? (if different)";
				modRhymalReporting.Statics.frfFreeReport[34].Tag = "COPYTONEWOWNER";
				modRhymalReporting.Statics.frfFreeReport[34].Required = true;
				modRhymalReporting.Statics.frfFreeReport[34].VariableType = 2;
				modRhymalReporting.Statics.frfFreeReport[34].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[34].ToolTip = "Send a copy of the notice to the new owner also.";
				// as well as the person that the bill was committed to."
				// MAL@20071231: Interested Party
				// Call Reference: 104274
				modRhymalReporting.Statics.frfFreeReport[35].ComboList = "0;No|1;Yes, without Certified Mail Fee.|2;Yes, charge Certified Mail Fee.";
				modRhymalReporting.Statics.frfFreeReport[35].NameToShowUser = "Would you like to send a copy to each Interested Party?";
				modRhymalReporting.Statics.frfFreeReport[35].Tag = "";
				modRhymalReporting.Statics.frfFreeReport[35].Required = true;
				modRhymalReporting.Statics.frfFreeReport[35].VariableType = 2;
				modRhymalReporting.Statics.frfFreeReport[35].ToolTip = "";
				modRhymalReporting.Statics.frfFreeReport[36].ComboList = "0;Show all locations.|1;Show location if only one.|2;Do not show location.";
				modRhymalReporting.Statics.frfFreeReport[36].NameToShowUser = "Show Location?";
				modRhymalReporting.Statics.frfFreeReport[36].Tag = "SHOWLOCATION";
				modRhymalReporting.Statics.frfFreeReport[36].Required = true;
				modRhymalReporting.Statics.frfFreeReport[36].VariableType = 2;
				modRhymalReporting.Statics.frfFreeReport[36].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[36].ToolTip = "Show multiple location with this rule.";
				modRhymalReporting.Statics.frfFreeReport[37].DatabaseFieldName = "CommitmentDate";
				modRhymalReporting.Statics.frfFreeReport[37].NameToShowUser = "Commitment Date";
				modRhymalReporting.Statics.frfFreeReport[37].Tag = "COMMITMENT";
				modRhymalReporting.Statics.frfFreeReport[37].Required = true;
				modRhymalReporting.Statics.frfFreeReport[37].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[37].Type = 2;
				modRhymalReporting.Statics.frfFreeReport[37].ToolTip = "Date on which the bills were commited.";
			}
			else if (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "LIENMATURITY")
			{
				modRhymalReporting.Statics.frfFreeReport[0].DatabaseFieldName = "BillingYear";
				modRhymalReporting.Statics.frfFreeReport[0].NameToShowUser = "Tax Year";
				modRhymalReporting.Statics.frfFreeReport[0].Tag = "TAXYEAR";
				modRhymalReporting.Statics.frfFreeReport[0].Required = false;
				modRhymalReporting.Statics.frfFreeReport[0].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[0].Type = 4;
				modRhymalReporting.Statics.frfFreeReport[0].ToolTip = "Billing year that this notice is being created for.";
				modRhymalReporting.Statics.frfFreeReport[1].DatabaseFieldName = "Notice Mail Date";
				modRhymalReporting.Statics.frfFreeReport[1].NameToShowUser = "Notice Mail Date (MM/dd/yyyy)";
				modRhymalReporting.Statics.frfFreeReport[1].Tag = "MAILDATE";
				modRhymalReporting.Statics.frfFreeReport[1].Required = true;
				modRhymalReporting.Statics.frfFreeReport[1].VariableType = 0;
				// 0 - Variable (Question Grid), 1 - Dynamic Variable (Recordset), 2 - Static
				modRhymalReporting.Statics.frfFreeReport[1].Type = 2;
				modRhymalReporting.Statics.frfFreeReport[1].ToolTip = "Date that this notice will be mailed.";
				modRhymalReporting.Statics.frfFreeReport[2].DatabaseFieldName = "Lien Filing Date";
				modRhymalReporting.Statics.frfFreeReport[2].NameToShowUser = "Lien Filing Date";
				modRhymalReporting.Statics.frfFreeReport[2].Tag = "LIENFILINGDATE";
				modRhymalReporting.Statics.frfFreeReport[2].Required = true;
				modRhymalReporting.Statics.frfFreeReport[2].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[2].Type = 2;
				modRhymalReporting.Statics.frfFreeReport[2].ToolTip = "Date that this lien will be filed.";
				modRhymalReporting.Statics.frfFreeReport[3].DatabaseFieldName = "BookPage";
				modRhymalReporting.Statics.frfFreeReport[3].NameToShowUser = "Book and Page";
				modRhymalReporting.Statics.frfFreeReport[3].Tag = "BOOKPAGE";
				modRhymalReporting.Statics.frfFreeReport[3].Required = false;
				modRhymalReporting.Statics.frfFreeReport[3].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[3].Type = 1;
				// we will see if this stays
				modRhymalReporting.Statics.frfFreeReport[4].DatabaseFieldName = "Collector";
				modRhymalReporting.Statics.frfFreeReport[4].NameToShowUser = "Current Collector";
				modRhymalReporting.Statics.frfFreeReport[4].Tag = "COLLECTOR";
				modRhymalReporting.Statics.frfFreeReport[4].Required = true;
				modRhymalReporting.Statics.frfFreeReport[4].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[4].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[5].DatabaseFieldName = "MuniName";
				modRhymalReporting.Statics.frfFreeReport[5].NameToShowUser = "Municipality";
				modRhymalReporting.Statics.frfFreeReport[5].Tag = "MUNI";
				modRhymalReporting.Statics.frfFreeReport[5].Required = true;
				modRhymalReporting.Statics.frfFreeReport[5].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[5].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[5].ToolTip = "Municipality's name the way that it should be shown.";
				modRhymalReporting.Statics.frfFreeReport[6].DatabaseFieldName = "County";
				modRhymalReporting.Statics.frfFreeReport[6].NameToShowUser = "County";
				modRhymalReporting.Statics.frfFreeReport[6].Tag = "COUNTY";
				modRhymalReporting.Statics.frfFreeReport[6].Required = true;
				modRhymalReporting.Statics.frfFreeReport[6].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[6].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[6].ToolTip = "County that the municipality is located in.";
				modRhymalReporting.Statics.frfFreeReport[7].DatabaseFieldName = "State (ie. Maine)";
				modRhymalReporting.Statics.frfFreeReport[7].NameToShowUser = "State";
				modRhymalReporting.Statics.frfFreeReport[7].Tag = "STATE";
				modRhymalReporting.Statics.frfFreeReport[7].Required = true;
				modRhymalReporting.Statics.frfFreeReport[7].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[7].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[7].ToolTip = "State that the municipality is located in.";
				modRhymalReporting.Statics.frfFreeReport[8].DatabaseFieldName = "JOP";
				modRhymalReporting.Statics.frfFreeReport[8].NameToShowUser = "Signer's Designation";
				modRhymalReporting.Statics.frfFreeReport[8].Tag = "JOP";
				modRhymalReporting.Statics.frfFreeReport[8].Required = false;
				modRhymalReporting.Statics.frfFreeReport[8].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[8].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[8].ToolTip = "Signer's title.  (ie. Justice of the Peace)";
				modRhymalReporting.Statics.frfFreeReport[9].DatabaseFieldName = "Signer";
				modRhymalReporting.Statics.frfFreeReport[9].NameToShowUser = "Signer";
				modRhymalReporting.Statics.frfFreeReport[9].Tag = "SIGNER";
				modRhymalReporting.Statics.frfFreeReport[9].Required = false;
				modRhymalReporting.Statics.frfFreeReport[9].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[9].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[9].ToolTip = "Signer's name.";
				modRhymalReporting.Statics.frfFreeReport[10].DatabaseFieldName = "Signer's Telephone";
				modRhymalReporting.Statics.frfFreeReport[10].NameToShowUser = "Signer's Telephone";
				modRhymalReporting.Statics.frfFreeReport[10].Tag = "SIGNERPHONE";
				modRhymalReporting.Statics.frfFreeReport[10].Required = false;
				modRhymalReporting.Statics.frfFreeReport[10].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[10].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[10].ToolTip = "Phone number where the Signer can be reached.";
				modRhymalReporting.Statics.frfFreeReport[11].DatabaseFieldName = "Treasurer Telephone";
				modRhymalReporting.Statics.frfFreeReport[11].NameToShowUser = "Treasurer's Telephone";
				modRhymalReporting.Statics.frfFreeReport[11].Tag = "COLLECTORPHONE";
				modRhymalReporting.Statics.frfFreeReport[11].Required = true;
				modRhymalReporting.Statics.frfFreeReport[11].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[11].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[11].ToolTip = "Phone number where the Tax Collector can be reached.";
				modRhymalReporting.Statics.frfFreeReport[12].DatabaseFieldName = "MailFee";
				modRhymalReporting.Statics.frfFreeReport[12].NameToShowUser = "Certified Mail Fee";
				modRhymalReporting.Statics.frfFreeReport[12].Tag = "MAILFEE";
				modRhymalReporting.Statics.frfFreeReport[12].Required = true;
				modRhymalReporting.Statics.frfFreeReport[12].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[12].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[12].ToolTip = "Amount of the Certified Mail Fee charged.";
				modRhymalReporting.Statics.frfFreeReport[13].DatabaseFieldName = "Address Bar";
				modRhymalReporting.Statics.frfFreeReport[13].NameToShowUser = "Address Bar (2-3 Lines)";
				modRhymalReporting.Statics.frfFreeReport[13].Tag = "ADDRESSBAR";
				modRhymalReporting.Statics.frfFreeReport[13].Required = true;
				modRhymalReporting.Statics.frfFreeReport[13].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[13].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[14].DatabaseFieldName = "ForeclosureFee";
				modRhymalReporting.Statics.frfFreeReport[14].NameToShowUser = "Foreclosure Fee";
				modRhymalReporting.Statics.frfFreeReport[14].Tag = "FORECLOSUREFEE";
				modRhymalReporting.Statics.frfFreeReport[14].Required = true;
				modRhymalReporting.Statics.frfFreeReport[14].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[14].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[14].ToolTip = "Amount of the lien maturity fee applied to the account.";
				modRhymalReporting.Statics.frfFreeReport[15].DatabaseFieldName = "Foreclosure Date";
				modRhymalReporting.Statics.frfFreeReport[15].NameToShowUser = "Foreclosure Date";
				modRhymalReporting.Statics.frfFreeReport[15].Tag = "FORECLOSUREDATE";
				modRhymalReporting.Statics.frfFreeReport[15].Required = true;
				modRhymalReporting.Statics.frfFreeReport[15].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[15].Type = 2;
				modRhymalReporting.Statics.frfFreeReport[15].ToolTip = "Date that the property will be forclosed apon.";
				modRhymalReporting.Statics.frfFreeReport[16].DatabaseFieldName = "Legal Description";
				modRhymalReporting.Statics.frfFreeReport[16].NameToShowUser = "Legal Description";
				modRhymalReporting.Statics.frfFreeReport[16].Tag = "LEGALDESCRIPTION";
				modRhymalReporting.Statics.frfFreeReport[16].Required = true;
				modRhymalReporting.Statics.frfFreeReport[16].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[16].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[16].ToolTip = "This is the statute that allows the municipality to forclose upon property.  (Title 36, MRSA Section 943)";
				boolUseQuestions = true;
				// this will fill/show the questions to be asked in the same grid as the variables
				modRhymalReporting.Statics.frfFreeReport[17].ComboList = "0;Owner/address at time of billing.|1;Owner at time of billing, C/O Current owner/address.|2;Owner at time of billing at last recorded address.";
				modRhymalReporting.Statics.frfFreeReport[17].NameToShowUser = "Mail To:";
				modRhymalReporting.Statics.frfFreeReport[17].Tag = "";
				modRhymalReporting.Statics.frfFreeReport[17].Required = true;
				modRhymalReporting.Statics.frfFreeReport[17].VariableType = 2;
				modRhymalReporting.Statics.frfFreeReport[17].ToolTip = "How would you like it addressed.";
				modRhymalReporting.Statics.frfFreeReport[18].ComboList = "0;No|1;Yes";
				modRhymalReporting.Statics.frfFreeReport[18].NameToShowUser = "Do you want each taxpayer to pay the Certified Mail Fee?";
				modRhymalReporting.Statics.frfFreeReport[18].Tag = "";
				modRhymalReporting.Statics.frfFreeReport[18].Required = true;
				modRhymalReporting.Statics.frfFreeReport[18].VariableType = 2;
				modRhymalReporting.Statics.frfFreeReport[18].ToolTip = "";
				modRhymalReporting.Statics.frfFreeReport[19].DatabaseFieldName = "dblPrincipal";
				modRhymalReporting.Statics.frfFreeReport[19].NameToShowUser = "Original Principal";
				modRhymalReporting.Statics.frfFreeReport[19].Tag = "PRINCIPAL";
				modRhymalReporting.Statics.frfFreeReport[19].Required = true;
				modRhymalReporting.Statics.frfFreeReport[19].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[19].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[20].DatabaseFieldName = "dblInterest";
				modRhymalReporting.Statics.frfFreeReport[20].NameToShowUser = "Interest Due";
				modRhymalReporting.Statics.frfFreeReport[20].Tag = "INTEREST";
				modRhymalReporting.Statics.frfFreeReport[20].Required = true;
				modRhymalReporting.Statics.frfFreeReport[20].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[20].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[21].DatabaseFieldName = "dblLessPayments";
				modRhymalReporting.Statics.frfFreeReport[21].NameToShowUser = "Less Payments Line";
				modRhymalReporting.Statics.frfFreeReport[21].Tag = "LESSPAYMENTS";
				modRhymalReporting.Statics.frfFreeReport[21].Required = true;
				modRhymalReporting.Statics.frfFreeReport[21].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[21].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[22].DatabaseFieldName = "CityTownOf";
				modRhymalReporting.Statics.frfFreeReport[22].NameToShowUser = "City or Town Of";
				modRhymalReporting.Statics.frfFreeReport[22].Tag = "CITYTOWNOF";
				modRhymalReporting.Statics.frfFreeReport[22].Required = true;
				modRhymalReporting.Statics.frfFreeReport[22].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[22].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[23].DatabaseFieldName = "Name1";
				modRhymalReporting.Statics.frfFreeReport[23].NameToShowUser = "Owner Names (on bill)";
				modRhymalReporting.Statics.frfFreeReport[23].Tag = "OWNER";
				modRhymalReporting.Statics.frfFreeReport[23].Required = true;
				modRhymalReporting.Statics.frfFreeReport[23].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[23].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[24].DatabaseFieldName = "Address1";
				modRhymalReporting.Statics.frfFreeReport[24].NameToShowUser = "Mailing Address Line 1";
				modRhymalReporting.Statics.frfFreeReport[24].Tag = "ADDRESS1";
				modRhymalReporting.Statics.frfFreeReport[24].Required = true;
				modRhymalReporting.Statics.frfFreeReport[24].VariableType = 1;
				modRhymalReporting.Statics.frfFreeReport[24].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[25].DatabaseFieldName = "Address2";
				modRhymalReporting.Statics.frfFreeReport[25].NameToShowUser = "Mailing Address Line 2";
				modRhymalReporting.Statics.frfFreeReport[25].Tag = "ADDRESS2";
				modRhymalReporting.Statics.frfFreeReport[25].Required = true;
				modRhymalReporting.Statics.frfFreeReport[25].VariableType = 1;
				modRhymalReporting.Statics.frfFreeReport[25].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[26].DatabaseFieldName = "Address3";
				modRhymalReporting.Statics.frfFreeReport[26].NameToShowUser = "Mailing Address Line 3";
				modRhymalReporting.Statics.frfFreeReport[26].Tag = "ADDRESS3";
				modRhymalReporting.Statics.frfFreeReport[26].Required = true;
				modRhymalReporting.Statics.frfFreeReport[26].VariableType = 1;
				modRhymalReporting.Statics.frfFreeReport[26].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[27].DatabaseFieldName = "TotalDue";
				modRhymalReporting.Statics.frfFreeReport[27].NameToShowUser = "Total Due";
				modRhymalReporting.Statics.frfFreeReport[27].Tag = "TOTALDUE";
				modRhymalReporting.Statics.frfFreeReport[27].Required = true;
				modRhymalReporting.Statics.frfFreeReport[27].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[27].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[28].DatabaseFieldName = "Location";
				modRhymalReporting.Statics.frfFreeReport[28].NameToShowUser = "Location";
				modRhymalReporting.Statics.frfFreeReport[28].Tag = "LOCATION";
				modRhymalReporting.Statics.frfFreeReport[28].Required = true;
				modRhymalReporting.Statics.frfFreeReport[28].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[28].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[29].DatabaseFieldName = "0.00";
				modRhymalReporting.Statics.frfFreeReport[29].NameToShowUser = "Certified Mail Fee Total";
				modRhymalReporting.Statics.frfFreeReport[29].Tag = "CERTTOTAL";
				modRhymalReporting.Statics.frfFreeReport[29].Required = true;
				modRhymalReporting.Statics.frfFreeReport[29].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[29].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[30].ComboList = "0;No|1;Yes, without Certified Mail Fee.|2;Yes, charge Certified Mail Fee.";
				modRhymalReporting.Statics.frfFreeReport[30].NameToShowUser = "Would you like to send a copy to each Mortgage Holder?";
				modRhymalReporting.Statics.frfFreeReport[30].Tag = "";
				modRhymalReporting.Statics.frfFreeReport[30].Required = true;
				modRhymalReporting.Statics.frfFreeReport[30].VariableType = 2;
				modRhymalReporting.Statics.frfFreeReport[30].ToolTip = "";
				modRhymalReporting.Statics.frfFreeReport[31].DatabaseFieldName = "DemandFees";
				modRhymalReporting.Statics.frfFreeReport[31].NameToShowUser = "Demand Fees";
				modRhymalReporting.Statics.frfFreeReport[31].Tag = "DEMAND";
				modRhymalReporting.Statics.frfFreeReport[31].Required = false;
				modRhymalReporting.Statics.frfFreeReport[31].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[31].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[32].DatabaseFieldName = "CommitmentDate";
				modRhymalReporting.Statics.frfFreeReport[32].NameToShowUser = "Commitment Date";
				modRhymalReporting.Statics.frfFreeReport[32].Tag = "COMMITMENT";
				modRhymalReporting.Statics.frfFreeReport[32].Required = true;
				modRhymalReporting.Statics.frfFreeReport[32].VariableType = 3;
				// MAL@20071109
				// frfFreeReport(32).Type = 0
				modRhymalReporting.Statics.frfFreeReport[32].Type = 2;
				modRhymalReporting.Statics.frfFreeReport[33].DatabaseFieldName = "MapLot";
				modRhymalReporting.Statics.frfFreeReport[33].NameToShowUser = "Map Lot";
				modRhymalReporting.Statics.frfFreeReport[33].Tag = "MAPLOT";
				modRhymalReporting.Statics.frfFreeReport[33].Required = false;
				modRhymalReporting.Statics.frfFreeReport[33].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[33].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[34].ComboList = "0;No|1;Yes, without Certified Mail Fee.|2;Yes, charge Certified Mail Fee.";
				modRhymalReporting.Statics.frfFreeReport[34].NameToShowUser = "Send Copy To New Owner? (if different)";
				modRhymalReporting.Statics.frfFreeReport[34].Tag = "COPYTONEWOWNER";
				modRhymalReporting.Statics.frfFreeReport[34].Required = true;
				modRhymalReporting.Statics.frfFreeReport[34].VariableType = 2;
				modRhymalReporting.Statics.frfFreeReport[34].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[34].ToolTip = "Send a copy of the notice to the new owner also.";
				// as well as the person that the bill was committed to."
				// MAL@20071231: Interested Party
				// Call Reference: 104274
				modRhymalReporting.Statics.frfFreeReport[35].ComboList = "0;No|1;Yes, without Certified Mail Fee.|2;Yes, charge Certified Mail Fee.";
				modRhymalReporting.Statics.frfFreeReport[35].NameToShowUser = "Would you like to send a copy to each Interested Party?";
				modRhymalReporting.Statics.frfFreeReport[35].Tag = "";
				modRhymalReporting.Statics.frfFreeReport[35].Required = true;
				modRhymalReporting.Statics.frfFreeReport[35].VariableType = 2;
				modRhymalReporting.Statics.frfFreeReport[35].ToolTip = "";
				modRhymalReporting.Statics.frfFreeReport[36].ComboList = "0;Show all locations.|1;Show location if only one.|2;Do not show location.";
				modRhymalReporting.Statics.frfFreeReport[36].NameToShowUser = "Show Location?";
				modRhymalReporting.Statics.frfFreeReport[36].Tag = "SHOWLOCATION";
				modRhymalReporting.Statics.frfFreeReport[36].Required = true;
				modRhymalReporting.Statics.frfFreeReport[36].VariableType = 2;
				modRhymalReporting.Statics.frfFreeReport[36].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[36].ToolTip = "Show multiple location with this rule.";
				// frfFreeReport(35).DatabaseFieldName = "Interest Grace Date"
				// frfFreeReport(35).NameToShowUser = "Interest Grace Date (MM/dd/yyyy)"
				// frfFreeReport(35).Tag = "GRACEDATE"
				// frfFreeReport(35).Required = True
				// frfFreeReport(35).VariableType = 0            '0 - Variable (Question Grid), 1 - Dynamic Variable (Recordset), 2 - Static
				// frfFreeReport(35).Type = 2
				// frfFreeReport(35).ToolTip = "Date that the interest will start accruing after the Maturity Fees are applied.  Do not put anything if interest should just accrue normally."
			}
			else if (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "RNFORM")
			{
				modRhymalReporting.Statics.frfFreeReport[0].DatabaseFieldName = "Account";
				modRhymalReporting.Statics.frfFreeReport[0].NameToShowUser = "Account Number";
				modRhymalReporting.Statics.frfFreeReport[0].Tag = "ACCOUNT";
				modRhymalReporting.Statics.frfFreeReport[0].Required = false;
				modRhymalReporting.Statics.frfFreeReport[0].VariableType = 3;
				// 0 - Variable (Question Grid), 1 - Dynamic Variable (Recordset), 2 - Static, 3 - Calculated
				modRhymalReporting.Statics.frfFreeReport[0].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[0].ToolTip = "";
				modRhymalReporting.Statics.frfFreeReport[1].DatabaseFieldName = "Name";
				modRhymalReporting.Statics.frfFreeReport[1].NameToShowUser = "Owner Name";
				modRhymalReporting.Statics.frfFreeReport[1].Tag = "OWNER";
				modRhymalReporting.Statics.frfFreeReport[1].Required = false;
				modRhymalReporting.Statics.frfFreeReport[1].VariableType = 3;
				// 0 - Variable (Question Grid), 1 - Dynamic Variable (Recordset), 2 - Static
				modRhymalReporting.Statics.frfFreeReport[1].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[1].ToolTip = "";
				modRhymalReporting.Statics.frfFreeReport[2].DatabaseFieldName = "MailDate";
				modRhymalReporting.Statics.frfFreeReport[2].NameToShowUser = "Mail Date (MM/dd/yyyy)";
				modRhymalReporting.Statics.frfFreeReport[2].Tag = "MAILDATE";
				modRhymalReporting.Statics.frfFreeReport[2].Required = false;
				modRhymalReporting.Statics.frfFreeReport[2].VariableType = 3;
				// 0 - Variable (Question Grid), 1 - Dynamic Variable (Recordset), 2 - Static
				modRhymalReporting.Statics.frfFreeReport[2].Type = 2;
				modRhymalReporting.Statics.frfFreeReport[2].ToolTip = "Mailing date.";
				modRhymalReporting.Statics.frfFreeReport[3].DatabaseFieldName = "Interest Department";
				modRhymalReporting.Statics.frfFreeReport[3].NameToShowUser = "Interest Department";
				modRhymalReporting.Statics.frfFreeReport[3].Tag = "INTDEPT";
				modRhymalReporting.Statics.frfFreeReport[3].Required = false;
				modRhymalReporting.Statics.frfFreeReport[3].VariableType = 3;
				// 0 - Variable (Question Grid), 1 - Dynamic Variable (Recordset), 2 - Static
				modRhymalReporting.Statics.frfFreeReport[3].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[3].ToolTip = "Date that this notice will be mailed.";
				modRhymalReporting.Statics.frfFreeReport[4].DatabaseFieldName = "Interest Department Phone";
				modRhymalReporting.Statics.frfFreeReport[4].NameToShowUser = "Interest Department Phone";
				modRhymalReporting.Statics.frfFreeReport[4].Tag = "INTDEPTPHONE";
				modRhymalReporting.Statics.frfFreeReport[4].Required = false;
				modRhymalReporting.Statics.frfFreeReport[4].VariableType = 3;
				// 0 - Variable (Question Grid), 1 - Dynamic Variable (Recordset), 2 - Static, 3 - Calculated
				modRhymalReporting.Statics.frfFreeReport[4].Type = 0;
				// 0 - Text, 1 - Number, 2 - Date, 3 - Do not ask for default (comes from DB), 4 = Formatted Year
				modRhymalReporting.Statics.frfFreeReport[4].ToolTip = "";
				modRhymalReporting.Statics.frfFreeReport[5].DatabaseFieldName = "Matured Year Department";
				modRhymalReporting.Statics.frfFreeReport[5].NameToShowUser = "Matured Year Department";
				modRhymalReporting.Statics.frfFreeReport[5].Tag = "MATDEPT";
				modRhymalReporting.Statics.frfFreeReport[5].Required = false;
				modRhymalReporting.Statics.frfFreeReport[5].VariableType = 3;
				// 0 - Variable (Question Grid), 1 - Dynamic Variable (Recordset), 2 - Static, 3 - Calculated
				modRhymalReporting.Statics.frfFreeReport[5].Type = 1;
				// 0 - Text, 1 - Number, 2 - Date, 3 - Do not ask for default (comes from DB), 4 = Formatted Year
				modRhymalReporting.Statics.frfFreeReport[5].ToolTip = "";
				modRhymalReporting.Statics.frfFreeReport[6].DatabaseFieldName = "Matured Year Department Phone";
				modRhymalReporting.Statics.frfFreeReport[6].NameToShowUser = "Matured Year Department Phone";
				modRhymalReporting.Statics.frfFreeReport[6].Tag = "MATDEPTPHONE";
				modRhymalReporting.Statics.frfFreeReport[6].Required = false;
				modRhymalReporting.Statics.frfFreeReport[6].VariableType = 3;
				// 0 - Variable (Question Grid), 1 - Dynamic Variable (Recordset), 2 - Static, 3 - Calculated
				modRhymalReporting.Statics.frfFreeReport[6].Type = 0;
				// 0 - Text, 1 - Number, 2 - Date, 3 - Do not ask for default (comes from DB), 4 = Formatted Year
				modRhymalReporting.Statics.frfFreeReport[6].ToolTip = "";
				modRhymalReporting.Statics.frfFreeReport[7].DatabaseFieldName = "High Year";
				modRhymalReporting.Statics.frfFreeReport[7].NameToShowUser = "High Year";
				modRhymalReporting.Statics.frfFreeReport[7].Tag = "HIGHYEAR";
				modRhymalReporting.Statics.frfFreeReport[7].Required = false;
				modRhymalReporting.Statics.frfFreeReport[7].VariableType = 3;
				// 0 - Variable (Question Grid), 1 - Dynamic Variable (Recordset), 2 - Static, 3 - Calculated
				modRhymalReporting.Statics.frfFreeReport[7].Type = 4;
				// 0 - Text, 1 - Number, 2 - Date, 3 - Do not ask for default (comes from DB), 4 = Formatted Year
				modRhymalReporting.Statics.frfFreeReport[7].ToolTip = "";
				modRhymalReporting.Statics.frfFreeReport[8].DatabaseFieldName = "Last Year Matured";
				modRhymalReporting.Statics.frfFreeReport[8].NameToShowUser = "Last Year Matured";
				modRhymalReporting.Statics.frfFreeReport[8].Tag = "LASTYEARMAT";
				modRhymalReporting.Statics.frfFreeReport[8].Required = false;
				modRhymalReporting.Statics.frfFreeReport[8].VariableType = 3;
				// 0 - Variable (Question Grid), 1 - Dynamic Variable (Recordset), 2 - Static, 3 - Calculated
				modRhymalReporting.Statics.frfFreeReport[8].Type = 4;
				// 0 - Text, 1 - Number, 2 - Date, 3 - Do not ask for default (comes from DB), 4 = Formatted Year
				modRhymalReporting.Statics.frfFreeReport[8].ToolTip = "";
				modRhymalReporting.Statics.frfFreeReport[9].DatabaseFieldName = "InterestDate";
				modRhymalReporting.Statics.frfFreeReport[9].NameToShowUser = "Interest Date (MM/dd/yyyy)";
				modRhymalReporting.Statics.frfFreeReport[9].Tag = "INTDATE";
				modRhymalReporting.Statics.frfFreeReport[9].Required = false;
				modRhymalReporting.Statics.frfFreeReport[9].VariableType = 3;
				// 0 - Variable (Question Grid), 1 - Dynamic Variable (Recordset), 2 - Static
				modRhymalReporting.Statics.frfFreeReport[9].Type = 2;
				modRhymalReporting.Statics.frfFreeReport[9].ToolTip = "Date that the interest will be calculated to.";
			}
			else
			{
				ResetVariables();
			}
		}

		private void LoadFreeVariablesGRID()
		{
			// this will actually put the variables and questions into the grid
			int intCT;
			int lngRows;
			lngRows = 0;
			vsVariableSetup.Rows = 0;
			// format the grid widths
			FormatVariableGrid();
			for (intCT = 0; intCT <= modRhymalReporting.MAXFREEVARIABLES; intCT++)
			{
				if (Strings.Trim(modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser) != "" && modRhymalReporting.Statics.frfFreeReport[intCT].VariableType != 1 && modRhymalReporting.Statics.frfFreeReport[intCT].VariableType != 3)
				{
					vsVariableSetup.AddItem(Strings.Trim(modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser) + "\t" + "\t" + FCConvert.ToString(intCT));
					modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber = lngRows;
					lngRows += 1;
				}
			}
		}

		private bool CheckControlDate_2(short intType)
		{
			return CheckControlDate(ref intType);
		}

		private bool CheckControlDate(ref short intType)
		{
			bool CheckControlDate = false;
			// this will find out if the old information should be used or the new information should be
			// by looking at the dates that the control records were used
			clsDRWrapper rsOld = new clsDRWrapper();
			clsDRWrapper rsNew = new clsDRWrapper();
			switch (intType)
			{
				case 1:
					{
						rsOld.OpenRecordset("SELECT * FROM Control_30DayNotice");
						rsNew.OpenRecordset("SELECT * FROM Control_LienProcess");
						if (!rsNew.EndOfFile())
						{
							if (!rsOld.EndOfFile())
							{
								if (Conversion.Val(rsOld.Get_Fields_Int32("BillingYear")) > Conversion.Val(rsNew.Get_Fields_Int32("BillingYear")))
								{
									CheckControlDate = true;
								}
								else
								{
									CheckControlDate = false;
								}
							}
							else
							{
								CheckControlDate = false;
							}
						}
						else
						{
							CheckControlDate = true;
						}
						break;
					}
				case 2:
					{
						rsOld.OpenRecordset("SELECT * FROM Control_LienProcess");
						rsNew.OpenRecordset("SELECT * FROM Control_LienMaturity");
						if (!rsNew.EndOfFile())
						{
							if (!rsOld.EndOfFile())
							{
								if (FCConvert.ToInt32(rsOld.Get_Fields_Int32("BillingYear")) - 2 > FCConvert.ToInt32(rsNew.Get_Fields_Int32("BillingYear")))
								{
									CheckControlDate = true;
								}
								else
								{
									CheckControlDate = false;
								}
							}
							else
							{
								CheckControlDate = false;
							}
						}
						else
						{
							CheckControlDate = true;
						}
						break;
					}
			}
			//end switch
			return CheckControlDate;
		}

		private void LoadDefaultInformation()
		{
			// this will put the default information into the grid if needed
			clsDRWrapper rsDefaults = new clsDRWrapper();
			clsDRWrapper rsOldDefaults = new clsDRWrapper();
			string strSQL = "";
			bool boolUseOldInfo = false;
			int intLocationType;
			if (modRhymalReporting.Statics.strFreeReportType == "30DAYNOTICE")
			{
				strSQL = "SELECT * FROM Control_30DayNotice";
			}
			else if (modRhymalReporting.Statics.strFreeReportType == "LIENPROCESS")
			{
				boolUseOldInfo = CheckControlDate_2(1);
				if (boolUseOldInfo)
				{
					strSQL = "SELECT * FROM Control_30DayNotice";
				}
				else
				{
					strSQL = "SELECT * FROM Control_LienProcess";
				}
			}
			else if (modRhymalReporting.Statics.strFreeReportType == "LIENMATURITY")
			{
				boolUseOldInfo = CheckControlDate_2(2);
				if (boolUseOldInfo)
				{
					strSQL = "SELECT * FROM Control_LienProcess";
				}
				else
				{
					strSQL = "SELECT * FROM Control_LienMaturity";
					rsOldDefaults.OpenRecordset("SELECT * FROM Control_LienMaturity", modExtraModules.strCLDatabase);
				}
			}
			else
			{
				return;
			}
			rsDefaults.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
			if (!rsDefaults.EndOfFile())
			{
				// there is a control record for the previous part of the process
				if (modRhymalReporting.Statics.strFreeReportType == "30DAYNOTICE")
				{
					// use the Control_30DayNotice table
					// 0  - Mail Date
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[0].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_DateTime("MailDate"), "MM/dd/yyyy"));
					// 4  - Collector Name
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[4].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("CollectorName")));
					// 5  - Municipality
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[5].RowNumber, 1, modGlobalConstants.Statics.MuniName);
					// 6  - County
					// TODO Get_Fields: Check the table for the column [County] and replace with corresponding Get_Field method
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[6].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields("County")));
					// 7  - State
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[7].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("State")));
					// 8  - Zip
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[8].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("Zip")));
					// 10 - Demand Fee
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[10].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_Double("Demand"), "#,##0.00"));
					// 11 - Mail Fee
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[11].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_Double("CertMailFee"), "#,##0.00"));
					// 12 - Description Code
					// .TextMatrix(frfFreeReport(7).RowNumber, 1) = rsDefaults.Get_Fields("DescriptionCode")
					// 13 - Map Preparer
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[13].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("MapPreparer")));
					// 14 - Map Prepared Date
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[14].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("MapPrepareDate")));
                    // 15 - Mail To:
                    if (rsDefaults.Get_Fields_Double("MailTo") == 0)
                    {
                        vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1, "Owner/address at time of billing."); // trocls-147 04.16.2018 kjr  CODE FREEZE, 5.22.18
                    }
                    else
                    {
                        if (FCConvert.ToInt32(rsDefaults.Get_Fields("MailTo")) == 1)
                        {
                            vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1, "Owner at time of billing, C/O Current owner/address.");
                        }
                        else
                        {
                            vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1, "Owner at time of billing at last recorded address.");
                        }
                    }
                    // 16 - Pay Certified Mail Fee?
                    if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("PayCertMailFee")))
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[16].RowNumber, 1, "Yes");
					}
					else
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[16].RowNumber, 1, "No");
					}
					// 29 - Send a copy to each mortgage holder?
					if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("SendCopyToMortHolder")))
					{
						if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("ChargeForMortHolder")))
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[29].RowNumber, 1, "Yes, charge Certified Mail Fee.");
						}
						else
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[29].RowNumber, 1, "Yes, without Certified Mail Fee.");
						}
					}
					else
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[29].RowNumber, 1, "No");
					}
					// 32 - Send copy to new owner?
					// "0;No|1;Yes, send it to the owner at the time of billing, C/O the current owner with current address.|2,Yes, owner and address as it was at the time of billing."
					if (FCConvert.ToString(rsDefaults.Get_Fields_String("SendCopyToNewOwner")) != "")
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[32].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("SendCopyToNewOwner")));
					}
					else
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[32].RowNumber, 1, "No");
					}
					intLocationType = FCConvert.ToInt32(Math.Round(Conversion.Val(rsDefaults.Get_Fields_Int32("ShowLocation"))));
					switch (intLocationType)
					{
						case 0:
							{
								frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[34].RowNumber, 1, "Show all locations.");
								break;
							}
						case 1:
							{
								frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[34].RowNumber, 1, "Show location if only one.");
								break;
							}
						case 2:
							{
								frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[34].RowNumber, 1, "Do not show location.");
								break;
							}
					}
					//end switch
					// 33 - Send Copy to Interested Parties
					if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("SendCopytoIntParty")))
					{
						if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("ChargeforIntParty")))
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[33].RowNumber, 1, "Yes, charge Certified Mail Fee.");
						}
						else
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[33].RowNumber, 1, "Yes, without Certified Mail Fee.");
						}
					}
					else
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[33].RowNumber, 1, "No");
					}
					if (FCConvert.ToString(rsDefaults.Get_Fields_String("OldCollector")) != "")
					{
						chkRecommitment.CheckState = Wisej.Web.CheckState.Checked;
						vsRecommitment.TextMatrix(0, 1, Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("OldCollector")) + " "));
						if (vsRecommitment.TextMatrix(1, 1) != "")
						{
							if (Information.IsDate(vsRecommitment.TextMatrix(1, 1)))
							{
								vsRecommitment.TextMatrix(1, 1, Strings.Format(rsDefaults.Get_Fields_DateTime("RecommitmentDate"), "MM/dd/yyyy"));
							}
							else
							{
								FCMessageBox.Show("Invalid date, defaulting to today's date.", MsgBoxStyle.Exclamation, "Invalid Date");
								vsRecommitment.TextMatrix(1, 1, Strings.Format(DateTime.Today, "MM/dd/yyyy"));
							}
						}
					}
					else
					{
						chkRecommitment.CheckState = Wisej.Web.CheckState.Unchecked;
						vsRecommitment.TextMatrix(0, 1, "");
						vsRecommitment.TextMatrix(1, 1, "");
					}
					// MAL@20080715: Add new sort order option
					// Tracker Reference: 11834
					string VBtoVar = rsDefaults.Get_Fields_String("DefaultSort");
					if (VBtoVar == "N")
					{
						cmbSortOrder.Text = "Name";
					}
					else if (VBtoVar == "A")
					{
						cmbSortOrder.Text = "Account";
					}
					else
					{
						cmbSortOrder.Text = "Name";
					}
				}
				else if (modRhymalReporting.Statics.strFreeReportType == "LIENPROCESS")
				{
					// use the Control_30DayNotice table
					if (boolUseOldInfo)
					{
						// 0  - Mail Date
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[0].RowNumber, 1, "");
					}
					else
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[0].RowNumber, 1, "");
						// .TextMatrix(frfFreeReport(0).RowNumber, 1) = Format(rsDefaults.Get_Fields("MailDate"), "MM/dd/yyyy")
					}
					// 4  - Collector Name
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[4].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("CollectorName")));
					// 5  - Municipality
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[5].RowNumber, 1, modGlobalConstants.Statics.MuniName);
					// 6  - County
					// TODO Get_Fields: Check the table for the column [County] and replace with corresponding Get_Field method
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[6].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields("County")));
					// 7  - State
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[7].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("State")));
					if (boolUseOldInfo)
					{
						// 8  - Designation
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[8].RowNumber, 1, "");
						// rsDefaults.Get_Fields("Designation")
						// 9  - Designation
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[9].RowNumber, 1, "");
						// rsDefaults.Get_Fields("Signer")
						// 10 - Commission Expiration Date
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[10].RowNumber, 1, "");
						// rsDefaults.Get_Fields("Signer")
						// 11 - Filing Fee
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[11].RowNumber, 1, "");
						// rsDefaults.Get_Fields("FilingFee")
					}
					else
					{
						// 8  - Designation
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[8].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("Designation")));
						// 9  - Designation
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[9].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("Signer")));
						// 10 - Commission Expiration Date
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[10].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_DateTime("CommissionExpirationDate")));
						// 11 - Filing Fee
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[11].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_Double("FilingFee")));
					}
					// 12 - Mail Fee
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[12].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_Double("CertMailFee"), "#,##0.00"));
					// 14 - Map Preparer
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[14].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("MapPreparer")));
					// 15 - Map Prepared Date
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("MapPrepareDate")));
                    // 16 - Mail To:
                    if (FCConvert.ToInt32(rsDefaults.Get_Fields("MailTo")) == 0)
                    {
                        vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[16].RowNumber, 1, "Owner/address at time of billing."); // trocls-147 04.16.2018 kjr  CODE FREEZE, 5.22.28
                    }
                    else
                    {
                        if (FCConvert.ToInt32(rsDefaults.Get_Fields("MailTo")) == 1)
                        {
                            vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[16].RowNumber, 1, "Owner at time of billing, C/O Current owner/address.");
                        }
                        else
                        {
                            vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[16].RowNumber, 1, "Owner at time of billing at last recorded address.");
                        }
                    }
                    // 17 - Pay Certified Mail Fee?
                    if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("PayCertMailFee")))
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[17].RowNumber, 1, "Yes");
					}
					else
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[17].RowNumber, 1, "No");
					}
					// 30 - Send a copy to each mortgage holder?
					if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("SendCopyToMortHolder")))
					{
						if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("ChargeForMortHolder")))
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[30].RowNumber, 1, "Yes, charge Certified Mail Fee.");
						}
						else
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[30].RowNumber, 1, "Yes, without Certified Mail Fee.");
						}
					}
					else
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[30].RowNumber, 1, "No");
					}
					// 34 - Send copy to new owner?
					if (FCConvert.ToString(rsDefaults.Get_Fields_String("SendCopyToNewOwner")) != "")
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[34].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("SendCopyToNewOwner")));
					}
					else
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[34].RowNumber, 1, "No");
					}
					// 35 - Send Copy to Interested Parties
					if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("SendCopytoIntParty")))
					{
						if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("ChargeforIntParty")))
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1, "Yes, charge Certified Mail Fee.");
						}
						else
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1, "Yes, without Certified Mail Fee.");
						}
					}
					else
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1, "No");
					}
					intLocationType = FCConvert.ToInt32(Math.Round(Conversion.Val(rsDefaults.Get_Fields_Int32("ShowLocation"))));
					switch (intLocationType)
					{
						case 0:
							{
								frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[36].RowNumber, 1, "Show all locations.");
								break;
							}
						case 1:
							{
								frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[36].RowNumber, 1, "Show location if only one.");
								break;
							}
						case 2:
							{
								frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[36].RowNumber, 1, "Do not show location.");
								break;
							}
					}
					//end switch
					if (FCConvert.ToString(rsDefaults.Get_Fields_String("OldCollector")) != "")
					{
						chkRecommitment.CheckState = Wisej.Web.CheckState.Checked;
						vsRecommitment.TextMatrix(0, 1, Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("OldCollector")) + " "));
						vsRecommitment.TextMatrix(1, 1, Strings.Format(rsDefaults.Get_Fields_DateTime("RecommitmentDate"), "MM/dd/yyyy"));
					}
					else
					{
						chkRecommitment.CheckState = Wisej.Web.CheckState.Unchecked;
						vsRecommitment.TextMatrix(0, 1, "");
						vsRecommitment.TextMatrix(1, 1, "");
					}
					// MAL@20080715: Add new sort order option
					// Tracker Reference: 11834
					string VBtoVar1 = rsDefaults.Get_Fields_String("DefaultSort");
					if (VBtoVar1 == "N")
					{
						cmbSortOrder.Text = "Name";
					}
					else if (VBtoVar1 == "A")
					{
						cmbSortOrder.Text = "Account";
					}
					else
					{
						cmbSortOrder.Text = "Name";
					}
					if (!boolUseOldInfo)
					{
						// this will fill in the rest of the information if there is any from the current year control
						strSQL = "SELECT * FROM Control_LienProcess";
						rsDefaults.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
						// open the lien control record
						if (!rsDefaults.EndOfFile())
						{
							// 0  - Mail Date
							if (rsDefaults.Get_Fields_DateTime("FilingDate") is DateTime)
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[0].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_DateTime("FilingDate"), "MM/dd/yyyy"));
							}
							// 8 - Signer's Designation
							if (FCConvert.ToString(rsDefaults.Get_Fields_String("Designation")) != "")
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[8].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("Designation")));
							}
							// 9 - Signer's Name
							if (FCConvert.ToString(rsDefaults.Get_Fields_String("Signer")) != "")
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[9].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("Signer")));
							}
							// 11 - Filing Fee
							if (!rsDefaults.IsFieldNull("FilingFee"))
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[11].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_Double("FilingFee"), "#,##0.00"));
							}
						}
						else
						{
							// leave everything blank
						}
					}
					else
					{
						// leave all of these fields blank
					}
				}
				else if (modRhymalReporting.Statics.strFreeReportType == "LIENMATURITY")
				{
					// use Control_LienProcess table
					if (boolUseOldInfo)
					{
						// 1  - Mail Date
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[1].RowNumber, 1, "");
						// Format(rsDefaults.Get_Fields("FilingDate"), "MM/dd/yyyy")
						// 2  - Lien Filing Date
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[2].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_DateTime("FilingDate"), "MM/dd/yyyy"));
					}
					else
					{
						// 1  - Mail Date
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[1].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_DateTime("MailDate"), "MM/dd/yyyy"));
						// 2  - Lien Filing Date
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[2].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_DateTime("LienFilingDate"), "MM/dd/yyyy"));
					}
					// 4  - Collector
					rsOldDefaults.OpenRecordset("SELECT * FROM Control_LienProcess");
					if (rsOldDefaults.EndOfFile())
					{
						modRhymalReporting.Statics.frfFreeReport[4].DatabaseFieldName = "";
					}
					else
					{
						// make sure that the collector field gets filled
						modRhymalReporting.Statics.frfFreeReport[4].DatabaseFieldName = FCConvert.ToString(rsOldDefaults.Get_Fields_String("CollectorName"));
					}
					// 5  - Municipality
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[5].RowNumber, 1, modGlobalConstants.Statics.MuniName);
					// 6  - County
					// TODO Get_Fields: Check the table for the column [County] and replace with corresponding Get_Field method
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[6].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields("County")));
					// 7  - State
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[7].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("State")));
					// 8  - Designation
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[8].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("Designation")));
					// 9  - Signer
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[9].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("Signer")));
					// 10 - Signer Phone
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[10].RowNumber, 1, "");
					// rsDefaults.Get_Fields("SignerPhone")
					// 11  - Collector Phone
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[11].RowNumber, 1, "");
					// rsDefaults.Get_Fields("CollectorPhone")
					// 12 - Mail Fee
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[12].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_Double("CertMailFee"), "#,##0.00"));
					if (boolUseOldInfo)
					{
						// 14 - Foreclosure Fee
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[14].RowNumber, 1, "");
					}
					else
					{
						// 14 - Foreclosure Fee
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[14].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_Double("ForeclosureFee"), "#,##0.00"));
					}
					// 15 - Foreclosure Date
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1, "");
					// rsDefaults.Get_Fields("ForeclosureDate")
					// 16 - Legal Description
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[16].RowNumber, 1, "Title 36, M.R.S.A. Section 943");
                    // 17 - Mail To:
                    if (FCConvert.ToInt32(rsDefaults.Get_Fields("MailTo")) == 0)
                    {
                        vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[17].RowNumber, 1, "Owner/address at time of billing."); // trocls-147 04.16.2018 kjr  CODE FREEZE,5.22.18
                    }
                    else
                    {
                        if (FCConvert.ToInt32(rsDefaults.Get_Fields("MailTo")) == 1)
                        {
                            vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[17].RowNumber, 1, "Owner at time of billing, C/O Current owner/address.");
                        }
                        else
                        {
                            vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[17].RowNumber, 1, "Owner at time of billing at last recorded address.");
                        }
                    }
                    // 18 - Pay Certified Mail Fee?
                    if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("PayCertMailFee")))
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[18].RowNumber, 1, "Yes");
					}
					else
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[18].RowNumber, 1, "No");
					}
					// 30 - Send a copy to each mortgage holder?
					if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("SendCopyToMortHolder")))
					{
						if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("ChargeForMortHolder")))
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[30].RowNumber, 1, "Yes, charge Certified Mail Fee.");
						}
						else
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[30].RowNumber, 1, "Yes, without Certified Mail Fee.");
						}
					}
					else
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[30].RowNumber, 1, "No");
					}
					// 34 - Send copy to new owner?
					if (FCConvert.ToString(rsDefaults.Get_Fields_String("SendCopyToNewOwner")) != "")
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[34].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("SendCopyToNewOwner")));
					}
					else
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[34].RowNumber, 1, "No");
					}
					// 36 - Send Copy to Interested Parties
					if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("SendCopytoIntParty")))
					{
						if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("ChargeforIntParty")))
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1, "Yes, charge Certified Mail Fee.");
						}
						else
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1, "Yes, without Certified Mail Fee.");
						}
					}
					else
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1, "No");
					}
					intLocationType = FCConvert.ToInt32(Math.Round(Conversion.Val(rsDefaults.Get_Fields_Int32("ShowLocation"))));
					switch (intLocationType)
					{
						case 0:
							{
								frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[36].RowNumber, 1, "Show all locations.");
								break;
							}
						case 1:
							{
								frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[36].RowNumber, 1, "Show location if only one.");
								break;
							}
						case 2:
							{
								frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[36].RowNumber, 1, "Do not show location.");
								break;
							}
					}
					//end switch
					// MAL@20080715: Add new sort order option
					// Tracker Reference: 11834
					string VBtoVar2 = rsDefaults.Get_Fields("DefaultSort");
					if (VBtoVar2 == "N")
					{
						cmbSortOrder.Text = "Name";
					}
					else if (VBtoVar2 == "A")
					{
						cmbSortOrder.Text = "Account";
					}
					else
					{
						cmbSortOrder.Text = "Name";
					}
					if (!boolUseOldInfo)
					{
						// this will fill in the rest of the information if there is any from the current year control
						strSQL = "SELECT * FROM Control_LienMaturity";
						rsDefaults.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
						// open the lien maturity control record
						if (!rsDefaults.EndOfFile())
						{
							// 1  - Mail Date
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[1].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_DateTime("MailDate"), "MM/dd/yyyy"));
							// 2  - Filing Date
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[2].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_DateTime("LienFilingDate"), "MM/dd/yyyy"));
							// 10 - Signer's Phone
							if (FCConvert.ToString(rsDefaults.Get_Fields_String("SignerPhone")) != "")
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[10].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("SignerPhone")));
							}
							// 11 - Tax Collectors Phone
							if (FCConvert.ToString(rsDefaults.Get_Fields_String("CollectorPhone")) != "")
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[11].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("CollectorPhone")));
							}
							// 14- Foreclosure Fee
							if (!rsDefaults.IsFieldNull("ForeclosureFee"))
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[14].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_Double("ForeclosureFee"), "#,##0.00"));
							}
							// 15- Foreclosure Date
							if (!rsDefaults.IsFieldNull("ForeclosureDate"))
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_DateTime("ForeclosureDate")));
							}
							// 16- Legal Description
							if (FCConvert.ToString(rsDefaults.Get_Fields_String("LegalDescription")) != "")
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[16].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("LegalDescription")));
							}
							else
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[16].RowNumber, 1, "Title 36, M.R.S.A. Section 943");
							}
						}
						else
						{
							// leave everything blank
						}
					}
					else
					{
						// leave everything blank
					}
				}
			}
			else
			{
				// if there is not a current control record for the last part of the process, then it will check
				// the control record of the previous year and the same part of the process
				if (modRhymalReporting.Statics.strFreeReportType == "30DAYNOTICE")
				{
					// use the 30 day notice table
				}
				else if (modRhymalReporting.Statics.strFreeReportType == "LIENPROCESS")
				{
					strSQL = "SELECT * FROM Control_LienProcess";
					rsDefaults.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
				}
				else if (modRhymalReporting.Statics.strFreeReportType == "LIENMATURITY")
				{
					strSQL = "SELECT * FROM Control_LienMaturity";
					rsDefaults.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
				}
				if (rsDefaults.EndOfFile() != true)
				{
					if (modRhymalReporting.Statics.strFreeReportType == "30DAYNOTICE")
					{
						// do nothing
						// MAL@20080715: Add new sort order option
						// Tracker Reference: 11834
						cmbSortOrder.Text = "Name";
					}
					else if (modRhymalReporting.Statics.strFreeReportType == "LIENPROCESS")
					{
						// this will use the Control_LienProcess table
						// 4  - Collector Name
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[4].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("CollectorName")));
						// 5  - Municipality
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[5].RowNumber, 1, modGlobalConstants.Statics.MuniName);
						// 6  - County
						// TODO Get_Fields: Check the table for the column [County] and replace with corresponding Get_Field method
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[6].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields("County")));
						// 7  - State
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[7].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("State")));
						// 8 - Signer
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[8].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("Signer")));
						// 9 - Designation
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[9].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("Designation")));
						// 10 - Commission Expiration Date
						if (rsDefaults.Get_Fields_DateTime("CommissionExpirationDate") is DateTime)
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[10].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_DateTime("CommissionExpirationDate"), "MM/dd/yyyy"));
						}
						else
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[10].RowNumber, 1, "");
						}
						// 11 - FilingFee
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[11].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_Double("FilingFee"), "#,##0.00"));
						// 12 - Mail Fee
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[12].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_Double("CertMailFee"), "#,##0.00"));
						// 14 - Map Preparer
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[14].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("MapPreparer")));
						// 15 - Map Prepared Date
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("MapPrepareDate")));
						// 16 - Mail To:
						if (((rsDefaults.Get_Fields_Double("MailTo"))) == 0)
                        { 
                            vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[16].RowNumber, 1, "Owner/address at time of billing."); // trocls-147 04.16.2018 kjr  CODE FREEZE, 5.22.18
                        }
                        else
                        {
                            if (FCConvert.ToInt32(rsDefaults.Get_Fields("MailTo")) == 1)
                            {
                                vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[16].RowNumber, 1, "Owner at time of billing, C/O Current owner/address.");
                            }
                            else
                            {
                                vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[16].RowNumber, 1, "Owner at time of billing at last recorded address.");
                            }
                        }
                    // 17 - Pay Certified Mail Fee?
                    if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("PayCertMailFee")))
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[17].RowNumber, 1, "Yes");
						}
						else
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[17].RowNumber, 1, "No");
						}
						// 17 - Send a copy to each mortgage holder?
						if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("SendCopyToMortHolder")))
						{
							if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("ChargeForMortHolder")))
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[30].RowNumber, 1, "Yes, charge Certified Mail Fee.");
							}
							else
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[30].RowNumber, 1, "Yes, without Certified Mail Fee.");
							}
						}
						else
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[30].RowNumber, 1, "No");
						}
						// 35 - Send Copy to Interested Parties
						if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("SendCopytoIntParty")))
						{
							if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("ChargeforIntParty")))
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1, "Yes, charge Certified Mail Fee.");
							}
							else
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1, "Yes, without Certified Mail Fee.");
							}
						}
						else
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1, "No");
						}
						// 34 - Send copy to new owner?
						if (FCConvert.ToString(rsDefaults.Get_Fields_String("SendCopyToNewOwner")) != "")
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[34].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("SendCopyToNewOwner")));
						}
						else
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[34].RowNumber, 1, "No");
						}
						intLocationType = FCConvert.ToInt32(Math.Round(Conversion.Val(rsDefaults.Get_Fields_Int32("ShowLocation"))));
						switch (intLocationType)
						{
							case 0:
								{
									frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[36].RowNumber, 1, "Show all locations.");
									break;
								}
							case 1:
								{
									frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[36].RowNumber, 1, "Show location if only one.");
									break;
								}
							case 2:
								{
									frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[36].RowNumber, 1, "Do not show location.");
									break;
								}
						}
						//end switch
						if (FCConvert.ToString(rsDefaults.Get_Fields_String("OldCollector")) != "")
						{
							chkRecommitment.CheckState = Wisej.Web.CheckState.Checked;
							vsRecommitment.TextMatrix(0, 1, Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("OldCollector")) + " "));
							vsRecommitment.TextMatrix(1, 1, Strings.Format(rsDefaults.Get_Fields_DateTime("RecommitmentDate"), "MM/dd/yyyy"));
						}
						else
						{
							chkRecommitment.CheckState = Wisej.Web.CheckState.Unchecked;
							vsRecommitment.TextMatrix(0, 1, "");
							vsRecommitment.TextMatrix(1, 1, "");
						}
						// MAL@20080715: Add new sort order option
						// Tracker Reference: 11834
						string VBtoVar3 = rsDefaults.Get_Fields_String("DefaultSort");
						if (VBtoVar3 == "N")
						{
							cmbSortOrder.Text = "Name";
						}
						else if (VBtoVar3 == "A")
						{
							cmbSortOrder.Text = "Account";
						}
						else
						{
							cmbSortOrder.Text = "Name";
						}
					}
					else if (modRhymalReporting.Statics.strFreeReportType == "LIENMATURITY")
					{
						// use Control_LienMaturity table
						// 1  - Mail Date
						// 2  - Lien Filing Date
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[2].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_DateTime("LienFilingDate")));
						// 4  - Collector
						// frfFreeReport(4).DatabaseFieldName = rsDefaults.Get_Fields("CollectorName")
						// 5  - Municipality
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[5].RowNumber, 1, modGlobalConstants.Statics.MuniName);
						// 6  - County
						// TODO Get_Fields: Check the table for the column [County] and replace with corresponding Get_Field method
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[6].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields("County")));
						// 7  - State
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[7].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("State")));
						// 8  - Designation
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[8].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("Designation")));
						// 9  - Signer
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[9].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("Signer")));
						// 10 - Signer Phone
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[10].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("SignerPhone")));
						// 11 - Collector Phone
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[11].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("CollectorPhone")));
						// 12 - Mail Fee
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[12].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_Double("CertMailFee"), "#,##0.00"));
						// 14 - Foreclosure Fee
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[13].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_Double("ForeclosureFee"), "#,##0.00"));
						// 15 - Foreclosure Date
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[14].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_DateTime("ForeclosureDate")));
						// 16 - Legal Description
						if (Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("LegalDescription"))) != "")
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("LegalDescription")));
						}
						else
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1, "Title 36, M.R.S.A. Section 943");
						}
						// 17 - Mail To:
						if (((rsDefaults.Get_Fields_Double("MailTo"))) == 0)
                        {
                            vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[17].RowNumber, 1, "Owner/address at time of billing."); // trocls-147 04.16.2018 kjr  CODE FREEZE, 5.22.18
                        }
                        else
                        {
                            if (FCConvert.ToInt32(rsDefaults.Get_Fields("MailTo")) == 1)
                            {
                                vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[17].RowNumber, 1, "Owner at time of billing, C/O Current owner/address.");
                            }
                            else
                            {
                                vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[17].RowNumber, 1, "Owner at time of billing at last recorded address.");
                            }
                        }
                        // 18 - Pay Certified Mail Fee?
                        if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("PayCertMailFee")))
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[18].RowNumber, 1, "Yes");
						}
						else
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[18].RowNumber, 1, "No");
						}
						// 30 - Send a copy to each mortgage holder?
						if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("SendCopyToMortHolder")))
						{
							if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("ChargeForMortHolder")))
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[30].RowNumber, 1, "Yes, charge Certified Mail Fee.");
							}
							else
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[30].RowNumber, 1, "Yes, without Certified Mail Fee.");
							}
						}
						else
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[30].RowNumber, 1, "No");
						}
						// 35 - Send Copy to Interested Parties
						if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("SendCopytoIntParty")))
						{
							if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("ChargeforIntParty")))
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1, "Yes, charge Certified Mail Fee.");
							}
							else
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1, "Yes, without Certified Mail Fee.");
							}
						}
						else
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1, "No");
						}
						// 34 - Send copy to new owner?
						if (FCConvert.ToString(rsDefaults.Get_Fields_String("SendCopyToNewOwner")) != "")
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[34].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("SendCopyToNewOwner")));
						}
						else
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[34].RowNumber, 1, "No");
						}
						intLocationType = FCConvert.ToInt32(Math.Round(Conversion.Val(rsDefaults.Get_Fields_Int32("ShowLocation"))));
						switch (intLocationType)
						{
							case 0:
								{
									frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[36].RowNumber, 1, "Show all locations.");
									break;
								}
							case 1:
								{
									frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[36].RowNumber, 1, "Show location if only one.");
									break;
								}
							case 2:
								{
									frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[36].RowNumber, 1, "Do not show location.");
									break;
								}
						}
						//end switch
						// MAL@20080715: Add new sort order option
						// Tracker Reference: 11834
						string VBtoVar4 = rsDefaults.Get_Fields_String("DefaultSort");
						if (VBtoVar4 == "N")
						{
							cmbSortOrder.Text = "Name";
						}
						else if (VBtoVar4 == "A")
						{
							cmbSortOrder.Text = "Account";
						}
						else
						{
							cmbSortOrder.Text = "Name";
						}
					}
				}
				else
				{
					// these are the defaults if there is no record
					if (modRhymalReporting.Statics.strFreeReportType == "30DAYNOTICE")
					{
						// do nothing
						// MAL@20080715: Add new sort order option
						// Tracker Reference: 11834
						cmbSortOrder.Text = "Name";
					}
					else if (modRhymalReporting.Statics.strFreeReportType == "LIENPROCESS")
					{
						// this will use the Control_LienProcess table
						// 4  - Collector Name
						// 5  - Municipality
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[5].RowNumber, 1, modGlobalConstants.Statics.MuniName);
						// 6  - County
						// 7  - State
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[7].RowNumber, 1, "Maine");
						// 8 - Signer
						// 9 - Designation
						// 11 - FilingFee
						// 12 - Mail Fee
						// 14 - Map Preparer
						// 15 - Map Prepared Date
						// 16 - Mail To:
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[16].RowNumber, 1, "Owner/address at time of billing.");
						// 17 - Pay Certified Mail Fee?
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[17].RowNumber, 1, "Yes");
						// 30 - Send a copy to each mortgage holder?
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[30].RowNumber, 1, "No");
						// 34 - Send copy to new owner?
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[34].RowNumber, 1, "No");
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1, "No");
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[36].RowNumber, 1, "Show all locations.");
						// MAL@20080715: Add new sort order option
						// Tracker Reference: 11834
						cmbSortOrder.Text = "Name";
					}
					else if (modRhymalReporting.Statics.strFreeReportType == "LIENMATURITY")
					{
						// use Control_LienMaturity table
						// 1  - Mail Date
						// 2  - Lien Filing Date
						// 4  - Collector
						// frfFreeReport(4).DatabaseFieldName = rsDefaults.Get_Fields("CollectorName")
						// 5  - Municipality
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[5].RowNumber, 1, modGlobalConstants.Statics.MuniName);
						// 6  - County
						// 7  - State
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[7].RowNumber, 1, "Maine");
						// 8  - Designation
						// 9  - Signer
						// 10 - Signer Phone
						// 11 - Collector Phone
						// 12 - Mail Fee
						// 14 - Foreclosure Fee
						// 15 - Foreclosure Date
						// 16 - Legal Description
						// MAL@20071002: Corrected the field setup.
						// Call Reference: 117007
						// .TextMatrix(frfFreeReport(15).RowNumber, 1) = "Title 36, M.R.S.A. Section 943"
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[16].RowNumber, 1, "Title 36, M.R.S.A. Section 943");
						// 17 - Mail To:
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[17].RowNumber, 1, "Owner/address at time of billing.");
						// 18 - Pay Certified Mail Fee?
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[18].RowNumber, 1, "Yes");
						// 30 - Send a copy to each mortgage holder?
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[30].RowNumber, 1, "No");
						// 34 - Send copy to new owner?
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[34].RowNumber, 1, "No");
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1, "No");
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[36].RowNumber, 1, "Show all locations.");
						// MAL@20080715: Add new sort order option
						// Tracker Reference: 11834
						cmbSortOrder.Text = "Name";
					}
				}
			}
		}

		private void mnuFilePrintCode_Click(object sender, System.EventArgs e)
		{
			// this will print the code from the RTB
			//! Load arTextDump;
			arTextDump.InstancePtr.strText = rtbData.Text;
			frmReportViewer.InstancePtr.Init(arTextDump.InstancePtr);
			// .Show vbModal, MDIParent
		}

		private void mnuFilePrintPreview_Click(object sender, System.EventArgs e)
		{
			try
			{
                // On Error GoTo ERROR_HANDLER
				string strReturnVariables = "";
				bool boolContinue = false;
				if (intAct > 0)
				{
					if (Strings.Trim(rtbData.Text) != "")
					{
						SetCellValue();
						if (intAct == 1)
						{
							if (Strings.UCase(modCustomReport.Statics.strReportType) != "RNFORM")
							{
								FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
								// xx                  frmWait.Init "Recalulating Eligibility." & vbCrLf & "This may take a few moments."
								if (boolInitial)
								{
									// this will check to see if this is an initial run...if so, the LienStatusEligibility field will
									// get set back to the previous level to be rechecked to see if all the accounts are still eligible
									InitialSettings();
								}
								FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
								// this will create the report and show the summary
								if (Strings.UCase(modCustomReport.Statics.strReportType) == "30DAYNOTICE")
								{
									if (CheckFormMandatoryVariables_2(1, rtbData.Text, ref strReturnVariables))
									{
										SaveDefaultNotice_8(1, cboSavedReport.Text);
										// save the default report for the next time
										// SaveKeyValue HKEY_CURRENT_USER, REGISTRYKEY & "CL\", "LASTFREEREPORT30DN", cboSavedReport.Text
										boolUnloadingReport = true;
										frmReportViewer.InstancePtr.Unload();
										//ar30DayNotice.InstancePtr.Hide();
										boolUnloadingReport = false;
										modCLCalculations.ClearCMFTable_Collections("30DN");
										// MAL@20071022: Call function to clear all existing records of this type
                                        //ar30DayNotice.InstancePtr.Unload();
										ar30DayNotice.InstancePtr.Run(false);
										boolContinue = true;
									}
									else
									{
										frmWait.InstancePtr.Unload();
										FCMessageBox.Show("Mandatory variables were omitted from the form created.  Please make sure that they are included." + "\r\n" + strReturnVariables, MsgBoxStyle.Exclamation, "Missing Variables");
									}
								}
								else if (Strings.UCase(modCustomReport.Statics.strReportType) == "LIENPROCESS")
								{
									if (CheckFormMandatoryVariables_2(2, rtbData.Text, ref strReturnVariables))
									{
										SaveDefaultNotice_8(2, cboSavedReport.Text);
										// save the default report for the next time
										// SaveKeyValue HKEY_CURRENT_USER, REGISTRYKEY & "CL\", "LASTFREEREPORTLIEN", cboSavedReport.Text
										boolUnloadingReport = true;
										// Unload arLienProcess
										frmReportViewer.InstancePtr.Unload();
										boolUnloadingReport = false;
										modCLCalculations.ClearCMFTable_Collections("LIEN");
										// MAL@20071022: Call function to clear all existing records of this type
                                        var lienProcessReport = new arLienProcess();
                                        lienProcessReport.ShowPaperSize = false;
                                        lienProcessReport.Run(false);
										//arLienProcess.InstancePtr.Run(false);
                                        reportObject = lienProcessReport;
										boolContinue = true;
									}
									else
									{
										FCMessageBox.Show("Mandatory variables were omitted from the form created.  Please make sure that they are included." + "\r\n" + strReturnVariables, MsgBoxStyle.Exclamation, "Missing Variables");
									}
								}
								else if (Strings.UCase(modCustomReport.Statics.strReportType) == "LIENMATURITY")
								{
									if (CheckFormMandatoryVariables_2(3, rtbData.Text, ref strReturnVariables))
									{
										SaveDefaultNotice_8(3, cboSavedReport.Text);
										// save the default report for the next time
										// SaveKeyValue HKEY_CURRENT_USER, REGISTRYKEY & "CL\", "LASTFREEREPORTMATURITY", cboSavedReport.Text
										boolUnloadingReport = true;
										// Unload arLienMaturity
										frmReportViewer.InstancePtr.Unload();
										boolUnloadingReport = false;
										modCLCalculations.ClearCMFTable_Collections("LMAT");
										// MAL@20071022: Call function to clear all existing records of this type
										var lienMaturityReport = new arLienMaturity();
										lienMaturityReport.ShowPaperSize = false;
										lienMaturityReport.Run(false);
										reportObject = lienMaturityReport;
										boolContinue = true;
									}
									else
									{
										FCMessageBox.Show("Mandatory variables were omitted from the form created.  Please make sure that they are included." + "\r\n" + strReturnVariables, MsgBoxStyle.Exclamation, "Missing Variables");
									}
								}
								if (boolContinue)
								{
									FormatSummaryGrid(false);
									SetAct(2);
									//SetPositions();
								}
							}
							else
							{
                                ReminderNoticeOptions options = (ReminderNoticeOptions)noticeOptions;
                                if (options.UseSignature)
								{
                                    modGlobal.Statics.gboolUseSigFile = true;
								}
								else
								{
									modGlobal.Statics.gboolUseSigFile = false;
								}
                                dblMinimumAmount = options.MinimumAmount;
                                options.OriginalString = rtbData.Text;
                                rptReminderForm.InstancePtr.Init(options); return;
							}
						}
						else if (intAct == 2)
						{
							FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
							// xx                frmWait.Init "Please Wait..." & vbCrLf & "Loading Data"
							this.Refresh();
							// this will actually show the report
							// and this will save all of the eligibility and status information
							if (Strings.UCase(modCustomReport.Statics.strReportType) == "30DAYNOTICE")
							{
                                // ar30DayNotice.Visible = True
                                frmReportViewer.InstancePtr.Init(ar30DayNotice.InstancePtr);
                                ar30DayNotice.InstancePtr.SaveLienStatus();
								// this will set all of the Bill status' to 1
								SaveContolInformation();
								// this saves the control information into the 30DayNotice control table with the defaults used for the variables
								SetAct(1);
								SaveRateRecInformation();
							}
							else if (Strings.UCase(modCustomReport.Statics.strReportType) == "LIENPROCESS")
							{
								// arLienProcess.Visible = True
                                //var lienProcessReport = new arLienProcess();
                                var lienProcessReport = (arLienProcess) reportObject;
								frmReportViewer.InstancePtr.Init(lienProcessReport);
								lienProcessReport.SaveLienStatus();
								// this will set all of the Bill status' to 3
								SaveContolInformation();
								// this saves the control information into the LienProcess control table with the defaults used for the variables
								SetAct(1);
							}
							else if (Strings.UCase(modCustomReport.Statics.strReportType) == "LIENMATURITY")
							{
								// arLienMaturity.Visible = True
								arLienMaturity lienMaturityReport = (arLienMaturity)reportObject;
								frmReportViewer.InstancePtr.Init(lienMaturityReport);
								lienMaturityReport.SaveLienStatus();
								// this will set all of the Bill status' to 5
								SaveContolInformation();
								// this saves the control information into the LienMaturity control table with the defaults used for the variables
								SetAct(1);
							}
							else if (Strings.UCase(modCustomReport.Statics.strReportType) == "RNFORM")
							{
								return;
							}
						}
						else
						{
							// intAct = 0
							SetAct(1);
							// move on to the next screen
						}
						// reset the frmwait and the turn the mousepointer back into the default rather than the hourglass
						frmWait.InstancePtr.Unload();
						FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
					}
					else
					{
						FCMessageBox.Show("There is no text to be printed.  Either load a saved report or create your own before printing.", MsgBoxStyle.Information, "Missing Data");
					}
				}
				else
				{
					// MAL@20071227: Move the focus to make sure that all recently added changes have been "set"
					// Tracker Reference: 11575
					App.MainForm.Focus();
					this.Focus();
					if (ValidateUserVariables())
					{
						// MAL@20080919: Check for CMF being charged for copies to new/current owners and warn user
						// Tracker Reference: 15104
						if (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "30DAYNOTICE")
						{
							if (Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[32].RowNumber, 1), 7) == "Yes, ch")
							{
								FCMessageBox.Show("A copy of 30 Day Notices is not required to be sent to new owners." + "\r\n" + "By selecting this option a certified mail fee will be charged.", MsgBoxStyle.Information | MsgBoxStyle.OkOnly, "Copy Not Required");
							}
						}
						else if (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "LIENPROCESS")
						{
							if (Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[17].RowNumber, 1), 7) == "Yes")
							{
								FCMessageBox.Show("A copy of Lien Notices is not required to be sent to taxpayers." + "\r\n" + "By selecting this option a certified mail fee will be charged.", MsgBoxStyle.Information | MsgBoxStyle.OkOnly, "Copy Not Required");
							}
						}
						else
						{
							// Do Nothing
						}
						if ((modCustomReport.Statics.strReportType == "LIENMATURITY") || (modCustomReport.Statics.strReportType == "30DAYNOTICE"))
						{
							// if this is a lien maturity or 30 DN
							// get the choice from the user of whether to use the mailing date or the foreclosure date as the interest date
							ASKQUESTIONAGAIN:
							;
							if (modCustomReport.Statics.strReportType == "LIENMATURITY")
							{
								frmQuestion.InstancePtr.Init(10, "Use the Mailing Date as the Interest Date", "Use the Maturity Date as the Interest Date", intDefSelection: 0);
							}
							else
							{
								frmQuestion.InstancePtr.Init(10, "Use the Mailing Date as the Interest Date", "Use the 30 Days after as the Interest Date", intDefSelection: 0);
							}
							switch (modGlobal.Statics.gintPassQuestion)
							{
								case -1:
									{
										FCMessageBox.Show("Please select an answer.", MsgBoxStyle.Exclamation, "Invalid Answer");
										goto ASKQUESTIONAGAIN;
										break;
									}
								case 0:
									{
										modGlobal.Statics.gboolUseMailDateForMaturity = true;
										break;
									}
								case 1:
									{
										modGlobal.Statics.gboolUseMailDateForMaturity = false;
										break;
									}
							}
							//end switch
						}
						if (SaveContolInformation())
						{
							SetAct(1);
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				FCMessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "PrintPreview");
			}
		}

		private void mnuFileRefresh_Click(object sender, System.EventArgs e)
		{
			RefreshHardCodes();
		}

		private void optReport_Click(int Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
					{
						// edit
						if (cboSavedReport.Items.Count == 0)
						{
							LoadCombo();
						}
						if (strCurrentReportName != "")
						{
							cboSavedReport.Text = strCurrentReportName;
						}
						cboSavedReport.Enabled = false;
						break;
					}
				case 1:
					{
						// show saved
						cboSavedReport.Enabled = true;
						LoadCombo();
						break;
					}
				case 2:
					{
						// delete
						cboSavedReport.Enabled = true;
						LoadCombo(1);
						break;
					}
				default:
					{
						break;
					}
			}
			//end switch
		}

		private void optReport_Click(object sender, System.EventArgs e)
		{
			int index = cmbReport.SelectedIndex;
			optReport_Click(index, sender, e);
		}
		//FC:FINAL:MHO: #i137: attach event handler
		//private void rtbData_KeyDown(ref Keys KeyCode, ref short Shift)
		private void rtbData_KeyDown(object sender, KeyEventArgs e)
		{
			Wisej.Web.Keys KeyCode = e.KeyCode;
			string strText = "";
			int lngStart = 0;
			int lngLen = 0;
			boolDontCheck = true;
			if (KeyCode == Keys.Back)
			{
				KeyCode = 0;
				// backspace - need to make sure that there is not a tag where the user is trying to delete
				// if it is a tag, then delete the whole thing
				NewRTB_BackSpace();
			}
			else if (KeyCode == Keys.Delete)
			{
				boolDontCheck = false;
				KeyCode = 0;
				NewRTB_Delete();
			}
			else if (KeyCode == Keys.Right)
			{
				// right arrow key
				strText = rtbData.Text;
				lngStart = rtbData.SelStart;
				lngLen = rtbData.SelLength;
				lngStart += 1;
				if (lngLen > 0)
				{
					if (e.Shift)
					{
						if (Strings.Mid(strText, lngStart + 1, 1) == "<")
						{
							KeyCode = 0;
							boolDontCheck = false;
							rtbData.SelLength = 0;
							rtbData.SelStart = lngStart + lngLen - 1;
						}
						else
						{
							rtbData.SelStart = lngStart + lngLen - 1;
						}
					}
					else
					{
						rtbData.SelStart = lngStart + lngLen - 2;
					}
				}
				else
				{
					if (Strings.Mid(strText, lngStart, 1) == "<")
					{
						KeyCode = 0;
						lngLen = Strings.InStr(lngStart, strText, ">") - lngStart;
						rtbData.SelLength = lngLen + 1;
					}
				}
			}
			else if (KeyCode == Keys.Left)
			{
				// left arrow key
				strText = rtbData.Text;
				lngStart = rtbData.SelStart;
				lngLen = rtbData.SelLength;
				if (lngStart != 0)
				{
					if (lngLen > 0)
					{
						if (e.Shift)
						{
							KeyCode = 0;
							boolDontCheck = false;
							rtbData.SelLength = 0;
							rtbData.SelStart = lngStart;
						}
						else
						{
						}
					}
					else
					{
						if (Strings.Mid(strText, lngStart, 1) == ">")
						{
							KeyCode = 0;
							lngLen += lngStart - Strings.InStrRev(strText, "<", lngStart, CompareConstants.vbTextCompare/*?*/);
							lngStart = Strings.InStrRev(strText, "<", lngStart, CompareConstants.vbTextCompare/*?*/);
							rtbData.SelStart = lngStart - 1;
							rtbData.SelLength = lngLen + 1;
							boolDontCheck = false;
						}
					}
				}
			}
			else if (KeyCode == Keys.Up)
			{
				rtbData.SelLength = 0;
				boolDontCheck = false;
			}
			else if (KeyCode == Keys.Down)
			{
				rtbData.SelLength = 0;
				boolDontCheck = false;
			}
			else if (KeyCode == Keys.Return)
			{
				rtbData.SelText = "<CRLF>";
			}
			boolDontCheck = false;
		}
		//FC:FINAL:MHO: #i137: attach event handler
		//private void rtbData_KeyPress(ref short KeyAscii)
		private void rtbData_KeyPress(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			switch (KeyAscii)
			{
				case 60:
				case 62:
					{
						KeyAscii = 0;
						break;
					}
			}
			e.KeyChar = Strings.Chr(KeyAscii);
			//end switch
		}

		private void rtbData_SelChange()
		{
			CheckInsideTag(rtbData.SelStart, rtbData.SelLength);
		}

		private void vsRecommitment_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			string strTemp;
			strTemp = Strings.Trim(vsRecommitment.TextMatrix(vsRecommitment.Row, 1));
			if (strTemp == "__/__/____" || strTemp == "____-_")
			{
				vsRecommitment.TextMatrix(vsRecommitment.Row, 1, string.Empty);
			}
		}

		private void vsRecommitment_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (vsRecommitment.Row == 1 && vsRecommitment.Col == 1)
			{
				vsRecommitment.EditMask = "##/##/####";
			}
			else
			{
				vsRecommitment.EditMask = "";
			}
		}

		private void vsRecommitment_Click(object sender, EventArgs e)
		{
			if (chkRecommitment.CheckState == Wisej.Web.CheckState.Checked)
			{
				if (vsRecommitment.Col == 1)
				{
					vsRecommitment.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsRecommitment.EditCell();
				}
				else
				{
					vsRecommitment.Editable = FCGrid.EditableSettings.flexEDNone;
				}
			}
			else
			{
				vsRecommitment.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void vsRecommitment_RowColChange(object sender, EventArgs e)
		{
			if (chkRecommitment.CheckState == Wisej.Web.CheckState.Checked)
			{
				if (vsRecommitment.Col == 1)
				{
					vsRecommitment.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsRecommitment.EditCell();
				}
				else
				{
					vsRecommitment.Editable = FCGrid.EditableSettings.flexEDNone;
				}
			}
			else
			{
				vsRecommitment.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void vsSummary_Click(object sender, EventArgs e)
		{
			// this will setup the grid below the vsSummary grid with
			// the accounts that make up the number that is in this grid
			SetupAccountList(vsSummary.Row);
		}

		private void SetupAccountList(int lngRow)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsBill = new clsDRWrapper();
				int lngRows/*unused?*/;
				string strTemp = "";
				int lngAcct = 0;
				int lngYear = 0;
				// format the grid
				vsReturnInfo.Cols = 3;
				vsReturnInfo.ColWidth(0, FCConvert.ToInt32(vsReturnInfo.WidthOriginal * 0.15));
				vsReturnInfo.ColWidth(1, 0);
				vsReturnInfo.ColWidth(2, FCConvert.ToInt32(vsReturnInfo.WidthOriginal * 0.6));
				vsReturnInfo.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsReturnInfo.ExtendLastCol = true;
				if (lngCurrentSummaryRow != lngRow)
				{
					// only refill the grid if it is a different row
					lngCurrentSummaryRow = lngRow;
					// reset the number of rows
					vsReturnInfo.Rows = 1;
					// fill the top row with titles
					vsReturnInfo.FixedRows = 1;
					vsReturnInfo.TextMatrix(0, 0, "Account");
					vsReturnInfo.TextMatrix(0, 1, "BillKey");
					vsReturnInfo.TextMatrix(0, 2, "Name");
					if (Strings.Trim(vsSummary.TextMatrix(lngRow, 2)) != "")
					{
						strTemp = modGlobal.Statics.setCont.GetSettingValue("LastRateRecChoiceYear", "", "", "", "");
						if (Conversion.Val(strTemp) > 0)
						{
							lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
						}
						else
						{
							// get the year
							if ((modCustomReport.Statics.strReportType == "30DAYNOTICE") || (modCustomReport.Statics.strReportType == "LIENPROCESS"))
							{
								lngYear = FCConvert.ToDateTime(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[1].RowNumber, 1)).Year;
								// frmRateRecChoice.cmbYear.List (frmRateRecChoice.cmbYear.ListIndex)
							}
							else if (modCustomReport.Statics.strReportType == "LIENMATURITY")
							{
								lngYear = FCConvert.ToDateTime(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[1].RowNumber, 1)).Year;
								// kgk 04-15-2011 frfFreeReport(0).RowNumber, 1)
							}
						}
						// set the string off accounts
						strTemp = Strings.Trim(vsSummary.TextMatrix(lngRow, 2));
						// open a recordset with all possible bills so that you can get the name info
						rsBill.OpenRecordset("SELECT Account, Name1, BillKey FROM BillingMaster WHERE BillingYear / 10 = " + FCConvert.ToString(lngYear));
						// fill the grid with the accounts
						while (!(Strings.Trim(strTemp).Length == 0))
						{
							if (Strings.InStr(1, strTemp, ",") != 0)
							{
								lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(strTemp, Strings.InStr(1, strTemp, ",")))));
								// get the next account
								strTemp = Strings.Right(strTemp, strTemp.Length - Strings.InStr(1, strTemp, ","));
								// remove the account from the list
							}
							else
							{
								lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
								// take the last number
								strTemp = "";
								// this will end the loop
							}
							// add the account
							if (lngAcct > 0)
							{
								rsBill.FindFirstRecord("Billkey", lngAcct);
								if (!rsBill.NoMatch)
								{
									// add the row
									// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
									vsReturnInfo.AddItem(FCConvert.ToString(rsBill.Get_Fields("Account")) + "\t" + FCConvert.ToString(rsBill.Get_Fields_Int32("BillKey")) + "\t" + FCConvert.ToString(rsBill.Get_Fields_String("Name1")));
								}
							}
						}
					}
					vsReturnInfo.Select(0, 1);
				}
				//FC:FINAL:MHO:#i182 - do not turn vsReturnInfo visible if no row 0 is selected
				//if (vsReturnInfo.Rows == 1)
				if (vsReturnInfo.Rows == 1 || lngRow == 0)
				{
					vsReturnInfo.Visible = false;
				}
				else
				{
					vsReturnInfo.Visible = true;
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Summary Set Up");
			}
		}

		private void vsVariableSetup_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			string strTemp;
			strTemp = Strings.Trim(vsVariableSetup.TextMatrix(vsVariableSetup.Row, 1));
			if (strTemp == "__/__/____" || strTemp == "____-_")
			{
				vsVariableSetup.TextMatrix(vsVariableSetup.Row, 1, string.Empty);
			}
		}

		private void vsVariableSetup_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			bool boolSkip = false;
			// find the telephone rows and format them to be local telephone numbers
			if (Strings.UCase(modCustomReport.Statics.strReportType) == "LIENMATURITY")
			{
				if (Conversion.Val(vsVariableSetup.TextMatrix(vsVariableSetup.Row, 2)) == 10 || Conversion.Val(vsVariableSetup.TextMatrix(vsVariableSetup.Row, 2)) == 11)
				{
					boolSkip = true;
					// this will make the code skip the next part
					vsVariableSetup.EditMask = "(###) ###-####";
				}
			}
			if (!boolSkip)
			{
				if (modRhymalReporting.Statics.frfFreeReport[FCConvert.ToInt32(vsVariableSetup.TextMatrix(vsVariableSetup.Row, 2))].Type == 2)
				{
					// date
					vsVariableSetup.EditMask = "##/##/####";
				}
				else
				{
					vsVariableSetup.EditMask = "";
				}
			}
        }

		private void vsVariableSetup_GotFocus()
		{
			if (Strings.UCase(modCustomReport.Statics.strReportType) == "RNFORM")
			{
			}
			else
			{
				vsVariableSetup.Select(0, 1);
			}
		}

        private void VsVariableSetup_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            // this will set the tool tip when the user moves over the row
            // ReturnToolTip vsVariableSetup.MouseRow
            int lngMR;
            lngMR = vsVariableSetup.GetFlexRowIndex(e.RowIndex);

            if (vsVariableSetup.IsValidCell(e.ColumnIndex, e.RowIndex))
            {
                DataGridViewCell cell = vsVariableSetup[e.ColumnIndex, e.RowIndex];
                    cell.ToolTipText = ReturnToolTip(lngMR);
            }
        }

        private string ReturnToolTip(int lngRow)
		{
			string ReturnToolTip = "";
			// this will return the correct tooltip for that row
			int lngIndex;
			ReturnToolTip = "";
			// this will check each of the array elements and return the tooltip that is to be shown
			for (lngIndex = 0; lngIndex <= modRhymalReporting.MAXFREEVARIABLES; lngIndex++)
			{
				if (lngRow == modRhymalReporting.Statics.frfFreeReport[lngIndex].RowNumber)
				{
					ReturnToolTip = modRhymalReporting.Statics.frfFreeReport[lngIndex].ToolTip;
					break;
				}
			}
			return ReturnToolTip;
		}

		private void vsVariableSetup_RowColChange(object sender, EventArgs e)
		{
			int intCT;
			// only allow input into col 1
			if (vsVariableSetup.Col == 1)
			{
				vsVariableSetup.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else
			{
				vsVariableSetup.Editable = FCGrid.EditableSettings.flexEDNone;
			}
            // set the combo boxes in the grid
            intCT = FCConvert.ToInt32(Math.Round(Conversion.Val(vsVariableSetup.TextMatrix(vsVariableSetup.Row, 2))));
            if (Strings.Trim(modRhymalReporting.Statics.frfFreeReport[intCT].ComboList) != "")
            {
                // does it have a combo list
                vsVariableSetup.ComboList = modRhymalReporting.Statics.frfFreeReport[intCT].ComboList;
            }
            else
            {
                vsVariableSetup.ComboList = "";
            }
            if (vsVariableSetup.Row == vsVariableSetup.Rows - 1)
			{
				vsVariableSetup.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				vsVariableSetup.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void ShowFrame(FCPanel fraFrame)
		{
			fraFrame.Visible = true;
		}

		private void FormatVariableGrid()
		{
			//SBE: exception is thrown on CreateGraphics() when Height is 0
			if (vsVariableSetup.Height > 0 && vsVariableSetup.Width > 0)
			{
				int wid = 0;
				wid = vsVariableSetup.WidthOriginal;
				vsVariableSetup.ColWidth(0, FCConvert.ToInt32(wid * 0.65));
				// Question
				vsVariableSetup.ColWidth(1, FCConvert.ToInt32(wid * 0.33));
				// Answer
				vsVariableSetup.ColWidth(2, FCConvert.ToInt32(wid * 0.0));
				// Required
				vsVariableSetup.ColWidth(3, FCConvert.ToInt32(wid * 0.0));
				// Hidden Field
			}
		}

		private void FormatRecommitmentGrid()
		{
			int wid = 0;
			wid = vsRecommitment.WidthOriginal;
			vsRecommitment.ColWidth(0, FCConvert.ToInt32(wid * 0.55));
			// Question
			vsRecommitment.ColWidth(1, FCConvert.ToInt32(wid * 0.38));
			// Answer
			vsRecommitment.ColWidth(2, FCConvert.ToInt32(wid * 0.0));
			// Required
			vsRecommitment.ColWidth(3, FCConvert.ToInt32(wid * 0.0));
			// Hidden Field
			vsRecommitment.ExtendLastCol = true;
			vsRecommitment.TextMatrix(0, 0, "Previous Tax Collector:");
			vsRecommitment.TextMatrix(1, 0, "Recommitment Date:");
		}

		private bool ValidateVariablesinGRID()
		{
			bool ValidateVariablesinGRID = false;
			// this function will return true if all of the required variables/Questions are filled in
			int intCT;
			bool boolGood;
			vsVariableSetup.Select(0, 0);
			boolGood = true;
			for (intCT = 0; intCT <= modRhymalReporting.MAXFREEVARIABLES; intCT++)
			{
				if (modRhymalReporting.Statics.frfFreeReport[FCConvert.ToInt32(vsVariableSetup.TextMatrix(intCT, 2))].Required)
				{
					if (Strings.Trim(vsVariableSetup.TextMatrix(intCT, 1)) == "")
					{
						boolGood = false;
						break;
					}
				}
			}
			ValidateVariablesinGRID = boolGood;
			return ValidateVariablesinGRID;
		}
		// VBto upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		private short ValidateVariablesInRTB()
		{
			short ValidateVariablesInRTB = 0;
			// this function will check through the string in the Rich Text Box
			// and make sure that all of the required tags are in the string at some point
			// if all the required fields are entered, then a 0 is returned
			// if one is missing, the first one that is found will have it's
			// index in the frffreereport array returned
			int intCT;
			ValidateVariablesInRTB = 0;
			for (intCT = 0; intCT <= modRhymalReporting.MAXFREEVARIABLES; intCT++)
			{
				if (modRhymalReporting.Statics.frfFreeReport[FCConvert.ToInt32(vsVariableSetup.TextMatrix(intCT, 2))].Required)
				{
					if (Strings.Trim(vsVariableSetup.TextMatrix(intCT, 1)) == "")
					{
						ValidateVariablesInRTB = FCConvert.ToInt16(intCT);
						break;
					}
				}
			}
			return ValidateVariablesInRTB;
		}

		private void ResetVariables()
		{
			// this will clear any variables on a restart
			int intCT;
			boolLoaded = false;
			for (intCT = 0; intCT <= modRhymalReporting.MAXFREEVARIABLES; intCT++)
			{
				modRhymalReporting.Statics.frfFreeReport[intCT].ComboList = "";
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "";
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "";
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber = -1;
				// kgk trocl-683  If left at zero and the first var is not in the grid, gets the wrong tooltip
			}
		}

		private void vsVariableSetup_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (vsVariableSetup.CurrentCell.IsInEditMode)
			{
				//FC:FINAL:MSH - save and use correct indexes of the cell
				int row = vsVariableSetup.GetFlexRowIndex(e.RowIndex);
				int col = vsVariableSetup.GetFlexColIndex(e.ColumnIndex);
				if (modRhymalReporting.Statics.frfFreeReport[FCConvert.ToInt32(vsVariableSetup.TextMatrix(row, 2))].Type == 2)
				{
					// date
					if (Strings.Trim(vsVariableSetup.EditText) == "/  /" || vsVariableSetup.EditText == "__/__/____")
					{
						vsVariableSetup.EditMask = string.Empty;
						vsVariableSetup.EditText = string.Empty;
						vsVariableSetup.TextMatrix(row, col, string.Empty);
						vsVariableSetup.Refresh();
						return;
					}
					if (Strings.Trim(vsVariableSetup.EditText).Length == 0)
					{
					}
					else if (Strings.Trim(vsVariableSetup.EditText).Length != 10)
					{
						FCMessageBox.Show("Invalid date.", MsgBoxStyle.Information, "TRIO Software");
						e.Cancel = true;
						return;
					}
					if (!modCustomReport.IsValidDate(vsVariableSetup.EditText))
					{
						e.Cancel = true;
						return;
					}
				}
				else if (modRhymalReporting.Statics.frfFreeReport[FCConvert.ToInt32(vsVariableSetup.TextMatrix(row, 2))].Type == 1)
				{
					// number
					vsVariableSetup.EditText = Strings.Format(vsVariableSetup.EditText, "0.00");
				}
			}
		}
		// VBto upgrade warning: intIndex As short	OnWriteFCConvert.ToInt32(
		private void InsertTagAtCursor_2(short intIndex)
		{
			InsertTagAtCursor(ref intIndex);
		}

		private void InsertTagAtCursor(ref short intIndex)
		{
			// this will insert the tag selected by the combobox into the rtb at the point where the cursor is
			rtbData.SelText = "<" + modRhymalReporting.Statics.frfFreeReport[intIndex].Tag + ">";
		}

		private void CheckInsideTag(int lngStart, int lngLen)
		{
			// this will check to make sure that neither the beginning or the end of the selection is in the middle of a tag
			int lngEnd = 0;
			int lngGT = 0;
			int lngLT = 0;
			string strText = "";
			string strTemp = "";
			if (!boolDontCheck)
			{
				if (lngStart == 0 && lngLen == 0)
					return;
				lngStart += 1;
				// adjust for the 1 based array the text functions use
				strText = rtbData.Text;
				// check the beginning of the selection
				if (lngStart == 1)
				{
					lngGT = Strings.InStrRev(strText, ">", lngStart, CompareConstants.vbTextCompare/*?*/);
				}
				else
				{
					lngGT = Strings.InStrRev(strText, ">", lngStart - 1, CompareConstants.vbTextCompare/*?*/);
				}
				lngLT = Strings.InStrRev(strText, "<", lngStart, CompareConstants.vbTextCompare/*?*/);
				if (lngLT > lngGT || (lngLT != 0 && lngGT == 0))
				{
					// this means that we are in a tag on the front side of the selection
					lngLen += (lngStart - lngLT);
					lngStart = lngLT;
				}
				else
				{
					// we are not in a tag from the front of the selection
				}
				// check the end of the selection
				lngEnd = lngStart + lngLen;
				lngGT = Strings.InStr(lngEnd, strText, ">");
				lngLT = Strings.InStr(lngEnd, strText, "<");
				if (lngGT < lngLT || (lngGT != 0 && lngLT == 0))
				{
					// this means that the end of the selection is inside of a tag
					lngLen += (lngGT - lngEnd + 1);
				}
				boolDontCheck = true;
				rtbData.SelStart = lngStart - 1;
				// adjust for the 0 based array the text boxes use
				if (lngStart > 0)
				{
					rtbData.SelLength = lngLen;
				}
				boolDontCheck = false;
			}
		}

		private void NewRTB_BackSpace()
		{
			// this will actually take over for the real backspace key
			string strText;
			string strTemp = "";
			int lngStart = 0;
			int lngDelNum = 0;
			strText = rtbData.Text;
			if (rtbData.SelLength > 0)
			{
				// if there is a selection, then just delete it
				rtbData.SelText = "";
			}
			else
			{
				lngStart = rtbData.SelStart;
				if (lngStart > 1)
				{
					boolDontCheck = true;
					// turn off the selction checking
					if (Strings.Mid(strText, lngStart, 1) == ">")
					{
						// I need to select and delete the whole tag
						lngDelNum = lngStart - Strings.InStrRev(strText, "<", lngStart, CompareConstants.vbTextCompare/*?*/) + 1;
						rtbData.SelStart = lngStart - lngDelNum;
						// start at the beginning
						rtbData.SelLength = lngDelNum;
						// the length of the tag to delete
						rtbData.SelText = "";
						// delete the text selected
					}
					else
					{
						rtbData.SelStart = lngStart - 1;
						rtbData.SelLength = 1;
						rtbData.SelText = "";
					}
					boolDontCheck = false;
				}
				else
				{
					// if at the beginning, then don't do anything
					// do nothing
				}
			}
		}

		private void NewRTB_Delete()
		{
			// this will do the delete key's job when inside a tag
			int lngStart = 0;
			int lngLen = 0;
			string strText;
			strText = rtbData.Text;
			boolDontCheck = true;
			if (rtbData.SelLength > 0)
			{
				// if there is a selection, then just delete it
				rtbData.SelText = "";
			}
			else
			{
				// if there is no selection made, then check to see if there is a tag
				lngStart = rtbData.SelStart;
				if (Strings.Mid(strText, lngStart + 1, 1) == "<")
				{
					lngLen = Strings.InStr(lngStart + 1, strText, ">") - lngStart;
					if (lngLen > 0)
					{
						rtbData.SelLength = lngLen;
						rtbData.SelText = "";
					}
				}
				else
				{
					// not a tag, just delete one character
					rtbData.SelLength = 1;
					rtbData.SelText = "";
				}
			}
			boolDontCheck = false;
		}

		private void CheckReportTable()
		{
			clsDRWrapper rsCreateTable = new clsDRWrapper();
			
		}

		public void LoadCombo(short intType = 0)
		{
			// LOAD THE COMBO WITH ALL PREVIOUSLY SAVED REPORTS
			clsDRWrapper rsReports = new clsDRWrapper();
			string strSQL = "";
			// CLEAR OUT THE CONTROL
			cboSavedReport.Clear();
			// OPEN THE RECORDSET
			switch (intType)
			{
				case 0:
					{
						// generic show all
						strSQL = "SELECT * FROM SavedFreeReports WHERE Type = '" + Strings.UCase(modCustomReport.Statics.strReportType) + "' AND Left(ReportName,7) <> 'ARCHIVE'";
						break;
					}
				case 1:
					{
						// show all to delete
						strSQL = "SELECT * FROM SavedFreeReports WHERE Type = '" + Strings.UCase(modCustomReport.Statics.strReportType) + "' AND Left(ReportName,7) <> 'ARCHIVE' AND Left(ReportName,7) <> 'DEFAULT'";
						break;
					}
				case 2:
					{
						break;
					}
			}
			//end switch
			rsReports.OpenRecordset(strSQL, modGlobal.DEFAULTDATABASE);
			while (!rsReports.EndOfFile())
			{
				// ADD THE ITEM TO THE COMBO
				if (Strings.Left(FCConvert.ToString(rsReports.Get_Fields_String("ReportName")), 7) != "ARCHIVE" && Strings.Left(FCConvert.ToString(rsReports.Get_Fields_String("ReportName")), 7) != "DEFAULT")
				{
					cboSavedReport.AddItem(FCConvert.ToString(rsReports.Get_Fields_String("ReportName")));
				}
				else
				{
					cboSavedReport.AddItem(Strings.Right(FCConvert.ToString(rsReports.Get_Fields_String("ReportName")), FCConvert.ToString(rsReports.Get_Fields_String("ReportName")).Length - 7));
				}
				// ADD THE AUTONUMBER AS THE ID TO EACH ITEM
				cboSavedReport.ItemData(cboSavedReport.NewIndex, FCConvert.ToInt32(rsReports.Get_Fields_Int32("ID")));
				// GET THE NEXT RECORD
				rsReports.MoveNext();
			}
		}

		private void FormatSummaryGrid(bool boolClear = false)
		{
			int wid = 0;
			if (Strings.UCase(modCustomReport.Statics.strReportType) == "30DAYNOTICE")
			{
				vsSummary.Cols = 3;
				wid = vsSummary.WidthOriginal;
				vsSummary.ColWidth(0, FCConvert.ToInt32(wid * 0.75));
				vsSummary.ColWidth(1, FCConvert.ToInt32(wid * 0.2));
				//vsSummary.ColWidth(2, 0);
				vsSummary.ColHidden(2, true);
				vsSummary.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				if (!boolResize)
				{
					if (boolClear)
					{
						vsSummary.Rows = 1;
					}
					vsSummary.Rows = 5;
					//FC:FINAL:MHO:#i182 - Hide the row instead of setting its height to 0
					//vsSummary.RowHeight(0, 0);
					vsSummary.RowHidden(0, true);
					vsSummary.TextMatrix(1, 0, "Zero balance accounts:");
					// .TextMatrix(1, 1) = ar30DayNotice.lngBalanceZero
					vsSummary.TextMatrix(2, 0, "Negative balance accounts:");
					// .TextMatrix(2, 1) = ar30DayNotice.lngBalanceNeg
					vsSummary.TextMatrix(3, 0, "Positive balance accounts:");
					// .TextMatrix(3, 1) = ar30DayNotice.lngBalancePos
					// this will look like a line between the values and the total
					vsSummary.Select(4, 1);
					vsSummary.CellBorder(Color.Black, 0, 1, 0, 0, 0, 2);
					vsSummary.Select(0, 0);
					vsSummary.TextMatrix(4, 0, "Total accounts:");
					// .TextMatrix(4, 1) = ar30DayNotice.lngBalanceZero + ar30DayNotice.lngBalancePos + ar30DayNotice.lngBalanceNeg
					//vsSummary.HeightOriginal = vsSummary.RowHeight(1) * 4;					
				}
				// lblStatusInstructions.Text = "This is the account breakdown.  To print the 30 Day Notices press F10.  This will also update the status of the accounts found."
				// lblStatusInstructions.Text = "Account Status:"
			}
			else if (Strings.UCase(modCustomReport.Statics.strReportType) == "LIENPROCESS")
			{
				wid = vsSummary.WidthOriginal;
				vsSummary.Cols = 3;
				vsSummary.ColWidth(0, FCConvert.ToInt32(wid * 0.75));
				vsSummary.ColWidth(1, FCConvert.ToInt32(wid * 0.2));
				//FC:FINAL:AM: hide the column
				//vsSummary.ColWidth(2, 0);
				vsSummary.ColHidden(2, true);
				vsSummary.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				if (!boolResize)
				{
					// clear the grid
					if (boolClear)
					{
						vsSummary.Rows = 1;
					}
					vsSummary.Rows = 5;
					//FC:FINAL:MHO:#i182 - Hide the row instead of setting its height to 0
					//vsSummary.RowHeight(0, 0);
					vsSummary.RowHidden(0, true);
					vsSummary.TextMatrix(1, 0, "Zero balance accounts:");
					// .TextMatrix(1, 1) = ar30DayNotice.lngBalanceZero
					vsSummary.TextMatrix(2, 0, "Negative balance accounts:");
					// .TextMatrix(2, 1) = ar30DayNotice.lngBalanceNeg
					vsSummary.TextMatrix(3, 0, "Positive balance accounts:");
					// .TextMatrix(3, 1) = ar30DayNotice.lngBalancePos
					// this will look like a line between the values and the total
					vsSummary.Select(4, 1);
					vsSummary.CellBorder(Color.Black, 0, 1, 0, 0, 0, 2);
					vsSummary.Select(0, 0);
					vsSummary.TextMatrix(4, 0, "Total accounts:");
					//vsSummary.HeightOriginal = vsSummary.RowHeight(1) * 4;					
				}
				// lblStatusInstructions.Text = "This is the account breakdown.  To print the Lien Notices press F12.  This will also update the status of the accounts found."
			}
			else if (Strings.UCase(modCustomReport.Statics.strReportType) == "LIENMATURITY")
			{
				wid = vsSummary.WidthOriginal;
				vsSummary.Cols = 3;
				vsSummary.ColWidth(0, FCConvert.ToInt32(wid * 0.75));
				vsSummary.ColWidth(1, FCConvert.ToInt32(wid * 0.2));
				vsSummary.ColWidth(2, 0);
				vsSummary.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				if (!boolResize)
				{
					if (boolClear)
					{
						vsSummary.Rows = 1;
					}
					vsSummary.Rows = 5;
					//FC:FINAL:MHO:#i182 - Hide the row instead of setting its height to 0
					//vsSummary.RowHeight(0, 0);
					vsSummary.RowHidden(0, true);
					vsSummary.TextMatrix(1, 0, "Zero balance accounts:");
					// .TextMatrix(1, 1) = ar30DayNotice.lngBalanceZero
					vsSummary.TextMatrix(2, 0, "Negative balance accounts:");
					// .TextMatrix(2, 1) = ar30DayNotice.lngBalanceNeg
					vsSummary.TextMatrix(3, 0, "Positive balance accounts:");
					// .TextMatrix(3, 1) = ar30DayNotice.lngBalancePos
					// this will look like a line between the values and the total
					vsSummary.Select(4, 1);
					vsSummary.CellBorder(Color.Black, 0, 1, 0, 0, 0, 2);
					vsSummary.Select(0, 0);
					vsSummary.TextMatrix(4, 0, "Total accounts:");
					// .TextMatrix(4, 1) = ar30DayNotice.lngBalanceZero + ar30DayNotice.lngBalancePos + ar30DayNotice.lngBalanceNeg
					//vsSummary.HeightOriginal = vsSummary.RowHeight(1) * 4;					
				}
				// lblStatusInstructions.Text = "This is the account breakdown.  To print the Lien Maturity Notices press F12.  This will also update the status of the accounts found."
			}
			else if (Strings.UCase(modCustomReport.Statics.strReportType) == "RNFORM")
			{
				vsSummary.Visible = false;
			}
		}

		private void RefreshHardCodes()
		{
			// this will check the whole document for missing CRLF or the <CRLF> tag and replace any that are missing
			string strText;
			int lngNextCRLF;
			int lngLast;
			strText = rtbData.Text;
			// find the hard codes and make sure that there is a tag before it
			lngLast = 1;
			//FC:FINAL:AM:#i218 - text doesn't contain \r
			//lngNextCRLF = Strings.InStr(lngLast, strText, "\r\n");
			lngNextCRLF = Strings.InStr(lngLast, strText, "\n");
			while (!(lngNextCRLF == 0))
			{
				if (lngNextCRLF > 6)
				{
					if (Strings.Mid(strText, lngNextCRLF - 6, 6) != "<CRLF>")
					{
						strText = Strings.Left(strText, lngNextCRLF - 1) + "<CRLF>" + Strings.Right(strText, strText.Length - (lngNextCRLF - 1));
					}
				}
				else
				{
					// it is too close to the beginning to have a tag before it
					// so put one in
					strText = Strings.Left(strText, lngNextCRLF - 1) + "<CRLF>" + Strings.Right(strText, strText.Length - (lngNextCRLF - 1));
				}
				lngLast = lngNextCRLF + 1;
				lngNextCRLF = Strings.InStr(lngLast, strText, "\n");
			}
			// find all of the tags and make sure that there is a hard code after it
			lngLast = 1;
			lngNextCRLF = Strings.InStr(lngLast, strText, "<CRLF>");
			while (!(lngNextCRLF == 0))
			{
				if (strText.Length >= lngNextCRLF + 6)
				{
					if (Strings.Mid(strText, lngNextCRLF + 6, 1) != "\n")
					{
						strText = Strings.Left(strText, lngNextCRLF + 5) + "\n" + Strings.Right(strText, (strText.Length - (lngNextCRLF + 5)));
					}
				}
				else
				{
					// is this the last thing...if so, then add a hard return at the end
					strText += "\n";
				}
				lngLast = lngNextCRLF + 1;
				lngNextCRLF = Strings.InStr(lngLast, strText, "<CRLF>");
			}
			rtbData.Text = strText;
		}

		private void SetPsuedoReportLabel()
		{
			if (Strings.UCase(modCustomReport.Statics.strReportType) == "30DAYNOTICE")
			{
				if (chkRecommitment.CheckState == Wisej.Web.CheckState.Unchecked)
				{
					lblPseudoHeader.Text = "State of Maine" + "\r\n" + "Tax Collector's Notice, Lien Claim and Demand";
					// & vbCrLf & "30 Day Notice"
				}
				else
				{
					lblPseudoHeader.Text = "30 Day Notice" + "\r\n" + "Recommitment";
				}
				// create the footer
				lblPsuedoFooter[0].Text = "Principal" + "\r\n";
				lblPsuedoFooter[0].Text = lblPsuedoFooter[0].Text + "Interest" + "\r\n";
				lblPsuedoFooter[0].Text = lblPsuedoFooter[0].Text + "Cert Mail Fee" + "\r\n";
				lblPsuedoFooter[0].Text = lblPsuedoFooter[0].Text + "Total";
				lblPsuedoFooter[1].Text = "0.00" + "\r\n" + "0.00" + "\r\n" + "0.00" + "\r\n" + "0.00";
				lblPsuedoFooter[2].Text = "___________________________" + "\r\n";
				lblPsuedoFooter[2].Text = lblPsuedoFooter[2].Text + vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[4].RowNumber, 1) + "\r\n";
				lblPsuedoFooter[2].Text = lblPsuedoFooter[2].Text + "Collector Of Taxes" + "\r\n";
				if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
				{
					lblPsuedoFooter[2].Text = lblPsuedoFooter[2].Text + modGlobalConstants.Statics.gstrCityTown + " of " + modGlobalConstants.Statics.MuniName;
				}
				else
				{
					lblPsuedoFooter[2].Text = lblPsuedoFooter[2].Text + modGlobalConstants.Statics.MuniName;
				}
			}
			else if (Strings.UCase(modCustomReport.Statics.strReportType) == "LIENPROCESS")
			{
				if (chkRecommitment.CheckState == Wisej.Web.CheckState.Checked)
				{
					lblPseudoHeader.Text = "State of Maine" + "\r\n" + "Tax Lien Certificate - Recommitment";
				}
				else
				{
					lblPseudoHeader.Text = "State of Maine" + "\r\n" + "Tax Lien Certificate";
				}
				// there is nothing automatically added at the bottom of report
				lblPsuedoFooter[0].Text = "";
				lblPsuedoFooter[1].Text = "";
				lblPsuedoFooter[2].Text = "";
			}
			else if (Strings.UCase(modCustomReport.Statics.strReportType) == "LIENMATURITY")
			{
				lblPseudoHeader.Text = "State of Maine" + "\r\n" + "Notice of Impending Automatic Foreclosure";

				lblPsuedoFooter[0].Text = "Principal" + "\r\n";
				lblPsuedoFooter[0].Text = lblPsuedoFooter[0].Text + "Interest" + "\r\n";
				lblPsuedoFooter[0].Text = lblPsuedoFooter[0].Text + "Lien Costs" + "\r\n";
				lblPsuedoFooter[0].Text = lblPsuedoFooter[0].Text + "Fee" + "\r\n";
				lblPsuedoFooter[0].Text = lblPsuedoFooter[0].Text + "Cert Mail Fee" + "\r\n";
				lblPsuedoFooter[0].Text = lblPsuedoFooter[0].Text + "Total";
				lblPsuedoFooter[1].Text = "0.00" + "\r\n" + "0.00" + "\r\n" + "0.00" + "\r\n" + "0.00" + "\r\n" + "0.00" + "\r\n" + "0.00";
				lblPsuedoFooter[2].Text = "___________________________" + "\r\n";
				lblPsuedoFooter[2].Text = lblPsuedoFooter[2].Text + "Signer's Name" + "\r\n";
				// vsVariableSetup.TextMatrix(frfFreeReport(4).RowNumber, 1) 'this is the collector name
				lblPsuedoFooter[2].Text = lblPsuedoFooter[2].Text + "Signer's Designation" + "\r\n";
				if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
				{
					lblPsuedoFooter[2].Text = lblPsuedoFooter[2].Text + modGlobalConstants.Statics.gstrCityTown + " of " + modGlobalConstants.Statics.MuniName + "\r\n";
				}
				else
				{
					lblPsuedoFooter[2].Text = lblPsuedoFooter[2].Text + modGlobalConstants.Statics.MuniName + "\r\n";
				}
				lblPsuedoFooter[2].Text = lblPsuedoFooter[2].Text + "Amount due is as of " + vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[1].RowNumber, 1) + ".";
			}
			else if (Strings.UCase(modCustomReport.Statics.strReportType) == "RNFORM")
			{
				//lblPseudoHeader.Alignment = 0;
				lblPseudoHeader.Text = "Account: 0" + "\r\n" + "Owner Name" + "\r\n" + "Address" + "\r\n";
				lblPseudoHeader.Text = lblPseudoHeader.Text + "    Year        Principal        Interest          Cost" + "\r\n" + "    2004-1        3020.00           45.30          7.42";
				lblPseudoHeader.Height = lblPseudoHeader.Height * 2;
				rtbData.Top = lblPseudoHeader.Top + lblPseudoHeader.Height;
				lblPsuedoFooter[0].Text = "";
				lblPsuedoFooter[1].Text = "";
				lblPsuedoFooter[2].Text = "";
			}
		}

		private bool SaveContolInformation()
		{
			bool SaveContolInformation = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will save a control record into the database
				clsDRWrapper rsControl = new clsDRWrapper();
				clsDRWrapper rsRate = new clsDRWrapper();
				int intLocationType = 0;
				SaveContolInformation = true;
				vsVariableSetup.Select(0, 0);
				if (Strings.UCase(modCustomReport.Statics.strReportType) == "30DAYNOTICE")
				{
					// ******************
					rsControl.OpenRecordset("SELECT * FROM Control_30DayNotice");
					if (rsControl.EndOfFile())
					{
						rsControl.AddNew();
					}
					else
					{
						rsControl.Edit();
					}
					if (Strings.UCase(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[33].RowNumber, 1), 7)) == Strings.UCase("Show al"))
					{
						intLocationType = 0;
					}
					else if (Strings.UCase(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[33].RowNumber, 1), 7)) == Strings.UCase("Show lo"))
					{
						intLocationType = 1;
					}
					else if (Strings.UCase(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[33].RowNumber, 1), 7)) == Strings.UCase("Do not "))
					{
						intLocationType = 2;
					}
					rsControl.Set_Fields("ShowLocation", intLocationType);
					rsControl.Set_Fields("DateCreated", DateTime.Today);
					rsControl.Set_Fields("User", modGlobalConstants.Statics.gstrUserID);
					rsControl.Set_Fields("BillingYear", FCConvert.ToString(Conversion.Val(modExtraModules.FormatYear(frmRateRecChoice.InstancePtr.cmbYear.Text))));
					rsControl.Set_Fields("MinimumAmount", FCConvert.ToDouble(frmRateRecChoice.InstancePtr.txtMinimumAmount.Text));
					dblMinimumAmount = FCConvert.ToDouble(frmRateRecChoice.InstancePtr.txtMinimumAmount.Text);
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[0].RowNumber, 1) != "")
					{
						if (Information.IsDate(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[0].RowNumber, 1)))
						{
							rsControl.Set_Fields("MailDate", DateAndTime.DateValue(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[0].RowNumber, 1)));
						}
						else
						{
							FCMessageBox.Show("Enter a valid Mail Date.", MsgBoxStyle.Exclamation, "Invalid Date");
							SaveContolInformation = false;
							return SaveContolInformation;
						}
					}
					else
					{
						FCMessageBox.Show("Enter a valid Mail Date.", MsgBoxStyle.Exclamation, "Invalid Date");
						SaveContolInformation = false;
						return SaveContolInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[4].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("CollectorName", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[4].RowNumber, 1));
					}
					else
					{
						FCMessageBox.Show("Enter a tax Collector's name.", MsgBoxStyle.Exclamation, "Invalid Name");
						SaveContolInformation = false;
						return SaveContolInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[5].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("Muni", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[5].RowNumber, 1));
					}
					else
					{
						FCMessageBox.Show("Enter the Municipality Name.", MsgBoxStyle.Exclamation, "Invalid Name");
						SaveContolInformation = false;
						return SaveContolInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[6].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("County", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[6].RowNumber, 1));
					}
					else
					{
						FCMessageBox.Show("Enter the County Name.", MsgBoxStyle.Exclamation, "Invalid Name");
						SaveContolInformation = false;
						return SaveContolInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[7].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("State", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[7].RowNumber, 1));
					}
					else
					{
						FCMessageBox.Show("Enter the State.", MsgBoxStyle.Exclamation, "Invalid State");
						SaveContolInformation = false;
						return SaveContolInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[8].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("Zip", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[8].RowNumber, 1));
					}
					else
					{
						FCMessageBox.Show("Enter the Zip Code.", MsgBoxStyle.Exclamation, "Invalid Zip");
						SaveContolInformation = false;
						return SaveContolInformation;
					}
					if (Conversion.Val(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[10].RowNumber, 1)) == 0)
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[10].RowNumber, 1, "0.00");
						rsControl.Set_Fields("Demand", 0);
					}
					else
					{
						rsControl.Set_Fields("Demand", FCConvert.ToDouble(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[10].RowNumber, 1)));
					}
					if (Conversion.Val(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[11].RowNumber, 1)) == 0)
					{
						rsControl.Set_Fields("CertMailFee", 0);
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[11].RowNumber, 1, "0.00");
					}
					else
					{
						rsControl.Set_Fields("CertMailFee", FCConvert.ToDouble(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[11].RowNumber, 1)));
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[13].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("MapPreparer", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[13].RowNumber, 1));
					}
					else
					{
						FCMessageBox.Show("Enter the Map Preparer's Name.", MsgBoxStyle.Exclamation, "Invalid Name");
						SaveContolInformation = false;
						return SaveContolInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[14].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("MapPrepareDate", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[14].RowNumber, 1));
					}
					else
					{
						FCMessageBox.Show("Enter the Map Prepared Date.", MsgBoxStyle.Exclamation, "Invalid Date");
						SaveContolInformation = false;
						return SaveContolInformation;
					}
                    if (Strings.Mid(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1), 29, 4).ToUpper() == "LAST")
                    { // trocls-147 04.16.2018 kjr  CODE FREEZE, 5.22.18
                        rsControl.Set_Fields("MailTo", 2);
                    }
                    else
                    {
                        if (Strings.Mid(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1), 27, 3) == "C/O")
                        {
                            rsControl.Set_Fields("MailTo", 1);
                        }
                        else
                        {
                            rsControl.Set_Fields("MailTo", 0);
                        }
                    }
                
					rsControl.Set_Fields("PayCertMailFee", FCConvert.CBool(Strings.Left(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[16].RowNumber, 1), 2) == "Ye"));
					rsControl.Set_Fields("SendCopyToMortHolder", FCConvert.CBool(Strings.Left(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[29].RowNumber, 1), 2) == "Ye"));
					if (FCConvert.ToBoolean(rsControl.Get_Fields_Boolean("SendCopyToMortHolder")))
					{
						rsControl.Set_Fields("ChargeForMortHolder", !FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[29].RowNumber, 1), 9) == "Yes, with"));
					}
					else
					{
						rsControl.Set_Fields("ChargeForMortHolder", false);
					}
					// MAL@20070102: Call Reference: 104274
					rsControl.Set_Fields("SendCopyToIntParty", FCConvert.CBool(Strings.Left(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[33].RowNumber, 1), 2) == "Ye"));
					if (FCConvert.ToBoolean(rsControl.Get_Fields_Boolean("SendCopyToIntParty")))
					{
						rsControl.Set_Fields("ChargeForIntParty", !FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[33].RowNumber, 1), 9) == "Yes, with"));
					}
					else
					{
						rsControl.Set_Fields("ChargeForIntParty", false);
					}
					rsControl.Set_Fields("SendCopyToNewOwner", Strings.Trim(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[32].RowNumber, 1)));
					if (chkRecommitment.CheckState == Wisej.Web.CheckState.Checked)
					{
						rsControl.Set_Fields("OldCollector", Strings.Trim(vsRecommitment.TextMatrix(0, 1) + " "));
						if (Strings.Trim(vsRecommitment.TextMatrix(1, 1) + " ") != "")
						{
							if (Information.IsDate(vsRecommitment.TextMatrix(1, 1)))
							{
								rsControl.Set_Fields("RecommitmentDate", vsRecommitment.TextMatrix(1, 1));
							}
							else
							{
								FCMessageBox.Show("Invalid date, defaulting to today's date.", MsgBoxStyle.Exclamation, "Invalid Date");
								rsControl.Set_Fields("RecommitmentDate", DateTime.Today);
							}
						}
						else
						{
							if (Strings.Trim(vsRecommitment.TextMatrix(0, 1) + " ") == "")
							{
								FCMessageBox.Show("No recommitment data entered.  This will not use the recommitment form.", MsgBoxStyle.Exclamation, "Missing Data");
								chkRecommitment.CheckState = Wisej.Web.CheckState.Unchecked;
							}
							else
							{
								FCMessageBox.Show("Missing date, defaulting to today's date.", MsgBoxStyle.Exclamation, "Missing Date");
								vsRecommitment.TextMatrix(1, 1, Strings.Format(DateTime.Today, "MM/dd/yyyy"));
								rsControl.Set_Fields("RecommitmentDate", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
							}
						}
					}
					else
					{
						rsControl.Set_Fields("OldCollector", "");
						rsControl.Set_Fields("RecommitmentDate", 0);
					}
					rsControl.Set_Fields("DateUpdated", DateTime.Today);
					rsControl.Set_Fields("UseMailDateForMaturity", modGlobal.Statics.gboolUseMailDateForMaturity);
					// MAL@20080715: Save new default sort order
					// Tracker Reference: 11834
					if (cmbSortOrder.Text == "Account")
					{
						rsControl.Set_Fields("DefaultSort", "A");
					}
					else
					{
						rsControl.Set_Fields("DefaultSort", "N");
					}
					rsControl.Update();
				}
				else if (Strings.UCase(modCustomReport.Statics.strReportType) == "LIENPROCESS")
				{
					// ******************
					rsControl.OpenRecordset("SELECT * FROM Control_LienProcess");
					if (rsControl.EndOfFile())
					{
						rsControl.AddNew();
					}
					else
					{
						rsControl.Edit();
					}
					if (Strings.UCase(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1), 7)) == Strings.UCase("Show al"))
					{
						intLocationType = 0;
					}
					else if (Strings.UCase(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1), 7)) == Strings.UCase("Show lo"))
					{
						intLocationType = 1;
					}
					else if (Strings.UCase(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1), 7)) == Strings.UCase("Do not "))
					{
						intLocationType = 2;
					}
					rsControl.Set_Fields("ShowLocation", intLocationType);
					rsControl.Set_Fields("DateCreated", DateTime.Today);
					rsControl.Set_Fields("User", modGlobalConstants.Statics.gstrUserID);
					rsControl.Set_Fields("BillingYear", FCConvert.ToString(Conversion.Val(modExtraModules.FormatYear(frmRateRecChoice.InstancePtr.cmbYear.Text))));
					rsControl.Set_Fields("MinimumAmount", FCConvert.ToDouble(frmRateRecChoice.InstancePtr.txtMinimumAmount.Text));
					dblMinimumAmount = FCConvert.ToDouble(frmRateRecChoice.InstancePtr.txtMinimumAmount.Text);
					if (Strings.Trim(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[0].RowNumber, 1)) != "")
					{
						if (Information.IsDate(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[0].RowNumber, 1)))
						{
							rsControl.Set_Fields("FilingDate", DateAndTime.DateValue(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[0].RowNumber, 1)));
						}
						else
						{
							FCMessageBox.Show("Enter a valid Filing Date.", MsgBoxStyle.Exclamation, "Invalid Date");
							SaveContolInformation = false;
							return SaveContolInformation;
						}
					}
					else
					{
						FCMessageBox.Show("Enter a valid Filing Date.", MsgBoxStyle.Exclamation, "Invalid Date");
						SaveContolInformation = false;
						return SaveContolInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[4].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("CollectorName", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[4].RowNumber, 1));
					}
					else
					{
						FCMessageBox.Show("Enter a tax Collector's name.", MsgBoxStyle.Exclamation, "Invalid Name");
						SaveContolInformation = false;
						return SaveContolInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[5].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("Muni", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[5].RowNumber, 1));
					}
					else
					{
						FCMessageBox.Show("Enter the Municipality Name.", MsgBoxStyle.Exclamation, "Invalid Name");
						SaveContolInformation = false;
						return SaveContolInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[6].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("County", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[6].RowNumber, 1));
					}
					else
					{
						FCMessageBox.Show("Enter the County Name.", MsgBoxStyle.Exclamation, "Invalid Name");
						SaveContolInformation = false;
						return SaveContolInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[7].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("State", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[7].RowNumber, 1));
					}
					else
					{
						FCMessageBox.Show("Enter the State.", MsgBoxStyle.Exclamation, "Invalid State");
						SaveContolInformation = false;
						return SaveContolInformation;
					}
					// If vsVariableSetup.TextMatrix(frfFreeReport(8).RowNumber, 1) <> "" Then
					rsControl.Set_Fields("Designation", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[8].RowNumber, 1));

					rsControl.Set_Fields("Signer", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[9].RowNumber, 1));

					if (Strings.Trim(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[10].RowNumber, 1)) != "")
					{
						if (Information.IsDate(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[10].RowNumber, 1)))
						{
							rsControl.Set_Fields("CommissionExpirationDate", DateAndTime.DateValue(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[10].RowNumber, 1)));
						}
						else
						{
							FCMessageBox.Show("Enter a valid Commission Expiration Date or leave blank.", MsgBoxStyle.Exclamation, "Invalid Date");
							SaveContolInformation = false;
							return SaveContolInformation;
						}
					}
					else
					{
						rsControl.Set_Fields("CommissionExpirationDate", 0);
					}
					if (Conversion.Val(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[11].RowNumber, 1)) == 0)
					{
						rsControl.Set_Fields("FilingFee", 0);
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[11].RowNumber, 1, "0.00");
					}
					else
					{
						rsControl.Set_Fields("FilingFee", FCConvert.ToDouble(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[11].RowNumber, 1)));
					}
					if (Conversion.Val(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[12].RowNumber, 1)) == 0)
					{
						rsControl.Set_Fields("CertMailFee", 0);
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[12].RowNumber, 1, "0.00");
					}
					else
					{
						rsControl.Set_Fields("CertMailFee", FCConvert.ToDouble(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[12].RowNumber, 1)));
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[14].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("MapPreparer", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[14].RowNumber, 1));
					}
					else
					{
						FCMessageBox.Show("Enter the Map Preparer's Name.", MsgBoxStyle.Exclamation, "Invalid Name");
						SaveContolInformation = false;
						return SaveContolInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("MapPrepareDate", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1));
					}
					else
					{
						FCMessageBox.Show("Enter the Map Prepared Date.", MsgBoxStyle.Exclamation, "Invalid Date");
						SaveContolInformation = false;
						return SaveContolInformation;
					}
					if (Strings.Mid(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[16].RowNumber, 1), 29, 4)=="Last") { // trocls-147 04.16.2018 kjr  CODE FREEZE, 5.22.18
						rsControl.Set_Fields("MailTo", 2);
					} else {
						if (Strings.Mid(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[16].RowNumber, 1), 27, 3)=="C/O") {
							rsControl.Set_Fields("MailTo", 1);
						} else {
							rsControl.Set_Fields("MailTo", 0);
						}
					}
					rsControl.Set_Fields("PayCertMailFee", FCConvert.CBool(Strings.Left(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[17].RowNumber, 1), 2) == "Ye"));
					rsControl.Set_Fields("SendCopyToMortHolder", FCConvert.CBool(Strings.Left(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[30].RowNumber, 1), 2) == "Ye"));
					if (FCConvert.ToBoolean(rsControl.Get_Fields_Boolean("SendCopyToMortHolder")))
					{
						rsControl.Set_Fields("ChargeForMortHolder", !FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[30].RowNumber, 1), 9) == "Yes, with"));
					}
					else
					{
						rsControl.Set_Fields("ChargeForMortHolder", false);
					}
					// MAL@20070102: Call Reference: 104274
					rsControl.Set_Fields("SendCopyToIntParty", FCConvert.CBool(Strings.Left(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1), 2) == "Ye"));
					if (FCConvert.ToBoolean(rsControl.Get_Fields_Boolean("SendCopyToIntParty")))
					{
						rsControl.Set_Fields("ChargeForIntParty", !FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1), 9) == "Yes, with"));
					}
					else
					{
						rsControl.Set_Fields("ChargeForIntParty", false);
					}
					rsControl.Set_Fields("SendCopyToNewOwner", Strings.Trim(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[34].RowNumber, 1)));
					if (chkRecommitment.CheckState == Wisej.Web.CheckState.Checked)
					{
						rsControl.Set_Fields("OldCollector", Strings.Trim(vsRecommitment.TextMatrix(0, 1) + " "));
						if (Strings.Trim(vsRecommitment.TextMatrix(1, 1) + " ") != "")
						{
							if (Information.IsDate(vsRecommitment.TextMatrix(1, 1)))
							{
								rsControl.Set_Fields("RecommitmentDate", Strings.Format(vsRecommitment.TextMatrix(1, 1), "MM/dd/yyyy"));
							}
							else
							{
								FCMessageBox.Show("Invalid date, defaulting to today's date.", MsgBoxStyle.Exclamation, "Invalid Date");
								rsControl.Set_Fields("RecommitmentDate", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
							}
						}
						else
						{
							if (Strings.Trim(vsRecommitment.TextMatrix(0, 1) + " ") == "")
							{
								FCMessageBox.Show("No recommitment data entered.  This will not use the recommitment form.", MsgBoxStyle.Exclamation, "Missing Data");
								chkRecommitment.CheckState = Wisej.Web.CheckState.Unchecked;
							}
							else
							{
								FCMessageBox.Show("Missing date, defaulting to today's date.", MsgBoxStyle.Exclamation, "Missing Date");
								vsRecommitment.TextMatrix(1, 1, Strings.Format(DateTime.Today, "MM/dd/yyyy"));
								rsControl.Set_Fields("RecommitmentDate", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
							}
						}
					}
					else
					{
						rsControl.Set_Fields("OldCollector", "");
						rsControl.Set_Fields("RecommitmentDate", 0);
					}
					rsControl.Set_Fields("DateUpdated", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
					// MAL@20080715: Save new default sort order
					// Tracker Reference: 11834
					if (cmbSortOrder.Text == "Account")
					{
						rsControl.Set_Fields("DefaultSort", "A");
					}
					else
					{
						rsControl.Set_Fields("DefaultSort", "N");
					}
					rsControl.Update();
				}
				else if (Strings.UCase(modCustomReport.Statics.strReportType) == "LIENMATURITY")
				{
					// ******************
					rsControl.OpenRecordset("SELECT * FROM Control_LienMaturity");
					if (rsControl.EndOfFile())
					{
						rsControl.AddNew();
					}
					else
					{
						rsControl.Edit();
					}
					if (Strings.UCase(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1), 7)) == Strings.UCase("Show al"))
					{
						intLocationType = 0;
					}
					else if (Strings.UCase(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1), 7)) == Strings.UCase("Show lo"))
					{
						intLocationType = 1;
					}
					else if (Strings.UCase(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1), 7)) == Strings.UCase("Do not "))
					{
						intLocationType = 2;
					}
					rsControl.Set_Fields("ShowLocation", intLocationType);
					rsControl.Set_Fields("DateCreated", DateTime.Today);
					rsControl.Set_Fields("User", modGlobalConstants.Statics.gstrUserID);
					rsControl.Set_Fields("BillingYear", FCConvert.ToString(Conversion.Val(modExtraModules.FormatYear(frmRateRecChoice.InstancePtr.cmbYear.Text))));
					rsControl.Set_Fields("MinimumAmount", FCConvert.ToDouble(frmRateRecChoice.InstancePtr.txtMinimumAmount.Text));
					dblMinimumAmount = FCConvert.ToDouble(frmRateRecChoice.InstancePtr.txtMinimumAmount.Text);
					if (Strings.Trim(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[1].RowNumber, 1)) != "")
					{
						if (Information.IsDate(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[1].RowNumber, 1)))
						{
							rsControl.Set_Fields("MailDate", DateAndTime.DateValue(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[1].RowNumber, 1)));
						}
						else
						{
							FCMessageBox.Show("Enter a valid Mail Date.", MsgBoxStyle.Exclamation, "Invalid Date");
							SaveContolInformation = false;
							return SaveContolInformation;
						}
					}
					else
					{
						FCMessageBox.Show("Enter a valid Mail Date.", MsgBoxStyle.Exclamation, "Invalid Date");
						SaveContolInformation = false;
						return SaveContolInformation;
					}
					if (Strings.Trim(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[2].RowNumber, 1)) != "")
					{
						if (Information.IsDate(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[2].RowNumber, 1)))
						{
							rsControl.Set_Fields("LienFilingDate", DateAndTime.DateValue(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[2].RowNumber, 1)));
						}
						else
						{
							FCMessageBox.Show("Enter a valid Lien Filing Date.", MsgBoxStyle.Exclamation, "Invalid Date");
							SaveContolInformation = false;
							return SaveContolInformation;
						}
					}
					else
					{
						FCMessageBox.Show("Enter a valid Lien Filing Date.", MsgBoxStyle.Exclamation, "Invalid Date");
						SaveContolInformation = false;
						return SaveContolInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[5].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("Muni", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[5].RowNumber, 1));
					}
					else
					{
						FCMessageBox.Show("Enter the Municipality Name.", MsgBoxStyle.Exclamation, "Invalid Name");
						SaveContolInformation = false;
						return SaveContolInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[6].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("County", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[6].RowNumber, 1));
					}
					else
					{
						FCMessageBox.Show("Enter the County Name.", MsgBoxStyle.Exclamation, "Invalid Name");
						SaveContolInformation = false;
						return SaveContolInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[7].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("State", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[7].RowNumber, 1));
					}
					else
					{
						FCMessageBox.Show("Enter the State.", MsgBoxStyle.Exclamation, "Invalid State");
						SaveContolInformation = false;
						return SaveContolInformation;
					}
					// If vsVariableSetup.TextMatrix(frfFreeReport(8).RowNumber, 1) <> "" Then
					rsControl.Set_Fields("Designation", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[8].RowNumber, 1));

					rsControl.Set_Fields("Signer", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[9].RowNumber, 1));

					rsControl.Set_Fields("SignerPhone", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[10].RowNumber, 1));

					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[11].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("CollectorPhone", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[11].RowNumber, 1));
					}
					else
					{
						FCMessageBox.Show("Enter the Collector's Phone.", MsgBoxStyle.Exclamation, "Invalid Phone");
						SaveContolInformation = false;
						return SaveContolInformation;
					}
					if (Conversion.Val(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[12].RowNumber, 1)) != 0)
					{
						rsControl.Set_Fields("CertMailFee", FCConvert.ToDouble(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[12].RowNumber, 1)));
					}
					else
					{
						rsControl.Set_Fields("CertMailFee", 0);
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[12].RowNumber, 1, "0.00");
					}
					if (Conversion.Val(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[14].RowNumber, 1)) == 0)
					{
						rsControl.Set_Fields("ForeclosureFee", 0);
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[14].RowNumber, 1, "0.00");
					}
					else
					{
						rsControl.Set_Fields("ForeclosureFee", FCConvert.ToDouble(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[14].RowNumber, 1)));
					}
					if (Strings.Trim(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1)) != "")
					{
						if (Information.IsDate(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1)))
						{
							rsControl.Set_Fields("ForeclosureDate", DateAndTime.DateValue(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1)));
						}
						else
						{
							FCMessageBox.Show("Enter a valid Foreclosure Date.", MsgBoxStyle.Exclamation, "Invalid Date");
							SaveContolInformation = false;
							return SaveContolInformation;
						}
					}
					else
					{
						FCMessageBox.Show("Enter a valid Foreclosure Date.", MsgBoxStyle.Exclamation, "Invalid Date");
						SaveContolInformation = false;
						return SaveContolInformation;
					}
					rsControl.Set_Fields("LegalDescription", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[16].RowNumber, 1));
                    if (Strings.Mid(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[17].RowNumber, 1), 29, 4).ToUpper() == "LAST")
                    { // trocls-147 04.16.2018 kjr  CODE FREEZE, 5.22.18
                        rsControl.Set_Fields("MailTo", 2);
                    }
                    else
                    {
                        if (Strings.Mid(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[17].RowNumber, 1), 27, 3) == "C/O")
                        {
                            rsControl.Set_Fields("MailTo", 1);
                        }
                        else
                        {
                            rsControl.Set_Fields("MailTo", 0);
                        }
                    }
                rsControl.Set_Fields("PayCertMailFee", FCConvert.CBool(Strings.Left(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[18].RowNumber, 1), 2) == "Ye"));
					rsControl.Set_Fields("SendCopyToMortHolder", FCConvert.CBool(Strings.Left(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[30].RowNumber, 1), 2) == "Ye"));
					if (FCConvert.ToBoolean(rsControl.Get_Fields_Boolean("SendCopyToMortHolder")))
					{
						rsControl.Set_Fields("ChargeForMortHolder", !FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[30].RowNumber, 1), 9) == "Yes, with"));
					}
					else
					{
						rsControl.Set_Fields("ChargeForMortHolder", false);
					}
					// MAL@20070102: Call Reference: 104274
					rsControl.Set_Fields("SendCopyToIntParty", FCConvert.CBool(Strings.Left(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1), 2) == "Ye"));
					if (FCConvert.ToBoolean(rsControl.Get_Fields_Boolean("SendCopyToIntParty")))
					{
						rsControl.Set_Fields("ChargeForIntParty", !FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1), 9) == "Yes, with"));
					}
					else
					{
						rsControl.Set_Fields("ChargeForIntParty", false);
					}
					rsControl.Set_Fields("SendCopyToNewOwner", Strings.Trim(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[34].RowNumber, 1)));
					rsControl.Set_Fields("DateUpdated", DateTime.Today);
					// need to save the interest question here
					rsControl.Set_Fields("UseMailDateForMaturity", modGlobal.Statics.gboolUseMailDateForMaturity);
					// MAL@20080715: Save new default sort order
					// Tracker Reference: 11834
					if (cmbSortOrder.Text == "Account")
					{
						rsControl.Set_Fields("DefaultSort", "A");
					}
					else
					{
						rsControl.Set_Fields("DefaultSort", "N");
					}
					rsControl.Update();
				}
				return SaveContolInformation;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				SaveContolInformation = false;
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Saving Control Information");
			}
			return SaveContolInformation;
		}

		private void SetCellValue()
		{
			// this sub will make sure that any edit text is put into the grid so that
			// the GetVariableValue function can find a recently edited value correctly
			if (vsVariableSetup.EditText != "")
			{
				vsVariableSetup.TextMatrix(vsVariableSetup.Row, vsVariableSetup.Col, vsVariableSetup.EditText);
				vsVariableSetup.Select(0, 0);
			}
			if (vsRecommitment.EditText != "")
			{
				vsRecommitment.TextMatrix(vsRecommitment.Row, vsRecommitment.Col, vsRecommitment.EditText);
				vsRecommitment.Select(0, 0);
			}
		}

		private void InitialSettings()
		{
			// this will reset the LienStatusEligibility field of the bills
			// to the previous and check to see if it is still eligibly for this step
			clsDRWrapper rsInitial = new clsDRWrapper();
			clsDRWrapper rsInitialLien = new clsDRWrapper();
			clsDRWrapper rsRE = new clsDRWrapper();
			string strSQL;
			string strAccountList;
			string strNoNEligibleAccounts;
			string strBankruptcyAccounts;
			bool boolEligible;
			int lngMaxAccount;
			int lngCount;
			string strTemp = "";
			// Debug.Print Time & " - Start Initial Settings"
			strSQL = "";
			strAccountList = "";
			strNoNEligibleAccounts = "";
			strBankruptcyAccounts = "";
			lngMaxAccount = 0;
			boolEligible = false;
			// this will set all of the accounts that are currently eligible back to the last eligibility
			if (Strings.UCase(modCustomReport.Statics.strReportType) == "30DAYNOTICE")
			{
				strSQL = "UPDATE BillingMaster SET LienStatusEligibility = 0 WHERE BillingType = 'RE' AND LienProcessStatus = 1 AND LienRecordNumber = 0 AND BillingYear / 10 = " + frmRateRecChoice.InstancePtr.cmbYear.Items[frmRateRecChoice.InstancePtr.cmbYear.SelectedIndex].ToString() + " AND RateKey IN " + strRK;
			}
			else if (Strings.UCase(modCustomReport.Statics.strReportType) == "LIENPROCESS")
			{
				strSQL = "UPDATE BillingMaster SET LienStatusEligibility = 2 WHERE BillingType = 'RE' AND LienProcessStatus = 3 AND LienRecordNumber = 0 AND BillingYear / 10 = " + frmRateRecChoice.InstancePtr.cmbYear.Items[frmRateRecChoice.InstancePtr.cmbYear.SelectedIndex].ToString() + " AND RateKey IN " + strRK;
			}
			else if (Strings.UCase(modCustomReport.Statics.strReportType) == "LIENMATURITY")
			{
				// kgk trocl-394 04-20-2011  This is the Lien RK and doesn't match up with the billing RK
				// strSQL = "UPDATE BillingMaster SET LienStatusEligibility = 4 WHERE BillingType = 'RE' AND LienProcessStatus = 5 AND BillingYear \ 10 = " & frmRateRecChoice.cmbYear.List(frmRateRecChoice.cmbYear.ListIndex) & " AND RateKey IN " & strRK
				// strSQL = "UPDATE (BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.LienRecordNumber) SET BillingMaster.LienStatusEligibility = 4 WHERE BillingMaster.BillingType = 'RE' AND BillingMaster.LienProcessStatus = 5 AND BillingMaster.BillingYear / 10 = " & frmRateRecChoice.cmbYear.List(frmRateRecChoice.cmbYear.ListIndex) & " AND LienRec.RateKey IN " & strRK
				strSQL = "UPDATE BillingMaster SET LienStatusEligibility = 4 FROM (BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.ID) WHERE BillingMaster.BillingType = 'RE' AND BillingMaster.LienProcessStatus = 5 AND BillingMaster.BillingYear / 10 = " + frmRateRecChoice.InstancePtr.cmbYear.Items[frmRateRecChoice.InstancePtr.cmbYear.SelectedIndex].ToString() + " AND LienRec.RateKey IN " + strRK;
			}
			if (rsInitial.Execute(strSQL, modExtraModules.strCLDatabase))
			{
				// Check all of the accounts that have a certain eligibility to recalculate that eligibility (check to see if the account was paid off)
				if (Strings.UCase(modCustomReport.Statics.strReportType) == "30DAYNOTICE")
				{
					// if initial run
					strSQL = "SELECT *, (TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4) AS Principal FROM (" + modGlobal.Statics.gstrBillYrQuery + ") AS qBY WHERE BillingType = 'RE' AND LienStatusEligibility = 0 AND NOT (LienProcessExclusion = 1) AND RateKey IN " + strRK;
				}
				else if (Strings.UCase(modCustomReport.Statics.strReportType) == "LIENPROCESS")
				{
					strSQL = "SELECT *, (TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4) AS Principal FROM (" + modGlobal.Statics.gstrBillYrQuery + ") AS qBY WHERE BillingType = 'RE' AND LienStatusEligibility = 2 AND NOT (LienProcessExclusion = 1) AND RateKey IN " + strRK;
				}
				else if (Strings.UCase(modCustomReport.Statics.strReportType) == "LIENMATURITY")
				{
					rsInitialLien = new clsDRWrapper();
					rsRE = new clsDRWrapper();
					// strSQL = "SELECT *, (TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4) AS Principal FROM BillingMasterYear WHERE BillingType = 'RE' AND LienProcessStatus = 4 AND NOT LienProcessExclusion AND LienRec.RateKey IN " & strRK
					// strSQL = "SELECT *, (TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4) AS Principal FROM BillingMasterYear WHERE BillingType = 'RE' AND LienSTATUSELIGIBILITY = 4 AND NOT LienProcessExclusion AND LienRec.RateKey IN " & strRK
					strSQL = "SELECT *, (TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4) AS Principal FROM (" + modGlobal.Statics.gstrBillYrQuery + ") AS qBY WHERE BillingType = 'RE' AND (LienSTATUSELIGIBILITY = 4 OR LIENPROCESSSTATUS = 4) AND NOT (LienProcessExclusion = 1) AND RateKey IN " + strRK;
					rsInitialLien.OpenRecordset("SELECT * FROM LienRec");
					// this will store the Lien Records
					if (modStatusPayments.Statics.boolRE)
					{
						rsRE.OpenRecordset("SELECT * FROM Master", modExtraModules.strREDatabase);
					}
					else
					{
						rsRE.OpenRecordset("SELECT * FROM PPMaster", modExtraModules.strPPDatabase);
					}
				}
				// Debug.Print Time & " - Find SQL"
				rsInitial.OpenRecordset(strSQL);
				// select all of the accounts that are eligible to have their eligibility recalculated
				if (rsInitial.EndOfFile() != true)
				{
					lngMaxAccount = rsInitial.RecordCount();
				}
				else
				{
					lngCount = 0;
				}
				lngCount = 0;
				// xx        frmWait.Init "Please Wait..." & vbCrLf & "Calculating Eligibility", True, lngMaxAccount, True
				//this.Refresh();
				// Debug.Print Time & " - Open Recordset"
				while (!rsInitial.EndOfFile())
				{
					// cycle through them and find the accounts that are still eligible
					// xx           frmWait.IncrementProgress
					App.DoEvents();
					boolEligible = false;
					if (Strings.UCase(modCustomReport.Statics.strReportType) == "30DAYNOTICE")
					{
						// check in the BillingMaster to see if there is any outstanding balances
						// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
						if (Conversion.Val(rsInitial.Get_Fields("Principal")) - Conversion.Val(rsInitial.Get_Fields_Decimal("InterestCharged")) + Conversion.Val(rsInitial.Get_Fields_Decimal("DemandFees")) > Conversion.Val(rsInitial.Get_Fields_Decimal("PrincipalPaid")) + Conversion.Val(rsInitial.Get_Fields_Decimal("InterestPaid")) + Conversion.Val(rsInitial.Get_Fields_Decimal("DemandFeesPaid")))
						{
							boolEligible = true;
						}
						else
						{
							boolEligible = false;
						}
					}
					else if (Strings.UCase(modCustomReport.Statics.strReportType) == "LIENPROCESS")
					{
						// check in the BillingMaster to see if there is any outstanding balances
						// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
						if (Conversion.Val(rsInitial.Get_Fields("Principal")) - Conversion.Val(rsInitial.Get_Fields_Decimal("InterestCharged")) + Conversion.Val(rsInitial.Get_Fields_Decimal("DemandFees")) > Conversion.Val(rsInitial.Get_Fields_Decimal("PrincipalPaid")) + Conversion.Val(rsInitial.Get_Fields_Decimal("InterestPaid")) + Conversion.Val(rsInitial.Get_Fields_Decimal("DemandFeesPaid")))
						{
							boolEligible = true;
						}
						else
						{
							boolEligible = false;
						}
					}
					else if (Strings.UCase(modCustomReport.Statics.strReportType) == "LIENMATURITY")
					{
						// check in the LienRec to see if there is any outstanding balances
						if (rsInitialLien.FindFirstRecord("ID", rsInitial.Get_Fields_Int32("LienRecordNumber")))
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							if (((rsInitial.Get_Fields("Account"))) == 188)
							{
								boolEligible = boolEligible;
							}
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
							if (Conversion.Val(rsInitialLien.Get_Fields("Principal")) - Conversion.Val(rsInitialLien.Get_Fields_Decimal("InterestCharged")) + Conversion.Val(rsInitialLien.Get_Fields("Costs")) + Conversion.Val(rsInitialLien.Get_Fields("MaturityFee")) > Conversion.Val(rsInitialLien.Get_Fields_Decimal("PrincipalPaid")) + Conversion.Val(rsInitialLien.Get_Fields_Decimal("InterestPaid")) + Conversion.Val(rsInitialLien.Get_Fields_Decimal("CostsPaid")))
							{
								if (modStatusPayments.Statics.boolRE)
								{
									// strTemp = "RSAccount = " & rsInitial.Get_Fields("Account") & " AND RSCard = 1"
									// If rsRE.FindFirstRecord(, , strTemp) Then
									// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
									if (rsRE.FindFirstRecord2("RSAccount,RSCard", FCConvert.ToString(rsInitial.Get_Fields("Account")) + ",1", ","))
									{
										if (!rsRE.NoMatch)
										{
											if (FCConvert.ToBoolean(rsRE.Get_Fields_Boolean("InBankruptcy")))
											{
												// check to see if this account is in bankruptcy
												boolEligible = false;
												// if it is then it is not eligible for lien maturity
												// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
												strBankruptcyAccounts += FCConvert.ToString(rsInitial.Get_Fields("Account")) + ",";
											}
											else
											{
												boolEligible = true;
											}
										}
										else
										{
											boolEligible = false;
										}
									}
									else
									{
										boolEligible = false;
										// if you can not find the account in the RE database, then it is not eligible
									}
								}
								else
								{
									// there is no bankruptcy in PP so if it gets this far then it is eligible
									boolEligible = true;
								}
							}
							else
							{
								boolEligible = false;
							}
						}
						else
						{
							boolEligible = false;
						}
					}
					if (boolEligible)
					{
						strAccountList += FCConvert.ToString(rsInitial.Get_Fields_Int32("ID"));
						// add this billkey to the account list
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						strNoNEligibleAccounts += FCConvert.ToString(rsInitial.Get_Fields("Account")) + ",";
					}
					if (strAccountList.Length > 0)
					{
						if (Strings.Right(strAccountList, 1) == ",")
						{
							strAccountList = Strings.Left(strAccountList, strAccountList.Length - 1);
						}
						if (Strings.UCase(modCustomReport.Statics.strReportType) == "30DAYNOTICE")
						{
							strSQL = "UPDATE BillingMaster SET LienStatusEligibility = 1 WHERE ID = " + strAccountList;
						}
						else if (Strings.UCase(modCustomReport.Statics.strReportType) == "LIENPROCESS")
						{
							strSQL = "UPDATE BillingMaster SET LienStatusEligibility = 3 WHERE ID = " + strAccountList;
						}
						else if (Strings.UCase(modCustomReport.Statics.strReportType) == "LIENMATURITY")
						{
							strSQL = "UPDATE BillingMaster SET LienStatusEligibility = 5 WHERE ID = " + strAccountList;
						}
						// Debug.Print Time & " - Updating Eligibility"
						if (strAccountList != "")
						{
							rsInitial.Execute(strSQL, modExtraModules.strCLDatabase);
							// set the account status eligibility
						}
						// Debug.Print Time & " - Updating Eligibility Finished"
						strAccountList = "";
						// this will reset the account list
					}
					// move to the next record
					rsInitial.MoveNext();
				}
				rsInitial.Reset();
				// xx       frmWait.Init "Please Wait..." & vbCrLf & "Setting Eligibility"
				// this will take the comma off of the end of the string if there is one
				if (strBankruptcyAccounts.Length > 0)
				{
					strBankruptcyAccounts = Strings.Left(strBankruptcyAccounts, strBankruptcyAccounts.Length - 1);
					frmWait.InstancePtr.Unload();
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					FCMessageBox.Show("Some accounts will not be processed due to bankruptcy.", MsgBoxStyle.Information, "Bankrupt Accounts");
					// this is where I actually run and save the report, It can be shown later from the Lien Maturity Menu
					rptBankruptcy.InstancePtr.Init("SELECT * FROM Master WHERE RSAccount IN (" + strBankruptcyAccounts + ") AND RSCard = 1", this.Modal);
				}
				if (strNoNEligibleAccounts.Length > 0)
				{
					strNoNEligibleAccounts = Strings.Left(strNoNEligibleAccounts, strNoNEligibleAccounts.Length - 1);
				}
				
			}
			else
			{
				FCMessageBox.Show("An ERROR occured while updating the database.", MsgBoxStyle.Critical, "Lien Status Eligibility");
			}
		}

		private void SetPositions()
		{
			int lngTemp;
			// this will set the positions of the frames and other controls on the form
			fraEdit.Top = 0;
			fraEdit.Left = FCConvert.ToInt32((this.Width - fraEdit.Width) / 2.0);
			lngTemp = (fraVariableSetup.Top + vsVariableSetup.Height);
			fraSummary.Top = FCConvert.ToInt32((this.Height - fraSummary.Height) / 2.0);
			fraSummary.Left = FCConvert.ToInt32((this.Width - fraSummary.Width) / 2.0);
		}

		private void SaveDefaultNotice_8(short intType, string strName)
		{
			SaveDefaultNotice(ref intType, ref strName);
		}

		private void SaveDefaultNotice(ref short intType, ref string strName)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsDef = new clsDRWrapper();
				// this will save the default notice into the database
				rsDef.OpenRecordset("SELECT * FROM Collections", modExtraModules.strCLDatabase);
				if (!rsDef.EndOfFile())
				{
					rsDef.Edit();
				}
				else
				{
					rsDef.AddNew();
				}
				if (chkRecommitment.CheckState == Wisej.Web.CheckState.Checked)
				{
					switch (intType)
					{
						case 1:
							{
								// 30DN
								rsDef.Set_Fields("30DayNoticeR", strName);
								break;
							}
						case 2:
							{
								// Lien
								rsDef.Set_Fields("LienNoticeR", strName);
								break;
							}
						case 3:
							{
								// Maturity
								rsDef.Set_Fields("MaturityNoticeR", strName);
								break;
							}
					}
					//end switch
				}
				else
				{
					switch (intType)
					{
						case 1:
							{
								// 30DN
								rsDef.Set_Fields("30DayNotice", strName);
								break;
							}
						case 2:
							{
								// Lien
								rsDef.Set_Fields("LienNotice", strName);
								break;
							}
						case 3:
							{
								// Maturity
								rsDef.Set_Fields("MaturityNotice", strName);
								break;
							}
					}
					//end switch
				}
				rsDef.Update();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Save Default Notice");
			}
		}

		private string GetDefaultNotice_2(short intType)
		{
			return GetDefaultNotice(ref intType);
		}

		private string GetDefaultNotice(ref short intType)
		{
			string GetDefaultNotice = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will return the string of the name of the default report
				clsDRWrapper rsDef = new clsDRWrapper();
				string strName = "";
				rsDef.OpenRecordset("SELECT * FROM Collections", modExtraModules.strCLDatabase);
				if (!rsDef.EndOfFile())
				{
					if (chkRecommitment.CheckState == Wisej.Web.CheckState.Checked)
					{
						switch (intType)
						{
							case 1:
								{
									// 30DN
									strName = FCConvert.ToString(rsDef.Get_Fields_String("30DayNoticeR"));
									break;
								}
							case 2:
								{
									// Lien
									strName = FCConvert.ToString(rsDef.Get_Fields_String("LienNoticeR"));
									break;
								}
							case 3:
								{
									// Maturity
									strName = FCConvert.ToString(rsDef.Get_Fields_String("MaturityNoticeR"));
									break;
								}
							case 10:
								{
									// Reminder Notice Form
									strName = FCConvert.ToString(rsDef.Get_Fields_String("RNForm"));
									break;
								}
						}
						//end switch
					}
					else
					{
						switch (intType)
						{
							case 1:
								{
									// 30DN
									strName = FCConvert.ToString(rsDef.Get_Fields_String("30DayNotice"));
									break;
								}
							case 2:
								{
									// Lien
									strName = FCConvert.ToString(rsDef.Get_Fields_String("LienNotice"));
									break;
								}
							case 3:
								{
									// Maturity
									strName = FCConvert.ToString(rsDef.Get_Fields_String("MaturityNotice"));
									break;
								}
							case 10:
								{
									// Reminder Notice Form
									strName = FCConvert.ToString(rsDef.Get_Fields_String("RNForm"));
									break;
								}
						}
						//end switch
					}
				}
				GetDefaultNotice = strName;
				return GetDefaultNotice;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				GetDefaultNotice = "";
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Get Default Notice");
			}
			return GetDefaultNotice;
		}

		private void SaveRateRecInformation()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this routine will save the dates in the rate record
				// save the dates into the Rate record
				DateTime dtDate = DateTime.FromOADate(0);
				clsDRWrapper rsSaveRate = new clsDRWrapper();
				clsDRWrapper rsGetKeys = new clsDRWrapper();
				string strField = "";
				string strRateKeys = "";
				frmWait.InstancePtr.Unload();
				// MsgBox ("This information will be saved into the Rate record and can be edited manually.")
				if (Strings.UCase(modCustomReport.Statics.strReportType) == "30DAYNOTICE")
				{
					dtDate = DateAndTime.DateValue(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[0].RowNumber, 1));
					strField = "30DNDate";
				}
				else if (Strings.UCase(modCustomReport.Statics.strReportType) == "LIENPROCESS")
				{
					dtDate = DateAndTime.DateValue(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[0].RowNumber, 1));
					strField = "LienDate";
				}
				else if (Strings.UCase(modCustomReport.Statics.strReportType) == "LIENMATURITY")
				{
					dtDate = DateAndTime.DateValue(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[1].RowNumber, 1));
					// kgk 04-15-2011  frfFreeReport(0).RowNumber, 1)
					strField = "MaturityDate";
				}
				rsGetKeys.OpenRecordset("SELECT DISTINCT RateKey FROM (" + modGlobal.Statics.gstrBillYrQuery + ") AS qBY");
				while (!rsGetKeys.EndOfFile())
				{
					strRateKeys += FCConvert.ToString(rsGetKeys.Get_Fields_Int32("RateKey")) + ",";
					rsGetKeys.MoveNext();
				}
				if (Strings.Trim(strRateKeys) != "")
				{
					strRateKeys = "(" + Strings.Left(strRateKeys, strRateKeys.Length - 1) + ")";
					rsSaveRate.Execute("UPDATE RateRec Set [" + strField + "] = '" + Strings.Format(dtDate, "MM/dd/yyyy") + "' WHERE [" + strField + "] IS NULL AND ID IN " + strRateKeys, modExtraModules.strCLDatabase);
				}
				else
				{
					// do nothing...no keys
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Saving Rate Info");
			}
		}

		private bool CheckFormMandatoryVariables_2(short intType, string strText, ref string strReturnVariables)
		{
			return CheckFormMandatoryVariables(ref intType, ref strText, ref strReturnVariables);
		}

		private bool CheckFormMandatoryVariables(ref short intType, ref string strText, ref string strReturnVariables)
		{
			bool CheckFormMandatoryVariables = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will check all of the text in the string saved by the user to make
				// sure that all of the mandatory information has been shown to the owner/MH.
				// It will return True if all of the variables have been used and false otherwise
				CheckFormMandatoryVariables = true;
				// set it to true and try to prove it wrong
				switch (intType)
				{
					case 1:
						{
							// 30 Day Notice
							if (Strings.InStr(1, strText, "<MUNI>") == 0)
							{
								CheckFormMandatoryVariables = false;
								strReturnVariables += "MUNI, ";
							}
							if (Strings.InStr(1, strText, "<MAILDATE>") == 0)
							{
								CheckFormMandatoryVariables = false;
								strReturnVariables += "MAILDATE, ";
							}
							if (Strings.InStr(1, strText, "<NAMEADDRESS>") == 0)
							{
								CheckFormMandatoryVariables = false;
								strReturnVariables += "NAMEADDRESS, ";
							}
							if (Strings.InStr(1, strText, "<TAXYEAR>") == 0)
							{
								CheckFormMandatoryVariables = false;
								strReturnVariables += "TAXYEAR, ";
							}
							if (Strings.InStr(1, strText, "<PRINCIPAL>") == 0)
							{
								CheckFormMandatoryVariables = false;
								strReturnVariables += "PRINCIPAL, ";
							}
							if (Strings.InStr(1, strText, "<OWNER>") == 0)
							{
								CheckFormMandatoryVariables = false;
								strReturnVariables += "OWNER, ";
							}

							break;
						}
					case 2:
						{
							// Lien
							break;
						}
					case 3:
						{
							// Lien Maturity
							break;
						}
				}
				//end switch
				return CheckFormMandatoryVariables;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Saving Rate Info");
			}
			return CheckFormMandatoryVariables;
		}

		private bool ContainsThisString(ref string strVariable, ref object strTotalString)
		{
			bool ContainsThisString = false;
			return ContainsThisString;
		}

		private bool ValidateUserVariables()
		{
			bool ValidateUserVariables = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				DateTime dtDate1;
				DateTime dtDate2;
				ValidateUserVariables = true;
				if (Strings.UCase(modCustomReport.Statics.strReportType) == "30DAYNOTICE")
				{
					// dtDate1 = CDate(vsVariableSetup.TextMatrix(frfFreeReport(0).RowNumber, 1))
				}
				else if (Strings.UCase(modCustomReport.Statics.strReportType) == "LIENPROCESS")
				{
					if (Strings.Trim(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[10].RowNumber, 1)) != "")
					{
						if (Information.IsDate(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[10].RowNumber, 1)))
						{
							dtDate1 = DateAndTime.DateValue(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[10].RowNumber, 1));
							if (DateTime.Today.ToOADate() > dtDate1.ToOADate())
							{
								FCMessageBox.Show("The commissioner's expiration is before the current date.", MsgBoxStyle.Exclamation, "Expired Date");
								ValidateUserVariables = false;
							}
						}
						else
						{
						}
					}
				}
				else if (Strings.UCase(modCustomReport.Statics.strReportType) == "LIENMATURITY")
				{
					if (Strings.Trim(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1)) != "")
					{
						// foreclosure date
						if (Information.IsDate(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1)))
						{
							dtDate2 = DateAndTime.DateValue(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1));
							if (Strings.Trim(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[1].RowNumber, 1)) != "")
							{
								// mail date  'kgk 04-15-2011  frfFreeReport(0).RowNumber
								if (Information.IsDate(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[1].RowNumber, 1)))
								{
									dtDate1 = DateAndTime.DateValue(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[1].RowNumber, 1));
									if (dtDate1.ToOADate() > dtDate2.ToOADate())
									{
										FCMessageBox.Show("The Foreclosure Date can not be earlier than the mail date.", MsgBoxStyle.Exclamation, "Invalid Date");
										ValidateUserVariables = false;
									}
								}
								else
								{
									FCMessageBox.Show("Please enter a valid mail date.", MsgBoxStyle.Exclamation, "Valid Date");
									ValidateUserVariables = false;
								}
							}
							if (Strings.Trim(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[2].RowNumber, 1)) != "")
							{
								// lien filing date
								if (Information.IsDate(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[2].RowNumber, 1)))
								{
									dtDate1 = DateAndTime.DateValue(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[2].RowNumber, 1));
									if (dtDate1.ToOADate() > dtDate2.ToOADate())
									{
										FCMessageBox.Show("The Foreclosure Date can not be earlier than the lien filing date.", MsgBoxStyle.Exclamation, "Invalid Date");
										ValidateUserVariables = false;
									}
								}
								else
								{
									FCMessageBox.Show("Please enter a valid lien filing date.", MsgBoxStyle.Exclamation, "Valid Date");
									ValidateUserVariables = false;
								}
							}
						}
						else
						{
							FCMessageBox.Show("Please enter a valid foreclosure date.", MsgBoxStyle.Exclamation, "Valid Date");
							ValidateUserVariables = false;
						}
					}
				}
				return ValidateUserVariables;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Saving Rate Info");
			}
			return ValidateUserVariables;
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFilePrintPreview_Click(sender, e);
		}

		private void cmdFilePrintCode_Click(object sender, EventArgs e)
		{
			this.mnuFilePrintCode_Click(sender, e);
		}

		private void cmdFileRefresh_Click(object sender, EventArgs e)
		{
			this.mnuFileRefresh_Click(sender, e);
		}

		private void cmdFileChangeAct_Click(object sender, EventArgs e)
		{
			this.mnuFileChangeAct_Click(sender, e);
		}

		private void cmdFileExclusion_Click(object sender, EventArgs e)
		{
			this.mnuFileExclusion_Click(sender, e);
		}


	}
}
