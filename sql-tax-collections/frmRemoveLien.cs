﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmRemoveLien.
	/// </summary>
	public partial class frmRemoveLien : BaseForm
	{
		public frmRemoveLien()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmRemoveLien InstancePtr
		{
			get
			{
				return (frmRemoveLien)Sys.GetInstance(typeof(frmRemoveLien));
			}
		}

		protected frmRemoveLien _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/26/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               09/14/2004              *
		// ********************************************************
		int lngAcct;
		int lngLR;
		int lngBK;
		int intAct;
		bool boolRefill;
		double dblPrin;
		bool boolTA;

		private void cmbYear_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolRefill)
			{
				lngLR = cmbYear.ItemData(cmbYear.SelectedIndex);
				if (lngLR > 0)
				{
					FillPaymentGrid();
					btnProcess.Enabled = true;
				}
				else
				{
					btnProcess.Enabled = false;
				}
			}
		}

		private void cmbYear_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Space)
			{
				KeyCode = 0;
				if (modAPIsConst.SendMessageByNum(cmbYear.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
				{
					modAPIsConst.SendMessageByNum(cmbYear.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
				}
			}
		}

		private void frmRemoveLien_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = 0;
			}
		}

		private void frmRemoveLien_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmRemoveLien.Icon	= "frmRemoveLien.frx":0000";
			//frmRemoveLien.FillStyle	= 0;
			//frmRemoveLien.ScaleWidth	= 9045;
			//frmRemoveLien.ScaleHeight	= 7560;
			//frmRemoveLien.LinkTopic	= "Form2";
			//frmRemoveLien.LockControls	= -1  'True;
			//frmRemoveLien.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "8.25";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//txtAccount.MaxLength	= 10;
			//vsPayments.BackColor	= "-2147483643";
			//			//vsPayments.ForeColor	= "-2147483640";
			//vsPayments.BorderStyle	= 1;
			//vsPayments.FillStyle	= 0;
			//vsPayments.Appearance	= 1;
			//vsPayments.GridLines	= 1;
			//vsPayments.WordWrap	= 0;
			//vsPayments.ScrollBars	= 3;
			//vsPayments.RightToLeft	= 0;
			//vsPayments._cx	= 15266;
			//vsPayments._cy	= 7992;
			//vsPayments._ConvInfo	= 1;
			//vsPayments.MousePointer	= 0;
			//vsPayments.BackColorFixed	= -2147483633;
			//			//vsPayments.ForeColorFixed	= -2147483630;
			//vsPayments.BackColorSel	= -2147483635;
			//			//vsPayments.ForeColorSel	= -2147483634;
			//vsPayments.BackColorBkg	= -2147483636;
			//vsPayments.BackColorAlternate	= -2147483643;
			//vsPayments.GridColor	= -2147483633;
			//vsPayments.GridColorFixed	= -2147483632;
			//vsPayments.TreeColor	= -2147483632;
			//vsPayments.FloodColor	= 192;
			//vsPayments.SheetBorder	= -2147483642;
			//vsPayments.FocusRect	= 1;
			//vsPayments.HighLight	= 1;
			//vsPayments.AllowSelection	= -1  'True;
			//vsPayments.AllowBigSelection	= -1  'True;
			//vsPayments.AllowUserResizing	= 0;
			//vsPayments.SelectionMode	= 0;
			//vsPayments.GridLinesFixed	= 2;
			//vsPayments.GridLineWidth	= 1;
			//vsPayments.RowHeightMin	= 0;
			//vsPayments.RowHeightMax	= 0;
			//vsPayments.ColWidthMin	= 0;
			//vsPayments.ColWidthMax	= 0;
			//vsPayments.ExtendLastCol	= 0   'False;
			//vsPayments.FormatString	= "";
			//vsPayments.ScrollTrack	= -1  'True;
			//vsPayments.ScrollTips	= 0   'False;
			//vsPayments.MergeCells	= 0;
			//vsPayments.MergeCompare	= 0;
			//vsPayments.AutoResize	= -1  'True;
			//vsPayments.AutoSizeMode	= 0;
			//vsPayments.AutoSearch	= 0;
			//vsPayments.AutoSearchDelay	= 2;
			//vsPayments.MultiTotals	= -1  'True;
			//vsPayments.SubtotalPosition	= 1;
			//vsPayments.OutlineBar	= 0;
			//vsPayments.OutlineCol	= 0;
			//vsPayments.Ellipsis	= 0;
			//vsPayments.ExplorerBar	= 0;
			//vsPayments.PicturesOver	= 0   'False;
			//vsPayments.PictureType	= 0;
			//vsPayments.TabBehavior	= 0;
			//vsPayments.OwnerDraw	= 0;
			//vsPayments.ShowComboButton	= -1  'True;
			//vsPayments.TextStyle	= 0;
			//vsPayments.TextStyleFixed	= 0;
			//vsPayments.OleDragMode	= 0;
			//vsPayments.OleDropMode	= 0;
			//vsPayments.ComboSearch	= 3;
			//vsPayments.AutoSizeMouse	= -1  'True;
			//vsPayments.AllowUserFreezing	= 0;
			//vsPayments.BackColorFrozen	= 0;
			//			//vsPayments.ForeColorFrozen	= 0;
			//vsPayments.WallPaperAlignment	= 9;
			//vsElasticLight1.OleObjectBlob	= "frmRemoveLien.frx":058A";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			FormatPaymentGrid();
			modGlobalFunctions.SetTRIOColors(this);
			intAct = 1;
			this.Text = "Remove From Lien Status";
			// lblInstructions.Text = "Please enter the account that you would like to remove from lien status, then press 'Enter' or F10."
			// lblInstructions.Text = "Account:"
		}

		private void frmRemoveLien_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				Close();
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void frmRemoveLien_Resize(object sender, System.EventArgs e)
		{
			SetPaymentGridColWidths();
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuFileRemove_Click(object sender, System.EventArgs e)
		{
			string strText = "";
			int lngYear = 0;
			string strBD = "";
			switch (intAct)
			{
				case 0:
					{
						// do nothing
						break;
					}
				case 1:
					{
						// this is to enter the account number
						txtAccount_Validate(false);
						break;
					}
				case 2:
					{
						// this is to remove the account/year from lien status
						if (lngAcct > 0)
						{
							strBD = "";
							if (modGlobalConstants.Statics.gboolBD)
							{
								if (vsPayments.Rows > 1)
								{
									strBD = "\r\n" + "A manual entry will have to be made to adjust any payments that were made to the lien accounts.";
								}
							}
							strText = "Are you sure that you want to remove account " + FCConvert.ToString(lngAcct) + ", year " + cmbYear.Items[cmbYear.SelectedIndex].ToString() + " from Lien Status and delete the post lien activity." + "\r\n" + "You will have to reenter the payments manually." + strBD;
							lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbYear.Items[cmbYear.SelectedIndex].ToString(), 4))));
							if (FCMessageBox.Show(strText, MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Confirmation") == DialogResult.Yes)
							{
								// run the payment report
								rptLienRemovalPayments.InstancePtr.Init(lngLR, this.Modal);
								RemoveAccountFromLienStatus();
								modGlobalFunctions.AddCYAEntry_80("CL", "Sent Remove Lien Entry To BD", dblPrin.ToString(), lngYear.ToString());
								if (modGlobalConstants.Statics.gboolBD)
								{
									if (boolTA)
									{
										modGlobal.CreateTransferTaxToLienBDEntry(0, dblPrin, lngYear);
										// kk10172014 trocl-1192  Posting for Tax Acquired accounts to BD
									}
									else
									{
										modGlobal.CreateTransferTaxToLienBDEntry(dblPrin, 0, lngYear);
									}
								}
							}
						}
						break;
					}
			}
			//end switch
		}

		private void txtAccount_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F12 && btnProcess.Enabled == false)
			{
				KeyCode = 0;
				if (Conversion.Val(txtAccount.Text) > 0)
				{
					txtAccount_Validate(false);
				}
			}
		}

		private void txtAccount_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			btnProcess.Enabled = false;
			cmbYear.Clear();
			vsPayments.Visible = false;
			lblPayments.Visible = false;
			if (KeyAscii == 13)
			{
				KeyAscii = 0;
				if (Conversion.Val(txtAccount.Text) > 0)
				{
					cmbYear.Enabled = true;
                    //FC:FINAL:SBE - global fix - validate event should not be triggered twice (issue #4605)
                    //txtAccount_Validate(false);
                    Support.SendKeys("{TAB}");
                }
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}
		// Private Sub txtAccount_KeyPress(KeyAscii As Integer)
		// mnuFileRemove.Enabled = False
		// cmbYear.Clear
		// cmbYear.Enabled = False
		// fraPayments.Visible = False
		// If KeyAscii = 13 Then
		// KeyAscii = 0
		// If Val(txtAccount.Text) > 0 Then
		// cmbYear.Enabled = True
		// txtAccount_Validate False
		// End If
		// End If
		// End Sub
		private void txtAccount_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			clsDRWrapper rsTaxAcq = new clsDRWrapper();
			vsPayments.Visible = false;
			lblPayments.Visible = false;
			if (Conversion.Val(txtAccount.Text) != 0)
			{
				lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(txtAccount.Text)));
				// kk10172014 trocl-1192  Check for TA account
				rsTaxAcq.OpenRecordset("SELECT TaxAcquired FROM Master WHERE RSCard = 1 AND RSAccount = " + FCConvert.ToString(lngAcct), modExtraModules.strREDatabase);
				if (!rsTaxAcq.EndOfFile())
				{
					boolTA = FCConvert.ToBoolean(rsTaxAcq.Get_Fields_Boolean("TaxAcquired"));
				}
				else
				{
					boolTA = false;
				}
				boolRefill = true;
				if (FillYearCombo())
				{
					vsPayments.Visible = false;
					lblPayments.Visible = false;
					if (cmbYear.Visible && cmbYear.Enabled)
					{
						// automatically set the focus to the year field
						boolRefill = false;
						cmbYear.SelectedIndex = cmbYear.Items.Count - 1;
						cmbYear.Focus();
					}
					intAct = 2;
				}
				else
				{
					intAct = 1;
					FCMessageBox.Show("No liened bill years were found for this account number.", MsgBoxStyle.Information, "No Liened Years");
					// cmbYear.Enabled = False
					// cmbYear.Visible = False
					vsPayments.Visible = false;
					lblPayments.Visible = false;
					e.Cancel = true;
					btnProcess.Enabled = false;
				}
				boolRefill = false;
			}
			else
			{
				FCMessageBox.Show("Please enter an account number and press enter.", MsgBoxStyle.Information, "Invalid Account Number");
				e.Cancel = true;
				btnProcess.Enabled = false;
			}
		}

		public void txtAccount_Validate(bool Cancel)
		{
			txtAccount_Validating(txtAccount, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private bool FillYearCombo()
		{
			bool FillYearCombo = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the grid with all of the eligible years for the account entered
				clsDRWrapper rsYear = new clsDRWrapper();
				cmbYear.Clear();
				FillYearCombo = false;
				rsYear.RecordCount();
				rsYear.OpenRecordset("SELECT BillingYear, LienRecordNumber FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAcct) + " AND LienRecordNumber <> 0 ORDER BY BillingYear", modExtraModules.strCLDatabase);
				while (!rsYear.EndOfFile())
				{
					FillYearCombo = true;
					cmbYear.AddItem(modExtraModules.FormatYear(FCConvert.ToString(rsYear.Get_Fields_Int32("BillingYear"))));
					cmbYear.ItemData(cmbYear.NewIndex, FCConvert.ToInt32(rsYear.Get_Fields_Int32("LienRecordNumber")));
					rsYear.MoveNext();
				}
				cmbYear.Visible = true;
				rsYear.Reset();
				rsYear = null;
				return FillYearCombo;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Year Combo Error");
				FillYearCombo = false;
			}
			return FillYearCombo;
		}

		private void FormatPaymentGrid()
		{
			// this will format the payment grid and set the widths of rows, header titles ect
			vsPayments.Cols = 10;
			vsPayments.Rows = 1;
			SetPaymentGridColWidths();
			// set the column headers
			vsPayments.TextMatrix(0, 1, "Date");
			vsPayments.TextMatrix(0, 2, "Reference");
			vsPayments.TextMatrix(0, 3, "Period");
			vsPayments.TextMatrix(0, 4, "Code");
			vsPayments.TextMatrix(0, 5, "Principal");
			vsPayments.TextMatrix(0, 6, "Interest");
			vsPayments.TextMatrix(0, 7, "Costs");
			vsPayments.TextMatrix(0, 8, "Total");
			// alignment and other formatting
			vsPayments.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsPayments.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsPayments.ColAlignment(7, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsPayments.ColAlignment(8, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsPayments.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			vsPayments.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, vsPayments.Cols - 1, true);
			vsPayments.ColDataType(0, FCGrid.DataTypeSettings.flexDTBoolean);
		}

		private void SetPaymentGridColWidths()
		{
			int lngWid = 0;
			// set the widths
			lngWid = vsPayments.WidthOriginal;
			vsPayments.Cols = 10;
			vsPayments.ColWidth(0, FCConvert.ToInt32(lngWid * 0.0));
			// Check Box
			vsPayments.ColWidth(1, FCConvert.ToInt32(lngWid * 0.14));
			// Payment Date
			vsPayments.ColWidth(2, FCConvert.ToInt32(lngWid * 0.16));
			// Reference
			vsPayments.ColWidth(3, FCConvert.ToInt32(lngWid * 0.06));
			// Period
			vsPayments.ColWidth(4, FCConvert.ToInt32(lngWid * 0.06));
			// Code
			vsPayments.ColWidth(5, FCConvert.ToInt32(lngWid * 0.14));
			// Principal
			vsPayments.ColWidth(6, FCConvert.ToInt32(lngWid * 0.14));
			// Interest
			vsPayments.ColWidth(7, FCConvert.ToInt32(lngWid * 0.14));
			// Costs
			vsPayments.ColWidth(8, FCConvert.ToInt32(lngWid * 0.15));
			// Total
			vsPayments.ColWidth(9, 0);
			// Key
		}

		private void FillPaymentGrid()
		{
			// this will fill the payment grid with the lien payments of the account that was entered
			clsDRWrapper rsPay = new clsDRWrapper();
			string strPayment = "";
			string strSQL = "";
			double dblTotal = 0;
			// get the original principal
			strSQL = "SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(lngLR);
			rsPay.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
			if (!rsPay.EndOfFile())
			{
				dblPrin = rsPay.Get_Fields_Double("Principal") * -1;
			}
			strSQL = "SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngLR) + " AND BillCode = 'L' AND [Year] = " + modExtraModules.FormatYear(cmbYear.Items[cmbYear.SelectedIndex].ToString()) + " ORDER BY [Year]";
			vsPayments.Rows = 1;
			rsPay.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
			while (!rsPay.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
				dblTotal = rsPay.Get_Fields_Double("Principal") + Conversion.Val(rsPay.Get_Fields_Decimal("CurrentInterest")) + Conversion.Val(rsPay.Get_Fields_Decimal("PreLienInterest")) + Conversion.Val(rsPay.Get_Fields_Decimal("LienCost"));
				// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
				strPayment = "\t" + FCConvert.ToString(rsPay.Get_Fields_DateTime("RecordedTransactionDate")) + "\t" + FCConvert.ToString(rsPay.Get_Fields_String("Reference")) + "\t" + FCConvert.ToString(rsPay.Get_Fields("Period")) + "\t" + FCConvert.ToString(rsPay.Get_Fields("Code")) + "\t" + Strings.Format(rsPay.Get_Fields_Decimal("Principal"), "#,##0.##") + "\t" + FCConvert.ToString(Conversion.Val(rsPay.Get_Fields_Decimal("CurrentInterest")) + Conversion.Val(rsPay.Get_Fields_Decimal("PreLienInterest"))) + "\t" + Strings.Format(rsPay.Get_Fields_Decimal("LienCost"), "#,##0.##") + "\t" + FCConvert.ToString(dblTotal) + "\t" + FCConvert.ToString(rsPay.Get_Fields_Int32("ID"));
				vsPayments.AddItem(strPayment);
				rsPay.MoveNext();
			}
			SetPaymentGridColWidths();
			vsPayments.Visible = true;
			lblPayments.Visible = true;
			// If vsPayments.rows > 1 Then
			// mnuFileRemove.Enabled = True
			// Else
			// mnuFileRemove.Enabled = False
			// End If
			rsPay = null;
		}

		private void RemoveAccountFromLienStatus()
		{
			// this sub will actually remove the account from lien status
			clsDRWrapper rsDel = new clsDRWrapper();
			clsDRWrapper rsRate = new clsDRWrapper();
			string strSQL;
			DateTime dtIntDate;
			rsDel.DefaultDB = modExtraModules.strCLDatabase;
			strSQL = "SELECT * FROM RateRec";
			rsRate.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
			strSQL = "SELECT * FROM BillingMaster WHERE LienRecordNumber = " + FCConvert.ToString(lngLR);
			rsDel.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
			if (rsDel.EndOfFile() != true && rsDel.BeginningOfFile() != true && rsDel.RecordCount() == 1)
			{
				lngBK = FCConvert.ToInt32(rsDel.Get_Fields_Int32("ID"));
				// this will save the original bill record
				// write to the CYA table
				modGlobalFunctions.AddCYAEntry_8("CL", "Removed From Lien Status: " + FCConvert.ToString(lngAcct) + " - " + cmbYear.Items[cmbYear.SelectedIndex].ToString());
				// delete the lien and CHGINT payments
				strSQL = "DELETE FROM PaymentRec WHERE Account = " + FCConvert.ToString(lngAcct) + " AND BillKey = " + FCConvert.ToString(lngLR) + " AND BillCode = 'L' AND (Code = '3' OR Code = 'L' OR Code = 'I')";
				if (rsDel.Execute(strSQL, modExtraModules.strCLDatabase))
				{
					// delete the lien record
					strSQL = "DELETE FROM LienRec WHERE ID = " + FCConvert.ToString(lngLR);
					if (rsDel.Execute(strSQL, modExtraModules.strCLDatabase))
					{
						FCMessageBox.Show("This account has been removed from lien status and the post lien payments have been erased.", MsgBoxStyle.Information, "Remove Account");
						strSQL = "SELECT * FROM BillingMaster WHERE ID = " + FCConvert.ToString(lngBK);
						rsDel.OpenRecordset(strSQL);
						if (rsDel.EndOfFile() != true && rsDel.BeginningOfFile() != true)
						{
							rsDel.Edit();
							rsDel.Set_Fields("LienRecordNumber", 0);
							rsRate.FindFirstRecord("ID", rsDel.Get_Fields_Int32("RateKey"));
							if (rsRate.NoMatch)
							{
								rsDel.Set_Fields("InterestAppliedThroughDate", 0);
								// this should throw an error
							}
							else
							{
								// MAL@20080530: Changed to correctly get the last interest date
								// Tracker Reference: 14041
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								dtIntDate = modStatusPayments.GetLastInterestDate_CL(FCConvert.ToInt32(rsDel.Get_Fields("Account")), FCConvert.ToInt32(rsDel.Get_Fields_Int32("BillingYear")), DateTime.Today, lngBK);
								// rsDel.Get_Fields("InterestAppliedThroughDate") = rsRate.Get_Fields("BillingDate")
								rsDel.Set_Fields("InterestAppliedThroughDate", dtIntDate);
							}
							rsDel.Set_Fields("LienProcessStatus", "3");
							rsDel.Set_Fields("LienStatusEligibility", "4");
							rsDel.Set_Fields("WhetherBilledBefore", "A");
							rsDel.Update();
						}
						// reapply the payments made, not the lien charges or the interest charged
						// set the interest paid date back to the beginning
						// and charge the interest as it would be without the lien
					}
					else
					{
						FCMessageBox.Show("Error deleting the lien record.", MsgBoxStyle.Critical, "Deletion Error");
					}
				}
				else
				{
					FCMessageBox.Show("Error deleting the lien/interest charges." + "\r\n" + "This account has not been removed from lien status.", MsgBoxStyle.Critical, "Error Removing Lien/Interest Charges");
				}
			}
			else
			{
				FCMessageBox.Show("Could not find Lien Record to delete.", MsgBoxStyle.Critical, "Cannot Find Lien Record");
			}
			Close();
			return;
			ERROR_HANDLER:
			;
			FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err().Number) + " - " + Information.Err().Description, MsgBoxStyle.Critical, "Remove From Lien Status Error");
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFileRemove_Click(sender, e);
		}
	}
}
