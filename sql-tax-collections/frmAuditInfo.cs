﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmAuditInfo.
	/// </summary>
	public partial class frmAuditInfo : BaseForm
	{
		public frmAuditInfo()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.frPeriod = new System.Collections.Generic.List<fecherFoundation.FCFrame>();
			this.t2kInterestDate = new System.Collections.Generic.List<T2KDateBox>();
			this.t2kDueDate = new System.Collections.Generic.List<T2KDateBox>();
			this.lblDDate = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.lblIDate = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.frPeriod.AddControlArrayElement(frPeriod_3, 3);
			this.frPeriod.AddControlArrayElement(frPeriod_2, 2);
			this.frPeriod.AddControlArrayElement(frPeriod_1, 1);
			this.frPeriod.AddControlArrayElement(frPeriod_0, 0);
			this.t2kInterestDate.AddControlArrayElement(t2kInterestDate_3, 3);
			this.t2kInterestDate.AddControlArrayElement(t2kInterestDate_2, 2);
			this.t2kInterestDate.AddControlArrayElement(t2kInterestDate_1, 1);
			this.t2kInterestDate.AddControlArrayElement(t2kInterestDate_0, 0);
			this.t2kDueDate.AddControlArrayElement(t2kDueDate_3, 3);
			this.t2kDueDate.AddControlArrayElement(t2kDueDate_2, 2);
			this.t2kDueDate.AddControlArrayElement(t2kDueDate_1, 1);
			this.t2kDueDate.AddControlArrayElement(t2kDueDate_0, 0);
			this.lblDDate.AddControlArrayElement(lblDDate_3, 3);
			this.lblDDate.AddControlArrayElement(lblDDate_2, 2);
			this.lblDDate.AddControlArrayElement(lblDDate_1, 1);
			this.lblDDate.AddControlArrayElement(lblDDate_0, 0);
			this.lblIDate.AddControlArrayElement(lblIDate_3, 3);
			this.lblIDate.AddControlArrayElement(lblIDate_2, 2);
			this.lblIDate.AddControlArrayElement(lblIDate_1, 1);
			this.lblIDate.AddControlArrayElement(lblIDate_0, 0);
            this.txtInterestRate.AllowOnlyNumericInput(true);
			this.txtTaxRate.AllowOnlyNumericInput(true);
            this.txtTaxYear.AllowOnlyNumericInput();
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmAuditInfo InstancePtr
		{
			get
			{
				return (frmAuditInfo)Sys.GetInstance(typeof(frmAuditInfo));
			}
		}

		protected frmAuditInfo _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/09/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/16/2006              *
		// ********************************************************
		string strREPP = "";
		public int lngRKey;
		bool boolLoaded;
		int intType;
		int intDefaultYear;
		DateTime dtLienDate;
		DateTime dtBillDate;
		DateTime dtCommitmentDate;
		bool boolNew;
		int lngRateKey;
		// VBto upgrade warning: intPassDefaultYear As short	OnWrite(string)
		public void Init(short intIncomingType, short intPassDefaultYear = 0, DateTime? dtPassCommitmentDate = null/*DateTime.Now*/, DateTime? dtPassLienDate = null/*DateTime.Now*/, DateTime? dtPassBillDate = null/*DateTime.Now*/, int lngPassRateKey = 0)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				intType = intIncomingType;
				// 10 is a lien rec, 1 is a load back
				boolNew = true;
				switch (intType)
				{
					case 1:
						{
							// Load Back
							//FC:FINAL:RPU: #79: Made also the label visible or not
							fcLabel1.Visible = true;
							cmbRateRecType.Visible = true;
							cmbRateRecType.SelectedIndex = 0;
							if (!modStatusPayments.Statics.boolRE)
							{
                                //cmbRateRecType.SelectedIndex = 1;
                                //FC:FINAL:BSE #2065 item should not be available 
                                cmbRateRecType.Items.Remove("Lien");
							}
							break;
						}
					case 10:
						{
							// Lien Rec
							cmbRateRecType.Visible = false;
							cmbRateRecType.SelectedIndex = 1;
							//FC:FINAL:RPU: #79: Made also the label visible or not
							fcLabel1.Visible = false;
							break;
						}
					case 100:
						{
							// Edit Key
							lngRateKey = lngPassRateKey;
							LoadRateInformation();
							cmbRateRecType.Visible = true;
							this.Text = "Update Rate Record";
							this.HeaderText.Text = "Update Rate Record";
							boolNew = false;
							// this will allow the user to edit an existing rate key
							//FC:FINAL:RPU: #79: Made also the label visible or not
							fcLabel1.Visible = true;
							break;
						}
					default:
						{
							cmbRateRecType.Visible = true;
							cmbRateRecType.SelectedIndex = 0;
							//FC:FINAL:RPU: #79: Made also the label visible or not
							fcLabel1.Visible = true;
							break;
						}
				}
				//end switch
				// If intType = 1 Then
				// fraRateRecType.Visible = True
				// Else
				// fraRateRecType.Visible = False
				// End If
				if (boolNew)
				{
					if (!dtPassCommitmentDate.HasValue)
					{
						dtPassCommitmentDate = DateTime.Now;
					}
					if (!dtPassLienDate.HasValue)
					{
						dtPassLienDate = DateTime.Now;
					}
					if (!dtPassBillDate.HasValue)
					{
						dtPassBillDate = DateTime.Now;
					}
					intDefaultYear = intPassDefaultYear;
					dtLienDate = dtPassLienDate.Value;
					dtCommitmentDate = dtPassCommitmentDate.Value;
					dtBillDate = dtPassBillDate.Value;
				}
				this.Show(App.MainForm);
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Initializing");
			}
		}

		private void cmbPeriods_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			int x;
			if (cmbPeriods.Items.Count == 1)
			{
				for (x = 1; x <= 3; x++)
				{
					frPeriod[FCConvert.ToInt16(x)].Enabled = false;
					t2kInterestDate[x].Enabled = false;
					t2kDueDate[x].Enabled = false;
					lblIDate[FCConvert.ToInt16(x)].Enabled = false;
					lblDDate[FCConvert.ToInt16(x)].Enabled = false;
				}
			}
			else
			{
				for (x = 1; x <= cmbPeriods.Items.Count - 1; x++)
				{
					if (x <= cmbPeriods.SelectedIndex)
					{
						frPeriod[FCConvert.ToInt16(x)].Enabled = true;
						t2kInterestDate[x].Enabled = true;
						t2kDueDate[x].Enabled = true;
						lblIDate[FCConvert.ToInt16(x)].Enabled = true;
						lblDDate[FCConvert.ToInt16(x)].Enabled = true;
					}
					else
					{
						frPeriod[FCConvert.ToInt16(x)].Enabled = false;
						t2kInterestDate[x].Enabled = false;
						t2kDueDate[x].Enabled = false;
						lblIDate[FCConvert.ToInt16(x)].Enabled = false;
						lblDDate[FCConvert.ToInt16(x)].Enabled = false;
					}
				}
				// x
			}
		}

		private void frmAuditInfo_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				// If intType <> 100 Then
				// LoadCombo (intType = 0)
				// End If
				// set some of the default information
				if (intDefaultYear == 0)
				{
					txtTaxYear.Text = frmRateRecChoice.InstancePtr.cmbYear.Items[frmRateRecChoice.InstancePtr.cmbYear.SelectedIndex].ToString();
					// this will set the year to the year on the last form
				}
				else
				{
					txtTaxYear.Text = FCConvert.ToString(intDefaultYear);
				}
				if (intType == 10)
				{
					// this is coming from the transfer tax to lien screen
					lblBillDate.Text = "Lien Creation Date:";
					//FC:FINAL:RPU: #79 Change label location to avoid overlapping
					//lblBillDate.Location = new System.Drawing.Point(30, 208);
					if (dtLienDate.ToOADate() != 0)
					{
						t2kBilldate.Text = Strings.Format(dtLienDate, "MM/dd/yyyy");
						t2kDueDate[0].Text = Strings.Format(dtLienDate, "MM/dd/yyyy");
						t2kInterestDate[0].Text = Strings.Format(DateAndTime.DateAdd("D", 1, dtLienDate), "MM/dd/yyyy");
						t2kBilldate.Enabled = false;
					}
					else
					{
						t2kBilldate.Enabled = true;
					}
					if (dtCommitmentDate.ToOADate() != 0)
					{
						t2kCommitment.Text = Strings.Format(dtCommitmentDate, "MM/dd/yyyy");
					}
					else
					{
						// kk09202014 trocl-1155  Fill the Filing date with the lien date (which is actually the filing date from the lien control file
						t2kCommitment.Text = t2kBilldate.Text;
					}
					if (dtBillDate.ToOADate() != 0)
					{
						t2kBilldate.Text = Strings.Format(dtLienDate, "MM/dd/yyyy");
						t2kDueDate[0].Text = Strings.Format(dtLienDate, "MM/dd/yyyy");
					}
					if (dtLienDate.ToOADate() != 0)
					{
						t2kInterestDate[0].Text = Strings.Format(dtBillDate, "MM/dd/yyyy");
					}
					txtDescription.Text = txtTaxYear.Text + " Tax Liens";
					txtTaxRate.Text = "10";
					txtTaxRate.Visible = false;
					Label1.Visible = false;
					txtTaxRate.Enabled = false;
				}
				boolLoaded = true;
			}
		}

		private void frmAuditInfo_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = 0;
				mnuFileExit_Click();
			}
		}

		private void frmAuditInfo_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii == 13)
			{
				if (this.ActiveControl is TextBox || this.ActiveControl is ComboBox || this.ActiveControl is T2KDateBox)
				{
					Support.SendKeys("{TAB}", false);
				}
			}
			else if (KeyAscii == 127 || KeyAscii == 8)
			{
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void LoadCombo(bool boolRegular = false)
		{
			cmbPeriods.Clear();
			cmbPeriods.AddItem(FCConvert.ToString(1));
			// anything other than a lien rec
			if (((intType < 10 || intType == 100) || intType == 0) && boolRegular)
			{
				cmbPeriods.AddItem(FCConvert.ToString(2));
				cmbPeriods.AddItem(FCConvert.ToString(3));
				cmbPeriods.AddItem(FCConvert.ToString(4));
			}
			cmbPeriods.SelectedIndex = 0;
		}

		private void frmAuditInfo_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmAuditInfo.Icon	= "frmAuditInfo.frx":0000";
			//frmAuditInfo.ScaleWidth	= 9300;
			//frmAuditInfo.ScaleHeight	= 7560;
			//frmAuditInfo.LinkTopic	= "Form1";
			//frmAuditInfo.LockControls	= -1  'True;
			//Font.Size	= "9.75";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//t2kBilldate.BorderStyle	= 1;
			//t2kBilldate.Appearance	= 1;
			//t2kBilldate.MaxLength	= 10;
			//t2kInterestDate_3.BorderStyle	= 1;
			//t2kInterestDate_3.Appearance	= 1;
			//t2kInterestDate_3.MaxLength	= 10;
			//t2kDueDate_3.BorderStyle	= 1;
			//t2kDueDate_3.Appearance	= 1;
			//t2kDueDate_3.MaxLength	= 10;
			//t2kInterestDate_2.BorderStyle	= 1;
			//t2kInterestDate_2.Appearance	= 1;
			//t2kInterestDate_2.MaxLength	= 10;
			//t2kDueDate_2.BorderStyle	= 1;
			//t2kDueDate_2.Appearance	= 1;
			//t2kDueDate_2.MaxLength	= 10;
			//t2kInterestDate_1.BorderStyle	= 1;
			//t2kInterestDate_1.Appearance	= 1;
			//t2kInterestDate_1.MaxLength	= 10;
			//t2kDueDate_1.BorderStyle	= 1;
			//t2kDueDate_1.Appearance	= 1;
			//t2kDueDate_1.MaxLength	= 10;
			//t2kInterestDate_0.BorderStyle	= 1;
			//t2kInterestDate_0.Appearance	= 1;
			//t2kInterestDate_0.MaxLength	= 10;
			//t2kDueDate_0.BorderStyle	= 1;
			//t2kDueDate_0.Appearance	= 1;
			//t2kDueDate_0.MaxLength	= 10;
			//txtTaxYear.MaxLength	= 4;
			//DataFormat.Type	= 0;
			//DataFormat.Format	= "0.000";
			//DataFormat.HaveTrueFalseNull	= 0;
			//DataFormat.FirstDayOfWeek	= 0;
			//DataFormat.FirstWeekOfYear	= 0;
			//DataFormat.LCID	= 1033;
			//DataFormat.SubFormatType	= 0;
			//t2kCommitment.BorderStyle	= 1;
			//t2kCommitment.Appearance	= 1;
			//t2kCommitment.MaxLength	= 10;
			//vsElasticLight1.OleObjectBlob	= "frmAuditInfo.frx":058A";
			//End Unmaped Properties
			modGlobalFunctions.SetTRIOColors(this);
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			//FC:FINAL:SBE - #70 - move code from FormClosing to FormClosed. If other forms are displayed during form closing, the Visible property is wrong
			//switch (intType)
			//{
			//	case 100:
			//		{
			//			frmRateRecChoice.InstancePtr.Hide();
			//			break;
			//		}
			//	case 10:
			//		{
			//			frmRateRecChoice.InstancePtr.intRateType = 6;
			//			frmRateRecChoice.InstancePtr.Show(App.MainForm);
			//			break;
			//		}
			//	default:
			//		{
			//			frmRateRecChoice.InstancePtr.Show(App.MainForm);
			//			break;
			//		}
			//}
			////end switch
			//boolLoaded = false;
		}

		//FC:FINAL:SBE - #70 - move code from FormClosing to FormClosed. If other forms are displayed during form closing, the Visible property is wrong
		protected override void OnFormClosed(FormClosedEventArgs e)
		{
			base.OnFormClosed(e);
			switch (intType)
			{
				case 100:
					{
						frmRateRecChoice.InstancePtr.Unload();
						break;
					}
				case 10:
					{
						frmRateRecChoice.InstancePtr.intRateType = 6;
						frmRateRecChoice.InstancePtr.Show(App.MainForm);
						break;
					}
				default:
					{
						frmRateRecChoice.InstancePtr.Show(App.MainForm);
						break;
					}
			}
			//end switch
			boolLoaded = false;
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuFileExit_Click()
		{
			//mnuFileExit_Click(mnuFileExit, new System.EventArgs());
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			if (CheckEntries())
			{
				SaveRateRec();
			}
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			if (CheckEntries())
			{
				SaveRateRec();
			}
		}

		private void optRateRecType_Click(int Index, object sender, System.EventArgs e)
		{
			// If intType <> 100 Then
			switch (cmbRateRecType.Text)
			{
				case "Regular":
				case "Supplemental":
					{
						// set as a regular rate record
						LoadCombo(true);
						lblBillDate.Text = "Bill Date:";
						lblCommitmentDate.Text = "Commitment Date:";
						//FC: FINAL:RPU: #79 use the initial location for label
						//this.lblBillDate.Location = new System.Drawing.Point(30, 208);
						break;
					}
				case "Lien":
					{
						// set as a lien record
						LoadCombo(false);
						lblBillDate.Text = "Lien Creation Date:";
						lblCommitmentDate.Text = "Lien Filing Date:";
						//FC: FINAL:RPU: #79 change the location for label due to length of text to avoid overlapping
						//this.lblBillDate.Location = new System.Drawing.Point(20, 208);
						break;
					}
			}
			//end switch
			// End If
		}

		private void optRateRecType_Click(object sender, System.EventArgs e)
		{
			int index = cmbRateRecType.SelectedIndex;
			optRateRecType_Click(index, sender, e);
		}

		private void txtInterestRate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			int lngDecPlace = 0;
			if ((KeyAscii == 8) || (KeyAscii >= 48 && KeyAscii <= 57))
			{
				// do nothing
			}
			else if (KeyAscii == 46)
			{
				// decimal point
				lngDecPlace = Strings.InStr(1, txtInterestRate.Text, ".");
				if (lngDecPlace != 0)
				{
					if (txtInterestRate.SelectionStart < lngDecPlace && txtInterestRate.SelectionLength + txtInterestRate.SelectionStart >= lngDecPlace)
					{
						// if the decimal place is being highlighted and will be written over, then allow it
					}
					else
					{
						// if there is already a decimal point then stop others
						KeyAscii = 0;
					}
				}
			}
			else
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtTaxRate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			int lngDecPlace = 0;
			if ((KeyAscii == 8) || (KeyAscii >= 48 && KeyAscii <= 57))
			{
				// do nothing
			}
			else if (KeyAscii == 46)
			{
				// decimal point
				lngDecPlace = Strings.InStr(1, txtTaxRate.Text, ".");
				if (lngDecPlace != 0)
				{
					if (txtTaxRate.SelectionStart < lngDecPlace && txtTaxRate.SelectionLength + txtTaxRate.SelectionStart >= lngDecPlace)
					{
						// if the decimal place is being highlighted and will be written over, then allow it
					}
					else
					{
						// if there is already a decimal point then stop others
						KeyAscii = 0;
					}
				}
			}
			else
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtTaxRate_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtTaxRate.Text) > 0)
			{
				if (FCConvert.ToDouble(txtTaxRate.Text) > 100)
				{
					FCMessageBox.Show("Please enter a tax rate that is less than 100.", MsgBoxStyle.Exclamation, "Invalid Rate");
					txtTaxRate.Text = "10.00";
				}
			}
		}

		private void txtTaxYear_Enter(object sender, System.EventArgs e)
		{
			txtTaxYear.SelectionStart = 0;
			txtTaxYear.SelectionLength = txtTaxYear.Text.Length;
		}

		private void SaveRateRec()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will save the new Rate Record
				clsDRWrapper rsRate = new clsDRWrapper();
				string strNumofBills = "";
				if (intType == 100)
				{
					rsRate.OpenRecordset("SELECT Count(ID) AS Num FROM BillingMaster WHERE RateKey = " + FCConvert.ToString(lngRateKey), modExtraModules.strCLDatabase);
					if (!rsRate.EndOfFile())
					{
						// TODO Get_Fields: Field [Num] not found!! (maybe it is an alias?)
						if (FCConvert.ToInt32(rsRate.Get_Fields("Num")) > 0)
						{
							// TODO Get_Fields: Field [Num] not found!! (maybe it is an alias?)
							strNumofBills = FCConvert.ToString(rsRate.Get_Fields("Num")) + " bills.";
						}
						else
						{
							strNumofBills = "one bill.";
						}
					}
					else
					{
						strNumofBills = "at least one bill.";
					}
					//FC:FINAL:DDU prevent FCMessageBox to show up twice
					DialogResult dialogResult = FCMessageBox.Show("This will affect " + strNumofBills + "  Are you sure that you would like to change this rate record?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Change Rate Information");
					if (dialogResult == DialogResult.Yes)
					{
						// keep rocking
						modGlobalFunctions.AddCYAEntry_26("CL", "Changing Rate Record", "Rate Record = " + FCConvert.ToString(lngRateKey));
					}
					else if ((dialogResult == DialogResult.No) || (dialogResult == DialogResult.Cancel))
					{
						FCMessageBox.Show("Save cancelled.", MsgBoxStyle.Exclamation, "Cancel");
						return;
					}
					rsRate.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(lngRateKey), modExtraModules.strCLDatabase);
					if (rsRate.EndOfFile())
					{
						FCMessageBox.Show("Cannot find Rate Record #" + FCConvert.ToString(lngRateKey) + ".", MsgBoxStyle.Critical, "Missing Rate Record");
					}
					rsRate.Edit();
				}
				else
				{
					rsRate.OpenRecordset("SELECT * FROM RateRec WHERE ID = 0", modExtraModules.strCLDatabase);
					rsRate.AddNew();
				}
				rsRate.Set_Fields("Year", FCConvert.ToString(Conversion.Val(txtTaxYear.Text)));
				rsRate.Set_Fields("TaxRate", FCConvert.ToDouble(txtTaxRate.Text) / 1000);
				switch (intType)
				{
					case 100:
                    {
                        switch (cmbRateRecType.SelectedIndex)
                        {
                            // editing a rate rec
                            case 1:
                                rsRate.Set_Fields("RateType", "L");

                                break;
                            case 0:
                                rsRate.Set_Fields("RateType", "R");

                                break;
                            default:
                                rsRate.Set_Fields("RateType", "S");

                                break;
                        }

                        break;
                    }
					case 10:
						{
							// lien rate rec for transfer to lien
							rsRate.Set_Fields("RateType", "L");
							break;
						}
					case 1:
                    {
                        switch (cmbRateRecType.SelectedIndex)
                        {
                            // this is when a load back occurs
                            case 1:
                                rsRate.Set_Fields("RateType", "L");

                                break;
                            case 0:
                                rsRate.Set_Fields("RateType", "R");

                                break;
                            default:
                                rsRate.Set_Fields("RateType", "S");

                                break;
                        }

                        break;
                    }
					default:
						{
							rsRate.Set_Fields("RateType", "R");
							break;
						}
				}
				//end switch
				rsRate.Set_Fields("CommitmentDate", DateAndTime.DateValue(t2kCommitment.Text));
				rsRate.Set_Fields("BillingDate", DateAndTime.DateValue(t2kBilldate.Text));
                
                var period = Conversion.Val(cmbPeriods.Items[cmbPeriods.SelectedIndex].ToString());
                rsRate.Set_Fields("NumberofPeriods", FCConvert.ToString(period));

				switch (period)
                {
                    case 1:
                        rsRate.Set_Fields("InterestStartDate1", DateAndTime.DateValue(t2kInterestDate[0].Text));
                        rsRate.Set_Fields("DueDate1", DateAndTime.DateValue(t2kDueDate[0].Text));
                        rsRate.Set_Fields("InterestStartDate2", FCConvert.ToDateTime(0));
                        rsRate.Set_Fields("DueDate2", FCConvert.ToDateTime(0));
                        rsRate.Set_Fields("InterestStartDate3", FCConvert.ToDateTime(0));
                        rsRate.Set_Fields("DueDate3", FCConvert.ToDateTime(0));
                        rsRate.Set_Fields("InterestStartDate4", FCConvert.ToDateTime(0));
                        rsRate.Set_Fields("DueDate4", FCConvert.ToDateTime(0));

                        break;
                    case 2:
                        rsRate.Set_Fields("InterestStartDate1", DateAndTime.DateValue(t2kInterestDate[0].Text));
                        rsRate.Set_Fields("DueDate1", DateAndTime.DateValue(t2kDueDate[0].Text));
                        rsRate.Set_Fields("InterestStartDate2", DateAndTime.DateValue(t2kInterestDate[1].Text));
                        rsRate.Set_Fields("DueDate2", DateAndTime.DateValue(t2kDueDate[1].Text));
                        rsRate.Set_Fields("InterestStartDate3", FCConvert.ToDateTime(0));
                        rsRate.Set_Fields("DueDate3", FCConvert.ToDateTime(0));
                        rsRate.Set_Fields("InterestStartDate4", FCConvert.ToDateTime(0));
                        rsRate.Set_Fields("DueDate4", FCConvert.ToDateTime(0));

                        break;
                    case 3:
                        rsRate.Set_Fields("InterestStartDate1", DateAndTime.DateValue(t2kInterestDate[0].Text));
                        rsRate.Set_Fields("DueDate1", DateAndTime.DateValue(t2kDueDate[0].Text));
                        rsRate.Set_Fields("InterestStartDate2", DateAndTime.DateValue(t2kInterestDate[1].Text));
                        rsRate.Set_Fields("DueDate2", DateAndTime.DateValue(t2kDueDate[1].Text));
                        rsRate.Set_Fields("InterestStartDate3", DateAndTime.DateValue(t2kInterestDate[2].Text));
                        rsRate.Set_Fields("DueDate3", DateAndTime.DateValue(t2kDueDate[2].Text));
                        rsRate.Set_Fields("InterestStartDate4", FCConvert.ToDateTime(0));
                        rsRate.Set_Fields("DueDate4", FCConvert.ToDateTime(0));

                        break;
                    case 4:
                        rsRate.Set_Fields("InterestStartDate1", DateAndTime.DateValue(t2kInterestDate[0].Text));
                        rsRate.Set_Fields("DueDate1", DateAndTime.DateValue(t2kDueDate[0].Text));
                        rsRate.Set_Fields("InterestStartDate2", DateAndTime.DateValue(t2kInterestDate[1].Text));
                        rsRate.Set_Fields("DueDate2", DateAndTime.DateValue(t2kDueDate[1].Text));
                        rsRate.Set_Fields("InterestStartDate3", DateAndTime.DateValue(t2kInterestDate[2].Text));
                        rsRate.Set_Fields("DueDate3", DateAndTime.DateValue(t2kDueDate[2].Text));
                        rsRate.Set_Fields("InterestStartDate4", DateAndTime.DateValue(t2kInterestDate[3].Text));
                        rsRate.Set_Fields("DueDate4", DateAndTime.DateValue(t2kDueDate[3].Text));

                        break;
                }
				if (Conversion.Val(txtInterestRate.Text) == 0)
				{
					rsRate.Set_Fields("InterestRate", 0);
				}
				else
				{
					rsRate.Set_Fields("InterestRate", FCConvert.ToDouble(txtInterestRate.Text) / 100);
				}
				rsRate.Set_Fields("Description", Strings.Trim(txtDescription.Text));
				rsRate.Update();
				if (intType != 100)
				{
					// when this saves correctly, unload the frmRateRecChoice form and
					// reload it so that the new rate rec is included in the year
					App.DoEvents();
					frmRateRecChoice.InstancePtr.Unload();
					App.DoEvents();
					frmRateRecChoice.InstancePtr.intRateType = intType;
					// frmRateRecChoice.Show
				}
				switch (intType)
				{
				// return nothing to the rate rec choice screen
					case 10:
						{
							// lien record
							frmRateRecChoice.InstancePtr.intRateType = 3;
							// 100
							break;
						}
					case 1:
						{
							// load back
							frmRateRecChoice.InstancePtr.intRateType = 100;
							break;
						}
				}
				//end switch
				frmRateRecChoice.InstancePtr.Unload();
				Close();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("ERROR #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Save Rate Record");
			}
		}

		private bool CheckEntries()
		{
			bool CheckEntries = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strTemp;
				int x;
				// VBto upgrade warning: strIDate As string	OnRead(DateTime)
				string[] strIDate = new string[4 + 1];
				// VBto upgrade warning: strDDate As string	OnRead(DateTime)
				string[] strDDate = new string[4 + 1];
				// VBto upgrade warning: dtTemp1 As DateTime	OnWrite(string)
				DateTime dtTemp1;
				// VBto upgrade warning: dtTemp2 As DateTime	OnWrite(string)
				DateTime dtTemp2;
				DialogResult intAnswer = 0;
				CheckEntries = false;
				// MAL@20071211: Change to ask for verification before continuing if year is 15 years from today in either direction
				// Tracker Reference: 11638
				// If (Val(txtTaxYear.Text) < 1985) Or (Val(txtTaxYear.Text > 2050)) Then
				// MsgBox "The tax year is not valid, cannot save." & ".", MsgBoxStyle.Exclamation, "Invalid Data"
				// Exit Function
				// End If
				if (Conversion.Val(txtTaxYear.Text) <= (DateTime.Today.Year - 15) || Conversion.Val(txtTaxYear.Text) >= (DateTime.Today.Year + 15))
                {
                    intAnswer = FCMessageBox.Show("The Year entered is 15 years greater or less than the current year." + "\r\n" + "Are you sure you want to continue?", MsgBoxStyle.YesNo | MsgBoxStyle.Question, "Year Validation");

                    if (intAnswer != DialogResult.Yes)
                    {
                        return CheckEntries;
                    }
                }
				strTemp = txtInterestRate.Text;
				if (Conversion.Val(strTemp) < 0 || Strings.Trim(strTemp) == "" || Conversion.Val(strTemp) > 100)
				{
					FCMessageBox.Show("The Interest Rate is not valid. Save Failed" + ".", MsgBoxStyle.Exclamation, "Invalid Data");
					return CheckEntries;
				}
				strTemp = t2kBilldate.Text;
				if (!Information.IsDate(strTemp))
				{
					FCMessageBox.Show("The Billing Date is not a valid date. Save Failed" + ".", MsgBoxStyle.Exclamation, "Invalid Data");
					return CheckEntries;
				}
				strTemp = t2kCommitment.Text;
				if (!Information.IsDate(strTemp))
				{
					FCMessageBox.Show("The Commitment Date is not a valid date. Save Failed" + ".", MsgBoxStyle.Exclamation, "Invalid Data");
					return CheckEntries;
				}
				if (Conversion.Val(txtTaxRate.Text) <= 0)
				{
					FCMessageBox.Show("The Tax Rate is invalid. Save Failed" + ".", MsgBoxStyle.Exclamation, "Invalid Data");
					return CheckEntries;
				}
				for (x = 0; x <= cmbPeriods.SelectedIndex; x++)
				{
					strIDate[x] = t2kInterestDate[x].Text;
					strDDate[x] = t2kDueDate[x].Text;
				}
				// x
				if (!Information.IsDate(strIDate[0]))
				{
					FCMessageBox.Show("Invalid interest date for Period 1" + ".", MsgBoxStyle.Exclamation, "Invalid Data");
					return CheckEntries;
				}
				if (!Information.IsDate(strDDate[0]))
				{
					FCMessageBox.Show("Invalid Due Date for Period 1" + ".", MsgBoxStyle.Exclamation, "Invalid Data");
					return CheckEntries;
				}
				dtTemp1 = FCConvert.ToDateTime(strIDate[0]);
				dtTemp2 = FCConvert.ToDateTime(strDDate[0]);
				if (!(dtTemp1.ToOADate() > dtTemp2.ToOADate()))
				{
					FCMessageBox.Show("The interest date for Period 1 must be greater than the due date.");
					return CheckEntries;
				}
				// kk09182014 trocl-1155  Add check for Due Date before Lien/Bill Date.  Only need to check Period 1 because all other Periods must be greater.
				dtTemp1 = FCConvert.ToDateTime(t2kBilldate.Text);
				if (dtTemp2.ToOADate() < dtTemp1.ToOADate())
				{
					if (dtLienDate.ToOADate() > 0)
					{
						strTemp = "The due date is before the lien date." + "\r\n" + "Are you sure you want to continue?";
					}
					else
					{
						strTemp = "The due date for Period 1 is before the bill date." + "\r\n" + "Are you sure you want to continue?";
					}
					intAnswer = FCMessageBox.Show(strTemp, MsgBoxStyle.YesNo | MsgBoxStyle.Question, "Due Date Validation");

                    if (intAnswer != DialogResult.Yes)
                    {
                        return CheckEntries;
                    }
                }
				for (x = 1; x <= cmbPeriods.SelectedIndex; x++)
				{
					if (!Information.IsDate(strIDate[x]))
					{
						FCMessageBox.Show("Invalid interest date for Period " + FCConvert.ToString(x + 1) + ".", MsgBoxStyle.Exclamation, "Invalid Data");
						return CheckEntries;
					}
					if (!Information.IsDate(strDDate[x]))
					{
						FCMessageBox.Show("Invalid due date for Period " + FCConvert.ToString(x + 1) + ".", MsgBoxStyle.Exclamation, "Invalid Data");
						return CheckEntries;
					}
					dtTemp1 = FCConvert.ToDateTime(strIDate[x]);
					dtTemp2 = FCConvert.ToDateTime(strDDate[x]);
					if (!(dtTemp1.ToOADate() > dtTemp2.ToOADate()))
					{
						FCMessageBox.Show("The interest date for Period " + FCConvert.ToString(x + 1) + " must be greater than the due date.", MsgBoxStyle.Exclamation, "Invalid Data");
						return CheckEntries;
					}
					dtTemp1 = FCConvert.ToDateTime(strDDate[x - 1]);
					if (!(dtTemp2.ToOADate() > dtTemp1.ToOADate()))
					{
						FCMessageBox.Show("The due date for Period " + FCConvert.ToString(x + 1) + " must be greater than the due date for Period " + FCConvert.ToString(x) + ".", MsgBoxStyle.Exclamation, "Invalid Data");
						return CheckEntries;
					}
				}
				// x
				CheckEntries = true;
				return CheckEntries;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "." + "\r\n" + "Occured in Check Entries.", MsgBoxStyle.Critical, "Validation Error");
			}
			return CheckEntries;
		}

		private bool LoadRateInformation()
		{
			bool LoadRateInformation = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsLoad = new clsDRWrapper();
				int intCT;
				rsLoad.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(lngRateKey));
				if (!rsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
					txtTaxYear.Text = FCConvert.ToString(rsLoad.Get_Fields("Year"));
					txtTaxRate.Text = FCConvert.ToString(rsLoad.Get_Fields_Double("TaxRate") * 1000);
                    var rateType = FCConvert.ToString(rsLoad.Get_Fields_String("RateType"));

                    switch (rateType)
                    {
                        case "R":
                            cmbRateRecType.SelectedIndex = 0;
                            LoadCombo(true);

                            break;
                        case "L":
                            cmbRateRecType.SelectedIndex = 1;
                            LoadCombo(false);

                            break;
                        default:
                            cmbRateRecType.SelectedIndex = 2;

                            break;
                    }
					for (intCT = 0; intCT <= cmbPeriods.Items.Count - 1; intCT++)
					{
						if (Conversion.Val(cmbPeriods.Items[intCT].ToString()) == Conversion.Val(rsLoad.Get_Fields_Int16("NumberofPeriods")))
						{
							cmbPeriods.SelectedIndex = intCT;
						}
					}
					t2kCommitment.Text = Strings.Format(rsLoad.Get_Fields_DateTime("CommitmentDate"), "MM/dd/yyyy");
					t2kBilldate.Text = Strings.Format(rsLoad.Get_Fields_DateTime("BillingDate"), "MM/dd/yyyy");
                    var periods = Conversion.Val(cmbPeriods.Items[cmbPeriods.SelectedIndex].ToString());

                    switch (periods)
                    {
                        case 1:
                            t2kInterestDate[0].Text = Strings.Format(rsLoad.Get_Fields_DateTime("InterestStartDate1"), "MM/dd/yyyy");
                            t2kDueDate[0].Text = Strings.Format(rsLoad.Get_Fields_DateTime("DueDate1"), "MM/dd/yyyy");

                            break;
                        case 2:
                            t2kInterestDate[0].Text = Strings.Format(rsLoad.Get_Fields_DateTime("InterestStartDate1"), "MM/dd/yyyy");
                            t2kDueDate[0].Text = Strings.Format(rsLoad.Get_Fields_DateTime("DueDate1"), "MM/dd/yyyy");
                            t2kInterestDate[1].Text = Strings.Format(rsLoad.Get_Fields_DateTime("InterestStartDate2"), "MM/dd/yyyy");
                            t2kDueDate[1].Text = Strings.Format(rsLoad.Get_Fields_DateTime("DueDate2"), "MM/dd/yyyy");

                            break;
                        case 3:
                            t2kInterestDate[0].Text = Strings.Format(rsLoad.Get_Fields_DateTime("InterestStartDate1"), "MM/dd/yyyy");
                            t2kDueDate[0].Text = Strings.Format(rsLoad.Get_Fields_DateTime("DueDate1"), "MM/dd/yyyy");
                            t2kInterestDate[1].Text = Strings.Format(rsLoad.Get_Fields_DateTime("InterestStartDate2"), "MM/dd/yyyy");
                            t2kDueDate[1].Text = Strings.Format(rsLoad.Get_Fields_DateTime("DueDate2"), "MM/dd/yyyy");
                            t2kInterestDate[2].Text = Strings.Format(rsLoad.Get_Fields_DateTime("InterestStartDate3"), "MM/dd/yyyy");
                            t2kDueDate[2].Text = Strings.Format(rsLoad.Get_Fields_DateTime("DueDate3"), "MM/dd/yyyy");

                            break;
                        case 4:
                            t2kInterestDate[0].Text = Strings.Format(rsLoad.Get_Fields_DateTime("InterestStartDate1"), "MM/dd/yyyy");
                            t2kDueDate[0].Text = Strings.Format(rsLoad.Get_Fields_DateTime("DueDate1"), "MM/dd/yyyy");
                            t2kInterestDate[1].Text = Strings.Format(rsLoad.Get_Fields_DateTime("InterestStartDate2"), "MM/dd/yyyy");
                            t2kDueDate[1].Text = Strings.Format(rsLoad.Get_Fields_DateTime("DueDate2"), "MM/dd/yyyy");
                            t2kInterestDate[2].Text = Strings.Format(rsLoad.Get_Fields_DateTime("InterestStartDate3"), "MM/dd/yyyy");
                            t2kDueDate[2].Text = Strings.Format(rsLoad.Get_Fields_DateTime("DueDate3"), "MM/dd/yyyy");
                            t2kInterestDate[3].Text = Strings.Format(rsLoad.Get_Fields_DateTime("InterestStartDate4"), "MM/dd/yyyy");
                            t2kDueDate[3].Text = Strings.Format(rsLoad.Get_Fields_DateTime("DueDate4"), "MM/dd/yyyy");

                            break;
                    }
					txtInterestRate.Text = FCConvert.ToString(rsLoad.Get_Fields_Double("InterestRate") * 100);
					txtDescription.Text = FCConvert.ToString(rsLoad.Get_Fields_String("Description"));
					LoadRateInformation = true;
				}
				else
				{
					FCMessageBox.Show("Cannot Load Rate Record #" + FCConvert.ToString(lngRateKey) + ".", MsgBoxStyle.Exclamation, "Missing Rate Record");
					LoadRateInformation = false;
					return LoadRateInformation;
				}
				return LoadRateInformation;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				LoadRateInformation = false;
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "." + "\r\n" + "Occured in Check Entries.", MsgBoxStyle.Critical, "Load Record Error");
			}
			return LoadRateInformation;
		}

		private void txtTaxYear_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if ((KeyAscii == 8) || (KeyAscii >= 48 && KeyAscii <= 57))
			{
				// do nothing
			}
			else
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			this.mnuFileSave_Click(sender, e);
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFileSave_Click(sender, e);
		}
	}
}
