﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmRateRecChoice.
	/// </summary>
	partial class frmRateRecChoice : BaseForm
	{
		public fecherFoundation.FCComboBox cmbReminderLien;
		public fecherFoundation.FCComboBox cmbPeriod;
		public fecherFoundation.FCComboBox cmbRePrint;
		public fecherFoundation.FCComboBox cmbPrint;
		public fecherFoundation.FCComboBox cmbRange;
		public fecherFoundation.FCComboBox cmbLien;
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> chkPrint;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtRange;
		public fecherFoundation.FCFrame fraRateInfo;
		public fecherFoundation.FCButton cmdRIClose;
		public fecherFoundation.FCGrid vsRateInfo;
		public fecherFoundation.FCFrame fraPrintOptions;
		public fecherFoundation.FCComboBox cmbExtraLines;
		public fecherFoundation.FCCheckBox chkPrint_3;
		public fecherFoundation.FCCheckBox chkPrint_2;
		public fecherFoundation.FCCheckBox chkPrint_1;
		public fecherFoundation.FCCheckBox chkPrint_0;
		public fecherFoundation.FCLabel lblExtraLines;
		public fecherFoundation.FCFrame fraPreLienEdit;
		public fecherFoundation.FCComboBox cmbReportDetail;
		public T2KDateBox txtMailDate;
		public fecherFoundation.FCLabel lblReportLen;
		public fecherFoundation.FCLabel lblMailDate;
		public fecherFoundation.FCFrame fraRange;
		//public Wisej.Web.VScrollBar vsbMinAmount;
		public fecherFoundation.FCTextBox txtMinimumAmount;
		public fecherFoundation.FCTextBox txtRange_1;
		public fecherFoundation.FCTextBox txtRange_0;
		public fecherFoundation.FCLabel lblMinAmount;
		public fecherFoundation.FCGrid vsRate;
		public fecherFoundation.FCFrame fraQuestions;
		public fecherFoundation.FCComboBox cmbYear;
		public fecherFoundation.FCLabel lblEligible;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Wisej.Web.JavaScript.ClientEvent clientEvent1 = new Wisej.Web.JavaScript.ClientEvent();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRateRecChoice));
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
            this.javaScriptTxtRange = new Wisej.Web.JavaScript(this.components);
            this.txtRange_1 = new fecherFoundation.FCTextBox();
            this.txtRange_0 = new fecherFoundation.FCTextBox();
            this.cmbReminderLien = new fecherFoundation.FCComboBox();
            this.cmbPeriod = new fecherFoundation.FCComboBox();
            this.cmbRePrint = new fecherFoundation.FCComboBox();
            this.cmbPrint = new fecherFoundation.FCComboBox();
            this.cmbRange = new fecherFoundation.FCComboBox();
            this.cmbLien = new fecherFoundation.FCComboBox();
            this.fraRateInfo = new fecherFoundation.FCFrame();
            this.cmdRIClose = new fecherFoundation.FCButton();
            this.vsRateInfo = new fecherFoundation.FCGrid();
            this.fraPrintOptions = new fecherFoundation.FCFrame();
            this.cmbExtraLines = new fecherFoundation.FCComboBox();
            this.chkPrint_3 = new fecherFoundation.FCCheckBox();
            this.chkPrint_2 = new fecherFoundation.FCCheckBox();
            this.chkPrint_1 = new fecherFoundation.FCCheckBox();
            this.chkPrint_0 = new fecherFoundation.FCCheckBox();
            this.lblExtraLines = new fecherFoundation.FCLabel();
            this.fraPreLienEdit = new fecherFoundation.FCFrame();
            this.cmbReportDetail = new fecherFoundation.FCComboBox();
            this.txtMailDate = new Global.T2KDateBox();
            this.lblReportLen = new fecherFoundation.FCLabel();
            this.lblMailDate = new fecherFoundation.FCLabel();
            this.fraRange = new fecherFoundation.FCFrame();
            this.txtMinimumAmount = new fecherFoundation.FCTextBox();
            this.lblMinAmount = new fecherFoundation.FCLabel();
            this.lblRange = new fecherFoundation.FCLabel();
            this.vsRate = new fecherFoundation.FCGrid();
            this.fraQuestions = new fecherFoundation.FCFrame();
            this.lblYear = new fecherFoundation.FCLabel();
            this.cmbYear = new fecherFoundation.FCComboBox();
            this.lblEligible = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.lblReminderLien = new fecherFoundation.FCLabel();
            this.lblPeriod = new fecherFoundation.FCLabel();
            this.lblRePrint = new fecherFoundation.FCLabel();
            this.lblPrintType = new fecherFoundation.FCLabel();
            this.btnProcess = new fecherFoundation.FCButton();
            this.cmdFileAddRateKey = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraRateInfo)).BeginInit();
            this.fraRateInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRIClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsRateInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraPrintOptions)).BeginInit();
            this.fraPrintOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrint_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrint_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrint_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrint_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraPreLienEdit)).BeginInit();
            this.fraPreLienEdit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMailDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraRange)).BeginInit();
            this.fraRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraQuestions)).BeginInit();
            this.fraQuestions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileAddRateKey)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 695);
            this.BottomPanel.Size = new System.Drawing.Size(1044, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraRange);
            this.ClientArea.Controls.Add(this.fraRateInfo);
            this.ClientArea.Controls.Add(this.fraPrintOptions);
            this.ClientArea.Controls.Add(this.vsRate);
            this.ClientArea.Controls.Add(this.lblPrintType);
            this.ClientArea.Controls.Add(this.lblRePrint);
            this.ClientArea.Controls.Add(this.cmbRePrint);
            this.ClientArea.Controls.Add(this.lblPeriod);
            this.ClientArea.Controls.Add(this.lblReminderLien);
            this.ClientArea.Controls.Add(this.cmbReminderLien);
            this.ClientArea.Controls.Add(this.cmbPeriod);
            this.ClientArea.Controls.Add(this.cmbPrint);
            this.ClientArea.Controls.Add(this.fraPreLienEdit);
            this.ClientArea.Controls.Add(this.fraQuestions);
           // this.ClientArea.Dock = Wisej.Web.DockStyle.None;
            this.ClientArea.Size = new System.Drawing.Size(1064, 775);
            this.ClientArea.Controls.SetChildIndex(this.fraQuestions, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraPreLienEdit, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbPrint, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbPeriod, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbReminderLien, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblReminderLien, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblPeriod, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbRePrint, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblRePrint, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblPrintType, 0);
            this.ClientArea.Controls.SetChildIndex(this.vsRate, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraPrintOptions, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraRateInfo, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraRange, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdFileAddRateKey);
            this.TopPanel.Size = new System.Drawing.Size(1064, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileAddRateKey, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(26, 26);
            this.HeaderText.Size = new System.Drawing.Size(211, 28);
            this.HeaderText.Text = "Select Rate Record";
            // 
            // txtRange_1
            // 
            this.txtRange_1.BackColor = System.Drawing.SystemColors.Window;
            this.txtRange_1.Enabled = false;
            clientEvent1.Event = "keypress";
            clientEvent1.JavaScript = resources.GetString("clientEvent1.JavaScript");
            this.javaScriptTxtRange.GetJavaScriptEvents(this.txtRange_1).Add(clientEvent1);
            this.txtRange_1.Location = new System.Drawing.Point(206, 90);
            this.txtRange_1.Name = "txtRange_1";
            this.txtRange_1.Size = new System.Drawing.Size(166, 40);
            this.txtRange_1.TabIndex = 2;
            this.txtRange_1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtRange_KeyPress);
            // 
            // txtRange_0
            // 
            this.txtRange_0.BackColor = System.Drawing.SystemColors.Window;
            this.txtRange_0.Enabled = false;
            this.javaScriptTxtRange.GetJavaScriptEvents(this.txtRange_0).Add(clientEvent1);
            this.txtRange_0.Location = new System.Drawing.Point(20, 90);
            this.txtRange_0.Name = "txtRange_0";
            this.txtRange_0.Size = new System.Drawing.Size(166, 40);
            this.txtRange_0.TabIndex = 1;
            this.txtRange_0.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtRange_KeyPress);
            // 
            // cmbReminderLien
            // 
            this.cmbReminderLien.Items.AddRange(new object[] {
            "Regular",
            "Lien"});
            this.cmbReminderLien.Location = new System.Drawing.Point(816, 76);
            this.cmbReminderLien.Name = "cmbReminderLien";
            this.cmbReminderLien.Size = new System.Drawing.Size(208, 40);
            this.cmbReminderLien.TabIndex = 9;
            this.cmbReminderLien.Text = "Lien";
            this.cmbReminderLien.Visible = false;
            this.cmbReminderLien.SelectedIndexChanged += new System.EventHandler(this.optReminderLien_Click);
            // 
            // cmbPeriod
            // 
            this.cmbPeriod.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
            this.cmbPeriod.Location = new System.Drawing.Point(433, 16);
            this.cmbPeriod.Name = "cmbPeriod";
            this.cmbPeriod.Size = new System.Drawing.Size(208, 40);
            this.cmbPeriod.TabIndex = 5;
            this.cmbPeriod.Text = "1";
            this.cmbPeriod.Visible = false;
            // 
            // cmbRePrint
            // 
            this.cmbRePrint.Items.AddRange(new object[] {
            "Initial",
            "Reprint"});
            this.cmbRePrint.Location = new System.Drawing.Point(433, 76);
            this.cmbRePrint.Name = "cmbRePrint";
            this.cmbRePrint.Size = new System.Drawing.Size(208, 40);
            this.cmbRePrint.TabIndex = 7;
            this.cmbRePrint.Text = "Initial";
            this.cmbRePrint.Visible = false;
            // 
            // cmbPrint
            // 
            this.cmbPrint.Items.AddRange(new object[] {
            "Labels",
            "Certified Mail Forms"});
            this.cmbPrint.Location = new System.Drawing.Point(172, 16);
            this.cmbPrint.Name = "cmbPrint";
            this.cmbPrint.Size = new System.Drawing.Size(208, 40);
            this.cmbPrint.TabIndex = 11;
            this.cmbPrint.Text = "Labels";
            this.cmbPrint.Visible = false;
            this.cmbPrint.KeyDown += new Wisej.Web.KeyEventHandler(this.optPrint_KeyDown);
            // 
            // cmbRange
            // 
            this.cmbRange.Items.AddRange(new object[] {
            "All Accounts",
            "Range by Name",
            "Range by Account"});
            this.cmbRange.Location = new System.Drawing.Point(114, 40);
            this.cmbRange.Name = "cmbRange";
            this.cmbRange.Size = new System.Drawing.Size(258, 40);
            this.cmbRange.TabIndex = 4;
            this.cmbRange.Text = "All Accounts";
            this.cmbRange.Visible = false;
            this.cmbRange.SelectedIndexChanged += new System.EventHandler(this.optRange_Click);
            // 
            // cmbLien
            // 
            this.cmbLien.Items.AddRange(new object[] {
            "All Records",
            "Pre Lien Only",
            "Liened Only",
            "30 Day Notice",
            "Lien",
            "Lien Maturity"});
            this.cmbLien.Location = new System.Drawing.Point(140, 76);
            this.cmbLien.Name = "cmbLien";
            this.cmbLien.Size = new System.Drawing.Size(152, 40);
            this.cmbLien.TabIndex = 3;
            this.cmbLien.Text = "Pre Lien Only";
            this.cmbLien.SelectedIndexChanged += new System.EventHandler(this.optLien_Click);
            // 
            // fraRateInfo
            // 
            this.fraRateInfo.Controls.Add(this.cmdRIClose);
            this.fraRateInfo.Controls.Add(this.vsRateInfo);
            this.fraRateInfo.Location = new System.Drawing.Point(692, 392);
            this.fraRateInfo.Name = "fraRateInfo";
            this.fraRateInfo.Size = new System.Drawing.Size(343, 198);
            this.fraRateInfo.TabIndex = 14;
            this.fraRateInfo.Text = "Year Information";
            this.fraRateInfo.UseMnemonic = false;
            this.fraRateInfo.Visible = false;
            // 
            // cmdRIClose
            // 
            this.cmdRIClose.AppearanceKey = "actionButton";
            this.cmdRIClose.Location = new System.Drawing.Point(135, 149);
            this.cmdRIClose.Name = "cmdRIClose";
            this.cmdRIClose.Size = new System.Drawing.Size(87, 40);
            this.cmdRIClose.TabIndex = 1;
            this.cmdRIClose.Text = "Close";
            this.cmdRIClose.Click += new System.EventHandler(this.cmdRIClose_Click);
            // 
            // vsRateInfo
            // 
            this.vsRateInfo.Cols = 3;
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.vsRateInfo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vsRateInfo.ColumnHeadersVisible = false;
            this.vsRateInfo.Dock = Wisej.Web.DockStyle.Top;
            this.vsRateInfo.FixedCols = 0;
            this.vsRateInfo.FixedRows = 0;
            this.vsRateInfo.ForeColorFixed = System.Drawing.SystemColors.ControlText;
            this.vsRateInfo.Location = new System.Drawing.Point(3, 19);
            this.vsRateInfo.Name = "vsRateInfo";
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.vsRateInfo.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.vsRateInfo.RowHeadersVisible = false;
            this.vsRateInfo.Rows = 12;
            this.vsRateInfo.Size = new System.Drawing.Size(337, 118);
            this.vsRateInfo.TabIndex = 2;
            // 
            // fraPrintOptions
            // 
            this.fraPrintOptions.Controls.Add(this.cmbExtraLines);
            this.fraPrintOptions.Controls.Add(this.chkPrint_3);
            this.fraPrintOptions.Controls.Add(this.chkPrint_2);
            this.fraPrintOptions.Controls.Add(this.chkPrint_1);
            this.fraPrintOptions.Controls.Add(this.chkPrint_0);
            this.fraPrintOptions.Controls.Add(this.lblExtraLines);
            this.fraPrintOptions.Location = new System.Drawing.Point(436, 392);
            this.fraPrintOptions.Name = "fraPrintOptions";
            this.fraPrintOptions.Size = new System.Drawing.Size(246, 198);
            this.fraPrintOptions.TabIndex = 17;
            this.fraPrintOptions.Text = "Print Options";
            this.fraPrintOptions.UseMnemonic = false;
            // 
            // cmbExtraLines
            // 
            this.cmbExtraLines.BackColor = System.Drawing.SystemColors.Window;
            this.cmbExtraLines.Location = new System.Drawing.Point(121, 40);
            this.cmbExtraLines.Name = "cmbExtraLines";
            this.cmbExtraLines.Size = new System.Drawing.Size(108, 40);
            this.cmbExtraLines.TabIndex = 1;
            // 
            // chkPrint_3
            // 
            this.chkPrint_3.Location = new System.Drawing.Point(130, 147);
            this.chkPrint_3.Name = "chkPrint_3";
            this.chkPrint_3.Size = new System.Drawing.Size(84, 23);
            this.chkPrint_3.TabIndex = 5;
            this.chkPrint_3.Text = "Address";
            // 
            // chkPrint_2
            // 
            this.chkPrint_2.Location = new System.Drawing.Point(130, 100);
            this.chkPrint_2.Name = "chkPrint_2";
            this.chkPrint_2.Size = new System.Drawing.Size(86, 23);
            this.chkPrint_2.TabIndex = 3;
            this.chkPrint_2.Text = "Location";
            // 
            // chkPrint_1
            // 
            this.chkPrint_1.Location = new System.Drawing.Point(13, 147);
            this.chkPrint_1.Name = "chkPrint_1";
            this.chkPrint_1.Size = new System.Drawing.Size(99, 23);
            this.chkPrint_1.TabIndex = 4;
            this.chkPrint_1.Text = "Book Page";
            // 
            // chkPrint_0
            // 
            this.chkPrint_0.Location = new System.Drawing.Point(13, 100);
            this.chkPrint_0.Name = "chkPrint_0";
            this.chkPrint_0.Size = new System.Drawing.Size(81, 23);
            this.chkPrint_0.TabIndex = 2;
            this.chkPrint_0.Text = "Map Lot";
            // 
            // lblExtraLines
            // 
            this.lblExtraLines.AutoSize = true;
            this.lblExtraLines.Location = new System.Drawing.Point(18, 54);
            this.lblExtraLines.Name = "lblExtraLines";
            this.lblExtraLines.Size = new System.Drawing.Size(89, 16);
            this.lblExtraLines.TabIndex = 6;
            this.lblExtraLines.Text = "EXTRA LINES";
            this.lblExtraLines.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fraPreLienEdit
            // 
            this.fraPreLienEdit.Controls.Add(this.cmbReportDetail);
            this.fraPreLienEdit.Controls.Add(this.txtMailDate);
            this.fraPreLienEdit.Controls.Add(this.lblReportLen);
            this.fraPreLienEdit.Controls.Add(this.lblMailDate);
            this.fraPreLienEdit.Location = new System.Drawing.Point(30, 596);
            this.fraPreLienEdit.Name = "fraPreLienEdit";
            this.fraPreLienEdit.Size = new System.Drawing.Size(776, 99);
            this.fraPreLienEdit.TabIndex = 16;
            this.fraPreLienEdit.Text = "Lien Edit Report Format";
            this.fraPreLienEdit.UseMnemonic = false;
            this.fraPreLienEdit.Visible = false;
            // 
            // cmbReportDetail
            // 
            this.cmbReportDetail.BackColor = System.Drawing.SystemColors.Window;
            this.cmbReportDetail.Location = new System.Drawing.Point(422, 40);
            this.cmbReportDetail.Name = "cmbReportDetail";
            this.cmbReportDetail.Size = new System.Drawing.Size(335, 40);
            this.cmbReportDetail.TabIndex = 3;
            this.cmbReportDetail.DropDown += new System.EventHandler(this.cmbReportDetail_DropDown);
            this.cmbReportDetail.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbReportDetail_KeyDown);
            // 
            // txtMailDate
            // 
            this.txtMailDate.Location = new System.Drawing.Point(170, 40);
            this.txtMailDate.Mask = "##/##/####";
            this.txtMailDate.Name = "txtMailDate";
            this.txtMailDate.Size = new System.Drawing.Size(115, 22);
            this.txtMailDate.TabIndex = 1;
            // 
            // lblReportLen
            // 
            this.lblReportLen.AutoSize = true;
            this.lblReportLen.Location = new System.Drawing.Point(302, 54);
            this.lblReportLen.Name = "lblReportLen";
            this.lblReportLen.Size = new System.Drawing.Size(108, 16);
            this.lblReportLen.TabIndex = 2;
            this.lblReportLen.Text = "REPORT DETAIL";
            this.lblReportLen.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMailDate
            // 
            this.lblMailDate.AutoSize = true;
            this.lblMailDate.Location = new System.Drawing.Point(16, 54);
            this.lblMailDate.Name = "lblMailDate";
            this.lblMailDate.Size = new System.Drawing.Size(149, 16);
            this.lblMailDate.TabIndex = 4;
            this.lblMailDate.Text = "MAIL / INTEREST DATE";
            this.lblMailDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fraRange
            // 
            this.fraRange.Controls.Add(this.txtRange_1);
            this.fraRange.Controls.Add(this.txtRange_0);
            this.fraRange.Controls.Add(this.txtMinimumAmount);
            this.fraRange.Controls.Add(this.lblMinAmount);
            this.fraRange.Controls.Add(this.lblRange);
            this.fraRange.Controls.Add(this.cmbRange);
            this.fraRange.Location = new System.Drawing.Point(30, 392);
            this.fraRange.Name = "fraRange";
            this.fraRange.Size = new System.Drawing.Size(392, 198);
            this.fraRange.TabIndex = 15;
            this.fraRange.Text = "Selection Criteria";
            this.fraRange.UseMnemonic = false;
            this.fraRange.Visible = false;
            // 
            // txtMinimumAmount
            // 
            this.txtMinimumAmount.BackColor = System.Drawing.SystemColors.Window;
            this.txtMinimumAmount.Location = new System.Drawing.Point(298, 140);
            this.txtMinimumAmount.Name = "txtMinimumAmount";
            this.txtMinimumAmount.Size = new System.Drawing.Size(74, 40);
            this.txtMinimumAmount.TabIndex = 3;
            this.txtMinimumAmount.Text = "0.00";
            this.txtMinimumAmount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtMinimumAmount, "Minimum principal due in order to process.");
            this.txtMinimumAmount.Enter += new System.EventHandler(this.txtMinimumAmount_Enter);
            this.txtMinimumAmount.Validating += new System.ComponentModel.CancelEventHandler(this.txtMinimumAmount_Validating);
            this.txtMinimumAmount.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMinimumAmount_KeyPress);
            // 
            // lblMinAmount
            // 
            this.lblMinAmount.AutoSize = true;
            this.lblMinAmount.Location = new System.Drawing.Point(20, 154);
            this.lblMinAmount.Name = "lblMinAmount";
            this.lblMinAmount.Size = new System.Drawing.Size(285, 16);
            this.lblMinAmount.TabIndex = 2;
            this.lblMinAmount.Text = "MINIMUM PRINCIPAL AMOUNT TO PROCESS";
            this.lblMinAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblRange
            // 
            this.lblRange.AutoSize = true;
            this.lblRange.Location = new System.Drawing.Point(20, 54);
            this.lblRange.Name = "lblRange";
            this.lblRange.Size = new System.Drawing.Size(50, 16);
            this.lblRange.TabIndex = 2;
            this.lblRange.Text = "RANGE";
            this.lblRange.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblRange.Visible = false;
            // 
            // vsRate
            // 
            this.vsRate.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsRate.Cols = 5;
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.vsRate.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.vsRate.FixedCols = 0;
            this.vsRate.ForeColorFixed = System.Drawing.SystemColors.ControlText;
            this.vsRate.Location = new System.Drawing.Point(30, 135);
            this.vsRate.MinimumSize = new System.Drawing.Size(800, 112);
            this.vsRate.Name = "vsRate";
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.vsRate.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.vsRate.RowHeadersVisible = false;
            this.vsRate.Rows = 1;
            this.vsRate.Size = new System.Drawing.Size(1018, 222);
            this.vsRate.TabIndex = 13;
            this.vsRate.Visible = false;
            this.vsRate.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsRate_BeforeEdit);
            this.vsRate.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsRate_ValidateEdit);
            this.vsRate.CurrentCellChanged += new System.EventHandler(this.vsRate_RowColChange);
            this.vsRate.CellDoubleClick += new Wisej.Web.DataGridViewCellEventHandler(this.vsRate_CellDoubleClick);
            this.vsRate.CellMouseClick += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vsRate_CellChanged);
            this.vsRate.GotFocus += new System.EventHandler(this.vsRate_GotFocus);
            this.vsRate.DoubleClick += new System.EventHandler(this.vsRate_DblClick);
            this.vsRate.MouseDown += new Wisej.Web.MouseEventHandler(this.vsRate_MouseDown);
            // 
            // fraQuestions
            // 
            this.fraQuestions.AppearanceKey = "groupBoxNoBorders";
            this.fraQuestions.Controls.Add(this.lblYear);
            this.fraQuestions.Controls.Add(this.cmbYear);
            this.fraQuestions.Controls.Add(this.lblEligible);
            this.fraQuestions.Controls.Add(this.cmbLien);
            this.fraQuestions.Name = "fraQuestions";
            this.fraQuestions.Size = new System.Drawing.Size(303, 147);
            this.fraQuestions.TabIndex = 1;
            this.fraQuestions.UseMnemonic = false;
            this.fraQuestions.Visible = false;
            // 
            // lblYear
            // 
            this.lblYear.AutoSize = true;
            this.lblYear.Location = new System.Drawing.Point(30, 30);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(40, 16);
            this.lblYear.TabIndex = 4;
            this.lblYear.Text = "YEAR";
            this.lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbYear
            // 
            this.cmbYear.BackColor = System.Drawing.SystemColors.Window;
            this.cmbYear.Location = new System.Drawing.Point(140, 16);
            this.cmbYear.Name = "cmbYear";
            this.cmbYear.Size = new System.Drawing.Size(152, 40);
            this.cmbYear.TabIndex = 5;
            this.cmbYear.SelectedIndexChanged += new System.EventHandler(this.cmbYear_SelectedIndexChanged);
            this.cmbYear.Enter += new System.EventHandler(this.cmbYear_Enter);
            // 
            // lblEligible
            // 
            this.lblEligible.AutoSize = true;
            this.lblEligible.Location = new System.Drawing.Point(30, 90);
            this.lblEligible.Name = "lblEligible";
            this.lblEligible.Size = new System.Drawing.Size(95, 16);
            this.lblEligible.TabIndex = 1;
            this.lblEligible.Text = "ELIGIBLE FOR";
            this.lblEligible.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // lblReminderLien
            // 
            this.lblReminderLien.AutoSize = true;
            this.lblReminderLien.Location = new System.Drawing.Point(674, 90);
            this.lblReminderLien.Name = "lblReminderLien";
            this.lblReminderLien.Size = new System.Drawing.Size(106, 16);
            this.lblReminderLien.TabIndex = 8;
            this.lblReminderLien.Text = "REMINDER LIEN";
            this.lblReminderLien.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblReminderLien.Visible = false;
            // 
            // lblPeriod
            // 
            this.lblPeriod.AutoSize = true;
            this.lblPeriod.Location = new System.Drawing.Point(334, 30);
            this.lblPeriod.Name = "lblPeriod";
            this.lblPeriod.Size = new System.Drawing.Size(53, 16);
            this.lblPeriod.TabIndex = 4;
            this.lblPeriod.Text = "PERIOD";
            this.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblPeriod.Visible = false;
            // 
            // lblRePrint
            // 
            this.lblRePrint.AutoSize = true;
            this.lblRePrint.Location = new System.Drawing.Point(334, 90);
            this.lblRePrint.Name = "lblRePrint";
            this.lblRePrint.Size = new System.Drawing.Size(81, 16);
            this.lblRePrint.TabIndex = 6;
            this.lblRePrint.Text = "INITIAL RUN";
            this.lblRePrint.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblRePrint.Visible = false;
            // 
            // lblPrintType
            // 
            this.lblPrintType.AutoSize = true;
            this.lblPrintType.Location = new System.Drawing.Point(30, 30);
            this.lblPrintType.Name = "lblPrintType";
            this.lblPrintType.Size = new System.Drawing.Size(135, 16);
            this.lblPrintType.TabIndex = 10;
            this.lblPrintType.Text = "SELECT PRINT TYPE";
            this.lblPrintType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblPrintType.Visible = false;
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Location = new System.Drawing.Point(467, 30);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcess.Size = new System.Drawing.Size(120, 48);
            this.btnProcess.TabIndex = 1;
            this.btnProcess.Text = "Continue";
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // cmdFileAddRateKey
            // 
            this.cmdFileAddRateKey.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileAddRateKey.Location = new System.Drawing.Point(926, 30);
            this.cmdFileAddRateKey.Name = "cmdFileAddRateKey";
            this.cmdFileAddRateKey.Shortcut = Wisej.Web.Shortcut.CtrlShiftN;
            this.cmdFileAddRateKey.Size = new System.Drawing.Size(105, 24);
            this.cmdFileAddRateKey.TabIndex = 52;
            this.cmdFileAddRateKey.Text = "New Rate Key";
            this.cmdFileAddRateKey.Click += new System.EventHandler(this.cmdFileAddRateKey_Click);
            // 
            // frmRateRecChoice
            // 
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1064, 834);
            this.KeyPreview = true;
            this.Name = "frmRateRecChoice";
            this.Text = "Select Rate Record";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmRateRecChoice_Load);
            this.Activated += new System.EventHandler(this.frmRateRecChoice_Activated);
            this.Enter += new System.EventHandler(this.frmRateRecChoice_Enter);
            this.Resize += new System.EventHandler(this.frmRateRecChoice_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmRateRecChoice_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraRateInfo)).EndInit();
            this.fraRateInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdRIClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsRateInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraPrintOptions)).EndInit();
            this.fraPrintOptions.ResumeLayout(false);
            this.fraPrintOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrint_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrint_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrint_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrint_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraPreLienEdit)).EndInit();
            this.fraPreLienEdit.ResumeLayout(false);
            this.fraPreLienEdit.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMailDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraRange)).EndInit();
            this.fraRange.ResumeLayout(false);
            this.fraRange.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraQuestions)).EndInit();
            this.fraQuestions.ResumeLayout(false);
            this.fraQuestions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileAddRateKey)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCLabel lblReminderLien;
		private System.ComponentModel.IContainer components;
		private FCLabel lblPeriod;
		private FCLabel lblRePrint;
		private FCLabel lblPrintType;
		private FCLabel lblRange;
		private FCLabel lblYear;
		private FCButton btnProcess;
		public FCButton cmdFileAddRateKey;
		private JavaScript javaScriptTxtRange;
		Wisej.Web.JavaScript.ClientEvent clientEventTxtRange;
	}
}
