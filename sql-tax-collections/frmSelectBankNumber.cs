﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmSelectBankNumber.
	/// </summary>
	public partial class frmSelectBankNumber : BaseForm
	{
		public frmSelectBankNumber()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmSelectBankNumber InstancePtr
		{
			get
			{
				return (frmSelectBankNumber)Sys.GetInstance(typeof(frmSelectBankNumber));
			}
		}

		protected frmSelectBankNumber _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		clsDRWrapper rsInfo = new clsDRWrapper();
		int AccountCol;
		int KeyCol;
		int BankNumberCol;
		bool blnSaved;

		private void frmSelectBankNumber_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
			vsBankNumber.Select(1, BankNumberCol);
			vsBankNumber.Focus();
		}

		private void frmSelectBankNumber_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSelectBankNumber.Icon	= "frmSelectBankNumber.frx":0000";
			//frmSelectBankNumber.FillStyle	= 0;
			//frmSelectBankNumber.ScaleWidth	= 3885;
			//frmSelectBankNumber.ScaleHeight	= 2625;
			//frmSelectBankNumber.LinkTopic	= "Form2";
			//frmSelectBankNumber.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "9";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//vsBankNumber.BackColor	= "-2147483643";
			//			//vsBankNumber.ForeColor	= "-2147483640";
			//vsBankNumber.BorderStyle	= 1;
			//vsBankNumber.FillStyle	= 0;
			//vsBankNumber.Appearance	= 1;
			//vsBankNumber.GridLines	= 1;
			//vsBankNumber.WordWrap	= 0;
			//vsBankNumber.ScrollBars	= 3;
			//vsBankNumber.RightToLeft	= 0;
			//vsBankNumber._cx	= 5424;
			//vsBankNumber._cy	= 2896;
			//vsBankNumber._ConvInfo	= 1;
			//vsBankNumber.MousePointer	= 0;
			//vsBankNumber.BackColorFixed	= -2147483633;
			//			//vsBankNumber.ForeColorFixed	= -2147483630;
			//vsBankNumber.BackColorSel	= -2147483635;
			//			//vsBankNumber.ForeColorSel	= -2147483634;
			//vsBankNumber.BackColorBkg	= -2147483636;
			//vsBankNumber.BackColorAlternate	= -2147483643;
			//vsBankNumber.GridColor	= -2147483633;
			//vsBankNumber.GridColorFixed	= -2147483632;
			//vsBankNumber.TreeColor	= -2147483632;
			//vsBankNumber.FloodColor	= 192;
			//vsBankNumber.SheetBorder	= -2147483642;
			//vsBankNumber.FocusRect	= 1;
			//vsBankNumber.HighLight	= 0;
			//vsBankNumber.AllowSelection	= -1  'True;
			//vsBankNumber.AllowBigSelection	= -1  'True;
			//vsBankNumber.AllowUserResizing	= 0;
			//vsBankNumber.SelectionMode	= 0;
			//vsBankNumber.GridLinesFixed	= 2;
			//vsBankNumber.GridLineWidth	= 1;
			//vsBankNumber.RowHeightMin	= 0;
			//vsBankNumber.RowHeightMax	= 0;
			//vsBankNumber.ColWidthMin	= 0;
			//vsBankNumber.ColWidthMax	= 0;
			//vsBankNumber.ExtendLastCol	= -1  'True;
			//vsBankNumber.FormatString	= "";
			//vsBankNumber.ScrollTrack	= -1  'True;
			//vsBankNumber.ScrollTips	= 0   'False;
			//vsBankNumber.MergeCells	= 0;
			//vsBankNumber.MergeCompare	= 0;
			//vsBankNumber.AutoResize	= -1  'True;
			//vsBankNumber.AutoSizeMode	= 0;
			//vsBankNumber.AutoSearch	= 0;
			//vsBankNumber.AutoSearchDelay	= 2;
			//vsBankNumber.MultiTotals	= -1  'True;
			//vsBankNumber.SubtotalPosition	= 1;
			//vsBankNumber.OutlineBar	= 0;
			//vsBankNumber.OutlineCol	= 0;
			//vsBankNumber.Ellipsis	= 0;
			//vsBankNumber.ExplorerBar	= 0;
			//vsBankNumber.PicturesOver	= 0   'False;
			//vsBankNumber.PictureType	= 0;
			//vsBankNumber.TabBehavior	= 1;
			//vsBankNumber.OwnerDraw	= 0;
			//vsBankNumber.ShowComboButton	= -1  'True;
			//vsBankNumber.TextStyle	= 0;
			//vsBankNumber.TextStyleFixed	= 0;
			//vsBankNumber.OleDragMode	= 0;
			//vsBankNumber.OleDropMode	= 0;
			//vsBankNumber.ComboSearch	= 3;
			//vsBankNumber.AutoSizeMouse	= -1  'True;
			//vsBankNumber.AllowUserFreezing	= 0;
			//vsBankNumber.BackColorFrozen	= 0;
			//			//vsBankNumber.ForeColorFrozen	= 0;
			//vsBankNumber.WallPaperAlignment	= 9;
			//vsElasticLight1.OleObjectBlob	= "frmSelectBankNumber.frx":058A";
			//End Unmaped Properties
			string strComboString;
			int counter;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			blnSaved = false;
			KeyCol = 0;
			AccountCol = 1;
			BankNumberCol = 2;
			vsBankNumber.ColHidden(KeyCol, true);
			vsBankNumber.ColWidth(AccountCol, FCConvert.ToInt32(vsBankNumber.Width * 0.7));
			vsBankNumber.TextMatrix(0, AccountCol, "Account");
			vsBankNumber.TextMatrix(0, BankNumberCol, "Bank");
			strComboString = "";
			rsInfo.OpenRecordset("SELECT * FROM Banks WHERE rTrim(Name) <> '' ORDER BY ID");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					strComboString += "'" + FCConvert.ToString(rsInfo.Get_Fields_Int32("ID")) + ";" + FCConvert.ToString(rsInfo.Get_Fields_Int32("ID")) + "\t" + FCConvert.ToString(rsInfo.Get_Fields_String("Name")) + "|";
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
				strComboString = Strings.Left(strComboString, strComboString.Length - 1);
			}
			vsBankNumber.ColComboList(BankNumberCol, strComboString);
			for (counter = 0; counter <= modBudgetaryAccounting.Statics.lngNumberOfAccounts - 1; counter++)
			{
				if (modBudgetaryAccounting.Statics.AcctsToPass[counter].intBank == 0)
				{
					vsBankNumber.Rows += 1;
					vsBankNumber.TextMatrix(vsBankNumber.Rows - 1, KeyCol, FCConvert.ToString(modBudgetaryAccounting.Statics.AcctsToPass[counter].lngKey));
					vsBankNumber.TextMatrix(vsBankNumber.Rows - 1, AccountCol, modBudgetaryAccounting.Statics.AcctsToPass[counter].strAccount);
				}
			}
		}

		private void frmSelectBankNumber_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				Close();
			}
			else if (KeyAscii == 13)
			{
				KeyAscii = 0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}
		// VBto upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			int counter;
			vsBankNumber.Row -= 1;
			for (counter = 1; counter <= vsBankNumber.Rows - 1; counter++)
			{
				if (vsBankNumber.TextMatrix(counter, BankNumberCol) == "")
				{
					vsBankNumber.Select(counter, BankNumberCol);
					FCMessageBox.Show("You must fill in a bank number for each account shown before you may continue.", MsgBoxStyle.Information, "Enter Bank Number");
					e.Cancel = true;
					return;
				}
			}
			if (!blnSaved)
			{
				mnuProcessSave_Click();
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			int counter;
			clsDRWrapper rsInfo = new clsDRWrapper();
			int lngRowCounter;
			vsBankNumber.Row = 0;
			for (counter = 1; counter <= vsBankNumber.Rows - 1; counter++)
			{
				if (vsBankNumber.TextMatrix(counter, BankNumberCol) == "")
				{
					vsBankNumber.Select(counter, BankNumberCol);
					FCMessageBox.Show("You must fill in a bank number for each account shown before you may continue.", MsgBoxStyle.Information, "Enter Bank Number");
					return;
				}
			}
			lngRowCounter = 1;
			for (counter = 0; counter <= modBudgetaryAccounting.Statics.lngNumberOfAccounts - 1; counter++)
			{
				if (modBudgetaryAccounting.Statics.AcctsToPass[counter].intBank == 0)
				{
					modBudgetaryAccounting.Statics.AcctsToPass[counter].intBank = FCConvert.ToInt32(Math.Round(Conversion.Val(vsBankNumber.TextMatrix(lngRowCounter, BankNumberCol))));
					rsInfo.Execute("UPDATE JournalEntries SET BankNumber = " + FCConvert.ToString(Conversion.Val(vsBankNumber.TextMatrix(lngRowCounter, BankNumberCol))) + " WHERE ID = " + FCConvert.ToString(Conversion.Val(vsBankNumber.TextMatrix(lngRowCounter, KeyCol))), "Budgetary");
					rsInfo.Execute("INSERT INTO BankCashAccounts (BankNumber, Account, AllSuffix) VALUES (" + FCConvert.ToString(Conversion.Val(vsBankNumber.TextMatrix(lngRowCounter, BankNumberCol))) + ", '" + vsBankNumber.TextMatrix(lngRowCounter, AccountCol) + "', 0)", "Budgetary");
					lngRowCounter += 1;
				}
			}
			Close();
		}

		public void mnuProcessSave_Click()
		{
			mnuProcessSave_Click(btnProcess, new System.EventArgs());
		}

		private void vsBankNumber_Click(object sender, EventArgs e)
		{
			vsBankNumber.EditCell();
		}

		private void vsBankNumber_RowColChange(object sender, EventArgs e)
		{
			vsBankNumber.EditCell();
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuProcessSave_Click(sender, e);
		}
	}
}
