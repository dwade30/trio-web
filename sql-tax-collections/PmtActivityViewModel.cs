﻿using Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using Microsoft.Ajax.Utilities;
using SharedDataAccess;
using SharedApplication;
using SharedApplication.Enums;
using SharedApplication.Extensions;
using SharedApplication.PersonalProperty.Extensions;
using SharedApplication.PersonalProperty.Models;
using SharedApplication.RealEstate.Models;
using SharedApplication.TaxCollections.Extensions;
using SharedApplication.TaxCollections.Models;
using TWCL0000.DataAccess;

namespace TWCL0000
{


    public enum TaxBillType
    {
        Regular = 0,
        Lien = 1
    }

    public enum PmtActivityAccountSortType
    {
        Account = 0,
        Name = 1
    }

    public class PmtActivityReportOption
    {
        public bool IncludePayments { get; set; } = false;
        public bool IncludePrePayments { get; set; } = false;
        public bool IncludeCorrections { get; set; } = false;
        public bool IncludeAbatements { get; set; } = false;
        public bool IncludeDiscounts { get; set; } = false;
        public bool IncludeTaxClub { get; set; } = false;
        public bool IncludeRefund { get; set; } = false;
        public bool IncludeNonBudgetary { get; set; } = false;
        public bool IncludeInterest { get; set; } = false;
        public bool IncludeDemandFee { get; set; } = false;
        public bool IncludeMaturityFee { get; set; } = false;
        
    }

    public class PmtActivityReportConfiguration
    {
        public PmtActivityReportOption Options { get; private set; }
        public PmtActivityReportFilter Filter { get; private set; }
        public string WhereClause { get; set; } = "";
        public string SortClause { get; set; } = "";
        public string SelectClause { get; set; } = "";
        public string SortListing { get; set; } = "";
        public string SQLStatement { get; set; } = "";       
        public PmtActivityReportConfiguration(PmtActivityReportOption reportOptions, PmtActivityReportFilter filter)
        {
            Options = reportOptions.GetCopy();
            Filter = filter.GetCopy();
        }
    }

    public class PmtActivityReportFilter
    {
       public int? TaxYearMin { get; set; } = null;
        public int? TaxYearMax { get; set; } = null;
        public int? AccountMin { get; set; } = null;
        public int? AccountMax { get; set; } = null;
        public DateTime? PaymentDateMin { get; set; } = null;
        public DateTime? PaymentDateMax { get; set; } = null;
        public TaxBillType? TaxBillType { get; set; } = null;
        public string Reference { get; set; } = "";
        public bool AccountRangeUsed()
        {
            return AccountMin.HasValue || AccountMax.HasValue;
        }

        public bool TaxYearRangeUsed()
        {
            return TaxYearMin.HasValue || TaxYearMax.HasValue;
        }

        public bool PaymentDateRangeUsed()
        {
            return PaymentDateMin.HasValue || PaymentDateMax.HasValue;
        }
    }

    public static class PaymentActivityReportExtensions
    {
        public static PmtActivityReportFilter GetCopy(this PmtActivityReportFilter filter)
        {
            return new PmtActivityReportFilter()
            {
                AccountMax = filter.AccountMax,
                AccountMin = filter.AccountMin,
                TaxBillType = filter.TaxBillType,
                TaxYearMax = filter.TaxYearMax,
                TaxYearMin = filter.TaxYearMin,
                PaymentDateMax = filter.PaymentDateMax,
                PaymentDateMin = filter.PaymentDateMin,
                Reference =  filter.Reference
            };
        }

        public static PmtActivityReportOption GetCopy(this PmtActivityReportOption option)
        {
            return new PmtActivityReportOption()
            {
                IncludePayments = option.IncludePayments,
                IncludePrePayments = option.IncludePrePayments,
                IncludeCorrections = option.IncludeCorrections,
                IncludeAbatements = option.IncludeAbatements,
                IncludeDiscounts = option.IncludeDiscounts,
                IncludeTaxClub = option.IncludeTaxClub,
                IncludeRefund = option.IncludeRefund,
                IncludeNonBudgetary = option.IncludeNonBudgetary,
                IncludeInterest = option.IncludeInterest,
                IncludeDemandFee = option.IncludeDemandFee,
                IncludeMaturityFee = option.IncludeMaturityFee
            };
        }
    }

    public class PmtActivityViewModel
    {

        public event EventHandler FilterChanged;
        public event EventHandler SortOptionsChanged;
        public event EventHandler SelectedOptionsChanged;
        public IEnumerable<string> TaxYears { get; private set; } = new List<string>();                
        public IEnumerable<SelectableItem<DescriptionIDPair>> SortOptions { get; protected set; } =
            new List<SelectableItem<DescriptionIDPair>>();

        public IEnumerable<DescriptionIDPair> BillTypes { get; private set; } = new List<DescriptionIDPair>();

        public PmtActivityReportOption ReportOption
        {
            get { return reportOption; }
        }

        private TrioContextFactory contextFactory;

        public PmtActivityViewModel(PropertyTaxBillType billType, ISavedStatusReportsRepository reportsRepository,
            IBillingMasterRepository billsRepository, TrioContextFactory trioContextFactory)
        {
            contextFactory = trioContextFactory;
            billingType = billType;
            billingMasterRepository = billsRepository;
            GetTaxYears();
            
        }

        //        public void SetReportType(string reportType)
        //        {
        //            if (reportType != ReportType)
        //            {
        //                ReportType = reportType;
        //                UpdateForReportType();
        //            }
        //        }
        //        private void UpdateForReportType()
        //        {
        //            FillAvailableFields();
        //            FillSortOptions();
        //            GetListOfStatusReports();
        //        }

        private PmtActivityReportFilter reportFilter = new PmtActivityReportFilter();

        private PmtActivityReportOption reportOption = new PmtActivityReportOption();

        //        private ISavedStatusReportsRepository statusReportsRepository;
        private IBillingMasterRepository billingMasterRepository;
        private PropertyTaxBillType billingType = PropertyTaxBillType.Real;

        public void SetFilter(PmtActivityReportFilter filter)
        {
            reportFilter = filter.GetCopy();
        }

        public PmtActivityReportFilter GetFilter()
        {
            return reportFilter.GetCopy();
        }

        public void ClearFilter()
        {
            reportFilter = new PmtActivityReportFilter();
            OnFilterChanged(new EventArgs() { });
        }

        public void SetOption(PmtActivityReportOption option)
        {
            reportOption = option.GetCopy();
        }
       
        protected void OnFilterChanged(EventArgs e)
        {
            EventHandler handler = FilterChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        protected void OnSortOptionsChanged(EventArgs e)
        {
            EventHandler handler = SortOptionsChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }       

        protected void OnSelectedOptionsChanged(EventArgs e)
        {
            EventHandler handler = SelectedOptionsChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
       
        private void GetTaxYears()
        {
            var taxYears = billingMasterRepository.GetTaxYears();
            TaxYears = taxYears.Select(value =>
                value.ToString().Substring(0, 4) + "-" + value.ToString().Reverse().FirstOrDefault());
        }

       

        //private void FillSortOptions()
        //{
        //    switch (ReportType.ToLower())
        //    {
        //        case "taxclub":
        //            //FillSortOptionsForTaxClub();
        //            break;
        //        default:
        //            FillSortOptionsForAccount();
        //            break;
        //    }

        //    OnSortOptionsChanged(new EventArgs() { });
        //}

        private void FillSortOptions()
        {
            SortOptions = new List<SelectableItem<DescriptionIDPair>>()
            {
                new SelectableItem<DescriptionIDPair>()
                {
                    IsSelected = false,
                    Item = new DescriptionIDPair()
                        {Description = "Account", ID = (int) TaxReportAccountSortType.Account}
                },
                new SelectableItem<DescriptionIDPair>()
                {
                    IsSelected = false,
                    Item = new DescriptionIDPair() {Description = "Name", ID = (int) TaxReportAccountSortType.Name}
                },
                new SelectableItem<DescriptionIDPair>()
                {
                    IsSelected = false,
                    Item = new DescriptionIDPair() {Description = "Year", ID = (int) TaxReportAccountSortType.Year}
                }
            };
        }       

        public PmtActivityReportConfiguration GetReportConfiguration()
        {
            var reportConfiguration = new PmtActivityReportConfiguration(reportOption, reportFilter);
            //reportConfiguration.BillingType = billingType;
            reportConfiguration.SelectClause = CreateSelectStatement(reportFilter, reportOption);
            reportConfiguration.WhereClause = CreateWhereClause(reportFilter, reportOption);
            reportConfiguration.SortClause = CreateSortParameter(reportOption);
            reportConfiguration.SQLStatement =
                reportConfiguration.SelectClause + reportConfiguration.WhereClause.Trim() != ""
                    ?
                    " Where " + reportConfiguration.WhereClause
                    : "" +
                      reportConfiguration.SortClause.Trim() != ""
                        ? " order by " + reportConfiguration.SortClause
                        : "";
            return reportConfiguration;
        }

        private string CreateSelectStatement(PmtActivityReportFilter filter, PmtActivityReportOption option)
        {
            string strPaymentDateRange = "";
            string selectStatement = "";

            if (filter.PaymentDateMin.HasValue && filter.PaymentDateMax.HasValue)
            {
                strPaymentDateRange = " AND (RecordedTransactionDate >= '" +
                                      filter.PaymentDateMin.Value.ToShortDateString() +
                                      "' AND RecordedTransactionDate <= '" +
                                      filter.PaymentDateMax.Value.ToShortDateString();
            }
            else
            {
                strPaymentDateRange = "";
            }

            //if (!filter.TaxBillType.HasValue)
            //{
            //    if (filter.PaymentDateMin.HasValue || filter.PaymentDateMax.HasValue)
            //    {
            //        selectStatement =
            //            "Select Distinct BillCode, BillKey as BK from PaymentRec Where Code In ('P','C','A','R','Y','U','D') " +
            //            strPaymentDateRange;
            //    }
            //    else
            //    {

            //        selectStatement = "Select Distinct BillingType, ID as BK from BillingMaster";
            //    }
            //}
            //else
            //{
            selectStatement = "SELECT Distinct BillCode, BillKey AS BK FROM PaymentRec ";

            //    switch (filter.AccountPaymentType.Value)
            //    {
            //        case 6:
            //            selectStatement += " Where Code = 'P'";
            //            break;
            //        case 7:
            //            selectStatement += " Where Code = 'C'";
            //            break;
            //        case 0:
            //            selectStatement += " Where Code = 'A'";
            //            break;
            //        case 2:
            //            selectStatement += " Where Code = 'R'";
            //            break;
            //        case 4:
            //            selectStatement += " Where Code = 'Y'";
            //            break;
            //        case 3:
            //            selectStatement += " Where Code = 'U'";
            //            break;
            //        //case "SUPPLEMENTALS":
            //        //    selectStatement += " Where Code = 'S'";
            //        //    break;
            //        case 1:
            //            break;
            //        case 5:
            //            break;
            //    }

            selectStatement += strPaymentDateRange;
            //}

            return selectStatement;
        }

        private string CreateWhereClause(PmtActivityReportFilter filter, PmtActivityReportOption option)
        {
            string whereParameter = " ";

            if (filter.AccountMin.HasValue)
            {
                if (filter.AccountMax.HasValue && filter.AccountMax > 0)
                {
                    whereParameter += " Account between " + filter.AccountMin.ToString() + " and " +
                                      filter.AccountMax.ToString();
                }
                else
                {
                    whereParameter += "Account >= " + filter.AccountMin.ToString();
                }
            }
            else
            {
                if (filter.AccountMax.HasValue && filter.AccountMax > 0)
                {
                    whereParameter += " Account <= " + filter.AccountMax.ToString();
                }
            }
            
            if (filter.TaxYearMin.HasValue && filter.TaxYearMin.Value > 0)
            {
                if (!String.IsNullOrWhiteSpace(whereParameter))
                {
                    whereParameter += " and ";
                }

                if (filter.TaxYearMax.HasValue && filter.TaxYearMax.Value > 0)
                {
                    whereParameter += " BillingYear between " + filter.TaxYearMin.ToString() + " and " +
                                      filter.TaxYearMax.ToString();
                }
                else
                {
                    whereParameter += "BillingYear = " + filter.TaxYearMin.ToString();
                }
            }

            return whereParameter;
        }

        private string CreateSortParameter(PmtActivityReportOption options)
        {
            string sortParameter = " ";
            string separator = "";
            // BUILD THE SORT CRITERIA FOR THE SQL STATEMENT

            // GET THE FIELDS TO SORT BY
            var selectedOptions = SortOptions.Where(o => o.IsSelected);
            foreach (var sortOption in selectedOptions)
            {
                sortParameter += separator;
                separator = ", ";
                switch (sortOption.Item.ID)
                {
                    case (int) TaxReportAccountSortType.Account:
                        sortParameter += "Account";
                        break;
                    case (int) TaxReportAccountSortType.Year:
                        sortParameter += "BillingYear";
                        break;
                }
            }

            //if (String.IsNullOrWhiteSpace(sortParameter))             is this needed for default, if nothing checked?????
            //{
            //    if (options.NameToShow == TaxReportNameOption.CurrentOwner ||
            //        options.NameToShow == TaxReportNameOption.CurrentOwnerBilledAsOwner)
            //    {
            //        sortParameter = "Own1FullName, Account, BillingYear";
            //    }
            //    else
            //    {
            //        sortParameter = "Name1, Account, BillingYear";
            //    }
            //}

            return sortParameter;
        }
 
    }
}
