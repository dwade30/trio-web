﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmPrintTaxClub.
	/// </summary>
	public partial class frmPrintTaxClub : BaseForm
	{
		public frmPrintTaxClub()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		int lngColAccount;
		int lngColYear;
		int lngColHidden;
		int lngColAgreementDate;
		int lngColStartDate;
		int lngColNumPayments;
		int lngColPaymentAmount;
		int lngColTotalPayments;

		public void Init()
		{
			this.Show(App.MainForm);
		}

		private void frmPrintTaxClub_Load(object sender, System.EventArgs e)
		{			
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			lngColAccount = 0;
			lngColYear = 1;
			lngColHidden = 2;
			lngColAgreementDate = 3;
			lngColStartDate = 4;
			lngColNumPayments = 5;
			lngColPaymentAmount = 6;
			lngColTotalPayments = 7;
		}

		private void frmPrintTaxClub_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// Catches the escape and enter keys
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				Close();
			}
			else if (KeyAscii == 13)
			{
				KeyAscii = 0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void ShowFrame(GroupBox fraFrame)
		{
			// this will place the frame in the middle of the form
			fraFrame.Top = FCConvert.ToInt32((this.Height - fraFrame.Height) / 3.0);
			fraFrame.Left = FCConvert.ToInt32((this.Width - fraFrame.Width) / 2.0);
			fraFrame.Visible = true;
		}

		private void FormatGrid()
		{
			vsTaxClub.ColWidth(lngColAccount, FCConvert.ToInt32(vsTaxClub.WidthOriginal * 0.1));
			vsTaxClub.ColWidth(lngColYear, FCConvert.ToInt32(vsTaxClub.WidthOriginal * 0.1));
			vsTaxClub.ColWidth(lngColHidden, 0);
			vsTaxClub.ColWidth(lngColAgreementDate, FCConvert.ToInt32(vsTaxClub.WidthOriginal * 0.2));
			vsTaxClub.ColWidth(lngColStartDate, FCConvert.ToInt32(vsTaxClub.WidthOriginal * 0.2));
			vsTaxClub.ColWidth(lngColNumPayments, FCConvert.ToInt32(vsTaxClub.WidthOriginal * 0.1));
			vsTaxClub.ColWidth(lngColPaymentAmount, FCConvert.ToInt32(vsTaxClub.WidthOriginal * 0.15));
			vsTaxClub.ColWidth(lngColTotalPayments, FCConvert.ToInt32(vsTaxClub.WidthOriginal * 0.1));
			vsTaxClub.ColAlignment(lngColAccount, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsTaxClub.ColAlignment(lngColYear, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsTaxClub.ColAlignment(lngColAgreementDate, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsTaxClub.ColAlignment(lngColStartDate, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsTaxClub.ColAlignment(lngColNumPayments, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsTaxClub.ColAlignment(lngColPaymentAmount, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsTaxClub.ColAlignment(lngColTotalPayments, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsTaxClub.ColFormat(lngColPaymentAmount, "#,##0.00");
			vsTaxClub.ExtendLastCol = true;
			vsTaxClub.TextMatrix(0, lngColAccount, "Account");
			vsTaxClub.TextMatrix(0, lngColYear, "Year");
			vsTaxClub.TextMatrix(0, lngColAgreementDate, "Agreement Date");
			vsTaxClub.TextMatrix(0, lngColStartDate, "Start Date");
			vsTaxClub.TextMatrix(0, lngColNumPayments, "Payments");
			vsTaxClub.TextMatrix(0, lngColPaymentAmount, "Amount");
			vsTaxClub.TextMatrix(0, lngColTotalPayments, FCGrid.AlignmentSettings.flexAlignRightCenter.ToString());
		}
	}
}
