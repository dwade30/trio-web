﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using Global;
using SharedApplication;
using SharedApplication.TaxCollections.Interfaces;

namespace TWCL0000
{
    
	public partial class frmRateRecChoice : BaseForm
    {
        private ITaxRateChoiceCommand continueCommand;
		public frmRateRecChoice()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        public void SetCommand(ITaxRateChoiceCommand commandObject)
        {
            continueCommand = commandObject;
        }

		private void InitializeComponentEx()
		{
			this.chkPrint = new System.Collections.Generic.List<fecherFoundation.FCCheckBox>();
			this.txtRange = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.chkPrint.AddControlArrayElement(chkPrint_3, 3);
			this.chkPrint.AddControlArrayElement(chkPrint_2, 2);
			this.chkPrint.AddControlArrayElement(chkPrint_1, 1);
			this.chkPrint.AddControlArrayElement(chkPrint_0, 0);
			this.txtRange.AddControlArrayElement(txtRange_1, 1);
			this.txtRange.AddControlArrayElement(txtRange_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmRateRecChoice InstancePtr
		{
			get
			{
				return (frmRateRecChoice)Sys.GetInstance(typeof(frmRateRecChoice));
			}
		}

		protected frmRateRecChoice _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/20/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               10/13/2006              *
		// ********************************************************
		public int intRateType;
		// this will be set by the form calling this form and will tell which form this should be setup for
		public int intYear;
		public int lngRateRecNumber;
		public string auxCmbLienItem0 = "All Records";
		public string auxCmbLienItem1 = "Pre Lien Only";
		int intLienedAccounts;
		clsDRWrapper rsRateRec = new clsDRWrapper();
		bool boolLoaded;
		int intAction;
		int lngCommitmentDateCol;
		// these are the column numbers for each variable so that they can be
		int lngBillingDateCol;
		// moved with minimal effort, since I know they will be (haha)
		int lngDueDate1Col;
		int lngStatusCol;
		int lngDescriptionCol;
		int lngHiddenCol;
		int lngLienCol;
		bool boolFormatting;
		bool boolUnloadMe;
		public string strRateKeyList = "";
		bool boolReminderInitial;
		bool boolLoading;

		private string BuildSQL()
		{
			string BuildSQL = "";
			// this function will determine from intRateType which report is calling it and will
			// create the SQL string accordingly then pass it back
			switch (intRateType)
			{
			// ******************************** Lien Process *********************************
				case 0:
					{
						// this means that nothing was setup
						BuildSQL = "SELECT * FROM RateRec WHERE ID = 0";
						// this should produce 0 records
						break;
					}
				case 1:
					{
						// Lien Edit Report
						if (cmbLien.SelectedIndex == 0)
						{
							// all accounts
							BuildSQL = "SELECT * FROM RateRec WHERE [Year] = " + FCConvert.ToString(intYear);
						}
						else if (cmbLien.SelectedIndex == 1)
						{
							// pre lien
							BuildSQL = "SELECT * FROM RateRec WHERE [Year] = " + FCConvert.ToString(intYear) + " AND RateType <> 'L'";
						}
						else if (cmbLien.SelectedIndex == 2)
						{
							// liened accounts
							this.Text = "Select Lien Rate Record";
							BuildSQL = "SELECT * FROM RateRec WHERE [Year] = " + FCConvert.ToString(intYear) + " AND RateType = 'L'";
						}
						else if (cmbLien.SelectedIndex == 3)
						{
							// eligible for 30 day notices
							BuildSQL = "SELECT * FROM RateRec WHERE [Year] = " + FCConvert.ToString(intYear) + " AND RateType <> 'L'";
						}
						else if (cmbLien.SelectedIndex == 4)
						{
							// eligible for liens
							BuildSQL = "SELECT * FROM RateRec WHERE [Year] = " + FCConvert.ToString(intYear) + " AND RateType <> 'L'";
						}
						else if (cmbLien.SelectedIndex == 5)
						{
							// eligible for lien maturity
							this.Text = "Select Lien Rate Record";
							BuildSQL = "SELECT * FROM RateRec WHERE [Year] = " + FCConvert.ToString(intYear) + " AND RateType = 'L'";
						}
						break;
					}
				case 2:
					{
						// 30 Day Process
						BuildSQL = "SELECT * FROM RateRec WHERE [Year] = " + FCConvert.ToString(intYear) + " AND RateType <> 'L'";
						break;
					}
				case 3:
					{
						// Tax Lien Process
						BuildSQL = "SELECT * FROM RateRec WHERE [Year] = " + FCConvert.ToString(intYear) + " AND RateType <> 'L'";
						break;
					}
				case 4:
					{
						// Lien Maturity Notice
						this.Text = "Select Lien Rate Record";
						BuildSQL = "SELECT * FROM RateRec WHERE [Year] = " + FCConvert.ToString(intYear) + " AND RateType = 'L'";
						break;
					}
				case 5:
					{
						// Apply Demand Fees
						BuildSQL = "SELECT * FROM RateRec WHERE [Year] = " + FCConvert.ToString(intYear) + " AND RateType <> 'L'";
						break;
					}
				case 6:
					{
						// Transfer Tax To Lien (Lien Record)
						this.Text = "Select Lien Rate Record";
						BuildSQL = "SELECT * FROM RateRec WHERE [Year] = " + FCConvert.ToString(intYear) + " AND RateType = 'L'";
						break;
					}
				case 61:
					{
						// Transfer Tax To Lien (Regular Records)
						this.Text = "Select Rate Records To Transfer";
						BuildSQL = "SELECT * FROM RateRec WHERE [Year] = " + FCConvert.ToString(intYear) + " AND RateType <> 'L'";
						break;
					}
				case 7:
					{
						// Apply Lien Maturity Notice
						this.Text = "Select Lien Rate Record";
						BuildSQL = "SELECT * FROM RateRec WHERE [Year] = " + FCConvert.ToString(intYear) + " AND RateType = 'L'";
						// ********************************    Labels    *********************************
						break;
					}
				case 10:
					{
						// Demand Labels
						BuildSQL = "SELECT * FROM RateRec WHERE [Year] = " + FCConvert.ToString(intYear) + " AND RateType <> 'L'";
						break;
					}
				case 11:
					{
						// Lien Process Labels
						BuildSQL = "SELECT * FROM RateRec WHERE [Year] = " + FCConvert.ToString(intYear) + " AND RateType <> 'L'";
						break;
					}
				case 12:
					{
						// Lien Maturity Labels
						this.Text = "Select Lien Rate Record";
						BuildSQL = "SELECT * FROM RateRec WHERE [Year] = " + FCConvert.ToString(intYear) + " AND RateType = 'L'";
						// ********************************  Mail Forms  *********************************
						break;
					}
				case 20:
					{
						// Demand Mail Forms
						BuildSQL = "SELECT * FROM RateRec WHERE [Year] = " + FCConvert.ToString(intYear) + " AND RateType <> 'L'";
						break;
					}
				case 21:
					{
						// Lien Process Forms
						BuildSQL = "SELECT * FROM RateRec WHERE [Year] = " + FCConvert.ToString(intYear) + " AND RateType <> 'L'";
						break;
					}
				case 22:
					{
						// Lien Maturity Forms
						this.Text = "Select Lien Rate Record";
						BuildSQL = "SELECT * FROM RateRec WHERE [Year] = " + FCConvert.ToString(intYear) + " AND RateType = 'L'";
						// *******************************Reminder Notices********************************
						break;
					}
				case 30:
					{
						// Reminder Notices
						if (cmbReminderLien.SelectedIndex == 0)
						{
							BuildSQL = "SELECT * FROM RateRec WHERE [Year] = " + FCConvert.ToString(intYear) + " AND RateType <> 'L'";
						}
						else
						{
							BuildSQL = "SELECT * FROM RateRec WHERE [Year] = " + FCConvert.ToString(intYear) + " AND RateType = 'L'";
						}
						// *******************************  Period Report  *******************************
						break;
					}
				case 50:
					{
						BuildSQL = "SELECT * FROM RateRec WHERE [Year] = " + FCConvert.ToString(intYear) + " AND RateType <> 'L'";
						// ******************************Reverse 30 DN Fees*******************************
						break;
					}
				case 60:
					{
						BuildSQL = "SELECT * FROM RateRec WHERE [Year] = " + FCConvert.ToString(intYear) + " AND RateType <> 'L'";
						// *******************************   Load Back    ********************************
						break;
					}
				case 100:
					{
						// this is to select a ratekey for the load back function
						BuildSQL = "SELECT * FROM RateRec WHERE [Year] = " + FCConvert.ToString(intYear);
						break;
					}
				case 200:
					{
						// this is the edit type
						if (modStatusPayments.Statics.boolRE)
						{
							BuildSQL = "SELECT * FROM RateRec WHERE (RateType = 'R' OR RateType = 'L' OR RateType = 'S') AND [Year] = " + FCConvert.ToString(intYear);
						}
						else
						{
							BuildSQL = "SELECT * FROM RateRec WHERE (RateType = 'R' OR RateType = 'S') AND [Year] = " + FCConvert.ToString(intYear);
						}
						break;
					}
				default:
					{
						BuildSQL = "SELECT * FROM RateRec WHERE ID = 0";
						// this should produce 0 records
						break;
					}
			}
			//end switch
			if (intRateType < 200)
			{
				if (modStatusPayments.Statics.boolRE)
				{
					BuildSQL = BuildSQL + " AND (RateType = 'R' OR RateType = 'S' OR RateType = 'L')";
				}
				else
				{
					BuildSQL = BuildSQL + " AND (RateType = 'R' OR RateType = 'S')";
				}
			}
			return BuildSQL;
		}

		private void cmbReportDetail_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbReportDetail.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 400, 0);
		}

		private void cmbReportDetail_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Space)
			{
				if (modAPIsConst.SendMessageByNum(cmbReportDetail.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
				{
					modAPIsConst.SendMessageByNum(cmbReportDetail.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
					KeyCode = 0;
				}
			}
		}

		private void cmbYear_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbYear.SelectedIndex > -1)
			{
				if (intYear != Conversion.Val(cmbYear.Items[cmbYear.SelectedIndex].ToString()))
				{
					intYear = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbYear.Items[cmbYear.SelectedIndex].ToString())));
					// save the last year selected for the default year
					//modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "LastRateRecChoiceYear", intYear.ToString());
					modGlobal.Statics.setCont.SaveSetting(intYear.ToString(), "LastRateRecChoiceYear", "", "", "", "");
					if (intRateType == 30)
					{
						// this will force the check of the liened years first
						boolReminderInitial = true;
						if (cmbReminderLien.SelectedIndex == 1)
						{
							ChangeRateKeys();
						}
						else
						{
							cmbReminderLien.SelectedIndex = 1;
						}
					}
					else
					{
						// update the rate grid
						ChangeRateKeys();
					}
				}
			}
			else
			{
				intYear = 0;
			}
		}

		private void cmbYear_Enter(object sender, System.EventArgs e)
		{
			if (fraRateInfo.Visible)
				fraRateInfo.Visible = false;
		}

		private void cmdRIClose_Click(object sender, System.EventArgs e)
		{
			fraRateInfo.Visible = false;
		}

		private void frmRateRecChoice_Activated(object sender, System.EventArgs e)
		{
			if (boolUnloadMe || intRateType == -1)
			{
				Close();
				return;
			}
			if (!boolLoaded)
			{
				boolLoaded = true;
				// show that the form was loaded
				boolFormatting = true;
				txtMailDate.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				if (!cmbPrint.Visible)
				{
					SetAct(0, false);
					// load the grid
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				boolFormatting = false;
				if (!modGlobalConstants.Statics.boolMaxForms)
				{
					this.Width = this.Width + 1;
				}
			}
			if (intRateType == 30)
			{
				boolReminderInitial = true;
			}
			// this will only show the option if it is transfer tax to lien
			if (intRateType == 6 || intRateType == 100)
			{
				cmdFileAddRateKey.Visible = true;
			}
			else
			{
				cmdFileAddRateKey.Visible = false;
			}
		}

		public void Form_Activate()
		{
			frmRateRecChoice_Activated(this, new System.EventArgs());
		}

		private void frmRateRecChoice_Enter(object sender, System.EventArgs e)
		{
			if (fraRateInfo.Visible)
				fraRateInfo.Visible = false;
		}

		private void frmRateRecChoice_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Return)
			{
				KeyCode = 0;
				Support.SendKeys("{Tab}", false);
			}
			else if (KeyCode == Keys.Escape)
			{
				KeyCode = 0;
				if (fraRateInfo.Visible)
				{
					fraRateInfo.Visible = false;
				}
				else
				{
					Close();
				}
			}
			else if (KeyCode == Keys.F10)
			{
				// turn off the F10 key
				KeyCode = 0;
			}
		}

		private void frmRateRecChoice_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmRateRecChoice.Icon	= "frmRateRecChoice.frx":0000";
			//frmRateRecChoice.ScaleWidth	= 9045;
			//frmRateRecChoice.ScaleHeight	= 7125;
			//frmRateRecChoice.LinkTopic	= "Form1";
			//frmRateRecChoice.LockControls	= -1  'True;
			//Font.Size	= "9.75";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//vsRateInfo.BackColor	= "-2147483643";
			//			//vsRateInfo.ForeColor	= "-2147483640";
			//vsRateInfo.BorderStyle	= 1;
			//vsRateInfo.FillStyle	= 0;
			//vsRateInfo.Appearance	= 1;
			//vsRateInfo.GridLines	= 1;
			//vsRateInfo.WordWrap	= 0;
			//vsRateInfo.ScrollBars	= 0;
			//vsRateInfo.RightToLeft	= 0;
			//vsRateInfo._ConvInfo	= 1;
			//vsRateInfo.MousePointer	= 0;
			//vsRateInfo.BackColorFixed	= -2147483633;
			//			//vsRateInfo.ForeColorFixed	= -2147483630;
			//vsRateInfo.BackColorSel	= -2147483635;
			//			//vsRateInfo.ForeColorSel	= -2147483634;
			//vsRateInfo.BackColorBkg	= -2147483636;
			//vsRateInfo.BackColorAlternate	= -2147483643;
			//vsRateInfo.GridColor	= -2147483633;
			//vsRateInfo.GridColorFixed	= -2147483632;
			//vsRateInfo.TreeColor	= -2147483632;
			//vsRateInfo.FloodColor	= 192;
			//vsRateInfo.SheetBorder	= -2147483642;
			//vsRateInfo.FocusRect	= 1;
			//vsRateInfo.HighLight	= 0;
			//vsRateInfo.AllowSelection	= -1  'True;
			//vsRateInfo.AllowBigSelection	= -1  'True;
			//vsRateInfo.AllowUserResizing	= 0;
			//vsRateInfo.SelectionMode	= 0;
			//vsRateInfo.GridLinesFixed	= 2;
			//vsRateInfo.GridLineWidth	= 1;
			//vsRateInfo.RowHeightMin	= 0;
			//vsRateInfo.RowHeightMax	= 0;
			//vsRateInfo.ColWidthMin	= 0;
			//vsRateInfo.ColWidthMax	= 0;
			//vsRateInfo.ExtendLastCol	= 0   'False;
			//vsRateInfo.FormatString	= "";
			//vsRateInfo.ScrollTrack	= -1  'True;
			//vsRateInfo.ScrollTips	= 0   'False;
			//vsRateInfo.MergeCells	= 0;
			//vsRateInfo.MergeCompare	= 0;
			//vsRateInfo.AutoResize	= -1  'True;
			//vsRateInfo.AutoSizeMode	= 0;
			//vsRateInfo.AutoSearch	= 0;
			//vsRateInfo.MultiTotals	= -1  'True;
			//vsRateInfo.SubtotalPosition	= 1;
			//vsRateInfo.OutlineBar	= 0;
			//vsRateInfo.OutlineCol	= 0;
			//vsRateInfo.Ellipsis	= 0;
			//vsRateInfo.ExplorerBar	= 0;
			//vsRateInfo.PicturesOver	= 0   'False;
			//vsRateInfo.PictureType	= 0;
			//vsRateInfo.TabBehavior	= 0;
			//vsRateInfo.OwnerDraw	= 0;
			//vsRateInfo.ShowComboButton	= -1  'True;
			//vsRateInfo.TextStyle	= 0;
			//vsRateInfo.TextStyleFixed	= 0;
			//vsRateInfo.OleDragMode	= 0;
			//vsRateInfo.OleDropMode	= 0;
			//vsRateInfo.DataMode	= 0;
			//vsRateInfo.VirtualData	= -1  'True;
			//txtMailDate.BorderStyle	= 1;
			//txtMailDate.Appearance	= 1;
			//vsRate.BackColor	= "-2147483643";
			//			//vsRate.ForeColor	= "-2147483640";
			//vsRate.BorderStyle	= 1;
			//vsRate.FillStyle	= 0;
			//vsRate.Appearance	= 1;
			//vsRate.GridLines	= 1;
			//vsRate.WordWrap	= 0;
			//vsRate.ScrollBars	= 3;
			//vsRate.RightToLeft	= 0;
			//vsRate._cx	= 15266;
			//vsRate._cy	= 471;
			//vsRate._ConvInfo	= 1;
			//vsRate.MousePointer	= 0;
			//vsRate.BackColorFixed	= -2147483633;
			//			//vsRate.ForeColorFixed	= -2147483630;
			//vsRate.BackColorSel	= -2147483635;
			//			//vsRate.ForeColorSel	= -2147483634;
			//vsRate.BackColorBkg	= -2147483636;
			//vsRate.BackColorAlternate	= -2147483643;
			//vsRate.GridColor	= -2147483633;
			//vsRate.GridColorFixed	= -2147483632;
			//vsRate.TreeColor	= -2147483632;
			//vsRate.FloodColor	= 192;
			//vsRate.SheetBorder	= -2147483642;
			//vsRate.FocusRect	= 1;
			//vsRate.HighLight	= 0;
			//vsRate.AllowSelection	= -1  'True;
			//vsRate.AllowBigSelection	= -1  'True;
			//vsRate.AllowUserResizing	= 0;
			//vsRate.SelectionMode	= 0;
			//vsRate.GridLinesFixed	= 2;
			//vsRate.GridLineWidth	= 1;
			//vsRate.RowHeightMin	= 0;
			//vsRate.RowHeightMax	= 0;
			//vsRate.ColWidthMin	= 0;
			//vsRate.ColWidthMax	= 0;
			//vsRate.ExtendLastCol	= -1  'True;
			//vsRate.FormatString	= "";
			//vsRate.ScrollTrack	= -1  'True;
			//vsRate.ScrollTips	= 0   'False;
			//vsRate.MergeCells	= 0;
			//vsRate.MergeCompare	= 0;
			//vsRate.AutoResize	= -1  'True;
			//vsRate.AutoSizeMode	= 0;
			//vsRate.AutoSearch	= 0;
			//vsRate.AutoSearchDelay	= 2;
			//vsRate.MultiTotals	= -1  'True;
			//vsRate.SubtotalPosition	= 1;
			//vsRate.OutlineBar	= 0;
			//vsRate.OutlineCol	= 0;
			//vsRate.Ellipsis	= 0;
			//vsRate.ExplorerBar	= 0;
			//vsRate.PicturesOver	= 0   'False;
			//vsRate.PictureType	= 0;
			//vsRate.TabBehavior	= 0;
			//vsRate.OwnerDraw	= 0;
			//vsRate.ShowComboButton	= -1  'True;
			//vsRate.TextStyle	= 0;
			//vsRate.TextStyleFixed	= 0;
			//vsRate.OleDragMode	= 0;
			//vsRate.OleDropMode	= 0;
			//vsRate.ComboSearch	= 3;
			//vsRate.AutoSizeMouse	= -1  'True;
			//vsRate.AllowUserFreezing	= 0;
			//vsRate.BackColorFrozen	= 0;
			//			//vsRate.ForeColorFrozen	= 0;
			//vsRate.WallPaperAlignment	= 9;
			//vsElasticLight1.OleObjectBlob	= "frmRateRecChoice.frx":058A";
			//End Unmaped Properties
			modGlobalFunctions.SetTRIOColors(this);
			// set the TRIO colors for the controls on this form
			// this will set the default column numbers
			lngCommitmentDateCol = 1;
			lngBillingDateCol = 2;
			lngDueDate1Col = 3;
			lngStatusCol = 4;
			lngDescriptionCol = 5;
			lngHiddenCol = 6;
			lngLienCol = 7;
			boolFormatting = true;
			// this will make sure that the form knows that I am formatting it instead of the user changing the size of the form
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			// set the size of the form
			App.DoEvents();
			FormatGrid();
			// format the grid
			intYear = 0;
			// lblReprint.Text = "Choose inital run only if accounts have changed."
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			boolLoading = true;
			if (FillYearCombo())
			{
				if (intRateType >= 10 && intRateType < 100 && intRateType != 30 && intRateType != 50 && intRateType != 60)
				{
					vsRate.Visible = false;
					//lblRate.Visible = false;
					fraQuestions.Visible = false;
					cmbPrint.Visible = true;
					lblPrintType.Visible = true;
					fraPrintOptions.Visible = false;
				}
				else
				{
					// + 100
					if (intRateType == 100 || intRateType == 200)
					{
						fraPrintOptions.Visible = false;
					}
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				boolFormatting = false;
			}
			else
			{
				boolFormatting = false;
				boolUnloadMe = true;
			}
			boolLoading = false;
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
			boolUnloadMe = false;
			intAction = 0;
		}

		private void SetAct(short intAct, bool boolQuery = true)
		{
			int intErrCode = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strTemp = "";
				int intTemp;
				// this will set up the grid and show/hide all of the frames depending on intRateType
				// set the cursor to the hourglass
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				intErrCode = 1;
				if (intRateType >= 1 && intRateType <= 9)
				{
					// pre lien edit report
					intErrCode = 2;
					if (intRateType == 1)
					{
						// Or intRateType = 2 Then
						cmbLien.Visible = true;
						fraPrintOptions.Visible = true;
						cmbExtraLines.Clear();
						cmbExtraLines.AddItem("0");
						cmbExtraLines.AddItem("1");
						cmbExtraLines.AddItem("2");
						cmbExtraLines.AddItem("3");
						cmbExtraLines.AddItem("4");
						cmbExtraLines.AddItem("5");
						cmbExtraLines.AddItem("6");
						intErrCode = 3;
						lblEligible.Visible = true;
						intErrCode = 4;
						FillReportDetailCombo();
						intErrCode = 5;
						intErrCode = 6;
						if (intRateType == 1)
						{
							fraPreLienEdit.Visible = true;
							fraPreLienEdit.Enabled = true;
							intErrCode = 7;
							// set the default mail date from the last time this was entered
							modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "LastLienEditReportDate", ref strTemp);
							intErrCode = 8;
							if (Strings.Trim(strTemp) != "")
							{
								if (Information.IsDate(strTemp))
								{
									txtMailDate.Text = strTemp;
								}
								else
								{
									// do nothing
								}
							}
							else
							{
								// do nothing
							}
						}
						intErrCode = 9;
						intErrCode = 10;
						// this is to show the Initial or Reprint message
						// fraReprint.Visible = True
						// fraReprint.Enabled = True
					}
					else
					{
						fraPrintOptions.Visible = false;
						cmbRePrint.Visible = false;
						lblRePrint.Visible = false;
						cmbRePrint.Enabled = false;
						cmbLien.Visible = false;
						lblEligible.Visible = false;
					}
					intErrCode = 11;
					// show the questions - need to shift this to the left so that fraReprint can fit
					fraQuestions.Visible = true;
					fraQuestions.Enabled = true;
					intErrCode = 12;
					// position the grid
					vsRate.Visible = true;
					//lblRate.Visible = true;
					vsRate.Enabled = true;
					intErrCode = 13;
					// + 100
					intErrCode = 14;
					fraRange.Visible = true;
					fraRange.Enabled = true;
					cmbRange.Visible = true;
					lblRange.Visible = true;
					cmbRange.Enabled = true;
					cmbReminderLien.Visible = false;
					lblReminderLien.Visible = false;
				}
				else if (intRateType == 100)
				{
					cmbLien.Visible = false;
					intErrCode = 15;
					lblEligible.Visible = false;
					intErrCode = 16;
					// position the grid
					vsRate.Visible = true;
					//lblRate.Visible = true;
					vsRate.Enabled = true;
					intErrCode = 17;
					// hide the questions
					fraQuestions.Visible = true;
					fraQuestions.Enabled = true;
					intErrCode = 18;
					// hide the reprint frame
					cmbRePrint.Visible = false;
					lblRePrint.Visible = false;
					fraPreLienEdit.Visible = false;
					cmbReminderLien.Visible = false;
					lblReminderLien.Visible = false;
				}
				else if (intRateType == 30)
				{
					if (cmbLien.Items[0].ToString() == auxCmbLienItem0)
					{
						cmbLien.Items.RemoveAt(0);
					}
					if (cmbLien.Items[1].ToString() == auxCmbLienItem1)
					{
						cmbLien.Items.RemoveAt(1);
					}
					cmbReminderLien.Visible = true;
					lblReminderLien.Visible = true;
					intErrCode = 120;
					lblEligible.Visible = false;
					cmbRePrint.Visible = false;
					lblRePrint.Visible = false;
					cmbRePrint.Enabled = false;
					intErrCode = 121;
					cmbLien.Visible = false;
					intErrCode = 122;
					// position the grid
					vsRate.Visible = true;
					//lblRate.Visible = true;
					intErrCode = 123;
					cmbPrint.Visible = false;
					lblPrintType.Visible = false;
					fraPrintOptions.Visible = false;
					// show the questions
					fraQuestions.Visible = true;
					fraQuestions.Enabled = true;
					intErrCode = 124;
					// hide the reprint frame
					cmbRePrint.Visible = false;
					lblRePrint.Visible = false;
					fraPreLienEdit.Visible = false;
				}
				else if ((intRateType == 50) || (intRateType == 60))
				{
					if (cmbLien.Items[0].ToString() == auxCmbLienItem0)
					{
						cmbLien.Items.RemoveAt(0);
					}
					if (cmbLien.Items[1].ToString() == auxCmbLienItem1)
					{
						cmbLien.Items.RemoveAt(1);
					}
					intErrCode = 120;
					lblEligible.Visible = false;
					cmbRePrint.Visible = false;
					lblRePrint.Visible = false;
					cmbRePrint.Enabled = false;
					intErrCode = 121;
					cmbLien.Visible = false;
					intErrCode = 122;
					// position the grid
					vsRate.Visible = true;
					//lblRate.Visible = true;
					intErrCode = 123;
					cmbPrint.Visible = false;
					lblPrintType.Visible = false;
					fraPrintOptions.Visible = false;
					// show the questions
					fraQuestions.Visible = true;
					fraQuestions.Enabled = true;
					intErrCode = 124;
					// hide the reprint frame
					cmbRePrint.Visible = false;
					lblRePrint.Visible = false;
					fraPreLienEdit.Visible = false;
					if (intRateType == 50)
					{
						cmbPeriod.Visible = true;
						lblPeriod.Visible = true;
					}
					cmbReminderLien.Visible = false;
					lblReminderLien.Visible = false;
				}
				else
				{
					intErrCode = 19;
					if (cmbLien.Items[0].ToString() == auxCmbLienItem0)
					{
						cmbLien.Items.RemoveAt(0);
					}
					if (cmbLien.Items[1].ToString() == auxCmbLienItem1)
					{
						cmbLien.Items.RemoveAt(1);
					}
					intErrCode = 20;
					lblEligible.Visible = false;
					cmbRePrint.Visible = false;
					lblRePrint.Visible = false;
					cmbRePrint.Enabled = false;
					intErrCode = 21;
					cmbLien.Visible = false;
					intErrCode = 22;
					// position the grid
					vsRate.Visible = true;
					//lblRate.Visible = true;
					intErrCode = 23;
					// show the questions
					fraQuestions.Visible = true;
					fraQuestions.Enabled = true;
					intErrCode = 24;
					// hide the reprint frame
					cmbRePrint.Visible = false;
					lblRePrint.Visible = false;
					cmbReminderLien.Visible = false;
					lblReminderLien.Visible = false;
					fraPreLienEdit.Visible = false;
				}
				cmbYear.Visible = true;
				cmbYear.Enabled = true;
				if (cmbYear.Visible)
				{
					// set the default year from the last time this was entered
					intErrCode = 25;
					//modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "LastRateRecChoiceYear", ref strTemp);
					strTemp = modGlobal.Statics.setCont.GetSettingValue("LastRateRecChoiceYear", "", "", "", "");
					if (cmbYear.Items.Count > 0)
					{
						// set the default year if it is in the list
						for (intTemp = 0; intTemp <= cmbYear.Items.Count - 1; intTemp++)
						{
							if (cmbYear.Items[intTemp].ToString() == strTemp)
							{
								cmbYear.SelectedIndex = intTemp;
								break;
							}
							else
							{
								// If cmbYear.ListCount > 0 Then
								// cmbYear.ListIndex = 0
								// End If
							}
						}
					}
					intErrCode = 26;
					cmbYear.Focus();
				}
				// hide/show the menu options
				btnProcess.Visible = true;
				intErrCode = 27;
				if (boolQuery)
				{
					App.DoEvents();
					CreateBillMasterYearQuery();
				}
				intErrCode = 28;
				// set the cursor back to the default
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Setup Error - " + FCConvert.ToString(intErrCode));
			}
		}

		private void frmRateRecChoice_Resize(object sender, System.EventArgs e)
		{
			if (!boolFormatting)
			{
				//FC:FINAL:AM:#i88 - don't format the grid again
				//if (!boolLoaded)
				//{
				//	FormatGrid();
				//}
				// SetAct intAction        'this will move everything to the correct place after the resize
				// set the height of the grid after resizing
				if (vsRate.Rows > 1)
				{
					if (vsRate.Rows == 2 || (intRateType == 6 && vsRate.Rows == 3))
					{
						// this only has one rate record, so select it automatically
						vsRate.TextMatrix(1, 0, FCConvert.ToString(-1));
					}
				}
				switch (intRateType)
				{
					case 1:
						{
							break;
						}
					default:
						{
							// if it is not a lien edit report then resize fraQuestions to the smaller version
							cmbRePrint.Visible = false;
							lblRePrint.Visible = false;
							cmbRePrint.Enabled = false;
							cmbLien.Visible = false;
							lblEligible.Visible = false;
							break;
						}
				}
				//end switch
			}
		}

		private void LoadGrid()
		{
			// this will take a recordset with records and fill the grid with it
			vsRate.Rows = 1;
			vsRate.Cols = 8;
			while (!rsRateRec.EndOfFile())
			{
				vsRate.AddItem("");
				vsRate.TextMatrix(vsRate.Rows - 1, lngBillingDateCol, rsRateRec.Get_Fields_DateTime("BillingDate"));
				vsRate.TextMatrix(vsRate.Rows - 1, lngDueDate1Col, rsRateRec.Get_Fields_DateTime("DueDate1"));
				vsRate.TextMatrix(vsRate.Rows - 1, lngDescriptionCol, FCConvert.ToString(rsRateRec.Get_Fields_String("Description")));
				vsRate.TextMatrix(vsRate.Rows - 1, lngHiddenCol, FCConvert.ToString(rsRateRec.Get_Fields_Int32("ID")));
				// RateKey")
				vsRate.TextMatrix(vsRate.Rows - 1, lngLienCol, FCConvert.ToString(rsRateRec.Get_Fields_String("RateType")));
				if (FCConvert.ToString(rsRateRec.Get_Fields_String("RateType")) == "L")
				{
					// change the backcolor to grey
					vsRate.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsRate.Rows - 1, 0, vsRate.Rows - 1, vsRate.Cols - 1, Information.RGB(230, 230, 230));
				}
				else
				{
					vsRate.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsRate.Rows - 1, 0, vsRate.Rows - 1, vsRate.Cols - 1, System.Drawing.Color.White);
				}
				if (rsRateRec.Get_Fields_DateTime("CommitmentDate") is DateTime)
				{
					vsRate.TextMatrix(vsRate.Rows - 1, lngCommitmentDateCol, rsRateRec.Get_Fields_DateTime("CommitmentDate"));
				}
				else
				{
					vsRate.TextMatrix(vsRate.Rows - 1, lngCommitmentDateCol, "");
					vsRate.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsRate.Rows - 1, lngCommitmentDateCol, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				}
				rsRateRec.MoveNext();
			}
			if (intRateType == 6)
			{
				vsRate.AddItem("\t" + "Add New" + "\t" + "Add New" + "\t" + "Add New" + "\t" + "Add New");
			}
			// this sets the height
			if (vsRate.Rows > 1)
			{
				//FC:FINAL:SBE - #128 - update grid size, based on rows count
				var vsRateGrid = vsRate as DataGridView;
				int visibleRowsCount = vsRateGrid.Rows.GetRowCount(DataGridViewElementStates.Visible);
				int rows = visibleRowsCount > 5 ? 5 : visibleRowsCount;
				vsRate.Height = (rows * vsRateGrid.Rows[0].Height) + vsRateGrid.ColumnHeadersHeight;
				//FC:FINAL:JEI:IT443 avoid scroll bars
				vsRate.Height += 2;
				if (vsRate.Rows == 2 || (intRateType == 6 && vsRate.Rows == 3))
				{
					// this only has one rate record, so select it automatically
					vsRate.TextMatrix(1, 0, FCConvert.ToString(-1));
				}
			}
		}

		private void CreateBillMasterYearQuery()
		{
			clsDRWrapper rsCreate = new clsDRWrapper();
			string strWhereClause = "";
			int intCT;
			string strSelect = "";
			string strFields = "";
			if (intRateType == 4 || intRateType == 7 || intRateType == 12 || intRateType == 22)
			{
				// this will inner join the lien rec
				for (intCT = 1; intCT <= vsRate.Rows - 1; intCT++)
				{
					if (Conversion.Val(vsRate.TextMatrix(intCT, 0)) == -1)
					{
						if (Strings.Trim(strWhereClause) == "")
						{
							strWhereClause = "(LienRec.RateKey = " + vsRate.TextMatrix(intCT, lngHiddenCol) + " OR BillingMaster.RateKey = " + vsRate.TextMatrix(intCT, lngHiddenCol);
						}
						else
						{
							strWhereClause += " OR LienRec.RateKey = " + vsRate.TextMatrix(intCT, lngHiddenCol) + " OR BillingMaster.RateKey = " + vsRate.TextMatrix(intCT, lngHiddenCol);
						}
					}
				}
				strFields = "LienRec.ID, LienRec.RateKey, Account, LienRecordNumber, LienStatusEligibility, LienProcessStatus, LienProcessExclusion, BillingType, TaxDue1, TaxDue2, TaxDue3, TaxDue4";
				if (Strings.Trim(strWhereClause) != "")
				{
					strWhereClause += ")";
					App.DoEvents();
					// rsCreate.CreateStoredProcedure "BillingMasterYear", "SELECT * FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.LienRecordNumber WHERE " & strWhereClause, strCLDatabase
					// kgk - duplicate ID in subquery:  gstrBillYrQuery = "SELECT * FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.ID WHERE " & strWhereClause
					modGlobal.Statics.gstrBillYrQuery = "SELECT " + strFields + " FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.ID WHERE " + strWhereClause;
				}
				else
				{
					App.DoEvents();
					// rsCreate.CreateStoredProcedure "BillingMasterYear", "SELECT * FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.LienRecordNumber"
					modGlobal.Statics.gstrBillYrQuery = "SELECT * FROM BillingMaster b INNER JOIN LienRec lr ON BillingMaster.LienRecordNumber = LienRec.ID";
				}
			}
			else
			{
				// this is for regular non-lien accounts
				// For intCT = 1 To vsRate.rows - 1
				// If Val(vsRate.TextMatrix(intCT, 0)) = -1 Then
				// If Trim(strWhereClause) = "" Then
				// strWhereClause = strWhereClause & " AND (RateKey = " & vsRate.TextMatrix(intCT, lngHiddenCol)
				// strRateKeyList = " AND (LienRec.RateKey = " & vsRate.TextMatrix(intCT, lngHiddenCol)
				// Else
				// strWhereClause = strWhereClause & " OR RateKey = " & vsRate.TextMatrix(intCT, lngHiddenCol)
				// strRateKeyList = strRateKeyList & " OR LienRec.RateKey = " & vsRate.TextMatrix(intCT, lngHiddenCol)
				// End If
				// End If
				// Next
				for (intCT = 1; intCT <= vsRate.Rows - 1; intCT++)
				{
					if (Conversion.Val(vsRate.TextMatrix(intCT, 0)) == -1)
					{
						if (Strings.Trim(strWhereClause) == "")
						{
							strWhereClause += " AND (RateKey = " + vsRate.TextMatrix(intCT, lngHiddenCol);
							strRateKeyList = " (" + vsRate.TextMatrix(intCT, lngHiddenCol) + ",";
						}
						else
						{
							strWhereClause += " OR RateKey = " + vsRate.TextMatrix(intCT, lngHiddenCol);
							strRateKeyList += vsRate.TextMatrix(intCT, lngHiddenCol) + ",";
						}
					}
				}
				if (Strings.Trim(strWhereClause) != "")
				{
					strRateKeyList = Strings.Left(strRateKeyList, strRateKeyList.Length - 1);
					// take the end comma off
					strWhereClause += ")";
					// add a parenthesis
					strRateKeyList += ")";
				}
				App.DoEvents();
				if (intLienedAccounts == 2 || intLienedAccounts == 5)
				{
					strSelect = "*";
					// rsCreate.CreateStoredProcedure "BillingMasterYear", "SELECT " & strSelect & " FROM LienRec INNER JOIN BillingMaster ON LienRec.LienRecordNumber = BillingMaster.LienRecordNumber WHERE LienRec.RateKey IN " & strRateKeyList, strCLDatabase        'Right(strRateKeyList, Len(strRateKeyList) - 4)
					modGlobal.Statics.gstrBillYrQuery = "SELECT " + strSelect + " FROM LienRec INNER JOIN BillingMaster ON LienRec.ID = BillingMaster.LienRecordNumber WHERE LienRec.RateKey IN " + strRateKeyList;
					// Right(strRateKeyList, Len(strRateKeyList) - 4)
				}
				else
				{
					// rsCreate.CreateStoredProcedure "BillingMasterYear", "SELECT * FROM BillingMaster WHERE BillingYear / 10 = " & intYear & strWhereClause, strCLDatabase
					modGlobal.Statics.gstrBillYrQuery = "SELECT * FROM BillingMaster WHERE BillingYear / 10 = " + FCConvert.ToString(intYear) + strWhereClause;
				}
			}
		}

		private void FormatGrid()
		{
			// this sub will set the format of the grid
			int wid = 0;
			vsRate.Cols = 8;
			vsRate.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			// set the widths
			wid = vsRate.WidthOriginal;
			vsRate.ColWidth(0, FCConvert.ToInt32(wid * 0.08));
			vsRate.ColWidth(lngCommitmentDateCol, FCConvert.ToInt32(wid * 0.18));
			// Commitment Date
			vsRate.ColWidth(lngBillingDateCol, FCConvert.ToInt32(wid * 0.18));
			// BillingDate
			vsRate.ColWidth(lngDueDate1Col, FCConvert.ToInt32(wid * 0.18));
			// DueDate1
			vsRate.ColWidth(lngStatusCol, FCConvert.ToInt32(wid * 0.0));
			// Status
			vsRate.ColWidth(lngDescriptionCol, FCConvert.ToInt32(wid * 0.38));
			// Description
			vsRate.ColWidth(lngHiddenCol, 0);
			vsRate.ColWidth(lngLienCol, 0);
			// set the alignment values
			//FC:FINAL:CHN - issue #1384: Change columns alignment to new design requirements.
			// vsRate.ColAlignment(lngCommitmentDateCol, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			// vsRate.ColAlignment(lngBillingDateCol, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			// vsRate.ColAlignment(lngDueDate1Col, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			// vsRate.ColAlignment(lngStatusCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			// vsRate.ColAlignment(lngDescriptionCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsRate.ColDataType(0, FCGrid.DataTypeSettings.flexDTBoolean);
			vsRate.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsRate.ColAlignment(lngCommitmentDateCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsRate.ColAlignment(lngBillingDateCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsRate.ColAlignment(lngDueDate1Col, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsRate.ColAlignment(lngStatusCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsRate.ColAlignment(lngDescriptionCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			// set the formatting
			vsRate.ColFormat(lngCommitmentDateCol, "MM/dd/yyyy");
			vsRate.ColFormat(lngBillingDateCol, "MM/dd/yyyy");
			vsRate.ColFormat(lngDueDate1Col, "MM/dd/yyyy");
			vsRate.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, 5, true);
			// vsRate.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 5, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsRate.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			// set the header titles
			vsRate.TextMatrix(0, 0, "Select");
			vsRate.TextMatrix(0, lngCommitmentDateCol, "Commitment");
			vsRate.TextMatrix(0, lngBillingDateCol, "Billing Date");
			vsRate.TextMatrix(0, lngDueDate1Col, "First Due Date");
			vsRate.TextMatrix(0, lngStatusCol, "Status");
			vsRate.TextMatrix(0, lngDescriptionCol, "Description");
		}

		private void mnuFileAddRateKey_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsDef = new clsDRWrapper();
				// VBto upgrade warning: dtCommit As DateTime	OnWriteFCConvert.ToInt16(
				DateTime dtCommit;
				// VBto upgrade warning: dtLienDate As DateTime	OnWriteFCConvert.ToInt16(
				DateTime dtLienDate;
				// VBto upgrade warning: dtBillDate As DateTime	OnWrite(DateTime, short)
				DateTime dtBillDate;
				this.Hide();
				switch (intRateType)
				{
					case 6:
						{
							// this will send the default information and the set the type of rate rec to create
							// find the default commitment date
							// kk09192014 trocl-1155  We want to look for default lien filing date, not the billing commitment date - Change to "RateType = 'L'" was 'R'
							//FC:FINAL:AM:#i88 - check for the index
							if (cmbYear.SelectedIndex != -1)
							{
								rsDef.OpenRecordset("SELECT * FROM RateRec WHERE [Year] = " + cmbYear.Items[cmbYear.SelectedIndex].ToString() + " AND RateType = 'L' ORDER BY CommitmentDate");
								if (rsDef.EndOfFile())
								{
									dtCommit = DateTime.FromOADate(0);
								}
								else
								{
									rsDef.FindFirstRecord("CommitmentDate", FCConvert.ToDateTime(0), "<>");
									if (rsDef.NoMatch)
									{
										dtCommit = DateTime.FromOADate(0);
									}
									else
									{
										dtCommit = (DateTime)rsDef.Get_Fields_DateTime("CommitmentDate");
									}
								}
							}
							else
							{
								dtCommit = DateTime.FromOADate(0);
							}
							// find the lien date
							rsDef.OpenRecordset("SELECT * FROM Control_LienProcess");
							if (!rsDef.EndOfFile())
							{
								if (rsDef.Get_Fields_DateTime("FilingDate") != null)
								{
									dtLienDate = (DateTime)rsDef.Get_Fields_DateTime("FilingDate");
								}
								else
								{
									dtLienDate = DateTime.FromOADate(0);
								}
							}
							else
							{
								dtLienDate = DateTime.FromOADate(0);
							}
							if (dtLienDate.ToOADate() != 0)
							{
								dtBillDate = DateAndTime.DateAdd("D", 1, dtLienDate);
							}
							else
							{
								dtBillDate = DateTime.FromOADate(0);
							}
							frmAuditInfo.InstancePtr.Init(10, FCConvert.ToInt16(cmbYear.Items[cmbYear.SelectedIndex].ToString()), dtCommit, dtLienDate, dtBillDate);
							break;
						}
					case 100:
						{
							frmAuditInfo.InstancePtr.Init(1);
							// load back
							break;
						}
					default:
						{
							frmAuditInfo.InstancePtr.Init(0);
							break;
						}
				}
				//end switch
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Adding Rate Record");
			}
		}

		public void mnuFileAddRateKey_Click()
		{
			mnuFileAddRateKey_Click(cmdFileAddRateKey, new System.EventArgs());
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strSQL = "";
				
				// check to see if this is an add ratekey row
				if (intRateType == 6)
				{
					if (Conversion.Val(vsRate.TextMatrix(vsRate.Rows - 1, 0)) == -1)
					{
						// this is trying to add a row
						mnuFileAddRateKey_Click();
						return;
					}
				}
				if (!cmbPrint.Visible)
				{
					if (!ValidateAnswers())
					{
						return;
					}
				}
				if (cmbLien.SelectedIndex == 0)
				{
					// all accounts
					intLienedAccounts = 0;
				}
				else if (cmbLien.SelectedIndex == 1)
				{
					// pre lien
					intLienedAccounts = 1;
				}
				else if (cmbLien.SelectedIndex == 2)
				{
					// liened accounts
					intLienedAccounts = 2;
				}
				else if (cmbLien.SelectedIndex == 3)
				{
					// eligible for 30 day notices
					// kgk - move   InitialSettings "30DAYNOTICE"
					intLienedAccounts = 3;
				}
				else if (cmbLien.SelectedIndex == 4)
				{
					// eligible for liens
					intLienedAccounts = 4;
					// kgk - move  InitialSettings "LIENPROCESS"
				}
				else if (cmbLien.SelectedIndex == 5)
				{
					// eligible for lien maturity
					intLienedAccounts = 5;
					// kgk - move  InitialSettings "LIENMATURITY"
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				CreateBillMasterYearQuery();
				// kgk 03-01-2012  Move this to before InitialSettings()
				// kgk 03-01-2012  Moved the InitialSettings down - need to have intLienedAccounts set before calling CreateBillMasterYearQuery
				if (cmbLien.SelectedIndex == 3)
				{
					// eligible for 30 day notices
					InitialSettings_2("30DAYNOTICE");
				}
				else if (cmbLien.SelectedIndex == 4)
				{
					// eligible for liens
					InitialSettings_2("LIENPROCESS");
				}
				else if (cmbLien.SelectedIndex == 5)
				{
					// eligible for lien maturity
					InitialSettings_2("LIENMATURITY");
				}
				if (!cmbPrint.Visible)
				{
					// this will check to see if the print type is being determined
					// if it is not then process normally
					if (vsRate.Enabled && vsRate.Visible)
					{
						vsRate.Select(0, 0);
					}
					App.DoEvents();
					// kgk 03-01-2012  Moved to before InitialSettings()        CreateBillMasterYearQuery
					// If intAction = 0 Then
					// SetAct 2
					// Else
					// this will load the next screen depending on how the user got here
					switch (intRateType)
					{
						case 0:
							{
								// this is nothing
								FCMessageBox.Show("The Rate Type was not setup.", MsgBoxStyle.Critical, "Rate Rec Error");
								// do nothing else...if this happens then there is a bug
								break;
							}
						case 1:
							{
								// lien process report
								if (ValidateAnswers())
								{
									this.Hide();
									App.DoEvents();
									arLienProcessEditReport.InstancePtr.StartLienEditReportQuery(FCConvert.ToInt16(intLienedAccounts), strRateKeyList);
									frmReportViewer.InstancePtr.Init(arLienProcessEditReport.InstancePtr);
									// arLienProcessEditReport.Show , MDIParent
								}
								break;
							}
						case 2:
							{
								// 30 day notice
								lngRateRecNumber = GetRateKey();
								if (ValidateAnswers())
								{
									this.Hide();
									Check30DNYear();
									modRhymalReporting.Statics.strFreeReportType = "30DAYNOTICE";
									frmFreeReport.InstancePtr.Show(App.MainForm);
									App.DoEvents();
									frmFreeReport.InstancePtr.Focus();
								}
								break;
							}
						case 3:
							{
								// lien process
								lngRateRecNumber = GetRateKey();
								if (ValidateAnswers())
								{
									this.Hide();
									modRhymalReporting.Statics.strFreeReportType = "LIENPROCESS";
									frmFreeReport.InstancePtr.Show(App.MainForm);
									App.DoEvents();
									frmFreeReport.InstancePtr.Focus();
								}
								break;
							}
						case 4:
							{
								// lien maturity
								lngRateRecNumber = GetRateKey();
								if (ValidateAnswers())
								{
									this.Hide();
									modRhymalReporting.Statics.strFreeReportType = "LIENMATURITY";
									strRateKeyList = RateKeyList();
									frmFreeReport.InstancePtr.Show(App.MainForm);
									App.DoEvents();
									frmFreeReport.InstancePtr.Focus();
								}
								break;
							}
						case 5:
							{
								// apply demand fees
								if (ValidateAnswers())
								{
									this.Hide();
									lngRateRecNumber = GetRateKey();
									if (modGlobal.FormExist(frmApplyDemandFees.InstancePtr))
									{
										frmApplyDemandFees.InstancePtr.Unload();
									}
									frmApplyDemandFees.InstancePtr.Init(RateKeyList());
								}
								break;
							}
						case 6:
							{
								// transfer tax to lien
								if (ValidateAnswers())
								{
									lngRateRecNumber = GetRateKey();
									// this will hold the lien record
									// now we need to get the regular rate records that will be used
									intRateType = 61;
									// change to regular rate records
									boolLoaded = false;
									Form_Activate();
									ChangeRateKeys();
									cmbYear.Enabled = false;
								}
								break;
							}
						case 61:
							{
								if (ValidateAnswers())
								{
									this.Hide();
									frmTransferTaxToLien.InstancePtr.Init(RateKeyList());
								}
								break;
							}
						case 7:
							{
								// apply lien maturity notice
								if (ValidateAnswers())
								{
									this.Hide();
									strRateKeyList = RateKeyList();
									// MAL@20071001
									if (modGlobal.FormExist(frmLienMaturityFees.InstancePtr))
									{
										frmLienMaturityFees.InstancePtr.Unload();
									}
									frmLienMaturityFees.InstancePtr.Show(App.MainForm);
								}
								break;
							}
						case 10:
							{
								// Demand Labels
								if (ValidateAnswers())
								{
									this.Hide();
									lngRateRecNumber = GetRateKey();
									strSQL = BuildLabelSQL_2(10);
									frmCustomLabels.InstancePtr.Init(strSQL, intRateType);
								}
								break;
							}
						case 11:
							{
								// Lien Process Labels
								if (ValidateAnswers())
								{
									this.Hide();
									lngRateRecNumber = GetRateKey();
									strSQL = BuildLabelSQL_2(11);
									frmCustomLabels.InstancePtr.Init(strSQL, intRateType);
								}
								break;
							}
						case 12:
							{
								// Lien Maturity Labels
								if (ValidateAnswers())
								{
									this.Hide();
									lngRateRecNumber = GetRateKey();
									strSQL = BuildLabelSQL_2(12);
									frmCustomLabels.InstancePtr.Init(strSQL, intRateType);
								}
								break;
							}
						case 20:
							{
								// Demand Forms
								if (ValidateAnswers())
								{
									this.Hide();
									lngRateRecNumber = GetRateKey();
									strSQL = BuildLabelSQL_2(20);
									frmCustomLabels.InstancePtr.Init(strSQL, intRateType);
								}
								break;
							}
						case 21:
							{
								// Lien Process Forms
								if (ValidateAnswers())
								{
									this.Hide();
									lngRateRecNumber = GetRateKey();
									strSQL = BuildLabelSQL_2(21);
									frmCustomLabels.InstancePtr.Init(strSQL, intRateType);
								}
								break;
							}
						case 22:
							{
								// Lien Maturity Forms
								if (ValidateAnswers())
								{
									this.Hide();
									lngRateRecNumber = GetRateKey();
									strSQL = BuildLabelSQL_2(22);
									frmCustomLabels.InstancePtr.Init(strSQL, intRateType);
								}
								break;
							}
						case 30:
							{
								// Reminder Notices
								if (ValidateAnswers())
								{
									this.Hide();
									lngRateRecNumber = GetRateKey();
									strSQL = BuildLabelSQL_2(30);
									frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Reminder Information");
									frmReminderNotices.InstancePtr.Init(strRateKeyList, cmbYear.Items[cmbYear.SelectedIndex].ToString(), FCConvert.CBool(cmbReminderLien.SelectedIndex == 1));
									frmWait.InstancePtr.Unload();
								}
								break;
							}
						case 50:
							{
								if (ValidateAnswers())
								{
									lngRateRecNumber = GetRateKey();
									strSQL = BuildLabelSQL_2(50);
                                    if (continueCommand != null)
                                    {
	                                    ((ShowOutstandingPeriodBalanceReportCommand) continueCommand)
		                                    .SetPeriod(cmbPeriod.SelectedIndex + 1);
                                        continueCommand.SetRateIdList(strRateKeyList);
                                        continueCommand.Execute();
                                    }
                                    Close();
								}
								break;
							}
						case 60:
							{
                                if (ValidateAnswers())
                                {
                                    this.Hide();
                                    lngRateRecNumber = GetRateKey();
                                    strSQL = BuildLabelSQL_2(60);
                                    frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Notice Information");
                                    //rptOutstandingPeriodBalances.InstancePtr.strRateKeyList = strRateKeyList;
                                    frmReverseDemandFees.InstancePtr.Init(ref strRateKeyList);
                                    frmWait.InstancePtr.Unload();
                                }
                                break;
							}
						case 100:
							{
								// load back
								if (ValidateAnswers())
								{
                                    this.Hide();
									lngRateRecNumber = GetRateKey();
                                    bool boolLienRR = IsRateKeyLiened();
									frmLoadBack.InstancePtr.Init(lngRateRecNumber, FCConvert.ToInt32(Strings.Left(cmbYear.Items[cmbYear.SelectedIndex].ToString(), 4)), boolLienRR);
								}
								break;
							}
						case 200:
							{
								if (ValidateAnswers())
								{
									this.Hide();
									lngRateRecNumber = GetRateKey();
									frmAuditInfo.InstancePtr.Init(100, 0, default(DateTime), default(DateTime), default(DateTime), lngRateRecNumber);
									// this will allow the user to edit the rate key
								}
								break;
							}
						default:
							{
								// this is nothing
								FCMessageBox.Show("The Rate Type was not setup.", MsgBoxStyle.Critical, "Rate Rec Error");
								// do nothing else...if this happens then there is a bug
								break;
							}
					}
					//end switch
					// End If
				}
				else
				{
					if (cmbPrint.SelectedIndex == 0)
					{
						PrintTypeSetup(0);
					}
					else
					{
						PrintTypeSetup(1);
					}
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Save Error");
			}
		}

		private void optLien_Click(int Index, object sender, System.EventArgs e)
		{
			// update the rate grid
			ChangeRateKeys();
		}

		private void optLien_Click(object sender, System.EventArgs e)
		{
			int index = cmbLien.SelectedIndex;
			optLien_Click(index, sender, e);
		}

		private void optPrint_KeyDown(int Index, object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Return)
			{
				if (cmbPrint.Visible)
				{
					KeyCode = 0;
					PrintTypeSetup(Index);
				}
			}
		}

		private void optPrint_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			int index = cmbPrint.SelectedIndex;
			optPrint_KeyDown(index, sender, e);
		}

		private void PrintTypeSetup(int intIndex)
		{
			if (!boolFormatting)
			{
				cmbPrint.Visible = false;
				lblPrintType.Visible = false;
				if (intIndex == 1)
				{
					// this will set the rate type to forms rather than labels
					intRateType += 10;
				}
				boolFormatting = true;
				SetAct(0);
				boolFormatting = false;
			}
		}

		private void optRange_Click(int Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
					{
						// All Accounts
						txtRange[0].Enabled = false;
						txtRange[1].Enabled = false;
						break;
					}
				case 1:
					{
						// Range by Name
						txtRange[0].Enabled = true;
						txtRange[1].Enabled = true;
						txtRange[0].MaxLength = 20;
						txtRange[1].MaxLength = 20;
						if (txtRange[0].Enabled && txtRange[0].Visible)
						{
							txtRange[0].Focus();
						}
						break;
					}
				case 2:
					{
						// Range by Account Number
						txtRange[0].Enabled = true;
						txtRange[1].Enabled = true;
						txtRange[0].MaxLength = 10;
						txtRange[1].MaxLength = 10;
						if (txtRange[0].Enabled && txtRange[0].Visible)
						{
							txtRange[0].Focus();
						}
						break;
					}
			}
			//end switch
			txtRange[0].Text = "";
			txtRange[1].Text = "";
		}

		private void optRange_Click(object sender, System.EventArgs e)
		{
			int index = cmbRange.SelectedIndex;
			optRange_Click(index, sender, e);
		}

		private void optReminderLien_Click(int Index, object sender, System.EventArgs e)
		{
			if (intRateType == 30)
			{
				ChangeRateKeys();
			}
		}

		private void optReminderLien_Click(object sender, System.EventArgs e)
		{
			int index = cmbReminderLien.SelectedIndex;
			optReminderLien_Click(index, sender, e);
		}

		private void txtMailDate_Validate(ref bool Cancel)
		{
			// set the default mail date from the last time this was entered
			string temp = txtMailDate.Text;
			modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "LastLienEditReportDate", temp);
			txtMailDate.Text = temp;
		}

		private void txtMinimumAmount_Enter(object sender, System.EventArgs e)
		{
			txtMinimumAmount.SelectionStart = 0;
			txtMinimumAmount.SelectionLength = txtMinimumAmount.Text.Length;
		}

		private void txtMinimumAmount_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if ((KeyAscii >= 48 && KeyAscii <= 57) || (KeyAscii == 8) || (KeyAscii == 13))
			{
			}
			else if (KeyAscii == 46)
			{
				if (Strings.InStr(1, txtMinimumAmount.Text, ".") > 0)
				{
					KeyAscii = 0;
				}
			}
			else
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtMinimumAmount_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtMinimumAmount.Text) > 0)
			{
				txtMinimumAmount.Text = Strings.Format(FCConvert.ToDouble(txtMinimumAmount.Text), "#,##0.00");
			}
			else
			{
				txtMinimumAmount.Text = "0.00";
			}
		}

		private void txtRange_KeyPress(int Index, object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			bool boolText;
			boolText = FCConvert.CBool(cmbRange.SelectedIndex == 1);
			if (boolText)
			{
				// this will allow all characters
				switch (KeyAscii)
				{
					case 39:
						{
							// stop single quotes
							KeyAscii = 0;
							break;
						}
				}
				//end switch
			}
			else
			{
				// this will only allow numbers
				if ((KeyAscii >= 48 && KeyAscii <= 57) || (KeyAscii == 8) || (KeyAscii == 13))
				{
				}
				else
				{
					KeyAscii = 0;
				}
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtRange_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int index = txtRange.IndexOf((FCTextBox)sender);
			txtRange_KeyPress(index, sender, e);
		}
		//private void vsbMinAmount_ValueChanged(object sender, System.EventArgs e)
		//{
		//    // this allows the scroll bar to adjust the value in the text box
		//    if (vsbMinAmount.Value == 0)
		//    {
		//        if (FCConvert.ToDouble(txtMinimumAmount.Text)) <= 0)
		//        {
		//            txtMinimumAmount.Text = "0.00";
		//        }
		//        else
		//        {
		//            txtMinimumAmount.Text = Strings.Format(FCConvert.ToDouble(txtMinimumAmount.Text)) - 0.1, "#,##0.00");
		//            vsbMinAmount.Value = 1;
		//        }
		//    }
		//    else if (vsbMinAmount.Value == 2)
		//    {
		//        txtMinimumAmount.Text = Strings.Format(FCConvert.ToDouble(txtMinimumAmount.Text)) + 0.1, "#,##0.00");
		//        vsbMinAmount.Value = 1;
		//    }
		//}
		//FC:FINAL:DDU #i11 Created an event which select all checkmarks in the column when pressing column header
		private void vsRate_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
		{
			int columnIndex = e.ColumnIndex;
			int RowIndex = e.RowIndex;
			//FC:FINAL:DDU #i273 prevent selection of all checkboxes when is allowed only one selection
			if ((intRateType == 1) || (intRateType == 2) || (intRateType == 3) || (intRateType == 5) || (intRateType >= 10 && intRateType <= 100))
			{
				if (columnIndex == 0 && RowIndex == -1)
				{
					//select all
					for (int i = 1; i < vsRate.Rows; i++)
					{
						vsRate.TextMatrix(i, 0, "-1");
					}
				}
			}
		}

		private void vsRate_DblClick(object sender, EventArgs e)
		{
			int lngCT;
			int lngMR = 0;
			int lngMC = 0;
			if ((intRateType == 1) || (intRateType == 2) || (intRateType == 3) || (intRateType == 5) || (intRateType >= 10 && intRateType <= 100))
			{
				lngMR = vsRate.MouseRow;
				lngMC = vsRate.MouseCol;
				if (lngMR == 0 && lngMC == 0)
				{
					// this will select all
					if (vsRate.Rows > 1)
					{
						for (lngCT = 1; lngCT <= vsRate.Rows - 1; lngCT++)
						{
							vsRate.TextMatrix(lngCT, 0, FCConvert.ToString(-1));
						}
					}
				}
			}
		}

		private void vsRate_RowColChange(object sender, EventArgs e)
		{
			if (vsRate.Col == 0)
			{
				vsRate.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsRate.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				if (vsRate.Row > 0)
				{
					vsRate.EditCell();
					if (FCConvert.ToDouble(vsRate.EditText) == 1)
					{
						// vsRate.TextMatrix(vsRate.Row, vsRate.Col) = 0 Then
						vsRate.EditText = FCConvert.ToString(-1);
						// vsRate.TextMatrix(vsRate.Row, vsRate.Col)
						vsRate.TextMatrix(vsRate.Row, vsRate.Col, FCConvert.ToString(-1));
					}
					else
					{
						vsRate.EditText = FCConvert.ToString(0);
						vsRate.TextMatrix(vsRate.Row, vsRate.Col, FCConvert.ToString(0));
					}
				}
			}
			else if (vsRate.Col == lngCommitmentDateCol)
			{
				vsRate.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsRate.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
			else if (vsRate.Col == vsRate.Cols - 1)
			{
				vsRate.Editable = FCGrid.EditableSettings.flexEDNone;
				if (vsRate.Row == vsRate.Rows - 1)
				{
					vsRate.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
				}
				else
				{
					vsRate.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
			}
			else
			{
				vsRate.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			if (vsRate.Col == 5 && vsRate.Row == vsRate.Rows - 1)
			{
				vsRate.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				vsRate.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private bool FillYearCombo()
		{
			bool FillYearCombo = false;
			// this will fill the Year combobox with all of the years that have bills associated with them
			clsDRWrapper rsYear = new clsDRWrapper();
			string strSQL = "";
			bool boolUseFullYear;
			int intTemp;
			string strTemp = "";
			boolUseFullYear = false;
			cmbYear.Clear();
			switch (intRateType)
			{
				case 0:
					{
						// load nothing
						strSQL = "";
						break;
					}
				case 1:
					{
						// Lien Process Edit Report
						strSQL = "SELECT DISTINCT BillingYear AS Year FROM BillingMaster ORDER BY BillingYear";
						break;
					}
				case 2:
					{
						// 30 Day Notice
						strSQL = "SELECT DISTINCT BillingYear AS Year FROM BillingMaster ORDER BY BillingYear";
						break;
					}
				case 3:
					{
						// Tax Liens Certificates
						strSQL = "SELECT DISTINCT BillingYear AS Year FROM BillingMaster WHERE LienProcessStatus = 2 OR LienProcessStatus = 3 ORDER BY BillingYear";
						break;
					}
				case 4:
					{
						// Lien Maturity Notices
						strSQL = "SELECT DISTINCT BillingYear AS Year FROM BillingMaster WHERE LienProcessStatus = 4 OR LienProcessStatus = 5 ORDER BY BillingYear";
						break;
					}
				case 5:
					{
						// Apply Demand Fees
						strSQL = "SELECT DISTINCT BillingYear AS Year FROM BillingMaster WHERE LienProcessStatus = 1 ORDER BY BillingYear";
						break;
					}
				case 6:
					{
						// Transfer Tax To Lien - this needs to be taken only from the rate rec because the lien record may not have been created yet
						strSQL = "SELECT DISTINCT BillingYear AS Year FROM BillingMaster ORDER BY BillingYear";
						// strSQL = "SELECT DISTINCT Year FROM RateRec WHERE RateType = 'L'"
						// boolUseFullYear = True
						break;
					}
				case 7:
					{
						// Apply Lien Maturity Fees
						strSQL = "SELECT DISTINCT BillingYear AS Year FROM BillingMaster WHERE LienProcessStatus = 5 ORDER BY BillingYear";
						break;
					}
				case 10:
					{
						// Demand Labels
						strSQL = "SELECT DISTINCT BillingYear AS Year FROM BillingMaster WHERE LienProcessStatus = 1 OR LienProcessStatus = 2 ORDER BY BillingYear";
						break;
					}
				case 11:
					{
						// Lien Process Labels
						strSQL = "SELECT DISTINCT BillingYear AS Year FROM BillingMaster WHERE LienProcessStatus = 3 OR LienProcessStatus = 4 ORDER BY BillingYear";
						break;
					}
				case 12:
					{
						// Lien Maturity Labels
						strSQL = "SELECT DISTINCT BillingYear AS Year FROM BillingMaster WHERE LienProcessStatus = 5 OR LienProcessStatus = 6 ORDER BY BillingYear";
						break;
					}
				case 20:
					{
						// Demand Forms
						strSQL = "SELECT DISTINCT BillingYear AS Year FROM BillingMaster WHERE LienProcessStatus = 1 OR LienProcessStatus = 2 ORDER BY BillingYear";
						break;
					}
				case 21:
					{
						// Lien Process Forms
						strSQL = "SELECT DISTINCT BillingYear AS Year FROM BillingMaster WHERE LienProcessStatus = 3 OR LienProcessStatus = 4 ORDER BY BillingYear";
						break;
					}
				case 22:
					{
						// Lien Maturity Forms
						strSQL = "SELECT DISTINCT BillingYear AS Year FROM BillingMaster WHERE LienProcessStatus = 5 OR LienProcessStatus = 6 ORDER BY BillingYear";
						break;
					}
				case 30:
					{
						// Reminder Notices
						strSQL = "SELECT DISTINCT BillingYear AS Year FROM BillingMaster ORDER BY BillingYear";
						break;
					}
				case 50:
					{
						// Outstanding Taxes By Period
						strSQL = "SELECT DISTINCT [Year] FROM RateRec WHERE RateType <> 'L'";
						// Year NOT IN (SELECT DISTINCT Year FROM RateRec WHERE RateType = 'L')"
						boolUseFullYear = true;
						break;
					}
				case 60:
					{
						// Reverse 30 DN Notice Fees
						strSQL = "SELECT DISTINCT BillingYear AS Year FROM BillingMaster ORDER BY BillingYear";
						break;
					}
				case 100:
					{
						strSQL = "SELECT DISTINCT [Year] FROM RateRec";
						boolUseFullYear = true;
						break;
					}
				case 200:
					{
						strSQL = "SELECT DISTINCT [Year] FROM RateRec";
						// "SELECT DISTINCT BillingYear AS Year FROM BillingMaster ORDER BY BillingYear"
						boolUseFullYear = true;
						break;
					}
				default:
					{
						// load nothing
						strSQL = "";
						break;
					}
			}
			//end switch
			if (strSQL != "")
			{
				rsYear.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
				if (rsYear.EndOfFile() != true)
					rsYear.MoveLast();
				while (!rsYear.BeginningOfFile())
				{
					if (boolUseFullYear)
					{
						// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
						if (cmbYear.NewIndex == -1 || Conversion.Val(cmbYear.Items[cmbYear.NewIndex].ToString()) != Conversion.Val(rsYear.Get_Fields("Year")))
						{
							// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
							cmbYear.AddItem(FCConvert.ToString(rsYear.Get_Fields("Year")));
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
						if (cmbYear.NewIndex == -1 || Conversion.Val(cmbYear.Items[cmbYear.NewIndex].ToString()) != FCUtils.iDiv(FCConvert.ToInt32(rsYear.Get_Fields("Year")), 10))
						{
							// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
							cmbYear.AddItem(FCConvert.ToString(FCUtils.iDiv(FCConvert.ToInt32(rsYear.Get_Fields("Year")), 10)));
						}
					}
					rsYear.MovePrevious();
				}
			}
			if (cmbYear.Items.Count > 0)
			{
				// set the default year from the last time this was entered
				//modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "LastRateRecChoiceYear", ref strTemp);
				strTemp = modGlobal.Statics.setCont.GetSettingValue("LastRateRecChoiceYear", "", "", "", "");
				//FC:FINAL:AM:#i88 - set the default year to the current year
				if (String.IsNullOrEmpty(strTemp))
				{
					strTemp = DateTime.Now.Year.ToString();
				}
				if (cmbYear.Items.Count > 0)
				{
					// set the default year if it is in the list
					for (intTemp = 0; intTemp <= cmbYear.Items.Count - 1; intTemp++)
					{
						if (cmbYear.Items[intTemp].ToString() == strTemp)
						{
							cmbYear.SelectedIndex = intTemp;
							break;
						}
						else
						{
							// cmbYear.ListIndex = 0
						}
					}
					if (cmbYear.SelectedIndex == -1)
					{
						cmbYear.SelectedIndex = 0;
					}
				}
				FillYearCombo = true;
			}
			else
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				FillYearCombo = false;
				if (intRateType == 100)
				{
					frmAuditInfo.InstancePtr.Init(0);
					intRateType = -1;
					// this will force this form to unload
				}
				else if (intRateType == -1)
				{
					// do nothing
				}
				else
				{
					if (intRateType != 0)
					{
						if (intRateType != 61)
						{
							FCMessageBox.Show("There are no accounts eligible.", MsgBoxStyle.Information, "No Eligible Accounts");
						}
						else
						{
						}
					}
					else
					{
						// load the combo again
						intRateType = 100;
						FillYearCombo = FillYearCombo;
						return FillYearCombo;
					}
				}
			}
			return FillYearCombo;
		}

		private bool ValidateAnswers()
		{
			bool ValidateAnswers = false;
			// this function will check to make sure that all of the answers that are given are valid
			// including the rate tables being checked and the ranges being correct
			int intCT;
			bool boolFound;
			ValidateAnswers = true;
			boolFound = false;
			for (intCT = 1; intCT <= vsRate.Rows - 1; intCT++)
			{
				if (Conversion.Val(vsRate.TextMatrix(intCT, 0)) == -1)
				{
					boolFound = true;
					if (vsRate.TextMatrix(intCT, lngCommitmentDateCol) == "")
					{
						frmWait.InstancePtr.Unload();
						FCMessageBox.Show("Please fill the commitment date field in for the selected rate table.", MsgBoxStyle.Critical, "Rate Table Selection");
						ValidateAnswers = false;
						SetAct(0);
						App.DoEvents();
						if (vsRate.Visible && vsRate.Enabled)
						{
							vsRate.Focus();
							vsRate.Select(intCT, lngCommitmentDateCol);
							vsRate.EditCell();
						}
						return ValidateAnswers;
					}
				}
			}
			if (!boolFound)
			{
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Please select a rate table.", MsgBoxStyle.Critical, "Rate Table Selection");
				ValidateAnswers = false;
				SetAct(0);
				vsRate.Focus();
				return ValidateAnswers;
			}
			if (cmbRange.SelectedIndex == 1)
			{
				if (Strings.CompareString(txtRange[0].Text, ">", txtRange[1].Text))
				{
					frmWait.InstancePtr.Unload();
					FCMessageBox.Show("Please enter a value in the second section of the range that is greater than the first.", MsgBoxStyle.Critical, "Range Criteria");
					ValidateAnswers = false;
					SetAct(2);
					txtRange[1].Focus();
					return ValidateAnswers;
				}
			}
			if (cmbRange.SelectedIndex == 2)
			{
				if (Conversion.Val(txtRange[0].Text) > Conversion.Val(txtRange[1].Text))
				{
					frmWait.InstancePtr.Unload();
					FCMessageBox.Show("Please enter a value in the second section of the range that is greater than the first.", MsgBoxStyle.Critical, "Range Criteria");
					ValidateAnswers = false;
					SetAct(2);
					txtRange[1].Focus();
					return ValidateAnswers;
				}
			}
			if (intRateType == 1)
			{
				if (txtMailDate.Text != "")
				{
					if (!Information.IsDate(txtMailDate.Text))
					{
						frmWait.InstancePtr.Unload();
						FCMessageBox.Show("Please enter a valid date in the mail date field.", MsgBoxStyle.Critical, "Mail Date");
						ValidateAnswers = false;
						SetAct(2);
						txtMailDate.Focus();
						return ValidateAnswers;
					}
				}
				else
				{
					frmWait.InstancePtr.Unload();
					FCMessageBox.Show("Please enter a value in the mail date field.", MsgBoxStyle.Critical, "Mail Date");
					ValidateAnswers = false;
					SetAct(2);
					txtMailDate.Focus();
					return ValidateAnswers;
				}
				if (cmbReportDetail.SelectedIndex < 0)
				{
					frmWait.InstancePtr.Unload();
					FCMessageBox.Show("Please enter a value in the report detail field.", MsgBoxStyle.Critical, "Report Detail");
					ValidateAnswers = false;
					SetAct(2);
					cmbReportDetail.Focus();
					return ValidateAnswers;
				}
			}
			return ValidateAnswers;
		}

		private void ChangeRateKeys()
		{
			// show the Rate Keys
			rsRateRec.OpenRecordset(BuildSQL(), modExtraModules.strCLDatabase);
			if (rsRateRec.EndOfFile() != true)
			{
				// show the rate keys and size the grid
				if (intAction != 2)
				{
					LoadGrid();
				}
			}
			else
			{
				// show nothing
				if (intYear == 0)
				{
					FCMessageBox.Show("Please select a year.", MsgBoxStyle.Information, "Input Error");
				}
				else
				{
					if (intRateType == 6)
					{
						vsRate.Rows = 1;
						vsRate.AddItem("\t" + "Add New" + "\t" + "Add New" + "\t" + "Add New" + "\t" + "Add New");
						// this sets the height
						if (vsRate.Rows > 1)
						{
							if (vsRate.Rows == 2 || (intRateType == 6 && vsRate.Rows == 3))
							{
								// this only has one rate record, so select it automatically
								boolFormatting = true;
								// this will make sure that it does not automatically go to the rate screen
								vsRate.TextMatrix(1, 0, FCConvert.ToString(-1));
								boolFormatting = false;
							}
						}
					}
					else
					{
						if (intRateType != 30)
						{
							if (!boolLoading)
							{
								FCMessageBox.Show("There are no rate records for the year entered.", MsgBoxStyle.Information, "No Rate Record");
							}
							cmbLien.SelectedIndex = 0;
						}
						else
						{
							if (!boolReminderInitial)
							{
								FCMessageBox.Show("There are no rate records for the year entered.", MsgBoxStyle.Information, "No Rate Record");
							}
							if (cmbReminderLien.SelectedIndex != 0)
							{
								cmbReminderLien.SelectedIndex = 0;
							}
							else
							{
								FCMessageBox.Show("There are no rate records for the year entered.", MsgBoxStyle.Information, "No Rate Record");
							}
						}
					}
					boolReminderInitial = false;
				}
			}
		}

		private void FillReportDetailCombo()
		{
			// this will fill the combo box with the different options for the detail level of the report
			cmbReportDetail.Clear();
			cmbReportDetail.AddItem("Account Information Only");
			cmbReportDetail.ItemData(cmbReportDetail.NewIndex, 0);
			cmbReportDetail.AddItem("Show Mortgage Holders");
			cmbReportDetail.ItemData(cmbReportDetail.NewIndex, 1);
			cmbReportDetail.AddItem("Show Mortgage Holders and Address");
			cmbReportDetail.ItemData(cmbReportDetail.NewIndex, 2);
			cmbReportDetail.SelectedIndex = 0;
		}

		private int GetRateKey()
		{
			int GetRateKey = 0;
			// this will check the grid and return whatever rate key is selected
			int lngRW;
			for (lngRW = 1; lngRW <= vsRate.Rows - 1; lngRW++)
			{
				if (Conversion.Val(vsRate.TextMatrix(lngRW, 0)) == -1)
				{
					GetRateKey = FCConvert.ToInt32(Math.Round(Conversion.Val(vsRate.TextMatrix(lngRW, lngHiddenCol))));
					break;
				}
			}
			return GetRateKey;
		}

        private bool IsRateKeyLiened()
        {
            int lngRW;
            for (lngRW = 1; lngRW <= vsRate.Rows - 1; lngRW++)
            {
                if (Conversion.Val(vsRate.TextMatrix(lngRW, 0)) == -1)
                {
                    return vsRate.TextMatrix(lngRW, lngLienCol) == "L";
                }
            }

            return false;
        }

		private string BuildLabelSQL_2(short intType)
		{
			return BuildLabelSQL(ref intType);
		}

		private string BuildLabelSQL(ref short intType)
		{
			string BuildLabelSQL = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will return the SQL statement for this batch of reports
				string strWhereClause = "";
				int intCT;
				clsDRWrapper rsData = new clsDRWrapper();
				string strQryFields = "";
				for (intCT = 1; intCT <= vsRate.Rows - 1; intCT++)
				{
					if (Conversion.Val(vsRate.TextMatrix(intCT, 0)) == -1)
					{
						switch (intType)
						{
							case 12:
							case 22:
								{
									if (Strings.Trim(strWhereClause) == "")
									{
										strWhereClause = " AND (LienRec.RateKey = " + vsRate.TextMatrix(intCT, lngHiddenCol) + " OR BillingMaster.RateKey = " + vsRate.TextMatrix(intCT, lngHiddenCol);
									}
									else
									{
										strWhereClause += " OR LienRec.RateKey = " + vsRate.TextMatrix(intCT, lngHiddenCol) + " OR BillingMaster.RateKey = " + vsRate.TextMatrix(intCT, lngHiddenCol);
									}
									break;
								}
							default:
								{
									if (Strings.Trim(strWhereClause) == "")
									{
										strWhereClause = " AND RateKey IN (" + vsRate.TextMatrix(intCT, lngHiddenCol);
									}
									else
									{
										strWhereClause += ", " + vsRate.TextMatrix(intCT, lngHiddenCol);
									}
									break;
								}
						}
						//end switch
					}
				}
				// Select Case intType
				// Case 12, 22
				// strWhereClause = " AND (LienRec.RateKey = " & frmRateRecChoice.lngRateRecNumber & " OR BillingMaster.RateKey = " & frmRateRecChoice.lngRateRecNumber & ")"
				// Case Else
				// strWhereClause = " AND RateKey = " & frmRateRecChoice.lngRateRecNumber
				// End Select
				if (strWhereClause != "")
				{
					strWhereClause += ")";
				}
				if (frmRateRecChoice.InstancePtr.cmbRange.SelectedIndex == 2)
				{
					// range of accounts
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[0].Text) != "")
					{
						if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
						{
							// both full
							strWhereClause = " AND Account >= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text)) + " AND Account <= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
						}
						else
						{
							// first full second empty
							strWhereClause = " AND Account >= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text));
						}
					}
					else
					{
						if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
						{
							// first empty second full
							strWhereClause = " AND Account <= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
						}
						else
						{
							// both empty
							strWhereClause = "";
						}
					}
				}
				else if (frmRateRecChoice.InstancePtr.cmbRange.SelectedIndex == 1)
				{
					// range of names
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[0].Text) != "")
					{
						if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
						{
							// both full
							strWhereClause = " AND Name1 >= '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "   ' AND Name1 <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "ZZZ'";
						}
						else
						{
							// first full second empty
							strWhereClause = " AND Name1 >= '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "   '";
						}
					}
					else
					{
						if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
						{
							// first empty second full
							strWhereClause = " AND Name1 <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "ZZZ'";
						}
						else
						{
							// both empty
							strWhereClause = "";
						}
					}
				}
				else
				{
				}
				switch (intType)
				{
					case 12:
					case 22:
						{
							strWhereClause += " AND Principal > LienRec.PrincipalPaid ";
							break;
						}
					default:
						{
							strWhereClause += " AND (TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4) > PrincipalPaid ";
							break;
						}
				}
				//end switch
				if (strWhereClause.Length > 3000)
				{
					FCMessageBox.Show("Too many ratekeys were selected, try selecting less and running this again.", MsgBoxStyle.Critical, "SQL String Too Long");
					return BuildLabelSQL;
				}
				switch (intType)
				{
				// kgk 2-14-2012  Change out stored procs
					case 10:
					case 20:
						{
							// 30 Day Labels
							// rsData.CreateStoredProcedure "LabelQuery", "SELECT * FROM BillingMaster INNER JOIN Master ON BillingMaster.Account = Master.RSAccount  WHERE RSCard = 1 AND (LienProcessStatus = 1 OR LienProcessStatus = 2) And BillingYear/10 = " & intYear & strWhereClause, strCLDatabase
							strQryFields = "Account,Name1,BillingType,RateKey";
							BuildLabelSQL = "SELECT " + strQryFields + " FROM BillingMaster INNER JOIN " + modGlobal.Statics.strDbRE + "Master ON BillingMaster.Account = Master.RSAccount  WHERE RSCard = 1 AND (LienProcessStatus = 1 OR LienProcessStatus = 2) And BillingYear/10 = " + FCConvert.ToString(intYear) + strWhereClause;
							break;
						}
					case 11:
					case 21:
						{
							// Transfer Tax To Lien Labels
							// rsData.CreateStoredProcedure "LabelQuery", "SELECT * FROM BillingMaster INNER JOIN Master ON BillingMaster.Account = Master.RSAccount  WHERE RSCard = 1 AND (LienProcessStatus = 3) And BillingYear/10 = " & intYear & strWhereClause, strCLDatabase
							strQryFields = "Account,Name1,BillingType,RateKey,CertifiedMailNumber";
							BuildLabelSQL = "SELECT " + strQryFields + " FROM BillingMaster INNER JOIN " + modGlobal.Statics.strDbRE + "Master ON BillingMaster.Account = Master.RSAccount  WHERE RSCard = 1 AND (LienProcessStatus = 3) And BillingYear/10 = " + FCConvert.ToString(intYear) + strWhereClause;
							break;
						}
					case 12:
					case 22:
						{
							// Lien Maturity Labels
							// rsData.CreateStoredProcedure "LabelQuery", "SELECT * FROM LienRec INNER JOIN (BillingMaster INNER JOIN Master ON BillingMaster.Account = Master.RSAccount) ON LienRec.LienRecordNumber = BillingMaster.LienRecordNumber WHERE RSCard = 1 AND (LienProcessStatus = 5) And BillingYear/10 = " & intYear & strWhereClause, strCLDatabase
							strQryFields = "Account,OwnerPartyID,Name1,BillingType,LienRec.RateKey";
							BuildLabelSQL = "SELECT " + strQryFields + " FROM LienRec INNER JOIN (BillingMaster INNER JOIN " + modGlobal.Statics.strDbRE + "Master ON BillingMaster.Account = Master.RSAccount) ON LienRec.ID = BillingMaster.LienRecordNumber WHERE RSCard = 1 AND (LienProcessStatus = 5) And BillingYear/10 = " + FCConvert.ToString(intYear) + strWhereClause;
							break;
						}
					case 30:
						{
							// Reminder Notice Labels
							// rsData.CreateStoredProcedure "LabelQuery", "SELECT * FROM BillingMaster INNER JOIN Master ON BillingMaster.Account = Master.RSAccount  WHERE RSCard = 1 AND BillingYear/10 = " & intYear & strWhereClause, strCLDatabase
							BuildLabelSQL = "SELECT * FROM BillingMaster INNER JOIN " + modGlobal.Statics.strDbRE + "Master ON BillingMaster.Account = Master.RSAccount  WHERE RSCard = 1 AND BillingYear/10 = " + FCConvert.ToString(intYear) + strWhereClause;
							break;
						}
				}
				//end switch
				// BuildLabelSQL = "SELECT * FROM LabelQuery"
				return BuildLabelSQL;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.OkOnly, "Build Label SQL Error");
			}
			return BuildLabelSQL;
		}

		private void SetToolTips()
		{
			// this procedure will set all of the tool tips
			// for each of the controls on the form
		}
		// VBto upgrade warning: lngRateKey As int	OnWriteFCConvert.ToDouble(
		private void ShowRateInfo_18(int cRow, int cCol, int lngRateKey)
		{
			ShowRateInfo(ref cRow, ref cCol, ref lngRateKey);
		}

		private void ShowRateInfo(ref int cRow, ref int cCol, ref int lngRateKey)
		{
			int lngErrCode = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this sub will show a box with the rate information in it
				// as soon as the box loses focus, I will make it disappear
				clsDRWrapper rsRI = new clsDRWrapper();
				clsDRWrapper rsLR = new clsDRWrapper();
				int intYear = 0;
				int I/*unused?*/;
				int RK;
				int LRN/*unused?*/;
				lngErrCode = 1;
				RK = lngRateKey;
				lngErrCode = 2;
				rsRI.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(RK), modExtraModules.strCLDatabase);
				// format grid
				vsRateInfo.Cols = 1;
				vsRateInfo.Cols = 2;
				vsRateInfo.Rows = 15;
				vsRateInfo.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				//				fraRateInfo.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
				lngErrCode = 3;
				intYear = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbYear.Items[cmbYear.SelectedIndex].ToString(), 4))));
				lngErrCode = 4;
				vsRateInfo.ColWidth(0, FCConvert.ToInt32(vsRateInfo.WidthOriginal * 0.45));
				vsRateInfo.ColWidth(1, FCConvert.ToInt32(vsRateInfo.WidthOriginal * 0.5));
				vsRateInfo.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsRateInfo.ExtendLastCol = true;
				vsRateInfo.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
				lngErrCode = 5;
				if (rsRI.BeginningOfFile() != true && rsRI.EndOfFile() != true)
				{
					// show the first set of information (rate information and period info)--------------
					vsRateInfo.TextMatrix(0, 0, "RateKey");
					vsRateInfo.TextMatrix(1, 0, "Tax Rate");
					vsRateInfo.TextMatrix(2, 0, "Number of Periods");
					vsRateInfo.TextMatrix(3, 0, "Rate Type");
					vsRateInfo.TextMatrix(4, 0, "Billing Date");
					vsRateInfo.TextMatrix(5, 0, "Creation Date");
					vsRateInfo.TextMatrix(6, 0, "Interest Per 1");
					vsRateInfo.TextMatrix(10, 0, "Due Date Per 1");
					vsRateInfo.TextMatrix(14, 0, "Interest Rate");
					lngErrCode = 6;
					vsRateInfo.TextMatrix(0, 1, FCConvert.ToString(rsRI.Get_Fields_Int32("ID")));
					lngErrCode = 7;
					if (!(FCConvert.ToString(rsRI.Get_Fields_String("ratetype")) == "L"))
					{
						vsRateInfo.TextMatrix(1, 1, Strings.Format(rsRI.Get_Fields_Double("TaxRate") * 1000, "#0.000"));
						// kk10282014 trocl-1209  Mill rate rounded off, change format from "0.00"
					}
					else
					{
						vsRateInfo.TextMatrix(1, 1, "N/A");
					}
					lngErrCode = 8;
					vsRateInfo.TextMatrix(2, 1, FCConvert.ToString(rsRI.Get_Fields_Int16("NumberofPeriods")));
					lngErrCode = 9;
					vsRateInfo.TextMatrix(3, 1, FCConvert.ToString(rsRI.Get_Fields_String("RateType")));
					lngErrCode = 10;
					vsRateInfo.TextMatrix(4, 1, FCConvert.ToString(rsRI.Get_Fields_DateTime("BillingDate")));
					lngErrCode = 11;
					vsRateInfo.TextMatrix(5, 1, FCConvert.ToString(rsRI.Get_Fields_DateTime("CreationDate")));
					lngErrCode = 12;
					//if (((rsRI.Get_Fields("InterestStartDate1"))) != 0)
					if (FCConvert.ToDateTime(rsRI.Get_Fields_DateTime("InterestStartDate1")) != null)
					{
						vsRateInfo.TextMatrix(6, 1, Strings.Format(rsRI.Get_Fields_DateTime("InterestStartDate1"), "MM/dd/yyyy"));
					}
					else
					{
						vsRateInfo.TextMatrix(6, 1, "");
					}
					if (FCConvert.ToDateTime(rsRI.Get_Fields_DateTime("DueDate1")) != null)
					{
						vsRateInfo.TextMatrix(10, 1, Strings.Format(rsRI.Get_Fields_DateTime("DueDate1"), "MM/dd/yyyy"));
					}
					else
					{
						vsRateInfo.TextMatrix(10, 1, "");
					}
					lngErrCode = 13;
					if (FCConvert.ToInt32(rsRI.Get_Fields_Int16("NumberofPeriods")) > 1)
					{
						vsRateInfo.TextMatrix(7, 0, "Interest Per 2");
						vsRateInfo.TextMatrix(11, 0, "Due Date Per 2");
						if (FCConvert.ToDateTime(rsRI.Get_Fields_DateTime("InterestStartDate2")) != null)
						{
							vsRateInfo.TextMatrix(7, 1, Strings.Format(rsRI.Get_Fields_DateTime("InterestStartDate2"), "MM/dd/yyyy"));
						}
						else
						{
							vsRateInfo.TextMatrix(7, 1, "");
						}
						if (FCConvert.ToDateTime(rsRI.Get_Fields_DateTime("DueDate2")) != null)
						{
							vsRateInfo.TextMatrix(11, 1, Strings.Format(rsRI.Get_Fields_DateTime("DueDate2"), "MM/dd/yyyy"));
						}
						else
						{
							vsRateInfo.TextMatrix(11, 1, "");
						}
						lngErrCode = 14;
						if (FCConvert.ToInt32(rsRI.Get_Fields_Int16("NumberofPeriods")) > 2)
						{
							vsRateInfo.TextMatrix(8, 0, "Interest Per 3");
							vsRateInfo.TextMatrix(12, 0, "Due Date Per 3");
							if (FCConvert.ToDateTime(rsRI.Get_Fields_DateTime("InterestStartDate3")) != null)
							{
								vsRateInfo.TextMatrix(8, 1, Strings.Format(rsRI.Get_Fields_DateTime("InterestStartDate3"), "MM/dd/yyyy"));
							}
							else
							{
								vsRateInfo.TextMatrix(8, 1, "");
							}
							if (FCConvert.ToDateTime(rsRI.Get_Fields_DateTime("DueDate3")) != null)
							{
								vsRateInfo.TextMatrix(12, 1, Strings.Format(rsRI.Get_Fields_DateTime("DueDate3"), "MM/dd/yyyy"));
							}
							else
							{
								vsRateInfo.TextMatrix(12, 1, "");
							}
							lngErrCode = 15;
							if (FCConvert.ToInt32(rsRI.Get_Fields_Int16("NumberofPeriods")) > 3)
							{
								vsRateInfo.TextMatrix(9, 0, "Interest Per 4");
								vsRateInfo.TextMatrix(13, 0, "Due Date Per 4");
								if (FCConvert.ToDateTime(rsRI.Get_Fields_DateTime("InterestStartDate4")) != null)
								{
									vsRateInfo.TextMatrix(9, 1, Strings.Format(rsRI.Get_Fields_DateTime("InterestStartDate4"), "MM/dd/yyyy"));
								}
								else
								{
									vsRateInfo.TextMatrix(9, 1, "");
								}
								if (FCConvert.ToDateTime(rsRI.Get_Fields_DateTime("DueDate4")) != null)
								{
									vsRateInfo.TextMatrix(13, 1, Strings.Format(rsRI.Get_Fields_DateTime("DueDate4"), "MM/dd/yyyy"));
								}
								else
								{
									vsRateInfo.TextMatrix(13, 1, "");
								}
								lngErrCode = 16;
							}
							else
							{
								vsRateInfo.TextMatrix(9, 0, "");
								vsRateInfo.TextMatrix(13, 0, "");
								vsRateInfo.TextMatrix(9, 1, "");
								vsRateInfo.TextMatrix(13, 1, "");
							}
						}
						else
						{
							vsRateInfo.TextMatrix(8, 0, "");
							vsRateInfo.TextMatrix(12, 0, "");
							vsRateInfo.TextMatrix(8, 1, "");
							vsRateInfo.TextMatrix(12, 1, "");
						}
					}
					else
					{
						vsRateInfo.TextMatrix(7, 0, "");
						vsRateInfo.TextMatrix(11, 0, "");
						vsRateInfo.TextMatrix(7, 1, "");
						vsRateInfo.TextMatrix(11, 1, "");
					}
					vsRateInfo.TextMatrix(14, 1, Strings.Format(rsRI.Get_Fields_Double("InterestRate"), "#0.00%"));
					lngErrCode = 17;
				}
				// show the frame-----------------------------------------------------
				fraRateInfo.Text = "Rate Information - " + modExtraModules.FormatYear(intYear.ToString());
				fraRateInfo.Visible = true;
				if (cmdRIClose.Visible)
				{
					cmdRIClose.Focus();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Rate Information Error" + " - " + FCConvert.ToString(lngErrCode));
			}
		}

		private void InitialSettings_2(string strReportType)
		{
			InitialSettings(ref strReportType);
		}

		private void InitialSettings(ref string strReportType)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will reset the LienStatusEligibility field of the bills
				// to the previous and check to see if it is still eligibly for this step
				clsDRWrapper rsInitial = new clsDRWrapper();
				clsDRWrapper rsInitialLien = new clsDRWrapper();
				clsDRWrapper rsRE = new clsDRWrapper();
				string strSQL;
				string strAccountList;
				string strNoNEligibleAccounts;
				string strBankruptcyAccounts;
				bool boolEligible;
				int lngMaxAccount;
				int lngCount;
				string strTemp = "";
				clsDRWrapper rsUpd = new clsDRWrapper();
				// Debug.Print Time & " - Start Initial Settings"
				strSQL = "";
				strAccountList = "";
				strNoNEligibleAccounts = "";
				strBankruptcyAccounts = "";
				lngMaxAccount = 0;
				boolEligible = false;
				// this will set all of the accounts that are currently eligible back to the last eligibility
				if (Strings.UCase(strReportType) == "30DAYNOTICE")
				{
					strSQL = "UPDATE BillingMaster SET LienStatusEligibility = 0 WHERE BillingType = 'RE' AND LienProcessStatus = 1 AND BillingYear / 10 = " + frmRateRecChoice.InstancePtr.cmbYear.Items[frmRateRecChoice.InstancePtr.cmbYear.SelectedIndex].ToString();
				}
				else if (Strings.UCase(strReportType) == "LIENPROCESS")
				{
					strSQL = "UPDATE BillingMaster SET LienStatusEligibility = 2 WHERE BillingType = 'RE' AND LienProcessStatus = 3 AND BillingYear / 10 = " + frmRateRecChoice.InstancePtr.cmbYear.Items[frmRateRecChoice.InstancePtr.cmbYear.SelectedIndex].ToString();
				}
				else if (Strings.UCase(strReportType) == "LIENMATURITY")
				{
					strSQL = "UPDATE BillingMaster SET LienStatusEligibility = 4 WHERE BillingType = 'RE' AND LienProcessStatus = 5 AND BillingYear / 10 = " + frmRateRecChoice.InstancePtr.cmbYear.Items[frmRateRecChoice.InstancePtr.cmbYear.SelectedIndex].ToString();
				}
				if (rsInitial.Execute(strSQL, modExtraModules.strCLDatabase))
				{
					// Check all of the accounts that have a certain eligibility to recalculate that eligibility (check to see if the account was paid off)
					if (Strings.UCase(strReportType) == "30DAYNOTICE")
					{
						// if initial run
						strSQL = "SELECT *, (TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4) AS Principal FROM (" + modGlobal.Statics.gstrBillYrQuery + ") AS qBY WHERE BillingType = 'RE' AND LienStatusEligibility = 0";
					}
					else if (Strings.UCase(strReportType) == "LIENPROCESS")
					{
						strSQL = "SELECT *, (TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4) AS Principal FROM (" + modGlobal.Statics.gstrBillYrQuery + ") AS qBY WHERE BillingType = 'RE' AND LienStatusEligibility = 2";
					}
					else if (Strings.UCase(strReportType) == "LIENMATURITY")
					{
						rsInitialLien = new clsDRWrapper();
						rsRE = new clsDRWrapper();
						// strSQL = "SELECT * FROM (" & gstrBillYrQuery & ") AS qBY WHERE BillingType = 'RE' AND LienStatusEligibility = 4"
						strSQL = modGlobal.Statics.gstrBillYrQuery + " AND BillingType = 'RE' AND LienStatusEligibility = 4";
						rsInitialLien.OpenRecordset("SELECT * FROM LienRec");
						// this will store the Lien Records
						if (modStatusPayments.Statics.boolRE)
						{
							rsRE.OpenRecordset("SELECT * FROM Master", modExtraModules.strREDatabase);
						}
						else
						{
							rsRE.OpenRecordset("SELECT * FROM PPMaster", modExtraModules.strPPDatabase);
						}
					}
					// Debug.Print Time & " - Find SQL"
					rsInitial.OpenRecordset(strSQL);
					// select all of the accounts that are eligible to have their eligibility recalculated
					if (rsInitial.EndOfFile() != true)
					{
						lngMaxAccount = rsInitial.RecordCount();
					}
					else
					{
						lngCount = 0;
					}
					lngCount = 0;
					frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Calculating Eligibility");
					// , True, lngMaxAccount, True
					this.Refresh();
					// Debug.Print Time & " - Open Recordset"
					while (!rsInitial.EndOfFile())
					{
						// cycle through them and find the accounts that are still eligible
						// frmWait.IncrementProgress
						App.DoEvents();
						boolEligible = false;
						if (Strings.UCase(strReportType) == "30DAYNOTICE")
						{
							// check in the BillingMaster to see if there is any outstanding balances
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							if (Conversion.Val(rsInitial.Get_Fields("Principal")) - Conversion.Val(rsInitial.Get_Fields_Decimal("InterestCharged")) + Conversion.Val(rsInitial.Get_Fields_Decimal("DemandFees")) > Conversion.Val(rsInitial.Get_Fields_Decimal("PrincipalPaid")) + Conversion.Val(rsInitial.Get_Fields_Decimal("InterestPaid")) + Conversion.Val(rsInitial.Get_Fields_Decimal("DemandFeesPaid")))
							{
								boolEligible = true;
							}
							else
							{
								boolEligible = false;
							}
						}
						else if (Strings.UCase(strReportType) == "LIENPROCESS")
						{
							// check in the BillingMaster to see if there is any outstanding balances
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							if (Conversion.Val(rsInitial.Get_Fields("Principal")) - Conversion.Val(rsInitial.Get_Fields_Decimal("InterestCharged")) + Conversion.Val(rsInitial.Get_Fields_Decimal("DemandFees")) > Conversion.Val(rsInitial.Get_Fields_Decimal("PrincipalPaid")) + Conversion.Val(rsInitial.Get_Fields_Decimal("InterestPaid")) + Conversion.Val(rsInitial.Get_Fields_Decimal("DemandFeesPaid")))
							{
								boolEligible = true;
							}
							else
							{
								boolEligible = false;
							}
						}
						else if (Strings.UCase(strReportType) == "LIENMATURITY")
						{
							// check in the LienRec to see if there is any outstanding balances
							if (rsInitialLien.FindFirstRecord("ID", rsInitial.Get_Fields_Int32("LienRecordNumber")))
							{
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
								if (Conversion.Val(rsInitialLien.Get_Fields("Principal")) - Conversion.Val(rsInitialLien.Get_Fields_Decimal("InterestCharged")) + Conversion.Val(rsInitialLien.Get_Fields("Costs")) + Conversion.Val(rsInitialLien.Get_Fields("MaturityFee")) > Conversion.Val(rsInitialLien.Get_Fields_Decimal("PrincipalPaid")) + Conversion.Val(rsInitialLien.Get_Fields_Decimal("InterestPaid")) + Conversion.Val(rsInitialLien.Get_Fields_Decimal("CostsPaid")))
								{
									if (modStatusPayments.Statics.boolRE)
									{
										// strTemp = "RSAccount = " & rsInitial.Get_Fields("Account") & " AND RSCard = 1"
										// If rsRE.FindFirstRecord(, , strTemp) Then
										// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
										if (rsRE.FindFirstRecord2("RSAccount,RSCard", FCConvert.ToString(rsInitial.Get_Fields("Account")) + ",1", ","))
										{
											if (FCConvert.ToBoolean(rsRE.Get_Fields_Boolean("InBankruptcy")))
											{
												// check to see if this account is in bankruptcy
												boolEligible = false;
												// if it is then it is not eligible for lien maturity
												// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
												strBankruptcyAccounts += FCConvert.ToString(rsInitial.Get_Fields("Account")) + ",";
											}
											else
											{
												boolEligible = true;
												// if not then it is eligible
											}
										}
										else
										{
											boolEligible = false;
											// if you can not find the account in the RE database, then it is not eligible
										}
									}
									else
									{
										// there is no bankruptcy in PP so if it gets this far then it is eligible
										boolEligible = true;
									}
								}
								else
								{
									boolEligible = false;
								}
							}
							else
							{
								boolEligible = false;
							}
						}
						if (boolEligible)
						{
							strAccountList += FCConvert.ToString(rsInitial.Get_Fields_Int32("ID"));
							// add this billkey to the account list
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							strNoNEligibleAccounts += FCConvert.ToString(rsInitial.Get_Fields("Account")) + ",";
						}
						if (strAccountList.Length > 0)
						{
							if (Strings.Right(strAccountList, 1) == ",")
							{
								strAccountList = Strings.Left(strAccountList, strAccountList.Length - 1);
							}
							if (Strings.UCase(strReportType) == "30DAYNOTICE")
							{
								strSQL = "UPDATE BillingMaster SET LienStatusEligibility = 1 WHERE ID = " + strAccountList;
							}
							else if (Strings.UCase(strReportType) == "LIENPROCESS")
							{
								strSQL = "UPDATE BillingMaster SET LienStatusEligibility = 3 WHERE ID = " + strAccountList;
							}
							else if (Strings.UCase(strReportType) == "LIENMATURITY")
							{
								strSQL = "UPDATE BillingMaster SET LienStatusEligibility = 5 WHERE ID = " + strAccountList;
							}
							// Debug.Print Time & " - Updating Eligibility"
							if (strAccountList != "")
							{
								// rsInitial.Execute strSQL, strCLDatabase            'set the account status eligibility
								rsUpd.Execute(strSQL, modExtraModules.strCLDatabase);
							}
							// Debug.Print Time & " - Updating Eligibility Finished"
							strAccountList = "";
							// this will reset the acocunt list
						}
						// move to the next record
						rsInitial.MoveNext();
					}
					rsInitial.Reset();
					frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Setting Eligibility");
					// this will take the comma off of the end of the string if there is one
					if (strBankruptcyAccounts.Length > 0)
					{
						strBankruptcyAccounts = Strings.Left(strBankruptcyAccounts, strBankruptcyAccounts.Length - 1);
						frmWait.InstancePtr.Unload();
						FCMessageBox.Show("Some accounts will not be processed due to bankruptcy.  Run the 'Print Bankruptcy Report' to see the accounts no eligible for Lien Maturity.", MsgBoxStyle.Information, "Bankrupt Accounts");
						// this is where I actually run and save the report, It can be shown later from the Lien Maturity Menu
						// kk11132015 trocl-1293  This is a list of Accounts not billkeys and the report Runs in the Init procedure
						// Copied the query from frmFreeReport
						// rptBankruptcy.Init "SELECT * FROM BillingMaster INNER JOIN " & strDbRE & "Master ON BillingMaster.Account = Master.RSAccount WHERE BillKey IN (" & strBankruptcyAccounts & ")"
						// rptBankruptcy.Run True
						rptBankruptcy.InstancePtr.Init("SELECT * FROM Master WHERE RSAccount IN (" + strBankruptcyAccounts + ") AND RSCard = 1", this.Modal);
					}
					// If Len(strAccountList) > 0 Then
					// strAccountList = Left$(strAccountList, Len(strAccountList) - 1)
					// 
					// Select Case UCase(strReportType)
					// Case "30DAYNOTICE"
					// strSQL = "UPDATE BillingMaster SET LienStatusEligibility = 1 WHERE BillKey IN (" & strAccountList & ")"
					// Case "LIENPROCESS"
					// strSQL = "UPDATE BillingMaster SET LienStatusEligibility = 3 WHERE BillKey IN (" & strAccountList & ")"
					// Case "LIENMATURITY"
					// strSQL = "UPDATE BillingMaster SET LienStatusEligibility = 5 WHERE BillKey IN (" & strAccountList & ")"
					// End Select
					// 
					// rsInitial.Execute strSQL            'set the account status eligibility
					// End If
					if (strNoNEligibleAccounts.Length > 0)
					{
						strNoNEligibleAccounts = Strings.Left(strNoNEligibleAccounts, strNoNEligibleAccounts.Length - 1);
					}
				}
				else
				{
					FCMessageBox.Show("An ERROR occured while updating the database.", MsgBoxStyle.Critical, "Lien Status Eligibility");
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Initializing Settings");
			}
		}

		public string RateKeyList()
		{
			string RateKeyList = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will return the string of rate keys
				string strTemp = "";
				int lngCT;
				for (lngCT = 1; lngCT <= vsRate.Rows - 1; lngCT++)
				{
					if (Conversion.Val(vsRate.TextMatrix(lngCT, 0)) == -1)
					{
						if (strTemp == "")
						{
							strTemp = "(" + vsRate.TextMatrix(lngCT, lngHiddenCol);
						}
						else
						{
							strTemp += "," + vsRate.TextMatrix(lngCT, lngHiddenCol);
						}
					}
				}
				if (strTemp != "")
				{
					strTemp += ")";
				}
				RateKeyList = strTemp;
				return RateKeyList;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Creating RK List");
			}
			return RateKeyList;
		}

		private void Check30DNYear()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this routine will check to see if this is the first run of the 30DN for this year
				int lngYear;
				// This is the current year being run
				clsDRWrapper rsYearCheck = new clsDRWrapper();
				bool boolAsk = false;
				lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbYear.Items[cmbYear.SelectedIndex].ToString())));
				rsYearCheck.OpenRecordset("SELECT * FROM Collections", modExtraModules.strCLDatabase);
				if (!rsYearCheck.EndOfFile())
				{
					if (Conversion.Val(rsYearCheck.Get_Fields_Int32("30DNYear")) < lngYear)
					{
						boolAsk = true;
					}
				}
				else
				{
					boolAsk = true;
					rsYearCheck.AddNew();
					rsYearCheck.Update();
				}
				if (boolAsk)
				{
					DialogResult messageBoxResult = FCMessageBox.Show("Is this the first run of 30 Day Notices for the " + FCConvert.ToString(lngYear) + " billing year?", MsgBoxStyle.YesNo, "Year Check");
					if (messageBoxResult == DialogResult.Yes)
					{
						// clear the CMFtable
						modGlobal.ClearCMFNumbers(true);
						// update the year
						rsYearCheck.Edit();
						rsYearCheck.Set_Fields("30DNYear", lngYear);
						rsYearCheck.Update();
					}
					else if (messageBoxResult == DialogResult.No)
					{
						// do not do anything
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Creating RK List");
			}
		}

		private void vsRate_MouseDown(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right && vsRate.MouseRow > 0)
			{
				ShowRateInfo_18(vsRate.MouseRow, vsRate.MouseCol, FCConvert.ToInt32(Conversion.Val(vsRate.TextMatrix(vsRate.MouseRow, lngHiddenCol))));
			}
		}

		private void vsRate_CellChanged(object sender, DataGridViewCellMouseEventArgs e)
		{
			int lngRW;
			int intTemp = 0;
			if (vsRate.Col == 0)
			{
				switch (intRateType)
				{
					case 100:
					case 200:
						{
							if (vsRate.Row > 0 && vsRate.Row <= vsRate.Rows - 1)
							{
								for (lngRW = 1; lngRW <= vsRate.Rows - 1; lngRW++)
								{
									// this does not have the extra add row feature
									// this will clear any other selection in the grid so the user can only choose one
									if (lngRW != vsRate.Row)
									{
										vsRate.TextMatrix(lngRW, 0, FCConvert.ToString(0));
										//vsRate.Cell(FCGrid.CellPropertySettings.flexcpPicture, lngRW, 0, "");
									}
									else
									{
										// and it will show the picture if chosen
										// vsRate.Cell(FCGrid.CellPropertySettings.flexcpPicture, lngRW, 0) = MDIParent.ImageList1.ListImages(5).Picture
									}
								}
							}
							break;
						}
					case 6:
						{
							// , 12, 22 'kgk 11-29-2011 trocl-840  Allow multi for lien maturity fees and CMF's            '7, 12, 22 'MAL@20071001: Change to allow multiple rate key selection for 7
							if ((vsRate.Row > 0 && vsRate.Row < vsRate.Rows - 1) || intRateType == 7 || intRateType == 22)
							{
								if (intRateType == 7 || intRateType == 22)
								{
									intTemp = 1;
								}
								else
								{
									intTemp = 2;
								}
								for (lngRW = 1; lngRW <= vsRate.Rows - intTemp; lngRW++)
								{
									// this will clear any other selection in the grid so the user can only choose one
									if (lngRW != vsRate.Row)
									{
										vsRate.TextMatrix(lngRW, 0, FCConvert.ToString(0));
										//vsRate.Cell(FCGrid.CellPropertySettings.flexcpPicture, lngRW, 0, "");
									}
									else
									{
										// and it will show the picture if chosen
										// vsRate.Cell(FCGrid.CellPropertySettings.flexcpPicture, lngRW, 0) = MDIParent.ImageList1.ListImages(5).Picture
									}
								}
							}
							else if (vsRate.Row == vsRate.Rows - 1 && intRateType == 6)
							{
								if (!boolFormatting)
								{
									mnuFileAddRateKey_Click();
								}
							}
							break;
						}
					default:
						{
							//if (vsRate.Row > 0)
							//{
							//	if (FCConvert.ToDouble(vsRate.TextMatrix(vsRate.Row, 0)) == 0)
							//	{
							//		// vsRate.Cell(FCGrid.CellPropertySettings.flexcpPicture, Row, 0) = MDIParent.ImageList1.ListImages(5).Picture
							//		vsRate.Cell(FCGrid.CellPropertySettings.flexcpPictureAlignment, vsRate.Row, 0, FCGrid.AlignmentSettings.flexAlignCenterCenter);
							//	}
							//	else
							//	{
							//		vsRate.Cell(FCGrid.CellPropertySettings.flexcpPicture, vsRate.Row, 0, "");
							//	}
							//}
							break;
						}
				}
				//end switch
			}
		}

		private void vsRate_GotFocus(object sender, EventArgs e)
		{
			if (vsRate.Rows > 1)
			{
				vsRate.Select(1, 0);
			}
			// If vsRate.Row = vsRate.rows - 1 And vsRate.Col = vsRate.Cols - 1 Then
			// vsRate.TabBehavior = flexTabControls
			// Else
			vsRate.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			// End If
			if (fraRateInfo.Visible)
				fraRateInfo.Visible = false;
		}

		private void vsRate_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (e.ColumnIndex == lngCommitmentDateCol)
			{
				vsRate.EditMask = "##/##/####";
				vsRate.EditMaxLength = 10;
			}
		}

		private void vsRate_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (vsRate.CurrentCell.IsInEditMode)
			{
			}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFileSave_Click(sender, e);
		}

		private void cmdFileAddRateKey_Click(object sender, EventArgs e)
		{
			this.mnuFileAddRateKey_Click(sender, e);
		}
	}
}
