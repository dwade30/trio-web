﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmLoadBack.
	/// </summary>
	public partial class frmLoadBack : BaseForm
	{
		public frmLoadBack()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmLoadBack InstancePtr
		{
			get
			{
				return (frmLoadBack)Sys.GetInstance(typeof(frmLoadBack));
			}
		}

		protected frmLoadBack _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               01/29/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/15/2005              *
		// ********************************************************
		clsDRWrapper rsRE = new clsDRWrapper();
		bool boolLoaded;
		bool boolLien;
		int intAction;
		int lngRateKey;
		DateTime dtDefaultInterestPTD;
		int lngColAccount;
		int lngColName;
		int lngColInterestPTD;
		int lngColIntOwed;
		int lngColTax1;
		int lngColTax2;
		int lngColTax3;
		int lngColTax4;
		int lngColOverwrite;
		// if the value is 0 then do not overwrite, if it is -1 then overwrite
		int lngHiddenCol;
		int lngYear;
		string strMod = "";
		bool boolPer2;
		bool boolPer3;
		bool boolPer4;
		bool blnSaved;
		clsDRWrapper rsRK = new clsDRWrapper();
		// VBto upgrade warning: lngPassYear As int	OnWrite(string)
		public object Init(int lngPassRK, int lngPassYear, bool boolPassLien)
		{
			object Init = null;
			// this is the ratekey
			lngRateKey = lngPassRK;
			// this is the actual year without the fifth digit
			lngYear = lngPassYear;
			boolLien = boolPassLien;
			rsRK.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(lngRateKey), modExtraModules.strCLDatabase);
			if (boolLien)
			{
				// will only use period 1, but should set all to true
				boolPer2 = true;
				boolPer3 = true;
				boolPer4 = true;
			}
			else
			{
				if (rsRK.EndOfFile())
				{
				}
				else
				{
					if (rsRK.Get_Fields_Int16("NumberOfPeriods") == 1)
					{
						boolPer2 = false;
						boolPer3 = false;
						boolPer4 = false;
					}
					else if (rsRK.Get_Fields_Int16("NumberOfPeriods") == 2)
					{
						boolPer2 = true;
						boolPer3 = false;
						boolPer4 = false;
					}
					else if (rsRK.Get_Fields_Int16("NumberOfPeriods") == 3)
					{
						boolPer2 = true;
						boolPer3 = true;
						boolPer4 = false;
					}
					else if (rsRK.Get_Fields_Int16("NumberOfPeriods") == 4)
					{
						boolPer2 = true;
						boolPer3 = true;
						boolPer4 = true;
					}
				}
			}
			if (modStatusPayments.Statics.boolRE)
			{
				strMod = "RE";
			}
			else
			{
				strMod = "PP";
			}
			txtDefaultDate.ToolTipText = "Effective interest date to start calculating interest for this bill.";
			ToolTip1.SetToolTip(lblInterestPTD, txtDefaultDate.ToolTipText);
			// "Effective interest date to start calculating interest for this bill."
			// these will keep track of the columns even if I move them
			lngColAccount = 0;
			lngColName = 1;
			lngColInterestPTD = 2;
			lngColIntOwed = 3;
			lngHiddenCol = 4;
			// this is the fifth number of the year
			lngColOverwrite = 5;
			lngColTax1 = 6;
			lngColTax2 = 7;
			lngColTax3 = 8;
			lngColTax4 = 9;
			this.Show(App.MainForm);
			FormatGrid();
			return Init;
		}

		private void frmLoadBack_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				boolLoaded = true;
			}
		}

		private void frmLoadBack_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Insert)
			{
				KeyCode = 0;
				if (cmdFileNew.Visible && cmdFileNew.Enabled)
				{
					mnuFileNew_Click();
				}
			}
		}

		private void frmLoadBack_Load(object sender, System.EventArgs e)
		{

			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			if (modStatusPayments.Statics.boolRE)
			{
				rsRE.OpenRecordset("Select * FROM Master", modExtraModules.strREDatabase);
			}
			else
			{
				rsRE.OpenRecordset("Select * FROM PPMaster", modExtraModules.strPPDatabase);
			}
			if (rsRE.EndOfFile())
			{
				if (modStatusPayments.Statics.boolRE)
				{
					FCMessageBox.Show("No Real Estate accounts have been found.", MsgBoxStyle.Critical, "Missing Accounts");
				}
				else
				{
					FCMessageBox.Show("No Personal Property accounts have been found.", MsgBoxStyle.Critical, "Missing Accounts");
				}
			}
		}

		private void frmLoadBack_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				if (fraAccountSearch.Visible)
				{
					ReturnFromSearch_2(0);
				}
				else if (vsAccounts.Rows > 1 && !blnSaved)
				{
					// MAL@20071220
					DialogResult = FCMessageBox.Show("Would you like to save your changes?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Save Changes");
					if (DialogResult == DialogResult.Yes)
					{
						if (SaveInformation())
						{
							blnSaved = true;
							Unload();
						}
					}
					else if (DialogResult == DialogResult.No)
					{
						Unload();
					}
					else if (DialogResult == DialogResult.Cancel)
					{
						// do nothing
						return;
					}
				}
				else
				{
					Unload();
				}
			}
			else if (KeyAscii == 13)
			{
				KeyAscii = 0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		public void Form_KeyPress(short KeyAscii)
		{
			frmLoadBack_KeyPress(this, new Wisej.Web.KeyPressEventArgs((char)KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			// MAL@20071109: Force check for changes
			Form_KeyPress(27);
		}

		private void mnuFileNew_Click(object sender, System.EventArgs e)
		{
			// add a line to the grid and put the focus there
			vsAccounts.AddItem("" + "\t" + "" + "\t" + txtDefaultDate.Text + "\t" + "0.00" + "\t" + "" + "\t" + "0.00" + "\t" + "0.00" + "\t" + "0.00" + "\t" + "0.00");
			if (vsAccounts.Visible && vsAccounts.Enabled)
			{
				vsAccounts.Focus();
				vsAccounts.Select(vsAccounts.Rows - 1, lngColAccount);
			}
		}

		public void mnuFileNew_Click()
		{
			mnuFileNew_Click(cmdFileNew, new System.EventArgs());
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			// save all of the information
			if (SaveInformation())
			{
				blnSaved = true;
				// this will clear the grid so that the user can keep going
				vsAccounts.Rows = 1;
			}
		}

		private bool LoadBackInformation()
		{
			bool LoadBackInformation = false;
			int lngErrorCode = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this routine will create a bill/lien record for the old records if needed
				int lngCT;
				clsDRWrapper rsLien = new clsDRWrapper();
				clsDRWrapper rsBill = new clsDRWrapper();
				clsDRWrapper rsLoadBack = new clsDRWrapper();
				clsDRWrapper rsBook = new clsDRWrapper();
				clsDRWrapper rsCollect = new clsDRWrapper();
				int lngLRN = 0;
				int lngWholeYear = 0;
				string strBookPage = "";
				// VBto upgrade warning: cCPOwner As cParty	OnWrite(cParty)
				cParty cCPOwner = new cParty();
				// VBto upgrade warning: cCPSecOwner As cParty	OnWrite(cParty)
				cParty cCPSecOwner = new cParty();
				cPartyAddress cCPAddress;
				LoadBackInformation = true;
				lngErrorCode = 1;
				// open the rsLoadBack recordset to add records to
				rsLoadBack.OpenRecordset("SELECT * FROM LoadBackOriginal WHERE ID = 0", modExtraModules.strCLDatabase);
				lngErrorCode = 2;
				// open the lien table if needed
				if (boolLien)
				{
					lngErrorCode = 3;
					rsLien.OpenRecordset("SELECT * FROM LienRec", modExtraModules.strCLDatabase);
				}
				lngErrorCode = 4;
				// open the billingmaster table
				rsBill.OpenRecordset("SELECT * FROM BillingMaster WHERE BillingType = '" + strMod + "'", modExtraModules.strCLDatabase);
				if (modStatusPayments.Statics.boolRE)
				{
					rsBook.OpenRecordset("SELECT * FROM BookPage ORDER BY Line", modExtraModules.strREDatabase);
				}
				lngErrorCode = 5;
				for (lngCT = 1; lngCT <= vsAccounts.Rows - 1; lngCT++)
				{
					if (Conversion.Val(vsAccounts.TextMatrix(lngCT, lngHiddenCol)) > 0)
					{
						// make sure that this account has been finished
						// create the whole year
						lngWholeYear = FCConvert.ToInt32(Math.Round(Conversion.Val(lngYear.ToString() + vsAccounts.TextMatrix(lngCT, lngHiddenCol))));
						if (Conversion.Val(vsAccounts.TextMatrix(lngCT, lngColOverwrite)) > 0)
						{
							rsBill.FindFirstRecord2("Account,BillingYear", vsAccounts.TextMatrix(lngCT, lngColAccount) + "," + FCConvert.ToString(lngWholeYear), ",");
							if (rsBill.NoMatch)
							{
								FCMessageBox.Show("The bill for account " + vsAccounts.TextMatrix(lngCT, lngColAccount) + " and year " + FCConvert.ToString(lngWholeYear) + " could not be found.", MsgBoxStyle.Critical, "Missing Bill Record");
								LoadBackInformation = false;
								return LoadBackInformation;
							}
							else
							{
								modGlobalFunctions.AddCYAEntry_62("CL", "Overwriting Bill Record", vsAccounts.TextMatrix(lngCT, lngColTax1), lngWholeYear.ToString(), strMod);
								rsBill.Edit();
							}
						}
						else
						{
							rsBill.AddNew();
						}
						// find the account in the RE master
						if (modStatusPayments.Statics.boolRE)
						{
							rsRE.FindFirstRecord("RSAccount", vsAccounts.TextMatrix(lngCT, lngColAccount));
							cCPOwner = modGlobal.Statics.gCPCtlr.GetParty(FCConvert.ToInt32(rsRE.Get_Fields_Int32("OwnerPartyID")));
							if (Conversion.Val(rsRE.Get_Fields_Int32("SecOwnerPartyID")) != 0)
							{
								cCPSecOwner = modGlobal.Statics.gCPCtlr.GetParty(FCConvert.ToInt32(rsRE.Get_Fields_Int32("SecOwnerPartyID")));
							}
							else
							{
								cCPSecOwner = null;
							}
							cCPAddress = cCPOwner.GetAddress("RE", rsRE.Get_Fields_Int32("RSAccount"));
						}
						else
						{
							rsRE.FindFirstRecord("Account", vsAccounts.TextMatrix(lngCT, lngColAccount));
							cCPOwner = modGlobal.Statics.gCPCtlr.GetParty(FCConvert.ToInt32(rsRE.Get_Fields_Int32("PartyID")));
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							cCPAddress = cCPOwner.GetAddress("PP", FCConvert.ToInt32(rsRE.Get_Fields("Account")));
						}
						if (boolLien)
						{
							// if this is a lien then create a lien record for it first
							if (Conversion.Val(vsAccounts.TextMatrix(lngCT, lngColOverwrite)) > 0)
							{
								// this is marked as overwrite
								rsLien.FindFirstRecord("ID", rsBill.Get_Fields_Int32("LienRecordNumber"));
								if (rsLien.NoMatch)
								{
									// this means that is was a regular record before and a Lien record has not been created yet...this should automatically add the lien rec
									rsLien.AddNew();
								}
								else
								{
									modGlobalFunctions.AddCYAEntry_62("CL", "Overwriting Lien Record", vsAccounts.TextMatrix(lngCT, lngColTax1), lngYear.ToString());
									rsLien.Edit();
								}
							}
							else
							{
								rsLien.AddNew();
							}
							lngErrorCode = 6;
							rsLien.Set_Fields("Principal", modGlobal.Round(FCConvert.ToDouble(vsAccounts.TextMatrix(lngCT, lngColTax1)), 2));
							rsLien.Set_Fields("Interest", modGlobal.Round(FCConvert.ToDouble(vsAccounts.TextMatrix(lngCT, lngColTax2)), 2));
							rsLien.Set_Fields("Costs", modGlobal.Round(FCConvert.ToDouble(vsAccounts.TextMatrix(lngCT, lngColTax3)), 2));
							rsLien.Set_Fields("RateKey", lngRateKey);
							lngErrorCode = 7;
							// create a CHGINT Line for this bill
							rsLien.Set_Fields("InterestCharged", modGlobal.Round(FCConvert.ToDouble(vsAccounts.TextMatrix(lngCT, lngColIntOwed)), 2));
							rsLien.Set_Fields("InterestCharged", rsLien.Get_Fields_Decimal("InterestCharged") * -1);
							lngErrorCode = 8;
							rsLien.Set_Fields("MaturityFee", 0);
							rsLien.Set_Fields("PrincipalPaid", 0);
							rsLien.Set_Fields("InterestPaid", 0);
							rsLien.Set_Fields("CostsPaid", 0);
							rsLien.Set_Fields("DateCreated", DateTime.Now);
							rsLien.Set_Fields("InterestAppliedThroughDate", DateAndTime.DateValue(vsAccounts.TextMatrix(lngCT, lngColInterestPTD)));
							rsLien.Set_Fields("Status", "A");
							rsLien.Update(true);
							lngLRN = FCConvert.ToInt32(rsLien.Get_Fields_Int32("ID"));
							if (FCConvert.ToDouble(vsAccounts.TextMatrix(lngCT, lngColIntOwed)) != 0)
							{
								CreateCHGINT_656(FCConvert.ToDouble(vsAccounts.TextMatrix(lngCT, lngColIntOwed)) * -1, true, lngLRN, lngWholeYear, DateAndTime.DateValue(vsAccounts.TextMatrix(lngCT, lngColInterestPTD)), FCConvert.ToInt32(FCConvert.ToDouble(vsAccounts.TextMatrix(lngCT, lngColAccount))));
							}
						}
						else
						{
							// if this is not a lien then no lien record number needs to be stored
							lngLRN = 0;
						}
						lngErrorCode = 9;
						// load back each account
						lngErrorCode = 10;
						rsBill.Set_Fields("Account", vsAccounts.TextMatrix(lngCT, lngColAccount));
						rsBill.Set_Fields("BillingType", strMod);
						rsBill.Set_Fields("BillingYear", lngWholeYear);
						if (modStatusPayments.Statics.boolRE)
						{
							if (Strings.Trim(vsAccounts.TextMatrix(lngCT, lngColName)) != "")
							{
								if (Strings.Trim(vsAccounts.TextMatrix(lngCT, lngColName)) == Strings.Trim(rsRE.Get_Fields_String("DeedName1").Trim()))
								{
									rsBill.Set_Fields("Name1", Strings.Trim(rsRE.Get_Fields_String("DeedName1")));
									
									rsBill.Set_Fields("Name2", Strings.Trim(rsRE.Get_Fields_String("DeedName2")));
									
								}
								else
								{
									rsBill.Set_Fields("Name1", Strings.Trim(vsAccounts.TextMatrix(lngCT, lngColName)));
								}
							}
							else
							{
								rsBill.Set_Fields("Name1", Strings.Trim(rsRE.Get_Fields_String("DeedName1")));
                                rsBill.Set_Fields("Name2",rsRE.Get_Fields_String("DeedName2").ToString());
							}
							rsBill.Set_Fields("Address1", Strings.Trim(cCPAddress.Address1));
							rsBill.Set_Fields("Address2", Strings.Trim(cCPAddress.Address2));
							rsBill.Set_Fields("Address3", Strings.Trim(cCPAddress.City));
							if (cCPAddress.State != "")
							{
								rsBill.Set_Fields("Address3", FCConvert.ToString(rsBill.Get_Fields_String("Address3")) + ", " + cCPAddress.State);
							}
							if (cCPAddress.Zip != "")
							{
								rsBill.Set_Fields("Address3", FCConvert.ToString(rsBill.Get_Fields_String("Address3")) + " " + cCPAddress.Zip);
							}
							rsBill.Set_Fields("MapLot", Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("RSMapLot"))));
							if (Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("RSLOCNUMALPH"))) != "")
							{
								rsBill.Set_Fields("StreetNumber", FCConvert.ToString(Conversion.Val(Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("RSLOCNUMALPH"))))));
							}
							rsBill.Set_Fields("Apt", Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("RSLOCAPT"))));
							rsBill.Set_Fields("StreetName", Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("RSLOCSTREET"))));
							rsBill.Set_Fields("LandValue", Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_Int32("RLLANDVAL"))));
							rsBill.Set_Fields("BuildingValue", Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_Int32("RLBLDGVAL"))));
						}
						else
						{
							if (Strings.Trim(vsAccounts.TextMatrix(lngCT, lngColName)) != "")
							{
								if (Strings.Trim(vsAccounts.TextMatrix(lngCT, lngColName)) == Strings.Trim(cCPOwner.FullNameLastFirst))
								{
									rsBill.Set_Fields("Name1", Strings.Trim(cCPOwner.FullNameLastFirst));
									rsBill.Set_Fields("Name2", "");
								}
								else
								{
									rsBill.Set_Fields("Name1", Strings.Trim(vsAccounts.TextMatrix(lngCT, lngColName)));
								}
							}
							else
							{
								rsBill.Set_Fields("Name1", Strings.Trim(cCPOwner.FullNameLastFirst));
								rsBill.Set_Fields("Name2", "");
							}
							rsBill.Set_Fields("Address1", Strings.Trim(cCPAddress.Address1));
							rsBill.Set_Fields("Address2", Strings.Trim(cCPAddress.Address2));
							rsBill.Set_Fields("Address3", Strings.Trim(cCPAddress.City) + ", " + Strings.Trim(cCPAddress.State) + " " + Strings.Trim(cCPAddress.Zip));
							rsBill.Set_Fields("MapLot", "");
							if (!rsRE.IsFieldNull("StreetNumber"))
							{
								// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
								rsBill.Set_Fields("StreetNumber", FCConvert.ToString(Conversion.Val(Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("StreetNumber"))))));
							}
							rsBill.Set_Fields("Apt", "");
							rsBill.Set_Fields("StreetName", Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("STREET"))));
						}
						lngErrorCode = 11;
						if (boolLien)
						{
							rsBill.Set_Fields("TaxDue1", modGlobal.Round(FCConvert.ToDouble(vsAccounts.TextMatrix(lngCT, lngColTax1)), 2));
							rsBill.Set_Fields("TaxDue2", 0);
							rsBill.Set_Fields("TaxDue3", 0);
							rsBill.Set_Fields("TaxDue4", 0);
							rsBill.Set_Fields("InterestCharged", 0);
							// CDbl(vsAccounts.TextMatrix(lngCT, lngColIntOwed)) * -1
							// CreateCHGINT CDbl(vsAccounts.TextMatrix(lngCT, lngColIntOwed)) * -1, True, lngLRN, lngWholeYear, CDate(vsAccounts.TextMatrix(lngCT, lngColInterestPTD)), CDbl(vsAccounts.TextMatrix(lngCT, lngColAccount))
							rsBill.Set_Fields("LienStatusEligibility", 6);
							rsBill.Set_Fields("LienProcessStatus", 6);
							rsBill.Set_Fields("WhetherBilledBefore", "L");
							// this should
							rsBill.Set_Fields("LienStatusEligibility", 5);
							rsBill.Set_Fields("LienProcessStatus", 4);
						}
						else
						{
							rsBill.Set_Fields("TaxDue1", modGlobal.Round(FCConvert.ToDouble(vsAccounts.TextMatrix(lngCT, lngColTax1)), 2));
							rsBill.Set_Fields("TaxDue2", modGlobal.Round(FCConvert.ToDouble(vsAccounts.TextMatrix(lngCT, lngColTax2)), 2));
							rsBill.Set_Fields("TaxDue3", modGlobal.Round(FCConvert.ToDouble(vsAccounts.TextMatrix(lngCT, lngColTax3)), 2));
							rsBill.Set_Fields("TaxDue4", modGlobal.Round(FCConvert.ToDouble(vsAccounts.TextMatrix(lngCT, lngColTax4)), 2));
							// create a CHGINT Line for this bill if needed
							rsBill.Set_Fields("InterestCharged", FCConvert.ToDouble(vsAccounts.TextMatrix(lngCT, lngColIntOwed)) * -1);
							// If CDbl(vsAccounts.TextMatrix(lngCT, lngColIntOwed)) <> 0 Then
							// CreateCHGINT CDbl(vsAccounts.TextMatrix(lngCT, lngColIntOwed)) * -1, False, .Get_Fields("BillKey"), lngWholeYear, CDate(vsAccounts.TextMatrix(lngCT, lngColInterestPTD)), CDbl(vsAccounts.TextMatrix(lngCT, lngColAccount))
							// End If
							rsBill.Set_Fields("LienStatusEligibility", 0);
							rsBill.Set_Fields("LienProcessStatus", 0);
							rsBill.Set_Fields("WhetherBilledBefore", "A");
						}
						lngErrorCode = 12;
						rsBill.Set_Fields("LienRecordNumber", lngLRN);
						rsBill.Set_Fields("PrincipalPaid", 0);
						rsBill.Set_Fields("InterestPaid", 0);
						rsBill.Set_Fields("DemandFees", 0);
						rsBill.Set_Fields("DemandFeesPaid", 0);
						rsBill.Set_Fields("InterestAppliedThroughDate", DateAndTime.DateValue(vsAccounts.TextMatrix(lngCT, lngColInterestPTD)));
						rsBill.Set_Fields("RateKey", lngRateKey);
						rsBill.Set_Fields("TransferFromBillingDateFirst", DateTime.Now);
						rsBill.Set_Fields("TransferFromBillingDateLast", DateTime.Now);
						rsBill.Set_Fields("OwnerGroup", 0);
						if (modStatusPayments.Statics.boolRE)
						{
							rsBill.Set_Fields("Acres", FCConvert.ToString(Conversion.Val(rsRE.Get_Fields_Double("PIAcres"))));
						}
						if (modStatusPayments.Statics.boolRE)
						{
							rsBook.FindFirstRecord2("Account,Card", FCConvert.ToString(rsRE.Get_Fields_Int32("RSAccount")) + ",1", ",");
							if (rsBook.NoMatch)
							{
								strBookPage = "";
							}
							else
							{
								strBookPage = "B" + FCConvert.ToString(rsBook.Get_Fields("Book")) + "P" + FCConvert.ToString(rsBook.Get_Fields("Page"));
							}
							rsBill.Set_Fields("BookPage", strBookPage);
						}
						rsBill.Update(true);
						if (!boolLien && FCConvert.ToDouble(vsAccounts.TextMatrix(lngCT, lngColIntOwed)) != 0)
						{
							CreateCHGINT_674(FCConvert.ToDouble(vsAccounts.TextMatrix(lngCT, lngColIntOwed)) * -1, false, FCConvert.ToInt32(rsBill.Get_Fields_Int32("ID")), lngWholeYear, DateAndTime.DateValue(vsAccounts.TextMatrix(lngCT, lngColInterestPTD)), FCConvert.ToInt32(FCConvert.ToDouble(vsAccounts.TextMatrix(lngCT, lngColAccount))));
						}
						lngErrorCode = 13;
						// save a copy of the brand new record in a seperate table
						rsLoadBack.AddNew();
						rsLoadBack.Set_Fields("Account", vsAccounts.TextMatrix(lngCT, lngColAccount));
						lngErrorCode = 14;
						rsLoadBack.Set_Fields("BillingType", strMod);
						lngErrorCode = 15;
						rsLoadBack.Set_Fields("BillingYear", lngWholeYear);
						if (modStatusPayments.Statics.boolRE)
						{
							if (Strings.Trim(vsAccounts.TextMatrix(lngCT, lngColName)) != "")
							{
								if (Strings.Trim(vsAccounts.TextMatrix(lngCT, lngColName)) == Strings.Trim(rsRE.Get_Fields_String("DeedName1")))
								{
									rsLoadBack.Set_Fields("Name1", rsRE.Get_Fields_String("DeedName1").Trim());
									rsLoadBack.Set_Fields("Name2", rsRE.Get_Fields_String("DeedName2").Trim());
								}
								else
								{
									rsLoadBack.Set_Fields("Name1", Strings.Trim(vsAccounts.TextMatrix(lngCT, lngColName)));
								}
							}
							else
							{
								rsLoadBack.Set_Fields("Name1", rsRE.Get_Fields_String("DeedName1").Trim());
								rsLoadBack.Set_Fields("Name2", rsRE.Get_Fields_String("DeedName2").Trim());
							}
							lngErrorCode = 16;
							rsLoadBack.Set_Fields("Address1", Strings.Trim(cCPAddress.Address1));
							rsLoadBack.Set_Fields("Address2", Strings.Trim(cCPAddress.Address2));
							rsLoadBack.Set_Fields("Address3", Strings.Trim(cCPAddress.City));
							lngErrorCode = 17;
							rsLoadBack.Set_Fields("MapLot", Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("RSMapLot"))));
							if (Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("RSLOCNUMALPH"))) != "")
							{
								rsLoadBack.Set_Fields("StreetNumber", FCConvert.ToString(Conversion.Val(Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("RSLOCNUMALPH"))))));
							}
							lngErrorCode = 18;
							rsLoadBack.Set_Fields("Apt", Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("RSLOCAPT"))));
							rsLoadBack.Set_Fields("StreetName", Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("RSLOCSTREET"))));
							lngErrorCode = 19;
							rsLoadBack.Set_Fields("LandValue", Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_Int32("RLLANDVAL"))));
							rsLoadBack.Set_Fields("BuildingValue", Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_Int32("RLBLDGVAL"))));
						}
						else
						{
							if (Strings.Trim(vsAccounts.TextMatrix(lngCT, lngColName)) != "")
							{
								if (Strings.Trim(vsAccounts.TextMatrix(lngCT, lngColName)) == Strings.Trim(cCPOwner.FullNameLastFirst))
								{
									rsLoadBack.Set_Fields("Name1", Strings.Trim(cCPOwner.FullNameLastFirst));
									rsLoadBack.Set_Fields("Name2", "");
								}
								else
								{
									rsLoadBack.Set_Fields("Name1", Strings.Trim(vsAccounts.TextMatrix(lngCT, lngColName)));
								}
							}
							else
							{
								rsLoadBack.Set_Fields("Name1", Strings.Trim(cCPOwner.FullNameLastFirst));
								rsLoadBack.Set_Fields("Name2", "");
							}
							lngErrorCode = 116;
							rsLoadBack.Set_Fields("Address1", Strings.Trim(cCPAddress.Address1));
							rsLoadBack.Set_Fields("Address2", Strings.Trim(cCPAddress.Address2));
							rsLoadBack.Set_Fields("Address3", Strings.Trim(cCPAddress.City) + ", " + Strings.Trim(cCPAddress.State) + " " + Strings.Trim(cCPAddress.Zip));
							lngErrorCode = 117;
							rsLoadBack.Set_Fields("MapLot", "");
							if (!rsRE.IsFieldNull("StreetNumber"))
							{
								// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
								rsLoadBack.Set_Fields("StreetNumber", FCConvert.ToString(Conversion.Val(Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("StreetNumber"))))));
							}
							rsLoadBack.Set_Fields("Apt", "");
							rsLoadBack.Set_Fields("StreetName", Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("STREET"))));
						}
						lngErrorCode = 20;
						if (boolLien)
						{
							rsLoadBack.Set_Fields("TaxDue1", modGlobal.Round(FCConvert.ToDouble(vsAccounts.TextMatrix(lngCT, lngColTax1)), 2));
							rsLoadBack.Set_Fields("TaxDue2", 0);
							rsLoadBack.Set_Fields("TaxDue3", 0);
							rsLoadBack.Set_Fields("TaxDue4", 0);
							lngErrorCode = 21;
							rsLoadBack.Set_Fields("InterestCharged", 0);
							// CDbl(vsAccounts.TextMatrix(lngCT, lngColIntOwed))
							rsLoadBack.Set_Fields("LienStatusEligibility", 6);
							rsLoadBack.Set_Fields("LienProcessStatus", 6);
							lngErrorCode = 22;
							rsLoadBack.Set_Fields("WhetherBilledBefore", "L");
						}
						else
						{
							rsLoadBack.Set_Fields("TaxDue1", modGlobal.Round(FCConvert.ToDouble(vsAccounts.TextMatrix(lngCT, lngColTax1)), 2));
							rsLoadBack.Set_Fields("TaxDue2", modGlobal.Round(FCConvert.ToDouble(vsAccounts.TextMatrix(lngCT, lngColTax2)), 2));
							rsLoadBack.Set_Fields("TaxDue3", modGlobal.Round(FCConvert.ToDouble(vsAccounts.TextMatrix(lngCT, lngColTax3)), 2));
							rsLoadBack.Set_Fields("TaxDue4", modGlobal.Round(FCConvert.ToDouble(vsAccounts.TextMatrix(lngCT, lngColTax4)), 2));
							lngErrorCode = 23;
							// create a CHGINT Line for this bill
							rsLoadBack.Set_Fields("InterestCharged", FCConvert.ToDouble(vsAccounts.TextMatrix(lngCT, lngColIntOwed)) * -1);
							// CreateCHGINT CDbl(vsAccounts.TextMatrix(lngCt, lngColIntOwed)), False, .Get_Fields("BillKey"), lngWholeYear, CDbl(vsAccounts.TextMatrix(lngCt, lngColInterestPTD))
							lngErrorCode = 24;
							rsLoadBack.Set_Fields("LienStatusEligibility", 0);
							rsLoadBack.Set_Fields("LienProcessStatus", 0);
							lngErrorCode = 25;
							rsLoadBack.Set_Fields("WhetherBilledBefore", "A");
						}
						lngErrorCode = 26;
						rsLoadBack.Set_Fields("LienRecordNumber", lngLRN);
						rsLoadBack.Set_Fields("PrincipalPaid", 0);
						rsLoadBack.Set_Fields("InterestPaid", 0);
						lngErrorCode = 27;
						rsLoadBack.Set_Fields("DemandFees", 0);
						rsLoadBack.Set_Fields("DemandFeesPaid", 0);
						rsLoadBack.Set_Fields("InterestAppliedThroughDate", DateAndTime.DateValue(vsAccounts.TextMatrix(lngCT, lngColInterestPTD)));
						lngErrorCode = 28;
						rsLoadBack.Set_Fields("RateKey", lngRateKey);
						rsLoadBack.Set_Fields("TransferFromBillingDateFirst", DateTime.Now);
						rsLoadBack.Set_Fields("TransferFromBillingDateLast", DateTime.Now);
						lngErrorCode = 29;
						rsLoadBack.Set_Fields("OwnerGroup", 0);
						if (modStatusPayments.Statics.boolRE)
						{
							rsLoadBack.Set_Fields("Acres", FCConvert.ToString(Conversion.Val(rsRE.Get_Fields_Double("PIAcres"))));
							rsLoadBack.Set_Fields("BookPage", strBookPage);
							// Trim(rsRE.Get_Fields("RSBook")) & Trim(rsRE.Get_Fields("RSPage"))
						}
						lngErrorCode = 30;
						rsLoadBack.Update(true);
						// MAL@20080603: Compare Bill Year with LoadBack Year. If Later, Update LastBillYear
						// Tracker Reference: 14051
						rsCollect.OpenRecordset("SELECT * FROM Collections", modExtraModules.strCLDatabase);
						if (rsCollect.RecordCount() > 0)
						{
							if (lngWholeYear > FCConvert.ToInt32(rsCollect.Get_Fields_Int32("LastBilledYear")))
							{
								rsCollect.Edit();
								rsCollect.Set_Fields("LastBilledYear", lngWholeYear);
								rsCollect.Update();
							}
							else
							{
								// Do Nothing - Bill Year before last billed year
							}
						}
					}
				}
				lngErrorCode = 31;
				return LoadBackInformation;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				LoadBackInformation = false;
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Load Back Error - " + FCConvert.ToString(lngErrorCode));
			}
			return LoadBackInformation;
		}

		private bool SaveInformation()
		{
			bool SaveInformation = false;
			SaveInformation = true;
			if (ValidateInformation())
			{
				if (LoadBackInformation())
				{
					FCMessageBox.Show("The load back was successful.", MsgBoxStyle.Information, "Success");
				}
				else
				{
					SaveInformation = false;
					FCMessageBox.Show("The load back was not successful.", MsgBoxStyle.Information, "TRIO Software");
				}
			}
			else
			{
				SaveInformation = false;
			}
			return SaveInformation;
		}

		private bool ValidateInformation()
		{
			bool ValidateInformation = false;
			int lngRow;
			int lngCounter;
			ValidateInformation = true;
			vsAccounts.Select(0, 0);
			// this will make the edittext be put into the grid
			// check to make sure that there are accounts loaded
			if (vsAccounts.Rows <= 1)
			{
				ValidateInformation = false;
				FCMessageBox.Show("Please add an account to be loaded.", MsgBoxStyle.Information, "No Accounts");
				return ValidateInformation;
			}
			for (lngRow = 1; lngRow <= vsAccounts.Rows - 1; lngRow++)
			{
				if (Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColTax1)) == 0)
				{
					if (Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColTax2)) == 0)
					{
						if (Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColTax3)) == 0)
						{
							if (Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColTax4)) == 0)
							{
								if (Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColIntOwed)) == 0)
								{
									ValidateInformation = false;
									FCMessageBox.Show("All accounts shown must have amounts to create a bill.", MsgBoxStyle.Critical, "Missing Amount");
									if (vsAccounts.Visible && vsAccounts.Enabled)
									{
										vsAccounts.Focus();
										vsAccounts.Select(lngRow, lngColIntOwed);
										return ValidateInformation;
									}
								}
							}
						}
					}
				}
				for (lngCounter = 1; lngCounter <= vsAccounts.Rows - 1; lngCounter++)
				{
					if (lngCounter != lngRow)
					{
						if (vsAccounts.TextMatrix(lngCounter, lngColAccount) == vsAccounts.TextMatrix(lngRow, lngColAccount))
						{
							ValidateInformation = false;
							FCMessageBox.Show("You have multiple entries for the same account.  You must delete all but 1 before you may continue.", MsgBoxStyle.Information, "Multiple Accounts");
							if (vsAccounts.Visible && vsAccounts.Enabled)
							{
								vsAccounts.Focus();
								vsAccounts.Select(lngRow, lngColAccount);
								return ValidateInformation;
							}
						}
					}
				}
				// MAL@20080528: Add check that account number is not blank
				// Tracker Reference: 13884
				if (Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColAccount)) == 0 || vsAccounts.TextMatrix(lngRow, lngColAccount) == "")
				{
					ValidateInformation = false;
					FCMessageBox.Show("You must enter an account number to continue.", MsgBoxStyle.Exclamation, "No Account Number");
					if (vsAccounts.Visible && vsAccounts.Enabled)
					{
						vsAccounts.Focus();
						vsAccounts.Select(lngRow, lngColAccount);
						return ValidateInformation;
					}
				}
			}
			return ValidateInformation;
		}

		private void FormatSearchGrid()
		{
			vsAccountSearch.Cols = 4;
			vsAccountSearch.ColWidth(0, 0);
			if (modStatusPayments.Statics.boolRE)
			{
				vsAccountSearch.ColWidth(1, FCConvert.ToInt32(vsAccountSearch.WidthOriginal * 0.12));
				// account number
				vsAccountSearch.ColWidth(2, FCConvert.ToInt32(vsAccountSearch.WidthOriginal * 0.55));
				// name
				vsAccountSearch.ColWidth(3, FCConvert.ToInt32(vsAccountSearch.WidthOriginal * 0.31));
				// maplot
			}
			else
			{
				vsAccountSearch.ColWidth(1, FCConvert.ToInt32(vsAccountSearch.WidthOriginal * 0.12));
				// account number
				vsAccountSearch.ColWidth(2, FCConvert.ToInt32(vsAccountSearch.WidthOriginal * 0.88));
				// name
				vsAccountSearch.ColWidth(3, FCConvert.ToInt32(vsAccountSearch.WidthOriginal * 0.0));
				// maplot     there is no map lot in PP
			}
			vsAccountSearch.ExtendLastCol = true;
			vsAccountSearch.TextMatrix(0, 1, "Account");
			vsAccountSearch.TextMatrix(0, 2, "Name");
			vsAccountSearch.TextMatrix(0, 3, "Map Lot");
			vsAccountSearch.ExplorerBar = FCGrid.ExplorerBarSettings.flexExSort;
		}

		private void FormatGrid()
		{
			vsAccounts.Cols = 10;
			if (boolLien)
			{
				vsAccounts.ColWidth(lngColAccount, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.06));
				vsAccounts.ColWidth(lngColName, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.26));
				vsAccounts.ColWidth(lngColInterestPTD, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.15));
				vsAccounts.ColWidth(lngColIntOwed, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.13));
				vsAccounts.ColWidth(lngHiddenCol, 0);
				vsAccounts.ColWidth(lngColOverwrite, 0);
				vsAccounts.ColWidth(lngColTax1, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.13));
				vsAccounts.ColWidth(lngColTax2, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.13));
				vsAccounts.ColWidth(lngColTax3, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.13));
				vsAccounts.ColWidth(lngColTax4, 0);
			}
			else
			{
				vsAccounts.ColWidth(lngColAccount, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.06));
				vsAccounts.ColWidth(lngColName, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.18));
				vsAccounts.ColWidth(lngColInterestPTD, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.15));
				vsAccounts.ColWidth(lngColIntOwed, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.12));
				vsAccounts.ColWidth(lngHiddenCol, 0);
				vsAccounts.ColWidth(lngColOverwrite, 0);
				vsAccounts.ColWidth(lngColTax1, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.12));
				vsAccounts.ColWidth(lngColTax2, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.12));
				vsAccounts.ColWidth(lngColTax3, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.12));
				vsAccounts.ColWidth(lngColTax4, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.12));
			}
			vsAccounts.ExtendLastCol = true;
            //FC:FINAL:BSE #2792 scrollbars should be enabled 
			//vsAccounts.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			// set the alignment
			vsAccounts.ColAlignment(lngColAccount, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsAccounts.ColAlignment(lngColInterestPTD, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsAccounts.ColAlignment(lngColIntOwed, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsAccounts.ColAlignment(lngColTax1, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsAccounts.ColAlignment(lngColTax2, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsAccounts.ColAlignment(lngColTax3, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsAccounts.ColAlignment(lngColTax4, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsAccounts.ColFormat(lngColIntOwed, "#,##0.00");
			vsAccounts.ColFormat(lngColTax1, "#,##0.00");
			vsAccounts.ColFormat(lngColTax2, "#,##0.00");
			vsAccounts.ColFormat(lngColTax3, "#,##0.00");
			vsAccounts.ColFormat(lngColTax4, "#,##0.00");
			vsAccounts.ColFormat(lngColInterestPTD, "MM/dd/yyyy");
			vsAccounts.TextMatrix(0, lngColAccount, "Acct");
			vsAccounts.TextMatrix(0, lngColName, "Name");
			vsAccounts.TextMatrix(0, lngColInterestPTD, "Int Pd Date");
			vsAccounts.TextMatrix(0, lngColIntOwed, "Int Owed");
			vsAccounts.AllowUserResizing = FCGrid.AllowUserResizeSettings.flexResizeColumns;
			vsAccounts.ExplorerBar = FCGrid.ExplorerBarSettings.flexExSortShow;
			if (boolLien)
			{
				vsAccounts.TextMatrix(0, lngColTax1, "Principal");
				vsAccounts.TextMatrix(0, lngColTax2, "PreLien Int");
				vsAccounts.TextMatrix(0, lngColTax3, "Costs");
				vsAccounts.TextMatrix(0, lngColTax4, "");
				vsAccounts.TextMatrix(0, lngHiddenCol, "");
				vsAccounts.TextMatrix(0, lngColOverwrite, "0");
			}
			else
			{
				vsAccounts.TextMatrix(0, lngColTax1, "Tax 1");
				vsAccounts.TextMatrix(0, lngColTax2, "Tax 2");
				vsAccounts.TextMatrix(0, lngColTax3, "Tax 3");
				vsAccounts.TextMatrix(0, lngColTax4, "Tax 4");
				vsAccounts.TextMatrix(0, lngHiddenCol, "");
				vsAccounts.TextMatrix(0, lngColOverwrite, "0");
			}
			// set the default information
			txtShowYear.Text = FCConvert.ToString(lngYear);
			txtDefaultDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			fraAccounts.Enabled = true;
		}
		// VBto upgrade warning: lngAcct As int	OnWrite(double, int)
		private void LoadAccountInformation_6(int lngAcct, int lngRow)
		{
			LoadAccountInformation(ref lngAcct, ref lngRow);
		}

		private void LoadAccountInformation(ref int lngAcct, ref int lngRow)
		{
			// this function will accept an account number and will fill the grid with the name from that account
			clsDRWrapper rsBillCheck = new clsDRWrapper();
			clsDRWrapper rsPayments = new clsDRWrapper();
			int lngBillingYearNumber;
			// VBto upgrade warning: cCPOwner As cParty	OnWrite(cParty)
			cParty cCPOwner = new cParty();
			cParty cCPSecOwner = new cParty();
			cPartyAddress cCPAddress;
			if (lngRow == 0)
			{
				// this will add a row so that the search does not write over any information
				vsAccounts.AddItem("");
				lngRow = vsAccounts.Rows - 1;
			}
			lngBillingYearNumber = 1;
			// check the billingmaster to find out if any other bills have been created for this year
			rsBillCheck.OpenRecordset("SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAcct) + " AND BillingYear/10 = " + FCConvert.ToString(lngYear) + " AND BillingType = '" + strMod + "'");
			if (rsBillCheck.EndOfFile())
			{
				// none created...keep going
				vsAccounts.TextMatrix(lngRow, lngColOverwrite, FCConvert.ToString(0));
			}
			else
			{
				// ask the user if they still want to create a bill for this account even though it has been created before
				if (FCMessageBox.Show("There is a bill already created for account " + FCConvert.ToString(lngAcct) + ".  Do you want to continue the load back process for this account and year?", MsgBoxStyle.Question | MsgBoxStyle.YesNoCancel, "Existing Bill") == DialogResult.Yes)
				{
					// get the last bill number used at the end of the year
					while (!rsBillCheck.EndOfFile())
					{
						// this will find the greatest last digit
						if ((FCUtils.iMod(FCConvert.ToInt16(rsBillCheck.Get_Fields_Int32("BillingYear")), 10)) > lngBillingYearNumber)
						{
							lngBillingYearNumber = (FCUtils.iMod(FCConvert.ToInt16(rsBillCheck.Get_Fields_Int32("BillingYear")), 10));
						}
						rsBillCheck.MoveNext();
					}
					rsBillCheck.MoveFirst();
					DialogResult = FCMessageBox.Show("Would you like to overwrite the current bill?  Select No to create another bill.", MsgBoxStyle.Question | MsgBoxStyle.YesNoCancel, "Overwrite Bill");
					if (DialogResult == DialogResult.Yes)
					{
						// check to see if there are already payments.  If so, then do not allow the overwrite
						if (((rsBillCheck.Get_Fields_Int32("LienRecordNumber"))) == 0)
						{
							rsPayments.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(rsBillCheck.Get_Fields_Int32("ID")) + " AND BillCode <> 'L' AND Code <> 'I'", modExtraModules.strCLDatabase);
							if (rsPayments.EndOfFile())
							{
								vsAccounts.TextMatrix(lngRow, lngColOverwrite, FCConvert.ToString(1));
								//FC:FINAL:DDU #i106 deleted '*' from sql statement
								rsPayments.Execute("DELETE FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(rsBillCheck.Get_Fields_Int32("ID")) + " AND BillCode <> 'L'", modExtraModules.strCLDatabase);
							}
							else
							{
								// the user has payments on this bill
								FCMessageBox.Show("This bill already has payments or adjustments made against it.  Please write off this bill manually for audit purposes.", MsgBoxStyle.Exclamation, "Cannot Overwrite");
								vsAccounts.TextMatrix(lngRow, lngColOverwrite, FCConvert.ToString(0));
								// add a another bill to the file
								lngBillingYearNumber += 1;
							}
						}
						else
						{
							rsPayments.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(rsBillCheck.Get_Fields_Int32("LienRecordNumber")) + " AND BillCode = 'L' AND Code <> 'I'", modExtraModules.strCLDatabase);
							if (rsPayments.EndOfFile())
							{
								vsAccounts.TextMatrix(lngRow, lngColOverwrite, FCConvert.ToString(1));
								//FC:FINAL:DDU #i106 deleted '*' from sql statement
								rsPayments.Execute("DELETE FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(rsBillCheck.Get_Fields_Int32("LienRecordNumber")) + " AND BillCode = 'L'", modExtraModules.strCLDatabase);
								// this will delete any interest amounts
							}
							else
							{
								// the user has payments on this bill
								FCMessageBox.Show("This lien already has payments or adjustments made against it.  Please write off this bill manually for audit purposes.", MsgBoxStyle.Exclamation, "Cannot Overwrite");
								vsAccounts.TextMatrix(lngRow, lngColOverwrite, FCConvert.ToString(0));
								// add a another bill to the file
								lngBillingYearNumber += 1;
							}
						}
					}
					else if (DialogResult == DialogResult.No)
					{
						vsAccounts.TextMatrix(lngRow, lngColOverwrite, FCConvert.ToString(0));
						// add a another bill to the file
						lngBillingYearNumber += 1;
					}
					else if (DialogResult == DialogResult.Cancel)
					{
						vsAccounts.TextMatrix(lngRow, lngColOverwrite, FCConvert.ToString(0));
						vsAccounts.TextMatrix(lngRow, lngColAccount, "");
						vsAccounts.Select(lngRow, lngColAccount);
						return;
					}
				}
				//FC:FINAL:DDU prevent fcmessagebox to show twice
				else// if ((FCMessageBox.Show("There is a bill already created for account " + FCConvert.ToString(lngAcct) + ".  Do you want to continue the load back process for this account and year?", MsgBoxStyle.Question | MsgBoxStyle.YesNoCancel, "Existing Bill") == DialogResult.No) || (FCMessageBox.Show("There is a bill already created for account " + FCConvert.ToString(lngAcct) + ".  Do you want to continue the load back process for this account and year?", MsgBoxStyle.Question | MsgBoxStyle.YesNoCancel, "Existing Bill") == DialogResult.Cancel))
				{
					vsAccounts.TextMatrix(lngRow, lngColOverwrite, FCConvert.ToString(0));
					vsAccounts.TextMatrix(lngRow, lngColAccount, "");
					vsAccounts.Select(lngRow, lngColAccount);
					return;
				}
			}
			if (modStatusPayments.Statics.boolRE)
			{
				rsRE.FindFirstRecord("RSAccount", lngAcct);
			}
			else
			{
				rsRE.FindFirstRecord("Account", lngAcct);
			}
			if (rsRE.NoMatch)
			{
				// no matching account number
				if (modStatusPayments.Statics.boolRE)
				{
					FCMessageBox.Show("There is no Real Estate account with that number.  Please try again.", MsgBoxStyle.Information, "Incorrect Account");
				}
				else
				{
					FCMessageBox.Show("There is no Personal Property account with that number.  Please try again.", MsgBoxStyle.Information, "Incorrect Account");
				}
				vsAccounts.Select(vsAccounts.Row, lngColAccount);
			}
			else
			{
				// matching account number is the current record
				if (modStatusPayments.Statics.boolRE)
				{
					//cCPOwner = modGlobal.Statics.gCPCtlr.GetParty(FCConvert.ToInt32(rsRE.Get_Fields_Int32("OwnerPartyID")));
				}
				else
				{
					cCPOwner = modGlobal.Statics.gCPCtlr.GetParty(FCConvert.ToInt32(rsRE.Get_Fields_Int32("PartyID")));
					// Set cCPAddress = cCPOwner.GetAddress("PP", rsRE.Get_Fields("Account"))
				}
				vsAccounts.TextMatrix(lngRow, lngColAccount, FCConvert.ToString(lngAcct));
				if (modStatusPayments.Statics.boolRE)
				{
					vsAccounts.TextMatrix(lngRow, lngColName, rsRE.Get_Fields_String("DeedName1"));
				}
				else
				{
					vsAccounts.TextMatrix(lngRow, lngColName, cCPOwner.FullNameLastFirst);
				}
				vsAccounts.TextMatrix(lngRow, lngColInterestPTD, txtDefaultDate.Text);
				vsAccounts.TextMatrix(lngRow, lngColIntOwed, "0.00");
				vsAccounts.TextMatrix(lngRow, lngHiddenCol, FCConvert.ToString(lngBillingYearNumber));
				// .TextMatrix(lngRow, lngColOverwrite) = "0"
				vsAccounts.TextMatrix(lngRow, lngColTax1, "0.00");
				vsAccounts.TextMatrix(lngRow, lngColTax2, "0.00");
				vsAccounts.TextMatrix(lngRow, lngColTax3, "0.00");
				vsAccounts.TextMatrix(lngRow, lngColTax4, "0.00");
			}
		}
		// VBto upgrade warning: lngAccountNumber As int	OnWriteFCConvert.ToDouble(
		private void CreateCHGINT_656(double dblIntAmount, bool boolLienCHGINT, int lngBillKey, int lngPaymentYear, DateTime dtIntPTD, int lngAccountNumber)
		{
			CreateCHGINT(ref dblIntAmount, ref boolLienCHGINT, ref lngBillKey, ref lngPaymentYear, ref dtIntPTD, ref lngAccountNumber);
		}

		private void CreateCHGINT_674(double dblIntAmount, bool boolLienCHGINT, int lngBillKey, int lngPaymentYear, DateTime dtIntPTD, int lngAccountNumber)
		{
			CreateCHGINT(ref dblIntAmount, ref boolLienCHGINT, ref lngBillKey, ref lngPaymentYear, ref dtIntPTD, ref lngAccountNumber);
		}

		private void CreateCHGINT(ref double dblIntAmount, ref bool boolLienCHGINT, ref int lngBillKey, ref int lngPaymentYear, ref DateTime dtIntPTD, ref int lngAccountNumber)
		{
			// this will create the CHGINT line in the paymentrec for this account
			clsDRWrapper rsCHGINT = new clsDRWrapper();
			rsCHGINT.OpenRecordset("SELECT * FROM PaymentRec WHERE Billkey = 0");
			rsCHGINT.AddNew();
			rsCHGINT.Set_Fields("Account", lngAccountNumber);
			rsCHGINT.Set_Fields("Year", lngPaymentYear);
			rsCHGINT.Set_Fields("BillKey", lngBillKey);
			rsCHGINT.Set_Fields("ActualSystemDate", DateTime.Now);
			rsCHGINT.Set_Fields("EffectiveInterestDate", dtIntPTD);
			rsCHGINT.Set_Fields("RecordedTransactionDate", dtIntPTD);
			rsCHGINT.Set_Fields("Period", "A");
			rsCHGINT.Set_Fields("Code", "I");
			rsCHGINT.Set_Fields("Reference", "CHGINT");
			rsCHGINT.Set_Fields("CurrentInterest", dblIntAmount);
			// rsCHGINT.Get_Fields("CashDrawer") = False
			// rsCHGINT.Get_Fields("GeneralLedger") = False
			if (modStatusPayments.Statics.boolRE)
			{
				if (boolLienCHGINT)
				{
					rsCHGINT.Set_Fields("BillCode", "L");
				}
				else
				{
					rsCHGINT.Set_Fields("BillCode", "R");
				}
			}
			else
			{
				rsCHGINT.Set_Fields("BillCode", "P");
			}
			rsCHGINT.Set_Fields("DailyCloseOut", true);
			rsCHGINT.Update(true);
		}

		private void mnuFileSearch_Click(object sender, System.EventArgs e)
		{
			// this will show the search frame
			cmdFileSearch.Enabled = false;
			btnProcess.Enabled = false;
			cmdFileNew.Enabled = false;
			//FC:FINAL:RPU:#i254 made also the buttons not visible
			cmdFileSearch.Visible = false;
			cmdFileNew.Visible = false;
			if (modStatusPayments.Statics.boolRE)
			{
				txtSearchMapLot.Visible = true;
				lblSearchMapLot.Visible = true;
			}
			else
			{
				txtSearchMapLot.Visible = false;
				lblSearchMapLot.Visible = false;
			}
			fraAccountSearch.Top = fraAccounts.Top;
			fraAccountSearch.Left = FCConvert.ToInt32((this.Width - fraAccountSearch.Width) / 2.0);
			fraAccounts.Visible = false;
			fraAccountSearch.Visible = true;
			if (txtSearchName.Enabled && txtSearchName.Visible)
			{
				txtSearchName.Focus();
			}
			txtSearchMapLot.Text = "";
			txtSearchName.Text = "";
			vsAccountSearch.Visible = false;
			vsAccountSearch.Rows = 1;
		}

		private void txtSearchMapLot_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Return)
			{
				KeyCode = 0;
				SearchForAccounts();
			}
		}

		private void txtSearchName_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Return)
			{
				KeyCode = 0;
				SearchForAccounts();
			}
		}

		private void vsAccounts_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (vsAccounts.Col == lngColInterestPTD)
			{
				vsAccounts.EditMask = "##/##/####";
			}
			else
			{
				vsAccounts.EditMask = "";
			}
		}

		private void vsAccounts_GotFocus(object sender, EventArgs e)
		{
			vsAccounts.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
		}

		private void vsAccounts_KeyDown(object sender, KeyEventArgs e)
		{
			int KeyCode = e.KeyValue;
			if (e.KeyCode == Keys.Delete)
			{
				KeyCode = 0;
				DialogResult messageBoxResult = FCMessageBox.Show("Are you sure that you would like to delete this row?", MsgBoxStyle.Question | MsgBoxStyle.YesNoCancel, "Delete Row");
				if (messageBoxResult == DialogResult.Yes)
				{
					vsAccounts.RemoveItem(vsAccounts.Row);
				}
			}
		}

        
        private void VsAccounts_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            int lngMR;
            int lngMC;
            lngMR = vsAccounts.GetFlexRowIndex(e.RowIndex);
            lngMC = vsAccounts.GetFlexColIndex(e.ColumnIndex);
            if (vsAccounts.IsValidCell(e.ColumnIndex,e.RowIndex))
            {
                DataGridViewCell cell = vsAccounts[e.ColumnIndex, e.RowIndex];
                if (lngMC == lngColName || lngMC == lngColInterestPTD)
                {
                    cell.ToolTipText = vsAccounts.TextMatrix(lngMR, lngMC);
                }
                else
                {
                   cell.ToolTipText = "";
                }
            }
        }


        private void vsAccounts_RowColChange(object sender, EventArgs e)
		{
			if (vsAccounts.Col == lngColAccount)
			{
				vsAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else if (vsAccounts.Col == lngColInterestPTD)
			{
				vsAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else if (vsAccounts.Col == lngColIntOwed)
			{
				vsAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else if (vsAccounts.Col == lngColName)
			{
				// allow the name to be edited
				vsAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else if (vsAccounts.Col == lngColTax1)
			{
				vsAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else if (vsAccounts.Col == lngColTax2)
			{
				if (boolPer2)
				{
					vsAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				}
				else
				{
					vsAccounts.Editable = FCGrid.EditableSettings.flexEDNone;
				}
			}
			else if (vsAccounts.Col == lngColTax3)
			{
				if (boolPer3)
				{
					vsAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				}
				else
				{
					vsAccounts.Editable = FCGrid.EditableSettings.flexEDNone;
				}
			}
			else if (vsAccounts.Col == lngColTax4)
			{
				if (boolPer4)
				{
					vsAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				}
				else
				{
					vsAccounts.Editable = FCGrid.EditableSettings.flexEDNone;
				}
			}
			else if (vsAccounts.Col == lngHiddenCol || vsAccounts.Col == lngColOverwrite)
			{
				vsAccounts.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			if (vsAccounts.Row == vsAccounts.Rows - 1 && vsAccounts.Col == vsAccounts.Cols - 1)
			{
				vsAccounts.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
		}

		private void vsAccounts_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (vsAccounts.CurrentCell.IsInEditMode)
			{
				//FC:FINAL:MSH - save and use correct indexes of the cell
				int row = vsAccounts.GetFlexRowIndex(e.RowIndex);
				int col = vsAccounts.GetFlexColIndex(e.ColumnIndex);
				if (col == lngColAccount)
				{
					// if this is validating an account
					LoadAccountInformation_6(FCConvert.ToInt32(Conversion.Val(vsAccounts.EditText)), row);
				}
				else if (col == lngColInterestPTD)
				{
					// validate the paid through date
					if (Information.IsDate(vsAccounts.EditText))
					{
						vsAccounts.TextMatrix(row, col, Strings.Format(vsAccounts.EditText, "MM/dd/yyyy"));
					}
					else
					{
						vsAccounts.TextMatrix(row, col, "");
						e.Cancel = true;
					}
				}
			}
		}

		private void SearchForAccounts()
		{
			clsDRWrapper rsSearch = new clsDRWrapper();
			string strSQL = "";
            string strWhere = "";
			FormatSearchGrid();
			if (modStatusPayments.Statics.boolRE)
			{
				strSQL = "SELECT DeedName1 as Own1FullName,RSAccount AS Acct, RSMapLot AS MapLot FROM Master";
				if (Strings.Trim(txtSearchMapLot.Text) != "")
				{
					strSQL += " WHERE RSMapLot > '" + Strings.Trim(txtSearchMapLot.Text) + "     ' AND RSMapLot < '" + Strings.Trim(txtSearchMapLot.Text) + "ZZZZZ'";
                    strWhere = " and ";
                }				
				if (Strings.Trim(txtSearchName.Text) != "")
                {
                    strSQL +=  strWhere + " DeedName1 > '" + txtSearchName.Text.Trim() + "     ' and DeedName1 <'" +
                              txtSearchName.Text.Trim() + "ZZZZZ'";
                }

                strSQL += " order by DeedName1";
                rsSearch.OpenRecordset(strSQL, "RealEstate");
            }
			else
			{
				strSQL = "SELECT PartyID, Account AS Acct, Street as MapLot FROM PPMaster";
				rsSearch.OpenRecordset(strSQL, modExtraModules.strPPDatabase);
				rsSearch.InsertName("PartyID", "Own1", false, true, false, "", false, "", true, "");
				if (Strings.Trim(txtSearchName.Text) != "")
				{
					rsSearch.Filter("Own1FullName > '" + Strings.Trim(txtSearchName.Text) + "     ' AND Own1FullName < '" + Strings.Trim(txtSearchName.Text) + "ZZZZZ'");
				}
			}
			if (rsSearch.EndOfFile())
			{
				// no accounts
				FCMessageBox.Show("No accounts match the search criteria.", MsgBoxStyle.Exclamation, "No Matches");
				return;
			}
			else
			{
				rsSearch.MoveFirst();
				vsAccountSearch.Rows = 1;
				while (!rsSearch.EndOfFile())
				{
					// TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
					vsAccountSearch.AddItem("\t" + FCConvert.ToString(rsSearch.Get_Fields_String("Acct")) + "\t" + FCConvert.ToString(rsSearch.Get_Fields("Own1FullName")) + "\t" + FCConvert.ToString(rsSearch.Get_Fields_String("MapLot")));
					rsSearch.MoveNext();
				}
			}
			vsAccountSearch.Visible = true;
		}

		private void vsAccountSearch_DblClick(object sender, EventArgs e)
		{
			// selecting an account to use
			int lngRW;
			int lngAccount;
			lngRW = vsAccountSearch.MouseRow;
			lngAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(vsAccountSearch.TextMatrix(lngRW, 1))));
			ReturnFromSearch(ref lngAccount);
		}

		private void ReturnFromSearch_2(int lngAcct)
		{
			ReturnFromSearch(ref lngAcct);
		}

		private void ReturnFromSearch(ref int lngAcct)
		{
			fraAccountSearch.Visible = false;
			fraAccounts.Visible = true;
			btnProcess.Enabled = true;
			cmdFileSearch.Enabled = true;
			cmdFileNew.Enabled = true;
			//FC:FINAL:RPU:#i254 made also the buttons visible
			cmdFileSearch.Visible = true;
			cmdFileNew.Visible = true;
			if (lngAcct == 0)
			{
				if (vsAccounts.Visible && vsAccounts.Enabled)
				{
					vsAccounts.Focus();
				}
			}
			else
			{
				LoadAccountInformation_6(lngAcct, 0);
			}
		}

		private void cmdFileNew_Click(object sender, EventArgs e)
		{
			this.mnuFileNew_Click(sender, e);
		}

		private void cmdFileSearch_Click(object sender, EventArgs e)
		{
			this.mnuFileSearch_Click(sender, e);
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFileSave_Click(sender, e);
		}
	}
}
