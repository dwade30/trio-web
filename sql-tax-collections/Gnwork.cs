﻿using fecherFoundation;

namespace TWCL0000
{
	public class modGNWork
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :                                       *
		// Date           :                                       *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// Last Updated   :               12/16/2002              *
		// ********************************************************
		public struct SECRECORD
		{
			public FCFixedString VERSION;
			public FCFixedString MUNI;
			public short rsYear;
			// 4
			public short RSMONTH;
			// 2
			public FCFixedString PASSWORD;
			public FCFixedString OVERRIDE;
			public short CTLD;
			// 4
			public short USECOUNT;
			// 6
			public short CTLM;
			// 4
			public int MAXACCT;
			// 6
			public FCFixedString SDRIVE;
			public short SCOLOR;
			public FCFixedString SPRODSUM;
			public FCFixedString SRYEAR;
			public FCFixedString SRMONTH;
			public FCFixedString CMYEAR;
			public FCFixedString CMMONTH;
			public FCFixedString PPYEAR;
			public FCFixedString PPMONTH;
			public FCFixedString BLYEAR;
			public FCFixedString BLMONTH;
			public FCFixedString CLYEAR;
			public FCFixedString CLMONTH;
			public FCFixedString CEYEAR;
			public FCFixedString CEMONTH;
			public FCFixedString BDYEAR;
			public FCFixedString BDMONTH;
			public FCFixedString CRYEAR;
			public FCFixedString CRMONTH;
			public FCFixedString PYYEAR;
			public FCFixedString PYMONTH;
			public FCFixedString UTYEAR;
			public FCFixedString UTMONTH;
			public FCFixedString MVYEAR;
			public FCFixedString MVMONTH;
			public FCFixedString TCYEAR;
			public FCFixedString TCMONTH;
			public FCFixedString E9YEAR;
			public FCFixedString E9MONTH;
			public FCFixedString CKYEAR;
			public FCFixedString CKMONTH;
			public FCFixedString NETWORKFLAG;
			public FCFixedString NETWORKFLAG2;
			public FCFixedString VALDATE;
			public short VALCOUNT;
			// 2
			public FCFixedString MAXPP;
			public FCFixedString VRYEAR;
			public FCFixedString VRMONTH;
			public FCFixedString VRMAX;
			public FCFixedString CEMAX;
			public FCFixedString PYMAX;
			public FCFixedString UTMAX;
			public FCFixedString MVMAX;
			public FCFixedString RBMAX;
			public FCFixedString EXPANSION;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public SECRECORD(int unusedParam)
			{
				this.VERSION = new FCFixedString(5);
				this.MUNI = new FCFixedString(20);
				this.rsYear = 0;
				this.RSMONTH = 0;
				this.PASSWORD = new FCFixedString(4);
				this.OVERRIDE = new FCFixedString(1);
				this.CTLD = 0;
				this.USECOUNT = 0;
				this.CTLM = 0;
				this.MAXACCT = 0;
				this.SDRIVE = new FCFixedString(1);
				this.SCOLOR = 0;
				this.SPRODSUM = new FCFixedString(6);
				this.SRYEAR = new FCFixedString(4);
				this.SRMONTH = new FCFixedString(2);
				this.CMYEAR = new FCFixedString(4);
				this.CMMONTH = new FCFixedString(2);
				this.PPYEAR = new FCFixedString(4);
				this.PPMONTH = new FCFixedString(2);
				this.BLYEAR = new FCFixedString(4);
				this.BLMONTH = new FCFixedString(2);
				this.CLYEAR = new FCFixedString(4);
				this.CLMONTH = new FCFixedString(2);
				this.CEYEAR = new FCFixedString(4);
				this.CEMONTH = new FCFixedString(2);
				this.BDYEAR = new FCFixedString(4);
				this.BDMONTH = new FCFixedString(2);
				this.CRYEAR = new FCFixedString(4);
				this.CRMONTH = new FCFixedString(2);
				this.PYYEAR = new FCFixedString(4);
				this.PYMONTH = new FCFixedString(2);
				this.UTYEAR = new FCFixedString(4);
				this.UTMONTH = new FCFixedString(2);
				this.MVYEAR = new FCFixedString(4);
				this.MVMONTH = new FCFixedString(2);
				this.TCYEAR = new FCFixedString(4);
				this.TCMONTH = new FCFixedString(2);
				this.E9YEAR = new FCFixedString(4);
				this.E9MONTH = new FCFixedString(2);
				this.CKYEAR = new FCFixedString(4);
				this.CKMONTH = new FCFixedString(2);
				this.NETWORKFLAG = new FCFixedString(1);
				this.NETWORKFLAG2 = new FCFixedString(1);
				this.VALDATE = new FCFixedString(10);
				this.VALCOUNT = 0;
				this.MAXPP = new FCFixedString(1);
				this.VRYEAR = new FCFixedString(4);
				this.VRMONTH = new FCFixedString(2);
				this.VRMAX = new FCFixedString(1);
				this.CEMAX = new FCFixedString(1);
				this.PYMAX = new FCFixedString(1);
				this.UTMAX = new FCFixedString(1);
				this.MVMAX = new FCFixedString(2);
				this.RBMAX = new FCFixedString(2);
				this.EXPANSION = new FCFixedString(87);
			}
		};

		public class StaticVariables
		{
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public SECRECORD SEC = new SECRECORD(0);
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
