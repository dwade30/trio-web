﻿using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using System;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using SharedApplication.Extensions;
using Wisej.Web;

namespace TWCL0000
{
    public class modTaxService
    {
        private static string _elevenZeros;

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct TaxServiceRecord
        {
            // 422 characters long
            // VBto upgrade warning: Year As FixedString	OnWrite(string)	OnRead(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
            public char[] YearCharArray;
            public string Year
            {
                get
                {
                    return FCUtils.FixedStringFromArray(YearCharArray);
                }
                set
                {
                    YearCharArray = FCUtils.FixedStringToArray(value, 4);
                }
            }
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] PeriodCharArray;
            public string Period
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PeriodCharArray);
                }
                set
                {
                    PeriodCharArray = FCUtils.FixedStringToArray(value, 1);
                }
            }
            // number of the period
            // VBto upgrade warning: MapLot As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
            public char[] MapLotCharArray;
            public string MapLot
            {
                get
                {
                    return FCUtils.FixedStringFromArray(MapLotCharArray);
                }
                set
                {
                    MapLotCharArray = FCUtils.FixedStringToArray(value, 20);
                }
            }
            // VBto upgrade warning: Name As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 40)]
            public char[] NameCharArray;
            public string Name
            {
                get
                {
                    return FCUtils.FixedStringFromArray(NameCharArray);
                }
                set
                {
                    NameCharArray = FCUtils.FixedStringToArray(value, 40);
                }
            }
            // VBto upgrade warning: LocationNumber As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
            public char[] LocationNumberCharArray;
            public string LocationNumber
            {
                get
                {
                    return FCUtils.FixedStringFromArray(LocationNumberCharArray);
                }
                set
                {
                    LocationNumberCharArray = FCUtils.FixedStringToArray(value, 5);
                }
            }
            // VBto upgrade warning: LocationStreet As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 30)]
            public char[] LocationStreetCharArray;
            public string LocationStreet
            {
                get
                {
                    return FCUtils.FixedStringFromArray(LocationStreetCharArray);
                }
                set
                {
                    LocationStreetCharArray = FCUtils.FixedStringToArray(value, 30);
                }
            }
            // VBto upgrade warning: Acres As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] AcresCharArray;
            public string Acres
            {
                get
                {
                    return FCUtils.FixedStringFromArray(AcresCharArray);
                }
                set
                {
                    AcresCharArray = FCUtils.FixedStringToArray(value, 8);
                }
            }
            // VBto upgrade warning: SQFootage As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] SQFootageCharArray;
            public string SQFootage
            {
                get
                {
                    return FCUtils.FixedStringFromArray(SQFootageCharArray);
                }
                set
                {
                    SQFootageCharArray = FCUtils.FixedStringToArray(value, 8);
                }
            }
            // VBto upgrade warning: BuildingVal As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 9)]
            public char[] BuildingValCharArray;
            public string BuildingVal
            {
                get
                {
                    return FCUtils.FixedStringFromArray(BuildingValCharArray);
                }
                set
                {
                    BuildingValCharArray = FCUtils.FixedStringToArray(value, 9);
                }
            }
            // VBto upgrade warning: Book As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
            public char[] BookCharArray;
            public string Book
            {
                get
                {
                    return FCUtils.FixedStringFromArray(BookCharArray);
                }
                set
                {
                    BookCharArray = FCUtils.FixedStringToArray(value, 5);
                }
            }
            // VBto upgrade warning: Page As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
            public char[] PageCharArray;
            public string Page
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PageCharArray);
                }
                set
                {
                    PageCharArray = FCUtils.FixedStringToArray(value, 5);
                }
            }
            // VBto upgrade warning: Account As FixedString	OnWrite(string)	OnRead(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public char[] AccountCharArray;
            public string Account
            {
                get
                {
                    return FCUtils.FixedStringFromArray(AccountCharArray);
                }
                set
                {
                    AccountCharArray = FCUtils.FixedStringToArray(value, 10);
                }
            }
            // record number
            // VBto upgrade warning: Abatement As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 11)]
            public char[] AbatementCharArray;
            public string Abatement
            {
                get
                {
                    return FCUtils.FixedStringFromArray(AbatementCharArray);
                }
                set
                {
                    AbatementCharArray = FCUtils.FixedStringToArray(value, 11);
                }
            }
            // VBto upgrade warning: Exemption As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 11)]
            public char[] ExemptionCharArray;
            public string Exemption
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ExemptionCharArray);
                }
                set
                {
                    ExemptionCharArray = FCUtils.FixedStringToArray(value, 11);
                }
            }
            // VBto upgrade warning: LienAmount As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 11)]
            public char[] LienAmountCharArray;
            public string LienAmount
            {
                get
                {
                    return FCUtils.FixedStringFromArray(LienAmountCharArray);
                }
                set
                {
                    LienAmountCharArray = FCUtils.FixedStringToArray(value, 11);
                }
            }
            // VBto upgrade warning: LienBalance As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 11)]
            public char[] LienBalanceCharArray;
            public string LienBalance
            {
                get
                {
                    return FCUtils.FixedStringFromArray(LienBalanceCharArray);
                }
                set
                {
                    LienBalanceCharArray = FCUtils.FixedStringToArray(value, 11);
                }
            }
            // VBto upgrade warning: BettermentAmount As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 11)]
            public char[] BettermentAmountCharArray;
            public string BettermentAmount
            {
                get
                {
                    return FCUtils.FixedStringFromArray(BettermentAmountCharArray);
                }
                set
                {
                    BettermentAmountCharArray = FCUtils.FixedStringToArray(value, 11);
                }
            }
            // VBto upgrade warning: BettermentBalance As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 11)]
            public char[] BettermentBalanceCharArray;
            public string BettermentBalance
            {
                get
                {
                    return FCUtils.FixedStringFromArray(BettermentBalanceCharArray);
                }
                set
                {
                    BettermentBalanceCharArray = FCUtils.FixedStringToArray(value, 11);
                }
            }
            // VBto upgrade warning: Tax1 As FixedString	OnWrite(string, FixedString)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 11)]
            public char[] Tax1CharArray;
            public string Tax1
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Tax1CharArray);
                }
                set
                {
                    Tax1CharArray = FCUtils.FixedStringToArray(value, 11);
                }
            }
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 11)]
            public char[] Tax1BalanceCharArray;
            public string Tax1Balance
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Tax1BalanceCharArray);
                }
                set
                {
                    Tax1BalanceCharArray = FCUtils.FixedStringToArray(value, 11);
                }
            }
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 11)]
            public char[] Tax1InterestDueCharArray;
            public string Tax1InterestDue
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Tax1InterestDueCharArray);
                }
                set
                {
                    Tax1InterestDueCharArray = FCUtils.FixedStringToArray(value, 11);
                }
            }
            // VBto upgrade warning: Tax2 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 11)]
            public char[] Tax2CharArray;
            public string Tax2
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Tax2CharArray);
                }
                set
                {
                    Tax2CharArray = FCUtils.FixedStringToArray(value, 11);
                }
            }
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 11)]
            public char[] Tax2BalanceCharArray;
            public string Tax2Balance
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Tax2BalanceCharArray);
                }
                set
                {
                    Tax2BalanceCharArray = FCUtils.FixedStringToArray(value, 11);
                }
            }
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 11)]
            public char[] Tax2InterestDueCharArray;
            public string Tax2InterestDue
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Tax2InterestDueCharArray);
                }
                set
                {
                    Tax2InterestDueCharArray = FCUtils.FixedStringToArray(value, 11);
                }
            }
            // VBto upgrade warning: Tax3 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 11)]
            public char[] Tax3CharArray;
            public string Tax3
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Tax3CharArray);
                }
                set
                {
                    Tax3CharArray = FCUtils.FixedStringToArray(value, 11);
                }
            }
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 11)]
            public char[] Tax3BalanceCharArray;
            public string Tax3Balance
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Tax3BalanceCharArray);
                }
                set
                {
                    Tax3BalanceCharArray = FCUtils.FixedStringToArray(value, 11);
                }
            }
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 11)]
            public char[] Tax3InterestDueCharArray;
            public string Tax3InterestDue
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Tax3InterestDueCharArray);
                }
                set
                {
                    Tax3InterestDueCharArray = FCUtils.FixedStringToArray(value, 11);
                }
            }
            // VBto upgrade warning: Tax4 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 11)]
            public char[] Tax4CharArray;
            public string Tax4
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Tax4CharArray);
                }
                set
                {
                    Tax4CharArray = FCUtils.FixedStringToArray(value, 11);
                }
            }
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 11)]
            public char[] Tax4BalanceCharArray;
            public string Tax4Balance
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Tax4BalanceCharArray);
                }
                set
                {
                    Tax4BalanceCharArray = FCUtils.FixedStringToArray(value, 11);
                }
            }
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 11)]
            public char[] Tax4InterestDueCharArray;
            public string Tax4InterestDue
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Tax4InterestDueCharArray);
                }
                set
                {
                    Tax4InterestDueCharArray = FCUtils.FixedStringToArray(value, 11);
                }
            }
            // VBto upgrade warning: TotalInterestDue As FixedString	OnWrite(string, FixedString)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 11)]
            public char[] TotalInterestDueCharArray;
            public string TotalInterestDue
            {
                get
                {
                    return FCUtils.FixedStringFromArray(TotalInterestDueCharArray);
                }
                set
                {
                    TotalInterestDueCharArray = FCUtils.FixedStringToArray(value, 11);
                }
            }
            // VBto upgrade warning: TotalDemandFeesDue As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 11)]
            public char[] TotalDemandFeesDueCharArray;
            public string TotalDemandFeesDue
            {
                get
                {
                    return FCUtils.FixedStringFromArray(TotalDemandFeesDueCharArray);
                }
                set
                {
                    TotalDemandFeesDueCharArray = FCUtils.FixedStringToArray(value, 11);
                }
            }
            // VBto upgrade warning: TotalAmountDue As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 11)]
            public char[] TotalAmountDueCharArray;
            public string TotalAmountDue
            {
                get
                {
                    return FCUtils.FixedStringFromArray(TotalAmountDueCharArray);
                }
                set
                {
                    TotalAmountDueCharArray = FCUtils.FixedStringToArray(value, 11);
                }
            }
            // VBto upgrade warning: PaidFlag As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] PaidFlagCharArray;
            public string PaidFlag
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PaidFlagCharArray);
                }
                set
                {
                    PaidFlagCharArray = FCUtils.FixedStringToArray(value, 2);
                }
            }
            // VBto upgrade warning: ExemptFlag As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] ExemptFlagCharArray;
            public string ExemptFlag
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ExemptFlagCharArray);
                }
                set
                {
                    ExemptFlagCharArray = FCUtils.FixedStringToArray(value, 2);
                }
            }
            // VBto upgrade warning: PriorYearDelinquent As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] PriorYearDelinquentCharArray;
            public string PriorYearDelinquent
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PriorYearDelinquentCharArray);
                }
                set
                {
                    PriorYearDelinquentCharArray = FCUtils.FixedStringToArray(value, 1);
                }
            }
            // VBto upgrade warning: LienIndicator As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] LienIndicatorCharArray;
            public string LienIndicator
            {
                get
                {
                    return FCUtils.FixedStringFromArray(LienIndicatorCharArray);
                }
                set
                {
                    LienIndicatorCharArray = FCUtils.FixedStringToArray(value, 8);
                }
            }
            // VBto upgrade warning: TotalDiscount As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 11)]
            public char[] TotalDiscountCharArray;
            public string TotalDiscount
            {
                get
                {
                    return FCUtils.FixedStringFromArray(TotalDiscountCharArray);
                }
                set
                {
                    TotalDiscountCharArray = FCUtils.FixedStringToArray(value, 11);
                }
            }
            // VBto upgrade warning: NetAmountDue As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 11)]
            public char[] NetAmountDueCharArray;
            public string NetAmountDue
            {
                get
                {
                    return FCUtils.FixedStringFromArray(NetAmountDueCharArray);
                }
                set
                {
                    NetAmountDueCharArray = FCUtils.FixedStringToArray(value, 11);
                }
            }
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 11)]
            public char[] ZZFillerCharArray;
            public string ZZFiller
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ZZFillerCharArray);
                }
                set
                {
                    ZZFillerCharArray = FCUtils.FixedStringToArray(value, 11);
                }
            }
            //FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
            public TaxServiceRecord(int unusedParam)
            {
                var fiveSpaces = new string(' ', 5).ToArray();
                var elevenSpaces = new string(' ', 11).ToArray();
                var elevenZeros = new string('0', 11).ToArray();

                this.YearCharArray = new string(' ', 4).ToArray();
                this.PeriodCharArray = new string(' ', 1).ToArray();
                this.MapLotCharArray = new string(' ', 20).ToArray();
                this.NameCharArray = new string(' ', 40).ToArray();
                this.LocationNumberCharArray = fiveSpaces;
                this.LocationStreetCharArray = new string(' ', 30).ToArray();
                this.AcresCharArray = new string(' ', 8).ToArray();
                this.SQFootageCharArray = new string(' ', 8).ToArray();
                this.BuildingValCharArray = new string(' ', 9).ToArray();
                this.BookCharArray = fiveSpaces;
                this.PageCharArray = fiveSpaces;
                this.AccountCharArray = new string(' ', 10).ToArray();
                this.AbatementCharArray = elevenSpaces;
                this.ExemptionCharArray = elevenSpaces;
                this.LienAmountCharArray = elevenSpaces;
                this.LienBalanceCharArray = elevenSpaces;
                this.BettermentAmountCharArray = elevenSpaces;
                this.BettermentBalanceCharArray = elevenSpaces;
                this.Tax1CharArray = elevenSpaces;
                this.Tax1BalanceCharArray = elevenSpaces;
                this.Tax1InterestDueCharArray = elevenSpaces;
                this.Tax2CharArray = elevenSpaces;
                this.Tax2BalanceCharArray = elevenSpaces;
                this.Tax2InterestDueCharArray = elevenSpaces;
                this.Tax3CharArray = elevenSpaces;
                this.Tax3BalanceCharArray = elevenSpaces;
                this.Tax3InterestDueCharArray = elevenSpaces;
                this.Tax4CharArray = elevenSpaces;
                this.Tax4BalanceCharArray = elevenSpaces;
                this.Tax4InterestDueCharArray = elevenSpaces;
                this.TotalInterestDueCharArray = elevenSpaces;
                this.TotalDemandFeesDueCharArray = elevenSpaces;
                this.TotalAmountDueCharArray = elevenSpaces;
                this.PaidFlagCharArray = new string(' ', 2).ToArray();
                this.ExemptFlagCharArray = new string(' ', 2).ToArray();
                this.PriorYearDelinquentCharArray = new string(' ', 1).ToArray();
                this.LienIndicatorCharArray = new string(' ', 8).ToArray();
                this.TotalDiscountCharArray = elevenSpaces;
                this.NetAmountDueCharArray = elevenSpaces;
                this.ZZFillerCharArray = elevenZeros;
            }
        };

        public static void CreateExtractFile(clsDRWrapper rsAccounts, ref string strPath, int lngLastYearBilled, short intPassPeriod, DateTime dtPassDate, bool boolTaxService/*= true*/, ref int lngLastRowUsed, ref int lngExtractYear, bool boolZip, string strNewFileName, string strTownEmail = "", int lngTownCode = 0)
        {
            clsDRWrapper rsCL = new clsDRWrapper();
            clsDRWrapper rsSetting = new clsDRWrapper();
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this routine will create an extract file for First American Tax Service
                // rsAccounts is a recordset with the list of accounts in order
                // strPath is the path where the file will be created
                // lngLastyearBilled is the year that will have a record for every account even if they are paid
                // intPeriod will be the period that the tax will be calculated to (1-4)
                //FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
                TaxServiceRecord trBill = new TaxServiceRecord(0);

                bool boolEmpty = false;
                string strFileNamed = "";
                string strZipFile = "";
                string strType = "";
                string strBaseFileName = "";

                strType = modStatusPayments.Statics.boolRE ? "'RE'" : "'PP'";

                // open the output file
                if (strPath == "")
                {
                    strPath = FCFileSystem.Statics.UserDataFolder;
                }

                if (Strings.Right(strPath, 1) == "\\")
                {
                    // kk01272017 trocls-89  Prevent "\\" in path
                    strPath = Strings.Left(strPath, strPath.Length - 1);
                }

                try
                {
                    if (boolTaxService)
                    {
                        FCFileSystem.FileOpen(38, strPath + "\\TSCoreLogic.ASC", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
                        strFileNamed = strPath + "\\TSCoreLogic.ASC";
                        strZipFile = strPath + "\\TSCoreLogic.ZIP";
                        strBaseFileName = "TSCoreLogic.zip";
                        FCFileSystem.FileOpen(39, strPath + "\\TTCoreLogic.CSV", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);

                        // strFileNamed = strPath & "\TT1STAM.CSV"
                    }
                    else
                    {
                        FCFileSystem.FileOpen(38, strPath + "\\" + strNewFileName, OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
                        strFileNamed = strPath + "\\" + strNewFileName;
                        strBaseFileName = Strings.Left(strNewFileName, strNewFileName.Length - 4) + ".zip";

                        strZipFile = strPath + "\\" + Strings.Left(strNewFileName, strNewFileName.Length - 4) + ".ZIP";
                    }

                    Statics.boolErrorPrintingToFile = false;
                    Statics.intPeriod = intPassPeriod;
                    Statics.dtTSDate = dtPassDate;
                    rsAccounts.MoveFirst();

                    // reset the account list
                    frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Processing Accounts", true, 100, false);

                    var strExtractYear = FCConvert.ToString(lngExtractYear);

                    const string strFields_RE = "BillingMaster.ID,Account,Name1,BillingType,BillingYear,RateKey,MapLot,BookPage,LienRecordNumber,TaxDue1,TaxDue2,TaxDue3,TaxDue4,PrincipalPaid,DemandFees,DemandFeesPaid,InterestCharged,InterestPaid,InterestAppliedThroughDate,ExemptValue,RSMapLot,RSLocNumAlph,RSLocStreet,PIACRES,LandValue,BuildingValue,LASTBLDGVAL,LASTLANDVAL,RSRef1";
                    var sqlBase_RE = $"SELECT {strFields_RE} FROM BillingMaster INNER JOIN {modGlobal.Statics.strDbRE}Master ON BillingMaster.Account = Master.RSAccount WHERE BillingType = 'RE'  AND RSCard = 1 ";
                    //
                    const string strFields_PP = "BillingMaster.ID,BillingMaster.Account,Name1,BillingType,BillingYear,RateKey,MapLot,BookPage,LienRecordNumber,TaxDue1,TaxDue2,TaxDue3,TaxDue4,PrincipalPaid,DemandFees,DemandFeesPaid,InterestCharged,InterestPaid,InterestAppliedThroughDate,ExemptValue,Street,PPMaster.StreetNumber";
                    var sqlBase_PP = $"SELECT {strFields_PP} FROM BillingMaster INNER JOIN {modGlobal.Statics.strDbPP}PPMaster ON BillingMaster.Account = PPMaster.Account WHERE BillingType = 'PP' ";
                    //
                    var sqlBase = modStatusPayments.Statics.boolRE ? sqlBase_RE : sqlBase_PP;
                    var sqlNonTaxService = $"AND BillingYear / 10 = {strExtractYear}";

                    var sb = new StringBuilder();

                    while (!rsAccounts.EndOfFile())
                    {
                        sb.Append($"{rsAccounts.Get_Fields("Account")},");
                        rsAccounts.MoveNext();
                    }

                    var currAcct = string.Empty;


                    var finalSQL = $"{sqlBase} AND BillingMaster.Account in ({sb.ToString().TrimEnd(',')})";

                    rsCL.OpenRecordset(boolTaxService ? finalSQL + " order by  BillingMaster.Account" : $"{finalSQL} {sqlNonTaxService}" + " order by BillingMaster.Account");
                    var numRecords = rsAccounts.RecordCount();
                  //  var recordNumber = 0;
                    decimal percentCompleted = 0;
                    frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Processing Accounts", true, numRecords, true);
                    while (!rsCL.EndOfFile())
                    {
                       // recordNumber++;

                        if (currAcct.ToIntegerValue() != rsCL.Get_Fields_Int32("Account"))
                        {
                            if (currAcct != string.Empty)
                            {
                                //percentCompleted = recordNumber / numRecords;
                                //var intCompleted = Convert.ToInt32(percentCompleted);
                                //if (intCompleted > 100)
                                //{
                                //    intCompleted = 100;
                                //}

                                //frmWait.InstancePtr.prgProgress.Value = intCompleted;
                                frmWait.InstancePtr.IncrementProgress();
                                App.DoEvents();
                            }

                            currAcct = rsCL.Get_Fields_Int32("Account").ToString();
                        }

                        if (Statics.boolErrorPrintingToFile)
                        {
                            break;
                        }

                        boolEmpty = false;

                        var billingYear = FCUtils.iDiv(rsCL.Get_Fields_Int32("BillingYear"), 10);

                        if (billingYear <= lngLastYearBilled)
                        {
                            trBill = FillTaxServiceRecord(rsCL, ref boolEmpty, FCConvert.CBool(billingYear < lngLastYearBilled));

                            if (Conversion.Val(trBill.Account) != 0)
                            {
                                // output the bill record information
                                if (boolTaxService)
                                {
                                    if ((trBill.PaidFlag != "PD" || Conversion.Val(trBill.Year) == lngLastYearBilled) && !boolEmpty)
                                    {
                                        if (FCConvert.ToInt32(trBill.LienAmount) > 0)
                                        {
                                            OutputExtractRecord(trBill, 1);
                                        }
                                        else
                                        {
                                            OutputExtractRecord(trBill, Statics.intPeriod);
                                        }
                                    }
                                }
                                else
                                {
                                    if (Conversion.Val(trBill.LienAmount) > 0)
                                    {
                                        OutputDatabaseExtractRecord_6(trBill, 1, lngLastRowUsed);
                                    }
                                    else
                                    {
                                        OutputDatabaseExtractRecord(ref trBill, ref Statics.intPeriod, ref lngLastRowUsed);
                                    }
                                }
                            }
                        }
                        else
                        {
                            boolEmpty = boolEmpty;
                        }

                        rsCL.MoveNext();

                        if (Statics.boolErrorPrintingToFile)
                        {
                            goto FINISH;
                        }
                    }

                    frmWait.InstancePtr.Unload();
                FINISH:;
                    FCFileSystem.FileClose(38);

                    // close the output file
                    FCFileSystem.FileClose(39);

                    if (boolTaxService)
                    {
                        if (File.Exists(strZipFile))
                        {
                            File.Delete(strZipFile);
                        }

                        var zip = fecherFoundation.ZipFile.Open(strZipFile, System.IO.Compression.ZipArchiveMode.Create);
                        zip.CreateEntryFromFile(System.IO.Path.Combine(strPath, "TSCoreLogic.ASC"), "TSCoreLogic.ASC", System.IO.Compression.CompressionLevel.Optimal);
                        zip.Dispose();
                        //FCUtils.DownloadAndOpen(strZipFile, strBaseFileName);
                        FCUtils.Download(strZipFile, strBaseFileName);
                    }

                    rsSetting.OpenRecordset("SELECT * FROM Collections", modExtraModules.strCLDatabase);

                    if (rsSetting.RecordCount() > 0)
                    {
                        if (String.Compare(FCConvert.ToString(rsSetting.Get_Fields_Int32("TSLastRunYear")), (FCConvert.ToString(lngLastYearBilled) + FCConvert.ToString(Statics.intPeriod))) > 0)
                        {
                            // Don't Change It - Later Year Already Run
                        }
                        else
                        {
                            rsSetting.Edit();
                            rsSetting.Set_Fields("TSLastRunYear", FCConvert.ToString(lngLastYearBilled) + FCConvert.ToString(Statics.intPeriod));
                            rsSetting.Set_Fields("TSRemindNextDate", null);
                            rsSetting.Set_Fields("TSRemindLastDate", DateTime.Today);
                            rsSetting.Update();
                        }
                    }

                    frmWait.InstancePtr.Unload();

                    if (boolZip)
                    { }
                    else
                    {
                        //FC:FINAL:DSE:#1840 Download the file
                        FCMessageBox.Show("Extract completed successfully.", MsgBoxStyle.Information, "Success");

                    }

                    if (boolTaxService)
                    {
                        // And Trim(strTownEmail) <> ""
                        if (FCMessageBox.Show("Would you like to E-mail this file to CoreLogic?", MsgBoxStyle.Question | MsgBoxStyle.YesNo, "Email") == DialogResult.Yes)
                        {
                            // MAL@20081203: Add Town Code value for E-Mail Subject
                            // Tracker Reference: 15673
                            EmailFirstAmerican(strZipFile, strTownEmail, false, lngTownCode);
                        }
                    }

                    return;
                }
                catch (Exception ex)
                {
                    // ERROR_HANDLER:
                    
                    FCMessageBox.Show("Error #"
                                      + FCConvert.ToString(Information.Err(ex)
                                                                      .Number)
                                      + " - "
                                      + ex.GetBaseException()
                                          .Message
                                      + ".", MsgBoxStyle.Critical, "Error Creating Extract");
                    FCMessageBox.Show("Extract not completed.", MsgBoxStyle.Information, "ERROR");
                }
            }
            finally
            {
                FCFileSystem.FileClose(38);
                FCFileSystem.FileClose(39);
                rsCL.DisposeOf();
                rsSetting.DisposeOf();
                rsAccounts.DisposeOf();
                frmWait.InstancePtr.Unload();

            }

        }

        private static TaxServiceRecord FillTaxServiceRecord(clsDRWrapper rsCL, ref bool boolEmpty, bool boolPreviousYear = false)
        {
            //FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
            TaxServiceRecord FillTaxServiceRecord = new TaxServiceRecord(0);
            try
            {
                // On Error GoTo ERROR_HANDLER
                clsDRWrapper rsLN = new clsDRWrapper();
                //FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
                TaxServiceRecord trBill = new TaxServiceRecord(0);
                clsDRWrapper rsBook = new clsDRWrapper();
                clsDRWrapper rsRK = new clsDRWrapper();
                bool boolPD = false;
                double dblTotal/*unused?*/;
                double dblDiscount = 0;
                double dblRemaining;
                double dblDiscTotal = 0;
                // MAL@20080918 ; Tracker Reference: 15326
                if (!rsCL.EndOfFile())
                {
                    var accountNumber = rsCL.Get_Fields_Int32("Account");

                    var mapLot = rsCL.Get_Fields_String("MapLot");

                    var tax1Balance = Conversion.Val(trBill.Tax1Balance);

                    var tax2Balance = Conversion.Val(trBill.Tax2Balance);

                    var tax3Balance = Conversion.Val(trBill.Tax3Balance);

                    var tax4Balance = Conversion.Val(trBill.Tax4Balance);

                    var totalDemandFeesDue = Conversion.Val(trBill.TotalDemandFeesDue);

                    var totInterestDue = Conversion.Val(trBill.TotalInterestDue);

                    var totalFeesAndInterestDue = totalDemandFeesDue + totInterestDue;

                    _elevenZeros = Strings.StrDup(11, "0");
                    var elevenZeros = _elevenZeros;

                    var fiveZeros = Strings.StrDup(5, "0");

                    if (rsCL.Get_Fields_String("BillingType") == "RE")
                    {
                        // regular RE bill record
                        // Year
                        
                        trBill.Year = modGlobal.PadToString((FCUtils.iDiv(rsCL.Get_Fields_Int32("BillingYear"), 10)), 4);
                        // Period                         'number of the period
                        // MapLot
                        if (mapLot == "")
                        {
                            trBill.MapLot = modGlobalFunctions.PadStringWithSpaces(rsCL.Get_Fields_String("RSMapLot"), 20, false);
                        }
                        else
                        {
                            trBill.MapLot = modGlobalFunctions.PadStringWithSpaces(mapLot, 20, false);
                        }
                        // Name
                        trBill.Name = modGlobalFunctions.PadStringWithSpaces(rsCL.Get_Fields_String("Name1"), 40, false);
                        // LocationNumber
                        trBill.LocationNumber = modGlobalFunctions.PadStringWithSpaces(rsCL.Get_Fields_String("RSLocNumAlph"), 5);
                        // LocationStreet
                        trBill.LocationStreet = modGlobalFunctions.PadStringWithSpaces(rsCL.Get_Fields_String("RSLocStreet"), 30, false);
                        // Acres
                        trBill.Acres = modGlobal.PadToString((FCUtils.iDiv((Conversion.Val(0 + rsCL.Get_Fields_Double("PIACRES")) * 100), 1)), 8);
                        // SQFootage
                        trBill.SQFootage = Strings.StrDup(8, "0");
                        // BuildingVal
                        if ((rsCL.Get_Fields_Int32("BuildingValue") + rsCL.Get_Fields_Int32("LandValue")) > 0)
                        {
                            trBill.BuildingVal = modGlobal.PadToString(rsCL.Get_Fields_Int32("BuildingValue") + rsCL.Get_Fields_Int32("LandValue"), 9);
                        }
                        else
                        {
                            trBill.BuildingVal = modGlobal.PadToString(FCConvert.ToInt32(Conversion.Val(rsCL.Get_Fields_Int32("LASTBLDGVAL")) + Conversion.Val(rsCL.Get_Fields_Int32("LASTLANDVAL"))), 9);
                        }
                        // Book & Page
                        // trBill.Book = PadToString(.Get_Fields("Book"), 5)
                        // trBill.Page = PadToString(.Get_Fields("Page"), 5)
                        // 
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        rsBook.OpenRecordset("SELECT Top 1 * FROM BookPage WHERE Account = " + accountNumber + " AND Card = 1 ORDER BY Line", modExtraModules.strREDatabase);
                        if (!rsBook.EndOfFile())
                        {
                            // TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
                            trBill.Book = rsBook.Get_Fields_String("Book").PadLeft(5, '0');
                            // TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
                            trBill.Page = rsBook.Get_Fields_String("Page").PadLeft(5, '0');
                        }
                        else
                        {
                            var bookPage = rsCL.Get_Fields_String("BookPage");

                            if (bookPage != "")
                            {
                                // parse the bookpage field
                                trBill.Book = ParseBookPage(bookPage, true).PadLeft(5, '0');
                                trBill.Page = ParseBookPage(bookPage, false).PadLeft(5, '0');
                            }
                            else
                            {
                                var rsRef1 = rsCL.Get_Fields_String("RSRef1");

                                if (rsRef1 != "")
                                {
                                    trBill.Book = ParseBookPage(rsRef1, true).PadLeft(5, '0');
                                    trBill.Page = ParseBookPage(rsRef1, false).PadLeft(5, '0');
                                }
                                else
                                {
                                    trBill.Book = fiveZeros;
                                    trBill.Page = fiveZeros;
                                }
                            }
                        }

                        // Account                   'record number
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        trBill.Account = modGlobal.PadToString(accountNumber, 10);
                        // Abatement
                        trBill.Abatement = elevenZeros;
                        // Exemption
                        if (Conversion.Val(rsCL.Get_Fields_Int32("ExemptValue")) != 0)
                        {
                            trBill.Exemption = GetIntMoneyString(rsCL.Get_Fields_Int32("ExemptValue"));
                        }
                        else
                        {
                            trBill.Exemption = modGlobal.PadToString(0, 11);
                        }
                        if (rsCL.Get_Fields_Int32("LienRecordNumber") == 0 && rsCL.Get_Fields_Int32("RateKey") != 0)
                        {
                            rsRK.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + rsCL.Get_Fields_Int32("RateKey"));
                            // LienAmount
                            trBill.LienAmount = elevenZeros;
                            // LienBalance
                            trBill.LienBalance = elevenZeros;
                            // BettermentAmount
                            trBill.BettermentAmount = elevenZeros;
                            // BettermentBalance
                            trBill.BettermentBalance = elevenZeros;
                            // Tax1       original tax
                            trBill.Tax1 = PadToString(rsCL, "TaxDue1");
                            // Tax2
                            trBill.Tax2 = PadToString(rsCL, "TaxDue2");
                            // Tax3
                            trBill.Tax3 = PadToString(rsCL, "TaxDue3");
                            // Tax4
                            trBill.Tax4 = PadToString(rsCL, "TaxDue4");
                            // calculate periods of the account
                            var refVar1 = trBill.Tax1Balance;
                            var refVar2 = trBill.Tax1InterestDue;
                            var refVar3 = trBill.Tax2Balance;
                            var refVar4 = trBill.Tax2InterestDue;
                            var refVar5 = trBill.Tax3Balance;
                            var refVar6 = trBill.Tax3InterestDue;
                            var refVar7 = trBill.Tax4Balance;
                            var refVar8 = trBill.Tax4InterestDue;
                            DeterminePeriodDue(ref rsCL, ref rsLN, ref refVar1, ref refVar2, ref refVar3, ref refVar4, ref refVar5, ref refVar6, ref refVar7, ref refVar8);
                            trBill.Tax1Balance = refVar1;
                            trBill.Tax1InterestDue = refVar2;
                            trBill.Tax2Balance = refVar3;
                            trBill.Tax2InterestDue = refVar4;
                            trBill.Tax3Balance = refVar5;
                            trBill.Tax3InterestDue = refVar6;
                            trBill.Tax4Balance = refVar7;
                            trBill.Tax4InterestDue = refVar8;

                            trBill.TotalInterestDue = modGlobal.PadToString(FCUtils.iDiv((Conversion.Val(trBill.Tax1InterestDue) + Conversion.Val(trBill.Tax2InterestDue) + Conversion.Val(trBill.Tax3InterestDue) + Conversion.Val(trBill.Tax4InterestDue)), 1), 11);
                            // TotalDemandFeesDue
                            trBill.TotalDemandFeesDue = modGlobal.PadToString(FCConvert.ToInt32(((rsCL.Get_Fields_Decimal("DemandFees") - rsCL.Get_Fields_Decimal("DemandFeesPaid")) * 100) / 1), 11);
                            tax1Balance = Conversion.Val(trBill.Tax1Balance);

                           tax2Balance = Conversion.Val(trBill.Tax2Balance);

                            tax3Balance = Conversion.Val(trBill.Tax3Balance);

                            tax4Balance = Conversion.Val(trBill.Tax4Balance);

                            totalDemandFeesDue = Conversion.Val(trBill.TotalDemandFeesDue);

                            totInterestDue = Conversion.Val(trBill.TotalInterestDue);

                            totalFeesAndInterestDue = totalDemandFeesDue + totInterestDue;

                            // TotalAmountDue
                            if (!boolPreviousYear)
                            {

                                switch (Statics.intPeriod)
                                {
                                    case 1:
                                        {
                                            trBill.TotalAmountDue = GetIntMoneyString(tax1Balance + totalFeesAndInterestDue);
                                            break;
                                        }
                                    case 2:
                                        {
                                            trBill.TotalAmountDue = GetIntMoneyString(tax1Balance + tax2Balance + totalFeesAndInterestDue);
                                            break;
                                        }
                                    case 3:
                                        {
                                            trBill.TotalAmountDue = GetIntMoneyString(tax1Balance + tax2Balance + tax3Balance + totalFeesAndInterestDue);
                                            break;
                                        }
                                    case 4:
                                        {
                                            trBill.TotalAmountDue = GetIntMoneyString(tax1Balance + tax2Balance + tax3Balance + tax4Balance + totalFeesAndInterestDue);
                                            break;
                                        }
                                }
                                //end switch
                            }
                            else
                            {
                                trBill.TotalAmountDue = GetIntMoneyString(tax1Balance + tax2Balance + tax3Balance + tax4Balance + totalFeesAndInterestDue);
                            }
                            // PaidFlag
                            if (Conversion.Val(trBill.TotalAmountDue) <= 0)
                            {
                                trBill.PaidFlag = "PD";
                                boolPD = true;
                            }
                            else
                            {
                                trBill.PaidFlag = "  ";
                                boolPD = false;
                            }
                            // ExemptFlag
                            // is this any partially exempt?
                            trBill.ExemptFlag = "  ";
                            // PriorYearDelinquent
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            if (PriorYearsAreDelinquent(accountNumber, boolPD) || boolPreviousYear)
                            {
                                trBill.PriorYearDelinquent = "Y";
                            }
                            else
                            {
                                trBill.PriorYearDelinquent = "N";
                            }
                            // LienIndicator
                            trBill.LienIndicator = Strings.StrDup(8, " ");
                            dblDiscTotal = (Conversion.Val(trBill.Tax1) + Conversion.Val(trBill.Tax2) + Conversion.Val(trBill.Tax3) + Conversion.Val(trBill.Tax4)) / 100;
                            dblRemaining = FCConvert.ToDouble(trBill.TotalAmountDue) / 100;
                            dblDiscount = modCLDiscount.CalculateDiscount(dblDiscTotal);
                            trBill.TotalDiscount = GetIntMoneyString(dblDiscount * 100);
                            // Net Amount Due is the ORIGINAL tax minus the discount
                            trBill.NetAmountDue = GetIntMoneyString((dblDiscTotal - dblDiscount) * 100);

                        }
                        else
                        {
                            // lien record
                            rsLN.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + rsCL.Get_Fields_Int32("LienRecordNumber"));
                            if (rsLN.EndOfFile())
                            {
                                boolEmpty = true;
                                return FillTaxServiceRecord;
                            }
                            else
                            {
                                if (((rsLN.Get_Fields_Int32("RateKey"))) == 0)
                                {
                                    boolEmpty = true;
                                    return FillTaxServiceRecord;
                                }
                            }
                            rsRK.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(rsLN.Get_Fields_Int32("RateKey")));
                            // LienAmount
                            var costs = Conversion.Val(rsLN.Get_Fields("Costs"));
                            var principal = Conversion.Val(rsLN.Get_Fields("Principal"));
                            var interest = Conversion.Val(rsLN.Get_Fields("Interest"));
                            trBill.LienAmount = GetIntMoneyString((principal + interest + costs) * 100);
                            // LienBalance
                            var costsPaid = Conversion.Val(rsLN.Get_Fields_Decimal("CostsPaid"));
                            var maturityFee = Conversion.Val(rsLN.Get_Fields("MaturityFee"));
                            var principalPaid = Conversion.Val(rsLN.Get_Fields_Decimal("PrincipalPaid"));
                            var interestCharged = Conversion.Val(rsLN.Get_Fields_Decimal("InterestCharged"));
                            var interestPaid = Conversion.Val(rsLN.Get_Fields_Decimal("InterestPaid"));
                            var pliPaid = Conversion.Val(rsLN.Get_Fields("PLIPaid"));
                            tax1Balance = (principal + interest + costs - maturityFee - interestCharged) - (principalPaid + interestPaid + pliPaid + costsPaid);
                            trBill.LienBalance = GetIntMoneyString(((principal + interest + costs - maturityFee - interestCharged) - (principalPaid + interestPaid + pliPaid + costsPaid)) * 100);
                            // BettermentAmount
                            trBill.BettermentAmount = elevenZeros;
                            // BettermentBalance
                            trBill.BettermentBalance = elevenZeros;
                            // Tax1
                            trBill.Tax1 = trBill.LienAmount;
                            // Tax1Balance
                            trBill.Tax1Balance = trBill.LienBalance;
                            var refVar2 = trBill.Tax1InterestDue;
                            var refVar3 = trBill.Tax2Balance;
                            var refVar4 = trBill.Tax2InterestDue;
                            var refVar5 = trBill.Tax3Balance;
                            var refVar6 = trBill.Tax3InterestDue;
                            var refVar7 = trBill.Tax4Balance;
                            var refVar8 = trBill.Tax4InterestDue;
                            //DeterminePeriodDue_18(rsCL, rsLN, FCConvert.ToString(FCUtils.iDiv(((principal - principalPaid) * 100), 1)), ref refVar2, ref refVar3, ref refVar4, ref refVar5, ref refVar6, ref refVar7, ref refVar8);
                            //trBill.Tax1InterestDue = refVar2;
                            //trBill.Tax2Balance = refVar3;
                            //trBill.Tax2InterestDue = refVar4;
                            //trBill.Tax3Balance = refVar5;
                            //trBill.Tax3InterestDue = refVar6;
                            //trBill.Tax4Balance = refVar7;
                            //trBill.Tax4InterestDue = refVar8;
                            DeterminePeriodDue_18(rsCL, rsLN, FCConvert.ToString(FCUtils.iDiv(((principal - principalPaid) * 100), 1)), ref refVar2, ref refVar3, ref refVar4, ref refVar5, ref refVar6, ref refVar7, ref refVar8);
                            trBill.Tax1InterestDue = refVar2;
                            trBill.Tax2Balance = refVar3;
                            trBill.Tax2InterestDue = refVar4;
                            trBill.Tax3Balance = refVar5;
                            trBill.Tax3InterestDue = refVar6;
                            trBill.Tax4Balance = refVar7;
                            trBill.Tax4InterestDue = refVar8;
                            // Tax2
                            trBill.Tax2 = elevenZeros;

                            trBill.Tax3 = elevenZeros;

                            trBill.Tax4 = elevenZeros;

                            trBill.TotalInterestDue = trBill.Tax1InterestDue;
                            totInterestDue = trBill.TotalInterestDue.ToDoubleValue() / 100;
                            if (totInterestDue != 0)
                            {
                                totInterestDue = totInterestDue;
                            }
                            if (costsPaid > costs)
                            {
                                // TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
                                // TODO Get_Fields: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
                                trBill.TotalDemandFeesDue = GetIntMoneyString((costs - maturityFee - costsPaid) * 100);
                            }
                            else
                            {
                                // TODO Get_Fields: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
                                trBill.TotalDemandFeesDue = modGlobal.PadToString(Convert.ToInt32(FCConvert.ToDouble(maturityFee) * -1 * 100), 11);
                            }
                            // TotalAmountDue
                            trBill.TotalAmountDue = modGlobal.PadToString(FCConvert.ToInt32((tax1Balance + totInterestDue) * 100), 11);
                            // PaidFlag
                            if (Conversion.Val(trBill.TotalAmountDue) <= 0)
                            {
                                trBill.PaidFlag = "PD";
                                boolPD = true;
                            }
                            else
                            {
                                trBill.PaidFlag = "  ";
                                boolPD = false;
                            }
                            // ExemptFlag
                            // is this any partially exempt?
                            trBill.ExemptFlag = "  ";
                            // PriorYearDelinquent
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            if (PriorYearsAreDelinquent(accountNumber, boolPD) || boolPreviousYear)
                            {
                                trBill.PriorYearDelinquent = "Y";
                            }
                            else
                            {
                                trBill.PriorYearDelinquent = "N";
                            }
                            // LienIndicator
                            // TODO Get_Fields: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
                            if (FCConvert.ToInt32(rsLN.Get_Fields("MaturityFee")) > 0)
                            {
                                trBill.LienIndicator = modGlobalFunctions.PadStringWithSpaces("FF", 8, false);
                            }
                            else
                            {
                                trBill.LienIndicator = modGlobalFunctions.PadStringWithSpaces(" ", 8, false);
                            }
                            dblRemaining = FCConvert.ToDouble(trBill.TotalAmountDue) / 100;
                            dblDiscTotal = (Conversion.Val(trBill.Tax1) + Conversion.Val(trBill.Tax2) + Conversion.Val(trBill.Tax3) + Conversion.Val(trBill.Tax4)) / 100;
                            dblDiscount = 0;
                            trBill.TotalDiscount = GetIntMoneyString(dblDiscount * 100);
                            trBill.NetAmountDue = GetIntMoneyString((dblDiscTotal - dblDiscount) * 100);
                        }
                    }
                    else
                    {
                        // PP Record
                        // Year
                        trBill.Year = modGlobal.PadToString((FCUtils.iDiv(rsCL.Get_Fields_Int32("BillingYear"), 10)), 4);
                        // Period                         'number of the period
                        // MapLot
                        if (mapLot != "")
                        {
                            trBill.MapLot = modGlobalFunctions.PadStringWithSpaces(mapLot, 20, false);
                        }
                        else
                        {
                            trBill.MapLot = modGlobalFunctions.PadStringWithSpaces(" ", 20, false);
                        }
                        // Name
                        trBill.Name = modGlobalFunctions.PadStringWithSpaces(rsCL.Get_Fields_String("Name1"), 40, false);
                        // LocationNumber
                        // TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
                        trBill.LocationNumber = modGlobalFunctions.PadStringWithSpaces(rsCL.Get_Fields_Int32("StreetNumber").ToString(), 5).ToString();
                        // LocationStreet
                        trBill.LocationStreet = modGlobalFunctions.PadStringWithSpaces(Strings.Trim(rsCL.Get_Fields_String("Street")), 30, false);
                        // Acres
                        trBill.Acres = Strings.StrDup(8, "0");
                        // SQFootage
                        trBill.SQFootage = Strings.StrDup(8, "0");
                        // BuildingVal
                        trBill.BuildingVal = Strings.StrDup(9, "0");
                        trBill.Book = fiveZeros;
                        trBill.Page = fiveZeros;
                        // Account                   'record number
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        trBill.Account = modGlobal.PadToString(accountNumber, 10).ToString();
                        // Abatement
                        trBill.Abatement = elevenZeros;
                        // Exemption
                        trBill.Exemption = GetIntMoneyString(rsCL.Get_Fields_Int32("ExemptValue"));
                        rsRK.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + rsCL.Get_Fields_Int32("RateKey"));
                        // LienAmount
                        trBill.LienAmount = elevenZeros;
                        // LienBalance
                        trBill.LienBalance = elevenZeros;
                        // BettermentAmount
                        trBill.BettermentAmount = elevenZeros;
                        // BettermentBalance
                        trBill.BettermentBalance = elevenZeros;
                        // Tax1       original tax
                        trBill.Tax1 = PadToString(rsCL, "TaxDue1");
                        // Tax2
                        trBill.Tax2 = PadToString(rsCL, "TaxDue2");
                        // Tax3
                        trBill.Tax3 = PadToString(rsCL, "TaxDue3");
                        // Tax4
                        trBill.Tax4 = PadToString(rsCL, "TaxDue4");
                        // calculate periods of the account
                        var refVar1 = trBill.Tax1Balance;
                        var refVar2 = trBill.Tax1InterestDue;
                        var refVar3 = trBill.Tax2Balance;
                        var refVar4 = trBill.Tax2InterestDue;
                        var refVar5 = trBill.Tax3Balance;
                        var refVar6 = trBill.Tax3InterestDue;
                        var refVar7 = trBill.Tax4Balance;
                        var refVar8 = trBill.Tax4InterestDue;
                        DeterminePeriodDue(ref rsCL, ref rsLN, ref refVar1, ref refVar2, ref refVar3, ref refVar4, ref refVar5, ref refVar6, ref refVar7, ref refVar8);
                        trBill.Tax1Balance = refVar1;
                        trBill.Tax1InterestDue = refVar2;
                        trBill.Tax2Balance = refVar3;
                        trBill.Tax2InterestDue = refVar4;
                        trBill.Tax3Balance = refVar5;
                        trBill.Tax3InterestDue = refVar6;
                        trBill.Tax4Balance = refVar7;
                        trBill.Tax4InterestDue = refVar8;
                         tax1Balance = Conversion.Val(trBill.Tax1Balance);

                         tax2Balance = Conversion.Val(trBill.Tax2Balance);

                         tax3Balance = Conversion.Val(trBill.Tax3Balance);

                         tax4Balance = Conversion.Val(trBill.Tax4Balance);

                         
                        trBill.TotalInterestDue = modGlobal.PadToString(FCConvert.ToInt32((Conversion.Val(trBill.Tax1InterestDue) + Conversion.Val(trBill.Tax2InterestDue) + Conversion.Val(trBill.Tax3InterestDue) + Conversion.Val(trBill.Tax4InterestDue))), 11);
                        // TotalDemandFeesDue
                        trBill.TotalDemandFeesDue = modGlobal.PadToString(FCConvert.ToInt32(((rsCL.Get_Fields_Decimal("DemandFees") - rsCL.Get_Fields_Decimal("DemandFeesPaid")) * 100) / 1), 11);
                        // TotalAmountDue
                        totalDemandFeesDue = Conversion.Val(trBill.TotalDemandFeesDue);

                        totInterestDue = Conversion.Val(trBill.TotalInterestDue);

                        totalFeesAndInterestDue = totalDemandFeesDue + totInterestDue;
                        switch (Statics.intPeriod)
                        {
                            case 1:
                                {
                                    trBill.TotalAmountDue = modGlobal.PadToString(FCConvert.ToInt32((tax1Balance + totalFeesAndInterestDue)), 11);
                                    break;
                                }
                            case 2:
                                {
                                    trBill.TotalAmountDue = modGlobal.PadToString(FCConvert.ToInt32((tax1Balance + tax2Balance + totalFeesAndInterestDue)), 11);
                                    break;
                                }
                            case 3:
                                {
                                    trBill.TotalAmountDue = modGlobal.PadToString(FCConvert.ToInt32((tax1Balance + tax2Balance + tax3Balance + totalFeesAndInterestDue)), 11);
                                    break;
                                }
                            case 4:
                                {
                                    trBill.TotalAmountDue = modGlobal.PadToString(FCConvert.ToInt32((tax1Balance + tax2Balance + tax3Balance + tax4Balance + totalFeesAndInterestDue)), 11);
                                    break;
                                }
                        }
                        //end switch
                        // PaidFlag
                        if (Conversion.Val(trBill.TotalAmountDue) <= 0)
                        {
                            trBill.PaidFlag = "PD";
                            boolPD = true;
                        }
                        else
                        {
                            trBill.PaidFlag = "  ";
                            boolPD = false;
                        }
                        // ExemptFlag
                        // is this any partially exempt?
                        trBill.ExemptFlag = "  ";
                        // PriorYearDelinquent
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        if (PriorYearsAreDelinquent(accountNumber, boolPD) || boolPreviousYear)
                        {
                            trBill.PriorYearDelinquent = "Y";
                        }
                        else
                        {
                            trBill.PriorYearDelinquent = "N";
                        }
                        // LienIndicator
                        trBill.LienIndicator = Strings.StrDup(8, " ");

                        dblDiscTotal = (Conversion.Val(trBill.Tax1) + Conversion.Val(trBill.Tax2) + Conversion.Val(trBill.Tax3) + Conversion.Val(trBill.Tax4)) / 100;
                        // MAL@20081113: Changed to always calculate the discount
                        // Tracker Reference: 15326
                        // If Not boolPreviousYear Then
                        dblRemaining = FCConvert.ToDouble(trBill.TotalAmountDue) / 100;
                        // dblDiscount = CalculateDiscount(dblTotal)
                        dblDiscount = modCLDiscount.CalculateDiscount(dblDiscTotal);
                        trBill.TotalDiscount = GetIntMoneyString(dblDiscount * 100);
                        trBill.NetAmountDue = GetIntMoneyString((dblDiscTotal - dblDiscount) * 100);

                    }
                    FillTaxServiceRecord = trBill;
                }
                else
                {
                    // this will make sure that it is not printed in the file
                    boolEmpty = true;
                }
                return FillTaxServiceRecord;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                frmWait.InstancePtr.Unload();
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Filling Tax Service Record");
            }
            return FillTaxServiceRecord;
        }

        private static string PadToString(clsDRWrapper rs, string fieldName)
        {
            return modGlobal.PadToString(FCConvert.ToInt32((rs.Get_Fields_Decimal(fieldName) * 100) / 1), 11);
        }

        private static string GetIntMoneyString(double moneyAmount)
        {
            return modGlobal.PadToString(FCUtils.iDiv(moneyAmount, 1), 11);
        }

        private static void OutputExtractRecord(TaxServiceRecord TR,  int intPassPeriod)
        {
            try
            {

                // this holds the record and gets output to the ASC file #38
                StringBuilder sb = new StringBuilder();

                TR.Period = intPassPeriod.ToString();

                sb.Append(TR.Year??"");
                FCFileSystem.Write(39, TR.Year??"");

                sb.Append(FCConvert.ToString(intPassPeriod));
                FCFileSystem.Write(39, intPassPeriod);

                sb.Append(TR.MapLot??"");
                FCFileSystem.Write(39, TR.MapLot??"");

                sb.Append(TR.Name??"");
                FCFileSystem.Write(39, TR.Name??"");

                sb.Append(TR.LocationNumber??"");
                FCFileSystem.Write(39, TR.LocationNumber??"");

                sb.Append(TR.LocationStreet??"");
                FCFileSystem.Write(39, TR.LocationStreet??"");

                sb.Append(TR.Acres??"");
                FCFileSystem.Write(39, TR.Acres??"");

                sb.Append(TR.SQFootage??"");
                FCFileSystem.Write(39, TR.SQFootage??"");

                sb.Append(TR.BuildingVal??"");
                FCFileSystem.Write(39, TR.BuildingVal??"");

                sb.Append(TR.Book??"");
                FCFileSystem.Write(39, TR.Book??"");

                sb.Append(TR.Page??"");
                FCFileSystem.Write(39, TR.Page??"");

                sb.Append(TR.Account??"");
                FCFileSystem.Write(39, TR.Account??"");

                sb.Append(TR.Abatement??"");
                FCFileSystem.Write(39, TR.Abatement??"");

                sb.Append(TR.Exemption??"");
                FCFileSystem.Write(39, TR.Exemption??"");

                sb.Append(TR.LienAmount??"");
                FCFileSystem.Write(39, TR.LienAmount??"");

                sb.Append(TR.LienBalance??"");
                FCFileSystem.Write(39, TR.LienBalance??"");

                sb.Append(TR.BettermentAmount??"");
                FCFileSystem.Write(39, TR.BettermentAmount??"");

                sb.Append(TR.BettermentBalance??"");
                FCFileSystem.Write(39, TR.BettermentBalance??"");

                sb.Append(TR.Tax1??"");
                FCFileSystem.Write(39, TR.Tax1??"");

                sb.Append(TR.Tax1Balance??"");
                FCFileSystem.Write(39, TR.Tax1Balance??"");

                sb.Append(TR.Tax1InterestDue??"");
                FCFileSystem.Write(39, TR.Tax1InterestDue??"");

                sb.Append(TR.Tax2??"");
                FCFileSystem.Write(39, TR.Tax2??"");

                sb.Append(TR.Tax2Balance??"");
                FCFileSystem.Write(39, TR.Tax2Balance);

                sb.Append(TR.Tax2InterestDue??"");
                FCFileSystem.Write(39, TR.Tax2InterestDue??"");

                sb.Append(TR.Tax3??"");
                FCFileSystem.Write(39, TR.Tax3??"");

                sb.Append(TR.Tax3Balance??"");
                FCFileSystem.Write(39, TR.Tax3Balance??"");

                sb.Append(TR.Tax3InterestDue??"");
                FCFileSystem.Write(39, TR.Tax3InterestDue??"");

                sb.Append(TR.Tax4??"");
                FCFileSystem.Write(39, TR.Tax4??"");

                sb.Append(TR.Tax4Balance??"");
                FCFileSystem.Write(39, TR.Tax4Balance??"");

                sb.Append(TR.Tax4InterestDue??"");
                FCFileSystem.Write(39, TR.Tax4InterestDue??"");

                sb.Append(TR.TotalInterestDue??"");
                FCFileSystem.Write(39, TR.TotalInterestDue??"");

                sb.Append(TR.TotalDemandFeesDue??"");
                FCFileSystem.Write(39, TR.TotalDemandFeesDue??"");

                sb.Append(TR.TotalAmountDue??"");
                FCFileSystem.Write(39, TR.TotalAmountDue??"");

                sb.Append(TR.PaidFlag??"");
                FCFileSystem.Write(39, TR.PaidFlag??"");

                sb.Append(TR.ExemptFlag??"");
                FCFileSystem.Write(39, TR.ExemptFlag);

                sb.Append(TR.PriorYearDelinquent);
                FCFileSystem.Write(39, TR.PriorYearDelinquent);

                sb.Append(TR.LienIndicator);
                FCFileSystem.Write(39, TR.LienIndicator);

                sb.Append(TR.TotalDiscount);
                FCFileSystem.Write(39, TR.TotalDiscount);

                sb.Append(TR.NetAmountDue);
                FCFileSystem.Write(39, TR.NetAmountDue);

                sb.Append(_elevenZeros);
                FCFileSystem.Write(39, _elevenZeros);

                //
                FCFileSystem.PrintLine(38, sb.ToString());
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                frmWait.InstancePtr.Unload();
                Statics.boolErrorPrintingToFile = true;
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error During Output");
            }
        }
        // VBto upgrade warning: intPassPeriod As short	OnWriteFCConvert.ToInt32(
        private static void OutputDatabaseExtractRecord_6(TaxServiceRecord TR, int intPassPeriod, int lngLastRowUsed)
        {
            OutputDatabaseExtractRecord(ref TR, ref intPassPeriod, ref lngLastRowUsed);
        }

        private static void OutputDatabaseExtractRecord(ref TaxServiceRecord TR, ref int intPassPeriod, ref int lngLastRowUsed)
        {
            clsDRWrapper rsRE = new clsDRWrapper();

            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will actually create the record and output it to the output file #38
                int lngRow;

                string strMailingAddress = "";

                // VBto upgrade warning: strData As string	OnWrite(FixedString, string)
                string strData = "";

                for (lngRow = 0; lngRow <= lngLastRowUsed; lngRow++)
                {
                    if (FCConvert.ToDouble(frmDatabaseExtract.InstancePtr.vsFields.TextMatrix(lngRow, frmDatabaseExtract.InstancePtr.lngColCheck)) != -1) continue;
                    //

                    if (lngRow == frmDatabaseExtract.InstancePtr.lngRowAccount)
                    {
                        strData = TR.Account;
                    }
                    else
                        if (lngRow == frmDatabaseExtract.InstancePtr.lngRowName)
                        {
                            strData = Strings.Trim(TR.Name);
                        }
                        else
                            if (lngRow == frmDatabaseExtract.InstancePtr.lngRowYear)
                            {
                                strData = Strings.Trim(TR.Year);
                            }
                            else
                                if (lngRow == frmDatabaseExtract.InstancePtr.lngRowLocation)
                                {
                                    strData = Strings.Trim($"{TR.LocationNumber} {Strings.Trim(TR.LocationStreet)}");
                                }
                                else
                                    if (lngRow == frmDatabaseExtract.InstancePtr.lngRowMapLot)
                                    {
                                        strData = $"ML:{Strings.Trim(TR.MapLot)}";
                                    }
                                    else
                                        if (lngRow == frmDatabaseExtract.InstancePtr.lngRowBookPage)
                                        {
                                            strData = $"B{TR.Book}P{TR.Page}";
                                        }
                                        else
                                            if (lngRow == frmDatabaseExtract.InstancePtr.lngRowOrigTax)
                                            {
                                                strData = Strings.Format((Conversion.Val(TR.Tax1) + Conversion.Val(TR.Tax2) + Conversion.Val(TR.Tax3) + Conversion.Val(TR.Tax4)) / 100, "###0.00");
                                            }
                                            else
                                                if (lngRow == frmDatabaseExtract.InstancePtr.lngRowOutstanding)
                                                {
                                                    strData = Strings.Format((Conversion.Val(TR.Tax1Balance) + Conversion.Val(TR.Tax2Balance) + Conversion.Val(TR.Tax3Balance) + Conversion.Val(TR.Tax4Balance)) / 100, "###0.00");
                                                }
                                                else
                                                    if (lngRow == frmDatabaseExtract.InstancePtr.lngRowAddress)
                                                    {
                                                        rsRE.OpenRecordset($"SELECT Address1,Address2,City,[State],Zip FROM Master INNER JOIN {modGlobal.Statics.strDbCP}PartyAndAddressView pOwn ON Master.OwnerPartyID = pOwn.PartyID WHERE RSAccount = {TR.Account}", modExtraModules.strREDatabase);

                                                        if (rsRE.EndOfFile())
                                                        {
                                                            strMailingAddress = "";
                                                        }
                                                        else
                                                        {
                                                            // kk rsRE.InsertName "OwnerPartyID,SecOwnerPartyID", "Own1,Own2", False, True, True, "", False, "", True, ""
                                                            var address1 = FCConvert.ToString(rsRE.Get_Fields_String("Address1"));
                                                            if (Strings.Trim(address1) != "") strMailingAddress = address1 + " ";

                                                            var address2 = FCConvert.ToString(rsRE.Get_Fields_String("Address2"));
                                                            if (Strings.Trim(address2) != "") strMailingAddress += address2 + " ";

                                                            var city = FCConvert.ToString(rsRE.Get_Fields_String("City"));
                                                            if (Strings.Trim(city) != "") strMailingAddress += city + " ";

                                                            strMailingAddress += FCConvert.ToString(rsRE.Get_Fields_String("State")) + " ";
                                                            strMailingAddress += FCConvert.ToString(rsRE.Get_Fields_String("Zip")) + " ";
                                                            strData = strMailingAddress;
                                                        }
                                                    }

                    if (lngRow == lngLastRowUsed)
                        FCFileSystem.WriteLine(38, strData);
                    else
                        FCFileSystem.Write(38, strData);
                }
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                frmWait.InstancePtr.Unload();
                Statics.boolErrorPrintingToFile = true;
                FCMessageBox.Show("Error #"
                                  + FCConvert.ToString(Information.Err(ex)
                                                                  .Number)
                                  + " - "
                                  + ex.GetBaseException()
                                      .Message
                                  + ".", MsgBoxStyle.Critical, "Error During Output");
            }
            finally
            {
                rsRE.DisposeOf();
            }
        }

        private static double DeterminePeriodDue_18(clsDRWrapper rsCol, clsDRWrapper rsLN, string strTax1Bal, ref string strTax1Int, ref string strTax2Bal, ref string strTax2Int, ref string strTax3Bal, ref string strTax3Int, ref string strTax4Bal, ref string strTax4Int)
        {
            return DeterminePeriodDue(ref rsCol, ref rsLN, ref strTax1Bal, ref strTax1Int, ref strTax2Bal, ref strTax2Int, ref strTax3Bal, ref strTax3Int, ref strTax4Bal, ref strTax4Int);
        }

        private static double DeterminePeriodDue(ref clsDRWrapper rsCol, ref clsDRWrapper rsLN, ref string strTax1Bal, ref string strTax1Int, ref string strTax2Bal, ref string strTax2Int, ref string strTax3Bal, ref string strTax3Int, ref string strTax4Bal, ref string strTax4Int)
        {
            double DeterminePeriodDue = 0;
            // VB6 Bad Scope Dim:
            decimal Tax1;
            try
            {
                decimal curPPaid;

                // make sure that these are initialized
                if (strTax1Bal.Trim() == "")
                    strTax1Bal = _elevenZeros;
                if (strTax1Int.Trim() == "")
                    strTax1Int = _elevenZeros;
                if (strTax2Bal.Trim() == "")
                    strTax2Bal = _elevenZeros;
                if (strTax2Int.Trim() == "")
                    strTax2Int = _elevenZeros;
                if (strTax3Bal.Trim() == "")
                    strTax3Bal = _elevenZeros;
                if (strTax3Int.Trim() == "")
                    strTax3Int = _elevenZeros;
                if (strTax4Bal.Trim() == "")
                    strTax4Bal = _elevenZeros;
                if (strTax4Int.Trim() == "")
                    strTax4Int = _elevenZeros;

                if (rsCol.Get_Fields_Int32("LienRecordNumber") == 0)
                {
                    // Determine How much is due for each period
                    decimal Tax2;
                    decimal Tax3;
                    decimal Tax4;
                    curPPaid = FCConvert.ToDecimal(rsCol.Get_Fields_Decimal("PrincipalPaid"));
                    // kgk 01-20-2012 trots-8  Allow abatements to be split between periods
                    double dblAbate1 = 0;
                    double dblAbate2 = 0;
                    double dblAbate3 = 0;
                    double dblAbate4 = 0;
                    modCLCalculations.CheckForAbatementPayments(rsCol.Get_Fields_Int32("ID"), rsCol.Get_Fields_Int32("RateKey"), rsCol.Get_Fields_String("BillingType") == "RE", ref dblAbate1, ref dblAbate2, ref dblAbate3, ref dblAbate4, FCConvert.ToDouble(rsCol.Get_Fields_Decimal("TaxDue1")), FCConvert.ToDouble(rsCol.Get_Fields_Decimal("TaxDue2")), FCConvert.ToDouble(rsCol.Get_Fields_Decimal("TaxDue3")), FCConvert.ToDouble(rsCol.Get_Fields_Decimal("TaxDue4")));
                    curPPaid -= FCConvert.ToDecimal(dblAbate1 + dblAbate2 + dblAbate3 + dblAbate4);
                    Tax1 = FCConvert.ToDecimal(rsCol.Get_Fields_Decimal("TaxDue1"));
                    // initialize the period taxes
                    Tax2 = FCConvert.ToDecimal(rsCol.Get_Fields_Decimal("TaxDue2"));
                    Tax3 = FCConvert.ToDecimal(rsCol.Get_Fields_Decimal("TaxDue3"));
                    Tax4 = FCConvert.ToDecimal(rsCol.Get_Fields_Decimal("TaxDue4"));
                    if (Tax1 < 0)
                    {

                    }
                    else
                    {
                        // kgk 01-20-2012 trots-8  Allow abatements to be split between periods
                        if (Tax1 >= (curPPaid + FCConvert.ToDecimal(dblAbate1)))
                        {
                            // If Tax1 >= curPPaid Then
                            Tax1 -= (curPPaid + FCConvert.ToDecimal(dblAbate1));
                            // Tax1 = Tax1 - curPPaid
                            Tax2 -= FCConvert.ToDecimal(dblAbate2);
                            Tax3 -= FCConvert.ToDecimal(dblAbate3);
                            Tax4 -= FCConvert.ToDecimal(dblAbate4);
                        }
                        else
                        {
                            curPPaid -= (Tax1 - FCConvert.ToDecimal(dblAbate1));
                            // curPPaid = curPPaid - Tax1
                            Tax1 = 0;
                            if (Tax2 >= (curPPaid + FCConvert.ToDecimal(dblAbate2)))
                            {
                                // If Tax2 >= curPPaid Then
                                Tax2 -= (curPPaid + FCConvert.ToDecimal(dblAbate2));
                                // Tax2 = Tax2 - curPPaid
                                Tax3 -= FCConvert.ToDecimal(dblAbate3);
                                Tax4 -= FCConvert.ToDecimal(dblAbate4);
                            }
                            else
                            {
                                curPPaid -= (Tax2 - FCConvert.ToDecimal(dblAbate2));
                                // curPPaid = curPPaid - Tax2
                                Tax2 = 0;
                                if (Tax3 >= (curPPaid + FCConvert.ToDecimal(dblAbate3)))
                                {
                                    // If Tax3 >= curPPaid Then
                                    Tax3 -= (curPPaid + FCConvert.ToDecimal(dblAbate3));
                                    // Tax3 = Tax3 - curPPaid
                                    Tax4 -= FCConvert.ToDecimal(dblAbate4);
                                }
                                else
                                {
                                    curPPaid -= (Tax3 - FCConvert.ToDecimal(dblAbate3));
                                    // curPPaid = curPPaid - Tax3
                                    Tax3 = 0;
                                    if (Tax4 >= (curPPaid + FCConvert.ToDecimal(dblAbate4)))
                                    {
                                        // If Tax4 >= curPPaid Then
                                        Tax4 -= (curPPaid + FCConvert.ToDecimal(dblAbate4));
                                        // Tax4 = Tax4 - curPPaid
                                    }
                                    else
                                    {
                                        curPPaid -= (Tax4 - FCConvert.ToDecimal(dblAbate4));
                                        // curPPaid = curPPaid - Tax4
                                        Tax4 = 0;
                                        if (curPPaid > 0)
                                        {
                                            Tax1 = curPPaid * -1;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    strTax1Bal = PadString(Tax1);
                    strTax1Int = PadString(DeterminePeriodInterest(rsCol, rsLN, ref Tax1, ref Statics.dtTSDate, 0, 1));

                    strTax2Bal = PadString(Tax2);
                    strTax2Int = PadString(DeterminePeriodInterest(rsCol, rsLN, ref Tax2, ref Statics.dtTSDate, 0, 2));

                    strTax3Bal = PadString(Tax3);
                    strTax3Int = PadString(DeterminePeriodInterest(rsCol, rsLN, ref Tax3, ref Statics.dtTSDate, 0, 3));

                    strTax4Bal = PadString(Tax4);
                    strTax4Int = PadString(DeterminePeriodInterest(rsCol, rsLN, ref Tax4, ref Statics.dtTSDate, 0, 4));
                }
                else
                {
                    // this is a lien record
                    Tax1 = FCConvert.ToDecimal((Conversion.Val(strTax1Bal) / 100));
                    strTax1Bal = modGlobal.PadToString(FCConvert.ToInt32(strTax1Bal), 11);
                    strTax1Int = PadString(DeterminePeriodInterest(rsCol, rsLN, ref Tax1, ref Statics.dtTSDate, 0, 1));

                    strTax2Bal = _elevenZeros;
                    strTax2Int = _elevenZeros;
                    strTax3Bal = _elevenZeros;
                    strTax3Int = _elevenZeros;
                    strTax4Bal = _elevenZeros;
                    strTax4Int = _elevenZeros;
                }
                return DeterminePeriodDue;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                frmWait.InstancePtr.Unload();
                DeterminePeriodDue = 0;
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Calculating Period Due");
            }
            return DeterminePeriodDue;
        }

        private static string PadString(decimal val)
        {
            return modGlobal.PadToString((FCUtils.iDiv((val * 100), 1)), 11);
        }

        // VBto upgrade warning: 'Return' As double	OnWriteFCConvert.ToDouble(
        private static decimal DeterminePeriodInterest(clsDRWrapper rsCollection, clsDRWrapper rsLien, ref decimal curPrin, ref DateTime dateTemp, double curPD, short intPer)
        {
            decimal DeterminePeriodInterest = 0;
            // VB6 Bad Scope Dim:
            decimal Tax1 = 0;
            decimal Tax2 = 0;
            decimal Tax3 = 0;
            decimal Tax4 = 0;

            try
            {
                // On Error GoTo ERROR_HANDLER
                // Determine Interest Calculation Date
                // VBto upgrade warning: MostRecentInterestDate As DateTime	OnWrite(string, DateTime)
                DateTime[] MostRecentInterestDate = new DateTime[4 + 1];
                // VBto upgrade warning: AppliedDate As DateTime	OnWrite(string, DateTime)
                DateTime AppliedDate;
                // VBto upgrade warning: InterestAmount As double	OnWriteFCConvert.ToInt16(
                decimal InterestAmount;
                int intUsingStartDate;
                // VBto upgrade warning: curCInt As double	OnWrite(short, double, double)
                decimal curCInt = 0;
                double curPDTemp = 0;
                clsDRWrapper rsRR = new clsDRWrapper();
                decimal curPerInt;
                bool boolLien = false;
                // VBto upgrade warning: dtStartDate As DateTime	OnWriteFCConvert.ToInt16(
                DateTime?[] dtStartDate = new DateTime?[4 + 1];
                double dblIntRate/*unused?*/;
                intUsingStartDate = 0;
                InterestAmount = 0;
                if (rsCollection.Get_Fields_Int32("LienRecordNumber") == 0)
                {
                    boolLien = false;
                    rsRR.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + rsCollection.Get_Fields_Int32("RateKey"));
                }
                else
                {
                    boolLien = true;
                    rsLien.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + rsCollection.Get_Fields_Int32("LienRecordNumber"));
                    if (rsLien.EndOfFile())
                    {
                        // missing lien record, exit this function
                        return DeterminePeriodInterest;
                    }
                    rsRR.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + rsLien.Get_Fields_Int32("RateKey"));
                }
                if (rsRR.EndOfFile())
                {
                    // missing rate record
                    dtStartDate[1] = DateTime.FromOADate(0);
                    dtStartDate[2] = DateTime.FromOADate(0);
                    dtStartDate[3] = DateTime.FromOADate(0);
                    dtStartDate[4] = DateTime.FromOADate(0);
                    return DeterminePeriodInterest;
                }
                else
                {

                    dtStartDate[1] = (DateTime)rsRR.Get_Fields_DateTime("InterestStartDate1");
                    if (!rsRR.IsFieldNull("InterestStartDate2"))
                    {
                        dtStartDate[2] = (DateTime)rsRR.Get_Fields_DateTime("InterestStartDate2");
                    }
                    else if (intPer > 1)
                    {
                        return 0;
                    }
                    if (!rsRR.IsFieldNull("InterestStartDate3"))
                    {
                        dtStartDate[3] = (DateTime)rsRR.Get_Fields_DateTime("InterestStartDate3");
                    }
                    else if (intPer > 2)
                    {
                        return 0;
                    }
                    if (!rsRR.IsFieldNull("InterestStartDate4"))
                    {
                        dtStartDate[4] = (DateTime)rsRR.Get_Fields_DateTime("InterestStartDate4");
                    }
                    else if (intPer > 3)
                    {
                        return 0;
                    }
                    modStatusPayments.Statics.dblInterestRate = modGlobal.Round(Conversion.Val(rsRR.Get_Fields_Double("InterestRate")), 6);
                }
                if (!boolLien)
                {
                    if (rsCollection.Get_Fields_DateTime("InterestAppliedThroughDate").ToOADate() != 0 && !rsCollection.IsFieldNull("InterestAppliedThroughDate"))
                    {
                        AppliedDate = FCConvert.ToDateTime(Strings.Format(rsCollection.Get_Fields_DateTime("InterestAppliedThroughDate"), "MM/dd/yyyy"));
                        intUsingStartDate = 0;
                        // 0
                    }
                    else
                    {
                        AppliedDate = dtStartDate[intPer] ?? new DateTime(1,1,1);
                        intUsingStartDate = 0;
                    }
                }
                else
                {
                    // lien
                    if (rsLien.Get_Fields_DateTime("InterestAppliedThroughDate").ToOADate() != 0 && !rsLien.IsFieldNull("InterestAppliedThroughDate"))
                    {
                        AppliedDate = rsLien.Get_Fields_DateTime("InterestAppliedThroughDate");
                        // dtStartDate(1) '= rsRR.Get_Fields("InterestStartDate1")
                        intUsingStartDate = 0;
                    }
                    else
                    {
                        AppliedDate = FCConvert.ToDateTime(Strings.Format(rsLien.Get_Fields_DateTime("InterestAppliedThroughDate"), "MM/dd/yyyy"));
                        intUsingStartDate = 0;
                    }
                }
                // check to make sure that the applied through date is a valid date
                // and find the most recent int date for each period
                var appliedDate = FCConvert.ToDateTime(Strings.Format(AppliedDate, "MM/dd/yyyy"));

                if (intPer == 1)
                {
                    var startDate = DateAndTime.DateAdd("d", 0, FCConvert.ToDateTime(Strings.Format(dtStartDate[1], "MM/dd/yyyy")));

                    if (Information.IsDate(AppliedDate) == true)
                    {
                        // if the applied through date is after the interestStart date then
                        if (DateAndTime.DateValue(FCConvert.ToString(dtStartDate[1])).ToOADate() < DateAndTime.DateValue(FCConvert.ToString(AppliedDate)).ToOADate())
                        {
                            // use the applied date to calculate from
                            MostRecentInterestDate[1] = appliedDate;
                        }
                        else
                        {
                            MostRecentInterestDate[1] = startDate;
                            intUsingStartDate = 1;
                        }
                    }
                    else
                    {
                        MostRecentInterestDate[1] = startDate;
                        intUsingStartDate = 0;
                    }
                }
                if (intPer == 2)
                {
                    var startDate = DateAndTime.DateAdd("d", 0, FCConvert.ToDateTime(Strings.Format(dtStartDate[2], "MM/dd/yyyy")));

                    if (Information.IsDate(AppliedDate) == true)
                    {
                        // if the applied through date is after the interestStart date then
                        if (DateAndTime.DateValue(FCConvert.ToString(dtStartDate[2])).ToOADate() < DateAndTime.DateValue(FCConvert.ToString(AppliedDate)).ToOADate())
                        {
                            // use the applied date to calculate from
                            MostRecentInterestDate[2] = appliedDate;
                        }
                        else
                        {
                            MostRecentInterestDate[2] = startDate;
                            intUsingStartDate = 1;
                        }
                    }
                    else
                    {
                        MostRecentInterestDate[2] = startDate;
                        intUsingStartDate = 0;
                    }
                }
                if (intPer == 3)
                {
                    var startDate = DateAndTime.DateAdd("d", 0, FCConvert.ToDateTime(Strings.Format(dtStartDate[3], "MM/dd/yyyy")));

                    if (Information.IsDate(AppliedDate) == true)
                    {
                        // if the applied through date is after the interestStart date then
                        if (DateAndTime.DateValue(FCConvert.ToString(dtStartDate[3])).ToOADate() < DateAndTime.DateValue(FCConvert.ToString(AppliedDate)).ToOADate())
                        {
                            // use the applied date to calculate from
                            MostRecentInterestDate[3] = appliedDate;
                        }
                        else
                        {
                            MostRecentInterestDate[3] = startDate;
                            intUsingStartDate = 1;
                        }
                    }
                    else
                    {
                        MostRecentInterestDate[3] = startDate;
                        intUsingStartDate = 0;
                    }
                }
                if (intPer == 4)
                {
                    var startDate = DateAndTime.DateAdd("d", 0, FCConvert.ToDateTime(Strings.Format(dtStartDate[4], "MM/dd/yyyy")));

                    if (Information.IsDate(AppliedDate) == true)
                    {
                        // if the applied through date is after the interestStart date then
                        if (DateAndTime.DateValue(FCConvert.ToString(dtStartDate[4])).ToOADate() < DateAndTime.DateValue(FCConvert.ToString(AppliedDate)).ToOADate())
                        {
                            // use the applied date to calculate from
                            MostRecentInterestDate[4] = appliedDate;
                        }
                        else
                        {
                            MostRecentInterestDate[4] = startDate;
                            intUsingStartDate = 0;
                        }
                    }
                    else
                    {
                        MostRecentInterestDate[4] = startDate;
                        intUsingStartDate = 0;
                    }
                }
                if (!boolLien)
                {
                    // Determine How much is due for each period
                    if (intPer == 1) Tax1 = curPrin;
                    if (intPer == 2) Tax2 = curPrin;
                    if (intPer == 3) Tax3 = curPrin;
                    if (intPer == 4) Tax4 = curPrin;
                }
                else
                {
                    Tax1 = curPrin;
                    // If Tax1 > 0 Then Stop
                }
                if (boolLien)
                {
                    if (Information.IsDate(MostRecentInterestDate[1]) == true)
                    {
                        if (MostRecentInterestDate[1].ToOADate() >= dateTemp.ToOADate())
                        {
                            curCInt = 0;
                        }
                        else
                        {
                            curCInt = modCLCalculations.CalculateInterest(Tax1, MostRecentInterestDate[1], dateTemp, modStatusPayments.Statics.dblInterestRate, modGlobal.Statics.gintBasis, intUsingStartDate, modExtraModules.Statics.dblOverPayRate, ref curPD);
                            curCInt = curCInt;
                            // - rsLien.Get_Fields("InterestCharged") - rsLien.Get_Fields("InterestPaid")
                        }
                    }
                }
                else
                {
                    decimal PeriodInterest;
                    if (intPer == 1)
                    {
                        // Period 1
                        curPDTemp = 0;
                        PeriodInterest = modCLCalculations.CalculateInterest(Tax1, MostRecentInterestDate[1], dateTemp, modStatusPayments.Statics.dblInterestRate, modGlobal.Statics.gintBasis, intUsingStartDate, modExtraModules.Statics.dblOverPayRate, ref curPDTemp);
                        curCInt += PeriodInterest;
                    }
                    if (intPer == 2)
                    {
                        // Period 2
                        curPDTemp = 0;
                        PeriodInterest = modCLCalculations.CalculateInterest(Tax2, MostRecentInterestDate[2], dateTemp, modStatusPayments.Statics.dblInterestRate, modGlobal.Statics.gintBasis, intUsingStartDate, modExtraModules.Statics.dblOverPayRate, ref curPDTemp);
                        curCInt += PeriodInterest;
                    }
                    if (intPer == 3)
                    {
                        // Period 3
                        curPDTemp = 0;
                        PeriodInterest = modCLCalculations.CalculateInterest(Tax3, MostRecentInterestDate[3], dateTemp, modStatusPayments.Statics.dblInterestRate, modGlobal.Statics.gintBasis, intUsingStartDate, modExtraModules.Statics.dblOverPayRate, ref curPDTemp);
                        curCInt += PeriodInterest;
                    }
                    if (intPer == 4)
                    {
                        // Period 4
                        curPDTemp = 0;
                        PeriodInterest = modCLCalculations.CalculateInterest(Tax4, MostRecentInterestDate[4], dateTemp, modStatusPayments.Statics.dblInterestRate, modGlobal.Statics.gintBasis, intUsingStartDate, modExtraModules.Statics.dblOverPayRate, ref curPDTemp);
                        curCInt += PeriodInterest;
                    }
                }
                DeterminePeriodInterest = curCInt;
                rsRR.Reset();
                return DeterminePeriodInterest;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                frmWait.InstancePtr.Unload();
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In Period Interest");
            }
            return DeterminePeriodInterest;
        }

        private static bool PriorYearsAreDelinquent(int lngAcct, bool boolPaid)
        {
            bool PriorYearsAreDelinquent = false;
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this function will return true if the account number matches the
                // last account number that has called this function and has not paid...
                if (lngAcct == Statics.lngLastDelAccount)
                {
                    // check to see if the last account that was delinquent was the same as this one
                    PriorYearsAreDelinquent = true;
                }
                else
                {
                    PriorYearsAreDelinquent = false;
                }
                // this will set the latest account to the last delinquent account if they have not paid
                if (!boolPaid)
                {
                    Statics.lngLastDelAccount = lngAcct;
                }
                return PriorYearsAreDelinquent;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                frmWait.InstancePtr.Unload();
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Finding Prior Years");
            }
            return PriorYearsAreDelinquent;
        }

        private static string ParseBookPage(string strRef, bool boolBook)
        {
            string ParseBookPage = "";
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this function will take a string and parse the book or page value out of it
                // if boolBool = True then it will return the book, else return the Page
                int intpos;
                string strLeftOver = "";
                string strTemp = "";
                string strTemp2 = "";
                int x;
                int intMonth = 0;
                int lngYear = 0;
                int lngBook = 0;
                int lngPage = 0;
                // VBto upgrade warning: dtBPDate As DateTime	OnWrite(string)
                DateTime dtBPDate;
                intpos = Strings.InStr(1, strRef, "B", CompareConstants.vbTextCompare);
                if (intpos > 0)
                {
                    strRef = Strings.Mid(strRef, intpos);
                }
                else
                {
                    return ParseBookPage;
                }
                intpos = Strings.InStr(1, strRef, "P", CompareConstants.vbTextCompare);
                if (intpos <= 0)
                    return ParseBookPage;
                // there is a b and a p.
                if (intpos > 2)
                {
                    strTemp = Strings.Trim(Strings.Mid(strRef, 2, intpos - 2));
                    // if this isn't numeric then exit
                    if (!Information.IsNumeric(strTemp))
                    {
                        // check if it is bk, this could still be legit
                        if (intpos > 3)
                        {
                            if (Strings.UCase(Strings.Mid(strTemp, 1, 1)) == "K")
                            {
                                strTemp = Strings.Trim(Strings.Mid(strRef, 3, intpos - 3));
                                if (!Information.IsNumeric(strTemp))
                                {
                                    return ParseBookPage;
                                }
                            }
                        }
                        else
                        {
                            return ParseBookPage;
                        }
                    }
                    lngBook = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
                    if (lngBook == 0) return ParseBookPage;

                    // we have the book, now parse the page
                    if (strRef.Length <= intpos) return ParseBookPage;

                    // there is at least one char after the p
                    strTemp = Strings.Trim(Strings.Mid(strRef, intpos + 1));
                    if (strTemp == string.Empty) return ParseBookPage;

                    if (Strings.UCase(Strings.Mid(strTemp, 1, 1)) == "G")
                    {
                        if (strTemp.Length > 1)
                        {
                            strTemp = Strings.Trim(Strings.Mid(strTemp, 2));
                            if (strTemp == string.Empty) return ParseBookPage;
                        }
                        else
                        {
                            return ParseBookPage;
                        }
                    }

                    if (Information.IsNumeric(strTemp))
                    {
                        lngPage = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
                        if (lngPage > 0)
                        {
                            ParseBookPage = boolBook ? lngBook.ToString() : lngPage.ToString();
                        }
                        else
                        {
                            return ParseBookPage;
                        }
                        // for now don't try to convert a date
                        // nothing left of the ref to parse
                        strRef = "";
                        return ParseBookPage;
                    }

                    // must try to parse it more
                    strTemp2 = "";
                    for (x = 1; x <= strTemp.Length; x++)
                    {
                        if (Information.IsNumeric(Strings.Mid(strTemp, x, 1)))
                        {
                            strTemp2 += Strings.Mid(strTemp, x, 1);
                        }
                        else
                        {
                            break;
                        }
                    }
                    // x
                    if (Information.IsNumeric(strTemp2))
                    {
                        lngPage = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp2)));
                        if (lngPage <= 0)
                            return ParseBookPage;
                        if (boolBook)
                        {
                            ParseBookPage = lngBook.ToString();
                        }
                        else
                        {
                            ParseBookPage = lngPage.ToString();
                        }
                        strRef = Strings.Trim(Strings.Mid(strRef, intpos + 1));
                        if (Strings.UCase(Strings.Mid(strRef, 1, 1)) == "G")
                        {
                            strRef = Strings.Trim(Strings.Mid(strRef, 2));
                        }
                        if (strRef.Length <= strTemp2.Length) return ParseBookPage;

                        strRef = Strings.Mid(strRef, strTemp2.Length + 1);
                        strRef = Strings.Trim(strRef);
                        if (strRef == string.Empty) return ParseBookPage;
                        // now check to see if it might be a valid date
                        if (strRef.Length < 4) return ParseBookPage;

                        strTemp = strRef.Length > 6 ? Strings.Mid(strRef, 1, 7) : strRef;

                        intpos = Strings.InStr(1, strTemp, "/", CompareConstants.vbTextCompare);
                        if (intpos < 1)
                        {
                            intpos = Strings.InStr(1, strTemp, "\\", CompareConstants.vbTextCompare);
                            if (intpos < 1) return ParseBookPage;
                        }
                        if (intpos == 1)
                            return ParseBookPage;
                        if (intpos == strTemp.Length) return ParseBookPage;

                        strTemp2 = Strings.Mid(strTemp, 1, intpos - 1);
                        if (!Information.IsNumeric(strTemp2)) return ParseBookPage;
                        intMonth = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(strTemp, 1, intpos - 1))));
                        if (intMonth < 1 || intMonth > 12) return ParseBookPage;
                        strTemp2 = Strings.Mid(strTemp, intpos + 1);
                        if (!Information.IsNumeric(strTemp2)) return ParseBookPage;
                        lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp2)));
                        if (lngYear < 0)
                            return ParseBookPage;
                        if (lngYear < 100)
                        {
                            if (lngYear > 3)
                            {
                                lngYear = 1900 + lngYear;
                            }
                            else
                            {
                                lngYear = 2000 + lngYear;
                            }
                        }
                        else
                        {
                            if (lngYear < 1920 || lngYear > 2003)
                                return ParseBookPage;
                        }
                        dtBPDate = FCConvert.ToDateTime(FCConvert.ToString(intMonth) + "/1/" + FCConvert.ToString(lngYear));
                    }
                    return ParseBookPage;
                }
                else
                {
                    return ParseBookPage;
                }
                if (boolBook)
                {
                    ParseBookPage = lngBook.ToString();
                }
                else
                {
                    ParseBookPage = lngPage.ToString();
                }
                return ParseBookPage;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                frmWait.InstancePtr.Unload();
                ParseBookPage = "-1";
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Parsing Book Page");
            }
            return ParseBookPage;
        }

        public static bool EmailFirstAmerican(string strAttachmentPath, string strTownEmail, bool blnShowEmail = false, int lngTownCode = 0)
        {
            bool EmailFirstAmerican = false;
            try
            {
                // On Error GoTo ERROR_HANDLER
                string strEmail = "";
                int lngAttach/*unused?*/;
                string strTownName = "";
                clsDRWrapper rsTown = new clsDRWrapper();
                rsTown.OpenRecordset("SELECT TaxServiceEmail FROM Collections", modExtraModules.strCLDatabase);
                if (!rsTown.BeginningOfFile() && !rsTown.EndOfFile())
                {
                    strEmail = Strings.Trim(FCConvert.ToString(rsTown.Get_Fields_String("TaxServiceEMail")));
                }
                else
                {
                    strEmail = modGlobal.gstrDefTaxServiceEmail;
                    // kk01162017 trocl-1326  Keep defaults
                }

                if (modGlobal.Statics.gboolMultipleTowns && lngTownCode > 0)
                {
                    rsTown.OpenRecordset("SELECT * FROM tblRegions WHERE TownNumber = " + FCConvert.ToString(lngTownCode), "CentralData");
                    if (rsTown.RecordCount() > 0)
                    {
                        strTownName = FCConvert.ToString(rsTown.Get_Fields_String("TownName"));
                    }
                    else
                    {
                        strTownName = modGlobalConstants.Statics.MuniName;
                    }
                }
                else
                {
                    strTownName = modGlobalConstants.Statics.MuniName;
                }
                // MAL@20080708: Change E-Mail subject line ; Tracker Reference: 13727
                // MAL@20081203: Change Subject line to use Town Name ; Tracker Reference: 15673
                if (blnShowEmail)
                    frmEMail.InstancePtr.Init(strAttachmentPath, strEmail, strTownName + " Billing File Export", "Email of " + strTownName + " export file.  " + Strings.Format(DateTime.Now, "MM/dd/yyyy hh:mm tt"), true, true, false, true, showAsModalForm: true);
                else
                    frmEMail.InstancePtr.Init(strAttachmentPath, strEmail, strTownName + " Billing File Export", "Email of " + strTownName + " export file.  " + Strings.Format(DateTime.Now, "MM/dd/yyyy hh:mm tt"), true, true, false, true, true, showAsModalForm: true);

                return EmailFirstAmerican;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                frmWait.InstancePtr.Unload();
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Emailing CoreLogic");
                // kgk trocl-767  Change "1st American" to "CoreLogic" and update menu in frmCLGetAccount
            }
            return EmailFirstAmerican;
        }

        public class StaticVariables
        {
            // ********************************************************
            // LAST UPDATED   :               11/22/2006              *
            // MODIFIED BY    :               Jim Bertolino           *
            // *
            // DATE           :               09/09/2003              *
            // WRITTEN BY     :               Jim Bertolino           *
            // *
            // PROPERTY OF TRIO SOFTWARE CORPORATION                  *
            // ********************************************************
            //=========================================================
            public int intPeriod;
            public int lngLastDelAccount;
            public DateTime dtTSDate;
            public bool boolErrorPrintingToFile;
        }

        public static StaticVariables Statics
        {
            get
            {
                return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
            }
        }
    }
}
