﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class arWaiverOfForeclosure : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               11/26/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/26/2004              *
		// ********************************************************
		string strMainText;
		string strTreasurerName = "";
		DateTime dtPaymentDate;
		string strOwnerName = "";
		DateTime dtLienDate;
		string strBook = "";
		string strPage = "";
		string strCounty = "";
		string strHisHer = "";
		string strMuniName = "";
		int lngLineLen;
		string strSignerDesignation = "";
		string strSignerName = "";
		DateTime dtCommissionExpiration;
		string strTitle = "";
		DateTime dtTreasSignDate;
		bool boolUseAbatementLine;
		DateTime dtAppearedDate;

		public arWaiverOfForeclosure()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Lien Discharge Notice";
		}

		public static arWaiverOfForeclosure InstancePtr
		{
			get
			{
				return (arWaiverOfForeclosure)Sys.GetInstance(typeof(arWaiverOfForeclosure));
			}
		}

		protected arWaiverOfForeclosure _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void SetStrings()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				//clsDRWrapper rsAbate = new clsDRWrapper();
				string strAppearedDateText = "";
				lngLineLen = 35;
				// this is how many underscores are in the printed lines
				if (dtAppearedDate.ToOADate() != 0)
				{
					strAppearedDateText = Strings.Format(dtAppearedDate, "MMMM dd, yyyy");
				}
				else
				{
					// if there is no treasurer signing date, then use the current date
					strAppearedDateText = Strings.StrDup(15, "_");
				}
				if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
				{
					lblTownHeader.Text = modGlobalConstants.Statics.gstrCityTown + " of " + strMuniName;
				}
				else
				{
					lblTownHeader.Text = strMuniName;
				}
				fldMuni.Text = lblTownHeader.Text;
				lblTitleBar.Text = "WAIVER OF FORECLOSURE FORM";
				// lblLegalDescription.Text = "Title 36, M.R.S.A. Section 943"
				strMainText = "The foreclosure of the tax lien mortgage on real estate for a tax assessed against " + strOwnerName;
				if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
				{
					strMainText += " to " + modGlobalConstants.Statics.gstrCityTown + " of " + strMuniName + " dated " + FCConvert.ToString(dtLienDate) + " and recorded in ";
				}
				else
				{
					strMainText += " to " + strMuniName + " dated " + FCConvert.ToString(dtLienDate) + " and recorded in ";
				}
				strMainText += strCounty + " County Registry of Deeds in Book " + strBook + " and Page " + strPage + " is hereby waived.";
				fldMainText.Text = strMainText;
				fldTopDate.Text = "";
				// "Dated: " & Format(dtPaymentDate, "MMMM dd, yyyy")
				fldSigLine.Text = Strings.StrDup(lngLineLen, "_");
				fldSigTitle.Text = "";
				fldSigName.Text = strTreasurerName + ", " + strTitle;
				fldSigDate.Text = "Dated: " + Strings.Format(dtTreasSignDate, "MMMM dd, yyyy");
				fldACKNOWLEDGEMENT.Text = "ACKNOWLEDGEMENT";
				strMainText = strMuniName + "\r\n";
				strMainText += "State of Maine" + "\r\n" + strCounty + " County, ss." + "\r\n" + "\r\n";
				strMainText += "Personally appeared before me, on " + strAppearedDateText + ", the above-named " + strTreasurerName;
				strMainText += ", who acknowledged the foregoing to be ";
				strMainText += strHisHer + " free act and deed in " + strHisHer + " capacity as " + strTitle + ".";
				fldBottomText.Text = strMainText;
				fldNotaryLine.Text = Strings.StrDup(lngLineLen, "_");
				if (strSignerName != "" && strSignerDesignation != "")
				{
					fldNotaryTitle.Text = strSignerName + ", " + strSignerDesignation;
					// "Notary Public / Attorney at Law"
				}
				else if (strSignerName != "")
				{
					fldNotaryTitle.Text = strSignerName;
				}
				else
				{
					fldNotaryTitle.Text = strSignerDesignation;
				}
				if (dtCommissionExpiration.ToOADate() != 0)
				{
					fldNotaryCommissionTitle.Text = "My commission expires: " + Strings.Format(dtCommissionExpiration, "MMMM dd, yyyy");
				}
				else
				{
					fldNotaryCommissionTitle.Text = "My commission expires: ";
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Setting Strings");
			}
		}

		public void Init(string strPassTeasName, string strPassMuni, string strPassCounty, string strPassName1, string strPassName2, DateTime dtPassPayDate, DateTime dtPassLienDate, string strPassBook, string strPassPage, string strPassHisHer, string strPassSignerName, string strPassSignerDesignation, DateTime dtPassCommissionExpiration, bool modalDialog, string strPassMapLot = "", string strPassTitle = "Treasurer", int lngPassAccount = 0, DateTime? dtPassTreasSignDate = null/*= DateTime.Now*/, bool boolAbatementPayment = false, DateTime? dtPassAppearedDate = null/*= DateTime.Now*/)
		{
			if (!dtPassTreasSignDate.HasValue)
			{
				dtPassTreasSignDate = DateTime.Now;
			}
			if (!dtPassAppearedDate.HasValue)
			{
				dtPassAppearedDate = DateTime.Now;
			}
			// this routine will set all of the variables needed
			// Row - 1  'Treasurer Name
			strTreasurerName = strPassTeasName;
			// Row - 2  'Muni Name
			strMuniName = strPassMuni;
			// Row - 3  'County
			strCounty = strPassCounty;
			// Row - 4  'Name1
			strOwnerName = strPassName1;
			// Row - 5  'Name2
			if (Strings.Trim(strPassName2) != "")
			{
				strOwnerName += " " + strPassName2;
			}
			// Row - 6  'Payment Date
			dtPaymentDate = dtPassPayDate;
			// Row - 7  'Filing Date
			dtLienDate = dtPassLienDate;
			// treasurer signing date
			dtTreasSignDate = dtPassTreasSignDate.Value;
			// Row - 8  'Book
			strBook = strPassBook;
			if (Strings.Trim(strBook) == "" || Conversion.Val(strBook) == 0)
			{
				strBook = Strings.StrDup(10, "_");
			}
			// Row - 9  'Page
			strPage = strPassPage;
			if (Strings.Trim(strPage) == "" || Conversion.Val(strPage) == 0)
			{
				strPage = Strings.StrDup(10, "_");
			}
			// abatement used to discharge lien
			boolUseAbatementLine = boolAbatementPayment;
			// Row - 10 'His/Her
			strHisHer = Strings.LCase(strPassHisHer);
			if (Strings.Trim(strHisHer) == "")
			{
				strHisHer = "his/her";
			}
			// title
			strTitle = strPassTitle;
			// Row - 11 'Signer Name
			strSignerName = strPassSignerName;
			// Row - 12 'Signer's Designation
			strSignerDesignation = strPassSignerDesignation;
			// Row - 13 ' Commission expiration date
			dtCommissionExpiration = dtPassCommissionExpiration;
			lblMapLot.Text = strPassMapLot;
			// appeared date
			dtAppearedDate = dtPassAppearedDate.Value;
			// account number
			if (lngPassAccount > 0)
			{
				fldAccount.Text = modGlobal.PadToString(FCConvert.ToInt16(lngPassAccount), 5);
			}
			else
			{
				fldAccount.Text = "";
			}
			if (modGlobal.Statics.gboolUseSigFile)
			{
				imgSig.Visible = true;
				//imgSig.ZOrder(0);
				//fldSigLine.ZOrder(0);
				imgSig.Image = FCUtils.LoadPicture(modSignatureFile.Statics.gstrTreasSigPath);
			}
			else
			{
				imgSig.Visible = false;
			}
			frmReportViewer.InstancePtr.Init(this, "", 1, showModal: modalDialog);
			frmLienDischargeNotice.InstancePtr.Unload();
			// Me.Show vbModal, MDIParent
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// set the top margin
			this.PageSettings.Margins.Top = 1 + FCConvert.ToSingle(modGlobal.Statics.gdblLDNAdjustmentTop);
			this.PageSettings.Margins.Bottom = 1 + FCConvert.ToSingle(modGlobal.Statics.gdblLDNAdjustmentBottom);
			// kk04212014 trocl-1156
			SetStrings();
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
		}

		
	}
}
