﻿using System;
using fecherFoundation;
using Wisej.Web;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptCertificateOfRecommitment : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               02/10/2006              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/27/2007              *
		// ********************************************************
		// filled by information from the user
		string strCollectorName = "";
		// Dim strTreasurerName            As String
		string strMuniName;
		string strCounty = "";
		string strRegistryCounty;
		string strLegalDescription;
		string strTaxYear;
		// Dim strInterestRate             As String
		DateTime dtDateSigned;
		// Dim dtEndingDate                As Date
		string strMainText;
		int lngLineLen;
		double dblBalanceDue;
		double dblREPrinTotal;
		double dblPPPrinTotal;

		public rptCertificateOfRecommitment()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Certificate Of Recommitment";
		}

		public static rptCertificateOfRecommitment InstancePtr
		{
			get
			{
				return (rptCertificateOfRecommitment)Sys.GetInstance(typeof(rptCertificateOfRecommitment));
			}
		}

		protected rptCertificateOfRecommitment _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void SetStrings()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				lngLineLen = 35;
				// this is how many underscores are in the printed lines
				// report caption
				lblTitleBar.Text = "Certificate Of Recommitment";
				// legal description
				lblLegalDescription.Text = "36 M.R.S.A " + Strings.Chr(167) + " 763";
				// strLegalDescription
				// County
				strMainText = "COUNTY OF " + strCounty + " ss.";
				fldCounty.Text = strMainText;
				// State
				strMainText = "STATE OF MAINE";
				fldState.Text = strMainText;
				// 1st paragraph
				strMainText = "\r\n" + "TO: " + strCollectorName + ", Tax Collector of the Municipality of ";
				strMainText += modGlobalConstants.Statics.MuniName + "\r\n";
				fldMain1.Text = strMainText;
				strMainText = "\r\n" + "";
				// 2nd paragraph
				strMainText += "Herewith are committed to you true lists of the assessments of the Estates of the persons " + "\r\n" + "\r\n" + "therein named; ";
				strMainText += "you are to levy and collect the same, of each one his/her respective" + "\r\n" + "\r\n" + "amount, therein set down of the sum ";
				strMainText += "total of " + Strings.Format(dblREPrinTotal, "$#,##0.00") + " (being the yet uncollected" + "\r\n" + "\r\n" + "amount of the lists contained herein), ";
				strMainText += "according to the tenor of the foregoing warrant.";
				fldMain2.Text = strMainText;
				if (dtDateSigned.ToOADate() == 0)
				{
					lblDated1.Text = "Given under our hands this ____________ day of ____________________ 20____.";
				}
				else
				{
					lblDated1.Text = "Given under our hands this " + ReturnDayWithSuffix_2(FCConvert.ToInt16(Strings.Format(dtDateSigned, "dd"))) + " day of " + Strings.Format(dtDateSigned, "MMMM yyyy") + ".";
				}
				lblMunicipalOfficers.Text = "Municipal Officers";
				lblReportVersion.Text = "PTA 200.1 (05/00)";
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Setting Strings");
			}
		}

		public void Init(string strPassCollectorName, string strPassMuni, string strPassCounty, string strPassRegistryCounty, string strPassLegalDescription, string strPassTaxYear, DateTime dtPassDateSigned)
		{
			// , dtPassEndingDate As Date, strPassTreasurerName As String, strPassInterestRate As String)
			// this routine will set all of the variables needed
			// CollectorName
			strCollectorName = strPassCollectorName;
			// Treasurer Name
			// strTreasurerName = strPassTreasurerName
			// Muni Name
			strMuniName = strPassMuni;
			// County
			strCounty = strPassCounty;
			// County Of Registry
			strRegistryCounty = strPassRegistryCounty;
			// Legal Description
			strLegalDescription = strPassLegalDescription;
			// Tax Year
			strTaxYear = strPassTaxYear;
			// Interest Rate
			// strInterestRate = strPassInterestRate
			// Date Signed
			dtDateSigned = dtPassDateSigned;
			// Ending Date
			// dtEndingDate = dtPassEndingDate
			FillAmountFields();
			// Unload frmCertOfSettlement
			// Me.Show vbModal, MDIParent
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			SetStrings();
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
		}

		private void FillAmountFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this sub will calculate all of the amounts needed for this form
				// and fill the amounts into the correct fields
				// Line 1     -   Real Estate and Personal Property Principal *****************
				dblREPrinTotal = GetYearPrincipal();
				// This will put all of the values into the fields on the report
				// fldCommitment.Text = Format(dblREPrinTotal + dblPPPrinTotal, "$#,##0.00")
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In Calculations");
			}
		}

		private string ReturnDayWithSuffix_2(short intDay)
		{
			return ReturnDayWithSuffix(ref intDay);
		}

		private string ReturnDayWithSuffix(ref short intDay)
		{
			string ReturnDayWithSuffix = "";
			switch (intDay)
			{
				case 1:
				case 21:
				case 31:
					{
						ReturnDayWithSuffix = FCConvert.ToString(intDay) + "st";
						break;
					}
				case 2:
				case 22:
					{
						ReturnDayWithSuffix = FCConvert.ToString(intDay) + "nd";
						break;
					}
				case 3:
				case 23:
					{
						ReturnDayWithSuffix = FCConvert.ToString(intDay) + "rd";
						break;
					}
				default:
					{
						ReturnDayWithSuffix = FCConvert.ToString(intDay) + "th";
						break;
					}
			}
			//end switch
			return ReturnDayWithSuffix;
		}

		private double GetYearPrincipal()
		{
			double GetYearPrincipal = 0;
			var rsTemp = new clsDRWrapper();
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will return the total principal committed during this tax year
                
                GetYearPrincipal = 0;
                rsTemp.OpenRecordset(
                    "SELECT SUM(Principal) AS TotalPrin FROM (SELECT SUM(TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4 - ROUND(PrincipalPaid,2)) AS Principal FROM BillingMaster WHERE  LienRecordNumber = 0 AND RateKey <> 0 AND (BillingType = 'PP' OR BillingType = 'RE')) As q1",
                    modExtraModules.strCLDatabase);
                if (!rsTemp.EndOfFile())
                {
                    // TODO Get_Fields: Field [TotalPrin] not found!! (maybe it is an alias?)
                    // TODO Get_Fields: Field [TotalPrin] not found!! (maybe it is an alias?)
                    if (Conversion.Val(rsTemp.Get_Fields("TotalPrin")) == 0)
                    {
                        GetYearPrincipal = 0;
                    }
                    else
                    {
                        // TODO Get_Fields: Field [TotalPrin] not found!! (maybe it is an alias?)
                        // TODO Get_Fields: Field [TotalPrin] not found!! (maybe it is an alias?)
                        GetYearPrincipal = Conversion.Val(rsTemp.Get_Fields("TotalPrin"));
                    }

                    // rsTemp.OpenRecordset "SELECT SUM(Principal) AS TotalPrin FROM (SELECT SUM(PrincipalPaid - TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4) AS Principal FROM BillingMaster WHERE LienRecordNumber = 0 AND RateKey <> 0 AND PrincipalPaid > (TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4) AND (BillingType = 'PP' OR BillingType = 'RE'))", strCLDatabase
                    // If Not rsTemp.EndOfFile Then
                    // If Val(rsTemp.Fields("TotalPrin")) <> 0 Then
                    // GetYearPrincipal = GetYearPrincipal - rsTemp.Fields("TotalPrin")
                    // End If
                    // End If
                }
                else
                {
                    return GetYearPrincipal;
                }

                return GetYearPrincipal;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                frmWait.InstancePtr.Unload();
                FCMessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message +
                    ".", MsgBoxStyle.Critical, "Error Getting Principal");
            }
            finally
            {
				rsTemp.Dispose();
            }
			return GetYearPrincipal;
		}

		
	}
}
