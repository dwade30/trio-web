using fecherFoundation;
using System;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptPmtActivity : BaseSectionReport
	{
        private PmtActivityReportConfiguration reportConfiguration;
        private PmtActivityReportOption reportOption;
        
        string strSQL;
		clsDRWrapper rsData = new clsDRWrapper();
        clsDRWrapper rsRegion = new clsDRWrapper();

        int count = 0;
        int regularCount = 0;
        int lienCount = 0;
        double dblTotalPrincipal = 0;
        double dblTotalInterest = 0;
        double dblTotalPLI = 0;
        double dblTotalCost = 0;
        double dblTotalRegularPrincipal = 0;
        double dblTotalRegularInterest = 0;
        double dblTotalRegularPLI = 0;
        double dblTotalRegularCost = 0;
        double dblTotalLienPrincipal = 0;
        double dblTotalLienInterest = 0;
        double dblTotalLienPLI = 0;
        double dblTotalLienCost = 0;
        bool showRegular;
        bool showLien;

        int lngCount;
        bool boolStarted;
		int lngLastAccount;
		int lngTAType;
		
        
		public rptPmtActivity()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}
               
        private void InitializeComponentEx()
		{
			this.Name = "Payment Activity Report";
		}

        public void Init(ref PmtActivityReportConfiguration reportConfig)
        {
            reportConfiguration = reportConfig;
        }

        public static rptPmtActivity InstancePtr
		{
			get
			{
				return (rptPmtActivity)Sys.GetInstance(typeof(rptPmtActivity));
			}
		}
		
		protected override void Dispose(bool disposing)
		{
            if (disposing)
            {
                rsData.Dispose();
                rsRegion.Dispose();
            }
            base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsData.EndOfFile())
			{
				if (boolStarted)
				{
					eArgs.EOF = true;
				}
				else
				{
				}
			}
			else
			{
				eArgs.EOF = false;
			}
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			
		}

		private void ActiveReport_PageEnd(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "t");
				lblMuniName.Text = modGlobalConstants.Statics.MuniName;
				lngCount = 0;
				
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading");
				SetupFields();
				SetReportHeader();
				boolStarted = false;
				strSQL = BuildSQL();
                
                rsData.OpenRecordset(strSQL, modGlobal.DEFAULTDATABASE);
                return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Starting Report");
			}
		}

		private string BuildSQL()
		{
			return reportConfiguration.SQLStatement;
        }
     
		private void SetupFields()
		{
			int intRow;
			int intRow2/*unused?*/;
			float lngHt;
			
		}

		private void BindFields()
		{
			try
			{
                // On Error GoTo ERROR_HANDLER
                // this will fill the information into the fields
                bool lienedBill = false;
                double dblPrincipal;
                double dblPLI;
                double dblCost;
                double dblInterest;
                double dblTax;
                
                TRYAGAIN:
				
                fldReceipt.Text = "";
                fldTotal.Text = "";
				fldReceipt.Text = "";
				fldCost.Text = "";
				fldAccount.Text = "";
				fldYear.Text = "";
    
                if (rsData.EndOfFile())
				{
					return;
				}

               
                lngCount += 1;
                fldDate.Text = FCConvert.ToString(rsData.Get_Fields_DateTime("RecordedTransactionDate")) ;
                fldAccount.Text = rsData.Get_Fields_String("Account");
				fldYear.Text = FCConvert.ToString(FCUtils.iDiv(FCConvert.ToInt32(rsData.Get_Fields_Int32("Year")), 10));
                if (rsData.Get_Fields_String("BillCode") == "L")
                {
                    fldYear.Text = fldYear.Text + "*";
                    lienedBill = true;
                }
                else
                {
                    lienedBill = false;
                }

                fldRef.Text = rsData.Get_Fields_String("Reference");
                fldCode.Text = rsData.Get_Fields_String("Code");
                //fldReceipt.Text = FCConvert.ToString(rsData.Get_Fields_Int32("ReceiptNumber"));
                if (FCConvert.ToString(rsData.Get_Fields_Int32("ReceiptNumber")) != "" &&
                     (rsData.Get_Fields_Int32("ReceiptNumber")) > 0)
                {
                    fldReceipt.Text = GetActualReceiptNumber(rsData.Get_Fields_Int32("ReceiptNumber"),
                        rsData.Get_Fields_DateTime("ActualSystemDate"));
                }

                //Payment
                dblPrincipal =  Conversion.Val(rsData.Get_Fields_Decimal("Principal"));
                dblInterest = Conversion.Val(rsData.Get_Fields_Decimal("CurrentInterest"));
                dblPLI = Conversion.Val(rsData.Get_Fields_Decimal("PreLienInterest"));
                dblCost = Conversion.Val(rsData.Get_Fields_Decimal("LienCost"));

                //show the payment values
                fldPrincipal.Text = Strings.Format(dblPrincipal, "#,##0.00");
                fldInterest.Text = Strings.Format(dblInterest, "#,##0.00");
                fldPLI.Text = Strings.Format(dblPLI, "#,##0.00");
                fldCost.Text = Strings.Format(dblCost, "#,##0.00");
                fldTotal.Text = Strings.Format(dblPrincipal + dblInterest + dblPLI + dblCost, "#,##0.00");

                //update running totals
                if (!lienedBill)
                {
                    showRegular = true;
                    dblTotalRegularPrincipal += dblPrincipal;
                    dblTotalRegularInterest += dblInterest;
                    dblTotalRegularPLI += dblPLI;
                    dblTotalRegularCost += dblCost;
                    regularCount += 1;
                }
                else
                {
                    showLien = true;
                    dblTotalLienPrincipal += dblPrincipal;
                    dblTotalLienInterest += dblInterest;
                    dblTotalLienPLI += dblPLI;
                    dblTotalLienCost += dblCost;
                    lienCount += 1;
                }

                dblTotalPrincipal += dblPrincipal;
                dblTotalInterest += dblInterest;
                dblTotalPLI += dblPLI;
                dblTotalCost += dblCost;
                count += 1;

                rsData.MoveNext();
                
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				if (Information.Err(ex).Number < 0 && Strings.Left(ex.GetBaseException().Message, 10) == "Automation")
				{
					// let it run
				}
				else
				{
					FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In BindFields");
				}
				/*? Resume; */
			}
		}
       
        private string GetActualReceiptNumber( int lngRN,  DateTime dtDate)
        {
            string GetActualReceiptNumber = "";
            
            clsDRWrapper rsRN = new clsDRWrapper();

            try
            {

                if (lngRN != 0)
                {
                    if (modGlobalConstants.Statics.gboolCR)
                    {
                        rsRN.OpenRecordset("SELECT * FROM Receipt WHERE ReceiptKey = " + FCConvert.ToString(lngRN) + " AND ReceiptDate > '" + Strings.Format(dtDate, "MM/dd/yyyy") + " 12:00:00 AM' AND ReceiptDate < '" + Strings.Format(dtDate, "MM/dd/yyyy") + " 11:59:59 PM'", modExtraModules.strCRDatabase);

                        if (rsRN.EndOfFile() != true && rsRN.BeginningOfFile() != true)
                        {
                            GetActualReceiptNumber = FCConvert.ToString(rsRN.Get_Fields_Int32("ReceiptNumber"));
                        }
                        else
                        {
                            rsRN.OpenRecordset("SELECT * FROM LastYearReceipt WHERE ReceiptKey = " + FCConvert.ToString(lngRN) + " AND LastYearReceiptDate > '" + Strings.Format(dtDate, "MM/dd/yyyy") + " 12:00:00 AM' AND LastYearReceiptDate < '" + Strings.Format(dtDate, "MM/dd/yyyy") + " 11:59:59 PM'", modExtraModules.strCRDatabase);

                            if (rsRN.EndOfFile() != true && rsRN.BeginningOfFile() != true)
                            {
                                GetActualReceiptNumber = FCConvert.ToString(rsRN.Get_Fields_Int32("ReceiptNumber"));
                            }
                            else
                            {
                                GetActualReceiptNumber = FCConvert.ToString(lngRN) + "*";
                            }
                        }
                    }
                    else
                    {
                        GetActualReceiptNumber = FCConvert.ToString(lngRN);
                    }
                }
                else
                {
                    GetActualReceiptNumber = FCConvert.ToString(0);
                }

                return GetActualReceiptNumber;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                FCMessageBox.Show("Error #"
                                  + FCConvert.ToString(Information.Err(ex)
                                                                  .Number)
                                  + " - "
                                  + ex.GetBaseException()
                                      .Message
                                  + ".", MsgBoxStyle.Critical, "Error Returning Receipt Number");
            }
            finally
            {
                rsRN.DisposeOf();
            }
            return GetActualReceiptNumber;
        }

private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void SetupTotals()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this sub will fill in the totals line at the bottom of the report
				int lastY;
				int delta;
				fldTotalPrincipal.Text = Strings.Format(dblTotalPrincipal, "#,##0.00");
                fldTotalInterest.Text = Strings.Format(dblTotalInterest, "#,##0.00");
                fldTotalPLI.Text = Strings.Format(dblTotalPLI, "#,##0.00");
				fldTotalCost.Text = Strings.Format(dblTotalCost, "#,##0.00");
				fldTotalTotal.Text = Strings.Format(dblTotalPrincipal + dblTotalInterest + dblTotalPLI + dblTotalCost, "#,##0.00");
				fldTotalCount.Text = Strings.Format(count, "#,##0");

                lblRegTotal.Visible = showRegular;
                fldRegularPrincipal.Visible = showRegular;
                fldRegularInterest.Visible = showRegular;
                fldRegularPLI.Visible = showRegular;
                fldRegularCost.Visible = showRegular;
                fldRegularTotal.Visible = showRegular;
                fldRegularCount.Visible = showRegular;

                lblLienTotal.Visible = showLien;
                fldLienPrincipal.Visible = showLien;
                fldLienInterest.Visible = showLien;
                fldLienPLI.Visible = showLien;
                fldLienCost.Visible = showLien;
                fldLienTotal.Visible = showLien;
                fldLienCount.Visible = showLien;

                lastY = (int)lblRegTotal.Top;
                delta = (int)lblLienTotal.Top - (int)lblRegTotal.Top;

                if (showRegular)
                {
                    lastY += delta;
                    fldRegularPrincipal.Text = Strings.Format(dblTotalRegularPrincipal, "#,##0.00");
                    fldRegularInterest.Text = Strings.Format(dblTotalRegularInterest, "#,##0.00");
                    fldRegularPLI.Text = Strings.Format(dblTotalRegularPLI, "#,##0.00");
                    fldRegularCost.Text = Strings.Format(dblTotalRegularCost, "#,##0.00");
                    fldRegularTotal.Text = Strings.Format(dblTotalRegularPrincipal + dblTotalRegularInterest + dblTotalRegularPLI + dblTotalRegularCost, "#,##0.00");
                    fldRegularCount.Text = Strings.Format(regularCount, "#,##0");
                }

                if (showLien)
                {                    
                    fldLienPrincipal.Text = Strings.Format(dblTotalLienPrincipal, "#,##0.00");
                    fldLienInterest.Text = Strings.Format(dblTotalLienInterest, "#,##0.00");
                    fldLienPLI.Text = Strings.Format(dblTotalLienPLI, "#,##0.00");
                    fldLienCost.Text = Strings.Format(dblTotalLienCost, "#,##0.00");
                    fldLienTotal.Text = Strings.Format(dblTotalLienPrincipal + dblTotalLienInterest + dblTotalLienPLI + dblTotalLienCost, "#,##0.00");
                    fldLienCount.Text = Strings.Format(lienCount, "#,##0");
                }
                return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Summary Creation");
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			SetupTotals();
		}

		private void SetReportHeader()
		{
			string strTemp = "";
            string strSeparator = "";
            var filter = reportConfiguration.Filter;
            var options = reportConfiguration.Options;

            fldReceipt.Visible = modGlobalConstants.Statics.gboolCR;

            if (filter.AccountRangeUsed())
            {
                if (filter.AccountMax.HasValue)
                {
                    if (filter.AccountMin.HasValue)
                    {
                        if (filter.AccountMin == filter.AccountMax)
                        {
                            strTemp += "Account: " + filter.AccountMin.Value.ToString();
                        }
                        else
                        {
                            strTemp += "Account: " + filter.AccountMin.Value.ToString() + " To " +
                                       filter.AccountMax.Value.ToString();
                        }
                    }
                    else
                    {
                        strTemp += "Below Account: " + filter.AccountMax.Value.ToString();
                    }
                }
                else
                {
                    strTemp += "Above Account: " + filter.AccountMin.Value.ToString();
                }

                strSeparator = ",";
            }

           if (filter.TaxYearMin.HasValue || filter.TaxYearMax.HasValue)
            {
                strTemp += strSeparator;
                if (filter.TaxYearMin.HasValue)
                {
                    if (filter.TaxYearMax.HasValue)
                    {
                        strTemp += " Tax Year: " + filter.TaxYearMin.ToString() + " To " + filter.TaxYearMax.ToString();
                    }
                    else
                    {
                        strTemp += " Tax Year: " + filter.TaxYearMin.ToString();
                    }
                }
                else
                {
                    strTemp += " Tax Year: " + filter.TaxYearMax.ToString();
                }
                strSeparator = ",";
            }

            if (filter.PaymentDateMin.HasValue && filter.PaymentDateMax.HasValue)
            {
                strTemp += strSeparator;
                strTemp += " Payment Range: " + filter.PaymentDateMin.Value.ToShortDateString() + " to " +
                           filter.PaymentDateMax.Value.ToShortDateString();
                strSeparator = ",";
            }

            if (options.PaymentTypes() != "")
            {
                strTemp += strSeparator + " Types: " + options.PaymentTypes().Replace("'","") + " ";
                strSeparator = ",";                
            }

            if (!string.IsNullOrWhiteSpace(filter.Reference))
            {
                strTemp += strSeparator + " Ref: " + filter.Reference + " ";
                strSeparator = ",";
            }

            if (!string.IsNullOrWhiteSpace(filter.TaxBillStatus))
            {
                strTemp += strSeparator;
                if (filter.TaxBillStatus == "R")
                {
                    strTemp += " Regular";
                }
                else
                {
                    strTemp += " Lien";
                }                
            }

            if (modGlobal.Statics.gboolMultipleTowns && filter.TownCode > 0)
            {
                rsRegion.OpenRecordset("Select * From tblRegions", "CentralData");
                if (rsRegion.RecordCount() > 0)
                {
                    strTemp += strSeparator + " Town: " + rsRegion.Get_Fields_String("TownName");
                }                
            }

            lblReportType.Text = Strings.Trim(strTemp) == "" ? "Complete List" : strTemp;

            strTemp = reportConfiguration.SortListing;
            if (string.IsNullOrEmpty(strTemp)) strTemp = "Order By: Transaction Date";
            lblReportType.Text = lblReportType.Text + "\r\n" + strTemp;
        }
	}
}