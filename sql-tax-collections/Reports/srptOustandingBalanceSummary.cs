using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using fecherFoundation;
using GrapeCity.ActiveReports.SectionReportModel;
using SharedApplication.Extensions;
using TWCL0000.Reports;

namespace TWCL0000
{
    /// <summary>
    /// Summary description for srptOutstandingBalanceSummary.
    /// </summary>
    public partial class srptOutstandingBalanceSummary : FCSectionReport
    {
        private IEnumerator<OutstandingBalanceSummaryItem> reportEnumerator;
        private decimal totalCount = 0;
        private decimal totalTotal = 0;
        public srptOutstandingBalanceSummary()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            this.ReportStart += srptOutstandingBalanceSummary_ReportStart;
            this.FetchData += srptOutstandingBalanceSummary_FetchData;
            this.Detail.Format += Detail_Format;
            this.ReportEnd += SrptOutstandingBalanceSummary_ReportEnd;
        }

        private void SrptOutstandingBalanceSummary_ReportEnd(object sender, EventArgs e)
        {
            textBox2.Text = totalCount.ToString();
            textBox1.Text = totalTotal.FormatAsCurrencyNoSymbol();
        }

        private void Detail_Format(object sender, EventArgs e)
        {
            if (reportEnumerator.Current != null)
            {
                var summary = reportEnumerator.Current;
                lblSummary1.Text = summary.Description;
                fldSumCount1.Text = summary.Count.ToString();
                fldSummary1.Text = summary.Total.FormatAsCurrencyNoSymbol();
                totalTotal += summary.Total;
                totalCount += summary.Count;
            }
        }

        private void srptOutstandingBalanceSummary_FetchData(object sender, FetchEventArgs eArgs)
        {
            eArgs.EOF = !reportEnumerator.MoveNext();
        }

        private void srptOutstandingBalanceSummary_ReportStart(object sender, EventArgs e)
        {
            var reportList = (IEnumerable<OutstandingBalanceSummaryItem>)this.UserData;
            reportEnumerator = reportList.GetEnumerator();
        }      
    }
}
