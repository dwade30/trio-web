﻿namespace TWCL0000
{
    public class TaxServiceSummarySummaryItem
    {
        public int Year { get; set; } = 0;
        public int Count { get; set; } = 0;
        public decimal Balance { get; set; } = 0;
        public decimal Interest { get; set; } = 0;
    }
}