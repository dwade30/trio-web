﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptDemandFeeList : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// Date           :               01/25/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// Last Updated   :               04/15/2004              *
		// ********************************************************
		int lngTotalAccounts;
		int lngCount;
		// this will keep track of which row I am on in the grid
		double dblDemandFee;
		bool boolDone;
		double dblTotalDemand;
		double dblTotalCMF;
		double dblTotalPrin;
		bool boolReverse;

		public rptDemandFeeList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Demand Fees List";
		}

		public static rptDemandFeeList InstancePtr
		{
			get
			{
				return (rptDemandFeeList)Sys.GetInstance(typeof(rptDemandFeeList));
			}
		}

		protected rptDemandFeeList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public void Init(int lngPassTotalAccounts, double dblPassDemand, bool boolPassReverse = false)
		{
			lngTotalAccounts = lngPassTotalAccounts;
			dblDemandFee = dblPassDemand;
			boolReverse = boolPassReverse;
			// Me.Show
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (boolDone)
			{
				// If Not arrDemand(lngCount).Used And boolDone Then
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
			}
			//Detail_Format();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			// save this report for future access
			if (this.Document.Pages.Count > 0)
			{
				if (boolReverse)
				{
					modGlobalFunctions.IncrementSavedReports("LastCLReverseDemand");
					this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastCLReverseDemand1.RDF"));
				}
				else
				{
					modGlobalFunctions.IncrementSavedReports("LastCLDemandFeesList");
					this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastCLDemandFeesList1.RDF"));
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "t");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			if (boolReverse)
			{
				lblHeader.Text = "Reverse Demand Fees List";
			}
			else
			{
				lblHeader.Text = "Demand Fees List";
			}
			lngCount = 0;
		}

		private void BindFields()
		{
			// this will fill the information into the fields
			if (!boolDone)
			{
				// MAL@20080826: Changed to only run this check for reversals
				// Tracker Reference: 15084
				// this will find the next available account that was processed
				// If boolReverse Then
				while (!(modGlobal.Statics.arrDemand[lngCount].Processed || lngCount >= Information.UBound(modGlobal.Statics.arrDemand, 1)))
				{
					lngCount += 1;
				}
				// End If
				if (lngCount == Information.UBound(modGlobal.Statics.arrDemand, 1))
				{
					if (!modGlobal.Statics.arrDemand[lngCount].Processed)
					{
						lngCount += 1;
					}
				}
				// check to see if we are at the end of the list
				if (lngCount > Information.UBound(modGlobal.Statics.arrDemand, 1))
				{
					boolDone = true;
					// clear the fields so there are no repeats
					fldAcct.Text = "";
					fldName.Text = "";
					fldDemand.Text = "";
					fldCMF.Text = "";
					fldTotal.Text = "";
					return;
				}
				fldAcct.Text = modGlobal.Statics.arrDemand[lngCount].Account.ToString();
				fldName.Text = modGlobal.Statics.arrDemand[lngCount].Name;
				fldDemand.Text = Strings.Format(modGlobal.Statics.arrDemand[lngCount].Fee, "#,##0.00");
				if (modGlobal.Statics.arrDemand[lngCount].CertifiedMailFee != 0)
				{
					fldCMF.Text = Strings.Format(modGlobal.Statics.arrDemand[lngCount].CertifiedMailFee - modGlobal.Statics.arrDemand[lngCount].Fee, "#,##0.00");
					fldTotal.Text = Strings.Format(modGlobal.Statics.arrDemand[lngCount].CertifiedMailFee, "#,##0.00");
					dblTotalCMF += modGlobal.Statics.arrDemand[lngCount].CertifiedMailFee - modGlobal.Statics.arrDemand[lngCount].Fee;
				}
				else
				{
					fldCMF.Text = "0.00";
					fldTotal.Text = fldDemand.Text;
				}
				dblTotalDemand += modGlobal.Statics.arrDemand[lngCount].Fee;
				// move to the next record in the array
				lngCount += 1;
			}
			else
			{
				// clear the fields
				fldAcct.Text = "";
				fldName.Text = "";
				fldDemand.Text = "";
				fldCMF.Text = "";
				fldTotal.Text = "";
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void SetupTotals()
		{
			// this sub will fill in the footer line at the bottom of the report
			lblNumberOfAccounts.Text = "There were " + FCConvert.ToString(lngTotalAccounts) + " accounts processed.";
			// lblTotalPrin.Text = Format(dblTotalPrin, "#,##0.00")
			lblTotalCMF.Text = Strings.Format(dblTotalCMF, "#,##0.00");
			lblTotalDemand.Text = Strings.Format(dblTotalDemand, "#,##0.00");
			lblTotalTotal.Text = Strings.Format(dblTotalCMF + dblTotalDemand, "#,##0.00");
			// + dblTotalPrin
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			SetupTotals();
		}

		
	}
}
