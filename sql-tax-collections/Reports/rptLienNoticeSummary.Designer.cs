﻿namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	partial class rptLienNoticeSummary
	{
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptLienNoticeSummary));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lnTotals = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.fldPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPLInt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCosts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCurrentInt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.lblFooterMessage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTotals = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldTotalPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalPLInt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalCosts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalCurrentInt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblReportType = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPrincipal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPLInt = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCosts = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCurrentInt = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPLInt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCosts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCurrentInt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFooterMessage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPLInt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalCosts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalCurrentInt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReportType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPLInt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCosts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCurrentInt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldAccount,
            this.fldAddress,
            this.fldName,
            this.lnTotals,
            this.fldPrincipal,
            this.fldPLInt,
            this.fldCosts,
            this.fldCurrentInt,
            this.fldTotal});
            this.Detail.Height = 0.4375F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // fldAccount
            // 
            this.fldAccount.Height = 0.1875F;
            this.fldAccount.Left = 0F;
            this.fldAccount.Name = "fldAccount";
            this.fldAccount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldAccount.Text = null;
            this.fldAccount.Top = 0F;
            this.fldAccount.Width = 0.6875F;
            // 
            // fldAddress
            // 
            this.fldAddress.Height = 0.25F;
            this.fldAddress.Left = 0.6875F;
            this.fldAddress.Name = "fldAddress";
            this.fldAddress.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
            this.fldAddress.Text = null;
            this.fldAddress.Top = 0.1875F;
            this.fldAddress.Width = 4.3125F;
            // 
            // fldName
            // 
            this.fldName.CanShrink = true;
            this.fldName.Height = 0.1875F;
            this.fldName.Left = 0.6875F;
            this.fldName.Name = "fldName";
            this.fldName.Style = "font-family: \'Tahoma\'; white-space: nowrap; ddo-char-set: 0";
            this.fldName.Text = null;
            this.fldName.Top = 0F;
            this.fldName.Width = 2F;
            // 
            // lnTotals
            // 
            this.lnTotals.Height = 0F;
            this.lnTotals.Left = 3F;
            this.lnTotals.LineWeight = 1F;
            this.lnTotals.Name = "lnTotals";
            this.lnTotals.Top = 0F;
            this.lnTotals.Visible = false;
            this.lnTotals.Width = 4F;
            this.lnTotals.X1 = 3F;
            this.lnTotals.X2 = 7F;
            this.lnTotals.Y1 = 0F;
            this.lnTotals.Y2 = 0F;
            // 
            // fldPrincipal
            // 
            this.fldPrincipal.Height = 0.1875F;
            this.fldPrincipal.Left = 2.7F;
            this.fldPrincipal.Name = "fldPrincipal";
            this.fldPrincipal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldPrincipal.Text = null;
            this.fldPrincipal.Top = 0F;
            this.fldPrincipal.Width = 0.925F;
            // 
            // fldPLInt
            // 
            this.fldPLInt.Height = 0.1875F;
            this.fldPLInt.Left = 3.625F;
            this.fldPLInt.Name = "fldPLInt";
            this.fldPLInt.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldPLInt.Text = null;
            this.fldPLInt.Top = 0F;
            this.fldPLInt.Width = 0.9375F;
            // 
            // fldCosts
            // 
            this.fldCosts.Height = 0.1875F;
            this.fldCosts.Left = 4.5625F;
            this.fldCosts.Name = "fldCosts";
            this.fldCosts.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldCosts.Text = null;
            this.fldCosts.Top = 0F;
            this.fldCosts.Width = 0.9375F;
            // 
            // fldCurrentInt
            // 
            this.fldCurrentInt.Height = 0.1875F;
            this.fldCurrentInt.Left = 5.5F;
            this.fldCurrentInt.Name = "fldCurrentInt";
            this.fldCurrentInt.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldCurrentInt.Text = null;
            this.fldCurrentInt.Top = 0F;
            this.fldCurrentInt.Width = 0.9375F;
            // 
            // fldTotal
            // 
            this.fldTotal.Height = 0.1875F;
            this.fldTotal.Left = 6.4375F;
            this.fldTotal.Name = "fldTotal";
            this.fldTotal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotal.Text = null;
            this.fldTotal.Top = 0F;
            this.fldTotal.Width = 1.0625F;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblFooterMessage,
            this.lblTotals,
            this.fldTotalPrincipal,
            this.fldTotalPLInt,
            this.fldTotalCosts,
            this.fldTotalCurrentInt,
            this.fldTotalTotal,
            this.Line2});
            this.ReportFooter.Height = 0.6875F;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
            // 
            // lblFooterMessage
            // 
            this.lblFooterMessage.Height = 0.3125F;
            this.lblFooterMessage.HyperLink = null;
            this.lblFooterMessage.Left = 1F;
            this.lblFooterMessage.Name = "lblFooterMessage";
            this.lblFooterMessage.Style = "font-family: \'Tahoma\'; text-align: left";
            this.lblFooterMessage.Text = null;
            this.lblFooterMessage.Top = 0.25F;
            this.lblFooterMessage.Width = 5.25F;
            // 
            // lblTotals
            // 
            this.lblTotals.Height = 0.1875F;
            this.lblTotals.HyperLink = null;
            this.lblTotals.Left = 0F;
            this.lblTotals.Name = "lblTotals";
            this.lblTotals.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblTotals.Text = "Total:";
            this.lblTotals.Top = 0F;
            this.lblTotals.Width = 2.6875F;
            // 
            // fldTotalPrincipal
            // 
            this.fldTotalPrincipal.Height = 0.1875F;
            this.fldTotalPrincipal.Left = 2.6875F;
            this.fldTotalPrincipal.Name = "fldTotalPrincipal";
            this.fldTotalPrincipal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalPrincipal.Text = "0.00";
            this.fldTotalPrincipal.Top = 0F;
            this.fldTotalPrincipal.Width = 0.9375F;
            // 
            // fldTotalPLInt
            // 
            this.fldTotalPLInt.Height = 0.1875F;
            this.fldTotalPLInt.Left = 3.625F;
            this.fldTotalPLInt.Name = "fldTotalPLInt";
            this.fldTotalPLInt.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalPLInt.Text = "0.00";
            this.fldTotalPLInt.Top = 0F;
            this.fldTotalPLInt.Width = 0.9375F;
            // 
            // fldTotalCosts
            // 
            this.fldTotalCosts.Height = 0.1875F;
            this.fldTotalCosts.Left = 4.5625F;
            this.fldTotalCosts.Name = "fldTotalCosts";
            this.fldTotalCosts.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalCosts.Text = "0.00";
            this.fldTotalCosts.Top = 0F;
            this.fldTotalCosts.Width = 0.9375F;
            // 
            // fldTotalCurrentInt
            // 
            this.fldTotalCurrentInt.Height = 0.1875F;
            this.fldTotalCurrentInt.Left = 5.5F;
            this.fldTotalCurrentInt.Name = "fldTotalCurrentInt";
            this.fldTotalCurrentInt.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalCurrentInt.Text = "0.00";
            this.fldTotalCurrentInt.Top = 0F;
            this.fldTotalCurrentInt.Width = 0.9375F;
            // 
            // fldTotalTotal
            // 
            this.fldTotalTotal.Height = 0.1875F;
            this.fldTotalTotal.Left = 6.4375F;
            this.fldTotalTotal.Name = "fldTotalTotal";
            this.fldTotalTotal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalTotal.Text = "0.00";
            this.fldTotalTotal.Top = 0F;
            this.fldTotalTotal.Width = 1.0625F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 2.1875F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0F;
            this.Line2.Width = 5.3125F;
            this.Line2.X1 = 2.1875F;
            this.Line2.X2 = 7.5F;
            this.Line2.Y1 = 0F;
            this.Line2.Y2 = 0F;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblHeader,
            this.lblMuniName,
            this.lblTime,
            this.lblDate,
            this.lblPage,
            this.lblAccount,
            this.Line1,
            this.lblReportType,
            this.lblName,
            this.lblPrincipal,
            this.lblPLInt,
            this.lblCosts,
            this.lblCurrentInt,
            this.lblTotal});
            this.PageHeader.Height = 0.90625F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            // 
            // lblHeader
            // 
            this.lblHeader.Height = 0.375F;
            this.lblHeader.HyperLink = null;
            this.lblHeader.Left = 0F;
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" +
    "o-char-set: 0";
            this.lblHeader.Text = "Summary Report";
            this.lblHeader.Top = 0F;
            this.lblHeader.Width = 7.5F;
            // 
            // lblMuniName
            // 
            this.lblMuniName.Height = 0.1875F;
            this.lblMuniName.HyperLink = null;
            this.lblMuniName.Left = 0F;
            this.lblMuniName.Name = "lblMuniName";
            this.lblMuniName.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
            this.lblMuniName.Text = null;
            this.lblMuniName.Top = 0F;
            this.lblMuniName.Width = 3F;
            // 
            // lblTime
            // 
            this.lblTime.Height = 0.1875F;
            this.lblTime.HyperLink = null;
            this.lblTime.Left = 0F;
            this.lblTime.Name = "lblTime";
            this.lblTime.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
            this.lblTime.Text = null;
            this.lblTime.Top = 0.1875F;
            this.lblTime.Width = 1.25F;
            // 
            // lblDate
            // 
            this.lblDate.Height = 0.1875F;
            this.lblDate.HyperLink = null;
            this.lblDate.Left = 6.25F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.lblDate.Text = null;
            this.lblDate.Top = 0F;
            this.lblDate.Width = 1.25F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1875F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 6.25F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.lblPage.Text = null;
            this.lblPage.Top = 0.1875F;
            this.lblPage.Width = 1.25F;
            // 
            // lblAccount
            // 
            this.lblAccount.Height = 0.1875F;
            this.lblAccount.HyperLink = null;
            this.lblAccount.Left = 0F;
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
            this.lblAccount.Text = "Account";
            this.lblAccount.Top = 0.6875F;
            this.lblAccount.Width = 0.6875F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.875F;
            this.Line1.Width = 7.5F;
            this.Line1.X1 = 7.5F;
            this.Line1.X2 = 0F;
            this.Line1.Y1 = 0.875F;
            this.Line1.Y2 = 0.875F;
            // 
            // lblReportType
            // 
            this.lblReportType.Height = 0.3375F;
            this.lblReportType.HyperLink = null;
            this.lblReportType.Left = 0F;
            this.lblReportType.Name = "lblReportType";
            this.lblReportType.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 0";
            this.lblReportType.Text = null;
            this.lblReportType.Top = 0.35F;
            this.lblReportType.Width = 7.5F;
            // 
            // lblName
            // 
            this.lblName.Height = 0.1875F;
            this.lblName.HyperLink = null;
            this.lblName.Left = 0.875F;
            this.lblName.Name = "lblName";
            this.lblName.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left; ddo-char-set: 0";
            this.lblName.Text = "Name";
            this.lblName.Top = 0.6875F;
            this.lblName.Width = 0.9375F;
            // 
            // lblPrincipal
            // 
            this.lblPrincipal.Height = 0.1875F;
            this.lblPrincipal.HyperLink = null;
            this.lblPrincipal.Left = 2.6875F;
            this.lblPrincipal.Name = "lblPrincipal";
            this.lblPrincipal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblPrincipal.Text = "Principal";
            this.lblPrincipal.Top = 0.6875F;
            this.lblPrincipal.Width = 0.9375F;
            // 
            // lblPLInt
            // 
            this.lblPLInt.Height = 0.1875F;
            this.lblPLInt.HyperLink = null;
            this.lblPLInt.Left = 3.625F;
            this.lblPLInt.Name = "lblPLInt";
            this.lblPLInt.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblPLInt.Text = "Interest";
            this.lblPLInt.Top = 0.6875F;
            this.lblPLInt.Width = 0.9375F;
            // 
            // lblCosts
            // 
            this.lblCosts.Height = 0.1875F;
            this.lblCosts.HyperLink = null;
            this.lblCosts.Left = 4.5625F;
            this.lblCosts.Name = "lblCosts";
            this.lblCosts.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblCosts.Text = "Costs";
            this.lblCosts.Top = 0.6875F;
            this.lblCosts.Width = 0.9375F;
            // 
            // lblCurrentInt
            // 
            this.lblCurrentInt.Height = 0.1875F;
            this.lblCurrentInt.HyperLink = null;
            this.lblCurrentInt.Left = 5.5F;
            this.lblCurrentInt.Name = "lblCurrentInt";
            this.lblCurrentInt.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblCurrentInt.Text = "Current Int";
            this.lblCurrentInt.Top = 0.6875F;
            this.lblCurrentInt.Width = 0.9375F;
            // 
            // lblTotal
            // 
            this.lblTotal.Height = 0.1875F;
            this.lblTotal.HyperLink = null;
            this.lblTotal.Left = 6.4375F;
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblTotal.Text = "Total";
            this.lblTotal.Top = 0.6875F;
            this.lblTotal.Width = 1.0625F;
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // rptLienNoticeSummary
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.25F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.25F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.5F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
            ((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPLInt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCosts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCurrentInt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFooterMessage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPLInt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalCosts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalCurrentInt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReportType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPLInt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCosts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCurrentInt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnTotals;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPLInt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCosts;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCurrentInt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFooterMessage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotals;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPLInt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCosts;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCurrentInt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReportType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPLInt;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCosts;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCurrentInt;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
