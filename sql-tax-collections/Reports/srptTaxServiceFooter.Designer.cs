namespace TWCL0000
{
    /// <summary>
    /// Summary description for srptTaxServiceFooter.
    /// </summary>
    partial class srptTaxServiceFooter
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(srptTaxServiceFooter));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.fldSumYear1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldSumCount1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldSumInterest1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldSumBalance1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.lblSumYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumCount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumBalance = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumInterest = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.lnSumTotal = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtTotalCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTotalBalance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.fldSumYear1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSumCount1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSumInterest1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSumBalance1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Height = 0F;
            this.pageHeader.Name = "pageHeader";
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldSumYear1,
            this.fldSumCount1,
            this.fldSumInterest1,
            this.fldSumBalance1});
            this.Detail.Height = 0.2187499F;
            this.Detail.Name = "Detail";
            // 
            // fldSumYear1
            // 
            this.fldSumYear1.Height = 0.1875F;
            this.fldSumYear1.Left = 1.437F;
            this.fldSumYear1.Name = "fldSumYear1";
            this.fldSumYear1.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldSumYear1.Text = null;
            this.fldSumYear1.Top = 0F;
            this.fldSumYear1.Width = 0.625F;
            // 
            // fldSumCount1
            // 
            this.fldSumCount1.Height = 0.1875F;
            this.fldSumCount1.Left = 2.062F;
            this.fldSumCount1.Name = "fldSumCount1";
            this.fldSumCount1.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldSumCount1.Text = null;
            this.fldSumCount1.Top = 0F;
            this.fldSumCount1.Width = 0.5F;
            // 
            // fldSumInterest1
            // 
            this.fldSumInterest1.Height = 0.1875F;
            this.fldSumInterest1.Left = 3.7495F;
            this.fldSumInterest1.Name = "fldSumInterest1";
            this.fldSumInterest1.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldSumInterest1.Text = null;
            this.fldSumInterest1.Top = 0F;
            this.fldSumInterest1.Width = 1.1875F;
            // 
            // fldSumBalance1
            // 
            this.fldSumBalance1.Height = 0.1875F;
            this.fldSumBalance1.Left = 2.562F;
            this.fldSumBalance1.Name = "fldSumBalance1";
            this.fldSumBalance1.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldSumBalance1.Text = null;
            this.fldSumBalance1.Top = 0F;
            this.fldSumBalance1.Width = 1.1875F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // reportHeader1
            // 
            this.reportHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblSumYear,
            this.lblSumCount,
            this.lblSumBalance,
            this.lblSumInterest,
            this.Line3});
            this.reportHeader1.Name = "reportHeader1";
            // 
            // lblSumYear
            // 
            this.lblSumYear.Height = 0.1875F;
            this.lblSumYear.HyperLink = null;
            this.lblSumYear.Left = 1.5625F;
            this.lblSumYear.Name = "lblSumYear";
            this.lblSumYear.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblSumYear.Text = "Year";
            this.lblSumYear.Top = 0.03125F;
            this.lblSumYear.Width = 0.5F;
            // 
            // lblSumCount
            // 
            this.lblSumCount.Height = 0.1875F;
            this.lblSumCount.HyperLink = null;
            this.lblSumCount.Left = 2.0625F;
            this.lblSumCount.Name = "lblSumCount";
            this.lblSumCount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblSumCount.Text = "Count";
            this.lblSumCount.Top = 0.03125F;
            this.lblSumCount.Width = 0.5F;
            // 
            // lblSumBalance
            // 
            this.lblSumBalance.Height = 0.1875F;
            this.lblSumBalance.HyperLink = null;
            this.lblSumBalance.Left = 2.5625F;
            this.lblSumBalance.Name = "lblSumBalance";
            this.lblSumBalance.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblSumBalance.Text = "Balance";
            this.lblSumBalance.Top = 0.03125F;
            this.lblSumBalance.Width = 1.1875F;
            // 
            // lblSumInterest
            // 
            this.lblSumInterest.Height = 0.1875F;
            this.lblSumInterest.HyperLink = null;
            this.lblSumInterest.Left = 3.75F;
            this.lblSumInterest.Name = "lblSumInterest";
            this.lblSumInterest.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblSumInterest.Text = "Interest";
            this.lblSumInterest.Top = 0.03125F;
            this.lblSumInterest.Width = 1.1875F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 1.4375F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0.21875F;
            this.Line3.Width = 3.625F;
            this.Line3.X1 = 1.4375F;
            this.Line3.X2 = 5.0625F;
            this.Line3.Y1 = 0.21875F;
            this.Line3.Y2 = 0.21875F;
            // 
            // reportFooter1
            // 
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lnSumTotal,
            this.txtTotalCount,
            this.txtTotalInterest,
            this.txtTotalBalance});
            this.reportFooter1.Height = 0.2916667F;
            this.reportFooter1.Name = "reportFooter1";
            // 
            // lnSumTotal
            // 
            this.lnSumTotal.Height = 0F;
            this.lnSumTotal.Left = 1.437F;
            this.lnSumTotal.LineWeight = 1F;
            this.lnSumTotal.Name = "lnSumTotal";
            this.lnSumTotal.Top = 0.022F;
            this.lnSumTotal.Width = 3.625F;
            this.lnSumTotal.X1 = 1.437F;
            this.lnSumTotal.X2 = 5.062F;
            this.lnSumTotal.Y1 = 0.022F;
            this.lnSumTotal.Y2 = 0.022F;
            // 
            // txtTotalCount
            // 
            this.txtTotalCount.Height = 0.1875F;
            this.txtTotalCount.Left = 2.074F;
            this.txtTotalCount.Name = "txtTotalCount";
            this.txtTotalCount.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtTotalCount.Text = null;
            this.txtTotalCount.Top = 0.048F;
            this.txtTotalCount.Width = 0.5F;
            // 
            // txtTotalInterest
            // 
            this.txtTotalInterest.Height = 0.1875F;
            this.txtTotalInterest.Left = 3.7615F;
            this.txtTotalInterest.Name = "txtTotalInterest";
            this.txtTotalInterest.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtTotalInterest.Text = null;
            this.txtTotalInterest.Top = 0.048F;
            this.txtTotalInterest.Width = 1.1875F;
            // 
            // txtTotalBalance
            // 
            this.txtTotalBalance.Height = 0.1875F;
            this.txtTotalBalance.Left = 2.574F;
            this.txtTotalBalance.Name = "txtTotalBalance";
            this.txtTotalBalance.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtTotalBalance.Text = null;
            this.txtTotalBalance.Top = 0.048F;
            this.txtTotalBalance.Width = 1.1875F;
            // 
            // srptTaxServiceFooter
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
            "l; font-size: 10pt; color: Black", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
            "lic", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.fldSumYear1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSumCount1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSumInterest1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSumBalance1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSumYear1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSumCount1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSumInterest1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSumBalance1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumYear;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCount;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumBalance;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInterest;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnSumTotal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalCount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalInterest;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalBalance;
    }
}
