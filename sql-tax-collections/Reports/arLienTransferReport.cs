using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class arLienTransferReport : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// Date           :               09/26/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// Last Updated   :               06/29/2005              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsLN = new clsDRWrapper();
		clsDRWrapper rsRE = new clsDRWrapper();
		string strSQL;
		double[] dblTotals = new double[4 + 1];
		int intYear;
		double dblMinimumAmount;
		int lngPosBal;
		int lngZeroBal;
		int lngNegBal;
		int lngDemandAlready;
		DateTime dtMailDate;
		int lngCount;
		public int intReportDetail;

		public arLienTransferReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Lien Transfer Report";
		}

		public static arLienTransferReport InstancePtr
		{
			get
			{
				return (arLienTransferReport)Sys.GetInstance(typeof(arLienTransferReport));
			}
		}

		protected arLienTransferReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsLN.Dispose();
				rsData.Dispose();
				rsRE.Dispose();
            }
			base.Dispose(disposing);
		}

		public void Init(string strAccountList)
		{
			// this sub actually lets me pass the data connection class in
			if (strAccountList != "")
			{
				rsData.OpenRecordset("SELECT * FROM BillingMaster WHERE ID IN (" + strAccountList + ") ORDER BY Name1, Account");
				StartLienEditReportQuery();
				if (rsData.RecordCount() > 0)
				{
					lngCount = rsData.RecordCount();
				}
				else
				{
					lngCount = 0;
				}
				// Me.Show
			}
			else
			{
				Cancel();
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void StartLienEditReportQuery()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				intYear = frmRateRecChoice.InstancePtr.intYear;
				// GetYearFromUser(Year(Now))
				// this will show the year on the report
				lblReportType.Text = intYear.ToString();
				// FormatYear(CStr(intYear))
				if (rsData.EndOfFile())
				{
					lblAccount.Left = 0;
					lblAccount.Width = this.PrintWidth;
					lblAccount.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
					lblAccount.Text = "No lien records for the year " + FCConvert.ToString(intYear) + " were found.";
					HideAllFields();
				}
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Data");
				// , True, rsData.RecordCount + 1
				// dtMailDate = CDate(frmRateRecChoice.txtMailDate.Text)
				dtMailDate = DateTime.Now;
				if (frmRateRecChoice.InstancePtr.cmbReportDetail.SelectedIndex < 0)
				{
					intReportDetail = 1;
				}
				else
				{
					intReportDetail = frmRateRecChoice.InstancePtr.cmbReportDetail.ItemData(frmRateRecChoice.InstancePtr.cmbReportDetail.SelectedIndex);
				}
				rsLN.OpenRecordset("SELECT * FROM LienRec", modExtraModules.strCLDatabase);
				frmWait.InstancePtr.IncrementProgress();
				// if the user has Real Estate, then use the information from the database
				// If gboolRE Then
				rsRE.OpenRecordset("SELECT * FROM Master", modExtraModules.strREDatabase);
				rsRE.InsertName("OwnerPartyID,SecOwnerPartyID", "Own1,Own2", true, true, false, "", false, "", true, "");
				//Application.DoEvents();
				// Me.Show , MDIParent
				frmWait.InstancePtr.Show(FCForm.FormShowEnum.Modeless);
				//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message, MsgBoxStyle.Critical, "Start Lien Edit Report Error");
			}
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_PageEnd(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
			// save this report
			if (this.Document.Pages.Count > 0)
			{
				modGlobalFunctions.IncrementSavedReports("LastCLTranfserToLien");
				this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastCLTranfserToLien1.RDF"));
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// format the report
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "t");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			lngPosBal = 0;
			lngZeroBal = 0;
			lngNegBal = 0;
			lngDemandAlready = 0;
			SetupDetailSection();
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
			frmTransferTaxToLien.InstancePtr.Unload();
			frmRateRecChoice.InstancePtr.Unload();
			// frmRateRecChoice.Show , MDIParent
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private string BuildSQL()
		{
			string BuildSQL = "";
			// this will return the correct SQL string
			string strFields = "";
			string strWhereClause = "";
			int intCT/*unused?*/;
			// strFields = "Account, Name1, LienRec.InterestCharged AS InterestCharged, LienRec.RateKey AS RateKey, LienRec.InterestAppliedThroughDate AS InterestAppliedThroughDate, Principal, Interest, Costs, LienRec.PrincipalPaid AS PrincipalPaid, LienRec.InterestPaid AS InterestPaid"
			// 
			// strSQL = "SELECT " & strFields & " FROM LienRec INNER JOIN BillingMaster ON LienRec.LienRecordNumber = BillingMaster.LienRecordNumber ORDER BY Account, LienRec.LienRecordNumber"
			if (frmRateRecChoice.InstancePtr.cmbRange.SelectedIndex == 2)
			{
				// range of accounts
				if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[0].Text) != "")
				{
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
					{
						// both full
						strWhereClause = " AND Account >= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text)) + " AND Account <= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
					}
					else
					{
						// first full second empty
						strWhereClause = " AND Account >= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text));
					}
				}
				else
				{
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
					{
						// first empty second full
						strWhereClause = " AND Account <= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
					}
					else
					{
						// both empty
						strWhereClause = "";
					}
				}
			}
			else if (frmRateRecChoice.InstancePtr.cmbRange.SelectedIndex == 1)
			{
				// range of names
				if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[0].Text) != "")
				{
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
					{
						// both full
						// strWhereClause = " AND Name1 BETWEEN '" & .txtRange(0).Text & "' AND '" & .txtRange(1).Text & "'"
						strWhereClause = " AND Name1 >= '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "    ' AND Name1 <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "ZZZZ'";
					}
					else
					{
						// first full second empty
						strWhereClause = " AND Name1 >= '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "   '";
					}
				}
				else
				{
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
					{
						// first empty second full
						strWhereClause = " AND Name1 <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "ZZZZ'";
					}
					else
					{
						// both empty
						strWhereClause = "";
					}
				}
			}
			else
			{
				strWhereClause = "";
			}
			// strSQL = "SELECT * FROM BillingMaster WHERE BillingYear \ 10 = " & intYear & " AND BillingType = 'RE'" & strWhereClause & " ORDER BY Account"
			strSQL = "SELECT * FROM (" + modGlobal.Statics.gstrBillYrQuery + ") AS qBY WHERE BillingType = 'RE'" + strWhereClause + " ORDER BY Name1, Account";
			// kgk 2-14-2012 rsData.CreateStoredProcedure "LienEditReportQuery", strSQL
			// strSQL = "SELECT * FROM LienEditReportQuery ORDER BY Name1, Account"
			BuildSQL = strSQL;
			return BuildSQL;
		}

		private void BindFields()
		{
			// this will fill the fields in
			double dblCurInt = 0;
			double dblTotal = 0;
			double dblPrin = 0;
			double dblInt = 0;
			float lngNextLine = 0;
			double dblDemandFeeAmount = 0;
			TryAgain:
			// this is where the program will start again if the current account is not acceptable
			if (rsData.EndOfFile() != true)
			{
				frmWait.InstancePtr.IncrementProgress();
				if (FCConvert.ToString(rsData.Get_Fields_String("WhetherBilledBefore")) == "L")
				{
					rsLN.FindFirstRecord("ID", rsData.Get_Fields_Int32("LienRecordNumber"));
					if (!rsLN.NoMatch)
					{
						dblPrin = Conversion.Val(rsLN.Get_Fields("Principal"));
						dblInt = Conversion.Val(rsLN.Get_Fields("Interest")) - Conversion.Val(rsLN.Get_Fields_Decimal("InterestCharged"));
						dblTotal = modCLCalculations.CalculateAccountCLLien(rsLN, dtMailDate, ref dblCurInt);
						if (modGlobal.Round(dblTotal, 2) == 0)
						{
							rsData.MoveNext();
							goto TryAgain;
						}
						else
						{
							fldAccount.Text = FCConvert.ToString(rsData.Get_Fields("Account"));
							fldName.Text = rsData.Get_Fields_String("Name1");
							fldPrincipal.Text = Strings.Format(dblPrin, "#,##0.00");
							fldPLInt.Text = Strings.Format(dblInt, "#,##0.00");
							dblDemandFeeAmount = Conversion.Val(rsData.Get_Fields_Decimal("DemandFees") - rsData.Get_Fields_Decimal("DemandFeesPaid"));
							fldCosts.Text = Strings.Format(rsLN.Get_Fields("Costs"), "#,##0.00");
							fldDemand.Text = Strings.Format(dblDemandFeeAmount, "#,##0.00");

							dblTotals[0] += dblPrin;
							dblTotals[1] += dblInt;
							dblTotals[2] += Conversion.Val(rsLN.Get_Fields("Costs"));
							// - rsLN.Fields("MaturityFee") - dblDemandFeeAmount
							dblTotals[3] += dblCurInt;
							dblTotals[4] += dblDemandFeeAmount;
							// this will shwo the demand fee on each line seperately
							if (modGlobal.Round(dblTotal, 2) == modGlobal.Round((dblPrin + dblInt + Conversion.Val(rsLN.Get_Fields("Costs")) - Conversion.Val(rsLN.Get_Fields("MaturityFee"))) - (Conversion.Val(rsLN.Get_Fields_Decimal("PrincipalPaid")) + Conversion.Val(rsLN.Get_Fields_Decimal("InterestPaid")) + Conversion.Val(rsLN.Get_Fields_Decimal("CostsPaid"))), 2))
							{
								// - dblCurInt
								fldTotal.Text = Strings.Format(dblTotal, "#,##0.00");
							}
							else
							{
								fldTotal.Text = Strings.Format((dblPrin + dblInt + Conversion.Val(rsLN.Get_Fields("Costs")) - Conversion.Val(rsLN.Get_Fields("MaturityFee"))) - (Conversion.Val(rsLN.Get_Fields_Decimal("PrincipalPaid")) + Conversion.Val(rsLN.Get_Fields_Decimal("InterestPaid")) + Conversion.Val(rsLN.Get_Fields_Decimal("CostsPaid"))), "#,##0.00");
								// - dblCurInt
							}
						}
					}
					else
					{
						fldTotal.Text = "ERROR";
					}
				}
				else
				{
					dblPrin = Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1") + rsData.Get_Fields_Decimal("TaxDue2") + rsData.Get_Fields_Decimal("TaxDue3") + rsData.Get_Fields_Decimal("TaxDue4") - rsData.Get_Fields_Decimal("PrincipalPaid"));
					dblInt = FCConvert.ToDouble((rsData.Get_Fields_Decimal("InterestCharged") * -1) - rsData.Get_Fields_Decimal("InterestPaid"));
					dblTotal = modCLCalculations.CalculateAccountCL(ref rsData, FCConvert.ToInt32(rsData.Get_Fields("Account")), dtMailDate, ref dblCurInt);
					if (modGlobal.Round(dblTotal, 2) <= dblMinimumAmount)
					{
						// if the total for the account = 0 then do not show this account
						if (Conversion.Val(modGlobal.Round(dblTotal, 2)) == 0)
						{
							lngZeroBal += 1;
						}
						else
						{
							lngNegBal += 1;
						}
						rsData.MoveNext();
						goto TryAgain;
					}
					else
					{
						fldAccount.Text = FCConvert.ToString(rsData.Get_Fields("Account"));
						fldName.Text = rsData.Get_Fields_String("Name1");
						fldPrincipal.Text = Strings.Format(dblPrin, "#,##0.00");
						fldPLInt.Text = Strings.Format(dblInt, "#,##0.00");
						dblDemandFeeAmount = Conversion.Val(rsData.Get_Fields_Decimal("DemandFees"));
						fldCosts.Text = Strings.Format(Conversion.Val(rsData.Get_Fields_Decimal("DemandFees") - rsData.Get_Fields_Decimal("DemandFeesPaid")) - dblDemandFeeAmount, "#,##0.00");
						fldDemand.Text = Strings.Format(dblDemandFeeAmount, "#,##0.00");
						if (rsData.Get_Fields_Decimal("DemandFees") > 0)
						{
							// keep track of the number of accounts dealt with (show this later)
							lngDemandAlready += 1;
						}
						else
						{
							lngPosBal += 1;
						}
						dblTotals[0] += dblPrin;
						dblTotals[1] += dblInt;
						dblTotals[2] += Conversion.Val(rsData.Get_Fields_Decimal("DemandFees")) - dblDemandFeeAmount;
						dblTotals[3] += dblCurInt;
						dblTotals[4] += dblDemandFeeAmount;
						// this will show the demand fee on each line seperately
						if (modGlobal.Round(dblTotal, 2) == modGlobal.Round((dblPrin + dblInt + Conversion.Val(rsData.Get_Fields_Decimal("DemandFees") - rsData.Get_Fields_Decimal("DemandFeesPaid"))), 2))
						{
							// - dblCurInt), 2) Then '- (rsData.Fields("PrincipalPaid") + rsData.Fields("InterestPaid") + rsData.Fields("DemandFeesPaid")), 2) Then
							fldTotal.Text = Strings.Format(dblTotal, "#,##0.00");
						}
						else
						{
							fldTotal.Text = Strings.Format((dblPrin + dblInt + Conversion.Val(rsData.Get_Fields_Decimal("DemandFees"))), "#,##0.00");
							// + dblCurInt), "#,##0.00")  '- (rsData.Fields("PrincipalPaid") + rsData.Fields("InterestPaid") + rsData.Fields("DemandFeesPaid")), "#,##0.00")
						}
					}
				}
				rsRE.FindFirstRecord("RSAccount", rsData.Get_Fields("Account"));
				if (!rsRE.NoMatch)
				{
					if (Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("DeedName1"))) == FCConvert.ToString(rsData.Get_Fields_String("Name1")))
					{
						fldCO.Top = 0;
						fldCO.Visible = false;
						fldCO.Text = "";
						lngNextLine = 180 / 1440F;
					}
					else
					{
						fldCO.Top = 180 / 1440F;
						fldCO.Visible = true;
						fldCO.Text = "C/O " + FCConvert.ToString(rsRE.Get_Fields("DeedName1"));
						lngNextLine = 360 / 1440F;
					}
				}
				else
				{
					fldCO.Top = 0;
					fldCO.Visible = false;
					fldCO.Text = "";
					lngNextLine = 180 / 1440F;
				}

				switch (intReportDetail)
				{
					case 1:
					case 2:
						{
							sarLienEditMortHolders.Top = lngNextLine + 100 / 1440F;
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							sarLienEditMortHolders.Report.UserData = FCConvert.ToInt32(rsData.Get_Fields("Account")) * -1;
							break;
						}
				}
				//end switch
				rsData.MoveNext();
			}
			else
			{
				fldTotal.Text = "";
				fldPrincipal.Text = "";
				fldAccount.Text = "";
				fldCosts.Text = "";
				// fldCurrentInt.Text = ""
				fldPLInt.Text = "";
				fldName.Text = "";
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			// fill the totals line in
			fldTotalPrincipal.Text = Strings.Format(dblTotals[0], "#,##0.00");
			fldTotalPLInt.Text = Strings.Format(dblTotals[1], "#,##0.00");
			fldTotalCosts.Text = Strings.Format(dblTotals[2], "#,##0.00");
			// fldTotalCurrentInt.Text = Format(dblTotals(3), "#,##0.00")
			fldTotalDemand.Text = Strings.Format(dblTotals[4], "#,##0.00");
			lblTotals.Text = "Count " + FCConvert.ToString(lngCount) + "  Total:";
			fldTotalTotal.Text = Strings.Format(dblTotals[0] + dblTotals[1] + dblTotals[2], "#,##0.00");
			// + dblTotals(3)+ dblTotals(4)
		}
		
		private void HideAllFields()
		{
			lblCosts.Visible = false;
			// lblCurrentInt.Visible = False
			lblName.Visible = false;
			lblPLInt.Visible = false;
			lblPrincipal.Visible = false;
			lblReportType.Visible = false;
			lblTotal.Visible = false;
			lblTotals.Visible = false;
			lblDemand.Visible = false;
			Line1.Visible = false;
			Line2.Visible = false;
			fldTotalCosts.Visible = false;
			// fldTotalCurrentInt.Visible = False
			fldTotalPLInt.Visible = false;
			fldTotalPrincipal.Visible = false;
			fldTotalTotal.Visible = false;
			fldTotalDemand.Visible = false;
		}

		private void SetupDetailSection()
		{
			// this will set the height of the detail section and the visibility of the sub report
			switch (intReportDetail)
			{
				case 0:
					{
						sarLienEditMortHolders.Visible = false;
						sarLienEditMortHolders.Top = 0;
						break;
					}
				case 1:
				case 2:
					{
						sarLienEditMortHolders.Visible = true;
						sarLienEditMortHolders.Report = new sarLienEditMortHolders();
						break;
					}
			}
			//end switch
		}

		
	}
}