﻿namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	partial class arLienMaturity
	{
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(arLienMaturity));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.rtbText = new GrapeCity.ActiveReports.SectionReportModel.RichTextBox();
			this.lblPrincipal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblInterest = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDemand = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCertMailFee = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDemand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCertMailFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldCollector = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblReportHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFilingFee = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldFilingFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.imgSig = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			this.lblTaxAcquired = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldCounty = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.imgTownSeal = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			this.fldBottomMessage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDemand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCertMailFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDemand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCertMailFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCollector)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFilingFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFilingFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgSig)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTaxAcquired)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCounty)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgTownSeal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.rtbText,
				this.lblPrincipal,
				this.lblInterest,
				this.lblDemand,
				this.lblCertMailFee,
				this.lblTotal,
				this.Line1,
				this.fldPrincipal,
				this.fldInterest,
				this.fldDemand,
				this.fldCertMailFee,
				this.fldTotal,
				this.Line2,
				this.fldCollector,
				this.fldTitle,
				this.fldMuni,
				this.lblReportHeader,
				this.lblDate,
				this.lblFilingFee,
				this.fldFilingFee,
				this.imgSig,
				this.lblTaxAcquired,
				this.fldCounty,
				this.imgTownSeal,
				this.fldBottomMessage
			});
			this.Detail.Height = 2.895833F;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// rtbText
			// 
			this.rtbText.AutoReplaceFields = true;
			this.rtbText.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.rtbText.Height = 0.125F;
			this.rtbText.Left = 0F;
			this.rtbText.Name = "rtbText";
			this.rtbText.RTF = resources.GetString("rtbText.RTF");
			this.rtbText.Top = 0.875F;
			this.rtbText.Width = 7.4375F;
			// 
			// lblPrincipal
			// 
			this.lblPrincipal.Height = 0.1875F;
			this.lblPrincipal.HyperLink = null;
			this.lblPrincipal.Left = 0.9375F;
			this.lblPrincipal.Name = "lblPrincipal";
			this.lblPrincipal.Style = "font-family: \'Tahoma\'";
			this.lblPrincipal.Text = "Principal";
			this.lblPrincipal.Top = 1.75F;
			this.lblPrincipal.Width = 0.875F;
			// 
			// lblInterest
			// 
			this.lblInterest.Height = 0.1875F;
			this.lblInterest.HyperLink = null;
			this.lblInterest.Left = 0.9375F;
			this.lblInterest.Name = "lblInterest";
			this.lblInterest.Style = "font-family: \'Tahoma\'";
			this.lblInterest.Text = "Interest";
			this.lblInterest.Top = 1.9375F;
			this.lblInterest.Width = 0.875F;
			// 
			// lblDemand
			// 
			this.lblDemand.Height = 0.1875F;
			this.lblDemand.HyperLink = null;
			this.lblDemand.Left = 0.9375F;
			this.lblDemand.Name = "lblDemand";
			this.lblDemand.Style = "font-family: \'Tahoma\'";
			this.lblDemand.Text = "Lien Costs";
			this.lblDemand.Top = 2.125F;
			this.lblDemand.Width = 0.875F;
			// 
			// lblCertMailFee
			// 
			this.lblCertMailFee.Height = 0.1875F;
			this.lblCertMailFee.HyperLink = null;
			this.lblCertMailFee.Left = 0.9375F;
			this.lblCertMailFee.Name = "lblCertMailFee";
			this.lblCertMailFee.Style = "font-family: \'Tahoma\'";
			this.lblCertMailFee.Text = "Cert Mail Fee";
			this.lblCertMailFee.Top = 2.5F;
			this.lblCertMailFee.Width = 0.875F;
			// 
			// lblTotal
			// 
			this.lblTotal.Height = 0.1875F;
			this.lblTotal.HyperLink = null;
			this.lblTotal.Left = 0.9375F;
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Style = "font-family: \'Tahoma\'";
			this.lblTotal.Text = "Total";
			this.lblTotal.Top = 2.6875F;
			this.lblTotal.Width = 0.875F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.9375F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 2.6875F;
			this.Line1.Width = 2.125F;
			this.Line1.X1 = 0.9375F;
			this.Line1.X2 = 3.0625F;
			this.Line1.Y1 = 2.6875F;
			this.Line1.Y2 = 2.6875F;
			// 
			// fldPrincipal
			// 
			this.fldPrincipal.Height = 0.1875F;
			this.fldPrincipal.Left = 1.8125F;
			this.fldPrincipal.Name = "fldPrincipal";
			this.fldPrincipal.Style = "font-family: \'Courier New\'; text-align: right";
			this.fldPrincipal.Text = "0.00";
			this.fldPrincipal.Top = 1.75F;
			this.fldPrincipal.Width = 1.1875F;
			// 
			// fldInterest
			// 
			this.fldInterest.Height = 0.1875F;
			this.fldInterest.Left = 1.8125F;
			this.fldInterest.Name = "fldInterest";
			this.fldInterest.Style = "font-family: \'Courier New\'; text-align: right";
			this.fldInterest.Text = "0.00";
			this.fldInterest.Top = 1.9375F;
			this.fldInterest.Width = 1.1875F;
			// 
			// fldDemand
			// 
			this.fldDemand.Height = 0.1875F;
			this.fldDemand.Left = 1.8125F;
			this.fldDemand.Name = "fldDemand";
			this.fldDemand.Style = "font-family: \'Courier New\'; text-align: right";
			this.fldDemand.Text = "0.00";
			this.fldDemand.Top = 2.125F;
			this.fldDemand.Width = 1.1875F;
			// 
			// fldCertMailFee
			// 
			this.fldCertMailFee.Height = 0.1875F;
			this.fldCertMailFee.Left = 1.8125F;
			this.fldCertMailFee.Name = "fldCertMailFee";
			this.fldCertMailFee.Style = "font-family: \'Courier New\'; text-align: right";
			this.fldCertMailFee.Text = "0.00";
			this.fldCertMailFee.Top = 2.5F;
			this.fldCertMailFee.Width = 1.1875F;
			// 
			// fldTotal
			// 
			this.fldTotal.Height = 0.1875F;
			this.fldTotal.Left = 1.8125F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "font-family: \'Courier New\'; text-align: right";
			this.fldTotal.Text = "0.00";
			this.fldTotal.Top = 2.6875F;
			this.fldTotal.Width = 1.1875F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 4F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 1.625F;
			this.Line2.Width = 2.4375F;
			this.Line2.X1 = 4F;
			this.Line2.X2 = 6.4375F;
			this.Line2.Y1 = 1.625F;
			this.Line2.Y2 = 1.625F;
			// 
			// fldCollector
			// 
			this.fldCollector.Height = 0.1875F;
			this.fldCollector.Left = 4F;
			this.fldCollector.Name = "fldCollector";
			this.fldCollector.Style = "font-family: \'Courier New\'";
			this.fldCollector.Text = null;
			this.fldCollector.Top = 1.625F;
			this.fldCollector.Width = 2.4375F;
			// 
			// fldTitle
			// 
			this.fldTitle.Height = 0.1875F;
			this.fldTitle.Left = 4F;
			this.fldTitle.Name = "fldTitle";
			this.fldTitle.Style = "font-family: \'Courier New\'";
			this.fldTitle.Text = "Collector of Taxes";
			this.fldTitle.Top = 1.8125F;
			this.fldTitle.Width = 2.4375F;
			// 
			// fldMuni
			// 
			this.fldMuni.Height = 0.1875F;
			this.fldMuni.Left = 4F;
			this.fldMuni.Name = "fldMuni";
			this.fldMuni.Style = "font-family: \'Courier New\'";
			this.fldMuni.Text = null;
			this.fldMuni.Top = 2F;
			this.fldMuni.Width = 2.4375F;
			// 
			// lblReportHeader
			// 
			this.lblReportHeader.Height = 0.5625F;
			this.lblReportHeader.HyperLink = null;
			this.lblReportHeader.Left = 0F;
			this.lblReportHeader.Name = "lblReportHeader";
			this.lblReportHeader.Style = "font-family: \'Courier New\'; font-size: 12pt; font-weight: bold; text-align: cente" + "r; ddo-char-set: 1";
			this.lblReportHeader.Text = null;
			this.lblReportHeader.Top = 0F;
			this.lblReportHeader.Width = 7.4375F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 5.9375F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Courier New\'; text-align: right; ddo-char-set: 1";
			this.lblDate.Text = null;
			this.lblDate.Top = 0.625F;
			this.lblDate.Width = 1.5F;
			// 
			// lblFilingFee
			// 
			this.lblFilingFee.Height = 0.1875F;
			this.lblFilingFee.HyperLink = null;
			this.lblFilingFee.Left = 0.9375F;
			this.lblFilingFee.Name = "lblFilingFee";
			this.lblFilingFee.Style = "font-family: \'Tahoma\'";
			this.lblFilingFee.Text = "Fee";
			this.lblFilingFee.Top = 2.3125F;
			this.lblFilingFee.Width = 0.875F;
			// 
			// fldFilingFee
			// 
			this.fldFilingFee.Height = 0.1875F;
			this.fldFilingFee.Left = 1.8125F;
			this.fldFilingFee.Name = "fldFilingFee";
			this.fldFilingFee.Style = "font-family: \'Courier New\'; text-align: right";
			this.fldFilingFee.Text = "0.00";
			this.fldFilingFee.Top = 2.3125F;
			this.fldFilingFee.Width = 1.1875F;
			// 
			// imgSig
			// 
			this.imgSig.Height = 0.5625F;
			this.imgSig.HyperLink = null;
			this.imgSig.ImageData = null;
			this.imgSig.Left = 4F;
			this.imgSig.LineWeight = 1F;
			this.imgSig.Name = "imgSig";
			this.imgSig.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
			this.imgSig.Top = 1.0625F;
			this.imgSig.Width = 2.375F;
			// 
			// lblTaxAcquired
			// 
			this.lblTaxAcquired.Height = 0.3125F;
			this.lblTaxAcquired.HyperLink = null;
			this.lblTaxAcquired.Left = 0F;
			this.lblTaxAcquired.Name = "lblTaxAcquired";
			this.lblTaxAcquired.Style = "font-family: \'Courier New\'; font-size: 24pt; font-weight: bold; text-align: cente" + "r; ddo-char-set: 0";
			this.lblTaxAcquired.Text = "TAX ACQUIRED";
			this.lblTaxAcquired.Top = 0.375F;
			this.lblTaxAcquired.Visible = false;
			this.lblTaxAcquired.Width = 7.4375F;
			// 
			// fldCounty
			// 
			this.fldCounty.Height = 0.1875F;
			this.fldCounty.Left = 4F;
			this.fldCounty.Name = "fldCounty";
			this.fldCounty.Style = "font-family: \'Courier New\'";
			this.fldCounty.Text = null;
			this.fldCounty.Top = 2.1875F;
			this.fldCounty.Width = 2.4375F;
			// 
			// imgTownSeal
			// 
			this.imgTownSeal.Height = 0.875F;
			this.imgTownSeal.HyperLink = null;
			this.imgTownSeal.ImageData = null;
			this.imgTownSeal.Left = 0F;
			this.imgTownSeal.LineWeight = 1F;
			this.imgTownSeal.Name = "imgTownSeal";
			this.imgTownSeal.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
			this.imgTownSeal.Top = 0F;
			this.imgTownSeal.Visible = false;
			this.imgTownSeal.Width = 0.875F;
			// 
			// fldBottomMessage
			// 
			this.fldBottomMessage.Height = 0.4375F;
			this.fldBottomMessage.Left = 3.1875F;
			this.fldBottomMessage.Name = "fldBottomMessage";
			this.fldBottomMessage.Style = "font-family: \'Courier New\'; text-align: left";
			this.fldBottomMessage.Text = null;
			this.fldBottomMessage.Top = 2.4375F;
			this.fldBottomMessage.Width = 3.8125F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.CanGrow = false;
			this.ReportFooter.CanShrink = true;
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// arLienMaturity
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.447917F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDemand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCertMailFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDemand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCertMailFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCollector)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFilingFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFilingFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgSig)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTaxAcquired)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCounty)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgTownSeal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.RichTextBox rtbText;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblInterest;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDemand;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCertMailFee;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInterest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDemand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCertMailFee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCollector;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMuni;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFilingFee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFilingFee;
		private GrapeCity.ActiveReports.SectionReportModel.Picture imgSig;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTaxAcquired;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCounty;
		private GrapeCity.ActiveReports.SectionReportModel.Picture imgTownSeal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomMessage;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
	}
}
