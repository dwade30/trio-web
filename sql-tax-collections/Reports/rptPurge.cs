﻿using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.VisualBasicLayer;
using Global;
using TWSharedLibrary;
using Wisej.Base;

namespace TWCL0000
{
  
    public partial class rptPurge : BaseSectionReport
    {
        protected rptPurge _InstancePtr;

        private bool boolFirstPass;

        private bool boolPurgeRE;

        public DateTime dtPurgeDate;

        public int lngPurgeYear;

        public int lngTotalCount;

        //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
        //public clsDRWrapper rsLien = new clsDRWrapper();
        public clsDRWrapper rsLien_AutoInitialized;

        public rptPurge()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        public clsDRWrapper rsLien
        {
            get
            {
                if (rsLien_AutoInitialized == null) rsLien_AutoInitialized = new clsDRWrapper();

                return rsLien_AutoInitialized;
            }
            set => rsLien_AutoInitialized = value;
        }

        public static rptPurge InstancePtr => (rptPurge) Sys.GetInstance(typeof(rptPurge));

        private void InitializeComponentEx()
        {
            if (_InstancePtr == null) _InstancePtr = this;
            Name = "Purge Report";
        }

        /// <summary>
        ///     Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
            {
                _InstancePtr = null;
                Sys.ClearInstance(this);
            }

            base.Dispose(disposing);
        }

        public bool Init(bool boolPassRE, DateTime dtPassDate, bool modalDialog, int lngErrNum = 0, string strErrString = "", int lngYear = 0)
        {
            var Init = false;

            try
            {

                boolPurgeRE = boolPassRE;
                dtPurgeDate = dtPassDate;
                lngPurgeYear = lngYear;

                // reset the totals
               // FCUtils.EraseSafe(modCLPurge.Statics.dblPurgeTotals);

                
                rsLien.OpenRecordset("SELECT * FROM LienRec", modExtraModules.strCLDatabase);
				frmReportViewer.InstancePtr.Init(this, "", 1, showModal: modalDialog);
                Init = true;

                return Init;
            }
            catch (Exception ex)
            {

                lngErrNum = Information.Err(ex)
                                       .Number;
                strErrString = Information.Err(ex)
                                          .Description;
                Init = false;
            }

            return Init;
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
        {
            eArgs.EOF = !boolFirstPass;
            if (boolFirstPass) boolFirstPass = false;

        }

        private void ActiveReport_ReportStart(object sender, EventArgs e)
        {
            boolFirstPass = true;
            lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
            lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
            lblMuniName.Text = modGlobalConstants.Statics.MuniName;

        }

        private void ActiveReport_Terminate(object sender, EventArgs e)
        {
            // save the report
            var strREPP = "";
            strREPP = boolPurgeRE ? "R" : "P";

            //Document.Save(Path.Combine($"{FCFileSystem.Statics.UserDataFolder}/RPT/", $"CL{strREPP}P{1}.RDF"));

        }

        private void Detail_Format(object sender, EventArgs e)
        {
 
            PurgeNonLien.Report = new rptPurgeBills();
            PurgeNonLien.Report.UserData = boolPurgeRE ? 0 : 2;

            if (!boolPurgeRE) return;

            PurgeLiens.Report = new rptPurgeBills();
            PurgeLiens.Report.UserData = 1;

        }

        private void GroupFooter1_Format(object sender, EventArgs e)
        {
            fldTotalCount.Text = $"{FCConvert.ToString(lngTotalCount)} bills will be purged.";
        }

        private void PageHeader_Format(object sender, EventArgs e)
        {
            lblPage.Text = $"Page {PageNumber}";
        }

    }
}