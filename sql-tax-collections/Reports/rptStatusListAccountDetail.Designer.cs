﻿namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	partial class rptStatusListAccountDetail
	{
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptStatusListAccountDetail));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAddress = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblLocation = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblMapLot = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBuilding = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldBuilding = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblExempt = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblLand = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblReference2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldRef2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblReference = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldRef1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.srptSLAllActivityDetailOB = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.lblYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lnTotals = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPrincipal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblInterest = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCosts = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblReportType = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.lblDeletedAcct = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMapLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBuilding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMapLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBuilding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExempt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldExempt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLand)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLand)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReference2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRef2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRef1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCosts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReportType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDeletedAcct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldName,
            this.lblAccount,
            this.lblAddress,
            this.lblLocation,
            this.fldAddress,
            this.fldLocation,
            this.lblMapLot,
            this.lblBuilding,
            this.fldMapLot,
            this.fldBuilding,
            this.lblExempt,
            this.fldExempt,
            this.lblLand,
            this.fldLand,
            this.lblReference2,
            this.fldRef2,
            this.lblReference,
            this.fldRef1,
            this.srptSLAllActivityDetailOB,
            this.lblYear});
            this.Detail.Height = 1.364583F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // fldName
            // 
            this.fldName.Height = 0.1875F;
            this.fldName.Left = 2F;
            this.fldName.Name = "fldName";
            this.fldName.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.fldName.Text = null;
            this.fldName.Top = 0F;
            this.fldName.Width = 5.5F;
            // 
            // lblAccount
            // 
            this.lblAccount.Height = 0.1875F;
            this.lblAccount.HyperLink = null;
            this.lblAccount.Left = 0F;
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblAccount.Text = null;
            this.lblAccount.Top = 0F;
            this.lblAccount.Width = 1F;
            // 
            // lblAddress
            // 
            this.lblAddress.Height = 0.1875F;
            this.lblAddress.HyperLink = null;
            this.lblAddress.Left = 0F;
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Style = "font-family: \'Tahoma\'";
            this.lblAddress.Text = "Address:";
            this.lblAddress.Top = 0.1875F;
            this.lblAddress.Width = 0.75F;
            // 
            // lblLocation
            // 
            this.lblLocation.Height = 0.1875F;
            this.lblLocation.HyperLink = null;
            this.lblLocation.Left = 0F;
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Style = "font-family: \'Tahoma\'";
            this.lblLocation.Text = "Location:";
            this.lblLocation.Top = 0.375F;
            this.lblLocation.Width = 0.75F;
            // 
            // fldAddress
            // 
            this.fldAddress.Height = 0.1875F;
            this.fldAddress.Left = 0.75F;
            this.fldAddress.Name = "fldAddress";
            this.fldAddress.Style = "font-family: \'Tahoma\'";
            this.fldAddress.Text = null;
            this.fldAddress.Top = 0.1875F;
            this.fldAddress.Width = 6.125F;
            // 
            // fldLocation
            // 
            this.fldLocation.Height = 0.1875F;
            this.fldLocation.Left = 0.75F;
            this.fldLocation.Name = "fldLocation";
            this.fldLocation.Style = "font-family: \'Tahoma\'";
            this.fldLocation.Text = null;
            this.fldLocation.Top = 0.375F;
            this.fldLocation.Width = 6.125F;
            // 
            // lblMapLot
            // 
            this.lblMapLot.Height = 0.1875F;
            this.lblMapLot.HyperLink = null;
            this.lblMapLot.Left = 0F;
            this.lblMapLot.Name = "lblMapLot";
            this.lblMapLot.Style = "font-family: \'Tahoma\'";
            this.lblMapLot.Text = "Map Lot:";
            this.lblMapLot.Top = 0.5625F;
            this.lblMapLot.Width = 0.75F;
            // 
            // lblBuilding
            // 
            this.lblBuilding.Height = 0.1875F;
            this.lblBuilding.HyperLink = null;
            this.lblBuilding.Left = 1.625F;
            this.lblBuilding.Name = "lblBuilding";
            this.lblBuilding.Style = "font-family: \'Tahoma\'";
            this.lblBuilding.Text = "Building:";
            this.lblBuilding.Top = 0.5625F;
            this.lblBuilding.Width = 0.625F;
            // 
            // fldMapLot
            // 
            this.fldMapLot.CanGrow = false;
            this.fldMapLot.Height = 0.1875F;
            this.fldMapLot.Left = 0.75F;
            this.fldMapLot.MultiLine = false;
            this.fldMapLot.Name = "fldMapLot";
            this.fldMapLot.Style = "font-family: \'Tahoma\'";
            this.fldMapLot.Text = null;
            this.fldMapLot.Top = 0.5625F;
            this.fldMapLot.Width = 0.875F;
            // 
            // fldBuilding
            // 
            this.fldBuilding.CanGrow = false;
            this.fldBuilding.Height = 0.1875F;
            this.fldBuilding.Left = 2.1875F;
            this.fldBuilding.MultiLine = false;
            this.fldBuilding.Name = "fldBuilding";
            this.fldBuilding.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldBuilding.Text = null;
            this.fldBuilding.Top = 0.5625F;
            this.fldBuilding.Width = 1F;
            // 
            // lblExempt
            // 
            this.lblExempt.Height = 0.1875F;
            this.lblExempt.HyperLink = null;
            this.lblExempt.Left = 1.625F;
            this.lblExempt.Name = "lblExempt";
            this.lblExempt.Style = "font-family: \'Tahoma\'";
            this.lblExempt.Text = "Exempt:";
            this.lblExempt.Top = 0.9375F;
            this.lblExempt.Width = 0.625F;
            // 
            // fldExempt
            // 
            this.fldExempt.CanGrow = false;
            this.fldExempt.Height = 0.1875F;
            this.fldExempt.Left = 2.1875F;
            this.fldExempt.MultiLine = false;
            this.fldExempt.Name = "fldExempt";
            this.fldExempt.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldExempt.Text = null;
            this.fldExempt.Top = 0.9375F;
            this.fldExempt.Width = 1F;
            // 
            // lblLand
            // 
            this.lblLand.Height = 0.1875F;
            this.lblLand.HyperLink = null;
            this.lblLand.Left = 1.625F;
            this.lblLand.Name = "lblLand";
            this.lblLand.Style = "font-family: \'Tahoma\'";
            this.lblLand.Text = "Land";
            this.lblLand.Top = 0.75F;
            this.lblLand.Width = 0.625F;
            // 
            // fldLand
            // 
            this.fldLand.CanGrow = false;
            this.fldLand.Height = 0.1875F;
            this.fldLand.Left = 2.1875F;
            this.fldLand.MultiLine = false;
            this.fldLand.Name = "fldLand";
            this.fldLand.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldLand.Text = null;
            this.fldLand.Top = 0.75F;
            this.fldLand.Width = 1F;
            // 
            // lblReference2
            // 
            this.lblReference2.Height = 0.1875F;
            this.lblReference2.HyperLink = null;
            this.lblReference2.Left = 0F;
            this.lblReference2.Name = "lblReference2";
            this.lblReference2.Style = "font-family: \'Tahoma\'";
            this.lblReference2.Text = "Reference:";
            this.lblReference2.Top = 0.9375F;
            this.lblReference2.Width = 0.75F;
            // 
            // fldRef2
            // 
            this.fldRef2.CanGrow = false;
            this.fldRef2.Height = 0.1875F;
            this.fldRef2.Left = 0.75F;
            this.fldRef2.MultiLine = false;
            this.fldRef2.Name = "fldRef2";
            this.fldRef2.Style = "font-family: \'Tahoma\'";
            this.fldRef2.Text = null;
            this.fldRef2.Top = 0.9375F;
            this.fldRef2.Width = 0.875F;
            // 
            // lblReference
            // 
            this.lblReference.Height = 0.1875F;
            this.lblReference.HyperLink = null;
            this.lblReference.Left = 0F;
            this.lblReference.Name = "lblReference";
            this.lblReference.Style = "font-family: \'Tahoma\'";
            this.lblReference.Text = "Reference:";
            this.lblReference.Top = 0.75F;
            this.lblReference.Width = 0.75F;
            // 
            // fldRef1
            // 
            this.fldRef1.CanGrow = false;
            this.fldRef1.Height = 0.1875F;
            this.fldRef1.Left = 0.75F;
            this.fldRef1.MultiLine = false;
            this.fldRef1.Name = "fldRef1";
            this.fldRef1.Style = "font-family: \'Tahoma\'";
            this.fldRef1.Text = null;
            this.fldRef1.Top = 0.75F;
            this.fldRef1.Width = 0.875F;
            // 
            // srptSLAllActivityDetailOB
            // 
            this.srptSLAllActivityDetailOB.CloseBorder = false;
            this.srptSLAllActivityDetailOB.Height = 0.125F;
            this.srptSLAllActivityDetailOB.Left = 0F;
            this.srptSLAllActivityDetailOB.Name = "srptSLAllActivityDetailOB";
            this.srptSLAllActivityDetailOB.Report = null;
            this.srptSLAllActivityDetailOB.Top = 1.125F;
            this.srptSLAllActivityDetailOB.Width = 7.5F;
            // 
            // lblYear
            // 
            this.lblYear.Height = 0.1875F;
            this.lblYear.HyperLink = null;
            this.lblYear.Left = 1F;
            this.lblYear.Name = "lblYear";
            this.lblYear.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblYear.Text = null;
            this.lblYear.Top = 0F;
            this.lblYear.Width = 1F;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Field6,
            this.Field7,
            this.Field8,
            this.Field9,
            this.lnTotals});
            this.ReportFooter.Height = 0.375F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // Field6
            // 
            this.Field6.Height = 0.1875F;
            this.Field6.Left = 2.75F;
            this.Field6.Name = "Field6";
            this.Field6.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.Field6.Text = null;
            this.Field6.Top = 0F;
            this.Field6.Width = 1.1875F;
            // 
            // Field7
            // 
            this.Field7.Height = 0.1875F;
            this.Field7.Left = 6.375F;
            this.Field7.Name = "Field7";
            this.Field7.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.Field7.Text = null;
            this.Field7.Top = 0F;
            this.Field7.Width = 1.125F;
            // 
            // Field8
            // 
            this.Field8.Height = 0.1875F;
            this.Field8.Left = 3.9375F;
            this.Field8.Name = "Field8";
            this.Field8.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.Field8.Text = null;
            this.Field8.Top = 0F;
            this.Field8.Width = 1.1875F;
            // 
            // Field9
            // 
            this.Field9.Height = 0.1875F;
            this.Field9.Left = 5.125F;
            this.Field9.Name = "Field9";
            this.Field9.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.Field9.Text = null;
            this.Field9.Top = 0F;
            this.Field9.Width = 1.25F;
            // 
            // lnTotals
            // 
            this.lnTotals.Height = 0F;
            this.lnTotals.Left = 2.75F;
            this.lnTotals.LineWeight = 1F;
            this.lnTotals.Name = "lnTotals";
            this.lnTotals.Top = 0F;
            this.lnTotals.Visible = false;
            this.lnTotals.Width = 4.75F;
            this.lnTotals.X1 = 2.75F;
            this.lnTotals.X2 = 7.5F;
            this.lnTotals.Y1 = 0F;
            this.lnTotals.Y2 = 0F;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblHeader,
            this.lblMuniName,
            this.lblTime,
            this.lblDate,
            this.lblPage,
            this.lblPrincipal,
            this.lblInterest,
            this.lblCosts,
            this.lblTotal,
            this.Line1,
            this.lblReportType});
            this.PageHeader.Height = 0.8125F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            // 
            // lblHeader
            // 
            this.lblHeader.Height = 0.25F;
            this.lblHeader.HyperLink = null;
            this.lblHeader.Left = 0F;
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" +
    "o-char-set: 0";
            this.lblHeader.Text = "Account Detail Report";
            this.lblHeader.Top = 0F;
            this.lblHeader.Width = 7.5F;
            // 
            // lblMuniName
            // 
            this.lblMuniName.Height = 0.1875F;
            this.lblMuniName.HyperLink = null;
            this.lblMuniName.Left = 0F;
            this.lblMuniName.Name = "lblMuniName";
            this.lblMuniName.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblMuniName.Text = null;
            this.lblMuniName.Top = 0F;
            this.lblMuniName.Width = 2.75F;
            // 
            // lblTime
            // 
            this.lblTime.Height = 0.1875F;
            this.lblTime.HyperLink = null;
            this.lblTime.Left = 0F;
            this.lblTime.Name = "lblTime";
            this.lblTime.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblTime.Text = null;
            this.lblTime.Top = 0.1875F;
            this.lblTime.Width = 1.25F;
            // 
            // lblDate
            // 
            this.lblDate.Height = 0.1875F;
            this.lblDate.HyperLink = null;
            this.lblDate.Left = 5.8125F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.lblDate.Text = null;
            this.lblDate.Top = 0F;
            this.lblDate.Width = 1.6875F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1875F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 5.8125F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.lblPage.Text = null;
            this.lblPage.Top = 0.1875F;
            this.lblPage.Width = 1.6875F;
            // 
            // lblPrincipal
            // 
            this.lblPrincipal.Height = 0.1875F;
            this.lblPrincipal.HyperLink = null;
            this.lblPrincipal.Left = 2.75F;
            this.lblPrincipal.Name = "lblPrincipal";
            this.lblPrincipal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.lblPrincipal.Text = "Principal";
            this.lblPrincipal.Top = 0.625F;
            this.lblPrincipal.Width = 1.1875F;
            // 
            // lblInterest
            // 
            this.lblInterest.Height = 0.1875F;
            this.lblInterest.HyperLink = null;
            this.lblInterest.Left = 3.9375F;
            this.lblInterest.Name = "lblInterest";
            this.lblInterest.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.lblInterest.Text = "Interest";
            this.lblInterest.Top = 0.625F;
            this.lblInterest.Width = 1.1875F;
            // 
            // lblCosts
            // 
            this.lblCosts.Height = 0.1875F;
            this.lblCosts.HyperLink = null;
            this.lblCosts.Left = 5.125F;
            this.lblCosts.Name = "lblCosts";
            this.lblCosts.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.lblCosts.Text = "Costs";
            this.lblCosts.Top = 0.625F;
            this.lblCosts.Width = 1.1875F;
            // 
            // lblTotal
            // 
            this.lblTotal.Height = 0.1875F;
            this.lblTotal.HyperLink = null;
            this.lblTotal.Left = 6.3125F;
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.lblTotal.Text = "Total";
            this.lblTotal.Top = 0.625F;
            this.lblTotal.Width = 1.1875F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.8125F;
            this.Line1.Width = 7.5F;
            this.Line1.X1 = 7.5F;
            this.Line1.X2 = 0F;
            this.Line1.Y1 = 0.8125F;
            this.Line1.Y2 = 0.8125F;
            // 
            // lblReportType
            // 
            this.lblReportType.Height = 0.375F;
            this.lblReportType.HyperLink = null;
            this.lblReportType.Left = 0F;
            this.lblReportType.Name = "lblReportType";
            this.lblReportType.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" +
    "o-char-set: 0";
            this.lblReportType.Text = null;
            this.lblReportType.Top = 0.25F;
            this.lblReportType.Width = 7.5F;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblDeletedAcct});
            this.PageFooter.Height = 0.28125F;
            this.PageFooter.Name = "PageFooter";
            // 
            // lblDeletedAcct
            // 
            this.lblDeletedAcct.Height = 0.1875F;
            this.lblDeletedAcct.HyperLink = null;
            this.lblDeletedAcct.Left = 0.0625F;
            this.lblDeletedAcct.Name = "lblDeletedAcct";
            this.lblDeletedAcct.Style = "font-family: \'Tahoma\'";
            this.lblDeletedAcct.Text = "! - Indicates Deleted Account";
            this.lblDeletedAcct.Top = 0.0625F;
            this.lblDeletedAcct.Width = 2.0625F;
            // 
            // rptStatusListAccountDetail
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.25F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.25F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.5F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportEndedAndCanceled += new System.EventHandler(this.rptStatusListAccountDetail_ReportEndedAndCanceled);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
            ((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMapLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBuilding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMapLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBuilding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExempt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldExempt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLand)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLand)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReference2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRef2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRef1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCosts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReportType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDeletedAcct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAddress;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLocation;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBuilding;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBuilding;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExempt;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLand;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReference2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRef2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReference;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRef1;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport srptSLAllActivityDetailOB;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnTotals;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblInterest;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCosts;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReportType;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDeletedAcct;
	}
}
