﻿namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	partial class rptPurgePayments
	{
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptPurgePayments));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldPrin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldInt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRecDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.fldPrin)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRecDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldPrin,
				this.fldInt,
				this.fldCost,
				this.fldRecDate
			});
			this.Detail.Height = 0.1770833F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldPrin
			// 
			this.fldPrin.Height = 0.1875F;
			this.fldPrin.Left = 1.875F;
			this.fldPrin.Name = "fldPrin";
			this.fldPrin.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldPrin.Text = null;
			this.fldPrin.Top = 0F;
			this.fldPrin.Width = 0.9375F;
			// 
			// fldInt
			// 
			this.fldInt.Height = 0.1875F;
			this.fldInt.Left = 2.8125F;
			this.fldInt.Name = "fldInt";
			this.fldInt.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldInt.Text = null;
			this.fldInt.Top = 0F;
			this.fldInt.Width = 0.9375F;
			// 
			// fldCost
			// 
			this.fldCost.Height = 0.1875F;
			this.fldCost.Left = 3.75F;
			this.fldCost.Name = "fldCost";
			this.fldCost.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldCost.Text = null;
			this.fldCost.Top = 0F;
			this.fldCost.Width = 0.9375F;
			// 
			// fldRecDate
			// 
			this.fldRecDate.Height = 0.1875F;
			this.fldRecDate.Left = 0.375F;
			this.fldRecDate.Name = "fldRecDate";
			this.fldRecDate.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldRecDate.Text = null;
			this.fldRecDate.Top = 0F;
			this.fldRecDate.Width = 1.5F;
			// 
			// rptPurgePayments
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			((System.ComponentModel.ISupportInitialize)(this.fldPrin)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRecDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrin;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRecDate;
	}
}
