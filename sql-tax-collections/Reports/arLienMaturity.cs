﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using System.IO;
using Global;
using fecherFoundation.Extensions;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class arLienMaturity : BaseSectionReport
	{
        //=========================================================
        // ********************************************************
        // PROPERTY OF TRIO SOFTWARE CORPORATION                  *
        // *
        // WRITTEN BY     :               Jim Bertolino           *
        // DATE           :               09/26/2002              *
        // *
        // MODIFIED BY    :               Jim Bertolino           *
        // LAST UPDATED   :               12/21/2006              *
        // ********************************************************
        private cAddressControllerCL tAddrCtrlr = new cAddressControllerCL();
        clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsCMFNumbers = new clsDRWrapper();
		string strSQL;
		string strText = "";
		// VBto upgrade warning: intYear As short --> As int	OnRead(string)
		int intYear;
		bool boolPayCert;
		bool boolMort;
		bool boolPayMortCert;
		bool boolPayNewOwner;
        short intBilledOwnerAddr; // trocl-1335 09.15.17 kjr
        clsDRWrapper rsCert;
		int lngCopies;
		// this is to keep track of the number of mortgage holder copies left
		bool boolCopy;
		// is this a copy or the original
		int intNewOwnerChoice;
		// this is if the user wants to send to any new owners 0 = Do not send any, 1 =
		clsDRWrapper rsMort = new clsDRWrapper();
		clsDRWrapper rsRE = new clsDRWrapper();
		clsDRWrapper rsRate = new clsDRWrapper();
		clsDRWrapper rsPayments = new clsDRWrapper();
		bool boolNewOwner;
		// this is true if there is a new owner
		bool boolAtLeastOneRecord;
		// this is true if there is at least one record to see
		string strAcctList = "";
		string strReportDesc = "";
		// this will be the string that will describe the parameters the user set for the report ie. Account 1 to 100
		bool boolREMatch;
		clsDRWrapper rsBook = new clsDRWrapper();
		int lngArrayIndex;
		string strDateCheck = "";
		DateTime dtInterestDate;
		int intLocationType;
		bool boolRemoveLastPage;
		int lngIPCopies;
		// this is to keep track of the number of Interested Party copies to print
		bool blnIP;
		bool blnPayIPCert;
		clsDRWrapper rsIntParty = new clsDRWrapper();
		int lngMortHolder;
		// Moved to Module level
		int lngIntParties;
		bool blnIsMortCopy;
		bool blnIsIPCopy;
		string strRateKeys = "";
		// kgk trocl-646 04-20-2011
		// these variables are for counting the accounts
		public int lngBalanceZero;
		public int lngBalanceNeg;
		public int lngBalancePos;
		public int lngDemandFeesApplied;
		string strBalanceZero = "";
		string strBalanceNeg = "";
		string strBalancePos = "";
		string strDemandFeesApplied = "";
        public bool ShowPaperSize { get; set; } = true;
		public arLienMaturity()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
            this.Name = "Lien Maturity Notice";

        }

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static arLienMaturity InstancePtr
		{
			get
			{
				return (arLienMaturity)Sys.GetInstance(typeof(arLienMaturity));
			}
		}

		protected arLienMaturity _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
                if (rsRate != null)
                    rsRate.Dispose();
                if (rsMort != null)
                    rsMort.Dispose();
                if (rsBook != null)
                    rsBook.Dispose();
                if (rsCMFNumbers != null)
                    rsCMFNumbers.Dispose();
                if (rsCert != null)
                    rsCert.Dispose();
            }
            base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				eArgs.EOF = rsData.EndOfFile();
				if (eArgs.EOF)
				{
					if (boolRemoveLastPage)
					{
						if (this.Document.Pages.Count > 0)
						{
							this.Document.Pages.RemoveAt(this.Document.Pages.Count - 1);
						}
						else
						{
							// there are no accounts eligible
							// lngDemandFeesApplied = 0
						}
						//this.Refresh();
					}
					EndReport();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MsgBoxStyle.Critical, "Fetch Data");
			}
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void EndReport()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will set all of the bill records to status of 1
				frmFreeReport.InstancePtr.vsSummary.TextMatrix(1, 1, FCConvert.ToString(lngBalanceZero));
				frmFreeReport.InstancePtr.vsSummary.TextMatrix(2, 1, FCConvert.ToString(lngBalanceNeg));
				frmFreeReport.InstancePtr.vsSummary.TextMatrix(3, 1, FCConvert.ToString(lngBalancePos));
				frmFreeReport.InstancePtr.vsSummary.TextMatrix(4, 1, FCConvert.ToString(lngBalanceZero + lngBalanceNeg + lngBalancePos));
				// this will take the trailing comma off
				if (strBalanceZero.Length > 0)
				{
					strBalanceZero = Strings.Left(strBalanceZero, strBalanceZero.Length - 1);
				}
				if (strBalanceNeg.Length > 0)
				{
					strBalanceNeg = Strings.Left(strBalanceNeg, strBalanceNeg.Length - 1);
				}
				if (strBalancePos.Length > 0)
				{
					strBalancePos = Strings.Left(strBalancePos, strBalancePos.Length - 1);
				}
				if (strDemandFeesApplied.Length > 0)
				{
					strDemandFeesApplied = Strings.Left(strDemandFeesApplied, strDemandFeesApplied.Length - 1);
				}
				// set the strings of accounts for the return grid to show
				frmFreeReport.InstancePtr.vsSummary.TextMatrix(1, 2, strBalanceZero);
				frmFreeReport.InstancePtr.vsSummary.TextMatrix(2, 2, strBalanceNeg);
				frmFreeReport.InstancePtr.vsSummary.TextMatrix(3, 2, strBalancePos);
				frmFreeReport.InstancePtr.vsSummary.TextMatrix(4, 2, strDemandFeesApplied);
				if (lngDemandFeesApplied > 0)
				{
					// show a special label that is highlighted to show this information
					// frmFreeReport.vsSummary.AddItem ""
					// frmFreeReport.vsSummary.AddItem ""
					if ((((frmFreeReport.InstancePtr.vsSummary.Rows - 1) * frmFreeReport.InstancePtr.vsSummary.RowHeight(1)) + 70) < (frmFreeReport.InstancePtr.fraSummary.Height - 200))
					{
						frmFreeReport.InstancePtr.vsSummary.Height = ((frmFreeReport.InstancePtr.vsSummary.Rows - 1) * frmFreeReport.InstancePtr.vsSummary.RowHeight(1)) + 70;
					}
					else
					{
						frmFreeReport.InstancePtr.vsSummary.Height = frmFreeReport.InstancePtr.fraSummary.Height - 200;
					}
				}
				frmWait.InstancePtr.Unload();
				frmFreeReport.InstancePtr.Focus();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MsgBoxStyle.Critical, "Report End");
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				int intDefSel = 0;
				string strTemp = "";
				FCUtils.EraseSafe(modGlobal.Statics.arrDemand);
				lblDate.Left = imgTownSeal.Width;
				lblDate.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				frmWait.InstancePtr.Unload();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				// kk04182014 trocl-1156  Select paper size beforehand since the Printer is selected later
				// kk09192014 trocl-1156  Save and load the selection as the default, default to Letter size
				modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL", "LienMaturityPaperSize", ref strTemp);
				if (strTemp == "Legal")
				{
					intDefSel = 1;
				}
				else
				{
					intDefSel = 0;
				}

                if (ShowPaperSize)
                {
                    frmQuestion.InstancePtr.Init(10, "Letter (8.5 x 11)", "Legal (8.5 x 14)", "Select Paper Size",
                        intDefSel);
                    if (modGlobal.Statics.gintPassQuestion == 0)
                    {
                        strTemp = "Letter";
                    }
                    else
                    {
                        strTemp = "Legal";
                    }
                }
                else
                {
                    strTemp = "Letter";
                }

                modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "LienMaturityPaperSize", strTemp);
				if (modGlobal.Statics.gintPassQuestion == 0)
				{
					this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
					// Letter size
				}
				else
				{
					this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Legal;
					// Legal size
				}
				frmQuestion.InstancePtr.Unload();
				//Application.DoEvents();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				frmWait.InstancePtr.Show();
				// kk02042016 trout-1197  1.5" top 1st page, 1.5" bottom last page, 0.75" left and right margins
				this.PageSettings.Margins.Top = 1.5F + FCConvert.ToSingle(modGlobal.Statics.gdblLienAdjustmentTop);
				this.PageSettings.Margins.Bottom = 1.5F + FCConvert.ToSingle(modGlobal.Statics.gdblLienAdjustmentBottom);
				// kk04212014 trocl-1156
				this.PageSettings.Margins.Left = 0.75F + FCConvert.ToSingle(modGlobal.Statics.gdblLienAdjustmentSide);
				this.PageSettings.Margins.Right = this.PageSettings.Margins.Left;
				this.PrintWidth = 8.5F - (this.PageSettings.Margins.Left + this.PageSettings.Margins.Right);
				rtbText.Width = this.PrintWidth;
				//this.Visible = false; // this will have to be set back to true to be shown
				if (Strings.Trim(modGlobalConstants.Statics.gstrTownSealPath) != "" && modCLCalculations.Statics.gboolCLUseTownSealLienMat)
				{
					// MAL@20080602: Add check for file existence
					// Tracker Reference: 14008
					if (File.Exists(modGlobalConstants.Statics.gstrTownSealPath))
					{
						imgTownSeal.Visible = true;
						imgTownSeal.Image = FCUtils.LoadPicture(modGlobalConstants.Statics.gstrTownSealPath);
					}
					else
					{
						imgTownSeal.Visible = false;
					}
				}
				else
				{
					imgTownSeal.Visible = false;
				}
				SetHeaderFooter(true);
				lngBalanceNeg = 0;
				lngBalancePos = 0;
				lngBalanceZero = 0;
				lngDemandFeesApplied = 0;
				if (Strings.UCase(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[36].RowNumber, 1), 7)) == Strings.UCase("Show al"))
				{
					intLocationType = 0;
				}
				else if (Strings.UCase(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[36].RowNumber, 1), 7)) == Strings.UCase("Show lo"))
				{
					intLocationType = 1;
				}
				else if (Strings.UCase(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[36].RowNumber, 1), 7)) == Strings.UCase("Do not "))
				{
					intLocationType = 2;
				}
				boolPayCert = FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[18].RowNumber, 1), 2) == "Ye");
				boolMort = FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[30].RowNumber, 1), 2) == "Ye");
				blnIP = FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1), 2) == "Ye");
                if (Strings.Mid(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[17].RowNumber, 1), 29, 4).ToUpper() == "LAST")
                { // trocl-1335 09.11.2017 kjr, 5.23.18
                    intBilledOwnerAddr = 2;
                }
                else
                {
                    if (Strings.Mid(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[17].RowNumber, 1), 27, 3) == "C/O")
                    {
                        intBilledOwnerAddr = 1;
                    }
                    else
                    {
                        intBilledOwnerAddr = 0;
                    }
                }
                boolPayNewOwner = false;
				if (Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[34].RowNumber, 1), 2) == "Ye")
				{
					if (Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[34].RowNumber, 1), 7) == "Yes, wi")
					{
						intNewOwnerChoice = 1;
					}
					else
					{
						intNewOwnerChoice = 2;
						boolPayNewOwner = true;
					}
				}
				else
				{
					intNewOwnerChoice = 0;
				}
				boolNewOwner = false;
				if (boolMort)
				{
					boolPayMortCert = !FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[30].RowNumber, 1), 9) == "Yes, with");
				}
				else
				{
					boolPayMortCert = false;
				}
				if (blnIP)
				{
					blnPayIPCert = !FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1), 9) == "Yes, with");
				}
				else
				{
					blnPayIPCert = false;
				}
				// this will add a picture of the signature of the treasurer to the 30 Day Notice
				if (modGlobal.Statics.gboolUseSigFile)
				{
					imgSig.Visible = true;
					//imgSig.ZOrder(1);
					//Line2.ZOrder(0);
					imgSig.Image = FCUtils.LoadPicture(modSignatureFile.Statics.gstrTreasSigPath);
					// imgSig.PictureAlignment = ddPALeft
				}
				else
				{
					imgSig.Visible = false;
				}
				// If gboolRE Then
				rsRE.OpenRecordset("SELECT * FROM MASTER", modExtraModules.strREDatabase);
				rsRE.InsertName("OwnerPartyID,SecOwnerPartyID", "Own1,Own2", false, true, true, "", false, "", true, "");
				rsBook.OpenRecordset("SELECT * FROM BookPage ORDER BY Line", modExtraModules.strREDatabase);
				// End If
				rsRate.OpenRecordset("SELECT * FROM RateRec", modExtraModules.strCLDatabase);
				//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
				intYear = FCConvert.ToInt32(Math.Round(Conversion.Val(modExtraModules.FormatYear(frmRateRecChoice.InstancePtr.cmbYear.Text))));
				strSQL = SetupSQL();
				rsData.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
				if (rsData.EndOfFile() != true)
				{
					// lngMaxNumber = rsData.RecordCount
					rsCMFNumbers.OpenRecordset("SELECT * FROM CMFNumbers", modExtraModules.strCLDatabase);
					// lblDate.Text = PadToString(rsData.Fields("Account"), 6)    'this will show the account number in the top right of the page
				}
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Data", true, rsData.RecordCount());
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MsgBoxStyle.Critical, "Report Start");
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			try
			{
                // On Error GoTo ERROR_HANDLER
                // MAL@20080723: Add unload to make sure that the summary report "refreshes"
                // Tracker Reference: 14749
                //rptLienNoticeSummary.InstancePtr.Hide();
                // this will create another active report with the summary information on it so the user can print if the feel like it
                fecherFoundation.Sys.ClearInstance(frmReportViewer.InstancePtr);
                if (Strings.Right(strAcctList, 1) == ",")
                {
                    strAcctList = Strings.Left(strAcctList, strAcctList.Length - 1);
                }
                rptLienNoticeSummary.InstancePtr.Unload();
                rptLienNoticeSummary.InstancePtr.Init(strAcctList, "Lien Maturity Notice Summary", strReportDesc, intYear, dtInterestDate, 2, strRateKeys, FCConvert.CBool(frmFreeReport.InstancePtr.cmbSortOrder.SelectedIndex == 0), intBilledOwnerAddr);
				// kgk trocl-646 04-20-2011  add ratekeys for Lien Maturity Summary
				frmFreeReport.InstancePtr.Unload();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MsgBoxStyle.Critical, "Report Terminate");
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this sub will put all of the information into the string
				if (rsData.EndOfFile() != true)
				{
					boolRemoveLastPage = false;
					// calculate fields
					CalculateVariableTotals();
					if (rsData.EndOfFile())
					{
						boolRemoveLastPage = true;
						return;
					}
					frmWait.InstancePtr.IncrementProgress();
					if (modGlobal.Statics.gboolUseSigFile)
					{
						// kk02052016 trout-1197  Rework lien notice. Change the way signature line is located.
						// The adjustments are applied before the richtext section is expanded, so we have to limit the adjustment amount.
						// If the signature moves into the rtbText area or above it, the signature will not be pushed down when the rtbText is expanded.
						if (modGlobal.Statics.gdblLienSigAdjust < 0)
						{
							// The digital signature starts out directly below the rich text box
							imgSig.Top = rtbText.Top + rtbText.Height;
							// Don't allow it to go any higher
						}
						else
						{
							if (imgSig.Top + modGlobal.Statics.gdblLienSigAdjust < Line2.Y1)
							{
								// The farthest it can move is so the top is even with the signature line
								imgSig.Top += FCConvert.ToSingle(modGlobal.Statics.gdblLienSigAdjust);
							}
							else
							{
								imgSig.Top = Line2.Y1;
							}
						}
					}
					SetHeaderFooter();
					// this will set the main string
					//FC:FINAL:DSE WordWrapping not working in RichTextBox
					//rtbText.Text = SetupVariablesInString(frmFreeReport.InstancePtr.rtbData.Text);
					rtbText.SetHtmlText(SetupVariablesInString(frmFreeReport.InstancePtr.rtbData.Text));
					//rtbText.SelStart = 0;
					//rtbText.SelLength = rtbText.Text.Length;
					//rtbText.SelectedText = rtbText.Text;
					//rtbText.Font = new Font("Courier New", rtbText.Font.Size);
					if (lngCopies > 1)
					{
						// this is to produce multiple copies for certain parties (mortgage holders)
						lngCopies -= 1;
						boolCopy = true;
						// Determine if the next will be mortgage holder copy or interested party copy
						// MAL@20080506: Changed to recalc copies correctly
						// Tracker Reference: 13621
						// If (lngMortHolder - 1) > 0 And lngCopies >= (lngMortHolder - 1) Then
						if (lngMortHolder > 0 && lngCopies >= lngMortHolder)
						{
							lngMortHolder -= 1;
							blnIsMortCopy = true;
							blnIsIPCopy = false;
						}
						else if (lngIntParties > 0 && lngCopies >= lngIntParties)
						{
							lngIntParties -= 1;
							blnIsMortCopy = false;
							blnIsIPCopy = true;
						}
						else
						{
							blnIsMortCopy = false;
							blnIsIPCopy = false;
						}
					}
					else
					{
						rsData.MoveNext();
						boolCopy = false;
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MsgBoxStyle.Critical, "Error Binding Fields");
			}
		}

		private void SetHeaderFooter(bool boolFirstRun = false)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will set the header values
				if (boolFirstRun)
				{
					lblReportHeader.Text = "State of Maine" + "\r\n" + "Notice of Impending Automatic Foreclosure" + "\r\n" + GetVariableValue_2("LEGALDESCRIPTION");
					this.Name = "Lien Maturity Notice";
				}
				else
				{
					if (boolCopy)
					{
						lblReportHeader.Text = "State of Maine" + "\r\n" + "Notice of Impending Automatic Foreclosure" + "\r\n" + " * * * COPY * * * " + GetVariableValue_2("LEGALDESCRIPTION") + " * * * COPY * * * ";
					}
					else
					{
						lblReportHeader.Text = "State of Maine" + "\r\n" + "Notice of Impending Automatic Foreclosure" + "\r\n" + GetVariableValue_2("LEGALDESCRIPTION");
					}
					// this will set the footer values
					// fldCollector.Text = GetVariableValue("COLLECTOR")
					fldCollector.Text = GetVariableValue_2("SIGNER");
					fldTitle.Text = GetVariableValue_2("JOP");
					if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
					{
						fldMuni.Text = GetVariableValue_2("CITYTOWNOF") + " of " + GetVariableValue_2("MUNI");
					}
					else
					{
						fldMuni.Text = GetVariableValue_2("MUNI");
					}
					fldPrincipal.Text = Strings.Format(GetVariableValue_2("PRINCIPAL"), "#,##0.00");
					fldInterest.Text = Strings.Format(GetVariableValue_2("INTEREST"), "#,##0.00");
					fldDemand.Text = Strings.Format(GetVariableValue_2("DEMAND"), "#,##0.00");
					fldFilingFee.Text = Strings.Format(GetVariableValue_2("FORECLOSUREFEE"), "#,##0.00");
					fldCertMailFee.Text = Strings.Format(GetVariableValue_2("CERTTOTAL"), "#,##0.00");
					if (Strings.Trim(fldPrincipal.Text) == "")
					{
						fldPrincipal.Text = "0.00";
					}
					if (Strings.Trim(fldInterest.Text) == "")
					{
						fldInterest.Text = "0.00";
					}
					if (Strings.Trim(fldDemand.Text) == "")
					{
						fldDemand.Text = "0.00";
					}
					if (Strings.Trim(fldCertMailFee.Text) == "")
					{
						fldCertMailFee.Text = "0.00";
					}
					if (Strings.Trim(FCConvert.ToString(FCConvert.ToDouble(fldFilingFee.Text))) == "")
					{
						fldFilingFee.Text = "0.00";
					}
					fldTotal.Text = Strings.Format(FCConvert.ToDouble(fldPrincipal.Text) + FCConvert.ToDouble(fldInterest.Text) + FCConvert.ToDouble(fldDemand.Text) + FCConvert.ToDouble(fldCertMailFee.Text) + FCConvert.ToDouble(fldFilingFee.Text), "#,##0.00");
					fldCounty.Text = "County of " + GetVariableValue_2("COUNTY");
					if (modGlobal.Statics.gboolUseMailDateForMaturity)
					{
						fldBottomMessage.Text = "Amount due as of " + GetVariableValue_2("MAILDATE") + ".  Please call " + GetVariableValue_2("SIGNERPHONE") + " for the amount due on any desired payment date.";
						// GetVariableValue("COLLECTORPHONE")'contact the Treasurer at
					}
					else
					{
						fldBottomMessage.Text = "Amount due as of " + GetVariableValue_2("FORECLOSUREDATE") + ".  Please call " + GetVariableValue_2("SIGNERPHONE") + " for the amount due on any desired payment date.";
						// GetVariableValue("COLLECTORPHONE")'contact the Treasurer at
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MsgBoxStyle.Critical, "Setting Up Header/Footer");
			}
		}

		private string SetupSQL()
		{
			string SetupSQL = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will return the SQL statement for this batch of reports
				string strWhereClause;
				int intCT;
				string strOrderBy = "";
				string strRK = "";
				strWhereClause = "";
				strReportDesc = "";
				// MAL@20080702: Add new option for sort order
				// Tracker Reference: 11834
				if (frmFreeReport.InstancePtr.cmbSortOrder.SelectedIndex == 0)
				{
					strOrderBy = " ORDER BY Account, Name1";
				}
				else
				{
					strOrderBy = " ORDER BY Name1, Account";
				}
				// this will select the correct ratekeys
				for (intCT = 1; intCT <= frmRateRecChoice.InstancePtr.vsRate.Rows - 1; intCT++)
				{
					// strWhereClause = " AND (LienRec.RateKey = " & frmRateRecChoice.lngRateRecNumber & " OR BillingMaster.RateKey = " & frmRateRecChoice.lngRateRecNumber & ")"
					if (Conversion.Val(frmRateRecChoice.InstancePtr.vsRate.TextMatrix(intCT, 0)) == -1)
					{
						if (Strings.Trim(strWhereClause) == "")
						{
							strWhereClause = " AND (";
							strWhereClause += "LienRec.RateKey = " + frmRateRecChoice.InstancePtr.vsRate.TextMatrix(intCT, 6) + " OR BillingMaster.RateKey = " + frmRateRecChoice.InstancePtr.vsRate.TextMatrix(intCT, 6);
						}
						else
						{
							strWhereClause += " OR LienRec.RateKey = " + frmRateRecChoice.InstancePtr.vsRate.TextMatrix(intCT, 6) + " OR BillingMaster.RateKey = " + frmRateRecChoice.InstancePtr.vsRate.TextMatrix(intCT, 6);
						}
					}
				}
				if (Strings.Trim(strWhereClause) != "")
				{
					strWhereClause += ")";
				}
				strRateKeys = strWhereClause;
				// kgk trocl-646 04-20-2011  add ratekeys for Lien Maturity Summary
				if (frmRateRecChoice.InstancePtr.cmbRange.SelectedIndex == 2)
				{
					// range of accounts
					// strOrderBy = " ORDER BY Name1, Account"
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[0].Text) != "")
					{
						if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
						{
							// both full
							strWhereClause = " AND Account >= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text)) + " AND Account <= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
							strReportDesc = "Account " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text)) + " To " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
						}
						else
						{
							// first full second empty
							strWhereClause = " AND Account >= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text));
							strReportDesc = "Account Above " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text));
						}
					}
					else
					{
						if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
						{
							// first empty second full
							strWhereClause = " AND Account <= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
							strReportDesc = "Account Below " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
						}
						else
						{
							// both empty
							strWhereClause = "";
							strReportDesc = "";
						}
					}
				}
				else if (frmRateRecChoice.InstancePtr.cmbRange.SelectedIndex == 1)
				{
					// range of names
					// strOrderBy = " ORDER BY Name1, Account"
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[0].Text) != "")
					{
						if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
						{
							// both full
							strWhereClause = " AND Name1 >= '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "' AND Name1 <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "ZZZ'";
							strReportDesc = "Name " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text)) + " To " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
						}
						else
						{
							// first full second empty
							strWhereClause = " AND Name1 >= '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "'";
							strReportDesc = "Name Above " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text));
						}
					}
					else
					{
						if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
						{
							// first empty second full
							strWhereClause = " AND Name1 <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "'";
							strReportDesc = "Account Below " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text));
						}
						else
						{
							// both empty
							strWhereClause = "";
							strReportDesc = "";
						}
					}
				}
				else
				{
					// strOrderBy = " ORDER BY Name1, Account"
				}
				// create the string for the minimum amount
				// MAL@20071012: Corrected so that it always takes Minimum Amount into account (even if zero)
				// Call Reference: 10853
				// If frmFreeReport.dblMinimumAmount > 0 Then
				strWhereClause += " AND (Principal - LienRec.PrincipalPaid) > " + FCConvert.ToString(frmFreeReport.InstancePtr.dblMinimumAmount);
				// End If
				// rsData.CreateStoredProcedure "LienMaturityQuery", "SELECT * From BillingMaster WHERE BillingType = 'RE' AND (LienStatusEligibility = 4 OR LienStatusEligibility= 5) AND BillingYear\10 = " & intYear & strWhereClause & strOrderBy 'AND LienProcessStatus  = 5
				// kgk 2-14-2012  Change from Stored Proc  rsData.CreateStoredProcedure "LienMaturityQuery", "SELECT *, BillingMaster.RateKey as BillingMasterRateKey, LienRec.RateKey as LienRecRateKey FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.LienRecordNumber WHERE BillingType = 'RE' AND NOT (LienProcessExclusion = 1) AND (LienStatusEligibility = 4 OR LienStatusEligibility= 5) AND BillingYear/10 = " & intYear & strWhereClause & strOrderBy
				// SetupSQL = "SELECT * FROM LienMaturityQuery"
				SetupSQL = "SELECT *, BillingMaster.RateKey as BillingMasterRateKey, LienRec.RateKey as LienRecRateKey FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.ID WHERE BillingType = 'RE' AND NOT (LienProcessExclusion = 1) AND (LienStatusEligibility = 4 OR LienStatusEligibility= 5) AND BillingYear/10 = " + FCConvert.ToString(intYear) + strWhereClause + strOrderBy;
				return SetupSQL;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MsgBoxStyle.Critical, "Setup SQL");
			}
			return SetupSQL;
		}

		private string SetupVariablesInString(string strOriginal)
		{
			string SetupVariablesInString = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will replace all of the variables with the information needed by recursively
				// calling this function to replace one variable each time
				string strBuildString;
				// this is the string that will be built and returned at the end
				string strTemp = "";
				// this is a temporary sting that will be used to store and transfer string segments
				int lngNextVariable;
				// this is the position of the beginning of the next variable (0 = no more variables)
				int lngEndOfLastVariable;
				// this is the position of the end of the last variable
				lngEndOfLastVariable = 0;
				lngNextVariable = 1;
				strBuildString = "";
				// priming read
				if (Strings.InStr(1, strOriginal, "<") > 0)
				{
					lngNextVariable = Strings.InStr(lngEndOfLastVariable + 1, strOriginal, "<");
					// do until there are no more variables left
					do
					{
						// add the string from lngEndOfLastVariable - lngNextVariable to the BuildString
						strBuildString += Strings.Mid(strOriginal, lngEndOfLastVariable + 1, lngNextVariable - lngEndOfLastVariable - 1);
						// set the end pointer
						lngEndOfLastVariable = Strings.InStr(lngNextVariable, strOriginal, ">");
						// replace the variable
						strBuildString += GetVariableValue_2(Strings.Mid(strOriginal, lngNextVariable + 1, lngEndOfLastVariable - lngNextVariable - 1));
						// check for another variable
						lngNextVariable = Strings.InStr(lngEndOfLastVariable, strOriginal, "<");
					}
					while (!(lngNextVariable == 0));
				}
				// take the last of the string and add it to the end
				strBuildString += Strings.Mid(strOriginal, lngEndOfLastVariable + 1);
				// check for variables
				if (Strings.InStr(1, "<", strOriginal) > 0)
				{
					// strBuildString = SetupVariablesInString(strBuildString)     'setup recursion
					FCMessageBox.Show("ERROR: There are still variables in the report string.", MsgBoxStyle.Critical, "SetupVariablesInString Error");
				}
				else
				{
					SetupVariablesInString = strBuildString;
				}
				return SetupVariablesInString;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MsgBoxStyle.Critical, "Setup Variables In String");
			}
			return SetupVariablesInString;
		}

		private string GetVariableValue_2(string strVarName)
		{
			return GetVariableValue(ref strVarName);
		}

		private string GetVariableValue(ref string strVarName)
		{
			string GetVariableValue = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will take a variable name and find it in the array, then get the value and return it as a string
				int intCT;
				string strValue = "";
				// this is a dummy code to show the user where the hard codes are
				if (strVarName == "CRLF")
				{
					GetVariableValue = "";
					return GetVariableValue;
				}
				//FC:FINAL:KS: #without nr.:fix Exit For inside switch/case statement
				bool endFor = false;
				for (intCT = 0; intCT <= modRhymalReporting.MAXFREEVARIABLES; intCT++)
				{
					if (Strings.UCase(strVarName) == Strings.UCase(modRhymalReporting.Statics.frfFreeReport[intCT].Tag))
					{
						switch (modRhymalReporting.Statics.frfFreeReport[intCT].VariableType)
						{
							case 0:
								{
									// value from the static variables in the grid
									switch (modRhymalReporting.Statics.frfFreeReport[intCT].Type)
									{
									// 0 = Text, 1 = Number, 2 = Date, 3 = Do not ask for default (comes from DB), 4 = Formatted Year
										case 0:
											{
												strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
												break;
											}
										case 1:
											{
												strValue = Strings.Format(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1), "#,##0.00");
												break;
											}
										case 2:
											{
												strValue = Strings.Format(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1), "MMMM d, yyyy");
												break;
											}
										case 3:
											{
												strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
												break;
											}
										case 4:
											{
												strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
												break;
											}
									}
									//end switch
									//break;
									//FC:FINAL:KS: #without nr.:fix Exit For inside switch/case statement
									endFor = true;
									break;
								}
							case 1:
								{
									// value from the dynamic variables in the database
									strValue = FCConvert.ToString(rsData.Get_Fields(modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName));
									if (Strings.Trim(strValue) == "")
									{
										if (Strings.UCase(modRhymalReporting.Statics.frfFreeReport[intCT].Tag) != "LESSPAYMENTS")
										{
											// maybe put a blank line there
											strValue = "__________";
										}
									}
									break;
									//FC:FINAL:KS: #without nr.:fix Exit For inside switch/case statement
									endFor = true;
									//break;
								}
							case 2:
								{
									// questions at the bottom of the grid
									strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
									//break;
									//FC:FINAL:KS: #without nr.:fix Exit For inside switch/case statement
									endFor = true;
									break;
								}
							case 3:
								{
									// calculated values that are stored in the DatabaseFieldName field
									strValue = modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName;
									if (Strings.Trim(strValue) == "")
									{
										if (Strings.UCase(modRhymalReporting.Statics.frfFreeReport[intCT].Tag) != "LESSPAYMENTS")
										{
											// maybe put a blank line there
											strValue = "__________";
										}
									}
									//FC:FINAL:KS: #without nr.:fix Exit For inside switch/case statement
									endFor = true;
									//break;
									break;
								}
						}
						//end switch
						//FC:FINAL:KS: #without nr.:fix Exit For inside switch/case statement
						if (endFor)
						{
							break;
						}
					}
				}
				GetVariableValue = strValue;
				return GetVariableValue;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MsgBoxStyle.Critical, "Get Variable Values");
			}
			return GetVariableValue;
		}

        private void CalculateVariableTotals()
        {
            try
            {   // On Error GoTo ERROR_HANDLER
                //fecherFoundation.Information.Err(ex).Clear();
                // this will calculate all of the fields that need it and store
                // the value in the DatabaseFieldName string in the frfFreeReport
                // struct that these fields include principal, interest, costs,
                // total due and certified mail fee

                clsDRWrapper rsRK = new clsDRWrapper();
                DateTime dtBillDate;
                clsDRWrapper rsLN = new clsDRWrapper();
                double dblTotalDue = 0;
                double dblInt;
                double dblPrin;
                double dblPayments;
                double dblDemand;
                double dblCertMailFee;
                // Dim lngMortHolder           As Long        'MAL@20080507: Commented out due to duplicate declaration
                string strAddressBar = "";
                string strCommitmentDate = "";
                string strLessPaymentsLine = "";
                string strLocation = "";
                string strTemp1 = "";
                string strTemp2 = "";
                string strTemp3 = "";
                string strTemp4 = "";
                string strTemp5 = "";
                string strOwnerName = "";
                string strMapLot = "";
                string strBookPage = "";
                double dblFCFee = 0;
                bool boolSameName = false;

                // for CMFNumbers
                string strCMFName = "";
                int lngCMFBillKey = 0;
                int lngCMFMHNumber = 0;
                bool boolCMFNewOwner;
                bool boolPrintCMF; // true if a CMF should be printed for that person
                string strCMFAddr1;
                string strCMFAddr2;
                string strCMFAddr3;
                string strCMFAddr4;
                int lngChargeCopies = 0;

                clsDRWrapper rsTemp = new clsDRWrapper();
                // MAL@20080514: Tracker Reference: 13621
                string strRSName1 = "";
                string strRSName2 = "";
                string strRSAddr1 = "";
                string strRSAddr2 = "";
                string strRSAddr3 = "";
                string strRSAddr4 = "";
                // vbPorter upgrade warning: intAddr1Width As short	OnWriteFCConvert.ToDouble(
                int intAddr1Width = 0;
				int intAddr2Left;
				int intAddr2Width = 0;

                // trocl-1335 09.11.17 kjr  4.23.18 CODE FREEZE
                bool boolNewAddress = false;
                string strPrevSecOwnerName = "";
                string strPrevAddress1 = "";
                string strPrevAddress2 = "";
                string strPrevAddress3 = "";

                TryAgain:;
                strCMFAddr1 = "";
                strCMFAddr2 = "";
                strCMFAddr3 = "";
                strCMFAddr4 = "";

                // MAL@20080812: Reset values for each account
                // Tracker Reference: 14980
                dblInt = 0;
                dblPrin = 0;
                dblPayments = 0;
                dblDemand = 0;
                dblCertMailFee = 0;

                boolPrintCMF = true;
                if (!boolCopy)
                {
                    // this will calculate the principal, interest and total due
                    if (FCConvert.ToInt32(rsData.Get_Fields("LienRecordNumber")) != 0)
                    {
                        rsLN.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + rsData.Get_Fields("LienRecordNumber"));
                        if (modGlobal.Statics.gboolUseMailDateForMaturity)
                        {
                            dtInterestDate = fecherFoundation.DateAndTime.DateValue(GetVariableValue_2("MAILDATE"));
                        }
                        else
                        {
                            dtInterestDate = fecherFoundation.DateAndTime.DateValue(GetVariableValue_2("FORECLOSUREDATE"));
                        }
                        dblTotalDue = modCLCalculations.CalculateAccountCLLien(rsLN, dtInterestDate, ref dblInt);

                        dblInt += Conversion.Val(rsLN.Get_Fields("Interest") - rsLN.Get_Fields("InterestCharged") - rsLN.Get_Fields("InterestPaid") - rsLN.Get_Fields("PLIPaid"));
                    }
                    else
                    {
                        rsData.MoveNext();
                        if (rsData.EndOfFile()) return;
                        goto TryAgain;
                    }

                    if (modGlobal.Round( dblTotalDue, 2) == 0)
                    {
                        lngBalanceZero += 1;
                        strBalanceZero += rsData.Get_Fields("ID") + ", ";
                        rsData.MoveNext();
                        if (rsData.EndOfFile()) return;
                        goto TryAgain;
                    }

                    if (modGlobal.Round(Conversion.Val(rsData.Get_Fields("MaturityFee")), 2) != 0)
                    {
                        rsData.MoveNext();
                        if (rsData.EndOfFile()) return;
                        goto TryAgain;
                    }

                    if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Name2"))) != "")
                    {
                        strOwnerName = rsData.Get_Fields("Name1") + " and " + rsData.Get_Fields("Name2");
                    }
                    else
                    {
                        strOwnerName = FCConvert.ToString(rsData.Get_Fields("Name1"));
                    }

                    rsRE.FindFirstRecord("RSAccount", rsData.Get_Fields("Account"));

                    rsRK.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + rsData.Get_Fields("BillingMasterRateKey"), modExtraModules.strCLDatabase);
                    if (!rsRK.EndOfFile())
                    {
                        dtBillDate = (DateTime)rsRK.Get_Fields("CommitmentDate");
                    }
                    else
                    {
                        dtBillDate = (DateTime)rsData.Get_Fields("TransferFromBillingDateFirst");
                    }
                    // MAL@20071204: Set starting copies
                    lngCopies = 1;
                    // check to see if one is going to be sent to the next owner
                    if (intNewOwnerChoice != 0 && modCLCalculations.NewOwner(rsData.Get_Fields("Name1"), rsData.Get_Fields("Account"),dtBillDate,  ref boolREMatch,  ref boolSameName))
                    {
                        // if there has been a new owner, then make a copy for the new owner as well
                        if (!boolNewOwner)
                        {
                            if (boolPayNewOwner)
                            { // charge for the new owner's copy
                                dblCertMailFee += FCConvert.ToDouble(GetVariableValue_2("MAILFEE"));
                            }
                            lngCopies += 1;
                            boolNewOwner = true;
                        }
                    }

                    if (boolREMatch)
                    {
                        if (intLocationType != 2)
                        {
                            if (FCConvert.ToInt32(rsData.Get_Fields("streetnumber")) != 0 && fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("streetnumber"))) != "")
                            {
                                if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("StreetName"))) != "")
                                {
                                    strLocation = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("streetnumber"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("StreetName")));
                                }
                                else
                                {
                                    strLocation = "";
                                }
                            }
                            else
                            {
                                if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("StreetName"))) != "")
                                {
                                    strLocation = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("StreetName")));
                                }
                                else
                                {
                                    strLocation = "";
                                }
                            }
                        }
                        else
                        {
                            strLocation = "";
                        }
                        strMapLot = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("MapLot")));

                        if (FCConvert.ToBoolean(rsRE.Get_Fields("InBankruptcy")))
                        { // Dave 9/24/07 Don't skip Tax Acquired  Or rsRE.Fields("TaxAcquired")
                          // skip this account because it is in bankruptcy
                            rsData.MoveNext();
                            if (rsData.EndOfFile()) return;
                            goto TryAgain;
                        }
                    }
                    else
                    {
                        strLocation = "";
                        strMapLot = "";
                    }

                    if (dblTotalDue > 0)
                    {
                        // these are the account that will be demanded
                        lngBalancePos += 1;
                        strAcctList += rsData.Get_Fields("Account") + ",";
                        strBalancePos += rsData.Get_Fields("ID") + ", ";
                    }
                    else
                    {
                        if (dblTotalDue < 0)
                        {
                            lngBalanceNeg += 1;
                            strBalanceNeg += rsData.Get_Fields("ID") + ", ";
                        }
                    }

                    // this will find out how many mortgage holders this account has and how many copies to print
                    if (boolMort)
                    {
                        lngMortHolder = CalculateMortgageHolders();
                        // MAL@20071204: Removed this as the new owner copies have already been added to lngCopies
                        lngCopies += lngMortHolder;
                    }
                    else
                    {
                        lngMortHolder = 0;
                        lngCopies = lngCopies;
                    }

                    // this will find out the certified mail fee
                    if (boolPayCert)
                    {
                        if (boolPayMortCert)
                        {
                            // dblCertMailFee = lngMortHolder * CDbl(GetVariableValue("MAILFEE"))
                            dblCertMailFee = (lngMortHolder * FCConvert.ToDouble(GetVariableValue_2("MAILFEE"))) + FCConvert.ToDouble(GetVariableValue_2("MAILFEE"));
                            lngChargeCopies = lngCopies;
                        }
                        else
                        {
                            dblCertMailFee = FCConvert.ToDouble(GetVariableValue_2("MAILFEE"));
                            lngChargeCopies = lngCopies - lngMortHolder;
                        }
                    }
                    else
                    {
                        if (boolPayMortCert)
                        {
                            dblCertMailFee = lngMortHolder * FCConvert.ToDouble(GetVariableValue_2("MAILFEE"));
                            lngChargeCopies = lngMortHolder;
                        }
                        else
                        {
                            dblCertMailFee = 0;
                            lngChargeCopies = 0;
                        }
                    }

                    // this will find out how many interested parties this account has and how many copies to print
                    if (blnIP)
                    {
                        lngIntParties = modCLCalculations.CalculateInterestedParties(rsData.Get_Fields("Account"),  rsIntParty);
                        lngCopies += lngIntParties;
                    }
                    else
                    {
                        lngIntParties = 0;
                        lngCopies = lngCopies;
                    }

                    // this will find out the certified mail fee
                    if (boolPayCert)
                    {
                        if (blnPayIPCert)
                        {
                            dblCertMailFee += (lngIntParties * FCConvert.ToDouble(GetVariableValue_2("MAILFEE")));
                            lngChargeCopies += lngIntParties;
                        }
                        else
                        {
                            dblCertMailFee = dblCertMailFee;
                            lngChargeCopies = lngChargeCopies;
                        }
                    }
                    else
                    {
                        if (blnPayIPCert)
                        {
                            dblCertMailFee += (lngIntParties * FCConvert.ToDouble(GetVariableValue_2("MAILFEE")));
                        }
                        else
                        {
                            dblCertMailFee = dblCertMailFee;
                        }
                    }

                    if (boolPayNewOwner && boolNewOwner)
                    {
                        dblCertMailFee += FCConvert.ToDouble(GetVariableValue_2("MAILFEE"));
                        lngChargeCopies += 1;
                    }

                    lblDate.Text = modGlobal.PadToString(rsData.Get_Fields("Account"), 6);

                    if (FCConvert.ToInt32(rsData.Get_Fields("LienRecordNumber")) != 0)
                    {
                        dblPrin = Conversion.Val(rsLN.Get_Fields("Principal"));
                        dblPayments = Conversion.Val(rsLN.Get_Fields("PrincipalPaid"));
                    }
                    else
                    {
                        dblPrin = Conversion.Val(rsData.Get_Fields("TaxDue1") + rsData.Get_Fields("TaxDue2") + rsData.Get_Fields("TaxDue3") + rsData.Get_Fields("TaxDue4"));
                        dblPayments = Conversion.Val(rsData.Get_Fields("PrincipalPaid"));
                        dblInt = Conversion.Val(rsData.Get_Fields("InterestCharged") * -1) - rsData.Get_Fields("InterestPaid") - rsData.Get_Fields("PLIPaid") + dblInt;
                    }

                    // less payments line
                    if (modGlobal.Round( dblPayments, 2) != 0)
                    { // only show this when there are payments
                        if (dblPayments < 0)
                        { // if the principal paid is a negative number then some adjustment has happened (supplemental) this should not happen later in the code life
                            rsPayments.OpenRecordset("SELECT ID FROM PaymentRec WHERE BillKey = " + rsData.Get_Fields("ID") + " AND (Code = 'A' OR Code = 'R' OR Code = 'S' OR Code = 'D')");
                            if (rsPayments.EndOfFile())
                            {
                                strLessPaymentsLine = "plus adjustment of " + Strings.Format(Math.Abs(dblPayments), "#,##0.00") + " for the net sum of " + Strings.Format(dblPrin - dblPayments, "#,##0.00") + ",";
                            }
                            else
                            {
                                strLessPaymentsLine = "plus adjustment of " + Strings.Format(Math.Abs(dblPayments), "#,##0.00") + " for the net sum of " + Strings.Format(dblPrin - dblPayments, "#,##0.00") + ",";
                            }
                        }
                        else
                        { // principal paid is greater then zero
                            rsPayments.OpenRecordset("SELECT ID FROM PaymentRec WHERE BillKey = " + rsData.Get_Fields("ID") + " AND (Code = 'A' OR Code = 'R' OR Code = 'S' OR Code = 'D')");
                            if (rsPayments.EndOfFile())
                            {
                                strLessPaymentsLine = "less payment and adjustment of " + Strings.Format(dblPayments, "#,##0.00") + " for the net sum of " + Strings.Format(dblPrin - dblPayments, "#,##0.00") + ",";
                            }
                            else
                            {
                                strLessPaymentsLine = "less payment of " + Strings.Format(dblPayments, "#,##0.00") + " for the net sum of " + Strings.Format(dblPrin - dblPayments, "#,##0.00") + ",";
                            }
                        }
                    }
                    else
                    {
                        strLessPaymentsLine = "";
                    }

                    // BookPage
                    // this should comd from the lien record in the CL database
                    if (fecherFoundation.Strings.Trim(rsLN.Get_Fields("Book") + " ") != "" || fecherFoundation.Strings.Trim(rsLN.Get_Fields("Page") + " ") != "")
                    {
                        strBookPage = "Book " + rsLN.Get_Fields("Book") + ", Page " + rsLN.Get_Fields("Page");
                    }
                    else
                    {
                        strBookPage = "";
                    }

                    if (fecherFoundation.Strings.Trim(strMapLot) == "")
                    {
                        if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("MapLot"))) != "")
                        {
                            strMapLot = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("MapLot")));
                        }
                        else
                        {
                            strMapLot = "________________";
                        }
                    }
                    if (fecherFoundation.Strings.Trim(strLocation) == "")
                    {
                        strLocation = "________________";
                    }
                    if (fecherFoundation.Strings.Trim(strBookPage) == "")
                    {
                        strBookPage = "________________";
                    }

                    // commitment date
                    rsRate.FindFirstRecord("ID", rsData.Get_Fields("LienRecRateKey"));
                    if (!rsRate.NoMatch)
                    {
						if (Information.IsDate(rsRate.Get_Fields("CommitmentDate")))
						{
							if (Convert.ToDateTime(rsRate.Get_Fields("CommitmentDate")).ToOADate() != 0)
							{
								strCommitmentDate = Strings.Format(rsRate.Get_Fields("CommitmentDate"), "MMMM d, yyyy");
							}
							else
							{
								strCommitmentDate = "__________";
							}
						}
                    }
                    else
                    {
                        strCommitmentDate = "__________";
                    }

                    // address bar
                    clsAddress tAddr;

                    switch (intBilledOwnerAddr)
                    {
                        case (int)CLBillingAddressType.AddressAtBilling:
                            tAddr = tAddrCtrlr.GetOwnerAndAddressAtBilling(rsData);
                            break;
                        case (int)CLBillingAddressType.CareOfCurrentOwner:
                            tAddr = tAddrCtrlr.GetOwnerAtBillingCareOfCurrentOwner(rsData);
                            break;
                        case (int)CLBillingAddressType.LastRecordedAddress:
                            tAddr = tAddrCtrlr.GetBilledOwnerAtLastAddress(rsData);
                            break;
                        default:
                            tAddr = new clsAddress();
                            break;
                    }

                    strAddressBar = "    " + tAddr.Addressee;
                    strAddressBar += "\n    " + tAddr.Get_Address(1).Trim();
                    strAddressBar += "\n    " + tAddr.Get_Address(2).Trim();
                    strAddressBar += "\n    " + tAddr.Get_Address(3).Trim();
                    strAddressBar += "\n    " + tAddr.Get_Address(4).Trim();

                    strCMFAddr1 = tAddr.Get_Address(1).Trim();
                    strCMFAddr2 = tAddr.Get_Address(2).Trim();
                    strCMFAddr3 = tAddr.Get_Address(3).Trim();
                    strCMFAddr4 = tAddr.Get_Address(4).Trim();

                    if (Conversion.Val(rsLN.Get_Fields("Costs")) > 0)
                    {
                        dblDemand = Conversion.Val(rsLN.Get_Fields("Costs") - rsLN.Get_Fields("CostsPaid"));
                    }
                    else
                    {
                        dblDemand = 0;
                    }

                    if (FCConvert.ToBoolean(rsRE.Get_Fields("TaxAcquired")))
                    {
                        // lblTaxAcquired.Visible = True
                    }
                    else
                    {
                        lblTaxAcquired.Visible = false;
                    }

                    // info for CMFNumbers table
                    strCMFName = strOwnerName;
                    lngCMFMHNumber = 0;
                    // boolNewOwner = False
                    lngCMFBillKey = FCConvert.ToInt32(rsData.Get_Fields("ID"));

                    // Billing year (0)
                    modRhymalReporting.Statics.frfFreeReport[0].DatabaseFieldName = FCConvert.ToString(intYear);
                    // BookPage (3)
                    modRhymalReporting.Statics.frfFreeReport[3].DatabaseFieldName = strBookPage;
                    // Address Bar (13)
                    modRhymalReporting.Statics.frfFreeReport[13].DatabaseFieldName = strAddressBar;
                    // principal (19)
                    modRhymalReporting.Statics.frfFreeReport[19].DatabaseFieldName = Strings.Format(dblPrin - dblPayments, "#,##0.00");
                    // interest (20)
                    modRhymalReporting.Statics.frfFreeReport[20].DatabaseFieldName = Strings.Format(dblInt, "#,##0.00");
                    // less payments (21)
                    modRhymalReporting.Statics.frfFreeReport[21].DatabaseFieldName = strLessPaymentsLine;
                    // city/town (22)
                    modRhymalReporting.Statics.frfFreeReport[22].DatabaseFieldName = modGlobalConstants.Statics.gstrCityTown;
                    // owner name (23)
                    modRhymalReporting.Statics.frfFreeReport[23].DatabaseFieldName = strOwnerName;
                    // location (28)
                    modRhymalReporting.Statics.frfFreeReport[28].DatabaseFieldName = strLocation;
                    // total certified mail fee (29)
                    modRhymalReporting.Statics.frfFreeReport[29].DatabaseFieldName = Strings.Format(dblCertMailFee, "#,##0.00");
                    // Demand Fees (31)
                    modRhymalReporting.Statics.frfFreeReport[31].DatabaseFieldName = Strings.Format(dblDemand, "#,##0.00");
                    // CommitmentDate (32)
                    modRhymalReporting.Statics.frfFreeReport[32].DatabaseFieldName = strCommitmentDate;
                    // total due (27)
                    modRhymalReporting.Statics.frfFreeReport[27].DatabaseFieldName = Strings.Format(dblTotalDue + dblCertMailFee + dblDemand, "#,##0.00");
                    // MapLot
                    modRhymalReporting.Statics.frfFreeReport[33].DatabaseFieldName = strMapLot;

                    dblFCFee = FCConvert.ToDouble(GetVariableValue_2("FORECLOSUREFEE"));

                    Array.Resize(ref modGlobal.Statics.arrDemand, lngArrayIndex + 1); // this will make sure that there are enough elements to use
                    modGlobal.Statics.arrDemand[lngArrayIndex].Used = true;
                    modGlobal.Statics.arrDemand[lngArrayIndex].Processed = false;
                    modGlobal.Statics.arrDemand[lngArrayIndex].Account = FCConvert.ToInt32(rsData.Get_Fields("Account"));
                    modGlobal.Statics.arrDemand[lngArrayIndex].Name = FCConvert.ToString(rsData.Get_Fields("Name1"));
                    modGlobal.Statics.arrDemand[lngArrayIndex].Fee = dblDemand + dblFCFee;
                    modGlobal.Statics.arrDemand[lngArrayIndex].CertifiedMailFee = dblCertMailFee;

                    lngArrayIndex += 1;

                    // rsData.Edit
                    // MAL@20071204
                    // rsData.Fields("Copies") = lngCopies
                    // rsData.Fields("Copies") = lngChargeCopies
                    // rsData.Update
                    rsTemp.Execute("UPDATE BillingMaster SET Copies = " + FCConvert.ToString(lngChargeCopies) + " WHERE ID = " + FCConvert.ToString(lngCMFBillKey), modExtraModules.strCLDatabase);

                }
                else
                { // this is the difference when it is a copy
                    if (blnIsMortCopy)
                    {
                        if (boolPayMortCert)
                        { // if the user is getting charged for certified mail, then save their info to print on Cert Mail Forms
                            boolPrintCMF = true;
                        }
                        else
                        {
                            boolPrintCMF = false;
                        }

                        // address bar
                        if (rsMort.EndOfFile() != true)
                        {
                            // info for CMFNumbers table
                            strCMFName = FCConvert.ToString(rsMort.Get_Fields("Name"));
                            lngCMFMHNumber = FCConvert.ToInt32(rsMort.Get_Fields("MortgageHolderID"));
                            // boolNewOwner = False
                            // lngCMFBillKey = 0
                            lngCMFBillKey = FCConvert.ToInt32(rsData.Get_Fields("ID"));
                            if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsMort.Get_Fields("Address1"))) != "")
                            {
                                strCMFAddr1 = FCConvert.ToString(rsMort.Get_Fields("Address1"));
                                if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsMort.Get_Fields("Address2"))) != "")
                                {
                                    strCMFAddr2 = FCConvert.ToString(rsMort.Get_Fields("Address2"));
                                    if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsMort.Get_Fields("Address3"))) != "")
                                    {
                                        strCMFAddr3 = FCConvert.ToString(rsMort.Get_Fields("Address3"));

                                        if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsMort.Get_Fields("Zip4"))) != "")
                                        {
                                            strCMFAddr4 = rsMort.Get_Fields("CITY") + ", " + rsMort.Get_Fields("STATE") + " " + rsMort.Get_Fields("ZIP") + "-" + rsMort.Get_Fields("ZIP4");
                                        }
                                        else
                                        {
                                            strCMFAddr4 = rsMort.Get_Fields("CITY") + ", " + rsMort.Get_Fields("STATE") + " " + rsMort.Get_Fields("ZIP");
                                        }
                                    }
                                    else
                                    {
                                        if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsMort.Get_Fields("Zip4"))) != "")
                                        {
                                            strCMFAddr3 = rsMort.Get_Fields("CITY") + ", " + rsMort.Get_Fields("STATE") + " " + rsMort.Get_Fields("ZIP") + "-" + rsMort.Get_Fields("ZIP4");
                                        }
                                        else
                                        {
                                            strCMFAddr3 = rsMort.Get_Fields("CITY") + ", " + rsMort.Get_Fields("STATE") + " " + rsMort.Get_Fields("ZIP");
                                        }
                                    }
                                }
                                else
                                {
                                    if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsMort.Get_Fields("Address3"))) != "")
                                    {
                                        strCMFAddr2 = FCConvert.ToString(rsMort.Get_Fields("Address3"));

                                        if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsMort.Get_Fields("Zip4"))) != "")
                                        {
                                            strCMFAddr3 = rsMort.Get_Fields("CITY") + ", " + rsMort.Get_Fields("STATE") + " " + rsMort.Get_Fields("ZIP") + "-" + rsMort.Get_Fields("ZIP4");
                                        }
                                        else
                                        {
                                            strCMFAddr3 = rsMort.Get_Fields("CITY") + ", " + rsMort.Get_Fields("STATE") + " " + rsMort.Get_Fields("ZIP");
                                        }
                                    }
                                    else
                                    {
                                        if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsMort.Get_Fields("Zip4"))) != "")
                                        {
                                            strCMFAddr2 = rsMort.Get_Fields("CITY") + ", " + rsMort.Get_Fields("STATE") + " " + rsMort.Get_Fields("ZIP") + "-" + rsMort.Get_Fields("ZIP4");
                                        }
                                        else
                                        {
                                            strCMFAddr2 = rsMort.Get_Fields("CITY") + ", " + rsMort.Get_Fields("STATE") + " " + rsMort.Get_Fields("ZIP");
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsMort.Get_Fields("Address2"))) != "")
                                {
                                    strCMFAddr1 = FCConvert.ToString(rsMort.Get_Fields("Address2"));
                                    if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsMort.Get_Fields("Address3"))) != "")
                                    {
                                        strCMFAddr2 = FCConvert.ToString(rsMort.Get_Fields("Address3"));

                                        if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsMort.Get_Fields("Zip4"))) != "")
                                        {
                                            strCMFAddr3 = rsMort.Get_Fields("CITY") + ", " + rsMort.Get_Fields("STATE") + " " + rsMort.Get_Fields("ZIP") + "-" + rsMort.Get_Fields("ZIP4");
                                        }
                                        else
                                        {
                                            strCMFAddr3 = rsMort.Get_Fields("CITY") + ", " + rsMort.Get_Fields("STATE") + " " + rsMort.Get_Fields("ZIP");
                                        }
                                    }
                                    else
                                    {
                                        if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsMort.Get_Fields("Zip4"))) != "")
                                        {
                                            strCMFAddr2 = rsMort.Get_Fields("CITY") + ", " + rsMort.Get_Fields("STATE") + " " + rsMort.Get_Fields("ZIP") + "-" + rsMort.Get_Fields("ZIP4");
                                        }
                                        else
                                        {
                                            strCMFAddr2 = rsMort.Get_Fields("CITY") + ", " + rsMort.Get_Fields("STATE") + " " + rsMort.Get_Fields("ZIP");
                                        }
                                    }
                                }
                                else
                                {
                                    if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsMort.Get_Fields("Address3"))) != "")
                                    {
                                        strCMFAddr1 = FCConvert.ToString(rsMort.Get_Fields("Address3"));

                                        if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsMort.Get_Fields("Zip4"))) != "")
                                        {
                                            strCMFAddr2 = rsMort.Get_Fields("CITY") + ", " + rsMort.Get_Fields("STATE") + " " + rsMort.Get_Fields("ZIP") + "-" + rsMort.Get_Fields("ZIP4");
                                        }
                                        else
                                        {
                                            strCMFAddr2 = rsMort.Get_Fields("CITY") + ", " + rsMort.Get_Fields("STATE") + " " + rsMort.Get_Fields("ZIP");
                                        }
                                    }
                                    else
                                    {
                                        if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsMort.Get_Fields("Zip4"))) != "")
                                        {
                                            strCMFAddr1 = rsMort.Get_Fields("CITY") + ", " + rsMort.Get_Fields("STATE") + " " + rsMort.Get_Fields("ZIP") + "-" + rsMort.Get_Fields("ZIP4");
                                        }
                                        else
                                        {
                                            strCMFAddr1 = rsMort.Get_Fields("CITY") + ", " + rsMort.Get_Fields("STATE") + " " + rsMort.Get_Fields("ZIP");
                                        }
                                    }
                                }
                            }

                            // MAL@20080514: Evaluate string length before adding to address bar
                            // Tracker Reference: 13621
                            // strRSName1 = rsRE.Fields("Own1FullName")
                            // strRSName2 = rsRE.Fields("Own2FullName")
                            // If Trim(rsRE.Fields("Own1Address1")) = "" And Trim(rsRE.Fields("Own1Address2")) = "" And Trim(rsRE.Fields("Own1City")) = "" Then
                            // strRSAddr1 = rsData.Fields("Address1")
                            // strRSAddr2 = rsData.Fields("Address2")
                            // strRSAddr3 = rsData.Fields("Address3")
                            // ElseIf Trim(rsRE.Fields("Own1Address1")) <> "" Then
                            // strRSAddr1 = rsRE.Fields("Own1Address1")
                            // strRSAddr2 = rsRE.Fields("Own1Address2")
                            // strRSAddr3 = rsRE.Fields("Own1City") & ", " & rsRE.Fields("Own1State") & " " & Trim(rsRE.Fields("Own1Zip"))
                            // Else
                            // strRSAddr1 = rsRE.Fields("Own1Address2")
                            // strRSAddr2 = rsRE.Fields("Own1City") & ", " & rsRE.Fields("Own1State") & " " & Trim(rsRE.Fields("Own1Zip"))
                            // strRSAddr3 = ""
                            // End If

                            // trocls-76 11.30.16 kjr Print Owner/Address at time of billing instead of Current Owner/Address
                            strRSName1 = FCConvert.ToString(rsData.Get_Fields("Name1"));
                            strRSName2 = FCConvert.ToString(rsData.Get_Fields("Name2"));
                            strRSAddr1 = FCConvert.ToString(rsData.Get_Fields("Address1")); // kk06282017 trocls-109 Rework the address to include the MailingAddress3 from BillingMaster
                            strRSAddr2 = FCConvert.ToString(rsData.Get_Fields("Address2"));
                            strRSAddr3 = FCConvert.ToString(rsData.Get_Fields("MailingAddress3"));
                            strRSAddr4 = FCConvert.ToString(rsData.Get_Fields("Address3"));

                            // kk07112017 trocls-109  Need to make these vary with the width of the side margins
                            // NOTE: If we change this to a proportional font, it might be better to break out the addresses into 2 separate groups of fields or rtf's
                            intAddr1Width = FCConvert.ToInt16(((this.PrintWidth * 12) - 4) / 2); // Split the address bar into 2, adjust for the extra 4 spaces added on the left
                            intAddr2Left = FCConvert.ToInt16(intAddr1Width + 11); // Line up the tax payer address after "Tax Payer: "
                            intAddr2Width = FCConvert.ToInt16(intAddr1Width - 11);

                            if (fecherFoundation.Strings.Trim(strRSName1).Length > intAddr2Width)
                            {
                                if (fecherFoundation.Strings.Trim(strRSName2) != "")
                                {
                                    strRSName2 = fecherFoundation.Strings.Trim(Strings.Mid(strRSName1, intAddr2Width + 1, strRSName1.Length - intAddr2Width));
                                    strRSName1 = fecherFoundation.Strings.Trim(Strings.Left(strRSName1, intAddr2Width));
                                }
                                else
                                {
                                    strRSName1 = fecherFoundation.Strings.Trim(Strings.Left(strRSName1, intAddr2Width));
                                }
                            }

                            // Remove blank lines
                            if (fecherFoundation.Strings.Trim(strRSAddr3) == "")
                            {
                                strRSAddr3 = strRSAddr4;
                                strRSAddr4 = "";
                            }
                            if (fecherFoundation.Strings.Trim(strRSAddr2) == "")
                            {
                                strRSAddr2 = strRSAddr3;
                                strRSAddr3 = strRSAddr4;
                                strRSAddr4 = "";
                            }
                            if (fecherFoundation.Strings.Trim(strRSAddr1) == "")
                            {
                                strRSAddr1 = strRSAddr2;
                                strRSAddr2 = strRSAddr3;
                                strRSAddr3 = strRSAddr4;
                                strRSAddr4 = "";
                            }
                            if (fecherFoundation.Strings.Trim(strRSName2) == "")
                            {
                                strRSName2 = strRSAddr1;
                                strRSAddr1 = strRSAddr2;
                                strRSAddr2 = strRSAddr3;
                                strRSAddr3 = strRSAddr4;
                                strRSAddr4 = "";
                            }

                            // Evaluate Address Line Lengths
                            if (fecherFoundation.Strings.Trim(strRSName2).Length > intAddr2Width)
                            {
                                if (fecherFoundation.Strings.Trim(strRSAddr1) == "" || fecherFoundation.Strings.Trim(strRSAddr2) == "" || fecherFoundation.Strings.Trim(strRSAddr3) == "" || fecherFoundation.Strings.Trim(strRSAddr4) == "")
                                {
                                    strRSAddr4 = strRSAddr3; // There's an extra line - use it
                                    strRSAddr3 = strRSAddr2;
                                    strRSAddr2 = strRSAddr1;
                                    strRSAddr1 = fecherFoundation.Strings.Trim(Strings.Mid(strRSName2, intAddr2Width + 1, strRSName2.Length - intAddr2Width));
                                    strRSName2 = fecherFoundation.Strings.Trim(Strings.Left(strRSName2, intAddr2Width));
                                }
                                else
                                { // No extra line - trim the address line
                                    strRSName2 = fecherFoundation.Strings.Trim(Strings.Left(strRSName2, intAddr2Width));
                                }
                            }

                            if (fecherFoundation.Strings.Trim(strRSAddr1).Length > intAddr2Width)
                            {
                                if (fecherFoundation.Strings.Trim(strRSAddr2) == "" || fecherFoundation.Strings.Trim(strRSAddr3) == "" || fecherFoundation.Strings.Trim(strRSAddr4) == "")
                                {
                                    strRSAddr4 = strRSAddr3; // There's an extra line - use it
                                    strRSAddr3 = strRSAddr2;
                                    strRSAddr2 = fecherFoundation.Strings.Trim(Strings.Mid(strRSAddr1, intAddr2Width + 1, strRSAddr1.Length - intAddr2Width));
                                    strRSAddr1 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr1, intAddr2Width));
                                }
                                else
                                { // No extra line - trim the address line
                                    strRSAddr1 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr1, intAddr2Width));
                                }
                            }

                            if (fecherFoundation.Strings.Trim(strRSAddr2).Length > intAddr2Width)
                            {
                                if (fecherFoundation.Strings.Trim(strRSAddr3) == "" || fecherFoundation.Strings.Trim(strRSAddr4) == "")
                                {
                                    strRSAddr4 = strRSAddr3; // There's an extra line - use it
                                    strRSAddr3 = fecherFoundation.Strings.Trim(Strings.Mid(strRSAddr2, intAddr2Width + 1, strRSAddr2.Length - intAddr2Width));
                                    strRSAddr2 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr2, intAddr2Width));
                                }
                                else
                                { // No extra line - trim the address line
                                    strRSAddr2 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr2, intAddr2Width));
                                }
                            }

                            if (fecherFoundation.Strings.Trim(strRSAddr3).Length > intAddr2Width)
                            {
                                if (fecherFoundation.Strings.Trim(strRSAddr4) == "")
                                { // There's an extra line - use it
                                    strRSAddr4 = fecherFoundation.Strings.Trim(Strings.Mid(strRSAddr3, intAddr2Width + 1, strRSAddr3.Length - intAddr2Width));
                                    strRSAddr3 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr3, intAddr2Width));
                                }
                                else
                                { // No extra line - trim the address line
                                    strRSAddr3 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr3, intAddr2Width));
                                }
                            }

                            if (fecherFoundation.Strings.Trim(strRSAddr4).Length > intAddr2Width)
                            {
                                strRSAddr4 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr4, intAddr2Width));
                            }

                            strAddressBar = "    " + FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces(rsMort.Get_Fields("Name"), intAddr1Width - 11, false)) + FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces("Tax Payer:", 11, false)) + strRSName1;

                            strTemp1 = strRSName2;
                            strTemp2 = strRSAddr1;
                            strTemp3 = strRSAddr2;
                            strTemp4 = strRSAddr3;
                            strTemp5 = strRSAddr4;

                           

                            strAddressBar += "\r\n" + "    " + FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces(strCMFAddr1,  intAddr1Width, false)) + strTemp1;
                            strAddressBar += "\r\n" + "    " + FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces(strCMFAddr2,  intAddr1Width, false)) + strTemp2;
                            if (fecherFoundation.Strings.Trim(strCMFAddr3) != "" || fecherFoundation.Strings.Trim(strTemp3) != "")
                            {
                                strAddressBar += "\r\n" + "    " + FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces( strCMFAddr3,  intAddr1Width, false)) + strTemp3;
                            }
                            if (fecherFoundation.Strings.Trim(strCMFAddr4) != "" || fecherFoundation.Strings.Trim(strTemp4) != "")
                            {
                                strAddressBar += "\r\n" + "    " + FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces( strCMFAddr4,  intAddr1Width, false)) + strTemp4;
                            }
                            if (fecherFoundation.Strings.Trim(strTemp5) != "")
                            {
                                strAddressBar += "\r\n" + "    " + Strings.Space(intAddr1Width) + strTemp5;
                            }

                            // set name and address bar for a mortgage holder
                            modRhymalReporting.Statics.frfFreeReport[13].DatabaseFieldName = strAddressBar;
                            rsMort.MoveNext();
                        }
                        else
                        {
                            strAddressBar = "    " + rsData.Get_Fields("Name1");
                            if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Address1"))) != "")
                            {
                                strAddressBar += "\r\n" + "    " + rsData.Get_Fields("Address1");
                            }
                            strAddressBar += "\r\n" + "    " + GetVariableValue_2("MUNI") + ", " + GetVariableValue_2("STATE") + " " + GetVariableValue_2("ZIP");

                            // set name and address bar for a mortgage holder
                            modRhymalReporting.Statics.frfFreeReport[13].DatabaseFieldName = strAddressBar;
                        }

                    }
                    else if (blnIsIPCopy)
                    {
                        if (blnPayIPCert)
                        { // if the user is getting charged for certified mail, then save their info to print on Cert Mail Forms
                            boolPrintCMF = true;
                        }
                        else
                        {
                            boolPrintCMF = false;
                        }

                        // address bar
                        if (!rsIntParty.EndOfFile())
                        {
                            // info for CMFNumbers table
                            strCMFName = FCConvert.ToString(rsIntParty.Get_Fields("Name"));
                            lngCMFMHNumber = FCConvert.ToInt32(rsIntParty.Get_Fields("ID")); // kk07102017 trocls-109  This is still .Fields("AutoID") ??
                            lngCMFBillKey = FCConvert.ToInt32(rsData.Get_Fields("ID")); // .Fields("BillKey") ??
                            if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsIntParty.Get_Fields("Address1"))) != "")
                            {
                                strCMFAddr1 = FCConvert.ToString(rsIntParty.Get_Fields("Address1"));
                                if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsIntParty.Get_Fields("Address2"))) != "")
                                {
                                    strCMFAddr2 = FCConvert.ToString(rsIntParty.Get_Fields("Address2"));
                                    if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsIntParty.Get_Fields("Zip4"))) != "")
                                    {
                                        strCMFAddr3 = rsIntParty.Get_Fields("City") + ", " + rsIntParty.Get_Fields("State") + " " + rsIntParty.Get_Fields("Zip") + "-" + rsIntParty.Get_Fields("Zip4");
                                    }
                                    else
                                    {
                                        strCMFAddr3 = rsIntParty.Get_Fields("City") + ", " + rsIntParty.Get_Fields("State") + " " + rsIntParty.Get_Fields("Zip");
                                    }
                                }
                                else
                                {
                                    if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsIntParty.Get_Fields("Zip4"))) != "")
                                    {
                                        strCMFAddr2 = rsIntParty.Get_Fields("CITY") + ", " + rsIntParty.Get_Fields("STATE") + " " + rsIntParty.Get_Fields("ZIP") + "-" + rsIntParty.Get_Fields("ZIP4");
                                    }
                                    else
                                    {
                                        strCMFAddr2 = rsIntParty.Get_Fields("CITY") + ", " + rsIntParty.Get_Fields("STATE") + " " + rsIntParty.Get_Fields("ZIP");
                                    }
                                }
                            }
                            else
                            {
                                if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsIntParty.Get_Fields("Address2"))) != "")
                                {
                                    strCMFAddr1 = FCConvert.ToString(rsIntParty.Get_Fields("Address2"));
                                    if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsIntParty.Get_Fields("Zip4"))) != "")
                                    {
                                        strCMFAddr2 = rsIntParty.Get_Fields("City") + ", " + rsIntParty.Get_Fields("State") + " " + rsIntParty.Get_Fields("Zip") + "-" + rsIntParty.Get_Fields("Zip4");
                                    }
                                    else
                                    {
                                        strCMFAddr2 = rsIntParty.Get_Fields("City") + ", " + rsIntParty.Get_Fields("State") + " " + rsIntParty.Get_Fields("Zip");
                                    }
                                }
                                else
                                {
                                    if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsIntParty.Get_Fields("Zip4"))) != "")
                                    {
                                        strCMFAddr1 = rsIntParty.Get_Fields("CITY") + ", " + rsIntParty.Get_Fields("STATE") + " " + rsIntParty.Get_Fields("ZIP") + "-" + rsIntParty.Get_Fields("ZIP4");
                                    }
                                    else
                                    {
                                        strCMFAddr1 = rsIntParty.Get_Fields("CITY") + ", " + rsIntParty.Get_Fields("STATE") + " " + rsIntParty.Get_Fields("ZIP");
                                    }
                                }
                            }

                            // MAL@20080514: Evaluate string length before adding to address bar
                            // Tracker Reference: 13621
                            // strRSName1 = rsRE.Fields("Own1FullName")
                            // strRSName2 = rsRE.Fields("Own2FullName")
                            // If Trim(rsRE.Fields("Own1Address1")) = "" And Trim(rsRE.Fields("Own1Address2")) = "" And Trim(rsRE.Fields("Own1City")) = "" Then
                            // strRSAddr1 = rsData.Fields("Address1")
                            // strRSAddr2 = rsData.Fields("Address2")
                            // strRSAddr3 = rsData.Fields("Address3")
                            // ElseIf Trim(rsRE.Fields("Own1Address1")) <> "" Then
                            // strRSAddr1 = rsRE.Fields("Own1Address1")
                            // strRSAddr2 = rsRE.Fields("Own1Address2")
                            // strRSAddr3 = rsRE.Fields("Own1City") & ", " & rsRE.Fields("Own1State") & " " & Trim(rsRE.Fields("Own1Zip"))
                            // Else
                            // strRSAddr1 = rsRE.Fields("Own1Address2")
                            // strRSAddr2 = rsRE.Fields("Own1City") & ", " & rsRE.Fields("Own1State") & " " & Trim(rsRE.Fields("Own1Zip"))
                            // strRSAddr3 = ""
                            // End If

                            // trocls-76 11.30.16 kjr Print Owner/Address at time of billing instead of Current Owner/Address
                            strRSName1 = FCConvert.ToString(rsData.Get_Fields("Name1"));
                            strRSName2 = FCConvert.ToString(rsData.Get_Fields("Name2"));
                            strRSAddr1 = FCConvert.ToString(rsData.Get_Fields("Address1"));
                            strRSAddr2 = FCConvert.ToString(rsData.Get_Fields("Address2"));
                            strRSAddr3 = FCConvert.ToString(rsData.Get_Fields("MailingAddress3")); // kk07112017 trocls-109  Include 3rd Address line from CP record (Corey added to BillingMaster)
                            strRSAddr4 = FCConvert.ToString(rsData.Get_Fields("Address3"));

                            // kk07112017 trocls-109  Need to make these vary with the width of the side margins
                            // NOTE: If we change this to a proportional font, it might be better to break out the addresses into 2 separate groups of fields or rtf's
                            intAddr1Width = FCConvert.ToInt16(((this.PrintWidth * 12) - 4) / 2); // Split the address bar into 2, adjust for the extra 4 spaces added on the left
                            intAddr2Left = FCConvert.ToInt16(intAddr1Width + 11); // Line up the tax payer address after "Tax Payer: "
                            intAddr2Width = FCConvert.ToInt16(intAddr1Width - 11);

                            if (fecherFoundation.Strings.Trim(strRSName1).Length > intAddr2Width)
                            {
                                if (fecherFoundation.Strings.Trim(strRSName2) == "")
                                {
                                    strRSName2 = fecherFoundation.Strings.Trim(Strings.Mid(strRSName1, intAddr2Width + 1, strRSName1.Length - intAddr2Width));
                                    strRSName1 = fecherFoundation.Strings.Trim(Strings.Left(strRSName1, intAddr2Width));
                                }
                                else
                                {
                                    strRSName1 = fecherFoundation.Strings.Trim(Strings.Left(strRSName1, intAddr2Width));
                                }
                            }

                            // Remove blank lines
                            if (fecherFoundation.Strings.Trim(strRSAddr3) == "")
                            {
                                strRSAddr3 = strRSAddr4;
                                strRSAddr4 = "";
                            }
                            if (fecherFoundation.Strings.Trim(strRSAddr2) == "")
                            {
                                strRSAddr2 = strRSAddr3;
                                strRSAddr3 = strRSAddr4;
                                strRSAddr4 = "";
                            }
                            if (fecherFoundation.Strings.Trim(strRSAddr1) == "")
                            {
                                strRSAddr1 = strRSAddr2;
                                strRSAddr2 = strRSAddr3;
                                strRSAddr3 = strRSAddr4;
                                strRSAddr4 = "";
                            }
                            if (fecherFoundation.Strings.Trim(strRSName2) == "")
                            {
                                strRSName2 = strRSAddr1;
                                strRSAddr1 = strRSAddr2;
                                strRSAddr2 = strRSAddr3;
                                strRSAddr3 = strRSAddr4;
                                strRSAddr4 = "";
                            }

                            // Evaluate Address Line Lengths
                            if (fecherFoundation.Strings.Trim(strRSName2).Length > intAddr2Width)
                            {
                                if (fecherFoundation.Strings.Trim(strRSAddr1) == "" || fecherFoundation.Strings.Trim(strRSAddr2) == "" || fecherFoundation.Strings.Trim(strRSAddr3) == "" || fecherFoundation.Strings.Trim(strRSAddr4) == "")
                                {
                                    strRSAddr4 = strRSAddr3; // There's an extra line - use it
                                    strRSAddr3 = strRSAddr2;
                                    strRSAddr2 = strRSAddr1;
                                    strRSAddr1 = fecherFoundation.Strings.Trim(Strings.Mid(strRSName2, intAddr2Width + 1, strRSName2.Length - intAddr2Width));
                                    strRSName2 = fecherFoundation.Strings.Trim(Strings.Left(strRSName2, intAddr2Width));
                                }
                                else
                                { // No extra line - trim the address line
                                    strRSName2 = fecherFoundation.Strings.Trim(Strings.Left(strRSName2, intAddr2Width));
                                }
                            }

                            if (fecherFoundation.Strings.Trim(strRSAddr1).Length > intAddr2Width)
                            {
                                if (fecherFoundation.Strings.Trim(strRSAddr2) == "" || fecherFoundation.Strings.Trim(strRSAddr3) == "" || fecherFoundation.Strings.Trim(strRSAddr4) == "")
                                {
                                    strRSAddr4 = strRSAddr3; // There's an extra line - use it
                                    strRSAddr3 = strRSAddr2;
                                    strRSAddr2 = fecherFoundation.Strings.Trim(Strings.Mid(strRSAddr1, intAddr2Width + 1, strRSAddr1.Length - intAddr2Width));
                                    strRSAddr1 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr1, intAddr2Width));
                                }
                                else
                                { // No extra line - trim the address line
                                    strRSAddr1 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr1, intAddr2Width));
                                }
                            }

                            if (fecherFoundation.Strings.Trim(strRSAddr2).Length > intAddr2Width)
                            {
                                if (fecherFoundation.Strings.Trim(strRSAddr3) == "" || fecherFoundation.Strings.Trim(strRSAddr4) == "")
                                {
                                    strRSAddr4 = strRSAddr3; // There's an extra line - use it
                                    strRSAddr3 = fecherFoundation.Strings.Trim(Strings.Mid(strRSAddr2, intAddr2Width + 1, strRSAddr2.Length - intAddr2Width));
                                    strRSAddr2 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr2, intAddr2Width));
                                }
                                else
                                { // No extra line - trim the address line
                                    strRSAddr2 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr2, intAddr2Width));
                                }
                            }

                            if (fecherFoundation.Strings.Trim(strRSAddr3).Length > intAddr2Width)
                            {
                                if (fecherFoundation.Strings.Trim(strRSAddr4) == "")
                                { // There's an extra line - use it
                                    strRSAddr4 = fecherFoundation.Strings.Trim(Strings.Mid(strRSAddr3, intAddr2Width + 1, strRSAddr3.Length - intAddr2Width));
                                    strRSAddr3 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr3, intAddr2Width));
                                }
                                else
                                { // No extra line - trim the address line
                                    strRSAddr3 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr3, intAddr2Width));
                                }
                            }

                            if (fecherFoundation.Strings.Trim(strRSAddr4).Length > intAddr2Width)
                            {
                                strRSAddr4 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr4, intAddr2Width));
                            }

                            strAddressBar = "    " + FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces(rsIntParty.Get_Fields("Name"), intAddr1Width - 11, false)) + FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces("Tax Payer:", 11, false)) + strRSName1;

                            strTemp1 = strRSName2;
                            strTemp2 = strRSAddr1;
                            strTemp3 = strRSAddr2;
                            strTemp4 = strRSAddr3;
                            strTemp5 = strRSAddr4;

                            // strAddressBar = "    " & PadStringWithSpaces(rsIntParty.Fields("Name"), 36, False) & PadStringWithSpaces("Tax Payer:", 11, False) & rsRE.Fields("RSName")
                            // 
                            // If Trim(rsRE.Fields("RSAddr1")) = "" And Trim(rsRE.Fields("RSAddr2")) = "" And Trim(rsRE.Fields("RSAddr3")) = "" Then
                            // strTemp1 = rsData.Fields("Address1")
                            // strTemp2 = rsData.Fields("Address2")
                            // strTemp3 = rsData.Fields("Address3")
                            // ElseIf Trim(rsRE.Fields("RSAddr1")) <> "" Then
                            // strTemp1 = rsRE.Fields("RSAddr1")
                            // strTemp2 = rsRE.Fields("RSAddr2")
                            // strTemp3 = rsRE.Fields("RSAddr3") & ", " & rsRE.Fields("RSState") & " " & rsRE.Fields("RSZip")
                            // Else
                            // strTemp1 = rsRE.Fields("RSAddr2")
                            // strTemp2 = rsRE.Fields("RSAddr3") & ", " & rsRE.Fields("RSState") & " " & rsRE.Fields("RSZip")
                            // strTemp3 = ""
                            // End If
                            // 
                            // If Trim(rsRE.Fields("RSSecOwner")) <> "" Then    'Trim(rsRE.Fields("RSSecOwner")) <> ""
                            // move all of the fields down a level
                            // strTemp4 = strTemp3
                            // strTemp3 = strTemp2
                            // strTemp2 = strTemp1
                            // strTemp1 = Trim(rsRE.Fields("RSSecOwner"))
                            // End If
                            // If Trim(strTemp2) = "" Then
                            // strTemp2 = strTemp3
                            // strTemp3 = strTemp4
                            // strTemp4 = ""
                            // End If
                            // 
                            // If Trim(strTemp1) = "" Then
                            // strTemp1 = strTemp2
                            // strTemp2 = strTemp3
                            // strTemp3 = strTemp4
                            // strTemp4 = ""
                            // End If

                            strAddressBar += "\r\n" + "    " + FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces( strCMFAddr1,  intAddr1Width, false)) + strTemp1;
                            strAddressBar += "\r\n" + "    " + FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces( strCMFAddr2,  intAddr1Width, false)) + strTemp2;
                            if (fecherFoundation.Strings.Trim(strCMFAddr3) != "" || fecherFoundation.Strings.Trim(strTemp3) != "")
                            {
                                strAddressBar += "\r\n" + "    " + FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces( strCMFAddr3,  intAddr1Width, false)) + strTemp3;
                            }
                            if (fecherFoundation.Strings.Trim(strCMFAddr4) != "" || fecherFoundation.Strings.Trim(strTemp4) != "")
                            {
                                strAddressBar += "\r\n" + "    " + FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces( strCMFAddr4,  intAddr1Width, false)) + strTemp4;
                            }
                            if (fecherFoundation.Strings.Trim(strTemp5) != "")
                            {
                                strAddressBar += "\r\n" + "    " + Strings.Space(intAddr1Width) + strTemp5;
                            }

                            // set name and address bar for a mortgage holder
                            modRhymalReporting.Statics.frfFreeReport[13].DatabaseFieldName = strAddressBar;
                            rsIntParty.MoveNext();
                        }
                        else
                        {
                            strAddressBar = "    " + rsData.Get_Fields("Name1");
                            if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Address1"))) != "")
                            {
                                strAddressBar += "\r\n" + "    " + rsData.Get_Fields("Address1");
                            }
                            strAddressBar += "\r\n" + "    " + GetVariableValue_2("MUNI") + ", " + GetVariableValue_2("STATE") + " " + GetVariableValue_2("ZIP");

                            // set name and address bar for a mortgage holder
                            modRhymalReporting.Statics.frfFreeReport[13].DatabaseFieldName = strAddressBar;
                        }

                    }
                    else if (!blnIsMortCopy && !blnIsIPCopy && boolNewOwner)
                    { // this is the last copy to be made, it is for the new owner
                      // boolNewOwner = False                            'this will set it off for the next account
                        if (intNewOwnerChoice > 0)
                        { // if the user is getting charged for certified mail, then save their info to print on Cert Mail Forms
                            boolPrintCMF = true;
                        }
                        else
                        {
                            boolPrintCMF = false;
                        }
                        // info for CMFNumbers table
                        strCMFName = FCConvert.ToString(rsRE.Get_Fields("Own1FullName"));
                        lngCMFMHNumber = 0;
                        // boolNewOwner = True
                        // lngCMFBillKey = 0
                        lngCMFBillKey = FCConvert.ToInt32(rsData.Get_Fields("ID"));
                        // this is for a copy for the new owner
                        // If boolAddressCO Then   'this is the owner at time of billing C/O the new owner and the new owner address

                        // trocls-76 11.30.16 kjr Print Owner/Address at time of billing on right side.  Set Current Owner/Address on Left side.
                        if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1Address1"))) != "")
                        {
                            strCMFAddr1 = FCConvert.ToString(rsRE.Get_Fields("Own1Address1"));
                            if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1Address2"))) != "")
                            {
                                strCMFAddr2 = FCConvert.ToString(rsRE.Get_Fields("Own1Address2"));
                                strCMFAddr3 = rsRE.Get_Fields("Own1City") + ", " + rsRE.Get_Fields("Own1State") + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1Zip")));
                            }
                            else
                            {
                                strCMFAddr2 = rsRE.Get_Fields("Own1City") + ", " + rsRE.Get_Fields("Own1State") + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1Zip")));
                                strCMFAddr3 = "";
                            }
                        }
                        else
                        {
                            if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1Address2"))) != "")
                            {
                                strCMFAddr1 = FCConvert.ToString(rsRE.Get_Fields("Own1Address2"));
                                strCMFAddr2 = rsRE.Get_Fields("Own1City") + ", " + rsRE.Get_Fields("Own1State") + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1Zip")));
                                strCMFAddr3 = "";
                            }
                            else
                            {
                                strCMFAddr1 = rsRE.Get_Fields("Own1City") + ", " + rsRE.Get_Fields("Own1State") + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1Zip")));
                                strCMFAddr2 = "";
                                strCMFAddr3 = "";
                            }
                        }

                        // trocls-76 11.30.16 kjr Print Owner/Address at time of billing on right side.
                        strRSName1 = FCConvert.ToString(rsData.Get_Fields("Name1"));
                        strRSName2 = FCConvert.ToString(rsData.Get_Fields("Name2"));
                        strRSAddr1 = FCConvert.ToString(rsData.Get_Fields("Address1"));
                        strRSAddr2 = FCConvert.ToString(rsData.Get_Fields("Address2"));
                        strRSAddr3 = FCConvert.ToString(rsData.Get_Fields("MailingAddress3")); // kk07112017 trocls-109  Include 3rd Address line from CP record (Corey added to BillingMaster)
                        strRSAddr4 = FCConvert.ToString(rsData.Get_Fields("Address3"));

                        // kk07112017 trocls-109  Need to make these vary with the width of the side margins
                        // NOTE: If we change this to a proportional font, it might be better to break out the addresses into 2 separate groups of fields or rtf's
                        intAddr1Width = FCConvert.ToInt16(((this.PrintWidth * 12) - 4) / 2); // Split the address bar into 2, adjust for the extra 4 spaces added on the left
                        intAddr2Left = FCConvert.ToInt16(intAddr1Width + 11); // Line up the tax payer address after "Tax Payer: "
                        intAddr2Width = FCConvert.ToInt16(intAddr1Width - 11);

                        if (fecherFoundation.Strings.Trim(strRSName1).Length > intAddr2Width)
                        {
                            if (fecherFoundation.Strings.Trim(strRSName2) == "")
                            {
                                strRSName2 = fecherFoundation.Strings.Trim(Strings.Mid(strRSName1, intAddr2Width + 1, strRSName1.Length - intAddr2Width));
                                strRSName1 = fecherFoundation.Strings.Trim(Strings.Left(strRSName1, intAddr2Width));
                            }
                            else
                            {
                                strRSName1 = fecherFoundation.Strings.Trim(Strings.Left(strRSName1, intAddr2Width));
                            }
                        }

                        // Remove blank lines
                        if (fecherFoundation.Strings.Trim(strRSAddr3) == "")
                        {
                            strRSAddr3 = strRSAddr4;
                            strRSAddr4 = "";
                        }
                        if (fecherFoundation.Strings.Trim(strRSAddr2) == "")
                        {
                            strRSAddr2 = strRSAddr3;
                            strRSAddr3 = strRSAddr4;
                            strRSAddr4 = "";
                        }
                        if (fecherFoundation.Strings.Trim(strRSAddr1) == "")
                        {
                            strRSAddr1 = strRSAddr2;
                            strRSAddr2 = strRSAddr3;
                            strRSAddr3 = strRSAddr4;
                            strRSAddr4 = "";
                        }
                        if (fecherFoundation.Strings.Trim(strRSName2) == "")
                        {
                            // Move all address lines up one
                            strRSName2 = strRSAddr1;
                            strRSAddr1 = strRSAddr2;
                            strRSAddr2 = strRSAddr3;
                            strRSAddr3 = strRSAddr4;
                            strRSAddr4 = "";
                        }

                        // Evaluate Address Line Lengths
                        if (fecherFoundation.Strings.Trim(strRSName2).Length > intAddr2Width)
                        {
                            if (fecherFoundation.Strings.Trim(strRSAddr1) == "" || fecherFoundation.Strings.Trim(strRSAddr2) == "" || fecherFoundation.Strings.Trim(strRSAddr3) == "" || fecherFoundation.Strings.Trim(strRSAddr4) == "")
                            {
                                strRSAddr4 = strRSAddr3; // There's an extra line - use it
                                strRSAddr3 = strRSAddr2;
                                strRSAddr2 = strRSAddr1;
                                strRSAddr1 = fecherFoundation.Strings.Trim(Strings.Mid(strRSName2, intAddr2Width + 1, strRSName2.Length - intAddr2Width));
                                strRSName2 = fecherFoundation.Strings.Trim(Strings.Left(strRSName2, intAddr2Width));
                            }
                            else
                            { // No extra line - trim the address line
                                strRSName2 = fecherFoundation.Strings.Trim(Strings.Left(strRSName2, intAddr2Width));
                            }
                        }

                        if (fecherFoundation.Strings.Trim(strRSAddr1).Length > intAddr2Width)
                        {
                            if (fecherFoundation.Strings.Trim(strRSAddr2) == "" || fecherFoundation.Strings.Trim(strRSAddr3) == "" || fecherFoundation.Strings.Trim(strRSAddr4) == "")
                            {
                                strRSAddr4 = strRSAddr3; // There's an extra line - use it
                                strRSAddr3 = strRSAddr2;
                                strRSAddr2 = fecherFoundation.Strings.Trim(Strings.Mid(strRSAddr1, intAddr2Width + 1, strRSAddr1.Length - intAddr2Width));
                                strRSAddr1 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr1, intAddr2Width));
                            }
                            else
                            { // No extra line - trim the address line
                                strRSAddr1 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr1, intAddr2Width));
                            }
                        }

                        if (fecherFoundation.Strings.Trim(strRSAddr2).Length > intAddr2Width)
                        {
                            if (fecherFoundation.Strings.Trim(strRSAddr3) == "" || fecherFoundation.Strings.Trim(strRSAddr4) == "")
                            {
                                strRSAddr4 = strRSAddr3; // There's an extra line - use it
                                strRSAddr3 = fecherFoundation.Strings.Trim(Strings.Mid(strRSAddr2, intAddr2Width + 1, strRSAddr2.Length - intAddr2Width));
                                strRSAddr2 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr2, intAddr2Width));
                            }
                            else
                            { // No extra line - trim the address line
                                strRSAddr2 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr2, intAddr2Width));
                            }
                        }

                        if (fecherFoundation.Strings.Trim(strRSAddr3).Length > intAddr2Width)
                        {
                            if (fecherFoundation.Strings.Trim(strRSAddr4) == "")
                            { // There's an extra line - use it
                                strRSAddr4 = fecherFoundation.Strings.Trim(Strings.Mid(strRSAddr3, intAddr2Width + 1, strRSAddr3.Length - intAddr2Width));
                                strRSAddr3 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr3, intAddr2Width));
                            }
                            else
                            { // No extra line - trim the address line
                                strRSAddr3 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr3, intAddr2Width));
                            }
                        }

                        if (fecherFoundation.Strings.Trim(strRSAddr4).Length > intAddr2Width)
                        {
                            strRSAddr4 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr4, intAddr2Width));
                        }

                        strAddressBar = "    " + FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces(rsRE.Get_Fields("Own1FullName"), intAddr1Width - 11, false)) + FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces("Tax Payer:", 11, false)) + strRSName1;

                        strTemp1 = strRSName2;
                        strTemp2 = strRSAddr1;
                        strTemp3 = strRSAddr2;
                        strTemp4 = strRSAddr3;
                        strTemp5 = strRSAddr4;

                        strAddressBar += "\r\n" + "    " + FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces( strCMFAddr1,  intAddr1Width, false)) + strTemp1;
                        strAddressBar += "\r\n" + "    " + FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces( strCMFAddr2,  intAddr1Width, false)) + strTemp2;
                        if (fecherFoundation.Strings.Trim(strCMFAddr3) != "" || fecherFoundation.Strings.Trim(strTemp3) != "")
                        {
                            strAddressBar += "\r\n" + "    " + FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces( strCMFAddr3,  intAddr1Width, false)) + strTemp3;
                        }
                        if (fecherFoundation.Strings.Trim(strCMFAddr4) != "" || fecherFoundation.Strings.Trim(strTemp4) != "")
                        {
                            strAddressBar += "\r\n" + "    " + FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces( strCMFAddr4,  intAddr1Width, false)) + strTemp4;
                        }
                        if (fecherFoundation.Strings.Trim(strTemp5) != "")
                        {
                            strAddressBar += "\r\n" + "    " + Strings.Space(intAddr1Width) + strTemp5;
                        }
                        // 

                        // trocls-76 commented
                        // strAddressBar = "    " & rsRE.Fields("Own1FullName")
                        // strAddressBar = strAddressBar & vbCrLf & "    C/O " & rsRE.Fields("RSName")   'name on the current RE account

                        // if the row needs to be added, then do it
                        // If Trim(rsRE.Fields("Own2FullName")) <> "" Then
                        // strAddressBar = strAddressBar & vbCrLf & "    " & PadStringWithSpaces(rsRE.Fields("Own2FullName"), 36, False)
                        // End If
                        // If Trim(rsRE.Fields("Own1Address1")) <> "" Then
                        // strAddressBar = strAddressBar & vbCrLf & "    " & PadStringWithSpaces(rsRE.Fields("Own1Address1"), 36, False)
                        // End If
                        // 
                        // If Trim(rsRE.Fields("Own1Address2")) <> "" Then
                        // strAddressBar = strAddressBar & vbCrLf & "    " & PadStringWithSpaces(rsRE.Fields("Own1Address2"), 36, False)
                        // End If
                        // 
                        // If Trim(rsRE.Fields("Own1City")) <> "" Then
                        // strAddressBar = strAddressBar & vbCrLf & "    " & PadStringWithSpaces(rsRE.Fields("Own1City") & ", " & rsRE.Fields("Own1State") & " " & Trim(rsRE.Fields("Own1Zip")), 36, False)
                        // End If
                        // 
                        // this is information for the Certified Mailing List
                        // strCMFAddr1 = rsRE.Fields("Own1Address1")
                        // strCMFAddr2 = rsRE.Fields("Own1Address2")
                        // strCMFAddr3 = rsRE.Fields("Own1City") & ", " & rsRE.Fields("Own1State") & " " & Trim(rsRE.Fields("Own1Zip"))

                        // Else        'this is the owner name at the time of billing and the address at the time of billing
                        // 
                        // strAddressBar = "    " & rsData.Fields("Name1")
                        // 
                        // if the row needs to be added, then do it
                        // If Trim(rsData.Fields("Address1")) <> "" Then
                        // strAddressBar = strAddressBar & vbCrLf & "    " & rsData.Fields("Address1")
                        // End If
                        // 
                        // If Trim(rsData.Fields("Address2")) <> "" Then
                        // strAddressBar = strAddressBar & vbCrLf & "    " & rsData.Fields("Address2")
                        // End If
                        // 
                        // If Trim(rsData.Fields("Address3")) <> "" Then
                        // strAddressBar = strAddressBar & vbCrLf & "    " & rsData.Fields("Address3")
                        // End If
                        // 
                        // this is information for the Certified Mailing List
                        // strCMFAddr1 = rsData.Fields("Address1")
                        // strCMFAddr2 = rsData.Fields("Address2")
                        // strCMFAddr3 = rsData.Fields("Address3")
                        // End If

                        // set name and address bar for a New Owner
                        modRhymalReporting.Statics.frfFreeReport[13].DatabaseFieldName = strAddressBar;
                        rsRE.MoveNext();
                    }
                    else
                    {
                        strAddressBar = "    " + rsData.Get_Fields("Name1");
                        if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Address1"))) != "")
                        {
                            strAddressBar += "\r\n" + "    " + rsData.Get_Fields("Address1");
                        }
                        strAddressBar += "\r\n" + "    " + GetVariableValue_2("MUNI") + ", " + GetVariableValue_2("STATE") + " " + GetVariableValue_2("ZIP");

                        // set name and address bar for a mortgage holder
                        modRhymalReporting.Statics.frfFreeReport[13].DatabaseFieldName = strAddressBar;
                    }     
                }

                // this will add the information to the CMFNumber table so that it will be
                // easier to figure out which accounts need certified mail forms to be printed for them
                // MAL@20071022: Added support for new Process Type
                // Call Reference: 114824
                if (lngCMFMHNumber != 0)
                {
                    if (blnIsMortCopy)
                    {
                        // rsCMFNumbers.FindFirstRecord , , "Account = " & rsData.Fields("Account") & " AND Type = 22 AND MortgageHolder = " & lngCMFMHNumber & " AND MortgageHolder <> 0"
                        // rsCMFNumbers.FindFirstRecord , , "Account = " & rsData.Fields("Account") & " AND Type = 22 AND MortgageHolder = " & lngCMFMHNumber & " AND MortgageHolder <> 0 AND ProcessType = 'LMAT'"
                        rsCMFNumbers.FindFirstRecord2("Account,Type,MortgageHolder,ProcessType", rsData.Get_Fields("Account") + ",22," + FCConvert.ToString(lngCMFMHNumber) + ",LMAT", ","); // kk08122014 trocls-49   ' "'LMAT'"
                    }
                    else
                    {
                        // rsCMFNumbers.FindFirstRecord , , "Account = " & rsData.Fields("Account") & " AND Type = 22 AND InterestedPartyID = " & lngCMFMHNumber & " AND InterestedPartyID <> 0 AND ProcessType = 'LMAT'"
                        rsCMFNumbers.FindFirstRecord2("Account,Type,InterestedParty,ProcessType", rsData.Get_Fields("Account") + ",22," + FCConvert.ToString(lngCMFMHNumber) + ",LMAT", ","); // kk08122014 trocls-49   ' "'LMAT'"
                    }
                }
                else
                {
                    if (boolCopy && boolNewOwner)
                    {
                        // rsCMFNumbers.FindFirstRecord , , "Account = " & rsData.Fields("Account") & " AND Type = 22 AND NewOwner = TRUE"
                        rsCMFNumbers.FindFirstRecord2("Account,Type,NewOwner,ProcessType", rsData.Get_Fields("Account") + ",22,1,LMAT", ","); // kk08122014 trocls-49   ' rsData.Fields("Account") & ", 22, 1, 'LMAT'"
                    }
                    else
                    {
                        // rsCMFNumbers.FindFirstRecord , , "Account = " & rsData.Fields("Account") & " AND Type = 22 AND NewOwner = FALSE"
                        rsCMFNumbers.FindFirstRecord2("Account,Type,NewOwner,ProcessType", rsData.Get_Fields("Account") + ",22,0,LMAT", ","); // kk08122014 trocls-49   ' rsData.Fields("Account") & ", 22, 0, 'LMAT'"
                    }
                }
                // If lngCMFMHNumber <> 0 Or (boolPayCert And lngCMFBillKey = 0) Then
                if (rsCMFNumbers.NoMatch)
                { // Or (boolCopy And boolPrintCMF) Then
                    rsCMFNumbers.AddNew();
                }
                else
                {
                    rsCMFNumbers.Edit();
                }
                rsCMFNumbers.Set_Fields("Name", strCMFName);
                rsCMFNumbers.Set_Fields("Address1", strCMFAddr1);
                rsCMFNumbers.Set_Fields("Address2", strCMFAddr2);
                rsCMFNumbers.Set_Fields("Address3", strCMFAddr3);
                rsCMFNumbers.Set_Fields("Address4", strCMFAddr4);
                rsCMFNumbers.Set_Fields("Billkey", lngCMFBillKey);
                if (blnIsMortCopy)
                {
                    rsCMFNumbers.Set_Fields("MortgageHolder", lngCMFMHNumber);
                    if (lngCMFMHNumber != 0)
                    {
                        // mort holder
                        // kgk 11-29-2011 trocl-840  rsCMFNumbers.Fields("LabelOnly") = Not boolPayMortCert
                    }
                }
                else if (blnIsIPCopy)
                {
                    rsCMFNumbers.Set_Fields("InterestedPartyID", lngCMFMHNumber);
                    if (lngCMFMHNumber != 0)
                    {
                        // Interested Party
                        // kgk 11-29-2011 trocl-840  rsCMFNumbers.Fields("LabelOnly") = Not blnPayIPCert
                    }
                }
                else
                {
                    rsCMFNumbers.Set_Fields("NewOwner", false);
                    if (boolCopy && boolNewOwner)
                    { // is this is a new owner copy
                      // kgk 11-29-2011 trocl-840  rsCMFNumbers.Fields("LabelOnly") = Not boolPayNewOwner
                        rsCMFNumbers.Set_Fields("NewOwner", true);
                        boolNewOwner = false;
                    }
                    else
                    {
                        // kgk 11-29-2011 trocl-840  rsCMFNumbers.Fields("LabelOnly") = Not boolPayCert
                    }
                }

                rsCMFNumbers.Set_Fields("Account", rsData.Get_Fields("Account"));
                rsCMFNumbers.Set_Fields("Type", 22);
                rsCMFNumbers.Set_Fields("ProcessType", "LMAT"); // MAL@20071022
                rsCMFNumbers.Set_Fields("REPP", "RE");
                rsCMFNumbers.Update(true);
                rsTemp.Dispose();
                rsRK.Dispose();
                rsLN.Dispose();
                return;
            }
            catch (Exception ex)
            {   // ERROR_HANDLER:
                frmWait.InstancePtr.Unload();
                MessageBox.Show("Error #" + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " - " + fecherFoundation.Information.Err(ex).Description, "Calculate Variable Error - " + rsData.Get_Fields("BillKey"), MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private int CalculateMortgageHolders()
		{
			int CalculateMortgageHolders = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will return the number of mortgage holders this account has
				// MAL@20080723: Change to take Receive Copies option into account
				// Tracker Reference: 13813
				// rsMort.OpenRecordset "SELECT * FROM MortgageAssociation INNER JOIN MortgageHolders ON MortgageAssociation.MortgageHolderID = MortgageHolders.MortgageHolderID WHERE Account = " & rsData.Fields("Account") & " AND Module = 'RE'", strGNDatabase
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				rsMort.OpenRecordset("SELECT * FROM MortgageAssociation INNER JOIN MortgageHolders ON MortgageAssociation.MortgageHolderID = MortgageHolders.ID WHERE Account = " + FCConvert.ToString(rsData.Get_Fields("Account")) + " AND Module = 'RE' AND ReceiveCopies = 1", "CentralData");
				if (rsMort.EndOfFile() != true)
				{
					CalculateMortgageHolders = rsMort.RecordCount();
				}
				else
				{
					CalculateMortgageHolders = 0;
				}
				return CalculateMortgageHolders;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				CalculateMortgageHolders = 0;
			}
			return CalculateMortgageHolders;
		}
		
		public void SaveLienStatus()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (Strings.Right(strAcctList, 1) == ",")
				{
					strAcctList = Strings.Left(strAcctList, strAcctList.Length - 1);
				}
				// this statement will change the status of the accounts that have been process
				if (Strings.Trim(strAcctList) != "")
				{
					rsData.Execute("UPDATE BillingMaster SET LienProcessStatus = 5, LienStatusEligibility = 5 WHERE BillingYear/10 = " + FCConvert.ToString(intYear) + " AND Account IN (" + strAcctList + ") AND LienRecordNumber <> 0", modExtraModules.strCLDatabase);
				} 
                return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MsgBoxStyle.Critical, "Save Lien Status");
			}
		}

		
	}
}
