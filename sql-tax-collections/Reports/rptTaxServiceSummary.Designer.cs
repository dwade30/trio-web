﻿namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	partial class rptTaxServiceSummary
	{
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptTaxServiceSummary));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.fldAcct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAcres = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblMapLot = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldBook = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblBookPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPR = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAcctInfo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTaxBal1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldInterest1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTaxAmt1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalDue1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPer1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTaxBal2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldInterest2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTaxAmt2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalDue2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPer2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTaxBal3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldInterest3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTaxAmt3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalDue3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPer3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTaxBal4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldInterest4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTaxAmt4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalDue4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPer4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lnDetailBottom = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.lblFootertop = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.subReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lnHeader = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAcctInfo = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTaxAmt = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTaxBalance = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblInterest = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTotalDue = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPR = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblReportType = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.fldAcct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMapLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAcres)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMapLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBookPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAcctInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTaxBal1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldInterest1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTaxAmt1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalDue1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPer1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTaxBal2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldInterest2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTaxAmt2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalDue2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPer2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTaxBal3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldInterest3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTaxAmt3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalDue3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPer3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTaxBal4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldInterest4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTaxAmt4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalDue4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPer4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFootertop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAcctInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTaxAmt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTaxBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalDue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReportType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanGrow = false;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldAcct,
            this.fldName,
            this.fldLocation,
            this.fldMapLot,
            this.fldAcres,
            this.lblMapLot,
            this.Label1,
            this.fldBook,
            this.lblBookPage,
            this.fldPage,
            this.fldYear,
            this.fldPR,
            this.fldAcctInfo,
            this.fldTaxBal1,
            this.fldInterest1,
            this.fldTaxAmt1,
            this.fldTotalDue1,
            this.fldPer1,
            this.fldTaxBal2,
            this.fldInterest2,
            this.fldTaxAmt2,
            this.fldTotalDue2,
            this.fldPer2,
            this.fldTaxBal3,
            this.fldInterest3,
            this.fldTaxAmt3,
            this.fldTotalDue3,
            this.fldPer3,
            this.fldTaxBal4,
            this.fldInterest4,
            this.fldTaxAmt4,
            this.fldTotalDue4,
            this.fldPer4,
            this.lnDetailBottom});
            this.Detail.Height = 1.229167F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // fldAcct
            // 
            this.fldAcct.Height = 0.1875F;
            this.fldAcct.Left = 0F;
            this.fldAcct.Name = "fldAcct";
            this.fldAcct.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldAcct.Text = null;
            this.fldAcct.Top = 0F;
            this.fldAcct.Width = 0.5F;
            // 
            // fldName
            // 
            this.fldName.CanGrow = false;
            this.fldName.Height = 0.1875F;
            this.fldName.Left = 0.5F;
            this.fldName.Name = "fldName";
            this.fldName.Style = "font-family: \'Tahoma\'; white-space: nowrap";
            this.fldName.Text = null;
            this.fldName.Top = 0F;
            this.fldName.Width = 3.125F;
            // 
            // fldLocation
            // 
            this.fldLocation.Height = 0.1875F;
            this.fldLocation.Left = 3.625F;
            this.fldLocation.Name = "fldLocation";
            this.fldLocation.Style = "font-family: \'Tahoma\'";
            this.fldLocation.Text = null;
            this.fldLocation.Top = 0F;
            this.fldLocation.Width = 3.375F;
            // 
            // fldMapLot
            // 
            this.fldMapLot.Height = 0.1875F;
            this.fldMapLot.Left = 1.5F;
            this.fldMapLot.Name = "fldMapLot";
            this.fldMapLot.Style = "font-family: \'Tahoma\'";
            this.fldMapLot.Text = null;
            this.fldMapLot.Top = 0.1875F;
            this.fldMapLot.Width = 1.25F;
            // 
            // fldAcres
            // 
            this.fldAcres.Height = 0.1875F;
            this.fldAcres.Left = 3.75F;
            this.fldAcres.Name = "fldAcres";
            this.fldAcres.Style = "font-family: \'Tahoma\'";
            this.fldAcres.Text = null;
            this.fldAcres.Top = 0.1875F;
            this.fldAcres.Width = 0.625F;
            // 
            // lblMapLot
            // 
            this.lblMapLot.Height = 0.1875F;
            this.lblMapLot.HyperLink = null;
            this.lblMapLot.Left = 0.5625F;
            this.lblMapLot.Name = "lblMapLot";
            this.lblMapLot.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
            this.lblMapLot.Text = "Map / Lot:";
            this.lblMapLot.Top = 0.1875F;
            this.lblMapLot.Width = 0.9375F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.1875F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 3.0625F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
            this.Label1.Text = "Acres:";
            this.Label1.Top = 0.1875F;
            this.Label1.Width = 0.6875F;
            // 
            // fldBook
            // 
            this.fldBook.Height = 0.1875F;
            this.fldBook.Left = 5.5625F;
            this.fldBook.Name = "fldBook";
            this.fldBook.Style = "font-family: \'Tahoma\'";
            this.fldBook.Text = null;
            this.fldBook.Top = 0.1875F;
            this.fldBook.Width = 0.625F;
            // 
            // lblBookPage
            // 
            this.lblBookPage.Height = 0.1875F;
            this.lblBookPage.HyperLink = null;
            this.lblBookPage.Left = 4.5625F;
            this.lblBookPage.Name = "lblBookPage";
            this.lblBookPage.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
            this.lblBookPage.Text = "Book Page:";
            this.lblBookPage.Top = 0.1875F;
            this.lblBookPage.Width = 1F;
            // 
            // fldPage
            // 
            this.fldPage.Height = 0.1875F;
            this.fldPage.Left = 6.1875F;
            this.fldPage.Name = "fldPage";
            this.fldPage.Style = "font-family: \'Tahoma\'";
            this.fldPage.Text = null;
            this.fldPage.Top = 0.1875F;
            this.fldPage.Width = 0.625F;
            // 
            // fldYear
            // 
            this.fldYear.Height = 0.1875F;
            this.fldYear.Left = 0.1875F;
            this.fldYear.Name = "fldYear";
            this.fldYear.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldYear.Text = null;
            this.fldYear.Top = 0.375F;
            this.fldYear.Width = 0.625F;
            // 
            // fldPR
            // 
            this.fldPR.Height = 0.1875F;
            this.fldPR.Left = 0.9375F;
            this.fldPR.Name = "fldPR";
            this.fldPR.Style = "font-family: \'Tahoma\'";
            this.fldPR.Text = null;
            this.fldPR.Top = 0.375F;
            this.fldPR.Width = 0.4375F;
            // 
            // fldAcctInfo
            // 
            this.fldAcctInfo.Height = 0.1875F;
            this.fldAcctInfo.Left = 1.5F;
            this.fldAcctInfo.Name = "fldAcctInfo";
            this.fldAcctInfo.Style = "font-family: \'Tahoma\'";
            this.fldAcctInfo.Text = null;
            this.fldAcctInfo.Top = 0.375F;
            this.fldAcctInfo.Width = 1.0625F;
            // 
            // fldTaxBal1
            // 
            this.fldTaxBal1.Height = 0.1875F;
            this.fldTaxBal1.Left = 4F;
            this.fldTaxBal1.Name = "fldTaxBal1";
            this.fldTaxBal1.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTaxBal1.Text = null;
            this.fldTaxBal1.Top = 0.375F;
            this.fldTaxBal1.Width = 1F;
            // 
            // fldInterest1
            // 
            this.fldInterest1.Height = 0.1875F;
            this.fldInterest1.Left = 5F;
            this.fldInterest1.Name = "fldInterest1";
            this.fldInterest1.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldInterest1.Text = null;
            this.fldInterest1.Top = 0.375F;
            this.fldInterest1.Width = 1F;
            // 
            // fldTaxAmt1
            // 
            this.fldTaxAmt1.Height = 0.1875F;
            this.fldTaxAmt1.Left = 3F;
            this.fldTaxAmt1.Name = "fldTaxAmt1";
            this.fldTaxAmt1.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTaxAmt1.Text = null;
            this.fldTaxAmt1.Top = 0.375F;
            this.fldTaxAmt1.Width = 1F;
            // 
            // fldTotalDue1
            // 
            this.fldTotalDue1.Height = 0.1875F;
            this.fldTotalDue1.Left = 6F;
            this.fldTotalDue1.Name = "fldTotalDue1";
            this.fldTotalDue1.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalDue1.Text = null;
            this.fldTotalDue1.Top = 0.375F;
            this.fldTotalDue1.Width = 1F;
            // 
            // fldPer1
            // 
            this.fldPer1.Height = 0.1875F;
            this.fldPer1.Left = 2.6875F;
            this.fldPer1.Name = "fldPer1";
            this.fldPer1.Style = "font-family: \'Tahoma\'";
            this.fldPer1.Text = null;
            this.fldPer1.Top = 0.375F;
            this.fldPer1.Width = 0.3125F;
            // 
            // fldTaxBal2
            // 
            this.fldTaxBal2.Height = 0.1875F;
            this.fldTaxBal2.Left = 4F;
            this.fldTaxBal2.Name = "fldTaxBal2";
            this.fldTaxBal2.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTaxBal2.Text = null;
            this.fldTaxBal2.Top = 0.5625F;
            this.fldTaxBal2.Width = 1F;
            // 
            // fldInterest2
            // 
            this.fldInterest2.Height = 0.1875F;
            this.fldInterest2.Left = 5F;
            this.fldInterest2.Name = "fldInterest2";
            this.fldInterest2.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldInterest2.Text = null;
            this.fldInterest2.Top = 0.5625F;
            this.fldInterest2.Width = 1F;
            // 
            // fldTaxAmt2
            // 
            this.fldTaxAmt2.Height = 0.1875F;
            this.fldTaxAmt2.Left = 3F;
            this.fldTaxAmt2.Name = "fldTaxAmt2";
            this.fldTaxAmt2.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTaxAmt2.Text = null;
            this.fldTaxAmt2.Top = 0.5625F;
            this.fldTaxAmt2.Width = 1F;
            // 
            // fldTotalDue2
            // 
            this.fldTotalDue2.Height = 0.1875F;
            this.fldTotalDue2.Left = 6F;
            this.fldTotalDue2.Name = "fldTotalDue2";
            this.fldTotalDue2.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalDue2.Text = null;
            this.fldTotalDue2.Top = 0.5625F;
            this.fldTotalDue2.Width = 1F;
            // 
            // fldPer2
            // 
            this.fldPer2.Height = 0.1875F;
            this.fldPer2.Left = 2.6875F;
            this.fldPer2.Name = "fldPer2";
            this.fldPer2.Style = "font-family: \'Tahoma\'";
            this.fldPer2.Text = null;
            this.fldPer2.Top = 0.5625F;
            this.fldPer2.Width = 0.3125F;
            // 
            // fldTaxBal3
            // 
            this.fldTaxBal3.Height = 0.1875F;
            this.fldTaxBal3.Left = 4F;
            this.fldTaxBal3.Name = "fldTaxBal3";
            this.fldTaxBal3.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTaxBal3.Text = null;
            this.fldTaxBal3.Top = 0.75F;
            this.fldTaxBal3.Width = 1F;
            // 
            // fldInterest3
            // 
            this.fldInterest3.Height = 0.1875F;
            this.fldInterest3.Left = 5F;
            this.fldInterest3.Name = "fldInterest3";
            this.fldInterest3.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldInterest3.Text = null;
            this.fldInterest3.Top = 0.75F;
            this.fldInterest3.Width = 1F;
            // 
            // fldTaxAmt3
            // 
            this.fldTaxAmt3.Height = 0.1875F;
            this.fldTaxAmt3.Left = 3F;
            this.fldTaxAmt3.Name = "fldTaxAmt3";
            this.fldTaxAmt3.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTaxAmt3.Text = null;
            this.fldTaxAmt3.Top = 0.75F;
            this.fldTaxAmt3.Width = 1F;
            // 
            // fldTotalDue3
            // 
            this.fldTotalDue3.Height = 0.1875F;
            this.fldTotalDue3.Left = 6F;
            this.fldTotalDue3.Name = "fldTotalDue3";
            this.fldTotalDue3.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalDue3.Text = null;
            this.fldTotalDue3.Top = 0.75F;
            this.fldTotalDue3.Width = 1F;
            // 
            // fldPer3
            // 
            this.fldPer3.Height = 0.1875F;
            this.fldPer3.Left = 2.6875F;
            this.fldPer3.Name = "fldPer3";
            this.fldPer3.Style = "font-family: \'Tahoma\'";
            this.fldPer3.Text = null;
            this.fldPer3.Top = 0.75F;
            this.fldPer3.Width = 0.3125F;
            // 
            // fldTaxBal4
            // 
            this.fldTaxBal4.Height = 0.1875F;
            this.fldTaxBal4.Left = 4F;
            this.fldTaxBal4.Name = "fldTaxBal4";
            this.fldTaxBal4.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTaxBal4.Text = null;
            this.fldTaxBal4.Top = 0.9375F;
            this.fldTaxBal4.Width = 1F;
            // 
            // fldInterest4
            // 
            this.fldInterest4.Height = 0.1875F;
            this.fldInterest4.Left = 5F;
            this.fldInterest4.Name = "fldInterest4";
            this.fldInterest4.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldInterest4.Text = null;
            this.fldInterest4.Top = 0.9375F;
            this.fldInterest4.Width = 1F;
            // 
            // fldTaxAmt4
            // 
            this.fldTaxAmt4.Height = 0.1875F;
            this.fldTaxAmt4.Left = 3F;
            this.fldTaxAmt4.Name = "fldTaxAmt4";
            this.fldTaxAmt4.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTaxAmt4.Text = null;
            this.fldTaxAmt4.Top = 0.9375F;
            this.fldTaxAmt4.Width = 1F;
            // 
            // fldTotalDue4
            // 
            this.fldTotalDue4.Height = 0.1875F;
            this.fldTotalDue4.Left = 6F;
            this.fldTotalDue4.Name = "fldTotalDue4";
            this.fldTotalDue4.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalDue4.Text = null;
            this.fldTotalDue4.Top = 0.9375F;
            this.fldTotalDue4.Width = 1F;
            // 
            // fldPer4
            // 
            this.fldPer4.Height = 0.1875F;
            this.fldPer4.Left = 2.6875F;
            this.fldPer4.Name = "fldPer4";
            this.fldPer4.Style = "font-family: \'Tahoma\'";
            this.fldPer4.Text = null;
            this.fldPer4.Top = 0.9375F;
            this.fldPer4.Width = 0.3125F;
            // 
            // lnDetailBottom
            // 
            this.lnDetailBottom.Height = 0F;
            this.lnDetailBottom.Left = 0F;
            this.lnDetailBottom.LineWeight = 1F;
            this.lnDetailBottom.Name = "lnDetailBottom";
            this.lnDetailBottom.Top = 1.1875F;
            this.lnDetailBottom.Width = 7F;
            this.lnDetailBottom.X1 = 0F;
            this.lnDetailBottom.X2 = 7F;
            this.lnDetailBottom.Y1 = 1.1875F;
            this.lnDetailBottom.Y2 = 1.1875F;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblFootertop,
            this.subReport1});
            this.ReportFooter.Height = 0.8229166F;
            this.ReportFooter.KeepTogether = true;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
            // 
            // lblFootertop
            // 
            this.lblFootertop.Height = 0.25F;
            this.lblFootertop.HyperLink = null;
            this.lblFootertop.Left = 0F;
            this.lblFootertop.Name = "lblFootertop";
            this.lblFootertop.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
            this.lblFootertop.Text = "Summary";
            this.lblFootertop.Top = 0.0625F;
            this.lblFootertop.Width = 7F;
            // 
            // subReport1
            // 
            this.subReport1.CloseBorder = false;
            this.subReport1.Height = 0.125F;
            this.subReport1.Left = 0F;
            this.subReport1.Name = "subReport1";
            this.subReport1.Report = null;
            this.subReport1.ReportName = "subReport1";
            this.subReport1.Top = 0.312F;
            this.subReport1.Width = 6.937F;
            // 
            // PageHeader
            // 
            this.PageHeader.CanGrow = false;
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblHeader,
            this.lblDate,
            this.lblPage,
            this.lblTime,
            this.lblMuniName,
            this.lnHeader,
            this.lblYear,
            this.lblAcctInfo,
            this.lblTaxAmt,
            this.lblTaxBalance,
            this.lblInterest,
            this.lblTotalDue,
            this.lblPR,
            this.lblReportType});
            this.PageHeader.Height = 0.8125F;
            this.PageHeader.Name = "PageHeader";
            // 
            // lblHeader
            // 
            this.lblHeader.Height = 0.3125F;
            this.lblHeader.HyperLink = null;
            this.lblHeader.Left = 0F;
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
            this.lblHeader.Text = "Tax Service Summary Report";
            this.lblHeader.Top = 0F;
            this.lblHeader.Width = 7F;
            // 
            // lblDate
            // 
            this.lblDate.Height = 0.1875F;
            this.lblDate.HyperLink = null;
            this.lblDate.Left = 5.875F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblDate.Text = null;
            this.lblDate.Top = 0F;
            this.lblDate.Width = 1.125F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1875F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 5.875F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblPage.Text = null;
            this.lblPage.Top = 0.1875F;
            this.lblPage.Width = 1.125F;
            // 
            // lblTime
            // 
            this.lblTime.Height = 0.1875F;
            this.lblTime.HyperLink = null;
            this.lblTime.Left = 0F;
            this.lblTime.Name = "lblTime";
            this.lblTime.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblTime.Text = null;
            this.lblTime.Top = 0.1875F;
            this.lblTime.Width = 1.125F;
            // 
            // lblMuniName
            // 
            this.lblMuniName.Height = 0.1875F;
            this.lblMuniName.HyperLink = null;
            this.lblMuniName.Left = 0F;
            this.lblMuniName.Name = "lblMuniName";
            this.lblMuniName.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblMuniName.Text = null;
            this.lblMuniName.Top = 0F;
            this.lblMuniName.Width = 2.5F;
            // 
            // lnHeader
            // 
            this.lnHeader.Height = 0F;
            this.lnHeader.Left = 0F;
            this.lnHeader.LineWeight = 1F;
            this.lnHeader.Name = "lnHeader";
            this.lnHeader.Top = 0.8125F;
            this.lnHeader.Width = 6.9375F;
            this.lnHeader.X1 = 0F;
            this.lnHeader.X2 = 6.9375F;
            this.lnHeader.Y1 = 0.8125F;
            this.lnHeader.Y2 = 0.8125F;
            // 
            // lblYear
            // 
            this.lblYear.Height = 0.1875F;
            this.lblYear.HyperLink = null;
            this.lblYear.Left = 0.3125F;
            this.lblYear.Name = "lblYear";
            this.lblYear.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblYear.Text = "Year";
            this.lblYear.Top = 0.625F;
            this.lblYear.Width = 0.5F;
            // 
            // lblAcctInfo
            // 
            this.lblAcctInfo.Height = 0.1875F;
            this.lblAcctInfo.HyperLink = null;
            this.lblAcctInfo.Left = 1.5F;
            this.lblAcctInfo.Name = "lblAcctInfo";
            this.lblAcctInfo.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblAcctInfo.Text = "PD/FC/OTH";
            this.lblAcctInfo.Top = 0.625F;
            this.lblAcctInfo.Width = 1.0625F;
            // 
            // lblTaxAmt
            // 
            this.lblTaxAmt.Height = 0.1875F;
            this.lblTaxAmt.HyperLink = null;
            this.lblTaxAmt.Left = 3F;
            this.lblTaxAmt.Name = "lblTaxAmt";
            this.lblTaxAmt.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblTaxAmt.Text = "Tax Amt";
            this.lblTaxAmt.Top = 0.625F;
            this.lblTaxAmt.Width = 1F;
            // 
            // lblTaxBalance
            // 
            this.lblTaxBalance.Height = 0.1875F;
            this.lblTaxBalance.HyperLink = null;
            this.lblTaxBalance.Left = 4F;
            this.lblTaxBalance.Name = "lblTaxBalance";
            this.lblTaxBalance.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblTaxBalance.Text = "Tax Bal";
            this.lblTaxBalance.Top = 0.625F;
            this.lblTaxBalance.Width = 1F;
            // 
            // lblInterest
            // 
            this.lblInterest.Height = 0.1875F;
            this.lblInterest.HyperLink = null;
            this.lblInterest.Left = 5F;
            this.lblInterest.Name = "lblInterest";
            this.lblInterest.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblInterest.Text = "Interest";
            this.lblInterest.Top = 0.625F;
            this.lblInterest.Width = 1F;
            // 
            // lblTotalDue
            // 
            this.lblTotalDue.Height = 0.1875F;
            this.lblTotalDue.HyperLink = null;
            this.lblTotalDue.Left = 6F;
            this.lblTotalDue.Name = "lblTotalDue";
            this.lblTotalDue.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblTotalDue.Text = "Tot Due";
            this.lblTotalDue.Top = 0.625F;
            this.lblTotalDue.Width = 1F;
            // 
            // lblPR
            // 
            this.lblPR.Height = 0.1875F;
            this.lblPR.HyperLink = null;
            this.lblPR.Left = 0.9375F;
            this.lblPR.Name = "lblPR";
            this.lblPR.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
            this.lblPR.Text = "PR";
            this.lblPR.Top = 0.625F;
            this.lblPR.Width = 0.4375F;
            // 
            // lblReportType
            // 
            this.lblReportType.Height = 0.25F;
            this.lblReportType.HyperLink = null;
            this.lblReportType.Left = 0F;
            this.lblReportType.Name = "lblReportType";
            this.lblReportType.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
            this.lblReportType.Text = "Interest Date";
            this.lblReportType.Top = 0.3125F;
            this.lblReportType.Width = 7F;
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // rptTaxServiceSummary
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.25F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.25F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
            this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
            ((System.ComponentModel.ISupportInitialize)(this.fldAcct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMapLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAcres)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMapLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBookPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAcctInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTaxBal1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldInterest1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTaxAmt1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalDue1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPer1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTaxBal2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldInterest2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTaxAmt2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalDue2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPer2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTaxBal3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldInterest3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTaxAmt3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalDue3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPer3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTaxBal4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldInterest4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTaxAmt4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalDue4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPer4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFootertop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAcctInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTaxAmt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTaxBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalDue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReportType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAcct;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAcres;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBook;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBookPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPR;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAcctInfo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTaxBal1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInterest1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTaxAmt1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalDue1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPer1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTaxBal2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInterest2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTaxAmt2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalDue2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPer2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTaxBal3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInterest3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTaxAmt3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalDue3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPer3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTaxBal4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInterest4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTaxAmt4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalDue4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPer4;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnDetailBottom;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFootertop;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAcctInfo;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTaxAmt;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTaxBalance;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblInterest;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalDue;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPR;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReportType;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
        private GrapeCity.ActiveReports.SectionReportModel.SubReport subReport1;
    }
}
