﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Global;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Models;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class srptSLAllActivityDetail : FCSectionReport
	{

		decimal dblPrincipal;
		decimal dblInterest;
		decimal dblCost;
		decimal dblTotalPrincipal;
		decimal dblTotalInterest;
		decimal dblTotalCost;
		bool boolLien;
		bool boolInterestLine;
		decimal dblCurrentInterest;
		bool boolInterestNotShown;
		decimal dblTotal;
		decimal dblTotalNonInt;
		decimal dblNonInt;
        private TaxCollectionStatusReportConfiguration reportConfiguration;
        public PropertyTaxAccountBill AccountBill { get; set; } = null;
        private IEnumerable<PaymentRec> paymentsToUse = new List<PaymentRec>();
        private IEnumerator<PaymentRec> paymentEnumerator;
		public srptSLAllActivityDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        public void SetReportOption(TaxCollectionStatusReportConfiguration configuration)
        {
            reportConfiguration = configuration;
        }
		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Activity Detail";
		}

		public static srptSLAllActivityDetail InstancePtr
		{
			get
			{
				return (srptSLAllActivityDetail)Sys.GetInstance(typeof(srptSLAllActivityDetail));
			}
		}

		protected srptSLAllActivityDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {

            }
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (paymentEnumerator.Current == null)
			{
				DateTime dtAsOfDate;
				if (reportConfiguration.Options.AsOfDate.ToOADate() != 0)
				{
					dtAsOfDate = reportConfiguration.Options.AsOfDate;
				}
				else
				{
					dtAsOfDate = DateTime.Today;
				}
				if (reportConfiguration.Options.ShowCurrentInterest && boolInterestLine != true)
                {
                    dblCurrentInterest = AccountBill.NewInterest;
					//if (AccountBill.BillType == PropertyTaxBillType.Personal || ((rsBill.Get_Fields_Int32("LienRecordNumber"))) == 0)
					//{
					//	//double dblAbate1/*unused?*/;
					//	//double dblAbate2/*unused?*/;
					//	//double dblAbate3/*unused?*/;
					//	//double dblAbate4/*unused?*/;
					//	// Call CheckForAbatementPayments(rsBill.Fields("billkey"), rsBill.Fields("RateKey"), rsBill.Fields("BillingType") = "RE", dblAbate1, dblAbate2, dblAbate3, dblAbate4, rsBill.Fields("TaxDue1"), rsBill.Fields("TaxDue2"), rsBill.Fields("TaxDue3"), rsBill.Fields("TaxDue4"))
					//	// dblTotal = CalculateAccountCL(rsBill, rsBill.Fields("Account"), rptStatusListAccountDetail.dtAsOfDate, dblCurrentInterest, , , , , , , dblAbate1, dblAbate2, dblAbate3, dblAbate4)
					//	modCLCalculations.CalcBillAsOfFromPayments_702(FCConvert.ToInt32(rsBill.Get_Fields_Int32("ID")), dtAsOfDate, ref dblTotal, 0, 0, 0, ref dblCurrentInterest, ref dblNonInt);
					//}
					//else
					//{
					//	// dblTotal = CalculateAccountCLLien(rsLN, rptStatusListAccountDetail.dtAsOfDate, dblCurrentInterest)
					//	modCLCalculations.CalcBillAsOfFromPayments_702(FCConvert.ToInt32(rsBill.Get_Fields_Int32("ID")), dtAsOfDate, ref dblTotal, 0, 0, 0, ref dblCurrentInterest, ref dblNonInt);
					//}
					if (dblCurrentInterest != 0)
					{
						boolInterestLine = true;
						boolInterestNotShown = true;
					}
				}
				else
				{
					boolInterestLine = false;
				}
				//rsBill.MoveNext();
				//if (rsBill.EndOfFile())
				//{
					if (!boolInterestNotShown)
					{
						eArgs.EOF = true;
					}
					else
					{
						boolInterestNotShown = false;
						eArgs.EOF = false;
					}
				//}
				//else
				//{
				//	eArgs.EOF = false;
				//}
			}
			else
			{
				eArgs.EOF = false;
			}
			//Detail_Format();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			
			string strDateCheck = "";
			//lngBillKey = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData)));
			if (reportConfiguration.Options.ShowPayments)
			{
				SetupSLFormat();
			}
			if (reportConfiguration.Options.UseAsOfDate())
			{
				strDateCheck = " AND ISNULL(RecordedTransactionDate, '12/30/1899') <= '" + FCConvert.ToString(reportConfiguration.Options.AsOfDate) + "'";
			}
			else
			{
				strDateCheck = "";
			}

            RealEstateAccountBill realEstateAccountBill = null;
            PersonalPropertyAccountBill personalPropertyAccountBill = null;
			if (AccountBill != null)
			{
                if (AccountBill.BillType == PropertyTaxBillType.Real)
                {
                    realEstateAccountBill = (RealEstateAccountBill) AccountBill;
                }
                else
                {
                    personalPropertyAccountBill = (PersonalPropertyAccountBill) AccountBill;
                }
				Detail.Height = 250 / 1440F;
				//rsBill.OpenRecordset("SELECT * FROM BillingMaster WHERE ID = " + FCConvert.ToString(lngBillKey), modExtraModules.strCLDatabase);
				// DJW 10/29/07 Added AND ReceiptNumber >= 0 clause to PaymentRec SQL so reversal lines of charged interest were not taken into account
				//if (((rsBill.Get_Fields_Int32("LienRecordNumber"))) != 0 && !reportConfiguration.Options.PreLienOnly)
                
				if (!reportConfiguration.Options.PreLienOnly && AccountBill.BillType == PropertyTaxBillType.Real && realEstateAccountBill.Bill.HasLien())
                {
					boolLien = true;
					//rsLN.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(rsBill.Get_Fields_Int32("LienRecordNumber")), modExtraModules.strCLDatabase);
                    var lienRate = realEstateAccountBill.Bill.Lien.RateRecord;
                    var payments = realEstateAccountBill.Bill.Payments.GetAll();
                   
					if (reportConfiguration.Options.AsOfDate.ToOADate() == 0 || lienRate.CreationDate.Equals(DateTime.MinValue))
					{
						//rsData.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(rsBill.Get_Fields_Int32("LienRecordNumber")) + " AND BillCode = 'L' AND ReceiptNumber >= 0" + strDateCheck, modExtraModules.strCLDatabase);
                        paymentsToUse = payments.Where(p => p.ReceiptNumber > 0);
                    }
					else
					{
						if (lienRate.CreationDate.IsLaterThan(reportConfiguration.Options.AsOfDate))
						//if (DateAndTime.DateDiff("d", reportConfiguration.Options.AsOfDate, (DateTime)rsLN.Get_Fields_DateTime("datecreated")) > 0)
						{
							boolLien = false;
							//rsData.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngBillKey) + " AND BillCode <> 'L' AND ReceiptNumber >= 0" + strDateCheck, modExtraModules.strCLDatabase);
                            paymentsToUse = realEstateAccountBill.Bill.Payments.GetAll().Where(p => p.ReceiptNumber > 0);
                        }
						else
						{
							//rsData.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(rsBill.Get_Fields_Int32("LienRecordNumber")) + " AND BillCode = 'L' AND ReceiptNumber >= 0" + strDateCheck, modExtraModules.strCLDatabase);
                            paymentsToUse = payments.Where(p => p.ReceiptNumber > 0);
						}
					}

                    if (reportConfiguration.Options.UseAsOfDate())
                    {
                        paymentsToUse = paymentsToUse.Where(p =>
                            p.RecordedTransactionDate < reportConfiguration.Options.AsOfDate);
                    }
				}
				else
				{
					boolLien = false;
					//rsData.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngBillKey) + " AND BillCode <> 'L' AND ReceiptNumber >= 0" + strDateCheck, modExtraModules.strCLDatabase);
                    paymentsToUse = AccountBill.Payments.GetAll().Where(p => p.ReceiptNumber > 0);
                }
				if (!paymentsToUse.Any())
				{
					// no payments were found for this account
					// but still print the original bill amount
					// Unload Me
					Detail.Height = 0;
				}
				// rock on
				if (boolLien)
                {
                    var lien = realEstateAccountBill.Bill.Lien;
                    dblTotalPrincipal = lien.LienSummary.OriginalPrincipalCharged;
                    dblTotalInterest = lien.LienSummary.OriginalPreLienInterestCharged;
                    dblTotalCost = lien.LienSummary.OriginalCostsCharged;
					dblTotalNonInt = dblTotalPrincipal + dblTotalCost;
				}
				else
				{
					if (AccountBill.TaxBill.TransferFromBillingDateFirst == null  || reportConfiguration.Options.AsOfDate.ToOADate() == 0 || AccountBill.TaxBill.TransferFromBillingDateFirst.Value.Equals(DateTime.MinValue))
					{
						//dblTotalPrincipal = Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue4"));
                        dblTotalPrincipal = AccountBill.TaxBill.TaxInstallments.GetOriginalCharged();
                        // + rsBill.Fields("DemandFees")
                    }
					else
					{
						if (AccountBill.TaxBill.TransferFromBillingDateFirst.Value.IsLaterThan(reportConfiguration.Options.AsOfDate))
						//if (DateAndTime.DateDiff("d", reportConfiguration.Options.AsOfDate, (DateTime)rsBill.Get_Fields_DateTime("transferfrombillingDATEFIRST")) > 0)
						{
							dblTotalPrincipal = 0;
						}
						else
                        {
                            dblTotalPrincipal = AccountBill.TaxBill.TaxInstallments.GetOriginalCharged();
							//dblTotalPrincipal = Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue4"));
							// + rsBill.Fields("DemandFees")
						}
					}
					dblTotalInterest = 0;
					dblTotalCost = 0;
					// rsBill.Fields("DemandFees")
					dblTotalNonInt = dblTotalPrincipal + dblTotalCost;
				}

                paymentEnumerator = paymentsToUse.GetEnumerator();
                paymentEnumerator.MoveNext();
            }
			else
			{
				Detail.Height = 0;
				// no bill key was passed in
				Cancel();
			}
		}

		private void BindFields()
		{
			fldDate.Visible = true;
			fldCode.Visible = true;
			fldRef.Visible = true;
			fldPrincipal.Visible = true;
			fldInterest.Visible = true;
			fldCost.Visible = true;
			fldTotal.Visible = true;
			fldNonTotal.Visible = true;
			Detail.Height = 250 / 1440F;
			// this will put the information into the fields
			if (paymentEnumerator.Current != null)
            {
                var payment = paymentEnumerator.Current;
				// Date
				fldDate.Text = payment.RecordedTransactionDate.GetValueOrDefault().FormatAndPadShortDate();
                fldCode.Text = payment.Code;
				// Ref
                fldRef.Text = payment.Reference;

                dblPrincipal = payment.Principal.GetValueOrDefault();
                dblInterest = payment.CurrentInterest.GetValueOrDefault() + payment.PreLienInterest.GetValueOrDefault();
                dblCost = payment.LienCost.GetValueOrDefault();
                fldPrincipal.Text = dblPrincipal.FormatAsCurrencyNoSymbol();
                fldInterest.Text = dblInterest.FormatAsCurrencyNoSymbol();
                fldCost.Text = dblCost.FormatAsCurrencyNoSymbol();
				fldTotal.Text = (dblPrincipal + dblInterest + dblCost).FormatAsCurrencyNoSymbol();
				dblNonInt = dblPrincipal + dblCost;
				fldNonTotal.Text = dblNonInt.FormatAsCurrencyNoSymbol();
				// update the running total
				dblTotalPrincipal -= dblPrincipal;
				dblTotalInterest -= dblInterest;
				dblTotalCost -= dblCost;
				dblTotalNonInt -= dblNonInt;
                paymentEnumerator.MoveNext();
            }
			else if (boolInterestLine)
			{
				// Date
				fldDate.Text = "";
				// Code
				fldCode.Text = "";
				// Ref
				fldRef.Text = "CURINT";
                dblPrincipal = 0;
				
				dblInterest = dblCurrentInterest * -1;
				dblCost = 0;
				dblNonInt = 0;
                fldPrincipal.Text = dblPrincipal.FormatAsCurrencyNoSymbol();
				fldInterest.Text = dblInterest.FormatAsCurrencyNoSymbol();
				fldCost.Text = dblCost.FormatAsCurrencyNoSymbol();
				fldTotal.Text = (dblPrincipal + dblInterest + dblCost).FormatAsCurrencyNoSymbol();
				fldNonTotal.Text = "0.00";
                dblTotalInterest -= dblInterest;
			}
			else
			{
				fldDate.Visible = false;
				fldCode.Visible = false;
				fldRef.Visible = false;
				fldPrincipal.Visible = false;
				fldInterest.Visible = false;
				fldCost.Visible = false;
				fldTotal.Visible = false;
				fldNonTotal.Visible = false;
				//FC:FINAL:DDU:#i324 remove the unnecessary line
				lnFooterTotal.Visible = false;
				Detail.Height = 0;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (reportConfiguration.Options.ShowPayments)
			{
				SetupSLFormat();
			}
			BindFields();
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			fldTotalPrincipal.Text = dblTotalPrincipal.FormatAsCurrencyNoSymbol();
			fldTotalInterest.Text = dblTotalInterest.FormatAsCurrencyNoSymbol();
			fldTotalCost.Text = dblTotalCost.FormatAsCurrencyNoSymbol();
			fldTotalTotal.Text = (dblTotalPrincipal + dblTotalInterest + dblTotalCost).FormatAsCurrencyNoSymbol();
			fldNonTotalTotal.Text = dblTotalNonInt.FormatAsCurrencyNoSymbol();
		}

		private void ReportHeader_Format(object sender, EventArgs e)
		{
			fldOriginalPrincipal.Text = dblTotalPrincipal.FormatAsCurrencyNoSymbol();
			fldOriginalInterest.Text = dblTotalInterest.FormatAsCurrencyNoSymbol();
			fldOriginalCost.Text = dblTotalCost.FormatAsCurrencyNoSymbol();
			fldOriginalTotal.Text = (dblTotalPrincipal + dblTotalInterest + dblTotalCost).FormatAsCurrencyNoSymbol();
			fldOriginalNonInt.Text = (dblTotalPrincipal + dblTotalCost).FormatAsCurrencyNoSymbol();
		}

		private void SetupSLFormat()
		{
			// this routine will move all of the fields to the correct places/sizes for Status Lists
			float lngTop;
			int lngFS;
			float[] lngLeft = new float[5 + 1];
			float lngWid;
			lngLeft[0] = 0;
			lngLeft[1] = 4000 / 1440F;
			// prin
			lngLeft[2] = 5500 / 1440F;
			// int
			lngLeft[3] = 7000 / 1440F;
			// costs
			lngLeft[4] = 8500 / 1440F;
			// total
			lngFS = 8;
			lngWid = 1500 / 1440F;
			// header
			// show the labels for the header
			fldHeaderPrincipal.Visible = true;
			fldHeaderInterest.Visible = true;
			fldHeaderCost.Visible = true;
			fldHeaderTotal.Visible = true;
			fldHeaderPrincipal.Text = "Principal";
			fldHeaderInterest.Text = "Interest";
			fldHeaderCost.Text = "Cost";
			fldHeaderTotal.Text = "Total";
			fldHeaderNonTotal.Text = "Non Int.";
			lngTop = fldHeaderPrincipal.Height;
			fldOriginalPrincipal.Top = lngTop;
			fldOriginalInterest.Top = lngTop;
			fldOriginalCost.Top = lngTop;
			fldOriginalTotal.Top = lngTop;
			fldOriginalNonInt.Top = lngTop;
			fldOriginal.Top = lngTop;
			fldHeaderDate.Top = lngTop;
			lnHeadTotals.Y1 = lngTop * 2;
			lnHeadTotals.Y2 = lngTop * 2;
			ReportHeader.Height = lngTop * 2;
			fldOriginalPrincipal.Font = new Font(fldOriginalPrincipal.Font.Name, lngFS);
			fldOriginalInterest.Font = new Font(fldOriginalInterest.Font.Name, lngFS);
			fldOriginalCost.Font = new Font(fldOriginalCost.Font.Name, lngFS);
			fldOriginalTotal.Font = new Font(fldOriginalTotal.Font.Name, lngFS);
			fldOriginalNonInt.Font = new Font(fldOriginalNonInt.Font.Name, lngFS);
			fldHeaderPrincipal.Font = new Font(fldHeaderPrincipal.Font.Name, lngFS);
			fldHeaderInterest.Font = new Font(fldHeaderInterest.Font.Name, lngFS);
			fldHeaderCost.Font = new Font(fldHeaderCost.Font.Name, lngFS);
			fldHeaderNonTotal.Font = new Font(fldHeaderNonTotal.Font.Name, lngFS);
			fldHeaderTotal.Font = new Font(fldHeaderTotal.Font.Name, lngFS);
			fldHeaderDate.Font = new Font(fldHeaderDate.Font.Name, lngFS);
			fldDate.Font = new Font(fldDate.Font.Name, lngFS);
			fldRef.Font = new Font(fldRef.Font.Name, lngFS);
			fldCode.Font = new Font(fldCode.Font.Name, lngFS);
			fldPrincipal.Font = new Font(fldPrincipal.Font.Name, lngFS);
			fldInterest.Font = new Font(fldInterest.Font.Name, lngFS);
			fldCost.Font = new Font(fldCost.Font.Name, lngFS);
			fldTotal.Font = new Font(fldTotal.Font.Name, lngFS);
			fldNonTotal.Font = new Font(fldNonTotal.Font.Name, lngFS);
			lblFooter.Font = new Font(lblFooter.Font.Name, lngFS);
			fldTotalPrincipal.Font = new Font(fldTotalPrincipal.Font.Name, lngFS);
			fldTotalInterest.Font = new Font(fldTotalInterest.Font.Name, lngFS);
			fldTotalCost.Font = new Font(fldTotalCost.Font.Name, lngFS);
			fldTotalTotal.Font = new Font(fldTotalTotal.Font.Name, lngFS);
			fldNonTotalTotal.Font = new Font(fldNonTotalTotal.Font.Name, lngFS);

		}

		
	}
}
