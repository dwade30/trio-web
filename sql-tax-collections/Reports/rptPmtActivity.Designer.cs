namespace TWCL0000
{
    /// <summary>
    /// Summary description for SectionReport1.
    /// </summary>
    partial class rptPmtActivity
    {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptPmtActivity));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldReceipt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPLI = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldRef = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.fldTotalCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lnTotals = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblTotals = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldTotalPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalPLI = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblRegTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblLienTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldRegularCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldLienCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldRegularPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldLienPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldRegularInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldLienInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldRegularPLI = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldLienPLI = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldRegularCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldLienCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldRegularTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldLienTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lnHeader = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblHeaderAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblHeaderYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblReceipt = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCost = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblReportType = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPrincipal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblInterest = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPLI = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblHeaderDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblHeaderReference = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblHeaderCode = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldReceipt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPLI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRef)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPLI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLienTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRegularCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLienCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRegularPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLienPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRegularInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLienInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRegularPLI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLienPLI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRegularCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLienCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRegularTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLienTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReceipt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReportType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPLI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanShrink = true;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldAccount,
            this.fldYear,
            this.fldReceipt,
            this.fldCost,
            this.fldTotal,
            this.fldPrincipal,
            this.fldInterest,
            this.fldPLI,
            this.fldRef,
            this.fldCode,
            this.fldDate});
            this.Detail.Height = 0.221F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // fldAccount
            // 
            this.fldAccount.Height = 0.1875F;
            this.fldAccount.Left = 1F;
            this.fldAccount.Name = "fldAccount";
            this.fldAccount.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldAccount.Text = null;
            this.fldAccount.Top = 0F;
            this.fldAccount.Width = 0.6980001F;
            // 
            // fldYear
            // 
            this.fldYear.Height = 0.1875F;
            this.fldYear.Left = 1.885F;
            this.fldYear.Name = "fldYear";
            this.fldYear.Style = "font-family: \'Tahoma\'; text-align: left";
            this.fldYear.Text = null;
            this.fldYear.Top = 0F;
            this.fldYear.Width = 0.4375F;
            // 
            // fldReceipt
            // 
            this.fldReceipt.Height = 0.1875F;
            this.fldReceipt.Left = 3.656F;
            this.fldReceipt.Name = "fldReceipt";
            this.fldReceipt.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldReceipt.Text = null;
            this.fldReceipt.Top = 0F;
            this.fldReceipt.Width = 0.7915001F;
            // 
            // fldCost
            // 
            this.fldCost.Height = 0.1875F;
            this.fldCost.Left = 7.542F;
            this.fldCost.Name = "fldCost";
            this.fldCost.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldCost.Text = null;
            this.fldCost.Top = 0F;
            this.fldCost.Width = 0.8954997F;
            // 
            // fldTotal
            // 
            this.fldTotal.Height = 0.1875F;
            this.fldTotal.Left = 8.5F;
            this.fldTotal.Name = "fldTotal";
            this.fldTotal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotal.Text = null;
            this.fldTotal.Top = 0F;
            this.fldTotal.Width = 1F;
            // 
            // fldPrincipal
            // 
            this.fldPrincipal.Height = 0.1875F;
            this.fldPrincipal.Left = 4.5F;
            this.fldPrincipal.Name = "fldPrincipal";
            this.fldPrincipal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldPrincipal.Text = null;
            this.fldPrincipal.Top = 0F;
            this.fldPrincipal.Width = 0.9375F;
            // 
            // fldInterest
            // 
            this.fldInterest.Height = 0.1875F;
            this.fldInterest.Left = 5.5F;
            this.fldInterest.Name = "fldInterest";
            this.fldInterest.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldInterest.Text = null;
            this.fldInterest.Top = 0F;
            this.fldInterest.Width = 0.9375F;
            // 
            // fldPLI
            // 
            this.fldPLI.Height = 0.1875F;
            this.fldPLI.Left = 6.499F;
            this.fldPLI.Name = "fldPLI";
            this.fldPLI.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldPLI.Text = null;
            this.fldPLI.Top = 0F;
            this.fldPLI.Width = 0.9375F;
            // 
            // fldRef
            // 
            this.fldRef.Height = 0.2F;
            this.fldRef.Left = 2.417F;
            this.fldRef.Name = "fldRef";
            this.fldRef.Style = "font-family: Tahoma; font-size: 10pt; ddo-char-set: 1";
            this.fldRef.Text = null;
            this.fldRef.Top = 0F;
            this.fldRef.Width = 0.7080002F;
            // 
            // fldCode
            // 
            this.fldCode.Height = 0.2F;
            this.fldCode.Left = 3.219F;
            this.fldCode.Name = "fldCode";
            this.fldCode.Text = null;
            this.fldCode.Top = 0F;
            this.fldCode.Width = 0.3430004F;
            // 
            // fldDate
            // 
            this.fldDate.Height = 0.2F;
            this.fldDate.Left = 0F;
            this.fldDate.Name = "fldDate";
            this.fldDate.Style = "font-family: Tahoma; font-size: 10pt; ddo-char-set: 1";
            this.fldDate.Text = null;
            this.fldDate.Top = 0F;
            this.fldDate.Width = 0.9480001F;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldTotalCount,
            this.fldTotalCost,
            this.fldTotalTotal,
            this.lnTotals,
            this.lblTotals,
            this.fldTotalPrincipal,
            this.fldTotalInterest,
            this.fldTotalPLI,
            this.lblRegTotal,
            this.lblLienTotal,
            this.fldRegularCount,
            this.fldLienCount,
            this.fldRegularPrincipal,
            this.fldLienPrincipal,
            this.fldRegularInterest,
            this.fldLienInterest,
            this.fldRegularPLI,
            this.fldLienPLI,
            this.fldRegularCost,
            this.fldLienCost,
            this.fldRegularTotal,
            this.fldLienTotal});
            this.ReportFooter.Height = 1.229167F;
            this.ReportFooter.KeepTogether = true;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
            // 
            // fldTotalCount
            // 
            this.fldTotalCount.Height = 0.1875F;
            this.fldTotalCount.Left = 3.802F;
            this.fldTotalCount.Name = "fldTotalCount";
            this.fldTotalCount.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalCount.Text = null;
            this.fldTotalCount.Top = 0F;
            this.fldTotalCount.Width = 0.6454999F;
            // 
            // fldTotalCost
            // 
            this.fldTotalCost.Height = 0.1875F;
            this.fldTotalCost.Left = 7.542F;
            this.fldTotalCost.Name = "fldTotalCost";
            this.fldTotalCost.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalCost.Text = null;
            this.fldTotalCost.Top = 0F;
            this.fldTotalCost.Width = 0.8954997F;
            // 
            // fldTotalTotal
            // 
            this.fldTotalTotal.Height = 0.1875F;
            this.fldTotalTotal.Left = 8.5F;
            this.fldTotalTotal.Name = "fldTotalTotal";
            this.fldTotalTotal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalTotal.Text = null;
            this.fldTotalTotal.Top = 0F;
            this.fldTotalTotal.Width = 1F;
            // 
            // lnTotals
            // 
            this.lnTotals.Height = 0F;
            this.lnTotals.Left = 3.375F;
            this.lnTotals.LineWeight = 1F;
            this.lnTotals.Name = "lnTotals";
            this.lnTotals.Top = 0F;
            this.lnTotals.Width = 6.125F;
            this.lnTotals.X1 = 3.375F;
            this.lnTotals.X2 = 9.5F;
            this.lnTotals.Y1 = 0F;
            this.lnTotals.Y2 = 0F;
            // 
            // lblTotals
            // 
            this.lblTotals.Height = 0.1875F;
            this.lblTotals.HyperLink = null;
            this.lblTotals.Left = 2.625F;
            this.lblTotals.Name = "lblTotals";
            this.lblTotals.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblTotals.Text = "Total";
            this.lblTotals.Top = 0F;
            this.lblTotals.Width = 1.115F;
            // 
            // fldTotalPrincipal
            // 
            this.fldTotalPrincipal.Height = 0.1875F;
            this.fldTotalPrincipal.Left = 4.5F;
            this.fldTotalPrincipal.Name = "fldTotalPrincipal";
            this.fldTotalPrincipal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalPrincipal.Text = null;
            this.fldTotalPrincipal.Top = 0F;
            this.fldTotalPrincipal.Width = 0.9375F;
            // 
            // fldTotalInterest
            // 
            this.fldTotalInterest.Height = 0.1875F;
            this.fldTotalInterest.Left = 5.5F;
            this.fldTotalInterest.Name = "fldTotalInterest";
            this.fldTotalInterest.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalInterest.Text = null;
            this.fldTotalInterest.Top = 0F;
            this.fldTotalInterest.Width = 0.9375F;
            // 
            // fldTotalPLI
            // 
            this.fldTotalPLI.Height = 0.1875F;
            this.fldTotalPLI.Left = 6.499F;
            this.fldTotalPLI.Name = "fldTotalPLI";
            this.fldTotalPLI.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalPLI.Text = null;
            this.fldTotalPLI.Top = 0F;
            this.fldTotalPLI.Width = 0.9375F;
            // 
            // lblRegTotal
            // 
            this.lblRegTotal.Height = 0.1875F;
            this.lblRegTotal.HyperLink = null;
            this.lblRegTotal.Left = 2.625F;
            this.lblRegTotal.Name = "lblRegTotal";
            this.lblRegTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblRegTotal.Text = "Regular Total";
            this.lblRegTotal.Top = 0.553F;
            this.lblRegTotal.Width = 1.115F;
            // 
            // lblLienTotal
            // 
            this.lblLienTotal.Height = 0.1875F;
            this.lblLienTotal.HyperLink = null;
            this.lblLienTotal.Left = 2.625F;
            this.lblLienTotal.Name = "lblLienTotal";
            this.lblLienTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblLienTotal.Text = "Lien Total";
            this.lblLienTotal.Top = 0.803F;
            this.lblLienTotal.Width = 1.115F;
            // 
            // fldRegularCount
            // 
            this.fldRegularCount.Height = 0.1875F;
            this.fldRegularCount.Left = 3.802F;
            this.fldRegularCount.Name = "fldRegularCount";
            this.fldRegularCount.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldRegularCount.Text = null;
            this.fldRegularCount.Top = 0.553F;
            this.fldRegularCount.Width = 0.6454999F;
            // 
            // fldLienCount
            // 
            this.fldLienCount.Height = 0.1875F;
            this.fldLienCount.Left = 3.802F;
            this.fldLienCount.Name = "fldLienCount";
            this.fldLienCount.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldLienCount.Text = null;
            this.fldLienCount.Top = 0.803F;
            this.fldLienCount.Width = 0.6454999F;
            // 
            // fldRegularPrincipal
            // 
            this.fldRegularPrincipal.Height = 0.1875F;
            this.fldRegularPrincipal.Left = 4.5F;
            this.fldRegularPrincipal.Name = "fldRegularPrincipal";
            this.fldRegularPrincipal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldRegularPrincipal.Text = null;
            this.fldRegularPrincipal.Top = 0.553F;
            this.fldRegularPrincipal.Width = 0.9375F;
            // 
            // fldLienPrincipal
            // 
            this.fldLienPrincipal.Height = 0.1875F;
            this.fldLienPrincipal.Left = 4.5F;
            this.fldLienPrincipal.Name = "fldLienPrincipal";
            this.fldLienPrincipal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldLienPrincipal.Text = null;
            this.fldLienPrincipal.Top = 0.803F;
            this.fldLienPrincipal.Width = 0.9375F;
            // 
            // fldRegularInterest
            // 
            this.fldRegularInterest.Height = 0.1875F;
            this.fldRegularInterest.Left = 5.5F;
            this.fldRegularInterest.Name = "fldRegularInterest";
            this.fldRegularInterest.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldRegularInterest.Text = null;
            this.fldRegularInterest.Top = 0.553F;
            this.fldRegularInterest.Width = 0.9375F;
            // 
            // fldLienInterest
            // 
            this.fldLienInterest.Height = 0.1875F;
            this.fldLienInterest.Left = 5.5F;
            this.fldLienInterest.Name = "fldLienInterest";
            this.fldLienInterest.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldLienInterest.Text = null;
            this.fldLienInterest.Top = 0.803F;
            this.fldLienInterest.Width = 0.9375F;
            // 
            // fldRegularPLI
            // 
            this.fldRegularPLI.Height = 0.1875F;
            this.fldRegularPLI.Left = 6.499F;
            this.fldRegularPLI.Name = "fldRegularPLI";
            this.fldRegularPLI.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldRegularPLI.Text = null;
            this.fldRegularPLI.Top = 0.553F;
            this.fldRegularPLI.Width = 0.9375F;
            // 
            // fldLienPLI
            // 
            this.fldLienPLI.Height = 0.1875F;
            this.fldLienPLI.Left = 6.499F;
            this.fldLienPLI.Name = "fldLienPLI";
            this.fldLienPLI.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldLienPLI.Text = null;
            this.fldLienPLI.Top = 0.803F;
            this.fldLienPLI.Width = 0.9375F;
            // 
            // fldRegularCost
            // 
            this.fldRegularCost.Height = 0.1875F;
            this.fldRegularCost.Left = 7.542F;
            this.fldRegularCost.Name = "fldRegularCost";
            this.fldRegularCost.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldRegularCost.Text = null;
            this.fldRegularCost.Top = 0.553F;
            this.fldRegularCost.Width = 0.8955F;
            // 
            // fldLienCost
            // 
            this.fldLienCost.Height = 0.1875F;
            this.fldLienCost.Left = 7.542F;
            this.fldLienCost.Name = "fldLienCost";
            this.fldLienCost.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldLienCost.Text = null;
            this.fldLienCost.Top = 0.803F;
            this.fldLienCost.Width = 0.8955F;
            // 
            // fldRegularTotal
            // 
            this.fldRegularTotal.Height = 0.1875F;
            this.fldRegularTotal.Left = 8.5F;
            this.fldRegularTotal.Name = "fldRegularTotal";
            this.fldRegularTotal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldRegularTotal.Text = null;
            this.fldRegularTotal.Top = 0.553F;
            this.fldRegularTotal.Width = 1F;
            // 
            // fldLienTotal
            // 
            this.fldLienTotal.Height = 0.1875F;
            this.fldLienTotal.Left = 8.5F;
            this.fldLienTotal.Name = "fldLienTotal";
            this.fldLienTotal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldLienTotal.Text = null;
            this.fldLienTotal.Top = 0.803F;
            this.fldLienTotal.Width = 1F;
            // 
            // PageHeader
            // 
            this.PageHeader.CanGrow = false;
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblHeader,
            this.lblDate,
            this.lblPage,
            this.lblTime,
            this.lblMuniName,
            this.lnHeader,
            this.lblHeaderAccount,
            this.lblHeaderYear,
            this.lblReceipt,
            this.lblCost,
            this.lblTotal,
            this.lblReportType,
            this.lblPrincipal,
            this.lblInterest,
            this.lblPLI,
            this.lblHeaderDate,
            this.lblHeaderReference,
            this.lblHeaderCode});
            this.PageHeader.Height = 1.231F;
            this.PageHeader.Name = "PageHeader";
            // 
            // lblHeader
            // 
            this.lblHeader.Height = 0.279F;
            this.lblHeader.HyperLink = null;
            this.lblHeader.Left = 0F;
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
            this.lblHeader.Text = "Payment Activity Report";
            this.lblHeader.Top = 0F;
            this.lblHeader.Width = 9.4375F;
            // 
            // lblDate
            // 
            this.lblDate.Height = 0.1875F;
            this.lblDate.HyperLink = null;
            this.lblDate.Left = 8.3125F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblDate.Text = null;
            this.lblDate.Top = 0F;
            this.lblDate.Width = 1.125F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1875F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 8.3125F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblPage.Text = null;
            this.lblPage.Top = 0.1875F;
            this.lblPage.Width = 1.125F;
            // 
            // lblTime
            // 
            this.lblTime.Height = 0.1875F;
            this.lblTime.HyperLink = null;
            this.lblTime.Left = 0F;
            this.lblTime.Name = "lblTime";
            this.lblTime.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblTime.Text = null;
            this.lblTime.Top = 0.1875F;
            this.lblTime.Width = 1.125F;
            // 
            // lblMuniName
            // 
            this.lblMuniName.Height = 0.1875F;
            this.lblMuniName.HyperLink = null;
            this.lblMuniName.Left = 0F;
            this.lblMuniName.Name = "lblMuniName";
            this.lblMuniName.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblMuniName.Text = null;
            this.lblMuniName.Top = 0F;
            this.lblMuniName.Width = 2.625F;
            // 
            // lnHeader
            // 
            this.lnHeader.Height = 0F;
            this.lnHeader.Left = 0F;
            this.lnHeader.LineWeight = 1F;
            this.lnHeader.Name = "lnHeader";
            this.lnHeader.Top = 1.1875F;
            this.lnHeader.Width = 9.5625F;
            this.lnHeader.X1 = 0F;
            this.lnHeader.X2 = 9.5625F;
            this.lnHeader.Y1 = 1.1875F;
            this.lnHeader.Y2 = 1.1875F;
            // 
            // lblHeaderAccount
            // 
            this.lblHeaderAccount.Height = 0.1875F;
            this.lblHeaderAccount.HyperLink = null;
            this.lblHeaderAccount.Left = 1F;
            this.lblHeaderAccount.Name = "lblHeaderAccount";
            this.lblHeaderAccount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblHeaderAccount.Text = "Account";
            this.lblHeaderAccount.Top = 1F;
            this.lblHeaderAccount.Width = 0.6980001F;
            // 
            // lblHeaderYear
            // 
            this.lblHeaderYear.Height = 0.1875F;
            this.lblHeaderYear.HyperLink = null;
            this.lblHeaderYear.Left = 1.885F;
            this.lblHeaderYear.Name = "lblHeaderYear";
            this.lblHeaderYear.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
            this.lblHeaderYear.Text = "Year";
            this.lblHeaderYear.Top = 1F;
            this.lblHeaderYear.Width = 0.4375F;
            // 
            // lblReceipt
            // 
            this.lblReceipt.Height = 0.1875F;
            this.lblReceipt.HyperLink = null;
            this.lblReceipt.Left = 3.656F;
            this.lblReceipt.Name = "lblReceipt";
            this.lblReceipt.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblReceipt.Text = "Receipt";
            this.lblReceipt.Top = 1F;
            this.lblReceipt.Width = 0.7915001F;
            // 
            // lblCost
            // 
            this.lblCost.Height = 0.2005F;
            this.lblCost.HyperLink = null;
            this.lblCost.Left = 7.542F;
            this.lblCost.Name = "lblCost";
            this.lblCost.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblCost.Text = "Cost";
            this.lblCost.Top = 1F;
            this.lblCost.Width = 0.8954997F;
            // 
            // lblTotal
            // 
            this.lblTotal.Height = 0.1875F;
            this.lblTotal.HyperLink = null;
            this.lblTotal.Left = 8.5F;
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblTotal.Text = "Total";
            this.lblTotal.Top = 1F;
            this.lblTotal.Width = 1F;
            // 
            // lblReportType
            // 
            this.lblReportType.Height = 0.5415F;
            this.lblReportType.HyperLink = null;
            this.lblReportType.Left = 1.125F;
            this.lblReportType.Name = "lblReportType";
            this.lblReportType.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
            this.lblReportType.Text = "Report Type";
            this.lblReportType.Top = 0.279F;
            this.lblReportType.Width = 7.187F;
            // 
            // lblPrincipal
            // 
            this.lblPrincipal.Height = 0.1875F;
            this.lblPrincipal.HyperLink = null;
            this.lblPrincipal.Left = 4.5F;
            this.lblPrincipal.Name = "lblPrincipal";
            this.lblPrincipal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblPrincipal.Text = "Principal";
            this.lblPrincipal.Top = 1F;
            this.lblPrincipal.Width = 0.9375F;
            // 
            // lblInterest
            // 
            this.lblInterest.Height = 0.2005F;
            this.lblInterest.HyperLink = null;
            this.lblInterest.Left = 5.5F;
            this.lblInterest.Name = "lblInterest";
            this.lblInterest.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblInterest.Text = "Interest";
            this.lblInterest.Top = 1F;
            this.lblInterest.Width = 0.9375F;
            // 
            // lblPLI
            // 
            this.lblPLI.Height = 0.2005F;
            this.lblPLI.HyperLink = null;
            this.lblPLI.Left = 6.499F;
            this.lblPLI.Name = "lblPLI";
            this.lblPLI.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblPLI.Text = "PLI";
            this.lblPLI.Top = 1F;
            this.lblPLI.Width = 0.9375F;
            // 
            // lblHeaderDate
            // 
            this.lblHeaderDate.Height = 0.2F;
            this.lblHeaderDate.HyperLink = null;
            this.lblHeaderDate.Left = 0F;
            this.lblHeaderDate.Name = "lblHeaderDate";
            this.lblHeaderDate.Style = "font-family: Tahoma; font-size: 10pt; font-weight: bold; text-align: center; ddo-" +
    "char-set: 1";
            this.lblHeaderDate.Text = "Date";
            this.lblHeaderDate.Top = 1F;
            this.lblHeaderDate.Width = 0.802F;
            // 
            // lblHeaderReference
            // 
            this.lblHeaderReference.Height = 0.2F;
            this.lblHeaderReference.HyperLink = null;
            this.lblHeaderReference.Left = 2.417F;
            this.lblHeaderReference.Name = "lblHeaderReference";
            this.lblHeaderReference.Style = "font-family: Tahoma; font-size: 10pt; font-weight: bold; ddo-char-set: 1";
            this.lblHeaderReference.Text = "Ref";
            this.lblHeaderReference.Top = 1F;
            this.lblHeaderReference.Width = 0.3959999F;
            // 
            // lblHeaderCode
            // 
            this.lblHeaderCode.Height = 0.2F;
            this.lblHeaderCode.HyperLink = null;
            this.lblHeaderCode.Left = 3.125F;
            this.lblHeaderCode.Name = "lblHeaderCode";
            this.lblHeaderCode.Style = "font-family: Tahoma; font-size: 10pt; font-weight: bold; text-align: right; ddo-c" +
    "har-set: 1";
            this.lblHeaderCode.Text = "Code";
            this.lblHeaderCode.Top = 1F;
            this.lblHeaderCode.Width = 0.437F;
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // rptPmtActivity
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Top = 0.25F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 8.5F;
            this.PageSettings.PaperWidth = 11F;
            this.PrintWidth = 9.5625F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
            this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
            this.PageEnd += new System.EventHandler(this.ActiveReport_PageEnd);
            ((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldReceipt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPLI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRef)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPLI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLienTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRegularCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLienCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRegularPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLienPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRegularInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLienInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRegularPLI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLienPLI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRegularCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLienCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRegularTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLienTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReceipt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReportType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPLI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReceipt;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCost;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrincipal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInterest;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPLI;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCost;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnTotals;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTotals;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPrincipal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalInterest;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPLI;
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHeaderAccount;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHeaderYear;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblReceipt;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCost;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblReportType;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPrincipal;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblInterest;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPLI;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHeaderDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRef;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHeaderReference;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHeaderCode;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblRegTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblLienTotal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegularCount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLienCount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegularPrincipal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLienPrincipal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegularInterest;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLienInterest;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegularPLI;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLienPLI;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegularCost;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLienCost;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegularTotal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLienTotal;
    }
}