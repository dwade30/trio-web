﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptReminderSummary : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               10/28/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/02/2004              *
		// ********************************************************
		bool boolDone;
		double dblTotal;
		int lngCount;
		int lngAccts;
		int lngRetry;

		public rptReminderSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Reminder Notice Summary";
		}

		public static rptReminderSummary InstancePtr
		{
			get
			{
				return (rptReminderSummary)Sys.GetInstance(typeof(rptReminderSummary));
			}
		}

		protected rptReminderSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = boolDone;
			//Detail_Format();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			// MAL@20070910: Added code to display page numbers - Per standards
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			dblTotal = 0;
			lngCount = 0;
			switch (modReminderNoticeSummary.Statics.intRPTReminderSummaryType)
			{
				case 0:
					{
						// Labels
						fldTotal.Visible = false;
						lblTotal.Visible = false;
						fldSumTotal.Visible = false;
						lnFooter.Visible = false;
						break;
					}
				case 1:
					{
						// Post Cards
						fldTotal.Visible = false;
						lblTotal.Visible = false;
						fldSumTotal.Visible = false;
						lnFooter.Visible = false;
						break;
					}
				case 2:
					{
						// Forms
						fldTotal.Visible = true;
						lblTotal.Visible = true;
						lblYear.Visible = false;
						break;
					}
				case 3:
					{
						// Mailers
						fldTotal.Visible = true;
						lblTotal.Visible = true;
						break;
					}
			}
			//end switch
			// MAL@20070910: Added code in to show Town Name, Date and Time - Per Standards
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will print the row from the grid
				TRYAGAIN:
				;
				if (lngCount <= Information.UBound(modReminderNoticeSummary.Statics.arrReminderSummaryList, 1))
				{
					if (modReminderNoticeSummary.Statics.arrReminderSummaryList[lngCount].Account == 0 && modReminderNoticeSummary.Statics.arrReminderSummaryList[lngCount].Name1 == "" && modReminderNoticeSummary.Statics.arrReminderSummaryList[lngCount].Total == 0)
					{
						lngRetry += 1;
						lngCount += 1;
						goto TRYAGAIN;
					}
					fldAccount.Text = modReminderNoticeSummary.Statics.arrReminderSummaryList[lngCount].Account.ToString();
					if (modReminderNoticeSummary.Statics.arrReminderSummaryList[lngCount].Year != 0 && modReminderNoticeSummary.Statics.intRPTReminderSummaryType != 2)
					{
						fldYear.Text = modReminderNoticeSummary.Statics.arrReminderSummaryList[lngCount].Year.ToString();
					}
					else
					{
						fldYear.Text = "";
					}
					fldName.Text = modReminderNoticeSummary.Statics.arrReminderSummaryList[lngCount].Name1;
					fldTotal.Text = Strings.Format(modReminderNoticeSummary.Statics.arrReminderSummaryList[lngCount].Total, "#,##0.00");
					dblTotal += modReminderNoticeSummary.Statics.arrReminderSummaryList[lngCount].Total;
					lngCount += 1;
					boolDone = false;
				}
				else
				{
					boolDone = true;
					fldYear.Text = "";
					fldName.Text = "";
					fldTotal.Text = "";
					fldAccount.Text = "";
					frmWait.InstancePtr.Unload();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				boolDone = true;
				if (Information.Err(ex).Number != 9)
				{
					FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Binding Fields");
				}
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			switch (modReminderNoticeSummary.Statics.intRPTReminderSummaryType)
			{
				case 0:
				case 1:
					{
						// Labels & Post Cards
						if (lngCount - lngRetry == 1)
						{
							fldTotalCount.Text = "There was " + FCConvert.ToString(lngCount - lngRetry) + " account processed.";
						}
						else
						{
							fldTotalCount.Text = "There were " + FCConvert.ToString(lngCount - lngRetry) + " accounts processed.";
						}
						fldSumTotal.Text = "";
						fldTotalCount.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
						fldTotalCount.Left = 0;
						break;
					}
				case 2:
				case 3:
					{
						// Forms & Mailers
						fldTotalCount.Text = "Total for " + FCConvert.ToString(lngCount - lngRetry) + " accounts:";
						fldSumTotal.Text = Strings.Format(dblTotal, "$#,##0.00");
						break;
					}
			}
			//end switch
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		
	}
}
