﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TWCL0000.Reports
{
    public class OutstandingBalanceSummaryItem
    {
        public string Description { get; set; }
        public decimal Total { get; set; }
        public int Count { get; set; }
        public OutstandingBalanceSummaryItem(string description,int count,decimal total)
        {
            Description = description;
            Count = count;
            Total = total;
        }
    }
}
