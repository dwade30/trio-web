﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptTaxClubActivity : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// Date           :               02/06/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// Last Updated   :               12/06/2005              *
		// ********************************************************
		string strSQL;
		int intType;
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsLData = new clsDRWrapper();
		int lngTotalAccounts;
		int lngCount;

		public rptTaxClubActivity()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptTaxClubActivity InstancePtr
		{
			get
			{
				return (rptTaxClubActivity)Sys.GetInstance(typeof(rptTaxClubActivity));
			}
		}

		protected rptTaxClubActivity _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsLData?.Dispose();
				rsData?.Dispose();
            }
			base.Dispose(disposing);
		}

		public void Init(short pintType)
		{
			// this sub will set the SQL string
			strSQL = BuildSQL(pintType);
			switch (pintType)
			{
				case 1:
					{
						// all accounts
						this.Name = "Tax Club Activity Report - All Accounts";
						lblReportType.Text = "All Accounts";
						break;
					}
				case 2:
					{
						// negative balance
						this.Name = "Tax Club Activity Report - Negative Balance";
						lblReportType.Text = "Negative Balance";
						break;
					}
				case 3:
					{
						// positive balance
						this.Name = "Tax Club Activity Report - Positive Balance";
						lblReportType.Text = "Positive Balance";
						break;
					}
				case 4:
					{
						// zero balance
						this.Name = "Tax Club Activity Report - Zero Balance";
						lblReportType.Text = "Zero Balance";
						break;
					}
			}
			//end switch
			rsData.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
			rsLData.OpenRecordset("SELECT * FROM LienRec");
			lngTotalAccounts = rsData.RecordCount();
			if (lngTotalAccounts == 0)
			{
				// hide the headers if there are no records
				FCMessageBox.Show("There are no accounts eligible.", MsgBoxStyle.Information, "No Accounts");
				Cancel();
			}
			else
			{
				frmReportViewer.InstancePtr.Init(this);
				// Me.Show , MDIParent
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
			//Detail_Format();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			// save this report for future access
			if (this.Document.Pages.Count > 0)
			{
				modGlobalFunctions.IncrementSavedReports("LastCLActivity");
				this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastCLActivity1.RDF"));
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "t");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			lngCount = 0;
			// this is the count of how many accounts were actually processed
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
		}

		private void BindFields()
		{
			// this will fill the information into the fields
			bool boolRetry;
			int lngLRN = 0;
			// Lien Record Number
			TRYNEXTACCOUNT:
			;
			boolRetry = false;
			if (!rsData.EndOfFile())
			{
				lngLRN = FCConvert.ToInt32(rsData.Get_Fields_Int32("LienRecordNumber"));
				// set the lien record if needed
				if (lngLRN == 0)
				{
					// this is a regular bill
				}
				else
				{
					// this is a lien
					rsLData.FindFirstRecord("ID", lngLRN);
				}
				// check to see if this account is elegible to be shown
				switch (intType)
				{
					case 1:
						{
							// All Accounts
							// let all accounts go
							boolRetry = false;
							break;
						}
					case 2:
						{
							// Negative Balances
							// check to see it the principal bill has been overpaid paid
							if (lngLRN == 0)
							{
								// this is a regular bill
								if ((Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue4"))) < Conversion.Val(rsData.Get_Fields_Decimal("PrincipalPaid")))
								{
									// show this account
									boolRetry = false;
								}
								else
								{
									// do not show this account
									boolRetry = true;
								}
							}
							else
							{
								// this is a lien
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								if (Conversion.Val(rsLData.Get_Fields("Principal")) < Conversion.Val(rsLData.Get_Fields_Decimal("PrincipalPaid")))
								{
									// show this account
									boolRetry = false;
								}
								else
								{
									// do not show this account
									boolRetry = true;
								}
							}
							break;
						}
					case 3:
						{
							// Positive Balances
							// check to see it the principal bill has been not been completely paid
							if (lngLRN == 0)
							{
								// this is a regular bill
								if ((Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue4"))) > Conversion.Val(rsData.Get_Fields_Decimal("PrincipalPaid")))
								{
									// show this account
									boolRetry = false;
								}
								else
								{
									// do not show this account
									boolRetry = true;
								}
							}
							else
							{
								// this is a lien
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								if (Conversion.Val(rsLData.Get_Fields("Principal")) > Conversion.Val(rsLData.Get_Fields_Decimal("PrincipalPaid")))
								{
									// show this account
									boolRetry = false;
								}
								else
								{
									// do not show this account
									boolRetry = true;
								}
							}
							break;
						}
					case 4:
						{
							// Zero Balances
							// check to see it the principal bill has been paid off or not
							if (lngLRN == 0)
							{
								// this is a regular bill
								if ((Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue4"))) == Conversion.Val(rsData.Get_Fields_Decimal("PrincipalPaid")))
								{
									// show this account
									boolRetry = false;
								}
								else
								{
									// do not show this account
									boolRetry = true;
								}
							}
							else
							{
								// this is a lien
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								if (Conversion.Val(rsLData.Get_Fields("Principal")) == Conversion.Val(rsLData.Get_Fields_Decimal("PrincipalPaid")))
								{
									// show this account
									boolRetry = false;
								}
								else
								{
									// do not show this account
									boolRetry = true;
								}
							}
							break;
						}
				}
				//end switch
				if (boolRetry)
				{
					rsData.MoveNext();
					goto TRYNEXTACCOUNT;
				}
				else
				{
					lngCount += 1;
				}
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				fldAcct.Text = FCConvert.ToString(rsData.Get_Fields("Account"));
				fldName.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name1")));
				if (lngLRN == 0)
				{
					// pass the billkey for the subreport to use to find all of the payments that match this account and year
					srptTaxClubActivityDetail.Report = new srptActivityDetail();
					srptTaxClubActivityDetail.Report.UserData = rsData.Get_Fields_Int32("BillKey");
				}
				else
				{
					// pass the lienrecordnumber for the subreport to use to find all of the payments that match this account and year
					srptTaxClubActivityDetail.Report = new srptActivityDetailLien();
					srptTaxClubActivityDetail.Report.UserData = rsData.Get_Fields_Int32("LienRecordNumber");
				}
				// move to the next record in the query
				rsData.MoveNext();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void SetupTotals()
		{
			// this sub will fill in the footer line at the bottom of the report
			if (lngCount == 1)
			{
				lblFooter.Text = "There was 1 account processed.";
			}
			else
			{
				lblFooter.Text = "There were " + FCConvert.ToString(lngCount) + " accounts processed.";
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			SetupTotals();
		}
		// VBto upgrade warning: intPassType As object	OnWriteFCConvert.ToInt16(	OnReadFCConvert.ToInt32(
		private string BuildSQL(int intPassType)
		{
			string BuildSQL = "";
			// this function will return the SQL string for the criteria that has been selected
			string strMod = "";
			clsDRWrapper rsOrder = new clsDRWrapper();
			string strOrder = "";
			intType = intPassType;
			if (modStatusPayments.Statics.boolRE)
			{
				// this will select the correct records from the tax table
				strMod = "RE";
			}
			else
			{
				strMod = "PP";
			}
			rsOrder.OpenRecordset("SELECT * FROM Collections", modExtraModules.strCLDatabase);
			switch (rsOrder.Get_Fields_Int32("TCReport"))
			{
				case 0:
					{
						strOrder = " Acct";
						break;
					}
				case 1:
					{
						strOrder = " Name";
						break;
					}
				default:
					{
						strOrder = " Acct";
						break;
					}
			}
			//end switch
			// kgk 2-14-2012 BuildSQL = "SELECT * FROM TaxClubJoin WHERE BillingType = '" & strMod & "' AND Type = '" & strMod & "' ORDER BY " & strOrder
			// TaxClubJoin = "SELECT BillingMaster.BillKey AS BK, BillingMaster.Account AS Acct, * FROM BillingMaster INNER JOIN TaxClub ON BillingMaster.Billkey = TaxClub.BillKey"
			BuildSQL = "SELECT BillingMaster.ID AS BK, BillingMaster.Account AS Acct, * FROM BillingMaster INNER JOIN TaxClub ON BillingMaster.ID = TaxClub.BillKey WHERE BillingType = '" + strMod + "' AND Type = '" + strMod + "' ORDER BY " + strOrder;
			rsOrder.Dispose();
            return BuildSQL;
		}

		
	}
}
