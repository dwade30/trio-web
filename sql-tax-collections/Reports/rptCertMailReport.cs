﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for rptCertMailReport.
	/// </summary>
	public partial class rptCertMailReport : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/05/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               08/25/2004              *
		// This will reflect the changes that were made in the    *
		// Last printing of CMF but it will show the whole list   *
		// ********************************************************
		int lngTotalAccounts;
		clsDRWrapper rsData = new clsDRWrapper();

		public rptCertMailReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Certified Mail Report";
		}

		public static rptCertMailReport InstancePtr
		{
			get
			{
				return (rptCertMailReport)Sys.GetInstance(typeof(rptCertMailReport));
			}
		}

		protected rptCertMailReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}
		// VBto upgrade warning: intPrintOption As short	OnWriteFCConvert.ToInt32(
		public void Init(short intType, short intPrintOption)
		{
			string strSQL;
			string strPrintOption = "";
			// inttype will be 20 for 30 day notice, 21 for transfer tax to lien and 22 for lien maturity
			switch (intPrintOption)
			{
				case 0:
					{
						// print nothing
						strPrintOption = " AND Account = 0";
						break;
					}
				case 1:
					{
						strPrintOption = " AND (NOT NewOwner = -1 AND MortgageHolder = 0)";
						break;
					}
				case 2:
					{
						strPrintOption = " AND MortgageHolder <> 0";
						break;
					}
				case 3:
					{
						strPrintOption = " AND NOT NewOwner = -1";
						break;
					}
				case 4:
					{
						strPrintOption = " AND NewOwner = -1";
						break;
					}
				case 5:
					{
						strPrintOption = " AND MortgageHolder = 0";
						break;
					}
				case 6:
					{
						strPrintOption = " AND (NewOwner = -1 OR MortgageHolder <> 0)";
						break;
					}
				case 7:
					{
						// do nothing...this is all options
						break;
					}
			}

			strSQL = "SELECT * FROM CMFNumbers WHERE Type = " + FCConvert.ToString(intType) + strPrintOption + " AND ISNULL(LabelOnly,0) = 0 ORDER BY BarCode";
			rsData.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
			if (!rsData.EndOfFile())
			{
				SetHeaderString();
				lngTotalAccounts = rsData.RecordCount();
				var viewer = new frmReportViewer();
				viewer.Init(this);
			}
			else
			{
				FCMessageBox.Show("There are no eligible accounts to print.", MsgBoxStyle.Information, "No Eligible Accounts");
				Cancel();
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			if (this.Document.Pages.Count > 0)
			{
				modGlobalFunctions.IncrementSavedReports("CLCMFList");
				this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "CLCMFList1.RDF"));
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
			try
			{
				if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
				{
					fldAccount.Text = FCConvert.ToString(rsData.Get_Fields("Account"));
					if (((rsData.Get_Fields_Int32("MortgageHolder"))) != 0)
					{
						fldType.Text = "M";
					}
					else if (rsData.Get_Fields_Boolean("NewOwner"))
					{
						fldType.Text = "N";
					}
					else if (FCConvert.ToInt32(Conversion.Val(rsData.Get_Fields_Int32("InterestedPartyID"))) != 0)
					{
						fldType.Text = "I";
					}
					else
					{
						fldType.Text = "A";
					}
					fldCMFNumber.Text = Strings.Mid(FCConvert.ToString(rsData.Get_Fields_String("BarCode")), 13, 4) + " " + Strings.Right(FCConvert.ToString(rsData.Get_Fields_String("BarCode")), 4);
					if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("IMPBTrackingNumber"))) != "")
					{
						fldIMPBnum.Text = Strings.Mid(FCConvert.ToString(rsData.Get_Fields_String("IMPBTrackingNumber")), 15, 2) + " " + Strings.Mid(FCConvert.ToString(rsData.Get_Fields_String("IMPBTrackingNumber")), 17, 4) + " " + Strings.Right(FCConvert.ToString(rsData.Get_Fields_String("IMPBTrackingNumber")), 2);
					}
					else
					{
						fldIMPBnum.Text = "";
					}
					fldName.Text = rsData.Get_Fields_String("Name");
					rsData.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Bind Fields");
			}
		}

		private void SetHeaderString()
		{
			// this will set the correct header for this account
			string strTemp = "";
			strTemp += "Certified Mail Report";
			lblHeader.Text = strTemp;
			if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
			{
				strTemp = modGlobalConstants.Statics.gstrCityTown + " of " + modGlobalConstants.Statics.MuniName;
			}
			else
			{
				strTemp = modGlobalConstants.Statics.MuniName;
			}
			strTemp += "\r\n" + Strings.Left(FCConvert.ToString(rsData.Get_Fields_String("BarCode")), 4) + " " + Strings.Mid(FCConvert.ToString(rsData.Get_Fields_String("BarCode")), 5, 4) + " " + Strings.Mid(FCConvert.ToString(rsData.Get_Fields_String("BarCode")), 9, 4);
			// kk09232015 trols-63  Add IMPB Tracking info
			if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("IMPBTrackingNumber"))) != "")
			{
				strTemp += "\r\n" + "IMPB " + Strings.Left(FCConvert.ToString(rsData.Get_Fields_String("IMPBTrackingNumber")), 4) + " " + Strings.Mid(FCConvert.ToString(rsData.Get_Fields_String("IMPBTrackingNumber")), 5, 4) + " " + Strings.Mid(FCConvert.ToString(rsData.Get_Fields_String("IMPBTrackingNumber")), 9, 4) + " " + Strings.Mid(FCConvert.ToString(rsData.Get_Fields_String("IMPBTrackingNumber")), 13, 2);
			}
			lblHeader2.Text = strTemp;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			if (lngTotalAccounts == 1)
			{
				fldTotalProcessed.Text = "Processed " + FCConvert.ToString(lngTotalAccounts) + " forms.";
			}
			else
			{
				fldTotalProcessed.Text = "Processed " + FCConvert.ToString(lngTotalAccounts) + " forms.";
			}
		}
	}
}
