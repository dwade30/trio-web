﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class sarLienEditMortHolders : FCSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// Date           :               09/26/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// Last Updated   :               07/14/2004              *
		// ********************************************************
		clsDRWrapper rsMort = new clsDRWrapper();
		int lngAcct;
		bool boolShowAll;
		int lngExtraLines;

		public sarLienEditMortHolders()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static sarLienEditMortHolders InstancePtr
		{
			get
			{
				return (sarLienEditMortHolders)Sys.GetInstance(typeof(sarLienEditMortHolders));
			}
		}

		protected sarLienEditMortHolders _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsMort?.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsMort.EndOfFile();
			//Detail_Format();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lngAcct = FCConvert.ToInt32(this.UserData);
			//FC:FINAL:BCU:#i200 - check index
			if (frmRateRecChoice.InstancePtr.cmbExtraLines.SelectedIndex != -1)
			{
				lngExtraLines = FCConvert.ToInt32(Math.Round(Conversion.Val(frmRateRecChoice.InstancePtr.cmbExtraLines.Items[frmRateRecChoice.InstancePtr.cmbExtraLines.SelectedIndex].ToString())));
			}
			if (lngAcct < 0)
			{
				boolShowAll = true;
				lngAcct *= -1;
				rsMort.OpenRecordset("SELECT * FROM MortgageAssociation INNER JOIN MortgageHolders ON MortgageAssociation.MortgageHolderID = MortgageHolders.ID WHERE Account = " + FCConvert.ToString(lngAcct) + " AND Module = 'RE'", "CentralData");
				fldMHAddr1.Visible = true;
				fldMHAddr2.Visible = true;
				fldMHAddr3.Visible = true;
				fldMHAddr4.Visible = true;
				fldBookPage.Visible = true;
				fldMHAddr1.Top = 180 / 1440F;
				fldMHAddr2.Top = 360 / 1440F;
				fldMHAddr3.Top = 540 / 1440F;
				fldMHAddr4.Top = 720 / 1440F;
				fldBookPage.Top = 900 / 1440F;
				this.Detail.Height = 540 / 1440F;
			}
			else
			{
				if (lngAcct > 0)
				{
					rsMort.OpenRecordset("SELECT * FROM MortgageAssociation INNER JOIN MortgageHolders ON MortgageAssociation.MortgageHolderID = MortgageHolders.ID WHERE Account = " + FCConvert.ToString(lngAcct) + " AND Module = 'RE'", "CentralData");
					switch (arLienProcessEditReport.InstancePtr.intReportDetail)
					{
						case 1:
							{
								fldMHAddr1.Visible = false;
								fldMHAddr2.Visible = false;
								fldMHAddr3.Visible = false;
								fldMHAddr4.Visible = false;
								fldBookPage.Visible = false;
								fldMHAddr1.Top = 0;
								fldMHAddr2.Top = 0;
								fldMHAddr3.Top = 0;
								fldMHAddr4.Top = 0;
								fldBookPage.Top = 0;
								this.Detail.Height = 180 / 1440F;
								break;
							}
						case 2:
							{
								fldMHAddr1.Visible = true;
								fldMHAddr2.Visible = true;
								fldMHAddr3.Visible = true;
								fldMHAddr4.Visible = true;
								fldBookPage.Visible = true;
								fldMHAddr1.Top = 180 / 1440F;
								fldMHAddr2.Top = 360 / 1440F;
								fldMHAddr3.Top = 540 / 1440F;
								fldMHAddr4.Top = 720 / 1440F;
								fldBookPage.Top = 900 / 1440F;
								this.Detail.Height = 540 / 1440F;
								break;
							}
					}
					//end switch
				}
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			float lngNextLine = 0;
			if (!rsMort.EndOfFile())
			{
				fldMHName.Text = rsMort.Get_Fields_String("Name");
				lngNextLine = 180 / 1440F;
				if (arLienProcessEditReport.InstancePtr.intReportDetail == 2 || boolShowAll)
				{
					if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address1"))) != "")
					{
						fldMHAddr1.Text = Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address1")));
						fldMHAddr1.Top = lngNextLine;
						lngNextLine += 180 / 1440F;
					}
					else
					{
						fldMHAddr1.Text = "";
						fldMHAddr1.Top = 0;
					}
					if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address2"))) != "")
					{
						fldMHAddr2.Text = Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address2")));
						fldMHAddr2.Top = lngNextLine;
						lngNextLine += 180 / 1440F;
					}
					else
					{
						fldMHAddr2.Text = "";
						fldMHAddr2.Top = 0;
					}
					if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address3"))) != "")
					{
						fldMHAddr3.Text = Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address3")));
						fldMHAddr3.Top = lngNextLine;
						lngNextLine += 180 / 1440F;
					}
					else
					{
						fldMHAddr3.Text = "";
						fldMHAddr3.Top = 0;
					}
					if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("City"))) != "")
					{
						fldMHAddr4.Text = Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("City"))) + " " + Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("State"))) + ", " + Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip")));
						if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
						{
							fldMHAddr4.Text = fldMHAddr4.Text + Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4")));
						}
						fldMHAddr4.Top = lngNextLine;
						lngNextLine += 180 / 1440F;
					}
					else
					{
						fldMHAddr4.Text = "";
						fldMHAddr4.Top = 0;
					}
					if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("BookPage"))) != "")
					{
						fldBookPage.Text = Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("BookPage")));
						fldBookPage.Top = lngNextLine;
						lngNextLine += 180 / 1440F;
					}
					else
					{
						fldBookPage.Text = "";
						fldBookPage.Top = 0;
					}
				}
				lblExtraLines.Top = lngNextLine + FCConvert.ToSingle((lngExtraLines / 2.0) - 1 / 1440F) * (180 / 1440F);
				Detail.Height = lngNextLine + FCConvert.ToSingle(lngExtraLines / 2.0) * (180 / 1440F);
				rsMort.MoveNext();
			}
			else
			{
				fldMHName.Text = "";
				fldMHAddr1.Text = "";
				fldMHAddr2.Text = "";
				fldMHAddr3.Text = "";
				fldMHAddr4.Text = "";
				fldBookPage.Text = "";
				lblExtraLines.Top = 0;
			}
		}

		
	}
}
