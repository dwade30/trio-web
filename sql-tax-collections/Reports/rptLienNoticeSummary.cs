﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using SharedApplication.Extensions;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptLienNoticeSummary : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               01/31/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               06/06/2005              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		int intYear;
		clsDRWrapper rsRE = new clsDRWrapper();
		clsDRWrapper rsLien = new clsDRWrapper();
		double dblTotalPrin;
		double dblTotalPLI;
		double dblTotalCost;
		double dblTotalCurrentInt;
		double dblTotalTotal;
		DateTime dtMailDate;
		double dblDemand;
		int intType;
		bool boolCO;
        short intBilledOwnerAddress; // trocl-1335
        private cAddressControllerCL tAddrCtrlr = new cAddressControllerCL();

		public rptLienNoticeSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Account Detail";
		}

		public static rptLienNoticeSummary InstancePtr
		{
			get
			{
				return (rptLienNoticeSummary)Sys.GetInstance(typeof(rptLienNoticeSummary));
			}
		}

		protected rptLienNoticeSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
				rsRE.Dispose();
				rsLien.Dispose();
            }
			base.Dispose(disposing);
		}
		// VBto upgrade warning: intPassYear As short	OnWriteFCConvert.ToInt32(
		public void Init(string strAccountList, string strPassHeaderTitle, string strPassReportType, int intPassYear, DateTime dtPassDate, short intPassType, string strRateKeys, bool blnAcctSort, short intBilledOwnerAddr)
		{
			try
			{
				if (strAccountList.Trim() == "")
				{
					Cancel();
					return;
				}

                if (strAccountList.Right(1) == ",")
                {
                    strAccountList = strAccountList.Substring(0, strAccountList.Length - 1);
                }
				string strOrder = "";
                intBilledOwnerAddress = intBilledOwnerAddr;
				dtMailDate = dtPassDate;
				intType = intPassType;
				// this will set the title of the report
				if (Strings.Trim(strPassHeaderTitle) != "")
				{
					lblHeader.Text = strPassHeaderTitle;
					this.Name = strPassHeaderTitle;
				}
				else
				{
					lblHeader.Text = "Summary Report";
				}
				// this will set the description of the report
				if (Strings.Trim(strPassReportType) != "")
				{
					lblReportType.Text = strPassReportType;
				}
				else
				{
					lblReportType.Text = strPassReportType;
				}
				intYear = intPassYear;
				if (blnAcctSort)
				{
					strOrder = "ORDER BY Account, Name1";
				}
				else
				{
					strOrder = "ORDER BY Name1, Account";
				}

				if (modStatusPayments.Statics.boolRE)
				{
					// kgk trocl-646 04-20-2011  Need lien rate keys for maturity notice
					if (intType != 2)
					{
						rsData.OpenRecordset("SELECT * FROM BillingMaster WHERE Account IN (" + strAccountList + ") AND BillingYear/10 = " + FCConvert.ToString(intYear) + strRateKeys + " AND BillingType = 'RE'  " + strOrder);
					}
					else
					{
						rsData.OpenRecordset("SELECT BillingMaster.*, LienRec.RateKey FROM (BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.ID) WHERE Account IN (" + strAccountList + ") AND BillingYear/10 = " + FCConvert.ToString(intYear) + strRateKeys + " AND BillingType = 'RE'  " + strOrder);
					}
				}
				else
				{
					rsData.OpenRecordset("SELECT * FROM BillingMaster WHERE Account IN (" + strAccountList + ") AND BillingYear/10 = " + FCConvert.ToString(intYear) + strRateKeys + " AND BillingType = 'PP' " + strOrder);
				}
				if (rsData.EndOfFile())
				{
					Cancel();
				}
				else
				{
					rsRE.OpenRecordset("SELECT RSAccount,DeedName1,DeedName2, ISNULL(OwnerPartyID, 0) AS OwnerPartyID, ISNULL(SecOwnerPartyID, 0) AS SecOwnerPartyID,DeedName1 as Own1, DeedName2 as Own2, InBankruptcy " + "FROM MASTER WHERE RSCard = 1 AND (RSDeleted = 0 OR RSDeleted IS NULL) ORDER BY RSAccount", modExtraModules.strREDatabase);
					rsRE.InsertName("OwnerPartyID", "Own1", false, true, true, "", false, "", true, "");
					rsLien.OpenRecordset("SELECT * FROM LienRec", modExtraModules.strCLDatabase);
					//frmRPTViewer.InstancePtr.Init("", this.Name, 100, this);
                    frmReportViewer.InstancePtr.Init(this );
				}
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Initializing Summary");
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
			//Detail_Format();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
		}

        private void ActiveReport_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
            rsLien.DisposeOf();
            rsRE.DisposeOf();
        }

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

        private void BindFields()
        {
            try
            {   // On Error GoTo ERROR_HANDLER
               // fecherFoundation.Information.Err(ex).Clear();
                double dblPrin = 0;
                double dblPLI = 0;
                double dblCosts = 0;
                double dblCurrInt = 0;
                double dblTotal = 0;
                double dblAddedCosts = 0;
                int lngIndex = 0;
                bool boolSameName = false;
                double dblMinimum = 0;
                bool blnContinue = false;
                double dblAbate1 = 0;
                double dblAbate2 = 0;
                double dblAbate3 = 0;
                double dblAbate4 = 0;

                // trocl-1335 09.11.17 kjr
                DateTime dtBillDate;
               // clsDRWrapper rsRK = new clsDRWrapper();
                string strRateKey = "";
                string strPrevSecOwnerName = "";
                string strPrevAddress1 = "";
                string strPrevAddress2 = "";
                string strPrevAddress3 = "";
                bool boolNewOwner;
                bool boolNewAddress = false;

                TryAgain:;

                if (!rsData.EndOfFile())
                {
					// this is the extra amount due from costs for this account
					int tmp = rsData.Get_Fields_Int32("Account");
                    lngIndex = ReturnAccountIndex(ref tmp);
                    if (lngIndex > -1)
                    {
                        dblAddedCosts = modGlobal.Statics.arrDemand[lngIndex].CertifiedMailFee + modGlobal.Statics.arrDemand[lngIndex].Fee;
                    }
                    else
                    {
                        dblAddedCosts = 0;
                    }
                    dblAbate1 = 0;
                    dblAbate2 = 0;
                    dblAbate3 = 0;
                    dblAbate4 = 0;
                    if (FCConvert.ToInt32(rsData.Get_Fields("LienRecordNumber")) == 0)
                    {
						// regular billing
						dblPrin = rsData.Get_Fields_Double ("TaxDue1") + rsData.Get_Fields_Double("TaxDue2") + rsData.Get_Fields_Double("TaxDue3") + rsData.Get_Fields_Double("TaxDue4") - rsData.Get_Fields_Double("PrincipalPaid");
                        dblPLI = (rsData.Get_Fields_Double("InterestCharged") * -1) - rsData.Get_Fields_Double("InterestPaid");
                        // MAL@20081022: Corrected to remove amount paid from the Costs total
                        // Tracker Reference: 15773
                        dblCosts = (rsData.Get_Fields_Double("DemandFees") + dblAddedCosts) - rsData.Get_Fields_Double("DemandFeesPaid");
                        dblCurrInt = 0; // this will get filled from the calculate account function call
                        modCLCalculations.CheckForAbatementPayments(rsData.Get_Fields_Int32("ID"), rsData.Get_Fields_Int32("RateKey"), rsData.Get_Fields_String("BillingType") == "RE", ref dblAbate1, ref dblAbate2, ref dblAbate3, ref dblAbate4, rsData.Get_Fields_Double("TaxDue1"), rsData.Get_Fields_Double("TaxDue2"), rsData.Get_Fields_Double("TaxDue3"), rsData.Get_Fields_Double("TaxDue4"));
                        dblTotal = modCLCalculations.CalculateAccountCL(ref rsData, rsData.Get_Fields("Account"),  dtMailDate, ref dblCurrInt,  ref dblAbate1, ref dblAbate2, ref dblAbate3, ref dblAbate4) + dblAddedCosts;
                    }
                    else
                    {
                        // lien record
                        rsLien.FindFirstRecord("ID", rsData.Get_Fields("LienRecordNumber"));
                        if (!rsLien.NoMatch)
                        {
                            if (rsLien.Get_Fields("Principal") - rsLien.Get_Fields("PrincipalPaid") <= 0)
                            {
                                rsData.MoveNext();
                                if (rsData.EndOfFile()) return;
                                goto TryAgain;
                            }
                            dblPrin = rsLien.Get_Fields_Double("Principal") - rsLien.Get_Fields_Double("PrincipalPaid");
                            dblPLI = rsLien.Get_Fields_Double("Interest") + (rsLien.Get_Fields_Double("InterestCharged") * -1) - rsLien.Get_Fields_Double("InterestPaid") - rsLien.Get_Fields_Double("PLIPaid");
                            if (intType == 1)
                            {
                                // Lien Transfer
                                dblCosts = (rsLien.Get_Fields_Double("Costs") + dblAddedCosts) - rsLien.Get_Fields_Double("CostsPaid");
                            }
                            else if (intType == 2)
                            {
                                // Lien Maturity
                                dblCosts = dblAddedCosts;
                            }
                            dblCurrInt = 0;
                            dblTotal = modCLCalculations.CalculateAccountCLLien( rsLien,  dtMailDate, ref dblCurrInt);
                            dblTotal = dblPrin + dblPLI + dblCurrInt + dblAddedCosts;
                        }
                        else
                        {
                            dblPrin = 0;
                            dblPLI = 0;
                            dblCosts = 0;
                            dblCurrInt = 0;
                        }
                    }

                    // MAL@20071001: Add check for minimum amount
                    if (frmFreeReport.InstancePtr.dblMinimumAmount > 0)
                    {
                        dblMinimum = frmFreeReport.InstancePtr.dblMinimumAmount;
                    }
                    else
                    {
                        dblMinimum = 0;
                    }

                    if (dblMinimum > 0)
                    {
                        blnContinue = ((dblTotal - dblAddedCosts) >= dblMinimum);
                    }
                    else
                    {
                        blnContinue = ((dblTotal - dblAddedCosts) > dblMinimum);
                    }

                    if (blnContinue)
                    {
                        // dblTotal = dblPrin + dblPLI + dblCosts + dblCurrInt

                        fldPrincipal.Text = Strings.Format(dblPrin, "$#,##0.00");
                        fldPLInt.Text = Strings.Format(dblPLI, "$#,##0.00");
                        fldCosts.Text = Strings.Format(dblCosts, "$#,##0.00");
                        fldCurrentInt.Text = Strings.Format(dblCurrInt, "$#,##0.00");
                        fldTotal.Text = Strings.Format(dblTotal, "$#,##0.00");

                        dblTotalPrin += dblPrin;
                        dblTotalPLI += dblPLI;
                        dblTotalCost += dblCosts;
                        dblTotalCurrentInt += dblCurrInt;
                        dblTotalTotal += dblTotal;

                        fldAccount.Text = rsData.Get_Fields_String("Account");
                        fldName.Text = rsData.Get_Fields_String("Name1"); // trocls-119 4.23.18 kjr CODE FREEZE
                        if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Name2"))) != "")
                        {
                            fldName.Text = fldName.Text + "\r\n" + rsData.Get_Fields("Name2");
                        }

                        clsAddress tAddr;

                        switch (intBilledOwnerAddress)
                        {
	                        case (int)ReminderNoticeOptions.AddressEnum.AddressAtBilling:
		                        tAddr = tAddrCtrlr.GetOwnerAndAddressAtBilling(rsData);
		                        break;
	                        case (int)ReminderNoticeOptions.AddressEnum.CareOfCurrentOwner:
		                        tAddr = tAddrCtrlr.GetOwnerAtBillingCareOfCurrentOwner(rsData);
		                        break;
	                        case (int)ReminderNoticeOptions.AddressEnum.LastKnownAddress:
		                        tAddr = tAddrCtrlr.GetBilledOwnerAtLastAddress(rsData);
		                        break;
	                        default:
		                        tAddr = new clsAddress();
		                        break;
                        }

                        fldAddress.Text = "";
						if (tAddr.Get_Address(1).Trim() != "")
						{
							fldAddress.Text = tAddr.Get_Address(1).Trim() + "\r\n";
						}
						if (tAddr.Get_Address(2).Trim() != "")
						{
							fldAddress.Text = fldAddress.Text + tAddr.Get_Address(2).Trim() + "\r\n";
						}
						if (tAddr.Get_Address(3).Trim() != "")
						{
							fldAddress.Text = fldAddress.Text + tAddr.Get_Address(3).Trim() + "\r\n";
						}
						if (tAddr.Get_Address(4).Trim() != "")
						{
							fldAddress.Text = fldAddress.Text + tAddr.Get_Address(4).Trim() + "\r\n";
						}


						

                        rsData.MoveNext();
                    }
                    else
                    { // MAL@20071001
                        rsData.MoveNext();
                        if (rsData.EndOfFile()) return;
                        goto TryAgain;
                    }
                }
                else
                {
                    fldAccount.Text = "";
                    fldAddress.Text = "";
                    fldName.Text = "";
                }
            }
            catch (Exception ex)
            {   // ERROR_HANDLER:
                MessageBox.Show($"Error - {ex.GetBaseException().Message}" , "Error Binding Fields", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        
        private int ReturnAccountIndex_2(int lngAcct)
		{
			return ReturnAccountIndex(ref lngAcct);
		}

		private int ReturnAccountIndex(ref int lngAcct)
		{
			int ReturnAccountIndex = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will return the array index for the account passed in...-1 if not found
				int lngCT;
				bool boolFound = false;
				for (lngCT = 0; lngCT <= Information.UBound(modGlobal.Statics.arrDemand, 1); lngCT++)
				{
					if (modGlobal.Statics.arrDemand[lngCT].Account == lngAcct)
					{
						boolFound = true;
						break;
					}
				}
				if (boolFound)
				{
					ReturnAccountIndex = lngCT;
				}
				else
				{
					ReturnAccountIndex = -1;
				}
				return ReturnAccountIndex;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				if (Information.Err(ex).Number != 9)
				{
					// this is when the array is empty
					FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Finding Array Index");
				}
				else
				{
					ReturnAccountIndex = -1;
				}
			}
			return ReturnAccountIndex;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			// fill in the totals
			fldTotalPrincipal.Text = Strings.Format(dblTotalPrin, "$#,##0.00");
			fldTotalPLInt.Text = Strings.Format(dblTotalPLI, "$#,##0.00");
			fldTotalCosts.Text = Strings.Format(dblTotalCost, "$#,##0.00");
			fldTotalCurrentInt.Text = Strings.Format(dblTotalCurrentInt, "$#,##0.00");
			fldTotalTotal.Text = Strings.Format(dblTotalTotal, "$#,##0.00");
		}

		
	}
}
