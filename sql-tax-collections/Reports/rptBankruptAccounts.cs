using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.Enums;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptBankruptAccounts : BaseSectionReport
	{
        private TaxCollectionStatusReportConfiguration reportConfiguration;
        string strSQL;
		clsDRWrapper rsData = new clsDRWrapper();
		double[] dblTotals = new double[6 + 1];
		bool boolRTError;
		int lngCount;
		bool boolStarted;
		double dblPDTotal;
		bool boolAdjustedSummary;
		bool boolSummaryOnly;
		bool boolDeletedAccount;
		int lngLastAccount;
		int lngTAType;
		// 0 = all, 1 = TA, 2 = non TA
		clsDRWrapper rsMaster = new clsDRWrapper();
		// this is for the summaries at the bottom
		double[] dblYearTotals = new double[2000 + 1];
		// billingyear - 19800
		double[,] dblPayments = new double[12 + 1, 5 + 1];
		// 0 - 3, 1 - A, 2 - C, 3 - D, 4 - I, 5 - L, 6 - P, 7 - U, 8 - X, 9 - Y, 10 - Total, 11 - W, 12 - F
		// 0 - Principal, 1 - PreLienInterest, 2 - CurrentInterest, 3 - Costs
		public rptBankruptAccounts()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        public void SetReportOption(ref TaxCollectionStatusReportConfiguration reportConfig)
        {
            reportConfiguration = reportConfig;
        }

        private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Bankrupt Accounts";
		}

		public static rptBankruptAccounts InstancePtr
		{
			get
			{
				return (rptBankruptAccounts)Sys.GetInstance(typeof(rptBankruptAccounts));
			}
		}

		protected rptBankruptAccounts _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsMaster.Dispose();
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// Write #1, Format(Time, "HH:mm:ss") & " - Start Fetch Data"
			if (rsData.EndOfFile())
			{
				if (boolStarted)
				{
					eArgs.EOF = true;
				}
				else
				{
				}
			}
			else
			{
				eArgs.EOF = false;
			}
			// Write #1, Format(Time, "HH:mm:ss") & " - End Fetch Data"
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_PageEnd(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "t");
				lblMuniName.Text = modGlobalConstants.Statics.MuniName;
				lngCount = 0;
				// MAL@20080813 ; Tracker Reference: 11805
                if (reportConfiguration.Filter.IsTaxAcquired.HasValue)
                {
                    if (reportConfiguration.Filter.IsTaxAcquired.Value)
                    {
                        lngTAType = 1;
                    }
                    else
                    {
                        lngTAType = 2;
                    }
                }
                else
                {
                    lngTAType = 0;
                }


				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading");
				//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
				boolRTError = false;
				SetupFields();
				// Moves/shows the correct fields into the right places
				SetReportHeader();
				// Sets the titles and moves labels in the header
				boolStarted = false;
				strSQL = BuildSQL();
                // Generates the SQL String
                if (lngTAType > 0)
                {
                    rsMaster.OpenRecordset("SELECT RSAccount, TaxAcquired FROM Master WHERE TaxAcquired = 1", modExtraModules.strREDatabase);
                }
                rsData.OpenRecordset(strSQL, modGlobal.DEFAULTDATABASE);
                return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Starting Report");
			}
		}

		private string BuildSQL()
		{
			string BuildSQL = "";
			string strTemp = "";
			string strWhereClause;
			string strREPPBill = "";
			string strREPPPayment = "";
			int intCT;
			string strSupp = "";
			strWhereClause = "WHERE InBankruptcy = 1 AND RSDeleted = 0 AND BillingType = 'RE'";
            var strAnd = "";
            var filter = reportConfiguration.Filter;
            if (filter.AccountRangeUsed())
            {
                if (filter.AccountMax.HasValue)
                {
                    if (filter.AccountMin.HasValue)
                    {
                        strTemp += "Account between " + filter.AccountMin.Value.ToString() + " and " +
                                   filter.AccountMax.ToString();
                    }
                    else
                    {
                        strTemp += "Account <= " + filter.AccountMax.Value.ToString();
                    }
                }
                else
                {
                    strTemp += "Account >= " + filter.AccountMin.Value.ToString();
                }
                strAnd = " and ";
            }

            if (filter.NameRangeUsed())
            {
                strTemp += strAnd;
                if (!String.IsNullOrWhiteSpace(filter.NameMin))
                {
                    if (!String.IsNullOrWhiteSpace(filter.NameMax))
                    {
                        strTemp += " Name1 between '" + filter.NameMin + "' and '" + filter.NameMax + "zzzz'";
                    }
                    else
                    {
                        strTemp += " Name1 >= '" + filter.NameMin + "'";
                    }
                }
                else
                {
                    strTemp += " Name1 <= '" + filter.NameMax + "zzzz'";
                }

                strAnd = " and ";
            }

            if (filter.TaxYearRangeUsed())
            {
                strTemp += strAnd;

                if (filter.TaxYearMin.HasValue)
                {
                    if (filter.TaxYearMax.HasValue)
                    {
                        strTemp += " BillingYear between " + filter.TaxYearMin.Value.ToString() + " and " +
                                   filter.TaxYearMax.Value.ToString();
                    }
                    else
                    {
                        strTemp += " BillingYear = " + filter.TaxYearMin.Value.ToString();
                    }
                }
                else
                {
                    strTemp += " BillingYear = " + filter.TaxYearMax.Value.ToString();
                }

                strAnd = " and ";
            }

            if (filter.RateRecordRangeUsed())
            {
                strTemp += strAnd;

                if (filter.RateRecordMin.HasValue)
                {
                    if (filter.RateRecordMax.HasValue)
                    {
                        strTemp += " RateKey between " + filter.RateRecordMin.Value.ToString() + " and " +
                                   filter.RateRecordMax.Value.ToString();
                    }
                    else
                    {
                        strTemp += " RateKey >= " + filter.RateRecordMin.Value.ToString();
                    }
                }
                else
                {
                    strTemp += " RateKey <= " + filter.RateRecordMax.Value.ToString();
                }

                strAnd = " and ";
            }

			if (Strings.Trim(strTemp) != "")
			{
				strWhereClause += " AND " + strTemp;
			}
			strTemp = "SELECT * FROM BillingMaster INNER JOIN " +  modGlobal.Statics.strDbRE + "Master ON BillingMaster.Account = Master.RSAccount " + strWhereClause + " ORDER BY Name1, Account, BillingYear";
			BuildSQL = strTemp;
			return BuildSQL;
		}

		private void SetupFields()
		{
			// Set each field and label's visible property to True/False depending on which fields the user
			// has selected from the Custom Report screen then move them accordingly
			int intRow;
			int intRow2/*unused?*/;
			float lngHt;
			intRow = 2;
			lngHt = 270 / 1440F;
			lblYear.Visible = true;
			fldYear.Visible = true;
			// if the year is not shown, then make the name field smaller
			fldName.Width = fldYear.Left - (fldType.Left + fldType.Width);
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the information into the fields
				clsDRWrapper rsPayment = new clsDRWrapper();
				//clsDRWrapper rsRE = new clsDRWrapper();
				clsDRWrapper rsCalLien = new clsDRWrapper();
				//clsDRWrapper rsRate = new clsDRWrapper();
				string strTemp = "";
				double dblTotalPayment/*unused?*/;
				double dblTotalAbate/*unused?*/;
				double dblTotalRefundAbate/*unused?*/;
				double dblCurInt/*unused?*/;
				double dblLienCurInt/*unused?*/;
				DateTime dtPaymentDate;
				bool boolREInfo/*unused?*/;
				double dblPaymentRecieved;
				double dblXtraInt = 0;
				double dblTotalDue;
				double dblCosts;
				double dblInterest;
				TRYAGAIN:
				;
				//Application.DoEvents();
				dblPaymentRecieved = 0;
				fldDue.Text = "";
				fldTaxDue.Text = "";
				fldPaymentReceived.Text = "";
				fldAccount.Text = "";
				fldYear.Text = "";
				fldName.Text = "";
				fldType.Text = "";
				dblCosts = 0;
				dblInterest = 0;

				if (rsData.EndOfFile())
				{
					return;
				}

                if (reportConfiguration.Filter.TranCodeMin.HasValue)
                {
                    if (!modCLCalculations.CheckTranCode_78(reportConfiguration.BillingType == PropertyTaxBillType.Real, rsData.Get_Fields_Int32("Account"), reportConfiguration.Filter.TranCodeMin.Value, reportConfiguration.Filter.TranCodeMax ?? 0));
                    {
                        rsData.MoveNext();
                        goto TRYAGAIN;
                    }
                }
				
                // MAL@20080813: Add check for Tax Acquired status
                // Tracker Reference: 11805
                if (lngTAType > 0)
                {
                    // rsMaster.FindFirstRecord "RSAccount", rsData.Fields("Account")
                    // If Not rsMaster.NoMatch Then
                    switch (lngTAType)
                    {

                        case 1:
                            {
                                // TA
                                // if the account is in the recordset then it is TA
                                rsMaster.FindFirstRecord("RSAccount", rsData.Get_Fields("Account"));
                                if (rsMaster.NoMatch)
                                {
                                    // If Not rsMaster.Fields("TaxAcquired") Then
                                    rsData.MoveNext();
                                    goto TRYAGAIN;
                                }
                                break;
                            }
                        case 2:
                            {
                                // Non TA
                                // if the account is NOT in the recordset then it is TA
                                rsMaster.FindFirstRecord("RSAccount", rsData.Get_Fields("Account"));
                                if (!rsMaster.NoMatch)
                                {
                                    // If rsMaster.Fields("TaxAcquired") Then
                                    rsData.MoveNext();
                                    goto TRYAGAIN;
                                }
                                break;
                            }
                    } //end switch
                      // End If
                }
                lngCount += 1;
				fldYear.Text = FCConvert.ToString(FCUtils.iDiv(rsData.Get_Fields_Int32("BillingYear"), 10));
				fldTaxDue.Text = Strings.Format(Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue4")), "#,##0.00");
				if (((rsData.Get_Fields_Int32("LienRecordNumber"))) == 0)
				{
					if (reportConfiguration.Options.UseAsOfDate())
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE Account = " + FCConvert.ToString(rsData.Get_Fields("Account")) + " AND [Year] = " + FCConvert.ToString(rsData.Get_Fields_Int32("BillingYear")) + " AND BillCode <> 'L' AND BillKey = " + FCConvert.ToString(rsData.Get_Fields_Int32("ID")) + " AND ISNULL(RecordedTransactionDate, '12/30/1899') <= '" + FCConvert.ToString(reportConfiguration.Options.AsOfDate) + "'");
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE Account = " + FCConvert.ToString(rsData.Get_Fields("Account")) + " AND [Year] = " + FCConvert.ToString(rsData.Get_Fields_Int32("BillingYear")) + " AND BillCode <> 'L' AND BillKey = " + FCConvert.ToString(rsData.Get_Fields_Int32("ID")));
					}
					while (!rsPayment.EndOfFile())
					{
						// 0 - 3, 1 - A, 2 - C, 3 - D, 4 - I, 5 - L, 6 - P, 7 - U, 8 - X, 9 - Y, 10 - Total
						// 0 - Principal, 1 - PreLienInterest, 2 - CurrentInterest, 3 - Costs
						// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
						dblPaymentRecieved += Conversion.Val(rsPayment.Get_Fields("Principal")) + Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")) + Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost"));
						if (rsPayment.Get_Fields_Decimal("CurrentInterest") < 0)
						{
							dblPaymentRecieved = dblPaymentRecieved;
						}
						else
						{
							dblPaymentRecieved += Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest"));
						}
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "P")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(6, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "U")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(8, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "X")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(9, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
								// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
								else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "Y")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(10, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
									// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
									else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "C")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(2, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
										// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
										else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "A")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(1, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
											// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
											// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
											else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "S")
						{
							// put these figures in the X category because there are going away
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(9, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
												// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
												// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
												else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "D")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(3, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
													// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
													// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
													else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "I")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(4, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
							dblInterest += Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest"));
						}
														// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
														// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
														else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "L")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(5, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
															// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
															// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
															else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "3")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(0, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
							dblCosts += Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost"));
						}
																// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
																// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
																else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "R")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(7, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
																	// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
																	// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
																	else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "W")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(11, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
																		// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
																		// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
																		else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "F")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(12, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
						rsPayment.MoveNext();
					}
				}
				else
				{
					if (reportConfiguration.Options.UseAsOfDate())
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE Account = " + FCConvert.ToString(rsData.Get_Fields("Account")) + " AND [Year] = " + FCConvert.ToString(rsData.Get_Fields_Int32("BillingYear")) + " AND (BillKey = " + FCConvert.ToString(rsData.Get_Fields_Int32("ID")) + " OR BillKey = " + FCConvert.ToString(rsData.Get_Fields_Int32("LienRecordNumber")) + ") AND ISNULL(RecordedTransactionDate, '12/30/1899') <= '" + FCConvert.ToString(reportConfiguration.Options.AsOfDate) + "'");
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE Account = " + FCConvert.ToString(rsData.Get_Fields("Account")) + " AND [Year] = " + FCConvert.ToString(rsData.Get_Fields_Int32("BillingYear")) + " AND (BillKey = " + FCConvert.ToString(rsData.Get_Fields_Int32("ID")) + " OR BillKey = " + FCConvert.ToString(rsData.Get_Fields_Int32("LienRecordNumber")) + ")");
					}
					while (!rsPayment.EndOfFile())
					{
						// 0 - 3, 1 - A, 2 - C, 3 - D, 4 - I, 5 - L, 6 - P, 7 - U, 8 - X, 9 - Y, 10 - Total
						// 0 - Principal, 1 - PreLienInterest, 2 - CurrentInterest, 3 - Costs
						if (rsPayment.Get_Fields_Decimal("LienCost") < 0)
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							dblPaymentRecieved += Conversion.Val(rsPayment.Get_Fields("Principal")) + Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")) + Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest"));
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							dblPaymentRecieved += Conversion.Val(rsPayment.Get_Fields("Principal")) + Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")) + Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")) + Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost"));
						}
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "P")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(6, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "U")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(8, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "X")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(9, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
								// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
								else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "Y")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(10, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
									// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
									else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "C")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(2, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
										// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
										else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "A")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(1, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
											// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
											// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
											else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "S")
						{
							// put these figures in the X category because there are going away
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(9, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
												// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
												// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
												else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "D")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(3, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
													// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
													// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
													else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "I")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(4, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
							dblInterest += Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest"));
						}
														// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
														// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
														else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "L")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(5, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
															// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
															// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
															else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "3")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(0, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
							dblCosts += Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost"));
						}
																// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
																// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
																else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "R")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(7, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
																	// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
																	// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
																	else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "W")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(11, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
																		// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
																		// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
																		else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "F")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(12, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
						rsPayment.MoveNext();
					}
					fldYear.Text = "*" + fldYear.Text;
					rsCalLien.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(rsData.Get_Fields_Int32("LienRecordNumber")), modExtraModules.strCLDatabase);
					if (rsCalLien.RecordCount() > 0)
					{
						// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
						dblCosts = FCConvert.ToDouble(rsCalLien.Get_Fields("Costs")) * -1;
						// TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
						dblInterest -= Conversion.Val(rsCalLien.Get_Fields("Interest"));
					}
				}
				// if the current interest is checked then calculate it and display it
				dblTotalDue = modCLCalculations.CalculateAccountCL(ref rsData, FCConvert.ToInt32(rsData.Get_Fields_Int32("BillingYear")), reportConfiguration.Options.AsOfDate, ref dblXtraInt);
				fldPaymentReceived.Text = Strings.Format(dblPaymentRecieved, "#,##0.00");
				fldCosts.Text = Strings.Format(dblCosts * -1, "#,##0.00");
				fldInterest.Text = Strings.Format(dblInterest * -1, "#,##0.00");
				if (FCConvert.ToInt32(rsData.Get_Fields_Int32("LienRecordNumber")) > 0)
				{
					fldCurrentInterest.Text = Strings.Format(0, "#,##0.00");
				}
				else
				{
					fldCurrentInterest.Text = Strings.Format(dblXtraInt, "#,##0.00");
				}
				fldDue.Text = Strings.Format((FCConvert.ToDouble(fldTaxDue.Text) + FCConvert.ToDouble(fldCosts.Text) + FCConvert.ToDouble(fldInterest.Text) + FCConvert.ToDouble(fldCurrentInterest.Text)) - (dblPaymentRecieved), "#,##0.00");
				if (FCConvert.ToDouble(fldDue.Text) <= 0)
				{
					// if this account is not outstanding, then it must not be used
					ReversePaymentsFromStatusArray(ref rsPayment);
					rsData.MoveNext();
					lngCount -= 1;
					goto TRYAGAIN;
				}
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				if (((rsData.Get_Fields("Account"))) == lngLastAccount)
				{
					fldAccount.Text = "";
					fldName.Text = "";
					fldType.Text = "";
				}
				else
				{
					//FC:FINAL:BCU - #i181 - add convert to string
					//fldAccount.Text = rsData.Get_Fields("Account");
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					fldAccount.Text = FCConvert.ToString(rsData.Get_Fields("Account"));
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					fldAccount.Text = rsData.Get_Fields_String("Account");
					fldName.Text = rsData.Get_Fields_String("Name1");
					if (FCConvert.ToString(rsData.Get_Fields_String("BillingType")) == "RE")
					{
						fldType.Text = "R";
					}
					else
					{
						fldType.Text = "P";
					}
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					lngLastAccount = FCConvert.ToInt32(rsData.Get_Fields("Account"));
				}
				dblTotals[0] += FCConvert.ToDouble(fldTaxDue.Text);
				dblTotals[1] += dblPaymentRecieved;
				dblTotals[3] += FCConvert.ToDouble(fldDue.Text);
				dblTotals[4] += FCConvert.ToDouble(fldCosts.Text);
				dblTotals[5] += FCConvert.ToDouble(fldInterest.Text);
				dblTotals[6] += FCConvert.ToDouble(fldCurrentInterest.Text);
				// keep track for the year totals
				dblYearTotals[rsData.Get_Fields_Int32("BillingYear") - 19800] = dblYearTotals[FCConvert.ToInt16(rsData.Get_Fields_Int32("BillingYear")) - 19800] + FCConvert.ToDouble(fldDue.Text);
				// move to the next record in the query
				rsData.MoveNext();
				rsPayment.Dispose();
				rsCalLien.Dispose();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				if (Information.Err(ex).Number < 0 && Strings.Left(ex.GetBaseException().Message, 10) == "Automation")
				{
					// let it run
				}
				else
				{
					FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In BindFields");
				}
				/*? Resume; */
			}

		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void SetupTotals()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this sub will fill in the totals line at the bottom of the report
				//clsDRWrapper rsSum = new clsDRWrapper();
				string strSUM = "";
				int intSumRows/*unused?*/;
				object obNew/*unused?*/;
				int intCT/*unused?*/;
				fldTotalTaxDue.Text = Strings.Format(dblTotals[0], "#,##0.00");
				fldTotalPaymentReceived.Text = Strings.Format(dblTotals[1], "#,##0.00");
				fldTotalDue.Text = Strings.Format(dblTotals[3], "#,##0.00");
				fldTotalCosts.Text = Strings.Format(dblTotals[4], "#,##0.00");
				fldTotalInterest.Text = Strings.Format(dblTotals[5], "#,##0.00");
				fldTotalCurInterest.Text = Strings.Format(dblTotals[6], "#,##0.00");
				if (lngCount > 1)
				{
					lblTotals.Text = "Total for " + FCConvert.ToString(lngCount) + " Accounts:";
				}
				else if (lngCount == 1)
				{
					lblTotals.Text = "Total for " + FCConvert.ToString(lngCount) + " Account:";
				}
				else
				{
					lblTotals.Text = "No Accounts";
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Summary Creation");
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			SetupTotals();
		}

		private void SetReportHeader()
		{
			int intCT;
			string strTemp = "";
            string strSeparator = ",";
            var filter = reportConfiguration.Filter;
            if (filter.AccountRangeUsed())
            {
                if (filter.AccountMax.HasValue)
                {
                    if (filter.AccountMin.HasValue)
                    {
                        if (filter.AccountMin == filter.AccountMax)
                        {
                            strTemp += "Account: " + filter.AccountMin.Value.ToString();
                        }
                        else
                        {
                            strTemp += "Account: " + filter.AccountMin.Value.ToString() + " To " +
                                       filter.AccountMax.Value.ToString();
                        }
                    }
                    else
                    {
                        strTemp += "Below Account: " + filter.AccountMax.Value.ToString();
                    }
                }
                else
                {
                    strTemp += "Above Account: " + filter.AccountMin.Value.ToString();
                }

                strSeparator = ",";
            }

            if (filter.TaxYearRangeUsed())
            {
                strTemp += strSeparator;

                if (filter.TaxYearMin.HasValue)
                {
                    if (filter.TaxYearMax.HasValue)
                    {
                        strTemp += " Tax Year: " + filter.TaxYearMin.Value.ToString() + " To " +
                                   filter.TaxYearMax.Value.ToString();
                    }
                    else
                    {
                        strTemp += " Tax Year: " + filter.TaxYearMin.Value.ToString();
                    }
                }
                else
                {
                    strTemp += " Tax Year: " + filter.TaxYearMax.Value.ToString();
                }

                strSeparator = ",";
            }

			if (lngTAType > 0)
			{
				if (Strings.Trim(strTemp) == "")
				{
					if (lngTAType == 1)
					{
						strTemp = "Tax Acquired";
					}
					else
					{
						strTemp = "Non-Tax Acquired";
					}
				}
				else
				{
					if (lngTAType == 1)
					{
						strTemp = ", Tax Acquired";
					}
					else
					{
						strTemp = ", Non-Tax Acquired";
					}
				}
			}

			if (reportConfiguration.Options.ShowCurrentInterest)
			{
				if (Strings.Trim(strTemp) == "")
				{
					strTemp += "Show Interest";
				}
				else
				{
					strTemp += ", Show Interest";
				}
			}

			if (Strings.Trim(strTemp) == "")
			{
				lblReportType.Text = "Complete List";
			}
			else
			{
				lblReportType.Text = strTemp;
			}
			if (reportConfiguration.Options.UseAsOfDate())
			{
				lblReportType.Text = lblReportType.Text + "\r\n" + "As of: " + Strings.Format(reportConfiguration.Options.AsOfDate, "MM/dd/yyyy");
			}
		}

		private void AddToPaymentArray_242(int lngIndex, double dblPrin, double dblPLI, double dblCurInt, double dblCost)
		{
			AddToPaymentArray(ref lngIndex, ref dblPrin, ref dblPLI, ref dblCurInt, ref dblCost);
		}

		private void AddToPaymentArray(ref int lngIndex, ref double dblPrin, ref double dblPLI, ref double dblCurInt, ref double dblCost)
		{
			dblPayments[lngIndex, 0] += dblPrin;
			dblPayments[lngIndex, 1] += dblPLI;
			dblPayments[lngIndex, 2] += dblCurInt;
			dblPayments[lngIndex, 3] += dblCost;
		}

		private void ReversePaymentsFromStatusArray(ref clsDRWrapper rsRev)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (rsRev.RecordCount() != 0)
				{
					rsRev.MoveFirst();
					while (!rsRev.EndOfFile())
					{
						if (Strings.UCase(rsRev.Get_Fields_String("Code")) == "P")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(6, Conversion.Val(rsRev.Get_Fields("Principal")) * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("CurrentInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1));
						}
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						else if ((Strings.UCase(rsRev.Get_Fields("Code")) == "X") || (Strings.UCase(rsRev.Get_Fields("Code")) == "S"))
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(9, Conversion.Val(rsRev.Get_Fields("Principal")) * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("CurrentInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1));
						}
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							else if (Strings.UCase(rsRev.Get_Fields("Code")) == "U")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(8, Conversion.Val(rsRev.Get_Fields("Principal")) * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("CurrentInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1));
						}
								// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
								else if (Strings.UCase(rsRev.Get_Fields("Code")) == "Y")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(10, Conversion.Val(rsRev.Get_Fields("Principal")) * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("CurrentInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1));
						}
									// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
									else if (Strings.UCase(rsRev.Get_Fields("Code")) == "C")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(2, Conversion.Val(rsRev.Get_Fields("Principal")) * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("CurrentInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1));
						}
										// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
										else if (Strings.UCase(rsRev.Get_Fields("Code")) == "A")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(1, Conversion.Val(rsRev.Get_Fields("Principal")) * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("CurrentInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1));
						}
											// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
											// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
											else if (Strings.UCase(rsRev.Get_Fields("Code")) == "D")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(3,Conversion.Val( rsRev.Get_Fields("Principal")) * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("CurrentInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1));
						}
												// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
												// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
												else if (Strings.UCase(rsRev.Get_Fields("Code")) == "I")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(4, Conversion.Val(rsRev.Get_Fields("Principal")) * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("CurrentInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1));
						}
													// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
													// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
													else if (Strings.UCase(rsRev.Get_Fields("Code")) == "L")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(5, Conversion.Val(rsRev.Get_Fields("Principal")) * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("CurrentInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1));
						}
														// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
														// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
														else if (Strings.UCase(rsRev.Get_Fields("Code")) == "3")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(0, Conversion.Val(rsRev.Get_Fields("Principal")) * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("CurrentInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1));
						}
															// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
															// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
															else if (Strings.UCase(rsRev.Get_Fields("Code")) == "R")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(7, Conversion.Val(rsRev.Get_Fields("Principal")) * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("CurrentInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1));
						}
						rsRev.MoveNext();
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Reversing Payment Counts");
			}
		}

		
	}
}