﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for rptCertificateOfSettlement.
	/// </summary>
	public partial class rptCertificateOfSettlement : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               12/31/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               10/29/2004              *
		// ********************************************************
		// filled by information from the user
		string strCollectorName = "";
		// Dim strTreasurerName            As String
		string strMuniName;
		string strCounty = "";
		string strRegistryCounty = "";
		string strLegalDescription;
		string strTaxYear = "";
		// Dim strInterestRate             As String
		DateTime dtDateSigned;
		// Dim dtEndingDate                As Date
		string strMainText;
		int lngLineLen;
		double dblBalanceDue;

		public rptCertificateOfSettlement()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Certificate Of Settlement";
		}

		public static rptCertificateOfSettlement InstancePtr
		{
			get
			{
				return (rptCertificateOfSettlement)Sys.GetInstance(typeof(rptCertificateOfSettlement));
			}
		}

		protected rptCertificateOfSettlement _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void SetStrings()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				lngLineLen = 35;
				// this is how many underscores are in the printed lines
				// report caption
				lblTitleBar.Text = "Certificate Of Settlement";
				// legal description
				lblLegalDescription.Text = "36 M.R.S.A " + Strings.Chr(167) + " 763";
				// strLegalDescription
				// county
				strMainText = "COUNTY OF " + strCounty + " ss.";
				fldCounty.Text = strMainText;
				// state
				strMainText = "STATE OF MAINE";
				fldState.Text = strMainText;
				// 1st paragraph
				strMainText = "TO: " + strCollectorName + ", Tax Collector of the Municipality of ";
				strMainText += modGlobalConstants.Statics.MuniName + " within this County: " + "\r\n";
				strMainText += "We hereby certify that the " + strTaxYear + " taxes committed to you consisting of:";
				fldMainText.Text = strMainText;
				lblCommitment.Text = "Real and Personal Tax commitments:";
				lblSupplemental.Text = "Supplemental commitments totaling:";
				lblInterest.Text = "Interest";
				lblTopTotal.Text = "A grand total of:";
				lblCashPayments.Text = "Cash Payments:";
				lblAbatements.Text = "Abatements Granted:";
				lblTaxLiens.Text = "Tax Lien Mortgages:" + "\r\n" + "(Recorded in the " + strRegistryCounty + " County Registry of Deeds)";
				lblOtherCredits.Text = "Other Credits:";
				lblNetTotal.Text = "A net total of:";
				lblBalanceDue.Text = "Balance Due of:";
				// 2nd paragraph
				strMainText = "Under authority contained in " + "MRSA, Title 36, Section 763" + ", as amended, we hereby discharge you from further liability or obligation to collect the balance due of :    " + Strings.Format(dblBalanceDue, "$#,##0.00");
				// strLegalDescription
				strMainText += "\r\n" + "and acknowledge receipt of the tax lists for the taxable year " + strTaxYear + ".";
				fldText2.Text = strMainText;
				if (dtDateSigned.ToOADate() == 0)
				{
					lblDated1.Text = "Given under our hands this ____________ day of ____________________ 20____.";
				}
				else
				{
					lblDated1.Text = "Given under our hands this " + ReturnDayWithSuffix_2(FCConvert.ToInt16(FCConvert.ToDouble(Strings.Format(dtDateSigned, "dd")))) + " day of " + Strings.Format(dtDateSigned, "MMMM yyyy") + ".";
				}
				lblMunicipalOfficers.Text = "Municipal Officers";
				// strMainText = "    You are to pay to " & strTreasurerName & ", the Municipal Treasurer, or to any successor in office, "
				// strMainText = strMainText & "the taxes herewith committed, paying on the last day of each month all money collected by you, "
				// strMainText = strMainText & "and you are to complete and make an account of your collections of the whole sum on or before "
				// strMainText = strMainText & Format(dtEndingDate, "MM/dd/yy") & "." & vbCrLf & vbCrLf
				// strMainText = strMainText & "    In case of neglect of any person to pay the sum required by said list until after " & Format(dtEndingDate, "MM/dd/yy") & "; "
				// strMainText = strMainText & "you will add interest to so much thereof as remains unpaid at the rate of " & strInterestRate & " percent per annum, commencing "
				// strMainText = strMainText & Format(dtEndingDate, "MM/dd/yy") & " to the time of payment, and collect the same with the tax remaining unpaid." & vbCrLf & vbCrLf
				// strMainText = strMainText & "    Given under our hands, as provided by a legal vote of the Municipality and Warrants recieved pursuant to the Laws of the Stats of Maine, this " & Format(dtDateSigned, "MM/dd/yy") & "."
				// fldText3.Text = strMainText
				// 
				// lblCommitmentHeader.Text = "CERTIFICATE OF COMMITMENT"
				// 
				// strMainText = "TO " & strCollectorName & ", The Collector of the Municipality of " & strMuniName & ", aforesaid." & vbCrLf
				// strMainText = strMainText & "    Herewith are committed to you true lists of the assesments of Estates of the persons names; you are to levy and collect the same, "
				// strMainText = strMainText & "of each one thierrespective amount, therein set down, of the sum total of " & Format(dblBalanceDue, "$#,##0.00") & " "
				// strMainText = strMainText & "(being the amount of the lists contained herein), according to the tenor of the foregoing warrant."
				// fldText4.Text = strMainText
				lblReportVersion.Text = "PTA 258 (05/00)";
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Setting Strings");
			}
		}

		public void Init(string strPassCollectorName, string strPassMuni, string strPassCounty, string strPassRegistryCounty, string strPassLegalDescription, string strPassTaxYear, DateTime dtPassDateSigned)
		{
			this.Name = "Certificate Of Settlement";
			// , dtPassEndingDate As Date, strPassTreasurerName As String, strPassInterestRate As String)
			// this routine will set all of the variables needed
			// CollectorName
			strCollectorName = strPassCollectorName;
			// Treasurer Name
			// strTreasurerName = strPassTreasurerName
			// Muni Name
			strMuniName = strPassMuni;
			// County
			strCounty = strPassCounty;
			// County Of Registry
			strRegistryCounty = strPassRegistryCounty;
			// Legal Description
			strLegalDescription = strPassLegalDescription;
			// Tax Year
			strTaxYear = strPassTaxYear;
			// Interest Rate
			// strInterestRate = strPassInterestRate
			// Date Signed
			dtDateSigned = dtPassDateSigned;
			// Ending Date
			// dtEndingDate = dtPassEndingDate
			FillAmountFields();
			// Unload frmCertOfSettlement
			// Me.Show vbModal, MDIParent
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			SetStrings();
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
		}

		private void FillAmountFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this sub will calculate all of the amounts needed for this form
				// and fill the amounts into the correct fields
				double dblREPrinTotal;
				double dblRESupplementals;
				double dblREIntTotal;
				double dblREPayments;
				double dblREAbatements;
				double dblRELien;
				double dblREOther;
				double dblREGrandTotal;
				double dblRENet;
				double dblPPPrinTotal;
				double dblPPSupplementals;
				double dblPPIntTotal;
				double dblPPPayments;
				double dblPPAbatements;
				double dblPPOther;
				double dblPPGrandTotal;
				double dblPPNet;
				// Line 1     -   Real Estate and Personal Property Principal *****************
				dblREPrinTotal = GetYearPrincipalRE();
				dblPPPrinTotal = GetYearPrincipalPP();
				// Line 2     -   Supplemental Commitments                    *****************
				dblRESupplementals = GetYearSupplementalRE();
				dblPPSupplementals = GetYearSupplementalPP();
				// Line 3     -   Interest                                    *****************
				dblREIntTotal = GetYearInterestRE();
				dblPPIntTotal = GetYearInterestPP();
				// Line 4     -   Line 1 + 2 + 3                              *****************
				dblREGrandTotal = dblREPrinTotal + dblRESupplementals + dblREIntTotal;
				dblPPGrandTotal = dblPPPrinTotal + dblPPSupplementals + dblPPIntTotal;
				// Line 6     -   Abatements                                  *****************
				dblREAbatements = GetYearAbatementsRE();
				dblPPAbatements = GetYearAbatementsPP();
				// Line 5     -   Cash Payments                               *****************
				dblREPayments = GetYearPaymentsRE() + dblREIntTotal - dblREAbatements;
				// this will return the principal of the payments so add it to the interest
				dblPPPayments = GetYearPaymentsPP() + dblPPIntTotal - dblPPAbatements;
				// dblREPayments = dblREPayments - dblREAbatements 'subtracts the Abatement amounts from the total payments
				// dblPPPayments = dblPPPayments - dblPPAbatements
				// Line 7     -   Tax Lien Mortgages                          *****************
				dblRELien = GetLienAmount();
				// Line 8     -   Other Credits                               *****************
				dblREOther = GetDiscountsRE();
				dblPPOther = GetDiscountsPP();
				dblREPayments -= dblREOther;
				// subtracts the Discounts amounts from the total payments
				dblPPPayments -= dblPPOther;
				// Line 9     -   Line 5 + 6 + 7 + 8                          *****************
				if (dblREGrandTotal < dblREPayments + dblREAbatements + dblRELien + dblREOther)
				{
					dblREPayments -= (dblREPayments + dblREAbatements + dblRELien + dblREOther - dblREGrandTotal);
				}
				dblRENet = dblREPayments + dblREAbatements + dblRELien + dblREOther;
				if (dblPPGrandTotal < dblPPPayments + dblPPAbatements + dblPPOther)
				{
					dblPPPayments -= (dblPPPayments + dblPPAbatements + dblPPOther - dblPPGrandTotal);
				}
				dblPPNet = dblPPPayments + dblPPAbatements + dblPPOther;
				// Line 10    -   Balance Due *************************************************
				dblBalanceDue = (dblREGrandTotal + dblPPGrandTotal) - (dblRENet + dblPPNet);
				// This will put all of the values into the fields on the report
				fldCommitment.Text = Strings.Format(dblREPrinTotal + dblPPPrinTotal, "$#,##0.00");
				fldSupplement.Text = Strings.Format(dblRESupplementals + dblPPSupplementals, "$#,##0.00");
				fldInterest.Text = (Strings.Format(dblREIntTotal + dblPPIntTotal, "$#,##0.00"));
				fldCashPayments.Text = (Strings.Format(dblREPayments + dblPPPayments, "$#,##0.00"));
				fldAbatements.Text = (Strings.Format(dblREAbatements + dblPPAbatements, "$#,##0.00"));
				fldTaxLiens.Text = (Strings.Format(dblRELien, "$#,##0.00"));
				fldOtherCredits.Text = (Strings.Format(dblREOther + dblPPOther, "$#,##0.00"));
				fldTopTotal.Text = (Strings.Format(dblREGrandTotal + dblPPGrandTotal, "$#,##0.00"));
				fldNetTotal.Text = (Strings.Format(dblRENet + dblPPNet, "$#,##0.00"));
				fldBalanceDue.Text = (Strings.Format((dblREGrandTotal + dblPPGrandTotal) - (dblRENet + dblPPNet), "$#,##0.00"));
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In Calculations");
			}
		}

		private string ReturnDayWithSuffix_2(short intDay)
		{
			return ReturnDayWithSuffix(ref intDay);
		}

		private string ReturnDayWithSuffix(ref short intDay)
		{
			string ReturnDayWithSuffix = "";
			switch (intDay)
			{
				case 1:
				case 21:
				case 31:
					{
						ReturnDayWithSuffix = FCConvert.ToString(intDay) + "st";
						break;
					}
				case 2:
				case 22:
					{
						ReturnDayWithSuffix = FCConvert.ToString(intDay) + "nd";
						break;
					}
				case 3:
				case 23:
					{
						ReturnDayWithSuffix = FCConvert.ToString(intDay) + "rd";
						break;
					}
				default:
					{
						ReturnDayWithSuffix = FCConvert.ToString(intDay) + "th";
						break;
					}
			}
			//end switch
			return ReturnDayWithSuffix;
		}

		private double GetYearPrincipalRE()
		{
			double GetYearPrincipalRE = 0;
			var rsTemp = new clsDRWrapper();
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will return the total RE principal committed during this tax year

                GetYearPrincipalRE = 0;
                rsTemp.OpenRecordset(
                    "SELECT SUM(Principal) AS TotalPrin FROM (SELECT SUM(TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4) AS Principal FROM BillingMaster INNER JOIN RateRec ON BillingMaster.RateKey = RateRec.ID WHERE BillingYear / 10 = " +
                    strTaxYear + " AND RateType <> 'S' AND BillingType = 'RE') AS q1", modExtraModules.strCLDatabase);
                // rsTemp.OpenRecordset "SELECT SUM(Principal) AS TotalPrin FROM (SELECT SUM(TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4) AS Principal FROM BillingMaster WHERE BillingYear \ 10 = " & strTaxYear & " AND BillingType = 'RE')", strCLDatabase
                if (!rsTemp.EndOfFile())
                {
                    // TODO Get_Fields: Field [TotalPrin] not found!! (maybe it is an alias?)
                    // TODO Get_Fields: Field [TotalPrin] not found!! (maybe it is an alias?)
                    if (Conversion.Val(rsTemp.Get_Fields("TotalPrin")) == 0)
                    {
                        GetYearPrincipalRE = 0;
                    }
                    else
                    {
                        // TODO Get_Fields: Field [TotalPrin] not found!! (maybe it is an alias?)
                        // TODO Get_Fields: Field [TotalPrin] not found!! (maybe it is an alias?)
                        GetYearPrincipalRE = Conversion.Val(rsTemp.Get_Fields("TotalPrin"));
                    }
                }
                else
                {
                    return GetYearPrincipalRE;
                }

                return GetYearPrincipalRE;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                frmWait.InstancePtr.Unload();
                FCMessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message +
                    ".", MsgBoxStyle.Critical, "Error Getting RE Principal");
            }
            finally
            {
				rsTemp.Dispose();
            }
			return GetYearPrincipalRE;
		}

		private double GetYearSupplementalRE()
		{
			double GetYearSupplementalRE = 0;
			var rsTemp = new clsDRWrapper();
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will return the total RE supplementals during this tax year

                GetYearSupplementalRE = 0;
                rsTemp.OpenRecordset(
                    "SELECT SUM(Principal) AS TotalPrin FROM (SELECT SUM(TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4) AS Principal FROM BillingMaster INNER JOIN RateRec ON BillingMaster.RateKey = RateRec.ID WHERE BillingYear / 10 = " +
                    strTaxYear + " AND RateType = 'S' AND BillingYear % 10 <> 1 AND BillingType = 'RE') AS q1",
                    modExtraModules.strCLDatabase);
                if (!rsTemp.EndOfFile())
                {
                    // TODO Get_Fields: Field [TotalPrin] not found!! (maybe it is an alias?)
                    // TODO Get_Fields: Field [TotalPrin] not found!! (maybe it is an alias?)
                    if (Conversion.Val(rsTemp.Get_Fields("TotalPrin")) == 0)
                    {
                        GetYearSupplementalRE = 0;
                    }
                    else
                    {
                        // TODO Get_Fields: Field [TotalPrin] not found!! (maybe it is an alias?)
                        // TODO Get_Fields: Field [TotalPrin] not found!! (maybe it is an alias?)
                        GetYearSupplementalRE = Conversion.Val(rsTemp.Get_Fields("TotalPrin"));
                    }
                }
                else
                {
                    return GetYearSupplementalRE;
                }

                return GetYearSupplementalRE;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                frmWait.InstancePtr.Unload();
                FCMessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message +
                    ".", MsgBoxStyle.Critical, "Error Getting RE Supplemental");
            }
            finally
            {
				rsTemp.Dispose();
            }
			return GetYearSupplementalRE;
		}

		private double GetYearInterestRE()
		{
			double GetYearInterestRE = 0;
			var rsTemp = new clsDRWrapper();
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will return the total RE principal committed during this tax year

                double dblInt /*unused?*/;
                double dblCHGint /*unused?*/;
                double dblTotals /*unused?*/;
                rsTemp.OpenRecordset(
                    "SELECT SUM(InterestPaid) AS TotalInt FROM BillingMaster WHERE BillingYear / 10 = " + strTaxYear +
                    " AND BillingType = 'RE'", modExtraModules.strCLDatabase);
                if (!rsTemp.EndOfFile())
                {
                    // TODO Get_Fields: Field [TotalInt] not found!! (maybe it is an alias?)
                    // TODO Get_Fields: Field [TotalInt] not found!! (maybe it is an alias?)
                    if (Conversion.Val(rsTemp.Get_Fields("TotalInt")) == 0)
                    {
                        GetYearInterestRE = 0;
                    }
                    else
                    {
                        // TODO Get_Fields: Field [TotalInt] not found!! (maybe it is an alias?)
                        // TODO Get_Fields: Field [TotalInt] not found!! (maybe it is an alias?)
                        GetYearInterestRE = Conversion.Val(rsTemp.Get_Fields("TotalInt"));
                    }
                }
                else
                {
                    return GetYearInterestRE;
                }

              
                return GetYearInterestRE;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                frmWait.InstancePtr.Unload();
                FCMessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message +
                    ".", MsgBoxStyle.Critical, "Error Getting RE Interest");
            }
            catch
            {
				rsTemp.Dispose();
            }
			return GetYearInterestRE;
		}

		private double GetYearPaymentsRE()
		{
			double GetYearPaymentsRE = 0;
			var rsTemp = new clsDRWrapper();
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will return the total RE principal committed during this tax year

                double dblLienPayments = 0;
                GetYearPaymentsRE = 0;
                // rsTemp.OpenRecordset "SELECT SUM (TaxDue1 + TaxDue2 + TaxDue3 + TaxDue1 - Principal) AS TotalPay FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.LienRecordNumber WHERE BillingYear \ 10  = " & strTaxYear & " AND BillingType = 'RE' AND BillingMaster.LienRecordNumber <> 0", strCLDatabase
                rsTemp.OpenRecordset(
                    "SELECT * FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.ID WHERE BillingYear / 10  = " +
                    strTaxYear + " AND BillingType = 'RE' AND BillingMaster.LienRecordNumber <> 0",
                    modExtraModules.strCLDatabase);
                while (!rsTemp.EndOfFile())
                {
                    // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                    dblLienPayments += (Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue1")) +
                                        Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue2")) +
                                        Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue3")) +
                                        Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue4")) -
                                        Conversion.Val(rsTemp.Get_Fields("Principal")));
                    rsTemp.MoveNext();
                }

                // rsTemp.OpenRecordset "SELECT SUM(Principal + CurrentInterest + PreLienInterest) AS TotalPay FROM PaymentRec WHERE Year \ 10  = " & strTaxYear & " AND BillCode = 'R' AND NOT (Code = 'A' OR Code = 'R' OR Code = 'I')", strCLDatabase
                // rsTemp.OpenRecordset "SELECT SUM(Principal) AS TotalPay FROM PaymentRec WHERE Year \ 10  = " & strTaxYear & " AND BillCode = 'R' AND NOT (Code = 'A' OR Code = 'R' OR Code = 'I')", strCLDatabase
                // rsTemp.OpenRecordset "SELECT SUM (PrincipalPaid) AS TotalPay FROM BillingMaster WHERE BillingYear \ 10  = " & strTaxYear & " AND BillingType = 'RE'", strCLDatabase
                rsTemp.OpenRecordset(
                    "SELECT SUM (PrincipalPaid) AS TotalPay, SUM (TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4) AS TotalDue FROM BillingMaster WHERE BillingYear / 10  = " +
                    strTaxYear + " AND BillingType = 'RE' AND LienRecordNumber = 0", modExtraModules.strCLDatabase);
                if (!rsTemp.EndOfFile())
                {
                    // TODO Get_Fields: Field [TotalPay] not found!! (maybe it is an alias?)
                    // TODO Get_Fields: Field [TotalPay] not found!! (maybe it is an alias?)
                    if (Conversion.Val(rsTemp.Get_Fields("TotalPay")) == 0)
                    {
                        GetYearPaymentsRE = dblLienPayments;
                    }
                    else
                    {
                        // TODO Get_Fields: Field [TotalPay] not found!! (maybe it is an alias?)
                        // TODO Get_Fields: Field [TotalDue] not found!! (maybe it is an alias?)
                        // TODO Get_Fields: Field [TotalPay] not found!! (maybe it is an alias?)
                        // TODO Get_Fields: Field [TotalDue] not found!! (maybe it is an alias?)
                        if (Conversion.Val(rsTemp.Get_Fields("TotalPay")) <=
                            Conversion.Val(rsTemp.Get_Fields("TotalDue")))
                        {
                            // TODO Get_Fields: Field [TotalPay] not found!! (maybe it is an alias?)
                            // TODO Get_Fields: Field [TotalPay] not found!! (maybe it is an alias?)
                            GetYearPaymentsRE = Conversion.Val(rsTemp.Get_Fields("TotalPay")) + dblLienPayments;
                        }
                        else
                        {
                            // TODO Get_Fields: Field [TotalDue] not found!! (maybe it is an alias?)
                            // TODO Get_Fields: Field [TotalDue] not found!! (maybe it is an alias?)
                            GetYearPaymentsRE = Conversion.Val(rsTemp.Get_Fields("TotalDue")) + dblLienPayments;
                        }
                    }
                }
                else
                {
                    return GetYearPaymentsRE;
                }

                return GetYearPaymentsRE;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                frmWait.InstancePtr.Unload();
                FCMessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message +
                    ".", MsgBoxStyle.Critical, "Error Getting RE Payments");
            }
            finally
            {
				rsTemp.Dispose();
            }
			return GetYearPaymentsRE;
		}

		private double GetYearAbatementsRE()
		{
			double GetYearAbatementsRE = 0;
			var rsTemp = new clsDRWrapper();
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will return the total RE principal committed during this tax year

                GetYearAbatementsRE = 0;
                // rsTemp.OpenRecordset "SELECT SUM(Principal + CurrentInterest + PreLienInterest) AS TotalPay FROM PaymentRec WHERE Year \ 10  = " & strTaxYear & " AND BillCode = 'R' AND (Code = 'A' OR Code = 'R')", strCLDatabase
                rsTemp.OpenRecordset(
                    "SELECT SUM(Principal) AS TotalPay FROM PaymentRec WHERE [Year] / 10  = " + strTaxYear +
                    " AND BillCode = 'R' AND (Code = 'A' OR Code = 'R')", modExtraModules.strCLDatabase);
                // rsTemp.OpenRecordset "SELECT SUM(PrincipalPaid) AS TotalPay FROM BillingMaster WHERE BillingYear \ 10  = " & strTaxYear & " AND BillingType = 'RE'", strCLDatabase
                if (!rsTemp.EndOfFile())
                {
                    // TODO Get_Fields: Field [TotalPay] not found!! (maybe it is an alias?)
                    // TODO Get_Fields: Field [TotalPay] not found!! (maybe it is an alias?)
                    if (Conversion.Val(rsTemp.Get_Fields("TotalPay")) == 0)
                    {
                        GetYearAbatementsRE = 0;
                    }
                    else
                    {
                        // TODO Get_Fields: Field [TotalPay] not found!! (maybe it is an alias?)
                        // TODO Get_Fields: Field [TotalPay] not found!! (maybe it is an alias?)
                        GetYearAbatementsRE = Conversion.Val(rsTemp.Get_Fields("TotalPay"));
                    }
                }
                else
                {
                    return GetYearAbatementsRE;
                }

                return GetYearAbatementsRE;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                frmWait.InstancePtr.Unload();
                FCMessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message +
                    ".", MsgBoxStyle.Critical, "Error Getting RE Abatements");
            }
            finally
            {
				rsTemp.Dispose();
            }
			return GetYearAbatementsRE;
		}

		private double GetDiscountsRE()
		{
			double GetDiscountsRE = 0;
			var rsTemp = new clsDRWrapper();
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will return the total RE principal committed during this tax year

                GetDiscountsRE = 0;
                rsTemp.OpenRecordset(
                    "SELECT SUM(Principal + CurrentInterest + PreLienInterest) AS TotalPay FROM PaymentRec WHERE [Year] / 10  = " +
                    strTaxYear + " AND BillCode = 'R' AND Code = 'D'", modExtraModules.strCLDatabase);
                if (!rsTemp.EndOfFile())
                {
                    // TODO Get_Fields: Field [TotalPay] not found!! (maybe it is an alias?)
                    // TODO Get_Fields: Field [TotalPay] not found!! (maybe it is an alias?)
                    if (Conversion.Val(rsTemp.Get_Fields("TotalPay")) == 0)
                    {
                        GetDiscountsRE = 0;
                    }
                    else
                    {
                        // TODO Get_Fields: Field [TotalPay] not found!! (maybe it is an alias?)
                        // TODO Get_Fields: Field [TotalPay] not found!! (maybe it is an alias?)
                        GetDiscountsRE = Conversion.Val(rsTemp.Get_Fields("TotalPay"));
                    }
                }
                else
                {
                    return GetDiscountsRE;
                }

                return GetDiscountsRE;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                frmWait.InstancePtr.Unload();
                FCMessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message +
                    ".", MsgBoxStyle.Critical, "Error Getting RE Discounts");
            }
            finally
            {
				rsTemp.Dispose();
            }
			return GetDiscountsRE;
		}
		// ********************************************************************************************
		// Same procedues as above except for PP                                                      *
		// ********************************************************************************************
		private double GetYearPrincipalPP()
		{
			double GetYearPrincipalPP = 0;
			var rsTemp = new clsDRWrapper();
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will return the total PP principal committed during this tax year

                GetYearPrincipalPP = 0;
                rsTemp.OpenRecordset(
                    "SELECT SUM(Principal) AS TotalPrin FROM (SELECT SUM(TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4) AS Principal FROM BillingMaster INNER JOIN RateRec ON BillingMaster.RateKey = RateRec.ID WHERE BillingYear / 10 = " +
                    strTaxYear + " AND RateType <> 'S' AND BillingType = 'PP') AS q1", modExtraModules.strCLDatabase);
                // rsTemp.OpenRecordset "SELECT SUM(Principal) AS TotalPrin FROM (SELECT SUM(TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4) AS Principal FROM BillingMaster WHERE BillingYear \ 10 = " & strTaxYear & " AND BillingType = 'PP')", strCLDatabase
                if (!rsTemp.EndOfFile())
                {
                    // TODO Get_Fields: Field [TotalPrin] not found!! (maybe it is an alias?)
                    // TODO Get_Fields: Field [TotalPrin] not found!! (maybe it is an alias?)
                    if (Conversion.Val(rsTemp.Get_Fields("TotalPrin")) == 0)
                    {
                        GetYearPrincipalPP = 0;
                    }
                    else
                    {
                        // TODO Get_Fields: Field [TotalPrin] not found!! (maybe it is an alias?)
                        // TODO Get_Fields: Field [TotalPrin] not found!! (maybe it is an alias?)
                        GetYearPrincipalPP = Conversion.Val(rsTemp.Get_Fields("TotalPrin"));
                    }
                }
                else
                {
                    return GetYearPrincipalPP;
                }

                return GetYearPrincipalPP;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                frmWait.InstancePtr.Unload();
                FCMessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message +
                    ".", MsgBoxStyle.Critical, "Error Getting PP Principal");
            }
            finally
            {
				rsTemp.Dispose();
            }
			return GetYearPrincipalPP;
		}

		private double GetYearSupplementalPP()
		{
			double GetYearSupplementalPP = 0;
            var rsTemp = new clsDRWrapper();
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will return the total RE supplementals during this tax year

                GetYearSupplementalPP = 0;
                rsTemp.OpenRecordset(
                    "SELECT SUM(Principal) AS TotalPrin FROM (SELECT SUM(TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4) AS Principal FROM BillingMaster INNER JOIN RateRec ON BillingMaster.RateKey = RateRec.ID WHERE BillingYear / 10 = " +
                    strTaxYear + " AND RateType = 'S' AND BillingType = 'PP') AS q1", modExtraModules.strCLDatabase);
                // AND BillingYear MOD 10 <> 1
                if (!rsTemp.EndOfFile())
                {
                    // TODO Get_Fields: Field [TotalPrin] not found!! (maybe it is an alias?)
                    // TODO Get_Fields: Field [TotalPrin] not found!! (maybe it is an alias?)
                    if (Conversion.Val(rsTemp.Get_Fields("TotalPrin")) == 0)
                    {
                        GetYearSupplementalPP = 0;
                    }
                    else
                    {
                        // TODO Get_Fields: Field [TotalPrin] not found!! (maybe it is an alias?)
                        // TODO Get_Fields: Field [TotalPrin] not found!! (maybe it is an alias?)
                        GetYearSupplementalPP = Conversion.Val(rsTemp.Get_Fields("TotalPrin"));
                    }
                }
                else
                {
                    return GetYearSupplementalPP;
                }

                return GetYearSupplementalPP;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                frmWait.InstancePtr.Unload();
                FCMessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message +
                    ".", MsgBoxStyle.Critical, "Error Getting PP Supplemental");
            }
            finally
            {
				rsTemp.Dispose();
            }
			return GetYearSupplementalPP;
		}

		private double GetYearInterestPP()
		{
			double GetYearInterestPP = 0;
			var rsTemp = new clsDRWrapper();
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will return the total RE principal committed during this tax year

                double dblInt /*unused?*/;
                double dblCHGint /*unused?*/;
                double dblTotals /*unused?*/;
                rsTemp.OpenRecordset(
                    "SELECT SUM(InterestPaid) AS TotalInt FROM BillingMaster WHERE BillingYear / 10 = " + strTaxYear +
                    " AND BillingType = 'PP'", modExtraModules.strCLDatabase);
                if (!rsTemp.EndOfFile())
                {
                    // TODO Get_Fields: Field [TotalInt] not found!! (maybe it is an alias?)
                    // TODO Get_Fields: Field [TotalInt] not found!! (maybe it is an alias?)
                    if (Conversion.Val(rsTemp.Get_Fields("TotalInt")) == 0)
                    {
                        GetYearInterestPP = 0;
                    }
                    else
                    {
                        // TODO Get_Fields: Field [TotalInt] not found!! (maybe it is an alias?)
                        // TODO Get_Fields: Field [TotalInt] not found!! (maybe it is an alias?)
                        GetYearInterestPP = Conversion.Val(rsTemp.Get_Fields("TotalInt"));
                    }
                }
                else
                {
                    return GetYearInterestPP;
                }

                return GetYearInterestPP;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                frmWait.InstancePtr.Unload();
                FCMessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message +
                    ".", MsgBoxStyle.Critical, "Error Getting PP Interest");
            }
            finally
            {
				rsTemp.Dispose();
            }
			return GetYearInterestPP;
		}

		private double GetYearPaymentsPP()
		{
			double GetYearPaymentsPP = 0;
			var rsTemp = new clsDRWrapper();
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will return the total RE principal committed during this tax year

                GetYearPaymentsPP = 0;
                // rsTemp.OpenRecordset "SELECT SUM(Principal + CurrentInterest + PreLienInterest) AS TotalPay FROM PaymentRec WHERE Year \ 10  = " & strTaxYear & " AND BillCode = 'P' AND NOT (Code = 'A' OR Code = 'R' OR Code = 'I')", strCLDatabase
                // rsTemp.OpenRecordset "SELECT SUM(Principal) AS TotalPay FROM PaymentRec WHERE Year \ 10  = " & strTaxYear & " AND BillCode = 'P' AND NOT (Code = 'A' OR Code = 'R' OR Code = 'I')", strCLDatabase
                rsTemp.OpenRecordset(
                    "SELECT SUM (PrincipalPaid) AS TotalPay FROM BillingMaster WHERE BillingYear / 10  = " +
                    strTaxYear + " AND BillingType = 'PP'", modExtraModules.strCLDatabase);
                if (!rsTemp.EndOfFile())
                {
                    // TODO Get_Fields: Field [TotalPay] not found!! (maybe it is an alias?)
                    // TODO Get_Fields: Field [TotalPay] not found!! (maybe it is an alias?)
                    if (Conversion.Val(rsTemp.Get_Fields("TotalPay")) == 0)
                    {
                        GetYearPaymentsPP = 0;
                    }
                    else
                    {
                        // TODO Get_Fields: Field [TotalPay] not found!! (maybe it is an alias?)
                        // TODO Get_Fields: Field [TotalPay] not found!! (maybe it is an alias?)
                        GetYearPaymentsPP = Conversion.Val(rsTemp.Get_Fields("TotalPay"));
                    }
                }
                else
                {
                    return GetYearPaymentsPP;
                }

                return GetYearPaymentsPP;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                frmWait.InstancePtr.Unload();
                FCMessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message +
                    ".", MsgBoxStyle.Critical, "Error Getting PP Payments");
            }
            finally
            {
				rsTemp.Dispose();
            }
			return GetYearPaymentsPP;
		}

		private double GetYearAbatementsPP()
		{
			double GetYearAbatementsPP = 0;
			var rsTemp = new clsDRWrapper();
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will return the total RE principal committed during this tax year

                GetYearAbatementsPP = 0;
                // rsTemp.OpenRecordset "SELECT SUM(Principal + CurrentInterest + PreLienInterest) AS TotalPay FROM PaymentRec WHERE Year \ 10  = " & strTaxYear & " AND BillCode = 'P' AND (Code = 'A' OR Code = 'R')", strCLDatabase
                rsTemp.OpenRecordset(
                    "SELECT SUM(Principal) AS TotalPay FROM PaymentRec WHERE [Year] / 10  = " + strTaxYear +
                    " AND BillCode = 'P' AND (Code = 'A' OR Code = 'R')", modExtraModules.strCLDatabase);
                if (!rsTemp.EndOfFile())
                {
                    // TODO Get_Fields: Field [TotalPay] not found!! (maybe it is an alias?)
                    // TODO Get_Fields: Field [TotalPay] not found!! (maybe it is an alias?)
                    if (Conversion.Val(rsTemp.Get_Fields("TotalPay")) == 0)
                    {
                        GetYearAbatementsPP = 0;
                    }
                    else
                    {
                        // TODO Get_Fields: Field [TotalPay] not found!! (maybe it is an alias?)
                        // TODO Get_Fields: Field [TotalPay] not found!! (maybe it is an alias?)
                        GetYearAbatementsPP = Conversion.Val(rsTemp.Get_Fields("TotalPay"));
                    }
                }
                else
                {
                    return GetYearAbatementsPP;
                }

                return GetYearAbatementsPP;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                frmWait.InstancePtr.Unload();
                FCMessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message +
                    ".", MsgBoxStyle.Critical, "Error Getting PP Abatement");
            }
            finally
            {
                rsTemp.Dispose();
            }
			return GetYearAbatementsPP;
		}

		private double GetLienAmount()
		{
			double GetLienAmount = 0;
            var rsTemp = new clsDRWrapper();
            try
            {
                // On Error GoTo ERROR_HANDLER

                GetLienAmount = 0;
                rsTemp.OpenRecordset(
                    "SELECT SUM(Principal) AS TotalLien FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.ID WHERE BillingYear / 10  = " +
                    strTaxYear, modExtraModules.strCLDatabase);
                if (!rsTemp.EndOfFile())
                {
                    // TODO Get_Fields: Field [TotalLien] not found!! (maybe it is an alias?)
                    // TODO Get_Fields: Field [TotalLien] not found!! (maybe it is an alias?)
                    if (Conversion.Val(rsTemp.Get_Fields("TotalLien")) == 0)
                    {
                        GetLienAmount = 0;
                    }
                    else
                    {
                        // TODO Get_Fields: Field [TotalLien] not found!! (maybe it is an alias?)
                        // TODO Get_Fields: Field [TotalLien] not found!! (maybe it is an alias?)
                        GetLienAmount = Conversion.Val(rsTemp.Get_Fields("TotalLien"));
                    }
                }

                return GetLienAmount;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                frmWait.InstancePtr.Unload();
                FCMessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message +
                    ".", MsgBoxStyle.Critical, "Error Getting Lien Amounts");
            }
            finally
            {
                rsTemp.Dispose();
            }
			return GetLienAmount;
		}

		private double GetDiscountsPP()
		{
			double GetDiscountsPP = 0;
            var rsTemp = new clsDRWrapper();
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will return the total RE principal committed during this tax year

                GetDiscountsPP = 0;
                rsTemp.OpenRecordset(
                    "SELECT SUM(Principal + CurrentInterest + PreLienInterest) AS TotalPay FROM PaymentRec WHERE [Year] / 10  = " +
                    strTaxYear + " AND BillCode = 'P' AND Code = 'D'", modExtraModules.strCLDatabase);
                if (!rsTemp.EndOfFile())
                {
                    // TODO Get_Fields: Field [TotalPay] not found!! (maybe it is an alias?)
                    // TODO Get_Fields: Field [TotalPay] not found!! (maybe it is an alias?)
                    if (Conversion.Val(rsTemp.Get_Fields("TotalPay")) == 0)
                    {
                        GetDiscountsPP = 0;
                    }
                    else
                    {
                        // TODO Get_Fields: Field [TotalPay] not found!! (maybe it is an alias?)
                        // TODO Get_Fields: Field [TotalPay] not found!! (maybe it is an alias?)
                        GetDiscountsPP = Conversion.Val(rsTemp.Get_Fields("TotalPay"));
                    }
                }
                else
                {
                    return GetDiscountsPP;
                }

                return GetDiscountsPP;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                frmWait.InstancePtr.Unload();
                FCMessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message +
                    ".", MsgBoxStyle.Critical, "Error Getting PP Discounts");
            }
            finally
            {
                rsTemp.Dispose();
            }
			return GetDiscountsPP;
		}

		
	}
}
