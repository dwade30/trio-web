﻿namespace TWCL0000
{
	/// <summary>
	/// Summary description for rptCertMailReport.
	/// </summary>
	partial class rptCertMailReport
	{
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptCertMailReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCMFNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldIMPBnum = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.fldTotalProcessed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCMFNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblHeader2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblIMPBnum = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCMFNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldIMPBnum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalProcessed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCMFNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblIMPBnum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAccount,
				this.fldCMFNumber,
				this.fldName,
				this.fldType,
				this.fldIMPBnum
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldAccount
			// 
			this.fldAccount.Height = 0.1875F;
			this.fldAccount.Left = 0F;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldAccount.Text = null;
			this.fldAccount.Top = 0F;
			this.fldAccount.Width = 0.625F;
			// 
			// fldCMFNumber
			// 
			this.fldCMFNumber.Height = 0.1875F;
			this.fldCMFNumber.Left = 5.25F;
			this.fldCMFNumber.Name = "fldCMFNumber";
			this.fldCMFNumber.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldCMFNumber.Text = null;
			this.fldCMFNumber.Top = 0F;
			this.fldCMFNumber.Width = 0.9375F;
			// 
			// fldName
			// 
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 1.0625F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldName.Text = null;
			this.fldName.Top = 0F;
			this.fldName.Width = 4.0625F;
			// 
			// fldType
			// 
			this.fldType.Height = 0.1875F;
			this.fldType.Left = 0.625F;
			this.fldType.Name = "fldType";
			this.fldType.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 0";
			this.fldType.Text = null;
			this.fldType.Top = 0F;
			this.fldType.Width = 0.4375F;
			// 
			// fldIMPBnum
			// 
			this.fldIMPBnum.Height = 0.1875F;
			this.fldIMPBnum.Left = 6.5625F;
			this.fldIMPBnum.Name = "fldIMPBnum";
			this.fldIMPBnum.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldIMPBnum.Text = null;
			this.fldIMPBnum.Top = 0F;
			this.fldIMPBnum.Width = 0.9375F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldTotalProcessed
			});
			this.ReportFooter.Height = 0.40625F;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			// 
			// fldTotalProcessed
			// 
			this.fldTotalProcessed.Height = 0.1875F;
			this.fldTotalProcessed.Left = 0F;
			this.fldTotalProcessed.Name = "fldTotalProcessed";
			this.fldTotalProcessed.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldTotalProcessed.Text = null;
			this.fldTotalProcessed.Top = 0.125F;
			this.fldTotalProcessed.Width = 5.6875F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblHeader,
				this.lblMuniName,
				this.lblTime,
				this.lblDate,
				this.lblPage,
				this.lblAccount,
				this.lblName,
				this.lblCMFNumber,
				this.Line1,
				this.lblHeader2,
				this.lblType,
				this.lblIMPBnum
			});
			this.PageHeader.Height = 0.9375F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.375F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.lblHeader.Text = null;
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 7.5F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 2.9375F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1875F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.1875F;
			this.lblTime.Width = 1.25F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 6.25F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1.25F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 6.25F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblPage.Text = null;
			this.lblPage.Top = 0.1875F;
			this.lblPage.Width = 1.25F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 0F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
			this.lblAccount.Text = "Acct";
			this.lblAccount.Top = 0.75F;
			this.lblAccount.Width = 0.625F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.1875F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 1.0625F;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left; ddo-char-set: 0";
			this.lblName.Text = "Name";
			this.lblName.Top = 0.75F;
			this.lblName.Width = 0.875F;
			// 
			// lblCMFNumber
			// 
			this.lblCMFNumber.Height = 0.1875F;
			this.lblCMFNumber.HyperLink = null;
			this.lblCMFNumber.Left = 5F;
			this.lblCMFNumber.Name = "lblCMFNumber";
			this.lblCMFNumber.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left; ddo-char-set: 0";
			this.lblCMFNumber.Text = "Cert Mail Number";
			this.lblCMFNumber.Top = 0.75F;
			this.lblCMFNumber.Width = 1.375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.9375F;
			this.Line1.Width = 7.5F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.5F;
			this.Line1.Y1 = 0.9375F;
			this.Line1.Y2 = 0.9375F;
			// 
			// lblHeader2
			// 
			this.lblHeader2.Height = 0.5625F;
			this.lblHeader2.HyperLink = null;
			this.lblHeader2.Left = 0F;
			this.lblHeader2.Name = "lblHeader2";
			this.lblHeader2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 0";
			this.lblHeader2.Text = null;
			this.lblHeader2.Top = 0.375F;
			this.lblHeader2.Width = 7.5F;
			// 
			// lblType
			// 
			this.lblType.Height = 0.1875F;
			this.lblType.HyperLink = null;
			this.lblType.Left = 0.625F;
			this.lblType.Name = "lblType";
			this.lblType.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
			this.lblType.Text = "Type";
			this.lblType.Top = 0.75F;
			this.lblType.Width = 0.375F;
			// 
			// lblIMPBnum
			// 
			this.lblIMPBnum.Height = 0.1875F;
			this.lblIMPBnum.HyperLink = null;
			this.lblIMPBnum.Left = 6.4375F;
			this.lblIMPBnum.Name = "lblIMPBnum";
			this.lblIMPBnum.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left; ddo-char-set: 0";
			this.lblIMPBnum.Text = "IMPB Number";
			this.lblIMPBnum.Top = 0.75F;
			this.lblIMPBnum.Width = 1.0625F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// rptCertMailReport
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCMFNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldIMPBnum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalProcessed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCMFNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblIMPBnum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCMFNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldIMPBnum;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalProcessed;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCMFNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblIMPBnum;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
