﻿using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System;
 using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using Global;
using SharedApplication.Extensions;
using TWSharedLibrary;

namespace TWCL0000
{
    /// <summary>
    /// Summary description for SectionReport1.
    /// </summary>
    public partial class arLienProcess : BaseSectionReport
    {
        //=========================================================
        // ********************************************************
        // PROPERTY OF TRIO SOFTWARE CORPORATION                  *
        // *
        // WRITTEN BY     :               Jim Bertolino           *
        // DATE           :               06/08/2002              *
        // *
        // MODIFIED BY    :               Jim Bertolino           *
        // LAST UPDATED   :               06/05/2006              *
        // ********************************************************
        const string strDefFontName = "Courier New";
        // kk02082016  Add font constants, can add option to set it in the future
        const int intDefFontSize = 10;
        //FC:FINAL:AM
        //const double lngCharWidth = 1440.0 / 12;
        const double lngCharWidth = 1.0 / 12;
        // kk06092016  Width in twips of a single Courier New 10pt char (12 char/inch)
        private cAddressControllerCL tAddrCtrlr = new cAddressControllerCL();
        clsDRWrapper rsData = new clsDRWrapper();
        clsDRWrapper rsCMFNumbers = new clsDRWrapper();
        string strSQL;
        string strText = "";
        // VBto upgrade warning: intYear As short --> As int	OnRead(string)
        int intYear;
        bool boolPayCert;
        bool boolMort;
        bool boolPayMortCert;
        bool boolPayNewOwner;
        short intBilledOwnerAddr; // trocl-1335 09.15.17 kjr
        clsDRWrapper rsCert;
        int lngCopies;
        // this is to keep track of the number of mortgage holder copies left
        bool boolCopy;
        // is this a copy or the original
        int intNewOwnerChoice;
        // this is if the user wants to send to any new owners 0 = Do not send any, 1 =
        clsDRWrapper rsMort = new clsDRWrapper();
        clsDRWrapper rsRE = new clsDRWrapper();
        clsDRWrapper rsRate = new clsDRWrapper();
        clsDRWrapper rsPayments = new clsDRWrapper();
        bool boolNewOwner;
        // this is true if there is a new owner
        bool boolAtLeastOneRecord;
        // this is true if there is at least one record to see
        string strAcctList = "";
        bool boolREMatch;
        clsDRWrapper rsBook = new clsDRWrapper();
        int lngArrayIndex;
        bool boolRepeatAccount;
        int intLocationType;
        string strRKList = "";
        string strReportDesc = "";
        // this will be the string that will describe the parameters the user set for the report ie. Account 1 to 100
        string strDateCheck = "";
        double dblFilingFee;
        int lngIPCopies;
        // this is to keep track of the number of Interested Party copies to print
        bool blnIP;
        bool blnPayIPCert;
        clsDRWrapper rsIntParty = new clsDRWrapper();
        int NumberOfMortgageHolders;
        // Moved to Module level
        int lngIntParties;
        bool blnIsMortCopy;
        bool blnIsIPCopy;
        int intLines;
        // Moved to Module level ; MAL@20080507: Tracker Reference: 13639
        // these variables are for counting the accounts
        public int lngBalanceZero;
        public int lngBalanceNeg;
        public int lngBalancePos;
        public int lngDemandFeesApplied;
        string strBalanceZero = "";
        string strBalanceNeg = "";
        string strBalancePos = "";
        string strDemandFeesApplied = "";
        public bool ShowPaperSize { get; set; } = true;

        public arLienProcess()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
            this.Name = "Tax Lien Certificate";
        }

        private void InitializeComponentEx()
        {
            if (_InstancePtr == null)
                _InstancePtr = this;
        }

        public static arLienProcess InstancePtr
        {
            get
            {
                return (arLienProcess)Sys.GetInstance(typeof(arLienProcess));
            }
        }

        protected arLienProcess _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
            {
                _InstancePtr = null;
                Sys.ClearInstance(this);
            }
            if (disposing)
            {
                if (rsMort != null)
                    rsMort.Dispose();
                if (rsBook != null)
                    rsBook.Dispose();
                if (rsRate != null)
                    rsRate.Dispose();
                if (rsCert != null)
                    rsCert.Dispose();
                if (rsCMFNumbers != null)
                    rsCMFNumbers.Dispose();
                if (rsIntParty != null)
                    rsIntParty.Dispose();
                if (rsRE != null)
                    rsRE.Dispose();
                if (rsData != null)
                    rsData.Dispose();
            }
            base.Dispose(disposing);
        }

        private void ActiveReport_DataInitialize(object sender, System.EventArgs e)
        {
            Fields.Add("Account_Copy");
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
        {
            eArgs.EOF = rsData.EndOfFile();
            if (eArgs.EOF)
            {
                EndReport();
            }
            else
            {
                if (boolCopy)
                {
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    Fields["Account_Copy"].Value = FCConvert.ToString(rsData.Get_Fields("Account")) + "-" + FCConvert.ToString(lngCopies);
                }
                else
                {
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    Fields["Account_Copy"].Value = FCConvert.ToString(rsData.Get_Fields("Account")) + "-0";
                }
                // Debug.Print Fields("Account-Copy").Value
            }
        }

        private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
        {
        }

        private void EndReport()
        {
            // this will set all of the bill records to status of 1
            frmFreeReport.InstancePtr.vsSummary.TextMatrix(1, 1, FCConvert.ToString(lngBalanceZero));
            frmFreeReport.InstancePtr.vsSummary.TextMatrix(2, 1, FCConvert.ToString(lngBalanceNeg));
            frmFreeReport.InstancePtr.vsSummary.TextMatrix(3, 1, FCConvert.ToString(lngBalancePos));
            frmFreeReport.InstancePtr.vsSummary.TextMatrix(4, 1, FCConvert.ToString(lngBalanceZero + lngBalanceNeg + lngBalancePos));
            // this will take the trailing comma off
            if (strBalanceZero.Length > 0)
            {
                strBalanceZero = Strings.Left(strBalanceZero, strBalanceZero.Length - 1);
            }
            if (strBalanceNeg.Length > 0)
            {
                strBalanceNeg = Strings.Left(strBalanceNeg, strBalanceNeg.Length - 1);
            }
            if (strBalancePos.Length > 0)
            {
                strBalancePos = Strings.Left(strBalancePos, strBalancePos.Length - 1);
            }
            if (strDemandFeesApplied.Length > 0)
            {
                strDemandFeesApplied = Strings.Left(strDemandFeesApplied, strDemandFeesApplied.Length - 1);
            }
            // set the strings of accounts for the return grid to show
            frmFreeReport.InstancePtr.vsSummary.TextMatrix(1, 2, strBalanceZero);
            frmFreeReport.InstancePtr.vsSummary.TextMatrix(2, 2, strBalanceNeg);
            frmFreeReport.InstancePtr.vsSummary.TextMatrix(3, 2, strBalancePos);
            frmFreeReport.InstancePtr.vsSummary.TextMatrix(4, 2, strDemandFeesApplied);
            if (lngDemandFeesApplied > 0)
            {
                // show a special label that is highlighted to show this information
                // frmFreeReport.vsSummary.AddItem ""
                // frmFreeReport.vsSummary.AddItem ""
                if ((((frmFreeReport.InstancePtr.vsSummary.Rows - 1) * frmFreeReport.InstancePtr.vsSummary.RowHeight(1)) + 70) < (frmFreeReport.InstancePtr.fraSummary.Height - 200))
                {
                    frmFreeReport.InstancePtr.vsSummary.Height = ((frmFreeReport.InstancePtr.vsSummary.Rows - 1) * frmFreeReport.InstancePtr.vsSummary.RowHeight(1)) + 70;
                }
                else
                {
                    frmFreeReport.InstancePtr.vsSummary.Height = frmFreeReport.InstancePtr.fraSummary.Height - 200;
                }
            }
            if (boolRepeatAccount)
            {
                // if this is a repeating account at the end then we need to remove the last page because it is a duplicate
                if (this.Document.Pages.Count > 0)
                {
                    this.Document.Pages.RemoveAt(this.Document.Pages.Count - 1);
                }
            }
            frmWait.InstancePtr.Unload();
            frmFreeReport.InstancePtr.Focus();
        }

        private void ActiveReport_ReportStart(object sender, EventArgs e)
        {
            int intDefSel = 0;
            string strTemp = "";
            frmWait.InstancePtr.Unload();
            FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
            // kk04182014 trocl-1156  Select paper size beforehand since the Printer is selected later
            // kk09192014 trocl-1156  Save and load the selection as the default, default to Letter size
            modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL", "LienPaperSize", ref strTemp);
            if (strTemp == "Legal")
            {
                intDefSel = 1;
            }
            else
            {
                intDefSel = 0;
            }

            if (ShowPaperSize)
            {
                frmQuestion.InstancePtr.Init(10, "Letter (8.5 x 11)", "Legal (8.5 x 14)", "Select Paper Size", intDefSel);
                if (modGlobal.Statics.gintPassQuestion == 0)
                {
                    strTemp = "Letter";
                }
                else
                {
                    strTemp = "Legal";
                }
            }
            else
            {
                strTemp = "Letter";
            }
            
            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "LienPaperSize", strTemp);
            if (modGlobal.Statics.gintPassQuestion == 0)
            {
                this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
                // Letter size
            }
            else
            {
                this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Legal;
            }
            frmQuestion.InstancePtr.Unload();
            //Application.DoEvents();
            FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
            frmWait.InstancePtr.Show();
            // kk02042016 trout-1197  Margins effective 10/2015   1.5" top 1st page, 1.5" bottom last page, 0.75" left and right margins
            this.PageSettings.Margins.Top = 1.5F + FCConvert.ToSingle(modGlobal.Statics.gdblLienAdjustmentTop);
            this.PageSettings.Margins.Bottom = 1.5F + FCConvert.ToSingle(modGlobal.Statics.gdblLienAdjustmentBottom);
            // kk04212014 trocl-1156
            this.PageSettings.Margins.Left = 0.75F + FCConvert.ToSingle(modGlobal.Statics.gdblLienAdjustmentSide);
            this.PageSettings.Margins.Right = this.PageSettings.Margins.Left;
            this.PrintWidth = 8.5F - (this.PageSettings.Margins.Left + this.PageSettings.Margins.Right);
            fldHeader.Width = this.PrintWidth;
            rtbText.Width = this.PrintWidth;
            rtbText2.Width = this.PrintWidth;
            //this.Visible = false; // this will have to be set back to true to be shown
            if (Strings.Trim(modGlobalConstants.Statics.gstrTownSealPath) != "" && modCLCalculations.Statics.gboolCLUseTownSealLien)
            {
                // MAL@20080602: Add check for file existence
                // Tracker Reference: 14008
                if (File.Exists(modGlobalConstants.Statics.gstrTownSealPath))
                {
                    imgTownSeal.Visible = true;
                    imgTownSeal.Image = FCUtils.LoadPicture(modGlobalConstants.Statics.gstrTownSealPath);
                    lblAccount.Left = imgTownSeal.Width;
                }
                else
                {
                    imgTownSeal.Visible = false;
                }
            }
            else
            {
                imgTownSeal.Visible = false;
            }
            SetReportFont();
            
            SetHeader(true);
            lngBalanceNeg = 0;
            lngBalancePos = 0;
            lngBalanceZero = 0;
            lngDemandFeesApplied = 0;
            boolPayNewOwner = false;
            if (Strings.UCase(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1), 7)) == Strings.UCase("Show al"))
            {
                intLocationType = 0;
            }
            else if (Strings.UCase(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1), 7)) == Strings.UCase("Show lo"))
            {
                intLocationType = 1;
            }
            else if (Strings.UCase(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1), 7)) == Strings.UCase("Do not "))
            {
                intLocationType = 2;
            }
            boolPayCert = FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[17].RowNumber, 1), 2) == "Ye");
            boolMort = FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[30].RowNumber, 1), 2) == "Ye");
            blnIP = FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1), 2) == "Ye");
            if (Strings.Mid(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[16].RowNumber, 1), 29, 4).ToUpper() == "LAST")
            { // trocl-1335 09.11.2017 kjr, 5.23.18
                intBilledOwnerAddr = 2;
            }
            else
            {
                if (Strings.Mid(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[16].RowNumber, 1), 27, 3) == "C/O")
                {
                    intBilledOwnerAddr = 1;
                }
                else
                {
                    intBilledOwnerAddr = 0;
                }
            }
            if (Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[34].RowNumber, 1), 2) == "Ye")
            {
                if (Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[34].RowNumber, 1), 7) == "Yes, ch")
                {
                    intNewOwnerChoice = 2;
                    boolPayNewOwner = true;
                }
                else
                {
                    intNewOwnerChoice = 1;
                }
            }
            else
            {
                intNewOwnerChoice = 0;
            }
            boolNewOwner = false;
            if (boolMort)
            {
                boolPayMortCert = !FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[30].RowNumber, 1), 9) == "Yes, with");
            }
            else
            {
                boolPayMortCert = false;
            }
            if (blnIP)
            {
                blnPayIPCert = !FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1), 9) == "Yes, with");
            }
            else
            {
                blnPayIPCert = false;
            }
            if (frmFreeReport.InstancePtr.chkRecommitment.CheckState == Wisej.Web.CheckState.Checked)
            {
                // make sure that the recommitment variables are set with whatever the user typed in
                modRhymalReporting.Statics.frfFreeReport[31].DatabaseFieldName = frmFreeReport.InstancePtr.vsRecommitment.TextMatrix(0, 1);
                if (Information.IsDate(frmFreeReport.InstancePtr.vsRecommitment.TextMatrix(1, 1)))
                {
                    //FC:FINAL:MSH - i.issue #1005: incorrect date format
                    modRhymalReporting.Statics.frfFreeReport[32].DatabaseFieldName = Strings.Format(frmFreeReport.InstancePtr.vsRecommitment.TextMatrix(1, 1), "MMMM d yyyy");
                }
                else
                {
                    //FC:FINAL:MSH - I.Issue #1005: incorrect date format
                    modRhymalReporting.Statics.frfFreeReport[32].DatabaseFieldName = Strings.Format(DateTime.Today, "MMMM d, yyyy");
                }
            }
            // this will add a picture of the signature of the treasurer to the 30 Day Notice
            if (modGlobal.Statics.gboolUseSigFile)
            {
                imgSig.Visible = true;
                //rtbText.ZOrder(0);
                imgSig.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
                //imgSig.ZOrder(1); // MAL@20071015: Changed from zero
                // Line2.ZOrder 0
                imgSig.Image = FCUtils.LoadPicture(modSignatureFile.Statics.gstrCollectorSigPath);
            }
            else
            {
                imgSig.Visible = false;
            }
            // If gboolRE Then
            rsRE.OpenRecordset("SELECT *, DeedName1 as Own1, DeedName2 as Own2 FROM MASTER", modExtraModules.strREDatabase);

            rsRate.OpenRecordset("SELECT * FROM RateRec", modExtraModules.strCLDatabase);
            //modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
            intYear = FCConvert.ToInt32(Math.Round(Conversion.Val(modExtraModules.FormatYear(frmRateRecChoice.InstancePtr.cmbYear.Text))));
            strSQL = SetupSQL();
            rsData.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
            if (rsData.EndOfFile() != true)
            {
                // lngMaxNumber = rsData.RecordCount
                rsCMFNumbers.OpenRecordset("SELECT * FROM CMFNumbers", modExtraModules.strCLDatabase);
                // lblPageNumber.Text = PadToString(rsData.Fields("Account"), 6)    'this will show the account number in the top right of the page
            }
            frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Data", true, rsData.RecordCount());
        }
        // VBto upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
        private short FindLineNumber()
        {
            short FindLineNumber = 0;
            // this will attempt to find the number of lines to the top of the signature line
            // by counting the number of soft and hard returns until the signature line is found
            string strText;
            // VBto upgrade warning: lngLines As int	OnWrite(int, double)
            double lngLines;
            int lngSigLine = 0;
            string strSigText = "";
            int lngDiff = 0;
            int intCharsPerLine;
            int lngSoftReturns = 0;
            bool blnLongMuniName;
            strText = rtbText.Text;
            // MAL@20071009: Adjusted for longer town names
            // Call Reference: 117168
            if (GetVariableValue_2("MUNI").Length > 10)
            {
                blnLongMuniName = true;
                strSigText = "Total             :";
                // strSigText = "                   " '----------- line
            }
            else
            {
                blnLongMuniName = false;
                strSigText = "                   ";
                // strSigText = "Interest          :"
            }
            intCharsPerLine = 80;
            lngLines = 0;
            // push the sig file onto the line
            do
            {
                lngDiff = Strings.InStr(1, strText, "\r\n");
                // find the next hard return
                lngSigLine = Strings.InStr(1, strText, strSigText);
                // find the signature line
                if (lngDiff == 0)
                {
                    lngLines += (strText.Length / intCharsPerLine);
                    // this counts soft returns
                }
                else
                {
                    if (lngSigLine > lngDiff)
                    {
                        // if the signature line is closer than the next hard return
                        lngSoftReturns = (lngDiff / intCharsPerLine);
                        // this counts soft returns
                        lngLines += lngSoftReturns;
                        if (lngDiff > intCharsPerLine && lngDiff % intCharsPerLine < 10)
                        {
                            // do not add a hard return because it already has calculated for it
                            // Stop
                        }
                        else
                        {
                            lngLines += 1;
                            // this will add the hard return
                        }
                    }
                    else
                    {
                        // Dave added 1
                        // If lngSigLine > intCharsPerLine Then
                        lngLines += (lngSigLine / intCharsPerLine);
                        lngDiff = 0;
                        // this will end the loop
                    }
                    strText = Strings.Right(strText, strText.Length - (lngDiff + 1));
                    // remove the text that I have already counted
                }
            }
            while (!(lngDiff == 0));
            // MAL@20080616: Add option for signature adjustment
            // Tracker Reference: 14279
            lngLines += modGlobal.Statics.gdblLienSigAdjust;
            FindLineNumber = FCConvert.ToInt16(lngLines);
            return FindLineNumber;
        }

        private void ActiveReport_Terminate(object sender, EventArgs e)
        {
            DateTime dtDate;
            // this will create another active report with the summary information on it so the user can print if the feel like it
            //FC:FINAl:SBE - #2046 - Report Terminate is execute after report viewer form is closed. Visible property is true in original
            //if (frmReportViewer.InstancePtr.Visible)
            {
                if (Information.IsDate(strDateCheck))
                {
                    dtDate = DateAndTime.DateValue(strDateCheck);
                }
                else
                {
                    dtDate = DateTime.Today;
                }
                //rptLienNoticeSummary.InstancePtr.Hide();
                fecherFoundation.Sys.ClearInstance(frmReportViewer.InstancePtr);
                rptLienNoticeSummary.InstancePtr.Init(strAcctList, "Lien Notice Summary", strReportDesc, intYear, dtDate, 1, strRKList, FCConvert.CBool(frmFreeReport.InstancePtr.cmbSortOrder.SelectedIndex == 0), intBilledOwnerAddr);
            }
            frmFreeReport.InstancePtr.Unload();
        }

        private void Detail_Format(object sender, EventArgs e)
        {
            BindFields();
        }

        private void BindFields()
        {
            // Dim intLines                    As Integer
            int lngSignStart = 0;
            int lngSignEnd = 0;
            string strText = "";
            // this sub will put all of the information into the string
            if (rsData.EndOfFile() != true)
            {
                // calculate fields
                CalculateVariableTotals();
                if (rsData.EndOfFile())
                    return;
                frmWait.InstancePtr.IncrementProgress();
                strText = frmFreeReport.InstancePtr.rtbData.Text;
                lngSignStart = Strings.InStr(strText, "<SIGNATURELINE>", CompareConstants.vbTextCompare/*?*/);
                if (lngSignStart > 0)
                {
                    lngSignEnd = lngSignStart + "<SIGNATURELINE>".Length + 2;
                    rtbText.SetHtmlText(SetupVariablesInString(Strings.Left(strText, lngSignStart - 3)));
                    SetupNonRTFCostBreakdownAndSignature();
                    rtbText2.SetHtmlText(SetupVariablesInString(Strings.Mid(strText, lngSignEnd)));
                }
                else
                {
                    rtbText.SetHtmlText(SetupVariablesInString(frmFreeReport.InstancePtr.rtbData.Text));
                }
                if (modGlobal.Statics.gboolUseSigFile)
                {
                    // kk02052016 trout-1197  Rework lien notice. Change the way signature line is located.
                    // The adjustments are applied before the richtext section is expanded, so we have to limit the adjustment amount.
                    // If the signature moves into the rtbText area or above it, the signature will not be pushed down when the rtbText is expanded.
                    if (modGlobal.Statics.gdblLienSigAdjust < 0)
                    {
                        // The digital signature starts out directly below the rich text box
                        imgSig.Top = rtbText.Top + rtbText.Height;
                        // Don't allow it to go any higher
                    }
                    else
                    {
                        if (imgSig.Top + (modGlobal.Statics.gdblLienSigAdjust * 1440) < Line2.Y1)
                        {
                            // The farthest it can move is so the top is even with the signature line
                            imgSig.Top += FCConvert.ToSingle(modGlobal.Statics.gdblLienSigAdjust * 1440);
                        }
                        else
                        {
                            imgSig.Top = Line2.Y1;
                        }
                    }

                }
                if (lngCopies > 1)
                {
                    // this is to produce multiple copies for certain parties (mortgage holders)
                    lngCopies -= 1;
                    boolCopy = true;

                    if (NumberOfMortgageHolders > 0 && lngCopies >= NumberOfMortgageHolders)
                    {
                        NumberOfMortgageHolders -= 1;
                        blnIsMortCopy = true;
                        blnIsIPCopy = false;
                    }
                    else if (lngIntParties > 0 && lngCopies >= lngIntParties)
                    {
                        lngIntParties -= 1;
                        blnIsMortCopy = false;
                        blnIsIPCopy = true;
                    }
                    else
                    {
                        blnIsMortCopy = false;
                        blnIsIPCopy = false;
                    }
                }
                else
                {
                    rsData.MoveNext();
                    boolCopy = false;
                }
            }
        }

        private void SetHeader(bool boolFirstRun = false)
        {
            if (boolFirstRun)
            {
                fldHeader.Text = "State of Maine" + "\r\n" + "Tax Lien Certificate" + "\r\n" + FCConvert.ToString(intYear);
                // GetVariableValue("TAXYEAR")
                this.Name = "Tax Lien Certificate";
            }
            else
            {
                if (boolCopy)
                {
                    fldHeader.Text = "State of Maine" + "\r\n" + " * * * COPY * * *  Tax Lien Certificate  * * * COPY * * * ";
                }
                else
                {
                    fldHeader.Text = "State of Maine" + "\r\n" + "Tax Lien Certificate" + "\r\n" + FCConvert.ToString(intYear);
                    // GetVariableValue("TAXYEAR")
                }
            }
        }

        private string SetupSQL()
        {
            string SetupSQL = "";
            // this will return the SQL statement for this batch of reports
            string strWhereClause = "";
            int intCT/*unused?*/;
            string strOrder = "";
            // MAL@20080702: Add new option for sort order
            // Tracker Reference: 11834
            if (frmFreeReport.InstancePtr.cmbSortOrder.SelectedIndex == 0)
            {
                strOrder = " ORDER BY Account, Name1";
            }
            else
            {
                strOrder = " ORDER BY Name1, Account";
            }
            if (frmRateRecChoice.InstancePtr.cmbRange.SelectedIndex == 2)
            {
                // range of accounts
                if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[0].Text) != "")
                {
                    if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
                    {
                        // both full
                        strWhereClause = " AND Account >= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text)) + " AND Account <= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
                    }
                    else
                    {
                        // first full second empty
                        strWhereClause = " AND Account >= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text));
                    }
                }
                else
                {
                    if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
                    {
                        // first empty second full
                        strWhereClause = " AND Account <= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
                    }
                    else
                    {
                        // both empty
                        strWhereClause = "";
                    }
                }
            }
            else if (frmRateRecChoice.InstancePtr.cmbRange.SelectedIndex == 1)
            {
                // range of names
                if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[0].Text) != "")
                {
                    if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
                    {
                        // both full
                        strWhereClause = " AND Name1 >= '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "   ' AND Name1 <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "ZZZ'";
                    }
                    else
                    {
                        // first full second empty
                        strWhereClause = " AND Name1 >= '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "'";
                    }
                }
                else
                {
                    if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
                    {
                        // first empty second full
                        strWhereClause = " AND Name1 <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "'";
                    }
                    else
                    {
                        // both empty
                        strWhereClause = "";
                    }
                }
            }
            else
            {
                strWhereClause = "";
            }
            strRKList = frmRateRecChoice.InstancePtr.RateKeyList();
            if (Strings.Trim(strRKList) != "")
            {
                strRKList = " AND RateKey IN " + strRKList;
            }
            // create the string for the minimum amount
            // If frmFreeReport.dblMinimumAmount > 0 Then
            strWhereClause += " AND (TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4 - PrincipalPaid) > " + FCConvert.ToString(frmFreeReport.InstancePtr.dblMinimumAmount) + strRKList;

            SetupSQL = "SELECT * From BillingMaster WHERE BillingType = 'RE' AND NOT (LienProcessExclusion = 1) AND (LienProcessStatus = 2 OR LienProcessStatus = 3) AND LienStatusEligibility >= 3 And BillingYear/10 = " + FCConvert.ToString(intYear) + strWhereClause + strOrder;
            return SetupSQL;
        }

        private string SetupVariablesInString(string strOriginal)
        {
            string SetupVariablesInString = "";
            // this will replace all of the variables with the information needed
            string strBuildString;
            // this is the string that will be built and returned at the end
            string strTemp = "";
            // this is a temporary sting that will be used to store and transfer string segments
            int lngNextVariable;
            // this is the position of the beginning of the next variable (0 = no more variables)
            int lngEndOfLastVariable;
            // this is the position of the end of the last variable
            lngEndOfLastVariable = 0;
            lngNextVariable = 1;
            strBuildString = "";
            // priming read
            if (Strings.InStr(1, strOriginal, "<") > 0)
            {
                lngNextVariable = Strings.InStr(lngEndOfLastVariable + 1, strOriginal, "<");
                // do until there are no more variables left
                do
                {
                    // add the string from lngEndOfLastVariable - lngNextVariable to the BuildString
                    strBuildString += Strings.Mid(strOriginal, lngEndOfLastVariable + 1, lngNextVariable - lngEndOfLastVariable - 1);
                    // set the end pointer
                    lngEndOfLastVariable = Strings.InStr(lngNextVariable, strOriginal, ">");
                    // replace the variable
                    strBuildString += GetVariableValue_2(Strings.Mid(strOriginal, lngNextVariable + 1, lngEndOfLastVariable - lngNextVariable - 1));
                    // check for another variable
                    lngNextVariable = Strings.InStr(lngEndOfLastVariable, strOriginal, "<");
                }
                while (!(lngNextVariable == 0));
            }
            // take the last of the string and add it to the end
            strBuildString += Strings.Mid(strOriginal, lngEndOfLastVariable + 1);
            // check for variables
            if (Strings.InStr(1, "<", strOriginal) > 0)
            {
                // strBuildString = SetupVariablesInString(strBuildString)     'setup recursion
                FCMessageBox.Show("ERROR: There are still variables in the report string.", MsgBoxStyle.Critical, "SetupVariablesInString Error");
            }
            else
            {
                SetupVariablesInString = strBuildString;
            }
            return SetupVariablesInString;
        }

        private string SetupCostBreakdownAndSignatureLine()
        {
            string SetupCostBreakdownAndSignatureLine = "";
            // this will setup the bottom part of the detail section
            string strLineText;
            double dblTotal;
            string strCollector = "";
            double dblDemand = 0;
            double dblCertTotal = 0;
            if (frmFreeReport.InstancePtr.chkRecommitment.CheckState == Wisej.Web.CheckState.Checked)
            {
                strCollector = GetVariableValue_2("COLLECTOR");
            }
            else
            {
                strCollector = GetVariableValue_2("COLLECTOR");
            }
            if (Conversion.Val(GetVariableValue_2("DEMAND")) != 0)
            {
                dblDemand = FCConvert.ToDouble(GetVariableValue_2("DEMAND"));
            }
            else
            {
                dblDemand = 0;
            }
            if (Conversion.Val(GetVariableValue_2("FILINGFEE")) != 0)
            {
                dblFilingFee = FCConvert.ToDouble(GetVariableValue_2("FILINGFEE"));
            }
            else
            {
                dblFilingFee = 0;
            }
            if (Conversion.Val(GetVariableValue_2("CERTTOTAL")) != 0)
            {
                dblCertTotal = FCConvert.ToDouble(GetVariableValue_2("CERTTOTAL"));
            }
            else
            {
                dblCertTotal = 0;
            }
            //FC:FINAL:MHO:#i182 - correct conversion
            //dblTotal = dblDemand + dblFilingFee + dblCertTotal + FCConvert.ToDouble(GetVariableValue_2("PRINCIPAL"))) - Conversion.Val(rsData.Get_Fields("PrincipalPaid")) + FCConvert.ToDouble(GetVariableValue_2("INTEREST")));
            dblTotal = dblDemand + dblFilingFee + dblCertTotal + FCConvert.ToDouble(GetVariableValue_2("PRINCIPAL").Replace("$", "")) - Conversion.Val(rsData.Get_Fields_Decimal("PrincipalPaid")) + FCConvert.ToDouble(GetVariableValue_2("INTEREST").Replace("$", ""));
            // Statutory Fees and Mailing Costs
            strLineText = "Costs to be paid by taxpayer:" + "\r\n";
            // we talked to an MMA lawyer 10/18/2004, they like the minimal amounts
            strLineText += "Statutory Fees and " + "\r\n";
            strLineText += "Mailing Costs     : " + modGlobalFunctions.PadStringWithSpaces(Strings.Format(dblFilingFee + dblDemand + dblCertTotal, "$#,##0.00"), 11, true) + "          _________________________" + "\r\n";
            //FC:FINAL:MHO:#i182 - correct conversion
            //strLineText += "Principal         : " + modGlobalFunctions.PadStringWithSpaces(Strings.Format(FCConvert.ToDouble(GetVariableValue_2("PRINCIPAL"))) - Conversion.Val(rsData.Get_Fields("PrincipalPaid")), "$#,##0.00"), 11, true) + "          " + strCollector + "\r\n";
            //strLineText += "Interest          : " + modGlobalFunctions.PadStringWithSpaces(Strings.Format(FCConvert.ToDouble(GetVariableValue_2("INTEREST"))), "$#,##0.00"), 11, true) + "          Tax Collector" + "\r\n";
            strLineText += "Principal         : " + modGlobalFunctions.PadStringWithSpaces(Strings.Format(FCConvert.ToDouble(GetVariableValue_2("PRINCIPAL").Replace("$", "")) - Conversion.Val(rsData.Get_Fields_Decimal("PrincipalPaid")), "$#,##0.00"), 11, true) + "          " + strCollector + "\r\n";
            strLineText += "Interest          : " + modGlobalFunctions.PadStringWithSpaces(Strings.Format(FCConvert.ToDouble(GetVariableValue_2("INTEREST").Replace("$", "")), "$#,##0.00"), 11, true) + "          Tax Collector" + "\r\n";
            strLineText += "                    " + Strings.StrDup(11, "-") + "          " + GetVariableValue_2("CITYTOWNOF") + " of " + GetVariableValue_2("MUNI") + "\r\n";
            strLineText += "Total             : " + modGlobalFunctions.PadStringWithSpaces(Strings.Format(dblTotal, "$#,##0.00"), 11, true);
 
            SetupCostBreakdownAndSignatureLine = strLineText;
            return SetupCostBreakdownAndSignatureLine;
        }

        private string SetupNonRTFCostBreakdownAndSignature()
        {
            string SetupNonRTFCostBreakdownAndSignature = "";
            // this will setup the bottom part of the detail section
            string strLineText = "";
            double dblTotal;
            string strCollector = "";
            double dblDemand = 0;
            double dblCertTotal = 0;
            if (frmFreeReport.InstancePtr.chkRecommitment.CheckState == Wisej.Web.CheckState.Checked)
            {
                strCollector = GetVariableValue_2("COLLECTOR");
            }
            else
            {
                strCollector = GetVariableValue_2("COLLECTOR");
            }
            if (Conversion.Val(GetVariableValue_2("DEMAND")) != 0)
            {
                dblDemand = FCConvert.ToDouble(GetVariableValue_2("DEMAND"));
            }
            else
            {
                dblDemand = 0;
            }
            if (Conversion.Val(GetVariableValue_2("FILINGFEE")) != 0)
            {
                dblFilingFee = FCConvert.ToDouble(GetVariableValue_2("FILINGFEE"));
            }
            else
            {
                dblFilingFee = 0;
            }
            if (Conversion.Val(GetVariableValue_2("CERTTOTAL")) != 0)
            {
                dblCertTotal = FCConvert.ToDouble(GetVariableValue_2("CERTTOTAL"));
            }
            else
            {
                dblCertTotal = 0;
            }
            //FC:FINAL:MHO:#i182 - correct conversion
            //dblTotal = dblDemand + dblFilingFee + dblCertTotal + FCConvert.ToDouble(GetVariableValue_2("PRINCIPAL"))) - Conversion.Val(rsData.Get_Fields("PrincipalPaid")) + FCConvert.ToDouble(GetVariableValue_2("INTEREST")));
            dblTotal = dblDemand + dblFilingFee + dblCertTotal + FCConvert.ToDouble(GetVariableValue_2("PRINCIPAL").Replace("$", "")) - Conversion.Val(rsData.Get_Fields_Decimal("PrincipalPaid")) + FCConvert.ToDouble(GetVariableValue_2("INTEREST").Replace("$", ""));
            // Statutory Fees and Mailing Costs
            fldCosts.Text = Strings.Format(dblFilingFee + dblDemand + dblCertTotal, "$#,##0.00");
            // Principal
            //FC:FINAL:MHO:#i182 - correct conversion
            //fldPrincipal.Text = Strings.Format(FCConvert.ToDouble(GetVariableValue_2("PRINCIPAL"))) - Conversion.Val(rsData.Get_Fields("PrincipalPaid")), "$#,##0.00");
            fldPrincipal.Text = Strings.Format(FCConvert.ToDouble(GetVariableValue_2("PRINCIPAL").Replace("$", "")) - Conversion.Val(rsData.Get_Fields_Decimal("PrincipalPaid")), "$#,##0.00");
            // Interest
            //FC:FINAL:MHO:#i182 - correct conversion
            //fldInterest.Text = Strings.Format(FCConvert.ToDouble(GetVariableValue_2("INTEREST"))), "$#,##0.00");
            fldInterest.Text = Strings.Format(FCConvert.ToDouble(GetVariableValue_2("INTEREST").Replace("$", "")), "$#,##0.00");
            // Total
            fldTotal.Text = Strings.Format(dblTotal, "$#,##0.00");
            fldCollector.Text = strCollector;
            fldMuni.Text = GetVariableValue_2("CITYTOWNOF") + " of " + GetVariableValue_2("MUNI");
            return SetupNonRTFCostBreakdownAndSignature;
        }

        private string GetVariableValue_2(string strVarName)
        {
            return GetVariableValue(ref strVarName);
        }

        private string GetVariableValue(ref string strVarName)
        {
            string GetVariableValue = "";
            // this function will take a variable name and find it in the array, then get the value and return it as a string
            int intCT;
            string strValue = "";
            // this is a dummy code to show the user where the hard codes are
            if (strVarName == "CRLF")
            {
                GetVariableValue = "";
                return GetVariableValue;
            }
            //FC:FINAL:KS: #without nr.:fix Exit For inside switch/case statement
            bool endFor = false;
            for (intCT = 0; intCT <= modRhymalReporting.MAXFREEVARIABLES; intCT++)
            {
                if (Strings.UCase(strVarName) == Strings.UCase(modRhymalReporting.Statics.frfFreeReport[intCT].Tag))
                {
                    switch (modRhymalReporting.Statics.frfFreeReport[intCT].VariableType)
                    {
                        case 0:
                            {
                                // value from the static variables in the grid
                                switch (modRhymalReporting.Statics.frfFreeReport[intCT].Type)
                                {
                                    // 0 = Text, 1 = Number, 2 = Date, 3 = Do not ask for default (comes from DB), 4 = Formatted Year
                                    case 0:
                                        {
                                            strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
                                            break;
                                        }
                                    case 1:
                                        {
                                            strValue = Strings.Format(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1), "#,##0.00");
                                            break;
                                        }
                                    case 2:
                                        {
                                            strValue = Strings.Format(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1), "MMMM dd, yyyy");
                                            break;
                                        }
                                    case 3:
                                        {
                                            strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
                                            break;
                                        }
                                    case 4:
                                        {
                                            strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
                                            break;
                                        }
                                }
                                //end switch
                                //FC:FINAL:KS: #without nr.:fix Exit For inside switch/case statement
                                endFor = true;
                                break;
                                //break;
                            }
                        case 1:
                            {
                                // value from the dynamic variables in the database
                                strValue = FCConvert.ToString(rsData.Get_Fields(modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName));
                                if (Strings.Trim(strValue) == "")
                                {
                                    if (Strings.UCase(modRhymalReporting.Statics.frfFreeReport[intCT].Tag) != "LESSPAYMENTS")
                                    {
                                        // maybe put a blank line there
                                        strValue = "__________";
                                    }
                                }
                                //FC:FINAL:KS: #without nr.:fix Exit For inside switch/case statement
                                endFor = true;
                                //break;
                                break;
                            }
                        case 2:
                            {
                                // questions at the bottom of the grid
                                strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
                                //FC:FINAL:KS: #without nr.:fix Exit For inside switch/case statement
                                endFor = true;
                                //break;
                                break;
                            }
                        case 3:
                            {
                                // calculated values that are stored in the DatabaseFieldName field
                                strValue = modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName;
                                if (Strings.Trim(strValue) == "")
                                {
                                    if (Strings.UCase(modRhymalReporting.Statics.frfFreeReport[intCT].Tag) != "LESSPAYMENTS" && Strings.UCase(modRhymalReporting.Statics.frfFreeReport[intCT].Tag) != "ADDRESS1" && Strings.UCase(modRhymalReporting.Statics.frfFreeReport[intCT].Tag) != "ADDRESS2" && Strings.UCase(modRhymalReporting.Statics.frfFreeReport[intCT].Tag) != "ADDRESS3")
                                    {
                                        // maybe put a blank line there
                                        strValue = "__________";
                                    }
                                }
                                //FC:FINAL:KS: #without nr.:fix Exit For inside switch/case statement
                                endFor = true;
                                //break;
                                break;
                            }
                    }
                    //end switch
                    //FC:FINAL:KS: #without nr.:fix Exit For inside switch/case statement
                    if (endFor)
                    {
                        break;
                    }
                }
            }
            GetVariableValue = strValue;
            return GetVariableValue;
        }

        private void CalculateVariableTotals()
        {
            try
            {   // On Error GoTo ERROR_HANDLER
                //fecherFoundation.Information.Err(ex).Clear();
                // this will calculate all of the fields that need it and store
                // the value in the DatabaseFieldName string in the frfFreeReport
                // struct that these fields include principal, interest, costs,
                // total due and certified mail fee

                clsDRWrapper rsRK = new clsDRWrapper();
                clsDRWrapper rsTemp = new clsDRWrapper();
                DateTime dtBillDate;
                clsDRWrapper rsSetCert;
                double dblTotalDue = 0;
                double dblInt;
                double dblPrin;
                double dblPayments;
                double dblDemand;
                double dblCertMailFee;
                // Dim lngMortHolder               As Long
                string strAddressBar = "";
                string strCommitmentDate = "";
                string strLessPaymentsLine = "";
                string strLocation = "";
                string strTemp1 = "";
                string strTemp2 = "";
                string strTemp3 = "";
                string strTemp4 = "";
                string strTemp5 = "";
                string strOwnerName = "";
                string strMapLot = "";
                string strBookPage = "";
                string strBP = "";
                bool boolSameName = false;
                string strCommissionExpirationDate = "";

                // for CMFNumbers
                string strCMFName = "";
                int lngCMFBillKey = 0;
                int lngCMFMHNumber = 0;
                bool boolCMFNewOwner;
                bool boolPrintCMF; // true if a CMF should be printed for that person
                string strCMFAddr1;
                string strCMFAddr2;
                string strCMFAddr3;
                string strCMFAddr4;

                // MAL@20080514: Tracker Reference: 13621
                string strRSName1 = "";
                string strRSName2 = "";
                string strRSAddr1 = "";
                string strRSAddr2 = "";
                string strRSAddr3 = "";
                string strRSAddr4 = "";
                // vbPorter upgrade warning: lngAddr1Width As int	OnWriteFCConvert.ToDouble(
                int lngAddr1Width = 0;
                // vbPorter upgrade warning: lngAddr2Left As int	OnWriteFCConvert.ToDouble(
                int lngAddr2Left;
                // vbPorter upgrade warning: lngAddr2Width As int	OnWriteFCConvert.ToDouble(
                int lngAddr2Width = 0;

                // vbPorter upgrade warning: AbateArray As double	OnWrite(short, bool, double)
                double[] AbateArray = new double[4 + 1];

                boolRepeatAccount = false;

                // trocl-1335 09.11.17 kjr  4.23.18 CODE FREEZE
                bool boolNewAddress = false;
                string strPrevSecOwnerName = "";
                string strPrevAddress1 = "";
                string strPrevAddress2 = "";
                string strPrevAddress3 = "";

            TryAgain:;
                strCMFAddr1 = "";
                strCMFAddr2 = "";
                strCMFAddr3 = "";
                strCMFAddr4 = "";
                AbateArray[0] = 0;
                AbateArray[1] = 0;
                AbateArray[2] = 0;
                AbateArray[3] = 0;
                // MAL@20080812: Reset values for each account
                // Tracker Reference: 14980
                dblInt = 0;
                dblPrin = 0;
                dblPayments = 0;
                dblDemand = 0;
                dblCertMailFee = 0;

                // If Me.pageNumber = 422 Then Stop
                var own2 = FCConvert.ToString(rsRE.Get_Fields("Own2FullName"));

                var rsData_ID = rsData.Get_Fields("ID");

                if (!boolCopy)
                {
                    // this will calculate the principal, interest and total due
                    rsRK.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + rsData.Get_Fields("RateKey"), modExtraModules.strCLDatabase);
                    var filingDate = fecherFoundation.DateAndTime.DateValue(GetVariableValue_2("FILINGDATE"));

                    if (FCConvert.ToInt32(rsData.Get_Fields("LienRecordNumber")) != 0)
                    {
                        dblTotalDue = modCLCalculations.CalculateAccountCLLien(rsData, filingDate, ref dblInt);
                    }
                    else
                    {
                        rsTemp = GetPaymentRecords();

                        double dblAdded = 0;

                        while (!rsTemp.EndOfFile())
                        {
                            string period = FCConvert.ToString(rsTemp.Get_Fields("Period"));
                            double principal = Conversion.Val(rsTemp.Get_Fields("Principal"));
                            SetAbateArray(period, principal, AbateArray, rsRK);
                            rsTemp.MoveNext();
                        }
                        dblTotalDue = modCLCalculations.CalculateAccountCL(ref rsData, rsData.Get_Fields("Account"), filingDate, ref dblInt, ref AbateArray[0], ref AbateArray[1], ref AbateArray[2], ref AbateArray[3], true);
                    }

                    strCommissionExpirationDate = GetVariableValue_2("COMMISSIONEXPIRATIONDATE");

                    if (modGlobal.Round(dblTotalDue, 2) == 0)
                    {
                        lngBalanceZero += 1;
                        strBalanceZero += rsData.Get_Fields("BillKey") + ", ";
                        rsData.MoveNext();
                        boolRepeatAccount = true;
                        if (rsData.EndOfFile()) return;
                        goto TryAgain;
                    }

                    boolRepeatAccount = false;

                    boolNewOwner = false;

                    strDateCheck = GetVariableValue_2("FILINGDATE");
                   

                    if (dblTotalDue > 0)
                    {
                        // these are the account that will be demanded
                        lngBalancePos += 1;
                        //if (!String.IsNullOrWhiteSpace(strAcctList) && (" " + strAcctList).Right(1) != ",")
                        //{
                        //    strAcctList += ",";
                        //}
                        strAcctList += rsData.Get_Fields("Account") + ",";
                        strBalancePos += rsData_ID + ", ";
                    }
                    else
                    {
                        if (dblTotalDue < 0)
                        {
                            lngBalanceNeg += 1;
                            strBalanceNeg += rsData_ID + ", ";
                        }
                    }

                    // this will find out how many mortgage holders this account has and how many copies to print
                    NumberOfMortgageHolders = CountMortgageHolders();
                    lngCopies = NumberOfMortgageHolders + 1;


                    // this will find out the certified mail fee
                    dblCertMailFee = DetermineCertifiedMailFee();

                    // this will find out how many interested parties this account has and how many copies to print
                    if (blnIP)
                    {
                        lngIntParties = modCLCalculations.CalculateInterestedParties(rsData.Get_Fields("Account"), rsIntParty);
                        lngCopies += lngIntParties;
                    }
                    else
                    {
                        lngIntParties = 0;
                        lngCopies = lngCopies;
                    }

                    // this will find out the certified mail fee
                    if (boolPayCert)
                    {
                        if (blnPayIPCert)
                        {
                            dblCertMailFee += (lngIntParties * FCConvert.ToDouble(GetVariableValue_2("MAILFEE")));
                        }
                        else
                        {
                            dblCertMailFee = dblCertMailFee;
                        }
                    }
                    else
                    {
                        if (blnPayIPCert)
                        {
                            dblCertMailFee += (lngIntParties * FCConvert.ToDouble(GetVariableValue_2("MAILFEE")));
                        }
                        else
                        {
                            dblCertMailFee = dblCertMailFee;
                        }
                    }

                    if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Name2"))) != "")
                    {
                        strOwnerName = rsData.Get_Fields("Name1") + " and " + rsData.Get_Fields("Name2");
                    }
                    else
                    {
                        strOwnerName = FCConvert.ToString(rsData.Get_Fields("Name1"));
                    }

                    rsRE.FindFirstRecord("RSAccount", rsData.Get_Fields("Account"));

                    if (!rsRK.EndOfFile())
                    {
                        dtBillDate = (DateTime)rsRK.Get_Fields("CommitmentDate");
                    }
                    else
                    {
                        if (Information.IsDate(rsData.Get_Fields("TransferFromBillingDateFirst")))
                        {
                            dtBillDate = (DateTime)rsData.Get_Fields("TransferFromBillingDateFirst");
                        }
                        else if (Information.IsDate(rsData.Get_Fields("TransferFromBillingDateLast")))
                        {
                            dtBillDate = (DateTime)rsData.Get_Fields("TransferFromBillingDateLast");
                        }
                        else
                        {
                            dtBillDate = DateTime.Today;
                        }
                    }
                    // check to see if one is going to be sent to the next owner
                    if (intNewOwnerChoice != 0 && modCLCalculations.NewOwner(rsData.Get_Fields("Name1"), rsData.Get_Fields("Account"), dtBillDate,  ref boolREMatch,  ref boolSameName))
                    {
                        // if there has been a new owner, then make a copy for the new owner as well
                        if (!boolNewOwner)
                        {
                            if (boolPayNewOwner)
                            { // charge for the new owner's copy
                                dblCertMailFee += FCConvert.ToDouble(GetVariableValue_2("MAILFEE"));
                            }
                            lngCopies += 1;
                            boolNewOwner = true;
                        }
                    }

                    // lblAccount.Caption = PadToString(rsData.Fields("Account"), 6)

                    dblPrin = rsData.Get_Fields_Double("TaxDue1") + rsData.Get_Fields_Double("TaxDue2") + rsData.Get_Fields_Double("TaxDue3") + rsData.Get_Fields_Double("TaxDue4");
                    dblPayments = rsData.Get_Fields_Double("PrincipalPaid");
                    dblInt = (rsData.Get_Fields_Double("InterestCharged") * -1) - rsData.Get_Fields_Double("InterestPaid") + dblInt;

                    // Less Payments Line
                    if (modGlobal.Round(dblPayments, 2) != 0)
                    { // only show this when there are payments
                        if (dblPayments < 0)
                        { // if the principal paid is a negative number then some adjustment has happened (supplemental) this should not happen later in the code life
                            rsPayments.OpenRecordset("SELECT ID FROM PaymentRec WHERE BillKey = " + rsData_ID + " AND (Code = 'A' OR Code = 'R' OR Code = 'S' OR Code = 'D')");
                            if (rsPayments.EndOfFile())
                            {
                                strLessPaymentsLine = "plus adjustment of " + Strings.Format(Math.Abs(dblPayments), "#,##0.00") + " for the net sum of " + Strings.Format(dblPrin - dblPayments, "#,##0.00") + ",";
                            }
                            else
                            {
                                strLessPaymentsLine = "plus adjustment of " + Strings.Format(Math.Abs(dblPayments), "#,##0.00") + " for the net sum of " + Strings.Format(dblPrin - dblPayments, "#,##0.00") + ",";
                            }
                        }
                        else
                        { // principal paid is greater then zero
                            rsPayments.OpenRecordset("SELECT ID FROM PaymentRec WHERE BillKey = " + rsData_ID + " AND (Code = 'A' OR Code = 'R' OR Code = 'S' OR Code = 'D')");
                            if (rsPayments.EndOfFile())
                            {
                                strLessPaymentsLine = "less payment and adjustment of " + Strings.Format(dblPayments, "#,##0.00") + " for the net sum of " + Strings.Format(dblPrin - dblPayments, "#,##0.00") + ",";
                            }
                            else
                            {
                                strLessPaymentsLine = "less payment of " + Strings.Format(dblPayments, "#,##0.00") + " for the net sum of " + Strings.Format(dblPrin - dblPayments, "#,##0.00") + ",";
                            }
                        }
                    }
                    else
                    {
                        strLessPaymentsLine = "";
                    }

                    // BookPage
                    strBookPage = "";
                    if (FCConvert.ToBoolean(rsData.Get_Fields("ShowRef1")))
                    {
                        if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Ref1"))) == "")
                        {
                            strBookPage = "";
                        }
                        else
                        {
                            strBookPage = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Ref1")));
                        }
                    }
                    else
                    {
                        if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("BookPage"))) == "")
                        {
                            strBookPage = "";
                        }
                        else
                        {
                            strBookPage = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("BookPage")));
                        }
                    }
                    strBP = strBookPage;

                    if (boolREMatch)
                    {
                        if (FCConvert.ToInt32(rsData.Get_Fields("streetnumber")) != 0 && fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("streetnumber"))) != "")
                        {
                            if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("StreetName"))) != "")
                            {
                                strLocation = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("streetnumber"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("StreetName")));
                            }
                            else
                            {
                                strLocation = "";
                            }
                        }
                        else
                        {
                            if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("StreetName"))) != "")
                            {
                                strLocation = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("StreetName")));
                            }
                            else
                            {
                                strLocation = "";
                            }
                        }
                        strMapLot = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("MapLot")));
                    }
                    else
                    {
                        strLocation = "";
                        strMapLot = "";
                    }

                    if (fecherFoundation.Strings.Trim(strMapLot) == "")
                    {
                        strMapLot = "________________";
                    }
                    if (fecherFoundation.Strings.Trim(strLocation) == "")
                    {
                        strLocation = "________________";
                    }
                    if (fecherFoundation.Strings.Trim(strBookPage) == "")
                    {
                        strBookPage = "________________";
                    }

                    // Commitment Date
                    rsRate.FindFirstRecord("ID", rsData.Get_Fields("RateKey"));
                    if (!rsRate.NoMatch)
                    {
                        if (rsRate.Get_Fields_DateTime("CommitmentDate").ToOADate() != 0)
                        {
                            strCommitmentDate = Strings.Format(rsRate.Get_Fields_DateTime("CommitmentDate"), "MMMM d, yyyy");
                        }
                        else
                        {
                            strCommitmentDate = "__________";
                        }
                    }
                    else
                    {
                        strCommitmentDate = "__________";
                    }

                    // Address Bar
                    clsAddress tAddr;

                    switch (intBilledOwnerAddr)
                    {
                        case (int)CLBillingAddressType.AddressAtBilling:
                            tAddr = tAddrCtrlr.GetOwnerAndAddressAtBilling(rsData);
                            break;
                        case (int)CLBillingAddressType.CareOfCurrentOwner:
                            tAddr = tAddrCtrlr.GetOwnerAtBillingCareOfCurrentOwner(rsData);
                            break;
                        case (int)CLBillingAddressType.LastRecordedAddress:
                            tAddr = tAddrCtrlr.GetBilledOwnerAtLastAddress(rsData);
                            break;
                        default:
                            tAddr = new clsAddress();
                            break;
                    }

                    strAddressBar = "    " + tAddr.Addressee;
                    strAddressBar += "\n    " + tAddr.Get_Address(1).Trim();
                    strAddressBar += "\n    " + tAddr.Get_Address(2).Trim();
                    strAddressBar += "\n    " + tAddr.Get_Address(3).Trim();
                    strAddressBar += "\n    " + tAddr.Get_Address(4).Trim();

                    strCMFAddr1 = tAddr.Get_Address(1).Trim();
                    strCMFAddr2 = tAddr.Get_Address(2).Trim();
                    strCMFAddr3 = tAddr.Get_Address(3).Trim();
                    strCMFAddr4 = tAddr.Get_Address(4).Trim();

                    if (Conversion.Val(rsData.Get_Fields("DemandFees")) > 0)
                    {
                        dblDemand = rsData.Get_Fields_Double("DemandFees") - rsData.Get_Fields_Double("DemandFeesPaid");
                    }
                    else
                    {
                        dblDemand = 0;
                    }

                    // info for CMFNumbers table
                    strCMFName = strOwnerName;
                    lngCMFMHNumber = 0;

                    lngCMFBillKey = FCConvert.ToInt32(rsData_ID);

                    // Billing year (1)
                    modRhymalReporting.Statics.frfFreeReport[1].DatabaseFieldName = FCConvert.ToString(intYear);
                    // MapLot
                    modRhymalReporting.Statics.frfFreeReport[2].DatabaseFieldName = strMapLot;
                    // BookPage
                    modRhymalReporting.Statics.frfFreeReport[3].DatabaseFieldName = strBookPage;
                    // Commission Expiration Date (10)
                    modRhymalReporting.Statics.frfFreeReport[10].DatabaseFieldName = strCommissionExpirationDate;
                    // Address Bar (13)
                    modRhymalReporting.Statics.frfFreeReport[13].DatabaseFieldName = strAddressBar;
                    // principal (18)
                    modRhymalReporting.Statics.frfFreeReport[18].DatabaseFieldName = Strings.Format(dblPrin, "$#,##0.00");
                    // interest (19)
                    modRhymalReporting.Statics.frfFreeReport[19].DatabaseFieldName = Strings.Format(dblInt, "$#,##0.00");
                    // less payments (20)
                    modRhymalReporting.Statics.frfFreeReport[20].DatabaseFieldName = strLessPaymentsLine;
                    // city/town (21)
                    modRhymalReporting.Statics.frfFreeReport[21].DatabaseFieldName = modGlobalConstants.Statics.gstrCityTown;
                    // owner name (22)
                    modRhymalReporting.Statics.frfFreeReport[22].DatabaseFieldName = strOwnerName;
                    // add1
                    modRhymalReporting.Statics.frfFreeReport[23].DatabaseFieldName = strCMFAddr1;
                    // add2
                    modRhymalReporting.Statics.frfFreeReport[24].DatabaseFieldName = strCMFAddr2;
                    // add3
                    modRhymalReporting.Statics.frfFreeReport[25].DatabaseFieldName = strCMFAddr3;
                    // total due (26)
                    modRhymalReporting.Statics.frfFreeReport[26].DatabaseFieldName = Strings.Format(dblTotalDue + dblCertMailFee + dblDemand, "#,##0.00");
                    // location (27)
                    modRhymalReporting.Statics.frfFreeReport[27].DatabaseFieldName = strLocation;
                    // Demand Fees (33)   'demand and CMF needs to be calculated before the signature line is used
                    modRhymalReporting.Statics.frfFreeReport[33].DatabaseFieldName = Strings.Format(dblDemand, "#,##0.00");
                    // total certified mail fee (29)
                    modRhymalReporting.Statics.frfFreeReport[29].DatabaseFieldName = Strings.Format(dblCertMailFee, "#,##0.00");
                    // cost and signature line (28)
                    modRhymalReporting.Statics.frfFreeReport[28].DatabaseFieldName = SetupCostBreakdownAndSignatureLine();
                    // OldCollector (31)
                    modRhymalReporting.Statics.frfFreeReport[31].DatabaseFieldName = frmFreeReport.InstancePtr.vsRecommitment.TextMatrix(0, 1);
                    // RecommitmentDate (32)
                    modRhymalReporting.Statics.frfFreeReport[32].DatabaseFieldName = frmFreeReport.InstancePtr.vsRecommitment.TextMatrix(1, 1);
                    // Commitment Date (37)   'MAL@20080527: Corrected to include change from 36 for Interested Party information ; Tracker Reference: 13753
                    modRhymalReporting.Statics.frfFreeReport[37].DatabaseFieldName = strCommitmentDate;

                    Array.Resize(ref modGlobal.Statics.arrDemand, lngArrayIndex + 1); // this will make sure that there are enough elements to use
                    modGlobal.Statics.arrDemand[lngArrayIndex].Used = true;
                    modGlobal.Statics.arrDemand[lngArrayIndex].Processed = false;
                    modGlobal.Statics.arrDemand[lngArrayIndex].Account = FCConvert.ToInt32(rsData.Get_Fields("Account"));
                    modGlobal.Statics.arrDemand[lngArrayIndex].Name = FCConvert.ToString(rsData.Get_Fields("Name1"));
                    modGlobal.Statics.arrDemand[lngArrayIndex].Fee = dblFilingFee;
                    modGlobal.Statics.arrDemand[lngArrayIndex].CertifiedMailFee = dblCertMailFee;

                    lngArrayIndex += 1;

                    rsData.Edit();
                    rsData.Set_Fields("Copies", lngCopies);
                    rsData.Update();
                }
                else
                {
                    // this is the difference when it is a copy
                    var own1 = FCConvert.ToString(rsRE.Get_Fields("Own1FullName"));

                    if (blnIsMortCopy)
                    {
                        if (boolPayMortCert)
                         // if the user is getting charged for certified mail, then save their info to print on Cert Mail Forms
                            boolPrintCMF = boolPayMortCert;

                        // address bar
                        if (!rsMort.EndOfFile())
                        {
                            
                            (strCMFName, lngCMFMHNumber, lngCMFBillKey, strCMFAddr1, strCMFAddr2, strCMFAddr3, strCMFAddr4) = SetCMFParty(rsMort, true);
                            
                            

                            // MAL@20080514: Evaluate string length before adding to address bar
                            // Tracker Reference: 13621
                            strRSName1 = own1;
                            strRSName2 = own2;
                            if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1Address1"))) == "" && fecherFoundation.Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1Address2"))) == "" && fecherFoundation.Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1City"))) == "")
                            {
                                strRSAddr1 = FCConvert.ToString(rsData.Get_Fields("Address1"));
                                strRSAddr2 = FCConvert.ToString(rsData.Get_Fields("Address2"));
                                strRSAddr3 = FCConvert.ToString(rsData.Get_Fields("MailingAddress3")); // kk05082017 trocls-109  Include 3rd Address line from CP record (Corey added to BillingMaster)
                                strRSAddr4 = FCConvert.ToString(rsData.Get_Fields("Address3"));
                            }
                            else if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1Address1"))) != "")
                            {
                                strRSAddr1 = FCConvert.ToString(rsRE.Get_Fields("Own1Address1"));
                                strRSAddr2 = FCConvert.ToString(rsRE.Get_Fields("Own1Address2"));
                                strRSAddr3 = FCConvert.ToString(rsRE.Get_Fields("Own1Address3"));
                                strRSAddr4 = rsRE.Get_Fields("Own1City") + ", " + rsRE.Get_Fields("Own1State") + " " + rsRE.Get_Fields("Own1Zip");
                            }
                            else
                            {
                                strRSAddr1 = FCConvert.ToString(rsRE.Get_Fields("Own1Address2"));
                                strRSAddr2 = FCConvert.ToString(rsRE.Get_Fields("Own1Address3"));
                                strRSAddr3 = rsRE.Get_Fields("Own1City") + ", " + rsRE.Get_Fields("Own1State") + " " + rsRE.Get_Fields("Own1Zip");
                                strRSAddr4 = "";
                            }

                            // kk06092016 trocl-1308  Need to make these vary with the width of the side margins
                            // NOTE: If we change this to a proportional font, it might be better to break out the addresses into 2 separate groups of fields or rtf's
                            lngAddr1Width = FCConvert.ToInt32((this.PrintWidth - (4 * lngCharWidth)) / 2); // Split the address bar into 2, adjust for the extra 4 spaces added on the left
                            lngAddr2Left = FCConvert.ToInt32(lngAddr1Width + (11 * lngCharWidth)); // Line up the tax payer address after "Tax Payer: "
                            lngAddr2Width = FCConvert.ToInt32(lngAddr1Width - (11 * lngCharWidth));

                            if (fecherFoundation.Strings.Trim(strRSName1).Length * lngCharWidth > lngAddr2Width)
                            { // kk05312017 trocls-109  Changed from lngAddr1Width, this is the Tax Payer Name on the copy
                                if (fecherFoundation.Strings.Trim(strRSName2) == "")
                                {
                                    strRSName2 = fecherFoundation.Strings.Trim(Strings.Mid(strRSName1, FCConvert.ToInt32(lngAddr2Width / lngCharWidth + 1), FCConvert.ToInt32(strRSName1.Length - lngAddr2Width / lngCharWidth + 1)));
                                    strRSName1 = fecherFoundation.Strings.Trim(Strings.Left(strRSName1, FCConvert.ToInt32(lngAddr2Width / lngCharWidth)));
                                }
                                else
                                {
                                    strRSName1 = fecherFoundation.Strings.Trim(Strings.Left(strRSName1, FCConvert.ToInt32(lngAddr2Width / lngCharWidth)));
                                }
                            }

                            // Remove blank lines
                            if (fecherFoundation.Strings.Trim(strRSAddr3) == "")
                            {
                                strRSAddr3 = strRSAddr4;
                                strRSAddr4 = "";
                            }
                            if (fecherFoundation.Strings.Trim(strRSAddr2) == "")
                            {
                                strRSAddr2 = strRSAddr3;
                                strRSAddr3 = strRSAddr4;
                                strRSAddr4 = "";
                            }
                            if (fecherFoundation.Strings.Trim(strRSAddr1) == "")
                            {
                                strRSAddr1 = strRSAddr2;
                                strRSAddr2 = strRSAddr3;
                                strRSAddr3 = strRSAddr4;
                                strRSAddr4 = "";
                            }
                            if (fecherFoundation.Strings.Trim(strRSName2) == "")
                            {
                                strRSName2 = strRSAddr1;
                                strRSAddr1 = strRSAddr2;
                                strRSAddr2 = strRSAddr3;
                                strRSAddr3 = strRSAddr4;
                                strRSAddr4 = "";
                            }

                            // Evaluate Line Lengths
                            if (fecherFoundation.Strings.Trim(strRSName2).Length * lngCharWidth > lngAddr2Width)
                            {
                                if (strRSAddr1 == "" || strRSAddr2 == "" || strRSAddr3 == "" || strRSAddr4 == "")
                                {
                                    // Line 2 is too long and there's an empty line, push everything down and wrap line 2 (strRSName2)
                                    strRSAddr4 = strRSAddr3;
                                    strRSAddr3 = strRSAddr2;
                                    strRSAddr2 = strRSAddr1;
                                    strRSAddr1 = fecherFoundation.Strings.Trim(Strings.Mid(strRSName2, FCConvert.ToInt32(lngAddr2Width / lngCharWidth + 1), FCConvert.ToInt32(strRSName2.Length - lngAddr2Width / lngCharWidth + 1)));
                                    strRSName2 = fecherFoundation.Strings.Trim(Strings.Left(strRSName2, FCConvert.ToInt32(lngAddr2Width / lngCharWidth)));
                                }
                                else
                                {
                                    // Line 2 is too long but there's no empty line, truncate line 2
                                    strRSName2 = fecherFoundation.Strings.Trim(Strings.Left(strRSName2, FCConvert.ToInt32(lngAddr2Width / lngCharWidth)));
                                }
                            }

                            if (fecherFoundation.Strings.Trim(strRSAddr1).Length * lngCharWidth > lngAddr2Width)
                            {
                                if (strRSAddr2 == "" || strRSAddr3 == "" || strRSAddr4 == "")
                                {
                                    // Line 3 is too long and there's an empty line, push everything down and wrap line 3 (strRSAddr1)
                                    strRSAddr4 = strRSAddr3;
                                    strRSAddr3 = strRSAddr2;
                                    strRSAddr2 = fecherFoundation.Strings.Trim(Strings.Mid(strRSAddr1, FCConvert.ToInt32(lngAddr2Width / lngCharWidth + 1), FCConvert.ToInt32(strRSAddr1.Length - lngAddr2Width / lngCharWidth + 1)));
                                    strRSAddr1 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr1, FCConvert.ToInt32(lngAddr2Width / lngCharWidth)));
                                }
                                else
                                {
                                    // Line 3 is too long but there's no empty line, truncate line 3
                                    strRSAddr1 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr1, FCConvert.ToInt32(lngAddr2Width / lngCharWidth)));
                                }
                            }

                            if (fecherFoundation.Strings.Trim(strRSAddr2).Length * lngCharWidth > lngAddr2Width)
                            {
                                if (strRSAddr3 == "" || strRSAddr4 == "")
                                {
                                    // Line 4 is too long and there's an empty line, push everything down and wrap line 4 (strRSAddr2)
                                    strRSAddr4 = strRSAddr3;
                                    strRSAddr3 = fecherFoundation.Strings.Trim(Strings.Mid(strRSAddr2, FCConvert.ToInt32(lngAddr2Width / lngCharWidth + 1), FCConvert.ToInt32(strRSAddr2.Length - lngAddr2Width / lngCharWidth + 1)));
                                    strRSAddr2 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr2, FCConvert.ToInt32(lngAddr2Width / lngCharWidth)));
                                }
                                else
                                {
                                    // Line 4 is too long but there's no empty line, truncate line 4
                                    strRSAddr2 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr2, FCConvert.ToInt32(lngAddr2Width / lngCharWidth)));
                                }
                            }

                            if (fecherFoundation.Strings.Trim(strRSAddr3).Length * lngCharWidth > lngAddr2Width)
                            {
                                if (strRSAddr4 == "")
                                {
                                    // Line 4 is too long and there's an empty line, push everything down and wrap line 4 (strRSAddr2)
                                    strRSAddr4 = fecherFoundation.Strings.Trim(Strings.Mid(strRSAddr3, FCConvert.ToInt32(lngAddr2Width / lngCharWidth + 1), FCConvert.ToInt32(strRSAddr3.Length - lngAddr2Width / lngCharWidth + 1)));
                                    strRSAddr3 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr3, FCConvert.ToInt32(lngAddr2Width / lngCharWidth)));
                                }
                                else
                                {
                                    // Line 5 is too long, truncate line 5 (strRSAddr3)
                                    strRSAddr3 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr3, FCConvert.ToInt32(lngAddr2Width / lngCharWidth)));
                                }
                            }

                            if (fecherFoundation.Strings.Trim(strRSAddr4).Length * lngCharWidth > lngAddr2Width)
                            {
                                // Line 5 is too long, truncate line 5 (strRSAddr3)
                                strRSAddr4 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr4, FCConvert.ToInt32(lngAddr2Width / lngCharWidth)));
                            }

                            strAddressBar = "    " + FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces(strCMFName, FCConvert.ToInt16(lngAddr1Width / lngCharWidth - 2), false)) + FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces("Tax Payer:", 11, false)) + strRSName1 + "\r\n" + "    " + FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces(strCMFAddr1, FCConvert.ToInt16(lngAddr1Width / lngCharWidth + 9), false)) + strRSName2 + "\r\n" + "    " + FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces(strCMFAddr2, FCConvert.ToInt16(lngAddr1Width / lngCharWidth + 9), false)) + strRSAddr1;
                            if (fecherFoundation.Strings.Trim(strCMFAddr3) != "" || fecherFoundation.Strings.Trim(strRSAddr2) != "")
                            {
                                strAddressBar += "\r\n" + "    " + FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces(strCMFAddr3, FCConvert.ToInt16(lngAddr1Width / lngCharWidth + 9), false)) + strRSAddr2;
                            }
                            if (fecherFoundation.Strings.Trim(strCMFAddr4) != "" || fecherFoundation.Strings.Trim(strRSAddr3) != "")
                            {
                                strAddressBar += "\r\n" + "    " + FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces(strCMFAddr4, FCConvert.ToInt16(lngAddr1Width / lngCharWidth + 9), false)) + strRSAddr3;
                            }
                            if (fecherFoundation.Strings.Trim(strRSAddr4) != "")
                            {
                                strAddressBar += "\r\n" + "    " + FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces("", FCConvert.ToInt16(lngAddr1Width / lngCharWidth + 9), false)) + strRSAddr4;
                            }

                            // set name and address bar for a mortgage holder
                            modRhymalReporting.Statics.frfFreeReport[13].DatabaseFieldName = strAddressBar;
                            rsMort.MoveNext();
                        }
                        else
                        {
                            strAddressBar = "    " + rsData.Get_Fields("Name1");
                            if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Address1"))) != "")
                            {
                                strAddressBar += "\r\n" + "    " + rsData.Get_Fields("Address1");
                            }
                            strAddressBar += "\r\n" + "    " + GetVariableValue_2("MUNI") + ", " + GetVariableValue_2("STATE") + " " + GetVariableValue_2("ZIP");

                            // set name and address bar for a mortgage holder
                            modRhymalReporting.Statics.frfFreeReport[13].DatabaseFieldName = strAddressBar;
                        }

                    }
                    else if (blnIsIPCopy)
                    {
                        //if (blnPayIPCert)
                         // if the user is getting charged for certified mail, then save their info to print on Cert Mail Forms
                            boolPrintCMF = blnPayIPCert;

                        // address bar
                        if (!rsIntParty.EndOfFile())
                        {
                            (strCMFName, lngCMFMHNumber, lngCMFBillKey, strCMFAddr1, strCMFAddr2, strCMFAddr3, strCMFAddr4) = SetCMFParty(rsIntParty);

                            // MAL@20080514: Evaluate string length before adding to address bar
                            // Tracker Reference: 13621
                            strRSName1 = own1;
                            strRSName2 = own2;
                            if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1Address1"))) == "" && fecherFoundation.Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1Address2"))) == "" && fecherFoundation.Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1City"))) == "")
                            {
                                strRSAddr1 = FCConvert.ToString(rsData.Get_Fields("Address1"));
                                strRSAddr2 = FCConvert.ToString(rsData.Get_Fields("Address2"));
                                strRSAddr3 = FCConvert.ToString(rsData.Get_Fields("Address3"));
                            }
                            else if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1Address1"))) != "")
                            {
                                strRSAddr1 = FCConvert.ToString(rsRE.Get_Fields("Own1Address1"));
                                if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1Address2"))) != "")
                                {
                                    strRSAddr2 = FCConvert.ToString(rsRE.Get_Fields("Own1Address2"));
                                    strRSAddr3 = rsRE.Get_Fields("Own1City") + ", " + rsRE.Get_Fields("Own1State") + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1Zip")));
                                }
                                else
                                {
                                    strRSAddr2 = rsRE.Get_Fields("Own1City") + ", " + rsRE.Get_Fields("Own1State") + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1Zip")));
                                    strRSAddr3 = "";
                                }
                            }
                            else if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1Address2"))) != "")
                            {
                                strRSAddr1 = FCConvert.ToString(rsRE.Get_Fields("Own1Address2"));
                                strRSAddr2 = rsRE.Get_Fields("Own1City") + ", " + rsRE.Get_Fields("Own1State") + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1Zip")));
                                strRSAddr3 = "";
                            }
                            else
                            {
                                strRSAddr1 = rsRE.Get_Fields("Own1City") + ", " + rsRE.Get_Fields("Own1State") + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1Zip")));
                                strRSAddr2 = "";
                                strRSAddr3 = "";
                            }

                            // kk06092016 trocl-1308  Need to make these vary with the width of the side margins
                            // NOTE: If we change this to a proportional font, it might be better to break out the addresses into 2 separate groups of fields or rtf's
                            lngAddr1Width = FCConvert.ToInt32((this.PrintWidth - 4 * lngCharWidth) / 2); // Split the address bar into 2, adjust for the extra 4 spaces added on the left
                            lngAddr2Left = FCConvert.ToInt32(lngAddr1Width + 11 * lngCharWidth); // Line up the tax payer address after "Tax Payer: "
                            lngAddr2Width = FCConvert.ToInt32(lngAddr1Width - 11 * lngCharWidth); // 

                            if (fecherFoundation.Strings.Trim(strRSName1).Length * lngCharWidth > lngAddr2Width)
                            {
                                if (fecherFoundation.Strings.Trim(strRSName2) == "")
                                {
                                    // Line 1 is too long, wrap to line 2
                                    strRSName2 = fecherFoundation.Strings.Trim(Strings.Mid(strRSName1, FCConvert.ToInt32(lngAddr2Width / lngCharWidth + 1), FCConvert.ToInt32(strRSName1.Length - lngAddr2Width / lngCharWidth + 1)));
                                    strRSName1 = fecherFoundation.Strings.Trim(Strings.Left(strRSName1, FCConvert.ToInt32(lngAddr2Width / lngCharWidth)));
                                }
                                else
                                {
                                    // Line 1 is too long but line 2 is not empty, truncate line 1
                                    strRSName1 = fecherFoundation.Strings.Trim(Strings.Left(strRSName1, FCConvert.ToInt32(lngAddr2Width / lngCharWidth)));
                                }
                            }

                            // Remove blank lines
                            if (fecherFoundation.Strings.Trim(strRSAddr3) == "")
                            {
                                strRSAddr3 = strRSAddr4;
                                strRSAddr4 = "";
                            }
                            if (fecherFoundation.Strings.Trim(strRSAddr2) == "")
                            {
                                strRSAddr2 = strRSAddr3;
                                strRSAddr3 = strRSAddr4;
                                strRSAddr4 = "";
                            }
                            if (fecherFoundation.Strings.Trim(strRSAddr1) == "")
                            {
                                strRSAddr1 = strRSAddr2;
                                strRSAddr2 = strRSAddr3;
                                strRSAddr3 = strRSAddr4;
                                strRSAddr4 = "";
                            }
                            if (fecherFoundation.Strings.Trim(strRSName2) == "")
                            {
                                strRSName2 = strRSAddr1;
                                strRSAddr1 = strRSAddr2;
                                strRSAddr2 = strRSAddr3;
                                strRSAddr3 = strRSAddr4;
                                strRSAddr4 = "";
                            }

                            // Evaluate Line Lengths
                            if (fecherFoundation.Strings.Trim(strRSName2).Length * lngCharWidth > lngAddr2Width)
                            {
                                if (strRSAddr1 == "" || strRSAddr2 == "" || strRSAddr3 == "" || strRSAddr4 == "")
                                {
                                    // Line 2 is too long and there's an empty line at the bottom, push everything down and wrap line 2 (strRSName2)
                                    strRSAddr4 = strRSAddr3;
                                    strRSAddr3 = strRSAddr2;
                                    strRSAddr2 = strRSAddr1;
                                    strRSAddr1 = fecherFoundation.Strings.Trim(Strings.Mid(strRSName2, FCConvert.ToInt32(lngAddr2Width / lngCharWidth + 1), FCConvert.ToInt32(strRSName2.Length - lngAddr2Width / lngCharWidth + 1)));
                                    strRSName2 = fecherFoundation.Strings.Trim(Strings.Left(strRSName2, FCConvert.ToInt32(lngAddr2Width / lngCharWidth)));
                                }
                                else
                                {
                                    // Line 2 is too long but there's no empty line, truncate line 2
                                    strRSName2 = fecherFoundation.Strings.Trim(Strings.Left(strRSName2, FCConvert.ToInt32(lngAddr2Width / lngCharWidth)));
                                }
                            }

                            if (fecherFoundation.Strings.Trim(strRSAddr1).Length * lngCharWidth > lngAddr2Width)
                            {
                                if (strRSAddr2 == "" || strRSAddr3 == "" || strRSAddr4 == "")
                                {
                                    // Line 3 is too long and there's an empty line, push everything down and wrap line 3 (strRSAddr1)
                                    strRSAddr4 = strRSAddr3;
                                    strRSAddr3 = strRSAddr2;
                                    strRSAddr2 = fecherFoundation.Strings.Trim(Strings.Mid(strRSAddr1, FCConvert.ToInt32(lngAddr2Width / lngCharWidth + 1), FCConvert.ToInt32(strRSAddr1.Length - lngAddr2Width / lngCharWidth + 1)));
                                    strRSAddr1 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr1, FCConvert.ToInt32(lngAddr2Width / lngCharWidth)));
                                }
                                else
                                {
                                    // Line 3 is too long but there's no empty line, truncate line 3
                                    strRSAddr1 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr1, FCConvert.ToInt32(lngAddr2Width / lngCharWidth)));
                                }
                            }

                            if (fecherFoundation.Strings.Trim(strRSAddr2).Length * lngCharWidth > lngAddr2Width)
                            {
                                if (strRSAddr3 == "" || strRSAddr4 == "")
                                {
                                    // Line 4 is too long and there's an empty line, push everything down and wrap line 4 (strRSAddr2)
                                    strRSAddr4 = strRSAddr3;
                                    strRSAddr3 = fecherFoundation.Strings.Trim(Strings.Mid(strRSAddr2, FCConvert.ToInt32(lngAddr2Width / lngCharWidth + 1), FCConvert.ToInt32(strRSAddr2.Length - lngAddr2Width / lngCharWidth + 1)));
                                    strRSAddr2 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr2, FCConvert.ToInt32(lngAddr2Width / lngCharWidth)));
                                }
                                else
                                {
                                    // Line 4 is too long but there's no empty line, truncate line 4
                                    strRSAddr2 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr2, FCConvert.ToInt32(lngAddr2Width / lngCharWidth)));
                                }
                            }

                            if (fecherFoundation.Strings.Trim(strRSAddr3).Length * lngCharWidth > lngAddr2Width)
                            {
                                if (strRSAddr4 == "")
                                {
                                    // Line 4 is too long and there's an empty line, push everything down and wrap line 4 (strRSAddr2)
                                    strRSAddr4 = fecherFoundation.Strings.Trim(Strings.Mid(strRSAddr3, FCConvert.ToInt32(lngAddr2Width / lngCharWidth + 1), FCConvert.ToInt32(strRSAddr3.Length - lngAddr2Width / lngCharWidth + 1)));
                                    strRSAddr3 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr3, FCConvert.ToInt32(lngAddr2Width / lngCharWidth)));
                                }
                                else
                                {
                                    // Line 4 is too long but there's no empty line, truncate line 4
                                    strRSAddr3 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr3, FCConvert.ToInt32(lngAddr2Width / lngCharWidth)));
                                }
                            }

                            if (fecherFoundation.Strings.Trim(strRSAddr4).Length * lngCharWidth > lngAddr2Width)
                            {
                                // Line 5 is too long, truncate line 5 (strRSAddr3)
                                strRSAddr4 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr4, FCConvert.ToInt32(lngAddr2Width / lngCharWidth)));
                            }

                            strAddressBar = "    " + FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces(strCMFName, FCConvert.ToInt16(lngAddr1Width / lngCharWidth - 2), false)) + FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces("Tax Payer:", 11, false)) + strRSName1 + "\r\n" + "    " + FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces(strCMFAddr1, FCConvert.ToInt16(lngAddr1Width / lngCharWidth + 9), false)) + strRSName2 + "\r\n" + "    " + FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces(strCMFAddr2, FCConvert.ToInt16(lngAddr1Width / lngCharWidth + 9), false)) + strRSAddr1;
                            if (fecherFoundation.Strings.Trim(strCMFAddr3) != "" || fecherFoundation.Strings.Trim(strRSAddr2) != "")
                            {
                                strAddressBar += "\r\n" + "    " + FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces(strCMFAddr3, FCConvert.ToInt16(lngAddr1Width / lngCharWidth + 9), false)) + strRSAddr2;
                            }
                            if (fecherFoundation.Strings.Trim(strCMFAddr4) != "" || fecherFoundation.Strings.Trim(strRSAddr3) != "")
                            {
                                strAddressBar += "\r\n" + "    " + FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces(strCMFAddr4, FCConvert.ToInt16(lngAddr1Width / lngCharWidth + 9), false)) + strRSAddr3;
                            }
                            if (fecherFoundation.Strings.Trim(strRSAddr4) != "")
                            {
                                strAddressBar += "\r\n" + "    " + FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces("", FCConvert.ToInt16(lngAddr1Width / lngCharWidth + 9), false)) + strRSAddr4;
                            }

                            // set name and address bar for a mortgage holder
                            modRhymalReporting.Statics.frfFreeReport[13].DatabaseFieldName = strAddressBar;
                            rsIntParty.MoveNext();
                        }
                        else
                        {
                            strAddressBar = "    " + rsData.Get_Fields("Name1");
                            if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Address1"))) != "")
                            {
                                strAddressBar += "\r\n" + "    " + rsData.Get_Fields("Address1");
                            }
                            strAddressBar += "\r\n" + "    " + GetVariableValue_2("MUNI") + ", " + GetVariableValue_2("STATE") + " " + GetVariableValue_2("ZIP");

                            // set name and address bar for a mortgage holder
                            modRhymalReporting.Statics.frfFreeReport[13].DatabaseFieldName = strAddressBar;
                        }

                    }
                    else if (!blnIsMortCopy && !blnIsIPCopy && boolNewOwner)
                    { // this is the last copy to be made, it is for the new owner
                      // boolNewOwner = False                            'this will set it off for the next account
                        if (intNewOwnerChoice > 0)
                        { // if the user is getting charged for certified mail, then save their info to print on Cert Mail Forms
                            boolPrintCMF = true;
                        }
                        else
                        {
                            boolPrintCMF = false;
                        }
                        // info for CMFNumbers table
                        strCMFName = rsRE.Get_Fields_String("DeedName1");
                        lngCMFMHNumber = 0;
                        lngCMFBillKey = FCConvert.ToInt32(rsData_ID);
                        

                        strAddressBar = "    " + rsRE.Get_Fields("DeedName1"); // name at time of billing
                                                                                  // strAddressBar = strAddressBar & vbCrLf & "    C/O " & rsRE.Fields("RSName")   'name on the current RE account

                        // if the row needs to be added, then do it
                        if (fecherFoundation.Strings.Trim(own2) != "")
                        {
                            strAddressBar += "\r\n" + "    " + rsRE.Get_Fields("Deedname2");
                        }
                        if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1Address1"))) != "")
                        {
                            strAddressBar += "\r\n" + "    " + rsRE.Get_Fields("Own1Address1");
                        }

                        if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1Address2"))) != "")
                        {
                            strAddressBar += "\r\n" + "    " + rsRE.Get_Fields("Own1Address2");
                        }

                        if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1City"))) != "")
                        {
                            strAddressBar += "\r\n" + "    " + rsRE.Get_Fields("Own1City") + ", " + rsRE.Get_Fields("Own1State") + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1Zip")));
                        }

                        // this is information for the Certified Mailing List
                        strCMFAddr1 = FCConvert.ToString(rsRE.Get_Fields("Own1Address1"));
                        strCMFAddr2 = FCConvert.ToString(rsRE.Get_Fields("Own1Address2"));
                        strCMFAddr3 = rsRE.Get_Fields("Own1City") + ", " + rsRE.Get_Fields("Own1State") + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1Zip")));
                       
                        // set name and address bar for a New Owner
                        modRhymalReporting.Statics.frfFreeReport[13].DatabaseFieldName = strAddressBar;
                        rsRE.MoveNext();
                    }
                    else
                    {
                        strAddressBar = "    " + rsData.Get_Fields("Name1");
                        if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Address1"))) != "")
                        {
                            strAddressBar += "\r\n" + "    " + rsData.Get_Fields("Address1");
                        }
                        strAddressBar += "\r\n" + "    " + GetVariableValue_2("MUNI") + ", " + GetVariableValue_2("STATE") + " " + GetVariableValue_2("ZIP");

                        // set name and address bar for a mortgage holder
                        modRhymalReporting.Statics.frfFreeReport[13].DatabaseFieldName = strAddressBar;
                    }
                }

                // this will add the information to the CMFNumber table so that it will be
                // easier to figure out which accounts need certified mail forms to be printed for them
                // MAL@20071022: Added support for new Process Type
                // Call Reference: 114824
                if (lngCMFMHNumber != 0)
                {
                    if (blnIsMortCopy)
                    {
                        rsCMFNumbers.FindFirstRecord2("Account,Type,MortgageHolder,ProcessType", rsData.Get_Fields("Account") + ",21," + FCConvert.ToString(lngCMFMHNumber) + ",LIEN", ","); // kk08122014 trocls-49  ' "'LIEN'"
                    }
                    else
                    {
                        rsCMFNumbers.FindFirstRecord2("Account,Type,InterestedPartyID,ProcessType", rsData.Get_Fields("Account") + ",21," + FCConvert.ToString(lngCMFMHNumber) + ",LIEN", ","); // kk08122014 trocls-49  ' "'LIEN'"
                    }
                }
                else
                {
                    if (boolCopy && boolNewOwner)
                    {
                        rsCMFNumbers.FindFirstRecord2("Account,Type,NewOwner,ProcessType", rsData.Get_Fields("Account") + ",21,1,LIEN", ","); // kk08122014 trocls-49  'rsData.Fields("Account") & ", 21, 1, 'LIEN'"
                    }
                    else
                    {                        
                        rsCMFNumbers.FindFirstRecord2("Account,Type,NewOwner,ProcessType", rsData.Get_Fields("Account") + ",21,0,LIEN", ","); // kk08122014 trocls-49  'rsData.Fields("Account") & ", 21, 0, 'LIEN'"
                    }
                }
               
                if (rsCMFNumbers.NoMatch)
                { 
                    rsCMFNumbers.AddNew();
                }
                else
                {
                    rsCMFNumbers.Edit();
                }
                rsCMFNumbers.Set_Fields("Name", strCMFName);
                rsCMFNumbers.Set_Fields("Address1", strCMFAddr1);
                rsCMFNumbers.Set_Fields("Address2", strCMFAddr2);
                rsCMFNumbers.Set_Fields("Address3", strCMFAddr3);
                rsCMFNumbers.Set_Fields("Address4", strCMFAddr4);
                rsCMFNumbers.Set_Fields("Billkey", lngCMFBillKey);
                if (blnIsMortCopy)
                {
                    rsCMFNumbers.Set_Fields("MortgageHolder", lngCMFMHNumber);
                    if (lngCMFMHNumber != 0)
                    {
                        // mort holder                        
                    }
                }
                else
                {
                    if (blnIsIPCopy)
                    {
                        rsCMFNumbers.Set_Fields("InterestedPartyID", lngCMFMHNumber);
                        if (lngCMFMHNumber != 0)
                        {
                            // Interested Party                            
                        }
                    }
                    else
                    {
                        rsCMFNumbers.Set_Fields("NewOwner", false);
                        if (boolCopy && boolNewOwner)
                        { // is this is a new owner copy
                            
                            rsCMFNumbers.Set_Fields("NewOwner", true);
                            boolNewOwner = false;
                        }
                        
                    }
                }

                rsCMFNumbers.Set_Fields("Account", rsData.Get_Fields("Account"));
                rsCMFNumbers.Set_Fields("Type", 21);
                rsCMFNumbers.Set_Fields("ProcessType", "LIEN"); 
                rsCMFNumbers.Set_Fields("REPP", "RE");
                rsCMFNumbers.Update(true);
                rsRK?.Dispose();
                rsTemp?.Dispose();
                return;
            }
            catch (Exception ex)
            {
                frmWait.InstancePtr.Unload();
                MessageBox.Show($"Error - {ex.GetBaseException().Message}", "Calculate Variable Error - " + rsData.Get_Fields("BillKey"), MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private (string strCMFName, int lngCMFMHNumber, int lngCMFBillKey, string strCMFAddr1, string strCMFAddr2, string strCMFAddr3, string strCMFAddr4) SetCMFParty(clsDRWrapper rs, bool isMortgage = false)
        {
            string strCMFName;
            int lngCMFMHNumber;
            int lngCMFBillKey;
            string strCMFAddr1;
            string strCMFAddr2;
            string strCMFAddr3;
            string strCMFAddr4;

            // info for CMFNumbers table
            strCMFName = FCConvert.ToString(rs.Get_Fields("Name"));
            lngCMFMHNumber = FCConvert.ToInt32(rs.Get_Fields(isMortgage?"MortgageID":"ID"));
            lngCMFBillKey = FCConvert.ToInt32(rsData.Get_Fields("ID"));

            // hold address pieces
            string addr1 = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields("Address1")));
            string addr2 = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields("Address2")));
            string addr3 = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields("Address3")));
            string citystzip = rs.Get_Fields("City") + ", " + rs.Get_Fields("State") + " " + rs.Get_Fields("Zip");
            string zip4 = rs.Get_Fields("Zip4");
            if (fecherFoundation.Strings.Trim(FCConvert.ToString(zip4)) != "") citystzip += "-" + zip4;

            // aggregate the lines, removing blank lines
            var addrLines = new string[4];
            int i = 0;

            if (addr1 != string.Empty)
            {
                addrLines[i] = addr1;
                i++;
            }

            if (addr2 != string.Empty)
            {
                addrLines[i] = addr2;
                i++;
            }

            if (addr3 != string.Empty)
            {
                addrLines[i] = addr3;
                i++;
            }

            if (citystzip != string.Empty)
            {
                addrLines[i] = citystzip;
                i++;
            }

            // output the address lines to the right variables
            strCMFAddr1 = addrLines[0];
            strCMFAddr2 = addrLines[1];
            strCMFAddr3 = addrLines[2];
            strCMFAddr4 = addrLines[3];

            return (strCMFName, lngCMFMHNumber, lngCMFBillKey, strCMFAddr1, strCMFAddr2, strCMFAddr3, strCMFAddr4);
        }

        private double DetermineCertifiedMailFee()
        {
            double ret;

            var certMailFee = FCConvert.ToDouble(GetVariableValue_2("MAILFEE"));

            if (boolPayCert)
            {

                if (boolPayMortCert)
                {                    
                    ret = (NumberOfMortgageHolders * certMailFee) + certMailFee;
                }
                else
                {
                    ret = certMailFee;
                }
            }
            else
            {
                if (boolPayMortCert)
                {
                    ret = NumberOfMortgageHolders * certMailFee;
                }
                else
                {
                    ret = 0;
                }
            }

            return ret;
        }

        private int CountMortgageHolders()
        {
            var ret = 0;

            if (boolMort) ret = CalculateMortgageHolders();

            return ret;
        }

        private void SetAbateArray(string period, double principal, double[] AbateArray, clsDRWrapper rsRK)
        {

            if (int.TryParse(period, out var periodNumber))
            {
                AbateArray[periodNumber - 1] = FCConvert.ToDouble(AbateArray[periodNumber - 1] == principal);
            }
            else
            {
                if (period != "A") return;

                if (rsRK.EndOfFile())
                {
                    AbateArray[0] += principal;
                }
                else
                {
                    // find out how many periods that this bill has
                    var numPeriods = (int?) FCConvert.ToInt32(Conversion.Val(rsRK.Get_Fields("NumberOfPeriods")));

                    switch (numPeriods)
                    {
                        case 1:
                        {
                            // only one period, so put it all into per 1
                            AbateArray[0] += principal;

                            break;
                        }
                        case 2:
                        {
                            AbateTwoPeriods(principal, AbateArray);

                            break;
                        }
                        case 3:
                        {
                            AbateThreePeriods(principal, AbateArray);

                            break;
                        }
                        case 4:
                        {
                            AbateFourPeriods(principal, AbateArray);

                            break;
                        }
                    } //end switch
                }
            }
        }

        private void AbateFourPeriods(double principal, double[] abateArray)
        {
            double dblAdded = 0;
            var onefourthPrincipal = modGlobal.Round(principal / 4, 2);
            var taxDue_4 = rsData.Get_Fields("TaxDue4");

            if (taxDue_4 >= abateArray[3] + onefourthPrincipal)
            {
                abateArray[3] += onefourthPrincipal;
                dblAdded += onefourthPrincipal;
            }
            else
            {
                dblAdded += (taxDue_4 - abateArray[3]);
                abateArray[3] = taxDue_4;
            }

            if (rsData.Get_Fields("TaxDue3") >= abateArray[2] + onefourthPrincipal)
            {
                abateArray[2] += onefourthPrincipal;
                dblAdded += onefourthPrincipal;
            }
            else
            {
                dblAdded += (rsData.Get_Fields("TaxDue3") - abateArray[2]);
                abateArray[2] += rsData.Get_Fields("TaxDue3");
            }

            if (rsData.Get_Fields("TaxDue2") >= abateArray[1] + onefourthPrincipal)
            {
                abateArray[1] += onefourthPrincipal;
                dblAdded += onefourthPrincipal;
            }
            else
            {
                dblAdded += (rsData.Get_Fields("TaxDue2") - abateArray[1]);
                abateArray[1] = rsData.Get_Fields("TaxDue2");
            }

            abateArray[0] += modGlobal.Round(principal - dblAdded, 2);

        }

        private void AbateThreePeriods(double principal, double[] AbateArray)
        {
            double dblAdded = 0;
            var principalAmt = modGlobal.Round(principal / 3, 2);

            var taxDue3 = rsData.Get_Fields("TaxDue3");

            if (taxDue3 >= AbateArray[2] + principalAmt)
            {
                AbateArray[2] += principalAmt;
                dblAdded += principalAmt;
            }
            else
            {
                dblAdded += (taxDue3 - AbateArray[2]);
                AbateArray[2] += taxDue3;
            }

            var taxDue2 = rsData.Get_Fields("TaxDue2");

            if (taxDue2 >= AbateArray[1] + principalAmt)
            {
                AbateArray[1] += principalAmt;
                dblAdded += principalAmt;
            }
            else
            {
                dblAdded += (taxDue2 - AbateArray[1]);
                AbateArray[1] = taxDue2;
            }

            AbateArray[0] += modGlobal.Round(principal - dblAdded, 2);

        }

        private void AbateTwoPeriods(double principal, double[] AbateArray)
        {
            double dblAdded = 0;
            // two periods so make sure that not too much has been put into period 2, once it is full then put the rest into period one
            var principalAmt = modGlobal.Round(principal / 2, 2);

            var taxDue2 = rsData.Get_Fields("TaxDue2");

            if (taxDue2 >= AbateArray[1] + principalAmt)
            {
                AbateArray[1] += principalAmt;
                dblAdded += principalAmt;
            }
            else
            {
                dblAdded += (taxDue2 - AbateArray[1]);
                AbateArray[1] = taxDue2;
            }

            AbateArray[0] += modGlobal.Round(principal - dblAdded, 2);
        }

        private clsDRWrapper GetPaymentRecords()
        {
            //var strSelect = $"select * from paymentrec where code = 'A' and billcode = '{(modStatusPayments.Statics.boolRE ? "R" : "P")}' and account = @acct and receiptnumber <> -1 and billkey = @billkey ORDER BY RecordedTransactionDate, ActualSystemDate, ReceiptNumber, ID";
            var strSelect = "select * from paymentrec where code = 'A' and billcode = '" + (modStatusPayments.Statics.boolRE ? "R" : "P") + "' and account = " + rsData.Get_Fields_Int32("Account") + " and receiptnumber <> -1 and billkey = " + rsData.Get_Fields_Int32("ID") + " ORDER BY RecordedTransactionDate, ActualSystemDate, ReceiptNumber, ID";
            var rsTemp = new clsDRWrapper();
            rsTemp.OpenRecordset(strSelect, "TWCL0000.VB1");
            //rsTemp.Set_ParameterValue("@acct", rsData.Get_Fields("Account"));
            //rsTemp.Set_ParameterValue("@billkey", rsData.Get_Fields_Int32("ID"));
            return rsTemp;
        }

        private int CalculateMortgageHolders()
        {
            int CalculateMortgageHolders = 0;
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this function will return the number of mortgage holders this account has
                // MAL@20080723: Change to take Receive Copies option into account
                // Tracker Reference: 13813
                // rsMort.OpenRecordset "SELECT MortgageHolders.MortgageHolderID AS ID, * FROM MortgageAssociation INNER JOIN MortgageHolders ON MortgageAssociation.MortgageHolderID = MortgageHolders.MortgageHolderID WHERE Account = " & rsData.Fields("Account") & " AND Module = 'RE'", strGNDatabase
                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                rsMort.OpenRecordset("SELECT * FROM MortgageAssociation INNER JOIN MortgageHolders ON MortgageAssociation.MortgageHolderID = MortgageHolders.ID WHERE Account = " + FCConvert.ToString(rsData.Get_Fields("Account")) + " AND Module = 'RE' AND ReceiveCopies = 1", "CentralData");
                if (rsMort.EndOfFile() != true)
                {
                    CalculateMortgageHolders = rsMort.RecordCount();
                }
                else
                {
                    CalculateMortgageHolders = 0;
                }
                return CalculateMortgageHolders;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                CalculateMortgageHolders = 0;
            }
            return CalculateMortgageHolders;
        }
        
        public void SaveLienStatus()
        {
            if (Strings.Right(strAcctList, 1) == ",")
            {
                strAcctList = Strings.Left(strAcctList, strAcctList.Length - 1);
            }
            // this statement will change the status of the accounts that have been process
            if (Strings.Trim(strAcctList) != "")
            {
                rsData.Execute("UPDATE BillingMaster SET LienProcessStatus = 3 WHERE BillingYear/10 = " + FCConvert.ToString(intYear) + " AND Account IN (" + strAcctList + ") AND RateKey IN " + frmFreeReport.InstancePtr.strRK, modExtraModules.strCLDatabase);
            }
        }

        private void SetReportFont()
        {
            fldHeader.Font = new Font(strDefFontName, intDefFontSize);
            rtbText.Font = new Font(strDefFontName, intDefFontSize);
            rtbText2.Font = new Font(strDefFontName, intDefFontSize);
            lblBreakdown.Font = new Font(strDefFontName, intDefFontSize);
            lblCosts1.Font = new Font(strDefFontName, intDefFontSize);
            lblCosts2.Font = new Font(strDefFontName, intDefFontSize);
            lblPrincipal.Font = new Font(strDefFontName, intDefFontSize);
            lblInterest.Font = new Font(strDefFontName, intDefFontSize);
            lblTotal.Font = new Font(strDefFontName, intDefFontSize);
            fldCollector.Font = new Font(strDefFontName, intDefFontSize);
            fldTitle.Font = new Font(strDefFontName, intDefFontSize);
            fldMuni.Font = new Font(strDefFontName, intDefFontSize);
        }

        private void GroupHeader1_Format(object sender, System.EventArgs e)
        {
            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
            lblAccount.Text = modGlobal.PadToString(rsData.Get_Fields("Account"), 6);
            SetHeader();
        }

        private void arLienProcess_Load(object sender, System.EventArgs e)
        {
        }
    }
}
