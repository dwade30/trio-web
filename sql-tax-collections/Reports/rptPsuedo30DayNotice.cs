﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using fecherFoundation.Extensions;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptPsuedo30DayNotice : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               02/14/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               04/22/2005              *
		// ********************************************************
		// this report will be used to show a default 30day notice, lien and lien maturity notice as it
		// is seen in the window of the editor.  It will add fake names, addresses and values into the fields
		clsDRWrapper rsData = new clsDRWrapper();
		// Dim rsCMFNumbers                As New clsDRWrapper
		string strSQL;
		string strText = "";
		// VBto upgrade warning: intYear As short --> As int	OnRead(string)
		int intYear;
		bool boolPayCert;
		bool boolMort;
		bool boolPayMortCert;
		// Dim rsCert                      As clsDRWrapper
		int lngCopies;
		// this is to keep track of the number of mortgage holder copies left
		bool boolCopy;
		// is this a copy or the original
		bool boolPrintNewOwner;
		// this is if the user wants to send to any new owners
		// Dim rsMort                      As New clsDRWrapper
		// Dim rsRE                        As New clsDRWrapper
		// Dim rsRate                      As New clsDRWrapper
		// Dim rsPayments                  As New clsDRWrapper
		bool boolNewOwner;
		// this is true if there is a new owner
		bool boolAtLeastOneRecord;
		// this is true if there is at least one record to see
		string strAcctList = "";
		int lngMaxNumber;
		int lngCount;
		string strReportDesc = "";
		// this will be the string that will describe the parameters the user set for the report ie. Account 1 to 100
		// these variables are for counting the accounts
		public int lngBalanceZero;
		public int lngBalanceNeg;
		public int lngBalancePos;
		public int lngDemandFeesApplied;

		public rptPsuedo30DayNotice()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "PrptPsuedo30DayNotice";
		}

		public static rptPsuedo30DayNotice InstancePtr
		{
			get
			{
				return (rptPsuedo30DayNotice)Sys.GetInstance(typeof(rptPsuedo30DayNotice));
			}
		}

		protected rptPsuedo30DayNotice _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
			if (!rsData.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				lblPageNumber.Text = modGlobal.PadToString(rsData.Get_Fields("Account"), 6);
			}
			//Detail_Format();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			// this will set all of the bill records to status of 1
			int lngCT/*unused?*/;
			// rsData.MoveFirst
			// Do Until rsData.EndOfFile
			// lngCT = rsData.Fields("LienProcessStatus")      'get the current status
			// If lngCT > 1 Then
			// 
			// ElseIf lngCT = 1 Then                           'already at this status
			// 
			// Else                                            'update
			// rsData.Edit
			// rsData.Fields("LienProcessStatus") = 1
			// rsData.Update
			// End If
			// rsData.MoveNext
			// Loop
			// frmFreeReport.vsSummary.TextMatrix(1, 1) = lngBalanceZero
			// frmFreeReport.vsSummary.TextMatrix(2, 1) = lngBalanceNeg
			// frmFreeReport.vsSummary.TextMatrix(3, 1) = lngBalancePos
			// frmFreeReport.vsSummary.TextMatrix(4, 1) = lngBalanceZero + lngBalanceNeg + lngBalancePos
			// 
			// If lngDemandFeesApplied > 0 Then
			// show a special label that is highlighted to show this information
			// With frmFreeReport.vsSummary
			// frmFreeReport.vsSummary.AddItem ""
			// frmFreeReport.vsSummary.AddItem ""
			// If (((.rows - 1) * .RowHeight(1)) + 70) < (frmFreeReport.fraSummary.Height - 200) Then
			// .Height = ((.rows - 1) * .RowHeight(1)) + 70
			// Else
			// .Height = frmFreeReport.fraSummary.Height - 200
			// End If
			// End With
			// End If
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// lblTime.Text = Format(Time, "h:mm AM/PM")
			// lblDate.Text = Format(Date, "MM/dd/yyyy")
			// lblMuniName.Text = MuniName
			//this.Visible = false; // this will have to be set back to true to be shown
			SetHeader(true);
			lngBalanceNeg = 0;
			lngBalancePos = 0;
			lngBalanceZero = 0;
			lngDemandFeesApplied = 0;
			lngCount = 0;
			boolPayCert = FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1), 2) == "Ye");
			boolMort = FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[16].RowNumber, 1), 2) == "Ye");
			boolPrintNewOwner = FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[29].RowNumber, 1), 2) == "Ye");
			boolNewOwner = false;
			if (boolMort)
			{
				boolPayMortCert = !FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[16].RowNumber, 1), 9) == "Yes, with");
			}
			else
			{
				boolPayMortCert = false;
			}
			// If gboolRE Then
			// rsRE.OpenRecordset "SELECT * FROM MASTER", strREDATABASE
			// End If
			// rsRate.OpenRecordset "SELECT * FROM RateRec", strCLDATABASE
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			intYear = FCConvert.ToInt32(Math.Round(Conversion.Val(modExtraModules.FormatYear(frmRateRecChoice.InstancePtr.cmbYear.Text))));
			strSQL = SetupSQL();
			rsData.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
			if (rsData.EndOfFile() != true)
			{
				lngMaxNumber = rsData.RecordCount();
				// rsCMFNumbers.OpenRecordset "SELECT * FROM CMFNumbers WHERE ID = 0", strCLDATABASE
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				lblPageNumber.Text = modGlobal.PadToString(rsData.Get_Fields("Account"), 6);
				// this will show the account number in the top right of the page
			}
			else
			{
				lngMaxNumber = 0;
			}
			frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Data", true, lngMaxNumber);
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			// this will create another active report with the summary information on it so the user can print if the feel like it
			// rptLienNoticeSummary.Init strAcctList, "30 Day Notice Summary", strReportDesc, intYear, cdate(str
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void BindFields()
		{
			// this sub will put all of the information into the string
			if (rsData.EndOfFile() != true)
			{
				// calculate fields
				frmWait.InstancePtr.IncrementProgress();
				CalculateVariableTotals();
				if (rsData.EndOfFile())
				{
					return;
					frmWait.InstancePtr.Unload();
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				}
				SetHeader();
				// this will set the main string
				//FC:FINAL:DSE WordWrapping not working in RichTextBox
				//rtbText.Text = SetupVariablesInString(frmFreeReport.InstancePtr.rtbData.Text);
				rtbText.SetHtmlText(SetupVariablesInString(frmFreeReport.InstancePtr.rtbData.Text));
				//rtbText.SelStart = 0;
				//rtbText.SelLength = rtbText.Text.Length;
				rtbText.SelectedText = rtbText.Text;
				rtbText.Font = new Font("Courier New", rtbText.Font.Size);
				// this will set the bottom of the report
				SetupFooterVariables();
				if (lngCopies > 1)
				{
					// this is to produce multiple copies for certain parties (mortgage holders)
					lngCopies -= 1;
					boolCopy = true;
				}
				else
				{
					rsData.MoveNext();
					boolCopy = false;
				}
			}
			else
			{
				frmWait.InstancePtr.Unload();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			}
		}

		private void SetHeader(bool boolFirstRun = false)
		{
			if (boolFirstRun)
			{
				if (Strings.UCase(modCustomReport.Statics.strReportType) == "30DAYNOTICE")
				{
					lblReportHeader.Text = "State of Maine" + "\r\n" + "Tax Collector's Notice, Lien Claim and Demand" + "\r\n" + "30 Day Notice";
					ar30DayNotice.InstancePtr.Name = "30 Day Notice";
				}
				else
				{
				}
			}
			else
			{
				if (boolCopy)
				{
					if (Strings.UCase(modCustomReport.Statics.strReportType) == "30DAYNOTICE")
					{
						lblReportHeader.Text = "State of Maine" + "\r\n" + "Tax Collector's Notice, Lien Claim and Demand" + "\r\n" + "30 Day Notice" + "\r\n" + "COPY";
					}
					else
					{
					}
				}
				else
				{
					if (Strings.UCase(modCustomReport.Statics.strReportType) == "30DAYNOTICE")
					{
						lblReportHeader.Text = "State of Maine" + "\r\n" + "Tax Collector's Notice, Lien Claim and Demand" + "\r\n" + "30 Day Notice";
					}
					else
					{
					}
				}
			}
		}

		private string SetupSQL()
		{
			string SetupSQL = "";
			// this will return the SQL statement for this batch of reports
			string strWhereClause = "";
			int intCT/*unused?*/;
			string strOrderBy = "";
			if (frmRateRecChoice.InstancePtr.cmbRange.SelectedIndex == 2)
			{
				// range of accounts
				strOrderBy = " ORDER BY Name1";
				if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[0].Text) != "")
				{
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
					{
						// both full
						strWhereClause = " AND Account >= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text)) + " AND Account <= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
						strReportDesc = "Account " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text)) + " To " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
					}
					else
					{
						// first full second empty
						strWhereClause = " AND Account >= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text));
						strReportDesc = "Account Above " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text));
					}
				}
				else
				{
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
					{
						// first empty second full
						strWhereClause = " AND Account <= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
						strReportDesc = "Account Below " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
					}
					else
					{
						// both empty
						strWhereClause = "";
						strReportDesc = "";
					}
				}
			}
			else if (frmRateRecChoice.InstancePtr.cmbRange.SelectedIndex == 1)
			{
				// range of names
				strOrderBy = " ORDER BY Name1";
				if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[0].Text) != "")
				{
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
					{
						// both full
						// strWhereClause = " AND Name1 BETWEEN '" & .txtRange(0).Text & "' AND '" & .txtRange(1).Text & "'"
						strWhereClause = " AND Name1 >= '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "    ' AND Name1 <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "ZZZZ'";
						strReportDesc = "Name " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text)) + " To " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
					}
					else
					{
						// first full second empty
						strWhereClause = " AND Name1 >= '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "    '";
						strReportDesc = "Name Above " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text));
					}
				}
				else
				{
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
					{
						// first empty second full
						strWhereClause = " AND Name1 <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "ZZZZ'";
						strReportDesc = "Account Below " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text));
					}
					else
					{
						// both empty
						strWhereClause = "";
						strReportDesc = "";
					}
				}
			}
			else
			{
				strOrderBy = " ORDER BY Name1";
				strWhereClause = "";
				strReportDesc = "";
			}
			// strWhereClause = strWhereClause & " AND ("
			// For intCT = 1 To .vsRate.rows - 1
			// If Val(.vsRate.TextMatrix(intCT, 0)) = -1 Then
			// If Right(strWhereClause, 1) = "(" Then
			// strWhereClause = strWhereClause & "RateKey = " & .vsRate.TextMatrix(intCT, 1)
			// Else
			// strWhereClause = strWhereClause & " OR RateKey = " & .vsRate.TextMatrix(intCT, 1)
			// End If
			// End If
			// Next
			// strWhereClause = strWhereClause & ")"
			if (frmFreeReport.InstancePtr.boolInitial)
			{
				// kgk 2-14-2012  change from stored procs
				// rsData.CreateStoredProcedure "30DayNoticeQuery", "SELECT * From BillingMaster WHERE BillingType = 'RE' AND (LienProcessStatus = 0 OR LienProcessStatus = 1) AND LienStatusEligibility = 1 AND BillingYear/10 = " & intYear & strWhereClause & strOrderBy
				SetupSQL = "SELECT * From BillingMaster WHERE BillingType = 'RE' AND (LienProcessStatus = 0 OR LienProcessStatus = 1) AND LienStatusEligibility = 1 AND BillingYear/10 = " + FCConvert.ToString(intYear) + strWhereClause + strOrderBy;
			}
			else
			{
				// rsData.CreateStoredProcedure "30DayNoticeQuery", "SELECT * From BillingMaster WHERE BillingType = 'RE' AND (LienProcessStatus = 0 OR LienProcessStatus = 1) AND (LienStatusEligibility = 1 OR LienStatusEligibility = 2) AND BillingYear/10 = " & intYear & strWhereClause & strOrderBy
				SetupSQL = "SELECT * From BillingMaster WHERE BillingType = 'RE' AND (LienProcessStatus = 0 OR LienProcessStatus = 1) AND (LienStatusEligibility = 1 OR LienStatusEligibility = 2) AND BillingYear/10 = " + FCConvert.ToString(intYear) + strWhereClause + strOrderBy;
			}
			// SetupSQL = "SELECT * FROM 30DayNoticeQuery"
			return SetupSQL;
		}

		private string SetupVariablesInString(string strOriginal)
		{
			string SetupVariablesInString = "";
			// this will replace all of the variables with the information needed
			string strBuildString;
			// this is the string that will be built and returned at the end
			string strTemp = "";
			// this is a temporary sting that will be used to store and transfer string segments
			int lngNextVariable;
			// this is the position of the beginning of the next variable (0 = no more variables)
			int lngEndOfLastVariable;
			// this is the position of the end of the last variable
			lngEndOfLastVariable = 0;
			lngNextVariable = 1;
			strBuildString = "";
			// priming read
			if (Strings.InStr(1, strOriginal, "<") > 0)
			{
				lngNextVariable = Strings.InStr(lngEndOfLastVariable + 1, strOriginal, "<");
				// do until there are no more variables left
				do
				{
					// add the string from lngEndOfLastVariable - lngNextVariable to the BuildString
					strBuildString += Strings.Mid(strOriginal, lngEndOfLastVariable + 1, lngNextVariable - lngEndOfLastVariable - 1);
					// set the end pointer
					lngEndOfLastVariable = Strings.InStr(lngNextVariable, strOriginal, ">");
					// replace the variable
					strBuildString += GetVariableValue_2(Strings.Mid(strOriginal, lngNextVariable + 1, lngEndOfLastVariable - lngNextVariable - 1));
					// check for another variable
					lngNextVariable = Strings.InStr(lngEndOfLastVariable, strOriginal, "<");
				}
				while (!(lngNextVariable == 0));
			}
			// take the last of the string and add it to the end
			strBuildString += Strings.Mid(strOriginal, lngEndOfLastVariable + 1);
			// check for variables
			if (Strings.InStr(1, "<", strOriginal) > 0)
			{
				// strBuildString = SetupVariablesInString(strBuildString)     'setup recursion
				FCMessageBox.Show("ERROR: There are still variables in the report string.", MsgBoxStyle.Critical, "SetupVariablesInString Error");
			}
			else
			{
				SetupVariablesInString = strBuildString;
			}
			return SetupVariablesInString;
		}

		private void SetupFooterVariables()
		{
			// this will setup the bottom part of the detail section
			fldCollector.Text = GetVariableValue_2("COLLECTOR");
			fldMuni.Text = GetVariableValue_2("CITYTOWNOF") + " of " + GetVariableValue_2("MUNI");
			fldPrincipal.Text = Strings.Format(FCConvert.ToDouble(GetVariableValue_2("PRINCIPAL")) - Conversion.Val(rsData.Get_Fields_Decimal("PrincipalPaid")), "#,##0.00");
			fldInterest.Text = Strings.Format(GetVariableValue_2("INTEREST"), "#,##0.00");
			fldDemand.Text = Strings.Format(GetVariableValue_2("DEMAND"), "#,##0.00");
			fldCertMailFee.Text = Strings.Format(GetVariableValue_2("CERTTOTAL"), "#,##0.00");
			if (fldPrincipal.Text == "")
			{
				fldPrincipal.Text = "0.00";
			}
			if (fldInterest.Text == "")
			{
				fldInterest.Text = "0.00";
			}
			if (fldDemand.Text == "")
			{
				fldDemand.Text = "0.00";
			}
			if (fldCertMailFee.Text == "")
			{
				fldCertMailFee.Text = "0.00";
			}
			fldTotal.Text = Strings.Format(FCConvert.ToDouble(fldPrincipal.Text) + FCConvert.ToDouble(fldInterest.Text) + FCConvert.ToDouble(fldDemand.Text) + FCConvert.ToDouble(fldCertMailFee.Text), "#,##0.00");
		}

		private string GetVariableValue_2(string strVarName)
		{
			return GetVariableValue(ref strVarName);
		}

		private string GetVariableValue(ref string strVarName)
		{
			string GetVariableValue = "";
			// this function will take a variable name and find it in the array, then get the value and return it as a string
			int intCT;
			string strValue = "";
			//FC:FINAL:KS: #without nr.:fix Exit For inside switch/case statement
			bool endFor = false;
			// this is a dummy code to show the user where the hard codes are
			if (strVarName == "CRLF")
			{
				GetVariableValue = "";
				return GetVariableValue;
			}
			for (intCT = 0; intCT <= modRhymalReporting.MAXFREEVARIABLES; intCT++)
			{
				if (Strings.UCase(strVarName) == Strings.UCase(modRhymalReporting.Statics.frfFreeReport[intCT].Tag))
				{
					switch (modRhymalReporting.Statics.frfFreeReport[intCT].VariableType)
					{
						case 0:
							{
								// value from the static variables in the grid
								switch (modRhymalReporting.Statics.frfFreeReport[intCT].Type)
								{
								// 0 = Text, 1 = Number, 2 = Date, 3 = Do not ask for default (comes from DB), 4 = Formatted Year
									case 0:
										{
											strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
											break;
										}
									case 1:
										{
											strValue = Strings.Format(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1), "#,##0.00");
											break;
										}
									case 2:
										{
											strValue = Strings.Format(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1), "MMMM dd, yyyy");
											break;
										}
									case 3:
										{
											strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
											break;
										}
									case 4:
										{
											strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
											break;
										}
								}
								//end switch
								//FC:FINAL:KS: #without nr.:fix Exit For inside switch/case statement
								endFor = true;
								//break;
								break;
							}
						case 1:
							{
								// value from the dynamic variables in the database
								strValue = FCConvert.ToString(rsData.Get_Fields(modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName));
								if (Strings.Trim(strValue) == "")
								{
									if (Strings.UCase(modRhymalReporting.Statics.frfFreeReport[intCT].Tag) != "LESSPAYMENTS")
									{
										// maybe put a blank line there
										strValue = "__________";
									}
								}
								//FC:FINAL:KS: #without nr.:fix Exit For inside switch/case statement
								endFor = true;
								//break;
								break;
							}
						case 2:
							{
								// questions at the bottom of the grid
								strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
								//break;
								//FC:FINAL:KS: #without nr.:fix Exit For inside switch/case statement
								endFor = true;
								break;
							}
						case 3:
							{
								// calculated values that are stored in the DatabaseFieldName field
								strValue = modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName;
								if (Strings.Trim(strValue) == "")
								{
									if (Strings.UCase(modRhymalReporting.Statics.frfFreeReport[intCT].Tag) != "LESSPAYMENTS")
									{
										// maybe put a blank line there
										strValue = "__________";
									}
								}
								//break;
								//FC:FINAL:KS: #without nr.:fix Exit For inside switch/case statement
								endFor = true;
								break;
							}
					}
					//end switch
					//FC:FINAL:KS: #without nr.:fix Exit For inside switch/case statement
					if (endFor)
					{
						break;
					}
				}
			}
			GetVariableValue = strValue;
			return GetVariableValue;
		}

		private void CalculateVariableTotals()
		{
			var rsRK = new clsDRWrapper();
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will calculate all of the fields that need it and store
                // the value in the DatabaseFieldName string in the frfFreeReport
                // struct that these fields include principal, interest, costs,
                // total due and certified mail fee

                DateTime dtBillDate;
                double dblTotalDue = 0;
                double dblInt = 0;
                double dblPrin = 0;
                double dblPayments;
                double dblDemand /*unused?*/;
                double dblCertMailFee = 0;
                int lngMortHolder = 0;
                string strAddressBar = "";
                string strCommitmentDate = "";
                string strLessPaymentsLine = "";
                string strLocation = "";
                string strTemp = "";
                string strTemp2 = "";
                string strTemp3 = "";
                string strOwnerName = "";
                // for CMFNumbers
                string strCMFName = "";
                int lngCMFBillKey;
                int lngCMFMHNumber;
                bool boolCMFNewOwner /*unused?*/;
                bool boolPrintCMF;
                // true if a CMF should be printed for that person
                string strCMFAddr1 = "";
                string strCMFAddr2 = "";
                string strCMFAddr3 = "";
                TRYAGAIN: ;
                if (!boolCopy)
                {
                    // this will calculate the principal, interest and total due
                    if (((rsData.Get_Fields_Int32("LienRecordNumber"))) != 0)
                    {
                        dblTotalDue = modCLCalculations.CalculateAccountCLLien(rsData,
                            DateAndTime.DateValue(GetVariableValue_2("MAILDATE")), ref dblInt);
                    }
                    else
                    {
                        // if a blank date is returned, then this line will crash
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        dblTotalDue = modCLCalculations.CalculateAccountCL(ref rsData,
                            FCConvert.ToInt32(rsData.Get_Fields("Account")),
                            DateAndTime.DateValue(GetVariableValue_2("MAILDATE")), ref dblInt);
                    }

                    if (modGlobal.Round(dblTotalDue, 2) == 0)
                    {
                        lngBalanceZero += 1;
                        rsData.MoveNext();
                        if (rsData.EndOfFile())
                            return;
                        goto TRYAGAIN;
                    }

                    // this will check to see if demand fees are already in place on this acocunt
                    if (modGlobal.Round(Conversion.Val(rsData.Get_Fields_Decimal("DemandFees")), 2) != 0)
                    {
                        if (lngDemandFeesApplied == 0)
                        {
                            frmFreeReport.InstancePtr.vsSummary.AddItem("");
                            frmFreeReport.InstancePtr.vsSummary.AddItem(
                                "Accounts that already have demand fees applied:");
                            frmFreeReport.InstancePtr.vsSummary.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
                            frmFreeReport.InstancePtr.vsSummary.IsSubtotal(frmFreeReport.InstancePtr.vsSummary.Rows - 1,
                                true);
                            frmFreeReport.InstancePtr.vsSummary.RowOutlineLevel(
                                frmFreeReport.InstancePtr.vsSummary.Rows - 1, 1);
                            frmFreeReport.InstancePtr.vsSummary.ScrollBars =
                                FCGrid.ScrollBarsSettings.flexScrollVertical;
                            frmFreeReport.InstancePtr.vsSummary.Height =
                                frmFreeReport.InstancePtr.vsSummary.Height + 540;
                        }

                        lngDemandFeesApplied += 1;
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        frmFreeReport.InstancePtr.vsSummary.AddItem(
                            "\t" + FCConvert.ToString(rsData.Get_Fields("Account")));
                        frmFreeReport.InstancePtr.vsSummary.IsSubtotal(frmFreeReport.InstancePtr.vsSummary.Rows - 1,
                            true);
                        frmFreeReport.InstancePtr.vsSummary.RowOutlineLevel(
                            frmFreeReport.InstancePtr.vsSummary.Rows - 1, 2);
                        frmFreeReport.InstancePtr.vsSummary.Height = frmFreeReport.InstancePtr.vsSummary.Height + 270;
                        rsData.MoveNext();
                        if (rsData.EndOfFile())
                            return;
                        goto TRYAGAIN;
                    }

                    if (dblTotalDue > 0)
                    {
                        // these are the account that will be demanded
                        lngBalancePos += 1;
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        strAcctList += FCConvert.ToString(rsData.Get_Fields("Account")) + ",";
                    }
                    else
                    {
                        if (dblTotalDue < 0)
                        {
                            lngBalanceNeg += 1;
                        }
                    }

                    // this will find out how many mortgage holders this account has and how many copies to print
                    if (boolMort)
                    {
                        lngMortHolder = CalculateMortgageHolders();
                        lngCopies = lngMortHolder + 1;
                    }
                    else
                    {
                        lngMortHolder = 0;
                        lngCopies = 1;
                    }

                    // this will find out the certified mail fee
                    if (boolPayCert)
                    {
                        lngMortHolder += 1;
                        if (boolPayMortCert)
                        {
                            dblCertMailFee = lngMortHolder * FCConvert.ToDouble(GetVariableValue_2("MAILFEE"));
                        }
                        else
                        {
                            dblCertMailFee = FCConvert.ToDouble(GetVariableValue_2("MAILFEE"));
                        }
                    }
                    else
                    {
                        if (boolPayMortCert)
                        {
                            dblCertMailFee = lngMortHolder * FCConvert.ToDouble(GetVariableValue_2("MAILFEE"));
                        }
                        else
                        {
                            dblCertMailFee = 0;
                        }
                    }

                    if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name2"))) != "")
                    {
                        strOwnerName = FCConvert.ToString(rsData.Get_Fields_String("Name1")) + " and " +
                                       FCConvert.ToString(rsData.Get_Fields_String("Name2"));
                    }
                    else
                    {
                        strOwnerName = FCConvert.ToString(rsData.Get_Fields_String("Name1"));
                    }

                    // check to see if one is going to be sent to the next owner
                    if (boolPrintNewOwner)
                    {
                        rsRK.OpenRecordset(
                            "SELECT * FROM RateRec WHERE RateKey = " +
                            FCConvert.ToString(rsData.Get_Fields_Int32("RateKey")), modExtraModules.strCLDatabase);
                        if (rsRK.EndOfFile())
                        {
                            dtBillDate = (DateTime) rsData.Get_Fields_DateTime("BillingDate");
                        }
                        else
                        {
                            dtBillDate = (DateTime) rsData.Get_Fields_DateTime("TransferFromBillingDateFirst");
                        }

                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        if (modCLCalculations.NewOwner2(FCConvert.ToString(rsData.Get_Fields_String("Name1")),
                            FCConvert.ToInt32(rsData.Get_Fields("Account")),  dtBillDate))
                        {
                            // if there has been a new owner, then make a copy for the new owner as well
                            if (!boolNewOwner)
                            {
                                lngCopies += 1;
                                boolNewOwner = true;
                            }
                        }
                    }

                    dblPrin = Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1") +
                                             rsData.Get_Fields_Decimal("TaxDue2") +
                                             rsData.Get_Fields_Decimal("TaxDue3") +
                                             rsData.Get_Fields_Decimal("TaxDue4"));
                    dblPayments = Conversion.Val(rsData.Get_Fields_Decimal("PrincipalPaid"));
                    dblInt = (FCConvert.ToDouble(rsData.Get_Fields_Decimal("InterestCharged")) * -1) -
                        Conversion.Val(rsData.Get_Fields_Decimal("InterestPaid")) + dblInt;
                    // less payments line
                    // If Round(dblPayments, 2) <> 0 Then  'only show this when there are payments
                    // If dblPayments < 0 Then         'if the principal paid is a negative number then some adjustment has happened (supplemental) this should not happen later in the code life
                    // rsPayments.OpenRecordset "SELECT ID FROM PaymentRec WHERE BillKey = " & rsData.Fields("BillKey") & " AND (Code = 'A' OR Code = 'R' OR Code = 'S' OR Code = 'D')"
                    // If rsPayments.EndOfFile Then
                    // strLessPaymentsLine = "plus adjustment of " & Format(Abs(dblPayments), "#,##0.00") & " for the net sum of " & Format(dblPrin - dblPayments, "#,##0.00") & ","
                    // Else
                    // strLessPaymentsLine = "plus adjustment of " & Format(Abs(dblPayments), "#,##0.00") & " for the net sum of " & Format(dblPrin - dblPayments, "#,##0.00") & ","
                    // End If
                    // Else                            'principal paid is greater then zero
                    // rsPayments.OpenRecordset "SELECT ID FROM PaymentRec WHERE BillKey = " & rsData.Fields("BillKey") & " AND (Code = 'A' OR Code = 'R' OR Code = 'S' OR Code = 'D')"
                    // If rsPayments.EndOfFile Then
                    // strLessPaymentsLine = "less payment and adjustment of " & Format(dblPayments, "#,##0.00") & " for the net sum of " & Format(dblPrin - dblPayments, "#,##0.00") & ","
                    // Else
                    // strLessPaymentsLine = "less payment of " & Format(dblPayments, "#,##0.00") & " for the net sum of " & Format(dblPrin - dblPayments, "#,##0.00") & ","
                    // End If
                    // End If
                    // Else
                    // strLessPaymentsLine = ""
                    // End If
                    // Location
                    if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("StreetName"))) != "")
                    {
                        // TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
                        strLocation = FCConvert.ToString(rsData.Get_Fields("StreetNumber")) + " " +
                                      FCConvert.ToString(rsData.Get_Fields_String("StreetName"));
                    }
                    else
                    {
                        strLocation = "__________";
                    }

                    // Commitment Date
                    // rsRate.FindFirstRecord , , "RateKey = " & rsData.Fields("RateKey")
                    // If Not rsRate.NoMatch Then
                    // If rsRate.Fields("CommitmentDate") <> 0 Then
                    // strCommitmentDate = Format(rsRate.Fields("CommitmentDate"), "MMMM d, yyyy")
                    // Else
                    // strCommitmentDate = "__________"
                    // End If
                    // Else
                    // strCommitmentDate = "__________"
                    // End If
                    // Address Bar
                    strAddressBar = "    " + FCConvert.ToString(rsData.Get_Fields_String("Name1"));
                    if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name2"))) != "")
                    {
                        strAddressBar += " and " + FCConvert.ToString(rsData.Get_Fields_String("Name2"));
                    }

                    if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address1"))) != "")
                    {
                        strAddressBar += "\r\n" + "    " + FCConvert.ToString(rsData.Get_Fields_String("Address1"));
                        strAddressBar += "\r\n" + "    " + FCConvert.ToString(rsData.Get_Fields_String("Address2"));
                    }
                    else
                    {
                        strAddressBar += "\r\n" + "    " + FCConvert.ToString(rsData.Get_Fields_String("Address2"));
                    }

                    if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address3"))) != "")
                    {
                        strAddressBar += "\r\n" + "    " + FCConvert.ToString(rsData.Get_Fields_String("Address3"));
                    }

                    // info for CMFNumbers table
                    strCMFName = strOwnerName;
                    lngCMFMHNumber = 0;
                    boolNewOwner = false;
                    lngCMFBillKey = FCConvert.ToInt32(rsData.Get_Fields_Int32("BillKey"));
                    strCMFAddr1 = FCConvert.ToString(rsData.Get_Fields_String("Address1"));
                    strCMFAddr2 = FCConvert.ToString(rsData.Get_Fields_String("Address2"));
                    strCMFAddr3 = FCConvert.ToString(rsData.Get_Fields_String("Address3"));
                    // Billing Year
                    modRhymalReporting.Statics.frfFreeReport[1].DatabaseFieldName = FCConvert.ToString(intYear);
                    // Commitment Date (9)
                    modRhymalReporting.Statics.frfFreeReport[9].DatabaseFieldName = strCommitmentDate;
                    // Principal (17)
                    modRhymalReporting.Statics.frfFreeReport[17].DatabaseFieldName =
                        Strings.Format(dblPrin, "#,##0.00");
                    // Interest (18)
                    modRhymalReporting.Statics.frfFreeReport[18].DatabaseFieldName = Strings.Format(dblInt, "#,##0.00");
                    // Less Payments (19)
                    modRhymalReporting.Statics.frfFreeReport[19].DatabaseFieldName = strLessPaymentsLine;
                    // City/Town (20)
                    modRhymalReporting.Statics.frfFreeReport[20].DatabaseFieldName =
                        modGlobalConstants.Statics.gstrCityTown;
                    // Owner Name (21)
                    modRhymalReporting.Statics.frfFreeReport[21].DatabaseFieldName = strOwnerName;
                    // Total Due (25)
                    modRhymalReporting.Statics.frfFreeReport[25].DatabaseFieldName = Strings.Format(
                        dblTotalDue + dblCertMailFee + FCConvert.ToDouble(GetVariableValue_2("DEMAND")), "#,##0.00");
                    // Location (26)
                    modRhymalReporting.Statics.frfFreeReport[26].DatabaseFieldName = strLocation;
                    // Name And Address Bar
                    modRhymalReporting.Statics.frfFreeReport[27].DatabaseFieldName = strAddressBar;
                    // Total Certified Mail Fee
                    modRhymalReporting.Statics.frfFreeReport[28].DatabaseFieldName =
                        Strings.Format(dblCertMailFee, "#,##0.00");
                    rsData.Edit();
                    rsData.Set_Fields("Copies", lngCopies);
                    rsData.Update();
                }
                else
                {
                    // this is the difference when it is a copy
                    if (boolPayMortCert)
                    {
                        // if the user is getting charged for certified mail, then save their info to print on Cert Mail Forms
                        boolPrintCMF = true;
                    }
                    else
                    {
                        boolPrintCMF = false;
                    }

                    // address bar
                    // If rsMort.EndOfFile <> True Then
                    // info for CMFNumbers table
                    // strCMFName = rsMort.Fields("Name")
                    // lngCMFMHNumber = rsMort.Fields("MortgageAssociation.MortgageHolderID")
                    // boolNewOwner = False
                    // lngCMFBillKey = 0
                    // strCMFAddr1 = rsMort.Fields("Address1")
                    // If Trim(rsMort.Fields("Zip4")) <> "" Then
                    // strCMFAddr2 = strAddressBar & vbCrLf & PadStringWithSpaces("    " & rsMort.Fields("CITY") & ", " & rsMort.Fields("STATE") & " " & rsMort.Fields("ZIP") & "-" & rsMort.Fields("ZIP4"), 47, False)
                    // Else
                    // strCMFAddr2 = strAddressBar & vbCrLf & PadStringWithSpaces("    " & rsMort.Fields("CITY") & ", " & rsMort.Fields("STATE") & " " & rsMort.Fields("ZIP"), 47, False)
                    // End If
                    // strCMFAddr3 = ""
                    // 
                    // strAddressBar = PadStringWithSpaces("    " & rsMort.Fields("Name"), 40, False) & PadStringWithSpaces("Tax Payer:", 11, False) & rsData.Fields("Name1")
                    // this gets the user info
                    // If Trim(rsData.Fields("Address1")) <> "" Then
                    // strTemp = rsData.Fields("Address1")
                    // strTemp2 = rsData.Fields("Address2")
                    // strTemp3 = rsData.Fields("Address3")
                    // Else
                    // strTemp = rsData.Fields("Address2")
                    // strTemp2 = rsData.Fields("Address3")
                    // strTemp3 = ""
                    // End If
                    // 
                    // If Trim(rsMort.Fields("Address1")) <> "" Then
                    // strAddressBar = strAddressBar & vbCrLf & PadStringWithSpaces("    " & rsMort.Fields("Address1"), 47, False) & strTemp
                    // If Trim(rsMort.Fields("Zip4")) <> "" Then
                    // strAddressBar = strAddressBar & vbCrLf & PadStringWithSpaces("    " & rsMort.Fields("CITY") & ", " & rsMort.Fields("STATE") & " " & rsMort.Fields("ZIP") & "-" & rsMort.Fields("ZIP4"), 47, False) & strTemp2
                    // Else
                    // strAddressBar = strAddressBar & vbCrLf & PadStringWithSpaces("    " & rsMort.Fields("CITY") & ", " & rsMort.Fields("STATE") & " " & rsMort.Fields("ZIP"), 47, False) & strTemp2
                    // End If
                    // strTemp = ""
                    // strTemp2 = ""
                    // Else
                    // If Trim(rsMort.Fields("Zip4")) <> "" Then
                    // strAddressBar = strAddressBar & vbCrLf & PadStringWithSpaces("    " & rsMort.Fields("CITY") & ", " & rsMort.Fields("STATE") & " " & rsMort.Fields("ZIP") & "-" & rsMort.Fields("ZIP4"), 47, False) & strTemp
                    // Else
                    // strAddressBar = strAddressBar & vbCrLf & PadStringWithSpaces("    " & rsMort.Fields("CITY") & ", " & rsMort.Fields("STATE") & " " & rsMort.Fields("ZIP"), 47, False) & strTemp
                    // End If
                    // strTemp = ""
                    // End If
                    // 
                    // If Trim(strTemp2) <> "" Then
                    // need to add two lines
                    // strAddressBar = strAddressBar & vbCrLf & PadStringWithSpaces(" ", 47, False) & strTemp2
                    // End If
                    // add the last line if needed
                    // If Trim(strTemp3) <> "" Then
                    // need to add two lines
                    // strAddressBar = strAddressBar & vbCrLf & PadStringWithSpaces(" ", 47, False) & strTemp3
                    // End If
                    // 
                    // set name and address bar for a mortgage holder
                    // frfFreeReport(27).DatabaseFieldName = strAddressBar
                    // rsMort.MoveNext
                    // ElseIf boolNewOwner Then                            'this is the last copy to be made, it is for the new owner
                    // boolNewOwner = False                            'this will set it off for the next account
                    // 
                    // info for CMFNumbers table
                    // strCMFName = rsRE.Fields("RSName")
                    // lngCMFMHNumber = 0
                    // boolNewOwner = True
                    // lngCMFBillKey = 0
                    // strCMFAddr1 = rsData.Fields("Address1")
                    // strCMFAddr2 = rsData.Fields("Address2")
                    // strCMFAddr3 = rsData.Fields("Address3")
                    // 
                    // strAddressBar = PadStringWithSpaces("    " & rsRE.Fields("RSName"), 40, False) & PadStringWithSpaces("Tax Payer:", 11, False) & rsData.Fields("Name1")
                    // 
                    // If Trim(rsData.Fields("Address1")) <> "" Then
                    // strTemp = rsData.Fields("Address1")
                    // strTemp2 = rsData.Fields("Address2")
                    // strTemp3 = rsData.Fields("Address3")
                    // Else
                    // strTemp = rsData.Fields("Address2")
                    // strTemp2 = rsData.Fields("Address3")
                    // strTemp3 = ""
                    // End If
                    // 
                    // If Trim(rsRE.Fields("RSAddr1")) <> "" Then
                    // strAddressBar = strAddressBar & vbCrLf & PadStringWithSpaces("    " & rsRE.Fields("RSAddr1"), 47, False) & strTemp
                    // strAddressBar = strAddressBar & vbCrLf & PadStringWithSpaces("    " & rsRE.Fields("RSAddr2"), 47, False) & strTemp2
                    // strTemp = ""
                    // strTemp2 = ""
                    // Else
                    // strAddressBar = strAddressBar & vbCrLf & PadStringWithSpaces("    " & rsRE.Fields("RSAddr2"), 47, False) & strTemp
                    // strTemp = ""
                    // End If
                    // 
                    // If Trim(strTemp2) <> "" Then
                    // need to add two lines
                    // strAddressBar = strAddressBar & vbCrLf & PadStringWithSpaces("    " & rsRE.Fields("RSAddr3"), 47, False) & strTemp2
                    // End If
                    // add the last line if needed
                    // If Trim(strTemp3) <> "" Then
                    // need to add two lines
                    // strAddressBar = strAddressBar & vbCrLf & PadStringWithSpaces(" ", 55, False) & strTemp3
                    // End If
                    // 
                    // set name and address bar for a mortgage holder
                    // frfFreeReport(27).DatabaseFieldName = strAddressBar
                    // rsRE.MoveNext
                    // Else
                    // strAddressBar = "    " & rsData.Fields("Name1")
                    // If Trim(rsData.Fields("Address1")) <> "" Then
                    // strAddressBar = strAddressBar & vbCrLf & "    " & rsData.Fields("Address1")
                    // End If
                    // strAddressBar = strAddressBar & vbCrLf & "    " & GetVariableValue("MUNI") & ", " & GetVariableValue("STATE") & " " & GetVariableValue("ZIP")
                    // 
                    // set name and address bar for a mortgage holder
                    // frfFreeReport(27).DatabaseFieldName = strAddressBar
                    // End If
                    // 
                    // this will add the information to the CMFNumber table so that it will be
                    // easier to figure out which accounts need certified mail forms to be printed for them
                    // If boolPrintCMF Then
                    // rsCMFNumbers.AddNew
                    // rsCMFNumbers.Fields("Name") = strCMFName
                    // rsCMFNumbers.Fields("Billkey") = lngCMFBillKey
                    // rsCMFNumbers.Fields("MortgageHolder") = lngCMFMHNumber
                    // rsCMFNumbers.Fields("Account") = rsData.Fields("Account")
                    // rsCMFNumbers.Fields("NewOwner") = boolNewOwner
                    // rsCMFNumbers.Fields("Type") = 20
                    // rsCMFNumbers.Update True
                    // End If
                }

                return;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                frmWait.InstancePtr.Unload();
                FCMessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message,
                    MsgBoxStyle.Critical,
                    "Calculate Variable Error - " + FCConvert.ToString(rsData.Get_Fields_Int32("BillKey")));
            }
            finally
            {
				rsRK.Dispose();
            }
		}

		private int CalculateMortgageHolders()
		{
			int CalculateMortgageHolders = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will return the number of mortgage holders this account has
				// rsMort.OpenRecordset "SELECT * FROM MortgageAssociation INNER JOIN MortgageHolders ON MortgageAssociation.MortgageHolderID = MortgageHolders.MortgageHolderID WHERE Account = " & rsData.Fields("Account") & " AND Module = 'RE'", "SystemSettings"
				// If rsMort.EndOfFile <> True Then
				// CalculateMortgageHolders = rsMort.RecordCount
				// Else
				CalculateMortgageHolders = 0;
				// End If
				return CalculateMortgageHolders;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				CalculateMortgageHolders = 0;
			}
			return CalculateMortgageHolders;
		}
		// Private Function NewOwner() As Boolean
		// On Error GoTo ERROR_HANDLER
		// this function will find out if there has been a new owner since the billing date
		//
		// NewOwner = False
		// If gboolRE Then
		// rsRE.FindFirstRecord , , "RSAccount = " & rsData.Fields("Account")
		// If Not rsRE.NoMatch Then
		// If rsRE.Fields("RSName") <> rsData.Fields("Name1") Then
		// NewOwner = True
		// End If
		// End If
		// End If
		// Exit Function
		// ERROR_HANDLER:
		// NewOwner = False
		// End Function
		public void SaveLienStatus()
		{
			string strTemp;
			string strAcct = "";
			int lngCommaPosition = 0;
			if (Strings.Right(strAcctList, 1) == ",")
			{
				strAcctList = Strings.Left(strAcctList, strAcctList.Length - 1);
			}
			strTemp = strAcctList;
			// this statement will change the status of the accounts that have been process
			if (Strings.Trim(strAcctList) != "")
			{
				while (!(strTemp == ""))
				{
					lngCommaPosition = Strings.InStr(1, strTemp, ",");
					if (lngCommaPosition > 0)
					{
						strAcct = Strings.Left(strTemp, lngCommaPosition - 1);
					}
					else
					{
						strAcct = strTemp;
						strTemp = "";
					}
					strTemp = Strings.Right(strTemp, strTemp.Length - lngCommaPosition);
					if (Strings.Trim(strAcct) != "")
					{
						rsData.Execute("UPDATE BillingMaster SET LienProcessStatus = 1, LienStatusEligibility = 2 WHERE BillingYear/10 = " + FCConvert.ToString(intYear) + " AND Account = " + strAcct, modExtraModules.strCLDatabase);
					}
				}
			}
		}

		
	}
}
