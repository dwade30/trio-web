﻿namespace TWCL0000
{
	/// <summary>
	/// Summary description for rptCertificateOfSettlement.
	/// </summary>
	partial class rptCertificateOfSettlement
	{
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptCertificateOfSettlement));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lblTitleBar = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblLegalDescription = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldMainText = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCommitment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCounty = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSupplement = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTopTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCashPayments = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAbatements = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTaxLiens = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOtherCredits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNetTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBalanceDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCommitment = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSupplemental = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblInterest = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTopTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCashPayments = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAbatements = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTaxLiens = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblOtherCredits = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblNetTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBalanceDue = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldText2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblDated1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMunicipalOfficers = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lnMO = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblReportVersion = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.lblTitleBar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLegalDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMainText)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCommitment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCounty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSupplement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTopTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCashPayments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAbatements)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTaxLiens)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldOtherCredits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldNetTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBalanceDue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCommitment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSupplemental)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTopTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCashPayments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAbatements)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTaxLiens)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOtherCredits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNetTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBalanceDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldText2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDated1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMunicipalOfficers)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportVersion)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldMainText,
				this.fldCommitment,
				this.fldCounty,
				this.fldState,
				this.fldSupplement,
				this.fldInterest,
				this.fldTopTotal,
				this.fldCashPayments,
				this.fldAbatements,
            this.fldTaxLiens,
            this.fldOtherCredits,
            this.fldNetTotal,
            this.fldBalanceDue,
            this.lblCommitment,
            this.lblSupplemental,
            this.lblInterest,
            this.lblTopTotal,
            this.lblCashPayments,
            this.lblAbatements,
            this.lblTaxLiens,
            this.lblOtherCredits,
            this.lblNetTotal,
            this.lblBalanceDue,
            this.fldText2,
            this.lblDated1,
				this.lblMunicipalOfficers,
				this.lnMO,
				this.Line1,
				this.Line2,
				this.Line3,
				this.Line4,
				this.lblReportVersion
			});
			this.Detail.Height = 7.302083F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblTitleBar,
				this.lblLegalDescription
			});
			this.PageHeader.Height = 0.7604167F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// lblTitleBar
			// 
			this.lblTitleBar.Height = 0.3F;
			this.lblTitleBar.HyperLink = null;
			this.lblTitleBar.Left = 0F;
			this.lblTitleBar.Name = "lblTitleBar";
			this.lblTitleBar.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblTitleBar.Text = "Certificate Of Settlement";
			this.lblTitleBar.Top = 0F;
			this.lblTitleBar.Width = 7F;
			// 
			// lblLegalDescription
			// 
			this.lblLegalDescription.Height = 0.3F;
			this.lblLegalDescription.HyperLink = null;
			this.lblLegalDescription.Left = 0F;
			this.lblLegalDescription.Name = "lblLegalDescription";
			this.lblLegalDescription.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: center";
			this.lblLegalDescription.Text = "Title 36, M.R.S.A. Section 943";
			this.lblLegalDescription.Top = 0.3F;
			this.lblLegalDescription.Width = 7F;
			// 
			// fldMainText
            // 
            this.fldMainText.Height = 0.47F;
            this.fldMainText.Left = 0F;
            this.fldMainText.Name = "fldMainText";
            this.fldMainText.Style = "font-family: \'Tahoma\'; font-size: 12pt";
            this.fldMainText.Text = null;
            this.fldMainText.Top = 0.5F;
            this.fldMainText.Width = 7F;
            // 
            // fldCommitment
            // 
            this.fldCommitment.Height = 0.1875F;
            this.fldCommitment.Left = 4F;
            this.fldCommitment.Name = "fldCommitment";
            this.fldCommitment.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: right";
            this.fldCommitment.Text = null;
            this.fldCommitment.Top = 1.125F;
            this.fldCommitment.Width = 1.875F;
            // 
            // fldCounty
            // 
            this.fldCounty.CanGrow = false;
            this.fldCounty.Height = 0.24F;
            this.fldCounty.Left = 0F;
            this.fldCounty.Name = "fldCounty";
            this.fldCounty.Style = "font-family: \'Tahoma\'; font-size: 12pt";
            this.fldCounty.Text = null;
            this.fldCounty.Top = 0.125F;
            this.fldCounty.Width = 4F;
            // 
            // fldState
            // 
            this.fldState.CanGrow = false;
            this.fldState.Height = 0.24F;
            this.fldState.Left = 4.125F;
            this.fldState.Name = "fldState";
            this.fldState.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: right";
            this.fldState.Text = null;
            this.fldState.Top = 0.125F;
            this.fldState.Width = 2.875F;
            // 
            // fldSupplement
            // 
            this.fldSupplement.Height = 0.1875F;
            this.fldSupplement.Left = 4F;
            this.fldSupplement.Name = "fldSupplement";
            this.fldSupplement.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: right";
            this.fldSupplement.Text = null;
            this.fldSupplement.Top = 1.375F;
            this.fldSupplement.Width = 1.875F;
            // 
            // fldInterest
            // 
            this.fldInterest.Height = 0.1875F;
            this.fldInterest.Left = 4F;
            this.fldInterest.Name = "fldInterest";
            this.fldInterest.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: right";
            this.fldInterest.Text = null;
            this.fldInterest.Top = 1.625F;
            this.fldInterest.Width = 1.875F;
            // 
            // fldTopTotal
            // 
            this.fldTopTotal.Height = 0.1875F;
            this.fldTopTotal.Left = 4.9375F;
            this.fldTopTotal.Name = "fldTopTotal";
            this.fldTopTotal.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: right";
            this.fldTopTotal.Text = null;
            this.fldTopTotal.Top = 1.875F;
            this.fldTopTotal.Width = 1.875F;
            // 
            // fldCashPayments
            // 
            this.fldCashPayments.Height = 0.1875F;
            this.fldCashPayments.Left = 4F;
            this.fldCashPayments.Name = "fldCashPayments";
            this.fldCashPayments.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: right";
            this.fldCashPayments.Text = null;
            this.fldCashPayments.Top = 2.125F;
            this.fldCashPayments.Width = 1.875F;
            // 
            // fldAbatements
            // 
            this.fldAbatements.Height = 0.1875F;
            this.fldAbatements.Left = 4F;
            this.fldAbatements.Name = "fldAbatements";
            this.fldAbatements.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: right";
            this.fldAbatements.Text = null;
            this.fldAbatements.Top = 2.375F;
            this.fldAbatements.Width = 1.875F;
            // 
            // fldTaxLiens
            // 
            this.fldTaxLiens.Height = 0.1875F;
            this.fldTaxLiens.Left = 4F;
            this.fldTaxLiens.Name = "fldTaxLiens";
            this.fldTaxLiens.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: right";
            this.fldTaxLiens.Text = null;
            this.fldTaxLiens.Top = 2.875F;
            this.fldTaxLiens.Width = 1.875F;
            // 
            // fldOtherCredits
            // 
            this.fldOtherCredits.Height = 0.1875F;
            this.fldOtherCredits.Left = 4F;
            this.fldOtherCredits.Name = "fldOtherCredits";
            this.fldOtherCredits.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: right";
            this.fldOtherCredits.Text = null;
            this.fldOtherCredits.Top = 3.125F;
            this.fldOtherCredits.Width = 1.875F;
            // 
            // fldNetTotal
            // 
            this.fldNetTotal.Height = 0.1875F;
            this.fldNetTotal.Left = 4.9375F;
            this.fldNetTotal.Name = "fldNetTotal";
            this.fldNetTotal.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: right";
            this.fldNetTotal.Text = null;
            this.fldNetTotal.Top = 3.375F;
            this.fldNetTotal.Width = 1.875F;
            // 
            // fldBalanceDue
            // 
            this.fldBalanceDue.Height = 0.1875F;
            this.fldBalanceDue.Left = 4.9375F;
            this.fldBalanceDue.Name = "fldBalanceDue";
            this.fldBalanceDue.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: right";
            this.fldBalanceDue.Text = null;
            this.fldBalanceDue.Top = 3.625F;
            this.fldBalanceDue.Width = 1.875F;
            // 
            // lblCommitment
            // 
            this.lblCommitment.Height = 0.24F;
            this.lblCommitment.HyperLink = null;
            this.lblCommitment.Left = 0F;
            this.lblCommitment.Name = "lblCommitment";
            this.lblCommitment.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: left";
            this.lblCommitment.Text = null;
            this.lblCommitment.Top = 1.125F;
            this.lblCommitment.Width = 4F;
            // 
            // lblSupplemental
            // 
            this.lblSupplemental.Height = 0.24F;
            this.lblSupplemental.HyperLink = null;
            this.lblSupplemental.Left = 0F;
            this.lblSupplemental.Name = "lblSupplemental";
            this.lblSupplemental.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: left";
            this.lblSupplemental.Text = null;
            this.lblSupplemental.Top = 1.375F;
            this.lblSupplemental.Width = 4F;
            // 
            // lblInterest
            // 
            this.lblInterest.Height = 0.24F;
            this.lblInterest.HyperLink = null;
            this.lblInterest.Left = 0F;
            this.lblInterest.Name = "lblInterest";
            this.lblInterest.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: left";
            this.lblInterest.Text = null;
            this.lblInterest.Top = 1.625F;
            this.lblInterest.Width = 4F;
            // 
            // lblTopTotal
            // 
            this.lblTopTotal.Height = 0.24F;
            this.lblTopTotal.HyperLink = null;
            this.lblTopTotal.Left = 0F;
            this.lblTopTotal.Name = "lblTopTotal";
            this.lblTopTotal.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: left";
            this.lblTopTotal.Text = null;
            this.lblTopTotal.Top = 1.875F;
            this.lblTopTotal.Width = 4F;
            // 
            // lblCashPayments
            // 
            this.lblCashPayments.Height = 0.24F;
            this.lblCashPayments.HyperLink = null;
            this.lblCashPayments.Left = 0F;
            this.lblCashPayments.Name = "lblCashPayments";
            this.lblCashPayments.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: left";
            this.lblCashPayments.Text = null;
            this.lblCashPayments.Top = 2.125F;
            this.lblCashPayments.Width = 4F;
            // 
            // lblAbatements
            // 
            this.lblAbatements.Height = 0.24F;
            this.lblAbatements.HyperLink = null;
            this.lblAbatements.Left = 0F;
            this.lblAbatements.Name = "lblAbatements";
            this.lblAbatements.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: left";
            this.lblAbatements.Text = null;
            this.lblAbatements.Top = 2.375F;
            this.lblAbatements.Width = 4F;
            // 
            // lblTaxLiens
            // 
            this.lblTaxLiens.Height = 0.4375F;
            this.lblTaxLiens.HyperLink = null;
            this.lblTaxLiens.Left = 0F;
            this.lblTaxLiens.Name = "lblTaxLiens";
            this.lblTaxLiens.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: left";
            this.lblTaxLiens.Text = null;
            this.lblTaxLiens.Top = 2.625F;
            this.lblTaxLiens.Width = 4.937F;
            // 
            // lblOtherCredits
            // 
            this.lblOtherCredits.Height = 0.24F;
            this.lblOtherCredits.HyperLink = null;
            this.lblOtherCredits.Left = 0F;
            this.lblOtherCredits.Name = "lblOtherCredits";
            this.lblOtherCredits.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: left";
            this.lblOtherCredits.Text = null;
            this.lblOtherCredits.Top = 3.125F;
            this.lblOtherCredits.Width = 4F;
            // 
            // lblNetTotal
            // 
            this.lblNetTotal.Height = 0.24F;
            this.lblNetTotal.HyperLink = null;
            this.lblNetTotal.Left = 0F;
            this.lblNetTotal.Name = "lblNetTotal";
            this.lblNetTotal.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: left";
            this.lblNetTotal.Text = null;
            this.lblNetTotal.Top = 3.375F;
            this.lblNetTotal.Width = 4F;
            // 
            // lblBalanceDue
            // 
            this.lblBalanceDue.Height = 0.24F;
            this.lblBalanceDue.HyperLink = null;
            this.lblBalanceDue.Left = 0F;
            this.lblBalanceDue.Name = "lblBalanceDue";
            this.lblBalanceDue.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: left";
            this.lblBalanceDue.Text = null;
            this.lblBalanceDue.Top = 3.625F;
            this.lblBalanceDue.Width = 4F;
            // 
            // fldText2
            // 
            this.fldText2.Height = 0.875F;
            this.fldText2.Left = 0F;
            this.fldText2.Name = "fldText2";
            this.fldText2.Style = "font-family: \'Tahoma\'; font-size: 12pt";
            this.fldText2.Text = null;
            this.fldText2.Top = 3.9375F;
            this.fldText2.Width = 7F;
            // 
            // lblDated1
            // 
            this.lblDated1.Height = 0.24F;
            this.lblDated1.HyperLink = null;
            this.lblDated1.Left = 0F;
            this.lblDated1.Name = "lblDated1";
            this.lblDated1.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: left";
            this.lblDated1.Text = null;
            this.lblDated1.Top = 5F;
            this.lblDated1.Width = 7F;
            // 
            // lblMunicipalOfficers
            // 
            this.lblMunicipalOfficers.Height = 0.24F;
            this.lblMunicipalOfficers.HyperLink = null;
            this.lblMunicipalOfficers.Left = 0.25F;
            this.lblMunicipalOfficers.Name = "lblMunicipalOfficers";
            this.lblMunicipalOfficers.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: center";
            this.lblMunicipalOfficers.Text = null;
            this.lblMunicipalOfficers.Top = 5.25F;
            this.lblMunicipalOfficers.Width = 3.75F;
            // 
            // lnMO
            // 
            this.lnMO.Height = 0F;
            this.lnMO.Left = 0.25F;
            this.lnMO.LineWeight = 1F;
            this.lnMO.Name = "lnMO";
            this.lnMO.Top = 5.6875F;
            this.lnMO.Width = 3.75F;
            this.lnMO.X1 = 0.25F;
            this.lnMO.X2 = 4F;
            this.lnMO.Y1 = 5.6875F;
            this.lnMO.Y2 = 5.6875F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0.25F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 5.9375F;
            this.Line1.Width = 3.75F;
            this.Line1.X1 = 0.25F;
            this.Line1.X2 = 4F;
            this.Line1.Y1 = 5.9375F;
            this.Line1.Y2 = 5.9375F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0.25F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 6.1875F;
            this.Line2.Width = 3.75F;
            this.Line2.X1 = 0.25F;
            this.Line2.X2 = 4F;
            this.Line2.Y1 = 6.1875F;
            this.Line2.Y2 = 6.1875F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 0.25F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 6.4375F;
            this.Line3.Width = 3.75F;
            this.Line3.X1 = 0.25F;
            this.Line3.X2 = 4F;
            this.Line3.Y1 = 6.4375F;
            this.Line3.Y2 = 6.4375F;
            // 
            // Line4
            // 
            this.Line4.Height = 0F;
            this.Line4.Left = 0.25F;
            this.Line4.LineWeight = 1F;
            this.Line4.Name = "Line4";
            this.Line4.Top = 6.6875F;
            this.Line4.Width = 3.75F;
            this.Line4.X1 = 0.25F;
            this.Line4.X2 = 4F;
            this.Line4.Y1 = 6.6875F;
            this.Line4.Y2 = 6.6875F;
            // 
            // lblReportVersion
            // 
            this.lblReportVersion.Height = 0.1875F;
			this.lblReportVersion.HyperLink = null;
			this.lblReportVersion.Left = 0.25F;
			this.lblReportVersion.Name = "lblReportVersion";
			this.lblReportVersion.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.lblReportVersion.Text = null;
			this.lblReportVersion.Top = 6.8125F;
			this.lblReportVersion.Width = 1.875F;
			// 
			// rptCertificateOfSettlement
			// 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.010417F;
            this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.lblTitleBar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLegalDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMainText)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCommitment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCounty)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSupplement)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTopTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCashPayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAbatements)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTaxLiens)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldOtherCredits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldNetTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBalanceDue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCommitment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSupplemental)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTopTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCashPayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAbatements)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTaxLiens)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOtherCredits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNetTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBalanceDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldText2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDated1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMunicipalOfficers)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportVersion)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMainText;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCommitment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCounty;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSupplement;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInterest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTopTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCashPayments;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAbatements;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTaxLiens;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOtherCredits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNetTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBalanceDue;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCommitment;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSupplemental;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblInterest;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTopTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCashPayments;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAbatements;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTaxLiens;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOtherCredits;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblNetTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBalanceDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldText2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDated1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMunicipalOfficers;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnMO;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReportVersion;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitleBar;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLegalDescription;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
