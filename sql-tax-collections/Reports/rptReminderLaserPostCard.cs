﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptReminderLaserPostCard : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               02/13/2006              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               02/13/2006              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsTemp = new clsDRWrapper();
		bool boolDeleteLastPage;
		bool boolReturnAddress;
		string strBulkMailer = "";
		int lngIndex;
		bool boolNoSummary;
		bool boolLienedPayment;
		double dblMinimumAmount;
		string strFields = "";
        string strQuerySQL = "";
        bool boolRE = false;
        ReminderNoticeOptions reminderNoticeOptions;
        private cAddressControllerCL tAddrCtrlr = new cAddressControllerCL();

		public rptReminderLaserPostCard()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Reminder Notice - Laser Post Card";
		}

		public static rptReminderLaserPostCard InstancePtr
		{
			get
			{
				return (rptReminderLaserPostCard)Sys.GetInstance(typeof(rptReminderLaserPostCard));
			}
		}

		protected rptReminderLaserPostCard _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
				rsTemp.Dispose();
            }
			base.Dispose(disposing);
		}

        public void Init(ReminderNoticeOptions options)
		{
            try
			{
                // On Error GoTo ERROR_HANDLER
                // this will be used to pass variables into the report
                reminderNoticeOptions = options;
				int intCopies = 0;     /*unused?*/
                int intStartPage = 0;  /*unused?*/
                int intEndPage = 0;    /*unused?*/
                string strOldPrinter = "";
                int intReturn;         /*unused?*/
                boolRE = modStatusPayments.Statics.boolRE;
               // clsReportPrinterFunctions clsP = new clsReportPrinterFunctions();
                dblMinimumAmount = reminderNoticeOptions.MinimumAmount;
                boolReturnAddress = reminderNoticeOptions.UseReturnAddress;
                boolLienedPayment = reminderNoticeOptions.LienedRecords;
                FCUtils.EraseSafe(modReminderNoticeSummary.Statics.arrReminderSummaryList);
				modReminderNoticeSummary.Statics.arrReminderSummaryList = new modReminderNoticeSummary.ReminderSummaryList[0 + 1];
				lngIndex = 0;
                if (reminderNoticeOptions.BulkMailing)
				{
                    strBulkMailer = modGlobal.GetBulkMailerString();
				}
				else
				{
					strBulkMailer = "";
				}
				rsData.OpenRecordset(reminderNoticeOptions.Query, modExtraModules.strCLDatabase);
				if (rsData.EndOfFile())
				{
					frmWait.InstancePtr.Unload();
					boolNoSummary = true;
					FCMessageBox.Show("No records meet the criteria selected.", MsgBoxStyle.Exclamation, "No Records");
					Cancel();
				}
				else
				{
					boolNoSummary = false;
                    if (boolRE)
                    {
                        if (boolLienedPayment)
                        {
                            strFields =
                                "Name1,Name2,Account,Address1,Address2,Address3,BillingMaster.RateKey,MapLot,StreetNumber,StreetName," +
                                "LienRec.Principal,LienRec.PrincipalPaid,TransferFromBillingDateFirst,TransferFromBillingDateLast,LienRecordNumber";
                        }
                        else
                        {
                            strFields =
                                "Name1,Name2,Account,Address1,Address2,Address3,RateKey,MapLot,StreetNumber,StreetName," +
                                "TaxDue1,TaxDue2,TaxDue3,TaxDue4,PrincipalPaid,TransferFromBillingDateFirst,TransferFromBillingDateLast";
                        }
                    }
                    else
                    {
                        strFields =
                            "Name1,Name2,BillingMaster.Account,BillingMaster.Address1,BillingMaster.Address2,Address3," +
                            "TaxDue1,TaxDue2,TaxDue3,TaxDue4,PrincipalPaid,TransferFromBillingDateFirst,TransferFromBillingDateLast,PartyID";
                    }

                    frmReportViewer.InstancePtr.Init(this, reminderNoticeOptions.Printer);
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Initializing Notices");
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
			//Detail_Format();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			if (boolDeleteLastPage)
			{
				this.Document.Pages.RemoveAt(this.Document.Pages.Count - 1);
				//this.Document.Pages.Commit();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			//this.Zoom = -1;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.Detail.ColumnCount = 2;
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			modReminderNoticeSummary.Statics.intRPTReminderSummaryType = 1;
            //FC:FINAL:SBE - #i331 - in VB6 Show is executed, and standard report viewer is displayed. Use frmReportViewer, because we don't have a standard report viewer
            //rptReminderSummary.InstancePtr.Run();
            fecherFoundation.Sys.ClearInstance(frmReportViewer.InstancePtr);
            frmReportViewer.InstancePtr.Init(rptReminderSummary.InstancePtr);
		}

        private void Detail_Format(object sender, EventArgs e)
        {
            int lngAcct = 0;

            TryAgain:;
            if (!rsData.EndOfFile())
            {
                // MAL@20080115: Check for minimum amount
                lngAcct = FCConvert.ToInt32(rsData.Get_Fields("Account"));
                if (modCLCalculations.CalculateAccountTotal( lngAcct,  boolRE) < dblMinimumAmount)
                {
                    rsData.MoveNext();
                    goto TryAgain;
                }
                else
                {
                    if (boolRE)
                    {
                        if (boolLienedPayment)
                        {

                            // from rptReminderForm trocls-119 ===========================
                            // kk 01082013 trocls-12  Take out InsertName to try to stop OutOfMemory errors
                            strQuerySQL = "SELECT Account,Name1,Name2,BillingMaster.Address1,BillingMaster.Address2,BillingMaster.Address3," + "LienRecordNumber,MapLot,StreetNumber,StreetName,TranCode," + "LienRec.PrincipalPaid,LienRec.Principal," + "TransferFromBillingDateFirst,TransferFromBillingDateLast " + "FROM (BillingMaster INNER JOIN LienRec ON BillingMaster.LienrecordNumber = LienRec.ID)" + "WHERE BillingType = 'RE' AND Account = " + rsData.Get_Fields("Account") + "ORDER BY BillingYear desc";
                            rsTemp.OpenRecordset(strQuerySQL, modExtraModules.strCLDatabase);
                        }
                        else
                        {
                            // from rptReminderForm trocls-119 ===========================
                            // kk 01082013 trocls-12  Take out InsertName to try to stop OutOfMemory errors
                            strQuerySQL = "SELECT Account,Name1,Name2,BillingMaster.Address1,BillingMaster.Address2,BillingMaster.Address3," + "LienRecordNumber,MapLot,StreetNumber,StreetName,TranCode," + "PrincipalPaid,TaxDue1,TaxDue2,TaxDue3,TaxDue4," + "TransferFromBillingDateFirst,TransferFromBillingDateLast " + "FROM  BillingMaster " + "WHERE BillingType = 'RE' AND Account = " + rsData.Get_Fields("Account") + "ORDER BY BillingYear desc";
                            rsTemp.OpenRecordset(strQuerySQL, modExtraModules.strCLDatabase);
                        }
                    }
                    else
                    {
                        // kk 01082013 trocls-12  Take out InsertName to try to stop OutOfMemory errors
                        strQuerySQL = "SELECT BillingMaster.Account,Name1,Name2,BillingMaster.Address1,BillingMaster.Address2,BillingMaster.Address3,BillingMaster.StreetNumber,StreetName," + "PrincipalPaid,TaxDue1,TaxDue2,TaxDue3,TaxDue4,TransferFromBillingDateFirst,TransferFromBillingDateLast " + "FROM  BillingMaster " + "WHERE BillingType = 'PP' AND BillingMaster.Account = " + rsData.Get_Fields("Account") + "ORDER BY BillingYear desc";
                        rsTemp.OpenRecordset(strQuerySQL, modExtraModules.strCLDatabase);
                    }

                    if (boolLienedPayment)
                    {
                        if (rsTemp.Get_Fields("Principal") - rsTemp.Get_Fields("PrincipalPaid") > 0)
                        {
                            PrintPostCards();
                        }
                        else
                        {
                            rsData.MoveNext();
                            goto TryAgain;
                        }
                    }
                    else
                    {
                        if (rsTemp.Get_Fields("TaxDue1") + rsTemp.Get_Fields("TaxDue2") + rsTemp.Get_Fields("TaxDue3") + rsTemp.Get_Fields("TaxDue4") - rsTemp.Get_Fields("PrincipalPaid") > 0)
                        {
                            PrintPostCards();
                        }
                        else
                        {
                            rsData.MoveNext();
                            goto TryAgain;
                        }
                    }

                    rsData.MoveNext();
                }
            }
        }

        private void PrintPostCards()
		{
			// this will fill in the fields for post card
			int intCTRLIndex/*unused?*/;
			string str1 = "";
			string str2 = "";
			string str3 = "";
			string str4 = "";
			string str5 = "";
			double dblDue = 0;
			double dblPaid = 0;
            int lngAcct;
            bool boolNewOwner = false; // trocls-119 10.6.17 kjr 4.23.18 CODE FREEZE
            bool boolNewAddress = false;
            DateTime dtBillDate;
            bool boolSameName = false;
            bool boolREMatch = false;
            string strPrevSecOwnerName = "";
            string strPrevAddress1 = "";
            string strPrevAddress2 = "";
            string strPrevAddress3 = "";

           // cPartyController pCont = new cPartyController();
            // vbPorter upgrade warning: pInfo As CParty	OnWrite(cParty)
          //  cParty pInfo = new cParty();
            cPartyAddress pAdd;
           // clsDRWrapper rsMastOwner = new clsDRWrapper();
           // cParty pInfo2 = new cParty();
            short intDummyVar; // kjr 4.30.18

            GETNEXTACCOUNT:
			;
            lngAcct = FCConvert.ToInt32(rsTemp.Get_Fields("Account"));
            if (Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Name2"))) != "")
			{
				str1 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Name1"))) + " and " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Name2")));
			}
			else
			{
				str1 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Name1")));
			}

            clsAddress tAddr;

            switch (reminderNoticeOptions.AddressType)
            {
	            case ReminderNoticeOptions.AddressEnum.AddressAtBilling:
		            tAddr = tAddrCtrlr.GetOwnerAndAddressAtBilling(rsTemp);
		            break;
	            case ReminderNoticeOptions.AddressEnum.CareOfCurrentOwner:
		            tAddr = tAddrCtrlr.GetOwnerAtBillingCareOfCurrentOwner(rsTemp);
		            break;
	            case ReminderNoticeOptions.AddressEnum.LastKnownAddress:
		            tAddr = tAddrCtrlr.GetBilledOwnerAtLastAddress(rsTemp);
		            break;
	            default:
		            tAddr = new clsAddress();
		            break;
            }

            str2 = tAddr.Get_Address(1).Trim();
            str3 = tAddr.Get_Address(2).Trim();
            str4 = tAddr.Get_Address(3).Trim();
            str5 = tAddr.Get_Address(4).Trim();

			
            if (Strings.Trim(str4) == string.Empty)
			{
				str4 = str5;
				str5 = "";
			}
			if (Strings.Trim(str3) == string.Empty)
			{
				str3 = str4;
				str4 = str5;
				str5 = "";
			}
			if (Strings.Trim(str2) == string.Empty)
			{
				str2 = str3;
				str3 = str4;
				str4 = str5;
				str5 = "";
			}
			if (Strings.Trim(str1) == string.Empty)
			{
				str1 = str2;
				str2 = str3;
				str3 = str4;
				str4 = str5;
				str5 = "";
			}
			fldBulkMailer.Text = strBulkMailer;
			if (boolLienedPayment)
			{
				// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
				dblDue = Conversion.Val(rsTemp.Get_Fields("Principal"));
				dblPaid = Conversion.Val(rsTemp.Get_Fields_Decimal("PrincipalPaid"));
			}
			else
			{
                // this will find all of the taxes due prior to the upcoming period and also how much is due for the next period
                if (reminderNoticeOptions.Period == ReminderNoticeOptions.PeriodEnum.Period1)
                {
					dblDue = Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue1"));
				}
                else if (reminderNoticeOptions.Period == ReminderNoticeOptions.PeriodEnum.Period2)
                {
					dblDue = Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue2"));
				}
                else if (reminderNoticeOptions.Period == ReminderNoticeOptions.PeriodEnum.Period3)
                {
                    dblDue = Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue3"));
				}
                else if (reminderNoticeOptions.Period == ReminderNoticeOptions.PeriodEnum.Period4 || reminderNoticeOptions.Period == ReminderNoticeOptions.PeriodEnum.Outstanding)
				{
                    dblDue = Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue4"));
				}
				dblPaid = Conversion.Val(rsTemp.Get_Fields_Decimal("PrincipalPaid"));
			}
			if (dblDue - dblPaid <= 0)
			{
				rsData.MoveNext();
				if (!rsData.EndOfFile())
				{
					if (modStatusPayments.Statics.boolRE)
					{
						if (boolLienedPayment)
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							rsTemp.OpenRecordset("SELECT " + strFields + " FROM (BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.LienRecordNumber) INNER JOIN " + modGlobal.Statics.strDbRE + "Master ON BillingMaster.Account = Master.RSAccount WHERE BillingType = 'RE' AND Account = " + FCConvert.ToString(rsData.Get_Fields("Account")) + " ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							rsTemp.OpenRecordset("SELECT " + strFields + " FROM BillingMaster INNER JOIN " + modGlobal.Statics.strDbRE + "Master ON BillingMaster.Account = Master.RSAccount WHERE BillingType = 'RE' AND Account = " + FCConvert.ToString(rsData.Get_Fields("Account")) + " ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
						}
						rsTemp.InsertName("Owner1PartyID,SecOwnerPartyID", "Own1,Own2", false, true, true, "", false, "", true, "");
					}
					else
					{
						// rsTemp.OpenRecordset "SELECT * FROM BillingMaster INNER JOIN Master ON BillingMaster.Account = Master.RSAccount WHERE BillingType = 'PP' AND Account = " & rsData.Fields("Account") & " ORDER BY BillingYear desc", strCLDatabase
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsTemp.OpenRecordset("SELECT " + strFields + " FROM BillingMaster INNER JOIN " + modGlobal.Statics.strDbPP + "PPMaster ON BillingMaster.Account = PPMaster.Account WHERE BillingType = 'PP' AND Account = " + FCConvert.ToString(rsData.Get_Fields("Account")) + " ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
						rsTemp.InsertName("PartyID", "Own1", false, true, true, "", false, "", true, "");
					}
					goto GETNEXTACCOUNT;
				}
				else
				{
					boolDeleteLastPage = true;
					return;
				}
			}
			if (boolReturnAddress)
			{
                // Return Address
                lblReturnAddress1.Text = reminderNoticeOptions.ReturnAddress1;
                lblReturnAddress2.Text = reminderNoticeOptions.ReturnAddress2;
                lblReturnAddress3.Text = reminderNoticeOptions.ReturnAddress3;
                lblReturnAddress4.Text = reminderNoticeOptions.ReturnAddress4;
			}
            Array.Resize(ref modReminderNoticeSummary.Statics.arrReminderSummaryList, lngIndex + 1);
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex] = new modReminderNoticeSummary.ReminderSummaryList(0);
			modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Name1 = str1;
			modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr1 = str2;
			modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr1 = str3;
			modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr1 = str4;
			modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr1 = str5;
			modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Total = dblDue - dblPaid;
			// Account Number
			if (modStatusPayments.Statics.boolRE)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				lblAccount.Text = "Account: " + FCConvert.ToString(rsTemp.Get_Fields("Account"));
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Account = FCConvert.ToInt32(rsTemp.Get_Fields("Account"));
				lblMapLot.Text = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("MapLot")));
				// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
				if (Conversion.Val(rsTemp.Get_Fields("StreetNumber")) != 0)
				{
					// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
					lblLocation.Text = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("StreetNumber")) + " " + FCConvert.ToString(rsTemp.Get_Fields_String("StreetName")));
				}
				else
				{
					lblLocation.Text = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("StreetName")));
				}
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				lblAccount.Text = "Account: " + FCConvert.ToString(rsTemp.Get_Fields("Account"));
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Account = FCConvert.ToInt32(rsTemp.Get_Fields("Account"));
			}
			// increment the index of the array
			lngIndex += 1;
            // Message
            fldMessage.Text = ForceHardReturns_6(reminderNoticeOptions.Message, 48);
			// Name
            lblName.Text = str1;
			// Mailing Address
			lblMailingAddress1.Text = str2;
			lblMailingAddress2.Text = str3;
			lblMailingAddress3.Text = str4;
			lblMailingAddress4.Text = str5;
			// move to the next record
			rsTemp.MoveNext();
		}

		private string ForceHardReturns_2(string strText, int lngChars)
		{
			return ForceHardReturns(ref strText, ref lngChars);
		}

		private string ForceHardReturns_6(string strText, int lngChars)
		{
			return ForceHardReturns(ref strText, ref lngChars);
		}

		private string ForceHardReturns(ref string strText, ref int lngChars)
		{
			string ForceHardReturnsRet = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngNext;
				string strLine = "";
				lngNext = 1;
				if (strText.Length <= lngChars)
				{
					ForceHardReturnsRet = ForceHardReturnsRet + strText;
				}
				else
				{
					lngNext = Strings.InStr(lngChars, strText, " ");
					strLine = Strings.Left(strText, lngNext);
					ForceHardReturnsRet = ForceHardReturnsRet + strLine + "\r\n" + ForceHardReturns_2(Strings.Right(strText, strText.Length - lngNext), lngChars);
				}
				return ForceHardReturnsRet;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Forcing Hard Returns");
			}
			return ForceHardReturnsRet;
		}

		
	}
}
