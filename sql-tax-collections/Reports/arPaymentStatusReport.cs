﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class arPaymentStatusReport : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// Date           :               09/26/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// Last Updated   :               01/31/2003              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		string strSQL = "";

		public arPaymentStatusReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Payment Report";
		}

		public static arPaymentStatusReport InstancePtr
		{
			get
			{
				return (arPaymentStatusReport)Sys.GetInstance(typeof(arPaymentStatusReport));
			}
		}

		protected arPaymentStatusReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode==vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			SetupFields();
			rsData.OpenRecordset(strSQL, modGlobal.DEFAULTDATABASE);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void BindFields()
		{
			if (rsData.EndOfFile() != true)
			{
				// fill the fields
				// move to the next record
				rsData.MoveNext();
			}
		}

		private void SetupFields()
		{
			//switch (modCustomReport.strReportType) {
			//		break;
			//	}
			//} //end switch
		}

		
	}
}
