using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptAuditStatusLien : BaseSectionReport
	{
        private TaxCollectionStatusReportConfiguration reportConfiguration;
        private rptAuditStatusAll parentReport;
        string strSQL;
		clsDRWrapper rsData = new clsDRWrapper();
		double[] dblTotals = new double[6 + 1];
		bool boolRTError;
		int lngCount;
		bool boolStarted;
		int intSumRows;
		double dblPDTotal;
		bool boolAdjustedSummary;
		bool boolSummaryOnly;
		bool boolDeletedAccount;
		double dblTotOSP;
		double dblTotOSPLI;
		double dblTotOSI;
		double dblTotOSC;
		double dblOSP;
		double dblOSPLI;
		double dblOSI;
		double dblOSC;
		int lngTAType;
		// 0 = all, 1 = TA, 2 = non TA
		clsDRWrapper rsMaster = new clsDRWrapper();
		Dictionary<object, object> dctAccounts = new Dictionary<object, object>();
		// kk01132014 trocls-13
		// these are for the summaries at the bottom of the report
		double[] dblYearTotals = new double[2000 + 1];
		// billingyear - 19800
		int[] lngArrBillCounts = new int[2000 + 1];
		// kk01132015 trocls-13
		double[,] dblPayments = new double[10 + 1, 5 + 1];
		// 0 - 3, 1 - A, 2 - C, 3 - D, 4 - I, 5 - L, 6 - P, 7 - U, 8 - X, 9 - Y, 10 - Total
		// 0 - Principal, 1 - PreLienInterest, 2 - CurrentInterest, 3 - Costs
		public rptAuditStatusLien()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Outstanding Lien Balance Report";
		}

        public void SetReportOptionAndParent(TaxCollectionStatusReportConfiguration reportConfig, rptAuditStatusAll parent)
        {
            reportConfiguration = reportConfig;
            parentReport = parent;
        }
        public static rptAuditStatusLien InstancePtr
		{
			get
			{
				return (rptAuditStatusLien)Sys.GetInstance(typeof(rptAuditStatusLien));
			}
		}

		protected rptAuditStatusLien _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
				rsMaster.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsData.EndOfFile())
			{
				// If boolStarted Then
				eArgs.EOF = true;
				// Else
				// 
				// End If
			}
			else
			{
				eArgs.EOF = false;
			}
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_PageEnd(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "t");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			lngCount = 0;
			boolSummaryOnly = reportConfiguration.Options.ShowSummaryOnly;
            if (reportConfiguration.Filter.IsTaxAcquired.HasValue)
            {
                if (reportConfiguration.Filter.IsTaxAcquired.Value)
                {
                    lngTAType = 1;
                }
                else
                {
                    lngTAType = 2;
                }
            }
            else
            {
                lngTAType = 0;
            }
			
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading");
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			boolRTError = false;
			SetupFields();
			// Moves/shows the correct fields into the right places
			SetReportHeader();
			// Sets the titles and moves labels in the header
			boolStarted = false;
			strSQL = BuildSQL();
			// Generates the SQL String
			if (lngTAType > 0)
			{
				// kk01132015 trocls-22
				rsMaster.OpenRecordset("SELECT RSAccount, TaxAcquired FROM Master where TaxAcquired = 1", modExtraModules.strREDatabase);
			}
			rsData.OpenRecordset(strSQL, modGlobal.DEFAULTDATABASE);
			//FC:BCU - controls can only be created on ReportStart
			//
			//create summary rows
			for (int i = 2; i < 22; i++)
			{
				AddSummaryRow(i, 0, 0, 0, true);
			}
			//create summary total
			GrapeCity.ActiveReports.SectionReportModel.TextBox fldSummaryTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			GrapeCity.ActiveReports.SectionReportModel.TextBox fldSumCountTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			GrapeCity.ActiveReports.SectionReportModel.Label lblPerDiemTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			GrapeCity.ActiveReports.SectionReportModel.Line lnFooterSummaryTotal = new GrapeCity.ActiveReports.SectionReportModel.Line();
			fldSummaryTotal.Name = "fldSummaryTotal";
			fldSumCountTotal.Name = "fldSumCountTotal";
			lblPerDiemTotal.Name = "lblPerDiemTotal";
			lnFooterSummaryTotal.Name = "lnFooterSummaryTotal";
			ReportFooter.Controls.Add(fldSummaryTotal);
			ReportFooter.Controls.Add(fldSumCountTotal);
			ReportFooter.Controls.Add(lblPerDiemTotal);
			ReportFooter.Controls.Add(lnFooterSummaryTotal);
			//
		}

		private string BuildSQL()
		{
			string BuildSQL = "";
			string strTemp = "";
			string strWhereClause;
			string strREPPBill = "";
			string strREPPPayment = "";
			int intCT;
			string strSupp = "";
			string strFields;
            var strAnd = "";
			if (modStatusPayments.Statics.boolRE)
			{
				// only get the accounts for the correct module
				strREPPBill = "'RE'";
			}
			else
			{
				strREPPBill = "'PP'";
			}
			if (rptOutstandingBalancesAll.InstancePtr.intSuppReportType == 1)
			{
				strSupp = " AND BillingYear % 10 > 1 ";
			}
			strWhereClause = "WHERE BillingType = " + strREPPBill + strSupp;

            var filter = reportConfiguration.Filter;
            if (filter.AccountRangeUsed())
            {
                if (filter.AccountMax.HasValue)
                {
                    if (filter.AccountMin.HasValue)
                    {
                        if (filter.AccountMin == filter.AccountMax)
                        {
                            strTemp += "Account = " + filter.AccountMin.Value.ToString();
                        }
                        else
                        {
                            strTemp += "(Account Between " + filter.AccountMin.Value.ToString() + " and " + filter.AccountMax.Value.ToString() + ") ";
                        }
                    }
                    else
                    {
                        strTemp += "Account <= " + filter.AccountMax.Value.ToString();
                    }
                }
                else
                {
                    strTemp += "Account >= " + filter.AccountMin.Value.ToString();
                }

                strAnd = " and ";
            }

            if (filter.NameRangeUsed())
            {
                strTemp += strAnd;

                if (!String.IsNullOrWhiteSpace(filter.NameMin))
                {
                    if (!String.IsNullOrWhiteSpace(filter.NameMax))
                    {
                        strTemp += "Name1 between '" + filter.NameMin + "' and '" + filter.NameMax + "'";
                    }
                    else
                    {
                        strTemp += "Name1 >= '" + filter.NameMin + "'";
                    }
                }
                else
                {
                    strTemp += "Name1 <= '" + filter.NameMax + "zzzz'";
                }

                strAnd = " and ";
            }

            if (filter.TaxYearRangeUsed())
            {
                strTemp += strAnd;

                if (filter.TaxYearMin.HasValue)
                {
                    if (filter.TaxYearMax.HasValue)
                    {
                        strTemp += "billingyear between " + filter.TaxYearMin.ToString() + " and " +
                                   filter.TaxYearMax.ToString();
                    }
                    else
                    {
                        strTemp += "billingyear = " + filter.TaxYearMin.ToString();
                    }
                }
                else
                {
                    strTemp += "billingyear = " + filter.TaxYearMax.ToString();
                }

                strAnd = " and ";
            }

            if (filter.TranCodeRangeUsed())
            {
                strTemp += strAnd;

                if (filter.TranCodeMax.HasValue)
                {
                    if (filter.TranCodeMin.HasValue)
                    {
                        strTemp += "Trancode between " + filter.TranCodeMin.Value.ToString() + " and " +
                                   filter.TranCodeMax.Value.ToString();
                    }
                    else
                    {
                        strTemp += "Trancode <= " + filter.TranCodeMax.Value.ToString();
                    }
                }
                else
                {
                    strTemp += "Trancode >= " + filter.TranCodeMin.Value.ToString();
                }

                strAnd = " and ";
            }

            if (filter.RateRecordRangeUsed())
            {
                strTemp += strAnd;

                if (filter.RateRecordMax.HasValue)
                {
                    if (filter.RateRecordMin.HasValue)
                    {
                        strTemp += " LienRec.RateKey between " + filter.RateRecordMin.Value.ToString() + " and " +
                                   filter.RateRecordMax.Value.ToString();
                    }
                    else
                    {
                        strTemp += "LienRec.RateKey <= " + filter.RateRecordMax.Value.ToString();
                    }
                }
                else
                {
                    strTemp += "LienRec.RateKey <= " + filter.RateRecordMin.Value.ToString();
                }

                strAnd = " and ";
            }

			if (Strings.Trim(strTemp) != "")
			{
				strWhereClause += " AND " + strTemp;
			}
			strFields = "Account, TransferFromBillingDateFirst, BillingYear, Name1, Name2, BillingMaster.RateKey AS BRateKey, LienRec.InterestCharged AS InterestCharged, LienRec.RateKey AS RateKey, LienRec.InterestAppliedThroughDate AS InterestAppliedThroughDate, Principal, Interest, Costs, LienRec.PrincipalPaid AS PrincipalPaid, LienRec.InterestPaid AS InterestPaid, PLIPaid, MaturityFee, CostsPaid, TransferFromBillingDateLast, LienRec.ID AS LRN ";
			if (reportConfiguration.Options.UseAsOfDate())
			{
				// MAL@20080624: Add check for As Of Date
				// Tracker Reference: 14306
				strWhereClause += " AND ISNULL(DateCreated, '12/30/1899') <= '" + FCConvert.ToString(reportConfiguration.Options.AsOfDate) + "'";
				strTemp = "SELECT " + strFields + " FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.ID " + strWhereClause + " ORDER BY Name1, Account, BillingYear";
			}
			else
			{
				strTemp = "SELECT * FROM (" + GetOSLienBalQuery() + ") AS qOSLnBal " + strWhereClause + " ORDER BY Name1, Account, BillingYear";
			}
			BuildSQL = strTemp;
			return BuildSQL;
		}


        private string GetOSLienBalQuery()
        {
            return  "SELECT LienRec.*,b.BillingYear,b.RateKey as BRateKey,b.BillingType,b.LienRecordNumber AS LRN,b.Account,b.Name1 " +
            "FROM BillingMaster b INNER JOIN LienRec ON b.LienRecordNumber = LienRec.ID " +
            "WHERE ISNULL(LienRec.Principal,0) <> ISNULL(LienRec.PrincipalPaid,0)";
        }
		private void SetupFields()
		{
			// Set each field and label's visible property to True/False depending on which fields the user
			// has selected from the Custom Report screen then move them accordingly
			int intRow;
			int intRow2/*unused?*/;
			float lngHt;
			if (boolSummaryOnly)
			{
				lblYear.Visible = false;
				fldYear.Visible = false;
				fldName.Visible = false;
				fldAccount.Visible = false;
				fldPaymentReceived.Visible = false;
				fldTaxDue.Visible = false;
				fldDue.Visible = false;
				lnHeader.Visible = false;
				lnTotals.Visible = false;
				fldType.Visible = false;
				fldOSP.Visible = false;
				fldOSPLI.Visible = false;
				fldOSI.Visible = false;
				fldOSC.Visible = false;
				Detail.Height = 0;
				return;
			}
			intRow = 2;
			lngHt = 270 / 1440F;
			lblYear.Visible = true;
			fldYear.Visible = true;
			// if the year is not shown, then make the name field smaller
			fldName.Width = fldYear.Left - (fldType.Left + fldType.Width);
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the information into the fields
				clsDRWrapper rsPayment = new clsDRWrapper();
				//clsDRWrapper rsRE = new clsDRWrapper();
				//clsDRWrapper rsCalLien;
				//clsDRWrapper rsRate = new clsDRWrapper();
				string strTemp = "";
				double dblTotalPayment/*unused?*/;
				double dblTotalAbate/*unused?*/;
				double dblTotalRefundAbate/*unused?*/;
				double dblCurInt/*unused?*/;
				double dblLienCurInt/*unused?*/;
				DateTime dtPaymentDate;
				bool boolREInfo/*unused?*/;
				double dblIntPaid;
				double dblCostPaid;
				double dblPaymentRecieved;
				double dblXtraInt = 0;
				double dblTotalDue;
				double dblPreLienIntNeed;
				double dblLienCostNeed;
				TRYAGAIN:
				;
				dblPaymentRecieved = 0;
				dblIntPaid = 0;
				dblCostPaid = 0;
				fldDue.Text = "";
				fldTaxDue.Text = "";
				fldPaymentReceived.Text = "";
				fldAccount.Text = "";
				fldYear.Text = "";
				fldName.Text = "";
				fldType.Text = "";
				dblOSP = 0;
				dblOSPLI = 0;
				dblOSI = 0;
				dblOSC = 0;
				fldOSP.Text = "";
				fldOSPLI.Text = "";
				fldOSI.Text = "";
				fldOSC.Text = "";
				if (rsData.EndOfFile())
				{
					return;
				}
                if (lngTAType > 0)
                {
                    switch (lngTAType)
                    {

                        case 1:
                            {
                                // TA
                                // if the account is in the recordset then it is TA
                                rsMaster.FindFirstRecord("RSAccount", rsData.Get_Fields("Account"));
                                if (rsMaster.NoMatch)
                                {
                                    rsData.MoveNext();
                                    goto TRYAGAIN;
                                }
                                break;
                            }
                        case 2:
                            {
                                // Non TA
                                // if the account is NOT in the recordset then it is TA
                                rsMaster.FindFirstRecord("RSAccount", rsData.Get_Fields("Account"));
                                if (!rsMaster.NoMatch)
                                {
                                    // If rsMaster.Fields("TaxAcquired") Then
                                    rsData.MoveNext();
                                    goto TRYAGAIN;
                                }
                                break;
                            }
                    } //end switch
                      // End If
                }
                lngCount += 1;
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				fldAccount.Text = FCConvert.ToString(rsData.Get_Fields("Account"));
				fldYear.Text = FCConvert.ToString(FCUtils.iDiv(FCConvert.ToInt32(rsData.Get_Fields_Int32("BillingYear")), 10));
				fldName.Text = rsData.Get_Fields_String("Name1");

				if (reportConfiguration.Options.UseAsOfDate())
				{
					// get the values from the payment records
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Field [LRN] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Field [LRN] not found!! (maybe it is an alias?)
					rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE Account = " + FCConvert.ToString(rsData.Get_Fields("Account")) + " AND [Year] = " + FCConvert.ToString(rsData.Get_Fields_Int32("BillingYear")) + " AND BillCode = 'L' AND BillKey = " + FCConvert.ToString(rsData.Get_Fields("LRN")) + " AND ISNULL(RecordedTransactionDate, '12/30/1899') <= '" + FCConvert.ToString(reportConfiguration.Options.AsOfDate) + "'");
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Field [LRN] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Field [LRN] not found!! (maybe it is an alias?)
					rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE Account = " + FCConvert.ToString(rsData.Get_Fields("Account")) + " AND [Year] = " + FCConvert.ToString(rsData.Get_Fields_Int32("BillingYear")) + " AND BillCode = 'L' AND BillKey = " + FCConvert.ToString(rsData.Get_Fields("LRN")));
				}
				dblPreLienIntNeed = Conversion.Val(rsData.Get_Fields_Decimal("Interest"));
				dblLienCostNeed = Conversion.Val(rsData.Get_Fields_Decimal("Costs"));
				while (!rsPayment.EndOfFile())
				{
					dblPaymentRecieved += Conversion.Val(rsPayment.Get_Fields_Decimal("Principal"));

					if (reportConfiguration.Options.FullStatusAmounts)
					{
						dblCostPaid += Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost"));
						dblIntPaid += Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")) + Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest"));
					}
					else
					{
						if (!reportConfiguration.Options.FullStatusAmounts && ((rsPayment.Get_Fields_Decimal("Principal"))) == 0 && ((rsPayment.Get_Fields_Decimal("LienCost"))) == 0 && rsPayment.Get_Fields_Decimal("CurrentInterest") < 0)
						{
							// do not even look at this payment
						}
						else
						{
							if (((rsPayment.Get_Fields_Decimal("PreLienInterest"))) != 0)
							{
								dblIntPaid += Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest"));
							}
						}
						dblCostPaid += Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost"));
					}
					if (reportConfiguration.Options.FullStatusAmounts)
					{
						if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields_String("Code"))) == "P")
						{
							AddToPaymentArray_242(6, Conversion.Val(rsPayment.Get_Fields_Decimal("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
						else if ((Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields_String("Code"))) == "X") || (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields_String("Code"))) == "S"))
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(9, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "U")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(8, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
								// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
								else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "Y")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(10, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
									// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
									else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "C")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(2, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
										// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
										else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "A")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(1, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
											// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
											// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
											else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "D")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(3, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
												// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
												// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
												else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "I")
						{
							if (!reportConfiguration.Options.FullStatusAmounts && ((rsPayment.Get_Fields_Decimal("Principal"))) == 0 && ((rsPayment.Get_Fields_Decimal("LienCost"))) == 0 && rsPayment.Get_Fields_Decimal("CurrentInterest") < 0)
							{
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								AddToPaymentArray_242(4, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
							}
						}
													// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
													// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
													else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "L")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(5, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
														// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
														// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
														else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "3")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(0, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
															// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
															// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
															else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "R")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(7, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")), Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "P")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(6, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), 0, Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						else if ((Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "X") || (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "S"))
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(9, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), 0, Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "U")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(8, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), 0, Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
								// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
								else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "Y")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(10, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), 0, Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
									// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
									else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "C")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(2, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), 0, Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
										// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
										else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "A")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(1, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), 0, Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
											// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
											// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
											else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "D")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(3, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), 0, Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
												// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
												// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
												else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "I")
						{
							if (!reportConfiguration.Options.FullStatusAmounts && ((rsPayment.Get_Fields_Decimal("Principal"))) == 0 && ((rsPayment.Get_Fields_Decimal("LienCost"))) == 0 && rsPayment.Get_Fields_Decimal("CurrentInterest") < 0)
							{
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								AddToPaymentArray_242(4, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), 0, Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
							}
						}
													// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
													// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
													else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "L")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(5, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), 0, Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
														// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
														// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
														else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "3")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(0, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), 0, Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
															// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
															// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
															else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "R")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(7, Conversion.Val(rsPayment.Get_Fields("Principal")), Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")), 0, Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
						}
					}
					rsPayment.MoveNext();
				}

				if (reportConfiguration.Options.FullStatusAmounts)
				{
					// this will show all payments even if int or costs paid are more than owed
					// if the current interest is checked then calculate it and display it
					dblTotalDue = modCLCalculations.CalculateAccountCLLien(rsData, reportConfiguration.Options.AsOfDate, ref dblXtraInt);
					// , , , , , True)
					fldPaymentReceived.Text = Strings.Format(dblPaymentRecieved + dblIntPaid + dblCostPaid - dblXtraInt, "#,##0.00");
				}
				else
				{
					// this will only allow the maximum to show as the
					// total PLI and original costs of the lien
					if (dblIntPaid > Conversion.Val(rsData.Get_Fields_Decimal("Interest")))
					{
						dblIntPaid = Conversion.Val(rsData.Get_Fields_Decimal("Interest"));
					}
					else
					{
					}
					// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
					if (Conversion.Val(rsData.Get_Fields_Decimal("CostsPaid")) > Conversion.Val(rsData.Get_Fields("Costs")))
					{
						// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
						dblCostPaid = Conversion.Val(rsData.Get_Fields("Costs"));
					}
					else
					{
						// dblCostPaid = rsData.Fields("CostsPaid")
					}
					fldPaymentReceived.Text = Strings.Format(dblPaymentRecieved + dblIntPaid + dblCostPaid, "#,##0.00");
				}

				fldTaxDue.Text = Strings.Format(FCConvert.ToDouble(Conversion.Val(rsData.Get_Fields("Principal")) + Conversion.Val(rsData.Get_Fields("Interest")) + Conversion.Val(rsData.Get_Fields("Costs"))), "#,##0.00");
				fldDue.Text = Strings.Format(FCConvert.ToDouble(fldTaxDue.Text) - FCConvert.ToDouble(fldPaymentReceived.Text), "#,##0.00");
				fldType.Text = "L";

				if (FCConvert.ToDouble(fldDue.Text) == 0)
				{
					// if this account is not outstanding, then it must not be used
					ReversePaymentsFromStatusArray(ref rsPayment);
					rsData.MoveNext();
					lngCount -= 1;
					goto TRYAGAIN;
				}
				// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
				dblOSP = Conversion.Val(rsData.Get_Fields("Principal")) - dblOSP;
				// TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
				dblOSPLI = Conversion.Val(rsData.Get_Fields("Interest")) - dblOSPLI;
				dblOSI = (dblOSI - dblXtraInt) * -1;
				// kgk trocl-663  dblOSI is Neg and dblXtraInt is Pos ' dblOSI + dblXtraInt
				// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
				dblOSC = Conversion.Val(rsData.Get_Fields("Costs")) - dblOSC;
				fldOSP.Text = Strings.Format(dblOSP, "#,##0.00");
				fldOSPLI.Text = Strings.Format(dblOSPLI, "#,##0.00");
				fldOSI.Text = Strings.Format(dblOSI, "#,##0.00");
				fldOSC.Text = Strings.Format(dblOSC, "#,##0.00");
				dblTotOSP += dblOSP;
				dblTotOSPLI += dblOSPLI;
				dblTotOSI += dblOSI;
				dblTotOSC += dblOSC;
				//if (modGlobal.Statics.boolSubReport)
				//{
					parentReport.dblTotalsTax += FCConvert.ToDouble(fldTaxDue.Text);
					parentReport.dblTotalsPay += FCConvert.ToDouble(fldPaymentReceived.Text);
					parentReport.dblTotalsPrin += FCConvert.ToDouble(fldOSP.Text);
					parentReport.dblTotalsPLI += FCConvert.ToDouble(fldOSPLI.Text);
					parentReport.dblTotalsInt += FCConvert.ToDouble(fldOSI.Text);
					parentReport.dblTotalsCost += FCConvert.ToDouble(fldOSC.Text);
					parentReport.lngCount += 1;
				//}
				dblTotals[0] += FCConvert.ToDouble(fldTaxDue.Text);
				dblTotals[1] += FCConvert.ToDouble(fldPaymentReceived.Text);
				dblTotals[3] += FCConvert.ToDouble(fldDue.Text);
				// keep track for the year totals
				dblYearTotals[rsData.Get_Fields_Int32("BillingYear") - 19800] = dblYearTotals[FCConvert.ToInt16(rsData.Get_Fields_Int32("BillingYear")) - 19800] + FCConvert.ToDouble(fldDue.Text);
				// kk01072015 trocls-13  Keep count of accounts and Bills by Year
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				if (!dctAccounts.ContainsKey(FCConvert.ToInt32(Conversion.Val(rsData.Get_Fields("Account")))))
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					dctAccounts.Add(FCConvert.ToInt32(Conversion.Val(rsData.Get_Fields("Account"))), rsData.Get_Fields_String("Name1"));
				}
				else
				{
					// Count duplicats account number with different name??
				}
				lngArrBillCounts[rsData.Get_Fields_Int32("BillingYear") - 19800] = lngArrBillCounts[FCConvert.ToInt16(rsData.Get_Fields_Int32("BillingYear")) - 19800] + 1;
				// move to the next record in the query
				rsData.MoveNext();
				
				rsPayment.Dispose();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In BindFields");
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void SetupTotals()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strSUM = "";
				//clsDRWrapper rsSum = new clsDRWrapper();
				GrapeCity.ActiveReports.SectionReportModel.TextBox obNew;
				int intCT;
				// this sub will fill in the totals line at the bottom of the report
				fldTotalTaxDue.Text = Strings.Format(dblTotals[0], "#,##0.00");
				fldTotalPaymentReceived.Text = Strings.Format(dblTotals[1], "#,##0.00");
				fldTotalDue.Text = Strings.Format(dblTotals[3], "#,##0.00");
				fldTotalOSP.Text = Strings.Format(dblTotOSP, "#,##0.00");
				fldTotalOSPLI.Text = Strings.Format(dblTotOSPLI, "#,##0.00");
				fldTotalOSI.Text = Strings.Format(dblTotOSI, "#,##0.00");
				fldTotalOSC.Text = Strings.Format(dblTotOSC, "#,##0.00");
				// kk01132015 trocls-13  Add count of accounts
				if (dctAccounts.Count > 1)
				{
					fldAcctCount.Text = FCConvert.ToString(dctAccounts.Count) + " Accounts";
				}
				else if (dctAccounts.Count == 1)
				{
					fldAcctCount.Text = FCConvert.ToString(dctAccounts.Count) + " Account";
				}
				else
				{
					fldAcctCount.Text = "";
				}
				if (lngCount > 1)
				{
					lblTotals.Text = "Total for " + FCConvert.ToString(lngCount) + " Bills:";
					// kk01092015 trocls-13  This is count of bills, not accounts   '& " Accounts:"
				}
				else if (lngCount == 1)
				{
					lblTotals.Text = "Total for " + FCConvert.ToString(lngCount) + " Bill:";
					// & " Account:"
				}
				else
				{
					lblTotals.Text = "No Liened Bills";
				}
				// this will setup the payment summary
				SetupTotalSummary();
				// Load Summary List
				intSumRows = 1;
				for (intCT = 0; intCT <= Information.UBound(dblYearTotals, 1) - 1; intCT++)
				{
					if (dblYearTotals[intCT] != 0)
					{
						AddSummaryRow(intSumRows, dblYearTotals[intCT], lngArrBillCounts[intCT], intCT + 19800, false);
						intSumRows += 1;
					}
				}

				obNew = ReportFooter.Controls["fldSummaryTotal"] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
				//
				obNew.Top = fldSummary1.Top + ((intSumRows - 1) * fldSummary1.Height);
				obNew.Left = fldSummary1.Left;
				obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				obNew.Width = fldSummary1.Width;
				obNew.Font = lblSummary1.Font;
				obNew.Text = Strings.Format(dblPDTotal, "#,##0.00");

				obNew = ReportFooter.Controls["fldSumCountTotal"] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
				//
				obNew.Top = fldSumCount1.Top + ((intSumRows - 1) * fldSumCount1.Height);
				obNew.Left = fldSumCount1.Left;
				obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				obNew.Width = fldSumCount1.Width;
				obNew.Font = fldSumCount1.Font;
				obNew.Text = lngCount.ToString();

				GrapeCity.ActiveReports.SectionReportModel.Label obLabel = ReportFooter.Controls["lblPerDiemTotal"] as GrapeCity.ActiveReports.SectionReportModel.Label;
				//
				obLabel.Top = lblSummary1.Top + ((intSumRows - 1) * lblSummary1.Height);
				obLabel.Left = lblSummary1.Left;
				obLabel.Font = lblSummary1.Font;
				obLabel.Text = "Total";

				GrapeCity.ActiveReports.SectionReportModel.Line obLine = ReportFooter.Controls["lnFooterSummaryTotal"] as GrapeCity.ActiveReports.SectionReportModel.Line;
				//
				obLine.X1 = fldSumCount1.Left;
				// kk01092015 trocls-13     fldSummary1.Left
				obLine.X2 = fldSummary1.Left + fldSummary1.Width;
				obLine.Y1 = lblSummary1.Top + ((intSumRows - 1) * lblSummary1.Height);
				obLine.Y2 = obLine.Y1;

				ReportFooter.Height = lblSummary1.Top + (intSumRows * lblSummary1.Height);
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Lien Summary Creation");
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			SetupTotals();
		}

		private void SetReportHeader()
		{
			int intCT;
			string strTemp = "";
			//FC:FINAL:KS: #without nr.:fix Exit For inside switch/case statement
			bool endFor = false;
            var strSeparator = "";
            var filter = reportConfiguration.Filter;
            if (filter.AccountMin.HasValue || filter.AccountMax.HasValue)
            {
                if (filter.AccountMax.HasValue)
                {
                    if (filter.AccountMin.HasValue)
                    {
                        if (filter.AccountMin == filter.AccountMax)
                        {
                            strTemp += " Account: " + filter.AccountMin.Value.ToString();
                        }
                        else
                        {
                            strTemp += " Account: " + filter.AccountMin.Value.ToString() + " To " +
                                       filter.AccountMax.Value.ToString();
                        }
                    }
                    else
                    {
                        strTemp += "Below Account: " + filter.AccountMax.Value.ToString();
                    }
                }
                else
                {
                    strTemp += "Above Account: " + filter.AccountMin.Value.ToString();
                }

                strSeparator = ",";
            }

            if (filter.TaxYearMin.HasValue || filter.TaxYearMax.HasValue)
            {
                strTemp += strSeparator;

                if (filter.TaxYearMin.HasValue)
                {
                    if (filter.TaxYearMax.HasValue)
                    {
                        strTemp += " Tax Year: " + filter.TaxYearMin.Value.ToString() + " To " +
                                   filter.TaxYearMax.Value.ToString();
                    }
                    else
                    {
                        strTemp += " Tax Year: " + filter.TaxYearMin.Value.ToString();
                    }
                }
                else
                {
                    strTemp += " Tax Year: " + filter.TaxYearMax.Value.ToString();
                }

                strSeparator = ",";
            }
	
			if (lngTAType > 0)
			{
				if (Strings.Trim(strTemp) == "")
				{
					if (lngTAType == 1)
					{
						strTemp = "Tax Acquired";
					}
					else
					{
						strTemp = "Non-Tax Acquired";
					}
				}
				else
				{
					if (lngTAType == 1)
					{
						strTemp = ", Tax Acquired";
					}
					else
					{
						strTemp = ", Non-Tax Acquired";
					}
				}
			}
			// check to see if they used the Show Interest checkbox
			if (reportConfiguration.Options.ShowCurrentInterest)
			{
				if (Strings.Trim(strTemp) == "")
				{
					strTemp += "Show Interest";
				}
				else
				{
					strTemp += ", Show Interest";
				}
			}
			if (Strings.Trim(strTemp) == "")
			{
				lblReportType.Text = "Complete List" + "\r\n" + "Outstanding Lien Accounts";
			}
			else
			{
				lblReportType.Text = strTemp + "\r\n" + "Outstanding Lien Accounts";
			}
			if (reportConfiguration.Options.UseAsOfDate())
			{
				lblReportType.Text = lblReportType.Text + "\r\n" + "As of: " + Strings.Format(reportConfiguration.Options.AsOfDate, "MM/dd/yyyy");
			}
		}
		// VBto upgrade warning: intRNum As short	OnWriteFCConvert.ToInt32(
		private void AddSummaryRow(int intRNum, double dblAmount, int lngBillCnt, int lngYear, bool createObjects)
		{
			GrapeCity.ActiveReports.SectionReportModel.TextBox obNew/*unused?*/;
			// this will add another per diem line in the report footer
			if (intRNum == 1)
			{
				fldSummary1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				fldSummary1.Text = Strings.Format(dblAmount, "#,##0.00");
				fldSumCount1.Text = lngBillCnt.ToString();
				// kk01092015 trocls-13
				fldSumCount1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				lblSummary1.Text = modExtraModules.FormatYear(lngYear.ToString());
				dblPDTotal += dblAmount;
			}
			else
			{
				if (createObjects)
				{
					// add a field
					obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
					obNew.Name = "fldSummary" + FCConvert.ToString(intRNum);
					ReportFooter.Controls.Add(obNew);
				}
				else
				{
					obNew = ReportFooter.Controls["fldSummary" + FCConvert.ToString(intRNum)] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
				}
				if (obNew != null)
				{
					obNew.Top = fldSummary1.Top + ((intRNum - 1) * fldSummary1.Height);
					obNew.Left = fldSummary1.Left;
					obNew.Width = fldSummary1.Width;
					obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
					obNew.Font = fldSummary1.Font;
					// this sets the font to the same as the field that is already created
					obNew.Text = Strings.Format(dblAmount, "#,##0.00");
					dblPDTotal += dblAmount;
				}
				if (createObjects)
				{
					// kk01092015 trocls-13  add a field for the number of liens
					obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
					obNew.Name = "fldSumCount" + FCConvert.ToString(intRNum);
					ReportFooter.Controls.Add(obNew);
				}
				else
				{
					obNew = ReportFooter.Controls["fldSumCount" + FCConvert.ToString(intRNum)] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
				}
				if (obNew != null)
				{
					obNew.Top = fldSumCount1.Top + ((intRNum - 1) * fldSumCount1.Height);
					obNew.Left = fldSumCount1.Left;
					obNew.Width = fldSumCount1.Width;
					obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
					obNew.Font = fldSumCount1.Font;
					// this sets the font to the same as the field that is already created
					obNew.Text = lngBillCnt.ToString();
				}
				GrapeCity.ActiveReports.SectionReportModel.Label obLabel = null;
				if (createObjects)
				{
					// add a label
					obLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
					obLabel.Name = "lblSummary" + FCConvert.ToString(intRNum);
					ReportFooter.Controls.Add(obLabel);
				}
				else
				{
					obLabel = ReportFooter.Controls["lblSummary" + FCConvert.ToString(intRNum)] as GrapeCity.ActiveReports.SectionReportModel.Label;
				}
				if (obLabel != null)
				{
					obLabel.Top = lblSummary1.Top + ((intRNum - 1) * lblSummary1.Height);
					obLabel.Left = lblSummary1.Left;
					obLabel.Font = fldSummary1.Font;
					// this sets the font to the same as the field that is already created
					obLabel.Text = modExtraModules.FormatYear(lngYear.ToString());
				}
			}
		}

		private void SetupTotalSummary()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the summary labels at the bottom of the page
				// and hide/show the labels when needed
				int intCT;
				int intRow;
				// this will keep track of the row  that I am adding values to
				string strDesc = "";
				double[] dblTotal = new double[5 + 1];
				// fill in the titles
				lblSumHeaderType.Text = "Type";
				lblSumHeaderPrin.Text = "Principal";
				lblSumHeaderInt.Text = "Interest";
				lblSumHeaderCost.Text = "Costs";
				lblSumHeaderTotal.Text = "Total";
				intRow = 1;
				// start at the first row
				for (intCT = 0; intCT <= 10; intCT++)
				{
					// this will fill the totals element
					dblPayments[intCT, 4] = dblPayments[intCT, 0] + dblPayments[intCT, 1] + dblPayments[intCT, 2] + dblPayments[intCT, 3];
				}
				for (intCT = 0; intCT <= 10; intCT++)
				{
					if (dblPayments[intCT, 4] != 0)
					{
						switch (intCT)
						{
							case 0:
								{
									strDesc = "3 - 30 DN Costs";
									break;
								}
							case 1:
								{
									strDesc = "A - Abatement";
									break;
								}
							case 2:
								{
									strDesc = "C - Correction";
									break;
								}
							case 3:
								{
									strDesc = "D - Discount";
									break;
								}
							case 4:
								{
									strDesc = "I - Interest Charged";
									break;
								}
							case 5:
								{
									strDesc = "L - Lien Costs";
									break;
								}
							case 6:
								{
									strDesc = "P - Payment";
									break;
								}
							case 7:
								{
									strDesc = "R - Refunded Abatement";
									break;
								}
							case 8:
								{
									strDesc = "U - Tax Club";
									break;
								}
							case 9:
								{
									strDesc = "X - DOS Correction";
									break;
								}
							case 10:
								{
									strDesc = "Y - Prepayment";
									break;
								}
						}
						//end switch
						FillSummaryLine(intRow, strDesc, dblPayments[intCT, 0], dblPayments[intCT, 1], dblPayments[intCT, 2], dblPayments[intCT, 3], dblPayments[intCT, 4]);
						dblTotal[0] += dblPayments[intCT, 0];
						// this will total all of the seperated payments for the total line
						dblTotal[1] += dblPayments[intCT, 1];
						dblTotal[2] += dblPayments[intCT, 2];
						dblTotal[3] += dblPayments[intCT, 3];
						dblTotal[4] += dblPayments[intCT, 4];
						intRow += 1;
					}
				}
				// show the total line
				FillSummaryLine(intRow, "Total", dblTotal[0], dblTotal[1], dblTotal[2], dblTotal[3], dblTotal[4]);
				SetSummaryTotalLine(intRow);
				for (intCT = intRow + 1; intCT <= 11; intCT++)
				{
					HideSummaryRow(intCT);
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Creating Summary Table");
			}
		}
		// VBto upgrade warning: intRw As short	OnWriteFCConvert.ToInt32(
		private void SetSummaryTotalLine(int intRw)
		{
			switch (intRw)
			{
				case 1:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal1.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal1.Top;
						break;
					}
				case 2:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal2.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal2.Top;
						break;
					}
				case 3:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal3.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal3.Top;
						break;
					}
				case 4:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal4.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal4.Top;
						break;
					}
				case 5:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal5.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal5.Top;
						break;
					}
				case 6:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal6.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal6.Top;
						break;
					}
				case 7:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal7.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal7.Top;
						break;
					}
				case 8:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal8.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal8.Top;
						break;
					}
				case 9:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal9.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal9.Top;
						break;
					}
				case 10:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal10.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal10.Top;
						break;
					}
				case 11:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal11.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal11.Top;
						break;
					}
			}
			//end switch
		}
		// VBto upgrade warning: intRw As short	OnWriteFCConvert.ToInt32(
		private void FillSummaryLine(int intRw, string strDescription, double dblPrin, double dblPLI, double dblCurInt, double dblCosts, double dblTotal)
		{
			// this routine will fill in the line summary row
			switch (intRw)
			{
				case 1:
					{
						lblSummaryPaymentType1.Text = strDescription;
						lblSumPrin1.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt1.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost1.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal1.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 2:
					{
						lblSummaryPaymentType2.Text = strDescription;
						lblSumPrin2.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt2.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost2.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal2.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 3:
					{
						lblSummaryPaymentType3.Text = strDescription;
						lblSumPrin3.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt3.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost3.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal3.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 4:
					{
						lblSummaryPaymentType4.Text = strDescription;
						lblSumPrin4.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt4.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost4.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal4.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 5:
					{
						lblSummaryPaymentType5.Text = strDescription;
						lblSumPrin5.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt5.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost5.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal5.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 6:
					{
						lblSummaryPaymentType6.Text = strDescription;
						lblSumPrin6.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt6.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost6.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal6.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 7:
					{
						lblSummaryPaymentType7.Text = strDescription;
						lblSumPrin7.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt7.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost7.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal7.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 8:
					{
						lblSummaryPaymentType8.Text = strDescription;
						lblSumPrin8.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt8.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost8.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal8.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 9:
					{
						lblSummaryPaymentType9.Text = strDescription;
						lblSumPrin9.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt9.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost9.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal9.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 10:
					{
						lblSummaryPaymentType10.Text = strDescription;
						lblSumPrin10.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt10.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost10.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal10.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 11:
					{
						lblSummaryPaymentType11.Text = strDescription;
						lblSumPrin11.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt11.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost11.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal11.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
			}
			//end switch
		}

		private void AddToPaymentArray_242(int lngIndex, double dblPrin, double dblPLI, double dblCurInt, double dblCost)
		{
			AddToPaymentArray(ref lngIndex, ref dblPrin, ref dblPLI, ref dblCurInt, ref dblCost);
		}

		private void AddToPaymentArray(ref int lngIndex, ref double dblPrin, ref double dblPLI, ref double dblCurInt, ref double dblCost)
		{
			dblPayments[lngIndex, 0] += dblPrin;
			dblPayments[lngIndex, 1] += dblPLI;
			dblPayments[lngIndex, 2] += dblCurInt;
			dblPayments[lngIndex, 3] += dblCost;
			dblOSP += dblPrin;
			dblOSPLI += dblPLI;
			dblOSI += dblCurInt;
			dblOSC += dblCost;
		}
		// VBto upgrade warning: intRw As short	OnWriteFCConvert.ToInt32(
		private void HideSummaryRow(int intRw)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intCT;
				for (intCT = intRw; intCT <= 11; intCT++)
				{
					switch (intRw)
					{
						case 1:
							{
								lblSummaryPaymentType1.Visible = false;
								lblSumPrin1.Visible = false;
								lblSumInt1.Visible = false;
								lblSumCost1.Visible = false;
								lblSummaryTotal1.Visible = false;
								break;
							}
						case 2:
							{
								lblSummaryPaymentType2.Visible = false;
								lblSumPrin2.Visible = false;
								lblSumInt2.Visible = false;
								lblSumCost2.Visible = false;
								lblSummaryTotal2.Visible = false;
								break;
							}
						case 3:
							{
								lblSummaryPaymentType3.Visible = false;
								lblSumPrin3.Visible = false;
								lblSumInt3.Visible = false;
								lblSumCost3.Visible = false;
								lblSummaryTotal3.Visible = false;
								break;
							}
						case 4:
							{
								lblSummaryPaymentType4.Visible = false;
								lblSumPrin4.Visible = false;
								lblSumInt4.Visible = false;
								lblSumCost4.Visible = false;
								lblSummaryTotal4.Visible = false;
								break;
							}
						case 5:
							{
								lblSummaryPaymentType5.Visible = false;
								lblSumPrin5.Visible = false;
								lblSumInt5.Visible = false;
								lblSumCost5.Visible = false;
								lblSummaryTotal5.Visible = false;
								break;
							}
						case 6:
							{
								lblSummaryPaymentType6.Visible = false;
								lblSumPrin6.Visible = false;
								lblSumInt6.Visible = false;
								lblSumCost6.Visible = false;
								lblSummaryTotal6.Visible = false;
								break;
							}
						case 7:
							{
								lblSummaryPaymentType7.Visible = false;
								lblSumPrin7.Visible = false;
								lblSumInt7.Visible = false;
								lblSumCost7.Visible = false;
								lblSummaryTotal7.Visible = false;
								break;
							}
						case 8:
							{
								lblSummaryPaymentType8.Visible = false;
								lblSumPrin8.Visible = false;
								lblSumInt8.Visible = false;
								lblSumCost8.Visible = false;
								lblSummaryTotal8.Visible = false;
								break;
							}
						case 9:
							{
								lblSummaryPaymentType9.Visible = false;
								lblSumPrin9.Visible = false;
								lblSumInt9.Visible = false;
								lblSumCost9.Visible = false;
								lblSummaryTotal9.Visible = false;
								break;
							}
						case 10:
							{
								lblSummaryPaymentType10.Visible = false;
								lblSumPrin10.Visible = false;
								lblSumInt10.Visible = false;
								lblSumCost10.Visible = false;
								lblSummaryTotal10.Visible = false;
								break;
							}
						case 11:
							{
								lblSummaryPaymentType11.Visible = false;
								lblSumPrin11.Visible = false;
								lblSumInt11.Visible = false;
								lblSumCost11.Visible = false;
								lblSummaryTotal11.Visible = false;
								break;
							}
					}
					//end switch
				}
				if (!boolAdjustedSummary)
				{
					SetYearSummaryTop_2(lblSummaryPaymentType1.Top + (intRw * lblSummaryPaymentType1.Height) + 100 / 1440F);
					boolAdjustedSummary = true;
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Hiding Summary Rows");
			}
		}

		private void SetYearSummaryTop_2(float lngTop)
		{
			SetYearSummaryTop(ref lngTop);
		}

		private void SetYearSummaryTop(ref float lngTop)
		{
			// this will start the year summary at the right height
			lblSummary.Top = lngTop;
			Line1.Y1 = lngTop + lblSummary.Height;
			Line1.Y2 = lngTop + lblSummary.Height;
			lblSummary1.Top = lngTop + lblSummary.Height;
			fldSumCount1.Top = lngTop + fldSumCount1.Height;
			// kk01092015 trocls-13
			fldSummary1.Top = lngTop + lblSummary.Height;
		}

		private void ReversePaymentsFromStatusArray(ref clsDRWrapper rsRev)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (rsRev.RecordCount() != 0)
				{
					rsRev.MoveFirst();
					while (!rsRev.EndOfFile())
					{
						if (reportConfiguration.Options.FullStatusAmounts)
						{
							if (Strings.UCase(rsRev.Get_Fields_String("Code")) == "P")
							{
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								AddToPaymentArray_242(6, rsRev.Get_Fields_Decimal("Principal").ToDouble() * -1, rsRev.Get_Fields_Decimal("PreLienInterest").ToDouble() * -1, rsRev.Get_Fields_Decimal("CurrentInterest").ToDouble() * -1, rsRev.Get_Fields_Decimal("LienCost").ToDouble() * -1);
							}
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							else if ((Strings.UCase(rsRev.Get_Fields_String("Code")) == "X") || (Strings.UCase(rsRev.Get_Fields_String("Code")) == "S"))
							{
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								AddToPaymentArray_242(9, rsRev.Get_Fields_Decimal("Principal").ToDouble() * -1, rsRev.Get_Fields_Decimal("PreLienInterest").ToDouble() * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("CurrentInterest") * -1), rsRev.Get_Fields_Decimal("LienCost").ToDouble() * -1);
							}
								// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
								else if (Strings.UCase(rsRev.Get_Fields_String("Code")) == "U")
							{
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								AddToPaymentArray_242(8, rsRev.Get_Fields_Decimal("Principal").ToDouble() * -1, rsRev.Get_Fields_Decimal("PreLienInterest").ToDouble() * -1, rsRev.Get_Fields_Decimal("CurrentInterest").ToDouble() * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1));
							}
									// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
									else if (Strings.UCase(rsRev.Get_Fields_String("Code")) == "Y")
							{
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								AddToPaymentArray_242(10, rsRev.Get_Fields_Decimal("Principal").ToDouble() * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("CurrentInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1));
							}
										// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
										else if (Strings.UCase(rsRev.Get_Fields_String("Code")) == "C")
							{
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								AddToPaymentArray_242(2, rsRev.Get_Fields_Decimal("Principal").ToDouble() * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("CurrentInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1));
							}
											// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
											// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
											else if (Strings.UCase(rsRev.Get_Fields_String("Code")) == "A")
							{
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								AddToPaymentArray_242(1, rsRev.Get_Fields_Decimal("Principal").ToDouble() * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("CurrentInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1));
							}
												// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
												// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
												else if (Strings.UCase(rsRev.Get_Fields_String("Code")) == "D")
							{
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								AddToPaymentArray_242(3, rsRev.Get_Fields_Decimal("Principal").ToDouble() * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("CurrentInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1));
							}
													// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
													// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
													else if (Strings.UCase(rsRev.Get_Fields_String("Code")) == "I")
							{
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								AddToPaymentArray_242(4, rsRev.Get_Fields_Decimal("Principal").ToDouble() * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("CurrentInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1));
							}
														// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
														// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
														else if (Strings.UCase(rsRev.Get_Fields_String("Code")) == "L")
							{
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								AddToPaymentArray_242(5, rsRev.Get_Fields_Decimal("Principal").ToDouble() * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("CurrentInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1));
							}
															// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
															// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
															else if (Strings.UCase(rsRev.Get_Fields_String("Code")) == "3")
							{
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								AddToPaymentArray_242(0, rsRev.Get_Fields_Decimal("Principal").ToDouble() * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("CurrentInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1));
							}
																// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
																// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
																else if (Strings.UCase(rsRev.Get_Fields_String("Code")) == "R")
							{
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								AddToPaymentArray_242(7, rsRev.Get_Fields_Decimal("Principal").ToDouble() * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("CurrentInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1));
							}
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							if (Strings.UCase(rsRev.Get_Fields_String("Code")) == "P")
							{
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								AddToPaymentArray_242(6, rsRev.Get_Fields_Decimal("Principal").ToDouble() * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), 0, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1));
							}
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							else if ((Strings.UCase(rsRev.Get_Fields_String("Code")) == "X") || (Strings.UCase(rsRev.Get_Fields_String("Code")) == "S"))
							{
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								AddToPaymentArray_242(9, rsRev.Get_Fields_Decimal("Principal").ToDouble() * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), 0, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1));
							}
								// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
								else if (Strings.UCase(rsRev.Get_Fields_String("Code")) == "U")
							{
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								AddToPaymentArray_242(8, rsRev.Get_Fields_Decimal("Principal").ToDouble() * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), 0, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1));
							}
									// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
									else if (Strings.UCase(rsRev.Get_Fields_String("Code")) == "Y")
							{
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								AddToPaymentArray_242(10, rsRev.Get_Fields_Decimal("Principal").ToDouble() * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), 0, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1));
							}
										// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
										else if (Strings.UCase(rsRev.Get_Fields_String("Code")) == "C")
							{
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								AddToPaymentArray_242(2, rsRev.Get_Fields_Decimal("Principal").ToDouble() * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), 0, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1));
							}
											// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
											// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
											else if (Strings.UCase(rsRev.Get_Fields_String("Code")) == "A")
							{
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								AddToPaymentArray_242(1, rsRev.Get_Fields_Decimal("Principal").ToDouble() * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), 0, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1));
							}
												// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
												// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
												else if (Strings.UCase(rsRev.Get_Fields_String("Code")) == "D")
							{
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								AddToPaymentArray_242(3, rsRev.Get_Fields_Decimal("Principal").ToDouble() * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), 0, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1));
							}
													// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
													// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
													else if (Strings.UCase(rsRev.Get_Fields_String("Code")) == "I")
							{
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								AddToPaymentArray_242(4, rsRev.Get_Fields_Decimal("Principal").ToDouble() * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), 0, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1));
							}
														// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
														// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
														else if (Strings.UCase(rsRev.Get_Fields_String("Code")) == "L")
							{
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								AddToPaymentArray_242(5, rsRev.Get_Fields_Decimal("Principal").ToDouble() * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), 0, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1));
							}
															// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
															// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
															else if (Strings.UCase(rsRev.Get_Fields_String("Code")) == "3")
							{
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								AddToPaymentArray_242(0, rsRev.Get_Fields_Decimal("Principal").ToDouble() * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), 0, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1));
							}
																// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
																// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
																else if (Strings.UCase(rsRev.Get_Fields_String("Code")) == "R")
							{
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								AddToPaymentArray_242(7, rsRev.Get_Fields_Decimal("Principal").ToDouble() * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), 0, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1));
							}
						}
						rsRev.MoveNext();
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Reversing Payment Counts");
			}
		}

		
	}
}