using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using SharedApplication.TaxCollections;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptAuditStatusAll : BaseSectionReport
	{
        private TaxCollectionStatusReportConfiguration reportConfiguration;
        public double dblTotalsTax;
		public double dblTotalsPay;
		public double dblTotalsPrin;
		public double dblTotalsPLI;
		public double dblTotalsInt;
		public double dblTotalsCost;
		public int lngCount;
		public int intSuppReportType;
		bool boolStarted;
		bool boolSummaryOnly;
		int lngTAType;

		public rptAuditStatusAll()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Audit Status Report";
		}

        public void SetReportOption( TaxCollectionStatusReportConfiguration reportConfig)
        {
            reportConfiguration = reportConfig;
        }
        public static rptAuditStatusAll InstancePtr
		{
			get
			{
				return (rptAuditStatusAll)Sys.GetInstance(typeof(rptAuditStatusAll));
			}
		}

		protected rptAuditStatusAll _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = boolStarted;
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_PageEnd(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			// fill in the page header
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "t");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			boolSummaryOnly =  reportConfiguration.Options.ShowSummaryOnly;
            // MAL@20080813 ; Tracker Reference: 11805
            if (reportConfiguration.Filter.IsTaxAcquired.HasValue)
            {
                if (reportConfiguration.Filter.IsTaxAcquired.Value)
                {
                    lngTAType = 1;
                }
                else
                {
                    lngTAType = 2;
                }
            }
            else
            {
                lngTAType = 0;
            }

			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			SetReportHeader();
			// this will find which criteria is used and create the page header for the report
			frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading");
			lngCount = 0;
			// reset the counters to 0
			dblTotalsTax = 0;
			dblTotalsPay = 0;
			dblTotalsPrin = 0;
			dblTotalsPLI = 0;
			dblTotalsInt = 0;
			dblTotalsCost = 0;
			modGlobal.Statics.boolSubReport = true;

			boolStarted = false;
			sarOB1.Width = this.PrintWidth;
			sarOb2.Width = this.PrintWidth;
			sarOb3.Width = this.PrintWidth;
            var auditSubReport = new rptAuditStatus();
            auditSubReport.SetReportOptionAndParent( reportConfiguration,  this);
            sarOB1.Report = auditSubReport;
			if (reportConfiguration.Options.DefaultReportType != TaxBillingHardCodedReport.SupplementalBills)
            {
                var auditLienSubReport = new rptAuditStatusLien();
                auditLienSubReport.SetReportOptionAndParent(reportConfiguration, this);
				sarOb2.Report = auditLienSubReport;
			}

			if (reportConfiguration.Options.DefaultReportType == TaxBillingHardCodedReport.SupplementalBills)
			{
				lblHeader.Text = "Supplemental Bills";
				this.Name = "Supplemental Bills";
			}
			else
			{
				lblHeader.Text = "Audit Summary Report";
				this.Name = "Audit Summary Report";
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			modGlobal.Statics.boolSubReport = false;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// BindFields
			boolStarted = true;
		}

		private void SetupTotals()
		{
			// this sub will fill in the totals line at the bottom of the report
			fldTotalTaxDue.Text = Strings.Format(dblTotalsTax, "#,##0.00");
			fldTotalPaymentReceived.Text = Strings.Format(dblTotalsPay, "#,##0.00");
			fldTotalOSP.Text = Strings.Format(dblTotalsPrin, "#,##0.00");
			fldTotalOSPLI.Text = Strings.Format(dblTotalsPLI, "#,##0.00");
			fldTotalOSI.Text = Strings.Format(dblTotalsInt, "#,##0.00");
			fldTotalOSC.Text = Strings.Format(dblTotalsCost, "#,##0.00");
			fldTotalDue.Text = Strings.Format(FCConvert.ToDouble(fldTotalTaxDue.Text) - FCConvert.ToDouble(fldTotalPaymentReceived.Text), "#,##0.00");
			if (lngCount > 1)
			{
				// this just shows the correct phrases
				lblTotals.Text = "Total for " + FCConvert.ToString(lngCount) + " Bills:";
				// kk01092015 trocls-13  This is count of bills, not accounts   '& " Accounts:"
			}
			else if (lngCount == 1)
			{
				lblTotals.Text = "Total for " + FCConvert.ToString(lngCount) + " Bill:";
				// & " Account:"
			}
			else
			{
				lblTotals.Text = "No Bills Processed";
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			SetupTotals();
		}

		private void SetReportHeader()
		{
			int intCT;
			string strTemp = "";
            string strSeparator = "";
            var filter = reportConfiguration.Filter;
            if (filter.AccountMin.HasValue || filter.AccountMax.HasValue)
            {
                if (filter.AccountMax.HasValue)
                {
                    if (filter.AccountMin.HasValue)
                    {
                        if (filter.AccountMin == filter.AccountMax)
                        {
                            strTemp += "Account: " + filter.AccountMin.Value.ToString() + " ";
                        }
                        else
                        {
                            strTemp += "Account: " + filter.AccountMin.Value.ToString() + " To " +
                                      filter.AccountMax.Value.ToString();
                        }
                    }
                    else
                    {
                        strTemp += "Below Account: " +filter.AccountMax.Value.ToString();
                    }
                }
                else
                {
                    strTemp += "Above Account: " + filter.AccountMin.Value.ToString();
                }

                strSeparator = ",";
            }

            if (filter.TaxYearMin.HasValue || filter.TaxYearMax.HasValue)
            {
                strTemp += strSeparator;

                if (filter.TaxYearMin.HasValue)
                {
                    if (filter.TaxYearMax.HasValue)
                    {
                        strTemp += " Tax Year: " + filter.TaxYearMin.Value.ToString() + " To " +
                                   filter.TaxYearMax.Value.ToString();
                    }
                    else
                    {
                        strTemp += " Tax Year: " + filter.TaxYearMin.Value.ToString();
                    }
                }
                else
                {
                    strTemp += " Tax Year: " + filter.TaxYearMax.Value.ToString();
                }

                strSeparator = ",";
            }

            if (reportConfiguration.Options.DefaultReportType == TaxBillingHardCodedReport.SupplementalBills)
            {
                if (filter.SupplementalBillDateStart.HasValue || filter.SupplementalBillDateEnd.HasValue)
                {
                    strTemp += strSeparator;

                    if (filter.SupplementalBillDateStart.HasValue)
                    {
                        strTemp += " Billing Date: " + filter.SupplementalBillDateStart.Value.ToShortDateString() + " To " + filter.SupplementalBillDateEnd.Value.ToShortDateString();
                    }
                    else
                    {
                        strTemp += " Billing Date: " + filter.SupplementalBillDateEnd.Value.ToShortDateString();
                    }

                    strSeparator = ",";
                }
            }

            if (filter.RateRecordMin.HasValue || filter.RateRecordMax.HasValue)
            {
                strTemp += strSeparator;

                if (filter.RateRecordMin.HasValue)
                {
                    if (filter.RateRecordMax.HasValue)
                    {
                        strTemp += " Rate Key: " + filter.RateRecordMin.Value.ToString() + " To " +
                                   filter.RateRecordMax.Value.ToString();
                    }
                    else
                    {
                        strTemp += " Rate Key: " + filter.RateRecordMin.Value.ToString();
                    }
                }
                else
                {
                    strTemp += " Rate Key: " + filter.RateRecordMax.Value.ToString();
                }

                strSeparator = ",";
            }

            if (filter.TaxBillStatus.HasValue)
            {
                if (filter.TaxBillStatus.Value == TaxBillStatusType.Lien)
                {
                    strTemp += strSeparator;
                    strTemp += " Showing Liens";
                }
                else if (filter.TaxBillStatus.Value == TaxBillStatusType.Regular)
                {
                    strTemp += strSeparator;
                    strTemp += " Showing Regular";
                }
            }


			if (lngTAType > 0)
			{
				if (Strings.Trim(strTemp) == "")
				{
					if (lngTAType == 1)
					{
						strTemp = "Tax Acquired";
					}
					else
					{
						strTemp = "Non-Tax Acquired";
					}
				}
				else
				{
					if (lngTAType == 1)
					{
						strTemp = ", Tax Acquired";
					}
					else
					{
						strTemp = ", Non-Tax Acquired";
					}
				}
			}

			if (reportConfiguration.Options.ShowCurrentInterest)
			{
				if (Strings.Trim(strTemp) == "")
				{
					strTemp += "Show Interest";
				}
				else
				{
					strTemp += ", Show Interest";
				}
			}
			if (boolSummaryOnly)
			{
				lblAccount.Visible = false;
				lblName.Visible = false;
				lblYear.Visible = false;
			}
			if (Strings.Trim(strTemp) == "")
			{
				lblReportType.Text = "Complete List";
			}
			else
			{
				lblReportType.Text = strTemp;
			}
			if (reportConfiguration.Options.UseAsOfDate())
			{
				lblReportType.Text = lblReportType.Text + "\r\n" + "As of: " + Strings.Format(reportConfiguration.Options.AsOfDate, "MM/dd/yyyy");
			}
		}

		
	}
}