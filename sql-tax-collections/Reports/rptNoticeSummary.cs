//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;


namespace TWCL0000
{
	/// <summary>
	/// Summary description for rptLienNoticeSummary.
	/// </summary>
	public partial class rptLienNoticeSummary : FCSectionReport
    {

		public rptLienNoticeSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptLienNoticeSummary InstancePtr
		{
			get
			{
				return (rptLienNoticeSummary)Sys.GetInstance(typeof(rptLienNoticeSummary));
			}
		}

		protected rptLienNoticeSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

// nObj = 1
//   0	rptLienNoticeSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}


		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               01/31/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               06/06/2005              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		short intYear;
		clsDRWrapper rsRE = new clsDRWrapper();
		clsDRWrapper rsLien = new clsDRWrapper();
		double dblTotalPrin;
		double dblTotalPLI;
		double dblTotalCost;
		double dblTotalCurrentInt;
		double dblTotalTotal;
		DateTime dtMailDate;
		double dblDemand;
		short intType;
		bool boolCO;
		short intBilledOwnerAddress; // trocl-1335

		public void Init(ref string strAccountList, ref string strPassHeaderTitle, ref string strPassReportType, ref short intPassYear, ref DateTime dtPassDate, ref short intPassType, ref string strRateKeys, bool blnAcctSort, short intBilledOwnerAddr)
		{
			try
			{	// On Error GoTo ERROR_HANDLER
				fecherFoundation.Information.Err(ex).Clear();

				string strOrder = "";
				intBilledOwnerAddress = intBilledOwnerAddr;
				dtMailDate = dtPassDate;
				// dblDemand = dblPassDemand
				// FillDemandArray

				intType = intPassType;

				// this will set the title of the report
				if (fecherFoundation.Strings.Trim(strPassHeaderTitle)!="") {
					lblHeader.Caption = strPassHeaderTitle;
					this.Text = strPassHeaderTitle;
				} else {
					lblHeader.Caption = "Summary Report";
				}

				// this will set the description of the report
				if (fecherFoundation.Strings.Trim(strPassReportType)!="") {
					lblReportType.Caption = strPassReportType;
				} else {
					lblReportType.Caption = strPassReportType;
				}

				intYear = intPassYear;

				// MAL@20080715: Add sort order option
				// Tracker Reference: 11834
				if (blnAcctSort) {
					strOrder = "ORDER BY Account, Name1";
				} else {
					strOrder = "ORDER BY Name1, Account";
				}

				// this will setup the data connection
				if (fecherFoundation.Strings.Trim(strAccountList)!="") {
					if (modStatusPayments.boolRE) {
						// rsData.OpenRecordset "SELECT * FROM BillingMaster WHERE Account IN (" & strAccountList & ") AND BillingYear\10 = " & intYear & strRateKeys & " AND BillingType = 'RE' AND Copies > 0 ORDER BY Name1, Account"
						// rsData.OpenRecordset "SELECT * FROM BillingMaster WHERE Account IN (" & strAccountList & ") AND BillingYear\10 = " & intYear & strRateKeys & " AND BillingType = 'RE' AND Copies > 0 " & strOrder
						// kgk trocl-646 04-20-2011  Need lien rate keys for maturity notice
						if (intType!=2) {
							rsData.OpenRecordset("SELECT * FROM BillingMaster WHERE Account IN ("+strAccountList+") AND BillingYear/10 = "+Convert.ToString(intYear)+strRateKeys+" AND BillingType = 'RE'  "+strOrder);
						} else {
							rsData.OpenRecordset("SELECT BillingMaster.*, LienRec.RateKey FROM (BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.ID) WHERE Account IN ("+strAccountList+") AND BillingYear/10 = "+Convert.ToString(intYear)+strRateKeys+" AND BillingType = 'RE'  "+strOrder);
						}
					} else {
						// rsData.OpenRecordset "SELECT * FROM BillingMaster WHERE Account IN (" & strAccountList & ") AND BillingYear\10 = " & intYear & strRateKeys & " AND BillingType = 'PP' AND Copies > 0 ORDER BY Name1, Account"
						// rsData.OpenRecordset "SELECT * FROM BillingMaster WHERE Account IN (" & strAccountList & ") AND BillingYear\10 = " & intYear & strRateKeys & " AND BillingType = 'PP' AND Copies > 0 " & strOrder
						rsData.OpenRecordset("SELECT * FROM BillingMaster WHERE Account IN ("+strAccountList+") AND BillingYear/10 = "+Convert.ToString(intYear)+strRateKeys+" AND BillingType = 'PP' "+strOrder);
					}
					if (rsData.EndOfFile()) {
						Close();
					} else {
						// kk05122016 trocls-82  Try to fix query
						// rsRE.OpenRecordset "SELECT RSAccount,OwnerPartyID,SecOwnerPartyID,InBankruptcy FROM Master WHERE RSCard = 1", strREDatabase
						rsRE.OpenRecordset("SELECT RSAccount, ISNULL(OwnerPartyID, 0) AS OwnerPartyID, ISNULL(SecOwnerPartyID, 0) AS SecOwnerPartyID, InBankruptcy "+"FROM MASTER WHERE RSCard = 1 AND (RSDeleted = 0 OR RSDeleted IS NULL) ORDER BY RSAccount", modExtraModules.strREDatabase);
						// rsRE.InsertName "OwnerPartyID,SecOwnerPartyID", "Own1,Own2", False, True, True, "", False, "", True, ""
						rsRE.InsertName("OwnerPartyID", "Own1", false, true, true, "", false, "", true, "");
						rsRE.InsertName("SecOwnerPartyID", "Own2", false, true, false, "", false, "", true, "");
						rsLien.OpenRecordset("SELECT * FROM LienRec", modExtraModules.strCLDatabase);
						frmRPTViewer.InstancePtr.Init("", ref this.Text, 100, ref this);
					}
				} else {
					Close();
				}
				return;
			}
			catch
			{	// ERROR_HANDLER:
				MessageBox.Show("Error #"+Convert.ToString(fecherFoundation.Information.Err(ex).Number)+" - "+fecherFoundation.Information.Err(ex).Description+".", "Error Initializing Summary", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			EOF = rsData.EndOfFile();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, short Shift)
		{
			switch (KeyCode) {
				
				case Keys.Escape:
				{
					Close();
					break;
				}
			} //end switch
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);
			lblTime.Caption = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm AM/PM");
			lblDate.Caption = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblMuniName.Caption = modGlobalConstants.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Caption = "Page "+this.pageNumber;
		}

		private void BindFields()
		{
			try
			{	// On Error GoTo ERROR_HANDLER
				fecherFoundation.Information.Err(ex).Clear();
				double dblPrin = 0;
				double dblPLI = 0;
				double dblCosts = 0;
				double dblCurrInt = 0;
				double dblTotal = 0;
				double dblAddedCosts = 0;
				int lngIndex = 0;
				bool boolSameName = false;
				double dblMinimum = 0;
				bool blnContinue = false;
				double dblAbate1 = 0;
				double dblAbate2 = 0;
				double dblAbate3 = 0;
				double dblAbate4 = 0;

				// trocl-1335 09.11.17 kjr
				DateTime dtBillDate;
				clsDRWrapper rsRK = new clsDRWrapper();
				string strRateKey = "";
				string strPrevSecOwnerName = "";
				string strPrevAddress1 = "";
				string strPrevAddress2 = "";
				string strPrevAddress3 = "";
				bool boolNewOwner;
				bool boolNewAddress = false;

			TryAgain: ;

				if (!rsData.EndOfFile()) {
					// this is the extra amount due from costs for this account
					lngIndex = ReturnAccountIndex(rsData.Get_Fields("Account"));
					if (lngIndex>-1) {
						dblAddedCosts = modCollections.arrDemand[lngIndex].CertifiedMailFee+modCollections.arrDemand[lngIndex].Fee;
					} else {
						dblAddedCosts = 0;
					}
					dblAbate1 = 0;
					dblAbate2 = 0;
					dblAbate3 = 0;
					dblAbate4 = 0;
					if (Convert.ToInt32(rsData.Get_Fields("LienRecordNumber")) == 0) {
						// regular billing
						dblPrin = rsData.Get_Fields("TaxDue1")+rsData.Get_Fields("TaxDue2")+rsData.Get_Fields("TaxDue3")+rsData.Get_Fields("TaxDue4")-rsData.Get_Fields("PrincipalPaid");
						dblPLI = (rsData.Get_Fields("InterestCharged")*-1)-rsData.Get_Fields("InterestPaid");
						// MAL@20081022: Corrected to remove amount paid from the Costs total
						// Tracker Reference: 15773
						dblCosts = (rsData.Get_Fields("DemandFees")+dblAddedCosts)-rsData.Get_Fields("DemandFeesPaid");
						dblCurrInt = 0; // this will get filled from the calculate account function call
						modCLCalculations.CheckForAbatementPayments(rsData.Get_Fields("ID"), rsData.Get_Fields("RateKey"), Convert.ToString(rsData.Get_Fields("BillingType"))=="RE", ref dblAbate1, ref dblAbate2, ref dblAbate3, ref dblAbate4, rsData.Get_Fields("TaxDue1"), rsData.Get_Fields("TaxDue2"), rsData.Get_Fields("TaxDue3"), rsData.Get_Fields("TaxDue4"));
						dblTotal = modCLCalculations.CalculateAccountCL(ref rsData, rsData.Get_Fields("Account"), ref dtMailDate, ref dblCurrInt, , , , , , , ref dblAbate1, ref dblAbate2, ref dblAbate3, ref dblAbate4)+dblAddedCosts;
					} else {
						// lien record
						rsLien.FindFirstRecord("ID", rsData.Get_Fields("LienRecordNumber"));
						if (!rsLien.NoMatch) {
							if (rsLien.Get_Fields("Principal")-rsLien.Get_Fields("PrincipalPaid")<=0) {
								rsData.MoveNext();
								if (rsData.EndOfFile()) return;
								goto TryAgain;
							}
							dblPrin = rsLien.Get_Fields("Principal")-rsLien.Get_Fields("PrincipalPaid");
							dblPLI = rsLien.Get_Fields("Interest")+(rsLien.Get_Fields("InterestCharged")*-1)-rsLien.Get_Fields("InterestPaid")-rsLien.Get_Fields("PLIPaid");
							if (intType==1) {
								// Lien Transfer
								dblCosts = (rsLien.Get_Fields("Costs")+dblAddedCosts)-rsLien.Get_Fields("CostsPaid");
							} else if (intType==2) {
								// Lien Maturity
								dblCosts = dblAddedCosts;
							}
							dblCurrInt = 0;
							dblTotal = modCLCalculations.CalculateAccountCLLien(ref rsLien, ref dtMailDate, ref dblCurrInt);
							dblTotal = dblPrin+dblPLI+dblCurrInt+dblAddedCosts;
						} else {
							dblPrin = 0;
							dblPLI = 0;
							dblCosts = 0;
							dblCurrInt = 0;
						}
					}

					// MAL@20071001: Add check for minimum amount
					if (frmFreeReport.InstancePtr.dblMinimumAmount>0) {
						dblMinimum = frmFreeReport.InstancePtr.dblMinimumAmount;
					} else {
						dblMinimum = 0;
					}

					if (dblMinimum>0) {
						blnContinue = ((dblTotal-dblAddedCosts)>=dblMinimum);
					} else {
						blnContinue = ((dblTotal-dblAddedCosts)>dblMinimum);
					}

					if (blnContinue) {
						// dblTotal = dblPrin + dblPLI + dblCosts + dblCurrInt

						fldPrincipal.Text = Strings.Format(dblPrin, "$#,##0.00");
						fldPLInt.Text = Strings.Format(dblPLI, "$#,##0.00");
						fldCosts.Text = Strings.Format(dblCosts, "$#,##0.00");
						fldCurrentInt.Text = Strings.Format(dblCurrInt, "$#,##0.00");
						fldTotal.Text = Strings.Format(dblTotal, "$#,##0.00");

						dblTotalPrin += dblPrin;
						dblTotalPLI += dblPLI;
						dblTotalCost += dblCosts;
						dblTotalCurrentInt += dblCurrInt;
						dblTotalTotal += dblTotal;

						fldAccount.Text = rsData.Get_Fields("Account");
						fldName.Text = rsData.Get_Fields("Name1"); // trocls-119 4.23.18 kjr CODE FREEZE
						if (fecherFoundation.Strings.Trim(Convert.ToString(rsData.Get_Fields("Name2")))!="") {
							fldName.Text = fldName.Text+"\r\n"+rsData.Get_Fields("Name2");
						}

						fldAddress.Text = "";
						if (fecherFoundation.Strings.Trim(Convert.ToString(rsData.Get_Fields("Address1")))!="") {
							fldAddress.Text = rsData.Get_Fields("Address1")+"\r\n";
						}
						if (fecherFoundation.Strings.Trim(Convert.ToString(rsData.Get_Fields("Address2")))!="") {
							fldAddress.Text = fldAddress.Text+rsData.Get_Fields("Address2")+"\r\n";
						}
						if (fecherFoundation.Strings.Trim(Convert.ToString(rsData.Get_Fields("Address3")))!="") {
							fldAddress.Text = fldAddress.Text+rsData.Get_Fields("Address3"); // rsData.Fields("City") & ", " & rsData.Fields("State") & " " & rsData.Fields("Zip")
						}

						strRateKey = Convert.ToString(rsData.Get_Fields("RateKey"));
						rsRK.OpenRecordset("SELECT * FROM RateRec WHERE ID = "+strRateKey, modExtraModules.strCLDatabase);
						if (!rsRK.EndOfFile()) {
							dtBillDate = (DateTime)rsRK.Get_Fields("CommitmentDate");
						} else {
							if (Information.IsDate(rsData.Get_Fields("TransferFromBillingDateFirst"))) {
								dtBillDate = (DateTime)rsData.Get_Fields("TransferFromBillingDateFirst");
							} else if (Information.IsDate(rsData.Get_Fields("TransferFromBillingDateLast"))) {
								dtBillDate = (DateTime)rsData.Get_Fields("TransferFromBillingDateLast");
							} else {
								dtBillDate = DateTime.Today;
							}
						}

						boolNewOwner = modCLCalculations.NewOwner(rsData.Get_Fields("Name1"), rsData.Get_Fields("Account"), ref dtBillDate, , ref boolSameName);
						rsRE.FindFirstRecord("RSAccount", rsData.Get_Fields("Account"));
						// If Not rsRE.NoMatch Then
						// boolSameName = Trim(UCase(rsData.Fields("Name1"))) = Trim(UCase(rsRE.Fields("Own1FullName")))
						// End If
						if (fecherFoundation.Strings.Trim(fldAddress.Text)=="" || intBilledOwnerAddress==1) {
							if (!rsRE.EndOfFile()) {
								if (rsRE.Get_Fields("InBankruptcy") && intType==2) {
									// skip this account because it is in bankruptcy
									rsData.MoveNext();
									fldAddress.Text = "";
									fldAccount.Text = "";
									fldPrincipal.Text = "";
									fldPLInt.Text = "";
									fldCosts.Text = "";
									fldCurrentInt.Text = "";
									fldTotal.Text = "";
									fldName.Text = "";
									if (rsData.EndOfFile()) return;
									goto TryAgain;
								}
								if (!boolSameName) {
									fldName.Text = fldName.Text+"\r\n"+"C/O "+rsRE.Get_Fields("Own1FullName");
								}
								if (fecherFoundation.Strings.Trim(Convert.ToString(rsRE.Get_Fields("Own1Address1")))!="") {
									fldAddress.Text = rsRE.Get_Fields("Own1Address1")+"\r\n";
								}
								if (fecherFoundation.Strings.Trim(Convert.ToString(rsRE.Get_Fields("Own1Address2")))!="") {
									fldAddress.Text = fldAddress.Text+rsRE.Get_Fields("Own1Address2")+"\r\n";
								}
								fldAddress.Text = fldAddress.Text+rsRE.Get_Fields("Own1City")+", "+rsRE.Get_Fields("Own1State")+" "+rsRE.Get_Fields("Own1Zip");
							}
						} else if (intBilledOwnerAddress==2 && boolNewOwner) {
							boolNewAddress = modCLCalculations.OwnerLastRecordedAddress(rsData.Get_Fields("Name1"), rsData.Get_Fields("Account"), ref dtBillDate, ref strPrevSecOwnerName, ref strPrevAddress1, ref strPrevAddress2, ref strPrevAddress3); // 4.20.18

							if (boolNewAddress) {
								// found the prev owner
								fldName.Text = rsData.Get_Fields("Name1");
								if (fecherFoundation.Strings.Trim(strPrevSecOwnerName)!="") {
									fldAddress.Text = strPrevSecOwnerName+"\r\n";
								}
								if (fecherFoundation.Strings.Trim(strPrevAddress1)!="") {
									fldAddress.Text = strPrevAddress1+"\r\n";
								}
								if (fecherFoundation.Strings.Trim(strPrevAddress2)!="") {
									fldAddress.Text = fldAddress.Text+strPrevAddress2+"\r\n";
								}
								fldAddress.Text = fldAddress.Text+strPrevAddress3;
							}
						}

						rsData.MoveNext();
					} else { // MAL@20071001
						rsData.MoveNext();
						if (rsData.EndOfFile()) return;
						goto TryAgain;
					}
				} else {
					fldAccount.Text = "";
					fldAddress.Text = "";
					fldName.Text = "";
				}

				return;
			}
			catch
			{	// ERROR_HANDLER:
				MessageBox.Show("Error #"+Convert.ToString(fecherFoundation.Information.Err(ex).Number)+" - "+fecherFoundation.Information.Err(ex).Description+".", "Error Binding Fields", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		// Private Sub FillDemandArray()
		// On Error GoTo ERROR_HANDLER
		// Dim rsDem                   As New clsDRWrapper
		// Dim lngIndex                As Long
		// Dim strSQL                  As String
		// Erase arrDemand
		// 
		// strSQL = "SELECT * FROM BillingMaster WHERE LienProcessStatus = 1 AND LienStatusEligibility = 2 AND BillingType = 'RE' ORDER BY Name1"     'this will be all the records that have had 30 Day Notices printed
		// rsDem.OpenRecordset strSQL, strCLDatabase
		// If rsDem.EndOfFile Then
		// Unload frmWait
		// MsgBox "There are no accounts eligible to have demand fees applied.", vbInformation, "No Eligible Accounts"
		// Unload Me
		// End If
		// 
		// Do Until rsDem.EndOfFile
		// ReDim Preserve arrDemand(lngIndex) 'this will make sure that there are enough elements to use
		// arrDemand(lngIndex).Used = True
		// arrDemand(lngIndex).Processed = False
		// arrDemand(lngIndex).Account = rsDem.Fields("Account")
		// arrDemand(lngIndex).Name = rsDem.Fields("Name1")
		// 
		// If rsDem.Fields("DemandFees") > 0 Then
		// highlight the row and set the value = zero
		// Else
		// gonna have to figure out the amount
		// arrDemand(lngIndex).Fee = dblDemand
		// 
		// If boolChargeCert Then
		// If boolChargeMort Then
		// arrDemand(lngIndex).CertifiedMailFee = CDbl(.TextMatrix(.rows - 1, 4))
		// Else    'may have to add the new owner charge here...
		// If boolChargeForNewOwner And NewOwner Then
		// arrDemand(lngIndex).CertifiedMailFee = CDbl(.TextMatrix(.rows - 1, 4))
		// Else
		// arrDemand(lngIndex).CertifiedMailFee = CDbl(.TextMatrix(.rows - 1, 4))
		// End If
		// End If
		// Else
		// arrDemand(lngIndex).CertifiedMailFee = 0
		// End If
		// End If
		// 
		// lngIndex = lngIndex + 1
		// rsDem.MoveNext
		// Loop
		// Exit Sub
		// ERROR_HANDLER:
		// MsgBox "Error #" & Err.Number & " - " & Err.Description & ".", vbCritical, "Error Filling Demand Array"
		// End Sub

		private int ReturnAccountIndex(ref int lngAcct)
		{
			int ReturnAccountIndex = 0;
			try
			{	// On Error GoTo ERROR_HANDLER
				fecherFoundation.Information.Err(ex).Clear();
				// this function will return the array index for the account passed in...-1 if not found
				int lngCT;
				bool boolFound = false;

				for(lngCT=0; lngCT<=Information.UBound(modCollections.arrDemand, 1); lngCT++) {
					if (modCollections.arrDemand[lngCT].Account==lngAcct) {
						boolFound = true;
						break;
					}
				}

				if (boolFound) {
					ReturnAccountIndex = lngCT;
				} else {
					ReturnAccountIndex = -1;
				}
				return ReturnAccountIndex;
			}
			catch
			{	// ERROR_HANDLER:
				if (fecherFoundation.Information.Err(ex).Number!=9) { // this is when the array is empty
					MessageBox.Show("Error #"+Convert.ToString(fecherFoundation.Information.Err(ex).Number)+" - "+fecherFoundation.Information.Err(ex).Description+".", "Error Finding Array Index", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				} else {
					ReturnAccountIndex = -1;
				}
			}
			return ReturnAccountIndex;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			// fill in the totals
			fldTotalPrincipal.Text = Strings.Format(dblTotalPrin, "$#,##0.00");
			fldTotalPLInt.Text = Strings.Format(dblTotalPLI, "$#,##0.00");
			fldTotalCosts.Text = Strings.Format(dblTotalCost, "$#,##0.00");
			fldTotalCurrentInt.Text = Strings.Format(dblTotalCurrentInt, "$#,##0.00");
			fldTotalTotal.Text = Strings.Format(dblTotalTotal, "$#,##0.00");
		}

		private void rptLienNoticeSummary_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptLienNoticeSummary properties;
			//rptLienNoticeSummary.Caption	= "Account Detail";
			//rptLienNoticeSummary.Icon	= "rptNoticeSummary.dsx":0000";
			//rptLienNoticeSummary.Left	= 0;
			//rptLienNoticeSummary.Top	= 0;
			//rptLienNoticeSummary.Width	= 11880;
			//rptLienNoticeSummary.Height	= 8595;
			//rptLienNoticeSummary.StartUpPosition	= 3;
			//rptLienNoticeSummary.SectionData	= "rptNoticeSummary.dsx":058A;
			//End Unmaped Properties
		}
	}
}