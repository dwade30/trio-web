﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using GrapeCity.ActiveReports;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptReminderMailer : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               12/23/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/31/2006              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsRate = new clsDRWrapper();
		clsDRWrapper rsTemp = new clsDRWrapper();
		string strHeader;
		string strPayment;
		string strNotPastDue;
		string strPastDue;
		string strPreMessage;
		DateTime dtDate;
		bool boolDeleteLastPage;
		int intPer;
		bool boolReturnAddress;
		// VBto upgrade warning: strYear As string	OnReadFCConvert.ToInt32(
		string strYear;
		bool boolShowPendingAmount;
		string strBulkMailer = "";
		int lngIndex;
		bool boolNoSummary;
		bool boolLienedRecords;
		double dblMinimumAmount;
		// MAL@20071207
		string strFields = "";
        private ReminderNoticeOptions reminderNoticeOptions;
        private cAddressControllerCL tAddrCtrlr = new cAddressControllerCL();

		public rptReminderMailer()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Reminder Notice - Mailer";
		}

		public static rptReminderMailer InstancePtr
		{
			get
			{
				return (rptReminderMailer)Sys.GetInstance(typeof(rptReminderMailer));
			}
		}

		protected rptReminderMailer _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsTemp?.Dispose();
				rsData?.Dispose();
				rsRate?.Dispose();
            }
			base.Dispose(disposing);
		}

        public void Init(ReminderNoticeOptions options)
        {
            // Init(string strSQL, string strPassHeader, string strPassYear, string strPassPayment, string strPassNotPastDue, string strPassPastDue, string strPassPreMessage, DateTime dtMailingDate, bool boolPassLienedRecords, double dblPassMinAmount)
            try
            {
                // On Error GoTo ERROR_HANDLER
                reminderNoticeOptions = options;
				string strMod = "";
				//clsReportPrinterFunctions rpfFont = new clsReportPrinterFunctions();
				string strPrinterFont;
				int const_PrintToolID;
				int lngCT;
				int intCopies/*unused?*/;
				int intStartPage/*unused?*/;
				int intEndPage/*unused?*/;
				string strOldPrinter = "";
				int intReturn;
                boolLienedRecords = reminderNoticeOptions.LienedRecords;
                frmWait.InstancePtr.Unload();
                boolShowPendingAmount = reminderNoticeOptions.ShowPending;       // What is this????   FCConvert.CBool(frmReminderNotices.InstancePtr.chkInterest.CheckState == Wisej.Web.CheckState.Checked);
                boolReturnAddress = reminderNoticeOptions.UseReturnAddress;
                strYear = reminderNoticeOptions.HighYear;
                dblMinimumAmount = reminderNoticeOptions.MinimumAmount;
				// reset the summary counter
                FCUtils.EraseSafe(modReminderNoticeSummary.Statics.arrReminderSummaryList);
				lngIndex = 0;
                if (reminderNoticeOptions.BulkMailing)
				{
                    strBulkMailer = modGlobal.GetBulkMailerString();
				}
				else
				{
					strBulkMailer = "";
				}

				const_PrintToolID = 9950;

				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Report");
                // use the info that is passed into the report
                strHeader = reminderNoticeOptions.ReportTitle;
                // strRateKeyList = strPassRK
                strNotPastDue = reminderNoticeOptions.Message;
                strPastDue = reminderNoticeOptions.PastDueMessage;
                strPreMessage = reminderNoticeOptions.PreMessage;
                dtDate = reminderNoticeOptions.MailDate;
                lblPrintedDate.Text = Strings.Format(dtDate, "MM/dd/yyyy");
				if (modStatusPayments.Statics.boolRE)
				{
					rsData.OpenRecordset(reminderNoticeOptions.Query, modExtraModules.strCLDatabase);
					strMod = "Real Estate";
				}
				else
				{
					rsData.OpenRecordset(reminderNoticeOptions.Query, modExtraModules.strCLDatabase);
					// strPPDatabase
					strMod = "Personal Property";
				}
				if (rsData.EndOfFile())
				{
					frmWait.InstancePtr.Unload();
					boolNoSummary = true;
					FCMessageBox.Show("No matching records were found.", MsgBoxStyle.Information, strMod + " Reminder Mailer");
				}
				else
				{
					boolNoSummary = false;
                    // this will reset the values used
                    if (reminderNoticeOptions.Period == ReminderNoticeOptions.PeriodEnum.Period1)
                    {
						// period 1
						intPer = 1;
						strPayment = "first ";
					}
					else if (reminderNoticeOptions.Period == ReminderNoticeOptions.PeriodEnum.Period2)
                    {
						// period 2
						intPer = 2;
						strPayment = "second ";
					}
					else if (reminderNoticeOptions.Period == ReminderNoticeOptions.PeriodEnum.Period3)
                    {
						// period 3
						intPer = 3;
						strPayment = "third ";
					}
					else if (reminderNoticeOptions.Period == ReminderNoticeOptions.PeriodEnum.Period4)
                    {
						intPer = 4;
						strPayment = "fourth ";
					}
                    else if (reminderNoticeOptions.Period == ReminderNoticeOptions.PeriodEnum.Outstanding)
					{
                        intPer = 4;
						strPayment = "";
					}
					else
					{
						intPer = 4;
						strPayment = "";
					}
					// keep going
					rsRate.OpenRecordset("SELECT * FROM RateRec", modExtraModules.strCLDatabase);
                    if (modStatusPayments.Statics.boolRE)
                    {
                        strFields = "Account,Name1,Name2,Address1,Address2,Address3,BillingYear,BillingMaster.ID,RateKey,LienRecordNumber,MapLot,StreetNumber,StreetName,TranCode," + "TaxDue1,TaxDue2,TaxDue3,TaxDue4,PrincipalPaid,DemandFees,DemandFeesPaid,InterestCharged,InterestPaid,InterestAppliedThroughDate,AbatementPaymentMade," + "TransferFromBillingDateFirst,TransferFromBillingDateLast";
                    }
                    else
                    {
                        strFields = "BillingMaster.Account,Name1,Name2,BillingMaster.Address1,BillingMaster.Address2,Address3,BillingYear,BillingMaster.ID,RateKey," + "TaxDue1,TaxDue2,TaxDue3,TaxDue4,PrincipalPaid,DemandFees,DemandFeesPaid,InterestCharged,InterestPaid,InterestAppliedThroughDate,AbatementPaymentMade," + "TransferFromBillingDateFirst,TransferFromBillingDateLast";
                    }
                    lblSpacer.Top = 4F - 256 / 1440F;
					// this sits on the bottom of the form to make sure that the detail section is still large enough
					frmWait.InstancePtr.Unload();

					frmReportViewer.InstancePtr.Init(this, this.Document.Printer.PrinterName);
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Initializing Mailers");
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
			//Detail_Format();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			frmWait.InstancePtr.Unload();
			if (boolDeleteLastPage)
			{
				this.Document.Pages.RemoveAt(this.Document.Pages.Count - 1);
				//this.Document.Pages.Commit();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			frmWait.InstancePtr.Unload();
			if (!boolNoSummary)
			{
				modReminderNoticeSummary.Statics.intRPTReminderSummaryType = 3;
                //FC:FINAL:SBE - #i331 - in VB6 Show is executed, and standard report viewer is displayed. Use frmReportViewer, because we don't have a standard report viewer
                //rptReminderSummary.InstancePtr.Run();
                Sys.ClearInstance(frmReportViewer.InstancePtr);
                frmReportViewer.InstancePtr.Init(rptReminderSummary.InstancePtr);
			}
		}

		private void ActiveReport_ToolbarClick(/*DDActiveReports2.DDTool Tool*/)
		{
			bool bReturn/*unused?*/;
			DialogResult intReturn = 0;
			string VBtoVar = ""/*Tool.Text*/;
			if (VBtoVar == "Print...")
			{
				if (this.Document.Pages.Count > 1)
				{
					intReturn = DialogResult.No;
					this.Document.Printer.PrinterSettings.FromPage = 1;
					this.Document.Printer.PrinterSettings.ToPage = 1;
					while (intReturn == DialogResult.No)
					{
						//FC:FINAL:SBE - use Print() extension for Document
						//this.Document.Printer.Print();
						this.Document.Print(false);
						intReturn = FCMessageBox.Show("Is the mailer correctly aligned?" + "\r\n" + "Choose No to re-align, Yes to continue.", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Align Mailer");
					}
					if (intReturn != DialogResult.Cancel)
					{
						this.Document.Printer.PrinterSettings.FromPage = 2;
						this.Document.Printer.PrinterSettings.ToPage = this.Document.Pages.Count;
						//FC:FINAL:SBE - use Print() extension for Document
						//this.Document.Printer.Print();
						this.Document.Print(false);
					}
				}
				else
				{
					//FC:FINAL:SBE - use Print() extension for Document
					//this.Document.Printer.Print();
					this.Document.Print(false);
				}
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			TRYAGAIN:
			;
			if (!rsData.EndOfFile())
			{
				// MAL@20080115: Check for minimum amount
				if (modStatusPayments.Statics.boolRE)
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (modCLCalculations.CalculateAccountTotal(FCConvert.ToInt32(rsData.Get_Fields("Account")), modStatusPayments.Statics.boolRE) < dblMinimumAmount)
					{
						rsData.MoveNext();
						goto TRYAGAIN;
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsTemp.OpenRecordset("SELECT " + strFields + " FROM BillingMaster INNER JOIN " + modGlobal.Statics.strDbRE + "Master ON BillingMaster.Account = Master.RSAccount WHERE BillingType = 'RE' AND Account = " + FCConvert.ToString(rsData.Get_Fields("Account")) + " ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
						rsTemp.InsertName("Owner1PartyID,SecOwnerPartyID", "Own1,Own2", false, true, true, "", false, "", true, "");
						if (!rsTemp.EndOfFile())
						{
							if (Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue4")) - Conversion.Val(rsTemp.Get_Fields_Decimal("PrincipalPaid")) > 0)
							{
								PrintPostCards();
							}
							else
							{
								rsData.MoveNext();
								goto TRYAGAIN;
							}
						}
						rsData.MoveNext();
					}
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (modCLCalculations.CalculateAccountTotal(FCConvert.ToInt32(rsData.Get_Fields("Account")), modStatusPayments.Statics.boolRE) < dblMinimumAmount)
					{
						rsData.MoveNext();
						goto TRYAGAIN;
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsTemp.OpenRecordset("SELECT " + strFields + " FROM BillingMaster INNER JOIN " + modGlobal.Statics.strDbPP + "PPMaster ON BillingMaster.Account = PPMaster.Account WHERE BillingType = 'PP' AND BillingMaster.Account = " + FCConvert.ToString(rsData.Get_Fields("Account")) + " ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
						rsTemp.InsertName("PartyID", "Own1", false, true, true, "", false, "", true, "");
						if (!rsTemp.EndOfFile())
						{
							if (Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue4")) - Conversion.Val(rsTemp.Get_Fields_Decimal("PrincipalPaid")) > 0)
							{
								PrintPostCards();
							}
							else
							{
								rsData.MoveNext();
								goto TRYAGAIN;
							}
						}
						rsData.MoveNext();
					}
				}
			}
		}

		private void PrintPostCards()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill in the fields for post card
				int intCTRLIndex/*unused?*/;
				string str1 = "";
				string str2 = "";
				string str3 = "";
				string str4 = "";
				string str5 = "";
				double dblDue = 0;
				double dblPaid = 0;
				double dblPeriodDue = 0;
				int intRatePer = 0;
				double dblPrinToPeriod = 0;
				double dblPrinAfterPer;
				double[] dblAbateAmt = new double[4 + 1];
				bool boolAbatementPayment = false;
				int lngAcct = 0;
				double dblInterest = 0;
                bool boolNewOwner = false; // trocls-119 10.6.17 kjr 4.23.18 CODE FREEZE
                bool boolNewAddress = false;
                DateTime dtBillDate;
                bool boolSameName = false;
                bool boolREMatch = false;
                string strPrevSecOwnerName = "";
                string strPrevAddress1 = "";
                string strPrevAddress2 = "";
                string strPrevAddress3 = "";

                cPartyController pCont = new cPartyController(); // trocls-143 2.9.18 kjr
                                                                 // vbPorter upgrade warning: pInfo As CParty	OnWrite(cParty)
                cParty pInfo = new cParty();
                cPartyAddress pAdd;
                clsDRWrapper rsMastOwner = new clsDRWrapper();
                short intDummyVar; // kjr 4.30.18

                GETNEXTACCOUNT:
				;
				if (Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Name2"))) != "")
				{
					str1 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Name1"))) + " and " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Name2")));
				}
				else
				{
					str1 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Name1")));
				}

				clsAddress tAddr;

				switch (reminderNoticeOptions.AddressType)
				{
					case ReminderNoticeOptions.AddressEnum.AddressAtBilling:
						tAddr = tAddrCtrlr.GetOwnerAndAddressAtBilling(rsTemp);
						break;
					case ReminderNoticeOptions.AddressEnum.CareOfCurrentOwner:
						tAddr = tAddrCtrlr.GetOwnerAtBillingCareOfCurrentOwner(rsTemp);
						break;
					case ReminderNoticeOptions.AddressEnum.LastKnownAddress:
						tAddr = tAddrCtrlr.GetBilledOwnerAtLastAddress(rsTemp);
						break;
					default:
						tAddr = new clsAddress();
						break;
				}

				str2 = tAddr.Get_Address(1).Trim();
				str3 = tAddr.Get_Address(2).Trim();
				str4 = tAddr.Get_Address(3).Trim();
				str5 = tAddr.Get_Address(4).Trim();


				////// get the mailing address from BillingMaster
				////str2 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address1")));
				////str3 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address2")));
				////str4 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address3")));
    ////            if (reminderNoticeOptions.AddressType != ReminderNoticeOptions.AddressEnum.AddressAtBilling)
    ////            { // 5.21.18 CODE FREEZE
    ////                if (modStatusPayments.Statics.boolRE)
    ////                { // trocls-119 10.6.17 kjr - For RE and Billed Owner @ last known addr, 5.21.18
    ////                    if (Information.IsDate(rsTemp.Get_Fields("TransferFromBillingDateFirst")))
    ////                    {
    ////                        dtBillDate = (DateTime)rsTemp.Get_Fields("TransferFromBillingDateFirst");
    ////                    }
    ////                    else
    ////                    {
    ////                        dtBillDate = (DateTime)rsTemp.Get_Fields("TransferFromBillingDateLast");
    ////                    }
    ////                    boolNewOwner = modCLCalculations.NewOwner(rsTemp.Get_Fields("Name1"), rsTemp.Get_Fields("Account"), dtBillDate,  ref boolREMatch,  ref boolSameName);

    ////                    if (reminderNoticeOptions.AddressType == ReminderNoticeOptions.AddressEnum.LastKnownAddress)
    ////                    {
    ////                        var strCity = "";
    ////                        var strState = "";
    ////                        var strZip = "";
    ////                        var strZip4 = "";
    ////                        boolNewAddress = modCLCalculations.OwnerLastRecordedAddress(rsTemp.Get_Fields("Name1"), rsTemp.Get_Fields("Account"),  dtBillDate, ref strPrevSecOwnerName, ref strPrevAddress1, ref strPrevAddress2, ref strPrevAddress3,ref strCity, ref strState, ref strZip, ref strZip4); // 4.20.18

    ////                        if (boolNewOwner && boolNewAddress)
    ////                        {
    ////                            str2 = fecherFoundation.Strings.Trim(strPrevAddress1);
    ////                            str3 = fecherFoundation.Strings.Trim(strPrevAddress2);
    ////                            str4 = fecherFoundation.Strings.Trim(strPrevAddress3);
    ////                        }
    ////                    }
    ////                }

    ////                if ((fecherFoundation.Strings.Trim(str2) == "" && fecherFoundation.Strings.Trim(str3) == "") || reminderNoticeOptions.AddressType == ReminderNoticeOptions.AddressEnum.CareOfCurrentOwner || (boolSameName && boolREMatch))
    ////                {
    ////                    var strDeedName = "";
    ////                    // this will fill the address form the RE Master/PP Master table for the mail address

    ////                    // We used to get the owner info in inner join that was slow.  I moved that down to here to speed it up.  We now use party classes to get infor to use in labels
    ////                    if (modStatusPayments.Statics.boolRE)
    ////                    {
    ////                        rsMastOwner.OpenRecordset("Select OwnerPartyID as OwnerID, deedname1 FROM Master WHERE RSAccount = " + rsTemp.Get_Fields("Account"), modExtraModules.strREDatabase);
    ////                    }
    ////                    else
    ////                    {
    ////                        rsMastOwner.OpenRecordset("Select PartyID as OwnerID FROM PPMaster WHERE Account = " + rsTemp.Get_Fields("Account"), modExtraModules.strPPDatabase);
    ////                    }

    ////                    // If we find the master record in PP or RE then we try to get party info
    ////                    if (rsMastOwner.EndOfFile() != true && rsMastOwner.BeginningOfFile() != true)
    ////                    {
    ////                        pInfo = pCont.GetParty(rsMastOwner.Get_Fields("OwnerID")); // kk07152015 trocls-58 .Fields("OwnerPartyID")
    ////                        if (modStatusPayments.Statics.boolRE)
    ////                        {
    ////                            strDeedName = rsMastOwner.Get_Fields_String("DeedName1");
    ////                        }
    ////                        else
    ////                        {
    ////                            strDeedName = pInfo.FullNameLastFirst;
    ////                        }
    ////                    }

    ////                    // If we find part info then we attempt to get address info
    ////                    if (!(pInfo == null))
    ////                    {
    ////                        pAdd = pInfo.GetAddress("CL", rsMastOwner.Get_Fields("OwnerID")); // kk07152015 trocls-58 .Fields("OwnerPartyID")

    ////                        if (rsTemp.Get_Fields("Name1") != strDeedName && reminderNoticeOptions.AddressType == ReminderNoticeOptions.AddressEnum.CareOfCurrentOwner)
    ////                        {
    ////                            str2 = "C\\O " + strDeedName;
    ////                        }
    ////                        else
    ////                        {
	   ////                         str2 = "";
    ////                        }

				////			// If we found an address we fill it in otherwise leave it blank
				////			if (!(pAdd == null))
    ////                        {
    ////                            str3 = fecherFoundation.Strings.Trim(pAdd.Address1);
    ////                            str4 = fecherFoundation.Strings.Trim(pAdd.Address2);
    ////                            str5 = fecherFoundation.Strings.Trim(pAdd.City) + " " + fecherFoundation.Strings.Trim(pAdd.State) + " " + fecherFoundation.Strings.Trim(pAdd.Zip);
    ////                        }
    ////                    }
    ////                }
    ////            }

                dblInterest = 0;

                if (boolShowPendingAmount)
                {
                    modCLCalculations.CalculateAccountCL(ref rsTemp, rsTemp.Get_Fields("Account"),  dtDate, ref dblInterest);
                }
                // condense the labels if some are blank
                if (Strings.Trim(str4) == string.Empty)
				{
					str4 = str5;
					str5 = "";
				}
				if (Strings.Trim(str3) == string.Empty)
				{
					str3 = str4;
					str4 = str5;
					str5 = "";
				}
				if (Strings.Trim(str2) == string.Empty)
				{
					str2 = str3;
					str3 = str4;
					str4 = str5;
					str5 = "";
				}
				if (Strings.Trim(str1) == string.Empty)
				{
					str1 = str2;
					str2 = str3;
					str3 = str4;
					str4 = str5;
					str5 = "";
				}
				lblBulk.Text = strBulkMailer;
				// this will check the abatement payments if the bill has any
				if (FCConvert.ToBoolean(rsTemp.Get_Fields_Boolean("AbatementPaymentMade")))
				{
					// check the abatement amounts
					clsDRWrapper rsPay = new clsDRWrapper();
					clsDRWrapper rsRate = new clsDRWrapper();
					boolAbatementPayment = true;
					if (modStatusPayments.Statics.boolRE)
					{
						rsPay.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + rsTemp.Get_Fields_Int32("ID") + " AND BillCode = 'R' AND Code = 'A'");
					}
					else
					{
						rsPay.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + rsTemp.Get_Fields("ID") + " AND BillCode = 'P' AND Code = 'A'");
					}
					rsRate.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + rsTemp.Get_Fields("RateKey"));
					if (!rsRate.EndOfFile())
					{
						intRatePer = rsRate.Get_Fields_Int16("NumberOfPeriods");
						while (!rsPay.EndOfFile())
						{
							if (rsPay.Get_Fields("Period") == "A")
							{
								dblAbateAmt[1] += rsPay.Get_Fields_Double( "Principal") / intRatePer;
								if (intRatePer >= 2)
									dblAbateAmt[2] += rsPay.Get_Fields_Double("Principal") / intRatePer;
								if (intRatePer >= 3)
									dblAbateAmt[3] += rsPay.Get_Fields_Double("Principal") / intRatePer;
								if (intRatePer >= 4)
									dblAbateAmt[4] += rsPay.Get_Fields_Double("Principal") / intRatePer;
							}
							else
							{
								dblAbateAmt[rsPay.Get_Fields_Int32( "Period")] += rsPay.Get_Fields_Double( "Principal");
							}
							rsPay.MoveNext();
						}
						// this will reset the values used
						switch (intPer)
						{
							case 1:
								{
									// period 1
									dblPrinToPeriod = Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue1")) - (Conversion.Val(rsTemp.Get_Fields_Decimal("PrincipalPaid")) - dblAbateAmt[2] - dblAbateAmt[3] - dblAbateAmt[4]);
									dblPrinAfterPer = Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue4")) - dblAbateAmt[2] - dblAbateAmt[3] - dblAbateAmt[4];
									break;
								}
							case 2:
								{
									// period 2
									dblPrinToPeriod = Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue2")) - (Conversion.Val(rsTemp.Get_Fields_Decimal("PrincipalPaid")) - dblAbateAmt[3] - dblAbateAmt[4]);
									dblPrinAfterPer = Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue4")) - dblAbateAmt[3] - dblAbateAmt[4];
									break;
								}
							case 3:
								{
									// period 3
									dblPrinToPeriod = Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue3")) - (Conversion.Val(rsTemp.Get_Fields_Decimal("PrincipalPaid")) - dblAbateAmt[4]);
									dblPrinAfterPer = Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue4")) - dblAbateAmt[4];
									break;
								}
							case 4:
								{
									// period 4
									//FC:FINAL:JEI:IT500: added Conversion.Val to the last value
									dblPrinToPeriod = Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue4")) - Conversion.Val(rsTemp.Get_Fields_Decimal("PrincipalPaid"));
									dblPrinAfterPer = 0;
									break;
								}
						}
						//end switch
					}
					else
					{
						// just let everything go as normal because this is
						// missing the rate key and probably is not billed yet
						// so will not have any amount due
					}
					// check to see if this year needs to be shown
					if (dblPrinToPeriod > 0)
					{
						// there is still tax due
						// so allow the program to
					}
					else
					{
						// there is no tax due, so try the next bill
						// then check to make sure that there are no ababtements that would allow this bill to be shown
						rsData.MoveNext();
						if (!rsData.EndOfFile())
						{
							goto GETNEXTACCOUNT;
						}
						else
						{
							boolDeleteLastPage = true;
							return;
						}
					}
				}
				else
				{
					switch (intPer)
					{
					// this will find all of the taxes due prior to the upcoming period and also how much is due for the next period
						case 1:
							{
								dblDue = Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue1"));
								dblPeriodDue = Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue1"));
								break;
							}
						case 2:
							{
								dblDue = Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue2"));
								dblPeriodDue = Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue2"));
								break;
							}
						case 3:
							{
								dblDue = Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue3"));
								dblPeriodDue = Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue3"));
								break;
							}
						case 4:
							{
								dblDue = Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue4"));
								dblPeriodDue = Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue4"));
								break;
							}
					}
					//end switch
					dblPaid = Conversion.Val(rsTemp.Get_Fields_Decimal("PrincipalPaid"));
					if (dblDue - dblPaid <= 0)
					{
						rsData.MoveNext();
						if (!rsData.EndOfFile())
						{
							if (modStatusPayments.Statics.boolRE)
							{
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								rsTemp.OpenRecordset("SELECT " + strFields + " FROM BillingMaster INNER JOIN " + modGlobal.Statics.strDbRE + "Master ON BillingMaster.Account = Master.RSAccount WHERE BillingType = 'RE' AND Account = " + FCConvert.ToString(rsData.Get_Fields("Account")) + " ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
								rsTemp.InsertName("Owner1PartyID,SecOwnerPartyID", "Own1,Own2", false, true, true, "", false, "", true, "");
							}
							else
							{
								// rsTemp.OpenRecordset "SELECT * FROM BillingMaster INNER JOIN Master ON BillingMaster.Account = Master.RSAccount WHERE BillingType = 'PP' AND Account = " & rsData.Fields("Account") & " ORDER BY BillingYear desc", strCLDatabase
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								rsTemp.OpenRecordset("SELECT " + strFields + " FROM BillingMaster INNER JOIN " + modGlobal.Statics.strDbPP + "PPMaster ON BillingMaster.Account = PPMaster.RSAccount WHERE BillingType = 'PP' AND Account = " + FCConvert.ToString(rsData.Get_Fields("Account")) + " ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
								rsTemp.InsertName("PartyID", "Own1", false, true, true, "", false, "", true, "");
							}
							goto GETNEXTACCOUNT;
						}
						else
						{
							boolDeleteLastPage = true;
							return;
						}
					}
				}
				if (boolReturnAddress)
				{
                    // Return Address
                    lblReturnAddress1.Text = reminderNoticeOptions.ReturnAddress1;
                    lblReturnAddress2.Text = reminderNoticeOptions.ReturnAddress2;
                    lblReturnAddress3.Text = reminderNoticeOptions.ReturnAddress3;
                    lblReturnAddress4.Text = reminderNoticeOptions.ReturnAddress4;
				}
                // Account Number
                if (modStatusPayments.Statics.boolRE)
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					lngAcct = FCConvert.ToInt32(rsTemp.Get_Fields("Account"));
					lblMapLot.Text = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("MapLot")));
					// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
					if (Conversion.Val(rsTemp.Get_Fields("StreetNumber")) != 0)
					{
						// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
						lblLocation.Text = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("StreetNumber")) + " " + FCConvert.ToString(rsTemp.Get_Fields_String("StreetName")));
					}
					else
					{
						lblLocation.Text = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("StreetName")));
					}
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					lngAcct = FCConvert.ToInt32(rsTemp.Get_Fields("Account"));
				}
				lblAccount.Text = "Account: " + FCConvert.ToString(lngAcct);
				// Name
				lblName.Text = str1;
				// Mailing Address
				lblMailingAddress1.Text = str2;
				lblMailingAddress2.Text = str3;
				lblMailingAddress3.Text = str4;
				lblMailingAddress4.Text = str5;
				// Message
				// find out if the account has outstanding taxes
				if ((!boolAbatementPayment && modGlobal.Round((dblDue - dblPeriodDue), 2) <= modGlobal.Round(dblPaid, 2)) || (boolAbatementPayment && dblPrinToPeriod > 0))
				{
					// check the taxes due before the upcoming period and compare it to the taxes already paid
					// this account has no outstanding taxes
					if (modStatusPayments.Statics.boolRE)
					{
						lblHeader.Text = "******  " + strYear + " Real Estate Tax - Reminder Notice  ******";
						lblPreMessage.Text = "This is to remind you that the " + strPayment + "payment on your " + strYear + " Real Estate tax bill is due as of " + Strings.Format(dtDate, "MM/dd/yyyy") + "." + "\r\n" + strPreMessage;
					}
					else
					{
						lblHeader.Text = "******  " + strYear + " Personal Property Tax - Reminder Notice  ******";
						lblPreMessage.Text = "This is to remind you that the " + strPayment + "payment on your " + strYear + " Personal Property tax bill is due as of " + Strings.Format(dtDate, "MM/dd/yyyy") + "." + "\r\n" + strPreMessage;
					}
					if (boolAbatementPayment)
					{
						lblAmount.Text = "Amount Due " + Strings.Format(dtDate, "MM/dd/yyyy") + "   $" + Strings.Format(dblPrinToPeriod, "#,##0.00");
					}
					else
					{
						if (dblPaid > 0)
						{
							if (dblDue >= dblPaid)
							{
								lblAmount.Text = "Amount Due " + Strings.Format(dtDate, "MM/dd/yyyy") + "   $" + Strings.Format(dblDue - dblPaid, "#,##0.00");
							}
							else
							{
								lblAmount.Text = "Amount Due " + Strings.Format(dtDate, "MM/dd/yyyy") + "   $" + Strings.Format(0, "#,##0.00");
							}
						}
						else
						{
							lblAmount.Text = "Amount Due " + Strings.Format(dtDate, "MM/dd/yyyy") + "   $" + Strings.Format(dblDue, "#,##0.00");
						}
					}
					lblMessage.Text = strNotPastDue;
				}
				else
				{
					// outstanding taxes due
					if (modStatusPayments.Statics.boolRE)
					{
						lblHeader.Text = "******  " + strYear + " Real Estate Tax - Reminder Notice  ******";
						lblPreMessage.Text = "This is to remind you that you have past due amounts on your " + strYear + " Real Estate tax bill and that the " + strPayment + "payment is due as of " + Strings.Format(dtDate, "MM/dd/yyyy") + "." + "\r\n" + strPreMessage;
					}
					else
					{
						lblHeader.Text = "******  " + strYear + " Personal Property Tax - Reminder Notice  ******";
						lblPreMessage.Text = "This is to remind you that you have past due amounts on your " + strYear + " Personal Property tax bill and that the " + strPayment + "payment is due as of " + Strings.Format(dtDate, "MM/dd/yyyy") + "." + "\r\n" + strPreMessage;
					}
					lblMessage.Text = strPastDue + "\r\n";
					lblMessage.Text = lblMessage.Text + "                    " + modGlobalFunctions.PadStringWithSpaces("Past Due", 10, false) + modGlobalFunctions.PadStringWithSpaces("$" + Strings.Format((dblDue - dblPaid) - dblPeriodDue, "#,##0.00"), 15) + "\r\n";
					if (boolShowPendingAmount)
					{
						lblMessage.Text = lblMessage.Text + "                    " + modGlobalFunctions.PadStringWithSpaces("Interest", 10, false) + modGlobalFunctions.PadStringWithSpaces("$" + Strings.Format(dblInterest, "#,##0.00"), 15) + "\r\n";
					}
					else
					{
						lblMessage.Text = lblMessage.Text + "                    " + modGlobalFunctions.PadStringWithSpaces("Interest", 10, false) + modGlobalFunctions.PadStringWithSpaces("Call for Total.", 15) + "\r\n";
					}
					lblMessage.Text = lblMessage.Text + "                    " + modGlobalFunctions.PadStringWithSpaces("Due", 10, false) + modGlobalFunctions.PadStringWithSpaces("$" + Strings.Format(dblPeriodDue, "#,##0.00"), 15) + "\r\n";
					lblMessage.Text = lblMessage.Text + "                    " + modGlobalFunctions.PadStringWithSpaces(" ", 10, false) + modGlobalFunctions.PadStringWithSpaces("------------", 15) + "\r\n";
					if (boolShowPendingAmount)
					{
						lblMessage.Text = lblMessage.Text + "                    " + modGlobalFunctions.PadStringWithSpaces("Total Due", 10, false) + modGlobalFunctions.PadStringWithSpaces("$" + Strings.Format(dblDue - dblPaid + dblInterest, "#,##0.00"), 15) + "\r\n";
					}
					else
					{
						lblMessage.Text = lblMessage.Text + "                    " + modGlobalFunctions.PadStringWithSpaces("Total Due", 10, false) + modGlobalFunctions.PadStringWithSpaces("Call for Total.", 15) + "\r\n";
					}
					lblAmount.Text = "";
				}
				Array.Resize(ref modReminderNoticeSummary.Statics.arrReminderSummaryList, lngIndex + 1);
				//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
				modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex] = new modReminderNoticeSummary.ReminderSummaryList(0);
				// fill the struct
				modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Account = lngAcct;
				modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Name1 = str1;
				modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr1 = str2;
				modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr2 = str3;
				modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr3 = str4;
				modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr4 = str5;
				modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Year = FCConvert.ToInt32(strYear);
				modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Total = dblDue - dblPaid;
				lngIndex += 1;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Printing Mailer");
			}
		}

		
	}
}
