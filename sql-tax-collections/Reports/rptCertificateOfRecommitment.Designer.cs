﻿namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	partial class rptCertificateOfRecommitment
	{
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptCertificateOfRecommitment));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.fldMain2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCounty = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblDated1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMunicipalOfficers = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lnMO = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblReportVersion = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldMain1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.lblTitleBar = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblLegalDescription = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.fldMain2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCounty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDated1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMunicipalOfficers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReportVersion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMain1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitleBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLegalDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldMain2,
            this.fldCounty,
            this.fldState,
            this.lblDated1,
            this.lblMunicipalOfficers,
				this.lnMO,
				this.Line1,
				this.Line2,
				this.Line3,
				this.Line4,
				this.lblReportVersion,
				this.fldMain1
			});
			this.Detail.Height = 7.302083F;
			this.Detail.Name = "Detail";
            // 
            // fldMain2
            // 
            this.fldMain2.Height = 3.5625F;
            this.fldMain2.Left = 0F;
            this.fldMain2.Name = "fldMain2";
            this.fldMain2.Style = "font-family: \'Tahoma\'; font-size: 12pt";
            this.fldMain2.Text = null;
            this.fldMain2.Top = 1.25F;
            this.fldMain2.Width = 7F;
            // 
            // fldCounty
            // 
            this.fldCounty.CanGrow = false;
            this.fldCounty.Height = 0.25F;
            this.fldCounty.Left = 0F;
            this.fldCounty.Name = "fldCounty";
            this.fldCounty.Style = "font-family: \'Tahoma\'; font-size: 12pt";
            this.fldCounty.Text = null;
            this.fldCounty.Top = 0.125F;
            this.fldCounty.Width = 4F;
            // 
            // fldState
            // 
            this.fldState.CanGrow = false;
            this.fldState.Height = 0.25F;
            this.fldState.Left = 4.125F;
            this.fldState.Name = "fldState";
            this.fldState.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: right";
            this.fldState.Text = null;
            this.fldState.Top = 0.125F;
            this.fldState.Width = 2.875F;
            // 
            // lblDated1
            // 
            this.lblDated1.Height = 0.25F;
            this.lblDated1.HyperLink = null;
            this.lblDated1.Left = 0F;
            this.lblDated1.Name = "lblDated1";
            this.lblDated1.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: left";
            this.lblDated1.Text = null;
            this.lblDated1.Top = 5F;
            this.lblDated1.Width = 7F;
            // 
            // lblMunicipalOfficers
            // 
            this.lblMunicipalOfficers.Height = 0.25F;
            this.lblMunicipalOfficers.HyperLink = null;
            this.lblMunicipalOfficers.Left = 0.25F;
            this.lblMunicipalOfficers.Name = "lblMunicipalOfficers";
            this.lblMunicipalOfficers.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: center";
            this.lblMunicipalOfficers.Text = null;
            this.lblMunicipalOfficers.Top = 5.25F;
            this.lblMunicipalOfficers.Width = 3.75F;
            // 
            // lnMO
            // 
            this.lnMO.Height = 0F;
            this.lnMO.Left = 0.25F;
            this.lnMO.LineWeight = 1F;
            this.lnMO.Name = "lnMO";
            this.lnMO.Top = 5.6875F;
            this.lnMO.Width = 3.75F;
            this.lnMO.X1 = 0.25F;
            this.lnMO.X2 = 4F;
            this.lnMO.Y1 = 5.6875F;
            this.lnMO.Y2 = 5.6875F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0.25F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 5.9375F;
            this.Line1.Width = 3.75F;
            this.Line1.X1 = 0.25F;
            this.Line1.X2 = 4F;
            this.Line1.Y1 = 5.9375F;
            this.Line1.Y2 = 5.9375F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0.25F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 6.1875F;
            this.Line2.Width = 3.75F;
            this.Line2.X1 = 0.25F;
            this.Line2.X2 = 4F;
            this.Line2.Y1 = 6.1875F;
            this.Line2.Y2 = 6.1875F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 0.25F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 6.4375F;
            this.Line3.Width = 3.75F;
            this.Line3.X1 = 0.25F;
            this.Line3.X2 = 4F;
            this.Line3.Y1 = 6.4375F;
            this.Line3.Y2 = 6.4375F;
            // 
            // Line4
            // 
            this.Line4.Height = 0F;
            this.Line4.Left = 0.25F;
            this.Line4.LineWeight = 1F;
            this.Line4.Name = "Line4";
            this.Line4.Top = 6.6875F;
            this.Line4.Width = 3.75F;
            this.Line4.X1 = 0.25F;
            this.Line4.X2 = 4F;
            this.Line4.Y1 = 6.6875F;
            this.Line4.Y2 = 6.6875F;
            // 
            // lblReportVersion
            // 
            this.lblReportVersion.Height = 0.1875F;
            this.lblReportVersion.HyperLink = null;
            this.lblReportVersion.Left = 0.25F;
            this.lblReportVersion.Name = "lblReportVersion";
            this.lblReportVersion.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
            this.lblReportVersion.Text = null;
            this.lblReportVersion.Top = 6.8125F;
            this.lblReportVersion.Width = 1.875F;
            // 
            // fldMain1
            // 
            this.fldMain1.Height = 0.5625F;
            this.fldMain1.Left = 0F;
            this.fldMain1.Name = "fldMain1";
            this.fldMain1.Style = "font-family: \'Tahoma\'; font-size: 12pt";
            this.fldMain1.Text = null;
            this.fldMain1.Top = 0.4375F;
            this.fldMain1.Width = 7F;
            // 
            // PageHeader
            // 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblTitleBar,
				this.lblLegalDescription
			});
			this.PageHeader.Height = 0.59375F;
			this.PageHeader.Name = "PageHeader";
			// 
			// lblTitleBar
            // 
            this.lblTitleBar.Height = 0.3F;
            this.lblTitleBar.HyperLink = null;
            this.lblTitleBar.Left = 0F;
            this.lblTitleBar.Name = "lblTitleBar";
            this.lblTitleBar.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
            this.lblTitleBar.Text = "Certificate Of Recommitment";
            this.lblTitleBar.Top = 0F;
            this.lblTitleBar.Width = 7F;
            // 
            // lblLegalDescription
            // 
            this.lblLegalDescription.Height = 0.3F;
            this.lblLegalDescription.HyperLink = null;
            this.lblLegalDescription.Left = 0F;
            this.lblLegalDescription.Name = "lblLegalDescription";
            this.lblLegalDescription.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: center";
            this.lblLegalDescription.Text = null;
            this.lblLegalDescription.Top = 0.3F;
            this.lblLegalDescription.Width = 7F;
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // rptCertificateOfRecommitment
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.010417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.fldMain2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCounty)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDated1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMunicipalOfficers)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportVersion)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMain1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitleBar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLegalDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMain2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCounty;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldState;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDated1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMunicipalOfficers;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnMO;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReportVersion;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMain1;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitleBar;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLegalDescription;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
