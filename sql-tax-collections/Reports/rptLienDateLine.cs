﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptLienDateLine : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               03/31/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               06/18/2004              *
		// ********************************************************
		bool boolDone;
		// VBto upgrade warning: strRK As Variant --> As string()
		string[] strRK = null;
		string strRKList;
		int intNumberOfRK;
		int intindex;

		public rptLienDateLine()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Lien Date Line";
		}

		public static rptLienDateLine InstancePtr
		{
			get
			{
				return (rptLienDateLine)Sys.GetInstance(typeof(rptLienDateLine));
			}
		}

		protected rptLienDateLine _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public void Init(string strPassList)
		{
			strRKList = strPassList;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = boolDone;
			//Detail_Format();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "t");
			lblHeader.Text = frmLienDates.InstancePtr.strReportHeader;
			lblMuni.Text = modGlobalConstants.Statics.MuniName;
			// here I will find out how many and which rate records to print
			// this is a comma delimited string and I will break it down into an array
			strRK = Strings.Split(strRKList, ",", -1, CompareConstants.vbTextCompare);
			intNumberOfRK = Information.UBound(strRK, 1);
			if (intNumberOfRK >= 0)
			{
				boolDone = false;
			}
			else
			{
				boolDone = true;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (intindex > intNumberOfRK)
			{
				boolDone = true;
				sarLienDateDetailOb.Report = null;
			}
			else
			{
				boolDone = false;
				sarLienDateDetailOb.Report = new sarLienDateDetail();
				sarLienDateDetailOb.Report.UserData = strRK[intindex];
				intindex += 1;
			}
		}

		
	}
}
