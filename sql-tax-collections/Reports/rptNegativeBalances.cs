using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using GrapeCity.ActiveReports.SectionReportModel;
using System.Diagnostics;
using Global;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.Enums;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptNegativeBalances : BaseSectionReport
	{
        private TaxCollectionStatusReportConfiguration reportConfiguration;
        private rptOutstandingBalancesAll parentReport;
        string strSQL;
		clsDRWrapper rsData = new clsDRWrapper();
		double[] dblTotals = new double[6 + 1];
		bool boolRTError;
		int lngCount;
		bool boolStarted;
		double dblPDTotal;
		bool boolAdjustedSummary;
		bool boolSummaryOnly;
		bool boolDeletedAccount;
		int lngTAType;
		// 0 = all, 1 = TA, 2 = non TA
		clsDRWrapper rsMaster = new clsDRWrapper();
		Dictionary<object, object> dctAccounts = new Dictionary<object, object>();
		// kk01132014 trocls-13
		// this is for the summaries at the bottom
		double[] dblYearTotals = new double[2000 + 1];
		// billingyear - 19800
		int[] lngArrBillCounts = new int[2000 + 1];
		// kk01132015 trocls-13
		// kk04082014 trocl-682  Dim dblPayments(10, 5)          As Double   '0 - 3, 1 - A, 2 - C, 3 - D, 4 - I, 5 - L, 6 - P, 7 - U, 8 - X, 9 - Y, 10 - Total
		// 0 - Principal, 1 - PreLienInterest, 2 - CurrentInterest, 3 - Costs
		clsPmtTypeSummaries tPayTypeSummaries = new clsPmtTypeSummaries();
		GrapeCity.ActiveReports.SectionReportModel.TextBox fldSummaryTotal = null;
		GrapeCity.ActiveReports.SectionReportModel.TextBox fldSumCountTotal = null;
		GrapeCity.ActiveReports.SectionReportModel.Label lblPerDiemTotal = null;
		GrapeCity.ActiveReports.SectionReportModel.Line lnFooterSummaryTotal = null;

		public rptNegativeBalances()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        public void SetReportOptionAndParent(TaxCollectionStatusReportConfiguration reportConfig, rptOutstandingBalancesAll parent)
        {
            reportConfiguration = reportConfig;
            parentReport = parent;
        }
        private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Negative Balance Report";
		}

		public static rptNegativeBalances InstancePtr
		{
			get
			{
				return (rptNegativeBalances)Sys.GetInstance(typeof(rptNegativeBalances));
			}
		}

		protected rptNegativeBalances _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
				rsMaster.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsData.EndOfFile())
			{
				if (boolStarted)
				{
					eArgs.EOF = true;
				}
				else
				{
				}
			}
			else
			{
				eArgs.EOF = false;
			}
			//Detail_Format();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_PageEnd(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			tPayTypeSummaries = null;
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "t");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			lngCount = 0;
			
			boolSummaryOnly = FCConvert.CBool(reportConfiguration.Options.ShowSummaryOnly);
			// MAL@20080813 ; Tracker Reference: 11805
            if (reportConfiguration.Filter.IsTaxAcquired.HasValue)
            {
                if (reportConfiguration.Filter.IsTaxAcquired.Value)
                {
                    lngTAType = 1;
                }
                else
                {
                    lngTAType = 2;
                }
            }
            else
            {
                lngTAType = 0;
            }

			frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading");
			boolRTError = false;
			SetupFields();
			SetReportHeader();
			boolStarted = false;
			strSQL = BuildSQL();
			rsData.OpenRecordset(strSQL, modGlobal.DEFAULTDATABASE);
			if (!rsData.EndOfFile())
			{
				frmWait.InstancePtr.Unload();
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading", true, rsData.RecordCount());
			}
			//FC:BCU - controls can only be created on ReportStart
			//
			//create summary rows
			for (int i = 2; i < 22; i++)
			{
				AddSummaryRow(i, 0, 0, 0, true);
			}
			//create summary total
			GrapeCity.ActiveReports.SectionReportModel.TextBox fldSummaryTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			GrapeCity.ActiveReports.SectionReportModel.TextBox fldSumCountTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			GrapeCity.ActiveReports.SectionReportModel.Label lblPerDiemTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			GrapeCity.ActiveReports.SectionReportModel.Line lnFooterSummaryTotal = new GrapeCity.ActiveReports.SectionReportModel.Line();
			fldSummaryTotal.Name = "fldSummaryTotal";
			fldSumCountTotal.Name = "fldSumCountTotal";
			lblPerDiemTotal.Name = "lblPerDiemTotal";
			lnFooterSummaryTotal.Name = "lnFooterSummaryTotal";
			ReportFooter.Controls.Add(fldSummaryTotal);
			ReportFooter.Controls.Add(fldSumCountTotal);
			ReportFooter.Controls.Add(lblPerDiemTotal);
			ReportFooter.Controls.Add(lnFooterSummaryTotal);
			//
		}

		private string BuildSQL()
		{
			string BuildSQL = "";
			string strTemp = "";
			string strWhereClause = "";
			string strREPPBill = "";
			string strREPPPayment = "";
			int intCT;
			string strSupp = "";
			if (reportConfiguration.BillingType == PropertyTaxBillType.Real)
			{
				// only get the accounts for the correct module
				strREPPBill = "'RE'";
			}
			else
			{
				strREPPBill = "'PP'";
			}
			if (reportConfiguration.Options.DefaultReportTypeIsSupplemental())
			{
				strSupp = " AND BillingYear % 10 > 1 ";
			}
			// MAL@20080624: Add check for As Of date and deal with the Lien Record Number later
			// Tracker Reference: 14306
			if (reportConfiguration.Options.UseAsOfDate())
			{
				strWhereClause = "WHERE BillingType = " + strREPPBill + strSupp;
			}
			else
			{
				strWhereClause = "WHERE LienRecordNumber = 0 AND BillingType = " + strREPPBill + strSupp;
			}

            var filter = reportConfiguration.Filter;
            var strAnd = "";

            if (filter.AccountRangeUsed())
            {
                if (filter.AccountMax.HasValue)
                {
                    if (filter.AccountMin.HasValue)
                    {
                        strTemp += "Account between " + filter.AccountMin.Value.ToString() + " and " +
                                   filter.AccountMax.Value.ToString();
                    }
                    else
                    {
                        strTemp += "Account <= " + filter.AccountMax.Value.ToString();
                    }
                }
                else
                {
                    strTemp += "Account >= " + filter.AccountMin.Value.ToString();
                }

                strAnd = " and ";
            }

            if (filter.NameRangeUsed())
            {
                strTemp += strAnd;

                if (!String.IsNullOrWhiteSpace(filter.NameMin))
                {
                    if (!String.IsNullOrWhiteSpace(filter.NameMax))
                    {
                        strTemp += "Name1 between '" + filter.NameMin + "' and '" + filter.NameMax + "zzzz'";
                    }
                    else
                    {
                        strTemp += "Name1 >= '" + filter.NameMin + "'";
                    }
                }
                else
                {
                    strTemp += "Name1 <= '" + filter.NameMax + "zzzz'";
                }

                strAnd = " and ";
            }

            if (filter.TaxYearRangeUsed())
            {
                strTemp += strAnd;

                if (filter.TaxYearMin.HasValue)
                {
                    if (filter.TaxYearMax.HasValue)
                    {
                        strTemp += "BillingYear between " + filter.TaxYearMin.Value.ToString() + " and " +
                                   filter.TaxYearMax.Value.ToString();
                    }
                    else
                    {
                        strTemp += "BillingYear = " + filter.TaxYearMin.Value.ToString();
                    }
                }
                else
                {
                    strTemp += "BillingYear = " + filter.TaxYearMax.Value.ToString();
                }

                strAnd = " and ";
            }

            if (filter.RateRecordRangeUsed())
            {
                strTemp += strAnd;

                if (filter.RateRecordMin.HasValue)
                {
                    if (filter.RateRecordMax.HasValue)
                    {
                        strTemp += "RateKey between " + filter.RateRecordMin.Value.ToString() + " and " +
                                   filter.RateRecordMax.Value.ToString();
                    }
                    else
                    {
                        strTemp += "RateKey >= " + filter.RateRecordMin.Value.ToString();
                    }
                }
                else
                {
                    strTemp += "RateKey >= " + filter.RateRecordMin.Value.ToString();
                }

                strAnd = " and ";
            }

			if (Strings.Trim(strTemp) != "")
			{
				strWhereClause += " AND " + strTemp;
			}

			strTemp = "select ID, RateKey, BillingType, Account, LienRecordNumber, BillingYear, Name1, TaxDue1, TaxDue2, TaxDue3, TaxDue4, transferfrombillingDATEFIRST " + "from billingmaster  " + strWhereClause + " order by name1,account,billingyear";
			BuildSQL = strTemp;
			return BuildSQL;
		}

		private void SetupFields()
		{
			// Set each field and label's visible property to True/False depending on which fields the user
			// has selected from the Custom Report screen then move them accordingly
			int intRow;
			int intRow2/*unused?*/;
			float lngHt;
			if (boolSummaryOnly)
			{
				lblYear.Visible = false;
				fldYear.Visible = false;
				fldName.Visible = false;
				fldAccount.Visible = false;
				fldPaymentReceived.Visible = false;
				fldTaxDue.Visible = false;
				fldDue.Visible = false;
				lnHeader.Visible = false;
				lnTotals.Visible = false;
				fldType.Visible = false;
				Detail.Height = 0;
				return;
			}
			intRow = 2;
			lngHt = 270 / 1440F;
			lblYear.Visible = true;
			fldYear.Visible = true;
			// if the year is not shown, then make the name field smaller
			fldName.Width = fldYear.Left - (fldType.Left + fldType.Width);
		}

		private void BindFields()
		{
			var rsPayment = new clsDRWrapper();
            clsDRWrapper rsCalLien = null;
			var rsRate = new clsDRWrapper();
			var rsLien = new clsDRWrapper();
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will fill the information into the fields

                string strTemp = "";
                double dblTotalPayment /*unused?*/;
                double dblTotalAbate /*unused?*/;
                double dblTotalRefundAbate /*unused?*/;
                double dblCurInt /*unused?*/;
                double dblLienCurInt /*unused?*/;
                DateTime dtPaymentDate;
                bool boolREInfo /*unused?*/;
                double dblPaymentRecieved;
                double dblXtraInt = 0;
                double dblTotalDue = 0;
                TRYAGAIN: ;
                frmWait.InstancePtr.IncrementProgress();
                dblPaymentRecieved = 0;
                fldDue.Text = "";
                fldTaxDue.Text = "";
                fldPaymentReceived.Text = "";
                fldAccount.Text = "";
                fldYear.Text = "";
                fldName.Text = "";
                fldType.Text = "";
                if (rsData.EndOfFile())
                {
                    return;
                }

                if (reportConfiguration.Filter.TranCodeMax.HasValue)
                {
                    if (!modCLCalculations.CheckTranCode_78(reportConfiguration.BillingType == PropertyTaxBillType.Real,
                        FCConvert.ToInt32(rsData.Get_Fields("Account")), reportConfiguration.Filter.TranCodeMin ?? 0,
                        reportConfiguration.Filter.TranCodeMax ?? 0))
                    {
                        rsData.MoveNext();
                        goto TRYAGAIN;
                    }
                }

                // MAL@20080624 ; Tracker Reference: 14306
                if (FCConvert.ToInt32(rsData.Get_Fields_Int32("LienRecordNumber")) > 0)
                {
                    // Check for bills using the As of Date
                    rsLien.OpenRecordset(
                        "SELECT ID, DateCreated FROM LienRec WHERE ID = " +
                        FCConvert.ToString(rsData.Get_Fields_Int32("LienRecordNumber")), modExtraModules.strCLDatabase);
                    if (rsLien.RecordCount() > 0)
                    {
                        // Does Lien Creation come before As Of Date?
                        if (Conversion.Val(rsLien.Get_Fields_DateTime("DateCreated")) <=
                            reportConfiguration.Options.AsOfDate.ToOADate() || rsLien.IsFieldNull("DateCreated") ||
                            Conversion.Val(rsLien.Get_Fields_DateTime("DateCreated")) == 0)
                        {
                            rsData.MoveNext();
                            goto TRYAGAIN;
                        }
                        else
                        {
                            // Continue
                        }
                    }
                }

                // MAL@20080813: Add check for Tax Acquired status
                // Tracker Reference: 11805
                if (lngTAType > 0)
                {
                    // kk trocls-3  Try to speed up  rsMaster.FindFirstRecord "RSAccount", rsData.Fields("Account")
                    // If Not rsMaster.NoMatch Then
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    rsMaster.OpenRecordset(
                        "SELECT RSAccount, TaxAcquired FROM Master WHERE RSAccount = " +
                        FCConvert.ToString(rsData.Get_Fields("Account")), modExtraModules.strREDatabase);
                    if (!rsMaster.EndOfFile())
                    {
                        switch (lngTAType)
                        {
                            case 1:
                            {
                                // TA
                                if (!FCConvert.ToBoolean(rsMaster.Get_Fields_Boolean("TaxAcquired")))
                                {
                                    rsData.MoveNext();
                                    goto TRYAGAIN;
                                }

                                break;
                            }
                            case 2:
                            {
                                // Non TA
                                if (FCConvert.ToBoolean(rsMaster.Get_Fields_Boolean("TaxAcquired")))
                                {
                                    rsData.MoveNext();
                                    goto TRYAGAIN;
                                }

                                break;
                            }
                        }

                        //end switch
                    }
                }

                lngCount += 1;
                //FC:FINAL:BCU - #i181 - add convert to string
                //fldAccount.Text = rsData.Get_Fields("Account");
                //fldYear.Text = FCUtils.iDiv(FCConvert.ToInt32(rsData.Get_Fields("BillingYear")), 10);
                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                fldAccount.Text = FCConvert.ToString(rsData.Get_Fields("Account"));
                fldYear.Text =
                    FCConvert.ToString(FCUtils.iDiv(FCConvert.ToInt32(rsData.Get_Fields_Int32("BillingYear")), 10));
                //
                fldName.Text = rsData.Get_Fields_String("Name1");
                // GetStatusName(rsData)          '( rsData.Fields("Name1")
                fldTaxDue.Text = Strings.Format(
                    Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1")) +
                    Conversion.Val(rsData.Get_Fields_Decimal("TaxDue2")) +
                    Conversion.Val(rsData.Get_Fields_Decimal("TaxDue3")) +
                    Conversion.Val(rsData.Get_Fields_Decimal("TaxDue4")), "#,##0.00");
                //Application.DoEvents();
                if (reportConfiguration.Options.UseAsOfDate())
                {
                    if (!rsData.IsFieldNull("transferfrombillingDATEFIRST") &&
                        Conversion.Val(rsData.Get_Fields_DateTime("transferfrombillingDATEFIRST")) != 0)
                    {
                        if (DateAndTime.DateDiff("d",
                            (DateTime) rsData.Get_Fields_DateTime("transferfrombillingDATEFIRST"),
                            reportConfiguration.Options.AsOfDate) < 0)
                        {
                            fldTaxDue.Text = "0.00";
                        }
                    }

                    rsPayment.OpenRecordset(
                        "SELECT Code, Principal, CurrentInterest, PreLienInterest, LienCost FROM PaymentRec WHERE BillCode <> 'L' AND BillKey = " +
                        FCConvert.ToString(rsData.Get_Fields_Int32("ID")) +
                        " AND ReceiptNumber >= 0 AND ISNULL(RecordedTransactionDate, '12/30/1899') <= '" +
                        FCConvert.ToString(reportConfiguration.Options.AsOfDate) + "'");
                }
                else
                {
                    // kk 101912    rsPayment.OpenRecordset "SELECT * FROM PaymentRec WHERE Account = " & rsData.Fields("Account") & " AND [Year] = " & rsData.Fields("BillingYear") & " AND BillCode <> 'L' AND BillKey = " & rsData.Fields("ID") & " AND ReceiptNumber >= 0"
                    // kk trocls-3  rsPayment.OpenRecordset "SELECT Code, Principal, CurrentInterest, PreLienInterest, LienCost FROM PaymentRec WHERE Account = " & rsData.Fields("Account") & " AND [Year] = " & rsData.Fields("BillingYear") & " AND BillCode <> 'L' AND BillKey = " & rsData.Fields("ID") & " AND ReceiptNumber >= 0"
                    rsPayment.OpenRecordset(
                        "SELECT Code, Principal, CurrentInterest, PreLienInterest, LienCost FROM PaymentRec WHERE BillCode <> 'L' AND BillKey = " +
                        FCConvert.ToString(rsData.Get_Fields_Int32("ID")) + " AND ReceiptNumber >= 0");
                }

                // kk04082014 trocl-682
                while (!rsPayment.EndOfFile())
                {
                    if (reportConfiguration.Options.FullStatusAmounts)
                    {
                        // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                        dblPaymentRecieved += Conversion.Val(rsPayment.Get_Fields("Principal")) +
                                              Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")) +
                                              Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")) +
                                              Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost"));
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                        tPayTypeSummaries.AddPaymentByType(
                            Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))),
                            Conversion.Val(rsPayment.Get_Fields("Principal")),
                            Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                            Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                            Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
                    }
                    else
                    {
                        // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                        dblPaymentRecieved += Conversion.Val(rsPayment.Get_Fields("Principal")) +
                                              Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")) +
                                              Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost"));
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                        tPayTypeSummaries.AddPaymentByType(
                            Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))),
                            Conversion.Val(rsPayment.Get_Fields("Principal")), 0,
                            Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                            Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
                    }

                    rsPayment.MoveNext();
                }

                if (reportConfiguration.Options.ShowCurrentInterest)
                {
                    double temp = 0;
                    modCLCalculations.CalcBillAsOfFromPayments_2160(FCConvert.ToInt32(rsData.Get_Fields_Int32("ID")),
                        reportConfiguration.Options.AsOfDate, ref dblTotalDue, 0, 0, 0, 0, ref temp);
                }
                else
                {
                    dblXtraInt = 0;
                }

                fldPaymentReceived.Text = Strings.Format(dblPaymentRecieved - dblXtraInt, "#,##0.00");
                fldDue.Text = Strings.Format(FCConvert.ToDouble(fldTaxDue.Text) - (dblPaymentRecieved - dblXtraInt),
                    "#,##0.00");
                if (rptOutstandingBalancesAll.InstancePtr.intSuppReportType == 1)
                {
                    rsRate.OpenRecordset(
                        "SELECT RateType FROM RateRec WHERE ID = " +
                        FCConvert.ToString(rsData.Get_Fields_Int32("RateKey")), modExtraModules.strCLDatabase);
                    if (!rsRate.EndOfFile())
                    {
                        if (FCConvert.ToString(rsRate.Get_Fields_String("RateType")) != "S")
                        {
                            // kk04082014 trocl-682     ' ReversePaymentsFromStatusArray rsPayment
                            tPayTypeSummaries.DiscardAllPayments();
                            lngCount -= 1;
                            rsData.MoveNext();
                            goto TRYAGAIN;
                        }
                    }
                    else
                    {
                        // kk04082014 trocl-682
                        tPayTypeSummaries.DiscardAllPayments();
                        lngCount -= 1;
                        rsData.MoveNext();
                        // kk04082014  Uncomment these lines - the rate record is missing
                        goto TRYAGAIN;
                    }
                }

                if (FCConvert.ToDouble(fldDue.Text) >= 0)
                {
                    // double check
                    // if this account is not zero balanced, then it must not be used
                    // kk04082014 trocl-682     ' ReversePaymentsFromStatusArray rsPayment
                    tPayTypeSummaries.DiscardAllPayments();
                    rsData.MoveNext();
                    lngCount -= 1;
                    goto TRYAGAIN;
                }
                else
                {
                    tPayTypeSummaries.SaveAllPayments();
                }

                if (FCConvert.ToString(rsData.Get_Fields_String("BillingType")) == "RE")
                {
                    fldType.Text = "R";
                }
                else
                {
                    fldType.Text = "P";
                }

                parentReport.dblTotalsPrin += FCConvert.ToDouble(fldTaxDue.Text);
                parentReport.dblTotalsPay += dblPaymentRecieved - dblXtraInt;
                parentReport.lngCount += 1;
                dblTotals[0] += FCConvert.ToDouble(fldTaxDue.Text);
                dblTotals[1] += dblPaymentRecieved - dblXtraInt;
                dblTotals[3] += FCConvert.ToDouble(fldDue.Text);
                // keep track for the year totals
                dblYearTotals[rsData.Get_Fields_Int32("BillingYear") - 19800] =
                    dblYearTotals[FCConvert.ToInt16(rsData.Get_Fields_Int32("BillingYear")) - 19800] +
                    FCConvert.ToDouble(fldDue.Text);
                // kk01072015 trocls-13  Keep count of accounts and Bills by Year
                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                if (!dctAccounts.ContainsKey(FCConvert.ToInt32(Conversion.Val(rsData.Get_Fields("Account")))))
                {
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    dctAccounts.Add(FCConvert.ToInt32(Conversion.Val(rsData.Get_Fields("Account"))),
                        rsData.Get_Fields_String("Name1"));
                }
                else
                {
                    // Count duplicats account number with different name??
                }

                lngArrBillCounts[rsData.Get_Fields_Int32("BillingYear") - 19800] =
                    lngArrBillCounts[FCConvert.ToInt16(rsData.Get_Fields_Int32("BillingYear")) - 19800] + 1;
                // move to the next record in the query
                rsData.MoveNext();
                return;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                frmWait.InstancePtr.Unload();
                FCMessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message +
                    ".", MsgBoxStyle.Critical, "Error In BindFields");
            }
            finally
            {
				rsLien.Dispose();
				rsRate.Dispose();
				rsCalLien?.Dispose();
				rsPayment.Dispose();
            }
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
			Debug.WriteLine(DateTime.Now);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void SetupTotals()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this sub will fill in the totals line at the bottom of the report
				//clsDRWrapper rsSum = new clsDRWrapper();
				string strSUM = "";
				int intSumRows;
				GrapeCity.ActiveReports.SectionReportModel.TextBox obNew;
				int intCT;
				fldTotalTaxDue.Text = Strings.Format(dblTotals[0], "#,##0.00");
				fldTotalPaymentReceived.Text = Strings.Format(dblTotals[1], "#,##0.00");
				fldTotalDue.Text = Strings.Format(dblTotals[3], "#,##0.00");
				// kk01132015 trocls-13  Add count of accounts
				if (dctAccounts.Count > 1)
				{
					fldAcctCount.Text = FCConvert.ToString(dctAccounts.Count) + " Accounts";
				}
				else if (dctAccounts.Count == 1)
				{
					fldAcctCount.Text = FCConvert.ToString(dctAccounts.Count) + " Account";
				}
				else
				{
					fldAcctCount.Text = "";
				}
				if (lngCount > 1)
				{
					lblTotals.Text = "Total for " + FCConvert.ToString(lngCount) + " Bills:";
					// kk01092015 trocls-13  This is count of bills, not accounts   '& " Accounts:"
				}
				else if (lngCount == 1)
				{
					lblTotals.Text = "Total for " + FCConvert.ToString(lngCount) + " Bill:";
					// & " Account:"
				}
				else
				{
					lblTotals.Text = "No Non Lien Bills";
				}
				// this will setup the payment summary
				SetupTotalSummary();
				// Load Summary List
				intSumRows = 1;
				for (intCT = 0; intCT <= Information.UBound(dblYearTotals, 1) - 1; intCT++)
				{
					if (dblYearTotals[intCT] != 0)
					{
						AddSummaryRow(intSumRows, dblYearTotals[intCT], lngArrBillCounts[intCT], intCT + 19800, false);
						intSumRows += 1;
					}
				}
				
				obNew = ReportFooter.Controls["fldSummaryTotal"] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
				//
				obNew.Top = fldSummary1.Top + ((intSumRows - 1) * fldSummary1.Height);
				obNew.Left = fldSummary1.Left;
				obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				obNew.Width = fldSummary1.Width;
				obNew.Font = lblSummary1.Font;
				obNew.Text = Strings.Format(dblPDTotal, "#,##0.00");
				//FC:BCU
				//ReportFooter.Controls.Add(obNew);
				// kk01132015 trocls-13   add a field for the total count
				//FC:BCU
				//obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
				//obNew.Name = "fldSumCountTotal";
				obNew = ReportFooter.Controls["fldSumCountTotal"] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
				//
				obNew.Top = fldSumCount1.Top + ((intSumRows - 1) * fldSumCount1.Height);
				obNew.Left = fldSumCount1.Left;
				obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				obNew.Width = fldSumCount1.Width;
				obNew.Font = fldSumCount1.Font;
				obNew.Text = lngCount.ToString();
				//FC:BCU
				//ReportFooter.Controls.Add(obNew);
				// add a label
				//FC:BCU
				//GrapeCity.ActiveReports.SectionReportModel.Label obLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
				//obLabel.Name = "lblPerDiemTotal";
				GrapeCity.ActiveReports.SectionReportModel.Label obLabel = ReportFooter.Controls["lblPerDiemTotal"] as GrapeCity.ActiveReports.SectionReportModel.Label;
				//
				obLabel.Top = lblSummary1.Top + ((intSumRows - 1) * lblSummary1.Height);
				obLabel.Left = lblSummary1.Left;
				obLabel.Font = lblSummary1.Font;
				obLabel.Text = "Total";
				//FC:BCU
				//ReportFooter.Controls.Add(obLabel);
				// add a line
				//FC:BCU
				//GrapeCity.ActiveReports.SectionReportModel.Line obLine = new GrapeCity.ActiveReports.SectionReportModel.Line();
				//obLine.Name = "lnFooterSummaryTotal";
				GrapeCity.ActiveReports.SectionReportModel.Line obLine = ReportFooter.Controls["lnFooterSummaryTotal"] as GrapeCity.ActiveReports.SectionReportModel.Line;
				//
				obLine.X1 = fldSumCount1.Left;
				// kk01092015 trocls-13     fldSummary1.Left
				obLine.X2 = fldSummary1.Left + fldSummary1.Width;
				obLine.Y1 = lblSummary1.Top + ((intSumRows - 1) * lblSummary1.Height);
				obLine.Y2 = obLine.Y1;
				//FC:BCU
				//ReportFooter.Controls.Add(obLine);
				ReportFooter.Height = lblSummary1.Top + (intSumRows * lblSummary1.Height);
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Summary Creation");
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			SetupTotals();
		}

		private void SetReportHeader()
		{
			int intCT;
			string strTemp = "";
            string strSeparator = "";
            var filter = reportConfiguration.Filter;
            if (filter.AccountRangeUsed())
            {
                if (filter.AccountMax.HasValue)
                {
                    if (filter.AccountMin.HasValue)
                    {
                        if (filter.AccountMin == filter.AccountMax)
                        {
                            strTemp += "Account: " + filter.AccountMin.Value.ToString();
                        }
                        else
                        {
                            strTemp += "Account: " + filter.AccountMin.Value.ToString() + " To " +
                                       filter.AccountMax.Value.ToString();
                        }
                    }
                    else
                    {
                        strTemp += "Below Account: " + filter.AccountMax.Value.ToString();
                    }
                }
                else
                {
                    strTemp += "Above Account: " + filter.AccountMin.Value.ToString();
                }
                strSeparator = ", ";
            }

            if (filter.TaxYearRangeUsed())
            {
                strTemp += strSeparator;

                if (filter.TaxYearMin.HasValue)
                {
                    if (filter.TaxYearMax.HasValue)
                    {
                        strTemp += "Tax Year: " + filter.TaxYearMin.Value.ToString() + " To " +
                                   filter.TaxYearMax.Value.ToString();
                    }
                    else
                    {
                        strTemp += "Tax Year: " + filter.TaxYearMin.Value.ToString();
                    }
                }
                else
                {
                    strTemp += "Tax Year: " + filter.TaxYearMax.Value.ToString();
                }

                strSeparator = ", ";
            }

			if (lngTAType > 0)
			{
				if (Strings.Trim(strTemp) == "")
				{
					if (lngTAType == 1)
					{
						strTemp = "Tax Acquired";
					}
					else
					{
						strTemp = "Non-Tax Acquired";
					}
				}
				else
				{
					if (lngTAType == 1)
					{
						strTemp = ", Tax Acquired";
					}
					else
					{
						strTemp = ", Non-Tax Acquired";
					}
				}
			}
			// check to see if they used the Show Interest checkbox
			if (reportConfiguration.Options.ShowCurrentInterest)
			{
				if (Strings.Trim(strTemp) == "")
				{
					strTemp += "Show Interest";
				}
				else
				{
					strTemp += ", Show Interest";
				}
			}
			if (Strings.Trim(strTemp) == "")
			{
				lblReportType.Text = "Complete List" + "\r\n" + "Negative Balance Non Liened Accounts";
			}
			else
			{
				lblReportType.Text = strTemp + "\r\n" + "Negative Balance Non Liened Accounts";
			}
			if (reportConfiguration.Options.UseAsOfDate())
			{
				lblReportType.Text = lblReportType.Text + "\r\n" + "As of: " + Strings.Format(reportConfiguration.Options.AsOfDate, "MM/dd/yyyy");
			}
		}
		
		private void AddSummaryRow(int intRNum, double dblAmount, int lngBillCnt, int lngYear, bool createObjects)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				GrapeCity.ActiveReports.SectionReportModel.TextBox obNew/*unused?*/;
				// this will add another per diem line in the report footer
				if (intRNum == 1)
				{
					fldSummary1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
					fldSummary1.Text = Strings.Format(dblAmount, "#,##0.00");
					fldSumCount1.Text = lngBillCnt.ToString();
					// kk01092015 trocls-13
					fldSumCount1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
					lblSummary1.Text = modExtraModules.FormatYear(lngYear.ToString());
					dblPDTotal += dblAmount;
				}
				else
				{
					if (createObjects)
					{
						// add a field
						obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						obNew.Name = "fldSummary" + FCConvert.ToString(intRNum);
						ReportFooter.Controls.Add(obNew);
					}
					else
					{
						obNew = ReportFooter.Controls["fldSummary" + FCConvert.ToString(intRNum)] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
					}
					if (obNew != null)
					{
						obNew.Top = fldSummary1.Top + ((intRNum - 1) * fldSummary1.Height);
						obNew.Left = fldSummary1.Left;
						obNew.Width = fldSummary1.Width;
						obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
						obNew.Font = fldSummary1.Font;
						// this sets the font to the same as the field that is already created
						obNew.Text = Strings.Format(dblAmount, "#,##0.00");
						dblPDTotal += dblAmount;
					}
					if (createObjects)
					{
						// kk01092015 trocls-13  add a field for the number of liens
						obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						obNew.Name = "fldSumCount" + FCConvert.ToString(intRNum);
						ReportFooter.Controls.Add(obNew);
					}
					else
					{
						obNew = ReportFooter.Controls["fldSumCount" + FCConvert.ToString(intRNum)] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
					}
					if (obNew != null)
					{
						obNew.Top = fldSumCount1.Top + ((intRNum - 1) * fldSumCount1.Height);
						obNew.Left = fldSumCount1.Left;
						obNew.Width = fldSumCount1.Width;
						obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
						obNew.Font = fldSumCount1.Font;
						// this sets the font to the same as the field that is already created
						obNew.Text = lngBillCnt.ToString();
					}
					GrapeCity.ActiveReports.SectionReportModel.Label obLabel = null;
					if (createObjects)
					{
						// add a label
						obLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
						obLabel.Name = "lblSummary" + FCConvert.ToString(intRNum);
						ReportFooter.Controls.Add(obLabel);
					}
					else
					{
						obLabel = ReportFooter.Controls["lblSummary" + FCConvert.ToString(intRNum)] as GrapeCity.ActiveReports.SectionReportModel.Label;
					}
					if (obLabel != null)
					{
						obLabel.Top = lblSummary1.Top + ((intRNum - 1) * lblSummary1.Height);
						obLabel.Left = lblSummary1.Left;
						obLabel.Font = fldSummary1.Font;
						// this sets the font to the same as the field that is already created
						obLabel.Text = modExtraModules.FormatYear(lngYear.ToString());
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Adding Summary Row");
			}
		}

		private void SetupTotalSummary()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the summary labels at the bottom of the page
				// and hide/show the labels when needed
				int intCT;
				int intRow;
				// this will keep track of the row  that I am adding values to
				string strDesc = "";
				// kk04082013 trocl-682   Dim dblTotal(5)         As Double
				// fill in the titles
				lblSumHeaderType.Text = "Type";
				lblSumHeaderPrin.Text = "Principal";
				lblSumHeaderInt.Text = "Interest";
				lblSumHeaderCost.Text = "Costs";
				lblSumHeaderTotal.Text = "Total";
				intRow = 1;
				// start at the first row
				// kk04082014 trocl-682
				foreach (clsPmtTypeSummary tSum in tPayTypeSummaries)
				{
					if (!tSum.IsEmpty)
					{
						strDesc = tSum.Code + " - " + tSum.Description;
						FillSummaryLine(intRow, strDesc, tSum.Principal, tSum.PreLienInterst, tSum.Interest, tSum.LienCost, tSum.Total);
						intRow += 1;
					}
				}
				
				FillSummaryLine(intRow, "Total", tPayTypeSummaries.SummaryTotal().Principal, tPayTypeSummaries.SummaryTotal().PreLienInterst, tPayTypeSummaries.SummaryTotal().Interest, tPayTypeSummaries.SummaryTotal().LienCost, tPayTypeSummaries.SummaryTotal().Total);
				SetSummaryTotalLine(intRow);
				for (intCT = intRow + 1; intCT <= 11; intCT++)
				{
					HideSummaryRow(intCT);
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Creating Summary Table");
			}
		}
		// VBto upgrade warning: intRw As short	OnWriteFCConvert.ToInt32(
		private void SetSummaryTotalLine(int intRw)
		{
			switch (intRw)
			{
				case 1:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal1.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal1.Top;
						break;
					}
				case 2:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal2.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal2.Top;
						break;
					}
				case 3:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal3.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal3.Top;
						break;
					}
				case 4:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal4.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal4.Top;
						break;
					}
				case 5:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal5.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal5.Top;
						break;
					}
				case 6:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal6.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal6.Top;
						break;
					}
				case 7:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal7.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal7.Top;
						break;
					}
				case 8:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal8.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal8.Top;
						break;
					}
				case 9:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal9.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal9.Top;
						break;
					}
				case 10:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal10.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal10.Top;
						break;
					}
				case 11:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal11.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal11.Top;
						break;
					}
			}
			//end switch
		}
		// VBto upgrade warning: intRw As short	OnWriteFCConvert.ToInt32(
		private void FillSummaryLine(int intRw, string strDescription, double dblPrin, double dblPLI, double dblCurInt, double dblCosts, double dblTotal)
		{
			// this routine will fill in the line summary row
			switch (intRw)
			{
				case 1:
					{
						lblSummaryPaymentType1.Text = strDescription;
						lblSumPrin1.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt1.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost1.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal1.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 2:
					{
						lblSummaryPaymentType2.Text = strDescription;
						lblSumPrin2.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt2.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost2.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal2.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 3:
					{
						lblSummaryPaymentType3.Text = strDescription;
						lblSumPrin3.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt3.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost3.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal3.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 4:
					{
						lblSummaryPaymentType4.Text = strDescription;
						lblSumPrin4.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt4.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost4.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal4.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 5:
					{
						lblSummaryPaymentType5.Text = strDescription;
						lblSumPrin5.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt5.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost5.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal5.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 6:
					{
						lblSummaryPaymentType6.Text = strDescription;
						lblSumPrin6.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt6.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost6.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal6.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 7:
					{
						lblSummaryPaymentType7.Text = strDescription;
						lblSumPrin7.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt7.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost7.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal7.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 8:
					{
						lblSummaryPaymentType8.Text = strDescription;
						lblSumPrin8.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt8.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost8.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal8.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 9:
					{
						lblSummaryPaymentType9.Text = strDescription;
						lblSumPrin9.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt9.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost9.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal9.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 10:
					{
						lblSummaryPaymentType10.Text = strDescription;
						lblSumPrin10.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt10.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost10.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal10.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 11:
					{
						lblSummaryPaymentType11.Text = strDescription;
						lblSumPrin11.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt11.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost11.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal11.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
			}
			//end switch
		}
		
		private void HideSummaryRow(int intRw)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intCT;
				for (intCT = intRw; intCT <= 11; intCT++)
				{
					switch (intRw)
					{
						case 1:
							{
								lblSummaryPaymentType1.Visible = false;
								lblSumPrin1.Visible = false;
								lblSumInt1.Visible = false;
								lblSumCost1.Visible = false;
								lblSummaryTotal1.Visible = false;
								break;
							}
						case 2:
							{
								lblSummaryPaymentType2.Visible = false;
								lblSumPrin2.Visible = false;
								lblSumInt2.Visible = false;
								lblSumCost2.Visible = false;
								lblSummaryTotal2.Visible = false;
								break;
							}
						case 3:
							{
								lblSummaryPaymentType3.Visible = false;
								lblSumPrin3.Visible = false;
								lblSumInt3.Visible = false;
								lblSumCost3.Visible = false;
								lblSummaryTotal3.Visible = false;
								break;
							}
						case 4:
							{
								lblSummaryPaymentType4.Visible = false;
								lblSumPrin4.Visible = false;
								lblSumInt4.Visible = false;
								lblSumCost4.Visible = false;
								lblSummaryTotal4.Visible = false;
								break;
							}
						case 5:
							{
								lblSummaryPaymentType5.Visible = false;
								lblSumPrin5.Visible = false;
								lblSumInt5.Visible = false;
								lblSumCost5.Visible = false;
								lblSummaryTotal5.Visible = false;
								break;
							}
						case 6:
							{
								lblSummaryPaymentType6.Visible = false;
								lblSumPrin6.Visible = false;
								lblSumInt6.Visible = false;
								lblSumCost6.Visible = false;
								lblSummaryTotal6.Visible = false;
								break;
							}
						case 7:
							{
								lblSummaryPaymentType7.Visible = false;
								lblSumPrin7.Visible = false;
								lblSumInt7.Visible = false;
								lblSumCost7.Visible = false;
								lblSummaryTotal7.Visible = false;
								break;
							}
						case 8:
							{
								lblSummaryPaymentType8.Visible = false;
								lblSumPrin8.Visible = false;
								lblSumInt8.Visible = false;
								lblSumCost8.Visible = false;
								lblSummaryTotal8.Visible = false;
								break;
							}
						case 9:
							{
								lblSummaryPaymentType9.Visible = false;
								lblSumPrin9.Visible = false;
								lblSumInt9.Visible = false;
								lblSumCost9.Visible = false;
								lblSummaryTotal9.Visible = false;
								break;
							}
						case 10:
							{
								lblSummaryPaymentType10.Visible = false;
								lblSumPrin10.Visible = false;
								lblSumInt10.Visible = false;
								lblSumCost10.Visible = false;
								lblSummaryTotal10.Visible = false;
								break;
							}
						case 11:
							{
								lblSummaryPaymentType11.Visible = false;
								lblSumPrin11.Visible = false;
								lblSumInt11.Visible = false;
								lblSumCost11.Visible = false;
								lblSummaryTotal11.Visible = false;
								break;
							}
					}
					//end switch
				}
				if (!boolAdjustedSummary)
				{
					SetYearSummaryTop_2(lblSummaryPaymentType1.Top + (intRw * lblSummaryPaymentType1.Height) + 100 / 1440F);
					boolAdjustedSummary = true;
				}

				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Hiding Summary Rows");
			}
		}

		private void SetYearSummaryTop_2(float lngTop)
		{
			SetYearSummaryTop(ref lngTop);
		}

		private void SetYearSummaryTop(ref float lngTop)
		{
			// this will start the year summary at the right height
			lblSummary.Top = lngTop;
			Line1.Y1 = lngTop + lblSummary.Height;
			Line1.Y2 = lngTop + lblSummary.Height;
			lblSummary1.Top = lngTop + lblSummary.Height;
			fldSumCount1.Top = lngTop + fldSumCount1.Height;
			// kk01092015 trocls-13
			fldSummary1.Top = lngTop + lblSummary.Height;
		}
		
		
	}
}