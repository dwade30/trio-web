﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptLienRemovalPayments : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               08/18/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/07/2006              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsPay = new clsDRWrapper();
		double dblPrin;
		double dblInt;
		double dblCost;

		public rptLienRemovalPayments()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Account Detail";
		}

		public static rptLienRemovalPayments InstancePtr
		{
			get
			{
				return (rptLienRemovalPayments)Sys.GetInstance(typeof(rptLienRemovalPayments));
			}
		}

		protected rptLienRemovalPayments _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
				rsPay.Dispose();
            }
			base.Dispose(disposing);
		}

		public void Init(int lngLRN, bool modalDialog)
		{
			this.Name = "Remove From Lien";
			rsData.OpenRecordset("SELECT * FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.ID  WHERE LienRec.ID = " + FCConvert.ToString(lngLRN), modExtraModules.strCLDatabase);
			if (!rsData.EndOfFile())
			{
				rsPay.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngLRN) + " AND BillCode = 'L'", modExtraModules.strCLDatabase);
			}
			else
			{
				// end report
				FCMessageBox.Show("Cannot find lien and bill record.", MsgBoxStyle.Exclamation, "Report Cancelled");
			}
			frmReportViewer.InstancePtr.Init(this, "", 1, showModal: modalDialog);
			//Application.DoEvents();
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsPay.EndOfFile();
			//Detail_Format();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			SetHeaderString();
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
			// this will print the row from the grid
			if (!rsPay.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
				fldPrincipal.Text = Strings.Format(rsPay.Get_Fields("Principal"), "#,##0.00");
				fldInterest.Text = Strings.Format(Conversion.Val(rsPay.Get_Fields_Decimal("PreLienInterest")) + Conversion.Val(rsPay.Get_Fields_Decimal("CurrentInterest")), "#,##0.00");
				fldCosts.Text = Strings.Format(rsPay.Get_Fields_Decimal("LienCost"), "#,##0.00");
				// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
				fldTotal.Text = Strings.Format(Conversion.Val(rsPay.Get_Fields("Principal")) + Conversion.Val(rsPay.Get_Fields_Decimal("PreLienInterest")) + Conversion.Val(rsPay.Get_Fields_Decimal("CurrentInterest")) + Conversion.Val(rsPay.Get_Fields_Decimal("LienCost")), "#,##0.00");
				// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
				fldCode.Text = rsPay.Get_Fields_String("Code");
				//FC:BCU #i183 - add convert to string
				//fldDate.Text = rsPay.Get_Fields("RecordedTransactionDate");
				//fldPer.Text = rsPay.Get_Fields("Period");
				//fldRef.Text = rsPay.Get_Fields("Reference");
				fldDate.Text = FCConvert.ToString(rsPay.Get_Fields_DateTime("RecordedTransactionDate"));
				// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
				fldPer.Text = FCConvert.ToString(rsPay.Get_Fields("Period"));
				fldRef.Text = FCConvert.ToString(rsPay.Get_Fields_String("Reference"));
				//
				// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
				fldPer.Text = rsPay.Get_Fields_String("Period");
				fldRef.Text = rsPay.Get_Fields_String("Reference");
				// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
				dblPrin += Conversion.Val(rsPay.Get_Fields("Principal"));
				dblInt += Conversion.Val(rsPay.Get_Fields_Decimal("PreLienInterest")) + Conversion.Val(rsPay.Get_Fields_Decimal("CurrentInterest"));
				dblCost += Conversion.Val(rsPay.Get_Fields_Decimal("LienCost"));
				rsPay.MoveNext();
			}
		}

		private void SetHeaderString()
		{
			// this will set the correct header for this account
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			lblAccount.Text = "Account " + FCConvert.ToString(rsData.Get_Fields("Account"));
			lblName.Text = "Name: " + FCConvert.ToString(rsData.Get_Fields_String("Name1"));
			// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
			lblLocation.Text = "Location: " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("APT")) + " " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields("StreetNumber")) + " " + FCConvert.ToString(rsData.Get_Fields_String("StreetName"))));
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			// show the totals
			fldTotalPrin.Text = Strings.Format(dblPrin, "#,##0.00");
			fldTotalInt.Text = Strings.Format(dblInt, "#,##0.00");
			fldTotalCost.Text = Strings.Format(dblCost, "#,##0.00");
			fldTotalTotal.Text = Strings.Format(dblPrin + dblInt + dblCost, "#,##0.00");
		}

		
	}
}
