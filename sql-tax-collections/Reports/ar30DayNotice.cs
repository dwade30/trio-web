﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Wisej.Web;
using fecherFoundation.VisualBasicLayer;
using fecherFoundation.Extensions;
using System.IO;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class ar30DayNotice : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/09/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               06/05/2006              *
		// ********************************************************
        private cAddressControllerCL tAddrCtrlr = new cAddressControllerCL();
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsCMFNumbers = new clsDRWrapper();
		string strSQL;
		string strText = "";
		// VBto upgrade warning: intYear As short --> As int	OnRead(string)
		int intYear;
		bool boolPayCert;
		bool boolMort;
		bool boolPayMortCert;
		bool boolPayNewOwner;
        short intBilledOwnerAddr;
		clsDRWrapper rsCert;
		int lngCopies;
		// this is to keep track of the number of mortgage holder copies left
		bool boolCopy;
		// is this a copy or the original
		int intNewOwnerChoice;
		// this is if the user wants to send to any new owners 0 = Do not send any, 1 =
		clsDRWrapper rsMort = new clsDRWrapper();
		clsDRWrapper rsRE = new clsDRWrapper();
		clsDRWrapper rsRate = new clsDRWrapper();
		clsDRWrapper rsPayments = new clsDRWrapper();
		// Dim rsBook                      As New clsDRWrapper
		bool boolNewOwner;
		// this is true if there is a new owner
		bool boolAtLeastOneRecord;
		// this is true if there is at least one record to see
		string strAcctList = "";
		int lngMaxNumber;
		int lngCount;
		string strReportDesc = "";
		// this will be the string that will describe the parameters the user set for the report ie. Account 1 to 100
		bool boolREMatch;
		string strAccountNumber = "";
		bool boolRemoveLastPage;
		// VBto upgrade warning: strDateCheck As string	OnWrite(string, DateTime)
		string strDateCheck = "";
		int lngArrayIndex;
		int intLocationType;
		string strRKList = "";
		int lngIPCopies;
		// this is to keep track of the number of Interested Party copies to print
		bool blnIP;
		bool blnPayIPCert;
		clsDRWrapper rsIntParty = new clsDRWrapper();
		int lngMortHolder;
		// Moved to Module level
		int lngIntParties;
		bool blnIsMortCopy;
		bool blnIsIPCopy;
		// these variables are for counting the accounts
		public int lngBalanceZero;
		public int lngBalanceNeg;
		public int lngBalancePos;
		public int lngDemandFeesApplied;
		string strBalanceZero = "";
		string strBalanceNeg = "";
		string strBalancePos = "";
		string strDemandFeesApplied = "";

        private enum AddressType
        {
            AddressAtBilling = 0,
            CareOfCurrentOwner = 1,
            LastRecordedAddress = 2
        }
		public ar30DayNotice()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "30 Day Notice";
		}

		public static ar30DayNotice InstancePtr
		{
			get
			{
				return (ar30DayNotice)Sys.GetInstance(typeof(ar30DayNotice));
			}
		}

		protected ar30DayNotice _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				if (rsRate != null)
                    rsRate.Dispose();
                if (rsCMFNumbers != null)
					rsCMFNumbers.Dispose();
                if (rsCert != null)
					rsCert.Dispose();
                if (rsMort != null)
					rsMort.Dispose();
                if (rsData != null)
					rsData.Dispose();
                if (rsIntParty != null)
					rsIntParty.Dispose();
                if (rsPayments != null)
					rsPayments.Dispose();
                if (rsRE != null)
					rsRE.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ar30DayNotice_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
			// If Not rsData.EndOfFile Then
			// lblPageNumber.Text = PadToString(rsData.Fields("Account"), 6)
			// Else
			// lblPageNumber.Text = ""
			// End If
			if (eArgs.EOF)
			{
				// remove the last page
				if (boolRemoveLastPage)
				{
					if (this.Document.Pages.Count > 0)
					{
						this.Document.Pages.RemoveAt(this.Document.Pages.Count - 1);
					}
					else
					{
						// there are no accounts eligible
						// lngDemandFeesApplied = 0
					}
					//this.Refresh();
				}
				EndReport();
			}
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			if (KeyCode == Keys.Escape)
			{
				Cancel();
			}
		}

		private void EndReport()
		{
			// this will set all of the bill records to status of 1
			int lngCT/*unused?*/;
			// rsData.MoveFirst
			// Do Until rsData.EndOfFile
			// lngCT = rsData.Fields("LienProcessStatus")      'get the current status
			// If lngCT > 1 Then
			// 
			// ElseIf lngCT = 1 Then                           'already at this status
			// 
			// Else                                            'update
			// rsData.Edit
			// rsData.Fields("LienProcessStatus") = 1
			// rsData.Update
			// End If
			// rsData.MoveNext
			// Loop
			frmFreeReport.InstancePtr.vsSummary.TextMatrix(1, 1, FCConvert.ToString(lngBalanceZero));
			frmFreeReport.InstancePtr.vsSummary.TextMatrix(2, 1, FCConvert.ToString(lngBalanceNeg));
			frmFreeReport.InstancePtr.vsSummary.TextMatrix(3, 1, FCConvert.ToString(lngBalancePos));
			frmFreeReport.InstancePtr.vsSummary.TextMatrix(4, 1, FCConvert.ToString(lngBalanceZero + lngBalanceNeg + lngBalancePos));
			// this will take the trailing comma off
			if (strBalanceZero.Length > 0)
			{
				strBalanceZero = Strings.Left(strBalanceZero, strBalanceZero.Length - 1);
			}
			if (strBalanceNeg.Length > 0)
			{
				strBalanceNeg = Strings.Left(strBalanceNeg, strBalanceNeg.Length - 1);
			}
			if (strBalancePos.Length > 0)
			{
				strBalancePos = Strings.Left(strBalancePos, strBalancePos.Length - 1);
			}
			if (strDemandFeesApplied.Length > 0)
			{
				strDemandFeesApplied = Strings.Left(strDemandFeesApplied, strDemandFeesApplied.Length - 1);
			}
			// set the strings of accounts for the return grid to show
			frmFreeReport.InstancePtr.vsSummary.TextMatrix(1, 2, strBalanceZero);
			frmFreeReport.InstancePtr.vsSummary.TextMatrix(2, 2, strBalanceNeg);
			frmFreeReport.InstancePtr.vsSummary.TextMatrix(3, 2, strBalancePos);
			frmFreeReport.InstancePtr.vsSummary.TextMatrix(4, 2, strDemandFeesApplied);
			if (lngDemandFeesApplied > 0)
			{
				// show a special label that is highlighted to show this information
				// frmFreeReport.vsSummary.AddItem ""
				// frmFreeReport.vsSummary.AddItem ""
				if ((((frmFreeReport.InstancePtr.vsSummary.Rows - 1) * frmFreeReport.InstancePtr.vsSummary.RowHeight(1)) + 70) < (frmFreeReport.InstancePtr.fraSummary.Height - 200))
				{
					frmFreeReport.InstancePtr.vsSummary.Height = ((frmFreeReport.InstancePtr.vsSummary.Rows - 1) * frmFreeReport.InstancePtr.vsSummary.RowHeight(1)) + 70;
				}
				else
				{
					frmFreeReport.InstancePtr.vsSummary.Height = frmFreeReport.InstancePtr.fraSummary.Height - 200;
				}
			}
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void ar30DayNotice_ReportStart(object sender, EventArgs e)
		{            

            lblPageNumber.Left = imgTownSeal.Width;
			lblPageNumber.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
			//this.Visible = false; // this will have to be set back to true to be shown
			if (Strings.Trim(modGlobalConstants.Statics.gstrTownSealPath) != "" && modCLCalculations.Statics.gboolCLUseTownSeal30DN)
			{
				// MAL@20080602: Add check for file existence
				// Tracker Reference: 14008
				if (File.Exists(modGlobalConstants.Statics.gstrTownSealPath))
				{
					imgTownSeal.Visible = true;
					imgTownSeal.Image = FCUtils.LoadPicture(modGlobalConstants.Statics.gstrTownSealPath);
				}
				else
				{
					imgTownSeal.Visible = false;
				}
			}
			else
			{
				imgTownSeal.Visible = false;
			}
			//fso = null;
			SetHeader(true);
			lngBalanceNeg = 0;
			lngBalancePos = 0;
			lngBalanceZero = 0;
			lngDemandFeesApplied = 0;
			lngCount = 0;
            boolPayNewOwner = false;
			// MAL@20080408: Update to match Interested Party changes
			// Tracker Reference: 13116
			// Select Case UCase(Left(frmFreeReport.vsVariableSetup.TextMatrix(frfFreeReport(33).RowNumber, 1), 7))
			if (Strings.UCase(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[34].RowNumber, 1), 7)) == Strings.UCase("Show al"))
			{
				intLocationType = 0;
			}
			else if (Strings.UCase(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[34].RowNumber, 1), 7)) == Strings.UCase("Show lo"))
			{
				intLocationType = 1;
			}
			else if (Strings.UCase(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[34].RowNumber, 1), 7)) == Strings.UCase("Do not "))
			{
				intLocationType = 2;
			}
			boolPayCert = FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[16].RowNumber, 1), 2) == "Ye");
			boolMort = FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[29].RowNumber, 1), 2) == "Ye");
			blnIP = FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[33].RowNumber, 1), 2) == "Ye");
            if (Strings.Mid(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1), 29, 4).ToUpper() == "LAST")
            { // trocl-1335 09.11.2017 kjr, 5.23.18
                intBilledOwnerAddr = 2;
            }
            else
            {
                if (Strings.Mid(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1), 27, 3) == "C/O")
                {
                    intBilledOwnerAddr = 1;
                }
                else
                {
                    intBilledOwnerAddr = 0;
                }
            }
            if (Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[32].RowNumber, 1), 2) == "Ye")
			{
				if (Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[32].RowNumber, 1), 7) == "Yes, ch")
				{
					intNewOwnerChoice = 2;
					boolPayNewOwner = true;
				}
				else
				{
					intNewOwnerChoice = 1;
				}
			}
			else
			{
				intNewOwnerChoice = 0;
			}
			boolNewOwner = false;
			if (boolMort)
			{
				boolPayMortCert = !FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[29].RowNumber, 1), 9) == "Yes, with");
			}
			else
			{
				boolPayMortCert = false;
			}
			if (blnIP)
			{
				blnPayIPCert = !FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[33].RowNumber, 1), 9) == "Yes, with");
			}
			else
			{
				blnPayIPCert = false;
			}
			if (frmFreeReport.InstancePtr.chkRecommitment.CheckState == Wisej.Web.CheckState.Checked)
			{
				// make sure that the recommitment variables are set with whatever the user typed in
				modRhymalReporting.Statics.frfFreeReport[30].DatabaseFieldName = frmFreeReport.InstancePtr.vsRecommitment.TextMatrix(0, 1);
				if (Information.IsDate(frmFreeReport.InstancePtr.vsRecommitment.TextMatrix(1, 1)))
				{
					modRhymalReporting.Statics.frfFreeReport[31].DatabaseFieldName = Strings.Format(frmFreeReport.InstancePtr.vsRecommitment.TextMatrix(1, 1), "MMMM d, yyyy");
				}
				else
				{
					//FC:FINAL:MSH - I.Issue #1005: incorrect date format
					modRhymalReporting.Statics.frfFreeReport[31].DatabaseFieldName = Strings.Format(DateTime.Today, "MMMM d, yyyy");
				}
			}
			// this will add a picture of the signature of the tax collector to the 30 Day Notice
			if (modGlobal.Statics.gboolUseSigFile)
			{
				imgSig.Visible = true;
				//imgSig.ZOrder(0);
				//Line2.ZOrder(0);
				imgSig.Image = FCUtils.LoadPicture(modSignatureFile.Statics.gstrCollectorSigPath);
				imgSig.PictureAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.BottomLeft;
			}
			else
			{
				imgSig.Visible = false;
			}

			rsRE.OpenRecordset("SELECT RSAccount, DeedName1, DeedName2, ISNULL(OwnerPartyID, 0) AS OwnerPartyID, ISNULL(SecOwnerPartyID, 0) AS SecOwnerPartyID " + "FROM MASTER WHERE RSCard = 1 AND (RSDeleted = 0 OR RSDeleted IS NULL) ORDER BY RSAccount", modExtraModules.strREDatabase);
			rsRE.MoveFirst();
			rsRE.InsertName("OwnerPartyID", "Own1", false, true, true, "", false, "", true, "");			
			rsRE.MoveFirst();
			rsRate.OpenRecordset("SELECT * FROM RateRec", modExtraModules.strCLDatabase);
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			intYear = FCConvert.ToInt32(Math.Round(Conversion.Val(modExtraModules.FormatYear(frmRateRecChoice.InstancePtr.cmbYear.Text))));
			strSQL = SetupSQL();
			rsData.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
			if (rsData.EndOfFile() != true)
			{
				lngMaxNumber = rsData.RecordCount();
				rsCMFNumbers.OpenRecordset("SELECT * FROM CMFNumbers", modExtraModules.strCLDatabase);
				// lblPageNumber.Text = PadToString(rsData.Fields("Account"), 6)    'this will show the account number in the top right of the page
			}
			else
			{
				lngMaxNumber = 0;
			}
			//fso = null;
			//Application.DoEvents();
			frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Data", true, lngMaxNumber);
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			DateTime dtDate;
			// this will create another active report with the summary information on it so the user can print if the feel like it
			//FC:FINAL:DDU:#859 - let second report to run
			//if (frmReportViewer.InstancePtr.Visible)
			//{
			if (Information.IsDate(strDateCheck))
			{
				dtDate = DateAndTime.DateValue(strDateCheck);
			}
			else
			{
				dtDate = DateTime.Today;
			}
            //rptLienNoticeSummary.InstancePtr.Hide();
            fecherFoundation.Sys.ClearInstance(frmReportViewer.InstancePtr);
            if (Strings.Right(strAcctList, 1) == ",")
            {
                strAcctList = Strings.Left(strAcctList, strAcctList.Length - 1);
            }
            rptLienNoticeSummary.InstancePtr.Unload();
            rptLienNoticeSummary.InstancePtr.Init(strAcctList, "30 Day Notice Summary", strReportDesc, intYear, dtDate, 0, strRKList, FCConvert.CBool(frmFreeReport.InstancePtr.cmbSortOrder.SelectedIndex == 0), intBilledOwnerAddr);
			//}
			frmFreeReport.InstancePtr.Unload();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void BindFields()
		{
			// this sub will put all of the information into the string
			if (rsData.EndOfFile() != true)
			{
				// calculate fields
				// If rsData.Fields("Account") = 656 Then
				// Stop
				// End If
				frmWait.InstancePtr.IncrementProgress();
				CalculateVariableTotals();
				if (rsData.EndOfFile())
				{
					return;
					frmWait.InstancePtr.Unload();
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				}
				SetHeader();
				// this will set the main string
				//JEI:IT110 bug in AR, when using Text, WordWrap is not working
				//rtbText.Text = SetupVariablesInString(frmFreeReport.InstancePtr.rtbData.Text);
				rtbText.SetHtmlText(SetupVariablesInString(frmFreeReport.InstancePtr.rtbData.Text));
				//rtbText.SelStart = 0;
				//rtbText.SelLength = rtbText.Text.Length;
				//JEI:IT110 strange behaviour when setting SelectedText, original text gets doubled
				//rtbText.SelectedText = rtbText.Text;
				//rtbText.Font = new Font("Courier New", rtbText.Font.Size);
				if (modGlobal.Statics.gboolUseSigFile)
				{
					// this will set the top of the signature line
					// imgSig.Top = (Line2.Y1 - imgSig.Height) + 100
					// imgSig.Picture = LoadPicture(gstrTreasSigPath)
				}
				// this will set the bottom of the report
				SetupFooterVariables();
				if (lngCopies > 1)
				{
					// this is to produce multiple copies for certain parties (mortgage holders)
					lngCopies -= 1;
					boolCopy = true;
					// Determine if the next will be mortgage holder copy or interested party copy
					// MAL@20080429: Changed to recalc copies correctly
					// Tracker Reference: 13412,13491
					// If (lngMortHolder - 1) > 0 And lngCopies >= (lngMortHolder - 1) Then
					if (lngMortHolder > 0 && lngCopies >= lngMortHolder)
					{
						lngMortHolder -= 1;
						blnIsMortCopy = true;
						blnIsIPCopy = false;
					}
					else if (lngIntParties > 0 && lngCopies >= lngIntParties)
					{
						lngIntParties -= 1;
						blnIsMortCopy = false;
						blnIsIPCopy = true;
					}
					else
					{
						blnIsMortCopy = false;
						blnIsIPCopy = false;
					}
				}
				else
				{
					rsData.MoveNext();
					boolCopy = false;
				}
			}
			else
			{
				frmWait.InstancePtr.Unload();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			}
		}

		private void SetHeader(bool boolFirstRun = false)
		{
			if (boolFirstRun)
			{
				if (Strings.UCase(modCustomReport.Statics.strReportType) == "30DAYNOTICE")
				{
					lblReportHeader.Text = "State of Maine" + "\r\n" + "Tax Collector's Notice, Lien Claim and Demand" + "\r\n" + "30 Day Notice";
					ar30DayNotice.InstancePtr.Name = "30 Day Notice";
				}
				else
				{
				}
			}
			else
			{
				if (boolCopy)
				{
					if (Strings.UCase(modCustomReport.Statics.strReportType) == "30DAYNOTICE")
					{
						lblReportHeader.Text = "State of Maine" + "\r\n" + "Tax Collector's Notice, Lien Claim and Demand" + "\r\n" + "30 Day Notice" + "\r\n" + " * * * COPY * * *     * * * COPY * * *     * * * COPY * * * ";
					}
					else
					{
					}
				}
				else
				{
					if (Strings.UCase(modCustomReport.Statics.strReportType) == "30DAYNOTICE")
					{
						lblReportHeader.Text = "State of Maine" + "\r\n" + "Tax Collector's Notice, Lien Claim and Demand" + "\r\n" + "30 Day Notice";
					}
					else
					{
					}
				}
			}
		}

		private string SetupSQL()
		{
			string SetupSQL = "";
			// this will return the SQL statement for this batch of reports
			string strWhereClause = "";
			int intCT/*unused?*/;
			string strOrderBy = "";
			// MAL@20080702: Add new option for sort order
			// Tracker Reference: 11834
			if (frmFreeReport.InstancePtr.cmbSortOrder.SelectedIndex == 0)
			{
				strOrderBy = " ORDER BY Account, Name1";
			}
			else
			{
				strOrderBy = " ORDER BY Name1, Account";
			}
			if (frmRateRecChoice.InstancePtr.cmbRange.SelectedIndex == 2)
			{
				// range of accounts
				// strOrderBy = " ORDER BY Name1, Account"
				if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[0].Text) != "")
				{
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
					{
						// both full
						strWhereClause = " AND Account >= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text)) + " AND Account <= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
						strReportDesc = "Account " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text)) + " To " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
					}
					else
					{
						// first full second empty
						strWhereClause = " AND Account >= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text));
						strReportDesc = "Account Above " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text));
					}
				}
				else
				{
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
					{
						// first empty second full
						strWhereClause = " AND Account <= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
						strReportDesc = "Account Below " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
					}
					else
					{
						// both empty
						strWhereClause = "";
						strReportDesc = "";
					}
				}
			}
			else if (frmRateRecChoice.InstancePtr.cmbRange.SelectedIndex == 1)
			{
				// range of names
				// strOrderBy = " ORDER BY Name1, Account"
				if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[0].Text) != "")
				{
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
					{
						// both full
						strWhereClause = " AND Name1 >= '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "    ' AND Name1 <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "ZZZZ'";
						strReportDesc = "Name " + Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[0].Text) + " To " + Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text);
					}
					else
					{
						// first full second empty
						strWhereClause = " AND Name1 >= '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "'";
						strReportDesc = "Name Above " + Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[0].Text);
					}
				}
				else
				{
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
					{
						// first empty second full
						strWhereClause = " AND Name1 <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "'";
						strReportDesc = "Account Below " + Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[0].Text);
					}
					else
					{
						// both empty
						strWhereClause = "";
						strReportDesc = "";
					}
				}
			}
			else
			{
				// strOrderBy = " ORDER BY Name1, Account"
				strWhereClause = "";
				strReportDesc = "";
			}
			strRKList = frmRateRecChoice.InstancePtr.RateKeyList();
			if (Strings.Trim(strRKList) != "")
			{
				strRKList = " AND RateKey IN " + strRKList;
			}
			// create the string for the minimum amount
			if (frmFreeReport.InstancePtr.dblMinimumAmount > 0)
			{
				strWhereClause += " AND (TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4 - PrincipalPaid) > " + FCConvert.ToString(frmFreeReport.InstancePtr.dblMinimumAmount);
			}
			if (frmFreeReport.InstancePtr.boolInitial)
			{
				// this should be looking in the billingmasteryear query so that the
				// rate record choices will be reflected in the initial run of the notices
				// Dave 5/14/07 Took out search criteria AND LienStatusEligibility = 1
				// kgk 2-14-2012  Change from Stored Procs  rsData.CreateStoredProcedure "30DayNoticeQuery", "SELECT * From BillingMasterYear WHERE BillingType = 'RE' AND NOT (LienProcessExclusion = 1) AND (LienProcessStatus = 0 OR LienProcessStatus = 1) AND BillingYear/10 = " & intYear & strWhereClause & strRKList & strOrderBy  'AND LienStatusEligibility = 1
				SetupSQL = "SELECT * From (" + modGlobal.Statics.gstrBillYrQuery + ") AS qBY WHERE BillingType = 'RE' AND NOT (LienProcessExclusion = 1) AND (LienProcessStatus = 0 OR LienProcessStatus = 1) AND BillingYear/10 = " + FCConvert.ToString(intYear) + strWhereClause + strRKList + strOrderBy;
				// AND LienStatusEligibility = 1
			}
			else
			{
				// rsData.CreateStoredProcedure "30DayNoticeQuery", "SELECT * From BillingMasterYear WHERE BillingType = 'RE' AND NOT (LienProcessExclusion = 1) AND (LienProcessStatus = 0 OR LienProcessStatus = 1) AND (LienStatusEligibility = 1 OR LienStatusEligibility = 2) AND BillingYear/10 = " & intYear & strWhereClause & strRKList & strOrderBy
				SetupSQL = "SELECT * From (" + modGlobal.Statics.gstrBillYrQuery + ") AS qBY WHERE BillingType = 'RE' AND NOT (LienProcessExclusion = 1) AND (LienProcessStatus = 0 OR LienProcessStatus = 1) AND (LienStatusEligibility = 1 OR LienStatusEligibility = 2) AND BillingYear/10 = " + FCConvert.ToString(intYear) + strWhereClause + strRKList + strOrderBy;
			}
			// SetupSQL = "SELECT * FROM 30DayNoticeQuery"
			return SetupSQL;
		}

		private string SetupVariablesInString(string strOriginal)
		{
			string SetupVariablesInString = "";
			// this will replace all of the variables with the information needed
			string strBuildString;
			// this is the string that will be built and returned at the end
			string strTemp = "";
			// this is a temporary sting that will be used to store and transfer string segments
			int lngNextVariable;
			// this is the position of the beginning of the next variable (0 = no more variables)
			int lngEndOfLastVariable;
			// this is the position of the end of the last variable
			lngEndOfLastVariable = 0;
			lngNextVariable = 1;
			strBuildString = "";
			// priming read
			if (Strings.InStr(1, strOriginal, "<") > 0)
			{
				lngNextVariable = Strings.InStr(lngEndOfLastVariable + 1, strOriginal, "<");
				// do until there are no more variables left
				do
				{
					// add the string from lngEndOfLastVariable - lngNextVariable to the BuildString
					strBuildString += Strings.Mid(strOriginal, lngEndOfLastVariable + 1, lngNextVariable - lngEndOfLastVariable - 1);
					// set the end pointer
					lngEndOfLastVariable = Strings.InStr(lngNextVariable, strOriginal, ">");
					// replace the variable
					strBuildString += GetVariableValue_2(Strings.Mid(strOriginal, lngNextVariable + 1, lngEndOfLastVariable - lngNextVariable - 1));
					// check for another variable
					lngNextVariable = Strings.InStr(lngEndOfLastVariable, strOriginal, "<");
				}
				while (!(lngNextVariable == 0));
			}
			// take the last of the string and add it to the end
			strBuildString += Strings.Mid(strOriginal, lngEndOfLastVariable + 1);
			// check for variables
			if (Strings.InStr(1, "<", strOriginal) > 0)
			{
				// strBuildString = SetupVariablesInString(strBuildString)     'setup recursion
				FCMessageBox.Show("ERROR: There are still variables in the report string.", MsgBoxStyle.Critical, "SetupVariablesInString Error");
			}
			else
			{
				SetupVariablesInString = strBuildString;
			}
			return SetupVariablesInString;
		}

		private void SetupFooterVariables()
		{
			// this will setup the bottom part of the detail section
			fldCollector.Text = GetVariableValue_2("COLLECTOR");
			if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
			{
				fldMuni.Text = GetVariableValue_2("CITYTOWNOF") + " of " + GetVariableValue_2("MUNI");
			}
			else
			{
				fldMuni.Text = GetVariableValue_2("MUNI");
			}
			fldPrincipal.Text = Strings.Format(FCConvert.ToDouble(GetVariableValue_2("PRINCIPAL")) - Conversion.Val(rsData.Get_Fields_Decimal("PrincipalPaid")), "#,##0.00");
			fldInterest.Text = Strings.Format(GetVariableValue_2("INTEREST"), "#,##0.00");
			fldDemand.Text = Strings.Format(GetVariableValue_2("DEMAND"), "#,##0.00");
			fldCertMailFee.Text = Strings.Format(GetVariableValue_2("CERTTOTAL"), "#,##0.00");
			if (fldPrincipal.Text == "")
			{
				fldPrincipal.Text = "0.00";
			}
			if (fldInterest.Text == "")
			{
				fldInterest.Text = "0.00";
			}
			if (fldDemand.Text == "")
			{
				fldDemand.Text = "0.00";
			}
			if (fldCertMailFee.Text == "")
			{
				fldCertMailFee.Text = "0.00";
			}
			fldTotal.Text = Strings.Format(FCConvert.ToDouble(fldPrincipal.Text) + FCConvert.ToDouble(fldInterest.Text) + FCConvert.ToDouble(fldDemand.Text) + FCConvert.ToDouble(fldCertMailFee.Text), "#,##0.00");
		}

		private string GetVariableValue_2(string strVarName)
		{
			return GetVariableValue(ref strVarName);
		}

		private string GetVariableValue(ref string strVarName)
		{
			string GetVariableValue = "";
			// this function will take a variable name and find it in the array, then get the value and return it as a string
			int intCT;
			string strValue = "";
			// this is a dummy code to show the user where the hard codes are
			if (strVarName == "CRLF")
			{
				GetVariableValue = "";
				return GetVariableValue;
			}
			if (strVarName == "ZIP4")
			{
				strValue = strValue;
			}
			//FC:FINAL:KS: #without nr.:fix Exit For inside switch/case statement
			bool endFor = false;
			for (intCT = 0; intCT <= modRhymalReporting.MAXFREEVARIABLES; intCT++)
			{
				if (Strings.UCase(strVarName) == Strings.UCase(modRhymalReporting.Statics.frfFreeReport[intCT].Tag))
				{
					switch (modRhymalReporting.Statics.frfFreeReport[intCT].VariableType)
					{
						case 0:
							{
								// value from the static variables in the grid
								switch (modRhymalReporting.Statics.frfFreeReport[intCT].Type)
								{
								// 0 = Text, 1 = Number, 2 = Date, 3 = Do not ask for default (comes from DB), 4 = Formatted Year
									case 0:
										{
											strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
											break;
										}
									case 1:
										{
											strValue = Strings.Format(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1), "#,##0.00");
											break;
										}
									case 2:
										{
											strValue = Strings.Format(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1), "MMMM d, yyyy");
											break;
										}
									case 3:
										{
											strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
											break;
										}
									case 4:
										{
											strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
											break;
										}
								}
								//FC:FINAL:KS: #without nr.:fix Exit For inside switch/case statement
								//end switch								
								//break;
								endFor = true;
								break;
							}
						case 1:
							{
								// value from the dynamic variables in the database
								strValue = FCConvert.ToString(rsData.Get_Fields(modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName));
								if (Strings.Trim(strValue) == "")
								{
									if (Strings.UCase(modRhymalReporting.Statics.frfFreeReport[intCT].Tag) != "LESSPAYMENTS")
									{
										// maybe put a blank line there
										strValue = "__________";
									}
								}
								//FC:FINAL:KS: #without nr.:fix Exit For inside switch/case statement                                								
								endFor = true;
								//break;
								break;
							}
						case 2:
							{
								// questions at the bottom of the grid
								strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
								//FC:FINAL:KS: #without nr.:fix Exit For inside switch/case statement                                								
								endFor = true;
								//break;
								break;
							}
						case 3:
							{
								// calculated values that are stored in the DatabaseFieldName field
								strValue = modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName;
								if (Strings.Trim(strValue) == "")
								{
									if (Strings.UCase(modRhymalReporting.Statics.frfFreeReport[intCT].Tag) != "LESSPAYMENTS")
									{
										// maybe put a blank line there
										strValue = "__________";
									}
								}
								//FC:FINAL:KS: #without nr.:fix Exit For inside switch/case statement                                								
								endFor = true;
								//break;
								break;
							}
					}
					//end switch
					//FC:FINAL:KS: #without nr.:fix Exit For inside switch/case statement                                								
					if (endFor)
					{
						break;
					}
				}
			}
			GetVariableValue = strValue;
			return GetVariableValue;
		}

		private void CalculateVariableTotals()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will calculate all of the fields that need it and store
				// the value in the DatabaseFieldName string in the frfFreeReport
				// struct that these fields include principal, interest, costs,
				// total due and certified mail fee
				clsDRWrapper rsRK = new clsDRWrapper();
				DateTime dtBillDate;
				double dblTotalDue = 0;
				double dblInt;
				double dblPrin;
				double dblPayments;
				double dblDemand;
				double dblCertMailFee;
				string strAddressBar = "";
				string strCommitmentDate = "";
				string strLessPaymentsLine = "";
				string strLocation = "";
				string strTemp1 = "";
				string strTemp2 = "";
				string strTemp3 = "";
				string strTemp4 = "";
                string strTemp5 = "";
				string strOwnerName = "";
				string strMapLot = "";
				string strBookPage = "";
				bool boolSameName = false;
				double dblAbate1 = 0;
				double dblAbate2 = 0;
				double dblAbate3 = 0;
				double dblAbate4 = 0;
				string strWhere = "";
				// for CMFNumbers
				string strCMFName = "";
				int lngCMFBillKey = 0;
				int lngCMFMHNumber = 0;
				bool boolPrintCMF;
				// true if a CMF should be printed for that person
				string strCMFAddr1;
				string strCMFAddr2;
				string strCMFAddr3;
				string strCMFAddr4;
                string strRSName1 = "";
                string strRSName2 = "";
                string strRSAddr1 = "";
                string strRSAddr2 = "";
                string strRSAddr3 = "";
                string strRSAddr4 = "";

                // trocl-1335 09.11.17 kjr   4.23.18 CODE FREEZE
                bool boolNewAddress = false;
                string strPrevSecOwnerName = "";
                string strPrevAddress1 = "";
                string strPrevAddress2 = "";
                string strPrevAddress3 = "";
                TryAgain:
				;
				strCMFAddr1 = "";
				strCMFAddr2 = "";
				strCMFAddr3 = "";
				strCMFAddr4 = "";
				// MAL@20080812: Reset the values
				// Tracker Reference: 14980
				dblInt = 0;
				dblPrin = 0;
				dblPayments = 0;
				dblDemand = 0;
				dblCertMailFee = 0;
				if (!boolCopy)
				{
					boolNewOwner = false;
					strDateCheck = GetVariableValue_2("MAILDATE");
					if (Strings.Trim(strDateCheck) == "")
					{
						strDateCheck = FCConvert.ToString(DateTime.Today);
					}
					if (!modGlobal.Statics.gboolUseMailDateForMaturity)
					{
						// this will calculate the interest ahead 30 days from the mail date
						strDateCheck = FCConvert.ToString(DateAndTime.DateAdd("D", 30, DateAndTime.DateValue(strDateCheck)));
					}
					rsRK.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(rsData.Get_Fields_Int32("RateKey")), modExtraModules.strCLDatabase);
					if (!rsRK.EndOfFile())
					{
						dtBillDate = rsRK.Get_Fields_DateTime("CommitmentDate");
					}
					else
					{
						// ***************************************************************************************************
						// MATTHEW 8/8/2005 CALL ID 74572
						if (Information.IsDate(rsData.Get_Fields("TransferFromBillingDateFirst")))
						{
							dtBillDate = rsData.Get_Fields_DateTime("TransferFromBillingDateFirst");
						}
						else
						{
							dtBillDate = rsData.Get_Fields_DateTime("TransferFromBillingDateLast");
						}
						// ***************************************************************************************************
						// dtBillDate = rsData.Fields("TransferFromBillingDateFirst")
					}
					// this will calculate the principal, interest and total due
					if (rsData.Get_Fields_Int32("LienRecordNumber") != 0)
					{
						dblTotalDue = modCLCalculations.CalculateAccountCLLien(rsData, DateAndTime.DateValue(strDateCheck), ref dblInt);
					}
					else
					{
						dblAbate1 = 0;
						dblAbate2 = 0;
						dblAbate3 = 0;
						dblAbate4 = 0;
						if (!rsRK.EndOfFile())
						{
							CalculateAbatementAmount_26(rsData.Get_Fields_Int32("ID"), false, rsRK.Get_Fields_Int16("NumberOfPeriods"), ref dblAbate1, ref dblAbate2, ref dblAbate3, ref dblAbate4);
						}
						else
						{
							CalculateAbatementAmount_26(rsData.Get_Fields_Int32("ID"), false, 1, ref dblAbate1, ref dblAbate2, ref dblAbate3, ref dblAbate4);
						}
						// if a blank date is returned, then this line will crash
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						dblTotalDue = modCLCalculations.CalculateAccountCL(ref rsData, FCConvert.ToInt32(rsData.Get_Fields("Account")), DateAndTime.DateValue(strDateCheck), ref dblInt, ref dblAbate1, ref dblAbate2, ref dblAbate3, ref dblAbate4, true);
					}
					if (modGlobal.Round(dblTotalDue, 2) == 0)
					{
						lngBalanceZero += 1;
						strBalanceZero += FCConvert.ToString(rsData.Get_Fields_Int32("ID")) + ", ";
						rsData.MoveNext();
						if (rsData.EndOfFile())
						{
							boolRemoveLastPage = true;
							return;
						}
						goto TryAgain;
					}
					// this will check to see if demand fees are already in place on this acocunt
					if (modGlobal.Round(Conversion.Val(rsData.Get_Fields_Decimal("DemandFees")), 2) != 0)
					{
						if (lngDemandFeesApplied == 0)
						{
							frmFreeReport.InstancePtr.vsSummary.AddItem("");
							frmFreeReport.InstancePtr.vsSummary.AddItem("Accounts that already have demand fees applied:");
							frmFreeReport.InstancePtr.vsSummary.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
							frmFreeReport.InstancePtr.vsSummary.IsSubtotal(frmFreeReport.InstancePtr.vsSummary.Rows - 1, true);
							frmFreeReport.InstancePtr.vsSummary.RowOutlineLevel(frmFreeReport.InstancePtr.vsSummary.Rows - 1, 1);
							frmFreeReport.InstancePtr.vsSummary.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
							frmFreeReport.InstancePtr.vsSummary.Height = frmFreeReport.InstancePtr.vsSummary.Height + 540;
						}
						lngDemandFeesApplied += 1;
						strDemandFeesApplied += FCConvert.ToString(rsData.Get_Fields_Int32("ID")) + ", ";
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						frmFreeReport.InstancePtr.vsSummary.AddItem("\t" + FCConvert.ToString(rsData.Get_Fields("Account")));
						frmFreeReport.InstancePtr.vsSummary.IsSubtotal(frmFreeReport.InstancePtr.vsSummary.Rows - 1, true);
						frmFreeReport.InstancePtr.vsSummary.RowOutlineLevel(frmFreeReport.InstancePtr.vsSummary.Rows - 1, 2);
						frmFreeReport.InstancePtr.vsSummary.Height = frmFreeReport.InstancePtr.vsSummary.Height + 270;
						rsData.MoveNext();
						if (rsData.EndOfFile())
						{
							boolRemoveLastPage = true;
							return;
						}
						goto TryAgain;
					}
					if (dblTotalDue > 0)
					{
						// these are the account that will be demanded
						lngBalancePos += 1;
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						strAcctList += FCConvert.ToString(rsData.Get_Fields("Account")) + ",";
						strBalancePos += FCConvert.ToString(rsData.Get_Fields_Int32("ID")) + ", ";
					}
					else if (dblTotalDue < 0)
					{
						lngBalanceNeg += 1;
						strBalanceNeg += FCConvert.ToString(rsData.Get_Fields_Int32("ID")) + ", ";
						rsData.MoveNext();
						if (rsData.EndOfFile())
						{
							boolRemoveLastPage = true;
							return;
						}
						goto TryAgain;
					}
					// this will find out how many mortgage holders this account has and how many copies to print
					if (boolMort)
					{
						lngMortHolder = CalculateMortgageHolders();
						lngCopies = lngMortHolder + 1;
					}
					else
					{
						lngMortHolder = 0;
						lngCopies = 1;
					}
					// this will find out the certified mail fee
					if (boolPayCert)
					{
						// lngMortHolder = lngMortHolder + 1
						if (boolPayMortCert)
						{
							dblCertMailFee = (lngMortHolder * FCConvert.ToDouble(GetVariableValue_2("MAILFEE"))) + FCConvert.ToDouble(GetVariableValue_2("MAILFEE"));
						}
						else
						{
							dblCertMailFee = FCConvert.ToDouble(GetVariableValue_2("MAILFEE"));
						}
					}
					else
					{
						if (boolPayMortCert)
						{
							dblCertMailFee = lngMortHolder * FCConvert.ToDouble(GetVariableValue_2("MAILFEE"));
						}
						else
						{
							dblCertMailFee = 0;
						}
					}
					// this will find out how many interested parties this account has and how many copies to print
					if (blnIP)
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						lngIntParties = modCLCalculations.CalculateInterestedParties(FCConvert.ToInt32(rsData.Get_Fields("Account")), rsIntParty);
						lngCopies += lngIntParties;
					}
					else
					{
						lngIntParties = 0;
						lngCopies = lngCopies;
					}
					// this will find out the certified mail fee
					if (boolPayCert)
					{
						if (blnPayIPCert)
						{
							dblCertMailFee += (lngIntParties * FCConvert.ToDouble(GetVariableValue_2("MAILFEE")));
						}
						else
						{
							dblCertMailFee = dblCertMailFee;
						}
					}
					else
					{
						if (blnPayIPCert)
						{
							dblCertMailFee += (lngIntParties * FCConvert.ToDouble(GetVariableValue_2("MAILFEE")));
						}
						else
						{
							dblCertMailFee = dblCertMailFee;
						}
					}
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					rsRE.FindFirstRecord("RSAccount", rsData.Get_Fields("Account"));
					// check to see if one is going to be sent to the next owner
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (intNewOwnerChoice != 0 && modCLCalculations.NewOwner(rsData.Get_Fields_String("Name1"), FCConvert.ToInt32(rsData.Get_Fields("Account")),  dtBillDate, ref boolREMatch, ref boolSameName))
					{
						// this should set the RE account as well
						// if there has been a new owner, then make a copy for the new owner as well
						if (!boolNewOwner)
						{
							if (intNewOwnerChoice == 2)
							{
								// charge for the new owner's copy
								dblCertMailFee += FCConvert.ToDouble(GetVariableValue_2("MAILFEE"));
							}
							lngCopies += 1;
							boolNewOwner = true;
						}
					}
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					strAccountNumber = modGlobal.PadToString(rsData.Get_Fields("Account"), 6);
					lblPageNumber.Text = strAccountNumber;
					if (Strings.Trim(rsData.Get_Fields_String("Name2")) != "")
					{
						strOwnerName = rsData.Get_Fields_String("Name1") + " and " + rsData.Get_Fields_String("Name2");
					}
					else
					{
						strOwnerName = rsData.Get_Fields_String("Name1");
					}
					dblPrin = Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1") + rsData.Get_Fields_Decimal("TaxDue2") + rsData.Get_Fields_Decimal("TaxDue3") + rsData.Get_Fields_Decimal("TaxDue4"));
					dblPayments = Conversion.Val(rsData.Get_Fields_Decimal("PrincipalPaid"));
					dblInt = (FCConvert.ToDouble(rsData.Get_Fields_Decimal("InterestCharged")) * -1) - Conversion.Val(rsData.Get_Fields_Decimal("InterestPaid")) + dblInt;
					// less payments line
					if (modGlobal.Round(dblPayments, 2) != 0)
					{
						// only show this when there are payments
						if (dblPayments < 0)
						{
							// if the principal paid is a negative number then some adjustment has happened (supplemental) this should not happen later in the code life
							rsPayments.OpenRecordset("SELECT ID FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(rsData.Get_Fields_Int32("ID")) + " AND (Code = 'A' OR Code = 'R' OR Code = 'S' OR Code = 'D')");
							if (rsPayments.EndOfFile())
							{
								// MAL@20080408: Correct to add currency symbol ; Tracker Reference: 13118
								// strLessPaymentsLine = "plus adjustment of " & Format(Abs(dblPayments), "#,##0.00") & " for the net sum of " & Format(dblPrin - dblPayments, "#,##0.00") & ","
								strLessPaymentsLine = "plus adjustment of " + Strings.Format(Math.Abs(dblPayments), "$#,##0.00") + " for the net sum of " + Strings.Format(dblPrin - dblPayments, "$#,##0.00") + ",";
							}
							else
							{
								// strLessPaymentsLine = "plus adjustment of " & Format(Abs(dblPayments), "#,##0.00") & " for the net sum of " & Format(dblPrin - dblPayments, "#,##0.00") & ","
								strLessPaymentsLine = "plus adjustment of " + Strings.Format(Math.Abs(dblPayments), "$#,##0.00") + " for the net sum of " + Strings.Format(dblPrin - dblPayments, "$#,##0.00") + ",";
							}
						}
						else
						{
							// principal paid is greater then zero
							rsPayments.OpenRecordset("SELECT ID FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(rsData.Get_Fields_Int32("ID")) + " AND (Code = 'A' OR Code = 'R' OR Code = 'S' OR Code = 'D')");
							if (rsPayments.EndOfFile())
							{
								// MAL@20080408: Correct to add currency symbol ; Tracker Reference: 13118
								// strLessPaymentsLine = "less payment and adjustment of " & Format(dblPayments, "#,##0.00") & " for the net sum of " & Format(dblPrin - dblPayments, "#,##0.00") & ","
								strLessPaymentsLine = "less payment and adjustment of " + Strings.Format(dblPayments, "$#,##0.00") + " for the net sum of " + Strings.Format(dblPrin - dblPayments, "$#,##0.00") + ",";
							}
							else
							{
								// strLessPaymentsLine = "less payment of " & Format(dblPayments, "#,##0.00") & " for the net sum of " & Format(dblPrin - dblPayments, "#,##0.00") & ","
								strLessPaymentsLine = "less payment of " + Strings.Format(dblPayments, "$#,##0.00") + " for the net sum of " + Strings.Format(dblPrin - dblPayments, "$#,##0.00") + ",";
							}
						}
					}
					else
					{
						strLessPaymentsLine = "";
					}
					if (boolREMatch)
					{
						if (intLocationType != 2)
						{
							// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
							if (((rsData.Get_Fields("streetnumber"))) != 0 && Strings.Trim(FCConvert.ToString(rsData.Get_Fields("streetnumber"))) != "")
							{
								if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("StreetName"))) != "")
								{
									// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [streetnumber] and replace with corresponding Get_Field method
									strLocation = Strings.Trim(FCConvert.ToString(rsData.Get_Fields("streetnumber"))) + " " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("StreetName")));
								}
								else
								{
									strLocation = "";
								}
							}
							else
							{
								if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("StreetName"))) != "")
								{
									strLocation = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("StreetName")));
								}
								else
								{
									strLocation = "";
								}
							}
							strMapLot = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("MapLot")));
						}
						else
						{
							strLocation = "";
							strMapLot = "";
						}
					}
					else
					{
						strLocation = "";
						strMapLot = "";
					}
					// BookPage
					if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("ShowRef1")))
					{
						if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Ref1"))) == "")
						{
							strBookPage = "";
						}
						else
						{
							strBookPage = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Ref1")));
						}
					}
					else
					{
						if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("BookPage"))) == "")
						{
							strBookPage = "";
						}
						else
						{
							strBookPage = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("BookPage")));
						}
					}
					if (Strings.Trim(strLocation) == "" || !boolREMatch)
					{
						strLocation = "________________";
					}
					if (Strings.Trim(strMapLot) == "" || !boolREMatch)
					{
						strMapLot = "________________";
					}
					if (Strings.Trim(strBookPage) == "")
					{
						strBookPage = "________________";
					}
					// Commitment Date
					rsRate.FindFirstRecord("ID", rsData.Get_Fields_Int32("RateKey"));
					if (!rsRate.NoMatch)
					{
						//FC:FINAL:DDU #i111 conversion to corect datetime
						DateTime dt;
						if (DateTime.TryParse(FCConvert.ToString(rsRate.Get_Fields_DateTime("CommitmentDate")), out dt))
						{
							strCommitmentDate = Strings.Format(dt, "MMMM d, yyyy");
						}
						else
						{
							strCommitmentDate = "__________";
						}
					}
					else
					{
						strCommitmentDate = "__________";
					}
					// Address Bar
                    clsAddress tAddr;

                    switch (intBilledOwnerAddr)
                    {
                        case (int)AddressType.AddressAtBilling:
                            tAddr = tAddrCtrlr.GetOwnerAndAddressAtBilling(rsData);
                            break;
                        case (int)AddressType.CareOfCurrentOwner:
                            tAddr = tAddrCtrlr.GetOwnerAtBillingCareOfCurrentOwner(rsData);
                            break;
                        case (int)AddressType.LastRecordedAddress:
                            tAddr = tAddrCtrlr.GetBilledOwnerAtLastAddress(rsData);
                            break;
                        default:
                            tAddr = new clsAddress();
                            break;
                    }

                    strAddressBar = "    " + tAddr.Addressee;
                    strAddressBar += "\n    " + tAddr.Get_Address(1).Trim();
                    strAddressBar += "\n    " + tAddr.Get_Address(2).Trim();
                    strAddressBar += "\n    " + tAddr.Get_Address(3).Trim();
                    strAddressBar += "\n    " + tAddr.Get_Address(4).Trim();

                    strCMFAddr1 = tAddr.Get_Address(1).Trim();
                    strCMFAddr2 = tAddr.Get_Address(2).Trim();
                    strCMFAddr3 = tAddr.Get_Address(3).Trim();
                    strCMFAddr4 = tAddr.Get_Address(4).Trim();

                    // info for CMFNumbers table
                    strCMFName = strOwnerName;
					lngCMFMHNumber = 0;
					// boolNewOwner = False
					lngCMFBillKey = FCConvert.ToInt32(rsData.Get_Fields_Int32("ID"));

					// Billing Year (1)
					modRhymalReporting.Statics.frfFreeReport[1].DatabaseFieldName = FCConvert.ToString(intYear);
					// MapLot (2)
					modRhymalReporting.Statics.frfFreeReport[2].DatabaseFieldName = strMapLot;
					// BookPage (3)
					modRhymalReporting.Statics.frfFreeReport[3].DatabaseFieldName = strBookPage;
					// Commitment Date (9)
					modRhymalReporting.Statics.frfFreeReport[9].DatabaseFieldName = strCommitmentDate;
					// Principal (17)
					modRhymalReporting.Statics.frfFreeReport[17].DatabaseFieldName = Strings.Format(dblPrin, "#,##0.00");
					// Interest (18)
					modRhymalReporting.Statics.frfFreeReport[18].DatabaseFieldName = Strings.Format(dblInt, "#,##0.00");
					// Less Payments (19)
					modRhymalReporting.Statics.frfFreeReport[19].DatabaseFieldName = strLessPaymentsLine;
					// City/Town (20)
					modRhymalReporting.Statics.frfFreeReport[20].DatabaseFieldName = modGlobalConstants.Statics.gstrCityTown;
					// Owner Name (21)
					modRhymalReporting.Statics.frfFreeReport[21].DatabaseFieldName = strOwnerName;
					// Total Certified Mail Fee
					modRhymalReporting.Statics.frfFreeReport[28].DatabaseFieldName = Strings.Format(dblCertMailFee, "#,##0.00");
					// Total Due (25)
					modRhymalReporting.Statics.frfFreeReport[25].DatabaseFieldName = Strings.Format(dblTotalDue + dblCertMailFee + FCConvert.ToDouble(GetVariableValue_2("DEMAND")), "#,##0.00");
					// Location (26)
					modRhymalReporting.Statics.frfFreeReport[26].DatabaseFieldName = strLocation;
					// Name And Address Bar
					modRhymalReporting.Statics.frfFreeReport[27].DatabaseFieldName = strAddressBar;
					Array.Resize(ref modGlobal.Statics.arrDemand, lngArrayIndex + 1);
					// this will make sure that there are enough elements to use
					//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
					modGlobal.Statics.arrDemand[lngArrayIndex] = new modGlobal.AccountFeesAdded(0);
					modGlobal.Statics.arrDemand[lngArrayIndex].Used = true;
					modGlobal.Statics.arrDemand[lngArrayIndex].Processed = false;
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					modGlobal.Statics.arrDemand[lngArrayIndex].Account = FCConvert.ToInt32(rsData.Get_Fields("Account"));
					modGlobal.Statics.arrDemand[lngArrayIndex].Name = FCConvert.ToString(rsData.Get_Fields_String("Name1"));
					modGlobal.Statics.arrDemand[lngArrayIndex].Fee = FCConvert.ToDouble(GetVariableValue_2("DEMAND"));
					modGlobal.Statics.arrDemand[lngArrayIndex].CertifiedMailFee = dblCertMailFee;
					lngArrayIndex += 1;
					rsData.Edit();
					if (intNewOwnerChoice == 1 && boolNewOwner && boolMort && lngMortHolder > 0 && !boolPayMortCert)
					{
						if (lngIPCopies > 0 && !blnPayIPCert)
						{
							rsData.Set_Fields("Copies", lngCopies - 1 - lngMortHolder - lngIPCopies);
						}
						else
						{
							rsData.Set_Fields("Copies", lngCopies - 1 - lngMortHolder);
						}
					}
					else if (intNewOwnerChoice == 1 && boolNewOwner)
					{
						if (lngIPCopies > 0 && !blnPayIPCert)
						{
							rsData.Set_Fields("Copies", lngCopies - 1 - lngIPCopies);
						}
						else
						{
							rsData.Set_Fields("Copies", lngCopies - 1);
						}
					}
					else if (boolMort && lngMortHolder > 0 && !boolPayMortCert)
					{
						if (lngIPCopies > 0 && !blnPayIPCert)
						{
							rsData.Set_Fields("Copies", lngCopies - lngMortHolder - lngIPCopies);
						}
						else
						{
							rsData.Set_Fields("Copies", lngCopies - lngMortHolder);
						}
					}
					else
					{
						if (lngIPCopies > 0 && !blnPayIPCert)
						{
							rsData.Set_Fields("Copies", lngCopies - lngIPCopies);
						}
						else
						{
							rsData.Set_Fields("Copies", lngCopies);
						}
					}
					rsData.Update();
				}
				else
				{
					// this is the difference when it is a copy
					if (blnIsMortCopy)
					{
						if (boolPayMortCert)
						{
							// if the user is getting charged for certified mail, then save their info to print on Cert Mail Forms
							boolPrintCMF = true;
						}
						else
						{
							boolPrintCMF = false;
						}
						// address bar
						if (rsMort.EndOfFile() != true)
						{
							// info for CMFNumbers table
							strCMFName = FCConvert.ToString(rsMort.Get_Fields_String("Name"));
							lngCMFMHNumber = FCConvert.ToInt32(rsMort.Get_Fields_Int32("MortgageHolderID"));
							// "MortgageAssociation.MortgageHolderID")
							// boolNewOwner = False
							// lngCMFBillKey = 0
							lngCMFBillKey = FCConvert.ToInt32(rsData.Get_Fields_Int32("ID"));
							if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address1"))) != "")
							{
								strCMFAddr1 = FCConvert.ToString(rsMort.Get_Fields_String("Address1"));
								if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address2"))) != "")
								{
									strCMFAddr2 = FCConvert.ToString(rsMort.Get_Fields_String("Address2"));
									if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address3"))) != "")
									{
										strCMFAddr3 = FCConvert.ToString(rsMort.Get_Fields_String("Address3"));
										if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
										{
											strCMFAddr4 = FCConvert.ToString(rsMort.Get_Fields_String("CITY")) + ", " + FCConvert.ToString(rsMort.Get_Fields_String("STATE")) + " " + FCConvert.ToString(rsMort.Get_Fields_String("ZIP")) + " " + FCConvert.ToString(rsMort.Get_Fields_String("ZIP4"));
										}
										else
										{
											strCMFAddr4 = FCConvert.ToString(rsMort.Get_Fields_String("CITY")) + ", " + FCConvert.ToString(rsMort.Get_Fields_String("STATE")) + " " + FCConvert.ToString(rsMort.Get_Fields_String("ZIP"));
										}
									}
									else
									{
										if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
										{
											strCMFAddr3 = FCConvert.ToString(rsMort.Get_Fields_String("CITY")) + ", " + FCConvert.ToString(rsMort.Get_Fields_String("STATE")) + " " + FCConvert.ToString(rsMort.Get_Fields_String("ZIP")) + " " + FCConvert.ToString(rsMort.Get_Fields_String("ZIP4"));
										}
										else
										{
											strCMFAddr3 = FCConvert.ToString(rsMort.Get_Fields_String("CITY")) + ", " + FCConvert.ToString(rsMort.Get_Fields_String("STATE")) + " " + FCConvert.ToString(rsMort.Get_Fields_String("ZIP"));
										}
									}
								}
								else
								{
									if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address3"))) != "")
									{
										strCMFAddr2 = FCConvert.ToString(rsMort.Get_Fields_String("Address3"));
										if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
										{
											strCMFAddr3 = FCConvert.ToString(rsMort.Get_Fields_String("CITY")) + ", " + FCConvert.ToString(rsMort.Get_Fields_String("STATE")) + " " + FCConvert.ToString(rsMort.Get_Fields_String("ZIP")) + " " + FCConvert.ToString(rsMort.Get_Fields_String("ZIP4"));
										}
										else
										{
											strCMFAddr3 = FCConvert.ToString(rsMort.Get_Fields_String("CITY")) + ", " + FCConvert.ToString(rsMort.Get_Fields_String("STATE")) + " " + FCConvert.ToString(rsMort.Get_Fields_String("ZIP"));
										}
									}
									else
									{
										if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
										{
											strCMFAddr2 = FCConvert.ToString(rsMort.Get_Fields_String("CITY")) + ", " + FCConvert.ToString(rsMort.Get_Fields_String("STATE")) + " " + FCConvert.ToString(rsMort.Get_Fields_String("ZIP")) + " " + FCConvert.ToString(rsMort.Get_Fields_String("ZIP4"));
										}
										else
										{
											strCMFAddr2 = FCConvert.ToString(rsMort.Get_Fields_String("CITY")) + ", " + FCConvert.ToString(rsMort.Get_Fields_String("STATE")) + " " + FCConvert.ToString(rsMort.Get_Fields_String("ZIP"));
										}
									}
								}
							}
							else
							{
								if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address2"))) != "")
								{
									strCMFAddr1 = FCConvert.ToString(rsMort.Get_Fields_String("Address2"));
									if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address3"))) != "")
									{
										strCMFAddr2 = FCConvert.ToString(rsMort.Get_Fields_String("Address3"));
										if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
										{
											strCMFAddr3 = FCConvert.ToString(rsMort.Get_Fields_String("CITY")) + ", " + FCConvert.ToString(rsMort.Get_Fields_String("STATE")) + " " + FCConvert.ToString(rsMort.Get_Fields_String("ZIP")) + " " + FCConvert.ToString(rsMort.Get_Fields_String("ZIP4"));
										}
										else
										{
											strCMFAddr3 = FCConvert.ToString(rsMort.Get_Fields_String("CITY")) + ", " + FCConvert.ToString(rsMort.Get_Fields_String("STATE")) + " " + FCConvert.ToString(rsMort.Get_Fields_String("ZIP"));
										}
									}
									else
									{
										if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
										{
											strCMFAddr2 = FCConvert.ToString(rsMort.Get_Fields_String("CITY")) + ", " + FCConvert.ToString(rsMort.Get_Fields_String("STATE")) + " " + FCConvert.ToString(rsMort.Get_Fields_String("ZIP")) + " " + FCConvert.ToString(rsMort.Get_Fields_String("ZIP4"));
										}
										else
										{
											strCMFAddr2 = FCConvert.ToString(rsMort.Get_Fields_String("CITY")) + ", " + FCConvert.ToString(rsMort.Get_Fields_String("STATE")) + " " + FCConvert.ToString(rsMort.Get_Fields_String("ZIP"));
										}
									}
								}
								else
								{
									if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address3"))) != "")
									{
										strCMFAddr1 = FCConvert.ToString(rsMort.Get_Fields_String("Address3"));
										if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
										{
											strCMFAddr2 = FCConvert.ToString(rsMort.Get_Fields_String("CITY")) + ", " + FCConvert.ToString(rsMort.Get_Fields_String("STATE")) + " " + FCConvert.ToString(rsMort.Get_Fields_String("ZIP")) + " " + FCConvert.ToString(rsMort.Get_Fields_String("ZIP4"));
										}
										else
										{
											strCMFAddr2 = FCConvert.ToString(rsMort.Get_Fields_String("CITY")) + ", " + FCConvert.ToString(rsMort.Get_Fields_String("STATE")) + " " + FCConvert.ToString(rsMort.Get_Fields_String("ZIP"));
										}
									}
									else
									{
										if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
										{
											strCMFAddr1 = FCConvert.ToString(rsMort.Get_Fields_String("CITY")) + ", " + FCConvert.ToString(rsMort.Get_Fields_String("STATE")) + " " + FCConvert.ToString(rsMort.Get_Fields_String("ZIP")) + " " + FCConvert.ToString(rsMort.Get_Fields_String("ZIP4"));
										}
										else
										{
											strCMFAddr1 = FCConvert.ToString(rsMort.Get_Fields_String("CITY")) + ", " + FCConvert.ToString(rsMort.Get_Fields_String("STATE")) + " " + FCConvert.ToString(rsMort.Get_Fields_String("ZIP"));
										}
									}
								}
							}
							// MAL@20080514: Evaluate string length before adding to address bar
							// Tracker Reference: 13621
							strRSName1 = FCConvert.ToString(rsRE.Get_Fields("DeedName1"));
							strRSName2 = FCConvert.ToString(rsRE.Get_Fields("DeedName2"));
							if (Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1Address1"))) == "" && Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1Address2"))) == "" && Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1City"))) == "")
							{
								strRSAddr1 = FCConvert.ToString(rsData.Get_Fields_String("Address1"));
								strRSAddr2 = FCConvert.ToString(rsData.Get_Fields_String("Address2"));
								strRSAddr3 = FCConvert.ToString(rsData.Get_Fields_String("MailingAddress3"));
                                strRSAddr4 = FCConvert.ToString(rsData.Get_Fields_String("Address3"));
							}
							else if (Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1Address1"))) != "")
							{
								strRSAddr1 = FCConvert.ToString(rsRE.Get_Fields("Own1Address1"));
								strRSAddr2 = FCConvert.ToString(rsRE.Get_Fields("Own1Address2"));
                                strRSAddr3 = FCConvert.ToString(rsRE.Get_Fields_String("Own1Address3"));
								strRSAddr4 = FCConvert.ToString(rsRE.Get_Fields("Own1City")) + ", " + FCConvert.ToString(rsRE.Get_Fields("Own1State")) + " " + FCConvert.ToString(rsRE.Get_Fields("Own1Zip"));
							}
							else
							{
								strRSAddr1 = FCConvert.ToString(rsRE.Get_Fields("Own1Address2"));
                                strRSAddr2 = FCConvert.ToString(rsRE.Get_Fields("Own2Address3"));
								strRSAddr3 = FCConvert.ToString(rsRE.Get_Fields("Own1City")) + ", " + FCConvert.ToString(rsRE.Get_Fields("Own1State")) + " " + FCConvert.ToString(rsRE.Get_Fields("Own1Zip"));
								strRSAddr4 = "";
							}
                            if (Strings.Trim(strRSName1).Length > 38)
                            {
                                if (Strings.Trim(strRSName2) == "")
                                {
                                    strRSName2 = Strings.Trim(Strings.Mid(strRSName1, 39, strRSName1.Length - 38));
                                    strRSName1 = Strings.Trim(Strings.Left(strRSName1, 38));
                                }
                                else
                                {
                                    strRSName1 = Strings.Trim(Strings.Left(strRSName1, 38));
                                }
                            }

                            // Remove blank lines
                            if (Strings.Trim(strRSAddr3) == "")
                            {
                                strRSAddr3 = strRSAddr4;
                                strRSAddr4 = "";
                            }
                            if (Strings.Trim(strRSAddr2) == "")
                            {
                                strRSAddr2 = strRSAddr3;
                                strRSAddr3 = strRSAddr4;
                                strRSAddr4 = "";
                            }
                            if (Strings.Trim(strRSAddr1) == "")
                            {
                                strRSAddr1 = strRSAddr2;
                                strRSAddr2 = strRSAddr3;
                                strRSAddr3 = strRSAddr4;
                                strRSAddr4 = "";
                            }
                            if (Strings.Trim(strRSName2) == "")
							{
								// Move all address lines up one
								strRSName2 = strRSAddr1;
								strRSAddr1 = strRSAddr2;
								strRSAddr2 = strRSAddr3;
								strRSAddr3 = strRSAddr4;
                                strRSAddr4 = "";
							}
							else
							{
								// Do Nothing - Everything Stays in the Same Spot
							}
                            // Evaluate Line Lengths
                            if (Strings.Trim(strRSName2).Length > 38)
                            {
                                if (strRSAddr1 == "" || strRSAddr2 == "" || strRSAddr3 == "" || strRSAddr4 == "")
                                {
                                    strRSAddr4 = strRSAddr3; // There's an extra line - use it
                                    strRSAddr3 = strRSAddr2;
                                    strRSAddr2 = strRSAddr1;
                                    strRSAddr1 = fecherFoundation.Strings.Trim(Strings.Mid(strRSName2, 39, strRSName2.Length - 38));
                                    strRSName2 = fecherFoundation.Strings.Trim(Strings.Left(strRSName2, 38));
                                }
                                else
                                { // No extra line - trim the address line
                                    strRSName2 = fecherFoundation.Strings.Trim(Strings.Left(strRSName2, 38));
                                }
                            }
                            if (fecherFoundation.Strings.Trim(strRSAddr1).Length > 38)
                            {
                                if (strRSAddr2 == "" || strRSAddr3 == "" || strRSAddr4 == "")
                                {
                                    strRSAddr4 = strRSAddr3; // There's an extra line - use it
                                    strRSAddr3 = strRSAddr2;
                                    strRSAddr2 = fecherFoundation.Strings.Trim(Strings.Mid(strRSAddr1, 39, strRSAddr1.Length - 38));
                                    strRSAddr1 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr1, 38));
                                }
                                else
                                { // No extra line - trim the address line
                                    strRSAddr1 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr1, 38));
                                }
                            }

                            if (Strings.Trim(strRSAddr2).Length > 38)
                            {
                                if (strRSAddr3 == "" || strRSAddr4 == "")
                                {
                                    strRSAddr4 = strRSAddr3; // There's an extra line - use it
                                    strRSAddr3 = Strings.Trim(Strings.Mid(strRSAddr2, 39, strRSAddr2.Length - 38));
                                    strRSAddr2 = Strings.Trim(Strings.Left(strRSAddr2, 38));
                                }
                                else
                                { // No extra line - trim the address line
                                    strRSAddr2 = Strings.Trim(Strings.Left(strRSAddr2, 38));
                                }
                            }

                            if (Strings.Trim(strRSAddr3).Length > 38)
                            {
                                if (strRSAddr4 == "")
                                { // There's an extra line - use it
                                    strRSAddr4 = Strings.Trim(Strings.Mid(strRSAddr3, 39, strRSAddr3.Length - 38));
                                    strRSAddr3 = Strings.Trim(Strings.Left(strRSAddr3, 38));
                                }
                                else
                                { // No extra line - trim the address line
                                    strRSAddr3 = Strings.Trim(Strings.Left(strRSAddr3, 38));
                                }
                            }

                            if (Strings.Trim(strRSAddr4).Length > 38)
                            {
                                strRSAddr4 = Strings.Trim(Strings.Left(strRSAddr4, 38));
                            }

                            strAddressBar = "    " + modGlobalFunctions.PadStringWithSpaces(FCConvert.ToString(rsMort.Get_Fields_String("Name")), 36, false) + modGlobalFunctions.PadStringWithSpaces("Tax Payer:", 11, false) + strRSName1;
							strTemp1 = strRSName2;
							strTemp2 = strRSAddr1;
							strTemp3 = strRSAddr2;
							strTemp4 = strRSAddr3;
                            strTemp5 = strRSAddr4;
							
							strAddressBar += "\r\n" + "    " + modGlobalFunctions.PadStringWithSpaces(strCMFAddr1, 47, false) + strTemp1;
							strAddressBar += "\r\n" + "    " + modGlobalFunctions.PadStringWithSpaces(strCMFAddr2, 47, false) + strTemp2;
							if (Strings.Trim(strCMFAddr3) != "" || Strings.Trim(strTemp3) != "")
							{
								strAddressBar += "\r\n" + "    " + modGlobalFunctions.PadStringWithSpaces(strCMFAddr3, 47, false) + strTemp3;
							}
							if (Strings.Trim(strCMFAddr4) != "" || Strings.Trim(strTemp4) != "")
							{
								strAddressBar += "\r\n" + "    " + modGlobalFunctions.PadStringWithSpaces(strCMFAddr4, 47, false) + strTemp4;
							}
                            if (Strings.Trim(strTemp5) != "")
                            {
                                strAddressBar += "\r\n" + "    " + Strings.Space(47) + strTemp5;
                            }
							// set name and address bar for a mortgage holder
							modRhymalReporting.Statics.frfFreeReport[27].DatabaseFieldName = strAddressBar;
							rsMort.MoveNext();
						}
						else
						{
							strAddressBar = "    " + FCConvert.ToString(rsData.Get_Fields_String("Name1"));
							if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address1"))) != "")
							{
								strAddressBar += "\r\n" + "    " + FCConvert.ToString(rsData.Get_Fields_String("Address1"));
							}
							strAddressBar += "\r\n" + "    " + GetVariableValue_2("MUNI") + ", " + GetVariableValue_2("STATE") + " " + GetVariableValue_2("ZIP") + " " + GetVariableValue_2("ZIP4");
							// set name and address bar for a mortgage holder
							modRhymalReporting.Statics.frfFreeReport[27].DatabaseFieldName = strAddressBar;
						}
					}
					else if (blnIsIPCopy)
					{
						if (blnPayIPCert)
						{
							// if the user is getting charged for certified mail, then save their info to print on Cert Mail Forms
							boolPrintCMF = true;
						}
						else
						{
							boolPrintCMF = false;
						}
						// address bar
						if (!rsIntParty.EndOfFile())
						{
							// info for CMFNumbers table
							strCMFName = FCConvert.ToString(rsIntParty.Get_Fields_String("Name"));
							// TODO Get_Fields: Field [AutoID] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [AutoID] not found!! (maybe it is an alias?)
							lngCMFMHNumber = FCConvert.ToInt32(rsIntParty.Get_Fields("ID"));
							lngCMFBillKey = FCConvert.ToInt32(rsData.Get_Fields_Int32("ID"));
							// kk05102016 trocls-82    rsData.Fields("BillKey")
							if (Strings.Trim(FCConvert.ToString(rsIntParty.Get_Fields_String("Address1"))) != "")
							{
								strCMFAddr1 = FCConvert.ToString(rsIntParty.Get_Fields_String("Address1"));
								if (Strings.Trim(FCConvert.ToString(rsIntParty.Get_Fields_String("Address2"))) != "")
								{
									strCMFAddr2 = FCConvert.ToString(rsIntParty.Get_Fields_String("Address2"));
									if (Strings.Trim(FCConvert.ToString(rsIntParty.Get_Fields_String("Zip4"))) != "")
									{
										strCMFAddr3 = FCConvert.ToString(rsIntParty.Get_Fields_String("City")) + ", " + FCConvert.ToString(rsIntParty.Get_Fields_String("State")) + " " + FCConvert.ToString(rsIntParty.Get_Fields_String("Zip")) + " " + FCConvert.ToString(rsIntParty.Get_Fields_String("Zip4"));
									}
									else
									{
										strCMFAddr3 = FCConvert.ToString(rsIntParty.Get_Fields_String("City")) + ", " + FCConvert.ToString(rsIntParty.Get_Fields_String("State")) + " " + FCConvert.ToString(rsIntParty.Get_Fields_String("Zip"));
									}
								}
								else
								{
									if (Strings.Trim(FCConvert.ToString(rsIntParty.Get_Fields_String("Zip4"))) != "")
									{
										strCMFAddr2 = FCConvert.ToString(rsIntParty.Get_Fields_String("CITY")) + ", " + FCConvert.ToString(rsIntParty.Get_Fields_String("STATE")) + " " + FCConvert.ToString(rsIntParty.Get_Fields_String("ZIP")) + " " + FCConvert.ToString(rsIntParty.Get_Fields_String("ZIP4"));
									}
									else
									{
										strCMFAddr2 = FCConvert.ToString(rsIntParty.Get_Fields_String("CITY")) + ", " + FCConvert.ToString(rsIntParty.Get_Fields_String("STATE")) + " " + FCConvert.ToString(rsIntParty.Get_Fields_String("ZIP"));
									}
								}
							}
							else
							{
								if (Strings.Trim(FCConvert.ToString(rsIntParty.Get_Fields_String("Address2"))) != "")
								{
									strCMFAddr1 = FCConvert.ToString(rsIntParty.Get_Fields_String("Address2"));
									if (Strings.Trim(FCConvert.ToString(rsIntParty.Get_Fields_String("Zip4"))) != "")
									{
										strCMFAddr2 = FCConvert.ToString(rsIntParty.Get_Fields_String("City")) + ", " + FCConvert.ToString(rsIntParty.Get_Fields_String("State")) + " " + FCConvert.ToString(rsIntParty.Get_Fields_String("Zip")) + " " + FCConvert.ToString(rsIntParty.Get_Fields_String("Zip4"));
									}
									else
									{
										strCMFAddr2 = FCConvert.ToString(rsIntParty.Get_Fields_String("City")) + ", " + FCConvert.ToString(rsIntParty.Get_Fields_String("State")) + " " + FCConvert.ToString(rsIntParty.Get_Fields_String("Zip"));
									}
								}
								else
								{
									if (Strings.Trim(FCConvert.ToString(rsIntParty.Get_Fields_String("Zip4"))) != "")
									{
										strCMFAddr1 = FCConvert.ToString(rsIntParty.Get_Fields_String("CITY")) + ", " + FCConvert.ToString(rsIntParty.Get_Fields_String("STATE")) + " " + FCConvert.ToString(rsIntParty.Get_Fields_String("ZIP")) + " " + FCConvert.ToString(rsIntParty.Get_Fields_String("ZIP4"));
									}
									else
									{
										strCMFAddr1 = FCConvert.ToString(rsIntParty.Get_Fields_String("CITY")) + ", " + FCConvert.ToString(rsIntParty.Get_Fields_String("STATE")) + " " + FCConvert.ToString(rsIntParty.Get_Fields_String("ZIP"));
									}
								}
							}
							// MAL@20080514: Evaluate string length before adding to address bar
							// Tracker Reference: 13621
							strRSName1 = FCConvert.ToString(rsRE.Get_Fields("DeedName1"));
							strRSName2 = FCConvert.ToString(rsRE.Get_Fields("DeedName2"));

							if (Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1Address1"))) == "" && Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1Address2"))) == "" && Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1City"))) == "")
							{
								strRSAddr1 = FCConvert.ToString(rsData.Get_Fields_String("Address1"));
								strRSAddr2 = FCConvert.ToString(rsData.Get_Fields_String("Address2"));
								strRSAddr3 = FCConvert.ToString(rsData.Get_Fields_String("Address3"));
							}
								else if (Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1Address1"))) != "")
							{
								strRSAddr1 = FCConvert.ToString(rsRE.Get_Fields("Own1Address1"));
								strRSAddr2 = FCConvert.ToString(rsRE.Get_Fields("Own1Address2"));
								strRSAddr3 = FCConvert.ToString(rsRE.Get_Fields("Own1City")) + ", " + FCConvert.ToString(rsRE.Get_Fields("Own1State")) + " " + FCConvert.ToString(rsRE.Get_Fields("Own1Zip"));
							}
							else
							{
								strRSAddr1 = FCConvert.ToString(rsRE.Get_Fields("Own1Address2"));
								strRSAddr2 = FCConvert.ToString(rsRE.Get_Fields("Own1City")) + ", " + FCConvert.ToString(rsRE.Get_Fields("Own1State")) + " " + FCConvert.ToString(rsRE.Get_Fields("Own1Zip"));
								strRSAddr3 = "";
							}
                            if (Strings.Trim(strRSName1).Length > 38)
                            {
                                if (Strings.Trim(strRSName2) == "")
                                {
                                    strRSName2 = Strings.Trim(Strings.Mid(strRSName1, 39, strRSName1.Length - 38));
                                    strRSName1 = Strings.Trim(Strings.Left(strRSName1, 38));
                                }
                                else
                                {
                                    strRSName1 = Strings.Trim(Strings.Left(strRSName1, 38));
                                }
                            }
                            // Remove blank lines
                            if (Strings.Trim(strRSAddr3) == "")
                            {
                                strRSAddr3 = strRSAddr4;
                                strRSAddr4 = "";
                            }
                            if (Strings.Trim(strRSAddr2) == "")
                            {
                                strRSAddr2 = strRSAddr3;
                                strRSAddr3 = strRSAddr4;
                                strRSAddr4 = "";
                            }
                            if (Strings.Trim(strRSAddr1) == "")
                            {
                                strRSAddr1 = strRSAddr2;
                                strRSAddr2 = strRSAddr3;
                                strRSAddr3 = strRSAddr4;
                                strRSAddr4 = "";
                            }
                            if (Strings.Trim(strRSName2) == "")
                            {
                                strRSName2 = strRSAddr1;
                                strRSAddr1 = strRSAddr2;
                                strRSAddr2 = strRSAddr3;
                                strRSAddr3 = strRSAddr4;
                                strRSAddr4 = "";
                            }
                            // Evaluate Line Lengths
                            if (fecherFoundation.Strings.Trim(strRSName2).Length > 38)
                            {
                                if (strRSAddr1 == "" || strRSAddr2 == "" || strRSAddr3 == "" || strRSAddr4 == "")
                                {
                                    strRSAddr4 = strRSAddr3; // There's an extra line - use it
                                    strRSAddr3 = strRSAddr2;
                                    strRSAddr2 = strRSAddr1;
                                    strRSAddr1 = fecherFoundation.Strings.Trim(Strings.Mid(strRSName2, 39, strRSName2.Length - 38));
                                    strRSName2 = fecherFoundation.Strings.Trim(Strings.Left(strRSName2, 38));
                                }
                                else
                                { // No extra line - trim the address line
                                    strRSName2 = fecherFoundation.Strings.Trim(Strings.Left(strRSName2, 38));
                                }
                            }

                            if (fecherFoundation.Strings.Trim(strRSAddr1).Length > 38)
                            {
                                if (strRSAddr2 == "" || strRSAddr3 == "" || strRSAddr4 == "")
                                {
                                    strRSAddr4 = strRSAddr3; // There's an extra line - use it
                                    strRSAddr3 = strRSAddr2;
                                    strRSAddr2 = fecherFoundation.Strings.Trim(Strings.Mid(strRSAddr1, 39, strRSAddr1.Length - 38));
                                    strRSAddr1 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr1, 38));
                                }
                                else
                                { // No extra line - trim the address line
                                    strRSAddr1 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr1, 38));
                                }
                            }

                            if (fecherFoundation.Strings.Trim(strRSAddr2).Length > 38)
                            {
                                if (strRSAddr3 == "" || strRSAddr4 == "")
                                {
                                    strRSAddr4 = strRSAddr3; // There's an extra line - use it
                                    strRSAddr3 = fecherFoundation.Strings.Trim(Strings.Mid(strRSAddr2, 39, strRSAddr2.Length - 38));
                                    strRSAddr2 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr2, 38));
                                }
                                else
                                { // No extra line - trim the address line
                                    strRSAddr2 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr2, 38));
                                }
                            }

                            if (fecherFoundation.Strings.Trim(strRSAddr3).Length > 38)
                            {
                                if (strRSAddr4 == "")
                                { // There's an extra line - use it
                                    strRSAddr4 = fecherFoundation.Strings.Trim(Strings.Mid(strRSAddr3, 39, strRSAddr3.Length - 38));
                                    strRSAddr3 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr3, 38));
                                }
                                else
                                { // No extra line - trim the address line
                                    strRSAddr3 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr3, 38));
                                }
                            }

                            if (fecherFoundation.Strings.Trim(strRSAddr4).Length > 38)
                            {
                                strRSAddr4 = fecherFoundation.Strings.Trim(Strings.Left(strRSAddr4, 38));
                            }

                            strAddressBar = "    " + FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces(rsIntParty.Get_Fields("Name"), 36, false)) + FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces("Tax Payer:", 11, false)) + strRSName1;

                            strTemp1 = strRSName2;
                            strTemp2 = strRSAddr1;
                            strTemp3 = strRSAddr2;
                            strTemp4 = strRSAddr3;
                            strTemp5 = strRSAddr4;
                            
                            strAddressBar += "\r\n" + "    " + modGlobalFunctions.PadStringWithSpaces(strCMFAddr1, 47, false) + strTemp1;
							strAddressBar += "\r\n" + "    " + modGlobalFunctions.PadStringWithSpaces(strCMFAddr2, 47, false) + strTemp2;
							if (Strings.Trim(strCMFAddr3) != "" || Strings.Trim(strTemp3) != "")
							{
								strAddressBar += "\r\n" + "    " + modGlobalFunctions.PadStringWithSpaces(strCMFAddr3, 47, false) + strTemp3;
							}
							if (Strings.Trim(strCMFAddr4) != "" || Strings.Trim(strTemp4) != "")
							{
								strAddressBar += "\r\n" + "    " + modGlobalFunctions.PadStringWithSpaces(strCMFAddr4, 47, false) + strTemp4;
							}
                            if (fecherFoundation.Strings.Trim(strTemp5) != "")
                            {
                                strAddressBar += "\r\n" + "    " + Strings.Space(47) + strTemp5;
                            }
                            // set name and address bar for a mortgage holder
                            modRhymalReporting.Statics.frfFreeReport[27].DatabaseFieldName = strAddressBar;
							rsIntParty.MoveNext();
						}
						else
						{
							strAddressBar = "    " + FCConvert.ToString(rsData.Get_Fields_String("Name1"));
							if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address1"))) != "")
							{
								strAddressBar += "\r\n" + "    " + FCConvert.ToString(rsData.Get_Fields_String("Address1"));
							}
							strAddressBar += "\r\n" + "    " + GetVariableValue_2("MUNI") + ", " + GetVariableValue_2("STATE") + " " + GetVariableValue_2("ZIP") + " " + GetVariableValue_2("ZIP4");
							// set name and address bar for a mortgage holder
							modRhymalReporting.Statics.frfFreeReport[27].DatabaseFieldName = strAddressBar;
						}
					}
					else if (!blnIsMortCopy && !blnIsIPCopy && boolNewOwner)
					{
						// this is the last copy to be made, it is for the new owner
						// boolNewOwner = False                            'this will set it off for the next account
						if (intNewOwnerChoice > 0)
						{
							// if the user is getting charged for certified mail, then save their info to print on Cert Mail Forms
							boolPrintCMF = true;
						}
						else
						{
							boolPrintCMF = false;
						}
						// info for CMFNumbers table

						strCMFName = FCConvert.ToString(rsRE.Get_Fields("DeedName1"));
						lngCMFMHNumber = 0;
						// boolNewOwner = True
						// lngCMFBillKey = 0
						lngCMFBillKey = FCConvert.ToInt32(rsData.Get_Fields_Int32("ID"));
						// kk05102016 trocls-82    rsData.Fields("BillKey")
						// this is for a copy for the new owner
						// If boolAddressCO Then   'this is the owner at time of billing C/O the new owner and the new owner address
						// TODO Get_Fields: Field [DeedName1] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [DeedName1] not found!! (maybe it is an alias?)
						strAddressBar = "    " + FCConvert.ToString(rsRE.Get_Fields("DeedName1"));
						// name at time of billing
						// strAddressBar = strAddressBar & vbCrLf & "    C/O " & rsRE.Fields("RSName")   'name on the current RE account
						// if the row needs to be added, then do it
						// TODO Get_Fields: Field [DeedName2] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [DeedName2] not found!! (maybe it is an alias?)
						if (Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("DeedName2"))) != "")
						{
							// TODO Get_Fields: Field [DeedName2] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [DeedName2] not found!! (maybe it is an alias?)
							strAddressBar += "\r\n" + "    " + FCConvert.ToString(rsRE.Get_Fields("DeedName2"));
						}
						if (Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1Address1"))) != "")
						{
							strAddressBar += "\r\n" + "    " + FCConvert.ToString(rsRE.Get_Fields("Own1Address1"));
						}
						if (Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1Address2"))) != "")
						{
							strAddressBar += "\r\n" + "    " + FCConvert.ToString(rsRE.Get_Fields("Own1Address2"));
						}
                        if (Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1Address3"))) != "")
                        { // kk05082017 trocls-109
                            strAddressBar += "\r\n" + "    " + rsRE.Get_Fields("Own1Address3");
                        }
                        if (Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1City"))) != "")
						{
							// TODO Get_Fields: Field [Own1City] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [Own1State] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [Own1Zip] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [Own1City] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [Own1State] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [Own1Zip] not found!! (maybe it is an alias?)
							strAddressBar += "\r\n" + "    " + FCConvert.ToString(rsRE.Get_Fields("Own1City")) + ", " + FCConvert.ToString(rsRE.Get_Fields("Own1State")) + " " + FCConvert.ToString(rsRE.Get_Fields("Own1Zip"));
						}
                        // this is information for the Certified Mailing List
                        strCMFAddr1 = FCConvert.ToString(rsRE.Get_Fields("Own1Address1"));
                        strCMFAddr2 = FCConvert.ToString(rsRE.Get_Fields("Own1Address2"));
                        strCMFAddr3 = FCConvert.ToString(rsRE.Get_Fields("Own1Address3")); // kk05082017 trocls-109
                        strCMFAddr4 = rsRE.Get_Fields("Own1City") + ", " + rsRE.Get_Fields("Own1State") + " " + rsRE.Get_Fields("Own1Zip");
                        // Else        'this is the owner name at the time of billing and the address at the time of billing
                        // 
                        // strAddressBar = "    " & rsData.Fields("Name1")
                        // 
                        // if the row needs to be added, then do it
                        // If Trim(rsData.Fields("Address1")) <> "" Then
                        // strAddressBar = strAddressBar & vbCrLf & "    " & rsData.Fields("Address1")
                        // End If
                        // 
                        // If Trim(rsData.Fields("Address2")) <> "" Then
                        // strAddressBar = strAddressBar & vbCrLf & "    " & rsData.Fields("Address2")
                        // End If
                        // 
                        // If Trim(rsData.Fields("Address3")) <> "" Then
                        // strAddressBar = strAddressBar & vbCrLf & "    " & rsData.Fields("Address3")
                        // End If
                        // 
                        // this is information for the Certified Mailing List
                        // strCMFAddr1 = rsData.Fields("Address1")
                        // strCMFAddr2 = rsData.Fields("Address2")
                        // strCMFAddr3 = rsData.Fields("Address3")
                        // End If
                        // set name and address bar for a New Owner
                        modRhymalReporting.Statics.frfFreeReport[27].DatabaseFieldName = strAddressBar;
						rsRE.MoveNext();
					}
					else
					{
						strAddressBar = "    " + FCConvert.ToString(rsData.Get_Fields_String("Name1"));
						if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address1"))) != "")
						{
							strAddressBar += "\r\n" + "    " + FCConvert.ToString(rsData.Get_Fields_String("Address1"));
						}
						strAddressBar += "\r\n" + "    " + GetVariableValue_2("MUNI") + ", " + GetVariableValue_2("STATE") + " " + GetVariableValue_2("ZIP") + " " + GetVariableValue_2("ZIP4");
						// MAL@20080429: Added because these records were having blank addresses in the CMF table when I changed the way the addresses were being populated
						// Tracker Reference: 13412,13491
						// this is information for the Certified Mailing List
						strCMFAddr1 = FCConvert.ToString(rsData.Get_Fields_String("Address1"));
						strCMFAddr2 = FCConvert.ToString(rsData.Get_Fields_String("Address2"));
                        strCMFAddr3 = FCConvert.ToString(rsData.Get_Fields("MailingAddress3")); // kk05082017 trocls-109
                        strCMFAddr4 = GetVariableValue_2("MUNI") + ", " + GetVariableValue_2("STATE") + " " + GetVariableValue_2("ZIP") + " " + GetVariableValue_2("ZIP4");// set name and address bar for a mortgage holder
                        modRhymalReporting.Statics.frfFreeReport[27].DatabaseFieldName = strAddressBar;
					}
					// Old Way : 1/2/2008: Added Interested Party functions ; Delete 7/2/2008
					// If boolPayMortCert Then     'if the user is getting charged for certified mail, then save their info to print on Cert Mail Forms
					// boolPrintCMF = True
					// Else
					// boolPrintCMF = False
					// End If
					// 
					// address bar
					// If rsMort.EndOfFile <> True Then
					// info for CMFNumbers table
					// strCMFName = rsMort.Fields("Name")
					// lngCMFMHNumber = rsMort.Fields("MortgageAssociation.MortgageHolderID")
					// boolNewOwner = False
					// lngCMFBillKey = 0
					// lngCMFBillKey = rsData.Fields("BillKey")
					// If Trim(rsMort.Fields("Address1")) <> "" Then
					// strCMFAddr1 = rsMort.Fields("Address1")
					// If Trim(rsMort.Fields("Address2")) <> "" Then
					// strCMFAddr2 = rsMort.Fields("Address2")
					// If Trim(rsMort.Fields("Address3")) <> "" Then
					// strCMFAddr3 = rsMort.Fields("Address3")
					// 
					// If Trim(rsMort.Fields("Zip4")) <> "" Then
					// strCMFAddr4 = rsMort.Fields("CITY") & ", " & rsMort.Fields("STATE") & " " & rsMort.Fields("ZIP") & "-" & rsMort.Fields("ZIP4")
					// Else
					// strCMFAddr4 = rsMort.Fields("CITY") & ", " & rsMort.Fields("STATE") & " " & rsMort.Fields("ZIP")
					// End If
					// Else
					// If Trim(rsMort.Fields("Zip4")) <> "" Then
					// strCMFAddr3 = rsMort.Fields("CITY") & ", " & rsMort.Fields("STATE") & " " & rsMort.Fields("ZIP") & "-" & rsMort.Fields("ZIP4")
					// Else
					// strCMFAddr3 = rsMort.Fields("CITY") & ", " & rsMort.Fields("STATE") & " " & rsMort.Fields("ZIP")
					// End If
					// End If
					// Else
					// If Trim(rsMort.Fields("Address3")) <> "" Then
					// strCMFAddr2 = rsMort.Fields("Address3")
					// 
					// If Trim(rsMort.Fields("Zip4")) <> "" Then
					// strCMFAddr3 = rsMort.Fields("CITY") & ", " & rsMort.Fields("STATE") & " " & rsMort.Fields("ZIP") & "-" & rsMort.Fields("ZIP4")
					// Else
					// strCMFAddr3 = rsMort.Fields("CITY") & ", " & rsMort.Fields("STATE") & " " & rsMort.Fields("ZIP")
					// End If
					// Else
					// If Trim(rsMort.Fields("Zip4")) <> "" Then
					// strCMFAddr2 = rsMort.Fields("CITY") & ", " & rsMort.Fields("STATE") & " " & rsMort.Fields("ZIP") & "-" & rsMort.Fields("ZIP4")
					// Else
					// strCMFAddr2 = rsMort.Fields("CITY") & ", " & rsMort.Fields("STATE") & " " & rsMort.Fields("ZIP")
					// End If
					// End If
					// End If
					// Else
					// If Trim(rsMort.Fields("Address2")) <> "" Then
					// strCMFAddr1 = rsMort.Fields("Address2")
					// If Trim(rsMort.Fields("Address3")) <> "" Then
					// strCMFAddr2 = rsMort.Fields("Address3")
					// 
					// If Trim(rsMort.Fields("Zip4")) <> "" Then
					// strCMFAddr3 = rsMort.Fields("CITY") & ", " & rsMort.Fields("STATE") & " " & rsMort.Fields("ZIP") & "-" & rsMort.Fields("ZIP4")
					// Else
					// strCMFAddr3 = rsMort.Fields("CITY") & ", " & rsMort.Fields("STATE") & " " & rsMort.Fields("ZIP")
					// End If
					// Else
					// If Trim(rsMort.Fields("Zip4")) <> "" Then
					// strCMFAddr2 = rsMort.Fields("CITY") & ", " & rsMort.Fields("STATE") & " " & rsMort.Fields("ZIP") & "-" & rsMort.Fields("ZIP4")
					// Else
					// strCMFAddr2 = rsMort.Fields("CITY") & ", " & rsMort.Fields("STATE") & " " & rsMort.Fields("ZIP")
					// End If
					// End If
					// Else
					// If Trim(rsMort.Fields("Address3")) <> "" Then
					// strCMFAddr1 = rsMort.Fields("Address3")
					// 
					// If Trim(rsMort.Fields("Zip4")) <> "" Then
					// strCMFAddr2 = rsMort.Fields("CITY") & ", " & rsMort.Fields("STATE") & " " & rsMort.Fields("ZIP") & "-" & rsMort.Fields("ZIP4")
					// Else
					// strCMFAddr2 = rsMort.Fields("CITY") & ", " & rsMort.Fields("STATE") & " " & rsMort.Fields("ZIP")
					// End If
					// Else
					// If Trim(rsMort.Fields("Zip4")) <> "" Then
					// strCMFAddr1 = rsMort.Fields("CITY") & ", " & rsMort.Fields("STATE") & " " & rsMort.Fields("ZIP") & "-" & rsMort.Fields("ZIP4")
					// Else
					// strCMFAddr1 = rsMort.Fields("CITY") & ", " & rsMort.Fields("STATE") & " " & rsMort.Fields("ZIP")
					// End If
					// End If
					// End If
					// End If
					// 
					// strAddressBar = "    " & PadStringWithSpaces(rsMort.Fields("Name"), 36, False) & PadStringWithSpaces("Tax Payer:", 11, False) & rsData.Fields("Name1")
					// strAddressBar = "    " & PadStringWithSpaces(rsMort.Fields("Name"), 36, False) & PadStringWithSpaces("Tax Payer:", 11, False) & rsRE.Fields("RSName")
					// 
					// 
					// this gets the user info
					// If Trim(rsData.Fields("Address1")) = "" And Trim(rsData.Fields("Address2")) = "" And Trim(rsData.Fields("Address3")) = "" Then
					// strTemp = rsRE.Fields("RSAddr1")
					// strTemp2 = rsRE.Fields("RSAddr2")
					// strTemp3 = rsRE.Fields("RSAddr3") & ", " & rsRE.Fields("RSState") & " " & rsRE.Fields("RSZip")
					// ElseIf Trim(rsData.Fields("Address1")) <> "" Then
					// strTemp = rsData.Fields("Address1")
					// strTemp2 = rsData.Fields("Address2")
					// strTemp3 = rsData.Fields("Address3")
					// Else
					// strTemp = rsData.Fields("Address2")
					// strTemp2 = rsData.Fields("Address3")
					// strTemp3 = ""
					// End If
					// If Trim(rsRE.Fields("RSAddr1")) = "" And Trim(rsRE.Fields("RSAddr2")) = "" And Trim(rsRE.Fields("RSAddr3")) = "" Then
					// strTemp = rsData.Fields("Address1")
					// strTemp2 = rsData.Fields("Address2")
					// strTemp3 = rsData.Fields("Address3")
					// ElseIf Trim(rsRE.Fields("RSAddr1")) <> "" Then
					// strTemp = rsRE.Fields("RSAddr1")
					// strTemp2 = rsRE.Fields("RSAddr2")
					// strTemp3 = rsRE.Fields("RSAddr3") & ", " & rsRE.Fields("RSState") & " " & rsRE.Fields("RSZip")
					// Else
					// strTemp = rsRE.Fields("RSAddr2")
					// strTemp2 = rsRE.Fields("RSAddr3") & ", " & rsRE.Fields("RSState") & " " & rsRE.Fields("RSZip")
					// strTemp3 = ""
					// End If
					// 
					// If Trim(rsData.Fields("Name2")) <> "" Then
					// move all of the fields down a level
					// strTemp4 = strTemp3
					// strTemp3 = strTemp2
					// strTemp2 = strTemp
					// strTemp = Trim(rsData.Fields("Name2"))
					// End If
					// If Trim(rsRE.Fields("RSSecOwner")) <> "" Then    'Trim(rsRE.Fields("RSSecOwner")) <> ""
					// move all of the fields down a level
					// strTemp4 = strTemp3
					// strTemp3 = strTemp2
					// strTemp2 = strTemp
					// strTemp = Trim(rsRE.Fields("RSSecOwner"))
					// End If
					// If Trim(strTemp2) = "" Then
					// strTemp2 = strTemp3
					// strTemp3 = strTemp4
					// strTemp4 = ""
					// End If
					// 
					// If Trim(strTemp) = "" Then
					// strTemp = strTemp2
					// strTemp2 = strTemp3
					// strTemp3 = strTemp4
					// strTemp4 = ""
					// End If
					// 
					// strAddressBar = strAddressBar & vbCrLf & "    " & PadStringWithSpaces(strCMFAddr1, 47, False) & strTemp
					// strAddressBar = strAddressBar & vbCrLf & "    " & PadStringWithSpaces(strCMFAddr2, 47, False) & strTemp2
					// If Trim(strCMFAddr3) <> "" Or Trim(strTemp3) <> "" Then
					// strAddressBar = strAddressBar & vbCrLf & "    " & PadStringWithSpaces(strCMFAddr3, 47, False) & strTemp3
					// End If
					// If Trim(strCMFAddr4) <> "" Or Trim(strTemp4) <> "" Then
					// strAddressBar = strAddressBar & vbCrLf & "    " & PadStringWithSpaces(strCMFAddr4, 47, False) & strTemp4
					// End If
					// 
					// set name and address bar for a mortgage holder
					// frfFreeReport(27).DatabaseFieldName = strAddressBar
					// rsMort.MoveNext
					// 
					// ElseIf boolNewOwner Then                            'this is the last copy to be made, it is for the new owner
					// boolNewOwner = False                            'this will set it off for the next account
					// If intNewOwnerChoice > 0 Then     'if the user is getting charged for certified mail, then save their info to print on Cert Mail Forms
					// boolPrintCMF = True
					// Else
					// boolPrintCMF = False
					// End If
					// info for CMFNumbers table
					// strCMFName = rsRE.Fields("RSName")
					// lngCMFMHNumber = 0
					// boolNewOwner = True
					// lngCMFBillKey = 0
					// lngCMFBillKey = rsData.Fields("BillKey")
					// this is for a copy for the new owner
					// If boolAddressCO Then   'this is the owner at time of billing C/O the new owner and the new owner address
					// 
					// strAddressBar = "    " & rsRE.Fields("RSName")                     'name at time of billing
					// strAddressBar = strAddressBar & vbCrLf & "    C/O " & rsRE.Fields("RSName")   'name on the current RE account
					// 
					// if the row needs to be added, then do it
					// If Trim(rsRE.Fields("RSSecOwner")) <> "" Then
					// strAddressBar = strAddressBar & vbCrLf & "    " & rsRE.Fields("RSSecOwner")
					// End If
					// If Trim(rsRE.Fields("RSAddr1")) <> "" Then
					// strAddressBar = strAddressBar & vbCrLf & "    " & rsRE.Fields("RSAddr1")
					// End If
					// 
					// If Trim(rsRE.Fields("RSAddr2")) <> "" Then
					// strAddressBar = strAddressBar & vbCrLf & "    " & rsRE.Fields("RSAddr2")
					// End If
					// 
					// If Trim(rsRE.Fields("RSAddr3")) <> "" Then
					// strAddressBar = strAddressBar & vbCrLf & "    " & rsRE.Fields("RSAddr3") & ", " & rsRE.Fields("RSState") & " " & rsRE.Fields("RSZip")
					// End If
					// 
					// this is information for the Certified Mailing List
					// strCMFAddr1 = rsRE.Fields("RSAddr1")
					// strCMFAddr2 = rsRE.Fields("RSAddr2")
					// strCMFAddr3 = rsRE.Fields("RSAddr3") & ", " & rsRE.Fields("RSState") & " " & rsRE.Fields("RSZip")
					// 
					// Else        'this is the owner name at the time of billing and the address at the time of billing
					// 
					// strAddressBar = "    " & rsData.Fields("Name1")
					// 
					// if the row needs to be added, then do it
					// If Trim(rsData.Fields("Address1")) <> "" Then
					// strAddressBar = strAddressBar & vbCrLf & "    " & rsData.Fields("Address1")
					// End If
					// 
					// If Trim(rsData.Fields("Address2")) <> "" Then
					// strAddressBar = strAddressBar & vbCrLf & "    " & rsData.Fields("Address2")
					// End If
					// 
					// If Trim(rsData.Fields("Address3")) <> "" Then
					// strAddressBar = strAddressBar & vbCrLf & "    " & rsData.Fields("Address3")
					// End If
					// 
					// this is information for the Certified Mailing List
					// strCMFAddr1 = rsData.Fields("Address1")
					// strCMFAddr2 = rsData.Fields("Address2")
					// strCMFAddr3 = rsData.Fields("Address3")
					// End If
					// 
					// set name and address bar for a New Owner
					// frfFreeReport(27).DatabaseFieldName = strAddressBar
					// rsRE.MoveNext
					// Else
					// strAddressBar = "    " & rsData.Fields("Name1")
					// If Trim(rsData.Fields("Address1")) <> "" Then
					// strAddressBar = strAddressBar & vbCrLf & "    " & rsData.Fields("Address1")
					// End If
					// strAddressBar = strAddressBar & vbCrLf & "    " & GetVariableValue("MUNI") & ", " & GetVariableValue("STATE") & " " & GetVariableValue("ZIP")
					// 
					// set name and address bar for a mortgage holder
					// frfFreeReport(27).DatabaseFieldName = strAddressBar
					// End If
				}
				// this will add the information to the CMFNumber table so that it will be
				// easier to figure out which accounts need certified mail forms to be printed for them
				// MAL@20071022: Added support for new Process Type
				// Call Reference: 114824
				if (lngCMFMHNumber != 0)
				{
					if (blnIsMortCopy)
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsCMFNumbers.FindFirstRecord2("Account,Type,MortgageHolder,ProcessType", FCConvert.ToString(rsData.Get_Fields("Account")) + ",20," + FCConvert.ToString(lngCMFMHNumber) + ",30DN", ",");
						// kk08122014 trocls-49   ' "'30DN'"
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsCMFNumbers.FindFirstRecord2("Account,Type,InterestedPartyID,ProcessType", FCConvert.ToString(rsData.Get_Fields("Account")) + ",20," + FCConvert.ToString(lngCMFMHNumber) + ",30DN", ",");
						// kk08122014 trocls-49   ' "'30DN'"
					}
				}
				else
				{
					if (boolCopy && boolNewOwner)
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsCMFNumbers.FindFirstRecord2("Account,Type,NewOwner,ProcessType", FCConvert.ToString(rsData.Get_Fields("Account")) + ",20,1,30DN", ",");
						// kk08122014 trocls-49   ' rsData.Fields("Account") & ", 20, 1, '30DN'"
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsCMFNumbers.FindFirstRecord2("Account,Type,NewOwner,ProcessType", FCConvert.ToString(rsData.Get_Fields("Account")) + ",20,0,30DN", ",");
						// kk08122014 trocls-49   ' rsData.Fields("Account") & ", 20, 0, '30DN'"
					}
				}
				if (rsCMFNumbers.NoMatch)
				{
					rsCMFNumbers.AddNew();
				}
				else
				{
					rsCMFNumbers.Edit();
				}
				rsCMFNumbers.Set_Fields("Name", strCMFName);
				rsCMFNumbers.Set_Fields("Address1", strCMFAddr1);
				rsCMFNumbers.Set_Fields("Address2", strCMFAddr2);
				rsCMFNumbers.Set_Fields("Address3", strCMFAddr3);
				rsCMFNumbers.Set_Fields("Address4", strCMFAddr4);
				rsCMFNumbers.Set_Fields("Billkey", lngCMFBillKey);
				if (blnIsMortCopy)
				{
					rsCMFNumbers.Set_Fields("MortgageHolder", lngCMFMHNumber);
					if (lngCMFMHNumber != 0)
					{
						// mort holder
						// kgk 11-29-2011 trocl-840  rsCMFNumbers.Fields("LabelOnly") = Not boolPayMortCert
					}
				}
				else if (blnIsIPCopy)
				{
					rsCMFNumbers.Set_Fields("InterestedPartyID", lngCMFMHNumber);
					if (lngCMFMHNumber != 0)
					{
						// Interested Party
						// kgk 11-29-2011 trocl-840  rsCMFNumbers.Fields("LabelOnly") = Not blnPayIPCert
					}
				}
				else
				{
					rsCMFNumbers.Set_Fields("NewOwner", false);
					if (boolCopy && boolNewOwner)
					{
						// is this is a new owner copy
						// kgk 11-29-2011 trocl-840  rsCMFNumbers.Fields("LabelOnly") = Not boolPayNewOwner
						rsCMFNumbers.Set_Fields("NewOwner", true);
						boolNewOwner = false;
					}
					else
					{
						// kgk 11-29-2011 trocl-840  rsCMFNumbers.Fields("LabelOnly") = Not boolPayCert
						rsCMFNumbers.Set_Fields("NewOwner", false);
					}
				}
				lngCMFBillKey = 0;
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				rsCMFNumbers.Set_Fields("Account", rsData.Get_Fields("Account"));
				rsCMFNumbers.Set_Fields("Type", 20);
				rsCMFNumbers.Set_Fields("ProcessType", "30DN");
				// MAL@20071022
				rsCMFNumbers.Set_Fields("REPP", "RE");
				rsCMFNumbers.Update(true);
				rsRK.Dispose();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message, MsgBoxStyle.Critical, "Calculate Variable Error - " + FCConvert.ToString(rsData.Get_Fields_Int32("BillKey")));
			}
		}

		private int CalculateMortgageHolders()
		{
			int CalculateMortgageHolders = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will return the number of mortgage holders this account has
				// MAL@20080723: Change to take Receive Copies option into account
				// Tracker Reference: 13813
				// rsMort.OpenRecordset "SELECT * FROM MortgageAssociation INNER JOIN MortgageHolders ON MortgageAssociation.MortgageHolderID = MortgageHolders.MortgageHolderID WHERE Account = " & rsData.Fields("Account") & " AND Module = 'RE'", strGNDatabase
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				rsMort.OpenRecordset("SELECT * FROM MortgageAssociation INNER JOIN MortgageHolders ON MortgageAssociation.MortgageHolderID = MortgageHolders.ID WHERE Account = " + FCConvert.ToString(rsData.Get_Fields("Account")) + " AND Module = 'RE' AND ReceiveCopies = 1", "CentralData");
				if (rsMort.EndOfFile() != true)
				{
					CalculateMortgageHolders = rsMort.RecordCount();
				}
				else
				{
					CalculateMortgageHolders = 0;
				}
				return CalculateMortgageHolders;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				CalculateMortgageHolders = 0;
			}
			return CalculateMortgageHolders;
		}
		
		public void SaveLienStatus()
		{
			string strTemp;
			string strAcct = "";
			int lngCommaPosition = 0;
			if (Strings.Right(strAcctList, 1) == ",")
			{
				strAcctList = Strings.Left(strAcctList, strAcctList.Length - 1);
			}
			strTemp = strAcctList;
			// this statement will change the status of the accounts that have been process
			if (Strings.Trim(strAcctList) != "")
			{
				while (!(strTemp == ""))
				{
					lngCommaPosition = Strings.InStr(1, strTemp, ",");
					if (lngCommaPosition > 0)
					{
						strAcct = Strings.Left(strTemp, lngCommaPosition - 1);
					}
					else
					{
						strAcct = strTemp;
						strTemp = "";
					}
					strTemp = Strings.Right(strTemp, strTemp.Length - lngCommaPosition);
					if (Strings.Trim(strAcct) != "")
					{
						rsData.Execute("UPDATE BillingMaster SET LienProcessStatus = 1, LienStatusEligibility = 2 WHERE BillingYear/10 = " + FCConvert.ToString(intYear) + " AND Account = " + strAcct + " AND RateKey IN " + frmFreeReport.InstancePtr.strRK, modExtraModules.strCLDatabase);
					}
				}
                strAcctList = "";
            }
		}

		private void CalculateAbatementAmount_26(int lngBK, bool boolLien, int lngNumOfPeriods, ref double dblAbate1, ref double dblAbate2, ref double dblAbate3, ref double dblAbate4)
		{
			CalculateAbatementAmount(ref lngBK, ref boolLien, ref lngNumOfPeriods, ref dblAbate1, ref dblAbate2, ref dblAbate3, ref dblAbate4);
		}

		private void CalculateAbatementAmount(ref int lngBK, ref bool boolLien, ref int lngNumOfPeriods, ref double dblAbate1, ref double dblAbate2, ref double dblAbate3, ref double dblAbate4)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsP = new clsDRWrapper();
				double dblAdded/*unused?*/;
				if (boolLien)
				{
					rsP.OpenRecordset("SELECT * FROM PaymentRec WHERE Code = 'A' AND BillCode = 'L' AND BillKey = " + FCConvert.ToString(lngBK), modExtraModules.strCLDatabase);
					while (!rsP.EndOfFile())
					{
						// only one period, so put it all into per 1
						// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
						dblAbate1 += Conversion.Val(rsP.Get_Fields("Principal"));
						rsP.MoveNext();
					}
				}
				else
				{
					rsP.OpenRecordset("SELECT * FROM PaymentRec WHERE Code = 'A' AND BillCode = 'R' AND BillKey = " + FCConvert.ToString(lngBK), modExtraModules.strCLDatabase);
					while (!rsP.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						string VBtoVar = FCConvert.ToString(rsP.Get_Fields("Period"));
						if (VBtoVar == "A")
						{
							switch (lngNumOfPeriods)
							{
								case 1:
									{
										// only one period, so put it all into per 1
										// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
										dblAbate1 += Conversion.Val(rsP.Get_Fields("Principal"));
										break;
									}
								case 2:
									{
										// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
										dblAbate1 += modGlobal.Round(FCConvert.ToInt32(rsP.Get_Fields("Principal")) / 2, 2);
										// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
										dblAbate2 += modGlobal.Round(FCConvert.ToInt32(rsP.Get_Fields("Principal")) / 2, 2);
										break;
									}
								case 3:
									{
										// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
										dblAbate1 += modGlobal.Round(FCConvert.ToInt32(rsP.Get_Fields("Principal")) / 3, 2);
										// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
										dblAbate2 += modGlobal.Round(FCConvert.ToInt32(rsP.Get_Fields("Principal")) / 3, 2);
										// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
										dblAbate3 += modGlobal.Round(FCConvert.ToInt32(rsP.Get_Fields("Principal")) / 3, 2);
										break;
									}
								case 4:
									{
										// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
										dblAbate1 += modGlobal.Round(FCConvert.ToInt32(rsP.Get_Fields("Principal")) / 4, 2);
										// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
										dblAbate2 += modGlobal.Round(FCConvert.ToInt32(rsP.Get_Fields("Principal")) / 4, 2);
										// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
										dblAbate3 += modGlobal.Round(FCConvert.ToInt32(rsP.Get_Fields("Principal")) / 4, 2);
										// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
										dblAbate4 += modGlobal.Round(FCConvert.ToInt32(rsP.Get_Fields("Principal")) / 4, 2);
										break;
									}
							}
							//end switch
						}
						else if (VBtoVar == "1")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							dblAbate1 += Conversion.Val(rsP.Get_Fields("Principal"));
						}
						else if (VBtoVar == "2")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							dblAbate2 += Conversion.Val(rsP.Get_Fields("Principal"));
						}
						else if (VBtoVar == "3")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							dblAbate3 += Conversion.Val(rsP.Get_Fields("Principal"));
						}
						else if (VBtoVar == "4")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							dblAbate4 += Conversion.Val(rsP.Get_Fields("Principal"));
						}
						rsP.MoveNext();
					}
				}
				rsP.Dispose();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + "Error Calculating Abatement Amount");
			}
		}

		
	}
}
