﻿using System;
using System.IO;
using fecherFoundation;
using Wisej.Web;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using fecherFoundation.VisualBasicLayer;
using System.Runtime.InteropServices;
using Global;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptTaxServiceSummary : BaseSectionReport
	{

		bool boolEndReport;
		int lngCount;
		DateTime dtIntDate;

        public rptTaxServiceSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Tax Service Summary";
		}

		public static rptTaxServiceSummary InstancePtr
		{
			get
			{
				return (rptTaxServiceSummary)Sys.GetInstance(typeof(rptTaxServiceSummary));
			}
		}

		protected rptTaxServiceSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private struct TaxServiceYearSummary
		{
			public int Count;
			public double Balance;
			public double Interest;
		};

		TaxServiceYearSummary[] TSArray = new TaxServiceYearSummary[100 + 1];
		float[] lngRowArr = new float[8 + 1];
        private List<TaxServiceExtractRecord> reportData = new List<TaxServiceExtractRecord>();
        private int counter = 0;
        private bool firstRecord = true;

		public void Init(DateTime interestDate, IEnumerable<TaxServiceExtractRecord> reportData)
		{
			dtIntDate = interestDate;
            this.reportData = reportData.ToList();
            counter = 0;
            firstRecord = true;

			lngRowArr[1] = 0;
			lngRowArr[2] = 270 / 1440F;
			lngRowArr[3] = 540 / 1440F;
			lngRowArr[4] = 810 / 1440F;
			lngRowArr[5] = 1080 / 1440F;
			lngRowArr[6] = 1350 / 1440F;
			lngRowArr[7] = 1620 / 1440F;
			lngRowArr[8] = 1890 / 1440F;

            frmReportViewer.InstancePtr.Init(this);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
            if (firstRecord)
            {
                firstRecord = false;
            }
            else
            {
                counter++;
            }

            eArgs.EOF = counter >= reportData.Count();
        }

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			if (this.Document.Pages.Count > 0)
			{
				modGlobalFunctions.IncrementSavedReports("LastTSReport");
				this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastTSReport1.RDF"));
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
        {
         
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "t");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			lblReportType.Text = "Interest Date: " + dtIntDate.ToString("MM/dd/yyyy");
		}

        private void BindFields()
		{
			    var record = reportData[counter];

                fldAcct.Text = record.Account.ToString();
                fldName.Text = record.Name.Trim();
                fldLocation.Text = (record.LocationNumber + " " + record.LocationStreet).Trim();
                fldMapLot.Text = record.MapLot.Trim();
                fldAcres.Text = record.Acres.ToString();
                fldBook.Text = record.Book;
                fldPage.Text = record.Page;
                fldYear.Text = record.Year.ToString();
                fldPR.Text = record.Period.ToString();
                fldAcctInfo.Text = record.PaidFlag + @"/" + record.LienIndicator + @"/" + record.PriorYearDelinquent;
				ClearAmtFields();
				fldPer1.Text = "1";
                fldTaxAmt1.Text = record.Tax1.FormatAsCurrencyNoSymbol();
                fldTaxBal1.Text = record.Tax1Balance.FormatAsCurrencyNoSymbol();
                fldInterest1.Text = record.Tax1InterestDue.FormatAsCurrencyNoSymbol();
                fldTotalDue1.Text = (record.Tax1Balance + record.Tax1InterestDue).FormatAsCurrencyNoSymbol();
                lnDetailBottom.Y1 = lngRowArr[record.Period + 3];
                lnDetailBottom.Y2 = lnDetailBottom.Y1;
                Detail.Height = lngRowArr[record.Period + 4];
                if (record.Period > 1)
                {
				    fldPer2.Text = "2";
                    fldTaxAmt2.Text = record.Tax2.FormatAsCurrencyNoSymbol();
                    fldTaxBal2.Text = record.Tax2Balance.FormatAsCurrencyNoSymbol();
                    fldInterest2.Text = record.Tax2InterestDue.FormatAsCurrencyNoSymbol();
                    fldTotalDue2.Text = (record.Tax2Balance + record.Tax2InterestDue).FormatAsCurrencyNoSymbol();
				    fldPer2.Top = lngRowArr[4];
				    fldTaxAmt2.Top = lngRowArr[4];
				    fldTaxBal2.Top = lngRowArr[4];
				    fldInterest2.Top = lngRowArr[4];
				    fldTotalDue2.Top = lngRowArr[4];
				    fldPer2.Visible = true;
				    fldTaxAmt2.Visible = true;
				    fldTaxBal2.Visible = true;
				    fldInterest2.Visible = true;
				    fldTotalDue2.Visible = true;
					if (record.Period > 2)
					{
						fldPer3.Text = "3";
                        fldTaxAmt3.Text = record.Tax3.FormatAsCurrencyNoSymbol();
                        fldTaxBal3.Text = record.Tax3Balance.FormatAsCurrencyNoSymbol();
                        fldInterest3.Text = record.Tax3InterestDue.FormatAsCurrencyNoSymbol();
                        fldTotalDue3.Text = (record.Tax3Balance + record.Tax3InterestDue).FormatAsCurrencyNoSymbol();
						fldPer3.Top = lngRowArr[5];
						fldTaxAmt3.Top = lngRowArr[5];
						fldTaxBal3.Top = lngRowArr[5];
						fldInterest3.Top = lngRowArr[5];
						fldTotalDue3.Top = lngRowArr[5];
						fldPer3.Visible = true;
						fldTaxAmt3.Visible = true;
						fldTaxBal3.Visible = true;
						fldInterest3.Visible = true;
						fldTotalDue3.Visible = true;
						if (record.Period > 3)
						{
							fldPer4.Text = "4";
                            fldTaxAmt4.Text = record.Tax4.FormatAsCurrencyNoSymbol();
                            fldTaxBal4.Text = record.Tax4Balance.FormatAsCurrencyNoSymbol();
                            fldInterest4.Text = record.Tax4InterestDue.FormatAsCurrencyNoSymbol();
                            fldTotalDue4.Text =
                                (record.Tax4Balance + record.Tax4InterestDue).FormatAsCurrencyNoSymbol();
							fldPer4.Top = lngRowArr[6];
							fldTaxAmt4.Top = lngRowArr[6];
							fldTaxBal4.Top = lngRowArr[6];
							fldInterest4.Top = lngRowArr[6];
							fldTotalDue4.Top = lngRowArr[6];
							fldPer4.Visible = true;
							fldTaxAmt4.Visible = true;
							fldTaxBal4.Visible = true;
							fldInterest4.Visible = true;
							fldTotalDue4.Visible = true;
						}
						else
						{
							fldPer4.Top = lngRowArr[1];
							fldTaxAmt4.Top = lngRowArr[1];
							fldTaxBal4.Top = lngRowArr[1];
							fldInterest4.Top = lngRowArr[1];
							fldTotalDue4.Top = lngRowArr[1];
							fldPer4.Visible = false;
							fldTaxAmt4.Visible = false;
							fldTaxBal4.Visible = false;
							fldInterest4.Visible = false;
							fldTotalDue4.Visible = false;
						}
					}
					else
					{
						fldPer3.Top = lngRowArr[1];
						fldTaxAmt3.Top = lngRowArr[1];
						fldTaxBal3.Top = lngRowArr[1];
						fldInterest3.Top = lngRowArr[1];
						fldTotalDue3.Top = lngRowArr[1];
						fldPer3.Visible = false;
						fldTaxAmt3.Visible = false;
						fldTaxBal3.Visible = false;
						fldInterest3.Visible = false;
						fldTotalDue3.Visible = false;
						fldPer4.Top = lngRowArr[1];
						fldTaxAmt4.Top = lngRowArr[1];
						fldTaxBal4.Top = lngRowArr[1];
						fldInterest4.Top = lngRowArr[1];
						fldTotalDue4.Top = lngRowArr[1];
						fldPer4.Visible = false;
						fldTaxAmt4.Visible = false;
						fldTaxBal4.Visible = false;
						fldInterest4.Visible = false;
						fldTotalDue4.Visible = false;
					}
				}
				else
				{
					fldPer2.Top = lngRowArr[1];
					fldTaxAmt2.Top = lngRowArr[1];
					fldTaxBal2.Top = lngRowArr[1];
					fldInterest2.Top = lngRowArr[1];
					fldTotalDue2.Top = lngRowArr[1];
					fldPer2.Visible = false;
					fldTaxAmt2.Visible = false;
					fldTaxBal2.Visible = false;
					fldInterest2.Visible = false;
					fldTotalDue2.Visible = false;
					fldPer3.Top = lngRowArr[1];
					fldTaxAmt3.Top = lngRowArr[1];
					fldTaxBal3.Top = lngRowArr[1];
					fldInterest3.Top = lngRowArr[1];
					fldTotalDue3.Top = lngRowArr[1];
					fldPer3.Visible = false;
					fldTaxAmt3.Visible = false;
					fldTaxBal3.Visible = false;
					fldInterest3.Visible = false;
					fldTotalDue3.Visible = false;
					fldPer4.Top = lngRowArr[1];
					fldTaxAmt4.Top = lngRowArr[1];
					fldTaxBal4.Top = lngRowArr[1];
					fldInterest4.Top = lngRowArr[1];
					fldTotalDue4.Top = lngRowArr[1];
					fldPer4.Visible = false;
					fldTaxAmt4.Visible = false;
					fldTaxBal4.Visible = false;
					fldInterest4.Visible = false;
					fldTotalDue4.Visible = false;
				}

                TSArray[record.Year - 1980].Count += 1;
                switch (record.Period)
				{
					case 1:
						{
							TSArray[record.Year - 1980].Balance += record.Tax1Balance.ToDouble();
                            TSArray[record.Year - 1980].Interest += record.Tax1InterestDue.ToDouble();
                            break;
						}
					case 2:
						{
                            TSArray[record.Year - 1980].Balance += (record.Tax1Balance + record.Tax2Balance).ToDouble();
                            TSArray[record.Year - 1980].Interest += (record.Tax1InterestDue + record.Tax2InterestDue).ToDouble();
                            break;
						}
					case 3:
						{
                            TSArray[record.Year - 1980].Balance += (record.Tax1Balance + record.Tax2Balance + record.Tax3Balance).ToDouble();
                            TSArray[record.Year - 1980].Interest += (record.Tax1InterestDue + record.Tax2InterestDue + record.Tax3InterestDue).ToDouble();
                            break;
						}
					case 4:
						{
                            TSArray[record.Year - 1980].Balance += (record.Tax1Balance + record.Tax2Balance + record.Tax3Balance + record.Tax4Balance).ToDouble();
                            TSArray[record.Year - 1980].Interest += (record.Tax1InterestDue + record.Tax2InterestDue + record.Tax3InterestDue + record.Tax4InterestDue).ToDouble();
                            break;
						}
				}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void ClearAmtFields()
		{
			fldPer1.Text = "";
			fldTaxAmt1.Text = "";
			fldTaxBal1.Text = "";
			fldInterest1.Text = "";
			fldTotalDue1.Text = "";
			fldPer2.Text = "";
			fldTaxAmt2.Text = "";
			fldTaxBal2.Text = "";
			fldInterest2.Text = "";
			fldTotalDue2.Text = "";
			fldPer3.Text = "";
			fldTaxAmt3.Text = "";
			fldTaxBal3.Text = "";
			fldInterest3.Text = "";
			fldTotalDue3.Text = "";
			fldPer4.Text = "";
			fldTaxAmt4.Text = "";
			fldTaxBal4.Text = "";
			fldInterest4.Text = "";
			fldTotalDue4.Text = "";
		}
		private void ReportFooter_Format(object sender, EventArgs e)
		{
			// load the summary report
			int lngCT;
			int lngRow;
			double dblSumTotBal = 0;
			double dblSumTotInt = 0;
			int lngSumCount = 0;
			lngRow = 1;
            var summaryItems = new List<TaxServiceSummarySummaryItem>();
			for (lngCT = 0; lngCT <= (DateTime.Now.Year - 1980) + 3; lngCT++)
			{
				if (TSArray[lngCT].Count > 0)
				{
                    summaryItems.Add(new TaxServiceSummarySummaryItem()
                    {
                        Balance = TSArray[lngCT].Balance.ToDecimal(),
                        Count = TSArray[lngCT].Count,
                        Interest = TSArray[lngCT].Interest.ToDecimal(),
                        Year = lngCT + 1980
                    });
					
					lngRow += 1;
				}
			}
            
            subReport1.Report = new srptTaxServiceFooter();
            subReport1.Report.UserData = summaryItems;
        }
	}
}
