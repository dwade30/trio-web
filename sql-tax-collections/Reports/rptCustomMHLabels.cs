﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using GrapeCity.ActiveReports;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptCustomMHLabels : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/12/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/08/2004              *
		// ********************************************************
		// THIS REPORT IS TOTOALLY NOT GENERIC AND HAS BEEN ALTERED FOR THIS TWCL
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsMort = new clsDRWrapper();
		// VBto upgrade warning: intLabelWidth As short --> As int	OnWrite(int, double)
		float intLabelWidth;
		bool boolDifferentPageSize;
		string strFont;
		bool boolPP;
		bool boolMort;
		bool boolEmpty;
		string strLeftAdjustment = "";
		float lngVertAdjust;
		int intTypeOfLabel;
		clsPrintLabel labLabels = new clsPrintLabel();
		bool boolDotMatrix;
		// VBto upgrade warning: lngPrintWidth As int	OnWrite(string, double)
		float lngPrintWidth;
		string strDefPrinterName = "";
		string reportType = "";

		public rptCustomMHLabels()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Custom Labels";
		}

		public static rptCustomMHLabels InstancePtr
		{
			get
			{
				return (rptCustomMHLabels)Sys.GetInstance(typeof(rptCustomMHLabels));
			}
		}

		protected rptCustomMHLabels _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
				rsMort.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// IF THIS IS THE END OF THE RECORDSET THEN WE NEED A WAY TO GET OUT
			eArgs.EOF = rsData.EndOfFile();
			//Detail_Format();
		}
		// VBto upgrade warning: intLabelType As short	OnWriteFCConvert.ToInt32(
		public void Init(string strSQL, string strReportType, int intLabelType, string strPrinterName, string strFonttoUse, string strPassDefaultPrinter = "")
		{
			string strDBName = "";
			int intReturn/*unused?*/;
			int x/*unused?*/;
			bool boolUseFont/*unused?*/;
			int intOrder/*unused?*/;
			strFont = strFonttoUse;
			reportType = modCustomReport.Statics.strReportType;
			
			boolPP = false;
			boolMort = false;
			if (strReportType == "MORTGAGEHOLDER")
			{
				strDBName = "CentralData";
				// strGNDatabase
				boolMort = true;
			}
			else if ((strReportType == "RECL") || (strReportType == "LABELS"))
			{
				strDBName = modExtraModules.strCLDatabase;
			}
			// rsData.OpenRecordset strSQL, strCLDatabase
			if (modStatusPayments.Statics.boolRE)
			{
				rsData.OpenRecordset(strSQL, strDBName);
			}
			rsMort.OpenRecordset("SELECT * FROM MortgageHolders", "CentralData");
			intTypeOfLabel = intLabelType;
			// make them choose the printer first if you have to use a custom form
			// RESET THE RECORDSET TO THE BEGINNING OF THE RECORDSET
			if (rsData.RecordCount() != 0)
				rsData.MoveFirst();
			boolDifferentPageSize = false;
			int intindex;
			int cnt/*unused?*/;
			intindex = labLabels.Get_IndexFromID(intTypeOfLabel);
			if (labLabels.Get_IsDymoLabel(intindex))
			{
				switch (labLabels.Get_ID(intindex))
				{
					case modLabels.CNSTLBLTYPEDYMO30256:
						{
							PageSettings.DefaultPaperSize = false;
							PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom;
							this.Document.Printer.DefaultPageSettings.Landscape = true;
							PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
							PageSettings.Margins.Top = FCConvert.ToSingle(labLabels.Get_TopMargin(intindex) + (modGlobal.Statics.gdblLabelsAdjustment * 240) / 1440f);
							PageSettings.Margins.Bottom = labLabels.Get_BottomMargin(intindex);
							PageSettings.Margins.Right = labLabels.Get_RightMargin(intindex);
							PageSettings.Margins.Left = labLabels.Get_LeftMargin(intindex);
							PrintWidth = labLabels.Get_LabelWidth(intindex) - PageSettings.Margins.Left - PageSettings.Margins.Right;
							intLabelWidth = labLabels.Get_LabelWidth(intindex);
							lngPrintWidth = PrintWidth;
							Detail.Height = labLabels.Get_LabelHeight(intindex) - PageSettings.Margins.Top - PageSettings.Margins.Bottom - 10 / 1440f;
							Detail.ColumnCount = 1;
							PageSettings.PaperHeight = labLabels.Get_LabelWidth(intindex);
							PageSettings.PaperWidth = labLabels.Get_LabelHeight(intindex);
							Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
							this.Document.Printer.DefaultPageSettings.Landscape = true;
							PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
							break;
						}
					case modLabels.CNSTLBLTYPEDYMO30252:
						{
							this.Document.Printer.DefaultPageSettings.Landscape = true;
							PageSettings.DefaultPaperSize = false;
							PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom;
							PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
							PageSettings.Margins.Top = 0.128f;
							PageSettings.Margins.Bottom = 0;
							PageSettings.Margins.Right = 0;
							PageSettings.Margins.Left = 0.128f;
							PrintWidth = 3.5f - PageSettings.Margins.Left - PageSettings.Margins.Right;
							intLabelWidth = 3.5f;
							lngPrintWidth = 3.5f - PageSettings.Margins.Left - PageSettings.Margins.Right;
							Detail.Height = 1.1f - PageSettings.Margins.Top - PageSettings.Margins.Bottom - 10 / 1440f;
							Detail.ColumnCount = 1;
							PageSettings.PaperHeight = 3.5f;
							PageSettings.PaperWidth = 1.1f;
							Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
							this.Document.Printer.DefaultPageSettings.Landscape = true;
							PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
							break;
						}
				}
			}
			else if (labLabels.Get_IsLaserLabel(intindex))
			{
				PageSettings.DefaultPaperSize = false;
				PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom;
				PageSettings.Margins.Top = 0.5f;
				PageSettings.Margins.Top += FCConvert.ToSingle(modGlobal.Statics.gdblLabelsAdjustment * 270) / 1440f;
				PageSettings.Margins.Left = labLabels.Get_LeftMargin(intindex);
				PageSettings.Margins.Right = labLabels.Get_RightMargin(intindex);
				PrintWidth = labLabels.Get_PageWidth(intindex) - labLabels.Get_LeftMargin(intindex) - labLabels.Get_RightMargin(intindex);
				intLabelWidth = labLabels.Get_LabelWidth(intindex);
				Detail.Height = labLabels.Get_LabelHeight(intindex) + labLabels.Get_VerticalSpace(intindex);
				if (labLabels.Get_LabelsWide(intindex) > 0)
				{
					Detail.ColumnCount = labLabels.Get_LabelsWide(intindex);
					Detail.ColumnSpacing = labLabels.Get_HorizontalSpace(intindex);
				}
				lngPrintWidth = PrintWidth;
			}
			
			CreateDataFields();
			frmReportViewer.InstancePtr.Init(this, this.Document.Printer.PrinterName);
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			if (rsData.RecordCount() > 0)
			{
				switch (frmRateRecChoice.InstancePtr.intRateType)
				{
					case 10:
						{
							modGlobalFunctions.IncrementSavedReports("LastCL30DayLabels");
							this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastCL30DayLabels1.RDF"));
							break;
						}
					case 11:
						{
							modGlobalFunctions.IncrementSavedReports("LastCLTransferToLienLabels");
							this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastCLTransferToLienLabels1.RDF"));
							break;
						}
					case 12:
						{
							modGlobalFunctions.IncrementSavedReports("LastCLLienMaturityLabels");
							this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastCLLienMaturityLabels1.RDF"));
							break;
						}
				}
				//end switch
			}
			else
			{
				FCMessageBox.Show("There are no eligible accounts.", MsgBoxStyle.Information, "No Labels");
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int intReturn = 0;
			int const_PrintToolID;
			int cnt;
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			// override the print button so the print dialog doesn't come up again
			const_PrintToolID = 9950;
			//FC:TODO:AM
			//for (cnt = 0; cnt <= this.Toolbar.Tools.Count - 1; cnt++)
			//{
			//	if ("Print..." == this.Toolbar.Tools(cnt).Caption)
			//	{
			//		this.Toolbar.Tools(cnt).ID = const_PrintToolID;
			//		this.Toolbar.Tools(cnt).Enabled = true;
			//	}
			//} // cnt
			if (intTypeOfLabel == modLabels.CNSTLBLTYPEDYMO30256 || intTypeOfLabel == modLabels.CNSTLBLTYPEDYMO30252)
			{
				for (cnt = 0; cnt <= this.Document.Printer.PaperSizes.Count - 1; cnt++)
				{
					switch (intTypeOfLabel)
					{
						case modLabels.CNSTLBLTYPEDYMO30252:
							{
								if (Strings.UCase(this.Document.Printer.PaperSizes[cnt].PaperName) == "30252 ADDRESS")
								{
									this.Document.Printer.PaperSize = this.Document.Printer.PaperSizes[cnt];
									this.Document.Printer.DefaultPageSettings.Landscape = true;
									break;
								}
								break;
							}
						case modLabels.CNSTLBLTYPEDYMO30256:
							{
								if (Strings.UCase(this.Document.Printer.PaperSizes[cnt].PaperName) == "30256 SHIPPING")
								{
									this.Document.Printer.PaperSize = this.Document.Printer.PaperSizes[cnt];
									this.Document.Printer.DefaultPageSettings.Landscape = true;
									break;
								}
								break;
							}
					}
					//end switch
				}
				// cnt
			}
			else
			{
				//FC:TEMP:AM: don't use the printer directly
				//if (modSysInfo.IsNTBased())
				//{
				//	// kk01132016 trout-1176  Default printer not being reset after prosess
				//	// CheckDefaultPrinter Me.Document.Printer.PrinterName
				//	intReturn = modCustomPageSize.SelectForm("MHCustomLabel", 0/*this.Handle.ToInt32()*/, FCConvert.ToInt32(PageSettings.PaperWidth * 25400 / 1440), FCConvert.ToInt32(PageSettings.PaperHeight * 25400 / 1440), true);
				//	if (intReturn > 0)
				//	{
				//		//this.Document.Printer.PaperSize = intReturn;
				//	}
				//	else
				//	{
				//		FCMessageBox.Show("Error. Could not access form on printer.");
				//		Cancel();
				//		return;
				//	}
				//}
				//else
				//{
				//	this.Document.Printer.PaperSize = new System.Drawing.Printing.PaperSize("Custom", FCConvert.ToInt32(PageSettings.PaperWidth, FCConvert.ToInt32(PageSettings.PaperHeight);
				//	//this.Document.Printer.PaperHeight = PageSettings.PaperHeight;
				//	//this.Document.Printer.PaperWidth = PageSettings.PaperWidth;
				//}
			}
		}

		private void CreateDataFields()
		{
			int intControlNumber/*unused?*/;
			GrapeCity.ActiveReports.SectionReportModel.TextBox NewField;
			/*unused?*/
			int intRow;
			int intCol/*unused?*/;
			int intNumber;
			// CREATE THE CONTROLS AND SET THE POSITION OF THE CONTROLS
			intNumber = 4;
			if (boolMort)
				intNumber = 5;
			for (intRow = 1; intRow <= intNumber; intRow++)
			{
				NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
				NewField.CanGrow = false;
				NewField.Name = "txtData" + FCConvert.ToString(intRow);
				NewField.Top = ((intRow - 1) * 0.15f) + lngVertAdjust;
				NewField.Left = 0.1f;
				// one space
				NewField.Width = (PrintWidth / Detail.ColumnCount - ((Detail.ColumnCount - 1) * Detail.ColumnSpacing)) - 0.1f;
				NewField.Height = 0.15f;
				NewField.Text = string.Empty;
				NewField.MultiLine = false;
				NewField.WordWrap = true;
				if (Strings.Trim(strFont) != string.Empty)
				{
					NewField.Font = new Font(strFont, NewField.Font.Size);
				}
				else
				{
					NewField.Font = new Font(lblFont.Font.Name, NewField.Font.Size);
				}
				Detail.Controls.Add(NewField);
				if (intRow == 1)
				{
					NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
					NewField.CanGrow = false;
					NewField.Name = "txtMort";
					NewField.Top = ((intRow - 1) * 0.15f) + lngVertAdjust;
					NewField.Left = (PrintWidth / Detail.ColumnCount - ((Detail.ColumnCount - 1) * Detail.ColumnSpacing)) - 0.79f;
					NewField.Width = 0.69f;
					NewField.Height = 0.15f;
					NewField.Text = string.Empty;
					NewField.MultiLine = false;
					NewField.WordWrap = true;
					if (Strings.Trim(strFont) != string.Empty)
					{
						NewField.Font = new Font(strFont, NewField.Font.Size);
					}
					else
					{
						NewField.Font = new Font(lblFont.Font.Name, NewField.Font.Size);
					}
					Detail.Controls.Add(NewField);
				}
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			
			App.MainForm.Show();
		}

		public void ActiveReport_ToolbarClick(/*DDActiveReports2.DDTool Tool*/)
		{
			int intindex = 0;
			int numberOfPages = this.Document.Pages.Count;
			int pageStart = 0;
			string VBtoVar = ""/*Tool.Caption*/;
			if (VBtoVar == "Print...")
			{
				if (frmNumPages.InstancePtr.Init(ref numberOfPages, ref pageStart, null, "Print Pages"))
				{
					this.Document.Print(false);
				}
			}
		}

		private void PrintMortgageHolders()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intControl;
				int intRow;
				string str1 = "";
				string str2 = "";
				string str3 = "";
				string str4 = "";
				string str5 = "";
				int lngAcctNumber/*unused?*/;
				if (rsData.EndOfFile() != true)
				{
					str1 = strLeftAdjustment + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name")));
					str2 = strLeftAdjustment + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address1")));
					str3 = strLeftAdjustment + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address2")));
					str4 = strLeftAdjustment + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address3")));
					str5 = strLeftAdjustment + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("City")) + ", " + FCConvert.ToString(rsData.Get_Fields_String("State"))) + " " + FCConvert.ToString(rsData.Get_Fields_String("Zip"));
					// MAL@20071004: Call Reference 117005
					// If Trim(rsData.Fields("Zip5")) <> "" Then
					if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Zip4"))) != "")
					{
						str5 += "-" + FCConvert.ToString(rsData.Get_Fields_String("Zip4"));
					}
					// condense the labels if some are blank
					if (boolMort)
					{
						if (Strings.Trim(str4) == string.Empty)
						{
							str4 = str5;
							str5 = "";
						}
					}
					if (Strings.Trim(str3) == string.Empty)
					{
						str3 = str4;
						if (boolMort)
						{
							str4 = str5;
							str5 = "";
						}
						else
						{
							str4 = "";
						}
					}
					if (Strings.Trim(str2) == string.Empty)
					{
						str2 = str3;
						str3 = str4;
						if (boolMort)
						{
							str4 = str5;
							str5 = "";
						}
						else
						{
							str4 = "";
						}
					}
					if (Strings.Trim(str1) == string.Empty)
					{
						str1 = str2;
						str2 = str3;
						str3 = str4;
						if (boolMort)
						{
							str4 = str5;
							str5 = "";
						}
						else
						{
							str4 = "";
						}
					}
					for (intControl = 0; intControl <= Detail.Controls.Count - 1; intControl++)
					{
						if (Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) == "txtData")
						{
							intRow = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Detail.Controls[intControl].Name, 8))));
							GrapeCity.ActiveReports.SectionReportModel.TextBox txtData = (GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl];
							switch (intRow)
							{
								case 1:
									{
										txtData.Text = strLeftAdjustment + str1;
										break;
									}
								case 2:
									{
										txtData.Text = strLeftAdjustment + str2;
										break;
									}
								case 3:
									{
										txtData.Text = strLeftAdjustment + str3;
										break;
									}
								case 4:
									{
										txtData.Text = strLeftAdjustment + str4;
										break;
									}
								case 5:
									{
										txtData.Text = strLeftAdjustment + str5;
										break;
									}
							}
							//end switch
						}
						// this will fill the account number in
						// If Trim(Left(Detail.Controls[intControl].Name & "      ", 7)) = "txtAcct" Then
						// ((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = Format(rsData.Fields("AccountNumber"), "00000")
						// End If
						if (str1.Length <= 19)
						{
							if (boolMort && Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) == "txtMort")
							{
								((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = Strings.Format(rsData.Get_Fields_Int32("ID"), "000");
							}
						}
						else
						{
							if (boolMort && Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) == "txtMort")
							{
								((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = "";
							}
						}
					}
					// intControl
					rsData.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Printing Mortgage Holders");
			}
		}

		private void PrintLabels()
		{
			// this will fill in the fields for labels
			int intControl;
			int intRow;
			string str1 = "";
			string str2 = "";
			string str3 = "";
			string str4 = "";
			string str5 = "";
			if (rsData.EndOfFile() != true)
			{
				if (((rsData.Get_Fields_Int32("MortgageHolder"))) != 0)
				{
					boolMort = true;
					rsMort.FindFirstRecord("ID", rsData.Get_Fields_Int32("MortgageHolder"));
					if (!rsMort.EndOfFile())
					{
						str1 = Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Name")));
						str2 = Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address1")));
						str3 = Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address2")));
						str4 = Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address3")));
						str5 = Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("City"))) + " " + FCConvert.ToString(rsMort.Get_Fields_String("State")) + " " + FCConvert.ToString(rsMort.Get_Fields_String("Zip"));
						if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
						{
							str5 += "-" + Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4")));
						}
					}
					else
					{
						str1 = "Missing Data for Mortgage Holder #" + FCConvert.ToString(rsData.Get_Fields_Int32("MortgageHolder"));
						str2 = "";
						str3 = "";
						str4 = "";
					}
				}
				else
				{
					boolMort = false;
					if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name2"))) != "")
					{
						str1 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name1"))) + " and " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name2")));
					}
					else
					{
						str1 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name1")));
					}
					str2 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("RSAddr1")));
					str3 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("RSAddr2")));
					str4 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("RSAddr3"))) + " " + FCConvert.ToString(rsData.Get_Fields_String("RSState")) + " " + FCConvert.ToString(rsData.Get_Fields_String("RSZip"));
					if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("RSZip4"))) != "")
					{
						str4 += "-" + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("RSZip4")));
					}
				}
				if (intTypeOfLabel == 2)
				{
					// small labels
					if (str1.Length > 18)
					{
						str5 = str4;
						str4 = str3;
						str3 = str2;
						str2 = Strings.Right(str1, str1.Length - 18);
						str1 = Strings.Left(str1, 18);
					}
				}
				else
				{
					if (str1.Length > 35)
					{
						// 4" labels
						str5 = str4;
						str4 = str3;
						str3 = str2;
						str2 = Strings.Right(str1, str1.Length - 35);
						str1 = Strings.Left(str1, 35);
					}
				}
				// condense the labels if some are blank
				// If boolMort Then
				if (Strings.Trim(str4) == string.Empty)
				{
					str4 = str5;
					str5 = "";
				}
				// End If
				if (Strings.Trim(str3) == string.Empty)
				{
					str3 = str4;
					if (boolMort)
					{
						str4 = str5;
						str5 = "";
					}
					else
					{
						str4 = "";
					}
				}
				if (Strings.Trim(str2) == string.Empty)
				{
					str2 = str3;
					str3 = str4;
					if (boolMort)
					{
						str4 = str5;
						str5 = "";
					}
					else
					{
						str4 = "";
					}
				}
				if (Strings.Trim(str1) == string.Empty)
				{
					str1 = str2;
					str2 = str3;
					str3 = str4;
					if (boolMort)
					{
						str4 = str5;
						str5 = "";
					}
					else
					{
						str4 = "";
					}
				}
				for (intControl = 0; intControl <= Detail.Controls.Count - 1; intControl++)
				{
					if (Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) == "txtData")
					{
						intRow = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Detail.Controls[intControl].Name, 8))));
						GrapeCity.ActiveReports.SectionReportModel.TextBox txtData = (GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl];
						switch (intRow)
						{
							case 1:
								{
									txtData.Text = strLeftAdjustment + str1;
									break;
								}
							case 2:
								{
									txtData.Text = strLeftAdjustment + str2;
									break;
								}
							case 3:
								{
									txtData.Text = strLeftAdjustment + str3;
									break;
								}
							case 4:
								{
									txtData.Text = strLeftAdjustment + str4;
									break;
								}
							case 5:
								{
									txtData.Text = strLeftAdjustment + str5;
									break;
								}
						}
						//end switch
					}
					// this will fill the account number in
					if (Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) == "txtAcct")
					{
						// TODO Get_Fields: Field [New.Account] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [New.Account] not found!! (maybe it is an alias?)
						((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = Strings.Format(rsData.Get_Fields("New.Account"), "#0000");
					}
					if (boolMort && Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) == "txtMort")
					{
						// TODO Get_Fields: Field [MortgageHolderNumber] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [MortgageHolderNumber] not found!! (maybe it is an alias?)
						((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = Strings.Format(rsData.Get_Fields("MortgageHolderNumber"), "00000");
					}
				}
				// intControl
				rsData.MoveNext();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (Strings.UCase(reportType) == "LABELS")
			{
				PrintLabels();
			}
			else if ((Strings.UCase(reportType) == "RECL") || (Strings.UCase(reportType) == "MORTGAGEHOLDER"))
			{
				PrintMortgageHolders();
			}
		}

		
	}
}
