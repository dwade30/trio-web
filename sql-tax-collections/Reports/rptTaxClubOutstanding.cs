﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptTaxClubOutstanding : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               02/26/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/15/2006              *
		// ********************************************************
		string strSQL;
		int intType;
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsLData = new clsDRWrapper();
		int lngTotalAccounts;
		int lngCount;

		public rptTaxClubOutstanding()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Tax Club Outstanding Balance Report";
		}

		public static rptTaxClubOutstanding InstancePtr
		{
			get
			{
				return (rptTaxClubOutstanding)Sys.GetInstance(typeof(rptTaxClubOutstanding));
			}
		}

		protected rptTaxClubOutstanding _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsLData?.Dispose();
				rsData?.Dispose();
            }
			base.Dispose(disposing);
		}

		public void Init()
		{
			// this sub will set the SQL string
			strSQL = BuildSQL();
			rsData.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
			rsLData.OpenRecordset("SELECT * FROM LienRec");
			lngTotalAccounts = rsData.RecordCount();
			if (lngTotalAccounts == 0)
			{
				// do not show the report if there are no records
				FCMessageBox.Show("There are no accounts eligible.", MsgBoxStyle.Information, "No Accounts");
				Cancel();
			}
			else
			{
				frmReportViewer.InstancePtr.Init(this);
				// Me.Show , MDIParent
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
			//Detail_Format();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			// save this report for future access
			if (this.Document.Pages.Count > 0)
			{
				modGlobalFunctions.IncrementSavedReports("LastCLActivity");
				this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastCLActivity1.RDF"));
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "t");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			lngCount = 0;
		}

		private void BindFields()
		{
			// this will fill the information into the fields
			bool boolRetry;
			int lngLRN = 0;
			// Lien Record Number
			TRYNEXTACCOUNT:
			;
			boolRetry = false;
            fldAcct.Visible = false;
            fldName.Visible = false;
            fldOwed.Visible = false;
            //srptTaxClubActivityDetail.Object = null;
            if (!rsData.EndOfFile())
			{
				lngLRN = FCConvert.ToInt32(rsData.Get_Fields_Int32("LienRecordNumber"));
				// set the lien record if needed
				if (lngLRN == 0)
				{
					// this is a regular bill
				}
				else
				{
					// this is a lien
					rsLData.FindFirstRecord("ID", lngLRN);
				}
				// check to see if this account is elegible to be shown and
				// the principal bill has been not been completely paid
				if (lngLRN == 0)
				{
					// this is a regular bill
					if ((Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue4"))) > Conversion.Val(rsData.Get_Fields_Decimal("PrincipalPaid")))
					{
						// show this account
						boolRetry = false;
					}
					else
					{
						// do not show this account
						boolRetry = true;
					}
				}
				else
				{
					// this is a lien
					// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
					if (Conversion.Val(rsLData.Get_Fields("Principal")) > Conversion.Val(rsLData.Get_Fields_Decimal("PrincipalPaid")))
					{
						// show this account
						boolRetry = false;
					}
					else
					{
						// do not show this account
						boolRetry = true;
					}
				}
				if (boolRetry)
				{
					rsData.MoveNext();
					goto TRYNEXTACCOUNT;
				}
				else
				{
					lngCount += 1;
				}
                fldAcct.Visible = true;
                fldName.Visible = true;
                fldOwed.Visible = true;
                fldAcct.Text = FCConvert.ToString(rsData.Get_Fields("Account"));
				fldName.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name1")));
				if (lngLRN == 0)
				{
					fldOwed.Text = Strings.Format((Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue4"))), "#,##0.00");
					// pass the BillKey for the subreport to use to find all of the payments that match this account and year
					srptTaxClubActivityDetail.Report = new srptActivityDetail();
					srptTaxClubActivityDetail.Report.UserData = rsData.Get_Fields_Int32("BillKey");
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
					fldOwed.Text = Strings.Format(rsLData.Get_Fields("Principal"), "#,##0.00");
					// kk03272015 trocl-1135  '(rsData.Fields("Principal")), "#,##0.00")
					// pass the LienRecordNumber for the subreport to use to find all of the payments that match this account and year
					srptTaxClubActivityDetail.Report = new srptActivityDetailLien();
					srptTaxClubActivityDetail.Report.UserData = rsData.Get_Fields_Int32("LienRecordNumber");
				}
				// move to the next record in the query
				rsData.MoveNext();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void SetupTotals()
		{
			// this sub will fill in the footer line at the bottom of the report
			lblFooter.Text = "There were " + FCConvert.ToString(lngCount) + " accounts processed.";
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			SetupTotals();
		}

		private string BuildSQL()
		{
			string BuildSQL = "";
			// this function will return the SQL string for the criteria that has been selected
			string strMod = "";
			if (modStatusPayments.Statics.boolRE)
			{
				// this will select the correct records from the tax table
				strMod = "RE";
			}
			else
			{
				strMod = "PP";
			}
			// kgk 2-14-2012  BuildSQL = "SELECT * FROM TaxClubJoin WHERE BillingType = '" & strMod & "' AND Type = '" & strMod & "'"
			// TaxClubJoin = "SELECT BillingMaster.BillKey AS BK, BillingMaster.Account AS Acct, * FROM BillingMaster INNER JOIN TaxClub ON BillingMaster.Billkey = TaxClub.BillKey"
			BuildSQL = "SELECT BillingMaster.ID AS BK, BillingMaster.Account AS Acct, * FROM BillingMaster INNER JOIN TaxClub ON BillingMaster.ID = TaxClub.BillKey WHERE BillingType = '" + strMod + "' AND Type = '" + strMod + "'";
			return BuildSQL;
		}

		private void rptTaxClubOutstanding_Load(object sender, System.EventArgs e)
		{

		}
	}
}
