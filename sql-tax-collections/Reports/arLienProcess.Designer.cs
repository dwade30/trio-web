﻿namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	partial class arLienProcess
	{
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(arLienProcess));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.rtbText = new GrapeCity.ActiveReports.SectionReportModel.RichTextBox();
			this.imgSig = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			this.rtbText2 = new GrapeCity.ActiveReports.SectionReportModel.RichTextBox();
			this.lblPrincipal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblInterest = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCosts2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldCosts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldCollector = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblBreakdown = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCosts1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.imgTownSeal = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			this.fldHeader = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			((System.ComponentModel.ISupportInitialize)(this.imgSig)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCosts2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCosts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCollector)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBreakdown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCosts1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgTownSeal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.rtbText,
				this.imgSig,
				this.rtbText2,
				this.lblPrincipal,
				this.lblInterest,
				this.lblCosts2,
				this.lblTotal,
				this.Line1,
				this.fldCosts,
				this.fldPrincipal,
				this.fldInterest,
				this.fldTotal,
				this.Line2,
				this.fldCollector,
				this.fldTitle,
				this.fldMuni,
				this.lblBreakdown,
				this.lblCosts1
			});
			this.Detail.Height = 1.708333F;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// rtbText
			// 
			this.rtbText.AutoReplaceFields = true;
			this.rtbText.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.rtbText.Height = 0.1875F;
			this.rtbText.Left = 0F;
			this.rtbText.Name = "rtbText";
			this.rtbText.RTF = resources.GetString("rtbText.RTF");
			this.rtbText.Top = 0.0625F;
			this.rtbText.Width = 7.4375F;
			// 
			// imgSig
			// 
			this.imgSig.Height = 0.5625F;
			this.imgSig.HyperLink = null;
			this.imgSig.ImageData = null;
			this.imgSig.Left = 3.4375F;
			this.imgSig.LineWeight = 1F;
			this.imgSig.Name = "imgSig";
			this.imgSig.PictureAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.BottomLeft;
			this.imgSig.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
			this.imgSig.Top = 0.25F;
			this.imgSig.Width = 2.375F;
			// 
			// rtbText2
			// 
			this.rtbText2.AutoReplaceFields = true;
			this.rtbText2.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.rtbText2.Height = 0.1875F;
			this.rtbText2.Left = 0F;
			this.rtbText2.Name = "rtbText2";
			this.rtbText2.RTF = resources.GetString("rtbText2.RTF");
			this.rtbText2.Top = 1.4375F;
			this.rtbText2.Width = 7.4375F;
			// 
			// lblPrincipal
			// 
			this.lblPrincipal.Height = 0.1875F;
			this.lblPrincipal.HyperLink = null;
			this.lblPrincipal.Left = 0.375F;
			this.lblPrincipal.Name = "lblPrincipal";
			this.lblPrincipal.Style = "font-family: \'Courier New\'";
			this.lblPrincipal.Text = "Principal";
			this.lblPrincipal.Top = 0.8125F;
			this.lblPrincipal.Width = 1F;
			// 
			// lblInterest
			// 
			this.lblInterest.Height = 0.1875F;
			this.lblInterest.HyperLink = null;
			this.lblInterest.Left = 0.375F;
			this.lblInterest.Name = "lblInterest";
			this.lblInterest.Style = "font-family: \'Courier New\'";
			this.lblInterest.Text = "Interest";
			this.lblInterest.Top = 1F;
			this.lblInterest.Width = 1F;
			// 
			// lblCosts2
			// 
			this.lblCosts2.Height = 0.1875F;
			this.lblCosts2.HyperLink = null;
			this.lblCosts2.Left = 0.375F;
			this.lblCosts2.Name = "lblCosts2";
			this.lblCosts2.Style = "font-family: \'Courier New\'";
			this.lblCosts2.Text = "Mailing Costs";
			this.lblCosts2.Top = 0.625F;
			this.lblCosts2.Width = 1.1875F;
			// 
			// lblTotal
			// 
			this.lblTotal.Height = 0.1875F;
			this.lblTotal.HyperLink = null;
			this.lblTotal.Left = 0.375F;
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Style = "font-family: \'Courier New\'";
			this.lblTotal.Text = "Total";
			this.lblTotal.Top = 1.1875F;
			this.lblTotal.Width = 0.875F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.375F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.1875F;
			this.Line1.Width = 2.25F;
			this.Line1.X1 = 0.375F;
			this.Line1.X2 = 2.625F;
			this.Line1.Y1 = 1.1875F;
			this.Line1.Y2 = 1.1875F;
			// 
			// fldCosts
			// 
			this.fldCosts.Height = 0.1875F;
			this.fldCosts.Left = 1.4375F;
			this.fldCosts.Name = "fldCosts";
			this.fldCosts.Style = "font-family: \'Courier New\'; text-align: right";
			this.fldCosts.Text = "0.00";
			this.fldCosts.Top = 0.625F;
			this.fldCosts.Width = 1.1875F;
			// 
			// fldPrincipal
			// 
			this.fldPrincipal.Height = 0.1875F;
			this.fldPrincipal.Left = 1.4375F;
			this.fldPrincipal.Name = "fldPrincipal";
			this.fldPrincipal.Style = "font-family: \'Courier New\'; text-align: right";
			this.fldPrincipal.Text = "0.00";
			this.fldPrincipal.Top = 0.8125F;
			this.fldPrincipal.Width = 1.1875F;
			// 
			// fldInterest
			// 
			this.fldInterest.Height = 0.1875F;
			this.fldInterest.Left = 1.4375F;
			this.fldInterest.Name = "fldInterest";
			this.fldInterest.Style = "font-family: \'Courier New\'; text-align: right";
			this.fldInterest.Text = "0.00";
			this.fldInterest.Top = 1F;
			this.fldInterest.Width = 1.1875F;
			// 
			// fldTotal
			// 
			this.fldTotal.Height = 0.1875F;
			this.fldTotal.Left = 1.4375F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "font-family: \'Courier New\'; text-align: right";
			this.fldTotal.Text = "0.00";
			this.fldTotal.Top = 1.1875F;
			this.fldTotal.Width = 1.1875F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 3.4375F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.8125F;
			this.Line2.Width = 2.4375F;
			this.Line2.X1 = 3.4375F;
			this.Line2.X2 = 5.875F;
			this.Line2.Y1 = 0.8125F;
			this.Line2.Y2 = 0.8125F;
			// 
			// fldCollector
			// 
			this.fldCollector.Height = 0.1875F;
			this.fldCollector.Left = 3.4375F;
			this.fldCollector.Name = "fldCollector";
			this.fldCollector.Style = "font-family: \'Courier New\'";
			this.fldCollector.Text = null;
			this.fldCollector.Top = 0.875F;
			this.fldCollector.Width = 2.4375F;
			// 
			// fldTitle
			// 
			this.fldTitle.Height = 0.1875F;
			this.fldTitle.Left = 3.4375F;
			this.fldTitle.Name = "fldTitle";
			this.fldTitle.Style = "font-family: \'Courier New\'";
			this.fldTitle.Text = "Tax Collector";
			this.fldTitle.Top = 1.0625F;
			this.fldTitle.Width = 2.4375F;
			// 
			// fldMuni
			// 
			this.fldMuni.Height = 0.1875F;
			this.fldMuni.Left = 3.4375F;
			this.fldMuni.Name = "fldMuni";
			this.fldMuni.Style = "font-family: \'Courier New\'";
			this.fldMuni.Text = null;
			this.fldMuni.Top = 1.25F;
			this.fldMuni.Width = 2.4375F;
			// 
			// lblBreakdown
			// 
			this.lblBreakdown.Height = 0.1875F;
			this.lblBreakdown.HyperLink = null;
			this.lblBreakdown.Left = 0F;
			this.lblBreakdown.Name = "lblBreakdown";
			this.lblBreakdown.Style = "font-family: \'Courier New\'";
			this.lblBreakdown.Text = "Costs to be paid by taxpayer:";
			this.lblBreakdown.Top = 0.25F;
			this.lblBreakdown.Width = 2.6875F;
			// 
			// lblCosts1
			// 
			this.lblCosts1.Height = 0.1875F;
			this.lblCosts1.HyperLink = null;
			this.lblCosts1.Left = 0.375F;
			this.lblCosts1.Name = "lblCosts1";
			this.lblCosts1.Style = "font-family: \'Courier New\'";
			this.lblCosts1.Text = "Statutory Fees and";
			this.lblCosts1.Top = 0.4375F;
			this.lblCosts1.Width = 1.625F;
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.CanShrink = true;
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.imgTownSeal,
				this.fldHeader,
				this.lblAccount
			});
			this.GroupHeader1.DataField = "Account_Copy";
			this.GroupHeader1.Height = 0.9166667F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			// 
			// imgTownSeal
			// 
			this.imgTownSeal.Height = 0.875F;
			this.imgTownSeal.HyperLink = null;
			this.imgTownSeal.ImageData = null;
			this.imgTownSeal.Left = 0F;
			this.imgTownSeal.LineWeight = 1F;
			this.imgTownSeal.Name = "imgTownSeal";
			this.imgTownSeal.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
			this.imgTownSeal.Top = 0F;
			this.imgTownSeal.Visible = false;
			this.imgTownSeal.Width = 0.875F;
			// 
			// fldHeader
			// 
			this.fldHeader.Height = 0.3125F;
			this.fldHeader.Left = 0F;
			this.fldHeader.Name = "fldHeader";
			this.fldHeader.Style = "font-family: \'Courier New\'; text-align: center";
			this.fldHeader.Text = null;
			this.fldHeader.Top = 0F;
			this.fldHeader.Width = 7.4375F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 0F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Courier New\'; text-align: left";
			this.lblAccount.Text = "99999999";
			this.lblAccount.Top = 0F;
			this.lblAccount.Width = 1.5F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.Visible = false;
			// 
			// arLienProcess
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.45F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			((System.ComponentModel.ISupportInitialize)(this.imgSig)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCosts2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCosts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCollector)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBreakdown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCosts1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgTownSeal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.RichTextBox rtbText;
		private GrapeCity.ActiveReports.SectionReportModel.Picture imgSig;
		private GrapeCity.ActiveReports.SectionReportModel.RichTextBox rtbText2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblInterest;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCosts2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCosts;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInterest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCollector;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMuni;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBreakdown;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCosts1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Picture imgTownSeal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
