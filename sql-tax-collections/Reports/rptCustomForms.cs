﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptCustomForms : BaseSectionReport
	{

		clsDRWrapper rsData = new clsDRWrapper();
		int intLabelWidth;
		bool boolDifferentPageSize;
		string strFont;
		bool boolPP;
		bool boolMort;
		int intType;
		string strBarCode = "";
		string strIMPBarcode = "";
		// kk09242015 trocls-63
		bool boolIMPB;
		//
		clsDRWrapper rsBC = new clsDRWrapper();
		bool boolUseCustomScreen;
		int lngLienFormType;
		//clsDRWrapper rsTest = new clsDRWrapper();
		float lngLineHeight;
		clsDRWrapper rsMort = new clsDRWrapper();
		string strLocalReportType;
		string strPassSQL;
		bool boolFailed;
		int intPrinterOption;
		string strExtraText;
		clsDRWrapper rsIntParty = new clsDRWrapper();

		public rptCustomForms()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Custom Forms";
		}

		public static rptCustomForms InstancePtr
		{
			get
			{
				return (rptCustomForms)Sys.GetInstance(typeof(rptCustomForms));
			}
		}

		protected rptCustomForms _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsMort.Dispose();
				rsIntParty.Dispose();
				rsBC.Dispose();
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// IF THIS IS THE END OF THE RECORDSET THEN WE NEED A WAY TO GET OUT
			eArgs.EOF = rsData.EndOfFile();
			//Detail_Format();
		}
		// VBto upgrade warning: intLabelType As short	OnWriteFCConvert.ToInt32(
		public void Init(string strSQL, string strPassReportType, int intLabelType, string strPrinterName, string strFonttoUse, int lngLienType, bool boolCustomScreen = true, string strPassDefaultPrinter = "", string strPassSortOrder = "", string strPassExtraText = "")
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intReturn/*unused?*/;
				int x/*unused?*/;
				bool boolUseFont/*unused?*/;
				int lngLeftAdjustment/*unused?*/;
				int lngTopAdjustment/*unused?*/;
				string strPrintOption = "";
				lngLineHeight = 240 / 1440f;
				strLocalReportType = strPassReportType;
				boolUseCustomScreen = boolCustomScreen;
				lngLienFormType = lngLienType;
				strPassSQL = strSQL;
				strExtraText = strPassExtraText;
				// kk09222015 trocls-63  Handle new CMF form with IMPBarcode and old 20 digit barcode - Added form to get and validate both numbers at once
				if (!frmGetStartingCMF.InstancePtr.Init(ref strBarCode, ref strIMPBarcode))
				{
					boolFailed = true;
					Cancel();
					if (boolUseCustomScreen)
					{
                        //FC:FINAL:SBE - Modeless forms should be displayed as MDI child of MainForm
                        //frmCustomLabels.InstancePtr.Show(FCForm.FormShowEnum.Modeless);
                        frmCustomLabels.InstancePtr.Show(App.MainForm);
                    }
					else
					{
                        //FC:FINAL:SBE - Modeless forms should be displayed as MDI child of MainForm
                        //frmReprintCMForm.InstancePtr.Show(FCForm.FormShowEnum.Modeless);
                        frmReprintCMForm.InstancePtr.Show(App.MainForm);
                    }
					return;
				}
				if (Strings.Trim(strIMPBarcode) != "")
				{
					boolIMPB = true;
				}
				else
				{
					boolIMPB = false;
				}
				strFont = strFonttoUse;
				
				intType = intLabelType;
				boolPP = false;
				boolMort = false;
				intPrinterOption = 7;
				// find out what types should be printed   ie.  Owners, Mortgage Holders and/or New Owners
				if (FCConvert.CBool(frmCustomLabels.InstancePtr.chkFormPrintOptions[0].CheckState != Wisej.Web.CheckState.Checked) || FCConvert.CBool(frmCustomLabels.InstancePtr.chkFormPrintOptions[1].CheckState != Wisej.Web.CheckState.Checked) || FCConvert.CBool(frmCustomLabels.InstancePtr.chkFormPrintOptions[2].CheckState != Wisej.Web.CheckState.Checked) || FCConvert.CBool(frmCustomLabels.InstancePtr.chkFormPrintOptions[3].CheckState != Wisej.Web.CheckState.Checked))
				{
					if (frmCustomLabels.InstancePtr.chkFormPrintOptions[0].CheckState == Wisej.Web.CheckState.Checked)
					{
						// Owner
						if (frmCustomLabels.InstancePtr.chkFormPrintOptions[1].CheckState == Wisej.Web.CheckState.Checked)
						{
							// MH
							if (frmCustomLabels.InstancePtr.chkFormPrintOptions[2].CheckState == Wisej.Web.CheckState.Checked)
							{
								// New Owner
								if (frmCustomLabels.InstancePtr.chkFormPrintOptions[3].CheckState == Wisej.Web.CheckState.Checked)
								{
									// Interested Party
									// this should not happen
								}
								else
								{
									// No Interested Party
									// Owner, MH and New Owner
									strPrintOption = "AND ISNULL(InterestedPartyID, 0) = 0";
									intPrinterOption = 8;
								}
							}
							else
							{
								// No New Owner
								if (frmCustomLabels.InstancePtr.chkFormPrintOptions[3].CheckState == Wisej.Web.CheckState.Checked)
								{
									// Owner, MH and Interested Party
									strPrintOption = "AND NOT ISNULL(NewOwner,0) = -1";
									intPrinterOption = 9;
								}
								else
								{
									// Owner and MH
									strPrintOption = "AND (NOT ISNULL(NewOwner,0) = -1 AND ISNULL(InterestedPartyID,0) = 0)";
									intPrinterOption = 3;
								}
							}
						}
						else
						{
							// No Mortgage Holder
							if (frmCustomLabels.InstancePtr.chkFormPrintOptions[2].CheckState == Wisej.Web.CheckState.Checked)
							{
								// New Owner
								if (frmCustomLabels.InstancePtr.chkFormPrintOptions[3].CheckState == Wisej.Web.CheckState.Checked)
								{
									// Owner, New Owner and Interested Party
									strPrintOption = "AND ISNULL(MortgageHolder,0) = 0";
									intPrinterOption = 5;
								}
								else
								{
									// @DJW 4/25/2008 Changed InterestedParty to InterestedPartyID
									// Owner and New Owner
									strPrintOption = "AND (ISNULL(MortgageHolder,0) = 0 AND ISNULL(InterestedPartyID,0) = 0)";
									intPrinterOption = 10;
								}
							}
							else
							{
								if (frmCustomLabels.InstancePtr.chkFormPrintOptions[3].CheckState == Wisej.Web.CheckState.Checked)
								{
									// Owner and Interested Party
									strPrintOption = "AND (NOT ISNULL(NewOwner,0) = -1 AND ISNULL(MortgageHolder,0) = 0)";
									intPrinterOption = 1;
								}
								else
								{
									// @DJW 4/25/2008 Changed InterestedParty to InterestedPartyID
									// Owner
									strPrintOption = "AND (NOT ISNULL(NewOwner,0) = -1 AND ISNULL(MortgageHolder,0) = 0 AND ISNULL(InterestedPartyID,0)= 0)";
									intPrinterOption = 11;
								}
							}
						}
					}
					else
					{
						// No Owner
						if (frmCustomLabels.InstancePtr.chkFormPrintOptions[1].CheckState == Wisej.Web.CheckState.Checked)
						{
							// MH
							if (frmCustomLabels.InstancePtr.chkFormPrintOptions[2].CheckState == Wisej.Web.CheckState.Checked)
							{
								// New Owner
								if (frmCustomLabels.InstancePtr.chkFormPrintOptions[3].CheckState == Wisej.Web.CheckState.Checked)
								{
									// MH and New Owner and Interested party
									strPrintOption = "AND (ISNULL(NewOwner,0) = -1 OR ISNULL(MortgageHolder,0) <> 0 OR ISNULL(InterestedPartyID,0) <> 0)";
									intPrinterOption = 6;
								}
								else
								{
									// No Interested Party
									// MH and New Owner
									strPrintOption = "AND (ISNULL(NewOwner,0) = -1 OR ISNULL(MortgageHolder,0) <> 0) AND ISNULL(InterestedPartyID,0) = 0";
									intPrinterOption = 12;
								}
							}
							else
							{
								// No New Owner
								if (frmCustomLabels.InstancePtr.chkFormPrintOptions[3].CheckState == Wisej.Web.CheckState.Checked)
								{
									// MH and Interested Party
									strPrintOption = "AND (ISNULL(MortgageHolder,0) <> 0 OR ISNULL(InterestedPartyID,0) <> 0)";
									intPrinterOption = 13;
								}
								else
								{
									// MH
									strPrintOption = "AND ISNULL(MortgageHolder,0) <> 0";
									intPrinterOption = 2;
								}
							}
						}
						else
						{
							// No Mortgage Holder
							if (frmCustomLabels.InstancePtr.chkFormPrintOptions[2].CheckState == Wisej.Web.CheckState.Checked)
							{
								// New Owner
								if (frmCustomLabels.InstancePtr.chkFormPrintOptions[3].CheckState == Wisej.Web.CheckState.Checked)
								{
									// New Owner and Interested Party
									strPrintOption = "AND (ISNULL(NewOwner,0) = -1 OR ISNULL(InterestedPartyID,0) <> 0)";
									intPrinterOption = 14;
								}
								else
								{
									// No Interested Party
									// New Owner
									strPrintOption = "AND ISNULL(NewOwner,0) = -1 AND ISNULL(InterestedPartyID,0) = 0";
									intPrinterOption = 4;
								}
							}
							else
							{
								// No New Owner
								if (frmCustomLabels.InstancePtr.chkFormPrintOptions[3].CheckState == Wisej.Web.CheckState.Checked)
								{
									// Interested Party
									strPrintOption = "AND ISNULL(InterestedPartyID,0) <> 0";
									intPrinterOption = 15;
								}
								else
								{
									// None
									strPrintOption = "AND Account = 0";
									intPrinterOption = 0;
								}
							}
						}
					}
				}
				else
				{
					// get all of the copies
				}
				// get the list of CMFnumber that must be printed because they match the SQL string passed into this form
				// if other CMF need to be printed then a reprint option will have to be used
				rsBC.OpenRecordset("SELECT * FROM CMFNumbers WHERE ISNULL(LabelOnly, 0) = 0 AND Type = " + FCConvert.ToString(lngLienFormType), modExtraModules.strCLDatabase);
				if (modStatusPayments.Statics.boolRE)
				{
					if (Strings.Trim(strPassSortOrder) != "")
					{
						rsData.OpenRecordset("SELECT * FROM ((" + strSQL.Replace("New.", "") + ") AS New INNER JOIN CMFNumbers ON New.Account = CMFNumbers.Account) WHERE ISNULL(LabelOnly,0) = 0 AND New.BillingType = 'RE' AND Type = " + FCConvert.ToString(lngLienFormType) + " " + strPrintOption + " ORDER BY " + strPassSortOrder + ", CMFNumbers.BillKey desc, NewOwner desc, ID", modExtraModules.strCLDatabase);
					}
					else
					{
						rsData.OpenRecordset("SELECT * FROM ((" + strSQL + ") AS New INNER JOIN CMFNumbers ON New.Account = CMFNumbers.Account) WHERE ISNULL(LabelOnly,0) = 0 AND New.BillingType = 'RE' AND Type = " + FCConvert.ToString(lngLienFormType) + strPrintOption + " ORDER BY NewOwner desc, MortgageHolder", modExtraModules.strCLDatabase);
					}
				}
				else
				{
					if (Strings.Trim(strPassSortOrder) != "")
					{
						rsData.OpenRecordset("SELECT * FROM ((" + strSQL.Replace("New.", "") + ") AS New INNER JOIN CMFNumbers ON New.Account = CMFNumbers.Account) WHERE ISNULL(LabelOnly,0) = 0 AND New.BillingType = 'PP' AND Type = " + FCConvert.ToString(lngLienFormType) + strPrintOption + " ORDER BY " + strPassSortOrder + ", CMFNumbers.BillKey desc, NewOwner desc, ID", modExtraModules.strCLDatabase);
						// , NewOwner desc, MortgageHolder, ID", strCLDatabase
					}
					else
					{
						rsData.OpenRecordset("SELECT * FROM ((" + strSQL + ") AS New INNER JOIN CMFNumbers ON New.Account = CMFNumbers.Account) WHERE ISNULL(LabelOnly,0) = 0 AND New.BillingType = 'PP' AND Type = " + FCConvert.ToString(lngLienFormType) + strPrintOption + " ORDER BY NewOwner desc, MortgageHolder", modExtraModules.strCLDatabase);
					}
				}
				rsMort.OpenRecordset("SELECT * FROM MortgageHolders", "CentralData");
				// rsIntParty.OpenRecordset "SELECT * FROM Owners WHERE ReceiveCopies = True AND (Module = 'RE' OR Module = 'CL')", strCLDatabase
				rsIntParty.OpenRecordset("SELECT * FROM Owners WHERE ReceiveCopies = 0 AND (ModuleCode = 'RE' AND ASSOCID = 0)", modExtraModules.strREDatabase);

				if (rsData.RecordCount() != 0)
					rsData.MoveFirst();
				boolDifferentPageSize = false;
				
							// Merit Certified Mail Form (Laser Format)
							this.Document.Printer.DefaultPageSettings.Landscape = true;
							PageSettings.Margins.Top = 0;
							PageSettings.Margins.Left = 0;
							PageSettings.Margins.Right = 0;
							//PageSettings.PaperSize = 255;
							PageSettings.PaperHeight = 15840 / 1440f;
							PageSettings.PaperWidth = 12240 / 1440f;
							PrintWidth = 14400 / 1440f;
						//	break;
						//}
				//}
				//end switch
				frmReportViewer.InstancePtr.Init(this, this.Document.Printer.PrinterName);
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Initializing Form");
			}
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (this.Document.Pages.Count > 0)
				{
					switch (lngLienFormType)
					{
						case 20:
							{
								modGlobalFunctions.IncrementSavedReports("LastCL30DayForms");
								this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastCL30DayForms1.RDF"));
								break;
							}
						case 21:
							{
								modGlobalFunctions.IncrementSavedReports("LastCLTransferToLienForms");
								this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastCLTransferToLienForms1.RDF"));
								break;
							}
						case 22:
							{
								modGlobalFunctions.IncrementSavedReports("LastCLLienMaturityForms");
								this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastCLLienMaturityForms1.RDF"));
								break;
							}
					}
					//end switch
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Saving Form");
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intReturn = 0;
				int const_PrintToolID;
				int cnt;
				lngLineHeight = 240 / 1440f;
				//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
				// override the print button so the print dialog doesn't come up again
				const_PrintToolID = 9950;
				//FC:TODO:AM
				//for (cnt = 0; cnt <= this.Toolbar.Tools.Count - 1; cnt++)
				//{
				//	if ("Print..." == this.Toolbar.Tools(cnt).Caption)
				//	{
				//		this.Toolbar.Tools(cnt).ID = const_PrintToolID;
				//		this.Toolbar.Tools(cnt).Enabled = true;
				//	}
				//} // cnt
				//FC:TEMP:AM: don't use the printer
				//if (modSysInfo.IsNTBased())
				//{
				//	// CheckDefaultPrinter Me.Document.Printer.PrinterName
				//	intReturn = modCustomPageSize.SelectForm("CLCustomLabel2", 0/*this.Handle.ToInt32()*/, FCConvert.ToInt32(PageSettings.PaperWidth * (25400.0 / 1440)), FCConvert.ToInt32(PageSettings.PaperHeight * (25400.0 / 1440)));
				//	if (intReturn > 0)
				//	{
				//		//this.Document.Printer.PaperSize = intReturn;
				//	}
				//	else
				//	{
				//		FCMessageBox.Show("Error.  Could not access form on printer.");
				//		Cancel();
				//		return;
				//	}
				//}
				//else
				//{
				//	this.Document.Printer.PaperSize = new System.Drawing.Printing.PaperSize("255", FCConvert.ToInt32(PageSettings.PaperWidth, FCConvert.ToInt32(PageSettings.PaperHeight);
				//	//255;
				//	//this.Document.Printer.PaperHeight = PageSettings.PaperHeight;
				//	//this.Document.Printer.PaperWidth = PageSettings.PaperWidth;
				//}
				//FC:FINAL:AM: moved code from Data_Initialize
				CreateDataFields();
				if (modGlobal.Statics.gdblCMFAdjustment != 0 && intType == 1)
				{
					// if there is a user adjustment and using a laser printer
					foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ctrl in this.Detail.Controls)
					{
						ctrl.Top += FCConvert.ToSingle(lngLineHeight * modGlobal.Statics.gdblCMFAdjustment);
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Starting Form");
			}
		}

		private void CreateDataFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intControlNumber/*unused?*/;
				GrapeCity.ActiveReports.SectionReportModel.TextBox NewField;
				/*unused?*/
				int intRow;
				int intCol/*unused?*/;
				int intNumber;
				int lngAccTop/*unused?*/;
				// CREATE THE CONTROLS AND SET THE POSITION OF THE CONTROLS
				intNumber = 17;
				for (intRow = 1; intRow <= intNumber; intRow++)
				{
					NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
					NewField.CanGrow = false;
					NewField.Name = "txtData" + FCConvert.ToString(intRow);
					switch (intType)
					{
						case 0:
							{
								if (intRow <= 7)
								{
									NewField.Top = FCConvert.ToSingle((((intRow - 1) * 225) + 3100) + (modGlobal.Statics.gdblCMFAdjustment * lngLineHeight)) / 1440f;
									NewField.Width = 5500 / 1440f;
									NewField.Left = 4200 / 1440f;
								}
								else
								{
									NewField.Top = FCConvert.ToSingle((((intRow - 2) * 225) + 9200) + (modGlobal.Statics.gdblCMFAdjustment * lngLineHeight)) / 1440f;
									NewField.Width = 5500 / 1440f;
									NewField.Left = 5000 / 1440f;
								}
								break;
							}
						case 1:
							{
								if (intRow <= 7)
								{
									NewField.Top = FCConvert.ToSingle((((intRow - 1) * 225) + 1700) + (modGlobal.Statics.gdblCMFAdjustment * lngLineHeight)) / 1440f;
									NewField.Width = 7500 / 1440f;
									NewField.Left = 4200 / 1440f;
								}
								else
								{
									NewField.Top = FCConvert.ToSingle((((intRow - 2) * 225) + 6300) + (modGlobal.Statics.gdblCMFAdjustment * lngLineHeight)) / 1440f;
									NewField.Width = 7000 / 1440f;
									NewField.Left = 5000 / 1440f;
								}
								break;
							}
					}
					//end switch
					//FC:FINAL:MSH - issue #1771: backcolor in original app will be changed only if we change BackStyle to ddBKNormal (by default is used ddBKTransparent)
					//NewField.BackColor = Color.Blue;
					NewField.Visible = true;
					if (intRow == 1)
					{
						NewField.Height = 550 / 1440f;
					}
					else
					{
						NewField.Height = 250 / 1440f;
					}
					NewField.Text = string.Empty;
					NewField.MultiLine = false;
					NewField.WordWrap = true;
					if (Strings.Trim(strFont) != string.Empty)
					{
						NewField.Font = new Font(strFont, NewField.Font.Size);
					}
					if (intRow > 7 && intRow < intNumber)
					{
						NewField.Font = new Font(NewField.Font.Name, NewField.Font.Size + 2);
					}
					if (intRow == intNumber - 1)
					{
						Detail.Height = NewField.Top + NewField.Height + 1000 / 1440f;
					}
					Detail.Controls.Add(NewField);
					// If intRow = 1 Then
					// lngAccTop = NewField.Top
					// End If
					// 
					// If intRow = intNumber Then
					// NewField.Top = lngAccTop - NewField.Height
					// NewField.Left = 4200
					// End If
				}
				// Add the extra text field at the bottom of the last address line
				// Set NewField = Detail.Controls.Add("DDActiveReports2.Field")
				// NewField.CanGrow = False
				// NewField.Name = "txtData" & intRow
				// 
				// Select Case intType
				// Case 0
				// If intRow <= 6 Then
				// NewField.Top = (((intRow - 1) * 225) + 3100) + (gdblCMFAdjustment * lngLineHeight)
				// NewField.Left = 4200
				// Else
				// NewField.Top = (((intRow - 2) * 225) + 9200) + (gdblCMFAdjustment * lngLineHeight)
				// NewField.Left = 5000
				// End If
				// Case 1
				// If intRow <= 6 Then
				// NewField.Top = (((intRow - 1) * 225) + 1700) + (gdblCMFAdjustment * lngLineHeight)
				// NewField.Left = 4200
				// Else
				// NewField.Top = (((intRow - 2) * 225) + 6300) + (gdblCMFAdjustment * lngLineHeight)
				// NewField.Left = 5000
				// End If
				// End Select
				// 
				// NewField.BackColor = vbBlue
				// NewField.Visible = True
				// NewField.Width = 6000 '550
				// NewField.Height = 550 '225
				// NewField.Text = vbNullString
				// NewField.MultiLine = True
				// NewField.WordWrap = True
				// 
				// intRow = intRow + 1
				// Add an extra text field to show the owner name when it is a mortgage holder
				// Set NewField = Detail.Controls.Add("DDActiveReports2.Field")
				// NewField.CanGrow = False
				// NewField.Name = "txtData" & intRow
				// 
				// Select Case intType
				// Case 0
				// NewField.Top = ((6 * 225) + 3100) + (gdblCMFAdjustment * lngLineHeight)
				// NewField.Left = 4200
				// Case 1
				// If intRow = 1 Then
				// NewField.Top = ((6 * 225) + 1700) + (gdblCMFAdjustment * lngLineHeight)
				// NewField.Left = 4200
				// Else
				// NewField.Top = ((7 * 225) + 1700) + (gdblCMFAdjustment * lngLineHeight)
				// NewField.Left = 4200
				// End If
				// End Select
				// 
				// NewField.BackColor = vbBlue
				// NewField.Visible = True
				// NewField.Width = 6000 '550
				// NewField.Height = 550 '225
				// NewField.Text = vbNullString
				// NewField.MultiLine = True
				// NewField.WordWrap = True
				// 
				// intRow = intRow + 1
				// Add an extra text field to show the owner name when it is a mortgage holder
				// Set NewField = Detail.Controls.Add("DDActiveReports2.Field")
				// NewField.CanGrow = False
				// NewField.Name = "txtData" & intRow
				// 
				// Select Case intType
				// Case 0
				// NewField.Top = ((7 * 225) + 3100) + (gdblCMFAdjustment * lngLineHeight)
				// NewField.Left = 4200
				// Case 1
				// If intRow = 1 Then
				// NewField.Top = ((7 * 225) + 1700) + (gdblCMFAdjustment * lngLineHeight)
				// NewField.Left = 4200
				// Else
				// NewField.Top = ((8 * 225) + 1700) + (gdblCMFAdjustment * lngLineHeight)
				// NewField.Left = 4200
				// End If
				// End Select
				// 
				// NewField.BackColor = vbBlue
				// NewField.Visible = True
				// NewField.Width = 6000 '550
				// NewField.Height = 550 '225
				// NewField.Text = vbNullString
				// NewField.MultiLine = True
				// NewField.WordWrap = True
				// 
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Creating Fields");
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			try
			{
				if (!boolFailed)
				{
					rptCertMailReport.InstancePtr.Init(FCConvert.ToInt16(lngLienFormType), FCConvert.ToInt16(intPrinterOption));
				}
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Initializing Report");
			}
		}

		private void PrintForms()
		{
			int lngErrCode = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill in the fields for labels
				int intControl;
				int intRow;
				string str1 = "";
				string str2 = "";
				string str3 = "";
				string str4 = "";
				string str5 = "";
				string str6 = "";
				string str7 = "";
				string str8 = "";
				string strOwner = "";
				string strU1 = "";
				string strU2 = "";
				string strU3 = "";
				string strU4 = "";
				string strU5 = "";
				string strU6 = "";
				string strU7 = "";
				string strAcct = "";
				clsDRWrapper rsTemp = new clsDRWrapper();
				lngErrCode = 1;
				if (rsData.EndOfFile() != true)
				{
					if (((rsData.Get_Fields_Int32("MortgageHolder"))) != 0)
					{
						boolMort = true;
					}
					else
					{
						boolMort = false;
					}
					str1 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name")));
					if (((rsData.Get_Fields_Int32("MortgageHolder"))) != 0)
					{
						strOwner = FCConvert.ToString(GetOwnerName_2(FCConvert.ToInt32(rsData.Get_Fields("Account"))));
						if (Strings.InStr(1, strOwner, " and ") != 0)
						{
							str2 = "RE:" + Strings.Left(strOwner, Strings.InStr(1, strOwner, " and "));
							str3 = "RE:" + Strings.Right(strOwner, strOwner.Length - Strings.InStr(1, strOwner, " and ") - 4);
						}
						else
						{
							str2 = Strings.Left("RE:" + FCConvert.ToString(GetOwnerName_2(FCConvert.ToInt32(rsData.Get_Fields("Account")))), 28);
							str3 = "";
						}
					}
					else
					{
						str2 = "";
						str3 = "";
					}
					str4 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address1")));
					str5 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address2")));
					str7 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address3")));
					str8 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address4")));
					str6 = strExtraText;
					// condense the labels if some are blank
					if (Strings.Trim(str7) == string.Empty)
					{
						lngErrCode = 100;
						str7 = str8;
						str8 = "";
					}
					if (Strings.Trim(str5) == string.Empty)
					{
						lngErrCode = 100;
						str5 = str7;
						str7 = str8;
						str8 = "";
					}
					if (Strings.Trim(str4) == string.Empty)
					{
						lngErrCode = 7;
						str4 = str5;
						str5 = str7;
						str7 = str8;
						str8 = "";
					}
					if (Strings.Trim(str3) == string.Empty)
					{
						lngErrCode = 8;
						str3 = str4;
						str4 = str5;
						str5 = str7;
						str7 = str8;
						str8 = "";
						// If boolMort Then
						// str4 = str5
						// str5 = ""
						// Else
						// str4 = ""
						// End If
					}
					if (Strings.Trim(str2) == string.Empty)
					{
						lngErrCode = 9;
						str2 = str3;
						str3 = str4;
						str4 = str5;
						str5 = str7;
						str7 = str8;
						str8 = "";
						// If boolMort Then
						// str4 = str5
						// str5 = ""
						// Else
						// str4 = ""
						// End If
					}
					if (Strings.Trim(str1) == string.Empty)
					{
						lngErrCode = 10;
						str1 = str2;
						str2 = str3;
						str3 = str4;
						str4 = str5;
						str5 = str7;
						str7 = str8;
						str8 = "";
						// If boolMort Then
						// str4 = str5
						// str5 = ""
						// Else
						// str4 = ""
						// End If
					}
					if (Strings.Trim(str4) != "" && Strings.Trim(str5) != "" && Strings.Trim(str7) != "" && Strings.Trim(str8) != "" && Strings.Trim(str1) != "" && Strings.Trim(str2) != "" && Strings.Trim(str3) != "")
					{
						if (boolMort || Strings.InStr(1, str1, " and ") <= 0)
						{
							strU1 = str1;
							strU2 = str2;
							strU3 = str3;
							strU4 = str4;
							strU5 = str5;
							strU6 = str7;
							strU7 = str8;
						}
						else
						{
							strU1 = Strings.Left(str1, Strings.InStr(1, str1, " and "));
							strU2 = str2;
							strU3 = str3;
							strU4 = str4;
							strU5 = str5;
							strU6 = str7;
							strU7 = str8;
						}
					}
					else
					{
						if (boolMort || Strings.InStr(1, str1, " and ") <= 0)
						{
							// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
							strU1 = FCConvert.ToString(rsData.Get_Fields("account"));
							strU2 = str1;
							strU3 = str2;
							strU4 = str3;
							strU5 = str4;
							strU6 = str5;
							strU7 = str7;
						}
						else
						{
							if (Strings.Trim(str7) != "")
							{
								strU1 = Strings.Left(str1, Strings.InStr(1, str1, " and "));
								strU2 = Strings.Right(str1, str1.Length - Strings.InStr(1, str1, " and ") - 4);
								strU3 = str2;
								strU4 = str3;
								strU5 = str4;
								strU6 = str5;
								strU7 = str7;
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								strU1 = FCConvert.ToString(rsData.Get_Fields("Account"));
								strU2 = Strings.Left(str1, Strings.InStr(1, str1, " and "));
								strU3 = Strings.Right(str1, str1.Length - Strings.InStr(1, str1, " and ") - 4);
								strU4 = str2;
								strU5 = str3;
								strU6 = str4;
								strU7 = str5;
							}
						}
					}
					lngErrCode = 11;
					for (intControl = 0; intControl <= Detail.Controls.Count - 1; intControl++)
					{
						if (Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) == "txtData")
						{
							intRow = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Detail.Controls[intControl].Name, 8))));
							GrapeCity.ActiveReports.SectionReportModel.TextBox txtData = (GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl];
							if (Strings.InStr(1, str1, " and ") != 0)
							{
								switch (intRow)
								{
									case 1:
										{
											// ((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = rsData.Fields("New.Account")
											txtData.Text = strU1;
											break;
										}
									case 2:
										{
											// ((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = Left(str1, InStr(1, str1, " and "))
											txtData.Text = strU2;
											break;
										}
									case 3:
										{
											// ((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = Right(str1, Len(str1) - InStr(1, str1, " and ") - 4)
											txtData.Text = strU3;
											break;
										}
									case 4:
										{
											// ((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = str2
											txtData.Text = strU4;
											break;
										}
									case 5:
										{
											// ((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = str3
											txtData.Text = strU5;
											break;
										}
									case 6:
										{
											// ((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = str4
											txtData.Text = strU6;
											break;
										}
									case 7:
										{
											// ((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = str5
											txtData.Text = strU7;
											break;
										}
									case 8:
										{
											//PPJ:FINAL:BCU #i204 - convert to string
											//txtData.Text = rsData.Get_Fields("Account");
											// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
											// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
											txtData.Text = FCConvert.ToString(rsData.Get_Fields("Account"));
											break;
										}
									case 9:
										{
											txtData.Text = Strings.Left(str1, Strings.InStr(1, str1, " and "));
											break;
										}
									case 10:
										{
											txtData.Text = Strings.Right(str1, str1.Length - Strings.InStr(1, str1, " and ") - 4);
											break;
										}
									case 11:
										{
											txtData.Text = str2;
											break;
										}
									case 12:
										{
											txtData.Text = str3;
											break;
										}
									case 13:
										{
											txtData.Text = str4;
											break;
										}
									case 14:
										{
											txtData.Text = str5;
											break;
										}
									case 15:
										{
											// If rsData.Fields("MortgageHolder") <> 0 Then
											// strOwner = GetOwnerName(rsData.Fields("New.Account"))
											// If InStr(1, strOwner, " and ") <> 0 Then
											// ((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = "For:" & Left(strOwner, InStr(1, strOwner, " and "))
											// Else
											// ((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = Left("For:" & GetOwnerName(rsData.Fields("New.Account")), 28)
											// End If
											// Else
											txtData.Text = str7;
											// End If
											break;
										}
									case 16:
										{
											// If rsData.Fields("MortgageHolder") <> 0 Then
											// strOwner = GetOwnerName(rsData.Fields("New.Account"))
											// If InStr(1, strOwner, " and ") <> 0 Then
											// ((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = "For:" & Right(strOwner, Len(strOwner) - InStr(1, strOwner, " and ") - 4)
											// Else
											// ((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = ""
											// End If
											// Else
											txtData.Text = str8;
											// End If
											break;
										}
									case 17:
										{
											txtData.Text = str6;
											break;
										}
								}
								//end switch
							}
							else
							{
								switch (intRow)
								{
									case 1:
										{
											// ((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = rsData.Fields("New.Account")
											txtData.Text = strU1;
											break;
										}
									case 2:
										{
											// ((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = str1
											txtData.Text = strU2;
											break;
										}
									case 3:
										{
											// ((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = str2
											txtData.Text = strU3;
											break;
										}
									case 4:
										{
											// ((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = str3
											txtData.Text = strU4;
											break;
										}
									case 5:
										{
											// ((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = str4
											txtData.Text = strU5;
											break;
										}
									case 6:
										{
											// ((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = str5
											txtData.Text = strU6;
											break;
										}
									case 7:
										{
											// ((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = ""
											txtData.Text = strU7;
											break;
										}
									case 8:
										{
											//PPJ:FINAL:BCU #i204 - convert to string
											//txtData.Text = rsData.Get_Fields("Account");
											// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
											// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
											txtData.Text = FCConvert.ToString(rsData.Get_Fields("Account"));
											break;
										}
									case 9:
										{
											txtData.Text = str1;
											break;
										}
									case 10:
										{
											txtData.Text = str2;
											break;
										}
									case 11:
										{
											txtData.Text = str3;
											break;
										}
									case 12:
										{
											txtData.Text = str4;
											break;
										}
									case 13:
										{
											txtData.Text = str5;
											break;
										}
									case 14:
										{
											// If rsData.Fields("MortgageHolder") <> 0 Then
											// strOwner = GetOwnerName(rsData.Fields("New.Account"))
											// If InStr(1, strOwner, " and ") <> 0 Then
											// ((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = "For:" & Left(strOwner, InStr(1, strOwner, " and "))
											// Else
											// ((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = Left("For:" & GetOwnerName(rsData.Fields("New.Account")), 28)
											// End If
											// Else
											txtData.Text = str7;
											// End If
											break;
										}
									case 15:
										{
											// If rsData.Fields("MortgageHolder") <> 0 Then
											// strOwner = GetOwnerName(rsData.Fields("New.Account"))
											// If InStr(1, strOwner, " and ") <> 0 Then
											// ((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = "For:" & Right(strOwner, Len(strOwner) - InStr(1, strOwner, " and ") - 4)
											// Else
											// ((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = ""
											// End If
											// Else
											txtData.Text = str8;
											// End If
											break;
										}
									case 16:
										{
											txtData.Text = "";
											break;
										}
									case 17:
										{
											txtData.Text = str6;
											break;
										}
								}
								//end switch
							}
						}
					}
					// intControl
					lngErrCode = 12;
					// Debug.Print rsData.Fields("New.Account") & " - " & rsData.Fields("Name1")
					// kgk        rsData.Edit    'True
					// rsData.Fields("CertifiedMailNumber") = strBarCode     'XXXXXXXXX
					// rsData.Update    'True
					// kk09242015 trocls-63  Add IMPB tracking number
					rsTemp.Execute("UPDATE BillingMaster SET CertifiedMailNumber = '" + Strings.Trim(strBarCode) + "', IMPBTrackingNumber = '" + Strings.Trim(strIMPBarcode) + "' WHERE ID = " + FCConvert.ToString(rsData.Get_Fields_Int32("ID")), modExtraModules.strCLDatabase);
					lngErrCode = 13;
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					SaveCMFNumber_8(FCConvert.ToInt32(rsData.Get_Fields("Account")), FCConvert.ToString(rsData.Get_Fields_String("Name1")), strBarCode, strIMPBarcode);
					lngErrCode = 14;
					strBarCode = modGlobalFunctions.GetNextMailFormCode(ref strBarCode);
					// kk09242015 trocls-63  Get the next IMPB Number in sequence
					if (boolIMPB)
					{
						strIMPBarcode = modGlobalFunctions.GetNextMailFormCode_6(strIMPBarcode, true);
					}
					lngErrCode = 15;
					rsData.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Printing Form - " + FCConvert.ToString(lngErrCode));
			}
		}

		private void SaveCMFNumber_8(int lngAcct, string strName, string strCMFNumber, string strIMPBNumber)
		{
			SaveCMFNumber(ref lngAcct, ref strName, ref strCMFNumber, ref strIMPBNumber);
		}

		private void SaveCMFNumber(ref int lngAcct, ref string strName, ref string strCMFNumber, ref string strIMPBNumber)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				rsBC.FindFirstRecord2("ID,Type", FCConvert.ToString(rsData.Get_Fields_Int32("ID")) + "," + FCConvert.ToString(lngLienFormType), ",");
				if (rsBC.NoMatch)
				{
					// no match?? this should never happen
				}
				else
				{
					// update the bar code number
					rsBC.Edit();
					// True
					rsBC.Set_Fields("BarCode", strCMFNumber);
					rsBC.Set_Fields("IMPBTrackingNumber", strIMPBNumber);
					rsBC.Set_Fields("Printed", true);
					rsBC.Update();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Saving CMF Number");
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (Strings.UCase(strLocalReportType) == "FORMS")
			{
				PrintForms();
			}
		}
		// VBto upgrade warning: 'Return' As object	OnWrite(string, object)
		private object GetOwnerName_2(int lngAcct)
		{
			return GetOwnerName(ref lngAcct);
		}

		private object GetOwnerName(ref int lngAcct)
		{
			object GetOwnerName = null;
			clsDRWrapper rsRE = new clsDRWrapper();
			rsRE.OpenRecordset("SELECT DeedName1 AS OwnerName, DeedName2 AS SecondOwnerName FROM Master WHERE RSAccount = " + FCConvert.ToString(lngAcct), modExtraModules.strREDatabase);
			if (!rsRE.EndOfFile())
			{
				if (Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("SecondOwnerName"))) != "")
				{
					GetOwnerName = Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("OwnerName"))) + " and " + Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("SecondOwnerName")));
				}
				else
				{
					GetOwnerName = rsRE.Get_Fields("OwnerName");
				}
			}
			rsRE.Dispose();
			return GetOwnerName;
		}

		
	}
}
