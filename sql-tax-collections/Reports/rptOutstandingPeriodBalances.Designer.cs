namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	partial class rptOutstandingPeriodBalances
	{
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptOutstandingPeriodBalances));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTaxDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPaymentReceived = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.fldTotalTaxDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalPaymentReceived = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lnTotals = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblTotals = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummary = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblSummary1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldSummary1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblSummaryHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblSummaryPaymentType1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryPaymentType2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryPaymentType3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryPaymentType4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryPaymentType5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryPaymentType6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryPaymentType7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryPaymentType8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryPaymentType9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryPaymentType10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryPaymentType11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lnSummaryTotal = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblSumPrin1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumPrin2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumPrin3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumPrin4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumPrin5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumPrin6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumPrin7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumPrin8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumPrin9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumPrin10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumPrin11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumHeaderType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumHeaderTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumHeaderPrin = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumHeaderInt = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumHeaderCost = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldAcctCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSumCount1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lnHeader = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTaxDue = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPaymentReceived = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDue = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReportType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTaxDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPaymentReceived)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTaxDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPaymentReceived)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummary)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummary1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSummary1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderPrin)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderInt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcctCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSumCount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTaxDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPaymentReceived)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
            //
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAccount,
				this.fldType,
				this.fldName,
				this.fldYear,
				this.fldTaxDue,
				this.fldPaymentReceived,
				this.fldDue
			});
			this.Detail.Height = 0.1979167F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldAccount
			// 
			this.fldAccount.Height = 0.1875F;
			this.fldAccount.Left = 0F;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldAccount.Text = null;
			this.fldAccount.Top = 0F;
			this.fldAccount.Width = 0.625F;
			// 
			// fldType
			// 
			this.fldType.Height = 0.1875F;
			this.fldType.Left = 0.625F;
			this.fldType.Name = "fldType";
			this.fldType.Style = "font-family: \'Tahoma\'; text-align: center";
			this.fldType.Text = null;
			this.fldType.Top = 0F;
			this.fldType.Width = 0.25F;
			// 
			// fldName
			// 
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 0.875F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'";
			this.fldName.Text = null;
			this.fldName.Top = 0F;
			this.fldName.Width = 2.4375F;
			// 
			// fldYear
			// 
			this.fldYear.Height = 0.1875F;
			this.fldYear.Left = 3.3125F;
			this.fldYear.Name = "fldYear";
			this.fldYear.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldYear.Text = null;
			this.fldYear.Top = 0F;
			this.fldYear.Width = 0.5F;
			// 
			// fldTaxDue
			// 
			this.fldTaxDue.Height = 0.1875F;
			this.fldTaxDue.Left = 3.8125F;
			this.fldTaxDue.Name = "fldTaxDue";
			this.fldTaxDue.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTaxDue.Text = "0.00";
			this.fldTaxDue.Top = 0F;
			this.fldTaxDue.Width = 1.0625F;
			// 
			// fldPaymentReceived
			// 
			this.fldPaymentReceived.Height = 0.1875F;
			this.fldPaymentReceived.Left = 4.875F;
			this.fldPaymentReceived.Name = "fldPaymentReceived";
			this.fldPaymentReceived.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldPaymentReceived.Text = "0.00";
			this.fldPaymentReceived.Top = 0F;
			this.fldPaymentReceived.Width = 1.0625F;
			// 
			// fldDue
			// 
			this.fldDue.Height = 0.1875F;
			this.fldDue.Left = 5.9375F;
			this.fldDue.Name = "fldDue";
			this.fldDue.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldDue.Text = "0.00";
			this.fldDue.Top = 0F;
			this.fldDue.Width = 1.0625F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
            //
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldTotalTaxDue,
				this.fldTotalPaymentReceived,
				this.fldTotalDue,
				this.lnTotals,
				this.lblTotals,
				this.lblSummary,
				this.Line1,
				this.lblSummary1,
				this.fldSummary1,
				this.lblSummaryHeader,
				this.Line2,
				this.lblSummaryPaymentType1,
				this.lblSummaryPaymentType2,
				this.lblSummaryPaymentType3,
				this.lblSummaryPaymentType4,
				this.lblSummaryPaymentType5,
				this.lblSummaryPaymentType6,
				this.lblSummaryPaymentType7,
				this.lblSummaryPaymentType8,
				this.lblSummaryPaymentType9,
				this.lblSummaryPaymentType10,
				this.lblSummaryPaymentType11,
				this.lblSummaryTotal1,
				this.lblSummaryTotal2,
				this.lblSummaryTotal3,
				this.lblSummaryTotal4,
				this.lblSummaryTotal5,
				this.lblSummaryTotal6,
				this.lblSummaryTotal7,
				this.lblSummaryTotal8,
				this.lblSummaryTotal9,
				this.lblSummaryTotal10,
				this.lblSummaryTotal11,
				this.lnSummaryTotal,
				this.lblSumPrin1,
				this.lblSumPrin2,
				this.lblSumPrin3,
				this.lblSumPrin4,
				this.lblSumPrin5,
				this.lblSumPrin6,
				this.lblSumPrin7,
				this.lblSumPrin8,
				this.lblSumPrin9,
				this.lblSumPrin10,
				this.lblSumPrin11,
				this.lblSumInt1,
				this.lblSumInt2,
				this.lblSumInt3,
				this.lblSumInt4,
				this.lblSumInt5,
				this.lblSumInt6,
				this.lblSumInt7,
				this.lblSumInt8,
				this.lblSumInt9,
				this.lblSumInt10,
				this.lblSumInt11,
				this.lblSumCost1,
				this.lblSumCost2,
				this.lblSumCost3,
				this.lblSumCost4,
				this.lblSumCost5,
				this.lblSumCost6,
				this.lblSumCost7,
				this.lblSumCost8,
				this.lblSumCost9,
				this.lblSumCost10,
				this.lblSumCost11,
				this.lblSumHeaderType,
				this.lblSumHeaderTotal,
				this.lblSumHeaderPrin,
				this.lblSumHeaderInt,
				this.lblSumHeaderCost,
				this.fldAcctCount,
				this.fldSumCount1
			});
			this.ReportFooter.Height = 3.489583F;
			this.ReportFooter.KeepTogether = true;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			// 
			// fldTotalTaxDue
			// 
			this.fldTotalTaxDue.Height = 0.1875F;
			this.fldTotalTaxDue.Left = 3.8125F;
			this.fldTotalTaxDue.Name = "fldTotalTaxDue";
			this.fldTotalTaxDue.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalTaxDue.Text = "0.00";
			this.fldTotalTaxDue.Top = 0F;
			this.fldTotalTaxDue.Width = 1.0625F;
			// 
			// fldTotalPaymentReceived
			// 
			this.fldTotalPaymentReceived.Height = 0.1875F;
			this.fldTotalPaymentReceived.Left = 4.875F;
			this.fldTotalPaymentReceived.Name = "fldTotalPaymentReceived";
			this.fldTotalPaymentReceived.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalPaymentReceived.Text = "0.00";
			this.fldTotalPaymentReceived.Top = 0F;
			this.fldTotalPaymentReceived.Width = 1.0625F;
			// 
			// fldTotalDue
			// 
			this.fldTotalDue.Height = 0.1875F;
			this.fldTotalDue.Left = 5.9375F;
			this.fldTotalDue.Name = "fldTotalDue";
			this.fldTotalDue.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalDue.Text = "0.00";
			this.fldTotalDue.Top = 0F;
			this.fldTotalDue.Width = 1.0625F;
			// 
			// lnTotals
			// 
			this.lnTotals.Height = 0F;
			this.lnTotals.Left = 2.875F;
			this.lnTotals.LineWeight = 1F;
			this.lnTotals.Name = "lnTotals";
			this.lnTotals.Top = 0F;
			this.lnTotals.Width = 4.125F;
			this.lnTotals.X1 = 2.875F;
			this.lnTotals.X2 = 7F;
			this.lnTotals.Y1 = 0F;
			this.lnTotals.Y2 = 0F;
			// 
			// lblTotals
			// 
			this.lblTotals.Height = 0.1875F;
			this.lblTotals.HyperLink = null;
			this.lblTotals.Left = 0.3125F;
			this.lblTotals.Name = "lblTotals";
			this.lblTotals.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblTotals.Text = "Total:";
			this.lblTotals.Top = 0F;
			this.lblTotals.Width = 1.8125F;
			// 
			// lblSummary
			// 
			this.lblSummary.Height = 0.1875F;
			this.lblSummary.HyperLink = null;
			this.lblSummary.Left = 0.3125F;
			this.lblSummary.Name = "lblSummary";
			this.lblSummary.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblSummary.Text = "Non Lien Summary";
			this.lblSummary.Top = 2.875F;
			this.lblSummary.Width = 2.25F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.3125F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 3.0625F;
			this.Line1.Width = 2.25F;
			this.Line1.X1 = 0.3125F;
			this.Line1.X2 = 2.5625F;
			this.Line1.Y1 = 3.0625F;
			this.Line1.Y2 = 3.0625F;
			// 
			// lblSummary1
			// 
			this.lblSummary1.Height = 0.1875F;
			this.lblSummary1.HyperLink = null;
			this.lblSummary1.Left = 0.3125F;
			this.lblSummary1.Name = "lblSummary1";
			this.lblSummary1.Style = "font-family: \'Tahoma\'; text-align: left";
			this.lblSummary1.Text = null;
			this.lblSummary1.Top = 3.0625F;
			this.lblSummary1.Width = 0.625F;
			// 
			// fldSummary1
			// 
			this.fldSummary1.Height = 0.1875F;
			this.fldSummary1.Left = 1.5625F;
			this.fldSummary1.Name = "fldSummary1";
			this.fldSummary1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldSummary1.Text = "0.00";
			this.fldSummary1.Top = 3.0625F;
			this.fldSummary1.Width = 1F;
			// 
			// lblSummaryHeader
			// 
			this.lblSummaryHeader.Height = 0.1875F;
			this.lblSummaryHeader.HyperLink = null;
			this.lblSummaryHeader.Left = 0.3125F;
			this.lblSummaryHeader.Name = "lblSummaryHeader";
			this.lblSummaryHeader.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblSummaryHeader.Text = "Payment Summary";
			this.lblSummaryHeader.Top = 0.3125F;
			this.lblSummaryHeader.Width = 6.25F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0.3125F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.5F;
			this.Line2.Width = 6.25F;
			this.Line2.X1 = 0.3125F;
			this.Line2.X2 = 6.5625F;
			this.Line2.Y1 = 0.5F;
			this.Line2.Y2 = 0.5F;
			// 
			// lblSummaryPaymentType1
			// 
			this.lblSummaryPaymentType1.Height = 0.1875F;
			this.lblSummaryPaymentType1.HyperLink = null;
			this.lblSummaryPaymentType1.Left = 0.3125F;
			this.lblSummaryPaymentType1.MultiLine = false;
			this.lblSummaryPaymentType1.Name = "lblSummaryPaymentType1";
			this.lblSummaryPaymentType1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType1.Text = null;
			this.lblSummaryPaymentType1.Top = 0.6875F;
			this.lblSummaryPaymentType1.Width = 2.5F;
			// 
			// lblSummaryPaymentType2
			// 
			this.lblSummaryPaymentType2.Height = 0.1875F;
			this.lblSummaryPaymentType2.HyperLink = null;
			this.lblSummaryPaymentType2.Left = 0.3125F;
			this.lblSummaryPaymentType2.MultiLine = false;
			this.lblSummaryPaymentType2.Name = "lblSummaryPaymentType2";
			this.lblSummaryPaymentType2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType2.Text = null;
			this.lblSummaryPaymentType2.Top = 0.875F;
			this.lblSummaryPaymentType2.Width = 2.5F;
			// 
			// lblSummaryPaymentType3
			// 
			this.lblSummaryPaymentType3.Height = 0.1875F;
			this.lblSummaryPaymentType3.HyperLink = null;
			this.lblSummaryPaymentType3.Left = 0.3125F;
			this.lblSummaryPaymentType3.MultiLine = false;
			this.lblSummaryPaymentType3.Name = "lblSummaryPaymentType3";
			this.lblSummaryPaymentType3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType3.Text = null;
			this.lblSummaryPaymentType3.Top = 1.0625F;
			this.lblSummaryPaymentType3.Width = 2.5F;
			// 
			// lblSummaryPaymentType4
			// 
			this.lblSummaryPaymentType4.Height = 0.1875F;
			this.lblSummaryPaymentType4.HyperLink = null;
			this.lblSummaryPaymentType4.Left = 0.3125F;
			this.lblSummaryPaymentType4.MultiLine = false;
			this.lblSummaryPaymentType4.Name = "lblSummaryPaymentType4";
			this.lblSummaryPaymentType4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType4.Text = null;
			this.lblSummaryPaymentType4.Top = 1.25F;
			this.lblSummaryPaymentType4.Width = 2.5F;
			// 
			// lblSummaryPaymentType5
			// 
			this.lblSummaryPaymentType5.Height = 0.1875F;
			this.lblSummaryPaymentType5.HyperLink = null;
			this.lblSummaryPaymentType5.Left = 0.3125F;
			this.lblSummaryPaymentType5.MultiLine = false;
			this.lblSummaryPaymentType5.Name = "lblSummaryPaymentType5";
			this.lblSummaryPaymentType5.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType5.Text = null;
			this.lblSummaryPaymentType5.Top = 1.4375F;
			this.lblSummaryPaymentType5.Width = 2.5F;
			// 
			// lblSummaryPaymentType6
			// 
			this.lblSummaryPaymentType6.Height = 0.1875F;
			this.lblSummaryPaymentType6.HyperLink = null;
			this.lblSummaryPaymentType6.Left = 0.3125F;
			this.lblSummaryPaymentType6.MultiLine = false;
			this.lblSummaryPaymentType6.Name = "lblSummaryPaymentType6";
			this.lblSummaryPaymentType6.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType6.Text = null;
			this.lblSummaryPaymentType6.Top = 1.625F;
			this.lblSummaryPaymentType6.Width = 2.5F;
			// 
			// lblSummaryPaymentType7
			// 
			this.lblSummaryPaymentType7.Height = 0.1875F;
			this.lblSummaryPaymentType7.HyperLink = null;
			this.lblSummaryPaymentType7.Left = 0.3125F;
			this.lblSummaryPaymentType7.MultiLine = false;
			this.lblSummaryPaymentType7.Name = "lblSummaryPaymentType7";
			this.lblSummaryPaymentType7.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType7.Text = null;
			this.lblSummaryPaymentType7.Top = 1.8125F;
			this.lblSummaryPaymentType7.Width = 2.5F;
			// 
			// lblSummaryPaymentType8
			// 
			this.lblSummaryPaymentType8.Height = 0.1875F;
			this.lblSummaryPaymentType8.HyperLink = null;
			this.lblSummaryPaymentType8.Left = 0.3125F;
			this.lblSummaryPaymentType8.MultiLine = false;
			this.lblSummaryPaymentType8.Name = "lblSummaryPaymentType8";
			this.lblSummaryPaymentType8.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType8.Text = null;
			this.lblSummaryPaymentType8.Top = 2F;
			this.lblSummaryPaymentType8.Width = 2.5F;
			// 
			// lblSummaryPaymentType9
			// 
			this.lblSummaryPaymentType9.Height = 0.1875F;
			this.lblSummaryPaymentType9.HyperLink = null;
			this.lblSummaryPaymentType9.Left = 0.3125F;
			this.lblSummaryPaymentType9.MultiLine = false;
			this.lblSummaryPaymentType9.Name = "lblSummaryPaymentType9";
			this.lblSummaryPaymentType9.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType9.Text = null;
			this.lblSummaryPaymentType9.Top = 2.1875F;
			this.lblSummaryPaymentType9.Width = 2.5F;
			// 
			// lblSummaryPaymentType10
			// 
			this.lblSummaryPaymentType10.Height = 0.1875F;
			this.lblSummaryPaymentType10.HyperLink = null;
			this.lblSummaryPaymentType10.Left = 0.3125F;
			this.lblSummaryPaymentType10.MultiLine = false;
			this.lblSummaryPaymentType10.Name = "lblSummaryPaymentType10";
			this.lblSummaryPaymentType10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType10.Text = null;
			this.lblSummaryPaymentType10.Top = 2.375F;
			this.lblSummaryPaymentType10.Width = 2.5F;
			// 
			// lblSummaryPaymentType11
			// 
			this.lblSummaryPaymentType11.Height = 0.1875F;
			this.lblSummaryPaymentType11.HyperLink = null;
			this.lblSummaryPaymentType11.Left = 0.3125F;
			this.lblSummaryPaymentType11.MultiLine = false;
			this.lblSummaryPaymentType11.Name = "lblSummaryPaymentType11";
			this.lblSummaryPaymentType11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType11.Text = null;
			this.lblSummaryPaymentType11.Top = 2.5625F;
			this.lblSummaryPaymentType11.Width = 2.5F;
			// 
			// lblSummaryTotal1
			// 
			this.lblSummaryTotal1.Height = 0.1875F;
			this.lblSummaryTotal1.HyperLink = null;
			this.lblSummaryTotal1.Left = 5.5625F;
			this.lblSummaryTotal1.Name = "lblSummaryTotal1";
			this.lblSummaryTotal1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal1.Text = null;
			this.lblSummaryTotal1.Top = 0.6875F;
			this.lblSummaryTotal1.Width = 1F;
			// 
			// lblSummaryTotal2
			// 
			this.lblSummaryTotal2.Height = 0.1875F;
			this.lblSummaryTotal2.HyperLink = null;
			this.lblSummaryTotal2.Left = 5.5625F;
			this.lblSummaryTotal2.Name = "lblSummaryTotal2";
			this.lblSummaryTotal2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal2.Text = null;
			this.lblSummaryTotal2.Top = 0.875F;
			this.lblSummaryTotal2.Width = 1F;
			// 
			// lblSummaryTotal3
			// 
			this.lblSummaryTotal3.Height = 0.1875F;
			this.lblSummaryTotal3.HyperLink = null;
			this.lblSummaryTotal3.Left = 5.5625F;
			this.lblSummaryTotal3.Name = "lblSummaryTotal3";
			this.lblSummaryTotal3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal3.Text = null;
			this.lblSummaryTotal3.Top = 1.0625F;
			this.lblSummaryTotal3.Width = 1F;
			// 
			// lblSummaryTotal4
			// 
			this.lblSummaryTotal4.Height = 0.1875F;
			this.lblSummaryTotal4.HyperLink = null;
			this.lblSummaryTotal4.Left = 5.5625F;
			this.lblSummaryTotal4.Name = "lblSummaryTotal4";
			this.lblSummaryTotal4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal4.Text = null;
			this.lblSummaryTotal4.Top = 1.25F;
			this.lblSummaryTotal4.Width = 1F;
			// 
			// lblSummaryTotal5
			// 
			this.lblSummaryTotal5.Height = 0.1875F;
			this.lblSummaryTotal5.HyperLink = null;
			this.lblSummaryTotal5.Left = 5.5625F;
			this.lblSummaryTotal5.Name = "lblSummaryTotal5";
			this.lblSummaryTotal5.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal5.Text = null;
			this.lblSummaryTotal5.Top = 1.4375F;
			this.lblSummaryTotal5.Width = 1F;
			// 
			// lblSummaryTotal6
			// 
			this.lblSummaryTotal6.Height = 0.1875F;
			this.lblSummaryTotal6.HyperLink = null;
			this.lblSummaryTotal6.Left = 5.5625F;
			this.lblSummaryTotal6.Name = "lblSummaryTotal6";
			this.lblSummaryTotal6.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal6.Text = null;
			this.lblSummaryTotal6.Top = 1.625F;
			this.lblSummaryTotal6.Width = 1F;
			// 
			// lblSummaryTotal7
			// 
			this.lblSummaryTotal7.Height = 0.1875F;
			this.lblSummaryTotal7.HyperLink = null;
			this.lblSummaryTotal7.Left = 5.5625F;
			this.lblSummaryTotal7.Name = "lblSummaryTotal7";
			this.lblSummaryTotal7.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal7.Text = null;
			this.lblSummaryTotal7.Top = 1.8125F;
			this.lblSummaryTotal7.Width = 1F;
			// 
			// lblSummaryTotal8
			// 
			this.lblSummaryTotal8.Height = 0.1875F;
			this.lblSummaryTotal8.HyperLink = null;
			this.lblSummaryTotal8.Left = 5.5625F;
			this.lblSummaryTotal8.Name = "lblSummaryTotal8";
			this.lblSummaryTotal8.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal8.Text = null;
			this.lblSummaryTotal8.Top = 2F;
			this.lblSummaryTotal8.Width = 1F;
			// 
			// lblSummaryTotal9
			// 
			this.lblSummaryTotal9.Height = 0.1875F;
			this.lblSummaryTotal9.HyperLink = null;
			this.lblSummaryTotal9.Left = 5.5625F;
			this.lblSummaryTotal9.Name = "lblSummaryTotal9";
			this.lblSummaryTotal9.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal9.Text = null;
			this.lblSummaryTotal9.Top = 2.1875F;
			this.lblSummaryTotal9.Width = 1F;
			// 
			// lblSummaryTotal10
			// 
			this.lblSummaryTotal10.Height = 0.1875F;
			this.lblSummaryTotal10.HyperLink = null;
			this.lblSummaryTotal10.Left = 5.5625F;
			this.lblSummaryTotal10.Name = "lblSummaryTotal10";
			this.lblSummaryTotal10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal10.Text = null;
			this.lblSummaryTotal10.Top = 2.375F;
			this.lblSummaryTotal10.Width = 1F;
			// 
			// lblSummaryTotal11
			// 
			this.lblSummaryTotal11.Height = 0.1875F;
			this.lblSummaryTotal11.HyperLink = null;
			this.lblSummaryTotal11.Left = 5.5625F;
			this.lblSummaryTotal11.Name = "lblSummaryTotal11";
			this.lblSummaryTotal11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal11.Text = null;
			this.lblSummaryTotal11.Top = 2.5625F;
			this.lblSummaryTotal11.Width = 1F;
			// 
			// lnSummaryTotal
			// 
			this.lnSummaryTotal.Height = 0F;
			this.lnSummaryTotal.Left = 2.8125F;
			this.lnSummaryTotal.LineWeight = 1F;
			this.lnSummaryTotal.Name = "lnSummaryTotal";
			this.lnSummaryTotal.Top = 2.5625F;
			this.lnSummaryTotal.Width = 3.75F;
			this.lnSummaryTotal.X1 = 2.8125F;
			this.lnSummaryTotal.X2 = 6.5625F;
			this.lnSummaryTotal.Y1 = 2.5625F;
			this.lnSummaryTotal.Y2 = 2.5625F;
			// 
			// lblSumPrin1
			// 
			this.lblSumPrin1.Height = 0.1875F;
			this.lblSumPrin1.HyperLink = null;
			this.lblSumPrin1.Left = 2.8125F;
			this.lblSumPrin1.Name = "lblSumPrin1";
			this.lblSumPrin1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin1.Text = null;
			this.lblSumPrin1.Top = 0.6875F;
			this.lblSumPrin1.Width = 1F;
			// 
			// lblSumPrin2
			// 
			this.lblSumPrin2.Height = 0.1875F;
			this.lblSumPrin2.HyperLink = null;
			this.lblSumPrin2.Left = 2.8125F;
			this.lblSumPrin2.Name = "lblSumPrin2";
			this.lblSumPrin2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin2.Text = null;
			this.lblSumPrin2.Top = 0.875F;
			this.lblSumPrin2.Width = 1F;
			// 
			// lblSumPrin3
			// 
			this.lblSumPrin3.Height = 0.1875F;
			this.lblSumPrin3.HyperLink = null;
			this.lblSumPrin3.Left = 2.8125F;
			this.lblSumPrin3.Name = "lblSumPrin3";
			this.lblSumPrin3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin3.Text = null;
			this.lblSumPrin3.Top = 1.0625F;
			this.lblSumPrin3.Width = 1F;
			// 
			// lblSumPrin4
			// 
			this.lblSumPrin4.Height = 0.1875F;
			this.lblSumPrin4.HyperLink = null;
			this.lblSumPrin4.Left = 2.8125F;
			this.lblSumPrin4.Name = "lblSumPrin4";
			this.lblSumPrin4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin4.Text = null;
			this.lblSumPrin4.Top = 1.25F;
			this.lblSumPrin4.Width = 1F;
			// 
			// lblSumPrin5
			// 
			this.lblSumPrin5.Height = 0.1875F;
			this.lblSumPrin5.HyperLink = null;
			this.lblSumPrin5.Left = 2.8125F;
			this.lblSumPrin5.Name = "lblSumPrin5";
			this.lblSumPrin5.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin5.Text = null;
			this.lblSumPrin5.Top = 1.4375F;
			this.lblSumPrin5.Width = 1F;
			// 
			// lblSumPrin6
			// 
			this.lblSumPrin6.Height = 0.1875F;
			this.lblSumPrin6.HyperLink = null;
			this.lblSumPrin6.Left = 2.8125F;
			this.lblSumPrin6.Name = "lblSumPrin6";
			this.lblSumPrin6.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin6.Text = null;
			this.lblSumPrin6.Top = 1.625F;
			this.lblSumPrin6.Width = 1F;
			// 
			// lblSumPrin7
			// 
			this.lblSumPrin7.Height = 0.1875F;
			this.lblSumPrin7.HyperLink = null;
			this.lblSumPrin7.Left = 2.8125F;
			this.lblSumPrin7.Name = "lblSumPrin7";
			this.lblSumPrin7.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin7.Text = null;
			this.lblSumPrin7.Top = 1.8125F;
			this.lblSumPrin7.Width = 1F;
			// 
			// lblSumPrin8
			// 
			this.lblSumPrin8.Height = 0.1875F;
			this.lblSumPrin8.HyperLink = null;
			this.lblSumPrin8.Left = 2.8125F;
			this.lblSumPrin8.Name = "lblSumPrin8";
			this.lblSumPrin8.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin8.Text = null;
			this.lblSumPrin8.Top = 2F;
			this.lblSumPrin8.Width = 1F;
			// 
			// lblSumPrin9
			// 
			this.lblSumPrin9.Height = 0.1875F;
			this.lblSumPrin9.HyperLink = null;
			this.lblSumPrin9.Left = 2.8125F;
			this.lblSumPrin9.Name = "lblSumPrin9";
			this.lblSumPrin9.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin9.Text = null;
			this.lblSumPrin9.Top = 2.1875F;
			this.lblSumPrin9.Width = 1F;
			// 
			// lblSumPrin10
			// 
			this.lblSumPrin10.Height = 0.1875F;
			this.lblSumPrin10.HyperLink = null;
			this.lblSumPrin10.Left = 2.8125F;
			this.lblSumPrin10.Name = "lblSumPrin10";
			this.lblSumPrin10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin10.Text = null;
			this.lblSumPrin10.Top = 2.375F;
			this.lblSumPrin10.Width = 1F;
			// 
			// lblSumPrin11
			// 
			this.lblSumPrin11.Height = 0.1875F;
			this.lblSumPrin11.HyperLink = null;
			this.lblSumPrin11.Left = 2.8125F;
			this.lblSumPrin11.Name = "lblSumPrin11";
			this.lblSumPrin11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin11.Text = null;
			this.lblSumPrin11.Top = 2.5625F;
			this.lblSumPrin11.Width = 1F;
			// 
			// lblSumInt1
			// 
			this.lblSumInt1.Height = 0.1875F;
			this.lblSumInt1.HyperLink = null;
			this.lblSumInt1.Left = 3.8125F;
			this.lblSumInt1.Name = "lblSumInt1";
			this.lblSumInt1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt1.Text = null;
			this.lblSumInt1.Top = 0.6875F;
			this.lblSumInt1.Width = 1F;
			// 
			// lblSumInt2
			// 
			this.lblSumInt2.Height = 0.1875F;
			this.lblSumInt2.HyperLink = null;
			this.lblSumInt2.Left = 3.8125F;
			this.lblSumInt2.Name = "lblSumInt2";
			this.lblSumInt2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt2.Text = null;
			this.lblSumInt2.Top = 0.875F;
			this.lblSumInt2.Width = 1F;
			// 
			// lblSumInt3
			// 
			this.lblSumInt3.Height = 0.1875F;
			this.lblSumInt3.HyperLink = null;
			this.lblSumInt3.Left = 3.8125F;
			this.lblSumInt3.Name = "lblSumInt3";
			this.lblSumInt3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt3.Text = null;
			this.lblSumInt3.Top = 1.0625F;
			this.lblSumInt3.Width = 1F;
			// 
			// lblSumInt4
			// 
			this.lblSumInt4.Height = 0.1875F;
			this.lblSumInt4.HyperLink = null;
			this.lblSumInt4.Left = 3.8125F;
			this.lblSumInt4.Name = "lblSumInt4";
			this.lblSumInt4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt4.Text = null;
			this.lblSumInt4.Top = 1.25F;
			this.lblSumInt4.Width = 1F;
			// 
			// lblSumInt5
			// 
			this.lblSumInt5.Height = 0.1875F;
			this.lblSumInt5.HyperLink = null;
			this.lblSumInt5.Left = 3.8125F;
			this.lblSumInt5.Name = "lblSumInt5";
			this.lblSumInt5.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt5.Text = null;
			this.lblSumInt5.Top = 1.4375F;
			this.lblSumInt5.Width = 1F;
			// 
			// lblSumInt6
			// 
			this.lblSumInt6.Height = 0.1875F;
			this.lblSumInt6.HyperLink = null;
			this.lblSumInt6.Left = 3.8125F;
			this.lblSumInt6.Name = "lblSumInt6";
			this.lblSumInt6.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt6.Text = null;
			this.lblSumInt6.Top = 1.625F;
			this.lblSumInt6.Width = 1F;
			// 
			// lblSumInt7
			// 
			this.lblSumInt7.Height = 0.1875F;
			this.lblSumInt7.HyperLink = null;
			this.lblSumInt7.Left = 3.8125F;
			this.lblSumInt7.Name = "lblSumInt7";
			this.lblSumInt7.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt7.Text = null;
			this.lblSumInt7.Top = 1.8125F;
			this.lblSumInt7.Width = 1F;
			// 
			// lblSumInt8
			// 
			this.lblSumInt8.Height = 0.1875F;
			this.lblSumInt8.HyperLink = null;
			this.lblSumInt8.Left = 3.8125F;
			this.lblSumInt8.Name = "lblSumInt8";
			this.lblSumInt8.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt8.Text = null;
			this.lblSumInt8.Top = 2F;
			this.lblSumInt8.Width = 1F;
			// 
			// lblSumInt9
			// 
			this.lblSumInt9.Height = 0.1875F;
			this.lblSumInt9.HyperLink = null;
			this.lblSumInt9.Left = 3.8125F;
			this.lblSumInt9.Name = "lblSumInt9";
			this.lblSumInt9.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt9.Text = null;
			this.lblSumInt9.Top = 2.1875F;
			this.lblSumInt9.Width = 1F;
			// 
			// lblSumInt10
			// 
			this.lblSumInt10.Height = 0.1875F;
			this.lblSumInt10.HyperLink = null;
			this.lblSumInt10.Left = 3.8125F;
			this.lblSumInt10.Name = "lblSumInt10";
			this.lblSumInt10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt10.Text = null;
			this.lblSumInt10.Top = 2.375F;
			this.lblSumInt10.Width = 1F;
			// 
			// lblSumInt11
			// 
			this.lblSumInt11.Height = 0.1875F;
			this.lblSumInt11.HyperLink = null;
			this.lblSumInt11.Left = 3.8125F;
			this.lblSumInt11.Name = "lblSumInt11";
			this.lblSumInt11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt11.Text = null;
			this.lblSumInt11.Top = 2.5625F;
			this.lblSumInt11.Width = 1F;
			// 
			// lblSumCost1
			// 
			this.lblSumCost1.Height = 0.1875F;
			this.lblSumCost1.HyperLink = null;
			this.lblSumCost1.Left = 4.8125F;
			this.lblSumCost1.Name = "lblSumCost1";
			this.lblSumCost1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost1.Text = null;
			this.lblSumCost1.Top = 0.6875F;
			this.lblSumCost1.Width = 0.75F;
			// 
			// lblSumCost2
			// 
			this.lblSumCost2.Height = 0.1875F;
			this.lblSumCost2.HyperLink = null;
			this.lblSumCost2.Left = 4.8125F;
			this.lblSumCost2.Name = "lblSumCost2";
			this.lblSumCost2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost2.Text = null;
			this.lblSumCost2.Top = 0.875F;
			this.lblSumCost2.Width = 0.75F;
			// 
			// lblSumCost3
			// 
			this.lblSumCost3.Height = 0.1875F;
			this.lblSumCost3.HyperLink = null;
			this.lblSumCost3.Left = 4.8125F;
			this.lblSumCost3.Name = "lblSumCost3";
			this.lblSumCost3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost3.Text = null;
			this.lblSumCost3.Top = 1.0625F;
			this.lblSumCost3.Width = 0.75F;
			// 
			// lblSumCost4
			// 
			this.lblSumCost4.Height = 0.1875F;
			this.lblSumCost4.HyperLink = null;
			this.lblSumCost4.Left = 4.8125F;
			this.lblSumCost4.Name = "lblSumCost4";
			this.lblSumCost4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost4.Text = null;
			this.lblSumCost4.Top = 1.25F;
			this.lblSumCost4.Width = 0.75F;
			// 
			// lblSumCost5
			// 
			this.lblSumCost5.Height = 0.1875F;
			this.lblSumCost5.HyperLink = null;
			this.lblSumCost5.Left = 4.8125F;
			this.lblSumCost5.Name = "lblSumCost5";
			this.lblSumCost5.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost5.Text = null;
			this.lblSumCost5.Top = 1.4375F;
			this.lblSumCost5.Width = 0.75F;
			// 
			// lblSumCost6
			// 
			this.lblSumCost6.Height = 0.1875F;
			this.lblSumCost6.HyperLink = null;
			this.lblSumCost6.Left = 4.8125F;
			this.lblSumCost6.Name = "lblSumCost6";
			this.lblSumCost6.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost6.Text = null;
			this.lblSumCost6.Top = 1.625F;
			this.lblSumCost6.Width = 0.75F;
			// 
			// lblSumCost7
			// 
			this.lblSumCost7.Height = 0.1875F;
			this.lblSumCost7.HyperLink = null;
			this.lblSumCost7.Left = 4.8125F;
			this.lblSumCost7.Name = "lblSumCost7";
			this.lblSumCost7.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost7.Text = null;
			this.lblSumCost7.Top = 1.8125F;
			this.lblSumCost7.Width = 0.75F;
			// 
			// lblSumCost8
			// 
			this.lblSumCost8.Height = 0.1875F;
			this.lblSumCost8.HyperLink = null;
			this.lblSumCost8.Left = 4.8125F;
			this.lblSumCost8.Name = "lblSumCost8";
			this.lblSumCost8.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost8.Text = null;
			this.lblSumCost8.Top = 2F;
			this.lblSumCost8.Width = 0.75F;
			// 
			// lblSumCost9
			// 
			this.lblSumCost9.Height = 0.1875F;
			this.lblSumCost9.HyperLink = null;
			this.lblSumCost9.Left = 4.8125F;
			this.lblSumCost9.Name = "lblSumCost9";
			this.lblSumCost9.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost9.Text = null;
			this.lblSumCost9.Top = 2.1875F;
			this.lblSumCost9.Width = 0.75F;
			// 
			// lblSumCost10
			// 
			this.lblSumCost10.Height = 0.1875F;
			this.lblSumCost10.HyperLink = null;
			this.lblSumCost10.Left = 4.8125F;
			this.lblSumCost10.Name = "lblSumCost10";
			this.lblSumCost10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost10.Text = null;
			this.lblSumCost10.Top = 2.375F;
			this.lblSumCost10.Width = 0.75F;
			// 
			// lblSumCost11
			// 
			this.lblSumCost11.Height = 0.1875F;
			this.lblSumCost11.HyperLink = null;
			this.lblSumCost11.Left = 4.8125F;
			this.lblSumCost11.Name = "lblSumCost11";
			this.lblSumCost11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost11.Text = null;
			this.lblSumCost11.Top = 2.5625F;
			this.lblSumCost11.Width = 0.75F;
			// 
			// lblSumHeaderType
			// 
			this.lblSumHeaderType.Height = 0.1875F;
			this.lblSumHeaderType.HyperLink = null;
			this.lblSumHeaderType.Left = 0.3125F;
			this.lblSumHeaderType.MultiLine = false;
			this.lblSumHeaderType.Name = "lblSumHeaderType";
			this.lblSumHeaderType.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSumHeaderType.Text = null;
			this.lblSumHeaderType.Top = 0.5F;
			this.lblSumHeaderType.Width = 2.5F;
			// 
			// lblSumHeaderTotal
			// 
			this.lblSumHeaderTotal.Height = 0.1875F;
			this.lblSumHeaderTotal.HyperLink = null;
			this.lblSumHeaderTotal.Left = 5.5625F;
			this.lblSumHeaderTotal.Name = "lblSumHeaderTotal";
			this.lblSumHeaderTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumHeaderTotal.Text = null;
			this.lblSumHeaderTotal.Top = 0.5F;
			this.lblSumHeaderTotal.Width = 1F;
			// 
			// lblSumHeaderPrin
			// 
			this.lblSumHeaderPrin.Height = 0.1875F;
			this.lblSumHeaderPrin.HyperLink = null;
			this.lblSumHeaderPrin.Left = 2.8125F;
			this.lblSumHeaderPrin.Name = "lblSumHeaderPrin";
			this.lblSumHeaderPrin.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumHeaderPrin.Text = null;
			this.lblSumHeaderPrin.Top = 0.5F;
			this.lblSumHeaderPrin.Width = 1F;
			// 
			// lblSumHeaderInt
			// 
			this.lblSumHeaderInt.Height = 0.1875F;
			this.lblSumHeaderInt.HyperLink = null;
			this.lblSumHeaderInt.Left = 3.8125F;
			this.lblSumHeaderInt.Name = "lblSumHeaderInt";
			this.lblSumHeaderInt.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumHeaderInt.Text = null;
			this.lblSumHeaderInt.Top = 0.5F;
			this.lblSumHeaderInt.Width = 1F;
			// 
			// lblSumHeaderCost
			// 
			this.lblSumHeaderCost.Height = 0.1875F;
			this.lblSumHeaderCost.HyperLink = null;
			this.lblSumHeaderCost.Left = 4.8125F;
			this.lblSumHeaderCost.Name = "lblSumHeaderCost";
			this.lblSumHeaderCost.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumHeaderCost.Text = null;
			this.lblSumHeaderCost.Top = 0.5F;
			this.lblSumHeaderCost.Width = 0.75F;
			// 
			// fldAcctCount
			// 
			this.fldAcctCount.Height = 0.1875F;
			this.fldAcctCount.Left = 2.125F;
			this.fldAcctCount.Name = "fldAcctCount";
			this.fldAcctCount.Style = "font-family: \'Tahoma\'; text-align: left";
			this.fldAcctCount.Text = "1 Account";
			this.fldAcctCount.Top = 0F;
			this.fldAcctCount.Width = 1.25F;
			// 
			// fldSumCount1
			// 
			this.fldSumCount1.Height = 0.1875F;
			this.fldSumCount1.Left = 1.0625F;
			this.fldSumCount1.Name = "fldSumCount1";
			this.fldSumCount1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldSumCount1.Text = null;
			this.fldSumCount1.Top = 3.0625F;
			this.fldSumCount1.Width = 0.375F;
			// 
			// PageHeader
			// 
			this.PageHeader.CanGrow = false;
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblHeader,
				this.lblDate,
				this.lblPage,
				this.lblTime,
				this.lblMuniName,
				this.lnHeader,
				this.lblAccount,
				this.lblYear,
				this.lblTaxDue,
				this.lblPaymentReceived,
				this.lblDue,
				this.lblReportType,
				this.lblName
			});
			this.PageHeader.Height = 1.1875F;
			this.PageHeader.Name = "PageHeader";
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.1875F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblHeader.Text = "Outstanding Balance By Period";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 7F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 5.875F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1.125F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 5.875F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblPage.Text = null;
			this.lblPage.Top = 0.1875F;
			this.lblPage.Width = 1.125F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1875F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.1875F;
			this.lblTime.Width = 1.125F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 2.625F;
			// 
			// lnHeader
			// 
			this.lnHeader.Height = 0F;
			this.lnHeader.Left = 0F;
			this.lnHeader.LineWeight = 1F;
			this.lnHeader.Name = "lnHeader";
			this.lnHeader.Top = 1.1875F;
			this.lnHeader.Width = 6.9375F;
			this.lnHeader.X1 = 0F;
			this.lnHeader.X2 = 6.9375F;
			this.lnHeader.Y1 = 1.1875F;
			this.lnHeader.Y2 = 1.1875F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 0F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblAccount.Text = "Acct";
			this.lblAccount.Top = 1F;
			this.lblAccount.Width = 0.625F;
			// 
			// lblYear
			// 
			this.lblYear.Height = 0.1875F;
			this.lblYear.HyperLink = null;
			this.lblYear.Left = 3.3125F;
			this.lblYear.Name = "lblYear";
			this.lblYear.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblYear.Text = "Year";
			this.lblYear.Top = 1F;
			this.lblYear.Width = 0.5F;
			// 
			// lblTaxDue
			// 
			this.lblTaxDue.Height = 0.375F;
			this.lblTaxDue.HyperLink = null;
			this.lblTaxDue.Left = 4.0625F;
			this.lblTaxDue.Name = "lblTaxDue";
			this.lblTaxDue.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblTaxDue.Text = "Original Tax";
			this.lblTaxDue.Top = 0.8125F;
			this.lblTaxDue.Width = 0.8125F;
			// 
			// lblPaymentReceived
			// 
			this.lblPaymentReceived.Height = 0.375F;
			this.lblPaymentReceived.HyperLink = null;
			this.lblPaymentReceived.Left = 5F;
			this.lblPaymentReceived.Name = "lblPaymentReceived";
			this.lblPaymentReceived.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblPaymentReceived.Text = "Payment / Adjustments";
			this.lblPaymentReceived.Top = 0.8125F;
			this.lblPaymentReceived.Width = 0.9375F;
			// 
			// lblDue
			// 
			this.lblDue.Height = 0.375F;
			this.lblDue.HyperLink = null;
			this.lblDue.Left = 6.25F;
			this.lblDue.Name = "lblDue";
			this.lblDue.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblDue.Text = "Amount Due";
			this.lblDue.Top = 0.8125F;
			this.lblDue.Width = 0.75F;
			// 
			// lblReportType
			// 
			this.lblReportType.Height = 0.625F;
			this.lblReportType.HyperLink = null;
			this.lblReportType.Left = 0F;
			this.lblReportType.Name = "lblReportType";
			this.lblReportType.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
			this.lblReportType.Text = "Report Type";
			this.lblReportType.Top = 0.1875F;
			this.lblReportType.Width = 7F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.1875F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 0.875F;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblName.Text = "Name ----";
			this.lblName.Top = 1F;
			this.lblName.Width = 1F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// rptOutstandingPeriodBalances
            //
			// 
			this.PageEnd += new System.EventHandler(this.ActiveReport_PageEnd);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTaxDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPaymentReceived)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTaxDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPaymentReceived)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummary)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummary1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSummary1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderPrin)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderInt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcctCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSumCount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTaxDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPaymentReceived)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTaxDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPaymentReceived;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDue;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTaxDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPaymentReceived;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalDue;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnTotals;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotals;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummary;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummary1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSummary1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType10;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType11;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal10;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal11;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnSummaryTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin10;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin11;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt10;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt11;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost10;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost11;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumHeaderType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumHeaderTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumHeaderPrin;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumHeaderInt;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumHeaderCost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAcctCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSumCount1;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTaxDue;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPaymentReceived;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDue;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReportType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}