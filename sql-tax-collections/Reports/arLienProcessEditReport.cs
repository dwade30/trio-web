﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class arLienProcessEditReport : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/26/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               06/12/2006              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsLN = new clsDRWrapper();
		clsDRWrapper rsRE = new clsDRWrapper();
		//clsDRWrapper rsBook = new clsDRWrapper();
		string strSQL = "";
		double[] dblTotals = new double[3 + 1];
		int intYear;
		// VBto upgrade warning: dblMinimumAmount As double	OnWrite(string)
		double dblMinimumAmount;
		int lngPosBal;
		int lngZeroBal;
		int lngNegBal;
		int lngDemandAlready;
		DateTime dtMailDate;
		public int intReportDetail;
		int intLienAccts;
		string strReportDesc = "";
		int lngNumOfAccounts;
		string strRateKeyList = "";
		bool boolShowMapLot;
		bool boolShowBookPage;
		bool boolShowLocation;
		bool boolShowMailingAddress;
		int lngExtraLines;

		public arLienProcessEditReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Lien Process Edit Report";
		}

		public static arLienProcessEditReport InstancePtr
		{
			get
			{
				return (arLienProcessEditReport)Sys.GetInstance(typeof(arLienProcessEditReport));
			}
		}

		protected arLienProcessEditReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsRE.Dispose();
				rsData.Dispose();
				rsLN.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}
		// VBto upgrade warning: intLienedAccts As short	OnWriteFCConvert.ToInt32(
		public void StartLienEditReportQuery(short intLienedAccts, string strRateList = "")
		{
			try
			{
				this.Name = "Lien Process Edit Report";
				// On Error GoTo ERROR_HANDLER
				intYear = frmRateRecChoice.InstancePtr.intYear;
				// GetYearFromUser(Year(Now))
				dblMinimumAmount = FCConvert.ToDouble(frmRateRecChoice.InstancePtr.txtMinimumAmount.Text);
				intLienAccts = intLienedAccts;
				// build the SQL statement
				if (intYear > 0)
				{
					strRateKeyList = strRateList;
					frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Data");
					//Application.DoEvents();
					strSQL = BuildSQL();
					rsData.OpenRecordset(strSQL);
					if (rsData.EndOfFile())
					{
						lblAccount.Left = 0;
						lblAccount.Width = this.PrintWidth;
						lblAccount.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						lblAccount.Text = "No records for the year " + FCConvert.ToString(intYear) + " were found.";
						HideAllFields();
					}
					lngNumOfAccounts = 0;
					frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Data", true, rsData.RecordCount() + 1);
					dtMailDate = DateAndTime.DateValue(frmRateRecChoice.InstancePtr.txtMailDate.Text);
					intReportDetail = frmRateRecChoice.InstancePtr.cmbReportDetail.ItemData(frmRateRecChoice.InstancePtr.cmbReportDetail.SelectedIndex);
					lblReportType.Text = "Year : " + frmRateRecChoice.InstancePtr.cmbYear.Items[frmRateRecChoice.InstancePtr.cmbYear.SelectedIndex].ToString() + "  Interest as of " + Strings.Format(dtMailDate, "MM/dd/yyyy") + "\r\n" + strReportDesc;
					rsLN.OpenRecordset("SELECT * FROM LienRec", modExtraModules.strCLDatabase);
					frmWait.InstancePtr.IncrementProgress();
					// if the user has Real Estate, then use the information from the database
					rsRE.OpenRecordset("SELECT * FROM Master", modExtraModules.strREDatabase);
					rsRE.InsertName("OwnerPartyID,SecOwnerPartyID", "Own1,Own2", false, true, true, "", false, "", true, "");
					//Application.DoEvents();
					// Me.Show , MDIParent
					frmWait.InstancePtr.Show(FCForm.FormShowEnum.Modeless);
					//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
				}
				else
				{
					Cancel();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Start Lien Edit Report Error");
			}
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// format the report
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "t");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			lngPosBal = 0;
			lngZeroBal = 0;
			lngNegBal = 0;
			lngDemandAlready = 0;
			boolShowMapLot = FCConvert.CBool(frmRateRecChoice.InstancePtr.chkPrint[0].CheckState == Wisej.Web.CheckState.Checked);
			boolShowBookPage = FCConvert.CBool(frmRateRecChoice.InstancePtr.chkPrint[1].CheckState == Wisej.Web.CheckState.Checked);
			boolShowLocation = FCConvert.CBool(frmRateRecChoice.InstancePtr.chkPrint[2].CheckState == Wisej.Web.CheckState.Checked);
			boolShowMailingAddress = FCConvert.CBool(frmRateRecChoice.InstancePtr.chkPrint[3].CheckState == Wisej.Web.CheckState.Checked);
			//FC:FINAL:AM:#i43 - check the index
			if (frmRateRecChoice.InstancePtr.cmbExtraLines.SelectedIndex != -1)
			{
				lngExtraLines = FCConvert.ToInt32(Math.Round(Conversion.Val(frmRateRecChoice.InstancePtr.cmbExtraLines.Items[frmRateRecChoice.InstancePtr.cmbExtraLines.SelectedIndex].ToString())));
			}
			SetupDetailSection();
			lblFooter.Left = 0;
			lblFooter.Width = this.PrintWidth - 10 / 1440F;
			lblFooter.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
			lblFooter.Text = "* - This account is currently liened.";
			lblBankruptDisclaimer.Left = 0;
			lblBankruptDisclaimer.Width = this.PrintWidth - 10 / 1440F;
			lblBankruptDisclaimer.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
			lblBankruptDisclaimer.Text = "Account numbers in parentheses are flagged as being in bankruptcy.";
			switch (intLienAccts)
			{
				case 0:
				case 2:
				case 5:
					{
						lblFooter.Visible = true;
						break;
					}
				case 1:
				case 3:
				case 4:
					{
						lblFooter.Visible = false;
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			frmWait.InstancePtr.Unload();
            // Unload frmRateRecChoice
            //FC:FINAL:SBE - Modeless forms should be displayed as MDI child of MainForm
            //frmRateRecChoice.InstancePtr.Show(FCForm.FormShowEnum.Modeless);
            frmRateRecChoice.InstancePtr.Show(App.MainForm);
            // DoEvents
            // frmRateRecChoice.SetFocus
        }

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private string BuildSQL()
		{
			string BuildSQL = "";
			// this will return the correct SQL string
			string strFields = "";
			string strWhereClause = "";
			if (frmRateRecChoice.InstancePtr.cmbRange.SelectedIndex == 2)
			{
				// range of accounts
				if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[0].Text) != "")
				{
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
					{
						// both full
						strWhereClause = " AND Account >= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text)) + " AND Account <= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
						strReportDesc += "Account " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text)) + " To " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
					}
					else
					{
						// first full second empty
						strWhereClause = " AND Account >= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text));
						strReportDesc += "Account Above " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text));
					}
				}
				else
				{
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
					{
						// first empty second full
						strWhereClause = " AND Account <= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
						strReportDesc += "Account Below " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
					}
					else
					{
						// both empty
						strWhereClause = "";
						strReportDesc += "";
					}
				}
			}
			else if (frmRateRecChoice.InstancePtr.cmbRange.SelectedIndex == 1)
			{
				// range of names
				if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[0].Text) != "")
				{
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
					{
						// both full
						// strWhereClause = " AND Name1 BETWEEN '" & .txtRange(0).Text & "' AND '" & .txtRange(1).Text & "'"
						strWhereClause = " AND Name1 >= '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "   ' AND Name1 <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "ZZZ'";
						strReportDesc += "Name " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text)) + " To " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
					}
					else
					{
						// first full second empty
						strWhereClause = " AND Name1 >= '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "  '";
						strReportDesc += "Name Above " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text));
					}
				}
				else
				{
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
					{
						// first empty second full
						strWhereClause = " AND Name1 <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "ZZZ'";
						strReportDesc += "Account Below " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text));
					}
					else
					{
						// both empty
						strWhereClause = "";
						strReportDesc += "";
					}
				}
			}
			else
			{
				strWhereClause = "";
				strReportDesc = "";
			}
			switch (intLienAccts)
			{
				case 0:
					{
						// this is all accounts
						break;
					}
				case 1:
					{
						strReportDesc = "Non-Liened Accounts" + "\r\n";
						strWhereClause += " AND LienRecordNumber = 0";
						break;
					}
				case 2:
					{
						strReportDesc = "Liened Accounts" + "\r\n";
						strWhereClause += " AND LienRec.ID <> 0";
						break;
					}
				case 3:
					{
						strReportDesc = "Accounts Eligible for 30 Day Notice" + "\r\n";
						strWhereClause += " AND (LienProcessStatus = 0 OR LienProcessStatus = 1) AND (LienStatusEligibility = 1 OR LienStatusEligibility = 0)";
						break;
					}
				case 4:
					{
						strReportDesc = "Accounts Eligible for Lien" + "\r\n";
						strWhereClause += " AND (LienProcessStatus = 2 OR LienProcessStatus = 3) AND LienStatusEligibility = 3";
						break;
					}
				case 5:
					{
						strReportDesc = "Accounts Eligible for Lien Maturity" + "\r\n";
						if (strRateKeyList != "")
						{
							strWhereClause += " AND (LienProcessStatus = 4 OR LienProcessStatus = 5) AND LienRec.RateKey IN " + strRateKeyList;
							// AND LienStatusEligibility = 5"
						}
						else
						{
							strWhereClause += " AND (LienProcessStatus = 4 OR LienProcessStatus = 5) ";
						}
						break;
					}
			}
			//end switch
			// strSQL = "SELECT * FROM BillingMaster WHERE BillingYear \ 10 = " & intYear & " AND BillingType = 'RE'" & strWhereClause & " ORDER BY Account"
			// If intLienAccts = 5 Then
			// strSQL = "SELECT BillingMaster.LienRecordNumber AS LRN, * FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.LienRecordNumber WHERE BillingType = 'RE'" & strWhereClause & " ORDER BY Name1"
			// Else
			// strSQL = "SELECT * FROM (" & gstrBillYrQuery & ") AS qBY WHERE BillingType = 'RE'" & strWhereClause & " ORDER BY Name1"
			// End If
			// rsData.CreateStoredProcedure "LienEditReportQuery", strSQL
			if (intLienAccts == 5)
			{
				strSQL = "SELECT BillingMaster.LienRecordNumber AS LRN, * FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.ID WHERE BillingType = 'RE'" + strWhereClause + " ORDER BY Name1, Account";
			}
			else if (intLienAccts == 2)
			{
				// strSQL = "SELECT * FROM (" & gstrBillYrQuery & ") AS qBY WHERE " & Right(strWhereClause, Len(strWhereClause) - 4) & " ORDER BY Name1, Account"
				strSQL = modGlobal.Statics.gstrBillYrQuery + " AND " + Strings.Right(strWhereClause, strWhereClause.Length - 4) + " ORDER BY Name1, Account";
			}
			else
			{
				// strSQL = "SELECT * FROM (" & gstrBillYrQuery & ") AS qBY WHERE BillingType = 'RE'" & strWhereClause & " ORDER BY Name1, Account"
				strSQL = modGlobal.Statics.gstrBillYrQuery + " AND BillingType = 'RE'" + strWhereClause + " ORDER BY Name1, Account";
			}
			// kgk 2-14-2012 rsData.CreateStoredProcedure "LienEditReportQuery", strSQL
			// strSQL = "SELECT * FROM LienEditReportQuery"
			BuildSQL = strSQL;
			return BuildSQL;
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the fields in
				double dblCurInt;
				double dblTotal = 0;
				double dblPrin = 0;
				double dblInt = 0;
				double dblCosts = 0;
				double dblPD/*unused?*/;
				float lngNextLine = 0;
				string strMapLot = "";
				string strLocation = "";
				string strBookPage = "";
				string strAddress = "";
				bool boolCO = false;
				clsDRWrapper rsSR = new clsDRWrapper();
				TryAgain:
				// this is where the program will start again if the current account is not acceptable
				fldCO.Top = 0;
				fldCO.Visible = false;
				fldCO.Text = "";
				fldMapLot.Text = "";
				fldLocation.Text = "";
				fldAddress.Text = "";
				fldBookPage.Text = "";
				fldCO.Text = "";
				dblCurInt = 0;
				if (rsData.EndOfFile() != true)
				{
					frmWait.InstancePtr.IncrementProgress();
					if (FCConvert.ToString(rsData.Get_Fields_String("WhetherBilledBefore")) == "L")
					{
						if (intLienAccts == 2)
						{
							rsLN.FindFirstRecord("ID", rsData.Get_Fields_Int32("LienRecordNumber"));
						}
						else
						{
							rsLN.FindFirstRecord("ID", rsData.Get_Fields_Int32("LienRecordNumber"));
						}
						if (!rsLN.NoMatch)
						{
							if (intLienAccts == 2 || intLienAccts == 5)
							{
								dblPrin = Conversion.Val(rsLN.Get_Fields("Principal"));
								dblInt = Conversion.Val(rsLN.Get_Fields("Interest"));
								dblCosts = Conversion.Val(rsLN.Get_Fields("MaturityFee"));
								dblTotal = modCLCalculations.CalculateAccountCLLien(rsLN, dtMailDate, ref dblCurInt);
								dblCurInt -= Conversion.Val(rsLN.Get_Fields_Decimal("InterestCharged"));
							}
							else
							{
								dblPrin = Conversion.Val(rsLN.Get_Fields("Principal"));
								dblInt = Conversion.Val(rsLN.Get_Fields("Interest"));
								dblCosts = Conversion.Val(rsLN.Get_Fields("MaturityFee"));
								dblTotal = modCLCalculations.CalculateAccountCLLien(rsLN, dtMailDate, ref dblCurInt);
								dblCurInt -= Conversion.Val(rsLN.Get_Fields_Decimal("InterestCharged"));
							}
							if (modGlobal.Round(dblTotal, 2) == 0)
							{
								rsData.MoveNext();
								goto TryAgain;
							}
							else
							{
								if (intLienAccts == 5 && dblCosts != 0)
								{
									// this will not show accounts taht have lien maturity fees added to it already
									rsData.MoveNext();
									goto TryAgain;
								}
								lngNumOfAccounts += 1;
								fldAccount.Text = FCConvert.ToString(rsData.Get_Fields("Account"));
								switch (intLienAccts)
								{
								// MAL@20080103: Changed to use the Lien Record (since it was found)
								// Tracker Reference: 11769
									case 0:
										{
											// all
											fldName.Text = "*" + FCConvert.ToString(rsData.Get_Fields_String("Name1"));
											dblPrin -= Conversion.Val(rsLN.Get_Fields_Decimal("PrincipalPaid"));
											dblInt -= Conversion.Val(rsLN.Get_Fields("PLIPaid"));
											dblCurInt -= Conversion.Val(rsLN.Get_Fields_Decimal("InterestPaid"));
											break;
										}
									case 1:
										{
											// prelien
											fldName.Text = rsData.Get_Fields_String("Name1");
											dblPrin -= Conversion.Val(rsData.Get_Fields_Decimal("PrincipalPaid"));
											dblInt -= Conversion.Val(rsData.Get_Fields_Decimal("InterestPaid"));
											break;
										}
									case 2:
										{
											// liened
											fldName.Text = "*" + FCConvert.ToString(rsData.Get_Fields_String("Name1"));
											dblPrin -= Conversion.Val(rsLN.Get_Fields_Decimal("PrincipalPaid"));
											dblInt -= Conversion.Val(rsLN.Get_Fields("PLIPaid"));
											dblCurInt -= Conversion.Val(rsLN.Get_Fields_Decimal("InterestPaid"));
											break;
										}
									case 3:
										{
											// eligible for 30 day notice
											fldName.Text = rsData.Get_Fields_String("Name1");
											dblPrin -= Conversion.Val(rsData.Get_Fields_Decimal("PrincipalPaid"));
											dblInt -= Conversion.Val(rsData.Get_Fields_Decimal("InterestPaid"));
											break;
										}
									case 4:
										{
											// eligible for lien
											// Dave 06/25/07 took out lienrec before paid amounts
											fldName.Text = rsData.Get_Fields_String("Name1");
											dblPrin -= Conversion.Val(rsLN.Get_Fields_Decimal("PrincipalPaid"));
											// TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
											// TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
											dblInt -= Conversion.Val(rsLN.Get_Fields("PLIPaid"));
											dblCurInt -= Conversion.Val(rsLN.Get_Fields_Decimal("InterestPaid"));
											break;
										}
									case 5:
										{
											// eligible for lien maturity
											fldName.Text = "*" + FCConvert.ToString(rsData.Get_Fields_String("Name1"));
											dblPrin -= Conversion.Val(rsLN.Get_Fields_Decimal("PrincipalPaid"));
											// TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
											// TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
											dblInt -= Conversion.Val(rsLN.Get_Fields("PLIPaid"));
											dblCurInt -= Conversion.Val(rsLN.Get_Fields_Decimal("InterestPaid"));
											break;
										}
								}
								dblCosts = Conversion.Val(rsLN.Get_Fields("Costs")) - Conversion.Val(rsLN.Get_Fields("MaturityFee")) - Conversion.Val(rsLN.Get_Fields_Decimal("CostsPaid"));
								fldPrincipal.Text = Strings.Format(dblPrin, "#,##0.00");
								fldPLInt.Text = Strings.Format(dblInt, "#,##0.00");
								fldCosts.Text = Strings.Format(dblCosts, "#,##0.00");
								fldCurrentInt.Text = Strings.Format(dblCurInt, "#,##0.00");
								dblTotals[0] += dblPrin;
								dblTotals[1] += dblInt;
								dblTotals[2] += dblCosts;
								dblTotals[3] += dblCurInt;
								if (modGlobal.Round(dblTotal, 2) == modGlobal.Round(dblPrin + dblInt + dblCosts + dblCurInt, 2))
								{
									fldTotal.Text = Strings.Format(dblTotal, "#,##0.00");
								}
								else
								{
									fldTotal.Text = Strings.Format(dblTotal, "#,##0.00");
									// "*" & Format(dblPrin + dblInt + dblcosts + dblCurInt , "#,##0.00")
								}
							}
						}
						else
						{
							fldAccount.Text = "";
							fldName.Text = "";
							fldPrincipal.Text = "";
							fldPLInt.Text = "";
							fldCosts.Text = "";
							fldTotal.Text = "";
							// ERROR"
							rsData.MoveNext();
							goto TryAgain;
						}
					}
					else
					{
						dblPrin = Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1") + rsData.Get_Fields_Decimal("TaxDue2") + rsData.Get_Fields_Decimal("TaxDue3") + rsData.Get_Fields_Decimal("TaxDue4") - rsData.Get_Fields_Decimal("PrincipalPaid"));
						dblInt = 0;
						dblTotal = modCLCalculations.CalculateAccountCL(ref rsData, FCConvert.ToInt32(rsData.Get_Fields("Account")), dtMailDate, ref dblCurInt);
						dblCurInt += FCConvert.ToDouble((rsData.Get_Fields_Decimal("InterestCharged") * -1) - rsData.Get_Fields_Decimal("InterestPaid"));
						// If Round(dblTotal, 2) <= dblMinimumAmount Then      'if the total for the account = 0 then do not show this account
						if (modGlobal.Round(dblPrin, 2) < dblMinimumAmount || modGlobal.Round(dblTotal, 2) <= 0)
						{
							if (Conversion.Val(modGlobal.Round(dblTotal, 2)) == 0)
							{
								lngZeroBal += 1;
							}
							else if (Conversion.Val(modGlobal.Round(dblTotal, 2)) < 0)
							{
								lngNegBal += 1;
							}
							rsData.MoveNext();
							goto TryAgain;
						}
						else
						{
							lngNumOfAccounts += 1;
							// set the eligibility to 1 so that this account can get to the next step of the lien process if it is at a zero eligibility
							// do not set the other BACK from a higher eligibility
							if (FCConvert.ToInt32(rsData.Get_Fields_Int32("LienStatusEligibility")) == 0)
							{
								rsData.Edit();
								rsData.Set_Fields("LienStatusEligibility", 1);
								rsData.Update(true);
							}

							fldAccount.Text = FCConvert.ToString(rsData.Get_Fields("Account"));
							fldName.Text = rsData.Get_Fields_String("Name1");
							fldPrincipal.Text = Strings.Format(dblPrin, "#,##0.00");
							fldPLInt.Text = Strings.Format(dblInt, "#,##0.00");
							fldCosts.Text = Strings.Format(Conversion.Val(rsData.Get_Fields_Decimal("DemandFees") - rsData.Get_Fields_Decimal("DemandFeesPaid")), "#,##0.00");
							if (rsData.Get_Fields_Decimal("DemandFees") > 0)
							{
								// keep track of the number of accounts dealt with (show this later)
								lngDemandAlready += 1;
							}
							else
							{
								lngPosBal += 1;
							}
							// dblCurInt = dblCurInt * -1
							fldCurrentInt.Text = Strings.Format(dblCurInt, "#,##0.00");
							dblTotals[0] += dblPrin;
							dblTotals[1] += dblInt;
							dblTotals[2] += Conversion.Val(rsData.Get_Fields_Decimal("DemandFees") - rsData.Get_Fields_Decimal("DemandFeesPaid"));
							dblTotals[3] += dblCurInt;
							if (modGlobal.Round(dblTotal, 2) == modGlobal.Round((dblPrin + dblInt + Conversion.Val(rsData.Get_Fields_Decimal("DemandFees")) - Conversion.Val(rsData.Get_Fields_Decimal("DemandFeesPaid")) + dblCurInt), 2))
							{
								// - (rsData.Fields("PrincipalPaid") + rsData.Fields("InterestPaid") + rsData.Fields("DemandFeesPaid")), 2) Then
								fldTotal.Text = Strings.Format(dblTotal, "#,##0.00");
							}
							else
							{
								fldTotal.Text = Strings.Format((dblPrin + dblInt + Conversion.Val(rsData.Get_Fields_Decimal("DemandFees")) - Conversion.Val(rsData.Get_Fields_Decimal("DemandFeesPaid")) + dblCurInt), "#,##0.00");
								// fldTotal.Text = Format(dblTotal, "#,##0.00") '"*" & Format((dblPrin + dblInt + rsData.Fields("DemandFees") + dblCurInt), "#,##0.00") '- (rsData.Fields("PrincipalPaid") + rsData.Fields("InterestPaid") + rsData.Fields("DemandFeesPaid")), "#,##0.00")
							}
						}
					}
					fldCO.Text = "";
					boolCO = false;
					lngNextLine = 240 / 1440F;
					fldCO.Visible = true;
					if (modStatusPayments.Statics.boolRE)
					{
						rsRE.FindFirstRecord("RSAccount", rsData.Get_Fields("Account"));
						if (!rsRE.NoMatch)
						{
							if (FCConvert.ToBoolean(rsRE.Get_Fields_Boolean("INBankruptcy")))
							{
								fldAccount.Text = "(" + fldAccount.Text + ")";
								lblBankruptDisclaimer.Visible = true;
							}
							strLocation = "Location: " + Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("RSLOCNUMALPH")) + " " + Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("RSLOCAPT")) + " " + FCConvert.ToString(rsRE.Get_Fields_String("RSLOCSTREET"))));
							strMapLot = "Map Lot : " + Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("RSMapLot")));
							strAddress = "";

							if (FCConvert.ToString(rsRE.Get_Fields("Own1Address1")) != "")
							{
								strAddress = FCConvert.ToString(rsRE.Get_Fields("Own1Address1")) + "\r\n";
							}
							if (FCConvert.ToString(rsRE.Get_Fields("Own1Address2")) != "")
							{
								strAddress += FCConvert.ToString(rsRE.Get_Fields("Own1Address2")) + "\r\n";
							}
							// TODO Get_Fields: Field [Own1City] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [Own1City] not found!! (maybe it is an alias?)
							if (FCConvert.ToString(rsRE.Get_Fields("Own1City")) != "")
							{
								// TODO Get_Fields: Field [Own1City] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [Own1City] not found!! (maybe it is an alias?)
								strAddress += FCConvert.ToString(rsRE.Get_Fields("Own1City"));
							}

							strAddress += " " + Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("Own1State")) + " " + FCConvert.ToString(rsRE.Get_Fields("Own1Zip")));
							if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name2"))) != "")
							{
								fldCO.Top = 240 / 1440F;
								fldCO.Visible = true;
								fldCO.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name2"))) + " ";
								lngNextLine = 480 / 1440F;
								boolCO = true;
							}
							// TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
							if (rsRE.Get_Fields_String("DeedName1").Trim().ToUpper() == Strings.UCase(Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name1")))))
							{

							}
							else
							{
								rsSR.OpenRecordset("SELECT * FROM SRMaster WHERE RSAccount = " + FCConvert.ToString(rsData.Get_Fields("Account")) + " AND RSCard = 1 AND SaleDate > '" + FCConvert.ToString(rsData.Get_Fields_DateTime("TransferFromBillingDateLast")) + "'", modExtraModules.strREDatabase);
								if (rsSR.EndOfFile())
								{
									// there has not been a sale in that time period
								}
								else
								{
									fldCO.Top = 240 / 1440F;
									fldCO.Visible = true;
									fldCO.Text = fldCO.Text + "C/O " + Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("DeedName1")) + " " + FCConvert.ToString(rsRE.Get_Fields("DeedName2")));
									lngNextLine = 480 / 1440F;
									boolCO = true;
								}
							}
						}
						else
						{
							if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name2"))) != "")
							{
								fldCO.Top = 240 / 1440F;
								fldCO.Visible = true;
								fldCO.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name2"))) + " ";
								lngNextLine = 480 / 1440F;
								boolCO = true;
							}
							else
							{
								fldCO.Top = 0;
								fldCO.Visible = false;
								fldCO.Text = "";
								lngNextLine = 240 / 1440F;
								boolCO = false;
							}
						}
					}
					else
					{
						fldCO.Top = 0;
						fldCO.Visible = false;
						fldCO.Text = "";
						lngNextLine = 240 / 1440F;
						boolCO = false;
						sarLienEditMortHolders.Visible = false;
					}
					if (boolShowMailingAddress)
					{
						fldAddress.Top = lngNextLine;
						fldAddress.Visible = true;
						fldAddress.Text = strAddress;
					}
					if (boolShowMapLot)
					{
						fldMapLot.Visible = true;
						fldMapLot.Text = strMapLot;
						fldMapLot.Top = lngNextLine;
						lngNextLine += 240 / 1440F;
					}
					if (boolShowLocation)
					{
						fldLocation.Top = lngNextLine;
						lngNextLine += 240 / 1440F;
						fldLocation.Visible = true;
						fldLocation.Text = strLocation;
					}
					if (boolShowBookPage)
					{
						// find the bookpage string for this account
						// rsBook.OpenRecordset "SELECT * FROM Bookpage WHERE Current AND Card = 1 AND Account = " & rsData.Fields("Account") & " ORDER BY Line asc", strREDatabase
						// If Not rsBook.EndOfFile Then
						// strBookPage = ""
						// Do Until rsBook.EndOfFile
						// strBookPage = strBookPage & "B" & rsBook.Fields("Book") & "P" & rsBook.Fields("Page") & ", "
						// rsBook.MoveNext
						// Loop
						// strBookPage = "BookPage: " & Left(strBookPage, Len(strBookPage) - 2)
						// Else
						// strBookPage = "BookPage: "
						// End If
						if (FCConvert.ToString(rsData.Get_Fields_String("WhetherBilledBefore")) != "L")
						{
							strBookPage = FCConvert.ToString(rsData.Get_Fields_String("BookPage"));
						}
						else
						{
							if (rsLN.NoMatch)
							{
								strBookPage = FCConvert.ToString(rsData.Get_Fields_String("BookPage"));
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
								if (Strings.Trim(FCConvert.ToString(rsLN.Get_Fields("Book"))) != "")
								{
									// TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
									strBookPage = "Lien - B" + FCConvert.ToString(rsLN.Get_Fields("Book")) + "P" + FCConvert.ToString(rsLN.Get_Fields("Page"));
								}
								else
								{
									strBookPage = FCConvert.ToString(rsData.Get_Fields_String("BookPage"));
								}
							}
						}
						fldBookPage.Top = lngNextLine;
						lngNextLine += 240 / 1440F;
						fldBookPage.Visible = true;
						fldBookPage.Text = strBookPage;
					}
					if (boolShowMailingAddress)
					{
						fldBookPage.Width = fldAddress.Left - fldBookPage.Left;
						if (boolCO)
						{
							lngNextLine = 1080 / 1440F;
						}
						else
						{
							lngNextLine = 900 / 1440F;
						}
					}
					if (lngExtraLines > 0)
					{
						lngNextLine += (lngExtraLines * 180) / 1440F;
					}
					Detail.Height = lngNextLine;
					switch (intReportDetail)
					{
						case 1:
						case 2:
							{
								sarLienEditMortHolders.Top = lngNextLine + (100 / 1440F);
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								sarLienEditMortHolders.Report.UserData = rsData.Get_Fields("Account");
								break;
							}
						default:
							{
								Detail.Height = lngNextLine;
								break;
							}
					}
					//end switch
					lblExtraLines.Top = lngNextLine - lblExtraLines.Height;
					rsData.MoveNext();
				}
				else
				{
					fldTotal.Text = "";
					fldPrincipal.Text = "";
					fldAccount.Text = "";
					fldCosts.Text = "";
					fldCurrentInt.Text = "";
					fldPLInt.Text = "";
					fldName.Text = "";
					sarLienEditMortHolders.Visible = false;
				}
				rsSR.Dispose();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Binding Fields");
				/*? Resume; */
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			// fill the totals line in
			lblTotals.Text = "Count: " + FCConvert.ToString(lngNumOfAccounts) + "   Totals:";
			fldTotalPrincipal.Text = Strings.Format(dblTotals[0], "#,##0.00");
			fldTotalPLInt.Text = Strings.Format(dblTotals[1], "#,##0.00");
			fldTotalCosts.Text = Strings.Format(dblTotals[2], "#,##0.00");
			fldTotalCurrentInt.Text = Strings.Format(dblTotals[3], "#,##0.00");
			fldTotalTotal.Text = Strings.Format(dblTotals[0] + dblTotals[1] + dblTotals[2] + dblTotals[3], "#,##0.00");
		}
		
		private void HideAllFields()
		{
			lblCosts.Visible = false;
			lblCurrentInt.Visible = false;
			lblName.Visible = false;
			lblPLInt.Visible = false;
			lblPrincipal.Visible = false;
			lblReportType.Visible = false;
			lblTotal.Visible = false;
			lblTotals.Visible = false;
			Line1.Visible = false;
			Line2.Visible = false;
			fldTotalCosts.Visible = false;
			fldTotalCurrentInt.Visible = false;
			fldTotalPLInt.Visible = false;
			fldTotalPrincipal.Visible = false;
			fldTotalTotal.Visible = false;
			this.Detail.Height = 0;
		}

		private void SetupDetailSection()
		{
			// this will set the height of the detail section and the visibility of the sub report
			switch (intReportDetail)
			{
				case 0:
					{
						sarLienEditMortHolders.Visible = false;
						sarLienEditMortHolders.Top = 0;
						break;
					}
				case 1:
				case 2:
					{
						sarLienEditMortHolders.Visible = true;
						sarLienEditMortHolders.Top = sarLienEditMortHolders.Top;
						sarLienEditMortHolders.Report = new sarLienEditMortHolders();
						break;
					}
			}
			//end switch
		}

		
	}
}
