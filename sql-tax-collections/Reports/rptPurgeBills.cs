﻿using fecherFoundation;
using System;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{

	public partial class rptPurgeBills : BaseSectionReport
	{
        //=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               07/22/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/02/2005              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		int lngNumOfAccts;
		double[] dblTotals = new double[5 + 1];
		short intType;

		public rptPurgeBills()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();            
        }

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Account Detail";
		}

		public static rptPurgeBills InstancePtr
		{
			get
			{
				return (rptPurgeBills)Sys.GetInstance(typeof(rptPurgeBills));
			}
		}

        protected rptPurgeBills _InstancePtr = null;
        private readonly int maxRecordsToPurge = modCLPurge.Statics.lngMaxPurgeAccounts;

        /// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			rsData.Reset();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				string strSQL;
				srptPayments.Report = new rptPurgePayments();
				intType = FCConvert.ToInt16(Conversion.Val(this.UserData));
				switch (intType)
				{
					case 0:
						{
							// RE Non Lien
							strSQL = modCLPurge.GetBillSql(intType);
							lblReportType.Text = "Real Estate Non Lien";
							rsData.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
							if (rsData.RecordCount() > maxRecordsToPurge)
							{
								FCMessageBox.Show($"Only {FCConvert.ToString(maxRecordsToPurge)} records will be purged at a time.  This routine may need to be run more than one time to purge all records.", MsgBoxStyle.Exclamation, "Over Maximum Bills");
							}
							break;
						}
					case 1:
						{
							// RE Lien
							strSQL = modCLPurge.GetBillSql(intType);
							lblReportType.Text = "Real Estate Liens";
							rsData.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
							lblTax1.Text = "Principal";
							lblTax2.Text = "Interest";
							lblTax3.Text = "Costs";
							lblTax4.Text = string.Empty;
							break;
						}
					case 2:
						{
							// PP
							strSQL = modCLPurge.GetBillSql(intType);
							lblReportType.Text = "Personal Property";
                            rsData.Set_ParameterValue("@year","1995");
							rsData.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
							if (rsData.RecordCount() > maxRecordsToPurge)
							{
								FCMessageBox.Show($"Only {FCConvert.ToString(maxRecordsToPurge)} records will be purged at a time.  This routine may need to be run more than one time to purge all records.", MsgBoxStyle.Exclamation, "Over Maximum Bills");
							}
							break;
						}
				}
				if (rsData.EndOfFile())
				{
					Cancel();
				}
			}
			catch (Exception ex)
			{
				FCMessageBox.Show($"Error - {ex.GetBaseException().Message}.", MsgBoxStyle.Critical, "Error Loading Report - " + FCConvert.ToString(intType));
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
                if (rsData.EndOfFile()) return;

                ShowPayments();
                ShowBillInfo();
            }
			catch (Exception ex)
			{
				FCMessageBox.Show($"Error - {ex.GetBaseException().Message}", MsgBoxStyle.Critical, "Detail Format Error");
			}
		}

        private void ShowBillInfo()
        {
            BindFields();
            lngNumOfAccts += 1;

            if (lngNumOfAccts >= maxRecordsToPurge && intType == 0)
            {
                rsData.MoveLast();
                rsData.MoveNext();
            }
            else
            {
                rsData.MoveNext();
            }
        }

        private void ShowPayments()
        {
            srptPayments.Report.UserData = rsData.Get_Fields_Int32("BillKey");
        }

        private void GroupFooter1_Format(object sender, EventArgs e)
		{
			try
			{
				fldFooterTotal.Text = lngNumOfAccts == 1 ? "Total for 1 account." : $"Total for {FCConvert.ToString(lngNumOfAccts)} accounts.";

				// show totals
				fldFooterTax1.Text = modUtilities.FormatTotal(dblTotals[1]);
				fldFooterTax2.Text = modUtilities.FormatTotal(dblTotals[2]);
				fldFooterTax3.Text = modUtilities.FormatTotal(dblTotals[3]);
				fldFooterTax4.Text = intType != 1 ? modUtilities.FormatTotal(dblTotals[4]) : string.Empty;
				fldFooterCHGINT.Text = modUtilities.FormatTotal(dblTotals[0]);
				fldFooterTaxTotal.Text = modUtilities.FormatTotal(dblTotals[0] + dblTotals[1] + dblTotals[2] + dblTotals[3] + dblTotals[4]);
			}
			catch (Exception ex)
			{
				FCMessageBox.Show($"Error - {ex.GetBaseException().Message}", MsgBoxStyle.Critical, "Group Format Error");
			}
		}

        private void BindFields()
		{
			try
			{
                if (rsData.EndOfFile()) return;

                // us the correct new information
                int lngAccount = FCConvert.ToInt32(rsData.Get_Fields("Account"));
                var lngYear = FCConvert.ToInt32(rsData.Get_Fields_Int32("BillingYear"));
                rptPurge.InstancePtr.lngTotalCount += 1;
                fldAcct.Text = lngAccount.ToString();
                fldName.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name1")));
                if (FCConvert.ToString(rsData.Get_Fields_String("Name2")) != "")
                {
                    lblName.Text = $"{lblName.Text} & {FCConvert.ToString(rsData.Get_Fields_String("Name2"))}";
                }
                fldYear.Text = modExtraModules.FormatYear(lngYear.ToString());
                if (intType != 1)
                {
                    fldTax1.Text = modUtilities.FormatTotal(rsData.Get_Fields_Decimal("TaxDue1"));
                    fldTax2.Text = modUtilities.FormatTotal(rsData.Get_Fields_Decimal("TaxDue2"));
                    fldTax3.Text = modUtilities.FormatTotal(rsData.Get_Fields_Decimal("TaxDue3"));
                    fldTax4.Text = modUtilities.FormatTotal(rsData.Get_Fields_Decimal("TaxDue4"));
                    fldCHGINT.Text = modUtilities.FormatTotal(rsData.Get_Fields_Decimal("InterestCharged") * -1);
                    fldTotal.Text = modUtilities.FormatTotal(Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue4")) - Conversion.Val(rsData.Get_Fields_Decimal("InterestCharged")));
                    dblTotals[1] += FCConvert.ToDouble(fldTax1.Text);
                    dblTotals[2] += FCConvert.ToDouble(fldTax2.Text);
                    dblTotals[3] += FCConvert.ToDouble(fldTax3.Text);
                    dblTotals[4] += FCConvert.ToDouble(fldTax4.Text);
                    dblTotals[0] += FCConvert.ToDouble(fldCHGINT.Text);
                }
                else
                {
                    rptPurge.InstancePtr.rsLien.FindFirstRecord("ID", rsData.Get_Fields("LRN"));
                    if (!rptPurge.InstancePtr.rsLien.NoMatch)
                    {
                        fldTax1.Text = modUtilities.FormatTotal(rptPurge.InstancePtr.rsLien.Get_Fields_Decimal("Principal"));
                        fldTax2.Text = modUtilities.FormatTotal(rptPurge.InstancePtr.rsLien.Get_Fields_Decimal("Interest"));
                        fldTax3.Text = modUtilities.FormatTotal(rptPurge.InstancePtr.rsLien.Get_Fields_Decimal("Costs"));
                        fldTax4.Text = string.Empty;
                        fldCHGINT.Text = modUtilities.FormatTotal(rptPurge.InstancePtr.rsLien.Get_Fields_Decimal("InterestCharged") * -1);
                        fldTotal.Text = modUtilities.FormatTotal(Conversion.Val(rptPurge.InstancePtr.rsLien.Get_Fields("Principal")) + Conversion.Val(rptPurge.InstancePtr.rsLien.Get_Fields("Interest")) + Conversion.Val(rptPurge.InstancePtr.rsLien.Get_Fields("Costs")) - Conversion.Val(rptPurge.InstancePtr.rsLien.Get_Fields_Decimal("InterestCharged")));
                    }
                    else
                    {
                        fldTax1.Text = "0.00";
                        fldTax2.Text = "0.00";
                        fldTax3.Text = "0.00";
                        fldTax4.Text = string.Empty;
                    }
                    dblTotals[0] += FCConvert.ToDouble(fldCHGINT.Text);
                    dblTotals[1] += FCConvert.ToDouble(fldTax1.Text);
                    dblTotals[2] += FCConvert.ToDouble(fldTax2.Text);
                    dblTotals[3] += FCConvert.ToDouble(fldTax3.Text);
                }
			}
			catch (Exception ex)
			{
				FCMessageBox.Show($"Error - {ex.GetBaseException().Message}", MsgBoxStyle.Critical, "Error Binding Fields");
			}
		}
    }
}
