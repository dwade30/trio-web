using fecherFoundation;
using Global;
using GrapeCity.ActiveReports.Extensions;
using SharedApplication.Enums;
using SharedApplication.TaxCollections.Enums;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using SharedApplication.TaxCollections;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCL0000
{
    /// <summary>
    /// Summary description for SectionReport1.
    /// </summary>
    public partial class arStatusLists : BaseSectionReport
    {
        private TaxCollectionStatusReportConfiguration reportConfiguration;
        string strSQL;
        clsDRWrapper rsData = new clsDRWrapper();
        double[] dblTotals = new double[7 + 1];
        bool boolRTError;
        int lngSummaryLineCT;
        int lngCount;
        bool boolStarted;
        clsDRWrapper rsCalLien = new clsDRWrapper();
        //double dblSpecialTotal;
        double dblPDTotal;
        double dblNonIntTotal;
        bool boolAdjustedSummary;
        bool boolLienRec;
        bool boolSummaryOnly;
        float lngExtraRows;
        int lngLastBK;
        bool boolDeletedAccount;
        bool boolShowPreLienPayments;
        bool boolExcludeZeroBalance;
        //Footer totals
        GrapeCity.ActiveReports.SectionReportModel.TextBox obNew1;
        GrapeCity.ActiveReports.SectionReportModel.TextBox obNew2;
        GrapeCity.ActiveReports.SectionReportModel.TextBox obNew3;
        GrapeCity.ActiveReports.SectionReportModel.Label obLabel;
        GrapeCity.ActiveReports.SectionReportModel.Line obLine;
        //DateTime dtPerf1;
        //DateTime dtPerf2;
        //double dblTmp1;
        //double dblTmp2;
       //int lngPrfCnt3;
        //int lngPrfCnt4;
        //double dblPerf3;
        //double dblPerf4;
        Dictionary<object, object> dctAccounts = new Dictionary<object, object>();
        // kk01132014 trocls-13
        // these are for the summaries in the report footer
        double[] dblYearTotals = new double[3000 + 1];
        // billingyear - 19800
        double[] dblYearNonIntTotals = new double[3000 + 1];
        int[] lngArrBillCounts = new int[3000 + 1];
        // kk01082015 trocl-13
        // kk03312014 trocl-682/trocls-22   Dim dblPayments(13, 7)          As Double   '0 - 3, 1 - A, 2 - C, 3 - D, 4 - I, 5 - L, 6 - P, 7 - U, 8 - X, 9 - Y, 10 - Total, 11 - Current Interest, 12 - N, 13 - F        'MAL@20080418
        // 0 - Principal, 1 - PreLienInterest, 2 - CurrentInterest, 3 - Costs
        clsPmtTypeSummaries tPayTypeSummaries = new clsPmtTypeSummaries();
        //int counter;
        // kk05262016 trocls-84   Change from Integer to Long - Bangor overflowing running large report
        public arStatusLists()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
            EnablePageDiskCache = true;
        }

        private void InitializeComponentEx()
        {
            //if (_InstancePtr == null)
            //	_InstancePtr = this;
            this.Name = "Status Lists";
        }

        public void Init(ref TaxCollectionStatusReportConfiguration reportConfig)
        {
            reportConfiguration = reportConfig;
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            //if (_InstancePtr == this)
            //{
            //	_InstancePtr = null;
            //	Sys.ClearInstance(this);
            //}
            if (disposing)
            {
                rsData.Dispose();
                rsCalLien.Dispose();
            }
            base.Dispose(disposing);
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
        {
            eArgs.EOF = boolStarted && rsData.EndOfFile();
        }


        private void ActiveReport_PageEnd(object sender, EventArgs e)
        {

        }

        private void ActiveReport_PageStart(object sender, EventArgs e)
        {
            lblPage.Text = $"Page {this.PageNumber}";
        }

        private void ActiveReport_ReportEnd(object sender, EventArgs e)
        {
            try
            {
                tPayTypeSummaries = null;
                modCustomReport.Statics.gboolShowPaymentsOnSL = false;
                //dtPerf2 = DateTime.Now;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            finally
            {
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
                rsData.DisposeOf();
                rsCalLien.DisposeOf();
            }
        }

        private void ActiveReport_ReportStart(object sender, EventArgs e)
        {
            lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
            lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "t");
            lblMuniName.Text = modGlobalConstants.Statics.MuniName;
            lngCount = 0;
            //counter = 0;
            //modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
            this.boolExcludeZeroBalance = reportConfiguration.Options.ExcludePaid;
            if (reportConfiguration.Options.ShowPayments)
            {
                modCustomReport.Statics.gboolShowPaymentsOnSL = true;
            }
            if (reportConfiguration.Filter.TaxBillStatus == TaxBillStatusType.PreLien && reportConfiguration.Options.ShowPayments)
            {
                boolShowPreLienPayments = true;
            }
            boolRTError = false;
            boolSummaryOnly = reportConfiguration.Options.ShowSummaryOnly;
            SetupFields();
            // Moves/shows the correct fields into the right places
            SetReportHeader();
            // Sets the titles and moves labels in the header
            boolStarted = false;
            strSQL = BuildSQL();
            // Generates the SQL String
            // KKKK
            //dtPerf1 = DateTime.Now;
            
            //lngPrfCnt4 = 0;
            //dblPerf3 = 0;
            //dblPerf4 = 0;
            StaticSettings.GlobalTelemetryService.TrackTrace($"AR Status List SQL that may be slow and GB intensive: {strSQL}");
            rsData.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
            this.Document.Printer.Landscape = false;
            if (rsData.EndOfFile())
            {
                NoRecords();
            }
            else
            {
                if (modStatusPayments.Statics.boolRE)
                {
                    // open the lien table to be used laters
                    rsCalLien.OpenRecordset("SELECT * FROM LienRec", modExtraModules.strCLDatabase);
                    //rsData.InsertName("SecOwnerPartyID", "Own2", false, true, false, "", false, "", true, "");
                }
                //FC:FINAL:DDU:#i172 add report footer controls when report starts
                //Add footer controls
                ReportFooter.Controls.Add(obNew1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox());
                ReportFooter.Controls.Add(obNew2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox());
                ReportFooter.Controls.Add(obNew3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox());
                ReportFooter.Controls.Add(obLabel = new GrapeCity.ActiveReports.SectionReportModel.Label());
                ReportFooter.Controls.Add(obLine = new GrapeCity.ActiveReports.SectionReportModel.Line());
                for (int lngIndex = 1; lngIndex <= 100; lngIndex++)
                {
                    AddSummaryLineControls(ref lngSummaryLineCT);
                }
                AddSummaryLineControls(ref lngSummaryLineCT);
                lngSummaryLineCT = 0;
            }
        }

        private void NoRecords()
        {
            // this will terminate the report and show a message to the user
            // MAL@20070907: Corrected Message Title ; Said "Receipt Search"
            FCMessageBox.Show("There are no records matching the criteria selected.", MsgBoxStyle.Information, "Account Search");
            Cancel();
            return;
        }

        private string BuildSQL()
        {
            string BuildSQL = "";
            string strTemp = "";
            string strWhereClause = "";
            string strREPPBill = "";
            string strREPPPayment = "";
            string strREInnerJoin = "";
            string strAsOfDateString = "";
            string strSelectFields = "";
            string strBillingType = "";

            if (reportConfiguration.BillingType == PropertyTaxBillType.Real)
            {
                strREInnerJoin = $"(BillingMaster INNER JOIN (SELECT Master.RSCard, Master.RSAccount, DeedName1 as Own1FullName, DeedName2 as Own2 ,cpo.Address1 AS Own1Address1, cpo.Address2 AS Own1Address2, cpo.City AS Own1City, cpo.State AS Own1State, cpo.Zip AS Own1Zip, PIAcres, OwnerPartyID, SecOwnerPartyID, RSLocApt, RSLocNumAlph, RSLocStreet, RSRef1, RSRef2, RSMapLot, RSDeleted, TaxAcquired, TADate FROM {modGlobal.Statics.strDbRE}Master LEFT JOIN {modGlobal.Statics.strDbCP}PartyAndAddressView cpo ON Master.OwnerPartyID = cpo.ID) AS mparty ON BillingMaster.Account = mparty.RSAccount)";
                strREPPBill = "'RE' AND RSCard = 1";
                strREPPPayment = "'R'";
                strBillingType = "'RE'";
            }
            else
            {
                strREInnerJoin = $"(BillingMaster INNER JOIN (SELECT Account AS PPAccount, Open1, Open2, StreetNumber AS PPStreetNumber, Street, Deleted, cpo.FullNameLF as Own1FullName, cpo.Address1 AS Own1Address1, cpo.Address2 AS Own1Address2, cpo.City AS Own1City, cpo.State AS Own1State, cpo.Zip AS Own1Zip FROM {modGlobal.Statics.strDbPP}PPMaster LEFT JOIN {modGlobal.Statics.strDbCP}PartyAndAddressView cpo ON PPMaster.PartyID = cpo.ID) AS mparty ON BillingMaster.Account = mparty.PPAccount)";
                strREPPBill = "'PP'";
                strREPPPayment = "'P'";
                strBillingType = "'PP'";
            }

            if (boolStarted)
            {
                switch (reportConfiguration.Options.ReportType.ToUpper())
                {
                    case "ACCOUNT":
                    case "ABATE":
                    case "SUPPLEMENTAL":
                    case "DISCOUNT":
                    case "TAXCLUB":
                    case "COSTS":
                    case "REFUNDEDABATE":
                    case "PREPAYMENT":
                    case "PAYMENTS":
                        {
                            // This is a regular report using the Where Criteria to report the correct information
                            strWhereClause = reportConfiguration.WhereClause;

                            if (Strings.Trim(strWhereClause) != "")
                            {
                                if (Strings.InStr(1, strWhereClause, "InterestCharged") != 0 || Strings.InStr(1, strWhereClause, "PrincipalPaid") != 0)
                                {
                                    strWhereClause = strWhereClause.Replace("InterestCharged", "LienRec.InterestCharged");
                                    strWhereClause = strWhereClause.Replace("PrincipalPaid", "LienRec.PrincipalPaid");
                                    strWhereClause = strWhereClause.Replace("InterestPaid", "LienRec.InterestPaid");
                                }

                                if (Strings.InStr(1, strWhereClause, "LienRecordNumber") != 0)
                                {
                                    strWhereClause = strWhereClause.Replace("LienRecordNumber", "LienRec.ID");
                                }

                                if (Strings.InStr(1, strWhereClause, "RateKey") != 0)
                                {
                                    strWhereClause = strWhereClause.Replace("RateKey", "LienRec.RateKey");
                                }

                                strTemp = String.IsNullOrWhiteSpace(reportConfiguration.SortClause)
                                    ? $"SELECT *, LienRec.ID AS LRN, LienRec.RateKey AS LRK FROM {strREInnerJoin} INNER JOIN (LienRec INNER JOIN ({reportConfiguration.SelectClause}) AS List ON LienRec.ID = List.BK) ON LienRec.ID = BillingMaster.LienRecordNumber WHERE BillingMaster.BillingType = 'L' AND{strWhereClause}"
                                    : $"SELECT *, LienRec.ID AS LRN, LienRec.RateKey AS LRK FROM {strREInnerJoin} INNER JOIN (LienRec INNER JOIN ({reportConfiguration.SelectClause}) AS List ON LienRec.ID = List.BK) ON LienRec.ID = BillingMaster.LienRecordNumber WHERE BillingMaster.BillingType = 'L' AND{strWhereClause} ORDER BY {reportConfiguration.SortClause}";
                            }
                            else
                            {
                                strTemp = String.IsNullOrWhiteSpace(reportConfiguration.SortClause)
                                    ? $"SELECT *, LienRec.ID AS LRN, LienRec.RateKey AS LRK FROM {strREInnerJoin} INNER JOIN (LienRec INNER JOIN ({reportConfiguration.SelectClause}) AS List ON LienRec.ID = List.BK) ON LienRec.ID = BillingMaster.LienRecordNumber  WHERE BillingMaster.BillingType = 'L'"
                                    : $"SELECT *, LienRec.ID AS LRN, LienRec.RateKey AS LRK FROM {strREInnerJoin} INNER JOIN (LienRec INNER JOIN ({reportConfiguration.SelectClause}) AS List ON LienRec.ID = List.BK) ON LienRec.ID = BillingMaster.LienRecordNumber  WHERE BillingMaster.BillingType = 'L' ORDER BY {reportConfiguration.SortClause}";
                            }

                            break;
                        }
                }
            }
            else
            {
                switch (reportConfiguration.Options.ReportType.ToUpper())
                {
                    // this is the first pass
                    // This is a regular report using the Where Criteria to report the correct information
                    case "ACCOUNT" when !String.IsNullOrWhiteSpace(reportConfiguration.WhereClause):
                        {
                            strTemp = String.IsNullOrWhiteSpace(reportConfiguration.SortClause)
                                ? $"SELECT *, BillingMaster.BillingType AS Bill_BillType FROM {strREInnerJoin} INNER JOIN ({reportConfiguration.SelectClause}) AS List ON BillingMaster.ID = List.BK WHERE BillingMaster.BillingType = {strREPPBill} AND {reportConfiguration.WhereClause}"
                                : $"SELECT *, BillingMaster.BillingType AS Bill_BillType FROM {strREInnerJoin} INNER JOIN ({reportConfiguration.SelectClause}) AS List ON BillingMaster.ID = List.BK WHERE BillingMaster.BillingType = {strREPPBill} AND {reportConfiguration.WhereClause} ORDER BY {reportConfiguration.SortClause}";

                            break;
                        }
                    case "ACCOUNT" when !String.IsNullOrWhiteSpace(reportConfiguration.SortClause):
                        strTemp = $"SELECT *, BillingMaster.BillingType AS Bill_BillType FROM {strREInnerJoin} INNER JOIN ({reportConfiguration.SelectClause}) AS List ON BillingMaster.ID = List.BK WHERE BillingMaster.BillingType = {strREPPBill} ORDER BY {reportConfiguration.SortClause}";

                        break;
                    case "ACCOUNT":
                        strTemp = $"SELECT *, BillingMaster.BillingType AS Bill_BillType FROM {strREInnerJoin} INNER JOIN ({reportConfiguration.SelectClause}) AS List ON BillingMaster.ID = List.BK WHERE BillingMaster.BillingType = {strREPPBill}";

                        break;
                    case "ABATE":
                    case "SUPPLEMENTAL":
                    case "DISCOUNT":
                    case "TAXCLUB":
                    case "COSTS":
                    case "REFUNDEDABATE":
                    case "PREPAYMENT":
                    case "PAYMENTS":
                    case "CORRECTIONS":
                        {
                            // This is a regular report using the Where Criteria to report the correct information
                            if (reportConfiguration.BillingType == PropertyTaxBillType.Real)
                            {
                                strSelectFields = "BillingMaster.ID, Account, BillingType, BillingYear, Name1, Name2, Address1, Address2, Address3, MapLot, StreetNumber, Apt, StreetName, " + "LandValue, BuildingValue, ExemptValue, TaxDue1, TaxDue2, TaxDue3, TaxDue4, LienRecordNumber, PrincipalPaid, InterestPaid, InterestCharged, " + "DemandFees, DemandFeespaid, InterestAppliedThroughDate,TransferFromBillingDateFirst, RateKey, BookPage, PIAcres, OwnerPartyID, SecOwnerPartyID, " + "Own1FullName, Own1Address1, Own1Address2, Own1City, Own1State, Own1Zip, RSLocApt, RSLocNumAlph, RSLocStreet, RSRef1, RSRef2, RSMapLot, RSDeleted, TaxAcquired, TADate, RSAccount, RSCard, BillCode, BK";
                            }
                            else
                            {
                                strSelectFields = "ID, Account, BillingType, BillingYear, Name1, Name2, Address1, Address2, Address3, MapLot, StreetNumber, Apt, StreetName, " + "LandValue, BuildingValue, ExemptValue, TaxDue1, TaxDue2, TaxDue3, TaxDue4, LienRecordNumber, PrincipalPaid, InterestPaid, InterestCharged, " + "DemandFees, DemandFeespaid, InterestAppliedThroughDate,TransferFromBillingDateFirst, RateKey, BookPage, " + "Own1FullName, Own1Address1, Own1Address2, Own1City, Own1State, Own1Zip, Open1, Open2, PPStreetNumber, Street, Deleted, BillCode, BK";
                            }

                            if (boolShowPreLienPayments && reportConfiguration.Options.ReportType.ToUpper() != "ACCOUNT")
                            {
                                if (!String.IsNullOrWhiteSpace(reportConfiguration.WhereClause))
                                {
                                    strTemp = String.IsNullOrWhiteSpace(reportConfiguration.SortClause)
                                        ? $"(SELECT {strSelectFields} FROM {strREInnerJoin} INNER JOIN ({reportConfiguration.SelectClause}) AS List ON BillingMaster.ID = List.BK WHERE LienRecordNumber > 0 AND BillingMaster.BillingType = {strREPPBill} AND BillCode = {strREPPPayment} AND {reportConfiguration.WhereClause}{strAsOfDateString}) "
                                        : $"SELECT {strSelectFields} FROM {strREInnerJoin} INNER JOIN ({reportConfiguration.SelectClause}) AS List ON BillingMaster.ID = List.BK WHERE LienRecordNumber > 0 AND BillingMaster.BillingType = {strREPPBill} AND BillCode = {strREPPPayment} AND {reportConfiguration.WhereClause}{strAsOfDateString} ORDER BY {reportConfiguration.SortClause}";
                                }
                                else
                                {
                                    strTemp = string.IsNullOrWhiteSpace(reportConfiguration.SortClause)
                                        ? $"(SELECT {strSelectFields} FROM {strREInnerJoin} INNER JOIN ({reportConfiguration.SelectClause}) AS List ON BillingMaster.ID = List.BK WHERE LienRecordNumber > 0 AND BillingMaster.BillingType = {strREPPBill} AND BillCode = {strREPPPayment}) "
                                        : $"SELECT {strSelectFields} FROM {strREInnerJoin} INNER JOIN ({reportConfiguration.SelectClause} AND BillCode = {strREPPPayment}) AS List ON BillingMaster.ID = List.BK WHERE LienRecordNumber > 0 AND BillingMaster.BillingType = {strREPPBill} AND BillCode = {strREPPPayment} ORDER BY {reportConfiguration.SortClause}";
                                }
                            }
                            else
                            {
                                if (String.IsNullOrWhiteSpace(reportConfiguration.WhereClause))
                                {
                                    if (String.IsNullOrWhiteSpace(reportConfiguration.SortClause))
                                    {
                                        strTemp = $"(SELECT {strSelectFields} FROM {strREInnerJoin} INNER JOIN ({reportConfiguration.SelectClause}) AS List ON BillingMaster.ID = List.BK WHERE BillingMaster.BillingType = {strREPPBill} AND BillCode = {strREPPPayment}) ";
                                        strTemp += $"UNION ALL (SELECT {strSelectFields} FROM {strREInnerJoin} INNER JOIN ({reportConfiguration.SelectClause}) AS List ON BillingMaster.LienRecordNumber = List.BK WHERE BillingMaster.BillingType = {strREPPBill} AND BillCode = 'L') ";
                                    }
                                    else
                                    {
                                        strTemp = $"SELECT * FROM (SELECT {strSelectFields} FROM {strREInnerJoin} INNER JOIN ({reportConfiguration.SelectClause} AND BillCode = {strREPPPayment}) AS List ON BillingMaster.ID = List.BK WHERE BillingMaster.BillingType = {strREPPBill} AND BillCode = {strREPPPayment}) AS Ex1 ";
                                        strTemp += $"UNION ALL (SELECT {strSelectFields} FROM {strREInnerJoin} INNER JOIN ({reportConfiguration.SelectClause} AND BillCode = 'L') AS List ON BillingMaster.LienRecordNumber = List.BK WHERE BillingMaster.BillingType = {strREPPBill} AND BillCode = 'L') ORDER BY {reportConfiguration.SortClause}";
                                    }
                                }
                                else
                                {
                                    if (String.IsNullOrWhiteSpace(reportConfiguration.SortClause))
                                    {
                                        strTemp = $"(SELECT {strSelectFields} FROM {strREInnerJoin} INNER JOIN ({reportConfiguration.SelectClause}) AS List ON BillingMaster.ID = List.BK WHERE BillingMaster.BillingType = {strREPPBill} AND BillCode = {strREPPPayment} AND {reportConfiguration.WhereClause}{strAsOfDateString}) ";
                                        strTemp += $"UNION ALL (SELECT {strSelectFields} FROM {strREInnerJoin} INNER JOIN ({reportConfiguration.SelectClause}) AS List ON BillingMaster.LienRecordNumber = List.BK WHERE BillingMaster.BillingType = {strREPPBill} AND BillCode = 'L' AND {reportConfiguration.WhereClause}{strAsOfDateString})";
                                    }
                                    else
                                    {
                                        strTemp = $"SELECT * FROM (SELECT {strSelectFields} FROM {strREInnerJoin} INNER JOIN ({reportConfiguration.SelectClause}) AS List ON BillingMaster.ID = List.BK WHERE BillingMaster.BillingType = {strREPPBill} AND BillCode = {strREPPPayment} AND {reportConfiguration.WhereClause}{strAsOfDateString}) AS Ex1 UNION ALL (SELECT {strSelectFields} FROM {strREInnerJoin} INNER JOIN ({reportConfiguration.SelectClause}) AS List ON BillingMaster.LienRecordNumber = List.BK WHERE BillingMaster.BillingType = {strREPPBill} AND BillCode = 'L' AND {reportConfiguration.WhereClause}{strAsOfDateString}) ORDER BY {reportConfiguration.SortClause}";
                                    }
                                }
                            }

                            break;
                        }
                }
            }

            BuildSQL = strTemp;
            return BuildSQL;
        }

        private void SetupFields()
        {
            // Set each field and label's visible property to True/False depending on which fields the user
            // has selected from the Custom Report screen then move them accordingly
            int intRow;
            int intRow2;
            float lngHt;
            if (boolSummaryOnly)
            {
                lblYear.Visible = false;
                fldYear.Visible = false;
                lblAddress.Visible = false;
                fldAddress.Visible = false;
                lblLocation.Visible = false;
                fldLocation.Visible = false;
                lblMapLot.Visible = false;
                fldMapLot.Visible = false;
                lblReference.Visible = false;
                fldRef1.Visible = false;
                lblBuilding.Visible = false;
                fldBuilding.Visible = false;
                lblLand.Visible = false;
                fldLand.Visible = false;
                lblExempt.Visible = false;
                fldExempt.Visible = false;
                fldName.Visible = false;
                fldRef2.Visible = false;
                fldAccount.Visible = false;
                fldAbatement.Visible = false;
                fldPaymentReceived.Visible = false;
                fldAbatementPayments.Visible = false;
                fldTaxDue.Visible = false;
                fldDue.Visible = false;
                fldNonInterestDue.Visible = false;
                lblAccount.Visible = false;
                lnHeader.Visible = false;
                lnTotals.Visible = false;
                lblSpecialTotals.Visible = false;
                fldSpecialTotals.Visible = false;
                Detail.Height = 0;
                return;
            }
            intRow = 2;
            lngHt = 270 / 1440F;
            // set all of the fields visible property
            // 0 = "Location"
            // 1 = "MapLot"
            // 2 = "Address"
            // 3 = "BillingYear"
            // 4 = "BookPage"
            // 5 = "LandValue"
            // 6 = "BuildingValue"
            // 7 = "ExemptValue"
            // ROW 1
            // Year

            lblYear.Visible = true;
            fldYear.Visible = true;
            lblSpecialTotals.Visible = false;
            fldSpecialTotals.Visible = false;

            switch (reportConfiguration.Options.ReportType.ToUpper())
            {
                case "ACCOUNT":
                    break;

                case "ABATE":
                case "REFUNDEDABATE":
                case "PREPAYMENT":
                case "SUPPLEMENTAL":
                case "DISCOUNT":
                case "TAXCLUB":
                case "PAYMENTS":
                case "CORRECTIONS":
                case "COSTS":
                    {
                        // MAL@20081118: Add new field for special amounts to allow for address line to print as well
                        // Tracker Reference: 16260
                        if (reportConfiguration.Options.FullStatusAmounts)
                        {
                            // Amount of type
                            if (reportConfiguration.Options.ReportType.ToUpper() != "COSTS")
                            {
                                lblSpecialTotals.Visible = true;
                                fldSpecialTotals.Visible = true;
                                lblSpecialTotals.Left = 2880 / 1440F;
                                lblSpecialTotals.Width = 2880 / 1440F;
                                lblSpecialTotals.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
                                fldSpecialTotals.Left = fldAbatement.Left;
                                fldSpecialTotals.Width = fldAbatement.Width;
                                fldSpecialTotals.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
                                fldSpecialTotals.Top = intRow * lngHt;
                                lblSpecialTotals.Top = intRow * lngHt;
                            }
                            intRow += 1;
                        }
                        else
                        {
                            fldSpecialTotals.Visible = false;
                            lblSpecialTotals.Visible = false;
                            fldSpecialTotals.Top = 0;
                            lblSpecialTotals.Top = 0;
                        }

                        break;
                    }
            }
            // Address
            // MAL@20081118: Have address line show regardless of report type

            if (reportConfiguration.Options.ShowAddress)
            {
                lblAddress.Visible = true;
                fldAddress.Visible = true;
                // lblAddress.Left = 2880
                lblAddress.Left = 1980 / 1440F;
                lblAddress.Width = 1440 / 1440F;
                // fldAddress.Left = 4320
                fldAddress.Left = 2970 / 1440F;
                fldAddress.Width = 8100 / 1440F;
                lblAddress.Top = intRow * lngHt;
                fldAddress.Top = intRow * lngHt;
                intRow += 1;
            }
            else
            {
                intRow = intRow;
                lblAddress.Visible = false;
                fldAddress.Visible = false;
            }
            // Location
            if (reportConfiguration.Options.ShowLocation)
            {
                lblLocation.Visible = true;
                fldLocation.Visible = true;
                // lblLocation.Left = 2880
                lblLocation.Left = 1980 / 1440F;
                lblLocation.Width = 1440 / 1440F;
                // fldLocation.Left = 4320
                fldLocation.Left = 2970 / 1440F;
                fldLocation.Width = 4320 / 1440F;
                lblLocation.Top = intRow * lngHt;
                fldLocation.Top = intRow * lngHt;
                intRow += 1;
            }
            else
            {
                lblLocation.Visible = false;
                fldLocation.Visible = false;
                fldLocation.Top = 0;
            }
            intRow2 = intRow;
            // splits into two columns
            // col 1
            if (reportConfiguration.Options.ShowMapLot)
            {
                lblMapLot.Visible = true;
                fldMapLot.Visible = true;
                // lblMapLot.Left = 2880
                lblMapLot.Left = 1980 / 1440F;
                lblMapLot.Width = 1440 / 1440F;
                // fldMapLot.Left = 4320
                fldMapLot.Left = 2970 / 1440F;
                fldMapLot.Width = 4320 / 1440F;
                lblMapLot.Top = intRow * lngHt;
                fldMapLot.Top = intRow * lngHt;
                intRow += 1;
            }
            else
            {
                lblMapLot.Visible = false;
                fldMapLot.Visible = false;
                lblMapLot.Top = 0;
                fldMapLot.Top = 0;
            }

            lblReference.Visible = false;
            fldRef1.Visible = false;
            lblReference.Top = 0;
            fldRef1.Top = 0;

            lblReference2.Visible = false;
            fldRef2.Visible = false;
            lblReference2.Top = 0;
            fldRef2.Top = 0;

            lblBuilding.Visible = false;
            fldBuilding.Visible = false;
            lblBuilding.Top = 0;
            fldBuilding.Top = 0;

            lblLand.Visible = false;
            fldLand.Visible = false;
            lblLand.Top = 0;
            fldLand.Top = 0;

            lblExempt.Visible = false;
            fldExempt.Visible = false;
            lblExempt.Top = 0;
            fldExempt.Top = 0;

            if (reportConfiguration.Options.ShowPayments)
            //if (frmCustomReport.InstancePtr.boolShowPaymentBreakdown)
            {
                this.Detail.CanGrow = true;
                if (intRow > intRow2)
                {
                    this.Detail.Height = intRow * lngHt + 100 / 1440F;
                }
                else
                {
                    this.Detail.Height = intRow2 * lngHt + 100 / 1440F;
                }
                srptSLAllActivityDetailOB.Top = this.Detail.Height;
            }
            else
            {
                if (intRow > intRow2)
                {
                    this.Detail.Height = intRow * lngHt + 100 / 1440F;
                }
                else
                {
                    this.Detail.Height = intRow2 * lngHt + 100 / 1440F;
                }
                srptSLAllActivityDetailOB.Top = 0;
                this.Detail.CanGrow = false;
                this.Detail.CanShrink = false;
            }
        }

        private void BindFields()
        {
            // On Error GoTo ERROR_HANDLER
            // this will fill the information into the fields
            clsDRWrapper rsPayment = new clsDRWrapper();
            clsDRWrapper rsRE = new clsDRWrapper();
            string strTemp = "";
            double dblTotalPayment;
            double dblTotalAbate;
            double dblTotalRefundAbate;
            double dblTotalRefundAbate2;
            double dblTotalPPayment;
            double dblTotalCorrection;
            double dblTaxClub;
            double dblCurInt;
            double dblLienCurInt;
            DateTime dtPaymentDate;
            bool boolREInfo;
            double dblDiscount;
            double dblPrePay;
            double dblSupplemental;
            double dblJustAbate;
            double dblPreLienIntNeed;
            double dblLienCostNeed;
            double dblPreLienIntPaid;
            double dblLienCostPaid;
            double dblInterestCharged;
            double dblInterestPaid;
            double dblCurrentInterest;
            // this is the calculated current interest for each bill
            double dblDue = 0;
            string strSLPayRange = "";
            double dblTest = 0;
            double dblTestInt = 0;
            double dblCostPaid;
            double dblAbate1 = 0;
            double dblAbate2 = 0;
            double dblAbate3 = 0;
            double dblAbate4 = 0;
            double dblNonInt;
            string strFields = "";

            try
            {


            TRYNEXTACCOUNT:
                
                //counter += 1;
                ClearFields();
                dblNonInt = 0;
                dblTotalPayment = 0;
                dblTotalAbate = 0;
                dblTotalRefundAbate = 0;
                dblTotalRefundAbate2 = 0;
                dblTotalPPayment = 0;
                dblTotalCorrection = 0;
                dblTaxClub = 0;
                dblCurInt = 0;
                dblLienCurInt = 0;
                dtPaymentDate = DateTime.Today;
                boolREInfo = false;
                dblDiscount = 0;
                dblPrePay = 0;
                dblSupplemental = 0;
                dblJustAbate = 0;
                dblPreLienIntNeed = 0;
                dblLienCostNeed = 0;
                dblPreLienIntPaid = 0;
                dblLienCostPaid = 0;
                dblCurrentInterest = 0;
                dblCostPaid = 0;
                // lblAddress.Text = ""
                if (rsData.EndOfFile())
                {
                    if (boolStarted)
                    {
                        srptSLAllActivityDetailOB.Visible = false;
                        return;
                    }
                    else
                    {
                        boolStarted = true;
                        strTemp = BuildSQL();
                        rsData.OpenRecordset(strTemp, modExtraModules.strCLDatabase);
                        strTemp = "";
                        if (rsData.EndOfFile())
                        {
                            srptSLAllActivityDetailOB.Visible = false;
                            return;
                        }
                    }
                }

                // this will not allow duplicates
                if (lngLastBK == rsData.Get_Fields("BK"))
                {
                    // if it is the same as the last billkey then do not use this bill
                    rsData.MoveNext();
                    // clear fields
                    goto TRYNEXTACCOUNT;
                }

                lngLastBK = FCConvert.ToInt32(rsData.Get_Fields("BK"));

                if (boolShowPreLienPayments)
                {
                    rsRE.OpenRecordset($"SELECT ID FROM PaymentRec WHERE BillKey = {FCConvert.ToString(rsData.Get_Fields_Int32("BillKey"))} AND BillCode = 'R'", modExtraModules.strCLDatabase);
                    if (rsRE.EndOfFile())
                    {
                        srptSLAllActivityDetailOB.Visible = false;
                        rsData.MoveNext();
                        // clear fields
                        goto TRYNEXTACCOUNT;
                    }
                }

                boolREInfo = modStatusPayments.Statics.boolRE ? !rsData.IsFieldNull("RSAccount") : (bool) (FCConvert.ToString(rsData.Get_Fields("Account")) != "");

                // if boolREInfo is true then get the information from the Master table of the Real Estate database
                // if it is false then get it from the BillingMaster of the Collections database
                var name1 = rsData.Get_Fields_String("Name1");

                var streetNumber = rsData.Get_Fields("StreetNumber");

                var streetName = FCConvert.ToString(rsData.Get_Fields_String("StreetName"));

                var apt = rsData.Get_Fields_String("Apt");

                if (reportConfiguration.BillingType == PropertyTaxBillType.Real)
                {
                    fldName.Text = GetStatusName(rsData);
                    if (boolREInfo && reportConfiguration.Options.NameToShow != TaxReportNameOption.BilledOwner)
                    {
                        fldAddress.Text = Strings.Trim(Strings.Trim(rsData.Get_Fields_String("Own1Address1")) + " " + Strings.Trim(rsData.Get_Fields_String("Own1Address2"))) + " " + Strings.Trim(Strings.Trim(rsData.Get_Fields_String("Own1City")) + " " + Strings.Trim(rsData.Get_Fields_String("Own1State")) + " " + Strings.Trim(rsData.Get_Fields_String("Own1Zip")));
                        fldLocation.Text = Strings.Trim(Strings.Trim(rsData.Get_Fields_String("RSLOCAPT")) + " " + Strings.Trim(rsData.Get_Fields_String("RSLOCNUMALPH")) + " " + Strings.Trim(rsData.Get_Fields_String("RSLOCSTREET")));
                        fldMapLot.Text = Strings.Trim(rsData.Get_Fields_String("RSMAPLOT") + " ");
                        fldRef1.Text = Strings.Trim(rsData.Get_Fields_String("RSREF1") + " ");
                        fldRef2.Text = Strings.Trim(rsData.Get_Fields_String("RSREF2") + " ");
                    }
                    else
                    {
                        fldAddress.Text = Strings.Trim(Strings.Trim(rsData.Get_Fields_String("Address1")) + " " + Strings.Trim(rsData.Get_Fields_String("Address2"))) + " " + Strings.Trim(Strings.Trim(rsData.Get_Fields_String("Address3")));

                        fldLocation.Text = Strings.Trim(streetNumber == 0
                                                            ? Strings.Trim(FCConvert.ToString(apt) + " " + streetName)
                                                            : (string)Strings.Trim(apt + " " + FCConvert.ToString(streetNumber) + " " + streetName));

                        fldMapLot.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("MapLot")) + " ");
                        fldRef1.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("BookPage")) + " ");
                        fldRef2.Text = "";
                    }
                }
                else
                {
                    if (boolREInfo && reportConfiguration.Options.NameToShow != TaxReportNameOption.BilledOwner)
                    {
                        // if this is true then get the info from the PP Master, else get it from the bill record
                        var owner1FullName = FCConvert.ToString(rsData.Get_Fields("Own1FullName"));

                        switch (reportConfiguration.Options.NameToShow)
                        {
                            case TaxReportNameOption.CurrentOwner:
                                {
                                    fldName.Text = owner1FullName;
                                    break;
                                }
                            case TaxReportNameOption.BilledOwnerCareOfCurrentOwner:
                                {
                                    if (Strings.UCase(FCConvert.ToString(name1)) != Strings.UCase(owner1FullName))
                                    {
                                        fldName.Text = FCConvert.ToString(name1) + " C\\O " + owner1FullName;
                                    }
                                    else
                                    {
                                        fldName.Text = name1;
                                    }
                                    break;
                                }
                            case TaxReportNameOption.CurrentOwnerBilledAsOwner:
                                {
                                    if (Strings.UCase(FCConvert.ToString(name1)) != Strings.UCase(owner1FullName))
                                    {
                                        fldName.Text = owner1FullName + " Bill = " + FCConvert.ToString(name1);
                                    }
                                    else
                                    {
                                        fldName.Text = owner1FullName;
                                    }
                                    break;
                                }
                        }

                        fldAddress.Text = Strings.Trim(Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Own1Address1"))) + " " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Own1Address2")))) + " " + Strings.Trim(Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Own1City"))) + " " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Own1State"))) + " " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Own1Zip"))));
                        fldLocation.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields("PPStreetNumber")) + " " + FCConvert.ToString(rsData.Get_Fields_String("Street")));
                        fldMapLot.Visible = false;
                        lblMapLot.Visible = false;
                        fldRef1.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Open1")) + " ");
                        fldRef2.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Open2")) + " ");
                    }
                    else
                    {
                        fldName.Text = boolREInfo ? name1 : "*" + FCConvert.ToString(name1);
                        fldAddress.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address1")) + " " + FCConvert.ToString(rsData.Get_Fields_String("Address2"))) + " " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address3")));
                        fldLocation.Text = streetNumber != 0
                            ? (string)Strings.Trim(FCConvert.ToString(apt) + " " + FCConvert.ToString(streetNumber) + " " + streetName)
                            : Strings.Trim(FCConvert.ToString(apt) + " " + streetName);

                        // fldMapLot.Text = Trim(rsData.Fields("MapLot") & " ")
                        fldMapLot.Visible = false;
                        lblMapLot.Visible = false;
                        fldRef1.Visible = false;
                        fldRef2.Visible = false;
                        lblReference.Visible = false;
                        lblReference2.Visible = false;
                        // fldRef1.Text = Trim(rsData.Fields("BookPage") & " ")
                        // fldRef2.Text = ""
                    }
                }
                fldAccount.Text = rsData.Get_Fields_Int32("Account").ToString();
                fldYear.Text = modExtraModules.FormatYear(rsData.Get_Fields_Int32("BillingYear").ToString());
                fldTADate.Text = "";
                fldTADate.Visible = false;

                if (reportConfiguration.BillingType == PropertyTaxBillType.Real)
                {
                    if (boolREInfo)
                    {
                        if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("RSDeleted")))
                        {
                            // fldName.Text = "!" & fldName.Text
                            boolDeletedAccount = true;
                        }
                        if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("TaxAcquired")))
                        {
                            fldTADate.Visible = true;
                            if (Information.IsDate(rsData.Get_Fields("TADate")))
                            {
                                fldTADate.Text = "TA : " + Strings.Format(rsData.Get_Fields_DateTime("TADate"), "MM/dd/yyyy");
                            }
                            else
                            {
                                fldTADate.Text = "TA";
                            }
                        }
                    }
                    else
                    {
                        boolDeletedAccount = true;
                    }
                }
                else
                {
                    if (boolREInfo)
                    {
                        if (!rsData.IsFieldNull("Deleted"))
                        {
                            if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("Deleted")))
                            {
                                // fldName.Text = "!" & fldName.Text
                                boolDeletedAccount = true;
                            }
                        }
                    }
                    else
                    {
                        boolDeletedAccount = true;
                    }
                }
                if (boolStarted)
                {
                    fldName.Text = "*" + fldName.Text;
                }
                dblTotalPayment = 0;
                dblTotalAbate = 0;
                dblTotalRefundAbate = 0;
                dblTotalRefundAbate2 = 0;
                dblCurrentInterest = 0;
                dblPreLienIntNeed = 0;
                dblLienCostNeed = 0;
                if (reportConfiguration.Filter.PaymentDateMin.HasValue && reportConfiguration.Filter.PaymentDateMax.HasValue)
                {
                    strSLPayRange = " AND (RecordedTransactionDate >= '" + reportConfiguration.Filter.PaymentDateMin.Value.ToShortDateString() + "' AND RecordedTransactionDate <= '" + reportConfiguration.Filter.PaymentDateMax.Value.ToShortDateString() + "')";
                }

                
                //dblTmp1 = DateAndTime.Timer();
                if (reportConfiguration.Options.UseAsOfDate())
                {
                    if (rsData.Get_Fields_Int32("LienRecordNumber") == 0)
                    {
                        strTemp = "SELECT Code, Principal, CurrentInterest, LienCost, PreLienInterest FROM PaymentRec WHERE (Account = " + FCConvert.ToString(rsData.Get_Fields("Account")) + " AND BillKey = " + FCConvert.ToString(rsData.Get_Fields_Int32("ID")) + " AND BillCode <> 'L') " + strSLPayRange + " AND ReceiptNumber >= 0 AND ISNULL(RecordedTransactionDate, '12/30/1899') <= '" + FCConvert.ToString(reportConfiguration.Options.AsOfDate) + "'";
                        boolLienRec = false;
                        if (reportConfiguration.Options.FullStatusAmounts)
                        {
                            modCLCalculations.CalcBillAsOfFromPayments_540(FCConvert.ToInt32(rsData.Get_Fields_Int32("ID")), reportConfiguration.Options.AsOfDate, ref dblDue, 0, ref dblCurInt, 0, ref dblCurrentInterest, ref dblNonInt);
                            if (!reportConfiguration.Options.ShowCurrentInterest)
                            {
                                dblDue -= dblCurrentInterest;
                            }
                        }
                    }
                    else
                    {
                        rsCalLien.FindFirstRecord("ID", rsData.Get_Fields_Int32("LienRecordNumber"));
                        if (rsCalLien.NoMatch)
                        {
                            boolLienRec = false;
                        }
                        else
                        {
                            // MAL@20080624: Change to check for lien creation date before selecting payments
                            // Tracker Reference: 14306
                            if (Conversion.Val(rsCalLien.Get_Fields_DateTime("DateCreated")) <= reportConfiguration.Options.AsOfDate.ToOADate() || rsCalLien.IsFieldNull("DateCreated") || Conversion.Val(rsCalLien.Get_Fields_DateTime("DateCreated")) == 0)
                            {
                                // Use Lien Record
                                boolLienRec = true;
                                strTemp = "SELECT Code, Principal, CurrentInterest, LienCost, PreLienInterest FROM PaymentRec WHERE (Account = " + FCConvert.ToString(rsData.Get_Fields("Account")) + " AND BillKey = " + FCConvert.ToString(rsData.Get_Fields_Int32("LienRecordNumber")) + " AND BillCode = 'L') " + strSLPayRange + " AND ReceiptNumber >= 0 AND ISNULL(RecordedTransactionDate, '12/30/1899') <= '" + FCConvert.ToString(reportConfiguration.Options.AsOfDate) + "'";
                                if (reportConfiguration.Options.FullStatusAmounts)
                                {
                                    dblDue = modCLCalculations.CalculateAccountCLLien(rsCalLien, reportConfiguration.Options.AsOfDate, ref dblCurrentInterest, ref dblCurInt, ref dblNonInt);
                                    if (!reportConfiguration.Options.ShowCurrentInterest)
                                    {
                                        dblDue -= dblCurrentInterest;
                                    }
                                }
                            }
                            else
                            {
                                boolLienRec = false;
                                strTemp = "SELECT Code, Principal, CurrentInterest, LienCost, PreLienInterest FROM PaymentRec WHERE (Account = " + FCConvert.ToString(rsData.Get_Fields("Account")) + " AND BillKey = " + FCConvert.ToString(rsData.Get_Fields_Int32("BillKey")) + " AND BillCode <> 'L') " + strSLPayRange + " AND ReceiptNumber >= 0 AND ISNULL(RecordedTransactionDate, '12/30/1899') <= '" + FCConvert.ToString(reportConfiguration.Options.AsOfDate) + "'";
                                if (reportConfiguration.Options.FullStatusAmounts)
                                {
                                    modCLCalculations.CalcBillAsOfFromPayments_540(FCConvert.ToInt32(rsData.Get_Fields_Int32("ID")), reportConfiguration.Options.AsOfDate, ref dblDue, 0, ref dblCurInt, 0, ref dblCurrentInterest, ref dblNonInt);
                                    if (!reportConfiguration.Options.ShowCurrentInterest)
                                    {
                                        dblDue -= dblCurrentInterest;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (FCConvert.ToInt32(rsData.Get_Fields_Int32("lienrecordnumber")) > 0)
                        boolLienRec = true;
                    if (rsData.Get_Fields_Int32("LienRecordNumber") == 0 || reportConfiguration.Options.PreLienOnly || !boolLienRec)
                    {
                        // not liened
                        strTemp = "SELECT Code, Principal, CurrentInterest, LienCost, PreLienInterest FROM PaymentRec WHERE (Account = " + FCConvert.ToString(rsData.Get_Fields("Account")) + " AND BillKey = " + FCConvert.ToString(rsData.Get_Fields_Int32("ID")) + strSLPayRange + " AND BillCode <> 'L' AND ReceiptNumber >= 0)";
                        boolLienRec = false;
                        if (reportConfiguration.Options.FullStatusAmounts)
                        {
                            if (Strings.InStr(1, rsData.Name(), "PPMaster") != 0)
                            {
                                dblDue = modCLCalculations.CalculateAccountCL(ref rsData, FCConvert.ToInt32(rsData.Get_Fields("Account")), DateTime.Today, ref dblCurrentInterest, ref dblCurInt, true, ref dblNonInt);
                            }
                            else
                            {
                                modCLCalculations.CheckForAbatementPayments(FCConvert.ToInt32(rsData.Get_Fields_Int32("ID")), FCConvert.ToInt32(rsData.Get_Fields_Int32("RateKey")), FCConvert.ToString(rsData.Get_Fields_String("BillingType")) == "RE", ref dblAbate1, ref dblAbate2, ref dblAbate3, ref dblAbate4, Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1")), Conversion.Val(rsData.Get_Fields_Decimal("TaxDue2")), Conversion.Val(rsData.Get_Fields_Decimal("TaxDue3")), Conversion.Val(rsData.Get_Fields_Decimal("TaxDue4")));
                                dblDue = modCLCalculations.CalculateAccountCL2(ref rsData, FCConvert.ToInt32(rsData.Get_Fields("Account")), DateTime.Today, ref dblCurrentInterest, ref dblCurInt, ref dblAbate1, ref dblAbate2, ref dblAbate3, ref dblAbate4, ref dblNonInt);
                            }
                            if (!reportConfiguration.Options.ShowCurrentInterest)
                            {
                                dblDue -= dblCurrentInterest;
                            }
                        }
                    }
                    else
                    {
                        // lien record
                        // find the information about the payments here
                        strTemp = "SELECT Code, Principal, CurrentInterest, LienCost, PreLienInterest FROM PaymentRec WHERE (Account = " + FCConvert.ToString(rsData.Get_Fields("Account")) + " AND BillKey = " + FCConvert.ToString(rsData.Get_Fields_Int32("LienRecordNumber")) + strSLPayRange + " AND BillCode = 'L' AND ReceiptNumber >= 0)";
                        rsCalLien.FindFirstRecord("ID", rsData.Get_Fields_Int32("LienRecordNumber"));
                        if (rsCalLien.NoMatch)
                        {
                            boolLienRec = false;
                        }
                        else
                        {
                            boolLienRec = true;
                            if (reportConfiguration.Options.FullStatusAmounts)
                            {
                                dblDue = modCLCalculations.CalculateAccountCLLien(rsCalLien, DateTime.Today, ref dblCurrentInterest, ref dblCurInt, ref dblNonInt);
                                if (!reportConfiguration.Options.ShowCurrentInterest)
                                {
                                    dblDue -= dblCurrentInterest;
                                }
                            }
                        }
                    }
                }

                //dblTmp2 = DateAndTime.Timer();
                //dblPerf3 += dblTmp2 - dblTmp1;
                // 0 - 3, 1 - A, 2 - C, 3 - D, 4 - I, 5 - L, 6 - P, 7 - R, 8 - U, 9 - X, 10 - Y
                // 0 - Principal, 1 - PreLienInterest, 2 - CurrentInterest, 3 - Costs, 4 - Total
                if (dblCurrentInterest != 0 && reportConfiguration.Options.ShowCurrentInterest)
                {
                    tPayTypeSummaries.AddPaymentByType("Z", 0, dblCurrentInterest * -1);
                    // current interest
                }
                // open the payment records
                rsPayment.OpenRecordset(strTemp, "Collections");
                if (rsPayment.EndOfFile() != true)
                {

                    if (reportConfiguration.Options.FullStatusAmounts)
                    {
                        // add up all of the payment records
                        while (!rsPayment.EndOfFile())
                        {
                            string code = Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code")));
                            double principal = Conversion.Val(rsPayment.Get_Fields("Principal"));
                            double preLienIntPaid = Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest"));
                            double lienCost = Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost"));
                            double currentInterest = (double)rsPayment.Get_Fields_Decimal("CurrentInterest");

                            if (principal != 0 || currentInterest != 0 || lienCost != 0 || preLienIntPaid != 0)
                            {
                                // kk03312014 trocl-682/trocls-22
                                tPayTypeSummaries.AddPaymentByType(code, principal, currentInterest, preLienIntPaid, lienCost);

                                switch (code)
                                {
                                    case "P":
                                        dblTotalPPayment += principal + preLienIntPaid + currentInterest + lienCost;
                                        dblTotalPayment += principal + preLienIntPaid + currentInterest + lienCost;
                                        // kk03312014 trocl-682/trocls-22  AddToPaymentArray 6, .Fields("Principal"), .Fields("PreLienInterest"), .Fields("CurrentInterest"), .Fields("LienCost")
                                        break;
                                    case "U":
                                        dblTaxClub += principal + preLienIntPaid + currentInterest + lienCost;
                                        dblTotalPayment += principal + preLienIntPaid + currentInterest + lienCost;
                                        // kk03312014 trocl-682/trocls-22  AddToPaymentArray 8, .Fields("Principal"), .Fields("PreLienInterest"), .Fields("CurrentInterest"), .Fields("LienCost")
                                        break;
                                    case "X":
                                        dblTotalPayment += principal + preLienIntPaid + currentInterest + lienCost;
                                        // kk03312014 trocl-682/trocls-22  AddToPaymentArray 9, .Fields("Principal"), .Fields("PreLienInterest"), .Fields("CurrentInterest"), .Fields("LienCost")
                                        break;
                                    case "Y":
                                        dblPrePay += principal + preLienIntPaid + currentInterest + lienCost;
                                        dblTotalPayment += principal + preLienIntPaid + currentInterest + lienCost;

                                        break;
                                    case "C":
                                    case "N":
                                    case "F":
                                        dblTotalAbate += principal + preLienIntPaid + currentInterest + lienCost;
                                        dblTotalCorrection += principal + preLienIntPaid + currentInterest + lienCost;
                                        // kk03312014 trocl-682/trocls-22  AddToPaymentArray 2, .Fields("Principal"), .Fields("PreLienInterest"), .Fields("CurrentInterest"), .Fields("LienCost")
                                        break;
                                    case "A":
                                        dblJustAbate += principal + preLienIntPaid + currentInterest + lienCost;
                                        dblTotalAbate += principal + preLienIntPaid + currentInterest + lienCost;
                                        // kk03312014 trocl-682/trocls-22  AddToPaymentArray 1, .Fields("Principal"), .Fields("PreLienInterest"), .Fields("CurrentInterest"), .Fields("LienCost")
                                        break;

                                    case "S":
                                        dblSupplemental += principal + preLienIntPaid + currentInterest + lienCost;
                                        dblTotalAbate += principal + preLienIntPaid + currentInterest + lienCost;
                                        // put these figures in the X category because there are going away
                                        // kk03312014 trocl-682/trocls-22  AddToPaymentArray 9, .Fields("Principal"), .Fields("PreLienInterest"), .Fields("CurrentInterest"), .Fields("LienCost")
                                        break;

                                    case "D":
                                        dblDiscount += principal + preLienIntPaid + currentInterest + lienCost;
                                        dblTotalAbate += principal + preLienIntPaid + currentInterest + lienCost;
                                        // kk03312014 trocl-682/trocls-22  AddToPaymentArray 3, .Fields("Principal"), .Fields("PreLienInterest"), .Fields("CurrentInterest"), .Fields("LienCost")
                                        break;

                                    case "I":
                                    // kk03312014 trocl-682/trocls-22  AddToPaymentArray 5, .Fields("Principal"), .Fields("PreLienInterest"), .Fields("CurrentInterest"), .Fields("LienCost")
                                    case "L":
                                    // kk03312014 trocl-682/trocls-22  AddToPaymentArray 0, .Fields("Principal"), .Fields("PreLienInterest"), .Fields("CurrentInterest"), .Fields("LienCost")
                                    case "3":
                                        dblTotalAbate += principal + preLienIntPaid + currentInterest + lienCost;
                                        // kk03312014 trocl-682/trocls-22  AddToPaymentArray 4, .Fields("Principal"), .Fields("PreLienInterest"), .Fields("CurrentInterest"), .Fields("LienCost")
                                        break;
                                    case "R":
                                        {
                                            // If .Fields("Principal") + .Fields("PreLienInterest") + .Fields("CurrentInterest") + .Fields("LienCost") > 0 Then
                                            // do not count the negatives because it will all balance to zero and be a worthless number
                                            if (principal + preLienIntPaid + currentInterest + lienCost > 0)
                                            {
                                                dblTotalRefundAbate += principal + preLienIntPaid + currentInterest + lienCost;
                                            }
                                            else
                                            {
                                                dblTotalAbate += principal + preLienIntPaid + currentInterest + lienCost;
                                            }
                                            dblTotalRefundAbate2 += principal + preLienIntPaid + currentInterest + lienCost;
                                            // End If
                                            // kk03312014 trocl-682/trocls-22  AddToPaymentArray 7, .Fields("Principal"), .Fields("PreLienInterest"), .Fields("CurrentInterest"), .Fields("LienCost")
                                            break;
                                        }
                                }
                            }
                            rsPayment.MoveNext();
                        }

                        const string currencyFormat = "$#,##0.00";

                        switch (reportConfiguration.Options.ReportType.ToUpper())
                        {
                            // *****************NEW 01/13/2004 - Will overwrite the address field with the amount of the adjustment asked for
                            // MAL@20081118: Changed to use a different field so address can still be printed
                            // Tracker Reference: 16260
                            case "ABATE":
                                lblSpecialTotals.Text = "Abatement:";
                                fldSpecialTotals.Text = Strings.Format(dblJustAbate, currencyFormat);
                                //dblSpecialTotal += dblJustAbate;

                                break;
                            case "REFUNDEDABATE":
                                lblSpecialTotals.Text = "Refunded Abatement:";
                                fldSpecialTotals.Text = Strings.Format(dblTotalRefundAbate, currencyFormat);
                                //dblSpecialTotal += dblTotalRefundAbate;

                                break;
                            case "PREPAYMENT":
                                lblSpecialTotals.Text = "Prepayment:";
                                fldSpecialTotals.Text = Strings.Format(dblPrePay, currencyFormat);
                                //dblSpecialTotal += dblPrePay;

                                break;
                            case "TAXCLUB":
                                lblSpecialTotals.Text = "Tax Club:";
                                fldSpecialTotals.Text = Strings.Format(dblTaxClub, currencyFormat);
                                //dblSpecialTotal += dblTaxClub;

                                break;
                            case "SUPPLEMENTAL":
                                lblSpecialTotals.Text = "Supplemental:";
                                fldSpecialTotals.Text = Strings.Format(dblSupplemental, currencyFormat);
                                //dblSpecialTotal += dblSupplemental;

                                break;
                            case "DISCOUNT":
                                lblSpecialTotals.Text = "Discount:";
                                fldSpecialTotals.Text = Strings.Format(dblDiscount, currencyFormat);
                                ////dblSpecialTotal += dblDiscount;

                                break;
                            case "PAYMENTS":
                                lblSpecialTotals.Text = "Payments:";
                                fldSpecialTotals.Text = Strings.Format(dblTotalPPayment, currencyFormat);
                                //dblSpecialTotal += dblTotalPPayment;

                                break;
                            case "CORRECTIONS":
                                lblSpecialTotals.Text = "Corrections:";
                                fldSpecialTotals.Text = Strings.Format(dblTotalCorrection, currencyFormat);
                                //dblSpecialTotal += dblTotalCorrection;

                                break;
                        }
                    }
                    else
                    {
                        if (boolLienRec)
                        {
                            // if this is a lien then I need to keep track of the PreLienInterest and Cost Paid as well as the principal
                            dblPreLienIntNeed = Conversion.Val(rsCalLien.Get_Fields("Interest"));
                            dblLienCostNeed = Conversion.Val(rsCalLien.Get_Fields("Costs"));
                            dblLienCostPaid = 0;
                            dblInterestPaid = 0;
                            dblInterestCharged = 0;
                            dblPreLienIntPaid = 0;
                        }

                        while (!rsPayment.EndOfFile())
                        {
                            string code = Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code")));
                            double principal = Conversion.Val(rsPayment.Get_Fields("Principal"));
                            double preLienIntPaid = Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest"));
                            double lienCost = Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost"));
                            double currentInterest = (double)rsPayment.Get_Fields_Decimal("CurrentInterest");

                            if (principal != 0 || lienCost != 0 || preLienIntPaid != 0)
                            {
                                dblPreLienIntPaid += preLienIntPaid;
                                // kk03312014 trocl-682/trocls-22
                                tPayTypeSummaries.AddPaymentByType(code, principal, 0.0, preLienIntPaid, lienCost);
                                var principalPlusLienCost = principal + lienCost;

                                switch (code)
                                {
                                    case "P" when boolLienRec:
                                    // MAL@20071205: Added check for Lien Cost back in
                                    // kk03312014 trocl-682/trocls-22   AddToPaymentArray 6, .Fields("Principal"), .Fields("PreLienInterest"), 0, .Fields("LienCost") '0 ' .Fields("CurrentInterest")
                                    case "P":
                                        dblTotalPayment += principalPlusLienCost;

                                        break;
                                    case "X":
                                    case "S":
                                        {
                                            dblTotalPayment += boolLienRec
                                                ? principalPlusLienCost
                                                : principalPlusLienCost;

                                            break;
                                        }
                                    case "U":
                                    // (.Fields("Principal") + .Fields("PreLienInterest") + .Fields("CurrentInterest") + .Fields("LienCost"))
                                    // kk03312014 trocl-682/trocls-22   AddToPaymentArray 10, .Fields("Principal"), .Fields("PreLienInterest"), 0, .Fields("LienCost")
                                    // MAL@20080418: Add support for previously added payment types
                                    // Tracker Reference: 13187
                                    case "Y":
                                        dblTotalPayment += principalPlusLienCost;
                                        // kk03312014 trocl-682/trocls-22   AddToPaymentArray 8, .Fields("Principal"), .Fields("PreLienInterest"), 0, .Fields("LienCost")
                                        break;
                                    case "C":
                                    case "N":
                                    case "F":
                                        {
                                            if (boolLienRec)
                                            {
                                                dblTotalAbate += principalPlusLienCost;
                                                // (rsPayment.Fields("Principal") + rsPayment.Fields("PreLienInterest") + rsPayment.Fields("CurrentInterest") + rsPayment.Fields("LienCost"))
                                                // kk03312014 trocl-682/trocls-22   AddToPaymentArray 2, rsPayment.Fields("Principal"), rsPayment.Fields("PreLienInterest"), 0, rsPayment.Fields("LienCost")
                                            }
                                            else
                                            {
                                                dblTotalAbate += principalPlusLienCost;
                                                // (rsPayment.Fields("Principal") + rsPayment.Fields("PreLienInterest") + rsPayment.Fields("CurrentInterest") + rsPayment.Fields("LienCost"))
                                                // kk03312014 trocl-682/trocls-22   AddToPaymentArray 2, rsPayment.Fields("Principal"), rsPayment.Fields("PreLienInterest"), 0, rsPayment.Fields("LienCost")    '0      'MAL@20071205
                                            }

                                            break;
                                        }
                                    case "A" when boolLienRec:
                                    // Case "S"
                                    // dblTotalAbate = dblTotalAbate + .Fields("Principal")       '(.Fields("Principal") + .Fields("PreLienInterest") + .Fields("CurrentInterest") + .Fields("LienCost"))
                                    // kk03312014 trocl-682/trocls-22   AddToPaymentArray 1, .Fields("Principal"), .Fields("PreLienInterest"), 0, .Fields("LienCost")    '0 MAL@20071205
                                    case "A":
                                        dblTotalAbate += principalPlusLienCost;
                                        // (.Fields("Principal") + .Fields("PreLienInterest") + .Fields("CurrentInterest") + .Fields("LienCost"))
                                        // kk03312014 trocl-682/trocls-22   AddToPaymentArray 1, .Fields("Principal"), .Fields("PreLienInterest"), 0, .Fields("LienCost")
                                        break;

                                    case "D":
                                        dblTotalAbate += principal;
                                        // + .Fields("LienCost")      '(.Fields("Principal") + .Fields("PreLienInterest") + .Fields("CurrentInterest") + .Fields("LienCost"))
                                        // kk03312014 trocl-682/trocls-22   AddToPaymentArray 3, .Fields("Principal"), .Fields("PreLienInterest"), 0, .Fields("LienCost")
                                        break;

                                    case "I":
                                    // (.Fields("Principal") + .Fields("PreLienInterest") + .Fields("CurrentInterest") + .Fields("LienCost"))
                                    // kk03312014 trocl-682/trocls-22   AddToPaymentArray 5, rsPayment.Fields("Principal"), rsPayment.Fields("PreLienInterest"), 0, .Fields("LienCost")
                                    case "L" when boolLienRec:
                                        dblTotalAbate += -principal + lienCost;
                                        // (.Fields("Principal") + .Fields("PreLienInterest") + .Fields("CurrentInterest") + .Fields("LienCost"))
                                        // kk03312014 trocl-682/trocls-22   AddToPaymentArray 4, .Fields("Principal"), .Fields("PreLienInterest"), 0, .Fields("LienCost")
                                        break;

                                    case "L":
                                        dblTotalAbate -= principal;
                                        // + .Fields("LienCost")      '(.Fields("Principal") + .Fields("PreLienInterest") + .Fields("CurrentInterest") + .Fields("LienCost"))
                                        // kk03312014 trocl-682/trocls-22   AddToPaymentArray 5, .Fields("Principal"), .Fields("PreLienInterest"), 0, .Fields("LienCost")    '0  MAL@20071205
                                        break;
                                    case "3" when boolLienRec:
                                        dblTotalAbate += -principal + lienCost;
                                        // (.Fields("Principal") + .Fields("PreLienInterest") + .Fields("CurrentInterest") + .Fields("LienCost"))
                                        // kk03312014 trocl-682/trocls-22   AddToPaymentArray 0, .Fields("Principal"), .Fields("PreLienInterest"), 0, .Fields("LienCost")
                                        break;

                                    case "3" when principal != 0:
                                        dblTotalPayment += principalPlusLienCost;
                                        // kk03312014 trocl-682/trocls-22   AddToPaymentArray 0, .Fields("Principal"), .Fields("PreLienInterest"), 0, .Fields("LienCost")
                                        break;

                                    case "3":
                                        dblTotalAbate += -principal + lienCost;
                                        // (.Fields("Principal") + .Fields("PreLienInterest") + .Fields("CurrentInterest") + .Fields("LienCost"))
                                        // kk03312014 trocl-682/trocls-22   AddToPaymentArray 0, .Fields("Principal"), .Fields("PreLienInterest"), 0, .Fields("LienCost")
                                        break;
                                    case "R":
                                        {
                                            // MAL@20071009: Changed the way it adds the total abatements
                                            // Call Reference: 117197
                                            if (rsPayment.Get_Fields("Period") == 1)
                                            {
                                                dblTotalRefundAbate += principalPlusLienCost + preLienIntPaid;
                                                // + .Fields("CurrentInterest")
                                            }
                                            else
                                            {
                                                dblTotalAbate += principalPlusLienCost + preLienIntPaid;
                                                // + .Fields("CurrentInterest")
                                            }

                                            dblTotalRefundAbate2 += principalPlusLienCost + preLienIntPaid;

                                            break;
                                        }
                                }
                            }
                            else
                            {
                                dblPreLienIntPaid += preLienIntPaid;
                                // kk03312014 trocl-682/trocls-22
                                tPayTypeSummaries.AddPaymentByType(code, principal, 0.0, preLienIntPaid, lienCost);

                            }
                            rsPayment.MoveNext();
                        }
                    }
                }
                else
                {
                    // if no payment records, then make everything zeros
                    fldPaymentReceived.Text = "0.00";
                    fldAbatement.Text = "0.00";
                    fldAbatementPayments.Text = "0.00";
                    if (modGlobal.Statics.gboolSLDateRange)
                    {
                        rsData.MoveNext();
                        var detailSubReport = new srptSLAllActivityDetail();
                        detailSubReport.SetReportOption(reportConfiguration);
                        srptSLAllActivityDetailOB.Report = detailSubReport;
                        // clear fields
                        tPayTypeSummaries.DiscardAllPayments();
                        // kk05022014 trocl-979  Need to clear Current Interest out
                        goto TRYNEXTACCOUNT;
                    }
                }
                
                // this is the current balance due
                if (!boolStarted)
                {
                    var trxFromBillingDateFirst = rsData.Get_Fields_DateTime("transferfrombillingdatefirst");

                    if (rsData.Get_Fields_Int32("LienRecordNumber") == 0)
                    {
                        // this is the original tax due
                        if (!reportConfiguration.Options.UseAsOfDate() || !Information.IsDate(trxFromBillingDateFirst))
                        {
                            fldTaxDue.Text = Strings.Format(Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue4")), "#,##0.00");
                        }
                        else
                        {
                            //FC:FINAL:MHO: #i160: correct date conversion
                            if (trxFromBillingDateFirst is DateTime)
                            {
                                fldTaxDue.Text = DateAndTime.DateDiff("d", reportConfiguration.Options.AsOfDate, (DateTime)trxFromBillingDateFirst) <= 0 
                                    ? Strings.Format(Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue4")), "#,##0.00") 
                                    : "0.00";
                            }
                            else
                            {
                                fldTaxDue.Text = Strings.Format(Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue4")), "#,##0.00");
                            }
                        }

                       // lngPrfCnt4 += 1;
                        //dblTmp1 = DateAndTime.Timer();
                        dblTest = dblDue;
                        //dblTmp2 = DateAndTime.Timer();
                       // dblPerf4 += dblTmp2 - dblTmp1;
                        if (reportConfiguration.Options.ShowCurrentInterest)
                        {
                            dblTotalAbate -= dblCurrentInterest;
                        }

                        fldDue.Text = Strings.Format(dblTest, "#,##0.00");
                        fldNonInterestDue.Text = Strings.Format(dblNonInt, "#,##0.00");
                    }
                    else
                    {
                        if (boolLienRec)
                        {
                            fldTaxDue.Text = Strings.Format(Conversion.Val(rsCalLien.Get_Fields("Principal")) + Conversion.Val(rsCalLien.Get_Fields("Interest")) + Conversion.Val(rsCalLien.Get_Fields("Costs")), "#,##0.00");
                            fldDue.Text = reportConfiguration.Options.ShowCurrentInterest ? Strings.Format(FCConvert.ToDouble(fldTaxDue.Text) - (dblTotalPayment + dblTotalAbate + dblTotalRefundAbate + dblPreLienIntPaid + dblCostPaid) + dblCurrentInterest, "#,##0.00") : Strings.Format(FCConvert.ToDouble(fldTaxDue.Text) - (dblTotalPayment + dblTotalAbate + dblTotalRefundAbate + dblPreLienIntPaid + dblCostPaid), "#,##0.00");
                            fldNonInterestDue.Text = Strings.Format(Conversion.Val(rsCalLien.Get_Fields("Principal")) + Conversion.Val(rsCalLien.Get_Fields("Costs")) + Conversion.Val(rsCalLien.Get_Fields("interest")) - (dblTotalPayment + dblTotalAbate + dblTotalRefundAbate + dblPreLienIntPaid + dblCostPaid), "#,##0.00");

                            if (modGlobal.Statics.gboolSLDateRange)
                            {
                                dblTest = modCLCalculations.CalculateAccountCLLien(rsCalLien, DateTime.Today, ref dblTestInt, ref dblCurInt, ref dblNonInt);
                                if (!reportConfiguration.Options.ShowCurrentInterest)
                                {
                                    dblTest -= dblTestInt;
                                }
                                fldDue.Text = Strings.Format(dblTest, "#,##0.00");
                                fldNonInterestDue.Text = Strings.Format(dblNonInt, "#,###,##0.00");
                            }
                        }
                        else
                        {
                            if (reportConfiguration.Options.PreLienOnly || reportConfiguration.Options.UseAsOfDate())
                            {
                                if (!reportConfiguration.Options.UseAsOfDate() || !Information.IsDate(trxFromBillingDateFirst))
                                {
                                    fldTaxDue.Text = Strings.Format(Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue4")), "#,##0.00");
                                }
                                else
                                {
                                    //FC:FINAL:MHO: #i160: correct date conversion
                                    if (trxFromBillingDateFirst is DateTime)
                                    {
                                        fldTaxDue.Text = DateAndTime.DateDiff("d", reportConfiguration.Options.AsOfDate, (DateTime)trxFromBillingDateFirst) <= 0 ? Strings.Format(Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue4")), "#,##0.00") : "0.00";
                                    }
                                    else
                                    {
                                        fldTaxDue.Text = Strings.Format(Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue4")), "#,##0.00");
                                    }
                                }
                                fldDue.Text = Strings.Format(FCConvert.ToDouble(fldTaxDue.Text) - (dblTotalPayment + dblTotalAbate + dblTotalRefundAbate + dblPreLienIntPaid + dblCostPaid) + dblCurrentInterest, "#,##0.00");
                                // + dblLienCostPaid
                                fldNonInterestDue.Text = Strings.Format(FCConvert.ToDouble(fldTaxDue.Text) - (dblTotalPayment + dblTotalAbate + dblTotalRefundAbate + dblPreLienIntPaid + dblCostPaid), "#,##0.00");
                            }
                            else
                            {
                                // this is the original tax due because of an error finding the Lien Record
                                fldTaxDue.Text = Strings.Format(Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue4")), "#,##0.00");
                                fldDue.Text = reportConfiguration.Options.ShowCurrentInterest ? Strings.Format(FCConvert.ToDouble(fldTaxDue.Text) - (dblTotalPayment + dblTotalAbate + dblTotalRefundAbate + dblPreLienIntPaid + dblCostPaid) + dblCurrentInterest, "#,##0.00") : Strings.Format(FCConvert.ToDouble(fldTaxDue.Text) - (dblTotalPayment + dblTotalAbate + dblTotalRefundAbate + dblPreLienIntPaid + dblCostPaid), "#,##0.00");
                                fldNonInterestDue.Text = Strings.Format(FCConvert.ToDouble(fldTaxDue.Text) - (dblTotalPayment + dblTotalAbate + dblTotalRefundAbate + dblPreLienIntPaid + dblCostPaid), "#,##0.00");
                                fldTaxDue.Text = "**" + fldTaxDue.Text;
                                boolRTError = true;
                                // show the error at the bottom of the report
                            }
                        }
                    }
                }
                else
                {
                    // check for a no rate table error
                    if (rsData.Get_Fields_Int32("RateKey") == 0)
                    {
                        if (rsData.Get_Fields_Int32("LienRecordNumber") != 0)
                        {
                            rsCalLien = new clsDRWrapper();

                            try
                            {
                                rsCalLien.OpenRecordset($"SELECT * FROM LienRec WHERE LienRecordNumber = {FCConvert.ToString(rsData.Get_Fields_Int32("LienRecordNumber"))}");
                                if (rsCalLien.EndOfFile() != true && rsCalLien.BeginningOfFile() != true)
                                {
                                    dtPaymentDate = DateTime.Today;
                                    fldDue.Text = "**" + fldTaxDue.Text;
                                    fldNonInterestDue.Text = fldDue.Text;
                                }
                                else
                                {
                                    fldDue.Text = "**" + fldDue.Text;
                                    fldNonInterestDue.Text = fldDue.Text;
                                    boolRTError = true;
                                    // show the error at the bottom of the report
                                }
                            }
                            finally
                            {
                                rsCalLien.DisposeOf();
                            }
                        }
                        else
                        {
                            fldDue.Text = "**" + fldDue.Text;
                            fldNonInterestDue.Text = fldDue.Text;
                            boolRTError = true;
                            // show the error at the bottom of the report
                        }
                    }
                }

                // this is how much was recieved in the payments for Principal, Interest and Costs
                fldPaymentReceived.Text = boolLienRec ? Strings.Format(dblTotalPayment + dblPreLienIntPaid + dblCostPaid, "#,##0.00") : Strings.Format(dblTotalPayment, "#,##0.00");
                // fldAbatement.Text = Format(dblTotalAbate - dblCurrentInterest, "#,##0.00")
                fldAbatement.Text = Strings.Format(dblTotalAbate, "#,##0.00");
                fldAbatementPayments.Text = Strings.Format(dblTotalRefundAbate, " #,##0.00");
                // now check to make sure that this account matches the balance due criteria, if not go to the next account
                if (reportConfiguration.Filter.BalanceDueComparisonType.HasValue && reportConfiguration.Filter.BalanceDueAmount.HasValue)
                {
                    if (CheckBalanceDue_2(FCConvert.ToDouble(fldDue.Text)))
                    {
                        tPayTypeSummaries.SaveAllPayments();
                    }
                    else
                    {
                        // reverse the payments that have already been calculated for this account so that they are not shown in the payment summary totals at the bottom			
                        tPayTypeSummaries.DiscardAllPayments();
                        rsData.MoveNext();
                        // clear fields
                        goto TRYNEXTACCOUNT;
                    }
                }
                else
                {
                    if (boolExcludeZeroBalance && !(Conversion.Val(fldDue.Text) > 0))
                    {
                        // reverse the payments that have already been calculated for this account so that they are not shown in the payment summary totals at the bottom
                        // kk03312014 trocl-682/trocls-22   ReversePaymentsFromStatusArray rsPayment
                        tPayTypeSummaries.DiscardAllPayments();
                        rsData.MoveNext();
                        // clear fields
                        goto TRYNEXTACCOUNT;
                    }
                    else
                    {
                        // kk03312014 trocl-682/trocls-22
                        tPayTypeSummaries.SaveAllPayments();
                    }
                }
                if (reportConfiguration.Options.ShowPayments)
                //if (frmCustomReport.InstancePtr.boolShowPaymentBreakdown)
                {
                    // if the user wants to have all of the payments shown, then set it here
                    srptSLAllActivityDetailOB.Visible = true;
                    var detailSubReport = new srptSLAllActivityDetail();
                    detailSubReport.SetReportOption(reportConfiguration);
                    srptSLAllActivityDetailOB.Report = detailSubReport;
                    srptSLAllActivityDetailOB.Report.UserData = rsData.Get_Fields_Int32("ID");
                }
                else
                {
                    srptSLAllActivityDetailOB.Visible = false;
                }
                lngCount += 1;
                dblTotals[0] += FCConvert.ToDouble(fldTaxDue.Text.Replace("*", ""));
                dblTotals[1] += FCConvert.ToDouble(fldPaymentReceived.Text.Replace("*", ""));
                dblTotals[2] += FCConvert.ToDouble(fldAbatement.Text.Replace("*", ""));
                if (Strings.Trim(fldNonInterestDue.Text) != "")
                {
                    dblTotals[6] += FCConvert.ToDouble(fldNonInterestDue.Text.Replace("*", ""));
                }
                var billingYear = rsData.Get_Fields_Int32("billingyear");
                if (billingYear > 19800 && Strings.Trim(fldDue.Text) != "")
                {

                    if (Strings.Left(fldDue.Text, 2) != "**")
                    {
                        dblTotals[3] += FCConvert.ToDouble(fldDue.Text.Replace("*", ""));
                        // keep track for the year totals
                        dblYearTotals[rsData.Get_Fields_Int32("BillingYear") - 19800] = dblYearTotals[FCConvert.ToInt16(rsData.Get_Fields_Int32("BillingYear")) - 19800] + FCConvert.ToDouble(fldDue.Text);
                        dblYearNonIntTotals[billingYear - 19800] = dblYearNonIntTotals[FCConvert.ToInt16(rsData.Get_Fields_Int32("BillingYear")) - 19800] + FCConvert.ToDouble(fldNonInterestDue.Text.Replace("*", ""));
                    }
                    else
                    {
                        dblTotals[3] += FCConvert.ToDouble(Strings.Right(fldDue.Text, fldDue.Text.Length - 2));
                        // keep track for the year totals
                        dblYearTotals[rsData.Get_Fields_Int32("BillingYear") - 19800] = dblYearTotals[FCConvert.ToInt16(rsData.Get_Fields_Int32("BillingYear")) - 19800] + FCConvert.ToDouble(Strings.Right(fldDue.Text, fldDue.Text.Length - 2));
                        dblYearNonIntTotals[billingYear - 19800] = dblYearNonIntTotals[FCConvert.ToInt16(rsData.Get_Fields_Int32("BillingYear")) - 19800] + FCConvert.ToDouble(fldNonInterestDue.Text.Replace("*", ""));
                    }
                }
                dblTotals[4] += FCConvert.ToDouble(fldAbatementPayments.Text);
                fldBuilding.Text = Strings.Format(rsData.Get_Fields_Int32("BuildingValue"), "#,##0");
                fldLand.Text = Strings.Format(rsData.Get_Fields_Int32("LandValue"), "#,##0");
                fldExempt.Text = Strings.Format(rsData.Get_Fields_Int32("ExemptValue"), "#,##0");
                lblAddress.Text = "Address :";
                lblMapLot.Text = "Map Lot : ";
                lblLocation.Text = "Location : ";
                // kk01132015 trocls-13  Keep count of accounts and Bills by Year
                if (!dctAccounts.ContainsKey(FCConvert.ToInt32(Conversion.Val(rsData.Get_Fields("Account")))))
                {
                    dctAccounts.Add(FCConvert.ToInt32(Conversion.Val(rsData.Get_Fields("Account"))), name1);
                }

                lngArrBillCounts[rsData.Get_Fields_Int32("BillingYear") - 19800] = lngArrBillCounts[FCConvert.ToInt16(rsData.Get_Fields_Int32("BillingYear")) - 19800] + 1;
                // move to the next record in the query
                rsData.MoveNext();
                return;
            }
            finally
            {
                rsPayment.DisposeOf();
                rsRE.DisposeOf();
            }
            
        }

        private bool CheckBalanceDue_2(double dblAmt)
        {
            return CheckBalanceDue(ref dblAmt);
        }

        private bool CheckBalanceDue(ref double dblAmt)
        {
            if (boolExcludeZeroBalance && dblAmt == 0)
            {
                return false;
            }

            return reportConfiguration.Filter.MeetsBalanceDueCriteria((decimal)dblAmt);
        }

        private void ClearFields()
        {
            // this routine will clear the fields
            fldAccount.Text = "";
            fldName.Text = "";
            fldYear.Text = "";
            fldTaxDue.Text = "";
            fldPaymentReceived.Text = "";
            fldAbatement.Text = "";
            fldDue.Text = "";
            fldNonInterestDue.Text = "";
            fldAbatementPayments.Text = "";
            fldMapLot.Text = "";
            fldLocation.Text = "";
            lblAddress.Text = "";
            fldAddress.Text = "";
            lblMapLot.Text = "";
            lblLocation.Text = "";
        }

        private void ActiveReport_Terminate(object sender, EventArgs e)
        {
            FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
            rsData.DisposeOf();
            rsCalLien.DisposeOf();
        }

        private void Detail_Format(object sender, EventArgs e)
        {
            BindFields();
        }

        private void SetupTotals()
        {
            int intSumRows;
            int intCT;
            // this sub will fill in the totals line at the bottom of the report
            SetupTotalSummary();
            // Load Summary List
            intSumRows = 1;
            for (intCT = 0; intCT <= Information.UBound(dblYearTotals, 1) - 1; intCT++)
            {
                if (dblYearTotals[intCT] != 0 || dblYearNonIntTotals[intCT] != 0)
                {
                    AddSummaryRow(intSumRows, dblYearTotals[intCT], intCT + 19800, dblYearNonIntTotals[intCT], lngArrBillCounts[intCT]);
                    intSumRows += 1;
                }
            }
            fldTotalNonInterestDue.Text = Strings.Format(dblTotals[6], "#,##0.00");
            fldTotalTaxDue.Text = Strings.Format(dblTotals[0], "#,##0.00");
            fldTotalPaymentReceived.Text = Strings.Format(dblTotals[1], "#,##0.00");
            fldTotalAbatement.Text = Strings.Format(dblTotals[2], "#,##0.00");
            fldTotalDue.Text = Strings.Format(dblTotals[3], "#,##0.00");
            fldTotalAbatementPayments.Text = Strings.Format(dblTotals[4], "#,##0.00");
            // kk01132015 trocls-13  Add count of accounts
            if (dctAccounts.Count > 1)
            {
                fldAcctCount.Text = FCConvert.ToString(dctAccounts.Count) + " Accounts";
            }
            else if (dctAccounts.Count == 1)
            {
                fldAcctCount.Text = FCConvert.ToString(dctAccounts.Count) + " Account";
            }
            else
            {
                fldAcctCount.Text = "";
            }
            if (lngCount > 1)
            {
                lblTotals.Text = "Total for " + FCConvert.ToString(lngCount) + " Bills:";
            }
            else if (lngCount == 1)
            {
                lblTotals.Text = "Total for " + FCConvert.ToString(lngCount) + " Bill:";
            }
            else
            {
                lblTotals.Text = "No Bills";
            }
            // create the total fields and fill them
            // add a field
            obNew1.Name = "fldSummaryTotal";
            obNew1.Top = fldSummary1.Top + ((intSumRows - 1) * fldSummary1.Height);
            obNew1.Left = fldSummary1.Left;
            obNew1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
            obNew1.Width = fldSummary1.Width;
            obNew1.Font = lblSummary1.Font;
            obNew1.Text = Strings.Format(dblPDTotal, "#,##0.00");
            obNew2.Name = "fldNonSummaryTotal";
            obNew2.Top = fldnonsummary1.Top + ((intSumRows - 1) * fldnonsummary1.Height);
            obNew2.Left = fldnonsummary1.Left;
            obNew2.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
            obNew2.Width = fldnonsummary1.Width;
            obNew2.Font = fldnonsummary1.Font;
            obNew2.Text = Strings.Format(dblNonIntTotal, "#,###,##0.00");
            // kk01082015 trocls-13   add a field for the total count
            obNew3.Name = "fldSumCountTotal";
            obNew3.Top = fldSumCount1.Top + ((intSumRows - 1) * fldSumCount1.Height);
            obNew3.Left = fldSumCount1.Left;
            obNew3.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
            obNew3.Width = fldSumCount1.Width;
            obNew3.Font = fldSumCount1.Font;
            obNew3.Text = lngCount.ToString();
            // add a label
            obLabel.Name = "lblPerDiemTotal";
            obLabel.Top = lblSummary1.Top + ((intSumRows - 1) * lblSummary1.Height);
            obLabel.Left = lblSummary1.Left;
            obLabel.Font = lblSummary1.Font;
            obLabel.Text = "Total";
            // add a line
            obLine.Name = "lnFooterSummaryTotal";
            obLine.X1 = fldSumCount1.Left;
            // kk01082015 trocls-13     fldnonsummary1.Left
            obLine.X2 = fldSummary1.Left + fldSummary1.Width;
            obLine.Y1 = lblSummary1.Top + ((intSumRows - 1) * lblSummary1.Height);
            obLine.Y2 = obLine.Y1;
            ReportFooter.Height = lblSummary1.Top + (intSumRows * lblSummary1.Height);
            lngExtraRows = ReportFooter.Height;
        }

        private void ReportFooter_Format(object sender, EventArgs e)
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                SetupTotals();
                if (boolRTError)
                {
                    lblRTError.Text = " *  - This account is a liened account. " + "\r\n" + " ** - The rate table or lien record number is missing for this account.";
                    lblRTError.Visible = true;
                    lblRTError.Top = lngExtraRows;
                    this.ReportFooter.Height = lblRTError.Top + lblRTError.Height;
                }
                else
                {
                    lblRTError.Visible = true;
                    lblRTError.Text = " ";
                    lblRTError.Top = lngExtraRows;
                    this.ReportFooter.Height = lblRTError.Top + lblRTError.Height;
                    lblRTError.Top = 0;
                    // Me.ReportFooter.Height = 700
                }
                if (boolDeletedAccount)
                {
                    lblRTError.Text = lblRTError.Text + "!  - This account is a deleted account.";
                    lblRTError.Visible = true;
                    lblRTError.Top = lngExtraRows;
                    this.ReportFooter.Height = lblRTError.Top + lblRTError.Height;
                }
                lblSpecialTotal.Visible = false;
                lblSpecialText.Visible = false;
            }
            catch (Exception ex)
            {
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In Footer Format");
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
        }

        private void SetReportHeader()
        {
            string strTemp = "";
            string strComma = "";
            var filter = reportConfiguration.Filter;
            if (filter.AccountMax.HasValue || filter.AccountMin.HasValue)
            {
                if (filter.AccountMax.HasValue)
                {
                    if (filter.AccountMin.HasValue)
                    {
                        if (filter.AccountMin == filter.AccountMax)
                        {
                            strTemp += "Account: " + filter.AccountMin.Value.ToString() + " ";
                        }
                        else
                        {
                            strTemp += "Account: " + filter.AccountMin.Value.ToString() + " To " +
                                       filter.AccountMax.Value.ToString();
                        }
                    }
                    else
                    {
                        strTemp += "Below Account: " + filter.AccountMax.Value.ToString();
                    }
                }
                else
                {
                    strTemp += "Above Account: " + filter.AccountMin.Value.ToString();
                }

                strComma = ",";
            }

            if (!String.IsNullOrWhiteSpace(filter.NameMin) || !String.IsNullOrWhiteSpace(filter.NameMax))
            {
                strTemp += strComma;
                if (!String.IsNullOrWhiteSpace(filter.NameMin))
                {
                    if (!String.IsNullOrWhiteSpace(filter.NameMax))
                    {
                        strTemp += " Name: " + filter.NameMin + " To " + filter.NameMax;
                    }
                    else
                    {
                        strTemp += " Name: " + filter.NameMin;
                    }
                }
                else
                {
                    strTemp += " Name: " + filter.NameMax;
                }
                strComma = ",";
            }

            if (filter.TaxYearMin.HasValue || filter.TaxYearMax.HasValue)
            {
                strTemp += strComma;
                if (filter.TaxYearMin.HasValue)
                {
                    if (filter.TaxYearMax.HasValue)
                    {
                        strTemp += " Tax Year: " + filter.TaxYearMin.ToString() + " To " + filter.TaxYearMax.ToString();
                    }
                    else
                    {
                        strTemp += " Tax Year: " + filter.TaxYearMin.ToString();
                    }
                }
                else
                {
                    strTemp += " Tax Year: " + filter.TaxYearMax.ToString();
                }
                strComma = ",";
            }

            if (filter.BalanceDueComparisonType.HasValue && filter.BalanceDueAmount.HasValue)
            {
                strTemp += strComma;
                strTemp += " Balance Due " + filter.BalanceDueComparisonType.Value.ToSymbolString() + " " +
                           filter.BalanceDueAmount.Value.ToString();
                strComma = ",";
            }

            if (filter.PaymentDateMin.HasValue && filter.PaymentDateMax.HasValue)
            {
                strTemp += strComma;
                strTemp += " Payment Range " + filter.PaymentDateMin.Value.ToShortDateString() + " to " +
                           filter.PaymentDateMax.Value.ToShortDateString();
                strComma = ",";
            }

            if (reportConfiguration.BillingType == PropertyTaxBillType.Real && (filter.TranCodeMin.HasValue || filter.TranCodeMax.HasValue))
            {
                strTemp += strComma;
                if (filter.TranCodeMin.HasValue)
                {
                    if (filter.TranCodeMax.HasValue)
                    {
                        strTemp += " Trancode: " + filter.TranCodeMin.Value.ToString() + " To " + filter.TranCodeMax.Value.ToString();
                    }
                    else
                    {
                        strTemp += " Trancode: >= " + filter.TranCodeMin.Value.ToString();
                    }
                }
                else
                {
                    strTemp += " Trancode: <= " + filter.TranCodeMax.Value.ToString();
                }
                strComma = ",";
            }

            if (filter.RateRecordMin.HasValue || filter.RateRecordMax.HasValue)
            {
                strTemp += strComma;
                if (filter.RateRecordMin.HasValue)
                {
                    if (filter.RateRecordMax.HasValue)
                    {
                        strTemp += " Rate Key: " + filter.RateRecordMin.Value.ToString() + " To " + filter.RateRecordMax.Value.ToString();
                    }
                    else
                    {
                        strTemp += " Rate Key: " + filter.RateRecordMin.Value.ToString();
                    }
                }
                else
                {
                    strTemp += " Rate Key: " + filter.RateRecordMax.Value.ToString();
                }
                strComma = ",";
            }

            if (filter.TaxBillStatus.HasValue)
            {
                strTemp += strComma;
                switch (filter.TaxBillStatus)
                {
                    case TaxBillStatusType.Lien:
                        strTemp += " Showing Liens";
                        break;
                    case TaxBillStatusType.Regular:
                        strTemp += " Showing Regular";
                        break;
                    case TaxBillStatusType.PreLien:
                        strTemp += " Showing Liens with PreLien Activity";
                        break;
                }
                strComma = ",";
            }


            // check to see if they used the Show Interest checkbox
            if (reportConfiguration.Options.ShowCurrentInterest)
            //if (frmCustomReport.InstancePtr.chkCurrentInterest.CheckState == Wisej.Web.CheckState.Checked)
            {
                if (Strings.Trim(strTemp) == "")
                {
                    strTemp += "Show Interest";
                }
                else
                {
                    strTemp += ", Show Interest";
                }
            }
            if (Strings.Trim(strTemp) == "")
            {
                lblReportType.Text = "Complete List";
            }
            else
            {
                lblReportType.Text = strTemp;
            }
            if (reportConfiguration.Options.ReportType.ToUpper() == "ACCOUNT")
            {
                lblHeader.Text = "Collection Account Status List";
                // find the sort order
                strTemp = reportConfiguration.SortListing;

                if (strTemp == "")
                {
                    strTemp = "Order By: Name, Account, Year";
                }
                else
                {
                    strTemp = "Order By: " + Strings.Left(strTemp, strTemp.Length - 2);
                }
                lblReportType.Text = lblReportType.Text + "\r\n" + strTemp;
            }
            else if (reportConfiguration.Options.ReportType.ToUpper() == "ABATE")
            {
                lblHeader.Text = "Abatement Status List";
            }
            else if (reportConfiguration.Options.ReportType.ToUpper() == "DISCOUNT")
            {
                lblHeader.Text = "Discount Status List";
            }
            else if (reportConfiguration.Options.ReportType.ToUpper() == "SUPPLEMENTAL")
            {
                lblHeader.Text = "Supplemental Status List";
            }
            else if (reportConfiguration.Options.ReportType.ToUpper() == "TAXCLUB")
            {
                lblHeader.Text = "Tax Club Status List";
            }
            else if (reportConfiguration.Options.ReportType.ToUpper() == "COSTS")
            {
                lblHeader.Text = "Lien Costs and 30 Day Notice List";
            }
            else if (reportConfiguration.Options.ReportType.ToUpper() == "REFUNDEDABATE")
            {
                lblHeader.Text = "Refunded Abatement Status List";
            }
            else if (reportConfiguration.Options.ReportType.ToUpper() == "PREPAYMENT")
            {
                lblHeader.Text = "Prepayment Status List";
            }
            else if (reportConfiguration.Options.ReportType.ToUpper() == "PAYMENTS")
            {
                lblHeader.Text = "Payment Status List";
            }
            else if (reportConfiguration.Options.ReportType.ToUpper() == "CORRECTIONS")
            {
                lblHeader.Text = "Correction Status List";
            }
            else
            {
            }
            if (reportConfiguration.Options.UseAsOfDate())
            {
                lblReportType.Text = lblReportType.Text + "   As Of Date: " + Strings.Format(reportConfiguration.Options.AsOfDate, "MM/dd/yyyy");
            }
            if (reportConfiguration.BillingType == PropertyTaxBillType.Real)
            {
                lblHeader.Text = "RE " + lblHeader.Text;
            }
            else
            {
                lblHeader.Text = "PP " + lblHeader.Text;
            }
        }
        private void AddSummaryLineControls(ref int lngCT)
        {
            GrapeCity.ActiveReports.SectionReportModel.TextBox obNew;
            if (lngCT == 0)
            {
                lngCT = 1;
            }
            else
            {
                // increment the number of rows of fields
                lngCT += 1;
                obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
                obNew.Name = "fldSummary" + FCConvert.ToString(lngCT);
                ReportFooter.Controls.Add(obNew);
                obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
                obNew.Name = "fldNonSummary" + FCConvert.ToString(lngCT);
                ReportFooter.Controls.Add(obNew);
                obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
                obNew.Name = "fldSumCount" + FCConvert.ToString(lngCT);
                ReportFooter.Controls.Add(obNew);
                GrapeCity.ActiveReports.SectionReportModel.Label obLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
                obLabel.Name = "lblSummary" + FCConvert.ToString(lngCT);
                ReportFooter.Controls.Add(obLabel);
            }
        }
        // VBto upgrade warning: intRNum As short	OnWriteFCConvert.ToInt32(
        private void AddSummaryRow(int intRNum, double dblAmount, int lngYear, double dblNonInt, int lngBillCnt)
        {
            GrapeCity.ActiveReports.SectionReportModel.TextBox obNew/*unused?*/;
            // this will add another per diem line in the report footer
            if (intRNum == 1)
            {
                fldSummary1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
                fldSummary1.Text = Strings.Format(dblAmount, "#,##0.00");
                fldSumCount1.Text = lngBillCnt.ToString();
                // kk01082015 trocls-13
                fldSumCount1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
                lblSummary1.Text = modExtraModules.FormatYear(lngYear.ToString());
                fldnonsummary1.Text = Strings.Format(dblNonInt, "#,###,##0.00");
                dblPDTotal += dblAmount;
                dblNonIntTotal += dblNonInt;
            }
            else
            {
                // add a field
                obNew = ReportFooter.Controls["fldSummary" + FCConvert.ToString(intRNum)] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
                obNew.Top = fldSummary1.Top + ((intRNum - 1) * fldSummary1.Height);
                obNew.Left = fldSummary1.Left;
                obNew.Width = fldSummary1.Width;
                obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
                obNew.Font = fldSummary1.Font;
                // this sets the font to the same as the field that is already created
                obNew.Text = Strings.Format(dblAmount, "#,##0.00");
                dblPDTotal += dblAmount;
                obNew = ReportFooter.Controls["fldNonSummary" + FCConvert.ToString(intRNum)] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
                obNew.Top = fldnonsummary1.Top + ((intRNum - 1) * fldnonsummary1.Height);
                obNew.Left = fldnonsummary1.Left;
                obNew.Width = fldnonsummary1.Width;
                obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
                obNew.Font = fldnonsummary1.Font;
                // this sets the font to the same as the field that is already created
                obNew.Text = Strings.Format(dblNonInt, "#,###,##0.00");
                dblNonIntTotal += dblNonInt;
                // kk01082015 trocls-13  add a field for the number of liens
                obNew = ReportFooter.Controls["fldSumCount" + FCConvert.ToString(intRNum)] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
                obNew.Top = fldSumCount1.Top + ((intRNum - 1) * fldSumCount1.Height);
                obNew.Left = fldSumCount1.Left;
                obNew.Width = fldSumCount1.Width;
                obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
                obNew.Font = fldSumCount1.Font;
                // this sets the font to the same as the field that is already created
                obNew.Text = lngBillCnt.ToString();
                // add a label
                GrapeCity.ActiveReports.SectionReportModel.Label obLabel = ReportFooter.Controls["lblSummary" + FCConvert.ToString(intRNum)] as GrapeCity.ActiveReports.SectionReportModel.Label;
                obLabel.Top = lblSummary1.Top + ((intRNum - 1) * lblSummary1.Height);
                obLabel.Left = lblSummary1.Left;
                obLabel.Font = fldSummary1.Font;
                // this sets the font to the same as the field that is already created
                obLabel.Text = modExtraModules.FormatYear(lngYear.ToString());
            }
        }

        private void SetupTotalSummary()
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will fill the summary labels at the bottom of the page
                // and hide/show the labels when needed
                int intCT;
                int intRow;
                // this will keep track of the row  that I am adding values to
                string strDesc = "";
                // kk03312014 trocl-682/trocls-22   Dim dblTotal(7)         As Double
                double dblTotPrin = 0;
                double dblTotPLI = 0;
                double dblTotInt = 0;
                double dblTotCost = 0;
                double dblTotal = 0;
                double dblTotNonInt = 0;
                double dblSubTotal/*unused?*/;
                bool boolAffectThis/*unused?*/;
                bool boolShowSubtotal;
                // fill in the titles
                lblSumHeaderType.Text = "Type";
                lblSumHeaderPrin.Text = "Principal";
                lblSumHeaderInt.Text = "Interest";
                lblSumHeaderCost.Text = "Costs";
                lblSumHeaderTotal.Text = "Total";
                lblNonSumHeaderTotal.Text = "Non Int.";
                intRow = 1;

                // kk03312014 trocl-682/trocls-22
                boolShowSubtotal = false;
                foreach (clsPmtTypeSummary tSum in tPayTypeSummaries)
                {
                    if (tSum.Code == "P" || tSum.Code == "U" || tSum.Code == "X" || tSum.Code == "Y")
                    {
                        if (!tSum.IsEmpty)
                        {
                            boolShowSubtotal = true;
                            strDesc = tSum.Code + " - " + tSum.Description;
                            FillSummaryLine(intRow, strDesc, tSum.Principal, tSum.PreLienInterst, tSum.Interest, tSum.LienCost, tSum.Total, false, tSum.TotalNonInt);
                            dblTotPrin += tSum.Principal;
                            // this will total all of the seperated payments for the total line
                            dblTotPLI += tSum.PreLienInterst;
                            dblTotInt += tSum.Interest;
                            dblTotCost += tSum.LienCost;
                            dblTotal += tSum.Total;
                            dblTotNonInt += tSum.TotalNonInt;
                            intRow += 1;
                        }
                    }
                }
                if (boolShowSubtotal)
                {
                    // create a subtotal line if needed
                    FillSummaryLine(FCConvert.ToInt16(intRow), "Subtotal", dblTotPrin, dblTotPLI, dblTotInt, dblTotCost, dblTotal, false, dblTotNonInt);
                    SetSummarySubTotalLine(FCConvert.ToInt16(intRow));
                    intRow += 1;
                }
                else
                {
                    lnSubtotal.Visible = false;
                }

                // kk03312014 trocl-682
                foreach (clsPmtTypeSummary tSum in tPayTypeSummaries)
                {
                    if (tSum.Code != "P" && tSum.Code != "U" && tSum.Code != "X" && tSum.Code != "Y")
                    {
                        if (!tSum.IsEmpty)
                        {
                            strDesc = tSum.Code + " - " + tSum.Description;
                            FillSummaryLine(intRow, strDesc, tSum.Principal, tSum.PreLienInterst, tSum.Interest, tSum.LienCost, tSum.Total, false, tSum.TotalNonInt);
                            intRow += 1;
                        }
                    }
                }
                // show the total line
                FillSummaryLine(intRow, "Total", tPayTypeSummaries.SummaryTotal().Principal, tPayTypeSummaries.SummaryTotal().PreLienInterst, tPayTypeSummaries.SummaryTotal().Interest, tPayTypeSummaries.SummaryTotal().LienCost, tPayTypeSummaries.SummaryTotal().Total, false, tPayTypeSummaries.SummaryTotal().TotalNonInt);
                SetSummaryTotalLine(intRow);
                for (intCT = intRow + 1; intCT <= 11; intCT++)
                {
                    HideSummaryRow(intCT);
                }
            }
            catch (Exception ex)
            {
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Creating Summary Table");
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
        }
        // VBto upgrade warning: intRw As short	OnWriteFCConvert.ToInt32(
        private void SetSummaryTotalLine(int intRw)
        {
            switch (intRw)
            {
                case 1:
                    {
                        lnSummaryTotal.Y1 = lblSummaryTotal1.Top;
                        lnSummaryTotal.Y2 = lblSummaryTotal1.Top;
                        break;
                    }
                case 2:
                    {
                        lnSummaryTotal.Y1 = lblSummaryTotal2.Top;
                        lnSummaryTotal.Y2 = lblSummaryTotal2.Top;
                        break;
                    }
                case 3:
                    {
                        lnSummaryTotal.Y1 = lblSummaryTotal3.Top;
                        lnSummaryTotal.Y2 = lblSummaryTotal3.Top;
                        break;
                    }
                case 4:
                    {
                        lnSummaryTotal.Y1 = lblSummaryTotal4.Top;
                        lnSummaryTotal.Y2 = lblSummaryTotal4.Top;
                        break;
                    }
                case 5:
                    {
                        lnSummaryTotal.Y1 = lblSummaryTotal5.Top;
                        lnSummaryTotal.Y2 = lblSummaryTotal5.Top;
                        break;
                    }
                case 6:
                    {
                        lnSummaryTotal.Y1 = lblSummaryTotal6.Top;
                        lnSummaryTotal.Y2 = lblSummaryTotal6.Top;
                        break;
                    }
                case 7:
                    {
                        lnSummaryTotal.Y1 = lblSummaryTotal7.Top;
                        lnSummaryTotal.Y2 = lblSummaryTotal7.Top;
                        break;
                    }
                case 8:
                    {
                        lnSummaryTotal.Y1 = lblSummaryTotal8.Top;
                        lnSummaryTotal.Y2 = lblSummaryTotal8.Top;
                        break;
                    }
                case 9:
                    {
                        lnSummaryTotal.Y1 = lblSummaryTotal9.Top;
                        lnSummaryTotal.Y2 = lblSummaryTotal9.Top;
                        break;
                    }
                case 10:
                    {
                        lnSummaryTotal.Y1 = lblSummaryTotal10.Top;
                        lnSummaryTotal.Y2 = lblSummaryTotal10.Top;
                        break;
                    }
                case 11:
                    {
                        lnSummaryTotal.Y1 = lblSummaryTotal11.Top;
                        lnSummaryTotal.Y2 = lblSummaryTotal11.Top;
                        break;
                    }
                case 12:
                    {
                        lnSummaryTotal.Y1 = lblSummaryTotal12.Top;
                        lnSummaryTotal.Y2 = lblSummaryTotal12.Top;
                        break;
                    }
                case 13:
                    {
                        lnSummaryTotal.Y1 = lblSummaryTotal13.Top;
                        lnSummaryTotal.Y2 = lblSummaryTotal13.Top;
                        break;
                    }
            }
            //end switch
        }
        // VBto upgrade warning: intRw As short	OnWriteFCConvert.ToInt32(
        private void SetSummarySubTotalLine(short intRw)
        {
            switch (intRw)
            {
                case 1:
                    {
                        lnSubtotal.Y1 = lblSummaryTotal1.Top;
                        lnSubtotal.Y2 = lblSummaryTotal1.Top;
                        break;
                    }
                case 2:
                    {
                        lnSubtotal.Y1 = lblSummaryTotal2.Top;
                        lnSubtotal.Y2 = lblSummaryTotal2.Top;
                        break;
                    }
                case 3:
                    {
                        lnSubtotal.Y1 = lblSummaryTotal3.Top;
                        lnSubtotal.Y2 = lblSummaryTotal3.Top;
                        break;
                    }
                case 4:
                    {
                        lnSubtotal.Y1 = lblSummaryTotal4.Top;
                        lnSubtotal.Y2 = lblSummaryTotal4.Top;
                        break;
                    }
                case 5:
                    {
                        lnSubtotal.Y1 = lblSummaryTotal5.Top;
                        lnSubtotal.Y2 = lblSummaryTotal5.Top;
                        break;
                    }
                case 6:
                    {
                        lnSubtotal.Y1 = lblSummaryTotal6.Top;
                        lnSubtotal.Y2 = lblSummaryTotal6.Top;
                        break;
                    }
                case 7:
                    {
                        lnSubtotal.Y1 = lblSummaryTotal7.Top;
                        lnSubtotal.Y2 = lblSummaryTotal7.Top;
                        break;
                    }
                case 8:
                    {
                        lnSubtotal.Y1 = lblSummaryTotal8.Top;
                        lnSubtotal.Y2 = lblSummaryTotal8.Top;
                        break;
                    }
                case 9:
                    {
                        lnSubtotal.Y1 = lblSummaryTotal9.Top;
                        lnSubtotal.Y2 = lblSummaryTotal9.Top;
                        break;
                    }
                case 10:
                    {
                        lnSubtotal.Y1 = lblSummaryTotal10.Top;
                        lnSubtotal.Y2 = lblSummaryTotal10.Top;
                        break;
                    }
                case 11:
                    {
                        lnSubtotal.Y1 = lblSummaryTotal11.Top;
                        lnSubtotal.Y2 = lblSummaryTotal11.Top;
                        break;
                    }
                case 12:
                    {
                        lnSubtotal.Y1 = lblSummaryTotal12.Top;
                        lnSubtotal.Y2 = lblSummaryTotal12.Top;
                        break;
                    }
                case 13:
                    {
                        lnSubtotal.Y1 = lblSummaryTotal13.Top;
                        lnSubtotal.Y2 = lblSummaryTotal13.Top;
                        break;
                    }
            }
            //end switch
            lnSubtotal.Visible = true;
        }
        // VBto upgrade warning: intRw As short	OnWriteFCConvert.ToInt32(
        private void FillSummaryLine(int intRw, string strDescription, double dblPrin, double dblPLI, double dblCurInt, double dblCosts, double dblTotal, bool boolTotalLine = false, double dblNonInt = 0)
        {
            // this routine will fill in the line summary row
            switch (intRw)
            {
                case 1:
                    {
                        lblSummaryPaymentType1.Text = strDescription;
                        lblSumPrin1.Text = Strings.Format(dblPrin, "#,##0.00");
                        lblSumInt1.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
                        lblSumCost1.Text = Strings.Format(dblCosts, "#,##0.00");
                        lblSummaryTotal1.Text = Strings.Format(dblTotal, "#,##0.00");
                        lblNonSummaryTotal1.Text = Strings.Format(dblNonInt, "#,###,##0.00");
                        break;
                    }
                case 2:
                    {
                        lblSummaryPaymentType2.Text = strDescription;
                        lblSumPrin2.Text = Strings.Format(dblPrin, "#,##0.00");
                        lblSumInt2.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
                        lblSumCost2.Text = Strings.Format(dblCosts, "#,##0.00");
                        lblSummaryTotal2.Text = Strings.Format(dblTotal, "#,##0.00");
                        lblNonSummaryTotal2.Text = Strings.Format(dblNonInt, "#,##0.00");
                        break;
                    }
                case 3:
                    {
                        lblSummaryPaymentType3.Text = strDescription;
                        lblSumPrin3.Text = Strings.Format(dblPrin, "#,##0.00");
                        lblSumInt3.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
                        lblSumCost3.Text = Strings.Format(dblCosts, "#,##0.00");
                        lblSummaryTotal3.Text = Strings.Format(dblTotal, "#,##0.00");
                        lblNonSummaryTotal3.Text = Strings.Format(dblNonInt, "#,##0.00");
                        break;
                    }
                case 4:
                    {
                        lblSummaryPaymentType4.Text = strDescription;
                        lblSumPrin4.Text = Strings.Format(dblPrin, "#,##0.00");
                        lblSumInt4.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
                        lblSumCost4.Text = Strings.Format(dblCosts, "#,##0.00");
                        lblSummaryTotal4.Text = Strings.Format(dblTotal, "#,##0.00");
                        lblNonSummaryTotal4.Text = Strings.Format(dblNonInt, "#,##0.00");
                        // kgk 02-10-11 was lblNonSummaryTotal2.Text =
                        break;
                    }
                case 5:
                    {
                        lblSummaryPaymentType5.Text = strDescription;
                        lblSumPrin5.Text = Strings.Format(dblPrin, "#,##0.00");
                        lblSumInt5.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
                        lblSumCost5.Text = Strings.Format(dblCosts, "#,##0.00");
                        lblSummaryTotal5.Text = Strings.Format(dblTotal, "#,##0.00");
                        lblNonSummaryTotal5.Text = Strings.Format(dblNonInt, "#,##0.00");
                        break;
                    }
                case 6:
                    {
                        lblSummaryPaymentType6.Text = strDescription;
                        lblSumPrin6.Text = Strings.Format(dblPrin, "#,##0.00");
                        lblSumInt6.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
                        lblSumCost6.Text = Strings.Format(dblCosts, "#,##0.00");
                        lblSummaryTotal6.Text = Strings.Format(dblTotal, "#,##0.00");
                        lblNonSummaryTotal6.Text = Strings.Format(dblNonInt, "#,##0.00");
                        break;
                    }
                case 7:
                    {
                        lblSummaryPaymentType7.Text = strDescription;
                        lblSumPrin7.Text = Strings.Format(dblPrin, "#,##0.00");
                        lblSumInt7.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
                        lblSumCost7.Text = Strings.Format(dblCosts, "#,##0.00");
                        lblSummaryTotal7.Text = Strings.Format(dblTotal, "#,##0.00");
                        lblNonSummaryTotal7.Text = Strings.Format(dblNonInt, "#,##0.00");
                        break;
                    }
                case 8:
                    {
                        lblSummaryPaymentType8.Text = strDescription;
                        lblSumPrin8.Text = Strings.Format(dblPrin, "#,##0.00");
                        lblSumInt8.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
                        lblSumCost8.Text = Strings.Format(dblCosts, "#,##0.00");
                        lblSummaryTotal8.Text = Strings.Format(dblTotal, "#,##0.00");
                        lblNonSummaryTotal8.Text = Strings.Format(dblNonInt, "#,##0.00");
                        break;
                    }
                case 9:
                    {
                        lblSummaryPaymentType9.Text = strDescription;
                        lblSumPrin9.Text = Strings.Format(dblPrin, "#,##0.00");
                        lblSumInt9.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
                        lblSumCost9.Text = Strings.Format(dblCosts, "#,##0.00");
                        lblSummaryTotal9.Text = Strings.Format(dblTotal, "#,##0.00");
                        lblNonSummaryTotal9.Text = Strings.Format(dblNonInt, "#,##0.00");
                        break;
                    }
                case 10:
                    {
                        lblSummaryPaymentType10.Text = strDescription;
                        lblSumPrin10.Text = Strings.Format(dblPrin, "#,##0.00");
                        lblSumInt10.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
                        lblSumCost10.Text = Strings.Format(dblCosts, "#,##0.00");
                        lblSummaryTotal10.Text = Strings.Format(dblTotal, "#,##0.00");
                        lblNonSummaryTotal10.Text = Strings.Format(dblNonInt, "#,##0.00");
                        break;
                    }
                case 11:
                    {
                        lblSummaryPaymentType11.Text = strDescription;
                        lblSumPrin11.Text = Strings.Format(dblPrin, "#,##0.00");
                        lblSumInt11.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
                        lblSumCost11.Text = Strings.Format(dblCosts, "#,##0.00");
                        lblSummaryTotal11.Text = Strings.Format(dblTotal, "#,##0.00");
                        lblNonSummaryTotal11.Text = Strings.Format(dblNonInt, "#,##0.00");
                        break;
                    }
                case 12:
                    {
                        lblSummaryPaymentType12.Text = strDescription;
                        lblSumPrin12.Text = Strings.Format(dblPrin, "#,##0.00");
                        lblSumInt12.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
                        lblSumCost12.Text = Strings.Format(dblCosts, "#,##0.00");
                        lblSummaryTotal12.Text = Strings.Format(dblTotal, "#,##0.00");
                        lblNonSummaryTotal12.Text = Strings.Format(dblNonInt, "#,##0.00");
                        break;
                    }
                case 13:
                    {
                        lblSummaryPaymentType13.Text = strDescription;
                        lblSumPrin13.Text = Strings.Format(dblPrin, "#,##0.00");
                        lblSumInt13.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
                        lblSumCost13.Text = Strings.Format(dblCosts, "#,##0.00");
                        lblSummaryTotal13.Text = Strings.Format(dblTotal, "#,##0.00");
                        lblNonSummaryTotal13.Text = Strings.Format(dblNonInt, "#,##0.00");
                        break;
                    }
            }
            //end switch
        }

        private void HideSummaryRow(int intRw)
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                int intCT;
                for (intCT = intRw; intCT <= 11; intCT++)
                {
                    switch (intRw)
                    {
                        case 1:
                            {
                                lblSummaryPaymentType1.Visible = false;
                                lblSumPrin1.Visible = false;
                                lblSumInt1.Visible = false;
                                lblSumCost1.Visible = false;
                                lblSummaryTotal1.Visible = false;
                                lblNonSummaryTotal1.Visible = false;
                                break;
                            }
                        case 2:
                            {
                                lblSummaryPaymentType2.Visible = false;
                                lblSumPrin2.Visible = false;
                                lblSumInt2.Visible = false;
                                lblSumCost2.Visible = false;
                                lblSummaryTotal2.Visible = false;
                                lblNonSummaryTotal2.Visible = false;
                                break;
                            }
                        case 3:
                            {
                                lblSummaryPaymentType3.Visible = false;
                                lblSumPrin3.Visible = false;
                                lblSumInt3.Visible = false;
                                lblSumCost3.Visible = false;
                                lblSummaryTotal3.Visible = false;
                                lblNonSummaryTotal3.Visible = false;
                                break;
                            }
                        case 4:
                            {
                                lblSummaryPaymentType4.Visible = false;
                                lblSumPrin4.Visible = false;
                                lblSumInt4.Visible = false;
                                lblSumCost4.Visible = false;
                                lblSummaryTotal4.Visible = false;
                                lblNonSummaryTotal4.Visible = false;
                                break;
                            }
                        case 5:
                            {
                                lblSummaryPaymentType5.Visible = false;
                                lblSumPrin5.Visible = false;
                                lblSumInt5.Visible = false;
                                lblSumCost5.Visible = false;
                                lblSummaryTotal5.Visible = false;
                                lblNonSummaryTotal5.Visible = false;
                                break;
                            }
                        case 6:
                            {
                                lblSummaryPaymentType6.Visible = false;
                                lblSumPrin6.Visible = false;
                                lblSumInt6.Visible = false;
                                lblSumCost6.Visible = false;
                                lblSummaryTotal6.Visible = false;
                                lblNonSummaryTotal6.Visible = false;
                                break;
                            }
                        case 7:
                            {
                                lblSummaryPaymentType7.Visible = false;
                                lblSumPrin7.Visible = false;
                                lblSumInt7.Visible = false;
                                lblSumCost7.Visible = false;
                                lblSummaryTotal7.Visible = false;
                                lblNonSummaryTotal7.Visible = false;
                                break;
                            }
                        case 8:
                            {
                                lblSummaryPaymentType8.Visible = false;
                                lblSumPrin8.Visible = false;
                                lblSumInt8.Visible = false;
                                lblSumCost8.Visible = false;
                                lblSummaryTotal8.Visible = false;
                                lblNonSummaryTotal8.Visible = false;
                                break;
                            }
                        case 9:
                            {
                                lblSummaryPaymentType9.Visible = false;
                                lblSumPrin9.Visible = false;
                                lblSumInt9.Visible = false;
                                lblSumCost9.Visible = false;
                                lblSummaryTotal9.Visible = false;
                                lblNonSummaryTotal9.Visible = false;
                                break;
                            }
                        case 10:
                            {
                                lblSummaryPaymentType10.Visible = false;
                                lblSumPrin10.Visible = false;
                                lblSumInt10.Visible = false;
                                lblSumCost10.Visible = false;
                                lblSummaryTotal10.Visible = false;
                                lblNonSummaryTotal10.Visible = false;
                                break;
                            }
                        case 11:
                            {
                                lblSummaryPaymentType11.Visible = false;
                                lblSumPrin11.Visible = false;
                                lblSumInt11.Visible = false;
                                lblSumCost11.Visible = false;
                                lblSummaryTotal11.Visible = false;
                                lblNonSummaryTotal11.Visible = false;
                                break;
                            }
                        case 12:
                            {
                                lblSummaryPaymentType12.Visible = false;
                                lblSumPrin12.Visible = false;
                                lblSumInt12.Visible = false;
                                lblSumCost12.Visible = false;
                                lblSummaryTotal12.Visible = false;
                                lblNonSummaryTotal12.Visible = false;
                                break;
                            }
                        case 13:
                            {
                                lblSummaryPaymentType13.Visible = false;
                                lblSumPrin13.Visible = false;
                                lblSumInt13.Visible = false;
                                lblSumCost13.Visible = false;
                                lblSummaryTotal13.Visible = false;
                                lblNonSummaryTotal13.Visible = false;
                                break;
                            }
                    }
                    //end switch
                }
                if (!boolAdjustedSummary)
                {
                    SetYearSummaryTop_2(lblSummaryPaymentType1.Top + (intRw * lblSummaryPaymentType1.Height) + (300 / 1440F));
                    boolAdjustedSummary = true;
                }
            }
            catch (Exception ex)
            {
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Hiding Summary Rows");
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
        }

        private void SetYearSummaryTop_2(float lngTop)
        {
            SetYearSummaryTop(ref lngTop);
        }

        private void SetYearSummaryTop(ref float lngTop)
        {
            // this will start the year summary at the right height
            lblSummary.Top = lngTop;
            lblNonSummary.Top = lblSummary.Top;
            Line1.Y1 = lngTop + lblSummary.Height;
            Line1.Y2 = lngTop + lblSummary.Height;
            lblSummary1.Top = lngTop + lblSummary.Height;
            fldSummary1.Top = lngTop + lblSummary.Height;
            fldnonsummary1.Top = lngTop + lblSummary.Height;
            fldSumCount1.Top = lngTop + fldSumCount1.Height;
            // kk01082015 trocls-13
        }

       

        private string GetStatusName(clsDRWrapper rsD, bool boolDelAccount = false)
        {
            string GetStatusName = "";
            try
            {

                string strDel = "";

                DateTime dtTransferFromBillDate;
                if (Strings.Trim(rsD.Get_Fields_String("Own1FullName")) != "")
                {
                    if (reportConfiguration.BillingType == PropertyTaxBillType.Real)
                    {
                        boolDelAccount = rsD.Get_Fields_Boolean("RSDeleted");
                    }
                    else
                    {
                        boolDelAccount = rsD.Get_Fields_Boolean("Deleted");
                    }
                    if (boolDelAccount)
                    {
                        strDel = "!";
                    }
                    else
                    {
                        strDel = "";
                    }
                    if (Information.IsDate(rsD.Get_Fields("TransferFromBillingDateFirst")))
                    {
                        dtTransferFromBillDate = rsD.Get_Fields_DateTime("TransferFromBillingDateFirst");
                    }
                    else
                    {
                        dtTransferFromBillDate = DateTime.FromOADate(0);
                    }
                    switch (reportConfiguration.Options.NameToShow)
                    {
                        case TaxReportNameOption.CurrentOwner:
                            {
                                if (modStatusPayments.Statics.boolRE)
                                {
                                    // TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
                                    // TODO Get_Fields: Field [Own2FullName] not found!! (maybe it is an alias?)
                                    GetStatusName = Strings.Trim(rsD.Get_Fields("Own1FullName")) + " " + rsD.Get_Fields("Own2FullName");
                                }
                                else
                                {
                                    // TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
                                    GetStatusName = Strings.Trim(rsD.Get_Fields("Own1FullName"));
                                }
                                break;
                            }
                        case TaxReportNameOption.BilledOwner:
                            {
                                // TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
                                if (Strings.UCase(rsD.Get_Fields_String("Name1")) != Strings.UCase(rsD.Get_Fields("Own1FullName")))
                                {
                                    // rsSR.OpenRecordset "SELECT * FROM SRMaster WHERE RSAccount = " & .Get_Fields("Account") & " AND RSCard = 1 AND SaleDate > #" & .Get_Fields("TransferFromBillingDateLast") & "#", strREDatabase
                                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                    if (!modCLCalculations.NewOwner2(rsD.Get_Fields_String("Name1"), rsD.Get_Fields("Account"),  dtTransferFromBillDate))
                                    {
                                        GetStatusName = rsD.Get_Fields_String("Name1") + " " + rsD.Get_Fields_String("Name2");
                                    }
                                    else
                                    {
                                        GetStatusName = rsD.Get_Fields_String("Name1") + " " + rsD.Get_Fields_String("Name2");
                                        // & " C\O " & rsRE.Get_Fields("Name")
                                    }
                                }
                                else
                                {
                                    GetStatusName = rsD.Get_Fields_String("Name1") + " " + rsD.Get_Fields_String("Name2");
                                }
                                break;
                            }
                        case TaxReportNameOption.BilledOwnerCareOfCurrentOwner:
                            {
                                // TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
                                if (Strings.UCase(rsD.Get_Fields_String("Name1")) != Strings.UCase(rsD.Get_Fields("Own1FullName")))
                                {
                                    // rsSR.OpenRecordset "SELECT * FROM SRMaster WHERE RSAccount = " & .Get_Fields("Account") & " AND RSCard = 1 AND SaleDate > #" & .Get_Fields("TransferFromBillingDateLast") & "#", strREDatabase
                                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                    if (!modCLCalculations.NewOwner2(rsD.Get_Fields_String("Name1"), rsD.Get_Fields("Account"),  dtTransferFromBillDate))
                                    {
                                        GetStatusName = Strings.Trim(Strings.Trim(rsD.Get_Fields_String("Name1")) + " " + rsD.Get_Fields_String("Name2"));
                                    }
                                    else
                                    {
                                        GetStatusName = Strings.Trim(Strings.Trim(rsD.Get_Fields_String("Name1")) + " " + rsD.Get_Fields_String("Name2")) + " C\\O " + Strings.Trim(rsD.Get_Fields_String("Name"));
                                    }
                                }
                                else
                                {
                                    GetStatusName = Strings.Trim(Strings.Trim(rsD.Get_Fields_String("Name1")) + " " + rsD.Get_Fields_String("Name2"));
                                }
                                break;
                            }
                        case TaxReportNameOption.CurrentOwnerBilledAsOwner:
                            {
                                // TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
                                if (Strings.UCase(rsD.Get_Fields_String("Name1")) != Strings.UCase(rsD.Get_Fields("Own1FullName")))
                                {
                                    // rsSR.OpenRecordset "SELECT * FROM SRMaster WHERE RSAccount = " & .Get_Fields("Account") & " AND RSCard = 1 AND SaleDate > #" & .Get_Fields("TransferFromBillingDateLast") & "#", strREDatabase
                                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                    if (!modCLCalculations.NewOwner2(rsD.Get_Fields_String("Name1"), rsD.Get_Fields("Account"),  dtTransferFromBillDate))
                                    {
                                        if (reportConfiguration.BillingType == PropertyTaxBillType.Real)
                                        {
                                            // TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
                                            // TODO Get_Fields: Field [Own2FullName] not found!! (maybe it is an alias?)
                                            GetStatusName = rsD.Get_Fields("Own1FullName") + " " + rsD.Get_Fields("Own2FullName");
                                        }
                                        else
                                        {
                                            // TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
                                            GetStatusName = rsD.Get_Fields("Own1FullName");
                                        }
                                    }
                                    else
                                    {
                                        // TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
                                        GetStatusName = Strings.Trim(rsD.Get_Fields("Own1FullName")) + " Bill = " + rsD.Get_Fields_String("Name1") + " " + rsD.Get_Fields_String("Name2");
                                    }
                                }
                                else
                                {
                                    if (modStatusPayments.Statics.boolRE)
                                    {
                                        // TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
                                        // TODO Get_Fields: Field [Own2FullName] not found!! (maybe it is an alias?)
                                        GetStatusName = rsD.Get_Fields("Own1FullName") + " " + rsD.Get_Fields("Own2FullName");
                                    }
                                    else
                                    {
                                        // TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
                                        GetStatusName = Strings.Trim(rsD.Get_Fields("Own1FullName"));
                                    }
                                }
                                break;
                            }
                    }
                    //end switch
                }
                else
                {
                    GetStatusName = rsD.Get_Fields_String("Name1") + " " + rsD.Get_Fields_String("Name2");
                }
                GetStatusName = strDel + GetStatusName;
                return GetStatusName;
            }
            catch (Exception ex)
            {
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In Get Status Name");
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return GetStatusName;
        }
    }
}