﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptPrintUnprintedPartialPaymentWaivers : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/09/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               06/05/2006              *
		// ********************************************************
		private string strSignerName = "";
		private string strState = "";
		private string strMuniName = "";
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsDRWrapper rsReport = new clsDRWrapper();
		private clsDRWrapper rsReport_AutoInitialized;

		private clsDRWrapper rsReport
		{
			get
			{
				if (rsReport_AutoInitialized == null)
				{
					rsReport_AutoInitialized = new clsDRWrapper();
				}
				return rsReport_AutoInitialized;
			}
			set
			{
				rsReport_AutoInitialized = value;
			}
		}

		public rptPrintUnprintedPartialPaymentWaivers()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Partial Payment Waivers";
		}

		public static rptPrintUnprintedPartialPaymentWaivers InstancePtr
		{
			get
			{
				return (rptPrintUnprintedPartialPaymentWaivers)Sys.GetInstance(typeof(rptPrintUnprintedPartialPaymentWaivers));
			}
		}

		protected rptPrintUnprintedPartialPaymentWaivers _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsReport_AutoInitialized?.Dispose();
            }
			base.Dispose(disposing);
		}

		private void SetStrings()
		{
			string strMainText;
			int lngLineLen;
			string strOwnerName = "";
			string strBillingYear = "";
			double dblPaymentAmount = 0;
			DateTime dtPaymentDate;
			lngLineLen = 35;
			// this is how many underscores are in the printed lines
			if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
			{
				lblTownHeader.Text = modGlobalConstants.Statics.gstrCityTown + " of " + strMuniName;
			}
			else
			{
				lblTownHeader.Text = strMuniName;
			}
			lblTitleBar.Text = "WAIVER FORM FOR USE WITH PARTIAL PAYMENTS";
			lblLegalDescription.Text = "30-DAY NOTICES & PROPERTY TAX LIENS";
			strMainText = "    I, " + strOwnerName + ", do hereby acknowledge that I have voluntarily made a partial payment upon my tax due the ";
			if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
			{
				strMainText += modGlobalConstants.Statics.gstrCityTown + " of " + strMuniName + ", " + strState + " for the year ";
			}
			else
			{
				strMainText += strMuniName + ", " + strState + " for the year ";
			}
			strMainText += strBillingYear + " in the sum of " + Strings.Format(dblPaymentAmount, "$#,##0.00");
			if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
			{
				strMainText += " and I agree that sum is accepted by said " + modGlobalConstants.Statics.gstrCityTown + " of " + strMuniName;
			}
			else
			{
				strMainText += " and I agree that sum is accepted by said " + strMuniName;
			}
			strMainText += " as partial payment as aforesaid without, in any way, waiving the lien of said ";
			if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
			{
				strMainText += modGlobalConstants.Statics.gstrCityTown + " of " + strMuniName + " for said tax.";
			}
			else
			{
				strMainText += strMuniName + " for said tax.";
			}
			fldMainText.Text = strMainText;
			fldDateLine.Text = "Dated at " + strMuniName + ", " + strState;
			// fldDateLine.Text = fldDateLine.Text & "                 " & Format(dtPaymentDate, "MM/dd/yyyy")
			fldDateLine.Text = fldDateLine.Text + "                 " + Strings.Format(DateTime.Now, "MM/dd/yyyy");
			fldWitnessLine.Text = Strings.StrDup(lngLineLen, "_");
			fldTaxPayerLine.Text = Strings.StrDup(lngLineLen, "_");
			fldWitnessName.Text = strSignerName;
			fldTaxPayerName.Text = strOwnerName;
		}

		public void Init(string strPassMuni, string strPassState, string strWitness)
		{
			// this routine will set all of the variables needed
			strMuniName = strPassMuni;
			strState = strPassState;
			strSignerName = strWitness;
			string strSQL;
			strSQL = "select partialpaymentwaiver.*,billingmaster.name1,billingmaster.name2,billingmaster.maplot from partialpaymentwaiver inner join billingmaster on (billingmaster.lienrecordnumber = partialpaymentwaIver.lienkey) WHERE printed = 0 order by partialpaymentwaiver.BILLYEAR,name1";
			rsReport.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
			if (rsReport.EndOfFile())
			{
				MessageBox.Show("No unprinted lien partial payment waivers found", "No Waivers", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			strSQL = "update partialpaymentwaiver set printed = 1";
			clsDRWrapper rsTemp = new clsDRWrapper();
			rsTemp.Execute(strSQL, modExtraModules.strCLDatabase);
			rsTemp.Dispose();
			frmReportViewer.InstancePtr.Init(this);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsReport.EndOfFile();
			//Detail_Format();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsReport.EndOfFile())
			{
				string strOwnerName = "";
				int lngLineLen = 0;
				string strMainText = "";
				string strBillingYear = "";
				DateTime dtPaymentDate;
				double dblPaymentAmount = 0;
				strOwnerName = Strings.Trim(FCConvert.ToString(rsReport.Get_Fields_String("name1")) + " " + FCConvert.ToString(rsReport.Get_Fields_String("name2")));
				lngLineLen = 35;
				// this is how many underscores are in the printed lines
				// TODO Get_Fields: Check the table for the column [billyear] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [billyear] and replace with corresponding Get_Field method
				strBillingYear = FCConvert.ToString(rsReport.Get_Fields("billyear"));
				dtPaymentDate = (DateTime)rsReport.Get_Fields_DateTime("effectivedate");
				dblPaymentAmount = Conversion.Val(rsReport.Get_Fields_Double("payment"));
				if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
				{
					lblTownHeader.Text = modGlobalConstants.Statics.gstrCityTown + " of " + strMuniName;
				}
				else
				{
					lblTownHeader.Text = strMuniName;
				}
				lblTitleBar.Text = "WAIVER FORM FOR USE WITH PARTIAL PAYMENTS";
				lblLegalDescription.Text = "30-DAY NOTICES & PROPERTY TAX LIENS";
				strMainText = "    I, " + strOwnerName + ", do hereby acknowledge that I have voluntarily made a partial payment upon my tax due the ";
				if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
				{
					strMainText += modGlobalConstants.Statics.gstrCityTown + " of " + strMuniName + ", " + strState + " for the year ";
				}
				else
				{
					strMainText += strMuniName + ", " + strState + " for the year ";
				}
				strMainText += strBillingYear + " in the sum of " + Strings.Format(dblPaymentAmount, "$#,##0.00");
				if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
				{
					strMainText += " and I agree that sum is accepted by said " + modGlobalConstants.Statics.gstrCityTown + " of " + strMuniName;
				}
				else
				{
					strMainText += " and I agree that sum is accepted by said " + strMuniName;
				}
				strMainText += " as partial payment as aforesaid without, in any way, waiving the lien of said ";
				if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
				{
					strMainText += modGlobalConstants.Statics.gstrCityTown + " of " + strMuniName + " for said tax.";
				}
				else
				{
					strMainText += strMuniName + " for said tax.";
				}
				fldMainText.Text = strMainText;
				fldDateLine.Text = "Dated at " + strMuniName + ", " + strState;
				fldDateLine.Text = fldDateLine.Text + "                 " + Strings.Format(dtPaymentDate, "MM/dd/yyyy");
				fldWitnessLine.Text = Strings.StrDup(lngLineLen, "_");
				fldTaxPayerLine.Text = Strings.StrDup(lngLineLen, "_");
				fldWitnessName.Text = strSignerName;
				fldTaxPayerName.Text = strOwnerName;
				rsReport.MoveNext();
			}
		}

		
	}
}
