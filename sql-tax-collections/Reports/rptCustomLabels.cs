﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using GrapeCity.ActiveReports;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptCustomLabels : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/12/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               07/31/2006              *
		// ********************************************************
		// THIS REPORT IS TOTALLY NOT GENERIC AND HAS BEEN ALTERED FOR THIS TWCL
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsMort = new clsDRWrapper();
		// VBto upgrade warning: intLabelWidth As short --> As int	OnWrite(int, double)
		float intLabelWidth;
		bool boolDifferentPageSize;
		string strFont;
		bool boolPP;
		bool boolMort;
		bool boolEmpty;
		string strLeftAdjustment = "";
		// VBto upgrade warning: lngVertAdjust As int	OnWriteFCConvert.ToDouble(
		float lngVertAdjust;
		int intTypeOfLabel;
		bool boolCondensed;
		int intPrinterOption;
		string strPrintOption;
		clsPrintLabel labLabels = new clsPrintLabel();
		
		// VBto upgrade warning: lngPrintWidth As int	OnWrite(string, double)
		float lngPrintWidth;
		string strDefPrinterName = "";

		public rptCustomLabels()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Custom Labels";
		}

		public static rptCustomLabels InstancePtr
		{
			get
			{
				return (rptCustomLabels)Sys.GetInstance(typeof(rptCustomLabels));
			}
		}

		protected rptCustomLabels _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
				rsMort.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// IF THIS IS THE END OF THE RECORDSET THEN WE NEED A WAY TO GET OUT
			eArgs.EOF = rsData.EndOfFile();
			//Detail_Format();
		}
		// VBto upgrade warning: intLabelType As short	OnWriteFCConvert.ToInt32(
		// VBto upgrade warning: intRateType As short	OnWriteFCConvert.ToInt32(
		public void Init(string strSQL, string strReportType, int intLabelType, string strPrinterName, string strFonttoUse, string strPassSortOrder, short intRateType, string strPassDefaultPrinter = "", bool boolPassCondensed = false)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strDBName = "";
				int intReturn/*unused?*/;
				int x/*unused?*/;
				bool boolUseFont/*unused?*/;
				boolCondensed = boolPassCondensed;
				strFont = strFonttoUse;
				this.Document.Printer.PrinterName = strPrinterName;
				
				boolPP = false;
				boolMort = false;
				intPrinterOption = 7;
				strPrintOption = "";
				// find out what types should be printed   ie.  Owners, Mortgage Holders and/or New Owners
				if (FCConvert.CBool(frmCustomLabels.InstancePtr.chkFormPrintOptions[0].CheckState != Wisej.Web.CheckState.Checked) || FCConvert.CBool(frmCustomLabels.InstancePtr.chkFormPrintOptions[1].CheckState != Wisej.Web.CheckState.Checked) || FCConvert.CBool(frmCustomLabels.InstancePtr.chkFormPrintOptions[2].CheckState != Wisej.Web.CheckState.Checked) || FCConvert.CBool(frmCustomLabels.InstancePtr.chkFormPrintOptions[3].CheckState != Wisej.Web.CheckState.Checked))
				{
					if (FCConvert.CBool(frmCustomLabels.InstancePtr.chkFormPrintOptions[0].CheckState != Wisej.Web.CheckState.Checked))
					{
						strPrintOption += " and (ISNULL(mortgageholder,0) <> 0 or ISNULL(newowner,0) = -1 or ISNULL(interestedpartyid,0) <> 0)";
					}
					if (FCConvert.CBool(frmCustomLabels.InstancePtr.chkFormPrintOptions[1].CheckState != Wisej.Web.CheckState.Checked))
					{
						strPrintOption += " and ISNULL(mortgageholder,0) = 0 ";
					}
					if (FCConvert.CBool(frmCustomLabels.InstancePtr.chkFormPrintOptions[2].CheckState != Wisej.Web.CheckState.Checked))
					{
						strPrintOption += " and not ISNULL(newowner,0) = -1 ";
					}
					if (FCConvert.CBool(frmCustomLabels.InstancePtr.chkFormPrintOptions[3].CheckState != Wisej.Web.CheckState.Checked))
					{
						strPrintOption += " and ISNULL(interestedpartyid,0) = 0 ";
					}
				}
				
				switch (strReportType)
                {
                    case "MORTGAGEHOLDER":
                        strDBName = "CentralData";
                        boolMort = true;

                        break;
                    case "RECL":
                    case "LABELS":
                        strDBName = modExtraModules.strCLDatabase;

                        break;
                }
				if (modStatusPayments.Statics.boolRE)
				{
					rsData.OpenRecordset("SELECT * FROM ((" + strSQL + ") AS New INNER JOIN CMFNumbers ON New.Account = CMFNumbers.Account) WHERE New.BillingType = 'RE' AND Type = " + FCConvert.ToString(intRateType) + " " + strPrintOption + " ORDER BY " + strPassSortOrder + ", NewOwner desc, MortgageHolder", modExtraModules.strCLDatabase);
					rsData.InsertName("OwnerPartyID", "Own1", false, true, true, "RE", false, "", true, "");
				}
				else
				{
					rsData.OpenRecordset("SELECT * FROM ((" + strSQL + ") AS New INNER JOIN CMFNumbers ON New.Account = CMFNumbers.Account) WHERE New.BillingType = 'PP' AND Type = " + FCConvert.ToString(intRateType) + " " + strPrintOption + " ORDER BY " + strPassSortOrder + ", NewOwner desc, MortgageHolder", modExtraModules.strCLDatabase);
					rsData.InsertName("PartyID", "Own1", false, true, true, "RE", false, "", true, "");
				}
				rsMort.OpenRecordset("SELECT * FROM MortgageHolders", "CentralData");
				intTypeOfLabel = intLabelType;
				// make them choose the printer first if you have to use a custom form
				// RESET THE RECORDSET TO THE BEGINNING OF THE RECORDSET
				if (rsData.RecordCount() != 0)
					rsData.MoveFirst();
				boolDifferentPageSize = false;
				int intindex;
				int cnt/*unused?*/;
				intindex = labLabels.Get_IndexFromID(intTypeOfLabel);
				if (labLabels.Get_IsDymoLabel(intindex))
				{
					switch (labLabels.Get_ID(intindex))
					{
						case modLabels.CNSTLBLTYPEDYMO30256:
							{
								strPrinterName = this.Document.Printer.PrinterName;
								this.Document.Printer.DefaultPageSettings.Landscape = true;
								PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
								// kk11542014 troge-241  Change to use global default settings
								PageSettings.Margins.Top = FCConvert.ToSingle(labLabels.Get_TopMargin(intindex) + FCConvert.ToSingle(modGlobal.Statics.gdblLabelsAdjustment * 240) / 1440F);
								// 0.128 * 1440
								PageSettings.Margins.Bottom = labLabels.Get_BottomMargin(intindex);
								// 0
								PageSettings.Margins.Right = labLabels.Get_RightMargin(intindex);
								// 0
								PageSettings.Margins.Left = labLabels.Get_LeftMargin(intindex);
								// 0.128 * 1440
								PrintWidth = labLabels.Get_LabelWidth(intindex) - PageSettings.Margins.Left - PageSettings.Margins.Right;
								// (2.31 * 1440) - PageSettings.Margins.Left - PageSettings.Margins.Right
								intLabelWidth = labLabels.Get_LabelWidth(intindex);
								// (4 * 1440)
								lngPrintWidth = PrintWidth;
								// (4 * 1440) - PageSettings.Margins.Left - PageSettings.Margins.Right
								Detail.Height = labLabels.Get_LabelHeight(intindex) - PageSettings.Margins.Top - PageSettings.Margins.Bottom - 10 / 1440F;
								// (1440 * 2.3125) - PageSettings.Margins.Top - PageSettings.Margins.Bottom - 10 '2 5/16"
								Detail.ColumnCount = 1;
								PageSettings.PaperHeight = labLabels.Get_LabelWidth(intindex);
								// Landscape                    '4 * 1440
								PageSettings.PaperWidth = labLabels.Get_LabelHeight(intindex);
								// 2.31 * 1440
								Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
								this.Document.Printer.DefaultPageSettings.Landscape = true;
								PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
								break;
							}
						case modLabels.CNSTLBLTYPEDYMO30252:
							{
								strPrinterName = this.Document.Printer.PrinterName;
								this.Document.Printer.DefaultPageSettings.Landscape = true;
								PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
								PageSettings.Margins.Top = 0.128F;
								PageSettings.Margins.Bottom = 0;
								PageSettings.Margins.Right = 0;
								PageSettings.Margins.Left = 0.128F;
								// 
								PrintWidth = 3.5F - PageSettings.Margins.Left - PageSettings.Margins.Right;
								intLabelWidth = 3.5f;
								lngPrintWidth = 3.5F - PageSettings.Margins.Left - PageSettings.Margins.Right;
								Detail.Height = 1.1F - PageSettings.Margins.Top - PageSettings.Margins.Bottom - 10 / 1440F;
								Detail.ColumnCount = 1;
								PageSettings.PaperHeight = 3.5f;
								PageSettings.PaperWidth = 1.1f;
								Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
								this.Document.Printer.DefaultPageSettings.Landscape = true;
								PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
								break;
							}
						case modLabels.CNSTLBLTYPEDYMO4150:
							{
								// MAL@20080918: Add new label type
								// Tracker Reference: 14365
								strPrinterName = this.Document.Printer.PrinterName;
								this.Document.Printer.DefaultPageSettings.Landscape = true;
								PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
								PageSettings.Margins.Top = 0.3F;
								PageSettings.Margins.Bottom = 0;
								PageSettings.Margins.Right = 0.25F;
								PageSettings.Margins.Left = 0.128F;
								// 
								PrintWidth = 5040 / 1440F - PageSettings.Margins.Left - PageSettings.Margins.Right;
								intLabelWidth = 5040 / 1440F - PageSettings.Margins.Left - PageSettings.Margins.Right;
								lngPrintWidth = 5040 / 1440F - PageSettings.Margins.Left - PageSettings.Margins.Right - 25 / 1440F;
								Detail.Height = 1620 / 1440F - PageSettings.Margins.Top - PageSettings.Margins.Bottom - 10 / 1440F;
								Detail.ColumnCount = 1;
								PageSettings.PaperHeight = 5040 / 1440F;
								PageSettings.PaperWidth = 1620 / 1440F;
								Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
								// Me.Printer.DefaultPageSettings.Landscape = true
								// PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape
								break;
							}
					}
					//end switch
				}
				else if (labLabels.Get_IsLaserLabel(intindex))
				{
					PageSettings.Margins.Top = 0.5F;
					PageSettings.Margins.Top += FCConvert.ToSingle(modGlobal.Statics.gdblLabelsAdjustment * 270) / 1440F;
					PageSettings.Margins.Left = labLabels.Get_LeftMargin(intindex);
					PageSettings.Margins.Right = labLabels.Get_RightMargin(intindex);
					PrintWidth = labLabels.Get_PageWidth(intindex) - labLabels.Get_LeftMargin(intindex) - labLabels.Get_RightMargin(intindex);
					intLabelWidth = labLabels.Get_LabelWidth(intindex);
					Detail.Height = labLabels.Get_LabelHeight(intindex) + labLabels.Get_VerticalSpace(intindex);
					if (labLabels.Get_LabelsWide(intindex) > 0)
					{
						Detail.ColumnCount = labLabels.Get_LabelsWide(intindex);
						Detail.ColumnSpacing = labLabels.Get_HorizontalSpace(intindex);
					}
					lngPrintWidth = PrintWidth;
				}
				
				CreateDataFields();
				frmReportViewer.InstancePtr.Init(this, this.Document.Printer.PrinterName);
			}
			catch (Exception ex)
			{
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Initializing Label Report");
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			if (rsData.RecordCount() > 0)
			{
				switch (frmRateRecChoice.InstancePtr.intRateType)
				{
					case 10:
						{
							modGlobalFunctions.IncrementSavedReports("LastCL30DayLabels");
							this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastCL30DayLabels1.RDF"));
							break;
						}
					case 11:
						{
							modGlobalFunctions.IncrementSavedReports("LastCLTransferToLienLabels");
							this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastCLTransferToLienLabels1.RDF"));
							break;
						}
					case 12:
						{
							modGlobalFunctions.IncrementSavedReports("LastCLLienMaturityLabels");
							this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastCLLienMaturityLabels1.RDF"));
							break;
						}
				}
			}
			else
			{
				FCMessageBox.Show("There are no eligible accounts.", MsgBoxStyle.Information, "No Labels");
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int intReturn = 0;
			int const_PrintToolID;
			int cnt;

			if (intTypeOfLabel == modLabels.CNSTLBLTYPEDYMO30256 || intTypeOfLabel == modLabels.CNSTLBLTYPEDYMO30252 || intTypeOfLabel == modLabels.CNSTLBLTYPEDYMO4150)
			{
				for (cnt = 0; cnt <= this.Document.Printer.PaperSizes.Count - 1; cnt++)
				{
					switch (intTypeOfLabel)
					{
						case modLabels.CNSTLBLTYPEDYMO30252:
							{
								if (Strings.UCase(this.Document.Printer.PaperSizes[cnt].PaperName) == "30252 ADDRESS")
								{
									this.Document.Printer.PaperSize = this.Document.Printer.PaperSizes[cnt];
									this.Document.Printer.DefaultPageSettings.Landscape = true;
									break;
								}
								break;
							}
						case modLabels.CNSTLBLTYPEDYMO30256:
							{
								if (Strings.UCase(this.Document.Printer.PaperSizes[cnt].PaperName) == "30256 SHIPPING")
								{
									this.Document.Printer.PaperSize = this.Document.Printer.PaperSizes[cnt];
									this.Document.Printer.DefaultPageSettings.Landscape = true;
									break;
								}
								break;
							}
						case modLabels.CNSTLBLTYPEDYMO4150:
							{
								// MAL@20080918: Add new label type
								// Tracker Reference: 14365
								if (Strings.UCase(this.Document.Printer.PaperSizes[cnt].PaperName) == "99012 LARGE ADDRESS")
								{
									this.Document.Printer.PaperSize = this.Document.Printer.PaperSizes[cnt];
									this.Document.Printer.DefaultPageSettings.Landscape = true;
									break;
								}
								break;
							}
					}
					//end switch
				}
				// cnt
			}
			else
			{
                this.Document.Printer.PaperSize = new System.Drawing.Printing.PaperSize("Size", FCConvert.ToInt32(PageSettings.PaperWidth * 100), FCConvert.ToInt32(PageSettings.PaperHeight * 100));
			}
		}

		private void CreateDataFields()
		{
			int intControlNumber/*unused?*/;
			GrapeCity.ActiveReports.SectionReportModel.TextBox NewField/*unused?*/;
			int intRow;
			int intCol/*unused?*/;
			int intNumber;
			float intRowH;
			intRowH = 210 / 1440F;
			// CREATE THE CONTROLS AND SET THE POSITION OF THE CONTROLS
			intNumber = 5;
			// If boolMort Then intNumber = 5
			for (intRow = 1; intRow <= intNumber; intRow++)
			{
				NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
				NewField.CanGrow = false;
				NewField.Name = "txtData" + FCConvert.ToString(intRow);
				NewField.Top = ((intRow - 1) * intRowH) + lngVertAdjust;
				NewField.Left = 144 / 1440F;
				// one space
				NewField.Width = (PrintWidth / Detail.ColumnCount - ((Detail.ColumnCount - 1) * Detail.ColumnSpacing));
				// - 145
				NewField.Height = intRowH;
				NewField.CanGrow = false;
				NewField.Text = string.Empty;
				NewField.MultiLine = false;
				NewField.WordWrap = true;
				if (Strings.Trim(strFont) != string.Empty)
				{
					NewField.Font = new Font(strFont, NewField.Font.Size);
				}
				else
				{
					NewField.Font = new Font(lblFont.Font.Name, NewField.Font.Size);
				}
				if (boolCondensed)
				{
					NewField.Font = new Font(NewField.Font.Name, NewField.Font.Size - 5);
				}
				Detail.Controls.Add(NewField);
				if (intRow == 1)
				{
					NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
					NewField.CanGrow = false;
					NewField.Name = "txtAcct";
					NewField.Top = ((intRow - 1) * intRowH) + lngVertAdjust;
					NewField.Left = (PrintWidth / Detail.ColumnCount - ((Detail.ColumnCount - 1) * Detail.ColumnSpacing)) - 1120 / 1440F;
					NewField.Width = 1120 / 1440F;
					NewField.Height = intRowH;
					NewField.Text = string.Empty;
					NewField.MultiLine = false;
					NewField.WordWrap = true;
					if (Strings.Trim(strFont) != string.Empty)
					{
						NewField.Font = new Font(strFont, NewField.Font.Size);
					}
					else
					{
						NewField.Font = new Font(lblFont.Font.Name, NewField.Font.Size);
					}
					if (boolCondensed)
					{
						NewField.Left = FCConvert.ToSingle(((PrintWidth / Detail.ColumnCount - ((Detail.ColumnCount - 1) * Detail.ColumnSpacing)) - 1120 / 1440F) / 2.0F);
						NewField.Font = new Font(NewField.Font.Name, NewField.Font.Size - 5);
					}
					Detail.Controls.Add(NewField);
				}
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			
			App.MainForm.Show();
		}

		public void ActiveReport_ToolbarClick(/*DDActiveReports2.DDTool Tool*/)
		{
			// Call VerifyPrintToFile(Me, Tool)
			int intNumSpaces/*unused?*/;
			int intindex = 0;
			int numberOfPages = this.Document.Pages.Count;
			int pageStart = 0;
			string VBtoVar = ""/*Tool.Caption*/;
			if (VBtoVar == "Print...")
			{
				if (frmNumPages.InstancePtr.Init(ref numberOfPages, ref pageStart, null, "Print Pages"))
				{
					this.Document.Print(false);
				}
			}
		}

		private void PrintMortgageHolders()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intControl;
				int intRow;
				string str1 = "";
				string str2 = "";
				string str3 = "";
				string str4 = "";
				string str5 = "";
				int lngAcctNumber/*unused?*/;
				if (rsData.EndOfFile() != true)
				{
					if (boolPP)
					{
						str1 = strLeftAdjustment + Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Own1FullName")));
						str2 = strLeftAdjustment + Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Own1Address1")));
						str3 = strLeftAdjustment + Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Own1Address2")));
						str4 = strLeftAdjustment + Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Own1City")) + "  " + FCConvert.ToString(rsData.Get_Fields("Own1State")));
					}
					else
					{
						str1 = strLeftAdjustment + Strings.Trim(FCConvert.ToString(rsData.Get_Fields("DeedName1")));
						str2 = strLeftAdjustment + Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Own1Address1")));
						str3 = strLeftAdjustment + Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Own1Address2")));
						str4 = strLeftAdjustment + Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Own1City")) + "  " + FCConvert.ToString(rsData.Get_Fields("Own1State")));
					}
					if (boolPP)
					{
						if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Own1Zip"))) != "")
						{
							str4 += " " + FCConvert.ToString(rsData.Get_Fields("Own1Zip"));
						}
					}
					else if (boolMort)
					{
						str4 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address3")));
						str5 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("City")) + "  " + FCConvert.ToString(rsData.Get_Fields_String("State")));
						str5 += " " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Zip")));
						if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Zip4"))) != string.Empty)
						{
							str5 += "-" + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Zip4")));
						}
					}
					else
					{
						str4 += " " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Zip")));
						if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Zip4"))) != string.Empty)
						{
							str4 += "-" + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Zip4")));
						}
					}
					// condense the labels if some are blank
					if (boolMort)
					{
						if (Strings.Trim(str4) == string.Empty)
						{
							str4 = str5;
							str5 = "";
						}
					}
					if (Strings.Trim(str3) == string.Empty)
					{
						str3 = str4;
						if (boolMort)
						{
							str4 = str5;
							str5 = "";
						}
						else
						{
							str4 = "";
						}
					}
					if (Strings.Trim(str2) == string.Empty)
					{
						str2 = str3;
						str3 = str4;
						if (boolMort)
						{
							str4 = str5;
							str5 = "";
						}
						else
						{
							str4 = "";
						}
					}
					if (Strings.Trim(str1) == string.Empty)
					{
						str1 = str2;
						str2 = str3;
						str3 = str4;
						if (boolMort)
						{
							str4 = str5;
							str5 = "";
						}
						else
						{
							str4 = "";
						}
					}
					for (intControl = 0; intControl <= Detail.Controls.Count - 1; intControl++)
					{
						if (Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) == "txtData")
						{
							intRow = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Detail.Controls[intControl].Name, 8))));
							GrapeCity.ActiveReports.SectionReportModel.TextBox txtData = (GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl];
							switch (intRow)
							{
								case 1:
									{
										txtData.Text = strLeftAdjustment + str1;
										break;
									}
								case 2:
									{
										txtData.Text = strLeftAdjustment + str2;
										break;
									}
								case 3:
									{
										txtData.Text = strLeftAdjustment + str3;
										break;
									}
								case 4:
									{
										txtData.Text = strLeftAdjustment + str4;
										break;
									}
								case 5:
									{
										txtData.Text = strLeftAdjustment + str5;
										break;
									}
							}
							//end switch
						}
						// this will fill the account number in
						if (Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) == "txtAcct")
						{
							// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
							((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = Strings.Format(rsData.Get_Fields("AccountNumber"), "00000");
						}
						if (boolMort && Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) == "txtMort")
						{
							// TODO Get_Fields: Field [MortgageHolderNumber] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [MortgageHolderNumber] not found!! (maybe it is an alias?)
							((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = Strings.Format(rsData.Get_Fields("MortgageHolderNumber"), "00000");
						}
					}
					// intControl
					rsData.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Printing Mortgage Holders");
			}
		}

		private void PrintLabels()
		{
			// this will fill in the fields for labels
			int intControl;
			int intRow;
			string str1 = "";
			string str2 = "";
			string str3 = "";
			string str4 = "";
			string str5 = "";
			string str6 = "";
			bool boolShowAccountField;
			if (rsData.EndOfFile() != true)
			{
				// If rsData.Fields("New.Account") = 109 Then
				// MsgBox "stop"
				// End If
				// XXXXXXXX kgk 03272012
				// If InStr(1, Trim(rsData.Fields("Name")), " and ") <> 0 And (Trim(rsData.Fields("CMFNumbers.Address1")) = "" Or Trim(rsData.Fields("CMFNumbers.Address2")) = "" Or Trim(rsData.Fields("CMFNumbers.Address3")) = "" Or Trim(rsData.Fields("Address4")) = "") Then
				// str1 = Trim(Left(rsData.Fields("Name"), InStr(1, Trim(rsData.Fields("Name")), " and ")))
				// str2 = Trim(Right(rsData.Fields("Name"), Len(Trim(rsData.Fields("Name"))) - InStr(1, Trim(rsData.Fields("Name")), " and ") - 3))
				// str3 = Trim(rsData.Fields("CMFNumbers.Address1"))
				// str4 = Trim(rsData.Fields("CMFNumbers.Address2"))
				// str5 = Trim(rsData.Fields("CMFNumbers.Address3"))
				// str6 = Trim(rsData.Fields("Address4"))
				// Else
				// str1 = Trim(rsData.Fields("Name"))
				// str2 = Trim(rsData.Fields("CMFNumbers.Address1"))
				// str3 = Trim(rsData.Fields("CMFNumbers.Address2"))
				// str4 = Trim(rsData.Fields("CMFNumbers.Address3"))
				// str5 = Trim(rsData.Fields("Address4"))
				// str6 = ""
				// End If
				// 
				if (Strings.InStr(1, Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name"))), " and ") != 0 && (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address1"))) == "" || Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address2"))) == "" || Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address3"))) == "" || Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address4"))) == ""))
				{
					str1 = Strings.Trim(Strings.Left(FCConvert.ToString(rsData.Get_Fields_String("Name")), Strings.InStr(1, Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name"))), " and ")));
					str2 = Strings.Trim(Strings.Right(FCConvert.ToString(rsData.Get_Fields_String("Name")), Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name"))).Length - Strings.InStr(1, Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name"))), " and ") - 3));
					str3 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address1")));
					str4 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address2")));
					str5 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address3")));
					str6 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address4")));
				}
				else
				{
					str1 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name")));
					str2 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address1")));
					str3 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address2")));
					str4 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address3")));
					str5 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address4")));
					str6 = "";
				}
				// XXXXXXXX
				// If rsData.Fields("MortgageHolder") <> 0 Then
				// boolMort = True
				// rsMort.FindFirstRecord , , "MortgageHolderID = " & rsData.Fields("MortgageHolder")
				// If Not rsMort.EndOfFile Then
				// str1 = strLeftAdjustment & Trim(rsMort.Fields("Name"))
				// str2 = strLeftAdjustment & Trim(rsMort.Fields("Address1"))
				// str3 = strLeftAdjustment & Trim(rsMort.Fields("Address2"))
				// str4 = strLeftAdjustment & Trim(rsMort.Fields("Address3"))
				// str5 = strLeftAdjustment & Trim(rsMort.Fields("City")) & " " & rsMort.Fields("State") & " " & rsMort.Fields("Zip")
				// If Trim(rsMort.Fields("Zip4")) <> "" Then
				// str5 = str5 & "-" & Trim(rsMort.Fields("Zip4"))
				// End If
				// Else
				// str1 = "Missing Data for Mortgage Holder #" & rsData.Fields("MortgageHolder")
				// str2 = ""
				// str3 = ""
				// str4 = ""
				// End If
				// Else
				// boolMort = False
				// If Trim(rsData.Fields("Name2")) <> "" Then
				// str1 = Trim(rsData.Fields("Name1")) & " and " & Trim(rsData.Fields("Name2"))
				// Else
				// str1 = Trim(rsData.Fields("Name1"))
				// End If
				// 
				// str2 = Trim(rsData.Fields("RSAddr1"))
				// str3 = Trim(rsData.Fields("RSAddr2"))
				// str4 = Trim(rsData.Fields("RSAddr3")) & " " & rsData.Fields("RSState") & " " & rsData.Fields("RSZip")
				// 
				// If Trim(rsData.Fields("RSZip4")) <> "" Then
				// str4 = str4 & "-" & Trim(rsData.Fields("RSZip4"))
				// End If
				// End If
				boolShowAccountField = true;
				if (intTypeOfLabel == modLabels.CNSTLBLTYPE4030)
				{
					// small labels
					if (str1.Length > 24)
					{
						// str5 = str4
						// str4 = str3
						// str3 = str2
						// str2 = Right(str1, Len(str1) - 18)
						boolShowAccountField = false;
						str1 = Strings.Left(str1, 28);
					}
				}
				else
				{
					if (str1.Length > 35)
					{
						// 4" labels
						str6 = str5;
						str5 = str4;
						str4 = str3;
						str3 = str2;
						str2 = Strings.Right(str1, str1.Length - 35);
						str1 = Strings.Left(str1, 35);
					}
				}
				// condense the labels if some are blank
				if (Strings.Trim(str5) == string.Empty)
				{
					str5 = str6;
					str6 = "";
				}
				if (Strings.Trim(str4) == string.Empty)
				{
					str4 = str5;
					str5 = str6;
					str6 = "";
				}
				if (Strings.Trim(str3) == string.Empty)
				{
					str3 = str4;
					// If boolMort Then
					str4 = str5;
					str5 = str6;
					str6 = "";
					// Else
					// str4 = ""
					// End If
				}
				if (Strings.Trim(str2) == string.Empty)
				{
					str2 = str3;
					str3 = str4;
					// If boolMort Then
					str4 = str5;
					str5 = str6;
					str6 = "";
					// Else
					// str4 = ""
					// End If
				}
				if (Strings.Trim(str1) == string.Empty)
				{
					str1 = str2;
					str2 = str3;
					str3 = str4;
					// If boolMort Then
					str4 = str5;
					str5 = str6;
					str6 = "";
					// Else
					// str4 = ""
					// End If
				}
                if (fecherFoundation.Strings.Trim(str6) == "" && fecherFoundation.Strings.Trim(str5) == "")
                {
                    // shift everything down so the top line can only contain the account number
                    str6 = str5;
                    str5 = str4;
                    str4 = str3;
                    str3 = str2;
                    str2 = str1;
                    str1 = "";
                }
                else
                {
                    if (str1.Length > 24 && intTypeOfLabel == modLabels.CNSTLBLTYPE4030)
                    { // kk09112017 trocls-110 Changed from hardcoded 2 - 4030 is 3.5"
                        str1 = Strings.Left(str1, 24);
                    }
                    else if (str1.Length > 18 && intTypeOfLabel == modLabels.CNSTLBLTYPE5160)
                    { // kk09112017 trocls-110 Added check for 5160 which is only 2.5"
                        str1 = Strings.Left(str1, 18);
                    }
                }
                for (intControl = 0; intControl <= Detail.Controls.Count - 1; intControl++)
				{
					if (Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) == "txtData")
					{
						intRow = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Detail.Controls[intControl].Name, 8))));
						GrapeCity.ActiveReports.SectionReportModel.TextBox txtData = (GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl];
						switch (intRow)
						{
							case 1:
								{
									txtData.Text = strLeftAdjustment + str1;
									break;
								}
							case 2:
								{
									txtData.Text = strLeftAdjustment + str2;
									break;
								}
							case 3:
								{
									txtData.Text = strLeftAdjustment + str3;
									break;
								}
							case 4:
								{
									txtData.Text = strLeftAdjustment + str4;
									break;
								}
							case 5:
								{
									txtData.Text = strLeftAdjustment + str5;
									break;
								}
						}
						//end switch
					}
					// this will fill the account number in
					if (Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) == "txtAcct")
					{
						// If boolShowAccountField Then
						if (modStatusPayments.Statics.boolRE)
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = "RE" + FCConvert.ToString(rsData.Get_Fields("Account"));
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = "PP" + FCConvert.ToString(rsData.Get_Fields("Account"));
						}
						// Else
						// ((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = ""
						// End If
					}
					if (boolMort && Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) == "txtMort")
					{
						// TODO Get_Fields: Field [MortgageHolderNumber] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [MortgageHolderNumber] not found!! (maybe it is an alias?)
						((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = Strings.Format(rsData.Get_Fields("MortgageHolderNumber"), "00000");
					}
				}
				// intControl
				rsData.MoveNext();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (Strings.UCase(modCustomReport.Statics.strReportType) == "LABELS")
			{
				PrintLabels();
			}
			else if ((Strings.UCase(modCustomReport.Statics.strReportType) == "RECL") || (Strings.UCase(modCustomReport.Statics.strReportType) == "MORTGAGEHOLDER"))
			{
				PrintMortgageHolders();
			}
		}

		
	}
}
