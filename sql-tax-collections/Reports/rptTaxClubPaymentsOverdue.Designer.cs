namespace TWCL0000
{
    /// <summary>
    /// Summary description for rptTaxClubPaymentsOverdue.
    /// </summary>
    partial class rptTaxClubPaymentsOverdue
    {

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptTaxClubPaymentsOverdue));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lnHeader = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblAcct = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPaymentsOverdue = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAmountExpected = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTotalPaid = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblOwedAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblReportType = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldAcct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.srptTaxClubActivityDetail = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.fldOwed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAmountExpected = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalPaid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPaymentsOverdue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblReceiptDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPaymentCode = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPaymentRef = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPaymentRecipt = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPaymentAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblFooter = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAcct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPaymentsOverdue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAmountExpected)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalPaid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOwedAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReportType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAcct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldOwed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAmountExpected)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPaid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPaymentsOverdue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReceiptDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPaymentCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPaymentRef)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPaymentRecipt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPaymentAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFooter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            //
            // 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldAcct,
            this.fldName,
            this.srptTaxClubActivityDetail,
            this.fldOwed,
            this.fldAmountExpected,
            this.fldTotalPaid,
            this.fldPaymentsOverdue,
            this.lblReceiptDate,
            this.lblPaymentCode,
            this.lblPaymentRef,
            this.lblPaymentRecipt,
            this.lblPaymentAmount});
            this.Detail.Height = 0.5520833F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            //
            // 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
            this.ReportFooter.CanGrow = false;
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblFooter});
            this.ReportFooter.Height = 0.4375F;
            this.ReportFooter.KeepTogether = true;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.CanGrow = false;
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblHeader,
            this.lblDate,
            this.lblPage,
            this.lblTime,
            this.lblMuniName,
            this.lnHeader,
            this.lblAcct,
            this.lblName,
            this.lblPaymentsOverdue,
            this.lblAmountExpected,
            this.lblTotalPaid,
            this.lblOwedAmount,
            this.lblReportType});
            this.PageHeader.Height = 0.75F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // lblHeader
            // 
            this.lblHeader.Height = 0.3125F;
            this.lblHeader.HyperLink = null;
            this.lblHeader.Left = 0F;
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
            this.lblHeader.Text = "Tax Club Outstanding Balance Report";
            this.lblHeader.Top = 0F;
            this.lblHeader.Width = 7.5F;
            // 
            // lblDate
            // 
            this.lblDate.Height = 0.1875F;
            this.lblDate.HyperLink = null;
            this.lblDate.Left = 6.4375F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblDate.Text = null;
            this.lblDate.Top = 0F;
            this.lblDate.Width = 1.0625F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1875F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 6.4375F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblPage.Text = null;
            this.lblPage.Top = 0.1875F;
            this.lblPage.Width = 1.0625F;
            // 
            // lblTime
            // 
            this.lblTime.Height = 0.1875F;
            this.lblTime.HyperLink = null;
            this.lblTime.Left = 0F;
            this.lblTime.Name = "lblTime";
            this.lblTime.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblTime.Text = null;
            this.lblTime.Top = 0.1875F;
            this.lblTime.Width = 1.125F;
            // 
            // lblMuniName
            // 
            this.lblMuniName.Height = 0.1875F;
            this.lblMuniName.HyperLink = null;
            this.lblMuniName.Left = 0F;
            this.lblMuniName.Name = "lblMuniName";
            this.lblMuniName.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblMuniName.Text = null;
            this.lblMuniName.Top = 0F;
            this.lblMuniName.Width = 2.5F;
            // 
            // lnHeader
            // 
            this.lnHeader.Height = 0F;
            this.lnHeader.Left = 0F;
            this.lnHeader.LineWeight = 1F;
            this.lnHeader.Name = "lnHeader";
            this.lnHeader.Top = 0.75F;
            this.lnHeader.Width = 6.9375F;
            this.lnHeader.X1 = 0F;
            this.lnHeader.X2 = 6.9375F;
            this.lnHeader.Y1 = 0.75F;
            this.lnHeader.Y2 = 0.75F;
            // 
            // lblAcct
            // 
            this.lblAcct.Height = 0.2F;
            this.lblAcct.HyperLink = null;
            this.lblAcct.Left = 0F;
            this.lblAcct.Name = "lblAcct";
            this.lblAcct.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblAcct.Text = "Acct";
            this.lblAcct.Top = 0.5486111F;
            this.lblAcct.Width = 0.5F;
            // 
            // lblName
            // 
            this.lblName.Height = 0.2F;
            this.lblName.HyperLink = null;
            this.lblName.Left = 0.5F;
            this.lblName.Name = "lblName";
            this.lblName.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblName.Text = "Name";
            this.lblName.Top = 0.5486111F;
            this.lblName.Width = 1.4F;
            // 
            // lblPaymentsOverdue
            // 
            this.lblPaymentsOverdue.Height = 0.375F;
            this.lblPaymentsOverdue.HyperLink = null;
            this.lblPaymentsOverdue.Left = 3.75F;
            this.lblPaymentsOverdue.Name = "lblPaymentsOverdue";
            this.lblPaymentsOverdue.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
            this.lblPaymentsOverdue.Text = "Pymts Overdue";
            this.lblPaymentsOverdue.Top = 0.375F;
            this.lblPaymentsOverdue.Width = 0.6875F;
            // 
            // lblAmountExpected
            // 
            this.lblAmountExpected.Height = 0.375F;
            this.lblAmountExpected.HyperLink = null;
            this.lblAmountExpected.Left = 4.5F;
            this.lblAmountExpected.Name = "lblAmountExpected";
            this.lblAmountExpected.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblAmountExpected.Text = "Amount Expected";
            this.lblAmountExpected.Top = 0.375F;
            this.lblAmountExpected.Width = 0.8125F;
            // 
            // lblTotalPaid
            // 
            this.lblTotalPaid.Height = 0.1875F;
            this.lblTotalPaid.HyperLink = null;
            this.lblTotalPaid.Left = 5.375F;
            this.lblTotalPaid.Name = "lblTotalPaid";
            this.lblTotalPaid.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblTotalPaid.Text = "Total Paid";
            this.lblTotalPaid.Top = 0.5625F;
            this.lblTotalPaid.Width = 1.0625F;
            // 
            // lblOwedAmount
            // 
            this.lblOwedAmount.Height = 0.1875F;
            this.lblOwedAmount.HyperLink = null;
            this.lblOwedAmount.Left = 6.4375F;
            this.lblOwedAmount.Name = "lblOwedAmount";
            this.lblOwedAmount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblOwedAmount.Text = "Balance";
            this.lblOwedAmount.Top = 0.5625F;
            this.lblOwedAmount.Width = 1.0625F;
            // 
            // lblReportType
            // 
            this.lblReportType.Height = 0.25F;
            this.lblReportType.HyperLink = null;
            this.lblReportType.Left = 0F;
            this.lblReportType.Name = "lblReportType";
            this.lblReportType.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
            this.lblReportType.Text = null;
            this.lblReportType.Top = 0.3125F;
            this.lblReportType.Width = 7.5F;
            // 
            // fldAcct
            // 
            this.fldAcct.Height = 0.1875F;
            this.fldAcct.Left = 0F;
            this.fldAcct.Name = "fldAcct";
            this.fldAcct.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldAcct.Text = null;
            this.fldAcct.Top = 0F;
            this.fldAcct.Width = 0.5F;
            // 
            // fldName
            // 
            this.fldName.CanGrow = false;
            this.fldName.Height = 0.1875F;
            this.fldName.Left = 0.5F;
            this.fldName.Name = "fldName";
            this.fldName.Style = "font-family: \'Tahoma\'; white-space: nowrap";
            this.fldName.Text = null;
            this.fldName.Top = 0F;
            this.fldName.Width = 3.1875F;
            // 
            // srptTaxClubActivityDetail
            // 
            this.srptTaxClubActivityDetail.CloseBorder = false;
            this.srptTaxClubActivityDetail.Height = 0.125F;
            this.srptTaxClubActivityDetail.Left = 0F;
            this.srptTaxClubActivityDetail.Name = "srptTaxClubActivityDetail";
            this.srptTaxClubActivityDetail.Report = null;
            this.srptTaxClubActivityDetail.Top = 0.375F;
            this.srptTaxClubActivityDetail.Width = 7.5F;
            // 
            // fldOwed
            // 
            this.fldOwed.CanGrow = false;
            this.fldOwed.Height = 0.1875F;
            this.fldOwed.Left = 6.4375F;
            this.fldOwed.Name = "fldOwed";
            this.fldOwed.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldOwed.Text = null;
            this.fldOwed.Top = 0F;
            this.fldOwed.Width = 1.0625F;
            // 
            // fldAmountExpected
            // 
            this.fldAmountExpected.CanGrow = false;
            this.fldAmountExpected.Height = 0.1875F;
            this.fldAmountExpected.Left = 4.25F;
            this.fldAmountExpected.Name = "fldAmountExpected";
            this.fldAmountExpected.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldAmountExpected.Text = null;
            this.fldAmountExpected.Top = 0F;
            this.fldAmountExpected.Width = 1.0625F;
            // 
            // fldTotalPaid
            // 
            this.fldTotalPaid.CanGrow = false;
            this.fldTotalPaid.Height = 0.1875F;
            this.fldTotalPaid.Left = 5.375F;
            this.fldTotalPaid.Name = "fldTotalPaid";
            this.fldTotalPaid.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalPaid.Text = null;
            this.fldTotalPaid.Top = 0F;
            this.fldTotalPaid.Width = 1.0625F;
            // 
            // fldPaymentsOverdue
            // 
            this.fldPaymentsOverdue.CanGrow = false;
            this.fldPaymentsOverdue.Height = 0.1875F;
            this.fldPaymentsOverdue.Left = 3.75F;
            this.fldPaymentsOverdue.Name = "fldPaymentsOverdue";
            this.fldPaymentsOverdue.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldPaymentsOverdue.Text = null;
            this.fldPaymentsOverdue.Top = 0F;
            this.fldPaymentsOverdue.Width = 0.5F;
            // 
            // lblReceiptDate
            // 
            this.lblReceiptDate.Height = 0.1875F;
            this.lblReceiptDate.HyperLink = null;
            this.lblReceiptDate.Left = 2.5F;
            this.lblReceiptDate.Name = "lblReceiptDate";
            this.lblReceiptDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
            this.lblReceiptDate.Text = "Date";
            this.lblReceiptDate.Top = 0.1875F;
            this.lblReceiptDate.Width = 1F;
            // 
            // lblPaymentCode
            // 
            this.lblPaymentCode.Height = 0.1875F;
            this.lblPaymentCode.HyperLink = null;
            this.lblPaymentCode.Left = 3.5F;
            this.lblPaymentCode.Name = "lblPaymentCode";
            this.lblPaymentCode.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
            this.lblPaymentCode.Text = "Code";
            this.lblPaymentCode.Top = 0.1875F;
            this.lblPaymentCode.Width = 0.5625F;
            // 
            // lblPaymentRef
            // 
            this.lblPaymentRef.Height = 0.1875F;
            this.lblPaymentRef.HyperLink = null;
            this.lblPaymentRef.Left = 4F;
            this.lblPaymentRef.Name = "lblPaymentRef";
            this.lblPaymentRef.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblPaymentRef.Text = "Ref";
            this.lblPaymentRef.Top = 0.1875F;
            this.lblPaymentRef.Width = 0.8125F;
            // 
            // lblPaymentRecipt
            // 
            this.lblPaymentRecipt.Height = 0.1875F;
            this.lblPaymentRecipt.HyperLink = null;
            this.lblPaymentRecipt.Left = 4.8125F;
            this.lblPaymentRecipt.Name = "lblPaymentRecipt";
            this.lblPaymentRecipt.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblPaymentRecipt.Text = "Rcpt";
            this.lblPaymentRecipt.Top = 0.1875F;
            this.lblPaymentRecipt.Width = 0.5625F;
            // 
            // lblPaymentAmount
            // 
            this.lblPaymentAmount.Height = 0.1875F;
            this.lblPaymentAmount.HyperLink = null;
            this.lblPaymentAmount.Left = 5.375F;
            this.lblPaymentAmount.Name = "lblPaymentAmount";
            this.lblPaymentAmount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblPaymentAmount.Text = "Payment";
            this.lblPaymentAmount.Top = 0.1875F;
            this.lblPaymentAmount.Width = 1.0625F;
            // 
            // lblFooter
            // 
            this.lblFooter.Height = 0.3125F;
            this.lblFooter.HyperLink = null;
            this.lblFooter.Left = 1F;
            this.lblFooter.Name = "lblFooter";
            this.lblFooter.Style = "font-family: \'Tahoma\'; text-align: center";
            this.lblFooter.Text = null;
            this.lblFooter.Top = 0.125F;
            this.lblFooter.Width = 5.5625F;
            // 
            // rptTaxClubPaymentsOverdue
            //
            // 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.25F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.25F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.510417F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAcct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPaymentsOverdue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAmountExpected)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalPaid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOwedAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReportType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAcct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldOwed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAmountExpected)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPaid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPaymentsOverdue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReceiptDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPaymentCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPaymentRef)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPaymentRecipt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPaymentAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFooter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAcct;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
        private GrapeCity.ActiveReports.SectionReportModel.SubReport srptTaxClubActivityDetail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOwed;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmountExpected;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPaid;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPaymentsOverdue;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblReceiptDate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPaymentCode;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPaymentRef;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPaymentRecipt;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPaymentAmount;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblFooter;
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAcct;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPaymentsOverdue;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAmountExpected;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalPaid;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblOwedAmount;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblReportType;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
    }
}