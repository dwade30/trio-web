﻿namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	partial class srptSLAllActivityDetail
	{
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptSLAllActivityDetail));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRef = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNonTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.fldOriginalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOriginalPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOriginalInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOriginalCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lnHeadTotals = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldHeaderTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldHeaderPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldHeaderInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldHeaderCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOriginal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldHeaderDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldHeaderNonTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOriginalNonInt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.fldTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblFooter = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNonTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lnFooterTotal = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRef)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNonTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOriginalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOriginalPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOriginalInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOriginalCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeaderTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeaderPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeaderInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeaderCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOriginal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeaderDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeaderNonTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOriginalNonInt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooter)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNonTotalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldDate,
				this.fldCode,
				this.fldRef,
				this.fldPrincipal,
				this.fldTotal,
				this.fldInterest,
				this.fldCost,
				this.fldNonTotal
			});
			this.Detail.Height = 0.1979167F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldDate
			// 
			this.fldDate.CanGrow = false;
			this.fldDate.Height = 0.1875F;
			this.fldDate.Left = 0.0625F;
			this.fldDate.Name = "fldDate";
			this.fldDate.Style = "font-family: \'Tahoma\'";
			this.fldDate.Text = null;
			this.fldDate.Top = 0F;
			this.fldDate.Width = 1F;
			// 
			// fldCode
			// 
			this.fldCode.CanGrow = false;
			this.fldCode.Height = 0.1875F;
			this.fldCode.Left = 1.9375F;
			this.fldCode.Name = "fldCode";
			this.fldCode.Style = "font-family: \'Tahoma\'";
			this.fldCode.Text = null;
			this.fldCode.Top = 0F;
			this.fldCode.Width = 0.375F;
			// 
			// fldRef
			// 
			this.fldRef.CanGrow = false;
			this.fldRef.Height = 0.1875F;
			this.fldRef.Left = 1.0625F;
			this.fldRef.Name = "fldRef";
			this.fldRef.Style = "font-family: \'Tahoma\'";
			this.fldRef.Text = null;
			this.fldRef.Top = 0F;
			this.fldRef.Width = 0.875F;
			// 
			// fldPrincipal
			// 
			this.fldPrincipal.CanGrow = false;
			this.fldPrincipal.Height = 0.1875F;
			this.fldPrincipal.Left = 2.40625F;
			this.fldPrincipal.Name = "fldPrincipal";
			this.fldPrincipal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldPrincipal.Text = null;
			this.fldPrincipal.Top = 0F;
			this.fldPrincipal.Width = 1.020833F;
			// 
			// fldTotal
			// 
			this.fldTotal.CanGrow = false;
			this.fldTotal.Height = 0.1875F;
			this.fldTotal.Left = 6.479167F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotal.Text = null;
			this.fldTotal.Top = 0F;
			this.fldTotal.Width = 1.020833F;
			// 
			// fldInterest
			// 
			this.fldInterest.CanGrow = false;
			this.fldInterest.Height = 0.1875F;
			this.fldInterest.Left = 3.427083F;
			this.fldInterest.Name = "fldInterest";
			this.fldInterest.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldInterest.Text = null;
			this.fldInterest.Top = 0F;
			this.fldInterest.Width = 1.020833F;
			// 
			// fldCost
			// 
			this.fldCost.CanGrow = false;
			this.fldCost.Height = 0.1875F;
			this.fldCost.Left = 4.4375F;
			this.fldCost.Name = "fldCost";
			this.fldCost.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldCost.Text = null;
			this.fldCost.Top = 0F;
			this.fldCost.Width = 1.020833F;
			// 
			// fldNonTotal
			// 
			this.fldNonTotal.Height = 0.1875F;
			this.fldNonTotal.Left = 5.458333F;
			this.fldNonTotal.Name = "fldNonTotal";
			this.fldNonTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldNonTotal.Text = null;
			this.fldNonTotal.Top = 0F;
			this.fldNonTotal.Width = 1.020833F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldOriginalTotal,
				this.fldOriginalPrincipal,
				this.fldOriginalInterest,
				this.fldOriginalCost,
				this.lnHeadTotals,
				this.fldHeaderTotal,
				this.fldHeaderPrincipal,
				this.fldHeaderInterest,
				this.fldHeaderCost,
				this.fldOriginal,
				this.fldHeaderDate,
				this.fldHeaderNonTotal,
				this.fldOriginalNonInt
			});
			this.ReportHeader.Height = 0.1979167F;
			this.ReportHeader.Name = "ReportHeader";
			this.ReportHeader.Format += new System.EventHandler(this.ReportHeader_Format);
			// 
			// fldOriginalTotal
			// 
			this.fldOriginalTotal.Height = 0.1875F;
			this.fldOriginalTotal.Left = 6.4375F;
			this.fldOriginalTotal.Name = "fldOriginalTotal";
			this.fldOriginalTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldOriginalTotal.Text = null;
			this.fldOriginalTotal.Top = 0F;
			this.fldOriginalTotal.Width = 1.0625F;
			// 
			// fldOriginalPrincipal
			// 
			this.fldOriginalPrincipal.Height = 0.1875F;
			this.fldOriginalPrincipal.Left = 2.40625F;
			this.fldOriginalPrincipal.Name = "fldOriginalPrincipal";
			this.fldOriginalPrincipal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldOriginalPrincipal.Text = null;
			this.fldOriginalPrincipal.Top = 0F;
			this.fldOriginalPrincipal.Width = 1.020833F;
			// 
			// fldOriginalInterest
			// 
			this.fldOriginalInterest.Height = 0.1875F;
			this.fldOriginalInterest.Left = 3.427083F;
			this.fldOriginalInterest.Name = "fldOriginalInterest";
			this.fldOriginalInterest.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldOriginalInterest.Text = null;
			this.fldOriginalInterest.Top = 0F;
			this.fldOriginalInterest.Width = 1.020833F;
			// 
			// fldOriginalCost
			// 
			this.fldOriginalCost.Height = 0.1875F;
			this.fldOriginalCost.Left = 4.4375F;
			this.fldOriginalCost.Name = "fldOriginalCost";
			this.fldOriginalCost.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldOriginalCost.Text = null;
			this.fldOriginalCost.Top = 0F;
			this.fldOriginalCost.Width = 1.020833F;
			// 
			// lnHeadTotals
			// 
			this.lnHeadTotals.Height = 0F;
			this.lnHeadTotals.Left = 2.4375F;
			this.lnHeadTotals.LineWeight = 1F;
			this.lnHeadTotals.Name = "lnHeadTotals";
			this.lnHeadTotals.Top = 0.1875F;
			this.lnHeadTotals.Width = 5.0625F;
			this.lnHeadTotals.X1 = 2.4375F;
			this.lnHeadTotals.X2 = 7.5F;
			this.lnHeadTotals.Y1 = 0.1875F;
			this.lnHeadTotals.Y2 = 0.1875F;
			// 
			// fldHeaderTotal
			// 
			this.fldHeaderTotal.Height = 0.1875F;
			this.fldHeaderTotal.Left = 6.479167F;
			this.fldHeaderTotal.Name = "fldHeaderTotal";
			this.fldHeaderTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldHeaderTotal.Text = null;
			this.fldHeaderTotal.Top = 0F;
			this.fldHeaderTotal.Visible = false;
			this.fldHeaderTotal.Width = 1.020833F;
			// 
			// fldHeaderPrincipal
			// 
			this.fldHeaderPrincipal.Height = 0.1875F;
			this.fldHeaderPrincipal.Left = 2.40625F;
			this.fldHeaderPrincipal.Name = "fldHeaderPrincipal";
			this.fldHeaderPrincipal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldHeaderPrincipal.Text = null;
			this.fldHeaderPrincipal.Top = 0F;
			this.fldHeaderPrincipal.Visible = false;
			this.fldHeaderPrincipal.Width = 1.020833F;
			// 
			// fldHeaderInterest
			// 
			this.fldHeaderInterest.Height = 0.1875F;
			this.fldHeaderInterest.Left = 3.427083F;
			this.fldHeaderInterest.Name = "fldHeaderInterest";
			this.fldHeaderInterest.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldHeaderInterest.Text = null;
			this.fldHeaderInterest.Top = 0F;
			this.fldHeaderInterest.Visible = false;
			this.fldHeaderInterest.Width = 1.020833F;
			// 
			// fldHeaderCost
			// 
			this.fldHeaderCost.Height = 0.1875F;
			this.fldHeaderCost.Left = 4.4375F;
			this.fldHeaderCost.Name = "fldHeaderCost";
			this.fldHeaderCost.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldHeaderCost.Text = null;
			this.fldHeaderCost.Top = 0F;
			this.fldHeaderCost.Visible = false;
			this.fldHeaderCost.Width = 1.020833F;
			// 
			// fldOriginal
			// 
			this.fldOriginal.Height = 0.1875F;
			this.fldOriginal.Left = 0.25F;
			this.fldOriginal.Name = "fldOriginal";
			this.fldOriginal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldOriginal.Text = null;
			this.fldOriginal.Top = 0F;
			this.fldOriginal.Width = 1.1875F;
			// 
			// fldHeaderDate
			// 
			this.fldHeaderDate.Height = 0.1875F;
			this.fldHeaderDate.Left = 1.4375F;
			this.fldHeaderDate.Name = "fldHeaderDate";
			this.fldHeaderDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldHeaderDate.Text = null;
			this.fldHeaderDate.Top = 0F;
			this.fldHeaderDate.Width = 1.1875F;
			// 
			// fldHeaderNonTotal
			// 
			this.fldHeaderNonTotal.Height = 0.1875F;
			this.fldHeaderNonTotal.Left = 5.458333F;
			this.fldHeaderNonTotal.Name = "fldHeaderNonTotal";
			this.fldHeaderNonTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldHeaderNonTotal.Text = null;
			this.fldHeaderNonTotal.Top = 0F;
			this.fldHeaderNonTotal.Width = 1.020833F;
			// 
			// fldOriginalNonInt
			// 
			this.fldOriginalNonInt.Height = 0.1875F;
			this.fldOriginalNonInt.Left = 5.458333F;
			this.fldOriginalNonInt.Name = "fldOriginalNonInt";
			this.fldOriginalNonInt.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldOriginalNonInt.Text = null;
			this.fldOriginalNonInt.Top = 0F;
			this.fldOriginalNonInt.Width = 1.020833F;
			// 
			// ReportFooter
			// 
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldTotalTotal,
				this.fldTotalPrincipal,
				this.lblFooter,
				this.fldTotalInterest,
				this.fldTotalCost,
				this.fldNonTotalTotal,
				this.lnFooterTotal
			});
			this.ReportFooter.Height = 0.28125F;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			// 
			// fldTotalTotal
			// 
			this.fldTotalTotal.Height = 0.1875F;
			this.fldTotalTotal.Left = 6.480556F;
			this.fldTotalTotal.Name = "fldTotalTotal";
			this.fldTotalTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalTotal.Text = null;
			this.fldTotalTotal.Top = 0F;
			this.fldTotalTotal.Width = 1.019444F;
			// 
			// fldTotalPrincipal
			// 
			this.fldTotalPrincipal.Height = 0.1875F;
			this.fldTotalPrincipal.Left = 2.402778F;
			this.fldTotalPrincipal.Name = "fldTotalPrincipal";
			this.fldTotalPrincipal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalPrincipal.Text = null;
			this.fldTotalPrincipal.Top = 0F;
			this.fldTotalPrincipal.Width = 1.019444F;
			// 
			// lblFooter
			// 
			this.lblFooter.Height = 0.1875F;
			this.lblFooter.HyperLink = null;
			this.lblFooter.Left = 0.875F;
			this.lblFooter.Name = "lblFooter";
			this.lblFooter.Style = "font-family: \'Tahoma\'; text-align: right";
			this.lblFooter.Text = "Total";
			this.lblFooter.Top = 0F;
			this.lblFooter.Width = 1.4375F;
			// 
			// fldTotalInterest
			// 
			this.fldTotalInterest.Height = 0.1875F;
			this.fldTotalInterest.Left = 3.422222F;
			this.fldTotalInterest.Name = "fldTotalInterest";
			this.fldTotalInterest.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalInterest.Text = null;
			this.fldTotalInterest.Top = 0F;
			this.fldTotalInterest.Width = 1.019444F;
			// 
			// fldTotalCost
			// 
			this.fldTotalCost.Height = 0.1875F;
			this.fldTotalCost.Left = 4.441667F;
			this.fldTotalCost.Name = "fldTotalCost";
			this.fldTotalCost.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalCost.Text = null;
			this.fldTotalCost.Top = 0F;
			this.fldTotalCost.Width = 1.019444F;
			// 
			// fldNonTotalTotal
			// 
			this.fldNonTotalTotal.Height = 0.1875F;
			this.fldNonTotalTotal.Left = 5.461111F;
			this.fldNonTotalTotal.Name = "fldNonTotalTotal";
			this.fldNonTotalTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldNonTotalTotal.Text = null;
			this.fldNonTotalTotal.Top = 0F;
			this.fldNonTotalTotal.Width = 1.019444F;
			// 
			// lnFooterTotal
			// 
			this.lnFooterTotal.Height = 0F;
			this.lnFooterTotal.Left = 2.4375F;
			this.lnFooterTotal.LineWeight = 1F;
			this.lnFooterTotal.Name = "lnFooterTotal";
			this.lnFooterTotal.Top = 0F;
			this.lnFooterTotal.Width = 5.0625F;
			this.lnFooterTotal.X1 = 2.4375F;
			this.lnFooterTotal.X2 = 7.5F;
			this.lnFooterTotal.Y1 = 0F;
			this.lnFooterTotal.Y2 = 0F;
			// 
			// srptSLAllActivityDetail
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRef)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNonTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOriginalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOriginalPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOriginalInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOriginalCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeaderTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeaderPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeaderInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeaderCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOriginal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeaderDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeaderNonTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOriginalNonInt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooter)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNonTotalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRef;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInterest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNonTotal;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOriginalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOriginalPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOriginalInterest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOriginalCost;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnHeadTotals;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeaderTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeaderPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeaderInterest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeaderCost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOriginal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeaderDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeaderNonTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOriginalNonInt;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalInterest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNonTotalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnFooterTotal;
	}
}
