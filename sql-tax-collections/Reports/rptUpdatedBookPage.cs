﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptUpdatedBookPage : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               06/10/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/02/2004              *
		// ********************************************************
		string strSQL;
		clsDRWrapper rsData = new clsDRWrapper();
		int lngTotalAccounts;
		int lngYear;

		public rptUpdatedBookPage()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Updated Book And Page Report";
		}

		public static rptUpdatedBookPage InstancePtr
		{
			get
			{
				return (rptUpdatedBookPage)Sys.GetInstance(typeof(rptUpdatedBookPage));
			}
		}

		protected rptUpdatedBookPage _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
            }
			base.Dispose(disposing);
		}

		public void Init()
		{
			this.Name = "Updated Book And Page Report";
			// this sub will set the SQL string
			lngYear = frmPrePayYear.InstancePtr.Init(1, 0, "Updated Book Page Report");
			if (lngYear <= 0)
			{
				FCMessageBox.Show("Year not selected.", MsgBoxStyle.Exclamation, "No Year Selected");
				Cancel();
			}
			else
			{
				frmReportViewer.InstancePtr.Init(this);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
			//Detail_Format();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			// save this report for future access
			if (this.Document.Pages.Count > 0)
			{
				modGlobalFunctions.IncrementSavedReports("LastBPReport");
				this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastBPReport.RDF"));
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "t");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			strSQL = BuildSQL();
			rsData.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
			if (rsData.EndOfFile())
			{
				// hide the headers if there are no records
				lblAcct.Visible = false;
				lblName.Visible = false;
			}
		}

		private string BuildSQL()
		{
			string BuildSQL = "";
			BuildSQL = "SELECT * FROM BillingMaster WHERE BillingYear = " + FCConvert.ToString(lngYear);
			return BuildSQL;
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strBookPage = "";
				// this will fill the information into the fields
				TRYAGAIN:
				;
				if (!rsData.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					strBookPage = modGlobalFunctions.GetCurrentBookPageString(FCConvert.ToInt32(rsData.Get_Fields("Account")));
					if (Strings.UCase(Strings.Trim(strBookPage)) == Strings.UCase(Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("BookPage")))))
					{
						rsData.MoveNext();
						goto TRYAGAIN;
					}
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					fldAcct.Text = FCConvert.ToString(rsData.Get_Fields("Account"));
					fldName.Text = rsData.Get_Fields_String("Name1");
					// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
					fldLocation.Text = FCConvert.ToString(rsData.Get_Fields("StreetNumber")) + " " + FCConvert.ToString(rsData.Get_Fields_String("StreetName"));
					if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name1"))) != "")
					{
						fldName.Text = fldName.Text + FCConvert.ToString(rsData.Get_Fields_String("Name2"));
					}
					fldMapLot.Text = rsData.Get_Fields_String("MapLot");
					fldBookPage1.Text = rsData.Get_Fields_String("BookPage");
					fldBookPage2.Text = strBookPage;
					lngTotalAccounts += 1;
					// move to the next record in the query
					rsData.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error in Bankruptcy Report");
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void SetupTotals()
		{
			// this sub will fill in the footer line at the bottom of the report
			lblFooter.Text = "There are " + FCConvert.ToString(lngTotalAccounts) + " accounts that have been updated.";
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			SetupTotals();
		}

		
	}
}
