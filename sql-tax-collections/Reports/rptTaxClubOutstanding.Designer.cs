﻿namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	partial class rptTaxClubOutstanding
	{
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptTaxClubOutstanding));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldAcct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.srptTaxClubActivityDetail = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.fldOwed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.lblFooter = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lnHeader = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblAcct = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPaymentDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPaymentCode = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPaymentRef = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPaymentRecipt = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPaymentAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblOwedAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReportType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.fldAcct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOwed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooter)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAcct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPaymentDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPaymentCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPaymentRef)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPaymentRecipt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPaymentAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOwedAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAcct,
				this.fldName,
				this.srptTaxClubActivityDetail,
				this.fldOwed
			});
			this.Detail.Height = 0.3333333F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldAcct
			// 
			this.fldAcct.Height = 0.1875F;
			this.fldAcct.Left = 0F;
			this.fldAcct.Name = "fldAcct";
			this.fldAcct.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldAcct.Text = null;
			this.fldAcct.Top = 0F;
			this.fldAcct.Width = 0.5F;
			// 
			// fldName
			// 
			this.fldName.CanGrow = false;
			this.fldName.Height = 0.2F;
			this.fldName.Left = 0.5F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'; white-space: nowrap";
			this.fldName.Text = null;
			this.fldName.Top = 0F;
			this.fldName.Width = 5.5F;
			// 
			// srptTaxClubActivityDetail
			// 
			this.srptTaxClubActivityDetail.CloseBorder = false;
			this.srptTaxClubActivityDetail.Height = 0.1125F;
			this.srptTaxClubActivityDetail.Left = 0F;
			this.srptTaxClubActivityDetail.Name = "srptTaxClubActivityDetail";
			this.srptTaxClubActivityDetail.Report = null;
			this.srptTaxClubActivityDetail.Top = 0.2F;
			this.srptTaxClubActivityDetail.Width = 7.5F;
			// 
			// fldOwed
			// 
			this.fldOwed.CanGrow = false;
			this.fldOwed.Height = 0.1875F;
			this.fldOwed.Left = 6.4375F;
			this.fldOwed.Name = "fldOwed";
			this.fldOwed.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldOwed.Text = null;
			this.fldOwed.Top = 0F;
			this.fldOwed.Width = 1.0625F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.CanGrow = false;
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblFooter
			});
			this.ReportFooter.Height = 0.4375F;
			this.ReportFooter.KeepTogether = true;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			// 
			// lblFooter
			// 
			this.lblFooter.Height = 0.3125F;
			this.lblFooter.HyperLink = null;
			this.lblFooter.Left = 1F;
			this.lblFooter.Name = "lblFooter";
			this.lblFooter.Style = "font-family: \'Tahoma\'; text-align: center";
			this.lblFooter.Text = null;
			this.lblFooter.Top = 0.125F;
			this.lblFooter.Width = 5.5625F;
			// 
			// PageHeader
			// 
			this.PageHeader.CanGrow = false;
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblHeader,
				this.lblDate,
				this.lblPage,
				this.lblTime,
				this.lblMuniName,
				this.lnHeader,
				this.lblAcct,
				this.lblName,
				this.lblPaymentDate,
				this.lblPaymentCode,
				this.lblPaymentRef,
				this.lblPaymentRecipt,
				this.lblPaymentAmount,
				this.lblOwedAmount,
				this.lblReportType
			});
			this.PageHeader.Height = 0.75F;
			this.PageHeader.Name = "PageHeader";
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.3125F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblHeader.Text = "Tax Club Outstanding Balance Report";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 7.5F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 6.4375F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1.0625F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 6.4375F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblPage.Text = null;
			this.lblPage.Top = 0.1875F;
			this.lblPage.Width = 1.0625F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1875F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.1875F;
			this.lblTime.Width = 1.125F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 2.5F;
			// 
			// lnHeader
			// 
			this.lnHeader.Height = 0F;
			this.lnHeader.Left = 0F;
			this.lnHeader.LineWeight = 1F;
			this.lnHeader.Name = "lnHeader";
			this.lnHeader.Top = 0.75F;
			this.lnHeader.Width = 6.9375F;
			this.lnHeader.X1 = 0F;
			this.lnHeader.X2 = 6.9375F;
			this.lnHeader.Y1 = 0.75F;
			this.lnHeader.Y2 = 0.75F;
			// 
			// lblAcct
			// 
			this.lblAcct.Height = 0.2F;
			this.lblAcct.HyperLink = null;
			this.lblAcct.Left = 0F;
			this.lblAcct.Name = "lblAcct";
			this.lblAcct.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblAcct.Text = "Acct";
			this.lblAcct.Top = 0.5486111F;
			this.lblAcct.Width = 0.5F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.2F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 0.5F;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblName.Text = "Name";
			this.lblName.Top = 0.5486111F;
			this.lblName.Width = 1.4F;
			// 
			// lblPaymentDate
			// 
			this.lblPaymentDate.Height = 0.1875F;
			this.lblPaymentDate.HyperLink = null;
			this.lblPaymentDate.Left = 2.5F;
			this.lblPaymentDate.Name = "lblPaymentDate";
			this.lblPaymentDate.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblPaymentDate.Text = "Date";
			this.lblPaymentDate.Top = 0.5625F;
			this.lblPaymentDate.Width = 1F;
			// 
			// lblPaymentCode
			// 
			this.lblPaymentCode.Height = 0.1875F;
			this.lblPaymentCode.HyperLink = null;
			this.lblPaymentCode.Left = 3.5F;
			this.lblPaymentCode.Name = "lblPaymentCode";
			this.lblPaymentCode.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblPaymentCode.Text = "Code";
			this.lblPaymentCode.Top = 0.5625F;
			this.lblPaymentCode.Width = 0.5625F;
			// 
			// lblPaymentRef
			// 
			this.lblPaymentRef.Height = 0.1875F;
			this.lblPaymentRef.HyperLink = null;
			this.lblPaymentRef.Left = 4F;
			this.lblPaymentRef.Name = "lblPaymentRef";
			this.lblPaymentRef.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblPaymentRef.Text = "Ref";
			this.lblPaymentRef.Top = 0.5625F;
			this.lblPaymentRef.Width = 0.8125F;
			// 
			// lblPaymentRecipt
			// 
			this.lblPaymentRecipt.Height = 0.1875F;
			this.lblPaymentRecipt.HyperLink = null;
			this.lblPaymentRecipt.Left = 4.8125F;
			this.lblPaymentRecipt.Name = "lblPaymentRecipt";
			this.lblPaymentRecipt.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblPaymentRecipt.Text = "Rcpt";
			this.lblPaymentRecipt.Top = 0.5625F;
			this.lblPaymentRecipt.Width = 0.5625F;
			// 
			// lblPaymentAmount
			// 
			this.lblPaymentAmount.Height = 0.1875F;
			this.lblPaymentAmount.HyperLink = null;
			this.lblPaymentAmount.Left = 5.375F;
			this.lblPaymentAmount.Name = "lblPaymentAmount";
			this.lblPaymentAmount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblPaymentAmount.Text = "Payment";
			this.lblPaymentAmount.Top = 0.5625F;
			this.lblPaymentAmount.Width = 1.0625F;
			// 
			// lblOwedAmount
			// 
			this.lblOwedAmount.Height = 0.1875F;
			this.lblOwedAmount.HyperLink = null;
			this.lblOwedAmount.Left = 6.4375F;
			this.lblOwedAmount.Name = "lblOwedAmount";
			this.lblOwedAmount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblOwedAmount.Text = "Balance";
			this.lblOwedAmount.Top = 0.5625F;
			this.lblOwedAmount.Width = 1.0625F;
			// 
			// lblReportType
			// 
			this.lblReportType.Height = 0.25F;
			this.lblReportType.HyperLink = null;
			this.lblReportType.Left = 0F;
			this.lblReportType.Name = "lblReportType";
			this.lblReportType.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
			this.lblReportType.Text = null;
			this.lblReportType.Top = 0.3125F;
			this.lblReportType.Width = 7.5F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// rptTaxClubOutstanding
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
			((System.ComponentModel.ISupportInitialize)(this.fldAcct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOwed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooter)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAcct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPaymentDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPaymentCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPaymentRef)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPaymentRecipt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPaymentAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOwedAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAcct;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport srptTaxClubActivityDetail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOwed;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFooter;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAcct;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPaymentDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPaymentCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPaymentRef;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPaymentRecipt;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPaymentAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOwedAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReportType;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
