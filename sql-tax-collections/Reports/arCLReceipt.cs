﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using SharedApplication.Enums;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class arCLReceipt : FCSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               03/24/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               08/16/2005              *
		// ********************************************************
		float lngTotalWidth;
		// total width of the report
		int intPaymentNumber;
		// this is the payment number in the grid
		bool boolTextExport;
		//FC:FINAL:AM: variable not used
		//FCPrinter prn = new FCPrinter();
		bool boolDone;
		// VBto upgrade warning: dblReceiptTotal As double	OnWriteFCConvert.ToDouble(
		double dblReceiptTotal;
		bool boolAsterisk;
		bool boolReprint;
		bool boolFirstPass;
		string strPrinterName = "";
		string strPrinterPath = "";
		string strDeviceName = "";

		public arCLReceipt()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Receipt";
		}

		public static arCLReceipt InstancePtr
		{
			get
			{
				return (arCLReceipt)Sys.GetInstance(typeof(arCLReceipt));
			}
		}

		protected arCLReceipt _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				eArgs.EOF = boolDone;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In Fetch Data");
			}
		}

		private void SetArraySizes()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this sub will set the array elements to the left of each column and it will set
				// the total size of the report depending on the size of the receipt
				int intReturn/*unused?*/;
				if (!modGlobal.Statics.gboolCLNarrowReceipt)
				{
					// narrow
					// Me.Printer.PaperSize = 255
					PageSettings.Margins.Left = 360 / 1440F;
					PageSettings.Margins.Right = 360 / 1440F;
					PageSettings.Margins.Top = 360 / 1440F;
					PageSettings.Margins.Bottom = 360 / 1440F;
					// Me.Printer.PaperWidth = 10000
				}
				else
				{
					this.Document.Printer.PaperSize = new System.Drawing.Printing.PaperSize("Size", 312, 1000);
                    //this.Document.Printer.PaperHeight = 1440 * 10; // 22
                    //this.Document.Printer.PaperWidth = 4500;
                    PageSettings.PaperHeight = 10;
                    PageSettings.PaperWidth = 4500 / 1440f;
					PageSettings.Margins.Top = 0;
					PageSettings.Margins.Bottom = 0;
					PageSettings.Margins.Left = 0;
					PageSettings.Margins.Right = 0;
					this.PrintWidth = 4102 / 1440F;
				}
				lngTotalWidth = this.PrintWidth;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Setting Array Size");
			}
		}

		private void SetupReceiptFormat()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// set the lefts and widths
				lblMuniName.Width = lngTotalWidth;
				lblHeader.Width = lngTotalWidth;
				lblComment.Width = lngTotalWidth;
				lblReprint.Width = lngTotalWidth;
				lblOtherAccounts.Width = lngTotalWidth;
				if (modGlobal.Statics.gboolCLNarrowReceipt)
				{
					// narrow
					// move all the header fields to the left and move then on top of one another
					lblDate.Left = 0;
					lblTime.Left = 0;
					lblAccount.Left = 0;
					lblTime.Top = lblDate.Top + lblDate.Height;
					// + 100
					lblAccount.Top = lblTime.Top + lblTime.Height;
					// + 100
					lblHeaderAmt.Left = 0;
					// (lngTotalWidth - lblHeaderAmt.Width) - 300
					lblHeaderAmt.Width = lngTotalWidth;
					lblHeaderAmt.Top = lblAccount.Top + lblAccount.Height;
					// + 100
					lblHeaderAmt.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
					lnHeader.Y1 = lblHeaderAmt.Top + lblHeaderAmt.Height;
					lnHeader.Y2 = lnHeader.Y1;
					lnHeader.Visible = false;
				}
				else
				{
					// wide
					lblDate.Left = 0;
					lblTime.Left = lblDate.Left + lblDate.Width;
					lblAccount.Left = lblTime.Left + lblTime.Width;
					lblTime.Top = lblDate.Top;
					lblAccount.Top = lblDate.Top;
					lblHeaderAmt.Top = lblAccount.Top + lblAccount.Height;
					// set the line
					lnHeader.Y1 = lblHeaderAmt.Top + lblHeaderAmt.Height;
					lnHeader.Y2 = lnHeader.Y1;
					lnHeader.X1 = lblHeaderAmt.Left + 100 / 1440F;
					lnHeader.X2 = lblHeaderAmt.Left + lblHeaderAmt.Width - 100 / 1440F;
				}
				ReportHeader.Height = lnHeader.Y1 + 100 / 1440F;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Setting Receipt Format", "Error Setting Receipt Format");
			}
		}

		private void SetMainLabels()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will put in all of the information on the top and bottom of the receipt..ie comments, date/time, and teller
				DateTime dtReceiptDate;
				dtReceiptDate = DateTime.Today;
				// 1st line
				if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
				{
					lblMuniName.Text = modGlobalConstants.Statics.gstrCityTown + " of " + modGlobalConstants.Statics.MuniName;
				}
				else
				{
					lblMuniName.Text = modGlobalConstants.Statics.MuniName;
				}
				// 2nd line
				lblHeader.Text = "-----  R e c e i p t  -----";
				// 3nd line
				lblDate.Text = Strings.Format(dtReceiptDate, "MM/dd/yyyy");
				lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
				if (modStatusPayments.Statics.boolRE)
				{
					lblAccount.Text = "Account " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE);
				}
				else
				{
					lblAccount.Text = "Account " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP);
				}
				// 4rd line
				if (modGlobal.Statics.gboolCLNarrowReceipt)
				{
					lblHeaderAmt.Text = modGlobalFunctions.PadStringWithSpaces("Amount", 28, true);
				}
				else
				{
					lblHeaderAmt.Text = "Amount";
				}
				if (boolReprint)
				{
					lblReprint.Visible = true;
					lblOtherAccounts.Text = " ";
					lblOtherAccounts.Visible = false;
				}
				else
				{
					lblOtherAccounts.Text = GetRemainingCLBalance();
					lblOtherAccounts.Visible = true;
					lblReprint.Visible = false;
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Setting Main Labels", "Error Setting Main Labels");
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			//Application.DoEvents();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				strPrinterName = StaticSettings.GlobalSettingService
					                 .GetSettingValue(SettingOwnerType.Machine, "RCPTPrinterName")?.SettingValue ?? "";
				//modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "RCPTPrinterName", ref strPrinterName);
				if (strPrinterName != "")
				{
					// check to see if the printer that is setup is a file rather than a printer
					if (SetupAsPrinterFile(ref strPrinterName))
					{
						// if it is then export it to a file
						boolTextExport = true;
					}
					else
					{
						boolTextExport = false;
						this.Document.Printer.PrinterName = strPrinterName;
					}
				}
				boolReprint = frmRECLStatus.InstancePtr.boolReprintReceipt;
				boolFirstPass = true;
				// this is for reprints
				// use the printers collection to set the printer settings
				SetArraySizes();
				SetupReceiptFormat();
				intPaymentNumber = 1;
				SetMainLabels();
				// If strPrinterName <> "" And strPrinterPath <> "" And strDeviceName <> "" Then
				// check to see if the printer that is setup is a file rather than a printer
				// If SetupAsPrinterFile(strPrinterName) Then
				// if it is then export it to a file
				// boolTextExport = True
				// Else
				// boolTextExport = False
				// arReceipt.Document.Printer.PrinterName = strDeviceName
				// End If
				// Else
				// MsgBox "No Receipt Printer is setup, please check the setup of your receipt printer in the printer setup on General Entry.", vbOKOnly + vbCritical, "Printer Setup Error"
				// Unload Me
				// Exit Sub
				// End If
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Starting Receipt");
			}
		}

		private bool SetupAsPrinterFile(ref string strName)
		{
			bool SetupAsPrinterFile = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will return true if the string passed int is "PRNT.txt"
				if (Strings.UCase(strName) == Strings.UCase("PRNT.TXT"))
				{
					SetupAsPrinterFile = true;
				}
				else
				{
					SetupAsPrinterFile = false;
				}
				return SetupAsPrinterFile;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Printer File");
			}
			return SetupAsPrinterFile;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				BindFields();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In Detail Format");
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// strTemp = GetInfoAboutOtherAccounts(intTag, lngNumOfRows)
				if (modGlobal.Statics.gboolCLNarrowReceipt)
				{
					fldTotal.Width = 0;
					fldTotal.Left = 0;
					fldTotal.Visible = false;
					fldTotal.Text = "";
					lblTotal.Top = 0;
					lblTotal.Left = 0;
					lblTotal.Width = lngTotalWidth;
					if (boolAsterisk)
					{
						lblTotal.Text = modGlobalFunctions.PadStringWithSpaces("Total:", 15, false) + modGlobalFunctions.PadStringWithSpaces(Strings.Format(dblReceiptTotal, "$#,##0.00"), 13, true);
					}
					else
					{
						lblTotal.Text = modGlobalFunctions.PadStringWithSpaces("Total:", 15, false) + modGlobalFunctions.PadStringWithSpaces(Strings.Format(dblReceiptTotal, "$#,##0.00") + "*", 13, true);
					}
				}
				else
				{
					fldTotal.Width = lblHeaderAmt.Width;
					fldTotal.Left = lblHeaderAmt.Left;
					lblTotal.Left = fldTotal.Left - lblTotal.Width;
					if (boolAsterisk)
					{
						fldTotal.Text = Strings.Format(dblReceiptTotal, "$#,##0.00");
					}
					else
					{
						fldTotal.Text = Strings.Format(dblReceiptTotal, "$#,##0.00") + "*";
					}
					lblTotal.Text = "Total:";
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In Footer Format");
			}
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intRow = 0;
				float lngStartingPoint = 0;
				float lngRowHeight;
				string strPrinType = "";
				bool boolAddPayment = false;
				lngRowHeight = 270 / 1440F;
				NEXTPAYMENT:
				;
				lblName.Text = "";
				lblLocation.Text = "";
				lblComment.Text = "";
				if (!boolReprint)
				{
					// fill the fields with the payment information
					if (intPaymentNumber <= modStatusPayments.MAX_PAYMENTS)
					{
						if (modStatusPayments.Statics.PaymentArray[intPaymentNumber].Account != 0)
						{
							// keep going
						}
						else
						{
							// chekc the next payment
							intPaymentNumber += 1;
							goto NEXTPAYMENT;
						}
					}
					else
					{
						boolDone = true;
						ClearFields();
						return;
					}
					if (modGlobal.Statics.gboolCLNarrowReceipt)
					{
						// this will set the size of the labels
						lblPrincipal.Width = lngTotalWidth;
						lblPLI.Width = lngTotalWidth;
						lblInterest.Width = lngTotalWidth;
						lblCost.Width = lngTotalWidth;
						lblPrincipalAmount.Width = 0;
						// lblHeaderAmt.Width
						lblPLIAmount.Width = 0;
						// lblHeaderAmt.Width
						lblInterestAmount.Width = 0;
						// lblHeaderAmt.Width
						lblCostAmount.Width = 0;
						// lblHeaderAmt.Width
					}
					string VBtoVar = modStatusPayments.Statics.PaymentArray[intPaymentNumber].Code;
					if (VBtoVar == "A")
					{
						strPrinType = "Abatement";
						boolAddPayment = false;
					}
					else if (VBtoVar == "D")
					{
						strPrinType = "Discount";
						boolAddPayment = false;
					}
					else if (VBtoVar == "I")
					{
						strPrinType = "Interest";
						boolAddPayment = false;
						intPaymentNumber += 1;
						goto NEXTPAYMENT;
					}
					else
					{
						strPrinType = "Principal";
						boolAddPayment = true;
					}
					// this is where the payment lines will be set to
					lngStartingPoint = lblLocation.Top + lblLocation.Height;
					if (Strings.Trim(modStatusPayments.Statics.PaymentArray[intPaymentNumber].PaidBy) != "")
					{
						lblName.Text = modStatusPayments.Statics.PaymentArray[intPaymentNumber].PaidBy;
					}
					else
					{
						if (modGlobal.Statics.gboolCLNarrowReceipt)
						{
							lblName.Text = Strings.Left(frmRECLStatus.InstancePtr.lblOwnersName.Text, 28);
						}
						else
						{
							lblName.Text = frmRECLStatus.InstancePtr.lblOwnersName.Text;
						}
					}
					lblLocation.Text = frmRECLStatus.InstancePtr.lblLocation.Text;
					lblComment.Text = modStatusPayments.Statics.PaymentArray[intPaymentNumber].Comments;
					if (modStatusPayments.Statics.PaymentArray[intPaymentNumber].Principal != 0)
					{
						// show it
						lblPrincipal.Visible = true;
						if (modGlobal.Statics.gboolCLNarrowReceipt)
						{
							lblPrincipal.Top = lngStartingPoint + (lngRowHeight * intRow);
							intRow += 1;
							lblPrincipalAmount.Visible = false;
							lblPrincipalAmount.Left = 0;
							if (boolAddPayment || !(FCConvert.CBool(modStatusPayments.Statics.PaymentArray[intPaymentNumber].CashDrawer == "N") && FCConvert.CBool(modStatusPayments.Statics.PaymentArray[intPaymentNumber].GeneralLedger == "N")))
							{
								lblPrincipal.Text = modGlobalFunctions.PadStringWithSpaces(strPrinType, 15, false) + modGlobalFunctions.PadStringWithSpaces(Strings.Format(modStatusPayments.Statics.PaymentArray[intPaymentNumber].Principal, "$#,##0.00"), 13, true);
							}
							else
							{
								boolAsterisk = true;
								lblPrincipal.Text = modGlobalFunctions.PadStringWithSpaces(strPrinType, 15, false) + modGlobalFunctions.PadStringWithSpaces(Strings.Format(modStatusPayments.Statics.PaymentArray[intPaymentNumber].Principal, "$#,##0.00") + "*", 13, true);
							}
							lblPrincipalAmount.Text = "";
						}
						else
						{
							lblPrincipal.Top = lngStartingPoint + (lngRowHeight * intRow);
							lblPrincipalAmount.Top = lngStartingPoint + (lngRowHeight * intRow);
							lblPrincipalAmount.Visible = true;
							lblPrincipalAmount.Left = lblHeaderAmt.Left;
							lblPrincipal.Text = strPrinType;
							if (boolAddPayment || !(FCConvert.CBool(modStatusPayments.Statics.PaymentArray[intPaymentNumber].CashDrawer == "N") && FCConvert.CBool(modStatusPayments.Statics.PaymentArray[intPaymentNumber].GeneralLedger == "N")))
							{
								lblPrincipalAmount.Text = Strings.Format(modStatusPayments.Statics.PaymentArray[intPaymentNumber].Principal, "$#,##0.00");
							}
							else
							{
								boolAsterisk = true;
								lblPrincipalAmount.Text = Strings.Format(modStatusPayments.Statics.PaymentArray[intPaymentNumber].Principal, "$#,##0.00") + "*";
							}
							intRow += 1;
						}
					}
					else
					{
						// hide it
						lblPrincipal.Visible = false;
						lblPrincipalAmount.Visible = false;
					}
					if (modStatusPayments.Statics.PaymentArray[intPaymentNumber].PreLienInterest != 0)
					{
						// show it
						lblPLI.Visible = true;
						lblPLI.Visible = true;
						lblPLIAmount.Left = lblHeaderAmt.Left;
						if (modGlobal.Statics.gboolCLNarrowReceipt)
						{
							lblPLI.Top = lngStartingPoint + (lngRowHeight * intRow);
							intRow += 1;
							lblPLIAmount.Visible = false;
							lblPLIAmount.Left = 0;
							lblPLIAmount.Top = 0;
							lblPLI.Text = modGlobalFunctions.PadStringWithSpaces("Pre Lien Interest", 15, false) + modGlobalFunctions.PadStringWithSpaces(Strings.Format(modStatusPayments.Statics.PaymentArray[intPaymentNumber].PreLienInterest, "$#,##0.00"), 13, true);
							lblPLIAmount.Text = "";
						}
						else
						{
							lblPLI.Text = "Pre Lien Interest";
							lblPLIAmount.Text = Strings.Format(modStatusPayments.Statics.PaymentArray[intPaymentNumber].PreLienInterest, "$#,##0.00");
							lblPLI.Top = lngStartingPoint + (lngRowHeight * intRow);
							lblPLIAmount.Top = lngStartingPoint + (lngRowHeight * intRow);
							intRow += 1;
						}
					}
					else
					{
						// hide it
						lblPLI.Visible = false;
						lblPLIAmount.Visible = false;
					}
					if (modStatusPayments.Statics.PaymentArray[intPaymentNumber].CurrentInterest != 0)
					{
						// show it
						lblInterest.Visible = true;
						lblInterestAmount.Visible = true;
						lblInterestAmount.Left = lblHeaderAmt.Left;
						if (modGlobal.Statics.gboolCLNarrowReceipt)
						{
							lblInterest.Top = lngStartingPoint + (lngRowHeight * intRow);
							intRow += 1;
							lblInterest.Text = modGlobalFunctions.PadStringWithSpaces("Interest", 15, false) + modGlobalFunctions.PadStringWithSpaces(Strings.Format(modStatusPayments.Statics.PaymentArray[intPaymentNumber].CurrentInterest, "$#,##0.00"), 13, true);
							lblInterestAmount.Text = "";
							lblInterestAmount.Top = 0;
							lblInterestAmount.Visible = false;
							lblInterestAmount.Left = 0;
						}
						else
						{
							lblInterestAmount.Text = Strings.Format(modStatusPayments.Statics.PaymentArray[intPaymentNumber].CurrentInterest, "$#,##0.00");
							lblInterest.Text = "Interest";
							lblInterest.Top = lngStartingPoint + (lngRowHeight * intRow);
							lblInterestAmount.Top = lngStartingPoint + (lngRowHeight * intRow);
							intRow += 1;
						}
					}
					else
					{
						// hide it
						lblInterest.Visible = false;
						lblInterestAmount.Visible = false;
					}
					if (modStatusPayments.Statics.PaymentArray[intPaymentNumber].LienCost != 0)
					{
						// show it
						lblCost.Visible = true;
						lblCostAmount.Visible = true;
						lblCostAmount.Left = lblHeaderAmt.Left;
						if (modGlobal.Statics.gboolCLNarrowReceipt)
						{
							lblCost.Top = lngStartingPoint + (lngRowHeight * intRow);
							intRow += 1;
							lblCost.Text = "Cost";
							lblCost.Text = modGlobalFunctions.PadStringWithSpaces("Cost", 15, false) + modGlobalFunctions.PadStringWithSpaces(Strings.Format(modStatusPayments.Statics.PaymentArray[intPaymentNumber].LienCost, "$#,##0.00"), 13, true);
							lblCostAmount.Top = 0;
							lblCostAmount.Visible = false;
							lblCostAmount.Text = "";
							lblCostAmount.Left = 0;
						}
						else
						{
							lblCost.Top = lngStartingPoint + (lngRowHeight * intRow);
							lblCostAmount.Top = lngStartingPoint + (lngRowHeight * intRow);
							lblCost.Text = "Cost";
							lblCostAmount.Text = Strings.Format(modStatusPayments.Statics.PaymentArray[intPaymentNumber].LienCost, "$#,##0.00");
							intRow += 1;
						}
					}
					else
					{
						// hide it
						lblCost.Visible = false;
						lblCostAmount.Visible = false;
					}
					if (boolAddPayment)
					{
						dblReceiptTotal += FCConvert.ToDouble(modStatusPayments.Statics.PaymentArray[intPaymentNumber].Principal + modStatusPayments.Statics.PaymentArray[intPaymentNumber].PreLienInterest + modStatusPayments.Statics.PaymentArray[intPaymentNumber].CurrentInterest + modStatusPayments.Statics.PaymentArray[intPaymentNumber].LienCost);
					}
				}
				else
				{
					// fill the fields with the payment information
					if (boolFirstPass)
					{
						boolFirstPass = false;
						if (modStatusPayments.Statics.ReprintCLReceipt.Account != 0)
						{
							// keep going
						}
						else
						{
							// chekc the next payment
							intPaymentNumber += 1;
							goto NEXTPAYMENT;
						}
					}
					else
					{
						boolDone = true;
						ClearFields();
						return;
					}
					if (modGlobal.Statics.gboolCLNarrowReceipt)
					{
						// this will set the size of the labels
						lblPrincipal.Width = lngTotalWidth;
						lblPLI.Width = lngTotalWidth;
						lblInterest.Width = lngTotalWidth;
						lblCost.Width = lngTotalWidth;
						lblPrincipalAmount.Width = 0;
						// lblHeaderAmt.Width
						lblPLIAmount.Width = 0;
						// lblHeaderAmt.Width
						lblInterestAmount.Width = 0;
						// lblHeaderAmt.Width
						lblCostAmount.Width = 0;
						// lblHeaderAmt.Width
					}
					string VBtoVar1 = modStatusPayments.Statics.ReprintCLReceipt.Code;
					if (VBtoVar1 == "A")
					{
						strPrinType = "Abatement";
						boolAddPayment = false;
					}
					else if (VBtoVar1 == "D")
					{
						strPrinType = "Discount";
						boolAddPayment = false;
					}
					else if (VBtoVar1 == "I")
					{
						strPrinType = "Interest";
						boolAddPayment = false;
						intPaymentNumber += 1;
						goto NEXTPAYMENT;
					}
					else
					{
						strPrinType = "Principal";
						boolAddPayment = true;
					}
					// this is where the payment lines will be set to
					lngStartingPoint = lblLocation.Top + lblLocation.Height;
					if (Strings.Trim(modStatusPayments.Statics.ReprintCLReceipt.PaidBy) != "")
					{
						lblName.Text = modStatusPayments.Statics.ReprintCLReceipt.PaidBy;
					}
					else
					{
						if (modGlobal.Statics.gboolCLNarrowReceipt)
						{
							lblName.Text = Strings.Left(frmRECLStatus.InstancePtr.lblOwnersName.Text, 28);
						}
						else
						{
							lblName.Text = frmRECLStatus.InstancePtr.lblOwnersName.Text;
						}
					}
					lblLocation.Text = frmRECLStatus.InstancePtr.lblLocation.Text;
					lblComment.Text = modStatusPayments.Statics.ReprintCLReceipt.Comments;
					if (modStatusPayments.Statics.ReprintCLReceipt.Principal != 0)
					{
						// show it
						lblPrincipal.Visible = true;
						if (modGlobal.Statics.gboolCLNarrowReceipt)
						{
							lblPrincipal.Top = lngStartingPoint + (lngRowHeight * intRow);
							intRow += 1;
							lblPrincipalAmount.Visible = false;
							lblPrincipalAmount.Left = 0;
							if (boolAddPayment || !(FCConvert.CBool(modStatusPayments.Statics.ReprintCLReceipt.CashDrawer == "N") && FCConvert.CBool(modStatusPayments.Statics.ReprintCLReceipt.GeneralLedger == "N")))
							{
								lblPrincipal.Text = modGlobalFunctions.PadStringWithSpaces(strPrinType, 15, false) + modGlobalFunctions.PadStringWithSpaces(Strings.Format(modStatusPayments.Statics.ReprintCLReceipt.Principal, "$#,##0.00"), 13, true);
							}
							else
							{
								boolAsterisk = true;
								lblPrincipal.Text = modGlobalFunctions.PadStringWithSpaces(strPrinType, 15, false) + modGlobalFunctions.PadStringWithSpaces(Strings.Format(modStatusPayments.Statics.ReprintCLReceipt.Principal, "$#,##0.00") + "*", 13, true);
							}
							lblPrincipalAmount.Text = "";
						}
						else
						{
							lblPrincipal.Top = lngStartingPoint + (lngRowHeight * intRow);
							lblPrincipalAmount.Top = lngStartingPoint + (lngRowHeight * intRow);
							lblPrincipalAmount.Visible = true;
							lblPrincipalAmount.Left = lblHeaderAmt.Left;
							lblPrincipal.Text = strPrinType;
							if (boolAddPayment || !(FCConvert.CBool(modStatusPayments.Statics.ReprintCLReceipt.CashDrawer == "N") && FCConvert.CBool(modStatusPayments.Statics.ReprintCLReceipt.GeneralLedger == "N")))
							{
								lblPrincipalAmount.Text = Strings.Format(modStatusPayments.Statics.ReprintCLReceipt.Principal, "$#,##0.00");
							}
							else
							{
								boolAsterisk = true;
								lblPrincipalAmount.Text = Strings.Format(modStatusPayments.Statics.ReprintCLReceipt.Principal, "$#,##0.00") + "*";
							}
							intRow += 1;
						}
					}
					else
					{
						// hide it
						lblPrincipal.Visible = false;
						lblPrincipalAmount.Visible = false;
					}
					if (modStatusPayments.Statics.ReprintCLReceipt.PreLienInterest != 0)
					{
						// show it
						lblPLI.Visible = true;
						lblPLI.Visible = true;
						lblPLIAmount.Left = lblHeaderAmt.Left;
						if (modGlobal.Statics.gboolCLNarrowReceipt)
						{
							lblPLI.Top = lngStartingPoint + (lngRowHeight * intRow);
							intRow += 1;
							lblPLIAmount.Visible = false;
							lblPLIAmount.Left = 0;
							lblPLIAmount.Top = 0;
							lblPLI.Text = modGlobalFunctions.PadStringWithSpaces("Pre Lien Interest", 15, false) + modGlobalFunctions.PadStringWithSpaces(Strings.Format(modStatusPayments.Statics.ReprintCLReceipt.PreLienInterest, "$#,##0.00"), 13, true);
							lblPLIAmount.Text = "";
						}
						else
						{
							lblPLI.Text = "Pre Lien Interest";
							lblPLIAmount.Text = Strings.Format(modStatusPayments.Statics.ReprintCLReceipt.PreLienInterest, "$#,##0.00");
							lblPLI.Top = lngStartingPoint + (lngRowHeight * intRow);
							lblPLIAmount.Top = lngStartingPoint + (lngRowHeight * intRow);
							intRow += 1;
						}
					}
					else
					{
						// hide it
						lblPLI.Visible = false;
						lblPLIAmount.Visible = false;
					}
					if (modStatusPayments.Statics.ReprintCLReceipt.CurrentInterest != 0)
					{
						// show it
						lblInterest.Visible = true;
						lblInterestAmount.Visible = true;
						lblInterestAmount.Left = lblHeaderAmt.Left;
						if (modGlobal.Statics.gboolCLNarrowReceipt)
						{
							lblInterest.Top = lngStartingPoint + (lngRowHeight * intRow);
							intRow += 1;
							lblInterest.Text = modGlobalFunctions.PadStringWithSpaces("Interest", 15, false) + modGlobalFunctions.PadStringWithSpaces(Strings.Format(modStatusPayments.Statics.ReprintCLReceipt.CurrentInterest, "$#,##0.00"), 13, true);
							lblInterestAmount.Text = "";
							lblInterestAmount.Top = 0;
							lblInterestAmount.Visible = false;
							lblInterestAmount.Left = 0;
						}
						else
						{
							lblInterestAmount.Text = Strings.Format(modStatusPayments.Statics.ReprintCLReceipt.CurrentInterest, "$#,##0.00");
							lblInterest.Text = "Interest";
							lblInterest.Top = lngStartingPoint + (lngRowHeight * intRow);
							lblInterestAmount.Top = lngStartingPoint + (lngRowHeight * intRow);
							intRow += 1;
						}
					}
					else
					{
						// hide it
						lblInterest.Visible = false;
						lblInterestAmount.Visible = false;
					}
					if (modStatusPayments.Statics.ReprintCLReceipt.LienCost != 0)
					{
						// show it
						lblCost.Visible = true;
						lblCostAmount.Visible = true;
						lblCostAmount.Left = lblHeaderAmt.Left;
						if (modGlobal.Statics.gboolCLNarrowReceipt)
						{
							lblCost.Top = lngStartingPoint + (lngRowHeight * intRow);
							intRow += 1;
							lblCost.Text = "Cost";
							lblCost.Text = modGlobalFunctions.PadStringWithSpaces("Cost", 15, false) + modGlobalFunctions.PadStringWithSpaces(Strings.Format(modStatusPayments.Statics.ReprintCLReceipt.LienCost, "$#,##0.00"), 13, true);
							lblCostAmount.Top = 0;
							lblCostAmount.Visible = false;
							lblCostAmount.Text = "";
							lblCostAmount.Left = 0;
						}
						else
						{
							lblCost.Top = lngStartingPoint + (lngRowHeight * intRow);
							lblCostAmount.Top = lngStartingPoint + (lngRowHeight * intRow);
							lblCost.Text = "Cost";
							lblCostAmount.Text = Strings.Format(modStatusPayments.Statics.ReprintCLReceipt.LienCost, "$#,##0.00");
							intRow += 1;
						}
					}
					else
					{
						// hide it
						lblCost.Visible = false;
						lblCostAmount.Visible = false;
					}
					if (boolAddPayment)
					{
						dblReceiptTotal += FCConvert.ToDouble(modStatusPayments.Statics.ReprintCLReceipt.Principal + modStatusPayments.Statics.ReprintCLReceipt.PreLienInterest + modStatusPayments.Statics.ReprintCLReceipt.CurrentInterest + modStatusPayments.Statics.ReprintCLReceipt.LienCost);
					}
				}
				// set the height of the detail section
				Detail.Height = lngStartingPoint + ((intRow) * lngRowHeight);
				intPaymentNumber += 1;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In Detail Format");
			}
		}

		private void ClearFields()
		{
			Detail.Height = 0;
			lblPrincipal.Text = "";
			lblPrincipalAmount.Text = "";
			lblPLI.Text = "";
			lblPLIAmount.Text = "";
			lblInterest.Text = "";
			lblInterestAmount.Text = "";
			lblCost.Text = "";
			lblCostAmount.Text = "";
		}

		private string GetRemainingCLBalance()
		{
			string GetRemainingCLBalance = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngGrpNum = 0;
				double dblTotal;
				int intNumAccts = 0;
				int lngAcctNumber = 0;
				bool boolError = false;
				if (modStatusPayments.Statics.boolRE)
				{
					lngAcctNumber = StaticSettings.TaxCollectionValues.LastAccountRE;
					lngGrpNum = modGlobalFunctions.GetGroupNumber_6(lngAcctNumber, "RE");
				}
				else
				{
					lngAcctNumber = StaticSettings.TaxCollectionValues.LastAccountPP;
					lngGrpNum = modGlobalFunctions.GetGroupNumber_6(lngAcctNumber, "PP");
				}
				dblTotal = 0;
				if (lngGrpNum > 0)
				{
					boolError = !GroupInformation(ref intNumAccts, ref dblTotal, ref lngGrpNum);
				}
				else
				{
					// this is when the account has not been set up in a group, but it still can have a remaining balance
					if (modStatusPayments.Statics.boolRE)
					{
						dblTotal += modCLCalculations.CalculateAccountTotal(lngAcctNumber, true);
					}
					else
					{
						dblTotal += modCLCalculations.CalculateAccountTotal(lngAcctNumber, false);
					}
				}
				if (boolError)
				{
					// do not do anything
					GetRemainingCLBalance = "";
				}
				else
				{
					if (intNumAccts > 1)
					{
						GetRemainingCLBalance = "Remaining Balance : " + "(" + FCConvert.ToString(intNumAccts) + ") " + Strings.Format(dblTotal, "$#,##0.00");
					}
					else
					{
						GetRemainingCLBalance = "Remaining Balance : " + Strings.Format(dblTotal, "$#,##0.00");
					}
				}
				return GetRemainingCLBalance;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Remaining Balance Error");
			}
			return GetRemainingCLBalance;
		}
		// VBto upgrade warning: intTotalAccounts As short	OnWriteFCConvert.ToInt32(
		public bool GroupInformation(ref int intTotalAccounts, ref double dblTotalOutstandingBalance, ref int lngGroupNumber)
		{
			bool GroupInformation = false;
			// this function will return true if a group number has been found and will pass the number of accounts and balance back in the parameters
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsGroup = new clsDRWrapper();
				string strSQL;
				int intTemp = 0;
				bool boolCL = false;
				bool boolUT = false;
				strSQL = "SELECT * FROM ModuleAssociation WHERE GroupNumber = " + FCConvert.ToString(lngGroupNumber);
				if (rsGroup.OpenRecordset(strSQL, "CentralData"))
				{
					if (!rsGroup.EndOfFile())
					{
						GroupInformation = true;
						// this will loop through all of the transactions on the receipt and check for groups
						while (!rsGroup.EndOfFile())
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							dblTotalOutstandingBalance += GetRemainingBalance(FCConvert.ToInt32(rsGroup.Get_Fields("Account")), FCConvert.ToString(rsGroup.Get_Fields_String("Module")), FCConvert.ToInt16(intTemp));
							// this will decide if the accounts have already been counted or not
							string VBtoVar = rsGroup.Get_Fields_String("Module");
							if ((VBtoVar == "RE") || (VBtoVar == "PP"))
							{
								if (boolCL)
								{
								}
								else
								{
									boolCL = true;
									// intTotalAccounts = intTotalAccounts + intTemp
								}
							}
							else if (VBtoVar == "UT")
							{
								if (boolUT)
								{
								}
								else
								{
									boolUT = true;
									// intTotalAccounts = intTotalAccounts + intTemp
								}
							}
							rsGroup.MoveNext();
						}
						intTotalAccounts = FCConvert.ToInt16(rsGroup.RecordCount());
					}
					else
					{
						intTotalAccounts = 0;
						dblTotalOutstandingBalance = 0;
						GroupInformation = false;
					}
				}
				return GroupInformation;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				intTotalAccounts = 0;
				dblTotalOutstandingBalance = 0;
				GroupInformation = false;
			}
			return GroupInformation;
		}
		// VBto upgrade warning: intNumAccts As short	OnWriteFCConvert.ToInt32(
		public double GetRemainingBalance(ref int lngAccountNumber, string strModule, short intNumAccts)
		{
			double GetRemainingBalance = 0;
			// this function will return the remaining balance from one account
			clsDRWrapper rsMod = new clsDRWrapper();
			clsDRWrapper rsTempCL = new clsDRWrapper();
			clsDRWrapper rsTempLien = new clsDRWrapper();
			string strSQL = "";
			// set the default database and the sql string for the
			rsMod.DefaultDB = modExtraModules.strCLDatabase;
			rsTempCL.OpenRecordset("SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAccountNumber) + " AND BillingType = '" + strModule + "' ORDER BY BillingYear", modExtraModules.strCLDatabase);
			if (rsTempCL.EndOfFile() != true && rsTempCL.BeginningOfFile() != true)
			{
				do
				{
					if (((rsTempCL.Get_Fields_Int32("LienRecordNumber"))) != 0)
					{
						rsTempLien.OpenRecordset("SELECT * FROM LienRec WHERE LienRecordNumber = " + FCConvert.ToString(rsTempCL.Get_Fields_Int32("LienRecordNumber")), modExtraModules.strCLDatabase);
						if (!rsTempLien.EndOfFile())
						{
							double temp = 0;
							GetRemainingBalance += modCLCalculations.CalculateAccountCLLien(rsTempLien, DateTime.Today, ref temp);
						}
						else
						{
							// keep going
						}
					}
					else
					{
						GetRemainingBalance += modCLCalculations.CalculateAccountCL(ref rsTempCL, lngAccountNumber, DateTime.Today, 0);
					}
					rsTempCL.MoveNext();
				}
				while (!rsTempCL.EndOfFile());
			}
			else
			{
				GetRemainingBalance = 0;
			}
			return GetRemainingBalance;
			ERROR_HANDLER:
			;
			GetRemainingBalance = 0;
			return GetRemainingBalance;
		}

		private void arCLReceipt_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//arCLReceipt.Text	= "Receipt";
			//arCLReceipt.Icon	= "arCLReceipt.dsx":0000";
			//arCLReceipt.Left	= 0;
			//arCLReceipt.Top	= 0;
			//arCLReceipt.Width	= 9525;
			//arCLReceipt.Height	= 8595;
			//arCLReceipt.SectionData	= "arCLReceipt.dsx":058A;
			//End Unmaped Properties
		}
	}
}
