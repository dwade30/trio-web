using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptCLDailyAuditMaster : FCSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               03/21/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/29/2005              *
		// ********************************************************
		public bool boolWide;
		// True if this report is in Wide Format
		public int lngWidth;
		// This is the Print Width of the report
		bool boolDone;
		public string strType = "";
		public bool boolShowRE;
		// Show RE Records?
		public bool boolOrderByReceipt;
		// order by receipt
		public bool boolCloseOutAudit;
		// Actually close out the payments
		bool boolAppendPP;
		// add the PP audit on after the RE audit
		public bool boolLandscape;
		// is the report to be printed landscape?
		bool boolFinishedRE;
		public bool boolRecreation;
		bool boolSaveThisFile;

		public rptCLDailyAuditMaster()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;

            this.Name = "Daily Audit Report";
        }

		public static rptCLDailyAuditMaster InstancePtr
		{
			get
			{
				return (rptCLDailyAuditMaster)Sys.GetInstance(typeof(rptCLDailyAuditMaster));
			}
		}

		protected rptCLDailyAuditMaster _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// this should let the detail section fire once
			if (boolDone)
			{
				if (boolAppendPP)
				{
					boolAppendPP = false;
					boolShowRE = false;
					strType = "AND BillCode = 'P' ";
                    eArgs.EOF = false;
				}
				else
				{
                    eArgs.EOF = boolDone;
				}
			}
			else
			{
                eArgs.EOF = boolDone;
				boolDone = true;
			}
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Hide();
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Hide();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading", true);
		}

		private void SetupFields()
		{
			// find out which way the user wants to print
			boolWide = boolLandscape;
			if (boolWide)
			{
				// wide format
				lngWidth = (int)(15000 / 1440f);
				this.Document.Printer.DefaultPageSettings.Landscape = true;
			}
			else
			{
                // narrow format
                //FC:FINAL:KS: #i241:total column missing
                //lngWidth = (int)(11400 / 1440f);
                lngWidth = (int)(11520 / 1440f);
				this.Document.Printer.Landscape = false;
			}
			this.PrintWidth = lngWidth;
			sarCLDailyAudit.Width = lngWidth;
		}

		public void StartUp(bool boolIsRE, bool boolCloseOut, bool boolPassLandscape, bool boolBoth = false, bool boolPassRecreation = false)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rs = new clsDRWrapper();
				boolCloseOutAudit = boolCloseOut;
				boolAppendPP = boolBoth;
				boolRecreation = boolPassRecreation;
				// if the boolAppend is on then the first report will ALWAYS be the RE audit
				// at the end of the first report the PP audit will be printed
				if (boolAppendPP)
				{
					boolShowRE = true;
				}
				else
				{
					boolShowRE = boolIsRE;
				}
				boolLandscape = boolPassLandscape;
				if (boolShowRE)
				{
					strType = "AND (BillCode = 'R' OR BillCode = 'L') ";
				}
				else
				{
					strType = "AND BillCode = 'P' ";
				}
				//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
				SetupFields();
				// MAL@20080311: Change to correctly select the Close Out
				// Tracker Reference: 12622
				// rs.OpenRecordset "SELECT * FROM PaymentRec WHERE DailyCloseOut = 0", strCLDatabase
				rs.OpenRecordset("SELECT * FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + Convert.ToString(modCollections.Statics.glngCurrentCloseOut), modExtraModules.strCLDatabase);
				if (rs.EndOfFile())
				{
					boolSaveThisFile = false;
				}
				else
				{
					boolSaveThisFile = true;
				}
				frmWait.InstancePtr.IncrementProgress();
				// Me.Show , MDIParent
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + Convert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MsgBoxStyle.Critical, "Error In Start Up");
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsCloseOut = new clsDRWrapper();
				string strSQL = "";
				// This will move all of the reports back 1 number until 9 and then save this as the first
				// MAL@20080826: Check for recreate
				// Tracker Reference: 14245
				if (boolCloseOutAudit && boolSaveThisFile && !modGlobalConstants.Statics.gblnRecreate)
				{
					modGlobalFunctions.IncrementSavedReports("LastCLDailyAudit");
					this.Document.Save(System.IO.Path.Combine(modGlobalConstants.ReportPath, "LastCLDailyAudit1.RDF"));
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (boolAppendPP)
			{
				boolShowRE = true;
			}
			sarCLDailyAudit.Report = new arCLDailyAudit();
			if (boolAppendPP && boolFinishedRE)
			{
				sarCLDailyAudit.Report.UserData = "PP";
			}
			else if (boolShowRE)
			{
				sarCLDailyAudit.Report.UserData = "RE";
			}
		}

		private void rptCLDailyAuditMaster_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptCLDailyAuditMaster.Caption	= "Daily Audit Report";
			//rptCLDailyAuditMaster.Icon	= "rptCLDailtAuditMaster.dsx":0000";
			//rptCLDailyAuditMaster.Left	= 0;
			//rptCLDailyAuditMaster.Top	= 0;
			//rptCLDailyAuditMaster.Width	= 11880;
			//rptCLDailyAuditMaster.Height	= 8595;
			//rptCLDailyAuditMaster.StartUpPosition	= 3;
			//rptCLDailyAuditMaster.SectionData	= "rptCLDailtAuditMaster.dsx":058A;
			//End Unmaped Properties
		}
	}
}