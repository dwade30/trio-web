﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptUpdatedAddressList : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               05/30/2006              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               05/30/2006              *
		// ********************************************************
		int lngTotalAccounts;
		int lngCount;
		// this will keep track of which row I am on in the grid
		bool boolDone;

		public rptUpdatedAddressList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Updated Address List";
		}

		public static rptUpdatedAddressList InstancePtr
		{
			get
			{
				return (rptUpdatedAddressList)Sys.GetInstance(typeof(rptUpdatedAddressList));
			}
		}

		protected rptUpdatedAddressList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public void Init(int lngPassTotalAccounts, string strYear)
		{
			lngTotalAccounts = lngPassTotalAccounts;
			lblYear.Text = strYear;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!modGlobal.Statics.arrDemand[lngCount].Used && boolDone)
			{
				eArgs.EOF = true;
				lblNew.Visible = false;
				lblOld.Visible = false;
				Detail.Height = 0;
			}
			else
			{
				eArgs.EOF = false;
			}
			//Detail_Format();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			// save this report for future access
			if (this.Document.Pages.Count > 0)
			{
				modGlobalFunctions.IncrementSavedReports("LastUpdateAddress");
				this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastUpdateAddress1.RDF"));
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "t");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			lblHeader.Text = "Update Address List";
			lngCount = 0;
		}

		private void BindFields()
		{
			// this will fill the information into the fields
			if (!boolDone)
			{
				// this will find the next available account that was processed
				while (!(modGlobal.Statics.arrDemand[lngCount].Processed || lngCount >= Information.UBound(modGlobal.Statics.arrDemand, 1)))
				{
					lngCount += 1;
				}
				// check to see if we are at the end of the list
				if (lngCount >= Information.UBound(modGlobal.Statics.arrDemand, 1))
				{
					boolDone = true;
					// clear the fields so there are no repeats
					fldAcct.Text = "";
					fldName.Text = "";
					fldOld.Text = "";
					fldNew.Text = "";
					lblNew.Visible = false;
					lblOld.Visible = false;
					Detail.Height = 0;
					return;
				}
				fldAcct.Text = modGlobal.Statics.arrDemand[lngCount].Account.ToString();
				fldName.Text = modGlobal.Statics.arrDemand[lngCount].Name;
				fldOld.Text = modGlobal.Statics.arrDemand[lngCount].OldAddress;
				fldNew.Text = modGlobal.Statics.arrDemand[lngCount].NewAddress;
				// move to the next record in the array
				lngCount += 1;
			}
			else
			{
				// clear the fields
				fldAcct.Text = "";
				fldName.Text = "";
				fldOld.Text = "";
				fldNew.Text = "";
				lblNew.Visible = false;
				lblOld.Visible = false;
				Detail.Height = 0;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			// this sub will fill in the footer line at the bottom of the report
			if (lngTotalAccounts == 1)
			{
				lblNumberOfAccounts.Text = "There was " + FCConvert.ToString(lngTotalAccounts) + " account processed.";
			}
			else
			{
				lblNumberOfAccounts.Text = "There were " + FCConvert.ToString(lngTotalAccounts) + " accounts processed.";
			}
		}

		
	}
}
