﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using GrapeCity.ActiveReports.Extensibility.Printing;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptReminderNoticeLabels : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/12/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               04/11/2005              *
		// ********************************************************
		// THIS REPORT IS TOTOALLY NOT GENERIC AND HAS BEEN ALTERED FOR THIS TWCL
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsLData = new clsDRWrapper();
		clsDRWrapper rsTemp = new clsDRWrapper();
		// VBto upgrade warning: intLabelWidth As short --> As int	OnWrite(int, double)
		float intLabelWidth;
		bool boolDifferentPageSize;
		string strFont;
		bool boolPP;
		// XXXXX This is always FALSE!
		bool boolMort;
		// XXXXX This is always FALSE!
		int lngReportType;
		clsDRWrapper rsRate = new clsDRWrapper();
		int intReminderType;
		// 1- 4 is the period that they want a reminder for and 0 is for past due
		string strLastPrinter = "";
		string strLeftAdjustment = "";
		// VBto upgrade warning: lngVertAdjust As int	OnWriteFCConvert.ToDouble(
		float lngVertAdjust;
		string strRateKeyList = "";
		int lngIndex;
		bool boolNoSummary;
		int lngNameLen;
		// This is the maximum amount of characters allowed in the name in order to show the account number
		clsPrintLabel labLabels = new clsPrintLabel();
		bool boolDotMatrix;
		// VBto upgrade warning: lngPrintWidth As int	OnWrite(string, double)
		float lngPrintWidth;
		int intTypeOfLabel;
		double dblMinimumAmount;
		// MAL@20071207
		Dictionary<object, object> objDict = new Dictionary<object, object>();
		// MAL@20080801
		string strDefPrinterName;
		bool blnHideDuplicates;
		string strFields = "";
        ReminderNoticeOptions reminderNoticeOptions;
        private cAddressControllerCL tAddrCtrlr = new cAddressControllerCL();

		public rptReminderNoticeLabels()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Custom Labels";
		}

		public static rptReminderNoticeLabels InstancePtr
		{
			get
			{
				return (rptReminderNoticeLabels)Sys.GetInstance(typeof(rptReminderNoticeLabels));
			}
		}

		protected rptReminderNoticeLabels _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsTemp?.Dispose();
				rsData?.Dispose();
				rsRate?.Dispose();
				rsLData?.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// IF THIS IS THE END OF THE RECORDSET THEN WE NEED A WAY TO GET OUT
			eArgs.EOF = rsData.EndOfFile();
			//Detail_Format();
		}
        // VBto upgrade warning: intLabelType As short	OnWriteFCConvert.ToInt32(
        // VBto upgrade warning: intPassType As short	OnWriteFCConvert.ToInt32(
        public void Init(ReminderNoticeOptions options)
        {
            //  Init(string strSQL, int intReportType, int intLabelType, string strPrinterName, string strFonttoUse, bool blnHideDup, int intPassType = 0, string strPassRateKeyList = "", double dblPassMinAmount = 0, string strPassDefaultPrinter = "")
            try
            {
                // On Error GoTo ERROR_HANDLER
                int intReportType = 0;
                reminderNoticeOptions = options;
                int intReturn/*unused?*/;
                int x/*unused?*/;
                bool boolUseFont/*unused?*/;
                strFont = reminderNoticeOptions.Font;
                this.Document.Printer.PrinterName = reminderNoticeOptions.Printer;
                // XXX				strDefPrinterName = strPassDefaultPrinter;
                // MAL@20080919 ; Tracker Reference: 14365
                FCUtils.EraseSafe(modReminderNoticeSummary.Statics.arrReminderSummaryList);
                if (Strings.Trim(reminderNoticeOptions.RateKeyList) != "")
                {
                    if (reminderNoticeOptions.LienedRecords)
                    {
                        strRateKeyList = " AND LienRec.RateKey IN" + reminderNoticeOptions.RateKeyList;
                    }
                    else
                    {
                        strRateKeyList = " AND RateKey IN" + reminderNoticeOptions.RateKeyList;
                    }
                }

                intReminderType = (int)options.Period;
                int intLabelType = reminderNoticeOptions.LabelType;
                // this will pass the type of reminder in
                dblMinimumAmount = reminderNoticeOptions.MinimumAmount;
                // MAL@20081105 ; Tracker Reference: 13831
                blnHideDuplicates = reminderNoticeOptions.HideDuplicateNames;
                boolPP = false;
                boolMort = false;
                lngNameLen = 40;
                rsData.OpenRecordset(reminderNoticeOptions.Query, modExtraModules.strCLDatabase);
                rsLData.OpenRecordset("SELECT * FROM LienRec", modExtraModules.strCLDatabase);
                rsRate.OpenRecordset("SELECT * FROM RateRec", modExtraModules.strCLDatabase);
                intTypeOfLabel = reminderNoticeOptions.LabelType;
				if (rsData.RecordCount() > 0)
				{
					boolNoSummary = false;
                    // 2.9.18
                    if (modStatusPayments.Statics.boolRE)
                    {
                        if (reminderNoticeOptions.LienedRecords)
                        {
                            strFields = "Account,Name1,Name2,Address1,Address2,Address3,LienRecordNumber,TransferFromBillingDateFirst,TransferFromBillingDateLast," + "LienRec.Principal,LienRec.PrincipalPaid,LienRec.RateKey,LienRec.InterestAppliedThroughDate";
                        }
                        else
                        {
                            strFields = "Account,Name1,Name2,Address1,Address2,Address3,LienRecordNumber,InterestAppliedThroughDate,TransferFromBillingDateFirst,TransferFromBillingDateLast," + "PrincipalPaid,TaxDue1,TaxDue2,TaxDue3,TaxDue4,RateKey";
                        }
                    }
                    else
                    {
                        strFields = "BillingMaster.Account,Name1,Name2,BillingMaster.Address1,BillingMaster.Address2,Address3,LienRecordNumber,InterestAppliedThroughDate," + "PrincipalPaid,TaxDue1,TaxDue2,TaxDue3,TaxDue4,RateKey";
                    }
                    switch (intReportType)
					{
						case 0:
							{
								// Labels
								modCustomReport.Statics.strReportType = "LABELS";
								// make them choose the printer first if you have to use a custom form
								// RESET THE RECORDSET TO THE BEGINNING OF THE RECORDSET
								if (rsData.RecordCount() != 0)
									rsData.MoveFirst();
								boolDifferentPageSize = false;
								int intIndex = 0;
								int cnt/*unused?*/;
								intIndex = labLabels.Get_IndexFromID(intTypeOfLabel);
								if (labLabels.Get_IsDymoLabel(intIndex))
								{
									switch (labLabels.Get_ID(intIndex))
									{
										case modLabels.CNSTLBLTYPEDYMO30256:
											{
												this.Document.Printer.DefaultPageSettings.Landscape = true;
												PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
												PageSettings.Margins.Top = FCConvert.ToSingle(labLabels.Get_TopMargin(intIndex) + FCConvert.ToSingle(modGlobal.Statics.gdblLabelsAdjustment * 240) / 1440f);
												PageSettings.Margins.Bottom = labLabels.Get_BottomMargin(intIndex);
												PageSettings.Margins.Right = labLabels.Get_RightMargin(intIndex);
												PageSettings.Margins.Left = labLabels.Get_LeftMargin(intIndex);
												PrintWidth = labLabels.Get_LabelWidth(intIndex) - PageSettings.Margins.Left - PageSettings.Margins.Right;
												intLabelWidth = labLabels.Get_LabelWidth(intIndex);
												lngPrintWidth = PrintWidth;
												Detail.Height = labLabels.Get_LabelHeight(intIndex) - PageSettings.Margins.Top - PageSettings.Margins.Bottom - 10 / 1440f;
												Detail.ColumnCount = 1;
												PageSettings.PaperHeight = labLabels.Get_LabelWidth(intIndex);
												PageSettings.PaperWidth = labLabels.Get_LabelHeight(intIndex);
												Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
												this.Document.Printer.DefaultPageSettings.Landscape = true;
												PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
												break;
											}
										case modLabels.CNSTLBLTYPEDYMO30252:
											{
												this.Document.Printer.DefaultPageSettings.Landscape = true;
												PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
												PageSettings.Margins.Top = 0.128F;
												PageSettings.Margins.Bottom = 0;
												PageSettings.Margins.Right = 0;
												PageSettings.Margins.Left = 0.128F;
												// 
												PrintWidth = 3.5F - PageSettings.Margins.Left - PageSettings.Margins.Right;
												intLabelWidth = 3.5F;
												lngPrintWidth = FCConvert.ToInt32(3.5F - PageSettings.Margins.Left - PageSettings.Margins.Right);
												Detail.Height = 1.1F - PageSettings.Margins.Top - PageSettings.Margins.Bottom - 10 / 1440f;
												Detail.ColumnCount = 1;
												PageSettings.PaperHeight = 3.5F;
												PageSettings.PaperWidth = 1.1F;
												this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
												this.Document.Printer.DefaultPageSettings.Landscape = true;
												PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
												break;
											}
										case modLabels.CNSTLBLTYPEDYMO4150:
											{
												this.Document.Printer.DefaultPageSettings.Landscape = true;
												PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
												PageSettings.Margins.Top = 0.3F;
												PageSettings.Margins.Bottom = 0;
												PageSettings.Margins.Right = 0.25F;
												PageSettings.Margins.Left = 0.128F;
												PrintWidth = 5040 / 1440f - PageSettings.Margins.Left - PageSettings.Margins.Right;
												intLabelWidth = 5040 / 1440f - PageSettings.Margins.Left - PageSettings.Margins.Right;
												lngPrintWidth = 5040 / 1440f - PageSettings.Margins.Left - PageSettings.Margins.Right - 25 / 1440f;
												Detail.Height = 1620 / 1440f - PageSettings.Margins.Top - PageSettings.Margins.Bottom - 10 / 1440f;
												Detail.ColumnCount = 1;
												PageSettings.PaperHeight = 5040 / 1440f;
												PageSettings.PaperWidth = 1620 / 1440f;
												this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
												break;
											}
									}
								}
								else if (labLabels.Get_IsLaserLabel(intIndex))
								{
									PageSettings.Margins.Top = 0.5F;
									PageSettings.Margins.Top += FCConvert.ToSingle(modGlobal.Statics.gdblLabelsAdjustment) * 270 / 1440f;
									PageSettings.Margins.Left = labLabels.Get_LeftMargin(intIndex);
									PageSettings.Margins.Right = labLabels.Get_RightMargin(intIndex);
									PrintWidth = labLabels.Get_PageWidth(intIndex) - labLabels.Get_LeftMargin(intIndex) - labLabels.Get_RightMargin(intIndex);
									intLabelWidth = labLabels.Get_LabelWidth(intIndex);
									Detail.Height = labLabels.Get_LabelHeight(intIndex) + labLabels.Get_VerticalSpace(intIndex);
									if (labLabels.Get_LabelsWide(intIndex) > 0)
									{
										Detail.ColumnCount = labLabels.Get_LabelsWide(intIndex);
										Detail.ColumnSpacing = labLabels.Get_HorizontalSpace(intIndex);
									}
									lngPrintWidth = PrintWidth;
								}
								
								break;
							}
						case 1:
							{
								// Post Cards
								modCustomReport.Statics.strReportType = "POSTCARDS";
								this.PageSettings.Margins.Top = (0);
								this.PageSettings.Margins.Bottom = 0;
								PrintWidth = 6.5F;
								this.PageSettings.Margins.Left = 1;
								this.PageSettings.Margins.Left = 1;
								Detail.KeepTogether = false;
								Detail.Height = 4F;
								Detail.CanShrink = false;
								Detail.ColumnCount = 1;
								lngReportType = 10;
								break;
							}
						case 2:
							{
								// Forms
								modCustomReport.Statics.strReportType = "FORMS";
								break;
							}
					}
					//end switch
					CreateDataFields(ref intReportType, ref intLabelType);
					frmReportViewer.InstancePtr.Init(this, "");
				}
				else
				{
					frmWait.InstancePtr.Unload();
					FCMessageBox.Show("No records meet the criteria selected.", MsgBoxStyle.Exclamation, "No Records");
					boolNoSummary = true;
					Cancel();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Initializing Notices");
			}
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			// If Me.Pages.Count > 0 Then
			// Select Case frmRateRecChoice.intRateType
			// Case 10
			// IncrementSavedReports "LastCL30DayLabels"
			// Me.Pages.Save "RPT\LastCL30DayLabels1.RDF"
			// Case 11
			// IncrementSavedReports "LastCLTransferToLienLabels"
			// Me.Pages.Save "RPT\LastCLTransferToLienLabels1.RDF"
			// Case 12
			// IncrementSavedReports "LastCLLienMaturityLabels"
			// Me.Pages.Save "RPT\LastCLLienMaturityLabels1.RDF"
			// End Select
			// End If
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int intReturn = 0;
			int const_PrintToolID;

            if (intTypeOfLabel == modLabels.CNSTLBLTYPEDYMO30256 || intTypeOfLabel == modLabels.CNSTLBLTYPEDYMO30252)
			{
                int cnt;

                for (cnt = 0; cnt <= this.Document.Printer.PaperSizes.Count - 1; cnt++)
				{
					switch (intTypeOfLabel)
					{
						case modLabels.CNSTLBLTYPEDYMO30252:
							{
								if (Strings.UCase(this.Document.Printer.PaperSizes[cnt].PaperName) == "30252 ADDRESS")
								{
									this.Document.Printer.PaperSize = this.Document.Printer.PaperSizes[cnt];
									this.Document.Printer.DefaultPageSettings.Landscape = true;
									break;
								}
								break;
							}
						case modLabels.CNSTLBLTYPEDYMO30256:
							{
								if (Strings.UCase(this.Document.Printer.PaperSizes[cnt].PaperName) == "30256 SHIPPING")
								{
									this.Document.Printer.PaperSize = this.Document.Printer.PaperSizes[cnt];
									this.Document.Printer.DefaultPageSettings.Landscape = true;
									break;
								}
								break;
							}
						case modLabels.CNSTLBLTYPEDYMO4150:
							{
								// MAL@20080918: Add new label type
								// Tracker Reference: 14365
								if (Strings.UCase(this.Document.Printer.PaperSizes[cnt].PaperName) == "99012 LARGE ADDRESS")
								{
									this.Document.Printer.PaperSize = this.Document.Printer.PaperSizes[cnt];
									this.Document.Printer.DefaultPageSettings.Landscape = true;
									break;
								}
								break;
							}
					}
					//end switch
				}
				// cnt
			}
			else
			{
                this.Document.Printer.PaperSize = new System.Drawing.Printing.PaperSize("CLCustomLabel1", FCConvert.ToInt32(this.PageSettings.PaperWidth * 100), FCConvert.ToInt32(this.PageSettings.PaperHeight * 100));
			}
		}

		private void CreateDataFields(ref int intRepType, ref int intLabType)
		{
			int intControlNumber/*unused?*/;
			GrapeCity.ActiveReports.SectionReportModel.TextBox NewField/*unused?*/;
			int intRow = 0;
			int intCol/*unused?*/;
			int intNumber = 0;
			float lngLineHeight;
			lngLineHeight = 225 / 1440F;
			// CREATE THE CONTROLS AND SET THE POSITION OF THE CONTROLS for each type
			switch (intRepType)
			{
				case 0:
					{
						// Labels
						intNumber = 4;
						if (boolMort)
							intNumber = 5;
						for (intRow = 1; intRow <= intNumber; intRow++)
						{
							NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
							NewField.CanGrow = false;
							NewField.Name = "txtData" + FCConvert.ToString(intRow);
							NewField.Top = (intRow - 1) * lngLineHeight;
							NewField.Left = 144 / 1440F;
							// one space
							if (intRow == 1)
							{
								NewField.Width = (PrintWidth / Detail.ColumnCount - ((Detail.ColumnCount - 1) * Detail.ColumnSpacing)) - 1014 / 1440F;
								// 870
							}
							else
							{
								NewField.Width = (PrintWidth / Detail.ColumnCount - ((Detail.ColumnCount - 1) * Detail.ColumnSpacing)) - 145 / 1440F;
							}
							NewField.Height = 225 / 1440F;
							NewField.Text = string.Empty;
							NewField.MultiLine = false;
							NewField.WordWrap = true;
							if (Strings.Trim(strFont) != string.Empty)
							{
								NewField.Font = new Font(strFont, NewField.Font.Size);
							}
							Detail.Controls.Add(NewField);
						}
						// This will add the account field
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtAccount";
						NewField.Top = 0;
						NewField.Left = PrintWidth / Detail.ColumnCount - ((Detail.ColumnCount - 1) * Detail.ColumnSpacing) - 1009 / 1440F;
						// 720   'align to the right
						// NewField.Width = 720
						NewField.Width = 864 / 1440F;
						NewField.Height = 225 / 1440F;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						break;
					}
				case 1:
					{
						// Post Cards
						// set up the form
						Detail.Height = 3F;
						// 3"
						this.PageSettings.PaperHeight = 4F;
						// 4"
						this.PageSettings.Margins.Left = 360 / 1440f;
						// 1/4"
						this.PageSettings.Margins.Top = 720 / 1440f;
						// 1/2"
						this.PageSettings.Margins.Bottom = 1;
						// 1"
						// Return Mailing Address 1
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtReturnMailingAddress1";
						NewField.Top = 255 / 1440f;
						NewField.Left = 0;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440f;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Return Mailing Address 2
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtReturnMailingAddress2";
						NewField.Top = 225 * 2 / 1440f;
						NewField.Left = 0;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440f;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Return Mailing Address 3
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtReturnMailingAddress3";
						NewField.Top = 225 * 3 / 1440F;
						NewField.Left = 0;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440F;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Return Mailing Address 4
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtReturnMailingAddress4";
						NewField.Top = 225 * 4 / 1440F;
						NewField.Left = 0;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440F;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Message (multiline)
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtMessage";
						NewField.Top = 1.25F;
						// 1 1/4"
						NewField.Left = 0.375F;
						// 3/8"
						NewField.Width = this.PrintWidth - NewField.Left;
						NewField.Height = 1F;
						// 1"
						NewField.Text = string.Empty;
						NewField.MultiLine = true;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Account
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtAccount";
						NewField.Top = 2.125F;
						// 2"
						NewField.Left = 0.875F;
						// 3/8"
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440F;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Name
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtName";
						NewField.Top = 2.125F + (255 / 1440F);
						NewField.Left = 0.875F;
						// 7/8"
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440F;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Mailing Address 1
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtMailingAddress1";
						NewField.Top = 2.125F + (255 * 2 / 1440F);
						NewField.Left = 0.875F;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440F;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Mailing Address 2
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtMailingAddress2";
						NewField.Top = 2.125F + (255 * 3 / 1440F);
						NewField.Left = 0.875F;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440F;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Mailing Address 3
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtMailingAddress3";
						NewField.Top = 2.125F + (255 * 4 / 1440F);
						NewField.Left = 0.875F;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440F;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Mailing Address 4
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtMailingAddress4";
						NewField.Top = 2.125F + (255 * 5 / 1440F);
						NewField.Left = 0.875F;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440F;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Mailing Address 5
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtMailingAddress5";
						NewField.Top = 2.125F + (255 * 6 / 1440F);
						// this will move this label to the bottom of the detail section to make sure that
						// the detail section cannot shrink to smaller than the post card height
						NewField.Top = 4F - (225 / 1440F);
						NewField.Text = " ";
						NewField.Left = 0.875F;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440F;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						break;
					}
				case 2:
					{
						// Forms
						// setup the page size
						switch (intLabType)
						{
							case 0:
								{
									// Plain Paper (1 per using half page)
									PrintWidth = 6.5F;
									PageSettings.Margins.Top = 0.5F;
									this.PageSettings.Margins.Left = 1;
									PageSettings.Margins.Right = 1;
									Detail.Height = 5.5F;
									Detail.ColumnCount = 1;
									lngReportType = 21;
									break;
								}
							case 1:
								{
									// Plain Paper (2 per page)
									PrintWidth = 6.5F;
									PageSettings.Margins.Top = 0.5F;
									PageSettings.Margins.Left = 1;
									PageSettings.Margins.Right = 1;
									Detail.Height = 5.5F;
									Detail.ColumnCount = 1;
									lngReportType = 22;
									break;
								}
							case 2:
								{
									// Plain Paper (1 per using whole page)
									PrintWidth = 6.5F;
									PageSettings.Margins.Top = 0.5F;
									PageSettings.Margins.Left = 1;
									PageSettings.Margins.Right = 1;
									Detail.Height = 11F;
									Detail.ColumnCount = 1;
									lngReportType = 23;
									break;
								}
						}
						//end switch
						// setup the fields
						switch (intLabType)
						{
							case 0:
							case 1:
								{
									// Plain Paper (1 or two per page)
									// Return Mailing Address 1
									NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
									NewField.CanGrow = false;
									NewField.Name = "txtReturnMailingAddress1";
									NewField.Top = 0;
									NewField.Left = 0;
									NewField.Width = this.PrintWidth / 2;
									NewField.Height = 225 / 1440f;
									NewField.Text = string.Empty;
									NewField.MultiLine = false;
									NewField.WordWrap = true;
									if (Strings.Trim(strFont) != string.Empty)
									{
										NewField.Font = new Font(strFont, NewField.Font.Size);
									}
									Detail.Controls.Add(NewField);
									// Return Mailing Address 2
									NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
									NewField.CanGrow = false;
									NewField.Name = "txtReturnMailingAddress2";
									NewField.Top = 225 / 1440f;
									NewField.Left = 0;
									NewField.Width = this.PrintWidth / 2;
									NewField.Height = 225 / 1440f;
									NewField.Text = string.Empty;
									NewField.MultiLine = false;
									NewField.WordWrap = true;
									if (Strings.Trim(strFont) != string.Empty)
									{
										NewField.Font = new Font(strFont, NewField.Font.Size);
									}
									Detail.Controls.Add(NewField);
									// Return Mailing Address 3
									NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
									NewField.CanGrow = false;
									NewField.Name = "txtReturnMailingAddress3";
									NewField.Top = 2 * 225 / 1440f;
									NewField.Left = 0;
									NewField.Width = this.PrintWidth / 2;
									NewField.Height = 225 / 1440f;
									NewField.Text = string.Empty;
									NewField.MultiLine = false;
									NewField.WordWrap = true;
									if (Strings.Trim(strFont) != string.Empty)
									{
										NewField.Font = new Font(strFont, NewField.Font.Size);
									}
									Detail.Controls.Add(NewField);
									// Return Mailing Address 4
									NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
									NewField.CanGrow = false;
									NewField.Name = "txtReturnMailingAddress4";
									NewField.Top = 3 * 225 / 1440F;
									NewField.Left = 0;
									NewField.Width = this.PrintWidth / 2;
									NewField.Height = 225 / 1440F;
									NewField.Text = string.Empty;
									NewField.MultiLine = false;
									NewField.WordWrap = true;
									if (Strings.Trim(strFont) != string.Empty)
									{
										NewField.Font = new Font(strFont, NewField.Font.Size);
									}
									Detail.Controls.Add(NewField);
									// Account
									NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
									NewField.CanGrow = false;
									NewField.Name = "txtAccountNumber";
									NewField.Top = (intRow - 1) * 225 / 1440F;
									NewField.Left = 0;
									NewField.Width = this.PrintWidth / 2;
									NewField.Height = 225 / 1440F;
									NewField.Text = string.Empty;
									NewField.MultiLine = false;
									NewField.WordWrap = true;
									if (Strings.Trim(strFont) != string.Empty)
									{
										NewField.Font = new Font(strFont, NewField.Font.Size);
									}
									Detail.Controls.Add(NewField);
									// Message (multiline)
									NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
									NewField.CanGrow = false;
									NewField.Name = "txtMessage";
									NewField.Top = 1F;
									// 1"
									NewField.Left = 0.375F;
									// 3/8"
									NewField.Width = this.PrintWidth - NewField.Left;
									NewField.Height = 1F;
									// 1"
									NewField.Text = string.Empty;
									NewField.MultiLine = true;
									NewField.WordWrap = true;
									if (Strings.Trim(strFont) != string.Empty)
									{
										NewField.Font = new Font(strFont, NewField.Font.Size);
									}
									Detail.Controls.Add(NewField);
									// Amount Due Line
									NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
									NewField.CanGrow = false;
									NewField.Name = "txtAmountDue";
									NewField.Top = (intRow - 1) * 225 / 1440F;
									NewField.Left = 0;
									NewField.Width = this.PrintWidth - NewField.Left;
									NewField.Height = 225 / 1440F;
									NewField.Text = string.Empty;
									NewField.MultiLine = false;
									NewField.WordWrap = true;
									if (Strings.Trim(strFont) != string.Empty)
									{
										NewField.Font = new Font(strFont, NewField.Font.Size);
									}
									Detail.Controls.Add(NewField);
									// Date Line
									NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
									NewField.CanGrow = false;
									NewField.Name = "txtDateLine";
									NewField.Top = 2F;
									NewField.Left = 0;
									NewField.Width = this.PrintWidth - NewField.Left;
									NewField.Height = 225 / 1440F;
									NewField.Text = string.Empty;
									NewField.MultiLine = false;
									NewField.WordWrap = true;
									if (Strings.Trim(strFont) != string.Empty)
									{
										NewField.Font = new Font(strFont, NewField.Font.Size);
									}
									Detail.Controls.Add(NewField);
									// Name
									NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
									NewField.CanGrow = false;
									NewField.Name = "txtName";
									NewField.Top = 2F + (255 / 1440F);
									NewField.Left = 0.875F;
									// 7/8"
									NewField.Width = this.PrintWidth / 2;
									NewField.Height = 225 / 1440F;
									NewField.Text = string.Empty;
									NewField.MultiLine = false;
									NewField.WordWrap = true;
									if (Strings.Trim(strFont) != string.Empty)
									{
										NewField.Font = new Font(strFont, NewField.Font.Size);
									}
									Detail.Controls.Add(NewField);
									// Mailing Address 1
									NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
									NewField.CanGrow = false;
									NewField.Name = "txtMailingAddress1";
									NewField.Top = 2f + (255 * 2 / 1440F);
									NewField.Left = 0.875F;
									NewField.Width = this.PrintWidth / 2;
									NewField.Height = 225 / 1440F;
									NewField.Text = string.Empty;
									NewField.MultiLine = false;
									NewField.WordWrap = true;
									if (Strings.Trim(strFont) != string.Empty)
									{
										NewField.Font = new Font(strFont, NewField.Font.Size);
									}
									Detail.Controls.Add(NewField);
									// Mailing Address 2
									NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
									NewField.CanGrow = false;
									NewField.Name = "txtMailingAddress2";
									NewField.Top = 2F + (255 * 3 / 1440F);
									NewField.Left = 0.875F;
									NewField.Width = this.PrintWidth / 2;
									NewField.Height = 225 / 1440F;
									NewField.Text = string.Empty;
									NewField.MultiLine = false;
									NewField.WordWrap = true;
									if (Strings.Trim(strFont) != string.Empty)
									{
										NewField.Font = new Font(strFont, NewField.Font.Size);
									}
									Detail.Controls.Add(NewField);
									// Mailing Address 3
									NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
									NewField.CanGrow = false;
									NewField.Name = "txtMailingAddress3";
									NewField.Top = 2F + (255 * 4 / 1440F);
									NewField.Left = 0.875F;
									NewField.Width = this.PrintWidth / 2;
									NewField.Height = 225 / 1440F;
									NewField.Text = string.Empty;
									NewField.MultiLine = false;
									NewField.WordWrap = true;
									if (Strings.Trim(strFont) != string.Empty)
									{
										NewField.Font = new Font(strFont, NewField.Font.Size);
									}
									Detail.Controls.Add(NewField);
									// Mailing Address 4
									NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
									NewField.CanGrow = false;
									NewField.Name = "txtMailingAddress4";
									NewField.Top = 2F + (255 * 5 / 1440F);
									NewField.Left = 0.875F;
									NewField.Width = this.PrintWidth / 2;
									NewField.Height = 225 / 1440F;
									NewField.Text = string.Empty;
									NewField.MultiLine = false;
									NewField.WordWrap = true;
									if (Strings.Trim(strFont) != string.Empty)
									{
										NewField.Font = new Font(strFont, NewField.Font.Size);
									}
									Detail.Controls.Add(NewField);
									// Mailing Address 5
									NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
									NewField.CanGrow = false;
									NewField.Name = "txtMailingAddress5";
									NewField.Top = 2F + (255 * 6 / 1440F);
									NewField.Left = 0.875F;
									NewField.Width = this.PrintWidth / 2;
									NewField.Height = 225 / 1440F;
									NewField.Text = string.Empty;
									NewField.MultiLine = false;
									NewField.WordWrap = true;
									if (Strings.Trim(strFont) != string.Empty)
									{
										NewField.Font = new Font(strFont, NewField.Font.Size);
									}
									Detail.Controls.Add(NewField);
									break;
								}
							case 2:
								{
									// Full page notice
									break;
								}
						}
						//end switch
						break;
					}
				case 3:
					{
						// Mailer
						// set up the form
						PrintWidth = 6.5F;
						Detail.Height = 3F;
						// 3"
						this.PageSettings.PaperHeight = 4F;
						// 4"
						this.PageSettings.Margins.Left = 360 / 1440f;
						// 1/4"
						this.PageSettings.Margins.Top = 720 / 1440f;
						// 1/2"
						this.PageSettings.Margins.Bottom = 720 / 1440f;
						// 1/2"
						// Return Mailing Address 1
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtReturnMailingAddress1";
						NewField.Top = 0;
						NewField.Left = 0;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440f;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Return Mailing Address 2
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtReturnMailingAddress2";
						NewField.Top = 225 / 1440f;
						NewField.Left = 0;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440f;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Return Mailing Address 3
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtReturnMailingAddress3";
						NewField.Top = 2 * 225 / 1440F;
						NewField.Left = 0;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440F;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Return Mailing Address 4
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtReturnMailingAddress4";
						NewField.Top = 3 * 225 / 1440F;
						NewField.Left = 0;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440F;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Account
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtAccountNumber";
						NewField.Top = (intRow - 1) * 225 / 1440F;
						NewField.Left = 0;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440F;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Message (multiline)
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtMessage";
						NewField.Top = 1F;
						// 1"
						NewField.Left = 0.375F;
						// 3/8"
						NewField.Width = this.PrintWidth - NewField.Left;
						NewField.Height = 1F;
						// 1"
						NewField.Text = string.Empty;
						NewField.MultiLine = true;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Amount Due Line
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtAmountDue";
						NewField.Top = (intRow - 1) * 225 / 1440F;
						NewField.Left = 0;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440F;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Date Line
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtDateLine";
						NewField.Top = (intRow - 1) * 225 / 1440F;
						NewField.Left = 0;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440F;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Name
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtName";
						NewField.Top = 2.125F;
						// 2 1/8"
						NewField.Left = 0.875F;
						// 7/8"
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440F;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Mailing Address 1
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtMailingAddress1";
						NewField.Top = 2.125F + 225 / 1440F;
						NewField.Left = 0.875F;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440F;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Mailing Address 2
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtMailingAddress2";
						NewField.Top = 2.125F + (225 * 2 / 1440F);
						NewField.Left = 0.875F;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440F;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Mailing Address 3
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtMailingAddress3";
						NewField.Top = 2.125F + (225 * 3 / 1440F);
						NewField.Left = 0.875F;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440F;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Mailing Address 4
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtMailingAddress4";
						NewField.Top = 2.125F + (225 * 4 / 1440F);
						NewField.Left = 0.875F;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440F;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Mailing Address 5
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtMailingAddress5";
						NewField.Top = 2.125F + (225 * 5 / 1440F);
						NewField.Left = 0.875F;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440F;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if (!boolNoSummary)
			{
				modReminderNoticeSummary.Statics.intRPTReminderSummaryType = 0;
                //FC:FINAL:SBE - #i331 - in VB6 Show is executed, and standard report viewer is displayed. Use frmReportViewer, because we don't have a standard report viewer
                //rptReminderSummary.InstancePtr.Run();
                fecherFoundation.Sys.ClearInstance(frmReportViewer.InstancePtr);
                frmReportViewer.InstancePtr.Init(rptReminderSummary.InstancePtr);
			}
		}
		//public void ActiveReport_ToolbarClick(DDActiveReports2.DDTool Tool)
		//{
		//	// Call VerifyPrintToFile(Me, Tool)
		//	string VBtoVar = Tool.Caption;
		//	if (VBtoVar == "Print...")
		//	{
		//		if (frmNumPages.InstancePtr.Init(this.Pages.Count, 1, this))
		//		{
		//			this.Document.Print(false);
		//		}
		//	}
		//}
		private void PrintForms()
		{
			int intControl;
			int intRow;
			string str1 = "";
			string str2 = "";
			string str3 = "";
			string str4 = "";
			string str5 = "";
			if (rsTemp.EndOfFile() != true)
			{
				str1 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Name1")));
				// TODO Get_Fields: Field [Own1Address1] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [Own1Address1] not found!! (maybe it is an alias?)
				str2 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("Own1Address1")));
				// TODO Get_Fields: Field [Own1Address2] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [Own1Address2] not found!! (maybe it is an alias?)
				str3 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("Own1Address2")));
				// TODO Get_Fields: Field [Own1City] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [Own1State] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [Own1City] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [Own1State] not found!! (maybe it is an alias?)
				str4 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("Own1City")) + "  " + FCConvert.ToString(rsTemp.Get_Fields("Own1State")));
				if (boolPP)
				{
					// XXXXXXXXXXXXXXXX WHAT IS THIS???  boolPP is always False!
					System.Diagnostics.Debug.Assert(!boolPP);
					if (Conversion.Val(rsTemp.Get_Fields_String("RSZip")) > 0)
					{
						// And then why is the ref to RE fields?
						str4 += " " + Strings.Format(rsTemp.Get_Fields_String("RSZip"), "00000");
						// TODO Get_Fields: Field [MRSZip4] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [MRSZip4] not found!! (maybe it is an alias?)
						if (Conversion.Val(rsTemp.Get_Fields("MRSZip4")) > 0)
						{
							str4 += "-" + Strings.Format(rsTemp.Get_Fields_String("RSZip4"), "0000");
						}
					}
				}
				else if (boolMort)
				{
					// XXXXXXXXXXXXXXXX WHAT IS THIS???  boolMort is always False!
					System.Diagnostics.Debug.Assert(!boolMort);
					str4 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("RSAddr3")) + "  " + FCConvert.ToString(rsTemp.Get_Fields_String("RSState")));
					str4 += " " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("RSZip")));
					if (Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("RSZip4"))) != string.Empty)
					{
						str4 += "-" + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("RSZip4")));
					}
				}
				else
				{
					// TODO Get_Fields: Field [Own1Zip] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [Own1Zip] not found!! (maybe it is an alias?)
					str4 += " " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("Own1Zip")));
				}
				// condense the labels if some are blank
				if (boolMort)
				{
					if (Strings.Trim(str4) == string.Empty)
					{
						str4 = str5;
						str5 = "";
					}
				}
				if (Strings.Trim(str3) == string.Empty)
				{
					str3 = str4;
					if (boolMort)
					{
						str4 = str5;
						str5 = "";
					}
					else
					{
						str4 = "";
					}
				}
				if (Strings.Trim(str2) == string.Empty)
				{
					str2 = str3;
					str3 = str4;
					if (boolMort)
					{
						str4 = str5;
						str5 = "";
					}
					else
					{
						str4 = "";
					}
				}
				if (Strings.Trim(str1) == string.Empty)
				{
					str1 = str2;
					str2 = str3;
					str3 = str4;
					if (boolMort)
					{
						str4 = str5;
						str5 = "";
					}
					else
					{
						str4 = "";
					}
				}
				for (intControl = 0; intControl <= Detail.Controls.Count - 1; intControl++)
				{
					if (Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) == "txtData")
					{
						intRow = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Detail.Controls[intControl].Name, 8))));
						switch (intRow)
						{
							case 1:
								{
									((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = str1 + " ";
									break;
								}
							case 2:
								{
									((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = str2 + " ";
									break;
								}
							case 3:
								{
									((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = str3 + " ";
									break;
								}
							case 4:
								{
									((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = str4 + " ";
									break;
								}
							case 5:
								{
									((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = str5 + " ";
									break;
								}
						}
						//end switch
					}
				}
				// intControl
				rsTemp.MoveNext();
			}
		}

		private void PrintLabels()
		{
			// this will fil in the fields for labels
			int intControl;
			int intRow;
			string str1 = "";
			string str2 = "";
			string str3 = "";
			string str4 = "";
			string str5 = "";
			int lngAcct = 0;
            bool boolNewOwner = false; // trocls-119 10.6.17 kjr 4.23.18 CODE FREEZE
            bool boolNewAddress = false;
            DateTime dtBillDate;
            bool boolSameName = false;
            bool boolREMatch = false;
            string strPrevSecOwnerName = "";
            string strPrevAddress1 = "";
            string strPrevAddress2 = "";
            string strPrevAddress3 = "";
            var strDeedName = "";
            //cPartyController pCont = new cPartyController();
            //// vbPorter upgrade warning: pInfo As CParty	OnWrite(cParty)
            //cParty pInfo = new cParty();
            //cPartyAddress pAdd;
            //clsDRWrapper rsMastOwner = new clsDRWrapper();
            short intDummyVar; // kjr 4.30.18

            TRYAGAIN:
			;
			if (rsTemp.EndOfFile() != true)
			{
				// check to make sure this account would be on this label report
				if (!CheckAccount())
				{
					// if the account shold not be here then
					Detail.Visible = false;
					rsTemp.MoveNext();
					goto TRYAGAIN;
				}
				else
				{
                    Detail.Visible = true;

                    lngAcct = FCConvert.ToInt32(rsTemp.Get_Fields("Account"));
                    if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("Name2"))) != "")
                    {
                        str1 = strLeftAdjustment + fecherFoundation.Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("Name1"))) + " and " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("Name2")));
                    }
                    else
                    {
                        str1 = strLeftAdjustment + fecherFoundation.Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("Name1")));
                    }

                    clsAddress tAddr;

                    switch (reminderNoticeOptions.AddressType)
                    {
	                    case ReminderNoticeOptions.AddressEnum.AddressAtBilling:
		                    tAddr = tAddrCtrlr.GetOwnerAndAddressAtBilling(rsTemp);
		                    break;
	                    case ReminderNoticeOptions.AddressEnum.CareOfCurrentOwner:
		                    tAddr = tAddrCtrlr.GetOwnerAtBillingCareOfCurrentOwner(rsTemp);
		                    break;
	                    case ReminderNoticeOptions.AddressEnum.LastKnownAddress:
		                    tAddr = tAddrCtrlr.GetBilledOwnerAtLastAddress(rsTemp);
		                    break;
	                    default:
		                    tAddr = new clsAddress();
		                    break;
                    }

                    str2 = tAddr.Get_Address(1).Trim();
                    str3 = tAddr.Get_Address(2).Trim();
                    str4 = tAddr.Get_Address(3).Trim();
                    str5 = tAddr.Get_Address(4).Trim();


					
                    // condense the labels if some are blank
                    // If boolMort Then
                    if (Strings.Trim(str4) == string.Empty)
					{
						str4 = str5;
						str5 = "";
					}
					// End If
					if (Strings.Trim(str3) == string.Empty)
					{
						str3 = str4;
						// If boolMort Then
						str4 = str5;
						str5 = "";
					}
					if (Strings.Trim(str2) == string.Empty)
					{
						str2 = str3;
						str3 = str4;
						// If boolMort Then
						str4 = str5;
						str5 = "";
					}
					if (Strings.Trim(str1) == string.Empty)
					{
						str1 = str2;
						str2 = str3;
						str3 = str4;
						// If boolMort Then
						str4 = str5;
						str5 = "";
					}
					Array.Resize(ref modReminderNoticeSummary.Statics.arrReminderSummaryList, lngIndex + 1);
					//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
					modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex] = new modReminderNoticeSummary.ReminderSummaryList(0);
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Account = FCConvert.ToInt32(rsTemp.Get_Fields("Account"));
					modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Name1 = str1;
					modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr1 = str2;
					modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr1 = str3;
					modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr1 = str4;
					modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr1 = str5;
					modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Total = 0;
					lngIndex += 1;
					for (intControl = 0; intControl <= Detail.Controls.Count - 1; intControl++)
					{
						if (Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) == "txtData")
						{
							intRow = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Detail.Controls[intControl].Name, 8))));
							switch (intRow)
							{
								case 1:
									{
										// ((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = Left(str1 & String(lngNameLen, " "), lngNameLen)
										((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = str1;
										break;
									}
								case 2:
									{
										((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = str2;
										break;
									}
								case 3:
									{
										((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = str3;
										break;
									}
								case 4:
									{
										((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = str4;
										break;
									}
								case 5:
									{
										((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = str5;
										break;
									}
							}
							//end switch
						}
						else if (Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "              ", 10)) == "txtAccount")
						{
							((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intControl]).Text = lngAcct.ToString();
						}
						Detail.Controls[intControl].Visible = true;
					}
					// intControl
				}
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			bool blnContinue;
			int lngAcct = 0;
			blnContinue = false;
			if (!rsData.EndOfFile())
			{
				// MAL@20080115: Check for minimum amount
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				lngAcct = FCConvert.ToInt32(rsData.Get_Fields("Account"));
				if (modCLCalculations.CalculateAccountTotal(lngAcct, modStatusPayments.Statics.boolRE) < dblMinimumAmount)
				{
					rsData.MoveNext();
					blnContinue = false;
				}
				else
				{
					if (modStatusPayments.Statics.boolRE)
					{
                        if (reminderNoticeOptions.LienedRecords)
                        {
                            rsTemp.OpenRecordset("SELECT " + strFields + " FROM (BillingMaster INNER JOIN LienRec ON BillingMaster.LienrecordNumber = LienRec.ID) INNER JOIN (" + modGlobal.GetMasterJoin( modStatusPayments.Statics.boolRE) + ") mparty ON BillingMaster.Account = mparty.RSAccount WHERE BillingType = 'RE' AND Account = " + rsData.Get_Fields("Account") + strRateKeyList, modExtraModules.strCLDatabase);
                        }
                        else
                        {
                            rsTemp.OpenRecordset("SELECT " + strFields + " FROM BillingMaster INNER JOIN " + modGlobal.Statics.strDbRE + "Master ON BillingMaster.Account = Master.RSAccount WHERE BillingType = 'RE' AND Account = " + rsData.Get_Fields("Account") + strRateKeyList, modExtraModules.strCLDatabase);
                        }
                    }
					else
					{
						rsTemp.OpenRecordset("SELECT " + strFields + " FROM BillingMaster INNER JOIN " + modGlobal.Statics.strDbPP + "PPMaster ON BillingMaster.Account = PPMaster.Account WHERE BillingType = 'PP' AND BillingMaster.Account = " + FCConvert.ToString(rsData.Get_Fields("Account")) + strRateKeyList, modExtraModules.strCLDatabase);
					}
					//Application.DoEvents();
					if (!rsTemp.EndOfFile())
					{
						blnContinue = true;
						if (blnContinue)
						{
							if (Strings.UCase(modCustomReport.Statics.strReportType) == "LABELS")
							{
								PrintLabels();
							}
							else if (Strings.UCase(modCustomReport.Statics.strReportType) == "FORMS")
							{
								PrintForms();
							}
							else if (Strings.UCase(modCustomReport.Statics.strReportType) == "POSTCARDS")
							{
								PrintPostCards();
							}
							else if (Strings.UCase(modCustomReport.Statics.strReportType) == "MAILER")
							{
								PrintMailers();
							}
						}
					}
					rsData.MoveNext();
				}
			}
		}

		private void PrintPostCards()
		{
			// this will fill in the fields for post card
			int intCTRLIndex;
			string str1;
			string str2;
			string str3;
			string str4;
			string str5 = "";
			// get the address info from BillingMaster
			str1 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Name1"))) + " " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Name2")));
			str2 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address1")));
			str3 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address2")));
			str4 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address3")));
			// condense the labels if some are blank
			if (Strings.Trim(str4) == string.Empty)
			{
				str4 = str5;
				str5 = "";
			}
			if (Strings.Trim(str3) == string.Empty)
			{
				str3 = str4;
				str4 = str5;
				str5 = "";
			}
			if (Strings.Trim(str2) == string.Empty)
			{
				str2 = str3;
				str3 = str4;
				str4 = str5;
				str5 = "";
			}
			if (Strings.Trim(str1) == string.Empty)
			{
				str1 = str2;
				str2 = str3;
				str3 = str4;
				str4 = str5;
				str5 = "";
			}
			// Return Address 1
			intCTRLIndex = FindControlIndex_2("txtReturnMailingAddress1");
			if (intCTRLIndex >= 0)
			{
				// row 3
				// ((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intCTRLIndex]).Text = frmReminderNotices.vsGrid.TextMatrix(3, 1)
				((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intCTRLIndex]).Text = reminderNoticeOptions.ReturnAddress1;
            }
			// Return Address 2
			intCTRLIndex = FindControlIndex_2("txtReturnMailingAddress2");
			if (intCTRLIndex >= 0)
			{
				// row 4
				// ((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intCTRLIndex]).Text = frmReminderNotices.vsGrid.TextMatrix(4, 1)
				((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intCTRLIndex]).Text = reminderNoticeOptions.ReturnAddress2;
            }
			// Return Address 3
			intCTRLIndex = FindControlIndex_2("txtReturnMailingAddress3");
			if (intCTRLIndex >= 0)
			{
				// row 5
				// ((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intCTRLIndex]).Text = frmReminderNotices.vsGrid.TextMatrix(5, 1)
				((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intCTRLIndex]).Text = reminderNoticeOptions.ReturnAddress3;
            }
			// Return Address 4
			intCTRLIndex = FindControlIndex_2("txtReturnMailingAddress4");
			if (intCTRLIndex >= 0)
			{
				((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intCTRLIndex]).Text = reminderNoticeOptions.ReturnAddress4;
            }
			// Account Number
			intCTRLIndex = FindControlIndex_2("txtAccount");
			if (intCTRLIndex >= 0)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intCTRLIndex]).Text = "Account: " + FCConvert.ToString(rsTemp.Get_Fields("Account"));
			}
			// Message
			intCTRLIndex = FindControlIndex_2("txtMessage");
			if (intCTRLIndex >= 0)
			{
				((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intCTRLIndex]).Text = reminderNoticeOptions.Message;
            }
			// Name
			intCTRLIndex = FindControlIndex_2("txtName");
			if (intCTRLIndex >= 0)
			{
				((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intCTRLIndex]).Text = str1;
			}
			// Mailing Address 1
			intCTRLIndex = FindControlIndex_2("txtMailingAddress1");
			if (intCTRLIndex >= 0)
			{
				((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intCTRLIndex]).Text = str2;
			}
			// Mailing Address 2
			intCTRLIndex = FindControlIndex_2("txtMailingAddress2");
			if (intCTRLIndex >= 0)
			{
				((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intCTRLIndex]).Text = str3;
			}
			// Mailing Address 3
			intCTRLIndex = FindControlIndex_2("txtMailingAddress3");
			if (intCTRLIndex >= 0)
			{
				((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intCTRLIndex]).Text = str4;
			}
			// Mailing Address 4
			intCTRLIndex = FindControlIndex_2("txtMailingAddress4");
			if (intCTRLIndex >= 0)
			{
				((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intCTRLIndex]).Text = str5;
			}
		}

		private void PrintMailers()
		{
			// this routine will add the data to the fields
			int intCTRLIndex;
			double dblTotalDue = 0;
			double dblXtraInt = 0;
			string str1;
			string str2;
			string str3;
			string str4;
			string str5 = "";
			// address info from BillingMaster
			str1 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Name1"))) + " " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Name2")));
			str2 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address1")));
			str3 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address2")));
			str4 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address3")));
			// condense the labels if some are blank
			if (Strings.Trim(str4) == string.Empty)
			{
				str4 = str5;
				str5 = "";
			}
			if (Strings.Trim(str3) == string.Empty)
			{
				str3 = str4;
				str4 = str5;
				str5 = "";
			}
			if (Strings.Trim(str2) == string.Empty)
			{
				str2 = str3;
				str3 = str4;
				str4 = str5;
				str5 = "";
			}
			if (Strings.Trim(str1) == string.Empty)
			{
				str1 = str2;
				str2 = str3;
				str3 = str4;
				str4 = str5;
				str5 = "";
			}
			// Return Address 1
			intCTRLIndex = FindControlIndex_2("txtReturnMailingAddress1");
			if (intCTRLIndex >= 0)
			{
				// row 3
				((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intCTRLIndex]).Text = reminderNoticeOptions.ReturnAddress1;
            }
			// Return Address 2
			intCTRLIndex = FindControlIndex_2("txtReturnMailingAddress2");
			if (intCTRLIndex >= 0)
			{
				// row 4
				((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intCTRLIndex]).Text = reminderNoticeOptions.ReturnAddress2;
            }
			// Return Address 3
			intCTRLIndex = FindControlIndex_2("txtReturnMailingAddress3");
			if (intCTRLIndex >= 0)
			{
				// row 5
				((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intCTRLIndex]).Text = reminderNoticeOptions.ReturnAddress3;
            }
			// Return Address 4
			intCTRLIndex = FindControlIndex_2("txtReturnMailingAddress4");
			if (intCTRLIndex >= 0)
			{
				// none for now
				((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intCTRLIndex]).Text = "";
			}
			// Account Number
			intCTRLIndex = FindControlIndex_2("txtAccount");
			if (intCTRLIndex >= 0)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intCTRLIndex]).Text = "Account: " + FCConvert.ToString(rsTemp.Get_Fields("Account"));
			}
			// Message
			intCTRLIndex = FindControlIndex_2("txtMessage");
			if (intCTRLIndex >= 0)
			{
				((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intCTRLIndex]).Text = reminderNoticeOptions.Message;
            }
			// Amount Due
			intCTRLIndex = FindControlIndex_2("txtAmountDue");
			if (intCTRLIndex >= 0)
			{
				if (((rsTemp.Get_Fields_Int32("LienRecordNumber"))) != 0)
				{
					rsLData.FindFirstRecord("LienRecordNumber", rsTemp.Get_Fields_Int32("LienRecordNumber"));
					if (rsLData.NoMatch)
					{
						dblTotalDue = 0;
					}
					else
					{
						dblTotalDue = modCLCalculations.CalculateAccountCLLien(rsTemp, DateTime.Today, ref dblXtraInt);
					}
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					dblTotalDue = modCLCalculations.CalculateAccountCL(ref rsTemp, FCConvert.ToInt32(rsTemp.Get_Fields("Account")), DateTime.Today, ref dblXtraInt);
				}
				((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intCTRLIndex]).Text = Strings.Format(dblTotalDue, "#,##0.00");
			}
			// Name
			intCTRLIndex = FindControlIndex_2("txtName");
			if (intCTRLIndex >= 0)
			{
				((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intCTRLIndex]).Text = str1;
			}
			// Mailing Address 1
			intCTRLIndex = FindControlIndex_2("txtMailingAddress1");
			if (intCTRLIndex >= 0)
			{
				((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intCTRLIndex]).Text = str2;
			}
			// Mailing Address 2
			intCTRLIndex = FindControlIndex_2("txtMailingAddress2");
			if (intCTRLIndex >= 0)
			{
				((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intCTRLIndex]).Text = str3;
			}
			// Mailing Address 3
			intCTRLIndex = FindControlIndex_2("txtMailingAddress3");
			if (intCTRLIndex >= 0)
			{
				((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intCTRLIndex]).Text = str4;
			}
			// Mailing Address 4
			intCTRLIndex = FindControlIndex_2("txtMailingAddress4");
			if (intCTRLIndex >= 0)
			{
				((GrapeCity.ActiveReports.SectionReportModel.TextBox)Detail.Controls[intCTRLIndex]).Text = str5;
			}
		}
		// VBto upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		private short FindControlIndex_2(string strName)
		{
			return FindControlIndex(ref strName);
		}

		private short FindControlIndex(ref string strName)
		{
			short FindControlIndex = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intIndex;
				// this function will accept the name of a field and return the index of
				// field in the control collection in order to access its properties
				FindControlIndex = -1;
				for (intIndex = 0; intIndex <= Detail.Controls.Count - 1; intIndex++)
				{
					// Debug.Print Detail.Controls(intIndex).Name
					if (Detail.Controls[intIndex].Name == strName)
					{
						FindControlIndex = FCConvert.ToInt16(intIndex);
						break;
					}
				}
				return FindControlIndex;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Control Index Search");
				FindControlIndex = -1;
			}
			return FindControlIndex;
		}

		private bool CheckAccount()
		{
			bool CheckAccount = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngAcctNumber = 0;
				// this function will check the current account to see if it needs a reminder notice
				if (reminderNoticeOptions.LienedRecords)
				{
					rsRate.FindFirstRecord("ID", rsTemp.Get_Fields_Int32("RateKey"));
					// ("LienRec.RateKey")
				}
				else
				{
					rsRate.FindFirstRecord("ID", rsTemp.Get_Fields_Int32("RateKey"));
				}
				if (rsRate.NoMatch)
				{
					CheckAccount = false;
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					lngAcctNumber = FCConvert.ToInt32(rsTemp.Get_Fields("Account"));
					if (intReminderType == 0)
					{
						// check to see if anything is due
						if (modCLCalculations.CalculateAccountTotal(lngAcctNumber, modStatusPayments.Statics.boolRE) != 0)
						{
							CheckAccount = true;
						}
						else
						{
							CheckAccount = false;
						}
					}
					else
					{
						// VBto upgrade warning: dblCDue As Decimal	OnWrite(short, Decimal)
						decimal dblCDue;
						int x;
						dblCDue = 0;
						for (x = 1; x <= intReminderType; x++)
						{
							// TODO Get_Fields: Field [TaxDue] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [TaxDue] not found!! (maybe it is an alias?)
							dblCDue += FCConvert.ToDecimal(rsTemp.Get_Fields("TaxDue" + FCConvert.ToString(x)));
						}
						// x
						Decimal dTemp = rsTemp.Get_Fields_Decimal("PrincipalPaid");
						if (DeterminePeriodDue(ref rsTemp, ref rsLData, ref dblCDue, DateTime.Today, ref dTemp, ref intReminderType) != 0)
						{
							CheckAccount = true;
						}
						else
						{
							CheckAccount = false;
						}
					}
					// MAL@20080919: Check for existing account number to avoid duplicate labels
					// Tracker Reference: 13831
					if (objDict.ContainsKey(lngAcctNumber))
					{
						CheckAccount = false;
					}
					else if (CheckAccount == false)
					{
						// Don't Add to dictionary if it wouldn't print anyway
						CheckAccount = CheckAccount;
					}
					else if (blnHideDuplicates && IsDuplicateName(FCConvert.ToString(rsTemp.Get_Fields_String("Name1"))))
					{
						// Eliminate Duplicate Names Regardless of Account Number
						objDict.Add(lngAcctNumber, rsTemp.Get_Fields_String("Name1"));
						CheckAccount = false;
					}
					else
					{
						objDict.Add(lngAcctNumber, rsTemp.Get_Fields_String("Name1"));
						CheckAccount = CheckAccount;
					}
				}
				return CheckAccount;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				CheckAccount = false;
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Checking Account");
			}
			return CheckAccount;
		}
		// VBto upgrade warning: intPer As short	OnWriteFCConvert.ToInt32(
		private decimal DeterminePeriodDue(ref clsDRWrapper rsCollection, ref clsDRWrapper rsLien, ref decimal curPrin, DateTime dateTemp, ref decimal curPD, ref int intPer)
		{
			decimal DeterminePeriodDue = 0;
			// VB6 Bad Scope Dim:
			decimal Tax1 = 0;
			decimal Tax2 = 0;
			decimal Tax3 = 0;
			decimal Tax4 = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// Determine Interest Calculation Date
				// VBto upgrade warning: MostRecentInterestDate As DateTime	OnWrite(string, DateTime)
				DateTime[] MostRecentInterestDate = new DateTime[4 + 1];
				// VBto upgrade warning: AppliedDate As DateTime	OnWrite(string, DateTime, bool)
				DateTime AppliedDate;
				// VBto upgrade warning: InterestAmount As Decimal	OnWriteFCConvert.ToInt16(
				double InterestAmount;
				int intUsingStartDate;
				// VBto upgrade warning: curCInt As double	OnWrite(short, Decimal)
				decimal curCInt = 0;
				double curPDTemp = 0;
				clsDRWrapper rsRR = new clsDRWrapper();
				decimal curPerInt;
				bool boolLien = false;
				// VBto upgrade warning: dtStartDate As DateTime	OnWriteFCConvert.ToInt16(
				DateTime[] dtStartDate = new DateTime[4 + 1];
				double dblIntRate/*unused?*/;
				intUsingStartDate = 0;
				InterestAmount = 0;
				if (rsCollection.Get_Fields_Int32("LienRecordNumber") == 0)
				{
					boolLien = false;
					rsRR.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + rsCollection.Get_Fields_Int32("RateKey"));
				}
				else
				{
					boolLien = true;
					rsLien.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + rsCollection.Get_Fields_Int32("LienRecordNumber"));
					if (rsLien.EndOfFile())
					{
						// missing lien record, exit this function
						return DeterminePeriodDue;
					}
					rsRR.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + rsLien.Get_Fields_Int32("RateKey"));
				}
				if (rsRR.EndOfFile())
				{
					// missing rate record
					dtStartDate[1] = DateTime.FromOADate(0);
					dtStartDate[2] = DateTime.FromOADate(0);
					dtStartDate[3] = DateTime.FromOADate(0);
					dtStartDate[4] = DateTime.FromOADate(0);
					return DeterminePeriodDue;
				}
				else
				{
					dtStartDate[1] = (DateTime)rsRR.Get_Fields_DateTime("InterestStartDate1");
					dtStartDate[2] = (DateTime)rsRR.Get_Fields_DateTime("InterestStartDate2");
					dtStartDate[3] = (DateTime)rsRR.Get_Fields_DateTime("InterestStartDate3");
					dtStartDate[4] = (DateTime)rsRR.Get_Fields_DateTime("InterestStartDate4");
					modStatusPayments.Statics.dblInterestRate = modGlobal.Round(Conversion.Val(rsRR.Get_Fields_Double("InterestRate")), 6);
				}
				if (!boolLien)
				{
					if (rsCollection.Get_Fields_DateTime("InterestAppliedThroughDate").ToOADate() != 0)
					{
						AppliedDate = FCConvert.ToDateTime(Strings.Format(rsCollection.Get_Fields_DateTime("InterestAppliedThroughDate"), "MM/dd/yyyy"));
						intUsingStartDate = 0;
						// 0
					}
					else
					{
						AppliedDate = dtStartDate[intPer];
						intUsingStartDate = 0;
					}
				}
				else
				{
					// lien
					if (FCConvert.ToString(rsLien.Get_Fields("InterestAppliedThroughDate")) == string.Empty)
					{
						AppliedDate = dtStartDate[1] = rsRR.Get_Fields_DateTime("InterestStartDate1");
						intUsingStartDate = 0;
					}
					else
					{
						AppliedDate = FCConvert.ToDateTime(Strings.Format(rsLien.Get_Fields_DateTime("InterestAppliedThroughDate"), "MM/dd/yyyy"));
						intUsingStartDate = 0;
					}
				}
				// check to make sure that the applied through date is a valid date
				// and find the most recent int date for each period
				if (intPer == 1)
				{
					if (Information.IsDate(AppliedDate) == true)
					{
						// if the applied through date is after the interestStart date then
						if (DateAndTime.DateValue(FCConvert.ToString(dtStartDate[1])).ToOADate() < DateAndTime.DateValue(FCConvert.ToString(AppliedDate)).ToOADate())
						{
							// use the applied date to calculate from
							MostRecentInterestDate[1] = FCConvert.ToDateTime(Strings.Format(AppliedDate, "MM/dd/yyyy"));
						}
						else
						{
							MostRecentInterestDate[1] = DateAndTime.DateAdd("d", -1, FCConvert.ToDateTime(Strings.Format(dtStartDate[1], "MM/dd/yyyy")));
							intUsingStartDate = 0;
						}
					}
					else
					{
						MostRecentInterestDate[1] = DateAndTime.DateAdd("d", -1, FCConvert.ToDateTime(Strings.Format(dtStartDate[1], "MM/dd/yyyy")));
						intUsingStartDate = 0;
					}
				}
				if (intPer == 2)
				{
					if (Information.IsDate(AppliedDate) == true)
					{
						// if the applied through date is after the interestStart date then
						if (DateAndTime.DateValue(FCConvert.ToString(dtStartDate[2])).ToOADate() < DateAndTime.DateValue(FCConvert.ToString(AppliedDate)).ToOADate())
						{
							// use the applied date to calculate from
							MostRecentInterestDate[2] = FCConvert.ToDateTime(Strings.Format(AppliedDate, "MM/dd/yyyy"));
						}
						else
						{
							MostRecentInterestDate[2] = DateAndTime.DateAdd("d", -1, FCConvert.ToDateTime(Strings.Format(dtStartDate[2], "MM/dd/yyyy")));
							intUsingStartDate = 0;
						}
					}
					else
					{
						MostRecentInterestDate[2] = DateAndTime.DateAdd("d", -1, FCConvert.ToDateTime(Strings.Format(dtStartDate[2], "MM/dd/yyyy")));
						intUsingStartDate = 0;
					}
				}
				if (intPer == 3)
				{
					if (Information.IsDate(AppliedDate) == true)
					{
						// if the applied through date is after the interestStart date then
						if (DateAndTime.DateValue(FCConvert.ToString(dtStartDate[3])).ToOADate() < DateAndTime.DateValue(FCConvert.ToString(AppliedDate)).ToOADate())
						{
							// use the applied date to calculate from
							MostRecentInterestDate[3] = FCConvert.ToDateTime(Strings.Format(AppliedDate, "MM/dd/yyyy"));
						}
						else
						{
							MostRecentInterestDate[3] = DateAndTime.DateAdd("d", -1, FCConvert.ToDateTime(Strings.Format(dtStartDate[3], "MM/dd/yyyy")));
							intUsingStartDate = 0;
						}
					}
					else
					{
						MostRecentInterestDate[3] = DateAndTime.DateAdd("d", -1, FCConvert.ToDateTime(Strings.Format(dtStartDate[3], "MM/dd/yyyy")));
						intUsingStartDate = 0;
					}
				}
				if (intPer == 4)
				{
					if (Information.IsDate(AppliedDate) == true)
					{
						// if the applied through date is after the interestStart date then
						if (DateAndTime.DateValue(FCConvert.ToString(dtStartDate[4])).ToOADate() < DateAndTime.DateValue(FCConvert.ToString(AppliedDate)).ToOADate())
						{
							// use the applied date to calculate from
							MostRecentInterestDate[4] = FCConvert.ToDateTime(Strings.Format(AppliedDate, "MM/dd/yyyy"));
						}
						else
						{
							MostRecentInterestDate[4] = DateAndTime.DateAdd("d", -1, FCConvert.ToDateTime(Strings.Format(dtStartDate[4], "MM/dd/yyyy")));
							intUsingStartDate = 0;
						}
					}
					else
					{
						MostRecentInterestDate[4] = DateAndTime.DateAdd("d", -1, FCConvert.ToDateTime(Strings.Format(dtStartDate[4], "MM/dd/yyyy")));
						intUsingStartDate = 0;
					}
				}
				if (!boolLien)
				{

					if (intPer == 1)
						Tax1 = curPrin - curPD;
					if (intPer == 2)
						Tax2 = curPrin - curPD;
					if (intPer == 3)
						Tax3 = curPrin - curPD;
					if (intPer == 4)
						Tax4 = curPrin - curPD;
				}
				else
				{
					// MAL@20080331: Tracker 12956
					// Tax1 = curPrin
					Tax1 = curPrin - curPD;
					// If Tax1 > 0 Then Stop
				}
				double temp = 0;
				if (boolLien)
				{
					if (Information.IsDate(MostRecentInterestDate[1]) == true)
					{
						if (MostRecentInterestDate[1].ToOADate() >= dateTemp.ToOADate())
						{
							curCInt = 0;
						}
						else
						{
							curCInt = modCLCalculations.CalculateInterest(Tax1, MostRecentInterestDate[1], dateTemp, modStatusPayments.Statics.dblInterestRate, FCConvert.ToInt16(modGlobal.Statics.gintBasis), FCConvert.ToInt16(intUsingStartDate), modExtraModules.Statics.dblOverPayRate, ref temp);
							curPD = FCConvert.ToDecimal(temp);
							// curCInt = curCInt + InterestAmount
						}
					}
				}
				else
				{
					decimal PeriodInterest;
					if (intPer == 1)
					{
						// Period 1
						curPDTemp = 0;
						PeriodInterest = modCLCalculations.CalculateInterest(Tax1, MostRecentInterestDate[1], dateTemp, modStatusPayments.Statics.dblInterestRate, FCConvert.ToInt16(modGlobal.Statics.gintBasis), FCConvert.ToInt16(intUsingStartDate), modExtraModules.Statics.dblOverPayRate, ref curPDTemp);
						curCInt += PeriodInterest;
					}
					if (intPer == 2)
					{
						// Period 2
						curPDTemp = 0;
						PeriodInterest = modCLCalculations.CalculateInterest(Tax2, MostRecentInterestDate[2], dateTemp, modStatusPayments.Statics.dblInterestRate, modGlobal.Statics.gintBasis, intUsingStartDate, modExtraModules.Statics.dblOverPayRate, ref curPDTemp);
						curCInt += PeriodInterest;
					}
					if (intPer == 3)
					{
						// Period 3
						curPDTemp = 0;
						PeriodInterest = modCLCalculations.CalculateInterest(Tax3, MostRecentInterestDate[3], dateTemp, modStatusPayments.Statics.dblInterestRate, modGlobal.Statics.gintBasis, intUsingStartDate, modExtraModules.Statics.dblOverPayRate, ref curPDTemp);
						curCInt += PeriodInterest;
					}
					if (intPer == 4)
					{
						// Period 4
						curPDTemp = 0;
						PeriodInterest = modCLCalculations.CalculateInterest(Tax4, MostRecentInterestDate[4], dateTemp, modStatusPayments.Statics.dblInterestRate, modGlobal.Statics.gintBasis, intUsingStartDate, modExtraModules.Statics.dblOverPayRate, ref curPDTemp);
						curCInt += PeriodInterest;
					}
				}
				DeterminePeriodDue = Tax1 + Tax2 + Tax3 + Tax4 + curCInt;
				rsRR.Dispose();
				return DeterminePeriodDue;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In Period Interest");
			}
			return DeterminePeriodDue;
		}

		private bool IsDuplicateName(string strName)
		{
			bool IsDuplicateName = false;
			// Tracker Reference: 13831
			bool blnMatch = false;
			foreach (object varKey in objDict.Keys)
			{
				if (objDict[varKey] == strName)
				{
					blnMatch = true;
					break;
				}
			}
			// varKey
			IsDuplicateName = blnMatch;
			return IsDuplicateName;
		}

		
	}
}
