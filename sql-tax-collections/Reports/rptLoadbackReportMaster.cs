﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptLoadbackReportMaster : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               11/24/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               07/12/2004              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		int lngNumOfAccts;
		bool boolFirstPass;

		public rptLoadbackReportMaster()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Loadback Report";
		}

		public static rptLoadbackReportMaster InstancePtr
		{
			get
			{
				return (rptLoadbackReportMaster)Sys.GetInstance(typeof(rptLoadbackReportMaster));
			}
		}

		protected rptLoadbackReportMaster _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (boolFirstPass)
			{
				boolFirstPass = false;
			}
			else
			{
				eArgs.EOF = !boolFirstPass;
			}
			//Detail_Format();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			// catch the esc key and unload the report
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			boolFirstPass = true;
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			if (modStatusPayments.Statics.boolRE)
			{
				// kgk trocl-658 03-25-11
				lblHeader.Text = lblHeader.Text + " - RE Collections";
			}
			else
			{
				lblHeader.Text = lblHeader.Text + " - PP Collections";
			}
			rsData.OpenRecordset("SELECT * FROM LoadBackOriginal ORDER BY Account", modExtraModules.strCLDatabase);
			if (rsData.EndOfFile())
			{
				MessageBox.Show("There are no accounts that have been loaded back.", "No Accounts Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
				Cancel();
				this.Close();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (!rsData.EndOfFile())
				{
					// this should set the two sub reports
					srptNonLienOb.Report = new srptLoadbackReport();
					srptNonLienOb.Report.UserData = "N";
					srptLienOb.Report = new srptLoadbackReport();
					srptLienOb.Report.UserData = "L";
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Detail Format Error");
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		
	}
}
