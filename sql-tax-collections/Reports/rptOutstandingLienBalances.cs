using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.Enums;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptOutstandingLienBalances : BaseSectionReport
	{
        private TaxCollectionStatusReportConfiguration reportConfiguration;
        private rptOutstandingBalancesAll parentReport;
        string strSQL;
		clsDRWrapper rsData = new clsDRWrapper();
		double[] dblTotals = new double[6 + 1];
		bool boolRTError;
		int lngCount;
		bool boolStarted;
		GrapeCity.ActiveReports.SectionReportModel.TextBox obNew1;
		GrapeCity.ActiveReports.SectionReportModel.TextBox obNew2;
		GrapeCity.ActiveReports.SectionReportModel.Label obLabel;
		GrapeCity.ActiveReports.SectionReportModel.Line obLine;
		int intSumRows;
		double dblPDTotal;
		bool boolAdjustedSummary;
		bool boolSummaryOnly;
		bool boolDeletedAccount;
		int lngTAType;

        private int intSuppReportType;
		// 0 = all, 1 = TA, 2 = non TA
		clsDRWrapper rsMaster = new clsDRWrapper();
		Dictionary<object, object> dctAccounts = new Dictionary<object, object>();
		// kk01132014 trocls-13
		// these are for the summaries at the bottom of the report
		double[] dblYearTotals = new double[2000 + 1];
		// billingyear - 19800
		int[] lngArrBillCounts = new int[2000 + 1];
		// kk01072015 trocl-13
		// kk11262014 trocl-682   Dim dblPayments(10, 5)          As Double   '0 - 3, 1 - A, 2 - C, 3 - D, 4 - I, 5 - L, 6 - P, 7 - U, 8 - X, 9 - Y, 10 - Total
		// 0 - Principal, 1 - PreLienInterest, 2 - CurrentInterest, 3 - Costs
		clsPmtTypeSummaries tPayTypeSummaries = new clsPmtTypeSummaries();

		public rptOutstandingLienBalances()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//if (_InstancePtr == null)
			//	_InstancePtr = this;
			this.Name = "Non Zero Balance on Lien Accounts";
		}

        public void SetReportOptionAndParent(TaxCollectionStatusReportConfiguration reportConfig, rptOutstandingBalancesAll parent,int suppReportType)
        {
            reportConfiguration = reportConfig;
            parentReport = parent;
            intSuppReportType = suppReportType;
        }
  //      public static rptOutstandingLienBalances InstancePtr
		//{
		//	get
		//	{
		//		return (rptOutstandingLienBalances)Sys.GetInstance(typeof(rptOutstandingLienBalances));
		//	}
		//}

		//protected rptOutstandingLienBalances _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			//if (_InstancePtr == this)
			//{
			//	_InstancePtr = null;
			//	Sys.ClearInstance(this);
			//}
            if (disposing)
            {
				rsMaster.Dispose();
				rsData.Dispose();
            }
			base.Dispose(disposing);
        }

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsData.EndOfFile())
			{
				if (boolStarted)
				{
					eArgs.EOF = true;
				}
				else
				{
				}
			}
			else
			{
				eArgs.EOF = false;
			}
			//Detail_Format();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_PageEnd(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			tPayTypeSummaries = null;
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "t");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			lngCount = 0;
			boolSummaryOnly = FCConvert.CBool(reportConfiguration.Options.ShowSummaryOnly);

            if (reportConfiguration.Filter.IsTaxAcquired.HasValue)
            {
                if (reportConfiguration.Filter.IsTaxAcquired.Value)
                {
                    lngTAType = 1;
                }
                else
                {
                    lngTAType = 2;
                }
            }
            else
            {
                lngTAType = 0;
            }
            frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading");
			boolRTError = false;
			SetupFields();		
			SetReportHeader();
			
			boolStarted = false;
			strSQL = BuildSQL();
			
			if (lngTAType > 0)
			{
				// kk01072015 trocl-22
				rsMaster.OpenRecordset("SELECT RSAccount, TaxAcquired FROM Master Where TaxAcquired = 1", modExtraModules.strREDatabase);
			}
			rsData.OpenRecordset(strSQL, modGlobal.DEFAULTDATABASE);
			//FC:FINAL:AM:#i101 - moved code from SetupTotals
			ReportFooter.Controls.Add(obNew1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox());
			// kk01082015 trocls-13   add a field for the total count
			ReportFooter.Controls.Add(obNew2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox());
			// add a label
			ReportFooter.Controls.Add(obLabel = new GrapeCity.ActiveReports.SectionReportModel.Label());
			// add a line
			ReportFooter.Controls.Add(obLine = new GrapeCity.ActiveReports.SectionReportModel.Line());
			for (int i = 2; i <= 100; i++)
			{
				AddSummaryRow(i, 0, 0, 0, true);
			}
		}

		private string BuildSQL()
		{
			string BuildSQL = "";
			string strTemp = "";
			string strWhereClause;
			string strREPPBill = "";
			string strREPPPayment = "";
			int intCT;
			string strSupp = "";
			string strFields;
			if (reportConfiguration.BillingType == PropertyTaxBillType.Real)
			{
				// only get the accounts for the correct module
				strREPPBill = "'RE'";
			}
			else
			{
				strREPPBill = "'PP'";
			}
			if (intSuppReportType == 1)
			{
				strSupp = " AND BillingYear % 10 > 1 ";
			}
			strWhereClause = "WHERE BillingType = " + strREPPBill + strSupp;

            var strAnd = "";
            var filter = reportConfiguration.Filter;
            if (filter.AccountRangeUsed())
            {
                if (filter.AccountMax.HasValue)
                {
                    if (filter.AccountMin.HasValue)
                    {
                        strTemp += "Account between " + filter.AccountMin.Value.ToString() + " and " +
                                   filter.AccountMax.Value.ToString();
                    }
                    else
                    {
                        strTemp += "Account <= " + filter.AccountMax.Value.ToString();
                    }
                }
                else
                {
                    strTemp += "Account >= " + filter.AccountMin.Value.ToString();
                }

                strAnd = " and ";
            }

            if (filter.NameRangeUsed())
            {
                strTemp += strAnd;

                if (!String.IsNullOrWhiteSpace(filter.NameMin))
                {
                    if (!String.IsNullOrWhiteSpace(filter.NameMax))
                    {
                        strTemp += "Name1 between '" + filter.NameMin + "' and '" + filter.NameMax + "zzzz'";
                    }
                    else
                    {
                        strTemp += "Name1 >= '" + filter.NameMin + "'";
                    }
                }
                else
                {
                    strTemp += "Name1 <= '" + filter.NameMax + "zzzz'";
                }

                strAnd = " and ";
            }

            if (filter.TaxYearRangeUsed())
            {
                strTemp += strAnd;

                if (filter.TaxYearMin.HasValue)
                {
                    if (filter.TaxYearMax.HasValue)
                    {
                        strTemp += "BillingYear between " + filter.TaxYearMin.Value.ToString() + " and " +
                                   filter.TaxYearMax.Value.ToString();
                    }
                    else
                    {
                        strTemp += "BillingYear = " + filter.TaxYearMin.Value.ToString();
                    }
                }
                else
                {
                    strTemp += "BillingYear = " + filter.TaxYearMax.Value.ToString();
                }

                strAnd = " and ";
            }

            if (filter.RateRecordRangeUsed())
            {
                strTemp += strAnd;

                if (filter.RateRecordMin.HasValue)
                {
                    if (filter.RateRecordMax.HasValue)
                    {
                        strTemp += "RateKey between " + filter.RateRecordMin.Value.ToString() + " and " +
                                   filter.RateRecordMax.Value.ToString();
                    }
                    else
                    {
                        strTemp += "RateKey >= " + filter.RateRecordMin.Value.ToString();
                    }
                }
                else
                {
                    strTemp += "RateKey <= " + filter.RateRecordMax.Value.ToString();
                }

                strAnd = " and ";
            }

			if (Strings.Trim(strTemp) != "")
			{
				strWhereClause += " AND " + strTemp;
			}
			strFields = "Account, TransferFromBillingDateFirst, BillingYear, Name1, Name2, BillingMaster.RateKey AS BRateKey, LienRec.InterestCharged AS InterestCharged, LienRec.RateKey AS RateKey, LienRec.InterestAppliedThroughDate AS InterestAppliedThroughDate, Principal, Interest, Costs, LienRec.PrincipalPaid AS PrincipalPaid, LienRec.InterestPaid AS InterestPaid, PLIPaid, MaturityFee, CostsPaid, TransferFromBillingDateLast, LienRec.ID AS LRN ";
			if (reportConfiguration.Options.DefaultReportType != TaxBillingHardCodedReport.NonZeroBalanceOnAllAccountsByYear)
			{
				if (reportConfiguration.Options.UseAsOfDate())
				{
					// MAL@20080624 ; Tracker Reference: 14306
					strWhereClause += " AND ISNULL(DateCreated, '12/30/1899') <= '" + FCConvert.ToString(reportConfiguration.Options.AsOfDate) + "'";
					strTemp = "SELECT " + strFields + " FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.ID " + strWhereClause + " ORDER BY Name1, Account, BillingYear";
				}
				else
				{
					strTemp = "SELECT * FROM (" + GetOSLienBalQuery() + ") AS qOSLnBal " + strWhereClause + " ORDER BY Name1, Account, BillingYear";
				}
			}
			else
			{
				if (reportConfiguration.Options.UseAsOfDate())
				{
					strWhereClause += " AND ISNULL(DateCreated, '12/30/1899') <= '" + FCConvert.ToString(reportConfiguration.Options.AsOfDate) + "'";
					strTemp = "SELECT " + strFields + " FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.ID " + strWhereClause + " ORDER BY BillingYear, Name1, Account";
				}
				else
				{
					strTemp = "SELECT * FROM (" + GetOSLienBalQuery() + ") AS qOSLnBal " + strWhereClause + " ORDER BY BillingYear, Name1, Account";
				}
			}
			BuildSQL = strTemp;
			return BuildSQL;
		}

		private void SetupFields()
		{
			// Set each field and label's visible property to True/False depending on which fields the user
			// has selected from the Custom Report screen then move them accordingly
			int intRow;
			int intRow2/*unused?*/;
			float lngHt;
			if (boolSummaryOnly)
			{
				lblYear.Visible = false;
				fldYear.Visible = false;
				fldName.Visible = false;
				fldAccount.Visible = false;
				fldPaymentReceived.Visible = false;
				fldTaxDue.Visible = false;
				fldDue.Visible = false;
				lnHeader.Visible = false;
				lnTotals.Visible = false;
				fldType.Visible = false;
				Detail.Height = 0;
				return;
			}
			intRow = 2;
			lngHt = 270 / 1440F;
			lblYear.Visible = true;
			fldYear.Visible = true;
			// if the year is not shown, then make the name field smaller
			fldName.Width = fldYear.Left - (fldType.Left + fldType.Width);
		}

		private void BindFields()
		{
			var rsPayment = new clsDRWrapper();
            clsDRWrapper rsCalLien = null;
			var rsRate = new clsDRWrapper();
            try
            {

                string strTemp = "";
                DateTime dtPaymentDate;
                double dblIntPaid;
                double dblCostPaid;
                double dblPaymentRecieved;
                double dblXtraInt = 0;
                double dblTotalDue;
                double dblPreLienIntNeed;
                double dblLienCostNeed;
                TRYAGAIN: ;
                dblPaymentRecieved = 0;
                dblIntPaid = 0;
                dblCostPaid = 0;
                fldDue.Text = "";
                fldTaxDue.Text = "";
                fldPaymentReceived.Text = "";
                fldAccount.Text = "";
                fldYear.Text = "";
                fldName.Text = "";
                fldType.Text = "";
                if (rsData.EndOfFile())
                {
                    return;
                }

                if (reportConfiguration.Filter.TranCodeMax.HasValue)
                {
                    if (!modCLCalculations.CheckTranCode_78(reportConfiguration.BillingType == PropertyTaxBillType.Real,
                        FCConvert.ToInt32(rsData.Get_Fields("Account")), reportConfiguration.Filter.TranCodeMin ?? 0,
                        reportConfiguration.Filter.TranCodeMax ?? 0))
                    {
                        rsData.MoveNext();
                        goto TRYAGAIN;
                    }
                }

                if (lngTAType > 0)
                {
                    switch (lngTAType)
                    {

                        case 1:
                        {
                            // TA
                            // if the account is in the recordset then it is TA
                            rsMaster.FindFirstRecord("RSAccount", rsData.Get_Fields("Account"));
                            if (rsMaster.NoMatch)
                            {
                                // If Not rsMaster.Fields("TaxAcquired") Then
                                rsData.MoveNext();
                                goto TRYAGAIN;
                            }

                            break;
                        }
                        case 2:
                        {
                            // Non TA
                            // if the account is NOT in the recordset then it is TA
                            rsMaster.FindFirstRecord("RSAccount", rsData.Get_Fields("Account"));
                            if (!rsMaster.NoMatch)
                            {
                                // If rsMaster.Fields("TaxAcquired") Then
                                rsData.MoveNext();
                                goto TRYAGAIN;
                            }

                            break;
                        }
                    } //end switch

                    // End If
                }

                lngCount += 1;
                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                fldAccount.Text = FCConvert.ToString(rsData.Get_Fields("Account"));
                fldYear.Text =
                    FCConvert.ToString(FCUtils.iDiv(FCConvert.ToInt32(rsData.Get_Fields_Int32("BillingYear")), 10));
                fldName.Text = rsData.Get_Fields_String("Name1");
                // GetStatusName(rsData)   'rsData.Fields("Name1")
                if (reportConfiguration.Options.UseAsOfDate())
                {
                    // get the values from the payment records
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Field [LRN] not found!! (maybe it is an alias?)
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Field [LRN] not found!! (maybe it is an alias?)
                    rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE Account = " +
                                            FCConvert.ToString(rsData.Get_Fields("Account")) + " AND [Year] = " +
                                            FCConvert.ToString(rsData.Get_Fields_Int32("BillingYear")) +
                                            " AND BillCode = 'L' AND BillKey = " +
                                            FCConvert.ToString(rsData.Get_Fields("LRN")) +
                                            " AND ISNULL(RecordedTransactionDate, '12/30/1899') <= '" +
                                            FCConvert.ToString(reportConfiguration.Options.AsOfDate) + "'");
                }
                else
                {
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Field [LRN] not found!! (maybe it is an alias?)
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Field [LRN] not found!! (maybe it is an alias?)
                    rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE Account = " +
                                            FCConvert.ToString(rsData.Get_Fields("Account")) + " AND [Year] = " +
                                            FCConvert.ToString(rsData.Get_Fields_Int32("BillingYear")) +
                                            " AND BillCode = 'L' AND BillKey = " +
                                            FCConvert.ToString(rsData.Get_Fields("LRN")));
                }

                // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                dblPreLienIntNeed = Conversion.Val(rsData.Get_Fields("Interest"));
                // TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
                dblLienCostNeed = Conversion.Val(rsData.Get_Fields("Costs"));
                while (!rsPayment.EndOfFile())
                {
                    // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                    dblPaymentRecieved += Conversion.Val(rsPayment.Get_Fields("Principal"));

                    if (reportConfiguration.Options.FullStatusAmounts)
                    {
                        dblCostPaid += Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost"));
                        if (reportConfiguration.Options.ShowCurrentInterest)
                        {
                            dblIntPaid += Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")) +
                                          Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest"));
                        }
                        else
                        {
                            dblIntPaid += Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest"));
                        }
                    }
                    else
                    {
                        // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                        if (!reportConfiguration.Options.FullStatusAmounts &&
                            ((rsPayment.Get_Fields("Principal"))) == 0 &&
                            ((rsPayment.Get_Fields_Decimal("LienCost"))) == 0 &&
                            rsPayment.Get_Fields_Decimal("CurrentInterest") < 0)
                        {
                            // do not even look at this payment
                        }
                        else
                        {
                            if (((rsPayment.Get_Fields_Decimal("PreLienInterest"))) != 0)
                            {
                                // rsPayment.Fields("CurrentInterest") +
                                dblIntPaid += Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest"));
                                // rsPayment.Fields("CurrentInterest") +
                            }
                        }

                        // If rsPayment.Fields("LienCost") > 0 Then
                        dblCostPaid += Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost"));
                        // End If
                    }

                    if (reportConfiguration.Options.FullStatusAmounts)
                    {
                        if (reportConfiguration.Options.ShowCurrentInterest)
                        {
                            // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            tPayTypeSummaries.AddPaymentByType(
                                Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))),
                                Conversion.Val(rsPayment.Get_Fields("Principal")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            tPayTypeSummaries.AddPaymentByType(
                                Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))),
                                Conversion.Val(rsPayment.Get_Fields("Principal")), 0.0,
                                Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
                        }

                    }
                    else
                    {
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                        tPayTypeSummaries.AddPaymentByType(
                            Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))),
                            Conversion.Val(rsPayment.Get_Fields("Principal")), 0.0,
                            Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                            Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));

                    }

                    rsPayment.MoveNext();
                }

                if (reportConfiguration.Options.FullStatusAmounts)
                {
                    // this will show all payments even if int or costs paid are more than owed
                    // if the current interest is checked then calculate it and display it
                    dblXtraInt = 0;
                    if (reportConfiguration.Options.ShowCurrentInterest)
                    {
                        dblTotalDue = modCLCalculations.CalculateAccountCLLien(rsData,
                            reportConfiguration.Options.AsOfDate, ref dblXtraInt);
                        tPayTypeSummaries.AddPaymentByType("Z", 0.0, dblXtraInt * -1, 0.0, 0.0);
                    }
                    else
                    {
                        dblXtraInt = 0;
                    }

                    fldPaymentReceived.Text = Strings.Format(dblPaymentRecieved + dblIntPaid + dblCostPaid - dblXtraInt,
                        "#,##0.00");
                }
                else
                {
                    // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                    if (dblIntPaid > Conversion.Val(rsData.Get_Fields("Interest")))
                    {
                        // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                        dblIntPaid = Conversion.Val(rsData.Get_Fields("Interest"));
                    }
                    else
                    {
                        // dblIntPaid = rsData.Fields("LienRec.InterestPaid")
                    }

                    // End If
                    // TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
                    if (Conversion.Val(rsData.Get_Fields_Decimal("CostsPaid")) >
                        Conversion.Val(rsData.Get_Fields("Costs")))
                    {
                        // TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
                        dblCostPaid = Conversion.Val(rsData.Get_Fields("Costs"));
                    }

                    fldPaymentReceived.Text = Strings.Format(dblPaymentRecieved + dblIntPaid + dblCostPaid, "#,##0.00");

                }

                // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
                fldTaxDue.Text =
                    Strings.Format(
                        FCConvert.ToDouble(Conversion.Val(rsData.Get_Fields("Principal")) +
                                           Conversion.Val(rsData.Get_Fields("Interest")) +
                                           Conversion.Val(rsData.Get_Fields("Costs"))), "#,##0.00");
                fldDue.Text =
                    Strings.Format(FCConvert.ToDouble(fldTaxDue.Text) - FCConvert.ToDouble(fldPaymentReceived.Text),
                        "#,##0.00");
                fldType.Text = "L";
                if (intSuppReportType == 1)
                {
                    // TODO Get_Fields: Field [BRateKey] not found!! (maybe it is an alias?)
                    // TODO Get_Fields: Field [BRateKey] not found!! (maybe it is an alias?)
                    rsRate.OpenRecordset(
                        "SELECT * FROM RateRec WHERE RateKey = " + FCConvert.ToString(rsData.Get_Fields("BRateKey")),
                        modExtraModules.strCLDatabase);
                    if (!rsRate.EndOfFile())
                    {
                        if (FCConvert.ToString(rsRate.Get_Fields_String("RateType")) != "S")
                        {
                            tPayTypeSummaries.DiscardAllPayments();
                            // kk04082018 trocl-682
                            lngCount -= 1;
                            rsData.MoveNext();
                            goto TRYAGAIN;
                        }
                    }
                    else
                    {
                        tPayTypeSummaries.DiscardAllPayments();
                        // kk04082018 trocl-682
                        lngCount -= 1;
                        rsData.MoveNext();
                        goto TRYAGAIN;
                    }
                }

                if (FCConvert.ToDouble(fldDue.Text) == 0)
                {
                    tPayTypeSummaries.DiscardAllPayments();

                    rsData.MoveNext();
                    lngCount -= 1;
                    goto TRYAGAIN;

                }
                else
                {
                    tPayTypeSummaries.SaveAllPayments();

                }

                if (parentReport != null)
                {
                    parentReport.dblTotalsPrin += FCConvert.ToDouble(fldTaxDue.Text);
                    parentReport.dblTotalsPay += FCConvert.ToDouble(fldPaymentReceived.Text);
                    parentReport.lngCount += 1;
                }

                dblTotals[0] += FCConvert.ToDouble(fldTaxDue.Text);
                dblTotals[1] += FCConvert.ToDouble(fldPaymentReceived.Text);
                dblTotals[3] += FCConvert.ToDouble(fldDue.Text);
                // keep track for the year totals
                dblYearTotals[rsData.Get_Fields_Int32("BillingYear") - 19800] =
                    dblYearTotals[FCConvert.ToInt16(rsData.Get_Fields_Int32("BillingYear")) - 19800] +
                    FCConvert.ToDouble(fldDue.Text);
                // kk0107205 trocls-13  Keep count of accounts and Bills by Year
                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                if (!dctAccounts.ContainsKey(FCConvert.ToInt32(Conversion.Val(rsData.Get_Fields("Account")))))
                {
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    dctAccounts.Add(FCConvert.ToInt32(Conversion.Val(rsData.Get_Fields("Account"))),
                        rsData.Get_Fields_String("Name1"));
                }
                else
                {
                    // Count duplicats account number with different name??
                }

                lngArrBillCounts[rsData.Get_Fields_Int32("BillingYear") - 19800] =
                    lngArrBillCounts[FCConvert.ToInt16(rsData.Get_Fields_Int32("BillingYear")) - 19800] + 1;
                // move to the next record in the query
                rsData.MoveNext();
                return;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                FCMessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message +
                    ".", MsgBoxStyle.Critical, "Error In BindFields");
            }
            finally
            {
                rsPayment.Dispose();
				rsRate.Dispose();
				rsCalLien?.Dispose();
            }
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void SetupTotals()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strSUM = "";
				// rsSum = new clsDRWrapper();
				int intCT;
				// this sub will fill in the totals line at the bottom of the report
				fldTotalTaxDue.Text = Strings.Format(dblTotals[0], "#,##0.00");
				fldTotalPaymentReceived.Text = Strings.Format(dblTotals[1], "#,##0.00");
				fldTotalDue.Text = Strings.Format(dblTotals[3], "#,##0.00");
				// kk01082015 trocls-13  Add count of accounts
				if (dctAccounts.Count > 1)
				{
					fldAcctCount.Text = FCConvert.ToString(dctAccounts.Count) + " Accounts";
				}
				else if (dctAccounts.Count == 1)
				{
					fldAcctCount.Text = FCConvert.ToString(dctAccounts.Count) + " Account";
				}
				else
				{
					fldAcctCount.Text = "";
				}
				if (lngCount > 1)
				{
					lblTotals.Text = "Total for " + FCConvert.ToString(lngCount) + " Bills:";
					// kk01082015 trocls-13  The count is bills, not accounts       & " Accounts:"
				}
				else if (lngCount == 1)
				{
					lblTotals.Text = "Total for " + FCConvert.ToString(lngCount) + " Bill:";
					// & " Account:"
				}
				else
				{
					lblTotals.Text = "No Bills";
				}
				// this will setup the payment summary
				SetupTotalSummary();
				// Load Summary List
				intSumRows = 1;
				for (intCT = 0; intCT <= Information.UBound(dblYearTotals, 1) - 1; intCT++)
				{
					if (dblYearTotals[intCT] != 0)
					{
						AddSummaryRow(intSumRows, dblYearTotals[intCT], lngArrBillCounts[intCT], intCT + 19800, false);
						intSumRows += 1;
					}
				}
				//FC:FINAL:DDU:#i130 moved code from ReportStart
				obNew1.Name = "fldSummaryTotal";
				obNew1.Top = fldSummary1.Top + ((intSumRows - 1) * fldSummary1.Height);
				obNew1.Left = fldSummary1.Left;
				obNew1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				obNew1.Width = fldSummary1.Width;
				obNew1.Font = lblSummary1.Font;
				obNew1.Text = Strings.Format(dblPDTotal, "#,##0.00");
				obNew2.Name = "fldSumCountTotal";
				obNew2.Top = fldSumCount1.Top + ((intSumRows - 1) * fldSumCount1.Height);
				obNew2.Left = fldSumCount1.Left;
				obNew2.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				obNew2.Width = fldSumCount1.Width;
				obNew2.Font = fldSumCount1.Font;
				obNew2.Text = lngCount.ToString();
				obLabel.Name = "lblPerDiemTotal";
				obLabel.Top = lblSummary1.Top + ((intSumRows - 1) * lblSummary1.Height);
				obLabel.Left = lblSummary1.Left;
				obLabel.Font = lblSummary1.Font;
				obLabel.Text = "Total";
				obLine.Name = "lnFooterSummaryTotal";
				obLine.X1 = fldSumCount1.Left;
				// kk01082015 trocls-13     fldSummary1.Left
				obLine.X2 = fldSummary1.Left + fldSummary1.Width;
				obLine.Y1 = lblSummary1.Top + ((intSumRows - 1) * lblSummary1.Height);
				obLine.Y2 = obLine.Y1;
				ReportFooter.Height = lblSummary1.Top + (intSumRows * lblSummary1.Height);
				// Load Summary List
				// strSUM = "SELECT BillingYear AS Year, SUM(Principal + Costs + Interest - LienRec.PrincipalPaid - MaturityFee - LienRec.InterestPaid - LienRec.InterestCharged - CostsPaid) AS Due FROM (" & strSQL & ") GROUP BY BillingYear"
				// 
				// intSumRows = 1
				// 
				// rsSum.OpenRecordset strSUM, strCLDatabase
				// If rsSum.EndOfFile Then
				// intSumRows = 1
				// Else
				// Do Until rsSum.EndOfFile
				// AddSummaryRow intSumRows, rsSum.Fields("Due"), rsSum.Fields("Year")
				// intSumRows = intSumRows + 1
				// rsSum.MoveNext
				// Loop
				// End If
				// create the total fields and fill them
				// add a field				
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Lien Summary Creation");
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			SetupTotals();
		}

		private void SetReportHeader()
		{
			int intCT;
			string strTemp = "";
            var strSeparator = "";
            var filter = reportConfiguration.Filter;
            if (filter.AccountRangeUsed())
            {
                if (filter.AccountMax.HasValue)
                {
                    if (filter.AccountMin.HasValue)
                    {
                        if (filter.AccountMin == filter.AccountMax)
                        {
                            strTemp += "Account: " + filter.AccountMin.Value.ToString();
                        }
                        else
                        {
                            strTemp += "Account: " + filter.AccountMin.Value.ToString() + " To " +
                                       filter.AccountMax.Value.ToString();
                        }
                    }
                    else
                    {
                        strTemp += "Below Account: " + filter.AccountMax.Value.ToString();
                    }
                }
                else
                {
                    strTemp += "Above Account: " + filter.AccountMin.Value.ToString();
                }

                strSeparator = ", ";
            }

            if (filter.TaxYearRangeUsed())
            {
                strTemp += strSeparator;
                if (filter.TaxYearMin.HasValue)
                {
                    if (filter.TaxYearMax.HasValue)
                    {
                        strTemp += "Tax Year: " + filter.TaxYearMin.Value.ToString() + " To " +
                                   filter.TaxYearMax.Value.ToString();
                    }
                    else
                    {
						strTemp += "Tax Year: " + filter.TaxYearMin.Value.ToString();
					}
                }
                else
                {
                    strTemp += "Tax Year: " + filter.TaxYearMax.Value.ToString();
                }

                strSeparator = ", ";
            }

			if (lngTAType > 0)
			{
				if (Strings.Trim(strTemp) == "")
				{
					if (lngTAType == 1)
					{
						strTemp = "Tax Acquired";
					}
					else
					{
						strTemp = "Non-Tax Acquired";
					}
				}
				else
				{
					if (lngTAType == 1)
					{
						strTemp = ", Tax Acquired";
					}
					else
					{
						strTemp = ", Non-Tax Acquired";
					}
				}
			}

			if (reportConfiguration.Options.ShowCurrentInterest)
			{
				if (Strings.Trim(strTemp) == "")
				{
					strTemp += "Show Interest";
				}
				else
				{
					strTemp += ", Show Interest";
				}
			}
			if (Strings.Trim(strTemp) == "")
			{
				lblReportType.Text = "Complete List";
			}
			else
			{
				lblReportType.Text = strTemp;
			}
			if (reportConfiguration.Options.UseAsOfDate())
			{
				lblReportType.Text = lblReportType.Text + "\r\n" + "As of: " + Strings.Format(reportConfiguration.Options.AsOfDate, "MM/dd/yyyy");
			}
		}
		private void AddSummaryRow(int intRNum, double dblAmount, int lngLnCnt, int lngYear, bool createObjects)
		{
			GrapeCity.ActiveReports.SectionReportModel.TextBox obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			GrapeCity.ActiveReports.SectionReportModel.Label obLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
			/*unused?*/// this will add another per diem line in the report footer
			if (intRNum == 1)
			{
				fldSummary1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				fldSummary1.Text = Strings.Format(dblAmount, "#,##0.00");
				fldSumCount1.Text = lngLnCnt.ToString();
				// kk01072015 trocls-13
				fldSumCount1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				lblSummary1.Text = modExtraModules.FormatYear(lngYear.ToString());
				dblPDTotal += dblAmount;
			}
			else
			{
				if (createObjects)
				{
					// add a field
					obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
					obNew.Name = "fldSummary" + FCConvert.ToString(intRNum);
					ReportFooter.Controls.Add(obNew);
				}
				else
				{
					obNew = ReportFooter.Controls["fldSummary" + FCConvert.ToString(intRNum)] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
				}
				if (obNew != null)
				{
					obNew.Top = fldSummary1.Top + ((intRNum - 1) * fldSummary1.Height);
					obNew.Left = fldSummary1.Left;
					obNew.Width = fldSummary1.Width;
					obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
					obNew.Font = fldSummary1.Font;
					// this sets the font to the same as the field that is already created
					obNew.Text = Strings.Format(dblAmount, "#,##0.00");
					dblPDTotal += dblAmount;
				}
				if (createObjects)
				{
					obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
					obNew.Name = "fldSumCount" + FCConvert.ToString(intRNum);
					ReportFooter.Controls.Add(obNew);
				}
				else
				{
					obNew = ReportFooter.Controls["fldSumCount" + FCConvert.ToString(intRNum)] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
				}
				if (obNew != null)
				{
					// kk01072015 trocls-13  add a field for the number of liens
					obNew.Top = fldSumCount1.Top + ((intRNum - 1) * fldSumCount1.Height);
					obNew.Left = fldSumCount1.Left;
					obNew.Width = fldSumCount1.Width;
					obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
					obNew.Font = fldSumCount1.Font;
					// this sets the font to the same as the field that is already created
					obNew.Text = lngLnCnt.ToString();
				}
				if (createObjects)
				{
					obLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
					// add a label
					obLabel.Name = "lblSummary" + FCConvert.ToString(intRNum);
					ReportFooter.Controls.Add(obLabel);
				}
				else
				{
					obLabel = ReportFooter.Controls["lblSummary" + FCConvert.ToString(intRNum)] as GrapeCity.ActiveReports.SectionReportModel.Label;
				}
				if (obLabel != null)
				{
					obLabel.Top = lblSummary1.Top + ((intRNum - 1) * lblSummary1.Height);
					obLabel.Left = lblSummary1.Left;
					obLabel.Font = fldSummary1.Font;
					// this sets the font to the same as the field that is already created
					obLabel.Text = modExtraModules.FormatYear(lngYear.ToString());
				}
			}
		}

		private void SetupTotalSummary()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the summary labels at the bottom of the page
				// and hide/show the labels when needed
				int intCT;
				int intRow;
				// this will keep track of the row  that I am adding values to
				string strDesc = "";
				
				lblSumHeaderType.Text = "Type";
				lblSumHeaderPrin.Text = "Principal";
				lblSumHeaderInt.Text = "Interest";
				lblSumHeaderCost.Text = "Costs";
				lblSumHeaderTotal.Text = "Total";
				intRow = 1;
				
				foreach (clsPmtTypeSummary tSum in tPayTypeSummaries)
				{
					if (!tSum.IsEmpty)
					{
						// OR If .Total <> 0 Then ???
						strDesc = tSum.Code + " - " + tSum.Description;
						FillSummaryLine(FCConvert.ToInt16(intRow), strDesc, tSum.Principal, tSum.PreLienInterst, tSum.Interest, tSum.LienCost, tSum.Total);
						intRow += 1;
					}
				}
				// Show the total line
				FillSummaryLine(intRow, "Total", tPayTypeSummaries.SummaryTotal().Principal, tPayTypeSummaries.SummaryTotal().PreLienInterst, tPayTypeSummaries.SummaryTotal().Interest, tPayTypeSummaries.SummaryTotal().LienCost, tPayTypeSummaries.SummaryTotal().Total);
				// 3XXXXXXXXXXXXXXXXXXXXXXXXXXXXX
				SetSummaryTotalLine(intRow);
				for (intCT = intRow + 1; intCT <= 11; intCT++)
				{
					HideSummaryRow(intCT);
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Creating Summary Table");
			}
		}
		// VBto upgrade warning: intRw As short	OnWriteFCConvert.ToInt32(
		private void SetSummaryTotalLine(int intRw)
		{
			switch (intRw)
			{
				case 1:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal1.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal1.Top;
						break;
					}
				case 2:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal2.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal2.Top;
						break;
					}
				case 3:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal3.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal3.Top;
						break;
					}
				case 4:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal4.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal4.Top;
						break;
					}
				case 5:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal5.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal5.Top;
						break;
					}
				case 6:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal6.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal6.Top;
						break;
					}
				case 7:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal7.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal7.Top;
						break;
					}
				case 8:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal8.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal8.Top;
						break;
					}
				case 9:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal9.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal9.Top;
						break;
					}
				case 10:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal10.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal10.Top;
						break;
					}
				case 11:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal11.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal11.Top;
						break;
					}
			}
			//end switch
		}
		// VBto upgrade warning: intRw As short	OnWriteFCConvert.ToInt32(
		private void FillSummaryLine(int intRw, string strDescription, double dblPrin, double dblPLI, double dblCurInt, double dblCosts, double dblTotal)
		{
			// this routine will fill in the line summary row
			switch (intRw)
			{
				case 1:
					{
						lblSummaryPaymentType1.Text = strDescription;
						lblSumPrin1.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt1.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost1.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal1.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 2:
					{
						lblSummaryPaymentType2.Text = strDescription;
						lblSumPrin2.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt2.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost2.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal2.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 3:
					{
						lblSummaryPaymentType3.Text = strDescription;
						lblSumPrin3.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt3.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost3.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal3.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 4:
					{
						lblSummaryPaymentType4.Text = strDescription;
						lblSumPrin4.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt4.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost4.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal4.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 5:
					{
						lblSummaryPaymentType5.Text = strDescription;
						lblSumPrin5.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt5.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost5.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal5.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 6:
					{
						lblSummaryPaymentType6.Text = strDescription;
						lblSumPrin6.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt6.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost6.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal6.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 7:
					{
						lblSummaryPaymentType7.Text = strDescription;
						lblSumPrin7.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt7.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost7.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal7.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 8:
					{
						lblSummaryPaymentType8.Text = strDescription;
						lblSumPrin8.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt8.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost8.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal8.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 9:
					{
						lblSummaryPaymentType9.Text = strDescription;
						lblSumPrin9.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt9.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost9.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal9.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 10:
					{
						lblSummaryPaymentType10.Text = strDescription;
						lblSumPrin10.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt10.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost10.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal10.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 11:
					{
						lblSummaryPaymentType11.Text = strDescription;
						lblSumPrin11.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt11.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost11.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal11.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
			}
			//end switch
		}
		// kk11262014 trocl-682  Fix inconsistencies in Payment Summary section
		// 1XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
		// Private Sub AddToPaymentArray(lngIndex As Long, dblPrin As Double, dblPLI As Double, dblCurInt As Double, dblCost As Double)
		// dblPayments(lngIndex, 0) = dblPayments(lngIndex, 0) + dblPrin
		// dblPayments(lngIndex, 1) = dblPayments(lngIndex, 1) + dblPLI
		// dblPayments(lngIndex, 2) = dblPayments(lngIndex, 2) + dblCurInt
		// dblPayments(lngIndex, 3) = dblPayments(lngIndex, 3) + dblCost
		// If dblCurInt > 0 Then
		// dblCurInt = dblCurInt
		// End If
		// End Sub
		// 2XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
		// VBto upgrade warning: intRw As short	OnWriteFCConvert.ToInt32(
		private void HideSummaryRow(int intRw)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intCT;
				for (intCT = intRw; intCT <= 11; intCT++)
				{
					switch (intRw)
					{
						case 1:
							{
								lblSummaryPaymentType1.Visible = false;
								lblSumPrin1.Visible = false;
								lblSumInt1.Visible = false;
								lblSumCost1.Visible = false;
								lblSummaryTotal1.Visible = false;
								break;
							}
						case 2:
							{
								lblSummaryPaymentType2.Visible = false;
								lblSumPrin2.Visible = false;
								lblSumInt2.Visible = false;
								lblSumCost2.Visible = false;
								lblSummaryTotal2.Visible = false;
								break;
							}
						case 3:
							{
								lblSummaryPaymentType3.Visible = false;
								lblSumPrin3.Visible = false;
								lblSumInt3.Visible = false;
								lblSumCost3.Visible = false;
								lblSummaryTotal3.Visible = false;
								break;
							}
						case 4:
							{
								lblSummaryPaymentType4.Visible = false;
								lblSumPrin4.Visible = false;
								lblSumInt4.Visible = false;
								lblSumCost4.Visible = false;
								lblSummaryTotal4.Visible = false;
								break;
							}
						case 5:
							{
								lblSummaryPaymentType5.Visible = false;
								lblSumPrin5.Visible = false;
								lblSumInt5.Visible = false;
								lblSumCost5.Visible = false;
								lblSummaryTotal5.Visible = false;
								break;
							}
						case 6:
							{
								lblSummaryPaymentType6.Visible = false;
								lblSumPrin6.Visible = false;
								lblSumInt6.Visible = false;
								lblSumCost6.Visible = false;
								lblSummaryTotal6.Visible = false;
								break;
							}
						case 7:
							{
								lblSummaryPaymentType7.Visible = false;
								lblSumPrin7.Visible = false;
								lblSumInt7.Visible = false;
								lblSumCost7.Visible = false;
								lblSummaryTotal7.Visible = false;
								break;
							}
						case 8:
							{
								lblSummaryPaymentType8.Visible = false;
								lblSumPrin8.Visible = false;
								lblSumInt8.Visible = false;
								lblSumCost8.Visible = false;
								lblSummaryTotal8.Visible = false;
								break;
							}
						case 9:
							{
								lblSummaryPaymentType9.Visible = false;
								lblSumPrin9.Visible = false;
								lblSumInt9.Visible = false;
								lblSumCost9.Visible = false;
								lblSummaryTotal9.Visible = false;
								break;
							}
						case 10:
							{
								lblSummaryPaymentType10.Visible = false;
								lblSumPrin10.Visible = false;
								lblSumInt10.Visible = false;
								lblSumCost10.Visible = false;
								lblSummaryTotal10.Visible = false;
								break;
							}
						case 11:
							{
								lblSummaryPaymentType11.Visible = false;
								lblSumPrin11.Visible = false;
								lblSumInt11.Visible = false;
								lblSumCost11.Visible = false;
								lblSummaryTotal11.Visible = false;
								break;
							}
					}
					//end switch
				}
				if (!boolAdjustedSummary)
				{
					SetYearSummaryTop_2(lblSummaryPaymentType1.Top + (intRw * lblSummaryPaymentType1.Height) + 100 / 1440F);
					boolAdjustedSummary = true;
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Hiding Summary Rows");
			}
		}

		private void SetYearSummaryTop_2(float lngTop)
		{
			SetYearSummaryTop(ref lngTop);
		}

		private void SetYearSummaryTop(ref float lngTop)
		{
			// this will start the year summary at the right height
			lblSummary.Top = lngTop;
			Line1.Y1 = lngTop + lblSummary.Height;
			Line1.Y2 = lngTop + lblSummary.Height;
			lblSummary1.Top = lngTop + lblSummary.Height;
			fldSumCount1.Top = lngTop + fldSumCount1.Height;
			// kk01072015 trocls-13
			fldSummary1.Top = lngTop + lblSummary.Height;
		}
		
		private void rptOutstandingLienBalances_Load(object sender, System.EventArgs e)
		{
			
		}

        private string GetOSLienBalQuery()
        {
            return "SELECT LienRec.*,b.BillingYear,b.RateKey as BRateKey,b.BillingType,b.LienRecordNumber AS LRN,b.Account,b.Name1 " +
                   "FROM BillingMaster b INNER JOIN LienRec ON b.LienRecordNumber = LienRec.ID WHERE ISNULL(LienRec.Principal,0) <> ISNULL(LienRec.PrincipalPaid,0)";
        }
    }
}