using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.Enums;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptZeroBalances : BaseSectionReport
	{
        //=========================================================
        private TaxCollectionStatusReportConfiguration reportConfiguration;
        private rptOutstandingBalancesAll parentReport;
        string strSQL;
		clsDRWrapper rsData = new clsDRWrapper();
		double[] dblTotals = new double[6 + 1];
		bool boolRTError;
		int lngCount;
		bool boolStarted;
		double dblPDTotal;
		bool boolAdjustedSummary;
		bool boolSummaryOnly;
		bool boolDeletedAccount;
		int lngTAType;

        private int intSuppReportType;
        // 0 = all, 1 = TA, 2 = non TA
        clsDRWrapper rsMaster = new clsDRWrapper();
		Dictionary<object, object> dctAccounts = new Dictionary<object, object>();
		// kk01132014 trocls-13
		// this is for the summaries at the bottom
		double[] dblYearTotals = new double[2000 + 1];
		// billingyear - 19800
		int[] lngArrBillCounts = new int[2000 + 1];
		// kk01092015 trocl-13
		double[,] dblPayments = new double[10 + 1, 5 + 1];
		// 0 - 3, 1 - A, 2 - C, 3 - D, 4 - I, 5 - L, 6 - P, 7 - U, 8 - X, 9 - Y, 10 - Total
		// 0 - Principal, 1 - PreLienInterest, 2 - CurrentInterest, 3 - Costs
		public rptZeroBalances()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//if (_InstancePtr == null)
			//	_InstancePtr = this;
			this.Name = "Zero Balance Report";
		}

        public void SetReportOptionAndParent(TaxCollectionStatusReportConfiguration reportConfig, rptOutstandingBalancesAll parent, int suppReportType)
        {
            reportConfiguration = reportConfig;
            parentReport = parent;
            intSuppReportType = suppReportType;
        }
  //      public static rptZeroBalances InstancePtr
		//{
		//	get
		//	{
		//		return (rptZeroBalances)Sys.GetInstance(typeof(rptZeroBalances));
		//	}
		//}

		//protected rptZeroBalances _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			//if (_InstancePtr == this)
			//{
			//	_InstancePtr = null;
			//	Sys.ClearInstance(this);
			//}
            if (disposing)
            {
				rsData?.Dispose();
				rsMaster?.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsData.EndOfFile())
			{
				if (boolStarted)
				{
					eArgs.EOF = true;
				}
				else
				{
				}
			}
			else
			{
				eArgs.EOF = false;
			}
			//Detail_Format();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_PageEnd(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "t");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			lngCount = 0;
			boolSummaryOnly = reportConfiguration.Options.ShowSummaryOnly;
            if (reportConfiguration.Filter.IsTaxAcquired.HasValue)
            {
                if (reportConfiguration.Filter.IsTaxAcquired.Value)
                {
                    lngTAType = 1;
                }
                else
                {
                    lngTAType = 2;
                }
            }
            else
            {
                lngTAType = 0;
            }
            frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading");
			
			boolRTError = false;
			SetupFields();
			
			SetReportHeader();
			
			boolStarted = false;
			strSQL = BuildSQL();
			// Generates the SQL String
			if (lngTAType > 0)
			{
				// kk01092015 trocls-22
				rsMaster.OpenRecordset("SELECT RSAccount, TaxAcquired FROM Master", modExtraModules.strREDatabase);
			}
			rsData.OpenRecordset(strSQL, modGlobal.DEFAULTDATABASE);
			SetupTotalsCreateControls();
		}

		private string BuildSQL()
		{
			string BuildSQL = "";
			string strTemp = "";
			string strWhereClause = "";
			string strREPPBill = "";
			string strREPPPayment = "";
			int intCT;
			string strSupp = "";
			//FC:FINAL:KS: #without nr.:fix Exit For inside switch/case statement
			bool endFor = false;
			if (reportConfiguration.BillingType == PropertyTaxBillType.Real)
			{
				// only get the accounts for the correct module
				strREPPBill = "'RE'";
			}
			else
			{
				strREPPBill = "'PP'";
			}
			if (intSuppReportType == 1)
			{
				strSupp = " AND BillingYear % 10 > 1 ";
			}
			
			if (reportConfiguration.Options.UseAsOfDate())
			{
				strWhereClause = "WHERE BillingType = " + strREPPBill + strSupp + " AND ISNULL(transferfrombillingdatefirst, '12/30/1899') <= '" + FCConvert.ToString(reportConfiguration.Options.AsOfDate) + "' ";
			}
			else
			{
				strWhereClause = "WHERE ISNULL(LienRecordNumber,0) = 0 AND BillingType = " + strREPPBill + strSupp;
			}
            var filter = reportConfiguration.Filter;
            var strAnd = "";

            if (filter.AccountRangeUsed())
            {
                if (filter.AccountMax.HasValue)
                {
                    if (filter.AccountMin.HasValue)
                    {
                        strTemp += "Account between " + filter.AccountMin.Value.ToString() + " and " +
                                   filter.AccountMax.Value.ToString();
                    }
                    else
                    {
                        strTemp += "Account <= " + filter.AccountMax.Value.ToString();
                    }
                }
                else
                {
                    strTemp += "Account >= " + filter.AccountMin.Value.ToString();
                }

                strAnd = " and ";
            }

            if (filter.NameRangeUsed())
            {
                strTemp += strAnd;

                if (!String.IsNullOrWhiteSpace(filter.NameMin))
                {
                    if (!String.IsNullOrWhiteSpace(filter.NameMax))
                    {
                        strTemp += "Name1 between '" + filter.NameMin + "' and '" + filter.NameMax + "zzzz'";
                    }
                    else
                    {
                        strTemp += "Name1 >= '" + filter.NameMin + "'";
                    }
                }
                else
                {
                    strTemp += "Name1 <= '" + filter.NameMax + "zzzz'";
                }

                strAnd = " and ";
            }

            if (filter.TaxYearRangeUsed())
            {
                strTemp += strAnd;

                if (filter.TaxYearMin.HasValue)
                {
                    if (filter.TaxYearMax.HasValue)
                    {
                        strTemp += "BillingYear between " + filter.TaxYearMin.Value.ToString() + " and " +
                                   filter.TaxYearMax.Value.ToString();
                    }
                    else
                    {
                        strTemp += "BillingYear = " + filter.TaxYearMin.Value.ToString();
                    }
                }
                else
                {
                    strTemp += "BillingYear = " + filter.TaxYearMax.Value.ToString();
                }

                strAnd = " and ";
            }

            if (filter.RateRecordRangeUsed())
            {
                strTemp += strAnd;

                if (filter.RateRecordMin.HasValue)
                {
                    if (filter.RateRecordMax.HasValue)
                    {
                        strTemp += "RateKey between " + filter.RateRecordMin.Value.ToString() + " and " +
                                   filter.RateRecordMax.Value.ToString();
                    }
                    else
                    {
                        strTemp += "RateKey >= " + filter.RateRecordMin.Value.ToString();
                    }
                }
                else
                {
                    strTemp += "RateKey >= " + filter.RateRecordMin.Value.ToString();
                }

                strAnd = " and ";
            }

            if (Strings.Trim(strTemp) != "")
			{
				strWhereClause += " AND " + strTemp;
			}
			if (reportConfiguration.Options.UseAsOfDate())
			{
				strTemp = "SELECT * FROM BillingMaster " + strWhereClause + " ORDER BY Name1, Account, BillingYear";
			}
			else
			{
				strTemp = "SELECT * FROM (" + GetZeroBalQuery() + ") AS qZeroBal " + strWhereClause + " ORDER BY Name1, Account, BillingYear";
			}
			BuildSQL = strTemp;
			return BuildSQL;
		}

		private void SetupFields()
		{
			// Set each field and label's visible property to True/False depending on which fields the user
			// has selected from the Custom Report screen then move them accordingly
			int intRow;
			int intRow2/*unused?*/;
			float lngHt;
			if (boolSummaryOnly)
			{
				lblYear.Visible = false;
				fldYear.Visible = false;
				fldName.Visible = false;
				fldAccount.Visible = false;
				fldPaymentReceived.Visible = false;
				fldTaxDue.Visible = false;
				fldDue.Visible = false;
				lnHeader.Visible = false;
				lnTotals.Visible = false;
				fldType.Visible = false;
				Detail.Height = 0;
				return;
			}
			intRow = 2;
			lngHt = 270 / 1440F;
			lblYear.Visible = true;
			fldYear.Visible = true;
			// if the year is not shown, then make the name field smaller
			fldName.Width = fldYear.Left - (fldType.Left + fldType.Width);
		}

		private void BindFields()
		{
			var rsPayment = new clsDRWrapper();
			//var rsCalLien = new clsDRWrapper();
			var rsRate = new clsDRWrapper();
			var rsLien = new clsDRWrapper();
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will fill the information into the fields

                //clsDRWrapper rsRE = new clsDRWrapper();



                string strTemp = "";

                DateTime dtPaymentDate;

                double dblPaymentRecieved;
                double dblXtraInt = 0;
                double dblTotalDue;
                TRYAGAIN: ;
                dblPaymentRecieved = 0;
                fldDue.Text = "";
                fldTaxDue.Text = "";
                fldPaymentReceived.Text = "";
                fldAccount.Text = "";
                fldYear.Text = "";
                fldName.Text = "";
                fldType.Text = "";
                if (rsData.EndOfFile())
                {
                    return;
                }

                if (reportConfiguration.Filter.TranCodeMax.HasValue)
                {
                    if (!modCLCalculations.CheckTranCode_78(reportConfiguration.BillingType == PropertyTaxBillType.Real,
                        FCConvert.ToInt32(rsData.Get_Fields("Account")), reportConfiguration.Filter.TranCodeMin ?? 0,
                        reportConfiguration.Filter.TranCodeMax ?? 0))
                    {
                        rsData.MoveNext();
                        goto TRYAGAIN;
                    }
                }

                // MAL@20080624 ; Tracker Reference: 14306
                if (FCConvert.ToInt32(rsData.Get_Fields_Int32("LienRecordNumber")) > 0)
                {
                    // Check for bills using the As of Date
                    rsLien.OpenRecordset(
                        "SELECT * FROM LienRec WHERE ID = " +
                        FCConvert.ToString(rsData.Get_Fields_Int32("LienRecordNumber")), modExtraModules.strCLDatabase);
                    if (rsLien.RecordCount() > 0)
                    {
                        // Does Lien Creation come before As Of Date?
                        if (Conversion.Val(rsLien.Get_Fields_DateTime("DateCreated")) <=
                            reportConfiguration.Options.AsOfDate.ToOADate())
                        {
                            rsData.MoveNext();
                            goto TRYAGAIN;
                        }
                        else
                        {
                            // Continue
                        }
                    }
                }

                // MAL@20080813: Add check for Tax Acquired status
                // Tracker Reference: 11805
                if (lngTAType > 0)
                {
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    rsMaster.FindFirstRecord("RSAccount", rsData.Get_Fields("Account"));
                    if (!rsMaster.NoMatch)
                    {
                        switch (lngTAType)
                        {
                            case 1:
                            {
                                // TA
                                if (!FCConvert.ToBoolean(rsMaster.Get_Fields_Boolean("TaxAcquired")))
                                {
                                    rsData.MoveNext();
                                    goto TRYAGAIN;
                                }

                                break;
                            }
                            case 2:
                            {
                                // Non TA
                                if (FCConvert.ToBoolean(rsMaster.Get_Fields_Boolean("TaxAcquired")))
                                {
                                    rsData.MoveNext();
                                    goto TRYAGAIN;
                                }

                                break;
                            }
                        }

                        //end switch
                    }
                }

                lngCount += 1;
                //FC:FINAL:BCU - #i181 - add convert to string
                //fldAccount.Text = rsData.Get_Fields("Account");
                //fldYear.Text = FCUtils.iDiv(FCConvert.ToInt32(rsData.Get_Fields("BillingYear")), 10);
                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                fldAccount.Text = FCConvert.ToString(rsData.Get_Fields("Account"));
                fldYear.Text =
                    FCConvert.ToString(FCUtils.iDiv(FCConvert.ToInt32(rsData.Get_Fields_Int32("BillingYear")), 10));
                fldName.Text = rsData.Get_Fields_String("Name1");
                // GetStatusName(rsData)   'rsData.Fields("Name1")
                fldTaxDue.Text = Strings.Format(
                    Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1")) +
                    Conversion.Val(rsData.Get_Fields_Decimal("TaxDue2")) +
                    Conversion.Val(rsData.Get_Fields_Decimal("TaxDue3")) +
                    Conversion.Val(rsData.Get_Fields_Decimal("TaxDue4")), "#,##0.00");
                if (reportConfiguration.Options.UseAsOfDate())
                {
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE Account = " +
                                            FCConvert.ToString(rsData.Get_Fields("Account")) + " AND [Year] = " +
                                            FCConvert.ToString(rsData.Get_Fields_Int32("BillingYear")) +
                                            " AND BillCode <> 'L' AND BillKey = " +
                                            FCConvert.ToString(rsData.Get_Fields_Int32("ID")) +
                                            " AND ISNULL(RecordedTransactionDate, '12/30/1899') <= '" +
                                            FCConvert.ToString(reportConfiguration.Options.AsOfDate) + "'");
                }
                else
                {
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE Account = " +
                                            FCConvert.ToString(rsData.Get_Fields("Account")) + " AND [Year] = " +
                                            FCConvert.ToString(rsData.Get_Fields_Int32("BillingYear")) +
                                            " AND BillCode <> 'L' AND BillKey = " +
                                            FCConvert.ToString(rsData.Get_Fields_Int32("ID")));
                }

                while (!rsPayment.EndOfFile())
                {
                    // 0 - 3, 1 - A, 2 - C, 3 - D, 4 - I, 5 - L, 6 - P, 7 - U, 8 - X, 9 - Y, 10 - Total
                    // 0 - Principal, 1 - PreLienInterest, 2 - CurrentInterest, 3 - Costs
                    if (reportConfiguration.Options.FullStatusAmounts)
                    {
                        // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                        dblPaymentRecieved += Conversion.Val(rsPayment.Get_Fields("Principal")) +
                                              Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")) +
                                              Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")) +
                                              Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost"));
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "P")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            AddToPaymentArray_242(6, Conversion.Val(rsPayment.Get_Fields("Principal")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "U")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            AddToPaymentArray_242(8, Conversion.Val(rsPayment.Get_Fields("Principal")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "X")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            AddToPaymentArray_242(9, Conversion.Val(rsPayment.Get_Fields("Principal")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "Y")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            AddToPaymentArray_242(10, Conversion.Val(rsPayment.Get_Fields("Principal")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "C")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            AddToPaymentArray_242(2, Conversion.Val(rsPayment.Get_Fields("Principal")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "A")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            AddToPaymentArray_242(1, Conversion.Val(rsPayment.Get_Fields("Principal")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "S")
                        {
                            // put these figures in the X category because there are going away
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            AddToPaymentArray_242(9, Conversion.Val(rsPayment.Get_Fields("Principal")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "D")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            AddToPaymentArray_242(3, Conversion.Val(rsPayment.Get_Fields("Principal")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "I")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            AddToPaymentArray_242(4, Conversion.Val(rsPayment.Get_Fields("Principal")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "L")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            AddToPaymentArray_242(5, Conversion.Val(rsPayment.Get_Fields("Principal")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "3")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            AddToPaymentArray_242(0, Conversion.Val(rsPayment.Get_Fields("Principal")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "R")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            AddToPaymentArray_242(7, Conversion.Val(rsPayment.Get_Fields("Principal")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
                        }
                    }
                    else
                    {
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "P")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            AddToPaymentArray_242(6, Conversion.Val(rsPayment.Get_Fields("Principal")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if ((Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "X") ||
                                 (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "S"))
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            AddToPaymentArray_242(9, Conversion.Val(rsPayment.Get_Fields("Principal")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "U")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            AddToPaymentArray_242(8, Conversion.Val(rsPayment.Get_Fields("Principal")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "Y")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            AddToPaymentArray_242(10, Conversion.Val(rsPayment.Get_Fields("Principal")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "C")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            AddToPaymentArray_242(2, Conversion.Val(rsPayment.Get_Fields("Principal")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "A")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            AddToPaymentArray_242(1, Conversion.Val(rsPayment.Get_Fields("Principal")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "D")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            AddToPaymentArray_242(3, Conversion.Val(rsPayment.Get_Fields("Principal")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "I")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            AddToPaymentArray_242(4, Conversion.Val(rsPayment.Get_Fields("Principal")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "L")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            AddToPaymentArray_242(5, Conversion.Val(rsPayment.Get_Fields("Principal")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "3")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            AddToPaymentArray_242(0, Conversion.Val(rsPayment.Get_Fields("Principal")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "R")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            AddToPaymentArray_242(7, Conversion.Val(rsPayment.Get_Fields("Principal")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost")));
                        }

                        // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                        dblPaymentRecieved += Conversion.Val(rsPayment.Get_Fields("Principal")) +
                                              Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost"));
                    }

                    rsPayment.MoveNext();
                }

                if (reportConfiguration.Options.FullStatusAmounts)
                {
                    // if the current interest is checked then calculate it and display it
                    dblTotalDue = modCLCalculations.CalculateAccountCL(ref rsData,
                        FCConvert.ToInt32(rsData.Get_Fields_Int32("BillingYear")), reportConfiguration.Options.AsOfDate,
                        ref dblXtraInt);
                }
                else
                {
                    dblXtraInt = 0;
                }

                fldPaymentReceived.Text = Strings.Format(dblPaymentRecieved - dblXtraInt, "#,##0.00");
                fldDue.Text = Strings.Format(FCConvert.ToDouble(fldTaxDue.Text) - (dblPaymentRecieved - dblXtraInt),
                    "#,##0.00");
                if (intSuppReportType == 1)
                {
                    rsRate.OpenRecordset(
                        "SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(rsData.Get_Fields_Int32("RateKey")),
                        modExtraModules.strCLDatabase);
                    if (!rsRate.EndOfFile())
                    {
                        if (FCConvert.ToString(rsRate.Get_Fields_String("RateType")) != "S")
                        {
                            rsData.MoveNext();
                            goto TRYAGAIN;
                        }
                    }
                    else
                    {
                        rsData.MoveNext();
                        goto TRYAGAIN;
                    }
                }

                if (FCConvert.ToDouble(fldDue.Text) != 0)
                {
                    // double check
                    // if this account is not zero balanced, then it must not be used
                    ReversePaymentsFromStatusArray(ref rsPayment);
                    rsData.MoveNext();
                    lngCount -= 1;
                    goto TRYAGAIN;
                }

                if (FCConvert.ToString(rsData.Get_Fields_String("BillingType")) == "RE")
                {
                    fldType.Text = "R";
                }
                else
                {
                    fldType.Text = "P";
                }

                //if (modGlobal.Statics.boolSubReport)
                //{
                parentReport.dblTotalsPrin += FCConvert.ToDouble(fldTaxDue.Text);
                parentReport.dblTotalsPay += dblPaymentRecieved - dblXtraInt;
                parentReport.lngCount += 1;
                //}
                dblTotals[0] += FCConvert.ToDouble(fldTaxDue.Text);
                dblTotals[1] += dblPaymentRecieved - dblXtraInt;
                dblTotals[3] += FCConvert.ToDouble(fldDue.Text);
                // keep track for the year totals
                dblYearTotals[rsData.Get_Fields_Int32("BillingYear") - 19800] =
                    dblYearTotals[FCConvert.ToInt16(rsData.Get_Fields_Int32("BillingYear")) - 19800] +
                    FCConvert.ToDouble(fldDue.Text);
                // kk0107205 trocls-13  Keep count of accounts and Bills by Year
                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                if (!dctAccounts.ContainsKey(FCConvert.ToInt32(Conversion.Val(rsData.Get_Fields("Account")))))
                {
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    dctAccounts.Add(FCConvert.ToInt32(Conversion.Val(rsData.Get_Fields("Account"))),
                        rsData.Get_Fields_String("Name1"));
                }
                else
                {
                    // Count different names with the same account number
                }

                lngArrBillCounts[rsData.Get_Fields_Int32("BillingYear") - 19800] =
                    lngArrBillCounts[FCConvert.ToInt16(rsData.Get_Fields_Int32("BillingYear")) - 19800] + 1;
                // move to the next record in the query
                rsData.MoveNext();
                return;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                FCMessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message +
                    ".", MsgBoxStyle.Critical, "Error In BindFields");
            }
            finally
            {
				rsPayment.Dispose();
				rsLien.Dispose();
				rsRate.Dispose();
            }
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void SetupTotals()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this sub will fill in the totals line at the bottom of the report
				//clsDRWrapper rsSum = new clsDRWrapper();
				string strSUM = "";
				int intSumRows;
				GrapeCity.ActiveReports.SectionReportModel.TextBox obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
				int intCT;
				fldTotalTaxDue.Text = Strings.Format(dblTotals[0], "#,##0.00");
				fldTotalPaymentReceived.Text = Strings.Format(dblTotals[1], "#,##0.00");
				fldTotalDue.Text = Strings.Format(dblTotals[3], "#,##0.00");
				// kk01092015 trocls-13  Add count of accounts
				if (dctAccounts.Count > 1)
				{
					fldAcctCount.Text = FCConvert.ToString(dctAccounts.Count) + " Accounts";
				}
				else if (dctAccounts.Count == 1)
				{
					fldAcctCount.Text = FCConvert.ToString(dctAccounts.Count) + " Account";
				}
				else
				{
					fldAcctCount.Text = "";
				}
				if (lngCount > 1)
				{
					lblTotals.Text = "Total for " + FCConvert.ToString(lngCount) + " Bills:";
					// kk01092015 trocls-13  This is count of bills, not accounts   '& " Accounts:"
				}
				else if (lngCount == 1)
				{
					lblTotals.Text = "Total for " + FCConvert.ToString(lngCount) + " Bills:";
					// & " Account:"
				}
				else
				{
					lblTotals.Text = "No Non Lien Bills";
				}
				// this will setup the payment summary
				SetupTotalSummary();
				// Load Summary List
				intSumRows = 1;
				for (intCT = 0; intCT <= Information.UBound(dblYearTotals, 1) - 1; intCT++)
				{
					if (dblYearTotals[intCT] != 0)
					{
						AddSummaryRow(intSumRows, dblYearTotals[intCT], lngArrBillCounts[intCT], intCT + 19800);
						intSumRows += 1;
					}
				}
				// strSUM = "SELECT BillingYear AS Year, SUM(TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4 - PrincipalPaid) AS Due FROM (" & strSQL & ") GROUP BY BillingYear"
				// 
				// intSumRows = 1
				// 
				// rsSum.OpenRecordset strSUM, strCLDatabase
				// If rsSum.EndOfFile Then
				// intSumRows = 1
				// Else
				// Do Until rsSum.EndOfFile
				// AddSummaryRow intSumRows, rsSum.Fields("Due"), rsSum.Fields("Year")
				// intSumRows = intSumRows + 1
				// rsSum.MoveNext
				// Loop
				// End If
				// create the total fields and fill them
				// add a field
				obNew = ReportFooter.Controls["fldSummaryTotal"] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
				obNew.Top = fldSummary1.Top + ((intSumRows - 1) * fldSummary1.Height);
				obNew.Left = fldSummary1.Left;
				obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				obNew.Width = fldSummary1.Width;
				obNew.Font = lblSummary1.Font;
				obNew.Text = Strings.Format(dblPDTotal, "#,##0.00");
				// kk01092015 trocls-13   add a field for the total count
				obNew = ReportFooter.Controls["fldSumCountTotal"] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
				obNew.Top = fldSumCount1.Top + ((intSumRows - 1) * fldSumCount1.Height);
				obNew.Left = fldSumCount1.Left;
				obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				obNew.Width = fldSumCount1.Width;
				obNew.Font = fldSumCount1.Font;
				obNew.Text = lngCount.ToString();
				// add a label
				GrapeCity.ActiveReports.SectionReportModel.Label obLabel = ReportFooter.Controls["lblPerDiemTotal"] as GrapeCity.ActiveReports.SectionReportModel.Label;
				obLabel.Top = lblSummary1.Top + ((intSumRows - 1) * lblSummary1.Height);
				obLabel.Left = lblSummary1.Left;
				obLabel.Font = lblSummary1.Font;
				obLabel.Text = "Total";
				// add a line
				GrapeCity.ActiveReports.SectionReportModel.Line obLine = ReportFooter.Controls["lnFooterSummaryTotal"] as GrapeCity.ActiveReports.SectionReportModel.Line;
				obLine.X1 = fldSumCount1.Left;
				// kk01092015 trocls-13     fldSummary1.Left
				obLine.X2 = fldSummary1.Left + fldSummary1.Width;
				obLine.Y1 = lblSummary1.Top + ((intSumRows - 1) * lblSummary1.Height);
				obLine.Y2 = obLine.Y1;
				ReportFooter.Height = lblSummary1.Top + (intSumRows * lblSummary1.Height);
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Summary Creation");
			}
		}

		private void SetupTotalsCreateControls()
		{
			GrapeCity.ActiveReports.SectionReportModel.TextBox obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			obNew.Name = "fldSummaryTotal";
			ReportFooter.Controls.Add(obNew);
			obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			obNew.Name = "fldSumCountTotal";
			ReportFooter.Controls.Add(obNew);
			GrapeCity.ActiveReports.SectionReportModel.Label obLabel = null;
			obLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
			obLabel.Name = "lblPerDiemTotal";
			ReportFooter.Controls.Add(obLabel);
			GrapeCity.ActiveReports.SectionReportModel.Line obLine = null;
			obLine = new GrapeCity.ActiveReports.SectionReportModel.Line();
			obLine.Name = "lnFooterSummaryTotal";
			ReportFooter.Controls.Add(obLine);
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			SetupTotals();
		}

		private void SetReportHeader()
		{
			int intCT;
			string strTemp = "";
            var strSeparator = "";
            var filter = reportConfiguration.Filter;
            if (filter.AccountRangeUsed())
            {
                if (filter.AccountMax.HasValue)
                {
                    if (filter.AccountMin.HasValue)
                    {
                        if (filter.AccountMin == filter.AccountMax)
                        {
                            strTemp += "Account: " + filter.AccountMin.Value.ToString();
                        }
                        else
                        {
                            strTemp += "Account: " + filter.AccountMin.Value.ToString() + " To " +
                                       filter.AccountMax.Value.ToString();
                        }
                    }
                    else
                    {
                        strTemp += "Account Below: " + filter.AccountMax.Value.ToString();
                    }
                }
                else
                {
                    strTemp += "Account Above: " + filter.AccountMin.Value.ToString();
                }
                strSeparator = ", ";
            }

            if (filter.TaxYearRangeUsed())
            {
                strTemp += strSeparator;

                if (filter.TaxYearMin.HasValue)
                {
                    if (filter.TaxYearMax.HasValue)
                    {
                        if (filter.TaxYearMin == filter.TaxYearMax)
                        {
                            strTemp += "Tax Year: " + filter.TaxYearMin.Value.ToString();
                        }
                        else
                        {
                            strTemp += "Tax Year: " + filter.TaxYearMin.Value.ToString() + " To " +
                                       filter.TaxYearMax.Value.ToString();
                        }
                    }
                    else
                    {
                        strTemp += "Tax Year: " + filter.TaxYearMin.Value.ToString();
                    }
                }
                else
                {
                    strTemp += "Tax Year: " + filter.TaxYearMax.Value.ToString();
                }

                strSeparator = ", ";
            }

            if (lngTAType > 0)
			{
				if (Strings.Trim(strTemp) == "")
				{
					if (lngTAType == 1)
					{
						strTemp = "Tax Acquired";
					}
					else
					{
						strTemp = "Non-Tax Acquired";
					}
				}
				else
				{
					if (lngTAType == 1)
					{
						strTemp = ", Tax Acquired";
					}
					else
					{
						strTemp = ", Non-Tax Acquired";
					}
				}
			}
			// check to see if they used the Show Interest checkbox
			if (reportConfiguration.Options.ShowCurrentInterest)
			{
				if (Strings.Trim(strTemp) == "")
				{
					strTemp += "Show Interest";
				}
				else
				{
					strTemp += ", Show Interest";
				}
			}
			if (Strings.Trim(strTemp) == "")
			{
				lblReportType.Text = "Complete List" + "\r\n" + "Zero Balance Non Liened Accounts";
			}
			else
			{
				lblReportType.Text = strTemp + "\r\n" + "Zero Balance Non Liened Accounts";
			}
			if (reportConfiguration.Options.UseAsOfDate())
			{
				lblReportType.Text = lblReportType.Text + "\r\n" + "As of: " + Strings.Format(reportConfiguration.Options.AsOfDate, "MM/dd/yyyy");
			}
		}
		// VBto upgrade warning: intRNum As short	OnWriteFCConvert.ToInt32(
		private void AddSummaryRow(int intRNum, double dblAmount, int lngBillCnt, int lngYear)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				GrapeCity.ActiveReports.SectionReportModel.TextBox obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
				/*unused?*/// this will add another per diem line in the report footer
				if (intRNum == 1)
				{
					fldSummary1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
					fldSummary1.Text = Strings.Format(dblAmount, "#,##0.00");
					fldSumCount1.Text = lngBillCnt.ToString();
					// kk01092015 trocls-13
					fldSumCount1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
					lblSummary1.Text = modExtraModules.FormatYear(lngYear.ToString());
					dblPDTotal += dblAmount;
				}
				else
				{
					// add a field
					obNew.Name = "fldSummary" + FCConvert.ToString(intRNum);
					obNew.Top = fldSummary1.Top + ((intRNum - 1) * fldSummary1.Height);
					obNew.Left = fldSummary1.Left;
					obNew.Width = fldSummary1.Width;
					obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
					obNew.Font = fldSummary1.Font;
					// this sets the font to the same as the field that is already created
					obNew.Text = Strings.Format(dblAmount, "#,##0.00");
					dblPDTotal += dblAmount;
					ReportFooter.Controls.Add(obNew);
					// kk01092015 trocls-13  add a field for the number of liens
					obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
					obNew.Name = "fldSumCount" + FCConvert.ToString(intRNum);
					obNew.Top = fldSumCount1.Top + ((intRNum - 1) * fldSumCount1.Height);
					obNew.Left = fldSumCount1.Left;
					obNew.Width = fldSumCount1.Width;
					obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
					obNew.Font = fldSumCount1.Font;
					// this sets the font to the same as the field that is already created
					obNew.Text = lngBillCnt.ToString();
					ReportFooter.Controls.Add(obNew);
					// add a label
					GrapeCity.ActiveReports.SectionReportModel.Label obLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
					obLabel.Name = "lblSummary" + FCConvert.ToString(intRNum);
					obLabel.Top = lblSummary1.Top + ((intRNum - 1) * lblSummary1.Height);
					obLabel.Left = lblSummary1.Left;
					obLabel.Font = fldSummary1.Font;
					// this sets the font to the same as the field that is already created
					obLabel.Text = modExtraModules.FormatYear(lngYear.ToString());
					ReportFooter.Controls.Add(obLabel);
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Adding Summary Row");
			}
		}

		private void SetupTotalSummary()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the summary labels at the bottom of the page
				// and hide/show the labels when needed
				int intCT;
				int intRow;
				// this will keep track of the row  that I am adding values to
				string strDesc = "";
				double[] dblTotal = new double[5 + 1];
				// fill in the titles
				lblSumHeaderType.Text = "Type";
				lblSumHeaderPrin.Text = "Principal";
				lblSumHeaderInt.Text = "Interest";
				lblSumHeaderCost.Text = "Costs";
				lblSumHeaderTotal.Text = "Total";
				intRow = 1;
				// start at the first row
				for (intCT = 0; intCT <= 10; intCT++)
				{
					// this will fill the totals element
					dblPayments[intCT, 4] = dblPayments[intCT, 0] + dblPayments[intCT, 1] + dblPayments[intCT, 2] + dblPayments[intCT, 3];
				}
				for (intCT = 0; intCT <= 10; intCT++)
				{
					if (dblPayments[intCT, 4] != 0)
					{
						switch (intCT)
						{
							case 0:
								{
									strDesc = "3 - 30 DN Costs";
									break;
								}
							case 1:
								{
									strDesc = "A - Abatement";
									break;
								}
							case 2:
								{
									strDesc = "C - Correction";
									break;
								}
							case 3:
								{
									strDesc = "D - Discount";
									break;
								}
							case 4:
								{
									strDesc = "I - Interest Charged";
									break;
								}
							case 5:
								{
									strDesc = "L - Lien Costs";
									break;
								}
							case 6:
								{
									strDesc = "P - Payment";
									break;
								}
							case 7:
								{
									strDesc = "R - Refunded Abatement";
									break;
								}
							case 8:
								{
									strDesc = "U - Tax Club";
									break;
								}
							case 9:
								{
									strDesc = "X - DOS Correction";
									break;
								}
							case 10:
								{
									strDesc = "Y - Prepayment";
									break;
								}
						}
						//end switch
						FillSummaryLine(intRow, strDesc, dblPayments[intCT, 0], dblPayments[intCT, 1], dblPayments[intCT, 2], dblPayments[intCT, 3], dblPayments[intCT, 4]);
						dblTotal[0] += dblPayments[intCT, 0];
						// this will total all of the seperated payments for the total line
						dblTotal[1] += dblPayments[intCT, 1];
						dblTotal[2] += dblPayments[intCT, 2];
						dblTotal[3] += dblPayments[intCT, 3];
						dblTotal[4] += dblPayments[intCT, 4];
						intRow += 1;
					}
				}
				// show the total line
				FillSummaryLine(intRow, "Total", dblTotal[0], dblTotal[1], dblTotal[2], dblTotal[3], dblTotal[4]);
				SetSummaryTotalLine(intRow);
				for (intCT = intRow + 1; intCT <= 11; intCT++)
				{
					HideSummaryRow(intCT);
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Creating Summary Table");
			}
		}
		// VBto upgrade warning: intRw As short	OnWriteFCConvert.ToInt32(
		private void SetSummaryTotalLine(int intRw)
		{
			switch (intRw)
			{
				case 1:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal1.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal1.Top;
						break;
					}
				case 2:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal2.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal2.Top;
						break;
					}
				case 3:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal3.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal3.Top;
						break;
					}
				case 4:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal4.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal4.Top;
						break;
					}
				case 5:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal5.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal5.Top;
						break;
					}
				case 6:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal6.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal6.Top;
						break;
					}
				case 7:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal7.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal7.Top;
						break;
					}
				case 8:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal8.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal8.Top;
						break;
					}
				case 9:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal9.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal9.Top;
						break;
					}
				case 10:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal10.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal10.Top;
						break;
					}
				case 11:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal11.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal11.Top;
						break;
					}
			}
			//end switch
		}
		// VBto upgrade warning: intRw As short	OnWriteFCConvert.ToInt32(
		private void FillSummaryLine(int intRw, string strDescription, double dblPrin, double dblPLI, double dblCurInt, double dblCosts, double dblTotal)
		{
			// this routine will fill in the line summary row
			switch (intRw)
			{
				case 1:
					{
						lblSummaryPaymentType1.Text = strDescription;
						lblSumPrin1.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt1.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost1.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal1.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 2:
					{
						lblSummaryPaymentType2.Text = strDescription;
						lblSumPrin2.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt2.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost2.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal2.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 3:
					{
						lblSummaryPaymentType3.Text = strDescription;
						lblSumPrin3.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt3.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost3.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal3.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 4:
					{
						lblSummaryPaymentType4.Text = strDescription;
						lblSumPrin4.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt4.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost4.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal4.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 5:
					{
						lblSummaryPaymentType5.Text = strDescription;
						lblSumPrin5.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt5.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost5.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal5.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 6:
					{
						lblSummaryPaymentType6.Text = strDescription;
						lblSumPrin6.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt6.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost6.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal6.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 7:
					{
						lblSummaryPaymentType7.Text = strDescription;
						lblSumPrin7.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt7.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost7.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal7.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 8:
					{
						lblSummaryPaymentType8.Text = strDescription;
						lblSumPrin8.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt8.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost8.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal8.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 9:
					{
						lblSummaryPaymentType9.Text = strDescription;
						lblSumPrin9.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt9.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost9.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal9.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 10:
					{
						lblSummaryPaymentType10.Text = strDescription;
						lblSumPrin10.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt10.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost10.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal10.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 11:
					{
						lblSummaryPaymentType11.Text = strDescription;
						lblSumPrin11.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumInt11.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost11.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal11.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
			}
			//end switch
		}
		//FC:FINAL:SBE - #i327 - add function with decimal datatype (instead of converting parameters for each function call)
		private void AddToPaymentArray_242(int lngIndex, decimal dblPrin, decimal dblPLI, decimal dblCurInt, decimal dblCost)
		{
			this.AddToPaymentArray_242(lngIndex, Conversion.Val(dblPrin), Conversion.Val(dblPLI), Conversion.Val(dblCurInt), Conversion.Val(dblCost));
		}

		private void AddToPaymentArray_242(int lngIndex, double dblPrin, double dblPLI, double dblCurInt, double dblCost)
		{
			AddToPaymentArray(ref lngIndex, ref dblPrin, ref dblPLI, ref dblCurInt, ref dblCost);
		}

		private void AddToPaymentArray(ref int lngIndex, ref double dblPrin, ref double dblPLI, ref double dblCurInt, ref double dblCost)
		{
			dblPayments[lngIndex, 0] += dblPrin;
			dblPayments[lngIndex, 1] += dblPLI;
			dblPayments[lngIndex, 2] += dblCurInt;
			dblPayments[lngIndex, 3] += dblCost;
		}
		// VBto upgrade warning: intRw As short	OnWriteFCConvert.ToInt32(
		private void HideSummaryRow(int intRw)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intCT;
				for (intCT = intRw; intCT <= 11; intCT++)
				{
					switch (intRw)
					{
						case 1:
							{
								lblSummaryPaymentType1.Visible = false;
								lblSumPrin1.Visible = false;
								lblSumInt1.Visible = false;
								lblSumCost1.Visible = false;
								lblSummaryTotal1.Visible = false;
								break;
							}
						case 2:
							{
								lblSummaryPaymentType2.Visible = false;
								lblSumPrin2.Visible = false;
								lblSumInt2.Visible = false;
								lblSumCost2.Visible = false;
								lblSummaryTotal2.Visible = false;
								break;
							}
						case 3:
							{
								lblSummaryPaymentType3.Visible = false;
								lblSumPrin3.Visible = false;
								lblSumInt3.Visible = false;
								lblSumCost3.Visible = false;
								lblSummaryTotal3.Visible = false;
								break;
							}
						case 4:
							{
								lblSummaryPaymentType4.Visible = false;
								lblSumPrin4.Visible = false;
								lblSumInt4.Visible = false;
								lblSumCost4.Visible = false;
								lblSummaryTotal4.Visible = false;
								break;
							}
						case 5:
							{
								lblSummaryPaymentType5.Visible = false;
								lblSumPrin5.Visible = false;
								lblSumInt5.Visible = false;
								lblSumCost5.Visible = false;
								lblSummaryTotal5.Visible = false;
								break;
							}
						case 6:
							{
								lblSummaryPaymentType6.Visible = false;
								lblSumPrin6.Visible = false;
								lblSumInt6.Visible = false;
								lblSumCost6.Visible = false;
								lblSummaryTotal6.Visible = false;
								break;
							}
						case 7:
							{
								lblSummaryPaymentType7.Visible = false;
								lblSumPrin7.Visible = false;
								lblSumInt7.Visible = false;
								lblSumCost7.Visible = false;
								lblSummaryTotal7.Visible = false;
								break;
							}
						case 8:
							{
								lblSummaryPaymentType8.Visible = false;
								lblSumPrin8.Visible = false;
								lblSumInt8.Visible = false;
								lblSumCost8.Visible = false;
								lblSummaryTotal8.Visible = false;
								break;
							}
						case 9:
							{
								lblSummaryPaymentType9.Visible = false;
								lblSumPrin9.Visible = false;
								lblSumInt9.Visible = false;
								lblSumCost9.Visible = false;
								lblSummaryTotal9.Visible = false;
								break;
							}
						case 10:
							{
								lblSummaryPaymentType10.Visible = false;
								lblSumPrin10.Visible = false;
								lblSumInt10.Visible = false;
								lblSumCost10.Visible = false;
								lblSummaryTotal10.Visible = false;
								break;
							}
						case 11:
							{
								lblSummaryPaymentType11.Visible = false;
								lblSumPrin11.Visible = false;
								lblSumInt11.Visible = false;
								lblSumCost11.Visible = false;
								lblSummaryTotal11.Visible = false;
								break;
							}
					}
					//end switch
				}
				if (!boolAdjustedSummary)
				{
					SetYearSummaryTop_2(lblSummaryPaymentType1.Top + (intRw * lblSummaryPaymentType1.Height) + 100 / 1440F);
					boolAdjustedSummary = true;
				}
				// set the size of the report footer depending on how many rows have been used
				// and move the error label to 300 pixels after the summary list
				// lblRTError.Top = lblSummaryPaymentType1.Top + (intRw * lblSummaryPaymentType1.Height) + 300
				// ReportFooter.Height = lblRTError.Top + lblRTError.Height + 100
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Hiding Summary Rows");
			}
		}

		private void SetYearSummaryTop_2(float lngTop)
		{
			SetYearSummaryTop(ref lngTop);
		}

		private void SetYearSummaryTop(ref float lngTop)
		{
			// this will start the year summary at the right height
			lblSummary.Top = lngTop;
			Line1.Y1 = lngTop + lblSummary.Height;
			Line1.Y2 = lngTop + lblSummary.Height;
			lblSummary1.Top = lngTop + lblSummary.Height;
			fldSumCount1.Top = lngTop + fldSumCount1.Height;
			// kk01092015 trocls-13
			fldSummary1.Top = lngTop + lblSummary.Height;
		}

		private void ReversePaymentsFromStatusArray(ref clsDRWrapper rsRev)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (rsRev.RecordCount() != 0)
				{
					rsRev.MoveFirst();
					while (!rsRev.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						if (Strings.UCase(rsRev.Get_Fields("Code")) == "P")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(6, rsRev.Get_Fields("Principal") * -1, rsRev.Get_Fields_Decimal("PreLienInterest") * -1, rsRev.Get_Fields_Decimal("CurrentInterest") * -1, rsRev.Get_Fields_Decimal("LienCost") * -1);
						}
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						else if ((Strings.UCase(rsRev.Get_Fields("Code")) == "X") || (Strings.UCase(rsRev.Get_Fields("Code")) == "S"))
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(9, rsRev.Get_Fields("Principal") * -1, rsRev.Get_Fields_Decimal("PreLienInterest") * -1, rsRev.Get_Fields_Decimal("CurrentInterest") * -1, rsRev.Get_Fields_Decimal("LienCost") * -1);
						}
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							else if (Strings.UCase(rsRev.Get_Fields("Code")) == "U")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(8, rsRev.Get_Fields("Principal") * -1, rsRev.Get_Fields_Decimal("PreLienInterest") * -1, rsRev.Get_Fields_Decimal("CurrentInterest") * -1, rsRev.Get_Fields_Decimal("LienCost") * -1);
						}
								// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
								else if (Strings.UCase(rsRev.Get_Fields("Code")) == "Y")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(10, rsRev.Get_Fields("Principal") * -1, rsRev.Get_Fields_Decimal("PreLienInterest") * -1, rsRev.Get_Fields_Decimal("CurrentInterest") * -1, rsRev.Get_Fields_Decimal("LienCost") * -1);
						}
									// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
									else if (Strings.UCase(rsRev.Get_Fields("Code")) == "C")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(2, rsRev.Get_Fields("Principal") * -1, rsRev.Get_Fields_Decimal("PreLienInterest") * -1, rsRev.Get_Fields_Decimal("CurrentInterest") * -1, rsRev.Get_Fields_Decimal("LienCost") * -1);
						}
										// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
										else if (Strings.UCase(rsRev.Get_Fields("Code")) == "A")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(1, rsRev.Get_Fields("Principal") * -1, rsRev.Get_Fields_Decimal("PreLienInterest") * -1, rsRev.Get_Fields_Decimal("CurrentInterest") * -1, rsRev.Get_Fields_Decimal("LienCost") * -1);
						}
											// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
											// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
											else if (Strings.UCase(rsRev.Get_Fields("Code")) == "D")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(3, rsRev.Get_Fields("Principal") * -1, rsRev.Get_Fields_Decimal("PreLienInterest") * -1, rsRev.Get_Fields_Decimal("CurrentInterest") * -1, rsRev.Get_Fields_Decimal("LienCost") * -1);
						}
												// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
												// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
												else if (Strings.UCase(rsRev.Get_Fields("Code")) == "I")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(4, rsRev.Get_Fields("Principal") * -1, rsRev.Get_Fields_Decimal("PreLienInterest") * -1, rsRev.Get_Fields_Decimal("CurrentInterest") * -1, rsRev.Get_Fields_Decimal("LienCost") * -1);
						}
													// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
													// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
													else if (Strings.UCase(rsRev.Get_Fields("Code")) == "L")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(5, rsRev.Get_Fields("Principal") * -1, rsRev.Get_Fields_Decimal("PreLienInterest") * -1, rsRev.Get_Fields_Decimal("CurrentInterest") * -1, rsRev.Get_Fields_Decimal("LienCost") * -1);
						}
														// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
														// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
														else if (Strings.UCase(rsRev.Get_Fields("Code")) == "3")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(0, rsRev.Get_Fields("Principal") * -1, rsRev.Get_Fields_Decimal("PreLienInterest") * -1, rsRev.Get_Fields_Decimal("CurrentInterest") * -1, rsRev.Get_Fields_Decimal("LienCost") * -1);
						}
															// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
															// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
															else if (Strings.UCase(rsRev.Get_Fields("Code")) == "R")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							AddToPaymentArray_242(7, rsRev.Get_Fields("Principal") * -1, rsRev.Get_Fields_Decimal("PreLienInterest") * -1, rsRev.Get_Fields_Decimal("CurrentInterest") * -1, rsRev.Get_Fields_Decimal("LienCost") * -1);
						}
						rsRev.MoveNext();
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Reversing Payment Counts");
			}
		}

		private void rptZeroBalances_Load(object sender, System.EventArgs e)
		{

		}

        private string GetZeroBalQuery()
        {
            return
                "SELECT * FROM BillingMaster WHERE ISNULL(LienRecordNumber,0) = 0 And (ISNULL(TaxDue1,0)+ISNULL(TaxDue2,0)+ISNULL(TaxDue3,0)+ISNULL(TaxDue4,0)) = ISNULL(PrincipalPaid,0)";
        }
	}
}