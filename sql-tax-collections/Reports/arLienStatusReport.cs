﻿using fecherFoundation;
using Global;
using SharedApplication.Enums;
using SharedApplication.TaxCollections.Enums;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using SharedApplication.TaxCollections;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCL0000
{
    /// <summary>
    /// Summary description for SectionReport1.
    /// </summary>
    public partial class arLienStatusReport : BaseSectionReport
    {
        private TaxCollectionStatusReportConfiguration reportConfiguration;
        clsDRWrapper rsData = new clsDRWrapper();
        clsDRWrapper rsMaster = new clsDRWrapper();
        string strSQL;
        double[] dblTotals = new double[4 + 1];
        double[] dblSummaryTotal = new double[5 + 1];
        int lngSummaryLineCT;
        int intTAType;
        // 0 = all, 1 = TA, 2 = non TA
        int lngLienCount;
        // kk01072015 trocls-13  Add count of accounts and liens
        Dictionary<object, object> dctAccounts = new Dictionary<object, object>();
        // kk01132014 trocls-13
        int[] lngArrLienCounts = new int[22000 + 1];
        // fldSummaryCount         'kk01072015 trocls-13  Show counts by year
        double[,] dblArrTotals = new double[22000 + 1, 5 + 1];
        // 0 - fldSummaryPrin1
        // 1 - fldSummaryPrinPaid1
        // 2 - fldSummaryPLI1
        // 3 - fldSummaryCosts1
        // 4 - fldSummaryCurrentInt1
        // 5 - fldSummaryTotal1
        bool boolSummaryOnly;
        // kk11262014 trocl-682   Dim dblPayments(10, 5)          As Double           '0 - 3, 1 - A, 2 - C, 3 - D, 4 - I, 5 - L, 6 - P, 7 - U, 8 - X, 9 - Y, 10 - Total
        // 0 - Principal, 1 - PreLienInterest, 2 - CurrentInterest, 3 - Costs
        public arLienStatusReport()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            if (_InstancePtr == null)
                _InstancePtr = this;
            this.Name = "Lien Breakdown";
        }

        public void SetReportOption(ref TaxCollectionStatusReportConfiguration reportConfig)
        {
            reportConfiguration = reportConfig;
        }

        public static arLienStatusReport InstancePtr
        {
            get
            {
                return (arLienStatusReport)Sys.GetInstance(typeof(arLienStatusReport));
            }
        }

        protected arLienStatusReport _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
            {
                _InstancePtr = null;
                Sys.ClearInstance(this);
            }
            if (disposing)
            {
                rsData.Dispose();
                rsMaster.Dispose();
            }
            base.Dispose(disposing);
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
        {
            // Write #1, Format(Time, "HH:mm:ss") & " - Start Fetch Data"
            eArgs.EOF = rsData.EndOfFile();
            // Write #1, Format(Time, "HH:mm:ss") & " - End Fetch Data"
        }

        private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
        {
            //if (KeyCode == vbKeyEscape)
            //{
            //	Close();
            //}
        }

        private void ActiveReport_ReportStart(object sender, EventArgs e)
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                // format the report
                lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
                lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "t");
                lblMuniName.Text = modGlobalConstants.Statics.MuniName;

                boolSummaryOnly = FCConvert.CBool(reportConfiguration.Options.ShowSummaryOnly);

                PrepIfTaxAcquired();

                SetReportHeader();
                SetupFields();
                // Open "TimeTrialLB.txt" For Output As #1
                // Write #1, Format(Time, "HH:mm:ss") & " - Start"
                // build the SQL statement
                strSQL = BuildSQL();
                // Write #1, Format(Time, "HH:mm:ss") & " - Setup"
                lngLienCount = 0;
                // kk01072015 trocls-13
                if (intTAType > 0)
                {
                    // kk01072015 trocls-22
                    rsMaster.OpenRecordset("SELECT RSAccount, TaxAcquired FROM Master where TaxAcquired = 1", modExtraModules.strREDatabase);
                }
                rsData.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
                SetFooterHeight(ref strSQL);
                // CreateSummaryTable strSQL
                // Write #1, Format(Time, "HH:mm:ss") & " - Open DB"
                for (int lngIndex = 1; lngIndex <= 100; lngIndex++)
                {
                    AddSummaryLineControls(ref lngSummaryLineCT);
                }
                AddSummaryLineControls(ref lngSummaryLineCT);
                lngSummaryLineCT = 0;
                return;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                frmWait.InstancePtr.Unload();
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Starting Report");
            }
        }

        /// <summary>
        /// Removes TaxAcquired from where clause since it is a field in RE so it will bomb your query.
        /// We just have to acknowledge the parameter and store it in intTAType for use later in BindFields
        /// </summary>
        private void PrepIfTaxAcquired()
        {
            switch (reportConfiguration.Filter.IsTaxAcquired)
            {
                case null:
                    intTAType = 0;

                    break;
                default:
                {
                    if (reportConfiguration.Filter.IsTaxAcquired.Value)
                    {
                        intTAType = 1;
                        reportConfiguration.WhereClause = reportConfiguration.WhereClause.Replace(" or  TaxAcquired = 1", "").Replace(" and  TaxAcquired = 1", "").Replace("TaxAcquired = 1", "");
                    }
                    else
                    {
                        if (!reportConfiguration.Filter.IsTaxAcquired.Value)
                        {
                            intTAType = 2;
                            reportConfiguration.WhereClause = reportConfiguration.WhereClause.Replace(" or  TaxAcquired = 0", "").Replace(" and  TaxAcquired = 0", "").Replace("TaxAcquired = 0", "");

                        }
                    }

                    break;
                }
            }
        }

        private void ActiveReport_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
            rsMaster.DisposeOf();
        }

        private void Detail_Format(object sender, EventArgs e)
        {
            BindFields();
        }

        private void PageHeader_Format(object sender, EventArgs e)
        {
            lblPage.Text = "Page " + this.PageNumber;
        }

        private string BuildSQL()
        {
            string BuildSQL = "";

            string strFields;
            string strTemp;
            string strTempWhere = "";
            if (!string.IsNullOrWhiteSpace(reportConfiguration.WhereClause))
            {
                strTempWhere = " WHERE " + reportConfiguration.WhereClause.Replace("Own1FullName", "Name1");
                strTempWhere = strTempWhere.Replace(" RateKey", " LienRec.RateKey");
            }
            else
            {
                strTempWhere = "";
            }
            // MAL@20080624: Add check for As Of Date
            // Tracker Reference: 14306
            if (reportConfiguration.Options.UseAsOfDate())
            {
                if (strTempWhere != "")
                {
                    strTempWhere += " AND ISNULL(DateCreated, '12/30/1899') <= '" + FCConvert.ToString(reportConfiguration.Options.AsOfDate) + "'";
                }
                else
                {
                    strTempWhere = " WHERE ISNULL(DateCreated, '12/30/1899') >= '" + FCConvert.ToString(reportConfiguration.Options.AsOfDate) + "'";
                }
            }
            else
            {
                strTempWhere = strTempWhere;
            }
            strFields = "Account, TransferFromBillingDateFirst, BillingYear, Name1, Name2, LienRec.InterestCharged AS InterestCharged, LienRec.RateKey AS RateKey, LienRec.InterestAppliedThroughDate AS InterestAppliedThroughDate, Principal, Interest, Costs, LienRec.PrincipalPaid AS PrincipalPaid, LienRec.InterestPaid AS InterestPaid, PLIPaid, MaturityFee, CostsPaid, TransferFromBillingDateLast, LienRec.ID AS LRN ";
            strTemp = reportConfiguration.SortClause.Replace("Own1FullName", "Name1");
            if (string.IsNullOrWhiteSpace(reportConfiguration.WhereClause))
            {
                if (Strings.Trim(strTemp) != "")
                {
                    strTemp = reportConfiguration.SortClause.Replace("Own1FullName", "Name1");
                    strSQL = "SELECT " + strFields + " FROM LienRec INNER JOIN BillingMaster ON LienRec.ID = BillingMaster.LienRecordNumber ORDER BY " + strTemp;
                    // Principal > LienRec.PrincipalPaid
                }
                else
                {
                    strSQL = "SELECT " + strFields + " FROM LienRec INNER JOIN BillingMaster ON LienRec.ID = BillingMaster.LienRecordNumber";
                    // WHERE Principal > LienRec.PrincipalPaid"
                }
            }
            else
            {
                if (Strings.Trim(strTemp) != "")
                {
                    strSQL = "SELECT " + strFields + " FROM LienRec INNER JOIN BillingMaster ON LienRec.ID = BillingMaster.LienRecordNumber " + strTempWhere + " ORDER BY " + strTemp;
                }
                else
                {
                    strSQL = "SELECT " + strFields + " FROM LienRec INNER JOIN BillingMaster ON LienRec.ID = BillingMaster.LienRecordNumber " + strTempWhere;
                }
            }
            BuildSQL = strSQL;
            return BuildSQL;
        }

        private void BindFields()
        {
            // this will fill the fields in
            clsDRWrapper rsPayment = new clsDRWrapper();
            double dblCurInt = 0;
            double dblCurIntCHG = 0;
            double dblTotal = 0;
            double dblAmt = 0;
            double dblSum = 0;
            double dblPaymentRecieved = 0;
            double dblIntPaid = 0;
            double dblPLI = 0;
            double dblCostPaid = 0;
            double dblTempTotal;
        // Write #1, Format(Time, "HH:mm:ss") & " - Start Bind Fields"
        TRYAGAIN:

            try
            {

                if (rsData.EndOfFile() != true)
                {
                    if (reportConfiguration.Filter.TranCodeMax.HasValue)
                    {
                        // if there is something in trancode then check this
                        if (!modCLCalculations.CheckTranCode_78(reportConfiguration.BillingType == PropertyTaxBillType.Real, FCConvert.ToInt32(rsData.Get_Fields("Account")), reportConfiguration.Filter.TranCodeMin.Value, reportConfiguration.Filter.TranCodeMax.Value))
                        {
                            rsData.MoveNext();
                            goto TRYAGAIN;
                        }
                    }
                    if (intTAType > 0)
                    {
                        switch (intTAType)
                        {

                            case 1:
                                {
                                    // TA
                                    // if the account is in the recordset then it is TA
                                    rsMaster.FindFirstRecord("RSAccount", rsData.Get_Fields("Account"));
                                    if (rsMaster.NoMatch)
                                    {
                                        // If Not rsMaster.Fields("TaxAcquired") Then
                                        rsData.MoveNext();
                                        goto TRYAGAIN;
                                    }
                                    break;
                                }
                            case 2:
                                {
                                    // Non TA
                                    // if the account is NOT in the recordset then it is TA
                                    rsMaster.FindFirstRecord("RSAccount", rsData.Get_Fields("Account"));
                                    if (!rsMaster.NoMatch)
                                    {
                                        // If rsMaster.Fields("TaxAcquired") Then
                                        rsData.MoveNext();
                                        goto TRYAGAIN;
                                    }
                                    break;
                                }
                        } //end switch
                          // End If
                    }
                    // Write #1, Format(Time, "HH:mm:ss") & " - New Account - " & rsData.Fields("Account")
                    //FC:FINAL:BCU - #i181 - add convert to string
                    //fldAccount.Text = rsData.Get_Fields("Account");
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    fldAccount.Text = FCConvert.ToString(rsData.Get_Fields("Account"));
                    // Write #1, Format(Time, "HH:mm:ss") & " - New Account - " & rsData.Fields("Account")
                    fldName.Text = rsData.Get_Fields_String("Name1");
                    // GetStatusName(rsData)   'Trim(rsData.Fields("Name1") & " " & rsData.Fields("Name2"))
                    // Write #1, Format(Time, "HH:mm:ss") & " - New Account - " & rsData.Fields("Account")
                    fldYear.Text = modExtraModules.FormatYear(FCConvert.ToString(rsData.Get_Fields_Int32("BillingYear")));
                    dblPaymentRecieved = 0;
                    dblIntPaid = 0;
                    dblPLI = 0;
                    dblCostPaid = 0;
                    dblAmt = 0;
                    // Write #1, Format(Time, "HH:mm:ss") & " - Open Payment Rec"
                    if (reportConfiguration.Options.UseAsOfDate())
                    {
                        // get the values from the payment records
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Field [LRN] not found!! (maybe it is an alias?)
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Field [LRN] not found!! (maybe it is an alias?)
                        rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE Account = " + FCConvert.ToString(rsData.Get_Fields("Account")) + " AND [Year] = " + FCConvert.ToString(rsData.Get_Fields_Int32("BillingYear")) + " AND BillCode = 'L' AND BillKey = " + FCConvert.ToString(rsData.Get_Fields("LRN")) + " AND ISNULL(RecordedTransactionDate, '12/30/1899') <= '" + FCConvert.ToString(reportConfiguration.Options.AsOfDate) + "'");
                    }
                    else
                    {
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Field [LRN] not found!! (maybe it is an alias?)
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Field [LRN] not found!! (maybe it is an alias?)
                        rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE Account = " + FCConvert.ToString(rsData.Get_Fields("Account")) + " AND [Year] = " + FCConvert.ToString(rsData.Get_Fields_Int32("BillingYear")) + " AND BillCode = 'L' AND BillKey = " + FCConvert.ToString(rsData.Get_Fields("LRN")));
                    }
                    // Write #1, Format(Time, "HH:mm:ss") & " - Calc Payment Rec"
                    while (!rsPayment.EndOfFile())
                    {
                        // Debug.Print rsPayment.Fields("ID") & " - " & rsPayment.Fields("RecordedTransactionDate") & " - "; rsPayment.Fields("Principal") & " - " & rsPayment.Fields("Reference")
                        // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                        dblPaymentRecieved += Conversion.Val(rsPayment.Get_Fields("Principal"));
                        dblIntPaid += Conversion.Val(rsPayment.Get_Fields_Decimal("CurrentInterest"));
                        dblPLI += Conversion.Val(rsPayment.Get_Fields_Decimal("PreLienInterest"));
                        dblCostPaid += Conversion.Val(rsPayment.Get_Fields_Decimal("LienCost"));
                        // kk11262014 trocl-682   The payment summary isn't even used in this report
                        // Select Case UCase(rsPayment.Fields("Code"))
                        // Case "P"
                        // AddToPaymentArray 6, rsPayment.Fields("Principal"), rsPayment.Fields("PreLienInterest"), rsPayment.Fields("CurrentInterest"), rsPayment.Fields("LienCost")
                        // Case "X", "S"
                        // AddToPaymentArray 9, rsPayment.Fields("Principal"), rsPayment.Fields("PreLienInterest"), rsPayment.Fields("CurrentInterest"), rsPayment.Fields("LienCost")
                        // Case "U"
                        // AddToPaymentArray 8, rsPayment.Fields("Principal"), rsPayment.Fields("PreLienInterest"), rsPayment.Fields("CurrentInterest"), rsPayment.Fields("LienCost")
                        // Case "Y"
                        // AddToPaymentArray 10, rsPayment.Fields("Principal"), rsPayment.Fields("PreLienInterest"), rsPayment.Fields("CurrentInterest"), rsPayment.Fields("LienCost")
                        // Case "C"
                        // AddToPaymentArray 2, rsPayment.Fields("Principal"), rsPayment.Fields("PreLienInterest"), rsPayment.Fields("CurrentInterest"), rsPayment.Fields("LienCost")
                        // Case "A"
                        // AddToPaymentArray 1, rsPayment.Fields("Principal"), rsPayment.Fields("PreLienInterest"), rsPayment.Fields("CurrentInterest"), rsPayment.Fields("LienCost")
                        // Case "D"
                        // AddToPaymentArray 3, rsPayment.Fields("Principal"), rsPayment.Fields("PreLienInterest"), rsPayment.Fields("CurrentInterest"), rsPayment.Fields("LienCost")
                        // Case "I"
                        // AddToPaymentArray 4, rsPayment.Fields("Principal"), rsPayment.Fields("PreLienInterest"), rsPayment.Fields("CurrentInterest"), rsPayment.Fields("LienCost")
                        // Case "L"
                        // AddToPaymentArray 5, rsPayment.Fields("Principal"), rsPayment.Fields("PreLienInterest"), rsPayment.Fields("CurrentInterest"), rsPayment.Fields("LienCost")
                        // Case "3"
                        // AddToPaymentArray 0, rsPayment.Fields("Principal"), rsPayment.Fields("PreLienInterest"), rsPayment.Fields("CurrentInterest"), rsPayment.Fields("LienCost")
                        // Case "R"
                        // AddToPaymentArray 7, rsPayment.Fields("Principal"), rsPayment.Fields("PreLienInterest"), rsPayment.Fields("CurrentInterest"), rsPayment.Fields("LienCost")
                        // End Select
                        // XXXXXXXXXXXXXX
                        rsPayment.MoveNext();
                    }
                    // Write #1, Format(Time, "HH:mm:ss") & " - End Payment Calc"
                    if (!reportConfiguration.Options.UseAsOfDate())
                    {
                        // Original Principal
                        // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                        dblAmt = Conversion.Val(rsData.Get_Fields("Principal"));
                        fldPrincipal.Text = Strings.Format(dblAmt, "#,##0.00");
                        // If rsData.Fields("BillingYear") = "20051" Then
                        // MsgBox "hi"
                        // End If
                        dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 0] += dblAmt;
                        dblTotals[4] += dblAmt;
                        // original principal total
                        // this does not account into the line sum
                        // Principal Due
                        // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                        dblAmt = Conversion.Val(rsData.Get_Fields("Principal") - rsData.Get_Fields_Decimal("PrincipalPaid"));
                        fldPrincipalDue.Text = Strings.Format(dblAmt, "#,##0.00");
                        dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 1] += dblAmt;
                        dblTotals[0] += dblAmt;
                        dblSum = dblAmt;
                        // Interest Due
                        // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
                        dblAmt = Conversion.Val(rsData.Get_Fields("Interest") - rsData.Get_Fields("PLIPaid"));
                        fldPLInt.Text = Strings.Format(dblAmt, "#,##0.00");
                        dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 2] += dblAmt;
                        dblTotals[1] += dblAmt;
                        dblSum += dblAmt;
                        // Costs Due
                        // TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
                        dblAmt = Conversion.Val(rsData.Get_Fields("Costs") - rsData.Get_Fields("MaturityFee") - rsData.Get_Fields_Decimal("CostsPaid"));
                        fldCosts.Text = Strings.Format(dblAmt, "#,##0.00");
                        dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 3] += dblAmt;
                        dblTotals[2] += dblAmt;
                        dblSum += dblAmt;
                        dblTotal = modCLCalculations.CalculateAccountCLLien2(rsData, DateTime.Now, ref dblCurInt, ref dblCurIntCHG);
                        dblCurInt += -Conversion.Val(rsData.Get_Fields_Decimal("InterestPaid") + rsData.Get_Fields_Decimal("InterestCharged"));
                        // - dblCurIntCHG
                    }
                    else
                    {
                        // Original Principal
                        // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                        dblAmt = Conversion.Val(rsData.Get_Fields("Principal"));
                        fldPrincipal.Text = Strings.Format(dblAmt, "#,##0.00");
                        // If rsData.Fields("BillingYear") = "20051" Then
                        // MsgBox "hi"
                        // End If
                        dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 0] += dblAmt;
                        dblTotals[4] += dblAmt;
                        // original principal total
                        // this does not account into the line sum
                        // Principal Due
                        // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                        dblAmt = Conversion.Val(rsData.Get_Fields("Principal")) - dblPaymentRecieved;
                        fldPrincipalDue.Text = Strings.Format(dblAmt, "#,##0.00");
                        dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 1] += dblAmt;
                        dblTotals[0] += dblAmt;
                        dblSum = dblAmt;
                        // Interest Due
                        // If dblIntPaid > 0 And (rsData.Fields("Interest") - dblPLI) > 0 Then
                        // If dblIntPaid >= rsData.Fields("Interest") - dblPLI Then
                        // dblIntPaid = dblIntPaid - (rsData.Fields("Interest") - dblPLI)
                        // dblPLI = rsData.Fields("Interest")
                        // Else
                        // dblPLI = dblPLI + dblIntPaid
                        // dblIntPaid = 0
                        // End If
                        // ElseIf dblIntPaid < 0 And (rsData.Fields("Interest") - dblPLI) < 0 Then
                        // If dblIntPaid >= rsData.Fields("Interest") - dblPLI Then
                        // dblIntPaid = dblIntPaid - (rsData.Fields("Interest") - dblPLI)
                        // dblPLI = rsData.Fields("Interest")
                        // Else
                        // dblPLI = dblPLI + dblIntPaid
                        // dblIntPaid = 0
                        // End If
                        // End If
                        // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                        dblAmt = Conversion.Val(rsData.Get_Fields("Interest")) - dblPLI;
                        fldPLInt.Text = Strings.Format(dblAmt, "#,##0.00");
                        dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 2] += dblAmt;
                        dblTotals[1] += dblAmt;
                        dblSum += dblAmt;
                        // Costs Due
                        // TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
                        dblAmt = Conversion.Val(rsData.Get_Fields("Costs")) - dblCostPaid;
                        fldCosts.Text = Strings.Format(dblAmt, "#,##0.00");
                        dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 3] += dblAmt;
                        dblTotals[2] += dblAmt;
                        dblSum += dblAmt;
                        // dblTotal = CalculateAccountCLLien(rsData, gdtStatusListAsOfDate, dblCurInt)
                        // If DateDiff("D", rsData.Fields("InterestAppliedThroughDate"), gdtStatusListAsOfDate) > 0 Then
                        // 
                        // Else
                        // dblCurInt = 0
                        // End If
                        // MAL@20080107; Changed to calculate current interest correctly
                        // Tracker Reference: 11860
                        dblTotal = dblSum;
                        dblTempTotal = modCLCalculations.CalculateAccountCLLien2(rsData, reportConfiguration.Options.AsOfDate, ref dblCurInt, ref dblCurIntCHG);
                        dblCurInt -= dblIntPaid;
                        // dblTotal = dblSum
                        // dblCurInt = dblCurInt - dblIntPaid
                    }
                    if (dblSum == 0 && dblTotal == 0)
                    {
                        // reverse all the the effects on the sums
                        if (rsData.EndOfFile())
                        {
                            ReverseLastRecord(dblPaymentRecieved, dblPLI, dblCostPaid);
                            dblSum = 0;
                            dblTotal = 0;
                        }
                        else
                        {
                            dblAmt = FCConvert.ToDouble(fldPrincipal.Text);
                            dblTotals[4] -= dblAmt;
                            // If rsData.Fields("BillingYear") = "20051" Then
                            // MsgBox "hi"
                            // End If
                            dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 0] -= dblAmt;
                            dblAmt = FCConvert.ToDouble(fldPrincipalDue.Text);
                            dblTotals[0] -= dblAmt;
                            dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 1] -= dblAmt;
                            dblAmt = FCConvert.ToDouble(fldPLInt.Text);
                            dblTotals[1] -= dblAmt;
                            dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 2] -= dblAmt;
                            dblAmt = FCConvert.ToDouble(fldCosts.Text);
                            dblTotals[2] -= dblAmt;
                            dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 3] -= dblAmt;
                            fldPrincipal.Text = "0.00";
                            fldPrincipalDue.Text = "0.00";
                            fldPLInt.Text = "0.00";
                            fldCosts.Text = "0.00";
                            dblCurInt = 0;
                            dblSum = 0;
                            dblTotal = 0;
                        }
                        rsData.MoveNext();
                        goto TRYAGAIN;
                    }
                    if (rsData.Get_Fields_Decimal("InterestPaid") > (-1 * rsData.Get_Fields_Decimal("InterestCharged")))
                    {
                        dblCurInt += -Conversion.Val(rsData.Get_Fields_Decimal("InterestCharged") + rsData.Get_Fields_Decimal("InterestPaid"));
                    }
                    fldCurrentInt.Text = Strings.Format(dblCurInt, "#,##0.00");
                    dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 4] += dblCurInt;
                    dblTotals[3] += dblCurInt;
                    dblSum += dblCurInt;
                    // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
                    if (modGlobal.Round(dblTotal, 2) == modGlobal.Round(Conversion.Val(rsData.Get_Fields("Principal") - rsData.Get_Fields_Decimal("PrincipalPaid") + rsData.Get_Fields("Interest") - rsData.Get_Fields_Decimal("InterestCharged") - rsData.Get_Fields_Decimal("InterestPaid") - rsData.Get_Fields("PLIPaid") + rsData.Get_Fields("Costs") - rsData.Get_Fields_Decimal("CostsPaid")) + dblCurInt, 2))
                    {
                        fldTotal.Text = Strings.Format(dblTotal, "#,##0.00");
                        dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 5] += dblTotal;
                    }
                    else
                    {
                        // I used the star to show that it did not match exactly, for debugging/testing purposes
                        // fldTotal.Text = "*" & Format(rsData.Fields("Principal") + rsData.Fields("Interest") + rsData.Fields("Costs") + dblCurInt, "#,##0.00")
                        fldTotal.Text = Strings.Format(dblSum, "#,##0.00");
                        dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 5] += dblSum;
                    }
                    // kk0107205 trocls-13  Keep count of accounts and Bills by Year
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    if (!dctAccounts.ContainsKey(FCConvert.ToInt32(Conversion.Val(rsData.Get_Fields("Account")))))
                    {
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        dctAccounts.Add(FCConvert.ToInt32(Conversion.Val(rsData.Get_Fields("Account"))), rsData.Get_Fields_String("Name1"));
                    }
                    else
                    {
                        // Count duplicats account number with different name??
                    }
                    lngLienCount += 1;
                    lngArrLienCounts[rsData.Get_Fields_Int32("BillingYear")] += 1;
                    // Write #1, Format(Time, "HH:mm:ss") & " - End Bind Fields"
                    rsData.MoveNext();
                    // Write #1, Format(Time, "HH:mm:ss") & " - After Move Next"
                }
                else
                {
                    fldAccount.Visible = false;
                    fldYear.Visible = false;
                    fldName.Visible = false;
                    fldPrincipal.Visible = false;
                    fldPrincipalDue.Visible = false;
                    fldPLInt.Visible = false;
                    fldCosts.Visible = false;
                    fldCurrentInt.Visible = false;
                    fldTotal.Visible = false;
                }
            }
            finally
            {
                rsPayment.DisposeOf();
            }
        }

        private void SetupFields()
        {
            // this will set up the fields in the correct format depending on which fields are choosen on the frmCustomReport
            if (boolSummaryOnly)
            {
                lblAccount.Visible = false;
                lblName.Visible = false;
                lblYear.Visible = false;
                fldAccount.Visible = false;
                fldYear.Visible = false;
                fldName.Visible = false;
                fldPrincipal.Visible = false;
                fldPrincipalDue.Visible = false;
                fldPLInt.Visible = false;
                fldCosts.Visible = false;
                fldCurrentInt.Visible = false;
                fldTotal.Visible = false;
                Line1.Visible = false;
                Line2.Visible = false;
                Detail.Height = 0;
                return;
            }
        }

        private void SetReportHeader()
        {
            int intCT;
            string strTemp = "";
            string strComma = "";
            //FC:FINAL:KS: #without nr.:fix Exit For inside switch/case statement
            bool endFor = false;
            var filter = reportConfiguration.Filter;
            if (reportConfiguration.Filter.AccountMin.HasValue || reportConfiguration.Filter.AccountMax.HasValue)
            {
                if (reportConfiguration.Filter.AccountMax.HasValue)
                {
                    if (reportConfiguration.Filter.AccountMin.HasValue)
                    {
                        if (reportConfiguration.Filter.AccountMin == reportConfiguration.Filter.AccountMax)
                        {
                            strTemp += "Account: " + reportConfiguration.Filter.AccountMin.ToString();
                        }
                        else
                        {
                            strTemp += "Account: " + reportConfiguration.Filter.AccountMin.ToString() + " To " +
                                       reportConfiguration.Filter.AccountMax.ToString();
                        }
                    }
                    else
                    {
                        strTemp += "Below Account: " + reportConfiguration.Filter.AccountMax.Value.ToString();
                    }
                }
                else
                {
                    strTemp += "Above Account: " + reportConfiguration.Filter.AccountMin.ToString();
                }
            }
            if (!String.IsNullOrWhiteSpace(filter.NameMin) || !String.IsNullOrWhiteSpace(filter.NameMax))
            {
                strTemp += strComma;
                if (!String.IsNullOrWhiteSpace(filter.NameMin))
                {
                    if (!String.IsNullOrWhiteSpace(filter.NameMax))
                    {
                        strTemp += " Name: " + filter.NameMin + " To " + filter.NameMax;
                    }
                    else
                    {
                        strTemp += " Name: " + filter.NameMin;
                    }
                }
                else
                {
                    strTemp += " Name: " + filter.NameMax;
                }
                strComma = "\r\nv";
            }

            if (filter.TaxYearMin.HasValue || filter.TaxYearMax.HasValue)
            {
                strTemp += strComma;
                if (filter.TaxYearMin.HasValue)
                {
                    if (filter.TaxYearMax.HasValue)
                    {
                        strTemp += " Tax Year: " + filter.TaxYearMin.ToString() + " To " + filter.TaxYearMax.ToString();
                    }
                    else
                    {
                        strTemp += " Tax Year: " + filter.TaxYearMin.ToString();
                    }
                }
                else
                {
                    strTemp += " Tax Year: " + filter.TaxYearMax.ToString();
                }
                strComma = "\r\n";
            }

            if (filter.BalanceDueComparisonType.HasValue && filter.BalanceDueAmount.HasValue)
            {
                strTemp += strComma;
                strTemp += " Balance Due " + filter.BalanceDueComparisonType.Value.ToSymbolString() + " " +
                           filter.BalanceDueAmount.Value.ToString();
                strComma = "\r\n";
            }

            // MAL@20080813: Add check for Tax Acquired status
            // Tracker Reference: 11805
            if (reportConfiguration.Filter.IsTaxAcquired.HasValue)
            {
                if (Strings.Trim(strTemp) == "")
                {
                    if (reportConfiguration.Filter.IsTaxAcquired.Value)
                    {
                        strTemp = "Tax Acquired";
                    }
                    else
                    {
                        strTemp = "Non-Tax Acquired";
                    }
                }
                else
                {
                    if (reportConfiguration.Filter.IsTaxAcquired.Value)
                    {
                        strTemp = ", Tax Acquired";
                    }
                    else
                    {
                        strTemp = ", Non-Tax Acquired";
                    }
                }
            }
            if (reportConfiguration.Options.UseAsOfDate())
            {
                strTemp += "\r\n" + " As Of: " + Strings.Format(reportConfiguration.Options.AsOfDate, "MM/dd/yyyy");
            }
            else
            {
                strTemp += "\r\n" + " As Of: " + Strings.Format(DateTime.Today, "MM/dd/yyyy");
            }

            lblReportType.Text = strTemp;
        }

        private void CreateSummaryTable()
        {
            // strSQLString As String)
            int lngIndex;
            for (lngIndex = 19800; lngIndex <= 22000; lngIndex++)
            {
                if (dblArrTotals[lngIndex, 5] != 0)
                {
                    AddSummaryLine(ref lngIndex, ref dblArrTotals[lngIndex, 0], ref dblArrTotals[lngIndex, 1], ref dblArrTotals[lngIndex, 2], ref dblArrTotals[lngIndex, 3], ref dblArrTotals[lngIndex, 4], ref dblArrTotals[lngIndex, 5], ref lngArrLienCounts[lngIndex], ref lngSummaryLineCT);
                }
            }
            AddSummaryLine_2(-1, dblSummaryTotal[0], dblSummaryTotal[1], dblSummaryTotal[2], dblSummaryTotal[3], dblSummaryTotal[4], dblSummaryTotal[5], lngLienCount, ref lngSummaryLineCT);
        }

        private void AddSummaryLine_2(int lngYear, double dblAmount0, double dblAmount1, double dblAmount2, double dblAmount3, double dblAmount4, double dblAmount5, int lngLnCnt, ref int lngCT)
        {
            AddSummaryLine(ref lngYear, ref dblAmount0, ref dblAmount1, ref dblAmount2, ref dblAmount3, ref dblAmount4, ref dblAmount5, ref lngLnCnt, ref lngCT);
        }

        private void AddSummaryLine(ref int lngYear, ref double dblAmount0, ref double dblAmount1, ref double dblAmount2, ref double dblAmount3, ref double dblAmount4, ref double dblAmount5, ref int lngLnCnt, ref int lngCT)
        {
            GrapeCity.ActiveReports.SectionReportModel.TextBox obNew;
            string strTemp = "";
            // this will add another per diem line in the report footer
            if (lngCT == 0)
            {
                lngCT = 1;
                fldSummaryTotal1.Visible = true;
                lblSummaryYear1.Visible = true;
                lblLienSummary.Visible = true;
                lnSummaryHeader.Visible = true;
                lnSummaryTotal.Visible = true;
                fldSummaryPrin1.Visible = true;
                fldSummaryPrinPaid1.Visible = true;
                fldSummaryPLI1.Visible = true;
                fldSummaryCosts1.Visible = true;
                fldSummaryCurrentInt1.Visible = true;
                fldSummaryCount.Visible = true;
                // kk01072015
                fldSummaryTotal1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
                fldSummaryPrin1.Text = Strings.Format(dblAmount0, "#,##0.00");
                fldSummaryPrinPaid1.Text = Strings.Format(dblAmount1, "#,##0.00");
                fldSummaryPLI1.Text = Strings.Format(dblAmount2, "#,##0.00");
                fldSummaryCosts1.Text = Strings.Format(dblAmount3, "#,##0.00");
                fldSummaryCurrentInt1.Text = Strings.Format(dblAmount4, "#,##0.00");
                fldSummaryTotal1.Text = Strings.Format(dblAmount5, "#,##0.00");
                if (lngYear > 0)
                {
                    lblSummaryYear1.Text = modExtraModules.FormatYear(lngYear.ToString());
                }
                else
                {
                    lblSummaryYear1.Text = "";
                }
                fldSummaryCount.Text = lngLnCnt.ToString();
                // kk01072015
                dblSummaryTotal[0] += dblAmount0;
                dblSummaryTotal[1] += dblAmount1;
                dblSummaryTotal[2] += dblAmount2;
                dblSummaryTotal[3] += dblAmount3;
                dblSummaryTotal[4] += dblAmount4;
                dblSummaryTotal[5] += dblAmount5;
            }
            else
            {
                // increment the number of rows of fields
                lngCT += 1;
                // 0 - fldSummaryPrin1
                obNew = ReportFooter.Controls["fldSummaryPrin" + FCConvert.ToString(lngCT)] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
                obNew.Top = fldSummaryTotal1.Top + ((lngCT - 1) * fldSummaryTotal1.Height);
                obNew.Left = fldSummaryPrin1.Left;
                obNew.Width = fldSummaryPrin1.Width;
                obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
                strTemp = obNew.Font.ToString();
                obNew.Font = fldSummaryTotal1.Font;
                // this sets the font to the same as the field that is already created
                obNew.Text = Strings.Format(dblAmount0, "#,##0.00");
                dblSummaryTotal[0] += dblAmount0;
                // 1 - fldSummaryPrinPaid1
                obNew = ReportFooter.Controls["fldSummaryPrinPaid" + FCConvert.ToString(lngCT)] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
                obNew.Top = fldSummaryTotal1.Top + ((lngCT - 1) * fldSummaryTotal1.Height);
                obNew.Left = fldSummaryPrinPaid1.Left;
                obNew.Width = fldSummaryPrinPaid1.Width;
                obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
                strTemp = obNew.Font.ToString();
                obNew.Font = fldSummaryTotal1.Font;
                // this sets the font to the same as the field that is already created
                obNew.Text = Strings.Format(dblAmount1, "#,##0.00");
                dblSummaryTotal[1] += dblAmount1;
                // 2 - fldSummaryPLI1
                obNew = ReportFooter.Controls["fldSummaryPLI" + FCConvert.ToString(lngCT)] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
                obNew.Top = fldSummaryTotal1.Top + ((lngCT - 1) * fldSummaryTotal1.Height);
                obNew.Left = fldSummaryPLI1.Left;
                obNew.Width = fldSummaryPLI1.Width;
                obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
                strTemp = obNew.Font.ToString();
                obNew.Font = fldSummaryTotal1.Font;
                // this sets the font to the same as the field that is already created
                obNew.Text = Strings.Format(dblAmount2, "#,##0.00");
                dblSummaryTotal[2] += dblAmount2;
                // 3 - fldSummaryCosts1
                obNew = ReportFooter.Controls["fldSummaryCosts" + FCConvert.ToString(lngCT)] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
                obNew.Top = fldSummaryTotal1.Top + ((lngCT - 1) * fldSummaryTotal1.Height);
                obNew.Left = fldSummaryCosts1.Left;
                obNew.Width = fldSummaryCosts1.Width;
                obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
                strTemp = obNew.Font.ToString();
                obNew.Font = fldSummaryTotal1.Font;
                // this sets the font to the same as the field that is already created
                obNew.Text = Strings.Format(dblAmount3, "#,##0.00");
                dblSummaryTotal[3] += dblAmount3;
                // 4 - fldSummaryCurrentInt1
                obNew = ReportFooter.Controls["fldSummaryCurrentInt" + FCConvert.ToString(lngCT)] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
                obNew.Top = fldSummaryTotal1.Top + ((lngCT - 1) * fldSummaryTotal1.Height);
                obNew.Left = fldSummaryCurrentInt1.Left;
                obNew.Width = fldSummaryCurrentInt1.Width;
                obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
                strTemp = obNew.Font.ToString();
                obNew.Font = fldSummaryTotal1.Font;
                // this sets the font to the same as the field that is already created
                obNew.Text = Strings.Format(dblAmount4, "#,##0.00");
                dblSummaryTotal[4] += dblAmount4;
                // 5 - fldSummaryTotal1
                // add a field
                obNew = ReportFooter.Controls["fldSummaryTotal" + FCConvert.ToString(lngCT)] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
                obNew.Top = fldSummaryTotal1.Top + ((lngCT - 1) * fldSummaryTotal1.Height);
                obNew.Left = fldSummaryTotal1.Left;
                obNew.Width = fldSummaryTotal1.Width;
                obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
                strTemp = obNew.Font.ToString();
                obNew.Font = fldSummaryTotal1.Font;
                // this sets the font to the same as the field that is already created
                obNew.Text = Strings.Format(dblAmount5, "#,##0.00");
                dblSummaryTotal[5] += dblAmount5;
                // Lien Count fldSummaryCount   'kk01072015
                obNew = ReportFooter.Controls["fldSummaryCount" + FCConvert.ToString(lngCT)] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
                obNew.Top = fldSummaryTotal1.Top + ((lngCT - 1) * fldSummaryTotal1.Height);
                obNew.Left = fldSummaryCount.Left;
                obNew.Width = fldSummaryCount.Width;
                obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
                strTemp = obNew.Font.ToString();
                obNew.Font = fldSummaryTotal1.Font;
                // this sets the font to the same as the field that is already created
                obNew.Text = lngLnCnt.ToString();
                // year
                // add a label
                GrapeCity.ActiveReports.SectionReportModel.Label obLabel = ReportFooter.Controls["lblSummaryYear" + FCConvert.ToString(lngCT)] as GrapeCity.ActiveReports.SectionReportModel.Label;
                obLabel.Top = lblSummaryYear1.Top + ((lngCT - 1) * lblSummaryYear1.Height);
                obLabel.Left = lblSummaryYear1.Left;
                strTemp = obLabel.Font.ToString();
                obLabel.Font = lblSummaryYear1.Font;
                // this sets the font to the same as the field that is already created
                if (lngYear == -1)
                {
                    obLabel.Text = "Total:";
                    lnSummaryTotal.Y1 = obNew.Top;
                    lnSummaryTotal.Y2 = obNew.Top;
                }
                else
                {
                    obLabel.Text = modExtraModules.FormatYear(lngYear.ToString());
                }
            }
        }

        private void AddSummaryLineControls(ref int lngCT)
        {
            GrapeCity.ActiveReports.SectionReportModel.TextBox obNew;
            if (lngCT == 0)
            {
                lngCT = 1;
            }
            else
            {
                // increment the number of rows of fields
                lngCT += 1;
                obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
                obNew.Name = "fldSummaryPrin" + FCConvert.ToString(lngCT);
                ReportFooter.Controls.Add(obNew);
                obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
                obNew.Name = "fldSummaryPrinPaid" + FCConvert.ToString(lngCT);
                ReportFooter.Controls.Add(obNew);
                obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
                obNew.Name = "fldSummaryPLI" + FCConvert.ToString(lngCT);
                ReportFooter.Controls.Add(obNew);
                obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
                obNew.Name = "fldSummaryCosts" + FCConvert.ToString(lngCT);
                ReportFooter.Controls.Add(obNew);
                obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
                obNew.Name = "fldSummaryCurrentInt" + FCConvert.ToString(lngCT);
                ReportFooter.Controls.Add(obNew);
                obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
                obNew.Name = "fldSummaryTotal" + FCConvert.ToString(lngCT);
                ReportFooter.Controls.Add(obNew);
                obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
                obNew.Name = "fldSummaryCount" + FCConvert.ToString(lngCT);
                ReportFooter.Controls.Add(obNew);
                GrapeCity.ActiveReports.SectionReportModel.Label obLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
                obLabel.Name = "lblSummaryYear" + FCConvert.ToString(lngCT);
                ReportFooter.Controls.Add(obLabel);
            }
        }

        private void ReportFooter_Format(object sender, EventArgs e)
        {
            // fill the totals line in
            // kk01092015 trocls-13  Add count of accounts
            if (dctAccounts.Count > 1)
            {
                fldAcctCount.Text = FCConvert.ToString(dctAccounts.Count) + " Accounts";
            }
            else if (dctAccounts.Count == 1)
            {
                fldAcctCount.Text = FCConvert.ToString(dctAccounts.Count) + " Account";
            }
            else
            {
                fldAcctCount.Text = "";
            }
            // kk01082015 trocls-13  Add count of Bills
            if (lngLienCount > 1)
            {
                lblTotals.Text = "Total for " + FCConvert.ToString(lngLienCount) + " Bills:";
            }
            else if (lngLienCount == 1)
            {
                lblTotals.Text = "Total for " + FCConvert.ToString(lngLienCount) + " Bill:";
            }
            else
            {
                lblTotals.Text = "No Bills";
            }
            fldTotalPrincipal.Text = Strings.Format(dblTotals[4], "#,##0.00");
            fldTotalPrincipalDue.Text = Strings.Format(dblTotals[0], "#,##0.00");
            fldTotalPLInt.Text = Strings.Format(dblTotals[1], "#,##0.00");
            fldTotalCosts.Text = Strings.Format(dblTotals[2], "#,##0.00");
            fldTotalCurrentInt.Text = Strings.Format(dblTotals[3], "#,##0.00");
            fldTotalTotal.Text = Strings.Format(dblTotals[0] + dblTotals[1] + dblTotals[2] + dblTotals[3], "#,##0.00");
            CreateSummaryTable();
            // strSQL
        }

        private void SetFooterHeight(ref string strSQLString)
        {
            clsDRWrapper rsSummary = new clsDRWrapper();
            string strTemp = "";
            int lngPos;
            // kgk 2-22-2012  SQL Server doesn't allow ORDER BY in subquery
            lngPos = Strings.InStr(strSQLString, "ORDER BY", CompareConstants.vbTextCompare/*?*/);
            if (lngPos > 0)
            {
                strTemp = Strings.Left(strSQLString, lngPos - 2);
            }
            else
            {
                strTemp = strSQLString;
            }
            rsSummary.OpenRecordset("SELECT BillingYear, SUM(Principal + Interest + Costs) AS Total FROM (" + strTemp + ") AS qTmp GROUP BY BillingYear");
            this.ReportFooter.Height = lblLienSummary.Top + (lblLienSummary.Height * (rsSummary.RecordCount() + 2));
        }
        // kk04082014 trocl-682
        // Private Sub AddToPaymentArray(lngIndex As Long, dblPrin As Double, dblPLI As Double, dblCurInt As Double, dblCost As Double)
        // dblPayments(lngIndex, 0) = dblPayments(lngIndex, 0) + dblPrin
        // dblPayments(lngIndex, 1) = dblPayments(lngIndex, 1) + dblPLI
        // dblPayments(lngIndex, 2) = dblPayments(lngIndex, 2) + dblCurInt
        // dblPayments(lngIndex, 3) = dblPayments(lngIndex, 3) + dblCost
        // End Sub
        private void ReverseLastRecord(double dblPaymentRecieved = 0, double dblIntPaid = 0, double dblCostPaid = 0)
        {
            // this will remove the totals of the last record from year and final totals
            double dblAmt = 0;
            rsData.MoveLast();
            // get the last record back
            fldAccount.Text = "";
            fldName.Text = "";
            fldYear.Text = "";
            fldCurrentInt.Text = "";
            fldTotal.Text = "";
            if (!reportConfiguration.Options.UseAsOfDate())
            {
                // Original Principal
                // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                dblAmt = Conversion.Val(rsData.Get_Fields("Principal"));
                fldPrincipal.Text = "";
                if (FCConvert.ToString(rsData.Get_Fields_Int32("BillingYear")) == "20051")
                {
                    FCMessageBox.Show("hi");
                }
                dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 0] -= dblAmt;
                dblTotals[4] -= dblAmt;
                // original principal total
                // this does not account into the line sum
                // Principal Due
                // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                dblAmt = Conversion.Val(rsData.Get_Fields("Principal") - rsData.Get_Fields_Decimal("PrincipalPaid"));
                fldPrincipalDue.Text = "";
                dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 1] -= dblAmt;
                dblTotals[0] -= dblAmt;
                // Interest Due
                // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
                dblAmt = Conversion.Val(rsData.Get_Fields("Interest") - rsData.Get_Fields("PLIPaid"));
                fldPLInt.Text = "";
                dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 2] -= dblAmt;
                dblTotals[1] -= dblAmt;
                // Costs Due
                // TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
                dblAmt = Conversion.Val(rsData.Get_Fields("Costs") - rsData.Get_Fields("MaturityFee") - rsData.Get_Fields_Decimal("CostsPaid"));
                fldCosts.Text = "";
                dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 3] -= dblAmt;
                dblTotals[2] -= dblAmt;
            }
            else
            {
                // Original Principal
                // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                dblAmt = Conversion.Val(rsData.Get_Fields("Principal"));
                fldPrincipal.Text = "";
                if (FCConvert.ToString(rsData.Get_Fields_Int32("BillingYear")) == "20051")
                {
                    FCMessageBox.Show("hi");
                }
                dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 0] -= dblAmt;
                dblTotals[4] -= dblAmt;
                // original principal total
                // this does not account into the line sum
                // Principal Due
                // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                dblAmt = Conversion.Val(rsData.Get_Fields("Principal")) - dblPaymentRecieved;
                fldPrincipalDue.Text = "";
                dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 1] -= dblAmt;
                dblTotals[0] -= dblAmt;
                // Interest Due
                // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                dblAmt = Conversion.Val(rsData.Get_Fields("Interest")) - dblIntPaid;
                fldPLInt.Text = "";
                dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 2] -= dblAmt;
                dblTotals[1] -= dblAmt;
                // Costs Due
                // TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
                dblAmt = Conversion.Val(rsData.Get_Fields("Costs")) - dblCostPaid;
                fldCosts.Text = "";
                dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 3] -= dblAmt;
                dblTotals[2] -= dblAmt;
            }
            this.Detail.Height = 0;
            // go to the next record
            rsData.MoveNext();
        }

        private void HideFields()
        {
            // if this is the last record, then I want to hide all of the fields so nothing shows
            fldAccount.Visible = false;
            fldName.Visible = false;
            fldCosts.Visible = false;
            fldCurrentInt.Visible = false;
            fldPLInt.Visible = false;
            fldPrincipal.Visible = false;
            fldPrincipalDue.Visible = false;
            fldYear.Visible = false;
            fldTotal.Visible = false;
        }

        private void arLienStatusReport_Load(object sender, System.EventArgs e)
        {
            //Begin Unmaped Properties
            //arLienStatusReport.Text	= "Lien Breakdown";
            //arLienStatusReport.Icon	= "arLienStatusReport.dsx":0000";
            //arLienStatusReport.Left	= 0;
            //arLienStatusReport.Top	= 0;
            //arLienStatusReport.Width	= 19080;
            //arLienStatusReport.Height	= 11490;
            //arLienStatusReport.StartUpPosition	= 3;
            //arLienStatusReport.SectionData	= "arLienStatusReport.dsx":058A;
            //End Unmaped Properties
        }
    }
}
