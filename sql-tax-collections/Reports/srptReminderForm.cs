﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class srptReminderForm : FCSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               01/14/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               01/08/2007              *
		// ********************************************************
		int lngAcct;
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsLData = new clsDRWrapper();
		int lngHighYear;
		double dblPrin;
		double dblInt;
		DateTime dtMailingDate;
        private DateTime dtInterestDate;
		double dblTotalPrin;
		double dblTotalInt;
		double dblTotalCost;
		// kk08182015 trocl-831
		int lngIndex;
		bool boolShowPastYears;
		bool boolRetry;
		// VBto upgrade warning: lngLastMatYr As int	OnWrite(double, int)
		int lngLastMatYr;
        private ReminderNoticeOptions reminderNoticeOptions;

        public srptReminderForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        public srptReminderForm(ReminderNoticeOptions options) : this()
        {
            reminderNoticeOptions = options;
        }

        private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Reminder Form Breakdown";
		}

		public static srptReminderForm InstancePtr
		{
			get
			{
				return (srptReminderForm)Sys.GetInstance(typeof(srptReminderForm));
			}
		}

		protected srptReminderForm _InstancePtr = null;

        public ReminderNoticeOptions Options
        {
            get { return reminderNoticeOptions; }
            set { reminderNoticeOptions = value; }
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
				rsLData?.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strFields;
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (FCConvert.ToInt32(this.UserData) != 0)
				{
					// if nothing is passed in, then exit this function
					lngAcct = FCConvert.ToInt32(this.UserData);
					// this is the account number to process
				}
				else
				{
					Cancel();
                    return;
                }
				boolRetry = true;
				dblTotalPrin = 0;
				dblTotalInt = 0;
				dblTotalCost = 0;
                // kk08182015 trocl-831
                boolShowPastYears = reminderNoticeOptions.ShowPastYears;
                // this will get the highest year that should be shown on the
                lngHighYear = FCConvert.ToInt32(reminderNoticeOptions.HighYear);
				if (modStatusPayments.Statics.boolRE)
				{
                    lngLastMatYr = FCConvert.ToInt32(Conversion.Val(Strings.Left(reminderNoticeOptions.LastMaturedYear, 4)) + 1);
				}
                else
				{
                    lngLastMatYr = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(reminderNoticeOptions.LastMaturedYear, 4))));
				}
                dtMailingDate = reminderNoticeOptions.MailDate;
                dtInterestDate = reminderNoticeOptions.InterestDate;
				strFields = "ID,Account,LienRecordNumber,BillingYear,TaxDue1,TaxDue2,TaxDue3,TaxDue4,PrincipalPaid,RateKey,InterestAppliedThroughDate," + "InterestCharged,InterestPaid,DemandFees,DemandFeesPaid,Address1,Address2,Address3,Name1";
				if (modStatusPayments.Statics.boolRE)
				{
					// kk 01082012   rsLData.OpenRecordset "SELECT * FROM LienRec", strCLDatabase
					if (boolShowPastYears)
					{
						rsData.OpenRecordset("SELECT " + strFields + " FROM BillingMaster WHERE BillingType = 'RE' AND Account = " + FCConvert.ToString(lngAcct) + " ORDER BY BillingYear", modExtraModules.strCLDatabase);
					}
					else
					{
						rsData.OpenRecordset("SELECT " + strFields + " FROM BillingMaster WHERE BillingType = 'RE' AND Account = " + FCConvert.ToString(lngAcct) + " AND BillingYear / 10 = " + FCConvert.ToString(Conversion.Val(rptReminderForm.InstancePtr.strHighYear)) + " ORDER BY BillingYear", modExtraModules.strCLDatabase);
					}
				}
				else
				{
					if (boolShowPastYears)
					{
						rsData.OpenRecordset("SELECT " + strFields + " FROM BillingMaster WHERE BillingType = 'PP' AND Account = " + FCConvert.ToString(lngAcct) + " ORDER BY BillingYear", modExtraModules.strCLDatabase);
					}
					else
					{
						rsData.OpenRecordset("SELECT " + strFields + " FROM BillingMaster WHERE BillingType = 'PP' AND Account = " + FCConvert.ToString(lngAcct) + " AND BillingYear / 10 = " + FCConvert.ToString(Conversion.Val(rptReminderForm.InstancePtr.strHighYear)) + " ORDER BY BillingYear", modExtraModules.strCLDatabase);
					}
				}
				if (rsData.EndOfFile())
				{
					// if I cannot find the account, then exit
					Cancel();
                    return;
                }
				else
				{
					// rock on
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Starting Subreport");
			}
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				double dblTotal;
				double dblPrinToPeriod = 0;
				int intPer;
				double dblPrinAfterPer = 0;
				bool boolCheckPayments = false;
				double[] dblAbateAmt = new double[4 + 1];
				double dblPrinAmt = 0;
				double dblCost = 0;
				// kk08182015 trocl-831
				int intRatePer = 0;
				double dblCurInt = 0;
				lblYear.Text = "";
				lblPrincipal.Text = "";
				lblInterest.Text = "";
				lblCost.Text = "";
				// kk08182015 trocl-831
				lblTotal.Text = "";
				boolRetry = true;
				TryAgain:
				;
				dblTotal = 0;
				dblAbateAmt[1] = 0;
				dblAbateAmt[2] = 0;
				dblAbateAmt[3] = 0;
				dblAbateAmt[4] = 0;
				if (!rsData.EndOfFile())
				{
					if (((rsData.Get_Fields_Int32("LienRecordNumber"))) == 0)
					{
						if (FCUtils.iDiv(FCConvert.ToInt32(rsData.Get_Fields_Int32("BillingYear")), 10) > Conversion.Val(Strings.Left(rptReminderForm.InstancePtr.strHighYear, 4)))
						{
							rsData.MoveNext();
							if (rsData.EndOfFile())
							{
								return;
							}
							else
							{
								goto TryAgain;
							}
						}
						// kgk trocl-693/trocl-391  Add option for Last Year Matured / to show
						if (FCUtils.iDiv(FCConvert.ToInt32(rsData.Get_Fields_Int32("BillingYear")), 10) < lngLastMatYr)
						{
							rsData.MoveNext();
							if (rsData.EndOfFile())
							{
								return;
							}
							else
							{
								goto TryAgain;
							}
						}
						if (FCUtils.iDiv(FCConvert.ToInt32(rsData.Get_Fields_Int32("BillingYear")), 10) == Conversion.Val(Strings.Left(rptReminderForm.InstancePtr.strHighYear, 4)))
						{
							// this will check to see if this is the year that shoul dbe affected by the period choice
							boolCheckPayments = true;
                            // if it is then check the period that was selected
                            //FC:FINAL:DDU:#i335 Changed from SelectedIndex to SelectedItem to find correct value in ComboBox
                            if (reminderNoticeOptions.Period == ReminderNoticeOptions.PeriodEnum.PastDueOnly)
							{
                                boolCheckPayments = false;
								dblPrinToPeriod = 0;
								dblPrinAmt = 0;
								dblPrinAfterPer = 0;
								intPer = 4;
							}
                            else if (reminderNoticeOptions.Period == ReminderNoticeOptions.PeriodEnum.Period1)
							{
                                // period 1
                                dblPrinToPeriod = Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1")) - Conversion.Val(rsData.Get_Fields_Decimal("PrincipalPaid"));
								dblPrinAmt = Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1"));
								dblPrinAfterPer = Conversion.Val(rsData.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue4"));
								intPer = 1;
							}
                            else if (reminderNoticeOptions.Period == ReminderNoticeOptions.PeriodEnum.Period2)
							{
                                // period 2
                                dblPrinToPeriod = Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue2")) - Conversion.Val(rsData.Get_Fields_Decimal("PrincipalPaid"));
								dblPrinAmt = Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue2"));
								dblPrinAfterPer = Conversion.Val(rsData.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue4"));
								intPer = 2;
							}
                            else if (reminderNoticeOptions.Period == ReminderNoticeOptions.PeriodEnum.Period3)
							{
                                // period 3
                                dblPrinToPeriod = Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue3")) - Conversion.Val(rsData.Get_Fields_Decimal("PrincipalPaid"));
								dblPrinAmt = Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue3"));
								dblPrinAfterPer = Conversion.Val(rsData.Get_Fields_Decimal("TaxDue4"));
								intPer = 3;
							}
							else
							{
								// kk03182014 trocl-560   Need to account for abatements for Interest to calc correctly    ' boolCheckPayments = False
								// period 4 or all outstanding
								dblPrinToPeriod = Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue4")) - Conversion.Val(rsData.Get_Fields_Decimal("PrincipalPaid"));
								dblPrinAmt = Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue4"));
								dblPrinAfterPer = 0;
								intPer = 4;
							}
						}
						else
						{
							// kk03182014 trocl-560    boolCheckPayments = False
							// else show all periods
							// period 4 or all outstanding
							dblPrinToPeriod = Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue4")) - Conversion.Val(rsData.Get_Fields_Decimal("PrincipalPaid"));
                            dblPrinAfterPer = 0;
							intPer = 4;
						}
                        if (dblPrinToPeriod > 0 && dblPrinToPeriod < .01) // fix tiny non-zero amounts that should be zero
                        {
                            dblPrinToPeriod = 0;
                        }
                        if (dblPrinAmt > 0 && dblPrinAmt < .01)
                        {
                            dblPrinAmt = 0;
                        }
                        if (boolCheckPayments)
						{
							// check the abatement amounts
							clsDRWrapper rsPay = new clsDRWrapper();
							clsDRWrapper rsRate = new clsDRWrapper();
							if (modStatusPayments.Statics.boolRE)
							{
								rsPay.OpenRecordset("SELECT Period, Principal FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(rsData.Get_Fields_Int32("ID")) + " AND BillCode = 'R' AND Code = 'A'");
							}
							else
							{
								rsPay.OpenRecordset("SELECT Period, Principal FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(rsData.Get_Fields_Int32("ID")) + " AND BillCode = 'P' AND Code = 'A'");
							}
							rsRate.OpenRecordset("SELECT NumberOfPeriods FROM RateRec WHERE ID = " + FCConvert.ToString(rsData.Get_Fields_Int32("RateKey")));
							if (!rsRate.EndOfFile())
							{
								intRatePer = FCConvert.ToInt32(rsRate.Get_Fields_Int16("NumberOfPeriods"));
								while (!rsPay.EndOfFile())
								{
									// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
									if (FCConvert.ToString(rsPay.Get_Fields("Period")) == "A")
									{
										// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
										dblAbateAmt[1] += FCConvert.ToInt32(rsPay.Get_Fields("Principal")) / intRatePer;
										if (intRatePer >= 2)
											// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
											// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
											dblAbateAmt[2] += FCConvert.ToInt32(rsPay.Get_Fields("Principal")) / intRatePer;
										if (intRatePer >= 3)
											// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
											// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
											dblAbateAmt[3] += FCConvert.ToInt32(rsPay.Get_Fields("Principal")) / intRatePer;
										if (intRatePer >= 4)
											// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
											// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
											dblAbateAmt[4] += FCConvert.ToInt32(rsPay.Get_Fields("Principal")) / intRatePer;
									}
									else
									{
										// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
										dblAbateAmt[Convert.ToInt32(rsPay.Get_Fields("Period"))] += Conversion.Val(rsPay.Get_Fields("Principal"));
									}
									rsPay.MoveNext();
								}
                                // this will reset the values used
                                //FC:FINAL:DDU:#i335 Changed from SelectedIndex to SelectedItem to find Past Due Only in ComboBox
                                if (reminderNoticeOptions.Period == ReminderNoticeOptions.PeriodEnum.PastDueOnly)
								{
                                    dblPrinToPeriod = 0;
									dblPrinAfterPer = 0;
								}
                                else if (reminderNoticeOptions.Period == ReminderNoticeOptions.PeriodEnum.Period1)
                                {
									// period 1
									dblPrinToPeriod = Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1")) - (Conversion.Val(rsData.Get_Fields_Decimal("PrincipalPaid")) - dblAbateAmt[2] - dblAbateAmt[3] - dblAbateAmt[4]);
									dblPrinAfterPer = Conversion.Val(rsData.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue4")) - dblAbateAmt[2] - dblAbateAmt[3] - dblAbateAmt[4];
								}
                                else if (reminderNoticeOptions.Period == ReminderNoticeOptions.PeriodEnum.Period2)
                                {
									// period 2
									dblPrinToPeriod = Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue2")) - (Conversion.Val(rsData.Get_Fields_Decimal("PrincipalPaid")) - dblAbateAmt[3] - dblAbateAmt[4]);
									dblPrinAfterPer = Conversion.Val(rsData.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue4")) - dblAbateAmt[3] - dblAbateAmt[4];
								}
                                else if (reminderNoticeOptions.Period == ReminderNoticeOptions.PeriodEnum.Period3)
                                {
									// period 3
									dblPrinToPeriod = Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue3")) - (Conversion.Val(rsData.Get_Fields_Decimal("PrincipalPaid")) - dblAbateAmt[4]);
									dblPrinAfterPer = Conversion.Val(rsData.Get_Fields_Decimal("TaxDue4")) - dblAbateAmt[4];
								}
							}
							else
							{
								// just let everything go as normal because this is
								// missing the rate key and probably is not billed yet
								// so will not have any amount due
							}
						}
						// check to see if this year needs to be shown
						if (dblPrinToPeriod > 0)
						{
							// there is still tax due
							// so allow the program to
						}
						else
						{
							// there is no tax due, so try the next year
							// then check to make sure that there are no ababtements that would allow this bill to be shown
							rsData.MoveNext();
							if (rsData.EndOfFile())
							{
								return;
							}
							else
							{
								goto TryAgain;
							}
						}
						// non lien year calculations
						dblPrin = dblPrinToPeriod;
						// .Fields("TaxDue1") + .Fields("TaxDue2") + .Fields("TaxDue3") + .Fields("TaxDue4") - .Fields("PrincipalPaid")
						dblCurInt = 0;
						dblInt = 0;
						dblCost = 0;
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						dblTotal = modCLCalculations.CalculateAccountCL(ref rsData, FCConvert.ToInt32(rsData.Get_Fields("Account")), dtInterestDate, ref dblInt, ref dblCurInt, ref dblCost, ref dblAbateAmt[1], ref dblAbateAmt[2], ref dblAbateAmt[3], ref dblAbateAmt[4]);
						// dblInt = dblInt - .Fields("InterestCharged") - .Fields("InterestPaid")
						// add the values into the sums
						dblTotalPrin += dblPrin;
						dblTotalInt = modGlobal.Round(dblTotalInt + dblTotal - dblPrin - dblCost - dblPrinAfterPer, 2);
						// kk08182015 trocl-831 Account for costs
						dblTotalCost += dblCost;
						// kk08182015 trocl-831
						// fill the labels
						lblYear.Text = FCConvert.ToString(FCUtils.iDiv(FCConvert.ToInt32(rsData.Get_Fields_Int32("BillingYear")), 10));
						lblPrincipal.Text = Strings.Format(dblPrin, "#,##0.00");
						lblInterest.Text = Strings.Format(dblTotal - dblPrin - dblCost - dblPrinAfterPer, "#,##0.00");
                        lblCost.Text = Strings.Format(dblCost, "#,##0.00");
						lblTotal.Text = Strings.Format(dblTotal - dblPrinAfterPer, "#,##0.00");
						if (!rptReminderForm.InstancePtr.boolRetry)
						{
							lngIndex = Information.UBound(modReminderNoticeSummary.Statics.arrReminderSummaryList, 1) + 1;
							Array.Resize(ref modReminderNoticeSummary.Statics.arrReminderSummaryList, lngIndex + 1);
							//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
							modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex] = new modReminderNoticeSummary.ReminderSummaryList(0);
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Account = rsData.Get_Fields("Account");
							modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr1 = rsData.Get_Fields_String("Address1");
							modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr2 = rsData.Get_Fields_String("Address2");
							modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr3 = rsData.Get_Fields_String("Address3");
							modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr4 = "";
							modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Year = rsData.Get_Fields_Int32("BillingYear");
							modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Name1 = rsData.Get_Fields_String("Name1");
							modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Total = dblTotal - dblPrinAfterPer;
						}
						boolRetry = false;
						// move to the next year
						rsData.MoveNext();
					}
					else
					{
						// MAL@20080111: Check for high year (same as regular bills)
						if (FCUtils.iDiv(FCConvert.ToInt32(rsData.Get_Fields_Int32("BillingYear")), 10) > Conversion.Val(Strings.Left(rptReminderForm.InstancePtr.strHighYear, 4)))
						{
							rsData.MoveNext();
							if (rsData.EndOfFile())
							{
								return;
							}
							else
							{
								goto TryAgain;
							}
						}
						// kgk trocl-693/trocl-391  Add option for last year matured / last year to show
						if (FCUtils.iDiv(FCConvert.ToInt32(rsData.Get_Fields_Int32("BillingYear")), 10) < lngLastMatYr)
						{
							rsData.MoveNext();
							if (rsData.EndOfFile())
							{
								return;
							}
							else
							{
								goto TryAgain;
							}
						}
						// reset this just in case
						dblPrinAfterPer = 0;
						// lien
						rsLData.OpenRecordset("SELECT ID,RateKey,InterestAppliedThroughDate,MaturityFee," + "Principal,Interest,InterestCharged,Costs," + "PrincipalPaid,PLIPaid,InterestPaid,CostsPaid " + "FROM LienRec WHERE ID = " + FCConvert.ToString(rsData.Get_Fields_Int32("LienRecordNumber")), modExtraModules.strCLDatabase);
						if (!rsLData.EndOfFile())
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							if (Conversion.Val(rsLData.Get_Fields("Principal")) - Conversion.Val(rsLData.Get_Fields_Decimal("PrincipalPaid")) > 0)
							{
								// allow this year to go through
							}
							else
							{
								rsData.MoveNext();
								if (rsData.EndOfFile())
								{
									return;
								}
								else
								{
									goto TryAgain;
								}
							}
							// lien year calculations
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							dblPrin = Conversion.Val(rsLData.Get_Fields("Principal")) - Conversion.Val(rsLData.Get_Fields_Decimal("PrincipalPaid"));
							dblTotal = modCLCalculations.CalculateAccountCLLien(rsLData, dtInterestDate, ref dblInt, ref dblCost);
							// kk08182015 trocl-831  Let the Calculate function set the costs in dblCost
							// dblInt = dblInt - rsLData.Fields("InterestCharged") + rsLData.Fields("Interest") - rsLData.Fields("InterestPaid")
							// add the values into the sums
							dblTotalPrin += dblPrin;
							dblTotalInt += dblTotal - dblPrin - dblCost;
							// kk08182015 trocl-831 Account for costs
							dblTotalCost += dblCost;
							// kk08182015 trocl-831
							// fill the labels
							lblYear.Text = "LIEN " + FCConvert.ToString(FCUtils.iDiv(FCConvert.ToInt32(rsData.Get_Fields_Int32("BillingYear")), 10));
							// this will get the billing year from the BillingMaster even if this is a lien
							lblPrincipal.Text = Strings.Format(dblPrin, "#,##0.00");
							lblInterest.Text = Strings.Format(dblTotal - dblPrin - dblCost, "#,##0.00");
							// kk08182015 trocl-831 Account for costs
							lblCost.Text = Strings.Format(dblCost, "#,##0.00");
							// kk08182015 trocl-831
							lblTotal.Text = Strings.Format(dblTotal, "#,##0.00");
							if (!rptReminderForm.InstancePtr.boolRetry)
							{
								lngIndex = Information.UBound(modReminderNoticeSummary.Statics.arrReminderSummaryList, 1) + 1;
								Array.Resize(ref modReminderNoticeSummary.Statics.arrReminderSummaryList, lngIndex + 1);
								//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
								modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex] = new modReminderNoticeSummary.ReminderSummaryList(0);
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Account = rsData.Get_Fields("Account");
								modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr1 = rsData.Get_Fields_String("Address1");
								modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr2 = rsData.Get_Fields_String("Address2");
								modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr3 = rsData.Get_Fields_String("Address3");
								modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr4 = "";
								modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Year = rsData.Get_Fields_Int32("BillingYear");
								modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Name1 = rsData.Get_Fields_String("Name1");
								modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Total = dblTotal;
							}
							boolRetry = false;
							// move to the next year
							rsData.MoveNext();
						}
						else
						{
							// no matching lien record
							rsData.MoveNext();
							if (rsData.EndOfFile())
							{
								return;
							}
							else
							{
								goto TryAgain;
							}
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Bind Fields - 122");
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
			rptReminderForm.InstancePtr.boolNoValues = boolRetry;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			int lngIndex;
			// this will fill the sums into the footer line
			lblFooterPrincipal.Text = Strings.Format(dblTotalPrin, "#,##0.00");
			lblFooterInterest.Text = Strings.Format(dblTotalInt, "#,##0.00");
			lblFooterCost.Text = Strings.Format(dblTotalCost, "#,##0.00");
			// kk08182015 trocl-831
			lblFooterTotal.Text = Strings.Format(dblTotalInt + dblTotalPrin + dblTotalCost, "#,##0.00");
			// kk08182015 trocl-831 Include costs in total
			lngIndex = FindIndex(ref lngAcct);
			if (lngIndex >= 0)
			{
				modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Total = dblTotalInt + dblTotalPrin + dblTotalCost;
				// kk08182015 trocl-831 Include costs for summary report
			}
		}

		private int FindIndex(ref int lngA)
		{
			int FindIndex = 0;
			int lngCT;
			FindIndex = -1;
			for (lngCT = 0; lngCT <= Information.UBound(modReminderNoticeSummary.Statics.arrReminderSummaryList, 1); lngCT++)
			{
				if (modReminderNoticeSummary.Statics.arrReminderSummaryList[lngCT].Account == lngA)
				{
					FindIndex = lngCT;
					return FindIndex;
				}
			}
			return FindIndex;
		}

		
	}
}
