﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using GrapeCity.ActiveReports.SectionReportModel;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptTaxAcquiredList : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/13/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               04/11/2006              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsLN = new clsDRWrapper();
		int lngTotalAccounts;
		int lngCount;
		// this will keep track of which row I am on in the grid
		bool boolDone;
		double dblTotalPrin;
		double dblTotal;
		double dblLTotal;
		bool boolReverse;
		int intSumRows;

		public rptTaxAcquiredList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Tax Acquired List";
		}

		public static rptTaxAcquiredList InstancePtr
		{
			get
			{
				return (rptTaxAcquiredList)Sys.GetInstance(typeof(rptTaxAcquiredList));
			}
		}

		protected rptTaxAcquiredList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
				rsLN?.Dispose();
            }
			base.Dispose(disposing);
		}

		public void Init(int lngPassTotalAccounts)
		{
			lngTotalAccounts = lngPassTotalAccounts;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!modGlobal.Statics.arrDemand[lngCount].Used && boolDone)
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
			}
			//Detail_Format();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			// save this report for future access
			if (this.Document.Pages.Count > 0)
			{
				// trocl-565 08-08-2011 kgk  Report saved to wrong file
				// If boolReverse Then
				// IncrementSavedReports "LastCLReverseDemand"
				// Me.Pages.Save "RPT\LastCLReverseDemand1.RDF"
				// Else
				// IncrementSavedReports "LastCLDemandFeesList"
				// Me.Pages.Save "RPT\LastCLDemandFeesList1.RDF"
				// End If
				modGlobalFunctions.IncrementSavedReports("LastTAPropertiesList");
				this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastTAPropertiesList1.RDF"));
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "t");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			lblHeader.Text = "Tax Acquired Properties";
			lngCount = 0;
			//FC:FINAL:AM: create the new fields here
			SetupTotals();
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the information into the fields
				if (!boolDone)
				{
					// this will find the next available account that was processed
					while (!(modGlobal.Statics.arrDemand[lngCount].Processed || lngCount >= Information.UBound(modGlobal.Statics.arrDemand, 1)))
					{
						lngCount += 1;
					}
					// check to see if we are at the end of the list
					if (lngCount >= Information.UBound(modGlobal.Statics.arrDemand, 1))
					{
						boolDone = true;
						// clear the fields so there are no repeats
						fldAcct.Text = "";
						fldName.Text = "";
						fldTotal.Text = "";
						return;
					}
					rsData.OpenRecordset("SELECT * FROM BillingMaster WHERE BillingType <> 'PP' AND Account = " + FCConvert.ToString(modGlobal.Statics.arrDemand[lngCount].Account), modExtraModules.strCLDatabase);
					while (!rsData.EndOfFile())
					{
						if (((rsData.Get_Fields_Int32("LienRecordNumber"))) != 0)
						{
							rsLN.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(rsData.Get_Fields_Int32("LienRecordNumber")), modExtraModules.strCLDatabase);
							if (!rsLN.EndOfFile())
							{
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								modGlobal.Statics.arrDemand[lngCount].Fee += (Conversion.Val(rsLN.Get_Fields("Principal")) - Conversion.Val(rsLN.Get_Fields_Decimal("PrincipalPaid")));
							}
						}
						else
						{
							modGlobal.Statics.arrDemand[lngCount].Fee += (Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue4")) - Conversion.Val(rsData.Get_Fields_Decimal("PrincipalPaid")));
						}
						rsData.MoveNext();
					}
					fldAcct.Text = modGlobal.Statics.arrDemand[lngCount].Account.ToString();
					fldName.Text = modGlobal.Statics.arrDemand[lngCount].Name;
					fldTotal.Text = Strings.Format(modGlobal.Statics.arrDemand[lngCount].Fee, "#,##0.00");
					dblTotalPrin += modGlobal.Statics.arrDemand[lngCount].Fee;
					// move to the next record in the array
					lngCount += 1;
				}
				else
				{
					// clear the fields
					fldAcct.Text = "";
					fldName.Text = "";
					fldTotal.Text = "";
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Binding Fields");
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void SetupTotals()
		{
			// this sub will fill in the footer line at the bottom of the report
			GrapeCity.ActiveReports.SectionReportModel.Label obNew;
			int intCT;
			//lblNumberOfAccounts.Text = "There were " + FCConvert.ToString(lngTotalAccounts) + " accounts processed.";
			//lblTotalTotal.Text = Strings.Format(dblTotalPrin, "#,##0.00");
			// setup the summary
			intSumRows = 1;
			for (intCT = 0; intCT <= Information.UBound(modGlobal.Statics.arrTALienPrincipal, 1); intCT++)
			{
				if (!modGlobal.Statics.gboolMultipleTowns)
				{
					if (modGlobal.Statics.arrTAPrincipal[intCT, 0] != 0 || modGlobal.Statics.arrTALienPrincipal[intCT, 0] != 0)
					{
						AddSummaryRow(intSumRows, modGlobal.Statics.arrTAPrincipal[intCT, 0], modGlobal.Statics.arrTALienPrincipal[intCT, 0], intCT + modExtraModules.DEFAULTSUBTRACTIONVALUE);
						intSumRows += 1;
					}
				}
				else
				{
					if ((modGlobal.Statics.arrTAPrincipal[intCT, 1] != 0 || modGlobal.Statics.arrTALienPrincipal[intCT, 1] != 0) || (modGlobal.Statics.arrTAPrincipal[intCT, 2] != 0 || modGlobal.Statics.arrTALienPrincipal[intCT, 2] != 0) || (modGlobal.Statics.arrTAPrincipal[intCT, 3] != 0 || modGlobal.Statics.arrTALienPrincipal[intCT, 3] != 0) || (modGlobal.Statics.arrTAPrincipal[intCT, 4] != 0 || modGlobal.Statics.arrTALienPrincipal[intCT, 4] != 0) || (modGlobal.Statics.arrTAPrincipal[intCT, 5] != 0 || modGlobal.Statics.arrTALienPrincipal[intCT, 5] != 0) || (modGlobal.Statics.arrTAPrincipal[intCT, 6] != 0 || modGlobal.Statics.arrTALienPrincipal[intCT, 6] != 0) || (modGlobal.Statics.arrTAPrincipal[intCT, 7] != 0 || modGlobal.Statics.arrTALienPrincipal[intCT, 7] != 0) || (modGlobal.Statics.arrTAPrincipal[intCT, 8] != 0 || modGlobal.Statics.arrTALienPrincipal[intCT, 8] != 0) || (modGlobal.Statics.arrTAPrincipal[intCT, 9] != 0 || modGlobal.Statics.arrTALienPrincipal[intCT, 9] != 0))
					{
						AddSummaryRow(intSumRows, modGlobal.Statics.arrTAPrincipal[intCT, 1] + modGlobal.Statics.arrTAPrincipal[intCT, 2] + modGlobal.Statics.arrTAPrincipal[intCT, 3] + modGlobal.Statics.arrTAPrincipal[intCT, 4] + modGlobal.Statics.arrTAPrincipal[intCT, 5] + modGlobal.Statics.arrTAPrincipal[intCT, 6] + modGlobal.Statics.arrTAPrincipal[intCT, 7] + modGlobal.Statics.arrTAPrincipal[intCT, 8] + modGlobal.Statics.arrTAPrincipal[intCT, 9], modGlobal.Statics.arrTALienPrincipal[intCT, 1] + modGlobal.Statics.arrTALienPrincipal[intCT, 2] + modGlobal.Statics.arrTALienPrincipal[intCT, 3] + modGlobal.Statics.arrTALienPrincipal[intCT, 4] + modGlobal.Statics.arrTALienPrincipal[intCT, 5] + modGlobal.Statics.arrTALienPrincipal[intCT, 6] + modGlobal.Statics.arrTALienPrincipal[intCT, 7] + modGlobal.Statics.arrTALienPrincipal[intCT, 8] + modGlobal.Statics.arrTALienPrincipal[intCT, 9], intCT + modExtraModules.DEFAULTSUBTRACTIONVALUE);
						intSumRows += 1;
					}
				}
			}
			// add a label
			obNew = new GrapeCity.ActiveReports.SectionReportModel.Label();
			obNew.Name = "lblTotalLine";
			obNew.Top = lblYear1.Top + ((intSumRows - 1) * lblYear1.Height);
			obNew.Left = lblYear1.Left;
			obNew.Font = lblYear1.Font;
			obNew.Text = "Total";
			ReportFooter.Controls.Add(obNew);
			// add a regular amount total label
			obNew = new GrapeCity.ActiveReports.SectionReportModel.Label();
			obNew.Name = "lblAmountTotal";
			obNew.Top = lblYear1.Top + ((intSumRows - 1) * lblYear1.Height);
			obNew.Left = lblAmount1.Left;
			obNew.Width = lblAmount1.Width;
			obNew.Font = lblAmount1.Font;
			obNew.Text = Strings.Format(dblTotal, "#,##0.00");
			obNew.Alignment = lblLAmount1.Alignment;
			ReportFooter.Controls.Add(obNew);
			// add a lien amount total label
			obNew = new GrapeCity.ActiveReports.SectionReportModel.Label();
			obNew.Name = "lblLAmountTotal";
			obNew.Top = lblYear1.Top + ((intSumRows - 1) * lblYear1.Height);
			obNew.Left = lblLAmount1.Left;
			obNew.Width = lblLAmount1.Width;
			obNew.Font = lblLAmount1.Font;
			obNew.Text = Strings.Format(dblLTotal, "#,##0.00");
			obNew.Alignment = lblLAmount1.Alignment;
			ReportFooter.Controls.Add(obNew);
			// add a line
			GrapeCity.ActiveReports.SectionReportModel.Line obLine = new GrapeCity.ActiveReports.SectionReportModel.Line();
			obLine.Name = "lnFooterSummaryTotal";
			obLine.X1 = lblYear1.Left;
			obLine.X2 = lblLAmount1.Left + lblLAmount1.Width;
			obLine.Y1 = lblYear1.Top + ((intSumRows - 1) * lblYear1.Height);
			obLine.Y2 = obLine.Y1;
			ReportFooter.Height = lblYear1.Top + (intSumRows * lblYear1.Height);
			ReportFooter.Controls.Add(obLine);
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			//SetupTotals();
			lblNumberOfAccounts.Text = "There were " + FCConvert.ToString(lngTotalAccounts) + " accounts processed.";
			lblTotalTotal.Text = Strings.Format(dblTotalPrin, "#,##0.00");
		}
		// VBto upgrade warning: intRNum As short	OnWriteFCConvert.ToInt32(
		private void AddSummaryRow(int intRNum, double dblAmount, double dblLAmount, int lngYear)
		{
			GrapeCity.ActiveReports.SectionReportModel.Label obNew = new GrapeCity.ActiveReports.SectionReportModel.Label();
			/*unused?*/// this will add another per diem line in the report footer
			if (intRNum == 1)
			{
				lblAmount1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				lblAmount1.Text = Strings.Format(dblAmount, "#,##0.00");
				lblLAmount1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				lblLAmount1.Text = Strings.Format(dblLAmount, "#,##0.00");
				lblYear1.Text = modExtraModules.FormatYear(lngYear.ToString());
			}
			else
			{
				// add a year label
				obNew.Name = "lblYear" + FCConvert.ToString(intRNum);
				obNew.Top = lblYear1.Top + ((intRNum - 1) * lblYear1.Height);
				obNew.Left = lblYear1.Left;
				obNew.Font = lblYear1.Font;
				// this sets the font to the same as the field that is already created
				obNew.Text = modExtraModules.FormatYear(lngYear.ToString());
				ReportFooter.Controls.Add(obNew);
				// add a regular amount label
				obNew = new GrapeCity.ActiveReports.SectionReportModel.Label();
				obNew.Name = "lblAmount" + FCConvert.ToString(intRNum);
				obNew.Top = lblYear1.Top + ((intRNum - 1) * lblYear1.Height);
				obNew.Left = lblAmount1.Left;
				obNew.Width = lblAmount1.Width;
				obNew.Font = lblAmount1.Font;
				// this sets the font to the same as the field that is already created
				obNew.Alignment = lblLAmount1.Alignment;
				obNew.Text = Strings.Format(dblAmount, "#,##0.00");
				ReportFooter.Controls.Add(obNew);
				// add a lien amount label
				obNew = new GrapeCity.ActiveReports.SectionReportModel.Label();
				obNew.Name = "lblLAmount" + FCConvert.ToString(intRNum);
				obNew.Top = lblYear1.Top + ((intRNum - 1) * lblYear1.Height);
				obNew.Left = lblLAmount1.Left;
				obNew.Width = lblLAmount1.Width;
				obNew.Font = lblLAmount1.Font;
				// this sets the font to the same as the field that is already created
				obNew.Alignment = lblLAmount1.Alignment;
				obNew.Text = Strings.Format(dblLAmount, "#,##0.00");
				ReportFooter.Controls.Add(obNew);
			}
			dblTotal += dblAmount;
			dblLTotal += dblLAmount;
		}

		
	}
}
