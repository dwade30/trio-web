﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class arTextDump : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// Date           :               09/26/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// Last Updated   :               01/31/2003              *
		// ********************************************************
		public string strText = string.Empty;
		bool boolFirstTime;

		public arTextDump()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static arTextDump InstancePtr
		{
			get
			{
				return (arTextDump)Sys.GetInstance(typeof(arTextDump));
			}
		}

		protected arTextDump _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = boolFirstTime;
			boolFirstTime = true;
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "t");
			if (Strings.UCase(modCustomReport.Statics.strReportType) == "30DAYNOTICE")
			{
				lblHeader.Text = "30 DayNotice";
			}
			else if (Strings.UCase(modCustomReport.Statics.strReportType) == "LIENPROCESS")
			{
				lblHeader.Text = "Transfer Tax To Lien";
			}
			else if (Strings.UCase(modCustomReport.Statics.strReportType) == "LIENMATURITY")
			{
				lblHeader.Text = "Lien Maturity";
			}
			fldOut.Text = strText;
		}

		
	}
}
