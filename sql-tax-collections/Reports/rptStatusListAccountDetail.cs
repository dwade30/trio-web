﻿using fecherFoundation;
using Global;
using SharedApplication.TaxCollections.Enums;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using SharedApplication.TaxCollections;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCL0000
{
    /// <summary>
    /// Summary description for SectionReport1.
    /// </summary>
    public partial class rptStatusListAccountDetail : BaseSectionReport
    {
        private TaxCollectionStatusReportConfiguration reportConfiguration;
        clsDRWrapper rsData = new clsDRWrapper();
        int lngRow;
        int lngPDNumber;
        int lngPDTop;
        double dblPDTotal;
        bool boolPerDiem;
        string strLastYearText = "";
        public DateTime dtAsOfDate;
        int lngTAType;
        // 0 = all, 1 = TA, 2 = non TA
        clsDRWrapper rsMaster = new clsDRWrapper();

        public rptStatusListAccountDetail()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            //if (_InstancePtr == null)
            //	_InstancePtr = this;
            this.Name = "Account Detail Report";
        }

        public void SetReportOption(TaxCollectionStatusReportConfiguration reportConfig)
        {
            reportConfiguration = reportConfig;
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            //if (_InstancePtr == this)
            //{
            //	_InstancePtr = null;
            //	Sys.ClearInstance(this);
            //}

            if (disposing)
            {
                rsData?.Dispose();
                rsMaster?.Dispose();
            }

            base.Dispose(disposing);
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
        {
            eArgs.EOF = rsData.EndOfFile();
            //Detail_Format();
        }

        private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
        {
            //if (KeyCode == vbKeyEscape)
            //{
            //	Close();
            //}
        }

        private void ActiveReport_ReportStart(object sender, EventArgs e)
        {
            string strSQL;
            this.Name = "Account Detail Report";

            if (reportConfiguration.Options.UseAsOfDate())
            {
                dtAsOfDate = reportConfiguration.Options.AsOfDate;
            }
            else
            {
                dtAsOfDate = DateTime.Today;
            }

            boolPerDiem = false;
            if (reportConfiguration.Filter.IsTaxAcquired.HasValue)
            {
                if (reportConfiguration.Filter.IsTaxAcquired.Value)
                {
                    lngTAType = 1;
                }
                else
                {
                    lngTAType = 2;
                }
            }
            else
            {
                lngTAType = 0;
            }
            lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
            lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
            lblMuniName.Text = modGlobalConstants.Statics.MuniName;
            strSQL = GetSQL();
            if (reportConfiguration.BillingType == PropertyTaxBillType.Real)
            {
                rsMaster.OpenRecordset("SELECT RSAccount, TaxAcquired FROM Master WHERE TaxAcquired = 1", modExtraModules.strREDatabase);
            }
            rsData.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
            if (rsData.EndOfFile())
            {
                frmWait.InstancePtr.Unload();
                FCMessageBox.Show("There are no accounts matching this criteria.", MsgBoxStyle.Exclamation, "No Accounts");
                Cancel();
            }
        }

        private void ActiveReport_ReportEnd(object sender, EventArgs e)
        {
            rsData.Dispose();
            rsMaster.Dispose();
        }

        private void Detail_Format(object sender, EventArgs e)
        {
            if (!rsData.EndOfFile())
            {
                BindFields();
            }
        }

        private void PageHeader_Format(object sender, EventArgs e)
        {
            lblPage.Text = "Page " + this.PageNumber;
        }

        private void BindFields()
        {
            try
            {
            // On Error GoTo ERROR_HANDLER
            // MAL@20080729: Added accomodations for PP report
            // Tracker Reference: 14805
            TRYAGAIN:
                ;
                if (rsData.EndOfFile())
                {
                    return;
                }
                if (reportConfiguration.Filter.TranCodeMax.HasValue)
                {
                    if (!modCLCalculations.CheckTranCode_78(reportConfiguration.BillingType == PropertyTaxBillType.Real,
                        FCConvert.ToInt32(rsData.Get_Fields("Account")), reportConfiguration.Filter.TranCodeMin ?? 0,
                        reportConfiguration.Filter.TranCodeMax ?? 0))
                    {
                        rsData.MoveNext();
                        goto TRYAGAIN;
                    }
                }

                if (reportConfiguration.BillingType == PropertyTaxBillType.Real)
                {
                    if (lngTAType > 0)
                    {
                        switch (lngTAType)
                        {

                            case 1:
                                {
                                    // TA
                                    // if the account is in the recordset then it is TA
                                    rsMaster.FindFirstRecord("RSAccount", rsData.Get_Fields("Account"));
                                    if (rsMaster.NoMatch)
                                    {
                                        // If Not rsMaster.Fields("TaxAcquired") Then
                                        rsData.MoveNext();
                                        goto TRYAGAIN;
                                    }
                                    break;
                                }
                            case 2:
                                {
                                    // Non TA
                                    // if the account is NOT in the recordset then it is TA
                                    rsMaster.FindFirstRecord("RSAccount", rsData.Get_Fields("Account"));
                                    if (!rsMaster.NoMatch)
                                    {
                                        // If rsMaster.Fields("TaxAcquired") Then
                                        rsData.MoveNext();
                                        goto TRYAGAIN;
                                    }
                                    break;
                                }
                        } //end switch
                          // End If
                    }
                }
                // Account
                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                lblAccount.Text = "Account: " + FCConvert.ToString(rsData.Get_Fields("Account"));
                if (rsData.Get_Fields_Boolean("Deleted") == true)
                {
                    lblAccount.Text = lblAccount.Text + " ! ";
                }
                // Billing Year
                lblYear.Text = "Year: " + modExtraModules.FormatYear(FCConvert.ToString(rsData.Get_Fields_Int32("BillingYear")));
                // Name
                if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name2"))) != "")
                {
                    fldName.Text = Strings.Trim(Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name1")) + " " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name2")))));
                }
                else
                {
                    fldName.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name1")));
                }
                if (reportConfiguration.BillingType == PropertyTaxBillType.Real)
                {
                    // Address
                    fldAddress.Text = Strings.Trim(Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address1"))) + " " + Strings.Trim(Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address2"))) + " " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address3")))));
                    // Location
                    // TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
                    if (((rsData.Get_Fields("StreetNumber"))) != 0)
                    {
                        // TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
                        fldLocation.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields("StreetNumber")) + " " + FCConvert.ToString(rsData.Get_Fields_String("StreetName")));
                    }
                    else
                    {
                        fldLocation.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("StreetName")));
                    }
                }
                else
                {
                    fldAddress.Text = Strings.Trim(Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address1"))) + " " + Strings.Trim(Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address2")))));
                    // Location
                    // TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
                    if (((rsData.Get_Fields("StreetNumber"))) != 0)
                    {
                        // TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
                        fldLocation.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields("StreetNumber")) + " " + FCConvert.ToString(rsData.Get_Fields_String("Street")));
                    }
                    else
                    {
                        fldLocation.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Street")));
                    }
                }
                // Map Lot
                fldMapLot.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("MapLot")));
                // Ref 1 & 2
                fldRef1.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Ref1")));
                fldRef2.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Ref2")));
                // Land, Building and Exempt value
                if (reportConfiguration.BillingType == PropertyTaxBillType.Real)
                {
                    fldLand.Text = Strings.Format(rsData.Get_Fields_Int32("LandValue"), "#,##0");
                    fldBuilding.Text = Strings.Format(rsData.Get_Fields_Int32("BuildingValue"), "#,##0");
                    fldExempt.Text = Strings.Format(rsData.Get_Fields_Int32("ExemptValue"), "#,##0");
                }
                else
                {
                    fldLand.Text = "";
                    fldBuilding.Text = "";
                    fldExempt.Text = "";
                    lblLand.Text = "";
                    lblBuilding.Text = "";
                    lblExempt.Text = "";
                }
                // Tag for the sub report = BillKey
                var srSLAllActivityDetail = new srptSLAllActivityDetail();
                srSLAllActivityDetail.SetReportOption(reportConfiguration);
                srptSLAllActivityDetailOB.Report = srSLAllActivityDetail;
                srptSLAllActivityDetailOB.Report.UserData = rsData.Get_Fields_Int32("ID");

                rsData.MoveNext();
                return;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In Bind Fields (Main)");
            }
        }

        private string GetSQL()
        {
            string GetSQL = "";
            string strTemp = "";
            string strREPP = "";
            string strFields = "";



            if (reportConfiguration.BillingType == PropertyTaxBillType.Real)
            {
                strREPP = " BillingMaster.BillingType = 'RE'";
                strFields = "Account,Name1,Name2,Address1,Address2,Address3,RSDeleted AS Deleted,BillingYear," + "StreetNumber,StreetName,MapLot,Ref1,Ref2,LandValue,BuildingValue,ExemptValue,BillingMaster.ID";

                if (fecherFoundation.Strings.Trim(reportConfiguration.WhereClause) != "")
                {
                    strTemp = "SELECT " + strFields + " FROM ((" + reportConfiguration.SelectClause + ") AS Bill INNER JOIN BillingMaster ON Bill.BK = BillingMaster.ID) INNER JOIN (" + modGlobal.GetMasterJoin(reportConfiguration.BillingType == PropertyTaxBillType.Real) + ") AS mparty ON BillingMaster.Account = mparty.RSAccount WHERE " + reportConfiguration.WhereClause + " AND " + strREPP + " AND RSCard = 1 ORDER BY " + reportConfiguration.SortClause;
                }
                else
                {
                    strTemp = "SELECT " + strFields + " FROM ((" + reportConfiguration.SelectClause + ") AS Bill INNER JOIN BillingMaster ON Bill.BK = BillingMaster.ID) INNER JOIN (" + modGlobal.GetMasterJoin(reportConfiguration.BillingType == PropertyTaxBillType.Real) + ") AS mparty ON BillingMaster.Account = mparty.RSAccount WHERE " + strREPP + " AND mparty.RSCard = 1 ORDER BY " + reportConfiguration.SortClause;
                }
            }
            else
            {
                strREPP = " BillingMaster.BillingType = 'PP'";
                strFields = "BillingMaster.Account,Name1,Name2,BillingMaster.Address1,BillingMaster.Address2,Deleted,BillingYear," + "BillingMaster.StreetNumber,Street,MapLot,Ref1,Ref2,BillingMaster.ID";

                if (fecherFoundation.Strings.Trim(reportConfiguration.WhereClause) != "")
                {
                    strTemp = "SELECT " + strFields + " FROM ((" + reportConfiguration.SelectClause + ") AS Bill INNER JOIN BillingMaster ON Bill.BK = BillingMaster.ID) INNER JOIN " + modGlobal.Statics.strDbPP + "PPMaster ON BillingMaster.Account = PPMaster.Account WHERE " + reportConfiguration.WhereClause + " AND " + strREPP + " ORDER BY " + reportConfiguration.SortClause;
                }
                else
                {
                    strTemp = "SELECT " + strFields + " FROM ((" + reportConfiguration.SelectClause + ") AS Bill INNER JOIN BillingMaster ON Bill.BK = BillingMaster.ID) INNER JOIN " + modGlobal.Statics.strDbPP + "PPMaster ON BillingMaster.Account = PPMaster.Account WHERE " + strREPP + " ORDER BY " + reportConfiguration.SortClause;
                }
            }

            GetSQL = strTemp;

            return GetSQL;
        }

       

        private void rptStatusListAccountDetail_ReportEndedAndCanceled(object sender, EventArgs e)
        {
            rsData.DisposeOf();
            rsMaster.DisposeOf();
        }
    }
}
