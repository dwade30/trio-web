﻿namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	partial class rptPurgeBills
	{
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptPurgeBills));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.fldAcct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTax1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTax2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTax3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCHGINT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTax4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldYearLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.srptPayments = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.lblAcct = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTax1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblReportType = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTax2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTax3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTax4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.fldFooterTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldFooterTax1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldFooterTax2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldFooterTax3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldFooterCHGINT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldFooterTaxTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldFooterTax4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lnFooter = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.fldAcct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTax1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTax2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTax3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCHGINT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTax4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYearLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAcct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTax1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReportType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTax2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTax3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTax4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFooterTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFooterTax1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFooterTax2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFooterTax3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFooterCHGINT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFooterTaxTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFooterTax4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldAcct,
            this.fldTax1,
            this.fldName,
            this.fldTax2,
            this.fldTax3,
            this.fldCHGINT,
            this.fldTotal,
            this.fldTax4,
            this.fldYear,
            this.fldYearLabel,
            this.srptPayments});
            this.Detail.Height = 0.7166667F;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // fldAcct
            // 
            this.fldAcct.Height = 0.1875F;
            this.fldAcct.Left = 0F;
            this.fldAcct.Name = "fldAcct";
            this.fldAcct.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldAcct.Text = null;
            this.fldAcct.Top = 0.217F;
            this.fldAcct.Width = 0.75F;
            // 
            // fldTax1
            // 
            this.fldTax1.Height = 0.1875F;
            this.fldTax1.Left = 1.875F;
            this.fldTax1.Name = "fldTax1";
            this.fldTax1.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldTax1.Text = null;
            this.fldTax1.Top = 0.4045F;
            this.fldTax1.Width = 0.9375F;
            // 
            // fldName
            // 
            this.fldName.Height = 0.1875F;
            this.fldName.Left = 0.75F;
            this.fldName.Name = "fldName";
            this.fldName.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.fldName.Text = null;
            this.fldName.Top = 0.217F;
            this.fldName.Width = 5.25F;
            // 
            // fldTax2
            // 
            this.fldTax2.Height = 0.1875F;
            this.fldTax2.Left = 2.8125F;
            this.fldTax2.Name = "fldTax2";
            this.fldTax2.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldTax2.Text = null;
            this.fldTax2.Top = 0.4045F;
            this.fldTax2.Width = 0.9375F;
            // 
            // fldTax3
            // 
            this.fldTax3.Height = 0.1875F;
            this.fldTax3.Left = 3.75F;
            this.fldTax3.Name = "fldTax3";
            this.fldTax3.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldTax3.Text = null;
            this.fldTax3.Top = 0.4045F;
            this.fldTax3.Width = 0.9375F;
            // 
            // fldCHGINT
            // 
            this.fldCHGINT.Height = 0.1875F;
            this.fldCHGINT.Left = 5.625F;
            this.fldCHGINT.Name = "fldCHGINT";
            this.fldCHGINT.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldCHGINT.Text = null;
            this.fldCHGINT.Top = 0.4045F;
            this.fldCHGINT.Width = 0.875F;
            // 
            // fldTotal
            // 
            this.fldTotal.Height = 0.1875F;
            this.fldTotal.Left = 6.5F;
            this.fldTotal.Name = "fldTotal";
            this.fldTotal.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldTotal.Text = null;
            this.fldTotal.Top = 0.4045F;
            this.fldTotal.Width = 1F;
            // 
            // fldTax4
            // 
            this.fldTax4.Height = 0.1875F;
            this.fldTax4.Left = 4.6875F;
            this.fldTax4.Name = "fldTax4";
            this.fldTax4.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldTax4.Text = null;
            this.fldTax4.Top = 0.4045F;
            this.fldTax4.Width = 0.9375F;
            // 
            // fldYear
            // 
            this.fldYear.Height = 0.1875F;
            this.fldYear.Left = 1.125F;
            this.fldYear.Name = "fldYear";
            this.fldYear.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
            this.fldYear.Text = null;
            this.fldYear.Top = 0.4045F;
            this.fldYear.Width = 0.75F;
            // 
            // fldYearLabel
            // 
            this.fldYearLabel.Height = 0.1875F;
            this.fldYearLabel.Left = 0.375F;
            this.fldYearLabel.Name = "fldYearLabel";
            this.fldYearLabel.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldYearLabel.Text = "Year:";
            this.fldYearLabel.Top = 0.4045F;
            this.fldYearLabel.Width = 0.75F;
            // 
            // srptPayments
            // 
            this.srptPayments.CanShrink = false;
            this.srptPayments.CloseBorder = false;
            this.srptPayments.Height = 0.125F;
            this.srptPayments.Left = 0F;
            this.srptPayments.Name = "srptPayments";
            this.srptPayments.Report = null;
            this.srptPayments.Top = 0.5920001F;
            this.srptPayments.Width = 7.5F;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblAcct,
            this.lblTax1,
            this.Line1,
            this.lblName,
            this.lblReportType,
            this.lblTax2,
            this.lblTax3,
            this.lblTotal,
            this.lblTax4,
            this.Label1});
            this.GroupHeader1.Height = 0.5729167F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // lblAcct
            // 
            this.lblAcct.Height = 0.1875F;
            this.lblAcct.HyperLink = null;
            this.lblAcct.Left = 0F;
            this.lblAcct.Name = "lblAcct";
            this.lblAcct.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.lblAcct.Text = "Account";
            this.lblAcct.Top = 0.375F;
            this.lblAcct.Width = 0.75F;
            // 
            // lblTax1
            // 
            this.lblTax1.Height = 0.1875F;
            this.lblTax1.HyperLink = null;
            this.lblTax1.Left = 1.875F;
            this.lblTax1.Name = "lblTax1";
            this.lblTax1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.lblTax1.Text = "Tax 1";
            this.lblTax1.Top = 0.375F;
            this.lblTax1.Width = 0.9375F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.5625F;
            this.Line1.Width = 7.5F;
            this.Line1.X1 = 7.5F;
            this.Line1.X2 = 0F;
            this.Line1.Y1 = 0.5625F;
            this.Line1.Y2 = 0.5625F;
            // 
            // lblName
            // 
            this.lblName.Height = 0.1875F;
            this.lblName.HyperLink = null;
            this.lblName.Left = 0.75F;
            this.lblName.Name = "lblName";
            this.lblName.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left; ddo-char-set: 0";
            this.lblName.Text = "Current Owner";
            this.lblName.Top = 0.375F;
            this.lblName.Width = 1.125F;
            // 
            // lblReportType
            // 
            this.lblReportType.Height = 0.25F;
            this.lblReportType.HyperLink = null;
            this.lblReportType.Left = 0F;
            this.lblReportType.Name = "lblReportType";
            this.lblReportType.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: center; ddo-char-set: 0";
            this.lblReportType.Text = "Non Lien";
            this.lblReportType.Top = 0.0625F;
            this.lblReportType.Width = 7.5F;
            // 
            // lblTax2
            // 
            this.lblTax2.Height = 0.1875F;
            this.lblTax2.HyperLink = null;
            this.lblTax2.Left = 2.8125F;
            this.lblTax2.Name = "lblTax2";
            this.lblTax2.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.lblTax2.Text = "Tax 2";
            this.lblTax2.Top = 0.375F;
            this.lblTax2.Width = 0.9375F;
            // 
            // lblTax3
            // 
            this.lblTax3.Height = 0.1875F;
            this.lblTax3.HyperLink = null;
            this.lblTax3.Left = 3.75F;
            this.lblTax3.Name = "lblTax3";
            this.lblTax3.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.lblTax3.Text = "Tax 3";
            this.lblTax3.Top = 0.375F;
            this.lblTax3.Width = 0.9375F;
            // 
            // lblTotal
            // 
            this.lblTotal.Height = 0.1875F;
            this.lblTotal.HyperLink = null;
            this.lblTotal.Left = 6.5F;
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.lblTotal.Text = "Total";
            this.lblTotal.Top = 0.375F;
            this.lblTotal.Width = 1F;
            // 
            // lblTax4
            // 
            this.lblTax4.Height = 0.1875F;
            this.lblTax4.HyperLink = null;
            this.lblTax4.Left = 4.6875F;
            this.lblTax4.Name = "lblTax4";
            this.lblTax4.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.lblTax4.Text = "Tax 4";
            this.lblTax4.Top = 0.375F;
            this.lblTax4.Width = 0.9375F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.1875F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 5.625F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.Label1.Text = "Charged Int";
            this.Label1.Top = 0.375F;
            this.Label1.Width = 0.875F;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldFooterTotal,
            this.fldFooterTax1,
            this.fldFooterTax2,
            this.fldFooterTax3,
            this.fldFooterCHGINT,
            this.fldFooterTaxTotal,
            this.fldFooterTax4,
            this.lnFooter});
            this.GroupFooter1.Height = 0.53125F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // fldFooterTotal
            // 
            this.fldFooterTotal.Height = 0.1875F;
            this.fldFooterTotal.Left = 0F;
            this.fldFooterTotal.Name = "fldFooterTotal";
            this.fldFooterTotal.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.fldFooterTotal.Text = null;
            this.fldFooterTotal.Top = 0.125F;
            this.fldFooterTotal.Visible = false;
            this.fldFooterTotal.Width = 1.875F;
            // 
            // fldFooterTax1
            // 
            this.fldFooterTax1.Height = 0.1875F;
            this.fldFooterTax1.Left = 1.875F;
            this.fldFooterTax1.Name = "fldFooterTax1";
            this.fldFooterTax1.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldFooterTax1.Text = null;
            this.fldFooterTax1.Top = 0.125F;
            this.fldFooterTax1.Visible = false;
            this.fldFooterTax1.Width = 0.9375F;
            // 
            // fldFooterTax2
            // 
            this.fldFooterTax2.Height = 0.1875F;
            this.fldFooterTax2.Left = 2.8125F;
            this.fldFooterTax2.Name = "fldFooterTax2";
            this.fldFooterTax2.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldFooterTax2.Text = null;
            this.fldFooterTax2.Top = 0.125F;
            this.fldFooterTax2.Visible = false;
            this.fldFooterTax2.Width = 0.9375F;
            // 
            // fldFooterTax3
            // 
            this.fldFooterTax3.Height = 0.1875F;
            this.fldFooterTax3.Left = 3.75F;
            this.fldFooterTax3.Name = "fldFooterTax3";
            this.fldFooterTax3.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldFooterTax3.Text = null;
            this.fldFooterTax3.Top = 0.125F;
            this.fldFooterTax3.Visible = false;
            this.fldFooterTax3.Width = 0.9375F;
            // 
            // fldFooterCHGINT
            // 
            this.fldFooterCHGINT.Height = 0.1875F;
            this.fldFooterCHGINT.Left = 5.625F;
            this.fldFooterCHGINT.Name = "fldFooterCHGINT";
            this.fldFooterCHGINT.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldFooterCHGINT.Text = null;
            this.fldFooterCHGINT.Top = 0.125F;
            this.fldFooterCHGINT.Visible = false;
            this.fldFooterCHGINT.Width = 0.875F;
            // 
            // fldFooterTaxTotal
            // 
            this.fldFooterTaxTotal.Height = 0.1875F;
            this.fldFooterTaxTotal.Left = 6.5F;
            this.fldFooterTaxTotal.Name = "fldFooterTaxTotal";
            this.fldFooterTaxTotal.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldFooterTaxTotal.Text = null;
            this.fldFooterTaxTotal.Top = 0.125F;
            this.fldFooterTaxTotal.Visible = false;
            this.fldFooterTaxTotal.Width = 1F;
            // 
            // fldFooterTax4
            // 
            this.fldFooterTax4.Height = 0.1875F;
            this.fldFooterTax4.Left = 4.6875F;
            this.fldFooterTax4.Name = "fldFooterTax4";
            this.fldFooterTax4.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldFooterTax4.Text = null;
            this.fldFooterTax4.Top = 0.125F;
            this.fldFooterTax4.Visible = false;
            this.fldFooterTax4.Width = 0.9375F;
            // 
            // lnFooter
            // 
            this.lnFooter.Height = 0F;
            this.lnFooter.Left = 1.875F;
            this.lnFooter.LineWeight = 1F;
            this.lnFooter.Name = "lnFooter";
            this.lnFooter.Top = 0.229F;
            this.lnFooter.Visible = false;
            this.lnFooter.Width = 5.625F;
            this.lnFooter.X1 = 1.875F;
            this.lnFooter.X2 = 7.5F;
            this.lnFooter.Y1 = 0.229F;
            this.lnFooter.Y2 = 0.229F;
            // 
            // rptPurgeBills
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.25F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.5F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
            ((System.ComponentModel.ISupportInitialize)(this.fldAcct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTax1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTax2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTax3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCHGINT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTax4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYearLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAcct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTax1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReportType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTax2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTax3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTax4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFooterTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFooterTax1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFooterTax2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFooterTax3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFooterCHGINT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFooterTaxTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFooterTax4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAcct;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTax1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTax2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTax3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCHGINT;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTax4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYearLabel;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport srptPayments;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAcct;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTax1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReportType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTax2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTax3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTax4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFooterTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFooterTax1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFooterTax2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFooterTax3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFooterCHGINT;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFooterTaxTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFooterTax4;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnFooter;
	}
}
