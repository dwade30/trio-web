﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class srptActivityDetailLien : FCSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// Date           :               02/11/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// Last Updated   :               07/07/2004              *
		// ********************************************************
		int lngBillKey;
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsBill = new clsDRWrapper();
		double dblAmountOwed;
		double dblAmountPaid;
		double dblPayment;

		public srptActivityDetailLien()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Activity Detail";
		}

		public static srptActivityDetailLien InstancePtr
		{
			get
			{
				return (srptActivityDetailLien)Sys.GetInstance(typeof(srptActivityDetailLien));
			}
		}

		protected srptActivityDetailLien _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsBill?.Dispose();
				rsData?.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
			//Detail_Format();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// this is how I will pass the billkey into this subreport
			lngBillKey = FCConvert.ToInt32(this.UserData);
			if (lngBillKey > 0)
			{
				rsBill.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(lngBillKey));
				rsData.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngBillKey) + " AND BillCode = 'L'");
				if (rsData.EndOfFile())
				{
					// no payments were found for this account
					Cancel();
				}
				else
				{
					
					dblAmountOwed = Conversion.Val(rsBill.Get_Fields("Principal")) + Conversion.Val(rsBill.Get_Fields("Costs")) + Conversion.Val(rsBill.Get_Fields("Interest")) - Conversion.Val(rsBill.Get_Fields_Decimal("InterestCharged"));
					dblAmountPaid = 0;
				}
			}
			else
			{
				// no bill key was passed in
				Cancel();
			}
		}

		private void BindFields()
		{
			// this will put the information into the fields
			if (!rsData.EndOfFile())
			{
				// Date
				fldDate.Text = FCConvert.ToString(rsData.Get_Fields_DateTime("RecordedTransactionDate"));
				// Code
				// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
				fldCode.Text = FCConvert.ToString(rsData.Get_Fields("Code"));
				// Ref
				fldRef.Text = rsData.Get_Fields_String("Reference");
				// Receipt
				fldReceipt.Text = GetActualReceiptNumber(FCConvert.ToInt32(rsData.Get_Fields_Int32("ReceiptNumber")), rsData.Get_Fields_DateTime("ActualSystemDate"));
				// Payment
				// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
				dblPayment = Conversion.Val(rsData.Get_Fields("Principal")) + Conversion.Val(rsData.Get_Fields_Decimal("CurrentInterest")) + Conversion.Val(rsData.Get_Fields_Decimal("LienCost")) + Conversion.Val(rsData.Get_Fields_Decimal("PreLienInterest"));
				fldPayment.Text = Strings.Format(dblPayment, "#,##0.00");
				// update the running total
				dblAmountPaid += dblPayment;
				// Owed
				fldOwed.Text = Strings.Format(dblAmountOwed - dblAmountPaid, "#,##0.00");
				rsData.MoveNext();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			fldTotalPayment.Text = Strings.Format(dblAmountPaid, "#,##0.00");
			fldTotalOwed.Text = Strings.Format(dblAmountOwed - dblAmountPaid, "#,##0.00");
		}
		// VBto upgrade warning: 'Return' As string	OnWrite(string, int)
		private string GetActualReceiptNumber(int lngRN, DateTime dtDate/*= DateTime.Now*/)
		{
			string GetActualReceiptNumber = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will accept a long of a receipt number key in the CR table
				// if the receipt does not exist or any other error occurs, the function will
				// return 0, otherwise it returns the receipt number shown on the receipt
                using (clsDRWrapper rsRN = new clsDRWrapper())
                {
                    if (lngRN > 0)
                    {
                        if (modGlobalConstants.Statics.gboolCR)
                        {
                            // kk03272015 trocr-357   rsRN.OpenRecordset "SELECT * FROM Receipt WHERE ReceiptKey = " & lngRN, strCRDatabase
                            rsRN.OpenRecordset(
                                "SELECT ReceiptNumber FROM Receipt WHERE ReceiptKey = " + FCConvert.ToString(lngRN) +
                                " AND ReceiptDate > '" + Strings.Format(dtDate, "MM/dd/yyyy") +
                                " 12:00:00 AM' AND ReceiptDate < '" + Strings.Format(dtDate, "MM/dd/yyyy") +
                                " 11:59:59 PM'", modExtraModules.strCRDatabase);
                            if (rsRN.EndOfFile() != true && rsRN.BeginningOfFile() != true)
                            {
                                GetActualReceiptNumber = FCConvert.ToString(rsRN.Get_Fields_Int32("ReceiptNumber"));
                            }
                            else
                            {
                                rsRN.OpenRecordset(
                                    "SELECT ReceiptNumber FROM LastYearReceipt WHERE ReceiptKey = " +
                                    FCConvert.ToString(lngRN) + " AND LastYearReceiptDate > '" +
                                    Strings.Format(dtDate, "MM/dd/yyyy") + " 12:00:00 AM' AND LastYearReceiptDate < '" +
                                    Strings.Format(dtDate, "MM/dd/yyyy") + " 11:59:59 PM'",
                                    modExtraModules.strCRDatabase);
                                if (rsRN.EndOfFile() != true && rsRN.BeginningOfFile() != true)
                                {
                                    GetActualReceiptNumber = FCConvert.ToString(rsRN.Get_Fields_Int32("ReceiptNumber"));
                                }
                                else
                                {
                                    GetActualReceiptNumber = FCConvert.ToString(lngRN) + "*";
                                }
                            }
                        }
                        else
                        {
                            GetActualReceiptNumber = FCConvert.ToString(lngRN);
                        }
                    }
                    else
                    {
                        GetActualReceiptNumber = FCConvert.ToString(0);
                    }

                }

                return GetActualReceiptNumber;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Returning Receipt Number");
			}
			return GetActualReceiptNumber;
		}

		private void srptActivityDetailLien_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//srptActivityDetailLien.Caption	= "ActivityDetail";
			//srptActivityDetailLien.Icon	= "srptActivityDetailLien.dsx":0000";
			//srptActivityDetailLien.Left	= 0;
			//srptActivityDetailLien.Top	= 0;
			//srptActivityDetailLien.Width	= 11880;
			//srptActivityDetailLien.Height	= 8580;
			//srptActivityDetailLien.StartUpPosition	= 3;
			//srptActivityDetailLien.SectionData	= "srptActivityDetailLien.dsx":058A;
			//End Unmaped Properties
		}
	}
}
