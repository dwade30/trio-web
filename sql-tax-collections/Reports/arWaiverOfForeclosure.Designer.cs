﻿namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	partial class arWaiverOfForeclosure
	{
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(arWaiverOfForeclosure));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldMainText = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTopDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSigLine = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSigTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSigDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldACKNOWLEDGEMENT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomText = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSigName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.imgSig = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			this.fldMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.lblTitleBar = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblLegalDescription = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMapLot = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTownHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.fldNotaryLine = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNotaryTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNotaryCommissionTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.fldMainText)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTopDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSigLine)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSigTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSigDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldACKNOWLEDGEMENT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomText)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSigName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgSig)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitleBar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLegalDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTownHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNotaryLine)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNotaryTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNotaryCommissionTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldMainText,
				this.fldTopDate,
				this.fldSigLine,
				this.fldSigTitle,
				this.fldSigDate,
				this.fldACKNOWLEDGEMENT,
				this.fldBottomText,
				this.fldSigName,
				this.imgSig,
				this.fldMuni
			});
			this.Detail.Height = 4.8125F;
			this.Detail.Name = "Detail";
			// 
			// fldMainText
			// 
			this.fldMainText.Height = 1.6F;
			this.fldMainText.Left = 0F;
			this.fldMainText.Name = "fldMainText";
			this.fldMainText.Style = "font-family: \'Tahoma\'";
			this.fldMainText.Text = null;
			this.fldMainText.Top = 0.4F;
			this.fldMainText.Width = 7F;
			// 
			// fldTopDate
			// 
			this.fldTopDate.Height = 0.2F;
			this.fldTopDate.Left = 0F;
			this.fldTopDate.Name = "fldTopDate";
			this.fldTopDate.Style = "font-family: \'Tahoma\'";
			this.fldTopDate.Text = null;
			this.fldTopDate.Top = 2F;
			this.fldTopDate.Width = 2.3F;
			// 
			// fldSigLine
			// 
			this.fldSigLine.Height = 0.2F;
			this.fldSigLine.Left = 3.9F;
			this.fldSigLine.Name = "fldSigLine";
			this.fldSigLine.Style = "font-family: \'Tahoma\'";
			this.fldSigLine.Text = null;
			this.fldSigLine.Top = 2.55F;
			this.fldSigLine.Width = 3.1F;
			// 
			// fldSigTitle
			// 
			this.fldSigTitle.Height = 0.2F;
			this.fldSigTitle.Left = 0.55F;
			this.fldSigTitle.Name = "fldSigTitle";
			this.fldSigTitle.Style = "font-family: \'Tahoma\'";
			this.fldSigTitle.Text = null;
			this.fldSigTitle.Top = 2.75F;
			this.fldSigTitle.Width = 3.1F;
			// 
			// fldSigDate
			// 
			this.fldSigDate.Height = 0.2F;
			this.fldSigDate.Left = 3.9F;
			this.fldSigDate.Name = "fldSigDate";
			this.fldSigDate.Style = "font-family: \'Tahoma\'";
			this.fldSigDate.Text = null;
			this.fldSigDate.Top = 3.125F;
			this.fldSigDate.Width = 3.1F;
			// 
			// fldACKNOWLEDGEMENT
			// 
			this.fldACKNOWLEDGEMENT.Height = 0.3125F;
			this.fldACKNOWLEDGEMENT.Left = 0F;
			this.fldACKNOWLEDGEMENT.Name = "fldACKNOWLEDGEMENT";
			this.fldACKNOWLEDGEMENT.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.fldACKNOWLEDGEMENT.Text = "ACKNOWLEDGEMENT";
			this.fldACKNOWLEDGEMENT.Top = 3.625F;
			this.fldACKNOWLEDGEMENT.Width = 7F;
			// 
			// fldBottomText
			// 
			this.fldBottomText.Height = 0.875F;
			this.fldBottomText.Left = 0F;
			this.fldBottomText.Name = "fldBottomText";
			this.fldBottomText.Style = "font-family: \'Tahoma\'";
			this.fldBottomText.Text = null;
			this.fldBottomText.Top = 3.9375F;
			this.fldBottomText.Width = 7F;
			// 
			// fldSigName
			// 
			this.fldSigName.Height = 0.2F;
			this.fldSigName.Left = 3.9F;
			this.fldSigName.Name = "fldSigName";
			this.fldSigName.Style = "font-family: \'Tahoma\'";
			this.fldSigName.Text = null;
			this.fldSigName.Top = 2.743056F;
			this.fldSigName.Width = 3.1F;
			// 
			// imgSig
			// 
			this.imgSig.Height = 0.8F;
			this.imgSig.HyperLink = null;
			this.imgSig.ImageData = null;
			this.imgSig.Left = 3.9F;
			this.imgSig.LineWeight = 1F;
			this.imgSig.Name = "imgSig";
			this.imgSig.PictureAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.BottomLeft;
			this.imgSig.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
			this.imgSig.Top = 1.95F;
			this.imgSig.Width = 3.05F;
			// 
			// fldMuni
			// 
			this.fldMuni.Height = 0.1875F;
			this.fldMuni.Left = 3.9F;
			this.fldMuni.Name = "fldMuni";
			this.fldMuni.Style = "font-family: \'Tahoma\'";
			this.fldMuni.Text = null;
			this.fldMuni.Top = 2.934028F;
			this.fldMuni.Width = 3.0625F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblTitleBar,
				this.lblLegalDescription,
				this.lblMapLot,
				this.fldAccount,
				this.lblTownHeader
			});
			this.PageHeader.Height = 0.8020833F;
			this.PageHeader.Name = "PageHeader";
			// 
			// lblTitleBar
			// 
			this.lblTitleBar.Height = 0.25F;
			this.lblTitleBar.HyperLink = null;
			this.lblTitleBar.Left = 0F;
			this.lblTitleBar.Name = "lblTitleBar";
			this.lblTitleBar.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblTitleBar.Text = "WAIVER OF FORECLOSURE FORM";
			this.lblTitleBar.Top = 0.25F;
			this.lblTitleBar.Width = 7F;
			// 
			// lblLegalDescription
			// 
			this.lblLegalDescription.Height = 0.25F;
			this.lblLegalDescription.HyperLink = null;
			this.lblLegalDescription.Left = 0F;
			this.lblLegalDescription.Name = "lblLegalDescription";
			this.lblLegalDescription.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblLegalDescription.Text = null;
			this.lblLegalDescription.Top = 0.5F;
			this.lblLegalDescription.Width = 7F;
			// 
			// lblMapLot
			// 
			this.lblMapLot.Height = 0.1875F;
			this.lblMapLot.HyperLink = null;
			this.lblMapLot.Left = 0F;
			this.lblMapLot.Name = "lblMapLot";
			this.lblMapLot.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
			this.lblMapLot.Text = null;
			this.lblMapLot.Top = 0.75F;
			this.lblMapLot.Width = 7F;
			// 
			// fldAccount
			// 
			this.fldAccount.Height = 0.125F;
			this.fldAccount.Left = 0F;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "font-family: \'Tahoma\'";
			this.fldAccount.Text = null;
			this.fldAccount.Top = 0.5625F;
			this.fldAccount.Width = 0.8125F;
			// 
			// lblTownHeader
			// 
			this.lblTownHeader.Height = 0.25F;
			this.lblTownHeader.HyperLink = null;
			this.lblTownHeader.Left = 0F;
			this.lblTownHeader.Name = "lblTownHeader";
			this.lblTownHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblTownHeader.Text = null;
			this.lblTownHeader.Top = 0F;
			this.lblTownHeader.Width = 7F;
			// 
			// PageFooter
			// 
			this.PageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldNotaryLine,
				this.fldNotaryTitle,
				this.fldNotaryCommissionTitle
			});
			this.PageFooter.Height = 0.9791667F;
			this.PageFooter.Name = "PageFooter";
			// 
			// fldNotaryLine
			// 
			this.fldNotaryLine.Height = 0.2F;
			this.fldNotaryLine.Left = 4F;
			this.fldNotaryLine.Name = "fldNotaryLine";
			this.fldNotaryLine.Style = "font-family: \'Tahoma\'";
			this.fldNotaryLine.Text = null;
			this.fldNotaryLine.Top = 0.05F;
			this.fldNotaryLine.Width = 3F;
			// 
			// fldNotaryTitle
			// 
			this.fldNotaryTitle.Height = 0.2F;
			this.fldNotaryTitle.Left = 4F;
			this.fldNotaryTitle.Name = "fldNotaryTitle";
			this.fldNotaryTitle.Style = "font-family: \'Tahoma\'";
			this.fldNotaryTitle.Text = null;
			this.fldNotaryTitle.Top = 0.25F;
			this.fldNotaryTitle.Width = 3F;
			// 
			// fldNotaryCommissionTitle
			// 
			this.fldNotaryCommissionTitle.Height = 0.2F;
			this.fldNotaryCommissionTitle.Left = 4F;
			this.fldNotaryCommissionTitle.Name = "fldNotaryCommissionTitle";
			this.fldNotaryCommissionTitle.Style = "font-family: \'Tahoma\'";
			this.fldNotaryCommissionTitle.Text = null;
			this.fldNotaryCommissionTitle.Top = 0.45F;
			this.fldNotaryCommissionTitle.Width = 3F;
			// 
			// arWaiverOfForeclosure
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 2F;
			this.PageSettings.Margins.Left = 0.75F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.010417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.fldMainText)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTopDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSigLine)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSigTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSigDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldACKNOWLEDGEMENT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomText)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSigName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgSig)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitleBar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLegalDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTownHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNotaryLine)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNotaryTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNotaryCommissionTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMainText;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTopDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSigLine;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSigTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSigDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldACKNOWLEDGEMENT;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomText;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSigName;
		private GrapeCity.ActiveReports.SectionReportModel.Picture imgSig;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMuni;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitleBar;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLegalDescription;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTownHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNotaryLine;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNotaryTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNotaryCommissionTitle;
	}
}
