﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptTaxClubBooklet : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               02/22/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/21/2006              *
		// ********************************************************
		clsDRWrapper rsTaxClub = new clsDRWrapper();
		int intAction;
		int intNumberOfPayments;
		string strAddr1 = "";
		string strAddr2 = "";
		string strAddr3 = "";
		string strAddr4 = "";
		DateTime dtStartDate;
		bool boolDone;
		int lngTaxClub;
		int lngTCRow;
		int lngCurrentTC;
		bool boolExtraPage;

		public rptTaxClubBooklet()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Tax Club Booklet";
		}

		public static rptTaxClubBooklet InstancePtr
		{
			get
			{
				return (rptTaxClubBooklet)Sys.GetInstance(typeof(rptTaxClubBooklet));
			}
		}

		protected rptTaxClubBooklet _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsTaxClub.Dispose();
            }
			base.Dispose(disposing);
		}

		public void Init(int lngPassTaxClub)
		{
			clsDRWrapper rsPage = new clsDRWrapper();
			rsPage.OpenRecordset("SELECT PageBreakForTCBooklet FROM Collections", modExtraModules.strCLDatabase);
			if (!rsPage.EndOfFile())
			{
				boolExtraPage = FCConvert.ToBoolean(rsPage.Get_Fields_Boolean("PageBreakForTCBooklet"));
			}
			// this will set the recordset to be used
			lngTaxClub = lngPassTaxClub;
			if (lngTaxClub > 0)
			{
				rsTaxClub.OpenRecordset("SELECT * FROM TaxClub WHERE ID = " + FCConvert.ToString(lngTaxClub), modExtraModules.strCLDatabase);
				if (rsTaxClub.EndOfFile())
				{
					FCMessageBox.Show("Cannot find the selected tax club.", MsgBoxStyle.Exclamation, "Missing Tax Club - " + FCConvert.ToString(lngTaxClub));
					Cancel();
				}
				else
				{
					intNumberOfPayments = FCConvert.ToInt32(Math.Round(Conversion.Val(rsTaxClub.Get_Fields_Int32("NumberOfPayments"))));
					dtStartDate = (DateTime)rsTaxClub.Get_Fields_DateTime("StartDate");
					intAction = 1;
					// get the return address from the last form
					strAddr1 = frmTaxClubBooklet.InstancePtr.txtAddress[0].Text;
					strAddr2 = frmTaxClubBooklet.InstancePtr.txtAddress[1].Text;
					strAddr3 = frmTaxClubBooklet.InstancePtr.txtAddress[2].Text;
					strAddr4 = frmTaxClubBooklet.InstancePtr.txtAddress[3].Text;
					frmReportViewer.InstancePtr.Init(this);
				}
			}
			else
			{
				GetNextTC();
				if (!boolDone)
				{
					SetUpNewBooklet();
					frmReportViewer.InstancePtr.Init(this);
				}
				else
				{
					FCMessageBox.Show("No tax clubs selected.", MsgBoxStyle.Exclamation, "Selection");
					lngTCRow = 0;
					boolDone = false;
				}
			}
			rsPage.Dispose();
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (boolDone)
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
			}
			//Detail_Format();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			// lngTCRow = 0
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
			if (intAction <= intNumberOfPayments)
			{
				BindFields();
			}
			else
			{
				// do nothing
			}
			intAction += 1;
			if (intAction > intNumberOfPayments)
			{
				if (lngTaxClub > 0)
				{
					boolDone = true;
				}
				else
				{
					// this means that this could be reset for another booklet
					GetNextTC();
					if (lngCurrentTC > 0)
					{
						if (boolExtraPage)
						{
							this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
						}
						SetUpNewBooklet();
					}
					else
					{
						boolDone = true;
					}
				}
			}
		}

		private void BindFields()
		{
			// this will fill the information into the report
			// left side
			// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
			lblYear.Text = FCConvert.ToString(rsTaxClub.Get_Fields("Year")) + " " + FCConvert.ToString(rsTaxClub.Get_Fields("Type")) + " Tax Club";
			switch (rsTaxClub.Get_Fields_Int16("Freq"))
			{
				case 0:
					{
						lblMonth.Text = GetMonth();
						break;
					}
				case 1:
					{
						lblMonth.Text = GetWeek();
						break;
					}
				case 2:
					{
						lblMonth.Text = GetBiWeek();
						break;
					}
			}
			//end switch
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			lblAccountPayment.Text = "Acct: " + FCConvert.ToString(rsTaxClub.Get_Fields("Account")) + "  Pymt: " + FCConvert.ToString(intAction);
			// right side
			// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
			lblHeader.Text = FCConvert.ToString(rsTaxClub.Get_Fields("Year")) + " " + FCConvert.ToString(rsTaxClub.Get_Fields("Type")) + " Tax Club - " + lblMonth.Text;
			if (intAction == intNumberOfPayments)
			{
				// Please call for remaining balance
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				lblAccountLine.Text = "Acct: " + FCConvert.ToString(rsTaxClub.Get_Fields("Account")) + "-" + FCConvert.ToString(intAction) + "   Pay Amount: Please call.";
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				lblAccountLine.Text = "Acct: " + FCConvert.ToString(rsTaxClub.Get_Fields("Account")) + "-" + FCConvert.ToString(intAction) + "   Pay Amount: " + Strings.Format(rsTaxClub.Get_Fields_Double("PaymentAmount"), "#,##0.00");
			}
			lblName.Text = rsTaxClub.Get_Fields_String("Name");
			// address lines
			lblAddress1.Text = strAddr1;
			lblAddress2.Text = strAddr2;
			lblAddress3.Text = strAddr3;
			lblAddress4.Text = strAddr4;
		}
		// VBto upgrade warning: 'Return' As Variant --> As string
		private string GetMonth()
		{
			string GetMonth = "";
			// this function will return a string with the correct month and year
			GetMonth = Strings.Format(DateAndTime.DateAdd("M", intAction - 1, dtStartDate), "MMMM dd \"'\"yy");
			return GetMonth;
		}
		// VBto upgrade warning: 'Return' As Variant --> As string
		private string GetWeek()
		{
			string GetWeek = "";
			// this function will return a string with the correct month and year
			GetWeek = Strings.Format(DateAndTime.DateAdd("D", (intAction - 1) * 7, dtStartDate), "MMMM dd \"'\"yy");
			return GetWeek;
		}
		// VBto upgrade warning: 'Return' As Variant --> As string
		private string GetBiWeek()
		{
			string GetBiWeek = "";
			// this function will return a string with the correct month and year
			GetBiWeek = Strings.Format(DateAndTime.DateAdd("D", (intAction - 1) * 14, dtStartDate), "MMMM dd \"'\"yy");
			// MAL@20071206: Changed GetWeek to GetBiWeek
			return GetBiWeek;
		}

		private void GetNextTC()
		{
			// This will set the current tax club variable and move the row ahead
			if (lngTCRow < frmTaxClubBooklet.InstancePtr.vsTC.Rows - 1)
			{
				//FC:FINAL:DDU #i104 instead of "-1" as in VB6, set value to true
				lngTCRow = frmTaxClubBooklet.InstancePtr.vsTC.FindRow(true, lngTCRow + 1, frmTaxClubBooklet.InstancePtr.lngColCheck);
				if (lngTCRow > 0)
				{
					lngCurrentTC = FCConvert.ToInt32(Math.Round(Conversion.Val(frmTaxClubBooklet.InstancePtr.vsTC.TextMatrix(lngTCRow, frmTaxClubBooklet.InstancePtr.lngColTC))));
					boolDone = false;
				}
				else
				{
					lngCurrentTC = -1;
					boolDone = true;
				}
			}
			else
			{
				lngCurrentTC = -1;
				boolDone = true;
			}
		}

		private void SetUpNewBooklet()
		{
			// This will reset all the variables for this booklet
			rsTaxClub.OpenRecordset("SELECT * FROM TaxClub WHERE ID = " + FCConvert.ToString(lngCurrentTC), modExtraModules.strCLDatabase);
			if (rsTaxClub.EndOfFile())
			{
				GetNextTC();
				if (lngCurrentTC > 0)
				{
					SetUpNewBooklet();
				}
				else
				{
					boolDone = true;
					return;
				}
			}
			else
			{
				intNumberOfPayments = FCConvert.ToInt32(Math.Round(Conversion.Val(rsTaxClub.Get_Fields_Int32("NumberOfPayments"))));
				dtStartDate = (DateTime)rsTaxClub.Get_Fields_DateTime("StartDate");
				intAction = 1;
				// get the return address from the last form
				strAddr1 = frmTaxClubBooklet.InstancePtr.txtAddress[0].Text;
				strAddr2 = frmTaxClubBooklet.InstancePtr.txtAddress[1].Text;
				strAddr3 = frmTaxClubBooklet.InstancePtr.txtAddress[2].Text;
				strAddr4 = frmTaxClubBooklet.InstancePtr.txtAddress[3].Text;
				// Me.Show , MDIParent
			}
		}

		
	}
}
