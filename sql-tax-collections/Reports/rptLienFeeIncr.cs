﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptLienFeeIncr : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/09/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               06/05/2006              *
		// ********************************************************
		int lngRecCnt;
		int lngProcCnt;

		public rptLienFeeIncr()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ActiveReport1";
		}

		public static rptLienFeeIncr InstancePtr
		{
			get
			{
				return (rptLienFeeIncr)Sys.GetInstance(typeof(rptLienFeeIncr));
			}
		}

		protected rptLienFeeIncr _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public void Init(string strResult, string strTitle)
		{
			this.Run();
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = FCConvert.CBool(lngRecCnt > Information.UBound(modGlobal.Statics.arrDemand, 1));
			//Detail_Format();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lngRecCnt = 0;
			lngProcCnt = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			while (modGlobal.Statics.arrDemand[lngRecCnt].Processed == false)
			{
				lngRecCnt += 1;
				if (lngRecCnt > Information.UBound(modGlobal.Statics.arrDemand, 1))
				{
					Detail.Visible = false;
					return;
				}
			}
			fldAccount.Text = modGlobal.Statics.arrDemand[lngRecCnt].Account.ToString();
			fldName.Text = modGlobal.Statics.arrDemand[lngRecCnt].Name;
			fldYear.Text = FCConvert.ToString(modGlobal.Statics.arrDemand[lngRecCnt].Year / 10) + "-" + FCConvert.ToString(modGlobal.Statics.arrDemand[lngRecCnt].Year % 10);
			lngProcCnt += 1;
			lngRecCnt += 1;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			fldProcCnt.Text = lngProcCnt.ToString();
			fldTotalIncrease.Text = Strings.Format(lngProcCnt * 6, "Currency");
		}

		private void ReportHeader_Format(object sender, EventArgs e)
		{
			fldDate.Text = Strings.Format(DateTime.Now, "MM/dd/yyyy");
			fldUserID.Text = modGlobalConstants.Statics.gstrUserID;
		}

		
	}
}
