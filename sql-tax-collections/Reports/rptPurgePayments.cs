﻿//=========================================================
// ********************************************************
// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
// *
// WRITTEN BY     :               Jim Bertolino           *
// DATE           :               07/22/2004              *
// *
// MODIFIED BY    :               Jim Bertolino           *
// LAST UPDATED   :               11/02/2005              *
// ********************************************************

using fecherFoundation;
using Global;
using System;
using TWSharedLibrary;

namespace TWCL0000
{

    public partial class rptPurgePayments : BaseSectionReport
    {
        protected rptPurgePayments _InstancePtr;
        private bool boolLien;
        private int lngBillKey;

        private readonly clsDRWrapper rsData = new clsDRWrapper();

        public rptPurgePayments()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        public static rptPurgePayments InstancePtr => (rptPurgePayments)Sys.GetInstance(typeof(rptPurgePayments));

        private void InitializeComponentEx()
        {
            if (_InstancePtr == null) _InstancePtr = this;
            Name = "Account Detail";
        }

        /// <summary>
        ///     Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
            {
                _InstancePtr = null;
                Sys.ClearInstance(this);
            }
            if (disposing)
            {
                rsData?.Dispose();
            }
            base.Dispose(disposing);
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
        {
            eArgs.EOF = rsData.EndOfFile();
        }

        private void ActiveReport_ReportEnd(object sender, EventArgs e)
        {
            rsData.Reset();
        }

        private void ActiveReport_ReportStart(object sender, EventArgs e)
        {
            lngBillKey = FCConvert.ToInt32(UserData);
            rsData.OpenRecordset($"SELECT LienRecordNumber FROM BillingMaster WHERE ID = {FCConvert.ToString(lngBillKey)}", modExtraModules.strCLDatabase);

            if (rsData.EndOfFile())
            {
                Cancel();

                return;
            }

            if (rsData.Get_Fields_Int32("LienRecordNumber") != 0)
            {
                boolLien = true;
                rsData.OpenRecordset($"SELECT * FROM PaymentRec WHERE BillKey = {FCConvert.ToString(rsData.Get_Fields_Int32("LienRecordNumber"))} AND BillCode = 'L' ORDER BY RecordedTransactionDate", modExtraModules.strCLDatabase);
            }
            else
            {
                boolLien = false;
                rsData.OpenRecordset($"SELECT * FROM PaymentRec WHERE BillKey = {FCConvert.ToString(lngBillKey)} AND BillCode <> 'L' ORDER BY RecordedTransactionDate", modExtraModules.strCLDatabase);
            }
        }

        private void Detail_Format(object sender, EventArgs e)
        {
            try
            {
                if (rsData.EndOfFile()) return;

                BindFields();
                rsData.MoveNext();
            }
            catch (Exception ex)
            {
                FCMessageBox.Show($"Error #{FCConvert.ToString(Information.Err(ex).Number)} - {ex.GetBaseException().Message}.", MsgBoxStyle.Critical, "Detail Format Error");
            }
        }

        private void BindFields()
        {
            int lngAccount /*unused?*/;
            int lngYear /*unused?*/;

            if (rsData.EndOfFile()) return;

            try
            {
                // us the correct new information
                // check the next record so see if it is the same
                if (!boolLien)
                {
                    // TODO verify access database code that this is the right layout for NON lien
                    fldPrin.Text = modUtilities.FormatTotal(rsData.Get_Fields_Decimal("Principal"));
                    fldInt.Text = modUtilities.FormatTotal(Conversion.Val(rsData.Get_Fields_Decimal("CurrentInterest")) + Conversion.Val(rsData.Get_Fields_Decimal("PreLienInterest")));
                    fldCost.Text = modUtilities.FormatTotal(rsData.Get_Fields_Decimal("LienCost"));
                    fldRecDate.Text = Strings.Format(rsData.Get_Fields_DateTime("RecordedTransactionDate"), "MM/dd/yyyy");
                }
                else
                {
                    fldPrin.Text = modUtilities.FormatTotal(rsData.Get_Fields_Decimal("Principal"));
                    fldInt.Text = modUtilities.FormatTotal(Conversion.Val(rsData.Get_Fields_Decimal("CurrentInterest")) + Conversion.Val(rsData.Get_Fields_Decimal("PreLienInterest")));
                    fldCost.Text = modUtilities.FormatTotal(rsData.Get_Fields_Decimal("LienCost"));
                    fldRecDate.Text = Strings.Format(rsData.Get_Fields_DateTime("RecordedTransactionDate"), "MM/dd/yyyy");
                }
            }
            catch (Exception e)
            {
                FCMessageBox.Show($"Error - {e.GetBaseException().Message}.", MsgBoxStyle.Critical, "Error Binding Fields");                
            }

        }

    }
}