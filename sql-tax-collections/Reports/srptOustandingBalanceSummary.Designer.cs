namespace TWCL0000
{
    /// <summary>
    /// Summary description for srptOutstandingBalanceSummary.
    /// </summary>
    partial class srptOutstandingBalanceSummary
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(srptOutstandingBalanceSummary));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.lblSummary1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldSummary1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldSumCount1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.lblSummary = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummary1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSummary1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSumCount1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Height = 0F;
            this.pageHeader.Name = "pageHeader";
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblSummary1,
            this.fldSummary1,
            this.fldSumCount1});
            this.Detail.Height = 0.2187499F;
            this.Detail.Name = "Detail";
            // 
            // lblSummary1
            // 
            this.lblSummary1.Height = 0.1875F;
            this.lblSummary1.HyperLink = null;
            this.lblSummary1.Left = 0.09500001F;
            this.lblSummary1.Name = "lblSummary1";
            this.lblSummary1.Style = "font-family: \'Tahoma\'; text-align: left";
            this.lblSummary1.Text = null;
            this.lblSummary1.Top = 0.015375F;
            this.lblSummary1.Width = 0.625F;
            // 
            // fldSummary1
            // 
            this.fldSummary1.Height = 0.1875F;
            this.fldSummary1.Left = 1.345001F;
            this.fldSummary1.Name = "fldSummary1";
            this.fldSummary1.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldSummary1.Text = "0.00";
            this.fldSummary1.Top = 0.015375F;
            this.fldSummary1.Width = 1F;
            // 
            // fldSumCount1
            // 
            this.fldSumCount1.Height = 0.1875F;
            this.fldSumCount1.Left = 0.8450012F;
            this.fldSumCount1.Name = "fldSumCount1";
            this.fldSumCount1.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldSumCount1.Text = null;
            this.fldSumCount1.Top = 0.015375F;
            this.fldSumCount1.Width = 0.375F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // reportHeader1
            // 
            this.reportHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblSummary});
            this.reportHeader1.Name = "reportHeader1";
            // 
            // lblSummary
            // 
            this.lblSummary.Height = 0.1875F;
            this.lblSummary.HyperLink = null;
            this.lblSummary.Left = 0.09500001F;
            this.lblSummary.Name = "lblSummary";
            this.lblSummary.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
            this.lblSummary.Text = "Non Lien Summary";
            this.lblSummary.Top = 0.031F;
            this.lblSummary.Width = 2.25F;
            // 
            // reportFooter1
            // 
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label1,
            this.textBox1,
            this.textBox2,
            this.Line1});
            this.reportFooter1.Height = 0.3229167F;
            this.reportFooter1.Name = "reportFooter1";
            // 
            // label1
            // 
            this.label1.Height = 0.1875F;
            this.label1.HyperLink = null;
            this.label1.Left = 0.095F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: \'Tahoma\'; text-align: left";
            this.label1.Text = "Total";
            this.label1.Top = 0.068F;
            this.label1.Width = 0.625F;
            // 
            // textBox1
            // 
            this.textBox1.Height = 0.1875F;
            this.textBox1.Left = 1.345F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "font-family: \'Tahoma\'; text-align: right";
            this.textBox1.Text = "0.00";
            this.textBox1.Top = 0.06770833F;
            this.textBox1.Width = 1F;
            // 
            // textBox2
            // 
            this.textBox2.Height = 0.1875F;
            this.textBox2.Left = 0.84F;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "font-family: \'Tahoma\'; text-align: right";
            this.textBox2.Text = null;
            this.textBox2.Top = 0.058F;
            this.textBox2.Width = 0.375F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0.151F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.031F;
            this.Line1.Width = 2.25F;
            this.Line1.X1 = 0.151F;
            this.Line1.X2 = 2.401F;
            this.Line1.Y1 = 0.031F;
            this.Line1.Y2 = 0.031F;
            // 
            // srptOutstandingBalanceSummary
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 2.552083F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
            "l; font-size: 10pt; color: Black", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
            "lic", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblSummary1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSummary1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSumCount1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSummary1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSummary1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSumCount1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSummary;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
    }
}
