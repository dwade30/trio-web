﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptTaxClubMaster : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               02/11/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/16/2006              *
		// ********************************************************
		bool boolDone;
		bool boolAllTC;
		clsDRWrapper rsD = new clsDRWrapper();
		int lngAcct;
		int intReportType;
		bool boolShowDetail;

		public rptTaxClubMaster()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Account Detail";
		}

		public static rptTaxClubMaster InstancePtr
		{
			get
			{
				return (rptTaxClubMaster)Sys.GetInstance(typeof(rptTaxClubMaster));
			}
		}

		protected rptTaxClubMaster _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsD.Dispose();
            }
			base.Dispose(disposing);
		}
		// VBto upgrade warning: lngPassAcct As int	OnWrite(short, double)
		// VBto upgrade warning: intPassReportType As short	OnWrite(short, bool)
		public void Init(int lngPassAcct, short intPassReportType)
		{
			string strSQL = "";
			intReportType = intPassReportType;
			lngAcct = lngPassAcct;
			switch (intReportType)
			{
				case 0:
					{
						// Single Account
						// strSQL = "SELECT * FROM TaxClub WHERE Account = " & lngAcct & " AND NonActive = FALSE"
						strSQL = "select taxclubquery1.* from lienrec right join (billingmaster inner join (select * from taxclub) as taxclubquery1 on (billingmaster.id = taxclubquery1.billkey)) on (billingmaster.lienrecordnumber = lienrec.id) where taxclubquery1.account = " + FCConvert.ToString(lngAcct) + " and taxclubquery1.year > 0 and not (nonactive = 1) and ((billingmaster.lienrecordnumber = 0 and (taxdue1 + taxdue2 + taxdue3 + taxdue4 + billingmaster.demandfees - billingmaster.interestcharged) > (billingmaster.demandfeespaid + billingmaster.principalpaid + billingmaster.interestpaid)) or (billingmaster.lienrecordnumber > 0 and (lienrec.principal + lienrec.interest + lienrec.costs + lienrec.maturityfee - lienrec.interestcharged) > (lienrec.principalpaid + lienrec.plipaid + lienrec.costspaid + lienrec.interestpaid))) order by taxclubquery1.account,taxclubquery1.year";
						this.Name = "Master Listing - Account " + FCConvert.ToString(lngPassAcct);
						break;
					}
				case 1:
					{
						// All
						strSQL = "SELECT * FROM TaxClub ORDER BY Account, [Year]";
						this.Name = "Master Listing - All";
						break;
					}
				case 2:
					{
						// Active
						// strSQL = "SELECT * FROM TaxClub WHERE NonActive = FALSE AND Year > 0  ORDER BY Account, Year"
						// strSQL = "SELECT * FROM TaxClub WHERE NonActive = FALSE AND Year > 0 and (totalpaid < totalpayments) ORDER BY Account, Year"
						strSQL = "select taxclubquery1.* from lienrec right join (billingmaster inner join  (select * from taxclub) as taxclubquery1 on (billingmaster.id = taxclubquery1.billkey)) on (billingmaster.lienrecordnumber = lienrec.id) where taxclubquery1.year > 0 and not (IsNull(nonactive, 0) = 1) and ((billingmaster.lienrecordnumber = 0 and (taxdue1 + taxdue2 + taxdue3 + taxdue4 + billingmaster.demandfees - billingmaster.interestcharged) > (billingmaster.demandfeespaid + billingmaster.principalpaid + billingmaster.interestpaid)) or (billingmaster.lienrecordnumber > 0 and (lienrec.principal + lienrec.interest + lienrec.costs + lienrec.maturityfee - lienrec.interestcharged) > (lienrec.principalpaid + lienrec.plipaid + lienrec.costspaid + lienrec.interestpaid))) order by taxclubquery1.account,taxclubquery1.year";
						this.Name = "Master Listing - Active";
						break;
					}
				case 3:
					{
						// Non Active
						// strSQL = "SELECT * FROM TaxClub WHERE NonActive = TRUE OR Year = 0 ORDER BY Account, Year"
						// strSQL = "SELECT * FROM TaxClub WHERE NonActive = TRUE OR Year = 0 or (totalpayments <= totalpaid) ORDER BY Account, Year"
						strSQL = "select taxclubquery1.* from lienrec right join (billingmaster inner join  (select * from taxclub) as taxclubquery1 on (billingmaster.id = taxclubquery1.billkey)) on (billingmaster.lienrecordnumber = lienrec.id) where taxclubquery1.year = 0 or IsNull(nonactive, 0) = 1 or ((billingmaster.lienrecordnumber = 0 and (taxdue1 + taxdue2 + taxdue3 + taxdue4 + billingmaster.demandfees - billingmaster.interestcharged) <= (billingmaster.demandfeespaid + billingmaster.principalpaid + billingmaster.interestpaid)) or (billingmaster.lienrecordnumber > 0 and (lienrec.principal + lienrec.interest + lienrec.costs + lienrec.maturityfee - lienrec.interestcharged) <= (lienrec.principalpaid + lienrec.plipaid + lienrec.costspaid + lienrec.interestpaid))) order by taxclubquery1.account,taxclubquery1.year";
						this.Name = "Master Listing - Inactive";
						break;
					}
			}
			//end switch
			rsD.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
			if (rsD.EndOfFile())
			{
				FCMessageBox.Show("No Tax Clubs found that match the criteria.", MsgBoxStyle.Exclamation, "No Tax Club Found");
				Cancel();
			}
			else
			{
				boolShowDetail = FCConvert.CBool(FCMessageBox.Show("Would you like to show full detail on the report?", MsgBoxStyle.YesNo, "Full Detail") == DialogResult.Yes);
				frmReportViewer.InstancePtr.Init(this);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// this will allow one detail section to fire
			// If boolAllTC Then
			eArgs.EOF = rsD.EndOfFile();
			// Else
			// If boolDone Then
			// EOF = True
			// Else
			// boolDone = True
			// EOF = False
			// End If
			// End If
			//Detail_Format();
			BindFields();
			if (!rsD.EndOfFile())
			{
				rsD.MoveNext();
			}
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "t");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			switch (intReportType)
			{
				case 0:
					{
						// Single Account
						lblHeader.Text = "Tax Club Information";
						this.PageHeader.Height = 1500 / 1440f;
						break;
					}
				case 1:
					{
						lblHeader.Text = "Tax Club Report";
						break;
					}
				case 2:
					{
						lblHeader.Text = "Active Tax Club Report";
						break;
					}
				case 3:
					{
						lblHeader.Text = "Inactive Tax Club Report";
						break;
					}
			}
			//end switch
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			SetupFields();
			//FC:FINAL:AM:#i105 - move code to FetchData
			//BindFields();
			//if (!rsD.EndOfFile())
			//{
			//	rsD.MoveNext();
			//}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
			if (boolShowDetail)
			{
			}
			else
			{
				Line1.Visible = true;
				lblHeaderAccount.Visible = true;
				lblHeaderYear.Visible = true;
				lblHeaderName.Visible = true;
				lblHeaderAccount.Left = 0;
				lblHeaderYear.Left = lblHeaderAccount.Width;
				lblHeaderYear.Width = lblHeaderAccount.Width;
				fldName.Left = lblHeaderYear.Left + lblHeaderAccount.Width;
			}
		}

		private void BindFields()
		{
			var rsP = new clsDRWrapper();
            try
            {
                // On Error GoTo ERROR_HANDLER

                if (modStatusPayments.Statics.boolRE)
                {
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    rsP.OpenRecordset(
                        "SELECT * FROM Master WHERE RSAccount = " + FCConvert.ToString(rsD.Get_Fields("Account")),
                        modExtraModules.strREDatabase);
                }
                else
                {
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    rsP.OpenRecordset(
                        "SELECT * FROM PPMaster WHERE Account = " + FCConvert.ToString(rsD.Get_Fields("Account")),
                        modExtraModules.strPPDatabase);
                }

                // this will print the row from the grid
                // If boolAllTC Then
                // Account
                lblAccount.Text = "Account";
                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                fldAccount.Text = FCConvert.ToString(rsD.Get_Fields("Account"));
                // Name
                lblName.Text = "Name";
                fldName.Text = rsD.Get_Fields_String("Name");
                // Year
                lblYear.Text = "Year";
                // TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
                fldYear.Text = modExtraModules.FormatYear(FCConvert.ToString(rsD.Get_Fields("Year")));
                // Agreement Date
                lblAgreementDate.Text = "Agreement Date";
                fldAgreementDate.Text = Strings.Format(rsD.Get_Fields_DateTime("AgreementDate"), "MM/dd/yyyy");
                // Start Date
                lblStartingDate.Text = "Start Date";
                fldStartingDate.Text = Strings.Format(rsD.Get_Fields_DateTime("StartDate"), "MM/dd/yyyy");
                // Number Of Payments
                lblNumberOfPayments.Text = "Number of Payments";
                fldNumberOfPayments.Text = FCConvert.ToString(rsD.Get_Fields_Int32("NumberOfPayments"));
                // Payment Amount
                lblPaymentAmount.Text = "Payment Amount";
                fldPaymentAmount.Text = Strings.Format(rsD.Get_Fields_Double("PaymentAmount"), "#,##0.00");
                // Total Amount
                lblTotalAmount.Text = "Total Amount";
                fldTotalAmount.Text = Strings.Format(rsD.Get_Fields_Double("TotalPayments"), "#,##0.00");
                // Total Paid
                lblTotalPaid.Text = "Total Paid";
                fldTotalPaid.Text = Strings.Format(rsD.Get_Fields_Double("TotalPaid"), "#,##0.00");
                // MAL@20071217: Changed to use RE values
                // Tracker Reference: 11665
                // Map Lot
                // lblMapLot.Text = "Map Lot"
                // fldMapLot.Text = Trim(rsD.Fields("MapLot"))
                // Location
                // lblLocation.Text = "Location"
                // fldLocation.Text = Trim(rsD.Fields("Location"))
                if (rsP.RecordCount() > 0 && modStatusPayments.Statics.boolRE)
                {
                    // kk03272015 trocl-1135  Need to check for PP/RE
                    // Map Lot
                    lblMapLot.Text = "Map Lot";
                    fldMapLot.Text = Strings.Trim(FCConvert.ToString(rsP.Get_Fields_String("RSMapLot")));
                    // Location
                    lblLocation.Text = "Location";
                    fldLocation.Text = Strings.Trim(FCConvert.ToString(rsP.Get_Fields_String("RSLOCNUMALPH")) + " " +
                                                    FCConvert.ToString(rsP.Get_Fields_String("RSLOCAPT")) + " " +
                                                    FCConvert.ToString(rsP.Get_Fields_String("RSLOCSTREET"))) + " ";
                }
                else
                {
                    lblMapLot.Text = "Map Lot";
                    fldMapLot.Text = "";
                    lblLocation.Text = "Location";
                    fldLocation.Text = "";
                }

                // Last Paid Date
                lblLastPaid.Text = "Last Paid";
                if (!(rsD.Get_Fields_DateTime("LastPaidDate") is DateTime) ||
                    ((DateTime) rsD.Get_Fields_DateTime("LastPaidDate")).ToOADate() == 0)
                {
                    fldLastPaid.Text = "None";
                }
                else
                {
                    fldLastPaid.Text = Strings.Format(rsD.Get_Fields_DateTime("LastPaidDate"), "MM/dd/yyyy");
                }

                return;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                FCMessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message +
                    ".", MsgBoxStyle.Critical, "Error Binding Fields");
            }
            finally
            {
				rsP.Dispose();
            }
		}

		private void SetupFields()
		{
			if (boolShowDetail)
			{
				// leave this alone
			}
			else
			{
				// hide fields
				lblNumberOfPayments.Top = 0;
				lblPaymentAmount.Top = 0;
				lblTotalAmount.Top = 0;
				lblTotalPaid.Top = 0;
				lblLastPaid.Top = 0;
				lblMapLot.Top = 0;
				lblAgreementDate.Top = 0;
				lblStartingDate.Top = 0;
				lblLocation.Top = 0;
				lblYear.Top = 0;
				lblAccount.Top = 0;
				lblName.Top = 0;
				fldNumberOfPayments.Top = 0;
				fldTotalAmount.Top = 0;
				fldTotalPaid.Top = 0;
				fldLastPaid.Top = 0;
				fldMapLot.Top = 0;
				fldAgreementDate.Top = 0;
				fldStartingDate.Top = 0;
				fldLocation.Top = 0;
				fldPaymentAmount.Top = 0;
				lblNumberOfPayments.Visible = false;
				lblPaymentAmount.Visible = false;
				lblTotalAmount.Visible = false;
				lblTotalPaid.Visible = false;
				lblLastPaid.Visible = false;
				lblMapLot.Visible = false;
				lblAgreementDate.Visible = false;
				lblStartingDate.Visible = false;
				lblLocation.Visible = false;
				lblYear.Visible = false;
				lblAccount.Visible = false;
				lblName.Visible = false;
				fldNumberOfPayments.Visible = false;
				// fldPaymentAmount.Visible = False
				fldTotalAmount.Visible = false;
				fldTotalPaid.Visible = false;
				fldLastPaid.Visible = false;
				fldMapLot.Visible = false;
				fldAgreementDate.Visible = false;
				fldStartingDate.Visible = false;
				fldLocation.Visible = false;
				fldAccount.Left = 0;
				fldYear.Left = fldAccount.Width;
				fldYear.Width = fldAccount.Width;
				fldName.Left = fldYear.Left + fldYear.Width;
				fldName.Top = fldAccount.Top;
				fldPaymentAmount.Left = lblHeaderPaymentAmount.Left;
				// fldName.Left + fldName.Width
				lblHeaderPaymentAmount.Visible = true;
				lblHeaderName.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				lblHeaderName.Left = fldName.Left;
				fldYear.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				// shrink the detail section
				this.Detail.Height = 300 / 1440f;
			}
		}

		
	}
}
