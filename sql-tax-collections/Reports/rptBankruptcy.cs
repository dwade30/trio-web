﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for rptBankruptcy.
	/// </summary>
	public partial class rptBankruptcy : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// Date           :               09/26/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// Last Updated   :               05/12/2006              *
		// ********************************************************
		string strSQL;
		clsDRWrapper rsData = new clsDRWrapper();
		int lngTotalMortgageHolders;

		public rptBankruptcy()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Bankruptcy Report";
		}

		public static rptBankruptcy InstancePtr
		{
			get
			{
				return (rptBankruptcy)Sys.GetInstance(typeof(rptBankruptcy));
			}
		}

		protected rptBankruptcy _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}

		public void Init(string pstrSQL, bool modalDialog)
		{
			// this sub will set the SQL string
			strSQL = pstrSQL;
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), showModal: modalDialog);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "t");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			rsData.OpenRecordset(strSQL, modExtraModules.strREDatabase);
			rsData.InsertName("OwnerPartyID", "Own1", false, true, true, "RE", false, "", true, "");
			lngTotalMortgageHolders = rsData.RecordCount();
			if (lngTotalMortgageHolders == 0)
			{
				// hide the headers if there are no records
				lblAcct.Visible = false;
				lblName.Visible = false;
				lblAddress.Visible = false;
			}
		}

		private void SetupFields()
		{
			// Set each field and label's visible property to True/False depending on which fields the user
			// has selected from the Custom Report screen then move them accordingly
			int intRow;
			float lngHt;
			intRow = 0;
			lngHt = 270 / 1440F;
			if (Strings.Trim(fldAddress1.Text) != "")
			{
				fldAddress1.Top = lngHt * intRow;
				intRow += 1;
			}
			else
			{
				fldAddress1.Top = 0;
			}
			if (Strings.Trim(fldAddress2.Text) != "")
			{
				fldAddress2.Top = lngHt * intRow;
				intRow += 1;
			}
			else
			{
				fldAddress2.Top = 0;
			}
			if (Strings.Trim(fldAddress3.Text) != "")
			{
				fldAddress3.Top = lngHt * intRow;
				intRow += 1;
			}
			else
			{
				fldAddress3.Top = 0;
			}
			this.Detail.Height = intRow * lngHt + 200 / 1440F;
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the information into the fields
				if (!rsData.EndOfFile())
				{
					fldAcct.Text = FCConvert.ToString(rsData.Get_Fields_Int32("RSAccount"));
					fldName.Text = rsData.Get_Fields_String("DeedName1");
					fldAddress1.Text = rsData.Get_Fields_String("Own1Address1");
					fldAddress2.Text = rsData.Get_Fields_String("Own1Address2");
					fldAddress3.Text = FCConvert.ToString(rsData.Get_Fields("Own1City")) + ", " + FCConvert.ToString(rsData.Get_Fields("Own1State")) + " " + FCConvert.ToString(rsData.Get_Fields("Own1Zip"));
					SetupFields();
					// move to the next record in the query
					rsData.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error in Bankruptcy Report");
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			if (this.Document.Pages.Count > 0)
			{
				modGlobalFunctions.IncrementSavedReports("LastCLBankruptcy");
				this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastCLBankruptcy1.RDF"));
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void SetupTotals()
		{
			// this sub will fill in the footer line at the bottom of the report
			lblFooter.Text = "There were " + FCConvert.ToString(lngTotalMortgageHolders) + " accounts not processed.";
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			SetupTotals();
		}

		public void Save_Report()
		{
			// save this report for future access
			if (this.Document.Pages.Count > 0)
			{
				modGlobalFunctions.IncrementSavedReports("LastCLBankruptcy");
				this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastCLBankruptcy1.RDF"));
			}
			// Unload Me
		}

		
	}
}
