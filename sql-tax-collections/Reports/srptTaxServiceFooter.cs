using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using fecherFoundation;
using GrapeCity.ActiveReports.SectionReportModel;
using SharedApplication.Extensions;

namespace TWCL0000
{
    /// <summary>
    /// Summary description for srptTaxServiceFooter.
    /// </summary>
    public partial class srptTaxServiceFooter : FCSectionReport
    {
        private IEnumerator<TaxServiceSummarySummaryItem> reportEnumerator;
        public srptTaxServiceFooter()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            this.ReportStart += SrptTaxServiceFooter_ReportStart;
            this.FetchData += SrptTaxServiceFooter_FetchData;
            this.Detail.Format += Detail_Format;
        }

        private void Detail_Format(object sender, EventArgs e)
        {
            if (reportEnumerator.Current != null)
            {
                var summary = reportEnumerator.Current;
                fldSumYear1.Text = summary.Year.ToString();
                fldSumBalance1.Text = summary.Balance.FormatAsCurrencyNoSymbol();
                fldSumCount1.Text = summary.Count.ToString("#,##0");
                fldSumInterest1.Text = summary.Interest.FormatAsCurrencyNoSymbol();
            }
        }

        private void SrptTaxServiceFooter_FetchData(object sender, FetchEventArgs eArgs)
        {
            eArgs.EOF = !reportEnumerator.MoveNext();
        }

        private void SrptTaxServiceFooter_ReportStart(object sender, EventArgs e)
        {
            var reportList = (IEnumerable<TaxServiceSummarySummaryItem>)this.UserData;
            reportEnumerator = reportList.GetEnumerator();
            txtTotalBalance.Text = reportList.Sum(r => r.Balance).FormatAsCurrencyNoSymbol();
            txtTotalCount.Text = reportList.Sum(r => r.Count).ToString("#,##0");
            txtTotalInterest.Text = reportList.Sum(r => r.Interest).FormatAsCurrencyNoSymbol();
        }      
    }
}
