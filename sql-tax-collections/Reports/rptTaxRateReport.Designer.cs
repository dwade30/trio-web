﻿namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	partial class rptTaxRateReport
	{
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptTaxRateReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldKey = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTaxRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldIntRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRateType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNumPer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDueDate1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldIntStart1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDueDate2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldIntStart2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDueDate3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldIntStart3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDueDate4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldIntStart4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lnHeader = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblKey = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTaxRate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblRateType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPeriods = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDueDate1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblIntStart1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDueDate2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblIntStart2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDueDate3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblIntStart3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDueDate4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblIntStart4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblIntRate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.fldKey)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTaxRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldIntRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRateType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNumPer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDueDate1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldIntStart1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDueDate2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldIntStart2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDueDate3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldIntStart3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDueDate4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldIntStart4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblKey)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTaxRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRateType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPeriods)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDueDate1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblIntStart1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDueDate2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblIntStart2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDueDate3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblIntStart3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDueDate4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblIntStart4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblIntRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldKey,
				this.fldYear,
				this.fldTaxRate,
				this.fldIntRate,
				this.fldRateType,
				this.fldNumPer,
				this.fldDueDate1,
				this.fldIntStart1,
				this.fldDueDate2,
				this.fldIntStart2,
				this.fldDueDate3,
				this.fldIntStart3,
				this.fldDueDate4,
				this.fldIntStart4
			});
			this.Detail.Height = 0.21875F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldKey
			// 
			this.fldKey.Height = 0.1875F;
			this.fldKey.Left = 0.0625F;
			this.fldKey.Name = "fldKey";
			this.fldKey.Style = "font-family: \'Tahoma\'; text-align: center";
			this.fldKey.Text = null;
			this.fldKey.Top = 0F;
			this.fldKey.Width = 0.375F;
			// 
			// fldYear
			// 
			this.fldYear.CanGrow = false;
			this.fldYear.Height = 0.1875F;
			this.fldYear.Left = 0.5F;
			this.fldYear.Name = "fldYear";
			this.fldYear.Style = "font-family: \'Tahoma\'; text-align: center; white-space: nowrap";
			this.fldYear.Text = null;
			this.fldYear.Top = 0F;
			this.fldYear.Width = 0.5F;
			// 
			// fldTaxRate
			// 
			this.fldTaxRate.Height = 0.1875F;
			this.fldTaxRate.Left = 1.0625F;
			this.fldTaxRate.Name = "fldTaxRate";
			this.fldTaxRate.Style = "font-family: \'Tahoma\'; text-align: center";
			this.fldTaxRate.Text = null;
			this.fldTaxRate.Top = 0F;
			this.fldTaxRate.Width = 0.6875F;
			// 
			// fldIntRate
			// 
			this.fldIntRate.Height = 0.1875F;
			this.fldIntRate.Left = 1.8125F;
			this.fldIntRate.Name = "fldIntRate";
			this.fldIntRate.Style = "font-family: \'Tahoma\'; text-align: center";
			this.fldIntRate.Text = null;
			this.fldIntRate.Top = 0F;
			this.fldIntRate.Width = 0.6875F;
			// 
			// fldRateType
			// 
			this.fldRateType.Height = 0.1875F;
			this.fldRateType.Left = 2.5625F;
			this.fldRateType.Name = "fldRateType";
			this.fldRateType.Style = "font-family: \'Tahoma\'; text-align: center";
			this.fldRateType.Text = null;
			this.fldRateType.Top = 0F;
			this.fldRateType.Width = 0.375F;
			// 
			// fldNumPer
			// 
			this.fldNumPer.Height = 0.1875F;
			this.fldNumPer.Left = 3F;
			this.fldNumPer.Name = "fldNumPer";
			this.fldNumPer.Style = "font-family: \'Tahoma\'; text-align: center";
			this.fldNumPer.Text = null;
			this.fldNumPer.Top = 0F;
			this.fldNumPer.Width = 0.625F;
			// 
			// fldDueDate1
			// 
			this.fldDueDate1.Height = 0.1875F;
			this.fldDueDate1.Left = 3.6875F;
			this.fldDueDate1.Name = "fldDueDate1";
			this.fldDueDate1.Style = "font-family: \'Tahoma\'; text-align: center";
			this.fldDueDate1.Text = null;
			this.fldDueDate1.Top = 0F;
			this.fldDueDate1.Width = 0.625F;
			// 
			// fldIntStart1
			// 
			this.fldIntStart1.Height = 0.1875F;
			this.fldIntStart1.Left = 4.375F;
			this.fldIntStart1.Name = "fldIntStart1";
			this.fldIntStart1.Style = "font-family: \'Tahoma\'; text-align: center";
			this.fldIntStart1.Text = null;
			this.fldIntStart1.Top = 0F;
			this.fldIntStart1.Width = 0.625F;
			// 
			// fldDueDate2
			// 
			this.fldDueDate2.Height = 0.1875F;
			this.fldDueDate2.Left = 5.0625F;
			this.fldDueDate2.Name = "fldDueDate2";
			this.fldDueDate2.Style = "font-family: \'Tahoma\'; text-align: center";
			this.fldDueDate2.Text = null;
			this.fldDueDate2.Top = 0F;
			this.fldDueDate2.Width = 0.625F;
			// 
			// fldIntStart2
			// 
			this.fldIntStart2.Height = 0.1875F;
			this.fldIntStart2.Left = 5.75F;
			this.fldIntStart2.Name = "fldIntStart2";
			this.fldIntStart2.Style = "font-family: \'Tahoma\'; text-align: center";
			this.fldIntStart2.Text = null;
			this.fldIntStart2.Top = 0F;
			this.fldIntStart2.Width = 0.625F;
			// 
			// fldDueDate3
			// 
			this.fldDueDate3.Height = 0.1875F;
			this.fldDueDate3.Left = 6.4375F;
			this.fldDueDate3.Name = "fldDueDate3";
			this.fldDueDate3.Style = "font-family: \'Tahoma\'; text-align: center";
			this.fldDueDate3.Text = null;
			this.fldDueDate3.Top = 0F;
			this.fldDueDate3.Width = 0.625F;
			// 
			// fldIntStart3
			// 
			this.fldIntStart3.Height = 0.1875F;
			this.fldIntStart3.Left = 7.125F;
			this.fldIntStart3.Name = "fldIntStart3";
			this.fldIntStart3.Style = "font-family: \'Tahoma\'; text-align: center";
			this.fldIntStart3.Text = null;
			this.fldIntStart3.Top = 0F;
			this.fldIntStart3.Width = 0.625F;
			// 
			// fldDueDate4
			// 
			this.fldDueDate4.Height = 0.1875F;
			this.fldDueDate4.Left = 7.8125F;
			this.fldDueDate4.Name = "fldDueDate4";
			this.fldDueDate4.Style = "font-family: \'Tahoma\'; text-align: center";
			this.fldDueDate4.Text = null;
			this.fldDueDate4.Top = 0F;
			this.fldDueDate4.Width = 0.625F;
			// 
			// fldIntStart4
			// 
			this.fldIntStart4.Height = 0.1875F;
			this.fldIntStart4.Left = 8.5F;
			this.fldIntStart4.Name = "fldIntStart4";
			this.fldIntStart4.Style = "font-family: \'Tahoma\'; text-align: center";
			this.fldIntStart4.Text = null;
			this.fldIntStart4.Top = 0F;
			this.fldIntStart4.Width = 0.625F;
			// 
			// PageHeader
			// 
			this.PageHeader.CanGrow = false;
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblHeader,
				this.lblDate,
				this.lblPage,
				this.lblTime,
				this.lblMuniName,
				this.lnHeader,
				this.lblKey,
				this.lblYear,
				this.lblTaxRate,
				this.lblRateType,
				this.lblPeriods,
				this.lblDueDate1,
				this.lblIntStart1,
				this.lblDueDate2,
				this.lblIntStart2,
				this.lblDueDate3,
				this.lblIntStart3,
				this.lblDueDate4,
				this.lblIntStart4,
				this.lblIntRate
			});
			this.PageHeader.Height = 0.8541667F;
			this.PageHeader.Name = "PageHeader";
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.3125F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblHeader.Text = "Tax Rate Report";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 9.1875F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 8.0625F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1.125F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 8.0625F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblPage.Text = null;
			this.lblPage.Top = 0.1875F;
			this.lblPage.Width = 1.125F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1875F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.1875F;
			this.lblTime.Width = 1.125F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 2.5625F;
			// 
			// lnHeader
			// 
			this.lnHeader.Height = 0F;
			this.lnHeader.Left = 0F;
			this.lnHeader.LineWeight = 1F;
			this.lnHeader.Name = "lnHeader";
			this.lnHeader.Top = 0.8125F;
			this.lnHeader.Width = 9.1875F;
			this.lnHeader.X1 = 0F;
			this.lnHeader.X2 = 9.1875F;
			this.lnHeader.Y1 = 0.8125F;
			this.lnHeader.Y2 = 0.8125F;
			// 
			// lblKey
			// 
			this.lblKey.Height = 0.1875F;
			this.lblKey.HyperLink = null;
			this.lblKey.Left = 0.0625F;
			this.lblKey.Name = "lblKey";
			this.lblKey.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblKey.Text = "Key";
			this.lblKey.Top = 0.625F;
			this.lblKey.Width = 0.375F;
			// 
			// lblYear
			// 
			this.lblYear.Height = 0.1875F;
			this.lblYear.HyperLink = null;
			this.lblYear.Left = 0.5F;
			this.lblYear.Name = "lblYear";
			this.lblYear.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblYear.Text = "Year";
			this.lblYear.Top = 0.625F;
			this.lblYear.Width = 0.5F;
			// 
			// lblTaxRate
			// 
			this.lblTaxRate.Height = 0.1875F;
			this.lblTaxRate.HyperLink = null;
			this.lblTaxRate.Left = 1.0625F;
			this.lblTaxRate.Name = "lblTaxRate";
			this.lblTaxRate.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblTaxRate.Text = "Tax Rate";
			this.lblTaxRate.Top = 0.625F;
			this.lblTaxRate.Width = 0.6875F;
			// 
			// lblRateType
			// 
			this.lblRateType.Height = 0.375F;
			this.lblRateType.HyperLink = null;
			this.lblRateType.Left = 2.5625F;
			this.lblRateType.Name = "lblRateType";
			this.lblRateType.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblRateType.Text = "Rate Type";
			this.lblRateType.Top = 0.4375F;
			this.lblRateType.Width = 0.375F;
			// 
			// lblPeriods
			// 
			this.lblPeriods.Height = 0.375F;
			this.lblPeriods.HyperLink = null;
			this.lblPeriods.Left = 3F;
			this.lblPeriods.Name = "lblPeriods";
			this.lblPeriods.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblPeriods.Text = "Num of Periods";
			this.lblPeriods.Top = 0.4375F;
			this.lblPeriods.Width = 0.625F;
			// 
			// lblDueDate1
			// 
			this.lblDueDate1.Height = 0.375F;
			this.lblDueDate1.HyperLink = null;
			this.lblDueDate1.Left = 3.6875F;
			this.lblDueDate1.Name = "lblDueDate1";
			this.lblDueDate1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblDueDate1.Text = "Due Date 1";
			this.lblDueDate1.Top = 0.4375F;
			this.lblDueDate1.Width = 0.625F;
			// 
			// lblIntStart1
			// 
			this.lblIntStart1.Height = 0.375F;
			this.lblIntStart1.HyperLink = null;
			this.lblIntStart1.Left = 4.375F;
			this.lblIntStart1.Name = "lblIntStart1";
			this.lblIntStart1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblIntStart1.Text = "Interest Start 1";
			this.lblIntStart1.Top = 0.4375F;
			this.lblIntStart1.Width = 0.625F;
			// 
			// lblDueDate2
			// 
			this.lblDueDate2.Height = 0.375F;
			this.lblDueDate2.HyperLink = null;
			this.lblDueDate2.Left = 5.0625F;
			this.lblDueDate2.Name = "lblDueDate2";
			this.lblDueDate2.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblDueDate2.Text = "Due Date 2";
			this.lblDueDate2.Top = 0.4375F;
			this.lblDueDate2.Width = 0.625F;
			// 
			// lblIntStart2
			// 
			this.lblIntStart2.Height = 0.375F;
			this.lblIntStart2.HyperLink = null;
			this.lblIntStart2.Left = 5.75F;
			this.lblIntStart2.Name = "lblIntStart2";
			this.lblIntStart2.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblIntStart2.Text = "Interest Start 2";
			this.lblIntStart2.Top = 0.4375F;
			this.lblIntStart2.Width = 0.625F;
			// 
			// lblDueDate3
			// 
			this.lblDueDate3.Height = 0.375F;
			this.lblDueDate3.HyperLink = null;
			this.lblDueDate3.Left = 6.4375F;
			this.lblDueDate3.Name = "lblDueDate3";
			this.lblDueDate3.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblDueDate3.Text = "Due Date 3";
			this.lblDueDate3.Top = 0.4375F;
			this.lblDueDate3.Width = 0.625F;
			// 
			// lblIntStart3
			// 
			this.lblIntStart3.Height = 0.375F;
			this.lblIntStart3.HyperLink = null;
			this.lblIntStart3.Left = 7.125F;
			this.lblIntStart3.Name = "lblIntStart3";
			this.lblIntStart3.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblIntStart3.Text = "Interest Start 3";
			this.lblIntStart3.Top = 0.4375F;
			this.lblIntStart3.Width = 0.625F;
			// 
			// lblDueDate4
			// 
			this.lblDueDate4.Height = 0.375F;
			this.lblDueDate4.HyperLink = null;
			this.lblDueDate4.Left = 7.8125F;
			this.lblDueDate4.Name = "lblDueDate4";
			this.lblDueDate4.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblDueDate4.Text = "Due Date 4";
			this.lblDueDate4.Top = 0.4375F;
			this.lblDueDate4.Width = 0.625F;
			// 
			// lblIntStart4
			// 
			this.lblIntStart4.Height = 0.375F;
			this.lblIntStart4.HyperLink = null;
			this.lblIntStart4.Left = 8.5F;
			this.lblIntStart4.Name = "lblIntStart4";
			this.lblIntStart4.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblIntStart4.Text = "Interest Start 4";
			this.lblIntStart4.Top = 0.4375F;
			this.lblIntStart4.Width = 0.625F;
			// 
			// lblIntRate
			// 
			this.lblIntRate.Height = 0.375F;
			this.lblIntRate.HyperLink = null;
			this.lblIntRate.Left = 1.8125F;
			this.lblIntRate.Name = "lblIntRate";
			this.lblIntRate.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblIntRate.Text = "Interest Rate";
			this.lblIntRate.Top = 0.4375F;
			this.lblIntRate.Width = 0.6875F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// rptTaxRateReport
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 9.239583F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
			((System.ComponentModel.ISupportInitialize)(this.fldKey)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTaxRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldIntRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRateType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNumPer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDueDate1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldIntStart1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDueDate2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldIntStart2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDueDate3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldIntStart3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDueDate4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldIntStart4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblKey)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTaxRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRateType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPeriods)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDueDate1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblIntStart1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDueDate2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblIntStart2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDueDate3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblIntStart3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDueDate4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblIntStart4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblIntRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldKey;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTaxRate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldIntRate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRateType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNumPer;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDueDate1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldIntStart1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDueDate2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldIntStart2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDueDate3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldIntStart3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDueDate4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldIntStart4;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblKey;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTaxRate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRateType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPeriods;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDueDate1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblIntStart1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDueDate2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblIntStart2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDueDate3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblIntStart3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDueDate4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblIntStart4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblIntRate;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
