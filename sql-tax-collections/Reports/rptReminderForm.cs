﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptReminderForm : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               12/23/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               01/10/2007              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		// Dim rsRate                      As New clsDRWrapper
		// Dim rsLData                     As New clsDRWrapper
		clsDRWrapper rsTemp = new clsDRWrapper();
		clsDRWrapper rsCheckAmt = new clsDRWrapper();
		DateTime dtDate;
		string strIntDept;
		string strIntDeptPhone;
		string strMatDept;
		string strMatDeptPhone;
		public string strHighYear = string.Empty;
		public string strLastMaturedYear = string.Empty;
		// kgk trocl-693/trocl-391  Set limit for oldest year to show
		string strMod = "";
		string strDataSting;
		public bool boolNoSummary;
		public bool boolLienedRecords;
		public bool boolRetry;
		public bool boolNoValues;
		string strReportTitle;
		DateTime dtMailDate;
		int lngIndex;
		double dblMinimumAmount;
		// MAL@20071207
		bool boolPrevYears;
		// kgk 06-27-2012 trocl-923  Add flag for prev years
		string strFields = "";
		string strQuerySQL = "";
		bool boolRE = false;
        ReminderNoticeOptions reminderNoticeOptions;
        private cAddressControllerCL tAddrCtrlr = new cAddressControllerCL();

		public rptReminderForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Reminder Notice - Form";
		}

		public static rptReminderForm InstancePtr
		{
			get
			{
				return (rptReminderForm)Sys.GetInstance(typeof(rptReminderForm));
			}
		}

		protected rptReminderForm _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
				rsTemp?.Dispose();
				rsCheckAmt?.Dispose();
            }
			base.Dispose(disposing);
		}
        // VBto upgrade warning: dblPassMinAmount As Variant --> As double
        //public void Init(string strSQL, DateTime dtInterestDate, DateTime dtMailingDate, string strOriginalString, string strPassReportTitle, double dblPassMinAmount = 0)
        public void Init(ReminderNoticeOptions options)
        {
			try
			{
                reminderNoticeOptions = options;
                boolRE = modStatusPayments.Statics.boolRE;
				// On Error GoTo ERROR_HANDLER
				//clsReportPrinterFunctions rpfFont = new clsReportPrinterFunctions();
				string strPrinterFont = "";
				int const_PrintToolID/*unused?*/;
				int lngCT/*unused?*/;
                strDataSting = options.OriginalString;
                strReportTitle = options.ReportTitle;
                dblMinimumAmount = options.MinimumAmount;
                frmWait.InstancePtr.Unload();
                boolLienedRecords = options.LienedRecords;
                boolPrevYears = options.PreviousYears;
				// kgk trocl-923
                lngIndex = 0;
				FCUtils.EraseSafe(modReminderNoticeSummary.Statics.arrReminderSummaryList);
				modReminderNoticeSummary.Statics.arrReminderSummaryList = new modReminderNoticeSummary.ReminderSummaryList[0 + 1];
				//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
				modReminderNoticeSummary.Statics.arrReminderSummaryList[0] = new modReminderNoticeSummary.ReminderSummaryList(0);

				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Report");
                // use the info that is passed into the report
                dtDate = options.InterestDate;
                dtMailDate = options.MailDate;
                strIntDept = options.InterestDept;
                strIntDeptPhone = options.InterestDeptPhone;
                strMatDept = options.MaturedDept;
                strMatDeptPhone = options.MaturedDeptPhone;
                strHighYear = options.HighYear;
                strLastMaturedYear = options.LastMaturedYear;
                fldSignOut.Text = options.SignOut;
                fldSignerName.Text = options.SignerName;
                fldSignerTitle.Text = options.SignerTitle;
				if (boolRE)
				{
					rsData.OpenRecordset(options.Query, modExtraModules.strCLDatabase);
					strMod = "Real Estate";
				}
				else
				{
					rsData.OpenRecordset(options.Query, modExtraModules.strCLDatabase);
					// strPPDatabase
					strMod = "Personal Property";
				}
				if (rsData.EndOfFile())
				{
					frmWait.InstancePtr.Unload();
					boolNoSummary = true;
					FCMessageBox.Show("No matching records were found.", MsgBoxStyle.Information, strMod + " Reminder Form");
				}
				else
				{
					boolNoSummary = false;
					frmReportViewer.InstancePtr.Init(this);
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Initializing Forms");
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
			//Detail_Format();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			if (boolRetry)
			{
				if (this.Document.Pages.Count > 1)
				{
					this.Document.Pages.RemoveAt(this.Document.Pages.Count - 1);
					//this.Document.Pages.Commit();
				}
				else
				{
					boolNoSummary = true;
					frmWait.InstancePtr.Unload();
					FCMessageBox.Show("No results found.", MsgBoxStyle.Exclamation, "No Results");
					//Cancel();                                        
				}
			}
			else
			{
				Array.Resize(ref modReminderNoticeSummary.Statics.arrReminderSummaryList, lngIndex + 1);
			}
			if (Information.UBound(modReminderNoticeSummary.Statics.arrReminderSummaryList, 1) > 0)
			{
				if (boolRetry)
				{
					Array.Resize(ref modReminderNoticeSummary.Statics.arrReminderSummaryList, Information.UBound(modReminderNoticeSummary.Statics.arrReminderSummaryList, 1) - 1 + 1);
				}
				else
				{
					if (Information.UBound(modReminderNoticeSummary.Statics.arrReminderSummaryList, 1) > 2)
					{
						if (modReminderNoticeSummary.Statics.arrReminderSummaryList[Information.UBound(modReminderNoticeSummary.Statics.arrReminderSummaryList, 1) - 2].Account == modReminderNoticeSummary.Statics.arrReminderSummaryList[Information.UBound(modReminderNoticeSummary.Statics.arrReminderSummaryList, 1) - 3].Account)
						{
							Array.Resize(ref modReminderNoticeSummary.Statics.arrReminderSummaryList, Information.UBound(modReminderNoticeSummary.Statics.arrReminderSummaryList, 1) - 2 + 1);
						}
						else
						{
							Array.Resize(ref modReminderNoticeSummary.Statics.arrReminderSummaryList, Information.UBound(modReminderNoticeSummary.Statics.arrReminderSummaryList, 1) - 1 + 1);
						}
					}
					else if (Information.UBound(modReminderNoticeSummary.Statics.arrReminderSummaryList, 1) > 1)
					{
						if (modReminderNoticeSummary.Statics.arrReminderSummaryList[Information.UBound(modReminderNoticeSummary.Statics.arrReminderSummaryList, 1) - 1].Account == modReminderNoticeSummary.Statics.arrReminderSummaryList[Information.UBound(modReminderNoticeSummary.Statics.arrReminderSummaryList, 1) - 2].Account)
						{
							Array.Resize(ref modReminderNoticeSummary.Statics.arrReminderSummaryList, Information.UBound(modReminderNoticeSummary.Statics.arrReminderSummaryList, 1) - 2 + 1);
						}
						else
						{
							Array.Resize(ref modReminderNoticeSummary.Statics.arrReminderSummaryList, Information.UBound(modReminderNoticeSummary.Statics.arrReminderSummaryList, 1) - 1 + 1);
						}
					}
					else
					{
						Array.Resize(ref modReminderNoticeSummary.Statics.arrReminderSummaryList, Information.UBound(modReminderNoticeSummary.Statics.arrReminderSummaryList, 1) - 1 + 1);
					}
				}
			}
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			this.PageSettings.Margins.Top = 1F + FCConvert.ToSingle(modGlobal.Statics.gdblReminderFormAdjustV);
			this.PageSettings.Margins.Left = 1F + FCConvert.ToSingle(modGlobal.Statics.gdblReminderFormAdjustH);
			lblDate.Text = Strings.Format(dtMailDate, "MM/dd/yyyy");
			if (modGlobal.Statics.gboolUseSigFile)
			{
				imgSig.Visible = true;
				//imgSig.ZOrder(0);
				// Line2.ZOrder 0
				imgSig.Image = FCUtils.LoadPicture(modSignatureFile.Statics.gstrCollectorSigPath);
				imgSig.PictureAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.BottomLeft;
			}
			else
			{
				imgSig.Visible = false;
			}
            if (reminderNoticeOptions.UseTownLogo && Strings.Trim(modGlobalConstants.Statics.gstrTownSealPath) != "")
			{
                // MAL@20080611: Check for file's existence
                // Tracker Reference: 14008
                if (File.Exists(modGlobalConstants.Statics.gstrTownSealPath))
				{
					imgLogo.Visible = true;
					imgLogo.Image = FCUtils.LoadPicture(modGlobalConstants.Statics.gstrTownSealPath);
					imgLogo.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Stretch;
				}
				else
				{
					imgLogo.Visible = false;
				}
			}
			srptReminderFormDetail.Report = new srptReminderForm();
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			frmWait.InstancePtr.Unload();
			if (!boolNoSummary)
			{
				modReminderNoticeSummary.Statics.intRPTReminderSummaryType = 2;
				// kk041112014  trocl-560 Open summary report in viewer so can be exported
				// rptReminderSummary.Show , MDIParent
				rptReminderSummary.InstancePtr.Close();
                // Unload the report; it's not refreshing otherwise
                //frmRPTViewer.InstancePtr.Init("", "Reminder Notice Summary", 100, rptReminderSummary.InstancePtr);
                fecherFoundation.Sys.ClearInstance(frmReportViewer.InstancePtr);
                frmReportViewer.InstancePtr.Init( rptReminderSummary.InstancePtr);
			}
        }

		private void ActiveReport_ToolbarClick(/*DDActiveReports2.DDTool Tool*/)
		{			
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
            {
                this.Detail.Visible = true;
				// On Error GoTo ERROR_HANDLER
				double dblInt = 0;
				int lngAcct = 0;
				int lngLienRec;
				int lngPer;
				int lngYrCntr;
				bool boolFoundYr = false;
				int lngHighYr = 0;
				int lngLowYr = 0;
				double dblTmpBal = 0;
				TryAgain:
				;
				if (!rsData.EndOfFile())
				{
					if (boolRE)
					{
                        strQuerySQL = "SELECT Account,Name1,Name2,BillingMaster.Address1,BillingMaster.Address2,BillingMaster.Address3,LienRecordNumber,MapLot,StreetNumber,StreetName,TranCode,TransferFromBillingDateFirst,TransferFromBillingDateLast " + "FROM  BillingMaster " + "WHERE BillingType = 'RE' AND Account = " + rsData.Get_Fields("Account") + " ORDER BY BillingYear desc";
                        rsTemp.OpenRecordset(strQuerySQL, modExtraModules.strCLDatabase);
					}
					else
					{
						strQuerySQL = "SELECT BillingMaster.Account,Name1,Name2,BillingMaster.Address1,BillingMaster.Address2,BillingMaster.Address3,BillingMaster.StreetNumber,StreetName " + "FROM  BillingMaster " + "WHERE BillingType = 'PP' AND BillingMaster.Account = " + FCConvert.ToString(rsData.Get_Fields("Account")) + "ORDER BY BillingYear desc";

						rsTemp.OpenRecordset(strQuerySQL, modExtraModules.strCLDatabase);
					}
					if (!rsTemp.EndOfFile())
					{
                        //FC:FINAL:DDU:#i335 Changed from SelectedIndex to SelectedItem to find correct value in ComboBox
                        if (reminderNoticeOptions.Period == ReminderNoticeOptions.PeriodEnum.PastDueOnly)
						{
                            // if this is for past due amounts only then
                            if (boolRE)
							{
								if (boolLienedRecords)
								{
									lngLienRec = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("LienRecordNumber"));
								}
							}
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							lngAcct = FCConvert.ToInt32(rsTemp.Get_Fields("Account"));
							if (boolRE)
							{
								if (boolLienedRecords)
								{
									// kk01272017 trocls-80  Add ID to the following sql for CalculateAccountCL()
									rsCheckAmt.OpenRecordset("SELECT ID,RateKey,InterestAppliedThroughDate,TaxDue1,TaxDue2,TaxDue3,TaxDue4,PrincipalPaid," + "BillingYear,InterestCharged,InterestPaid,DemandFees,DemandFeesPaid " + "FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAcct) + " AND BillingType = 'RE' AND RateKey <> 0 AND LienRecordNumber <> 0 ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
								}
								else
								{
									rsCheckAmt.OpenRecordset("SELECT ID,RateKey,InterestAppliedThroughDate,TaxDue1,TaxDue2,TaxDue3,TaxDue4,PrincipalPaid," + "BillingYear,InterestCharged,InterestPaid,DemandFees,DemandFeesPaid " + "FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAcct) + " AND BillingType = 'RE' AND RateKey <> 0 AND LienRecordNumber = 0 ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
								}
							}
							else
							{
								rsCheckAmt.OpenRecordset("SELECT ID,RateKey,InterestAppliedThroughDate,TaxDue1,TaxDue2,TaxDue3,TaxDue4,PrincipalPaid," + "BillingYear,InterestCharged,InterestPaid,DemandFees,DemandFeesPaid " + "FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAcct) + " AND BillingType = 'PP' AND RateKey <> 0 AND LienRecordNumber = 0 ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
							}
							if (!rsCheckAmt.EndOfFile())
							{
								// check to see if the total amount due of the account minus the last billed bill is less then or equal to 0
								if (modCLCalculations.CalculateAccountTotal(lngAcct, boolRE) - modCLCalculations.CalculateAccountCL(ref rsCheckAmt, lngAcct, DateTime.Today, ref dblInt) <= 0)
								{
									// if so, then get the next account
									boolRetry = true;
									rsData.MoveNext();
									goto TryAgain;
								}
								else if (strLastMaturedYear != "")
								{
									boolFoundYr = false;
									if (boolRE)
									{
										lngLowYr = FCConvert.ToInt32(FCConvert.ToDouble(strLastMaturedYear)) + 1;
									}
									else
									{
										lngLowYr = FCConvert.ToInt32(FCConvert.ToDouble(strLastMaturedYear));
									}
									lngHighYr = FCConvert.ToInt32(FCConvert.ToDouble(strHighYear));
									for (lngYrCntr = lngLowYr; lngYrCntr <= lngHighYr; lngYrCntr++)
									{
										dblTmpBal = modCLCalculations.CalculateAccountTotal(lngAcct, boolRE, false, default(DateTime), 0, lngYrCntr);
										if ((dblMinimumAmount != 0 && dblTmpBal >= dblMinimumAmount) || (dblMinimumAmount == 0 && dblTmpBal > 0))
										{
											boolFoundYr = true;
											break;
										}
									}
									if (!boolFoundYr)
									{
										boolRetry = true;
										rsData.MoveNext();
										goto TryAgain;
									}
								}
							}
						}
						else
						{
                            if (reminderNoticeOptions.Period == ReminderNoticeOptions.PeriodEnum.Outstanding)
							{
                                lngPer = 4;
							}
                            else if (reminderNoticeOptions.Period == ReminderNoticeOptions.PeriodEnum.Period1)
							{
                                lngPer = 1;
							}
                            else if (reminderNoticeOptions.Period == ReminderNoticeOptions.PeriodEnum.Period2)
							{
                                lngPer = 2;
							}
                            else if (reminderNoticeOptions.Period == ReminderNoticeOptions.PeriodEnum.Period3)
							{
                                lngPer = 3;
							}
							else
							{
								lngPer = 4;
							}
							if (boolRE)
							{
								if (boolLienedRecords)
								{
									lngLienRec = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("LienRecordNumber"));
								}
							}
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							lngAcct = FCConvert.ToInt32(rsTemp.Get_Fields("Account"));
							if (boolRE)
							{
								if (boolLienedRecords)
								{
									rsCheckAmt.OpenRecordset("SELECT ID FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAcct) + " AND BillingType = 'RE' AND RateKey <> 0 AND LienRecordNumber <> 0 ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
								}
								else
								{
									rsCheckAmt.OpenRecordset("SELECT ID FROM BillingMaster WHERE RateKey IN " + reminderNoticeOptions.RateKeyList + " AND Account = " + FCConvert.ToString(lngAcct) + " AND BillingType = 'RE' AND RateKey <> 0 AND LienRecordNumber = 0 ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
								}
							}
							else
							{
								rsCheckAmt.OpenRecordset("SELECT ID FROM BillingMaster WHERE RateKey IN " + reminderNoticeOptions.RateKeyList + " AND Account = " + FCConvert.ToString(lngAcct) + " AND BillingType = 'PP' AND RateKey <> 0 AND LienRecordNumber = 0 ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
							}
							if (!rsCheckAmt.EndOfFile())
							{
								if (modCLCalculations.CalculateAccountTotal(lngAcct, boolRE) <= 0)
								{
									// if so, then get the next account
									boolRetry = true;
									rsData.MoveNext();
									goto TryAgain;
									// kgk 04112012 trocl-899  Not sure what was intended here.  This is only checking the selected highest year
									// kgk 06272012 trocl-693  Put back in with check for Prev Years so we're not showing blanks
									// MAL@20080115: Check for minimum amount
								}
								else if (!boolPrevYears && modCLCalculations.CalculateAccountTotal(lngAcct, boolRE, false, default(DateTime), 0, FCConvert.ToInt32(FCConvert.ToDouble(strHighYear))) < dblMinimumAmount)
								{
									boolRetry = true;
									rsData.MoveNext();
									goto TryAgain;
									// kgk 04-10-2012 trocl-899  Have to check each year
									// kgk 12-14-2011 trocl-693  Check for Last Year Mature balance
									// ElseIf CalculateAccountTotal(lngAcct, boolRE, , , , CLng(strHighYear)) - CalculateAccountTotal(lngAcct, boolRE, , , , CLng(strLastMaturedYear)) = 0 Then  ' Zero or minimum
								}
								else if (strLastMaturedYear != "")
								{
									boolFoundYr = false;
									if (boolRE)
									{
										lngLowYr = FCConvert.ToInt32(FCConvert.ToDouble(strLastMaturedYear)) + 1;
									}
									else
									{
										lngLowYr = FCConvert.ToInt32(FCConvert.ToDouble(strLastMaturedYear));
									}
									lngHighYr = FCConvert.ToInt32(FCConvert.ToDouble(strHighYear));
									for (lngYrCntr = lngLowYr; lngYrCntr <= lngHighYr; lngYrCntr++)
									{
										dblTmpBal = modCLCalculations.CalculateAccountTotal(lngAcct, boolRE, false, default(DateTime), 0, lngYrCntr);
										if ((dblMinimumAmount != 0 && dblTmpBal >= dblMinimumAmount) || (dblMinimumAmount == 0 && dblTmpBal > 0))
										{
											boolFoundYr = true;
											break;
										}
									}
									if (!boolFoundYr)
									{
										boolRetry = true;
										rsData.MoveNext();
										goto TryAgain;
									}
								}
							}
						}
						if (!rsData.EndOfFile())
						{
                            srptReminderFormDetail.Report = new srptReminderForm(reminderNoticeOptions);
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            srptReminderFormDetail.Report.UserData = rsTemp.Get_Fields("Account");
                            srptReminderFormDetail.Report.AddNamedItem("reminderNoticeOptions", reminderNoticeOptions);
                        }
						// If boolNoValues Then
						// boolRetry = True
						// rsData.MoveNext
						// GoTo TRYAGAIN
						// Else
						boolRetry = false;
						// End If
						boolNoValues = false;
						PrintPostCards();
						rsData.MoveNext();
					}
					else
					{
						boolRetry = true;
						rsData.MoveNext();
						goto TryAgain;
					}
				}

                if (boolRetry)
                {
                    this.Detail.Visible = false;
                }
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In Detail Format");
			}
		}

		private void PrintPostCards()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill in the fields for post card
				int intCTRLIndex/*unused?*/;
				string str1 = "";
				string str2 = "";
				string str3 = "";
				string str4 = "";
				string str5 = "";
				double dblDue = 0;
				double dblPaid = 0;
				double dblPeriodDue/*unused?*/;
				int lngAcct = 0;
				string strYear = "";
                bool boolNewOwner = false; // trocls-119 10.6.17 kjr 4.23.18 CODE FREEZE
                bool boolNewAddress = false;
                DateTime dtBillDate;
                bool boolSameName = false;
                bool boolREMatch = false;
                string strPrevSecOwnerName = "";
                string strPrevAddress1 = "";
                string strPrevAddress2 = "";
                string strPrevAddress3 = "";
                string strMuni = "";
                string strOwnerName = "";
				clsDRWrapper rsMulti;
				cPartyController pCont = new cPartyController();
				// VBto upgrade warning: pInfo As CParty	OnWrite(cParty)
				cParty pInfo = new cParty();
				cPartyAddress pAdd;
				clsDRWrapper rsMastOwner = new clsDRWrapper();
                short intDummyVar;
                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                lngAcct = FCConvert.ToInt32(rsTemp.Get_Fields("Account"));
				if (Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Name2"))) != "")
				{
					str1 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Name1"))) + " and " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Name2")));
				}
				else
				{
					str1 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Name1")));
				}
				modRhymalReporting.Statics.frfFreeReport[1].DatabaseFieldName = str1;

				clsAddress tAddr;

				switch (reminderNoticeOptions.AddressType)
				{
					case ReminderNoticeOptions.AddressEnum.AddressAtBilling:
						tAddr = tAddrCtrlr.GetOwnerAndAddressAtBilling(rsTemp);
						break;
					case ReminderNoticeOptions.AddressEnum.CareOfCurrentOwner:
						tAddr = tAddrCtrlr.GetOwnerAtBillingCareOfCurrentOwner(rsTemp);
						break;
					case ReminderNoticeOptions.AddressEnum.LastKnownAddress:
						tAddr = tAddrCtrlr.GetBilledOwnerAtLastAddress(rsTemp);
						break;
					default:
						tAddr = new clsAddress();
						break;
				}

				str2 = tAddr.Get_Address(1).Trim();
				str3 = tAddr.Get_Address(2).Trim();
				str4 = tAddr.Get_Address(3).Trim();
				str5 = tAddr.Get_Address(4).Trim();


				//////// get the mailing address from BillingMaster
				//////str2 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address1")));
				//////str3 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address2")));
				//////str4 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address3")));
    //////            // 4.23.18 CODE FREEZE
    //////            if (reminderNoticeOptions.AddressType != ReminderNoticeOptions.AddressEnum.AddressAtBilling)
    //////            { // 5.21.18
    //////                if (modStatusPayments.Statics.boolRE)
    //////                { // trocls-119 10.6.17 kjr - For RE and Billed Owner @ last known addr, 5.21.18
    //////                    if (Information.IsDate(rsTemp.Get_Fields("TransferFromBillingDateFirst")))
    //////                    {
    //////                        dtBillDate = (DateTime)rsTemp.Get_Fields("TransferFromBillingDateFirst");
    //////                    }
    //////                    else
    //////                    {
    //////                        dtBillDate = (DateTime)rsTemp.Get_Fields("TransferFromBillingDateLast");
    //////                    }
    //////                    boolNewOwner = modCLCalculations.NewOwner(rsTemp.Get_Fields("Name1"), rsTemp.Get_Fields("Account"), dtBillDate,  ref boolREMatch,  ref boolSameName);

    //////                    if (reminderNoticeOptions.AddressType == ReminderNoticeOptions.AddressEnum.LastKnownAddress)
    //////                    {
    //////                        var strCity = "";
    //////                        var strState = "";
    //////                        var strZip = "";
    //////                        var strZip4 = "";
    //////                        boolNewAddress = modCLCalculations.OwnerLastRecordedAddress(rsTemp.Get_Fields("Name1"), rsTemp.Get_Fields("Account"),  dtBillDate, ref strPrevSecOwnerName, ref strPrevAddress1, ref strPrevAddress2, ref strPrevAddress3); // 4.20.18

    //////                        if (boolNewOwner && boolNewAddress)
    //////                        {
    //////                            str2 = fecherFoundation.Strings.Trim(strPrevAddress1);
    //////                            str3 = fecherFoundation.Strings.Trim(strPrevAddress2);
    //////                            str4 = fecherFoundation.Strings.Trim(strPrevAddress3);
    //////                        }
    //////                    }
    //////                }

    //////                if ((fecherFoundation.Strings.Trim(str2) == "" && fecherFoundation.Strings.Trim(str3) == "") || reminderNoticeOptions.AddressType == ReminderNoticeOptions.AddressEnum.CareOfCurrentOwner || (boolSameName && boolREMatch))
    //////                {

    //////                    // this will fill the address from the RE or PP DB for the mail address

    //////                    // condense the labels if some are blank            'We used to get the owner info in inner join that was slow.  I moved that down to here to speed it up.  We now use party classes to get infor to use in labels
    //////                    if (boolRE)
    //////                    {
    //////                        rsMastOwner.OpenRecordset("Select OwnerPartyID as OwnerID,DeedName1 FROM Master WHERE RSAccount = " + rsTemp.Get_Fields("Account"), modExtraModules.strREDatabase);
    //////                    }
    //////                    else
    //////                    {
    //////                        rsMastOwner.OpenRecordset("Select PartyID as OwnerID FROM PPMaster WHERE Account = " + rsTemp.Get_Fields("Account"), modExtraModules.strPPDatabase);
    //////                    }

    //////                    strOwnerName = "";
    //////                    // If we find the master record in PP or RE then we try to get party info
    //////                    if (rsMastOwner.EndOfFile() != true && rsMastOwner.BeginningOfFile() != true)
    //////                    {
    //////                        pInfo = pCont.GetParty(rsMastOwner.Get_Fields("OwnerID")); // kk07152015 trocls-58 .Fields("OwnerPartyID")
    //////                        strOwnerName = pInfo.FullNameLastFirst;
    //////                        if (boolRE)
    //////                        {
    //////                            strOwnerName = rsMastOwner.Get_Fields_String("DeedName1");
    //////                        }
    //////                    }

    //////                    // If we find part info then we attempt to get address info
    //////                    if (!(pInfo == null))
    //////                    {
    //////                        pAdd = pInfo.GetAddress("CL", rsMastOwner.Get_Fields("OwnerID")); // kk07152015 trocls-58 .Fields("OwnerPartyID")

    //////                        if (rsTemp.Get_Fields("Name1") != strOwnerName && reminderNoticeOptions.AddressType == ReminderNoticeOptions.AddressEnum.CareOfCurrentOwner)
    //////                        {
    //////                            str2 = "C\\O " + strOwnerName;
    //////                        }
    //////                        else
    //////                        {
	   //////                         str2 = "";
    //////                        }

				//////			// If we found an address we fill it in otherwise leave it blank
				//////			if (!(pAdd == null))
    //////                        {
    //////                            str3 = fecherFoundation.Strings.Trim(pAdd.Address1);
    //////                            str4 = fecherFoundation.Strings.Trim(pAdd.Address2);
    //////                            str5 = fecherFoundation.Strings.Trim(pAdd.City) + " " + fecherFoundation.Strings.Trim(pAdd.State) + " " + fecherFoundation.Strings.Trim(pAdd.Zip);
    //////                        }
    //////                    }
    //////                }
    //////            }


                if (fecherFoundation.Strings.Trim(str4) == string.Empty)
                {
                    str4 = str5;
                    str5 = "";
                }

                if (fecherFoundation.Strings.Trim(str3) == string.Empty)
                {
                    str3 = str4;
                    str4 = str5;
                    str5 = "";
                }

                if (fecherFoundation.Strings.Trim(str2) == string.Empty)
                {
                    str2 = str3;
                    str3 = str4;
                    str4 = str5;
                    str5 = "";
                }

                if (fecherFoundation.Strings.Trim(str1) == string.Empty)
                {
                    str1 = str2;
                    str2 = str3;
                    str3 = str4;
                    str4 = str5;
                    str5 = "";
                }
                // Account Number
                if (boolRE)
				{
					// If .Fields("Account") = 2972 Then Stop
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					lblAccount.Text = "Account: " + FCConvert.ToString(rsTemp.Get_Fields("Account"));
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					modRhymalReporting.Statics.frfFreeReport[0].DatabaseFieldName = FCConvert.ToString(rsTemp.Get_Fields("Account"));
					lblMapLot.Text = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("MapLot")));
					// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
					if (Conversion.Val(rsTemp.Get_Fields("StreetNumber")) != 0)
					{
						// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
						lblLocation.Text = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("StreetNumber")) + " " + FCConvert.ToString(rsTemp.Get_Fields_String("StreetName")));
					}
					else
					{
						lblLocation.Text = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("StreetName")));
					}
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					lblAccount.Text = "Account: " + FCConvert.ToString(rsTemp.Get_Fields("Account"));
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					modRhymalReporting.Statics.frfFreeReport[0].DatabaseFieldName = FCConvert.ToString(rsTemp.Get_Fields("Account"));
				}
				// Name
				lblName.Text = str1;
				// Mailing Address
				lblMailingAddress1.Text = str2;
				lblMailingAddress2.Text = str3;
				lblMailingAddress3.Text = str4;
				lblMailingAddress4.Text = str5;
				Array.Resize(ref modReminderNoticeSummary.Statics.arrReminderSummaryList, lngIndex + 1);
				//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
				modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex] = new modReminderNoticeSummary.ReminderSummaryList(0);
				// fill the struct
				modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Account = lngAcct;
				modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Name1 = str1;
				modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr1 = str2;
				modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr2 = str3;
				modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr3 = str4;
				modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr4 = str5;
				modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Year = 0;
				// Val(strYear)
				modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Total = dblDue - dblPaid;
				lngIndex += 1;

				lblHeader.Text = "************  " + strReportTitle + "  ************";
				// Past Due Tax Reminder Statement
				// If boolLienedRecords Then
				if (modGlobal.Statics.gboolMultipleTowns)
				{
					rsMulti = new clsDRWrapper();
					if (boolRE)
					{
						// TODO Get_Fields: Check the table for the column [TranCode] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [TranCode] and replace with corresponding Get_Field method
						rsMulti.OpenRecordset("SELECT * FROM tblRegions WHERE TownNumber = " + FCConvert.ToString(rsTemp.Get_Fields("TranCode")), "CentralData");
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [TranCode] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [TranCode] and replace with corresponding Get_Field method
						rsMulti.OpenRecordset("SELECT * FROM tblRegions WHERE TownNumber = " + FCConvert.ToString(rsTemp.Get_Fields("TranCode")), "CentralData");
					}
					if (!rsMulti.EndOfFile())
					{
						if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
						{
							strMuni = modGlobalConstants.Statics.gstrCityTown + " of " + FCConvert.ToString(rsMulti.Get_Fields_String("TownName"));
						}
						else
						{
							strMuni = FCConvert.ToString(rsMulti.Get_Fields_String("TownName"));
						}
					}
					else
					{
						if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
						{
							strMuni = modGlobalConstants.Statics.gstrCityTown + " of " + modGlobalConstants.Statics.MuniName;
						}
						else
						{
							strMuni = modGlobalConstants.Statics.MuniName;
						}
					}
				}
				else
				{
					if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
					{
						strMuni = modGlobalConstants.Statics.gstrCityTown + " of " + modGlobalConstants.Statics.MuniName;
					}
					else
					{
						strMuni = modGlobalConstants.Statics.MuniName;
					}
				}
				lblPreMessage.Text = "The " + strMuni + "'s records indicate that you have " + strMod + " taxes due for the following year(s):";
				// outstanding
				fldMessage.Text = GetReportString(ref strDataSting);
				rsTemp.MoveNext();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Printing");
			}
		}

		private string GetReportString(ref string strDataString)
		{
			string GetReportString = "";
			// this routine will fill in all of the variables and create the string to be shown on the form
			CalculateVariableTotals();
			GetReportString = SetupVariablesInString(ref strDataString);
			return GetReportString;
		}

		private void CalculateVariableTotals()
		{
			// this routine will gather all of the correct data for this account and fill in the struct
			// that holds all of the data, this will allow another function to create the string
			// ACCOUNT    - 0
			// NAME       - 1
			// Mail DATE- 4
			modRhymalReporting.Statics.frfFreeReport[2].DatabaseFieldName = Strings.Format(dtMailDate, "MM/dd/yyyy");
			// INTDEPT    - 5
			modRhymalReporting.Statics.frfFreeReport[3].DatabaseFieldName = strIntDept;
			// INTDEPTPHONE-6
			modRhymalReporting.Statics.frfFreeReport[4].DatabaseFieldName = strIntDeptPhone;
			// MATDEPT    - 7
			modRhymalReporting.Statics.frfFreeReport[5].DatabaseFieldName = strMatDept;
			// MATDEPTPHONE-8
			modRhymalReporting.Statics.frfFreeReport[6].DatabaseFieldName = strMatDeptPhone;
			// HIGHYEAR   - 9
			modRhymalReporting.Statics.frfFreeReport[7].DatabaseFieldName = strHighYear;
			// LASTYEARMAT- 10
			modRhymalReporting.Statics.frfFreeReport[8].DatabaseFieldName = strLastMaturedYear;
			// Interest Date
			modRhymalReporting.Statics.frfFreeReport[9].DatabaseFieldName = Strings.Format(dtDate, "MM/dd/yyyy");
		}

		private string SetupVariablesInString(ref string strOriginal)
		{
			string SetupVariablesInString = "";
			// this will replace all of the variables with the information needed
			string strBuildString;
			// this is the string that will be built and returned at the end
			string strTemp = "";
			// this is a temporary sting that will be used to store and transfer string segments
			int lngNextVariable;
			// this is the position of the beginning of the next variable (0 = no more variables)
			int lngEndOfLastVariable;
			// this is the position of the end of the last variable
			lngEndOfLastVariable = 0;
			lngNextVariable = 1;
			strBuildString = "";
			if (!boolRetry)
			{
				// priming read
				if (Strings.InStr(1, strOriginal, "<") > 0)
				{
					lngNextVariable = Strings.InStr(lngEndOfLastVariable + 1, strOriginal, "<");
					// do until there are no more variables left
					do
					{
						// add the string from lngEndOfLastVariable - lngNextVariable to the BuildString
						strBuildString += Strings.Mid(strOriginal, lngEndOfLastVariable + 1, lngNextVariable - lngEndOfLastVariable - 1);
						// set the end pointer
						lngEndOfLastVariable = Strings.InStr(lngNextVariable, strOriginal, ">");
						// replace the variable
						strBuildString += GetVariableValue_2(Strings.Mid(strOriginal, lngNextVariable + 1, lngEndOfLastVariable - lngNextVariable - 1));
						// check for another variable
						lngNextVariable = Strings.InStr(lngEndOfLastVariable, strOriginal, "<");
					}
					while (!(lngNextVariable == 0));
				}
				// take the last of the string and add it to the end
				strBuildString += Strings.Mid(strOriginal, lngEndOfLastVariable + 1);
				// check for variables
				if (Strings.InStr(1, "<", strOriginal) > 0)
				{
					// strBuildString = SetupVariablesInString(strBuildString)     'setup recursion
					FCMessageBox.Show("ERROR: There are still variables in the report string.", MsgBoxStyle.Critical, "SetupVariablesInString Error");
				}
				else
				{
					SetupVariablesInString = strBuildString;
				}
			}
			return SetupVariablesInString;
		}

		private string GetVariableValue_2(string strVarName)
		{
			return GetVariableValue(ref strVarName);
		}

		private string GetVariableValue(ref string strVarName)
		{
			string GetVariableValue = "";
			// this function will take a variable name and find it in the array, then get the value and return it as a string
			int intCT;
			string strValue = "";
			// this is a dummy code to show the user where the hard codes are
			if (strVarName == "CRLF")
			{
				GetVariableValue = "";
				return GetVariableValue;
			}
			//FC:FINAL:KS: #without nr.:fix Exit For inside switch/case statement
			bool endFor = false;
			for (intCT = 0; intCT <= modRhymalReporting.MAXFREEVARIABLES; intCT++)
			{
				if (Strings.UCase(strVarName) == Strings.UCase(modRhymalReporting.Statics.frfFreeReport[intCT].Tag))
				{
					switch (modRhymalReporting.Statics.frfFreeReport[intCT].VariableType)
					{
						case 0:
							{
								// value from the static variables in the grid
								switch (modRhymalReporting.Statics.frfFreeReport[intCT].Type)
								{
								// 0 = Text, 1 = Number, 2 = Date, 3 = Do not ask for default (comes from DB), 4 = Formatted Year
									case 0:
										{
											strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
											break;
										}
									case 1:
										{
											strValue = Strings.Format(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1), "#,##0.00");
											break;
										}
									case 2:
										{
											strValue = Strings.Format(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1), "MMMM dd, yyyy");
											break;
										}
									case 3:
										{
											strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
											break;
										}
									case 4:
										{
											strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
											break;
										}
								}
								//end switch
								//break;
								//FC:FINAL:KS: #without nr.:fix Exit For inside switch/case statement
								endFor = true;
								break;
							}
						case 1:
							{
								// value from the dynamic variables in the database
								strValue = FCConvert.ToString(rsData.Get_Fields(modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName));
								if (Strings.Trim(strValue) == "")
								{
									if (Strings.UCase(modRhymalReporting.Statics.frfFreeReport[intCT].Tag) != "LESSPAYMENTS")
									{
										// maybe put a blank line there
										strValue = "__________";
									}
								}
								//break;
								//FC:FINAL:KS: #without nr.:fix Exit For inside switch/case statement
								endFor = true;
								break;
							}
						case 2:
							{
								// questions at the bottom of the grid
								strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
								//break;
								//FC:FINAL:KS: #without nr.:fix Exit For inside switch/case statement
								endFor = true;
								break;
							}
						case 3:
							{
								// calculated values that are stored in the DatabaseFieldName field
								strValue = modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName;
								if (Strings.Trim(strValue) == "")
								{
									if (Strings.UCase(modRhymalReporting.Statics.frfFreeReport[intCT].Tag) != "LESSPAYMENTS")
									{
										// maybe put a blank line there
										strValue = "__________";
									}
								}
								endFor = true;
								break;
							}
					}
					if (endFor)
					{
						break;
					}
				}
			}
			GetVariableValue = strValue;
			return GetVariableValue;
		}

		
	}
}
