﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class srptLoadbackReport : FCSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               07/20/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/15/2005              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		int lngNumOfAccts;
		bool boolLien;
		double[] dblTotals = new double[5 + 1];
		int lngYear;

		public srptLoadbackReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Account Detail";
		}

		public static srptLoadbackReport InstancePtr
		{
			get
			{
				return (srptLoadbackReport)Sys.GetInstance(typeof(srptLoadbackReport));
			}
		}

		protected srptLoadbackReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			int lngYear = 0;
			int lngAccount = 0;
			eArgs.EOF = rsData.EndOfFile();
			if (!rsData.EndOfFile())
			{
				Fields["BillingYear"].Value = rsData.Get_Fields_Int32("BillingYear");
				lngYear = FCConvert.ToInt32(rsData.Get_Fields_Int32("BillingYear"));
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				lngAccount = FCConvert.ToInt32(rsData.Get_Fields("Account"));
				DUPLICATEBILL:
				;
				// check the next record so see if it is the same
				rsData.MoveNext();
				if (!rsData.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (((rsData.Get_Fields("Account"))) == lngAccount && ((rsData.Get_Fields_Int32("BillingYear"))) == lngYear)
					{
						// if the next account is the same then skip it by staying on this account and starting over
						goto DUPLICATEBILL;
					}
				}
				// move back to the record at hand
				rsData.MovePrevious();
			}
			//Detail_Format();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			// catch the esc key and unload the report
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
            using (clsDRWrapper rsLB = new clsDRWrapper())
            {
                string strSortOrder = "";
                rsLB.OpenRecordset("SELECT * FROM Collections");
                switch (rsLB.Get_Fields_Int32("LBOrder"))
                {
                    case 0:
                    {
                        strSortOrder = " BillingYear, Account, ID ";
                        break;
                    }
                    case 1:
                    {
                        strSortOrder = " Account, BillingYear, ID ";
                        break;
                    }
                    case 2:
                    {
                        strSortOrder = " Name1, BillingYear, ID ";
                        break;
                    }
                    case 3:
                    {
                        strSortOrder = " BillingYear, Name1, ID ";
                        break;
                    }
                }

                //end switch
                //modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
                boolLien = FCConvert.CBool(Strings.Trim(FCConvert.ToString(this.UserData)) == "L");
                lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
                lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
                lblMuniName.Text = modGlobalConstants.Statics.MuniName;
                if (!boolLien)
                {
                    if (modStatusPayments.Statics.boolRE)
                    {
                        rsData.OpenRecordset(
                            "SELECT * FROM LoadBackOriginal WHERE LienRecordNumber = 0 AND BillingType = 'RE' ORDER BY " +
                            strSortOrder, modExtraModules.strCLDatabase);
                    }
                    else
                    {
                        rsData.OpenRecordset(
                            "SELECT * FROM LoadBackOriginal WHERE LienRecordNumber = 0 AND BillingType = 'PP' ORDER BY " +
                            strSortOrder, modExtraModules.strCLDatabase);
                    }

                    lblReportType.Text = "Non Lien Loadbacks";
                }
                else
                {
                    // trocl-658 kgk 03-25-11  Check the bill type here too - are there PP liens?
                    // rsData.OpenRecordset "SELECT * FROM LoadBackOriginal WHERE LienRecordNumber <> 0 ORDER BY " & strSortOrder, strCLDatabase
                    if (modStatusPayments.Statics.boolRE)
                    {
                        rsData.OpenRecordset(
                            "SELECT * FROM LoadBackOriginal WHERE LienRecordNumber <> 0 AND BillingType = 'RE' ORDER BY " +
                            strSortOrder, modExtraModules.strCLDatabase);
                    }
                    else
                    {
                        rsData.OpenRecordset(
                            "SELECT * FROM LoadBackOriginal WHERE LienRecordNumber <> 0 AND BillingType = 'PP' ORDER BY " +
                            strSortOrder, modExtraModules.strCLDatabase);
                    }

                    lblReportType.Text = "Lien Loadbacks";
                    lblTax1.Text = "Principal";
                    lblTax2.Text = "Interest";
                    lblTax3.Text = "Costs";
                    lblTax4.Text = "";
                }

                if (rsData.EndOfFile())
                {
                    // MsgBox "There are no accounts that have been loaded back.", vbOKOnly, "No Accounts Found"
                    Cancel();
                    return;
                }
            }
        }

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (!rsData.EndOfFile())
				{
					BindFields();
					lngNumOfAccts += 1;
					rsData.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Detail Format Error");
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			try
			{
				
				SetupGroupFooter();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Group Format Error");
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
			int lngAccount = 0;
            using (clsDRWrapper rsLien = new clsDRWrapper())
            {
                DUPLICATEBILL: ;
                if (!rsData.EndOfFile())
                {
                    // us the correct new information
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    lngAccount = FCConvert.ToInt32(rsData.Get_Fields("Account"));
                    lngYear = FCConvert.ToInt32(rsData.Get_Fields_Int32("BillingYear"));
                    fldAcct.Text = lngAccount.ToString();
                    fldName.Text = rsData.Get_Fields_String("Name1");
                    if (FCConvert.ToString(rsData.Get_Fields_String("Name2")) != "")
                    {
                        lblName.Text = lblName.Text + " & " + FCConvert.ToString(rsData.Get_Fields_String("Name2"));
                    }

                    fldYear.Text = modExtraModules.FormatYear(lngYear.ToString());
                    if (!boolLien)
                    {
                        fldTax1.Text = Strings.Format(rsData.Get_Fields_Decimal("TaxDue1"), "#,##0.00");
                        fldTax2.Text = Strings.Format(rsData.Get_Fields_Decimal("TaxDue2"), "#,##0.00");
                        fldTax3.Text = Strings.Format(rsData.Get_Fields_Decimal("TaxDue3"), "#,##0.00");
                        fldTax4.Text = Strings.Format(rsData.Get_Fields_Decimal("TaxDue4"), "#,##0.00");
                        fldCHGINT.Text = Strings.Format(rsData.Get_Fields_Decimal("InterestCharged") * -1, "#,##0.00");
                        fldTotal.Text = Strings.Format(
                            Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1")) +
                            Conversion.Val(rsData.Get_Fields_Decimal("TaxDue2")) +
                            Conversion.Val(rsData.Get_Fields_Decimal("TaxDue3")) +
                            Conversion.Val(rsData.Get_Fields_Decimal("TaxDue4")) -
                            Conversion.Val(rsData.Get_Fields_Decimal("InterestCharged")), "#,##0.00");
                        dblTotals[1] += FCConvert.ToDouble(fldTax1.Text);
                        dblTotals[2] += FCConvert.ToDouble(fldTax2.Text);
                        dblTotals[3] += FCConvert.ToDouble(fldTax3.Text);
                        dblTotals[4] += FCConvert.ToDouble(fldTax4.Text);
                        dblTotals[0] += FCConvert.ToDouble(fldCHGINT.Text);
                    }
                    else
                    {
                        rsLien.OpenRecordset(
                            "SELECT * FROM LienRec WHERE id = " +
                            FCConvert.ToString(rsData.Get_Fields_Int32("LienRecordNumber")),
                            modExtraModules.strCLDatabase);
                        if (!rsLien.EndOfFile())
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            fldTax1.Text = Strings.Format(rsLien.Get_Fields("Principal"), "#,##0.00");
                            // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                            fldTax2.Text = Strings.Format(rsLien.Get_Fields("Interest"), "#,##0.00");
                            // TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
                            fldTax3.Text = Strings.Format(rsLien.Get_Fields("Costs"), "#,##0.00");
                            fldTax4.Text = "";
                            fldCHGINT.Text = Strings.Format(rsLien.Get_Fields_Decimal("InterestCharged") * -1,
                                "#,##0.00");
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
                            fldTotal.Text = Strings.Format(
                                Conversion.Val(rsLien.Get_Fields("Principal")) +
                                Conversion.Val(rsLien.Get_Fields("Interest")) +
                                Conversion.Val(rsLien.Get_Fields("Costs")) -
                                Conversion.Val(rsLien.Get_Fields_Decimal("InterestCharged")), "#,##0.00");
                        }
                        else
                        {
                            fldTax1.Text = "0.00";
                            fldTax2.Text = "0.00";
                            fldTax3.Text = "0.00";
                            fldTax4.Text = "";
                        }

                        dblTotals[0] += FCConvert.ToDouble(fldCHGINT.Text);
                        dblTotals[1] += FCConvert.ToDouble(fldTax1.Text);
                        dblTotals[2] += FCConvert.ToDouble(fldTax2.Text);
                        dblTotals[3] += FCConvert.ToDouble(fldTax3.Text);
                    }
                }
            }
            return;

            ERROR_HANDLER:
			;
			FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err().Number) + " - " + Information.Err().Description + ".", MsgBoxStyle.Critical, "Error Binding Fields");
		}

		private void SetupGroupFooter()
		{
			if (lngNumOfAccts == 1)
			{
				fldFooterTotal.Text = "Total for 1 account.";
			}
			else
			{
				fldFooterTotal.Text = "Total for " + FCConvert.ToString(lngNumOfAccts) + " accounts.";
			}
			// show totals
			fldFooterTax1.Text = Strings.Format(dblTotals[1], "#,##0.00");
			fldFooterTax2.Text = Strings.Format(dblTotals[2], "#,##0.00");
			fldFooterTax3.Text = Strings.Format(dblTotals[3], "#,##0.00");
			if (!boolLien)
			{
				fldFooterTax4.Text = Strings.Format(dblTotals[4], "#,##0.00");
				fldFooterTax4.Visible = true;
			}
			else
			{
				fldFooterTax4.Text = "";
			}
			fldFooterCHGINT.Text = Strings.Format(dblTotals[0], "#,##0.00");
			fldFooterTaxTotal.Text = Strings.Format(dblTotals[0] + dblTotals[1] + dblTotals[2] + dblTotals[3] + dblTotals[4], "#,##0.00");
			FCUtils.EraseSafe(dblTotals);
			lngNumOfAccts = 0;
		}

		

		private void srptLoadbackReport_DataInitialize(object sender, EventArgs e)
		{
			Fields.Add("BillingYear");
		}
	}
}
