﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptOutprintingFormat : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               03/17/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/18/2004              *
		// ********************************************************
		bool boolDone;
		int lngCount;

		public rptOutprintingFormat()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Outprinting Format";
		}

		public static rptOutprintingFormat InstancePtr
		{
			get
			{
				return (rptOutprintingFormat)Sys.GetInstance(typeof(rptOutprintingFormat));
			}
		}

		protected rptOutprintingFormat _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = boolDone;
			//Detail_Format();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "t");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			lngCount = 1;
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the information into the fields
				boolDone = false;
				lblFieldName.Text = "";
				lblSize.Text = "";
				fldDescription.Text = "";
				switch (lngCount)
				{
					case 1:
						{
							lblFieldName.Text = "Account";
							lblSize.Text = "6";
							fldDescription.Text = "Account Number";
							break;
						}
					case 2:
						{
							lblFieldName.Text = "RE Name";
							lblSize.Text = "38";
							fldDescription.Text = "The name of the current owner if it it not the same as the billed owner.";
							break;
						}
					case 3:
						{
							lblFieldName.Text = "Address 1";
							lblSize.Text = "34";
							fldDescription.Text = "Current Address";
							break;
						}
					case 4:
						{
							lblFieldName.Text = "Address 2";
							lblSize.Text = "34";
							fldDescription.Text = "";
							break;
						}
					case 5:
						{
							lblFieldName.Text = "Address 3";
							lblSize.Text = "24";
							fldDescription.Text = "";
							break;
						}
					case 6:
						{
							lblFieldName.Text = "State";
							lblSize.Text = "2";
							fldDescription.Text = "Two character code for the state.";
							break;
						}
					case 7:
						{
							lblFieldName.Text = "Zip";
							lblSize.Text = "5";
							fldDescription.Text = "Zip Code";
							break;
						}
					case 8:
						{
							lblFieldName.Text = "Zip 4";
							lblSize.Text = "5";
							fldDescription.Text = "Four character zipcode preceded by a hyphen if a value exists.";
							break;
						}
					case 9:
						{
							lblFieldName.Text = "Name";
							lblSize.Text = "34";
							fldDescription.Text = "Billed owners name.";
							break;
						}
					case 10:
						{
							lblFieldName.Text = "Land Value";
							lblSize.Text = "12";
							fldDescription.Text = "Assessed value of the Land.";
							break;
						}
					case 11:
						{
							lblFieldName.Text = "Building Value";
							lblSize.Text = "12";
							fldDescription.Text = "Assessed value of the Building.";
							break;
						}
					case 12:
						{
							lblFieldName.Text = "Total Assessment Value";
							lblSize.Text = "12";
							fldDescription.Text = "Assessed value of the Land + Building.";
							break;
						}
					case 13:
						{
							lblFieldName.Text = "Exempt Value";
							lblSize.Text = "12";
							fldDescription.Text = "Exemption value.";
							break;
						}
					case 14:
						{
							lblFieldName.Text = "Net Assessment";
							lblSize.Text = "12";
							fldDescription.Text = "Assessed value of the Land + Building - Exempt.";
							break;
						}
					case 15:
						{
							lblFieldName.Text = "Location";
							lblSize.Text = "31";
							fldDescription.Text = "Location of the property.";
							break;
						}
					case 16:
						{
							lblFieldName.Text = "Map and Lot";
							lblSize.Text = "17";
							fldDescription.Text = "This is the Map and Lot of the property.";
							break;
						}
					case 17:
						{
							lblFieldName.Text = "Date";
							lblSize.Text = "10";
							fldDescription.Text = "This is the date that the interest was calculated up until.";
							break;
						}
					case 18:
						{
							lblFieldName.Text = "Past Due";
							lblSize.Text = "15";
							fldDescription.Text = "Principal that is due as of the period selected.";
							break;
						}
					case 19:
						{
							lblFieldName.Text = "Amount Coming Due";
							lblSize.Text = "15";
							fldDescription.Text = "Amount of principal that will be due after the period that was selected.";
							break;
						}
					case 20:
						{
							lblFieldName.Text = "Interest / Costs Due";
							lblSize.Text = "15";
							// MAL@20080305: Corrected wording to reflect that the interest date is used
							// Tracker Reference: 12613
							// fldDescription.Text = "This is the amount of interest and costs due for the period selected up to the date of the outprinting."
							fldDescription.Text = "This is the amount of interest and costs due for the period selected as of the Interest Date.";
							break;
						}
					case 21:
						{
							lblFieldName.Text = "Total Due";
							lblSize.Text = "15";
							// MAL@20080305: Corrected wording to reflect that the interest date is used
							// Tracker Reference: 12613
							// fldDescription.Text = "This total amount due for the period selected up to the date of the outprinting."
							fldDescription.Text = "This total amount due for the period selected as of the Interest Date.";
							break;
						}
					case 22:
						{
							lblFieldName.Text = "Total Account Due";
							lblSize.Text = "15";
							// MAL@20080305: Corrected wording to reflect that the interest date is used
							// Tracker Reference: 12613
							// fldDescription.Text = "This total amount due for the the whole account up to the day of the outprinting."
							fldDescription.Text = "This total amount due for the the whole account as of the Interest Date.";
							break;
						}
					case 23:
						{
							lblFieldName.Text = "Type";
							lblSize.Text = "1";
							fldDescription.Text = "P - Personal Property, R - Regular Real Estate, L - Liened Real Estate";
							break;
						}
                    case 24:
                        {
                            lblFieldName.Text = "RE Second Owner Name";
                            lblSize.Text = "38";
                            fldDescription.Text = "The name of the second owner.  (Real Estate Only)";
                            break;
                        }
                    case 25:
                        {
                            lblFieldName.Text = "Second Owner Address 1";
                            lblSize.Text = "34";
                            fldDescription.Text = "Current Address of the second Owner";
                            break;
                        }
                    case 26:
                        {
                            lblFieldName.Text = "Second Owner Address 2";
                            lblSize.Text = "34";
                            fldDescription.Text = "";
                            break;
                        }
                    case 27:
                        {
                            lblFieldName.Text = "Second Owner Address 3";
                            lblSize.Text = "24";
                            fldDescription.Text = "";
                            break;
                        }
                    case 28:
                        {
                            lblFieldName.Text = "Second Owner State";
                            lblSize.Text = "2";
                            fldDescription.Text = "Two character code for the state.";
                            break;
                        }
                    case 29:
                        {
                            lblFieldName.Text = "Second Owner Zip";
                            lblSize.Text = "5";
                            fldDescription.Text = "Zip Code";
                            break;
                        }
                    case 30:
                        {
                            lblFieldName.Text = "Second Owner Name";
                            lblSize.Text = "38";
                            fldDescription.Text = "Billed Second Owners name.";
                            break;
                        }
                    default:
						{
							boolDone = true;
							break;
						}
				}
				//end switch
				lngCount += 1;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error in Outprinting Format Report");
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		
	}
}
