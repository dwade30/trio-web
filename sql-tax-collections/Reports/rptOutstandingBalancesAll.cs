using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using SharedApplication.TaxCollections;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptOutstandingBalancesAll : BaseSectionReport
	{
        private TaxCollectionStatusReportConfiguration reportConfiguration;
        public double dblTotalsPay;
		public double dblTotalsPrin;
		public int lngCount;
		public int intSuppReportType;
		bool boolStarted;
		bool boolSummaryOnly;
		int lngTAType;
		private bool boolShowCurrentInterest;

		public rptOutstandingBalancesAll()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        public void SetReportOption(ref TaxCollectionStatusReportConfiguration reportConfig)
        {
            reportConfiguration = reportConfig;
        }
        private void InitializeComponentEx()
		{
			//if (_InstancePtr == null)
			//	_InstancePtr = this;
			this.Name = "Outstanding Balance Report";
		}

		public static rptOutstandingBalancesAll InstancePtr
		{
			get
			{
				return (rptOutstandingBalancesAll)Sys.GetInstance(typeof(rptOutstandingBalancesAll));
			}
		}

		//protected rptOutstandingBalancesAll _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			//if (_InstancePtr == this)
			//{
			//	_InstancePtr = null;
			//	Sys.ClearInstance(this);
			//}
			base.Dispose(disposing);
		}

		public bool ShowCurrentInterest
		{
			get
			{
				bool ShowCurrentInterest = false;
				ShowCurrentInterest = boolShowCurrentInterest;
				return ShowCurrentInterest;
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = boolStarted;
			//Detail_Format();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_PageEnd(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			// fill in the page header
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "t");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			boolSummaryOnly = FCConvert.CBool( reportConfiguration.Options.ShowSummaryOnly);
			// MAL@20080813 ; Tracker Reference: 11805
            if (reportConfiguration.Filter.IsTaxAcquired.HasValue)
            {
                if (reportConfiguration.Filter.IsTaxAcquired.Value)
                {
                    lngTAType = 1;
                }
                else
                {
                    lngTAType = 2;
                }
            }
            else
            {
                lngTAType = 0;
            }

			SetReportHeader();
			
			frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading");
			lngCount = 0;

			dblTotalsPay = 0;
			dblTotalsPrin = 0;

			boolStarted = false;
			
			sarOB1.Width = this.PrintWidth;			
			sarOb2.Width = this.PrintWidth;
			sarOb3.Width = this.PrintWidth;
			
			boolShowCurrentInterest = reportConfiguration.Options.ShowCurrentInterest;
			switch (reportConfiguration.Options.DefaultReportType)
			{
				case TaxBillingHardCodedReport.NonZeroBalanceOnAllAccounts:
					{
                        // outstanding balance
                        var srOutstandingBalances = new rptOutstandingBalances();
                        srOutstandingBalances.SetReportOptionAndParent(reportConfiguration, this,intSuppReportType);
                        sarOB1.Report = srOutstandingBalances;
                        var srOutstandingLienBalances = new rptOutstandingLienBalances();
                        srOutstandingLienBalances.SetReportOptionAndParent(reportConfiguration, this,intSuppReportType);
                        sarOb2.Report = srOutstandingLienBalances;
                        if (reportConfiguration.Options.ShowPersonalPropertyInformation)
						{
                            var srOutstandingREPPBalances = new rptOutstandingREPPBalances();
                            srOutstandingREPPBalances.SetReportOptionAndParent(reportConfiguration,this);
							sarOb3.Report = srOutstandingREPPBalances;
						}
						else
						{
							Line2.Visible = false;
						}
						lblHeader.Text = "Non Zero Balance on All Accounts";
						break;
					}
				case TaxBillingHardCodedReport.ZeroBalanceReport:
					{
						// zero balance
                        var srZeroBalances = new rptZeroBalances();
                        srZeroBalances.SetReportOptionAndParent(reportConfiguration,this,intSuppReportType);
						sarOB1.Report = srZeroBalances;
                        var srZeroLienBalances = new rptZeroLienBalances();
                        srZeroLienBalances.SetReportOptionAndParent(reportConfiguration,this,intSuppReportType);
						sarOb2.Report = srZeroLienBalances;
						lblHeader.Text = "Zero Balance Report";
						break;
					}
				case TaxBillingHardCodedReport.NegativeBalanceReport:
					{
						// negative balance
                        var srNegativeBalances = new rptNegativeBalances();
                        srNegativeBalances.SetReportOptionAndParent(reportConfiguration,this);
						sarOB1.Report = srNegativeBalances;
						var srNegativeLienBalances = new rptNegativeLienBalances();
						srNegativeLienBalances.SetReportOptionAndParent(reportConfiguration, this);
						sarOb2.Report = srNegativeLienBalances;
						lblHeader.Text = "Negative Balance Report";
						break;
					}
				case TaxBillingHardCodedReport.SupplementalOutstandingBalanceReport:
					{
						// supplemental balances
						intSuppReportType = 1;
                        var srOutstandingBalances = new rptOutstandingBalances();
                        srOutstandingBalances.SetReportOptionAndParent(reportConfiguration,this,intSuppReportType);
						sarOB1.Report = srOutstandingBalances;
                        var srOutstandingLienBalances = new rptOutstandingLienBalances();
                        srOutstandingLienBalances.SetReportOptionAndParent(reportConfiguration, this,intSuppReportType);
						sarOb2.Report = srOutstandingLienBalances;
						lblHeader.Text = "Supplemental Outstanding Balance Report";
						break;
					}
				case TaxBillingHardCodedReport.SupplementalZeroBalanceReport:
					{
						// zero balance
						intSuppReportType = 1;
                        var srZeroBalances = new rptZeroBalances();
                        srZeroBalances.SetReportOptionAndParent(reportConfiguration, this,intSuppReportType);
                        sarOB1.Report = srZeroBalances;
                        var srZeroLienBalances = new rptZeroLienBalances();
                        srZeroLienBalances.SetReportOptionAndParent(reportConfiguration, this, intSuppReportType);
                        sarOb2.Report = srZeroLienBalances;
						lblHeader.Text = "Supplemental Zero Balance Report";
						break;
					}
				case TaxBillingHardCodedReport.SupplementalNegativeBalanceReport:
					{
						// negative balance
						intSuppReportType = 1;
                        var srNegativeBalances = new rptNegativeBalances();
                        srNegativeBalances.SetReportOptionAndParent(reportConfiguration, this);
                        sarOB1.Report = srNegativeBalances;
                        var srNegativeLienBalances = new rptNegativeLienBalances();
                        srNegativeLienBalances.SetReportOptionAndParent(reportConfiguration,this);
                        sarOb2.Report = srNegativeLienBalances;
						lblHeader.Text = "Supplemental Negative Balance Report";
						break;
					}
				case TaxBillingHardCodedReport.NonZeroBalanceOnAllAccountsByYear:
					{
                        var srOutstandingBalances = new rptOutstandingBalances();
                        srOutstandingBalances.SetReportOptionAndParent(reportConfiguration, this,intSuppReportType);
                        sarOB1.Report = srOutstandingBalances;
                        var srOutstandingLienBalances = new rptOutstandingLienBalances();
                        srOutstandingLienBalances.SetReportOptionAndParent(reportConfiguration, this,intSuppReportType);
                        sarOb2.Report = srOutstandingLienBalances;
                        if (reportConfiguration.Options.ShowPersonalPropertyInformation)
						{
                            var srOutstandingREPPBalances = new rptOutstandingREPPBalances();
                            srOutstandingREPPBalances.SetReportOptionAndParent(reportConfiguration, this);
                            sarOb3.Report = srOutstandingREPPBalances;
                        }
						else
						{
							Line2.Visible = false;
						}
						lblHeader.Text = "Non Zero Balance on All Accounts By Year";
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;			
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// BindFields
			boolStarted = true;
		}

		private void SetupTotals()
		{
			// this sub will fill in the totals line at the bottom of the report
			fldTotalTaxDue.Text = Strings.Format(dblTotalsPrin, "#,##0.00");
			fldTotalPaymentReceived.Text = Strings.Format(dblTotalsPay, "#,##0.00");
			fldTotalDue.Text = Strings.Format(FCConvert.ToDouble(fldTotalTaxDue.Text) - FCConvert.ToDouble(fldTotalPaymentReceived.Text), "#,##0.00");
			if (lngCount > 1)
			{
				// this just shows the correct phrases
				lblTotals.Text = "Total for " + FCConvert.ToString(lngCount) + " Bills:";
				// kk01082015 trocls-13  This is count of bills, not accounts     & " Accounts:"
			}
			else if (lngCount == 1)
			{
				lblTotals.Text = "Total for " + FCConvert.ToString(lngCount) + " Bill:";
				// & " Account:"
			}
			else
			{
				lblTotals.Text = "No Bills Processed";
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			SetupTotals();
		}

		private void SetReportHeader()
		{
			int intCT;
			string strTemp = "";
            var strSeparator = "";
            var filter = reportConfiguration.Filter;
            if (filter.AccountRangeUsed())
            {
                if (filter.AccountMax.HasValue)
                {
                    if (filter.AccountMin.HasValue)
                    {
                        if (filter.AccountMin == filter.AccountMax)
                        {
                            strTemp += "Account: " + filter.AccountMin.Value.ToString();
                        }
                        else
                        {
                            strTemp += "Account: " + filter.AccountMin.Value.ToString() + " To " +
                                       filter.AccountMax.Value.ToString();
                        }
                    }
                    else
                    {
                        strTemp += "Account Below: " + filter.AccountMax.Value.ToString();
                    }
                }
                else
                {
                    strTemp += "Account Above: " + filter.AccountMin.Value.ToString();
                }
                strSeparator = ", ";
            }

            if (filter.TaxYearRangeUsed())
            {
                strTemp += strSeparator;

                if (filter.TaxYearMin.HasValue)
                {
                    if (filter.TaxYearMax.HasValue)
                    {
                        if (filter.TaxYearMin == filter.TaxYearMax)
                        {
                            strTemp += "Tax Year: " + filter.TaxYearMin.Value.ToString();
                        }
                        else
                        {
                            strTemp += "Tax Year: " + filter.TaxYearMin.Value.ToString() + " To " +
                                       filter.TaxYearMax.Value.ToString();
                        }
                    }
                    else
                    {
                        strTemp += "Tax Year: " + filter.TaxYearMin.Value.ToString();
                    }
                }
                else
                {
                    strTemp += "Tax Year: " + filter.TaxYearMax.Value.ToString();
                }

                strSeparator = ", ";
            }

            if (filter.RateRecordRangeUsed())
            {
                strTemp += strSeparator;

                if (filter.RateRecordMin.HasValue)
                {
                    if (filter.RateRecordMax.HasValue)
                    {
                        strTemp += "Rate Key: " + filter.RateRecordMin.Value.ToString() + " To " +
                                   filter.RateRecordMax.Value.ToString();
                    }
                    else
                    {
                        strTemp += "Rate Key: " + filter.RateRecordMin.Value.ToString();
                    }
                }
                else
                {
                    strTemp += "Rate Key: " + filter.RateRecordMax.Value.ToString();
                }

                strSeparator = ", ";
            }

            if (filter.TaxBillStatus.HasValue)
            {
                if (filter.TaxBillStatus.Value == TaxBillStatusType.Lien)
                {
                    strTemp += strSeparator;
                    strTemp += "Showing Liens";
                    strSeparator = ", ";
                }
                else if (filter.TaxBillStatus == TaxBillStatusType.Regular)
                {
                    strTemp += strSeparator;
                    strTemp += "Showing Regular";
                    strSeparator = ", ";
                }
            }

			if (lngTAType > 0)
			{
				if (Strings.Trim(strTemp) == "")
				{
					if (lngTAType == 1)
					{
						strTemp = "Tax Acquired";
					}
					else
					{
						strTemp = "Non-Tax Acquired";
					}
				}
				else
				{
					if (lngTAType == 1)
					{
						strTemp = ", Tax Acquired";
					}
					else
					{
						strTemp = ", Non-Tax Acquired";
					}
				}
			}

			if (reportConfiguration.Options.ShowCurrentInterest)
			{
				if (Strings.Trim(strTemp) == "")
				{
					strTemp += "Show Current Interest";
				}
				else
				{
					strTemp += ", Show Current Interest";
				}
			}
			if (boolSummaryOnly)
			{
				lblAccount.Visible = false;
				lblName.Visible = false;
				lblYear.Visible = false;
			}
			if (Strings.Trim(strTemp) == "")
			{
				lblReportType.Text = "Complete List";
			}
			else
			{
				lblReportType.Text = strTemp;
			}
			if (reportConfiguration.Options.UseAsOfDate())
			{
				lblReportType.Text = lblReportType.Text + "\r\n" + "As of: " + Strings.Format(reportConfiguration.Options.AsOfDate, "MM/dd/yyyy");
			}
		}

        public override  void StopFetchingData()
        {
            sarOB1?.Report?.Stop();
			sarOb2?.Report?.Stop();
            sarOb3?.Report?.Stop();
            base.StopFetchingData();
        }
	}
}