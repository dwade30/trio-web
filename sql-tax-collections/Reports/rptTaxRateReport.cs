﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptTaxRateReport : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               06/10/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/02/2004              *
		// ********************************************************
		string strSQL;
		clsDRWrapper rsData = new clsDRWrapper();
		int lngTotalAccounts;
		int lngStartYear;
		int lngEndYear;

		public rptTaxRateReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Tax Rate Report";
		}

		public static rptTaxRateReport InstancePtr
		{
			get
			{
				return (rptTaxRateReport)Sys.GetInstance(typeof(rptTaxRateReport));
			}
		}

		protected rptTaxRateReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
            }
			base.Dispose(disposing);
		}

		public void Init(string strStartYear, string strEndYear)
		{
			lngStartYear = FCConvert.ToInt32(FCConvert.ToDouble(strStartYear));
			lngEndYear = FCConvert.ToInt32(FCConvert.ToDouble(strEndYear));
			frmReportViewer.InstancePtr.Init(this);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
			//Detail_Format();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "t");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			strSQL = BuildSQL();
			rsData.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
			if (rsData.EndOfFile())
			{
				// hide the headers if there are no records
				lblDueDate1.Visible = false;
				lblDueDate2.Visible = false;
				lblDueDate3.Visible = false;
				lblDueDate4.Visible = false;
				lblIntRate.Visible = false;
				lblIntStart1.Visible = false;
				lblIntStart2.Visible = false;
				lblIntStart3.Visible = false;
				lblIntStart4.Visible = false;
				lblKey.Visible = false;
				lblPeriods.Visible = false;
				lblRateType.Visible = false;
				lblTaxRate.Visible = false;
				lblYear.Visible = false;
			}
		}

		private string BuildSQL()
		{
			string BuildSQL = "";
			BuildSQL = "SELECT * FROM RateRec WHERE [Year] >= " + FCConvert.ToString(lngStartYear) + " AND [Year] <= " + FCConvert.ToString(lngEndYear) + " ORDER BY [Year], DueDate1";
			return BuildSQL;
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intPer = 0;
				// this will fill the information into the fields
				TRYAGAIN:
				;
				if (!rsData.EndOfFile())
				{
					intPer = FCConvert.ToInt32(rsData.Get_Fields_Int16("NumberofPeriods"));
					fldKey.Text = FCConvert.ToString(rsData.Get_Fields_Int32("ID"));
					// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
					fldYear.Text = Strings.Format(rsData.Get_Fields("Year"), "0000");
					fldTaxRate.Text = Strings.Format(rsData.Get_Fields_Double("TaxRate") * 1000, "0.000");
					// kk10282014 trocl-1209  Tax rate is a mill rate not a percent  ' fldTaxRate.Text = Format(.Fields("TaxRate") * 10, "0.00%")
					fldIntRate.Text = Strings.Format(rsData.Get_Fields_Double("InterestRate"), "0.00%");
					fldRateType.Text = rsData.Get_Fields_String("RateType");
					fldNumPer.Text = FCConvert.ToString(rsData.Get_Fields_Int16("NumberofPeriods"));
					fldDueDate1.Text = Strings.Format(rsData.Get_Fields_DateTime("DueDate1"), "MM/dd/yy");
					fldIntStart1.Text = Strings.Format(rsData.Get_Fields_DateTime("InterestStartDate1"), "MM/dd/yy");
					if (intPer > 1)
					{
						if (intPer > 2)
						{
							fldDueDate2.Text = Strings.Format(rsData.Get_Fields_DateTime("DueDate2"), "MM/dd/yy");
							fldIntStart2.Text = Strings.Format(rsData.Get_Fields_DateTime("InterestStartDate2"), "MM/dd/yy");
							if (intPer > 3)
							{
								fldDueDate3.Text = Strings.Format(rsData.Get_Fields_DateTime("DueDate3"), "MM/dd/yy");
								fldIntStart3.Text = Strings.Format(rsData.Get_Fields_DateTime("InterestStartDate3"), "MM/dd/yy");
								fldDueDate4.Text = Strings.Format(rsData.Get_Fields_DateTime("DueDate4"), "MM/dd/yy");
								fldIntStart4.Text = Strings.Format(rsData.Get_Fields_DateTime("InterestStartDate4"), "MM/dd/yy");
							}
							else
							{
								fldDueDate4.Text = "N/A";
								fldIntStart4.Text = "N/A";
							}
						}
						else
						{
							fldDueDate3.Text = "N/A";
							fldDueDate4.Text = "N/A";
							fldIntStart3.Text = "N/A";
							fldIntStart4.Text = "N/A";
						}
					}
					else
					{
						fldDueDate2.Text = "N/A";
						fldDueDate3.Text = "N/A";
						fldDueDate4.Text = "N/A";
						fldIntStart2.Text = "N/A";
						fldIntStart3.Text = "N/A";
						fldIntStart4.Text = "N/A";
					}
					// move to the next record in the query
					rsData.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error in Tax Rate Report");
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		
	}
}
