﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptLienMaturityFees : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               01/25/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/21/2003              *
		// ********************************************************
		int lngTotalAccounts;
		int lngCount;
		// this will keep track of which row I am on in the grid
		double dblDemandFee;
		bool boolDone;
		double dblTotalDemand;
		double dblTotalCMF;

		public rptLienMaturityFees()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Lien Maturity Fees List";
		}

		public static rptLienMaturityFees InstancePtr
		{
			get
			{
				return (rptLienMaturityFees)Sys.GetInstance(typeof(rptLienMaturityFees));
			}
		}

		protected rptLienMaturityFees _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public void Init(int lngPassTotalAccounts, double dblPassDemand)
		{
			lngTotalAccounts = lngPassTotalAccounts;
			dblDemandFee = dblPassDemand;
			// Me.Show
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!modGlobal.Statics.arrDemand[lngCount].Used && boolDone)
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
			}
			//Detail_Format();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			// save this report for future access
			if (this.Document.Pages.Count > 0)
			{
				modGlobalFunctions.IncrementSavedReports("LastCLLienMaturityList");
				this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastCLLienMaturityList1.RDF"));
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "t");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			lngCount = 0;
		}

		private void BindFields()
		{
			// this will fill the information into the fields
			NEXTACCOUNT:
			;
			if (!boolDone)
			{
				if (!modGlobal.Statics.arrDemand[lngCount].Processed)
				{
					lngCount += 1;
					if (lngCount >= Information.UBound(modGlobal.Statics.arrDemand, 1))
					{
						boolDone = true;
					}
					goto NEXTACCOUNT;
				}
				
				fldAcct.Text = modGlobal.Statics.arrDemand[lngCount].Account.ToString();
				fldName.Text = modGlobal.Statics.arrDemand[lngCount].Name;
				fldDemand.Text = Strings.Format(modGlobal.Statics.arrDemand[lngCount].Fee, "#,##0.00");
				if (modGlobal.Statics.arrDemand[lngCount].CertifiedMailFee != 0)
				{
					fldCMF.Text = Strings.Format(modGlobal.Statics.arrDemand[lngCount].CertifiedMailFee - modGlobal.Statics.arrDemand[lngCount].Fee, "#,##0.00");
					fldTotal.Text = Strings.Format(modGlobal.Statics.arrDemand[lngCount].CertifiedMailFee, "#,##0.00");
					dblTotalDemand += modGlobal.Statics.arrDemand[lngCount].Fee;
					dblTotalCMF += modGlobal.Statics.arrDemand[lngCount].CertifiedMailFee - modGlobal.Statics.arrDemand[lngCount].Fee;
				}
				else
				{
					fldCMF.Text = Strings.Format(modGlobal.Statics.arrDemand[lngCount].CertifiedMailFee, "#,##0.00");
					fldTotal.Text = Strings.Format(modGlobal.Statics.arrDemand[lngCount].Fee, "#,##0.00");
					dblTotalDemand += modGlobal.Statics.arrDemand[lngCount].Fee;
					// dblTotalCMF = dblTotalCMF + arrDemand(lngCount).CertifiedMailFee
				}
				// move to the next record in the grid that is checked
				do
				{
					lngCount += 1;
				}
				while (!(modGlobal.Statics.arrDemand[lngCount].Processed || lngCount >= Information.UBound(modGlobal.Statics.arrDemand, 1)));
				if (lngCount >= Information.UBound(modGlobal.Statics.arrDemand, 1))
				{
					boolDone = true;
				}
				// End With
			}
			else
			{
				// clear the fields
				fldAcct.Text = "";
				fldName.Text = "";
				fldDemand.Text = "";
				fldCMF.Text = "";
				fldTotal.Text = "";
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			frmFreeReport.InstancePtr.Unload();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void SetupTotals()
		{
			// this sub will fill in the footer line at the bottom of the report
			lblNumberOfAccounts.Text = "There were " + FCConvert.ToString(lngTotalAccounts) + " accounts processed.";
			lblTotalCMF.Text = Strings.Format(dblTotalCMF, "#,##0.00");
			lblTotalDemand.Text = Strings.Format(dblTotalDemand, "#,##0.00");
			lblTotalTotal.Text = Strings.Format(dblTotalCMF + dblTotalDemand, "#,##0.00");
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			SetupTotals();
		}

		
	}
}
