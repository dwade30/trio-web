﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Collections.Generic;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmStatusLists.
	/// </summary>
	partial class frmStatusLists : BaseForm
	{
		public fecherFoundation.FCComboBox cmbReport;
		public fecherFoundation.FCFrame fraWhere;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCFrame fraSave;
		public fecherFoundation.FCButton cmdAdd;
		public fecherFoundation.FCComboBox cboSavedReport;
		public fecherFoundation.FCDraggableListBox lstSort;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCDraggableListBox lstFields;
		public fecherFoundation.FCButton cmdExit;
		public fecherFoundation.FCFrame fraQuestions;
		public fecherFoundation.FCCheckBox chkExcludePaid;
		public fecherFoundation.FCCheckBox chkShowPayments;
		public fecherFoundation.FCCheckBox chkSummaryOnly;
		public fecherFoundation.FCComboBox cmbNameOption;
		public fecherFoundation.FCCheckBox chkCurrentInterest;
		public fecherFoundation.FCCheckBox chkUseFullStatus;
		public fecherFoundation.FCCheckBox chkHardCode;
		public fecherFoundation.FCCheckBox chkREPPHardCoded;
		public fecherFoundation.FCComboBox cmbHardCode;
		public fecherFoundation.FCGrid vsQuestions;
		public fecherFoundation.FCLabel lblShowFields;
		public Wisej.Web.ImageList ImageList1;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuLayout;
		public fecherFoundation.FCToolStripMenuItem mnuAddRow;
		public fecherFoundation.FCToolStripMenuItem mnuAddColumn;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteRow;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteColumn;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmStatusLists));
            this.cmbReport = new fecherFoundation.FCComboBox();
            this.fraWhere = new fecherFoundation.FCFrame();
            this.cmdClear = new fecherFoundation.FCButton();
            this.fraSave = new fecherFoundation.FCFrame();
            this.cmdAdd = new fecherFoundation.FCButton();
            this.cboSavedReport = new fecherFoundation.FCComboBox();
            this.lstSort = new fecherFoundation.FCDraggableListBox();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.lstFields = new fecherFoundation.FCDraggableListBox();
            this.cmdExit = new fecherFoundation.FCButton();
            this.fraQuestions = new fecherFoundation.FCFrame();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.cmbHardCode = new fecherFoundation.FCComboBox();
            this.chkREPPHardCoded = new fecherFoundation.FCCheckBox();
            this.chkExcludePaid = new fecherFoundation.FCCheckBox();
            this.chkShowPayments = new fecherFoundation.FCCheckBox();
            this.chkSummaryOnly = new fecherFoundation.FCCheckBox();
            this.cmbNameOption = new fecherFoundation.FCComboBox();
            this.chkCurrentInterest = new fecherFoundation.FCCheckBox();
            this.chkUseFullStatus = new fecherFoundation.FCCheckBox();
            this.chkHardCode = new fecherFoundation.FCCheckBox();
            this.vsQuestions = new fecherFoundation.FCGrid();
            this.lblShowFields = new fecherFoundation.FCLabel();
            this.ImageList1 = new Wisej.Web.ImageList(this.components);
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuLayout = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddRow = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddColumn = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteRow = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteColumn = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.fcLabel2 = new fecherFoundation.FCLabel();
            this.fcLabel3 = new fecherFoundation.FCLabel();
            this.btnProcess = new fecherFoundation.FCButton();
            this.cmdClear1 = new fecherFoundation.FCButton();
            this.flexLayoutPanel1 = new Wisej.Web.FlexLayoutPanel();
            this.label1 = new Wisej.Web.Label();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraWhere)).BeginInit();
            this.fraWhere.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSave)).BeginInit();
            this.fraSave.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraQuestions)).BeginInit();
            this.fraQuestions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkREPPHardCoded)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExcludePaid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowPayments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSummaryOnly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCurrentInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseFullStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHardCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsQuestions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear1)).BeginInit();
            this.flexLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 650);
            this.BottomPanel.Size = new System.Drawing.Size(1008, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.flexLayoutPanel1);
            this.ClientArea.Controls.Add(this.fcLabel3);
            this.ClientArea.Controls.Add(this.lstFields);
            this.ClientArea.Controls.Add(this.fcLabel2);
            this.ClientArea.Controls.Add(this.fraWhere);
            this.ClientArea.Controls.Add(this.fraSave);
            this.ClientArea.Controls.Add(this.cmdPrint);
            this.ClientArea.Controls.Add(this.cmdExit);
            this.ClientArea.Controls.Add(this.fraQuestions);
            this.ClientArea.Controls.Add(this.lstSort);
            this.ClientArea.Location = new System.Drawing.Point(0, 65);
            this.ClientArea.Size = new System.Drawing.Size(1008, 585);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdClear1);
            this.TopPanel.Size = new System.Drawing.Size(1008, 65);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdClear1, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(177, 30);
            this.HeaderText.Text = "Custom Report";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbReport
            // 
            this.cmbReport.AutoSize = false;
            this.cmbReport.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbReport.FormattingEnabled = true;
            this.cmbReport.Items.AddRange(new object[] {
            "Edit or Create New Report",
            "Show Saved Report",
            "Delete Saved Report"});
            this.cmbReport.Location = new System.Drawing.Point(20, 30);
            this.cmbReport.Name = "cmbReport";
            this.cmbReport.Size = new System.Drawing.Size(261, 40);
            this.cmbReport.TabIndex = 1;
            this.cmbReport.Text = "Edit or Create New Report";
            this.ToolTip1.SetToolTip(this.cmbReport, null);
            this.cmbReport.SelectedIndexChanged += new System.EventHandler(this.optReport_Click);
            // 
            // fraWhere
            // 
            this.fraWhere.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraWhere.AppearanceKey = "groupboxnoborder";
            this.fraWhere.Controls.Add(this.cmdClear);
            this.fraWhere.Location = new System.Drawing.Point(30, 477);
            this.fraWhere.Name = "fraWhere";
            this.fraWhere.Size = new System.Drawing.Size(966, 267);
            this.fraWhere.TabIndex = 9;
            this.fraWhere.Text = "Select Search Criteria";
            this.ToolTip1.SetToolTip(this.fraWhere, null);
            this.fraWhere.UseMnemonic = false;
            this.fraWhere.DoubleClick += new System.EventHandler(this.fraWhere_DoubleClick);
            // 
            // cmdClear
            // 
            this.cmdClear.AppearanceKey = "toolbarButton";
            this.cmdClear.Location = new System.Drawing.Point(853, 30);
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Size = new System.Drawing.Size(94, 23);
            this.cmdClear.TabIndex = 1;
            this.cmdClear.Text = "Clear";
            this.ToolTip1.SetToolTip(this.cmdClear, null);
            this.cmdClear.Visible = false;
            this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
            // 
            // fraSave
            // 
            this.fraSave.Controls.Add(this.cmdAdd);
            this.fraSave.Controls.Add(this.cmbReport);
            this.fraSave.Controls.Add(this.cboSavedReport);
            this.fraSave.Location = new System.Drawing.Point(685, 255);
            this.fraSave.Name = "fraSave";
            this.fraSave.Size = new System.Drawing.Size(301, 176);
            this.fraSave.TabIndex = 5;
            this.fraSave.Text = "Report";
            this.ToolTip1.SetToolTip(this.fraSave, null);
            this.fraSave.UseMnemonic = false;
            // 
            // cmdAdd
            // 
            this.cmdAdd.AppearanceKey = "actionButton";
            this.cmdAdd.Location = new System.Drawing.Point(20, 130);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Size = new System.Drawing.Size(261, 40);
            this.cmdAdd.TabIndex = 3;
            this.cmdAdd.Text = "Add Custom report to Library";
            this.ToolTip1.SetToolTip(this.cmdAdd, null);
            this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
            // 
            // cboSavedReport
            // 
            this.cboSavedReport.AutoSize = false;
            this.cboSavedReport.BackColor = System.Drawing.SystemColors.Window;
            this.cboSavedReport.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboSavedReport.FormattingEnabled = true;
            this.cboSavedReport.Location = new System.Drawing.Point(20, 80);
            this.cboSavedReport.Name = "cboSavedReport";
            this.cboSavedReport.Size = new System.Drawing.Size(261, 40);
            this.cboSavedReport.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.cboSavedReport, null);
            this.cboSavedReport.SelectedIndexChanged += new System.EventHandler(this.cboSavedReport_SelectedIndexChanged);
            // 
            // lstSort
            // 
            this.lstSort.AllowDrag = true;
            this.lstSort.AllowDrop = true;
            this.lstSort.Appearance = 0;
            this.lstSort.BackColor = System.Drawing.SystemColors.Window;
            this.lstSort.CheckBoxes = true;
            this.lstSort.Location = new System.Drawing.Point(372, 280);
            this.lstSort.MultiSelect = 0;
            this.lstSort.Name = "lstSort";
            this.lstSort.Size = new System.Drawing.Size(269, 130);
            this.lstSort.Sorted = false;
            this.lstSort.Style = 1;
            this.lstSort.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.lstSort, null);
            // 
            // cmdPrint
            // 
            this.cmdPrint.AppearanceKey = "toolbarButton";
            this.cmdPrint.Location = new System.Drawing.Point(705, 441);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(96, 26);
            this.cmdPrint.TabIndex = 7;
            this.cmdPrint.Text = "Print";
            this.ToolTip1.SetToolTip(this.cmdPrint, null);
            this.cmdPrint.Visible = false;
            this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
            // 
            // lstFields
            // 
            this.lstFields.AllowDrag = true;
            this.lstFields.AllowDrop = true;
            this.lstFields.Appearance = 0;
            this.lstFields.BackColor = System.Drawing.SystemColors.Window;
            this.lstFields.CheckBoxes = true;
            this.lstFields.Location = new System.Drawing.Point(30, 280);
            this.lstFields.MultiSelect = 0;
            this.lstFields.Name = "lstFields";
            this.lstFields.Size = new System.Drawing.Size(289, 130);
            this.lstFields.Sorted = false;
            this.lstFields.Style = 1;
            this.lstFields.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.lstFields, null);
            this.lstFields.SelectedIndexChanged += new System.EventHandler(this.lstFields_SelectedIndexChanged);
            // 
            // cmdExit
            // 
            this.cmdExit.AppearanceKey = "toolbarButton";
            this.cmdExit.Location = new System.Drawing.Point(823, 441);
            this.cmdExit.Name = "cmdExit";
            this.cmdExit.Size = new System.Drawing.Size(96, 26);
            this.cmdExit.TabIndex = 8;
            this.cmdExit.Text = "Exit";
            this.ToolTip1.SetToolTip(this.cmdExit, null);
            this.cmdExit.Visible = false;
            this.cmdExit.Click += new System.EventHandler(this.cmdExit_Click);
            // 
            // fraQuestions
            // 
            this.fraQuestions.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraQuestions.AppearanceKey = "groupboxnoborder";
            this.fraQuestions.Controls.Add(this.fcLabel1);
            this.fraQuestions.Controls.Add(this.cmbHardCode);
            this.fraQuestions.Controls.Add(this.chkREPPHardCoded);
            this.fraQuestions.Controls.Add(this.chkExcludePaid);
            this.fraQuestions.Controls.Add(this.chkShowPayments);
            this.fraQuestions.Controls.Add(this.chkSummaryOnly);
            this.fraQuestions.Controls.Add(this.cmbNameOption);
            this.fraQuestions.Controls.Add(this.chkCurrentInterest);
            this.fraQuestions.Controls.Add(this.chkUseFullStatus);
            this.fraQuestions.Controls.Add(this.chkHardCode);
            this.fraQuestions.Controls.Add(this.vsQuestions);
            this.fraQuestions.Controls.Add(this.lblShowFields);
            this.fraQuestions.Location = new System.Drawing.Point(0, 0);
            this.fraQuestions.Name = "fraQuestions";
            this.fraQuestions.Size = new System.Drawing.Size(1005, 244);
            this.fraQuestions.TabIndex = 0;
            this.ToolTip1.SetToolTip(this.fraQuestions, null);
            this.fraQuestions.UseMnemonic = false;
            // 
            // fcLabel1
            // 
            this.fcLabel1.AutoSize = true;
            this.fcLabel1.Location = new System.Drawing.Point(387, 198);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(205, 17);
            this.fcLabel1.TabIndex = 1;
            this.fcLabel1.Text = "CHOOSE A DEFAULT REPORT";
            this.fcLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.fcLabel1, null);
            // 
            // cmbHardCode
            // 
            this.cmbHardCode.AutoSize = false;
            this.cmbHardCode.BackColor = System.Drawing.SystemColors.Window;
            this.cmbHardCode.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbHardCode.Enabled = false;
            this.cmbHardCode.FormattingEnabled = true;
            this.cmbHardCode.Location = new System.Drawing.Point(609, 184);
            this.cmbHardCode.Name = "cmbHardCode";
            this.cmbHardCode.Size = new System.Drawing.Size(376, 40);
            this.cmbHardCode.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.cmbHardCode, null);
            this.cmbHardCode.SelectedIndexChanged += new System.EventHandler(this.cmbHardCode_SelectedIndexChanged);
            // 
            // chkREPPHardCoded
            // 
            this.chkREPPHardCoded.Enabled = false;
            this.chkREPPHardCoded.Location = new System.Drawing.Point(603, 108);
            this.chkREPPHardCoded.Name = "chkREPPHardCoded";
            this.chkREPPHardCoded.Size = new System.Drawing.Size(160, 24);
            this.chkREPPHardCoded.TabIndex = 10;
            this.chkREPPHardCoded.Text = "Show PP Information";
            this.ToolTip1.SetToolTip(this.chkREPPHardCoded, null);
            // 
            // chkExcludePaid
            // 
            this.chkExcludePaid.Location = new System.Drawing.Point(387, 146);
            this.chkExcludePaid.Name = "chkExcludePaid";
            this.chkExcludePaid.Size = new System.Drawing.Size(109, 24);
            this.chkExcludePaid.TabIndex = 6;
            this.chkExcludePaid.Text = "Exclude Paid";
            this.ToolTip1.SetToolTip(this.chkExcludePaid, "This will exclude all the bills that have a zero balance.");
            // 
            // chkShowPayments
            // 
            this.chkShowPayments.Location = new System.Drawing.Point(387, 108);
            this.chkShowPayments.Name = "chkShowPayments";
            this.chkShowPayments.Size = new System.Drawing.Size(129, 24);
            this.chkShowPayments.TabIndex = 5;
            this.chkShowPayments.Text = "Show Payments";
            this.ToolTip1.SetToolTip(this.chkShowPayments, "This will show all of the payments for this account.");
            // 
            // chkSummaryOnly
            // 
            this.chkSummaryOnly.Location = new System.Drawing.Point(603, 70);
            this.chkSummaryOnly.Name = "chkSummaryOnly";
            this.chkSummaryOnly.Size = new System.Drawing.Size(160, 24);
            this.chkSummaryOnly.TabIndex = 8;
            this.chkSummaryOnly.Text = "Show Summary Only";
            this.ToolTip1.SetToolTip(this.chkSummaryOnly, null);
            // 
            // cmbNameOption
            // 
            this.cmbNameOption.AutoSize = false;
            this.cmbNameOption.BackColor = System.Drawing.SystemColors.Window;
            this.cmbNameOption.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbNameOption.FormattingEnabled = true;
            this.cmbNameOption.Location = new System.Drawing.Point(387, 20);
            this.cmbNameOption.Name = "cmbNameOption";
            this.cmbNameOption.Size = new System.Drawing.Size(393, 40);
            this.cmbNameOption.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.cmbNameOption, null);
            this.cmbNameOption.DropDown += new System.EventHandler(this.cmbNameOption_DropDown);
            this.cmbNameOption.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbNameOption_KeyDown);
            // 
            // chkCurrentInterest
            // 
            this.chkCurrentInterest.Enabled = false;
            this.chkCurrentInterest.Location = new System.Drawing.Point(800, 108);
            this.chkCurrentInterest.Name = "chkCurrentInterest";
            this.chkCurrentInterest.Size = new System.Drawing.Size(166, 24);
            this.chkCurrentInterest.TabIndex = 7;
            this.chkCurrentInterest.Text = "Show Current Interest";
            this.ToolTip1.SetToolTip(this.chkCurrentInterest, null);
            this.chkCurrentInterest.Visible = false;
            // 
            // chkUseFullStatus
            // 
            this.chkUseFullStatus.Location = new System.Drawing.Point(387, 70);
            this.chkUseFullStatus.Name = "chkUseFullStatus";
            this.chkUseFullStatus.Size = new System.Drawing.Size(166, 24);
            this.chkUseFullStatus.TabIndex = 4;
            this.chkUseFullStatus.Text = "Show Current Interest";
            this.ToolTip1.SetToolTip(this.chkUseFullStatus, null);
            this.chkUseFullStatus.Click += new System.EventHandler(this.chkUseFullStatus_Click);
            // 
            // chkHardCode
            // 
            this.chkHardCode.Location = new System.Drawing.Point(603, 146);
            this.chkHardCode.Name = "chkHardCode";
            this.chkHardCode.Size = new System.Drawing.Size(158, 24);
            this.chkHardCode.TabIndex = 9;
            this.chkHardCode.Text = "Use a Default Report";
            this.ToolTip1.SetToolTip(this.chkHardCode, null);
            this.chkHardCode.KeyDown += new Wisej.Web.KeyEventHandler(this.chkHardCode_KeyDown);
            this.chkHardCode.Click += new System.EventHandler(this.chkHardCode_Click);
            // 
            // vsQuestions
            // 
            this.vsQuestions.AllowSelection = false;
            this.vsQuestions.AllowUserToResizeColumns = false;
            this.vsQuestions.AllowUserToResizeRows = false;
            this.vsQuestions.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsQuestions.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsQuestions.BackColorBkg = System.Drawing.Color.Empty;
            this.vsQuestions.BackColorFixed = System.Drawing.Color.Empty;
            this.vsQuestions.BackColorSel = System.Drawing.Color.Empty;
            this.vsQuestions.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsQuestions.Cols = 2;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsQuestions.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vsQuestions.ColumnHeadersHeight = 30;
            this.vsQuestions.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.vsQuestions.ColumnHeadersVisible = false;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsQuestions.DefaultCellStyle = dataGridViewCellStyle2;
            this.vsQuestions.DragIcon = null;
            this.vsQuestions.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsQuestions.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsQuestions.FixedRows = 0;
            this.vsQuestions.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsQuestions.FrozenCols = 0;
            this.vsQuestions.GridColor = System.Drawing.Color.Empty;
            this.vsQuestions.GridColorFixed = System.Drawing.Color.Empty;
            this.vsQuestions.Location = new System.Drawing.Point(800, 21);
            this.vsQuestions.Name = "vsQuestions";
            this.vsQuestions.OutlineCol = 0;
            this.vsQuestions.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsQuestions.RowHeightMin = 0;
            this.vsQuestions.Rows = 5;
            this.vsQuestions.ScrollTipText = null;
            this.vsQuestions.ShowColumnVisibilityMenu = false;
            this.vsQuestions.Size = new System.Drawing.Size(185, 57);
            this.vsQuestions.StandardTab = true;
            this.vsQuestions.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsQuestions.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsQuestions.TabIndex = 11;
            this.ToolTip1.SetToolTip(this.vsQuestions, null);
            this.vsQuestions.Visible = false;
            this.vsQuestions.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsQuestions_ValidateEdit);
            this.vsQuestions.CurrentCellChanged += new System.EventHandler(this.vsQuestions_RowColChange);
            this.vsQuestions.KeyDown += new Wisej.Web.KeyEventHandler(this.vsQuestions_KeyDown);
            this.vsQuestions.Click += new System.EventHandler(this.vsQuestions_Click);
            this.vsQuestions.DoubleClick += new System.EventHandler(this.vsQuestions_DblClick);
            // 
            // lblShowFields
            // 
            this.lblShowFields.Location = new System.Drawing.Point(30, 25);
            this.lblShowFields.Name = "lblShowFields";
            this.lblShowFields.Size = new System.Drawing.Size(337, 199);
            this.lblShowFields.TabIndex = 0;
            this.ToolTip1.SetToolTip(this.lblShowFields, null);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageSize = new System.Drawing.Size(256, 18);
            this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(192, 192, 192);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuLayout
            // 
            this.mnuLayout.Enabled = false;
            this.mnuLayout.Index = -1;
            this.mnuLayout.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddRow,
            this.mnuAddColumn,
            this.mnuDeleteRow,
            this.mnuDeleteColumn});
            this.mnuLayout.Name = "mnuLayout";
            this.mnuLayout.Text = "Layout";
            // 
            // mnuAddRow
            // 
            this.mnuAddRow.Index = 0;
            this.mnuAddRow.Name = "mnuAddRow";
            this.mnuAddRow.Shortcut = Wisej.Web.Shortcut.F2;
            this.mnuAddRow.Text = "Add Row";
            // 
            // mnuAddColumn
            // 
            this.mnuAddColumn.Index = 1;
            this.mnuAddColumn.Name = "mnuAddColumn";
            this.mnuAddColumn.Shortcut = Wisej.Web.Shortcut.F3;
            this.mnuAddColumn.Text = "Add Column";
            // 
            // mnuDeleteRow
            // 
            this.mnuDeleteRow.Index = 2;
            this.mnuDeleteRow.Name = "mnuDeleteRow";
            this.mnuDeleteRow.Shortcut = Wisej.Web.Shortcut.F4;
            this.mnuDeleteRow.Text = "Delete Row";
            // 
            // mnuDeleteColumn
            // 
            this.mnuDeleteColumn.Index = 3;
            this.mnuDeleteColumn.Name = "mnuDeleteColumn";
            this.mnuDeleteColumn.Shortcut = Wisej.Web.Shortcut.F5;
            this.mnuDeleteColumn.Text = "Delete Column";
            // 
            // fcLabel2
            // 
            this.fcLabel2.AutoSize = true;
            this.fcLabel2.Location = new System.Drawing.Point(30, 254);
            this.fcLabel2.Name = "fcLabel2";
            this.fcLabel2.Size = new System.Drawing.Size(223, 17);
            this.fcLabel2.TabIndex = 1;
            this.fcLabel2.Text = "FIELDS TO DISPLAY ON REPORT";
            this.fcLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.fcLabel2, null);
            // 
            // fcLabel3
            // 
            this.fcLabel3.AutoSize = true;
            this.fcLabel3.Location = new System.Drawing.Point(372, 255);
            this.fcLabel3.Name = "fcLabel3";
            this.fcLabel3.Size = new System.Drawing.Size(141, 17);
            this.fcLabel3.TabIndex = 3;
            this.fcLabel3.Text = "FIELDS TO SORT BY";
            this.fcLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.fcLabel3, null);
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Location = new System.Drawing.Point(444, 30);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcess.Size = new System.Drawing.Size(120, 48);
            this.btnProcess.TabIndex = 1;
            this.btnProcess.Text = "Process";
            this.ToolTip1.SetToolTip(this.btnProcess, null);
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // cmdClear1
            // 
            this.cmdClear1.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdClear1.AppearanceKey = "toolbarButton";
            this.cmdClear1.Location = new System.Drawing.Point(833, 29);
            this.cmdClear1.Name = "cmdClear1";
            this.cmdClear1.Size = new System.Drawing.Size(152, 24);
            this.cmdClear1.TabIndex = 52;
            this.cmdClear1.Text = "Clear Search Criteria";
            this.ToolTip1.SetToolTip(this.cmdClear1, null);
            this.cmdClear1.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // flexLayoutPanel1
            // 
            this.flexLayoutPanel1.Controls.Add(this.label1);
            this.flexLayoutPanel1.LayoutStyle = Wisej.Web.FlexLayoutStyle.Vertical;
            this.flexLayoutPanel1.Location = new System.Drawing.Point(19, 464);
            this.flexLayoutPanel1.Name = "flexLayoutPanel1";
            this.flexLayoutPanel1.Size = new System.Drawing.Size(590, 106);
            this.flexLayoutPanel1.TabIndex = 10;
            this.ToolTip1.SetToolTip(this.flexLayoutPanel1, null);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(584, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "label1";
            this.ToolTip1.SetToolTip(this.label1, null);
            // 
            // frmStatusLists
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1008, 758);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmStatusLists";
            this.Text = "Custom Report";
            this.ToolTip1.SetToolTip(this, null);
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmStatusLists_Load);
            this.Activated += new System.EventHandler(this.frmStatusLists_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmStatusLists_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraWhere)).EndInit();
            this.fraWhere.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSave)).EndInit();
            this.fraSave.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraQuestions)).EndInit();
            this.fraQuestions.ResumeLayout(false);
            this.fraQuestions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkREPPHardCoded)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExcludePaid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowPayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSummaryOnly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCurrentInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseFullStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHardCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsQuestions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear1)).EndInit();
            this.flexLayoutPanel1.ResumeLayout(false);
            this.flexLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCLabel fcLabel1;
		private FCLabel fcLabel2;
		private FCLabel fcLabel3;
		private FCButton btnProcess;
		public FCButton cmdClear1;
        private FlexLayoutPanel flexLayoutPanel1;
        private Label label1;
    }
}
