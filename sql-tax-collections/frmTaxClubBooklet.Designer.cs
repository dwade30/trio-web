﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmTaxClubBooklet.
	/// </summary>
	partial class frmTaxClubBooklet : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtAddress;
		public fecherFoundation.FCGrid vsTC;
		public fecherFoundation.FCFrame frmAddress;
		public fecherFoundation.FCTextBox txtAddress_3;
		public fecherFoundation.FCTextBox txtAddress_2;
		public fecherFoundation.FCTextBox txtAddress_1;
		public fecherFoundation.FCTextBox txtAddress_0;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTaxClubBooklet));
			this.vsTC = new fecherFoundation.FCGrid();
			this.frmAddress = new fecherFoundation.FCFrame();
			this.txtAddress_3 = new fecherFoundation.FCTextBox();
			this.txtAddress_2 = new fecherFoundation.FCTextBox();
			this.txtAddress_1 = new fecherFoundation.FCTextBox();
			this.txtAddress_0 = new fecherFoundation.FCTextBox();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.lblTC = new fecherFoundation.FCLabel();
			this.btnProcess = new fecherFoundation.FCButton();
			this.cmdFileClear = new fecherFoundation.FCButton();
			this.cmdFileSelectAll = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsTC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.frmAddress)).BeginInit();
			this.frmAddress.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileClear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSelectAll)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Size = new System.Drawing.Size(693, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.lblTC);
			this.ClientArea.Controls.Add(this.vsTC);
			this.ClientArea.Controls.Add(this.frmAddress);
			this.ClientArea.Size = new System.Drawing.Size(693, 532);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFileSelectAll);
			this.TopPanel.Controls.Add(this.cmdFileClear);
			this.TopPanel.Size = new System.Drawing.Size(693, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileClear, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileSelectAll, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(257, 30);
			this.HeaderText.Text = "Print Tax Club Booklet";
			// 
			// vsTC
			// 
			this.vsTC.AllowSelection = false;
			this.vsTC.AllowUserToResizeColumns = false;
			this.vsTC.AllowUserToResizeRows = false;
			this.vsTC.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsTC.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsTC.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsTC.BackColorBkg = System.Drawing.Color.Empty;
			this.vsTC.BackColorFixed = System.Drawing.Color.Empty;
			this.vsTC.BackColorSel = System.Drawing.Color.Empty;
			this.vsTC.Cols = 5;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsTC.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsTC.ColumnHeadersHeight = 30;
			this.vsTC.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsTC.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsTC.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsTC.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vsTC.FixedCols = 0;
			this.vsTC.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsTC.FrozenCols = 0;
			this.vsTC.FrozenRows = 0;
			this.vsTC.GridColor = System.Drawing.Color.Empty;
			this.vsTC.Location = new System.Drawing.Point(30, 356);
			this.vsTC.Name = "vsTC";
			this.vsTC.ReadOnly = true;
			this.vsTC.RowHeadersVisible = false;
			this.vsTC.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsTC.RowHeightMin = 0;
			this.vsTC.Rows = 1;
			this.vsTC.ShowColumnVisibilityMenu = false;
			this.vsTC.Size = new System.Drawing.Size(645, 156);
			this.vsTC.StandardTab = true;
			this.vsTC.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsTC.TabIndex = 2;
			this.vsTC.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vsTC_KeyPress);
			this.vsTC.Click += new System.EventHandler(this.vsTC_Click);
			this.vsTC.DoubleClick += new System.EventHandler(this.vsTC_DblClick);
			// 
			// frmAddress
			// 
			this.frmAddress.Controls.Add(this.txtAddress_3);
			this.frmAddress.Controls.Add(this.txtAddress_2);
			this.frmAddress.Controls.Add(this.txtAddress_1);
			this.frmAddress.Controls.Add(this.txtAddress_0);
			this.frmAddress.Location = new System.Drawing.Point(30, 30);
			this.frmAddress.Name = "frmAddress";
			this.frmAddress.Size = new System.Drawing.Size(312, 270);
			this.frmAddress.TabIndex = 0;
			this.frmAddress.Text = "Return Address";
			this.frmAddress.UseMnemonic = false;
			// 
			// txtAddress_3
			// 
			this.txtAddress_3.MaxLength = 50;
			this.txtAddress_3.AutoSize = false;
			this.txtAddress_3.BackColor = System.Drawing.SystemColors.Window;
			this.txtAddress_3.Location = new System.Drawing.Point(20, 210);
			this.txtAddress_3.Name = "txtAddress_3";
			this.txtAddress_3.Size = new System.Drawing.Size(272, 40);
			this.txtAddress_3.TabIndex = 4;
			// 
			// txtAddress_2
			// 
			this.txtAddress_2.MaxLength = 50;
			this.txtAddress_2.AutoSize = false;
			this.txtAddress_2.BackColor = System.Drawing.SystemColors.Window;
			this.txtAddress_2.Location = new System.Drawing.Point(20, 150);
			this.txtAddress_2.Name = "txtAddress_2";
			this.txtAddress_2.Size = new System.Drawing.Size(272, 40);
			this.txtAddress_2.TabIndex = 3;
			// 
			// txtAddress_1
			// 
			this.txtAddress_1.MaxLength = 50;
			this.txtAddress_1.AutoSize = false;
			this.txtAddress_1.BackColor = System.Drawing.SystemColors.Window;
			this.txtAddress_1.Location = new System.Drawing.Point(20, 90);
			this.txtAddress_1.Name = "txtAddress_1";
			this.txtAddress_1.Size = new System.Drawing.Size(272, 40);
			this.txtAddress_1.TabIndex = 2;
			// 
			// txtAddress_0
			// 
			this.txtAddress_0.MaxLength = 50;
			this.txtAddress_0.AutoSize = false;
			this.txtAddress_0.BackColor = System.Drawing.SystemColors.Window;
			this.txtAddress_0.Location = new System.Drawing.Point(20, 30);
			this.txtAddress_0.Name = "txtAddress_0";
			this.txtAddress_0.Size = new System.Drawing.Size(272, 40);
			this.txtAddress_0.TabIndex = 1;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.Text = "File";
			// 
			// lblTC
			// 
			this.lblTC.AutoSize = true;
			this.lblTC.Location = new System.Drawing.Point(30, 320);
			this.lblTC.Name = "lblTC";
			this.lblTC.Size = new System.Drawing.Size(189, 16);
			this.lblTC.TabIndex = 1;
			this.lblTC.Text = "SELECT ACCOUNTS TO PRINT";
			this.lblTC.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(278, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(144, 48);
			this.btnProcess.TabIndex = 1;
			this.btnProcess.Text = "Print Booklet";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// cmdFileClear
			// 
			this.cmdFileClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileClear.AppearanceKey = "toolbarButton";
			this.cmdFileClear.Location = new System.Drawing.Point(564, 30);
			this.cmdFileClear.Name = "cmdFileClear";
			this.cmdFileClear.Size = new System.Drawing.Size(111, 24);
			this.cmdFileClear.TabIndex = 54;
			this.cmdFileClear.Text = "Clear Selection";
			this.cmdFileClear.Click += new System.EventHandler(this.cmdFileClear_Click);
			// 
			// cmdFileSelectAll
			// 
			this.cmdFileSelectAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileSelectAll.AppearanceKey = "toolbarButton";
			this.cmdFileSelectAll.Location = new System.Drawing.Point(484, 30);
			this.cmdFileSelectAll.Name = "cmdFileSelectAll";
			this.cmdFileSelectAll.Size = new System.Drawing.Size(74, 24);
			this.cmdFileSelectAll.TabIndex = 55;
			this.cmdFileSelectAll.Text = "Select All";
			this.cmdFileSelectAll.Click += new System.EventHandler(this.cmdFileSelectAll_Click);
			// 
			// frmTaxClubBooklet
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(693, 700);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmTaxClubBooklet";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Print Tax Club Booklet";
			this.Load += new System.EventHandler(this.frmTaxClubBooklet_Load);
			this.Activated += new System.EventHandler(this.frmTaxClubBooklet_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmTaxClubBooklet_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsTC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.frmAddress)).EndInit();
			this.frmAddress.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileClear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSelectAll)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCLabel lblTC;
		private FCButton btnProcess;
		public FCButton cmdFileClear;
		public FCButton cmdFileSelectAll;
	}
}
