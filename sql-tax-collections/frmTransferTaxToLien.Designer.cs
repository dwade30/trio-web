﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmTransferTaxToLien.
	/// </summary>
	partial class frmTransferTaxToLien : BaseForm
	{
		public fecherFoundation.FCPanel fraGrid;
		public fecherFoundation.FCGrid vsDemand;
		public fecherFoundation.FCLabel lblInstruction;
		public fecherFoundation.FCPanel fraValidate;
		public fecherFoundation.FCGrid vsValidate;
		public fecherFoundation.FCLabel lblValidateInstruction;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTransferTaxToLien));
			this.fraGrid = new fecherFoundation.FCPanel();
			this.vsDemand = new fecherFoundation.FCGrid();
			this.lblInstruction = new fecherFoundation.FCLabel();
			this.fraValidate = new fecherFoundation.FCPanel();
			this.vsValidate = new fecherFoundation.FCGrid();
			this.lblValidateInstruction = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.btnProcess = new fecherFoundation.FCButton();
			this.cmdFileSelectAll = new fecherFoundation.FCButton();
			this.cmdFileClear = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraGrid)).BeginInit();
			this.fraGrid.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsDemand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraValidate)).BeginInit();
			this.fraValidate.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsValidate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSelectAll)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileClear)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 393);
			this.BottomPanel.Size = new System.Drawing.Size(651, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraGrid);
			this.ClientArea.Controls.Add(this.fraValidate);
			this.ClientArea.Size = new System.Drawing.Size(651, 333);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFileSelectAll);
			this.TopPanel.Controls.Add(this.cmdFileClear);
			this.TopPanel.Size = new System.Drawing.Size(651, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileClear, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileSelectAll, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(232, 30);
			this.HeaderText.Text = "Transfer Tax to Lien";
			// 
			// fraGrid
			// 
			this.fraGrid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.fraGrid.AppearanceKey = "groupBoxNoBorders";
			this.fraGrid.Controls.Add(this.vsDemand);
			this.fraGrid.Controls.Add(this.lblInstruction);
			this.fraGrid.Location = new System.Drawing.Point(10, 10);
			this.fraGrid.Name = "fraGrid";
			this.fraGrid.Size = new System.Drawing.Size(631, 320);
			this.fraGrid.TabIndex = 1;
			this.fraGrid.Visible = false;
			// 
			// vsDemand
			// 
			this.vsDemand.AllowSelection = false;
			this.vsDemand.AllowUserToResizeColumns = false;
			this.vsDemand.AllowUserToResizeRows = false;
			this.vsDemand.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsDemand.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsDemand.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsDemand.BackColorBkg = System.Drawing.Color.Empty;
			this.vsDemand.BackColorFixed = System.Drawing.Color.Empty;
			this.vsDemand.BackColorSel = System.Drawing.Color.Empty;
			this.vsDemand.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsDemand.Cols = 5;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsDemand.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsDemand.ColumnHeadersHeight = 30;
			this.vsDemand.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsDemand.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsDemand.DragIcon = null;
			this.vsDemand.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsDemand.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vsDemand.FixedCols = 0;
			this.vsDemand.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsDemand.FrozenCols = 0;
			this.vsDemand.GridColor = System.Drawing.Color.Empty;
			this.vsDemand.GridColorFixed = System.Drawing.Color.Empty;
			this.vsDemand.Location = new System.Drawing.Point(20, 140);
			this.vsDemand.Name = "vsDemand";
			this.vsDemand.OutlineCol = 0;
			this.vsDemand.ReadOnly = true;
			this.vsDemand.RowHeadersVisible = false;
			this.vsDemand.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsDemand.RowHeightMin = 0;
			this.vsDemand.Rows = 1;
			this.vsDemand.ScrollTipText = null;
			this.vsDemand.ShowColumnVisibilityMenu = false;
			this.vsDemand.Size = new System.Drawing.Size(591, 160);
			this.vsDemand.StandardTab = true;
			this.vsDemand.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsDemand.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsDemand.TabIndex = 1;
			this.vsDemand.CurrentCellChanged += new System.EventHandler(this.vsDemand_RowColChange);
			//this.vsDemand.CellMouseMove += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vsDemand_MouseMove);
			this.vsDemand.DoubleClick += new System.EventHandler(this.vsDemand_DblClick);
            this.vsDemand.CellFormatting += new DataGridViewCellFormattingEventHandler(vsDemand_CellFormatting);
			// 
			// lblInstruction
			// 
			this.lblInstruction.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.lblInstruction.Location = new System.Drawing.Point(20, 30);
			this.lblInstruction.Name = "lblInstruction";
			this.lblInstruction.Size = new System.Drawing.Size(591, 69);
			this.lblInstruction.TabIndex = 0;
			this.lblInstruction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fraValidate
			// 
			this.fraValidate.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.fraValidate.AppearanceKey = "groupBoxNoBorders";
			this.fraValidate.Controls.Add(this.vsValidate);
			this.fraValidate.Controls.Add(this.lblValidateInstruction);
			this.fraValidate.Location = new System.Drawing.Point(10, 10);
			this.fraValidate.Name = "fraValidate";
			this.fraValidate.Size = new System.Drawing.Size(631, 320);
			this.fraValidate.TabIndex = 0;
			this.fraValidate.Visible = false;
			// 
			// vsValidate
			// 
			this.vsValidate.AllowSelection = false;
			this.vsValidate.AllowUserToResizeColumns = false;
			this.vsValidate.AllowUserToResizeRows = false;
			this.vsValidate.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsValidate.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsValidate.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsValidate.BackColorBkg = System.Drawing.Color.Empty;
			this.vsValidate.BackColorFixed = System.Drawing.Color.Empty;
			this.vsValidate.BackColorSel = System.Drawing.Color.Empty;
			this.vsValidate.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsValidate.Cols = 2;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsValidate.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.vsValidate.ColumnHeadersHeight = 30;
			this.vsValidate.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsValidate.DefaultCellStyle = dataGridViewCellStyle4;
			this.vsValidate.DragIcon = null;
			this.vsValidate.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsValidate.FixedCols = 0;
			this.vsValidate.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsValidate.FrozenCols = 0;
			this.vsValidate.GridColor = System.Drawing.Color.Empty;
			this.vsValidate.GridColorFixed = System.Drawing.Color.Empty;
			this.vsValidate.Location = new System.Drawing.Point(20, 115);
			this.vsValidate.Name = "vsValidate";
			this.vsValidate.OutlineCol = 0;
			this.vsValidate.ReadOnly = true;
			this.vsValidate.RowHeadersVisible = false;
			this.vsValidate.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsValidate.RowHeightMin = 0;
			this.vsValidate.Rows = 1;
			this.vsValidate.ScrollTipText = null;
			this.vsValidate.ShowColumnVisibilityMenu = false;
			this.vsValidate.Size = new System.Drawing.Size(591, 185);
			this.vsValidate.StandardTab = true;
			this.vsValidate.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsValidate.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsValidate.TabIndex = 1;
			// 
			// lblValidateInstruction
			// 
			this.lblValidateInstruction.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.lblValidateInstruction.Location = new System.Drawing.Point(20, 30);
			this.lblValidateInstruction.Name = "lblValidateInstruction";
			this.lblValidateInstruction.Size = new System.Drawing.Size(591, 45);
			this.lblValidateInstruction.TabIndex = 0;
			this.lblValidateInstruction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(265, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(120, 48);
			this.btnProcess.TabIndex = 1;
			this.btnProcess.Text = "Process";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// cmdFileSelectAll
			// 
			this.cmdFileSelectAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileSelectAll.AppearanceKey = "toolbarButton";
			this.cmdFileSelectAll.Location = new System.Drawing.Point(558, 30);
			this.cmdFileSelectAll.Name = "cmdFileSelectAll";
			this.cmdFileSelectAll.Size = new System.Drawing.Size(73, 24);
			this.cmdFileSelectAll.TabIndex = 57;
			this.cmdFileSelectAll.Text = "Select All";
			this.cmdFileSelectAll.Click += new System.EventHandler(this.cmdFileSelectAll_Click);
			// 
			// cmdFileClear
			// 
			this.cmdFileClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileClear.AppearanceKey = "toolbarButton";
			this.cmdFileClear.Location = new System.Drawing.Point(444, 30);
			this.cmdFileClear.Name = "cmdFileClear";
			this.cmdFileClear.Size = new System.Drawing.Size(107, 24);
			this.cmdFileClear.TabIndex = 56;
			this.cmdFileClear.Text = "Clear Selection";
			this.cmdFileClear.Click += new System.EventHandler(this.cmdFileClear_Click);
			// 
			// frmTransferTaxToLien
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(651, 501);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmTransferTaxToLien";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Transfer Tax to Lien";
			this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.Load += new System.EventHandler(this.frmTransferTaxToLien_Load);
			this.Activated += new System.EventHandler(this.frmTransferTaxToLien_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmTransferTaxToLien_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmTransferTaxToLien_KeyPress);
			this.Resize += new System.EventHandler(this.frmTransferTaxToLien_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraGrid)).EndInit();
			this.fraGrid.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsDemand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraValidate)).EndInit();
			this.fraValidate.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsValidate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSelectAll)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileClear)).EndInit();
			this.ResumeLayout(false);

		}

       
        #endregion

        private System.ComponentModel.IContainer components;
		private FCButton btnProcess;
		public FCButton cmdFileSelectAll;
		public FCButton cmdFileClear;
	}
}
