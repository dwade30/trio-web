﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmTaxAcquiredList.
	/// </summary>
	partial class frmTaxAcquiredList : BaseForm
	{
		public fecherFoundation.FCPanel fraValidate;
		public fecherFoundation.FCLabel lblValidateInstruction;
		public fecherFoundation.FCPanel fraGrid;
		public T2KDateBox txtTADate;
		public fecherFoundation.FCGrid vsDemand;
		public fecherFoundation.FCLabel lblTADate;
		public fecherFoundation.FCLabel lblInstructionHeader;
		public fecherFoundation.FCLabel lblInstruction;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTaxAcquiredList));
            this.fraValidate = new fecherFoundation.FCPanel();
            this.lblBillingYear = new fecherFoundation.FCLabel();
            this.cmbBillingYear = new fecherFoundation.FCComboBox();
            this.lblValidateInstruction = new fecherFoundation.FCLabel();
            this.fraGrid = new fecherFoundation.FCPanel();
            this.txtTADate = new Global.T2KDateBox();
            this.vsDemand = new fecherFoundation.FCGrid();
            this.lblTADate = new fecherFoundation.FCLabel();
            this.lblInstructionHeader = new fecherFoundation.FCLabel();
            this.lblInstruction = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.btnProcess = new fecherFoundation.FCButton();
            this.cmdFileSelectAll = new fecherFoundation.FCButton();
            this.cmdFileClear = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraValidate)).BeginInit();
            this.fraValidate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraGrid)).BeginInit();
            this.fraGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTADate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsDemand)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileSelectAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileClear)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 650);
            this.BottomPanel.Size = new System.Drawing.Size(912, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraGrid);
            this.ClientArea.Controls.Add(this.fraValidate);
            this.ClientArea.Size = new System.Drawing.Size(912, 590);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdFileClear);
            this.TopPanel.Controls.Add(this.cmdFileSelectAll);
            this.TopPanel.Size = new System.Drawing.Size(912, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileSelectAll, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileClear, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(157, 30);
            this.HeaderText.Text = "Tax Acquired";
            // 
            // fraValidate
            // 
            this.fraValidate.AppearanceKey = "groupBoxNoBorders";
            this.fraValidate.Controls.Add(this.lblBillingYear);
            this.fraValidate.Controls.Add(this.cmbBillingYear);
            this.fraValidate.Controls.Add(this.lblValidateInstruction);
            this.fraValidate.Location = new System.Drawing.Point(30, 10);
            this.fraValidate.Name = "fraValidate";
            this.fraValidate.Size = new System.Drawing.Size(489, 155);
            this.fraValidate.TabIndex = 1;
            this.fraValidate.Visible = false;
            // 
            // lblBillingYear
            // 
            this.lblBillingYear.AutoSize = true;
            this.lblBillingYear.Location = new System.Drawing.Point(20, 100);
            this.lblBillingYear.Name = "lblBillingYear";
            this.lblBillingYear.Size = new System.Drawing.Size(102, 17);
            this.lblBillingYear.TabIndex = 6;
            this.lblBillingYear.Text = "BILLING YEAR";
            // 
            // cmbBillingYear
            // 
            this.cmbBillingYear.Location = new System.Drawing.Point(149, 88);
            this.cmbBillingYear.Name = "cmbBillingYear";
            this.cmbBillingYear.Size = new System.Drawing.Size(120, 40);
            this.cmbBillingYear.TabIndex = 1;
            // 
            // lblValidateInstruction
            // 
            this.lblValidateInstruction.Location = new System.Drawing.Point(0, 20);
            this.lblValidateInstruction.Name = "lblValidateInstruction";
            this.lblValidateInstruction.Size = new System.Drawing.Size(449, 43);
            this.lblValidateInstruction.TabIndex = 2;
            this.lblValidateInstruction.Visible = false;
            // 
            // fraGrid
            // 
            this.fraGrid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraGrid.AppearanceKey = "groupBoxNoBorders";
            this.fraGrid.Controls.Add(this.txtTADate);
            this.fraGrid.Controls.Add(this.vsDemand);
            this.fraGrid.Controls.Add(this.lblTADate);
            this.fraGrid.Controls.Add(this.lblInstructionHeader);
            this.fraGrid.Controls.Add(this.lblInstruction);
            this.fraGrid.Name = "fraGrid";
            this.fraGrid.Size = new System.Drawing.Size(910, 589);
            this.fraGrid.Visible = false;
            // 
            // txtTADate
            // 
            this.txtTADate.Location = new System.Drawing.Point(597, 68);
            this.txtTADate.Mask = "##/##/####";
            this.txtTADate.Name = "txtTADate";
            this.txtTADate.Size = new System.Drawing.Size(148, 22);
            this.txtTADate.TabIndex = 3;
            // 
            // vsDemand
            // 
            this.vsDemand.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsDemand.Cols = 5;
            this.vsDemand.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.vsDemand.FixedCols = 0;
            this.vsDemand.Location = new System.Drawing.Point(30, 172);
            this.vsDemand.Name = "vsDemand";
            this.vsDemand.RowHeadersVisible = false;
            this.vsDemand.Rows = 1;
            this.vsDemand.Size = new System.Drawing.Size(860, 397);
            this.vsDemand.TabIndex = 4;
            this.vsDemand.CurrentCellChanged += new System.EventHandler(this.vsDemand_RowColChange);
            this.vsDemand.DoubleClick += new System.EventHandler(this.vsDemand_DblClick);
            // 
            // lblTADate
            // 
            this.lblTADate.AutoSize = true;
            this.lblTADate.Location = new System.Drawing.Point(410, 82);
            this.lblTADate.Name = "lblTADate";
            this.lblTADate.Size = new System.Drawing.Size(150, 17);
            this.lblTADate.TabIndex = 2;
            this.lblTADate.Text = "TAX ACQUIRED DATE";
            this.lblTADate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblInstructionHeader
            // 
            this.lblInstructionHeader.Location = new System.Drawing.Point(30, 30);
            this.lblInstructionHeader.Name = "lblInstructionHeader";
            this.lblInstructionHeader.Size = new System.Drawing.Size(585, 18);
            this.lblInstructionHeader.TabIndex = 7;
            this.lblInstructionHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblInstruction
            // 
            this.lblInstruction.Location = new System.Drawing.Point(30, 68);
            this.lblInstruction.Name = "lblInstruction";
            this.lblInstruction.Size = new System.Drawing.Size(360, 94);
            this.lblInstruction.TabIndex = 1;
            this.lblInstruction.Visible = false;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Location = new System.Drawing.Point(396, 30);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcess.Size = new System.Drawing.Size(120, 48);
            this.btnProcess.TabIndex = 1;
            this.btnProcess.Text = "Save";
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // cmdFileSelectAll
            // 
            this.cmdFileSelectAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileSelectAll.Location = new System.Drawing.Point(785, 30);
            this.cmdFileSelectAll.Name = "cmdFileSelectAll";
            this.cmdFileSelectAll.Size = new System.Drawing.Size(87, 24);
            this.cmdFileSelectAll.TabIndex = 52;
            this.cmdFileSelectAll.Text = "Select All";
            this.cmdFileSelectAll.Click += new System.EventHandler(this.cmdFileSelectAll_Click);
            // 
            // cmdFileClear
            // 
            this.cmdFileClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileClear.Location = new System.Drawing.Point(662, 30);
            this.cmdFileClear.Name = "cmdFileClear";
            this.cmdFileClear.Size = new System.Drawing.Size(117, 24);
            this.cmdFileClear.TabIndex = 53;
            this.cmdFileClear.Text = "Clear Selection";
            this.cmdFileClear.Click += new System.EventHandler(this.cmdFileClear_Click);
            // 
            // frmTaxAcquiredList
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(912, 758);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmTaxAcquiredList";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Tax Acquired";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmTaxAcquiredList_Load);
            this.Activated += new System.EventHandler(this.frmTaxAcquiredList_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmTaxAcquiredList_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraValidate)).EndInit();
            this.fraValidate.ResumeLayout(false);
            this.fraValidate.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraGrid)).EndInit();
            this.fraGrid.ResumeLayout(false);
            this.fraGrid.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTADate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsDemand)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileSelectAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileClear)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton btnProcess;
		public FCButton cmdFileSelectAll;
		public FCButton cmdFileClear;
        private FCLabel lblBillingYear;
        private FCComboBox cmbBillingYear;
    }
}
