﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmTaxClub.
	/// </summary>
	public partial class frmTaxClub : BaseForm
	{
		public frmTaxClub()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmTaxClub InstancePtr
		{
			get
			{
				return (frmTaxClub)Sys.GetInstance(typeof(frmTaxClub));
			}
		}

		protected frmTaxClub _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               02/04/2003              *
		// *
		// MODIFIED BY    :               Melissa Lamprecht       *
		// LAST UPDATED   :               07/23/2008              *
		// ********************************************************
		int lngColQuestions;
		int lngColInfo;
		int lngColHidden;
		// Detail
		int lngRowName;
		int lngRowYear;
		int lngRowAgreementDate;
		int lngRowStartDate;
		int lngRowNumberOFPayments;
		int lngRowPaymentAmount;
		int lngRowTotalPayments;
		int lngRowMapLot;
		int lngRowLocation;
		// Summary
		int lngDColRecordID;
		int lngDColName;
		int lngDColYear;
		int lngDColDate;
		string strYear;
		clsDRWrapper rsAccountInfo = new clsDRWrapper();
		clsDRWrapper rsRE = new clsDRWrapper();
		clsDRWrapper rsBill = new clsDRWrapper();
		int lngCurrentLoadedAccount;
		// VBto upgrade warning: lngYear As int	OnWrite(short, string)
		int lngYear;
		bool boolNoValidate;
		bool blnDirty;
		int lngCurrentRecordID;
		// MAL@20080723: Reworked the entire form and process to allow for multiple tax clubs per account
		// Tracker Reference: 14033
		public void Init(int lngAccount = 0)
		{
			string strAcct = "";
			if (lngAccount > 0)
			{
				txtAccount.Text = FCConvert.ToString(lngAccount);
			}
			else
			{
				// GetKeyValues HKEY_CURRENT_USER, REGISTRYKEY & "CL\", "CLPreviousTaxClub", strAcct
				// txtAccount.Text = Val(strAcct)
			}
			this.Show(App.MainForm);
			//FC:FINAL:SBE - #150 - focus textbox on form show
			this.txtAccount.Focus();
			this.txtAccount.SelectAll();
		}

		private void frmTaxClub_Load(object sender, System.EventArgs e)
		{			
			try
			{
				// On Error GoTo ERROR_HANDLER
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
				modGlobalFunctions.SetTRIOColors(this);
				// Setup Summary Grid
				SetupSummaryGrid();
				FormatSummaryGrid();
				// Setup Detail Grid
				SetupDetailGrid();
				FormatGrid();
				// set the tool tips for the textbox and label
				ToolTip1.SetToolTip(txtAccount, "Enter an account here and press enter.");
				ToolTip1.SetToolTip(lblAccount, "Enter an account in the provided box and press enter.");
				strYear = "";
				if (modStatusPayments.Statics.boolRE)
				{
					rsAccountInfo.OpenRecordset("SELECT DISTINCT BillingYear FROM BillingMaster WHERE BillingType = 'RE' ORDER BY BillingYear desc");
				}
				else
				{
					rsAccountInfo.OpenRecordset("SELECT DISTINCT BillingYear FROM BillingMaster WHERE BillingType = 'PP' ORDER BY BillingYear desc");
				}
				while (!rsAccountInfo.EndOfFile())
				{
					strYear += "|";
					strYear += "#" + FCConvert.ToString(rsAccountInfo.Get_Fields_Int32("BillingYear")) + ";" + modExtraModules.FormatYear(FCConvert.ToString(rsAccountInfo.Get_Fields_Int32("BillingYear")));
					rsAccountInfo.MoveNext();
				}
				// DJW@9/23/2013 Added cross apply to get name and address info for accounts
				if (modStatusPayments.Statics.boolRE)
				{
					// rsRE.OpenRecordset "SELECT * FROM Master", strREDatabase
					rsRE.OpenRecordset("SELECT *, DeedName1 as FullNameLF, DeedName2 as SecOwnerFullNameLF FROM Master  WHERE RSCard = 1", modExtraModules.strREDatabase);
					rsAccountInfo.OpenRecordset("SELECT * FROM TaxClub WHERE Type = 'RE'", modExtraModules.strCLDatabase);
					rsBill.OpenRecordset("SELECT BillingYear, Account, ID FROM BillingMaster WHERE BillingType = 'RE'");
				}
				else
				{
					// rsRE.OpenRecordset "SELECT * FROM PPMaster", strPPDatabase
					rsRE.OpenRecordset("SELECT c.*, p.FullNameLF FROM PPMaster as c CROSS APPLY " + rsRE.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(PartyID,NULL,'PP',Account) as p", modExtraModules.strPPDatabase);
					rsAccountInfo.OpenRecordset("SELECT * FROM TaxClub WHERE Type = 'PP'", modExtraModules.strCLDatabase);
					rsBill.OpenRecordset("SELECT BillingYear, Account, ID FROM BillingMaster WHERE BillingType = 'PP'");
				}
				if (modStatusPayments.Statics.boolRE)
				{
				}
				else
				{
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error On Load");
			}
		}

		private void frmTaxClub_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// Catches the escape and enter keys
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				Close();
			}
			else if (KeyAscii == 13)
			{
				KeyAscii = 0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}
		// VBto upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			// MAL@20070910: Check for 'dirty' status before exiting form ; this catches the 'X'
			if (blnDirty)
			{
				DialogResult result = FCMessageBox.Show("Changes have been made.  Would you like to save the information?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Save Changes");
				if (result == DialogResult.Yes)
				{
					SaveCurrentInformation();
				}
				else if (result == DialogResult.No)
				{
					//FC:FINAL:AM: don't close the form again
					//Close();
				}
				else if (result == DialogResult.Cancel)
				{
					e.Cancel = true;
				}
			}
			//FC:FINAL:AM: don't close the form again
			//else
			//{
			//	Close();
			//}
		}

		//FC:FINAL:AM:#64 - no need for resize
		//private void frmTaxClub_Resize(object sender, System.EventArgs e)
		//{
		//	ResizeGrid();
		//}
		private void mnuFileDelete_Click(object sender, System.EventArgs e)
		{
			DeleteCurrentTaxClub();
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void ShowFrame(GroupBox fraFrame)
		{
			// this will place the frame in the middle of the form
			fraFrame.Top = FCConvert.ToInt32((this.Height - fraFrame.Height) / 3.0);
			fraFrame.Left = FCConvert.ToInt32((this.Width - fraFrame.Width) / 2.0);
			fraFrame.Visible = true;
		}

		private void FormatGrid()
		{
			vsTaxClub.Cols = 3;
			vsTaxClub.Rows = 9;
			vsTaxClub.ColWidth(lngColQuestions, FCConvert.ToInt32(vsTaxClub.WidthOriginal * 0.5));
			vsTaxClub.ColWidth(lngColInfo, FCConvert.ToInt32(vsTaxClub.WidthOriginal * 0.45));
			vsTaxClub.ColWidth(lngColHidden, 0);
			vsTaxClub.ExtendLastCol = true;
			vsTaxClub.ColAlignment(lngColQuestions, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsTaxClub.ColAlignment(lngColInfo, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			// Name
			vsTaxClub.TextMatrix(lngRowName, lngColQuestions, "Name");
			// Year
			vsTaxClub.TextMatrix(lngRowYear, lngColQuestions, "Year");
			// Agreement Date
			vsTaxClub.TextMatrix(lngRowAgreementDate, lngColQuestions, "Agreement Date");
			// Start Date
			vsTaxClub.TextMatrix(lngRowStartDate, lngColQuestions, "Starting Date");
			// # of Payments (Max 12)
			vsTaxClub.TextMatrix(lngRowNumberOFPayments, lngColQuestions, "Number of Payments");
			// Payment Amount
			vsTaxClub.TextMatrix(lngRowPaymentAmount, lngColQuestions, "Payment Amount");
			// Total of Payments (Calculated)
			vsTaxClub.TextMatrix(lngRowTotalPayments, lngColQuestions, "Total amount of all payments.");
			vsTaxClub.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowTotalPayments, lngColInfo, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			vsTaxClub.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRowTotalPayments, lngColInfo, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT);
			// Map Lot
			vsTaxClub.TextMatrix(lngRowMapLot, lngColQuestions, "Map Lot");
			vsTaxClub.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowMapLot, lngColInfo, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			vsTaxClub.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRowMapLot, lngColInfo, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT);
			// Location
			vsTaxClub.TextMatrix(lngRowLocation, lngColQuestions, "Location");
			vsTaxClub.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowLocation, lngColInfo, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			vsTaxClub.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRowLocation, lngColInfo, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT);
			//vsTaxClub.HeightOriginal = (vsTaxClub.Rows * vsTaxClub.RowHeight(0)) + 70;
		}

		private void ResizeGrid()
		{
			vsTaxClub.ColWidth(lngColQuestions, FCConvert.ToInt32(vsTaxClub.WidthOriginal * 0.5));
			vsTaxClub.ColWidth(lngColInfo, FCConvert.ToInt32(vsTaxClub.WidthOriginal * 0.45));
			vsTaxClub.ColWidth(lngColHidden, 0);
			//vsTaxClub.HeightOriginal = (vsTaxClub.Rows * vsTaxClub.RowHeight(0)) + 70;
		}

		private void mnuFileNew_Click(object sender, System.EventArgs e)
		{
			AddNewTaxClub();
		}

		private void mnuFilePrintBooklet_Click(object sender, System.EventArgs e)
		{
			// MAL@20080925: Add support for printing from the summary grid
			// Tracker Reference: 15485
			if (vsTaxClub.Visible && vsTaxClub.Rows > 1)
			{
				frmTaxClubBooklet.InstancePtr.Init(FCConvert.ToInt32(rsAccountInfo.Get_Fields_Int32("ID")));
				Close();
			}
			else if (vsTaxClubSummary.Visible && vsTaxClubSummary.TextMatrix(vsTaxClubSummary.Row, lngDColYear) != "")
			{
				frmTaxClubBooklet.InstancePtr.Init(FCConvert.ToInt32(vsTaxClubSummary.TextMatrix(vsTaxClubSummary.Row, lngDColRecordID)));
			}
		}

		private void mnuFilePrintMaster_Click(object sender, System.EventArgs e)
		{
			if (vsTaxClub.Visible && vsTaxClub.Rows > 1)
			{
				// this will create the master activity report for this tax club
				if (Conversion.Val(txtAccount.Text) != 0)
				{
					rptTaxClubMaster.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(txtAccount.Text)), FCConvert.ToInt16(false));
				}
			}
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			if (SaveCurrentInformation())
			{
				blnDirty = false;
				//#120 KS Overlapped items. In VB6 is only visible one item.   
				fraTaxClubDetails.Visible = false;
				GetAccountInformation_Summary();
			}
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveCurrentInformation())
			{
				blnDirty = false;
				Close();
				App.MainForm.Focus();
			}
		}

		private void txtAccount_Enter(object sender, System.EventArgs e)
		{
			txtAccount.SelectionStart = 0;
			txtAccount.SelectionLength = txtAccount.Text.Length;
		}

		private void txtAccount_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
            //FC:FINAL:SBE - #4605 - Validate event is triggered anyway. We don't have to trigger it manually.
			//Keys KeyCode = e.KeyCode;
			//int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			//if (KeyCode == Keys.Return)
			//{
			//	txtAccount_Validate(false);
			//	blnDirty = false;
			//	// MAL@20070910: Reset status when account changes
			//}
		}

		private void txtAccount_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
            //FC:FINAL:AM:#2078 - validate only if the text has been modified
            if (this.txtAccount.Modified)
            {
                if (GetAccountInformation_Summary())
                {
                    e.Cancel = false;
                    lngCurrentLoadedAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(txtAccount.Text)));
                    if (vsTaxClubSummary.Visible)
                    {
                        //RPU: Not just make focus on vsTaxClubSummary, also make it enabled and make fraTaxClubDetails hidden in order to avoid overlapping
                        //vsTaxClubSummary.Focus();
                        vsTaxClubSummary.Focus();
                        vsTaxClubSummary.Enabled = true;
                        fraTaxClubDetails.Visible = false;
                        btnProcess.Visible = true;
                    }
                    else if (vsTaxClub.Visible)
                    {
                        //RPU: Not just make focus on vsTaxClub also make fraTaxClubSummary hidden in order to avoid overlapping
                        //vsTaxClub.Focus();
                        vsTaxClub.Focus();
                        vsTaxClub.Visible = true;
                        fraTaxClubDetails.Visible = true;
                        fraTaxClubSummary.Visible = false;
                        lblTaxClubSummary.Visible = false;
                        btnProcess.Visible = true;
                    }
                }
                else
                {
                    txtAccount.Text = FCConvert.ToString(lngCurrentLoadedAccount);
                    e.Cancel = true;
                }
            }
		}

		public void txtAccount_Validate(bool Cancel)
		{
			txtAccount_Validating(txtAccount, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private bool GetAccountInformation_Summary()
		{
			bool GetAccountInformation_Summary = false;
			DialogResult intAnswer = 0;
			int intCount = 0;
			int lngRows;
			int lngCurRow/*unused?*/;
			// VBto upgrade warning: lngAcct As int	OnWrite(string)
			int lngAcct = 0;
			string strRecordID = "";
			string strName = "";
			string strDYear = "";
			string strDate = "";
			ResetSummaryGrid();
			lngRows = vsTaxClubSummary.Rows;
			if (!String.IsNullOrEmpty(txtAccount.Text))
			{
				lngAcct = FCConvert.ToInt32(txtAccount.Text);
			}
			rsAccountInfo.MoveFirst();
			if (modStatusPayments.Statics.boolRE)
			{
				rsRE.FindFirstRecord("RSAccount", lngAcct);
			}
			else
			{
				rsRE.FindFirstRecord("Account", lngAcct);
			}
			rsAccountInfo.FindFirstRecord("Account", lngAcct);
			if (!rsAccountInfo.NoMatch)
			{
				while (!rsAccountInfo.NoMatch)
				{
					strRecordID = FCConvert.ToString(rsAccountInfo.Get_Fields_Int32("ID"));
					if (modStatusPayments.Statics.boolRE)
					{
						strName = Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("FullNameLF")) + " ");
					}
					else
					{
						strName = Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("Name")) + " ");
					}
					// Year
					// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
					if (Conversion.Val(rsAccountInfo.Get_Fields("Year")) > 0)
					{
						// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
						strDYear = modExtraModules.FormatYear(FCConvert.ToString(rsAccountInfo.Get_Fields("Year")));
						// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
						lngYear = FCConvert.ToInt32(rsAccountInfo.Get_Fields("Year"));
					}
					else
					{
						strDYear = "";
						lngYear = 0;
					}
					// Agreement Date
					strDate = Strings.Format(rsAccountInfo.Get_Fields_DateTime("AgreementDate"), "MM/dd/yyyy");
					vsTaxClubSummary.AddItem(strRecordID + "\t" + strName + "\t" + strDYear + "\t" + strDate);
					intCount += 1;
					rsAccountInfo.FindNextRecord("Account", lngAcct);
				}
			}
			if (intCount > 0)
			{
				GetAccountInformation_Summary = true;
				fraTaxClubSummary.Visible = true;
				lblTaxClubSummary.Visible = true;
				lngCurrentRecordID = 0;
				ResizeSummaryGrid();
			}
			else
			{
				GetAccountInformation_Summary = false;
				intAnswer = FCMessageBox.Show("There are no tax clubs created for this account. " + "\r\n" + "Would you like to create one?", MsgBoxStyle.YesNo | MsgBoxStyle.Question, "No Records");
				if (intAnswer == DialogResult.Yes)
				{
					AddNewTaxClub();
					GetAccountInformation_Summary = true;
					fraTaxClubSummary.Visible = false;
					lblTaxClubSummary.Visible = false;
					fraTaxClubDetails.Visible = true;
					fraTaxClubDetails.Enabled = true;
				}
				else
				{
					GetAccountInformation_Summary = false;
					fraTaxClubSummary.Visible = false;
					lblTaxClubSummary.Visible = false;
					fraTaxClubDetails.Visible = false;
					lngCurrentRecordID = 0;
				}
			}
			boolNoValidate = true;
			return GetAccountInformation_Summary;
		}

		private void AddNewTaxClub()
		{
			int lngID = 0;
			int lngAcct = 0;
			if (Conversion.Val(txtAccount.Text) == 0)
			{
				lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(Interaction.InputBox("Please enter the new account number", "New Tax Club"))));
				if (lngAcct > 0)
				{
					// the user entered a valid account number
					txtAccount.Text = FCConvert.ToString(lngAcct);
					boolNoValidate = true;
					btnProcess.Visible = true;
				}
				else
				{
					// no valid data or canceled
					return;
				}
			}
			else
			{
				lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(txtAccount.Text)));
			}
			lngYear = 0;
			if (modStatusPayments.Statics.boolRE)
			{
				rsRE.FindFirstRecord("RSAccount", lngAcct);
			}
			else
			{
				rsRE.FindFirstRecord("Account", lngAcct);
			}
			if (!rsRE.EndOfFile())
			{
				rsAccountInfo.AddNew();
				if (modStatusPayments.Statics.boolRE)
				{
					rsAccountInfo.Set_Fields("Name", rsRE.Get_Fields_String("FullNameLF"));
					rsAccountInfo.Set_Fields("Account", lngAcct);
					rsAccountInfo.Set_Fields("Year", 0);
					// 0 for now, must be entered by the user
					lngYear = 0;
					rsAccountInfo.Set_Fields("AgreementDate", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
					rsAccountInfo.Set_Fields("StartDate", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
					rsAccountInfo.Set_Fields("PaymentAmount", 0);
					rsAccountInfo.Set_Fields("NumberOfPayments", 0);
					rsAccountInfo.Set_Fields("TotalPayments", 0);
					rsAccountInfo.Set_Fields("MapLot", FCConvert.ToString(rsRE.Get_Fields_String("RSMapLot")) + " ");
					rsAccountInfo.Set_Fields("Location", rsRE.Get_Fields_String("RSLOCNUMALPH"));
					rsAccountInfo.Set_Fields("Location", Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("Location")) + " " + FCConvert.ToString(rsRE.Get_Fields_String("RSLOCAPT"))));
					rsAccountInfo.Set_Fields("Location", Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("Location")) + " " + FCConvert.ToString(rsRE.Get_Fields_String("RSLOCSTREET"))) + " ");
					rsAccountInfo.Set_Fields("Type", "RE");
				}
				else
				{
					rsAccountInfo.Set_Fields("Name", rsRE.Get_Fields_String("FullNameLF"));
					rsAccountInfo.Set_Fields("Account", lngAcct);
					rsAccountInfo.Set_Fields("Year", 0);
					// 0 for now, must be entered by the user
					lngYear = 0;
					rsAccountInfo.Set_Fields("AgreementDate", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
					rsAccountInfo.Set_Fields("StartDate", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
					rsAccountInfo.Set_Fields("PaymentAmount", 0);
					rsAccountInfo.Set_Fields("NumberOfPayments", 0);
					rsAccountInfo.Set_Fields("TotalPayments", 0);
					// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
					rsAccountInfo.Set_Fields("Location", FCConvert.ToString(rsRE.Get_Fields("StreetNumber")) + " " + FCConvert.ToString(rsRE.Get_Fields_String("Street")));
					rsAccountInfo.Set_Fields("Type", "PP");
				}
				rsAccountInfo.Update();
				rsAccountInfo.FindFirstRecord2("Account,Year", FCConvert.ToString(lngAcct) + ",0", ",");
				lngID = FCConvert.ToInt32(rsAccountInfo.Get_Fields_Int32("ID"));
				// MAL@20080923: Add refresh to make sure that the information is refreshed
				// Tracker Reference: 15420
				lngCurrentLoadedAccount = lngAcct;
				if (GetAccountInformation_Detail(lngID))
				{
					fraTaxClubDetails.Visible = true;
					fraTaxClubDetails.Enabled = true;
					fraTaxClubSummary.Visible = false;
					lblTaxClubSummary.Visible = false;
					mnuFilePrint.Enabled = true;
					cmdFileDelete.Enabled = true;
					cmbFreq.Enabled = true;
					cmbFreq.Visible = true;
				}
				else
				{
					FCMessageBox.Show("Unable to load the new record!", MsgBoxStyle.OkOnly | MsgBoxStyle.Information, "No Record");
				}
				rsAccountInfo.OpenRecordset(rsAccountInfo.Name(), modExtraModules.strCLDatabase);
			}
			else
			{
				// no real estate account is found
				FCMessageBox.Show("Account " + FCConvert.ToString(lngAcct) + " was not found.");
				fraTaxClubDetails.Enabled = false;
			}
			boolNoValidate = true;
		}

		private bool GetAccountInformation()
		{
			bool GetAccountInformationRet = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will return true if the information was found or created
				int lngAccount = 0;
				bool boolNewTaxClub;
				DialogResult intAnswer;
				GetAccountInformationRet = true;
				boolNewTaxClub = false;
				if (boolNoValidate)
				{
					return GetAccountInformationRet;
				}
				if (Conversion.Val(txtAccount.Text) == 0)
				{
					lngAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(Interaction.InputBox("Please enter the new account number", "New Tax Club"))));
					if (lngAccount > 0)
					{
						// the user entered a valid account number
						txtAccount.Text = FCConvert.ToString(lngAccount);
						boolNewTaxClub = true;
					}
					else
					{
						// no valid data or canceled
						GetAccountInformationRet = false;
						return GetAccountInformationRet;
					}
				}
				else
				{
					lngAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(txtAccount.Text)));
				}
				if (!boolNoValidate)
				{
					boolNoValidate = true;
					fraTaxClubSummary.Enabled = false;
					boolNoValidate = false;
				}
				rsAccountInfo.FindFirstRecord("Account", lngAccount);
				if (rsAccountInfo.NoMatch)
				{
					// if there is no match
					if (modStatusPayments.Statics.boolRE)
					{
						rsRE.FindFirstRecord("RSAccount", lngAccount);
					}
					else
					{
						rsRE.FindFirstRecord("Account", lngAccount);
					}
					if (!rsRE.EndOfFile())
					{
						if (boolNewTaxClub)
						{
							intAnswer = DialogResult.Yes;
						}
						else
						{
							intAnswer = FCMessageBox.Show("There is no tax club for account " + FCConvert.ToString(lngAccount) + ".  Would you like to create one?", MsgBoxStyle.Question | MsgBoxStyle.YesNoCancel, "Account Not Setup");
						}
						if (intAnswer == DialogResult.Yes)
						{
							rsAccountInfo.AddNew();
							if (modStatusPayments.Statics.boolRE)
							{
								rsAccountInfo.Set_Fields("Name", rsRE.Get_Fields_String("FullNameLF"));
								rsAccountInfo.Set_Fields("Account", lngAccount);
								rsAccountInfo.Set_Fields("Year", 0);
								// 0 for now, must be entered by the user
								lngYear = 0;
								rsAccountInfo.Set_Fields("AgreementDate", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
								rsAccountInfo.Set_Fields("StartDate", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
								rsAccountInfo.Set_Fields("PaymentAmount", 0);
								rsAccountInfo.Set_Fields("NumberOfPayments", 0);
								rsAccountInfo.Set_Fields("TotalPayments", 0);
								rsAccountInfo.Set_Fields("MapLot", FCConvert.ToString(rsRE.Get_Fields_String("RSMapLot")) + " ");
								rsAccountInfo.Set_Fields("Location", rsRE.Get_Fields_String("RSLOCNUMALPH"));
								rsAccountInfo.Set_Fields("Location", Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("Location")) + " " + FCConvert.ToString(rsRE.Get_Fields_String("RSLOCAPT"))));
								rsAccountInfo.Set_Fields("Location", Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("Location")) + " " + FCConvert.ToString(rsRE.Get_Fields_String("RSLOCSTREET"))) + " ");
								rsAccountInfo.Set_Fields("Type", "RE");
							}
							else
							{
								rsAccountInfo.Set_Fields("Name", rsRE.Get_Fields_String("FullNameLF"));
								rsAccountInfo.Set_Fields("Account", lngAccount);
								rsAccountInfo.Set_Fields("Year", 0);
								// 0 for now, must be entered by the user
								lngYear = 0;
								rsAccountInfo.Set_Fields("AgreementDate", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
								rsAccountInfo.Set_Fields("StartDate", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
								rsAccountInfo.Set_Fields("PaymentAmount", 0);
								rsAccountInfo.Set_Fields("NumberOfPayments", 0);
								rsAccountInfo.Set_Fields("TotalPayments", 0);
								// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
								rsAccountInfo.Set_Fields("Location", FCConvert.ToString(rsRE.Get_Fields("StreetNumber")) + " " + FCConvert.ToString(rsRE.Get_Fields_String("Street")));
								rsAccountInfo.Set_Fields("Type", "PP");
							}
							rsAccountInfo.Update();
							GetAccountInformation();
							// MAL@20070910: Added second call to function to reload the grid
							rsAccountInfo.OpenRecordset(rsAccountInfo.Name(), modExtraModules.strCLDatabase);
							RETRYFINDINGACCOUNT:
							;
							rsAccountInfo.FindFirstRecord("Account", lngAccount);
							if (rsAccountInfo.NoMatch)
							{
								DialogResult messageBoxResult = FCMessageBox.Show("Could not find account.  Would you like to retry?", MsgBoxStyle.Question | MsgBoxStyle.YesNoCancel, "Searching");
								if (messageBoxResult == DialogResult.Yes)
								{
									goto RETRYFINDINGACCOUNT;
								}
								else if (messageBoxResult == DialogResult.Cancel)
								{
									GetAccountInformationRet = false;
									return GetAccountInformationRet;
								}
							}
							else
							{
								App.DoEvents();
								// this should give the database enough time to reload the recordset
								GetAccountInformationRet = GetAccountInformationRet;
								App.DoEvents();
							}
						}
						else if ((intAnswer == DialogResult.No) || (intAnswer == DialogResult.Cancel))
						{
							// set the focus back to the textbox and highlight the text inside
							fraTaxClubDetails.Enabled = false;
							GetAccountInformationRet = false;
							return GetAccountInformationRet;
							// If txtAccount.Visible And txtAccount.Enabled Then
							// txtAccount.SetFocus
							// End If
							// txtAccount.SelStart = 1
							// txtAccount.SelLength = Len(txtAccount.Text)
						}
					}
					else
					{
						// no real estate account is found
						FCMessageBox.Show("Account " + FCConvert.ToString(lngAccount) + " was not found.");
						fraTaxClubDetails.Enabled = false;
						GetAccountInformationRet = false;
						return GetAccountInformationRet;
					}
				}
				else
				{
					// there is a match, so allow the user to edit the grid
					// Name
					// MAL@20080130: Get Name from Correct Place
					// Tracker Reference: 12143
					if (modStatusPayments.Statics.boolRE)
					{
						vsTaxClub.TextMatrix(lngRowName, lngColInfo, Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("FullNameLF")) + " "));
					}
					else
					{
						vsTaxClub.TextMatrix(lngRowName, lngColInfo, Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("FullNameLF")) + " "));
					}
					// Year
					// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
					if (Conversion.Val(rsAccountInfo.Get_Fields("Year")) > 0)
					{
						// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
						vsTaxClub.TextMatrix(lngRowYear, lngColInfo, modExtraModules.FormatYear(FCConvert.ToString(rsAccountInfo.Get_Fields("Year"))));
						// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
						lngYear = FCConvert.ToInt32(rsAccountInfo.Get_Fields("Year"));
					}
					else
					{
						vsTaxClub.TextMatrix(lngRowYear, lngColInfo, "");
						lngYear = 0;
					}
					// Agreement Date
					vsTaxClub.TextMatrix(lngRowAgreementDate, lngColInfo, Strings.Format(rsAccountInfo.Get_Fields_DateTime("AgreementDate"), "MM/dd/yyyy"));
					// Start Date
					vsTaxClub.TextMatrix(lngRowStartDate, lngColInfo, Strings.Format(rsAccountInfo.Get_Fields_DateTime("StartDate"), "MM/dd/yyyy"));
					// # of Payments (Max 12)
					vsTaxClub.TextMatrix(lngRowNumberOFPayments, lngColInfo, FCConvert.ToString(rsAccountInfo.Get_Fields_Int32("NumberOfPayments")));
					// Payment Amount
					vsTaxClub.TextMatrix(lngRowPaymentAmount, lngColInfo, Strings.Format(rsAccountInfo.Get_Fields_Double("PaymentAmount"), "#,##0.00"));
					// Total of Payments (Calculated)
					vsTaxClub.TextMatrix(lngRowTotalPayments, lngColInfo, Strings.Format(rsAccountInfo.Get_Fields_Double("TotalPayments"), "#,##0.00"));
					// MAL@20071217: Change to always grab the information from Real Estate
					// Tracker Reference: 11665
					// Map Lot
					// vsTaxClub.TextMatrix(lngRowMapLot, lngColInfo) = Trim(.Get_Fields("MapLot") & " ")
					// Location
					// vsTaxClub.TextMatrix(lngRowLocation, lngColInfo) = Trim(.Get_Fields("Location") & " ")
					if (modStatusPayments.Statics.boolRE)
					{
						rsRE.FindFirstRecord("RSAccount", lngAccount);
					}
					else
					{
						rsRE.FindFirstRecord("Account", lngAccount);
					}
					if (rsRE.RecordCount() > 0 && modStatusPayments.Statics.boolRE)
					{
						// Map/Lot
						vsTaxClub.TextMatrix(lngRowMapLot, lngColInfo, FCConvert.ToString(rsRE.Get_Fields_String("RSMapLot")) + " ");
						// Location
						vsTaxClub.TextMatrix(lngRowLocation, lngColInfo, Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("RSLOCNUMALPH")) + " " + FCConvert.ToString(rsRE.Get_Fields_String("RSLOCAPT")) + " " + FCConvert.ToString(rsRE.Get_Fields_String("RSLOCSTREET"))) + " ");
					}
					else
					{
						vsTaxClub.TextMatrix(lngRowMapLot, lngColInfo, "");
						vsTaxClub.TextMatrix(lngRowLocation, lngColInfo, "");
					}
					// set the frequency
					cmbFreq.SelectedIndex = rsAccountInfo.Get_Fields_Int16("Freq");
				}
				if (GetAccountInformationRet)
				{
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "CLPreviousTaxClub", modGlobal.PadToString(FCConvert.ToInt32(Conversion.Val(txtAccount.Text)), 8));
				}
				fraTaxClubDetails.Enabled = true;
				fraTaxClubDetails.Visible = true;
				fraTaxClubSummary.Visible = false;
				lblTaxClubSummary.Visible = false;
				mnuFilePrint.Enabled = true;
				cmdFileDelete.Enabled = true;
				cmbFreq.Enabled = true;
				cmbFreq.Visible = true;
				return GetAccountInformationRet;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Retrieving Account");
			}
			return GetAccountInformationRet;
		}

		private void vsTaxClub_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			vsTaxClub.EditMask = "";
			vsTaxClub.ComboList = "";
			if (vsTaxClub.Col == lngColInfo)
			{
				// if this is the year field
				if (vsTaxClub.Row == lngRowYear)
				{
					vsTaxClub.ComboList = strYear;
					vsTaxClub.EditMask = "####-#";
				}
				else if (vsTaxClub.Row == lngRowAgreementDate || vsTaxClub.Row == lngRowStartDate)
				{
					vsTaxClub.EditMask = "##/##/####";
				}
			}
		}

		private void vsTaxClub_GotFocus()
		{
			vsTaxClub.Select(0, lngColInfo);
		}

		private void vsTaxClub_KeyPressEdit(int Row, int Col, ref Keys KeyAscii)
		{
			if (Row == lngRowAgreementDate || Row == lngRowNumberOFPayments || Row == lngRowPaymentAmount || Row == lngRowStartDate || Row == lngRowYear)
			{
				// only allow numeric characters in these fields
				if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back) || (KeyAscii == Keys.Return) || (FCConvert.ToInt32(KeyAscii) == 46))
				{
					// let these go
					if (FCConvert.ToInt32(KeyAscii) == 46)
					{
						if (Row == lngRowPaymentAmount)
						{
							// let this go
						}
						else
						{
							KeyAscii = 0;
						}
					}
					else
					{
					}
				}
				else if ((KeyAscii == Keys.Divide) || (FCConvert.ToInt32(KeyAscii) == 47))
				{
					if (Row == lngRowStartDate || Row == lngRowAgreementDate)
					{
						// let the slash go
					}
					else
					{
						KeyAscii = 0;
					}
				}
				else if (FCConvert.ToInt32(KeyAscii) == 27)
				{
					Close();
				}
				else
				{
					KeyAscii = 0;
				}
			}
			else
			{
			}
			blnDirty = true;
		}

		private void vsTaxClub_RowColChange(object sender, EventArgs e)
		{
			// make sure that the user does not change any of the calculated fields
			if (vsTaxClub.Col == lngColInfo)
			{
				if (vsTaxClub.Row == lngRowName)
				{
					// Name
					vsTaxClub.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsTaxClub.EditCell();
				}
				else if (vsTaxClub.Row == lngRowYear)
				{
					// Year
					vsTaxClub.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsTaxClub.EditCell();
				}
				else if (vsTaxClub.Row == lngRowAgreementDate)
				{
					// Agreement Date
					vsTaxClub.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsTaxClub.EditCell();
				}
				else if (vsTaxClub.Row == lngRowStartDate)
				{
					// Start Date
					vsTaxClub.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsTaxClub.EditCell();
				}
				else if (vsTaxClub.Row == lngRowNumberOFPayments)
				{
					// # of Payments (Max 12)
					vsTaxClub.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsTaxClub.EditCell();
				}
				else if (vsTaxClub.Row == lngRowPaymentAmount)
				{
					// Payment Amount
					vsTaxClub.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsTaxClub.EditCell();
				}
				else if (vsTaxClub.Row == lngRowTotalPayments)
				{
					// Total of Payments (Calculated)
					vsTaxClub.Editable = FCGrid.EditableSettings.flexEDNone;
				}
				else if (vsTaxClub.Row == lngRowMapLot)
				{
					// Map Lot
					vsTaxClub.Editable = FCGrid.EditableSettings.flexEDNone;
				}
				else if (vsTaxClub.Row == lngRowLocation)
				{
					// Location
					vsTaxClub.Editable = FCGrid.EditableSettings.flexEDNone;
				}
				else
				{
					vsTaxClub.Editable = FCGrid.EditableSettings.flexEDNone;
				}
			}
			if (vsTaxClub.Col == vsTaxClub.Cols - 1 && vsTaxClub.Row == vsTaxClub.Rows - 1)
			{
				vsTaxClub.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				vsTaxClub.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
			//FC:FINAL:AM:#64 - don't send validate again
			//if (blnDirty && vsTaxClub.CurrentCell.IsInEditMode)
			//{
			//	//send null event instead of (DataGridViewCellValidating)EventArgs.Empty to avoid invalid cast exception
			//	vsTaxClub_ValidateEdit(vsTaxClub, null);
			//}
		}

		private void vsTaxClub_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (vsTaxClub.CurrentCell.IsInEditMode)
			{
				//FC:FINAL:MSH - save and use correct indexes of the cell
				int row = vsTaxClub.GetFlexRowIndex(e.RowIndex);
				int col = vsTaxClub.GetFlexColIndex(e.ColumnIndex);
				// MAL@20070910: Added track for dirty status to use to prompt for save
				if (row == lngRowNumberOFPayments)
				{
					// check to make sure that there is a number entered in the grid
					if (Strings.Trim(vsTaxClub.EditText) == "")
					{
						FCMessageBox.Show("Please enter an amount in this field.", MsgBoxStyle.Information, "Invalid Data");
						e.Cancel = true;
						return;
					}
					else
					{
						blnDirty = true;
					}
					if (cmbFreq.SelectedIndex == 0)
					{
						// make sure that the number of payments is not greater than 12 months
						if (Conversion.Val(vsTaxClub.EditText) > 12)
						{
							FCMessageBox.Show("Please enter a value of 12 months or less in this field.", MsgBoxStyle.Information, "Invalid Data");
							e.Cancel = true;
							return;
						}
						else
						{
							blnDirty = true;
						}
					}
					else if (cmbFreq.SelectedIndex == 1)
					{
						// make sure that the number of payments is not greater than 52 weeks
						if (Conversion.Val(vsTaxClub.EditText) > 52)
						{
							FCMessageBox.Show("Please enter a value of 52 weeks or less in this field.", MsgBoxStyle.Information, "Invalid Data");
							e.Cancel = true;
							return;
						}
						else
						{
							blnDirty = true;
						}
					}
					else if (cmbFreq.SelectedIndex == 2)
					{
						// make sure that the number of payments is not greater than 26 bi-weekly periods
						if (Conversion.Val(vsTaxClub.EditText) > 26)
						{
							FCMessageBox.Show("Please enter a value of 26 bi-weekly periods or less in this field.", MsgBoxStyle.Information, "Invalid Data");
							e.Cancel = true;
							return;
						}
						else
						{
							blnDirty = true;
						}
					}
					// format this row first
					vsTaxClub.TextMatrix(row, lngColInfo, Strings.Format(vsTaxClub.EditText, "#,##0"));
					vsTaxClub.EditText = Strings.Format(vsTaxClub.EditText, "#,##0");
					if (FCConvert.ToDouble(vsTaxClub.TextMatrix(lngRowPaymentAmount, lngColInfo)) != 0)
					{
						// this will calculate the total amount of payments
						vsTaxClub.TextMatrix(lngRowTotalPayments, lngColInfo, Strings.Format(FCConvert.ToDouble(vsTaxClub.TextMatrix(lngRowNumberOFPayments, lngColInfo)) * FCConvert.ToDouble(vsTaxClub.TextMatrix(lngRowPaymentAmount, lngColInfo)), "#,##0.00"));
					}
					else if (FCConvert.ToDouble(vsTaxClub.TextMatrix(lngRowTotalPayments, lngColInfo)) != 0 && Conversion.Val(vsTaxClub.TextMatrix(lngRowNumberOFPayments, lngColInfo)) != 0)
					{
						vsTaxClub.TextMatrix(lngRowPaymentAmount, lngColInfo, Strings.Format(modGlobal.Round(FCConvert.ToDouble(vsTaxClub.TextMatrix(lngRowTotalPayments, lngColInfo)) / FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(vsTaxClub.TextMatrix(lngRowNumberOFPayments, lngColInfo)))), 2), "#,##0.00"));
					}
					else
					{
						// do nothing
					}
				}
				else if (row == lngRowPaymentAmount)
				{
					// check to make sure that there is a number entered in the grid
					if (Strings.Trim(vsTaxClub.EditText) == "")
					{
						FCMessageBox.Show("Please enter an amount in this field.", MsgBoxStyle.Information, "Invalid Data");
						e.Cancel = true;
						return;
					}
					else
					{
						blnDirty = true;
					}
					// format this row first
					vsTaxClub.TextMatrix(row, lngColInfo, Strings.Format(vsTaxClub.EditText, "#,##0.00"));
					vsTaxClub.EditText = Strings.Format(vsTaxClub.EditText, "#,##0.00");
					// this will calculate the total amount of payments
					vsTaxClub.TextMatrix(lngRowTotalPayments, lngColInfo, Strings.Format(FCConvert.ToDouble(vsTaxClub.TextMatrix(lngRowNumberOFPayments, lngColInfo)) * FCConvert.ToDouble(vsTaxClub.TextMatrix(lngRowPaymentAmount, lngColInfo)), "#,##0.00"));
				}
				else if (row == lngRowAgreementDate || row == lngRowStartDate)
				{
					if (Strings.Trim(vsTaxClub.EditText) != "")
					{
						if (Information.IsDate(vsTaxClub.EditText) == false)
						{
							FCMessageBox.Show("Please enter a valid date in this field.", MsgBoxStyle.Information, "Invalid Data");
							e.Cancel = true;
							return;
						}
						else
						{
							blnDirty = true;
						}
					}
					else
					{
						FCMessageBox.Show("Please enter a valid date in this field.", MsgBoxStyle.Information, "Invalid Data");
						e.Cancel = true;
						return;
					}
					// If Trim(.TextMatrix(Row, lngColInfo)) <> "" Then
					// If IsDate(.TextMatrix(Row, lngColInfo)) = False Then
					// MsgBox "Please enter a valid date in this field.", MsgBoxStyle.Information, "Invalid Data"
					// Cancel = True
					// Exit Sub
					// End If
					// Else
					// MsgBox "Please enter a valid date in this field.", MsgBoxStyle.Information, "Invalid Data"
					// Cancel = True
					// Exit Sub
					// End If
				}
				else if (row == lngRowYear)
				{
					if (Strings.Trim(vsTaxClub.EditText) == "")
					{
						// If Val(.TextMatrix(Row, lngColInfo)) < 1980 Or Val(.TextMatrix(Row, lngColInfo)) > 3000 Then
						FCMessageBox.Show("Please enter a valid year in this field.", MsgBoxStyle.Information, "Invalid Data");
						e.Cancel = true;
						return;
					}
					else if (Strings.InStr(1, vsTaxClub.EditText, "_") != 0)
					{
						FCMessageBox.Show("Please enter a valid year in this field.", MsgBoxStyle.Information, "Invalid Data");
						e.Cancel = true;
						return;
					}
					else if (Conversion.Val(modExtraModules.FormatYear(vsTaxClub.EditText)) < 19801 || Conversion.Val(modExtraModules.FormatYear(vsTaxClub.EditText)) > 30009)
					{
						FCMessageBox.Show("Please enter a valid year in this field.", MsgBoxStyle.Information, "Invalid Data");
						e.Cancel = true;
						return;
					}
					else if (Conversion.Val(Strings.Right(vsTaxClub.EditText, 1)) == 0)
					{
						FCMessageBox.Show("Please enter a valid year in this field.", MsgBoxStyle.Information, "Invalid Data");
						e.Cancel = true;
						return;
					}
					else
					{
						blnDirty = true;
					}
					if (lngYear != FCConvert.ToDouble(modExtraModules.FormatYear(vsTaxClub.EditText)))
					{
						lngYear = FCConvert.ToInt32(modExtraModules.FormatYear(vsTaxClub.EditText));
						// Since this changed then get the previous year taxes
						LoadPreviousYearTaxes(ref lngYear);
					}
				}
				else
				{
				}
			}
		}

		private void DeleteCurrentTaxClub()
		{
			// this will delete the current tax club that has been loaded on the screen
			string strTemp = "";
			if (rsAccountInfo.EndOfFile() && rsAccountInfo.BeginningOfFile())
			{
				FCMessageBox.Show("No account is loaded.  Please load an account before attemping a deletion.", MsgBoxStyle.Information, "No Current Record");
				return;
			}
			// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
			if (FCConvert.ToInt32(rsAccountInfo.Get_Fields("Year")) > 0)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
				strTemp = "Are you sure that you would like to delete the Tax Club for account " + FCConvert.ToString(rsAccountInfo.Get_Fields("Account")) + " and year " + FCConvert.ToString(rsAccountInfo.Get_Fields("Year")) + "?";
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				strTemp = "Are you sure that you would like to delete the Tax Club for account " + FCConvert.ToString(rsAccountInfo.Get_Fields("Account")) + "?";
			}
			DialogResult messageBoxResult = FCMessageBox.Show(strTemp, MsgBoxStyle.Question | MsgBoxStyle.YesNoCancel, "Delete Tax Club");
			if (messageBoxResult == DialogResult.Yes)
			{
				btnProcess.Visible = false;
				// delete the tax club
				rsAccountInfo.Delete();
				// vsTaxClub.rows = 0          'this should delete all of the rows
				// FormatGrid                  'this should refill all of the rows with the no information
				rsAccountInfo.Update();
				ResetDetailGrid();
				fraTaxClubDetails.Visible = false;
				if (txtAccount.Visible && txtAccount.Enabled)
				{
					txtAccount.Focus();
					// this will put the focus on the account box
				}
				txtAccount.Text = "";
				ResetSummaryGrid();
				fraTaxClubSummary.Visible = false;
				lblTaxClubSummary.Visible = false;
			}
			else if (messageBoxResult == DialogResult.Cancel)
			{
				// exit this function
				return;
			}
		}

		private bool SaveCurrentInformation()
		{
			bool SaveCurrentInformation = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will save the data currently stored in the grid into the current record of the recordset
				// it will return true if the data is saved correctly wit
				bool boolOverwrite = false;
				clsDRWrapper rsBillKey = new clsDRWrapper();
				clsDRWrapper rsTemp = new clsDRWrapper();
				double dblTotPayments;
				// VBto upgrade warning: dtLastPaymentDate As DateTime	OnWriteFCConvert.ToInt16(
				DateTime dtLastPaymentDate;
				SaveCurrentInformation = true;
				if (ValidateInformation())
				{
					// keep going
				}
				else
				{
					SaveCurrentInformation = false;
					return SaveCurrentInformation;
				}
				if (Conversion.Val(vsTaxClub.TextMatrix(lngRowYear, lngColInfo)) == 0)
				{
					SaveCurrentInformation = false;
					FCMessageBox.Show("Invalid year entered.  Please enter a valid year and try again.", MsgBoxStyle.Information, "Save Information");
					vsTaxClub.Select(lngRowYear, lngColInfo);
					return SaveCurrentInformation;
				}
				rsAccountInfo.FindFirstRecord("ID", lngCurrentRecordID);
				if (rsAccountInfo.EndOfFile())
				{
					SaveCurrentInformation = false;
					FCMessageBox.Show("No tax club loaded.  Please load a tax club and try again.", MsgBoxStyle.Information, "Save Information");
					return SaveCurrentInformation;
				}
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				if (Conversion.Val(rsAccountInfo.Get_Fields("Account")) != Conversion.Val(txtAccount.Text))
				{
					SaveCurrentInformation = false;
					FCMessageBox.Show("The current record does not match the account that is loaded.  Please load a tax club and try again.", MsgBoxStyle.Information, "Save Information");
					return SaveCurrentInformation;
				}
				if (FCConvert.ToBoolean(rsAccountInfo.Get_Fields_Boolean("NonActive")))
				{
					// If MsgBox("This Tax Club is not active.  Would you like to reactivate it?", MsgBoxStyle.YesNo | MsgBoxStyle.Question, "Non Active Tax Club") = DialogResult.Yes Then
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
					modGlobalFunctions.AddCYAEntry_242("CL", "Reactivating Tax Club", "Account : " + FCConvert.ToString(rsAccountInfo.Get_Fields("Account")), "Year : " + FCConvert.ToString(rsAccountInfo.Get_Fields("Year")), "Type : " + FCConvert.ToString(rsAccountInfo.Get_Fields("Type")));
					rsAccountInfo.Edit();
					rsAccountInfo.Set_Fields("NonActive", false);
					rsAccountInfo.Update();
					// Else
					// SaveCurrentInformation = False
					// MsgBox "The current record has not been reactivated and not saved.", MsgBoxStyle.Information, "Save Information"
					// Exit Function
					// End If
				}
				// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
				if (Conversion.Val(rsAccountInfo.Get_Fields("Year")) != Conversion.Val(vsTaxClub.TextMatrix(lngRowYear, lngColInfo)))
				{
					// if the years do not match, then check to make sure that there is
					// no other tax club with this account and year
					if (modStatusPayments.Statics.boolRE)
					{
						rsTemp.OpenRecordset("SELECT * FROM TaxClub WHERE Account = " + Strings.Trim(txtAccount.Text) + " AND [Year] = " + FCConvert.ToString(Conversion.Val(vsTaxClub.TextMatrix(lngRowYear, lngColInfo))) + " AND Type = 'RE'");
					}
					else
					{
						rsTemp.OpenRecordset("SELECT * FROM TaxClub WHERE Account = " + Strings.Trim(txtAccount.Text) + " AND [Year] = " + FCConvert.ToString(Conversion.Val(vsTaxClub.TextMatrix(lngRowYear, lngColInfo))) + " AND Type = 'PP'");
					}
					if (rsTemp.EndOfFile())
					{
						// all set
						boolOverwrite = true;
					}
					else
					{
						// TODO Get_Fields: Field [Key] not found!! (maybe it is an alias?)
						if (Conversion.Val(rsTemp.Get_Fields_Int32("ID")) == Conversion.Val(rsAccountInfo.Get_Fields("Key")))
						{
							// this is ok because it is the same record
							boolOverwrite = true;
						}
						else
						{
							SaveCurrentInformation = false;
							FCMessageBox.Show("There is already a tax club for account " + Strings.Trim(txtAccount.Text) + " and year " + FCConvert.ToString(Conversion.Val(vsTaxClub.TextMatrix(lngRowYear, lngColInfo))) + ".", MsgBoxStyle.Information, "Save Information");
							return SaveCurrentInformation;
						}
					}
				}
				dtLastPaymentDate = DateTime.FromOADate(0);
				dblTotPayments = 0;
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				rsBillKey.OpenRecordset("SELECT Principal+LienCost AS TotPayment, RecordedTransactionDate FROM PaymentRec WHERE Account = " + FCConvert.ToString(rsAccountInfo.Get_Fields("Account")) + " AND BillKey = " + FCConvert.ToString(rsAccountInfo.Get_Fields_Int32("BillKey")) + " AND BillCode = '" + Strings.Left(FCConvert.ToString(rsAccountInfo.Get_Fields("Type")), 1) + "'", modExtraModules.strCLDatabase);
				if (!rsBillKey.EndOfFile() && !boolOverwrite)
				{
					while (!rsBillKey.EndOfFile())
					{
						// TODO Get_Fields: Field [TotPayment] not found!! (maybe it is an alias?)
						if (Conversion.Val(rsBillKey.Get_Fields("TotPayment")) != 0)
						{
							// TODO Get_Fields: Field [TotPayment] not found!! (maybe it is an alias?)
							dblTotPayments += Conversion.Val(rsBillKey.Get_Fields("TotPayment"));
						}
						if (((rsBillKey.Get_Fields_DateTime("Recordedtransactiondate").ToOADate())) != 0)
						{
							if (DateAndTime.DateDiff("d", dtLastPaymentDate, (DateTime)rsBillKey.Get_Fields_DateTime("recordedtransactiondate")) > 0)
							{
								dtLastPaymentDate = (DateTime)rsBillKey.Get_Fields_DateTime("RecordedTransactionDate");
							}
						}
						rsBillKey.MoveNext();
					}
				}
				else
				{
					dblTotPayments = 0;
				}
				rsAccountInfo.Edit();
				if (boolOverwrite)
				{
					// if this is overwriting the old file, then get the new billkey and reset the payment amounts
					if (modStatusPayments.Statics.boolRE)
					{
						rsBillKey.OpenRecordset("SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngCurrentLoadedAccount) + " AND BillingType = 'RE' AND BillingYear = " + modExtraModules.FormatYear(vsTaxClub.TextMatrix(lngRowYear, lngColInfo)), modExtraModules.strCLDatabase);
					}
					else
					{
						rsBillKey.OpenRecordset("SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngCurrentLoadedAccount) + " AND BillingType = 'PP' AND BillingYear = " + modExtraModules.FormatYear(vsTaxClub.TextMatrix(lngRowYear, lngColInfo)), modExtraModules.strCLDatabase);
					}
					if (!rsBillKey.EndOfFile())
					{
						rsAccountInfo.Set_Fields("BillKey", rsBillKey.Get_Fields_Int32("ID"));
					}
				}
				rsAccountInfo.Set_Fields("Name", Strings.Trim(vsTaxClub.TextMatrix(lngRowName, lngColInfo)));
				rsAccountInfo.Set_Fields("Year", modExtraModules.FormatYear(vsTaxClub.TextMatrix(lngRowYear, lngColInfo)));
				rsAccountInfo.Set_Fields("AgreementDate", DateAndTime.DateValue(vsTaxClub.TextMatrix(lngRowAgreementDate, lngColInfo)));
				rsAccountInfo.Set_Fields("StartDate", DateAndTime.DateValue(vsTaxClub.TextMatrix(lngRowStartDate, lngColInfo)));
				rsAccountInfo.Set_Fields("NumberOfPayments", FCConvert.ToString(Conversion.Val(vsTaxClub.TextMatrix(lngRowNumberOFPayments, lngColInfo))));
				rsAccountInfo.Set_Fields("PaymentAmount", FCConvert.ToDouble(vsTaxClub.TextMatrix(lngRowPaymentAmount, lngColInfo)));
				rsAccountInfo.Set_Fields("TotalPayments", FCConvert.ToDouble(vsTaxClub.TextMatrix(lngRowTotalPayments, lngColInfo)));
				rsAccountInfo.Set_Fields("MapLot", Strings.Trim(vsTaxClub.TextMatrix(lngRowMapLot, lngColInfo)) + " ");
				rsAccountInfo.Set_Fields("Location", Strings.Trim(vsTaxClub.TextMatrix(lngRowLocation, lngColInfo)) + " ");
				rsAccountInfo.Set_Fields("TotalPaid", dblTotPayments);
				rsAccountInfo.Set_Fields("LastPaidDate", dtLastPaymentDate);
				if (cmbFreq.SelectedIndex == 0)
				{
					rsAccountInfo.Set_Fields("Freq", 0);
				}
				else if (cmbFreq.SelectedIndex == 1)
				{
					rsAccountInfo.Set_Fields("Freq", 1);
				}
				else if (cmbFreq.SelectedIndex == 2)
				{
					rsAccountInfo.Set_Fields("Freq", 2);
				}
				// find the correct billing record if possible
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				rsBill.FindFirstRecord2("Account,BillingYear", FCConvert.ToString(rsAccountInfo.Get_Fields("Account")) + "," + FCConvert.ToString(Conversion.Val(modExtraModules.FormatYear(vsTaxClub.TextMatrix(lngRowYear, lngColInfo)))), ",");
				if (rsBill.NoMatch)
				{
					// check to make sure that the user is creating a bill for a future bill
					// and not a past billing period that will never get filled in
					if (Conversion.Val(modExtraModules.FormatYear(vsTaxClub.TextMatrix(lngRowYear, lngColInfo))) < DateTime.Today.Year * 10)
					{
						SaveCurrentInformation = false;
						FCMessageBox.Show("This account has not been tied to a bill record.  There is no bill record for this account and year.", MsgBoxStyle.Information, "Missing Bill Record");
						return SaveCurrentInformation;
					}
					else
					{
						DialogResult messageBoxResult = FCMessageBox.Show("There is no bill record created for this account and year.  Would you like to create a bill record now?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Create Bill Record");
						if (messageBoxResult == DialogResult.Yes)
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							rsAccountInfo.Set_Fields("BillKey", CreateTaxClubBillRecord_8(FCConvert.ToInt32(rsAccountInfo.Get_Fields("Account")), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(modExtraModules.FormatYear(vsTaxClub.TextMatrix(lngRowYear, lngColInfo)))))));
							if (((rsAccountInfo.Get_Fields_Int32("BillKey"))) == -1)
							{
								// this returned an error and should not continue to save
								rsAccountInfo.Set_Fields("BillKey", 0);
								FCMessageBox.Show("Tax club not saved.", MsgBoxStyle.Information, "Save Information");
								return SaveCurrentInformation;
							}
						}
						else if (messageBoxResult == DialogResult.Cancel)
						{
							rsAccountInfo.Set_Fields("BillKey", 0);
							FCMessageBox.Show("Tax club not saved.", MsgBoxStyle.Information, "Save Information");
							return SaveCurrentInformation;
						}
					}
				}
				else
				{
					rsAccountInfo.Set_Fields("BillKey", rsBill.Get_Fields_Int32("ID"));
				}
				if (rsAccountInfo.Update(true))
				{
					FCMessageBox.Show("Tax club saved successfully.", MsgBoxStyle.Information, "Save Information");
				}
				else
				{
					FCMessageBox.Show("Tax club not saved.", MsgBoxStyle.Information, "Save Information");
				}
				return SaveCurrentInformation;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				SaveCurrentInformation = false;
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Save Information");
			}
			return SaveCurrentInformation;
		}

		private bool ValidateInformation()
		{
			bool ValidateInformation = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will return true if all of the data is entered and valid
				ValidateInformation = false;
				vsTaxClub.Select(0, 0);
				if (txtAccount.Visible && txtAccount.Enabled)
				{
					txtAccount.Focus();
				}
				// check the year
				if (Strings.Trim(vsTaxClub.TextMatrix(lngRowYear, lngColInfo)) == "")
				{
					ValidateInformation = false;
					FCMessageBox.Show("Please enter a valid year.", MsgBoxStyle.Exclamation, "Invalid data");
					vsTaxClub.Select(lngRowYear, lngColInfo);
					return ValidateInformation;
				}
				if (Conversion.Val(vsTaxClub.TextMatrix(lngRowYear, lngColInfo)) == 0)
				{
					FCMessageBox.Show("Please enter a valid year.", MsgBoxStyle.Exclamation, "Invalid data");
					vsTaxClub.Select(lngRowYear, lngColInfo);
					return ValidateInformation;
				}
				if (Conversion.Val(Strings.Right(vsTaxClub.TextMatrix(lngRowYear, lngColInfo), 1)) == 0)
				{
					FCMessageBox.Show("Please enter a valid year.", MsgBoxStyle.Exclamation, "Invalid data");
					vsTaxClub.Select(lngRowYear, lngColInfo);
					return ValidateInformation;
				}
				// check the agreement date
				if (Information.IsDate(vsTaxClub.TextMatrix(lngRowAgreementDate, lngColInfo)) == false)
				{
					ValidateInformation = false;
					FCMessageBox.Show("Please enter a valid date.", MsgBoxStyle.Exclamation, "Invalid data");
					vsTaxClub.Select(lngRowAgreementDate, lngColInfo);
					return ValidateInformation;
				}
				// check the start date
				if (Information.IsDate(vsTaxClub.TextMatrix(lngRowStartDate, lngColInfo)) == false)
				{
					ValidateInformation = false;
					FCMessageBox.Show("Please enter a valid date.", MsgBoxStyle.Exclamation, "Invalid data");
					vsTaxClub.Select(lngRowStartDate, lngColInfo);
					return ValidateInformation;
				}
				// check the number of payments
				if (cmbFreq.SelectedIndex == 0)
				{
					if (Conversion.Val(vsTaxClub.TextMatrix(lngRowNumberOFPayments, lngColInfo)) < 1 || Conversion.Val(vsTaxClub.TextMatrix(lngRowNumberOFPayments, lngColInfo)) > 12)
					{
						ValidateInformation = false;
						FCMessageBox.Show("Please enter a valid number of monthly payments. (1-12)", MsgBoxStyle.Exclamation, "Invalid data");
						vsTaxClub.Select(lngRowNumberOFPayments, lngColInfo);
						return ValidateInformation;
					}
				}
				else if (cmbFreq.SelectedIndex == 1)
				{
					if (Conversion.Val(vsTaxClub.TextMatrix(lngRowNumberOFPayments, lngColInfo)) < 1 || Conversion.Val(vsTaxClub.TextMatrix(lngRowNumberOFPayments, lngColInfo)) > 52)
					{
						ValidateInformation = false;
						FCMessageBox.Show("Please enter a valid number of weekly payments. (1-52)", MsgBoxStyle.Exclamation, "Invalid data");
						vsTaxClub.Select(lngRowNumberOFPayments, lngColInfo);
						return ValidateInformation;
					}
				}
				else if (cmbFreq.SelectedIndex == 2)
				{
					if (Conversion.Val(vsTaxClub.TextMatrix(lngRowNumberOFPayments, lngColInfo)) < 1 || Conversion.Val(vsTaxClub.TextMatrix(lngRowNumberOFPayments, lngColInfo)) > 26)
					{
						ValidateInformation = false;
						FCMessageBox.Show("Please enter a valid number of bi-weekly payments. (1-26)", MsgBoxStyle.Exclamation, "Invalid data");
						vsTaxClub.Select(lngRowNumberOFPayments, lngColInfo);
						return ValidateInformation;
					}
				}
				// check the amounts
				if (Conversion.Val(vsTaxClub.TextMatrix(lngRowPaymentAmount, lngColInfo)) < 0.01)
				{
					ValidateInformation = false;
					FCMessageBox.Show("Please enter a valid payment amount.", MsgBoxStyle.Exclamation, "Invalid data");
					vsTaxClub.Select(lngRowPaymentAmount, lngColInfo);
					return ValidateInformation;
				}
				// check the total amount
				if (Conversion.Val(vsTaxClub.TextMatrix(lngRowTotalPayments, lngColInfo)) < 0.01)
				{
					ValidateInformation = false;
					FCMessageBox.Show("Please enter a valid total amount.", MsgBoxStyle.Exclamation, "Invalid data");
					vsTaxClub.Select(lngRowTotalPayments, lngColInfo);
					return ValidateInformation;
				}
				ValidateInformation = true;
				return ValidateInformation;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				ValidateInformation = false;
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Data Validation");
			}
			return ValidateInformation;
		}
		// VBto upgrade warning: lngPreYear As int	OnWrite(string)
		private int CreateTaxClubBillRecord_8(int lngAcct, int lngPreYear)
		{
			return CreateTaxClubBillRecord(ref lngAcct, ref lngPreYear);
		}

		private int CreateTaxClubBillRecord(ref int lngAcct, ref int lngPreYear)
		{
			int CreateTaxClubBillRecord = 0;
			int intError = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will create an empty bill record with basic RE info (like a prepayment)
				// and then return the BillKey back to the tax club
				clsDRWrapper rsRE = new clsDRWrapper();
				clsDRWrapper rsPre = new clsDRWrapper();
				int lngBK;
				intError = 1;
				modGlobalFunctions.AddCYAEntry_80("CL", "Creating a bill for a tax club.", "Account: " + FCConvert.ToString(lngAcct), "Year: " + FCConvert.ToString(lngPreYear));
				intError = 2;
				// DJW@9/23/2013 Added cross apply to get name and address info for accounts
				if (modStatusPayments.Statics.boolRE)
				{
					rsRE.OpenRecordset("SELECT *, DeedName1 as FullNameLF, DeedName2 as SecOwnerFullNameLF FROM Master WHERE RSCard = 1 AND RSAccount = " + FCConvert.ToString(lngAcct), modExtraModules.strREDatabase);
				}
				else
				{
					rsRE.OpenRecordset("SELECT c.*, p.FullNameLF FROM PPMaster as c CROSS APPLY " + rsRE.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(PartyID,NULL,'PP',Account) as p WHERE Account = " + FCConvert.ToString(lngAcct), modExtraModules.strPPDatabase);
				}
				intError = 5;
				if (rsRE.RecordCount() != 0)
				{
					intError = 6;
					if (modStatusPayments.Statics.boolRE)
					{
						rsPre.OpenRecordset("SELECT * FROM BillingMaster WHERE billingtype = 'RE' and Account = " + FCConvert.ToString(lngAcct) + " AND BillingYear = " + FCConvert.ToString(lngPreYear), modExtraModules.strCLDatabase);
						// BillKey = 0"
					}
					else
					{
						rsPre.OpenRecordset("SELECT * FROM BillingMaster WHERE billingtype = 'PP' and Account = " + FCConvert.ToString(lngAcct) + " AND BillingYear = " + FCConvert.ToString(lngPreYear), modExtraModules.strCLDatabase);
						// BillKey = 0"
					}
					if (rsPre.EndOfFile())
					{
						rsPre.AddNew();
					}
					else
					{
						// failsafe - if the account is found then tie it to this record instead of creating a new one
						CreateTaxClubBillRecord = FCConvert.ToInt32(rsPre.Get_Fields_Int32("ID"));
						return CreateTaxClubBillRecord;
					}
					intError = 8;
					// lngBK = rsPre.Get_Fields("BillKey")
					if (modStatusPayments.Statics.boolRE)
					{
						intError = 9;
						rsPre.Set_Fields("Name1", rsRE.Get_Fields_String("FullNameLF"));
						intError = 10;
						// TODO Get_Fields: Field [SecOwnerFullNameLF] not found!! (maybe it is an alias?)
						rsPre.Set_Fields("Name2", rsRE.Get_Fields("SecOwnerFullNameLF"));
						intError = 11;
						if (Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("Address1"))) != "")
						{
							rsPre.Set_Fields("Address1", rsRE.Get_Fields_String("Address1"));
							intError = 12;
							rsPre.Set_Fields("Address2", rsRE.Get_Fields_String("Address2"));
						}
						else
						{
							rsPre.Set_Fields("Address1", rsRE.Get_Fields_String("Address2"));
						}
						intError = 13;
						// TODO Get_Fields: Field [PartyState] not found!! (maybe it is an alias?)
						rsPre.Set_Fields("Address3", FCConvert.ToString(rsRE.Get_Fields_String("Address3")) + " " + FCConvert.ToString(rsRE.Get_Fields_String("City")) + " " + FCConvert.ToString(rsRE.Get_Fields("PartyState")) + " " + FCConvert.ToString(rsRE.Get_Fields_String("Zip")));
						intError = 14;
						// If rsRE.Get_Fields("RSZip4") <> "" Then
						// rsPre.Get_Fields("Address3") = rsPre.Get_Fields("Address3") & " " & rsRE.Get_Fields("RSZip4")
						// End If
						intError = 15;
						rsPre.Set_Fields("StreetNumber", FCConvert.ToString(Conversion.Val(rsRE.Get_Fields_String("RSLOCNUMALPH"))));
						intError = 16;
						rsPre.Set_Fields("Apt", rsRE.Get_Fields_String("RSLOCApt"));
						intError = 17;
						rsPre.Set_Fields("StreetName", rsRE.Get_Fields_String("RSLocStreet"));
						intError = 18;
						rsPre.Set_Fields("MapLot", Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("RSMapLot"))));
						intError = 19;
						rsPre.Set_Fields("Account", lngAcct);
						intError = 20;
						rsPre.Set_Fields("BillingType", "RE");
					}
					else
					{
						intError = 21;
						rsPre.Set_Fields("Name1", rsRE.Get_Fields_String("FullNameLF"));
						intError = 22;
						rsPre.Set_Fields("Name2", "");
						intError = 23;
						rsPre.Set_Fields("Address1", rsRE.Get_Fields_String("Address1"));
						intError = 24;
						rsPre.Set_Fields("Address2", rsRE.Get_Fields_String("Address2"));
						intError = 25;
						if (modStatusPayments.Statics.boolRE)
						{
							// TODO Get_Fields: Field [PartyState] not found!! (maybe it is an alias?)
							rsPre.Set_Fields("Address3", FCConvert.ToString(rsRE.Get_Fields_String("City")) + " " + FCConvert.ToString(rsRE.Get_Fields("PartyState")) + " " + FCConvert.ToString(rsRE.Get_Fields_String("Zip")));
						}
						intError = 26;
						// If rsRE.Get_Fields("Zip4") <> "" And val(rsRE.Get_Fields("Zip4")) <> 0 Then
						// rsPre.Get_Fields("Address3") = rsPre.Get_Fields("Address3") & "-" & rsRE.Get_Fields("Zip4")
						// End If
						intError = 27;
						// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
						rsPre.Set_Fields("StreetNumber", FCConvert.ToString(Conversion.Val(rsRE.Get_Fields("StreetNumber"))));
						intError = 28;
						rsPre.Set_Fields("Apt", "");
						intError = 29;
						rsPre.Set_Fields("StreetName", rsRE.Get_Fields_String("Street"));
						intError = 30;
						rsPre.Set_Fields("MapLot", "");
						intError = 31;
						rsPre.Set_Fields("Account", lngAcct);
						intError = 32;
						rsPre.Set_Fields("BillingType", "PP");
					}
				}
				intError = 33;
				rsPre.Set_Fields("BillingYear", lngPreYear);
				intError = 34;
				rsPre.Set_Fields("InterestAppliedThroughDate", DateTime.Today);
				intError = 35;
				rsPre.Set_Fields("WhetherBilledBefore", "A");
				intError = 36;
				rsPre.Update();
				lngBK = FCConvert.ToInt32(rsPre.Get_Fields_Int32("ID"));
				CreateTaxClubBillRecord = lngBK;
				return CreateTaxClubBillRecord;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				CreateTaxClubBillRecord = -1;
				// return an error
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Creating Bill Record - " + FCConvert.ToString(intError));
			}
			return CreateTaxClubBillRecord;
		}

		private void LoadPreviousYearTaxes(ref int lngYR)
		{
			// this routine will load the previous tax amount when the year is changed
			clsDRWrapper rsBillAmt = new clsDRWrapper();
			double dblTotAmount = 0;
			// VBto upgrade warning: strPrevYear As double	OnWrite(string)
			double strPrevYear = 0;
			if (modStatusPayments.Statics.boolRE)
			{
				rsBillAmt.OpenRecordset("SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngCurrentLoadedAccount) + " AND BillingType = 'RE' AND BillingYear = " + (FCConvert.ToString(lngYear)), modExtraModules.strCLDatabase);
			}
			else
			{
				rsBillAmt.OpenRecordset("SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngCurrentLoadedAccount) + " AND BillingType = 'PP' AND BillingYear = " + (FCConvert.ToString(lngYear)), modExtraModules.strCLDatabase);
			}
			if (!rsBillAmt.EndOfFile())
			{
				// This found the current year, so use the tax due
				dblTotAmount = Conversion.Val(rsBillAmt.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsBillAmt.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsBillAmt.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsBillAmt.Get_Fields_Decimal("TaxDue4"));
				if (dblTotAmount > 0)
				{
					if (Conversion.Val(vsTaxClub.TextMatrix(lngRowNumberOFPayments, lngColInfo)) > 0)
					{
						vsTaxClub.TextMatrix(lngRowPaymentAmount, lngColInfo, Strings.Format(modGlobal.Round(dblTotAmount / FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(vsTaxClub.TextMatrix(lngRowNumberOFPayments, lngColInfo)))), 2), "#,##0.00"));
						// this will calculate the total amount of payments
						vsTaxClub.TextMatrix(lngRowTotalPayments, lngColInfo, Strings.Format(FCConvert.ToDouble(vsTaxClub.TextMatrix(lngRowNumberOFPayments, lngColInfo)) * FCConvert.ToDouble(vsTaxClub.TextMatrix(lngRowPaymentAmount, lngColInfo)), "#,##0.00"));
					}
					else
					{
						vsTaxClub.TextMatrix(lngRowTotalPayments, lngColInfo, Strings.Format(dblTotAmount, "#,##0.00"));
					}
				}
				else
				{
					strPrevYear = FCConvert.ToDouble(((((lngYear / 10) + 1) * 10) + 1).ToString());
					// get the previous year information
					rsBillAmt.OpenRecordset("SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngCurrentLoadedAccount) + " AND BillingType = 'RE' AND BillingYear = " + FCConvert.ToString(strPrevYear), modExtraModules.strCLDatabase);
					if (!rsBillAmt.EndOfFile())
					{
						dblTotAmount = Conversion.Val(rsBillAmt.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsBillAmt.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsBillAmt.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsBillAmt.Get_Fields_Decimal("TaxDue4"));
						if (Conversion.Val(vsTaxClub.TextMatrix(lngRowNumberOFPayments, lngColInfo)) > 0)
						{
							vsTaxClub.TextMatrix(lngRowPaymentAmount, lngColInfo, Strings.Format(modGlobal.Round(dblTotAmount / FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(vsTaxClub.TextMatrix(lngRowNumberOFPayments, lngColInfo)))), 2), "#,##0.00"));
							// this will calculate the total amount of payments
							vsTaxClub.TextMatrix(lngRowTotalPayments, lngColInfo, Strings.Format(FCConvert.ToDouble(vsTaxClub.TextMatrix(lngRowNumberOFPayments, lngColInfo)) * FCConvert.ToDouble(vsTaxClub.TextMatrix(lngRowPaymentAmount, lngColInfo)), "#,##0.00"));
						}
						else
						{
							vsTaxClub.TextMatrix(lngRowTotalPayments, lngColInfo, Strings.Format(dblTotAmount, "#,##0.00"));
						}
					}
					else
					{
						// leave the amount in there
					}
				}
			}
			else
			{
				strPrevYear = FCConvert.ToDouble(((((lngYear / 10) - 1) * 10) + 1).ToString());
				// get the previous year information
				rsBillAmt.OpenRecordset("SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngCurrentLoadedAccount) + " AND BillingType = 'RE' AND BillingYear = " + FCConvert.ToString(strPrevYear), modExtraModules.strCLDatabase);
				if (!rsBillAmt.EndOfFile())
				{
					dblTotAmount = Conversion.Val(rsBillAmt.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsBillAmt.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsBillAmt.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsBillAmt.Get_Fields_Decimal("TaxDue4"));
					if (Conversion.Val(vsTaxClub.TextMatrix(lngRowNumberOFPayments, lngColInfo)) > 0)
					{
						vsTaxClub.TextMatrix(lngRowPaymentAmount, lngColInfo, Strings.Format(modGlobal.Round(dblTotAmount / FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(vsTaxClub.TextMatrix(lngRowNumberOFPayments, lngColInfo)))), 2), "#,##0.00"));
						// this will calculate the total amount of payments
						vsTaxClub.TextMatrix(lngRowTotalPayments, lngColInfo, Strings.Format(FCConvert.ToDouble(vsTaxClub.TextMatrix(lngRowNumberOFPayments, lngColInfo)) * FCConvert.ToDouble(vsTaxClub.TextMatrix(lngRowPaymentAmount, lngColInfo)), "#,##0.00"));
					}
					else
					{
						vsTaxClub.TextMatrix(lngRowTotalPayments, lngColInfo, Strings.Format(dblTotAmount, "#,##0.00"));
					}
				}
				else
				{
					// leave the amount in there
				}
			}
		}

		private void SetupSummaryGrid()
		{
			lngDColRecordID = 0;
			lngDColName = 1;
			lngDColYear = 2;
			lngDColDate = 3;
		}

		private void FormatSummaryGrid()
		{
			vsTaxClubSummary.Cols = 4;
			vsTaxClubSummary.Rows = 1;
			vsTaxClubSummary.ColWidth(lngDColRecordID, 0);
			vsTaxClubSummary.ColWidth(lngDColName, FCConvert.ToInt32(vsTaxClubSummary.Width * 0.5));
			vsTaxClubSummary.ColWidth(lngDColYear, FCConvert.ToInt32(vsTaxClubSummary.Width * 0.25));
			vsTaxClubSummary.ColWidth(lngDColDate, FCConvert.ToInt32(vsTaxClubSummary.Width * 0.25));
			vsTaxClubSummary.ExtendLastCol = true;
			vsTaxClubSummary.ColAlignment(lngDColYear, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsTaxClubSummary.ColAlignment(lngDColDate, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			// Record ID
			vsTaxClubSummary.TextMatrix(0, lngDColRecordID, "ID");
			// Name
			vsTaxClubSummary.TextMatrix(0, lngDColName, "Name");
			// Year
			vsTaxClubSummary.TextMatrix(0, lngDColYear, "Year");
			// Agreement Date
			vsTaxClubSummary.TextMatrix(0, lngDColDate, "Agreement Date");
			//vsTaxClubSummary.Height = (vsTaxClubSummary.Rows * vsTaxClubSummary.RowHeight(0)) + 350;
			vsTaxClubSummary.SelectionMode = FCGrid.SelectionModeSettings.flexSelectionByRow;
		}

		private void SetupDetailGrid()
		{
			// setup the grid rows and cols
			lngColQuestions = 0;
			lngColHidden = 1;
			lngColInfo = 2;
			lngRowName = 0;
			lngRowYear = 1;
			lngRowAgreementDate = 2;
			lngRowStartDate = 3;
			lngRowNumberOFPayments = 4;
			lngRowPaymentAmount = 5;
			lngRowTotalPayments = 6;
			lngRowMapLot = 7;
			lngRowLocation = 8;
		}

		private void ResetSummaryGrid()
		{
			vsTaxClubSummary.Clear();
			FormatSummaryGrid();
		}

		private void ResetDetailGrid()
		{
			vsTaxClub.Clear();
			FormatGrid();
		}

		private void ResizeSummaryGrid()
		{
			vsTaxClubSummary.ColWidth(lngDColRecordID, 0);
			vsTaxClubSummary.ColWidth(lngDColName, FCConvert.ToInt32(vsTaxClubSummary.WidthOriginal * 0.5));
			vsTaxClubSummary.ColWidth(lngDColYear, FCConvert.ToInt32(vsTaxClubSummary.WidthOriginal * 0.25));
			vsTaxClubSummary.ColWidth(lngDColDate, FCConvert.ToInt32(vsTaxClubSummary.WidthOriginal * 0.25));
			//vsTaxClubSummary.HeightOriginal = (vsTaxClubSummary.Rows * vsTaxClubSummary.RowHeight(0)) + 350;
		}

		private void vsTaxClubSummary_DblClick(object sender, EventArgs e)
		{
			// Double-click displays details
			int lngMouseRow = 0;
			// VBto upgrade warning: lngID As int	OnWrite(string)
			int lngID = 0;
			lngMouseRow = vsTaxClubSummary.MouseRow;
			lngID = FCConvert.ToInt32(vsTaxClubSummary.TextMatrix(lngMouseRow, lngDColRecordID));
			if (GetAccountInformation_Detail(lngID))
			{
				fraTaxClubDetails.Visible = true;
				fraTaxClubDetails.Enabled = true;
				fraTaxClubSummary.Visible = false;
				lblTaxClubSummary.Visible = false;
				mnuFilePrint.Enabled = true;
				cmdFileDelete.Enabled = true;
				cmbFreq.Enabled = true;
				cmbFreq.Visible = true;
			}
			else
			{
				FCMessageBox.Show("Unable to load the Tax Club details.", MsgBoxStyle.OkOnly | MsgBoxStyle.Information, "No Data");
			}
		}

		private bool GetAccountInformation_Detail(int lngID)
		{
			bool GetAccountInformation_Detail = false;
			ResetDetailGrid();
			int lngAccount;
			lngAccount = lngCurrentLoadedAccount;
			GetAccountInformation_Detail = true;
			// Optimistic
			if (!boolNoValidate)
			{
				boolNoValidate = true;
				fraTaxClubSummary.Enabled = false;
				boolNoValidate = false;
			}
			rsAccountInfo.FindFirstRecord("ID", lngID);
			if (rsAccountInfo.NoMatch)
			{
				GetAccountInformation_Detail = false;
			}
			else
			{
				if (modStatusPayments.Statics.boolRE)
				{
					vsTaxClub.TextMatrix(lngRowName, lngColInfo, Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("FullNameLF")) + " "));
				}
				else
				{
					vsTaxClub.TextMatrix(lngRowName, lngColInfo, Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("Name")) + " "));
				}
				// Year
				// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
				if (Conversion.Val(rsAccountInfo.Get_Fields("Year")) > 0)
				{
					// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
					vsTaxClub.TextMatrix(lngRowYear, lngColInfo, modExtraModules.FormatYear(FCConvert.ToString(rsAccountInfo.Get_Fields("Year"))));
					// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
					lngYear = FCConvert.ToInt32(rsAccountInfo.Get_Fields("Year"));
				}
				else
				{
					vsTaxClub.TextMatrix(lngRowYear, lngColInfo, FCConvert.ToString(0));
					lngYear = 0;
				}
				// Agreement Date
				vsTaxClub.TextMatrix(lngRowAgreementDate, lngColInfo, Strings.Format(rsAccountInfo.Get_Fields_DateTime("AgreementDate"), "MM/dd/yyyy"));
				// Start Date
				vsTaxClub.TextMatrix(lngRowStartDate, lngColInfo, Strings.Format(rsAccountInfo.Get_Fields_DateTime("StartDate"), "MM/dd/yyyy"));
				// # of Payments (Max 12)
				vsTaxClub.TextMatrix(lngRowNumberOFPayments, lngColInfo, FCConvert.ToString(rsAccountInfo.Get_Fields_Int32("NumberOfPayments")));
				// Payment Amount
				vsTaxClub.TextMatrix(lngRowPaymentAmount, lngColInfo, Strings.Format(rsAccountInfo.Get_Fields_Double("PaymentAmount"), "#,##0.00"));
				// Total of Payments (Calculated)
				vsTaxClub.TextMatrix(lngRowTotalPayments, lngColInfo, Strings.Format(rsAccountInfo.Get_Fields_Double("TotalPayments"), "#,##0.00"));
				// MAL@20071217: Change to always grab the information from Real Estate
				// Tracker Reference: 11665
				// Map Lot
				// vsTaxClub.TextMatrix(lngRowMapLot, lngColInfo) = Trim(.Get_Fields("MapLot") & " ")
				// Location
				// vsTaxClub.TextMatrix(lngRowLocation, lngColInfo) = Trim(.Get_Fields("Location") & " ")
				if (modStatusPayments.Statics.boolRE)
				{
					rsRE.FindFirstRecord("RSAccount", lngAccount);
				}
				else
				{
					rsRE.FindFirstRecord("Account", lngAccount);
				}
				if (rsRE.RecordCount() > 0 && modStatusPayments.Statics.boolRE)
				{
					// Map/Lot
					vsTaxClub.TextMatrix(lngRowMapLot, lngColInfo, FCConvert.ToString(rsRE.Get_Fields_String("RSMapLot")) + " ");
					// Location
					vsTaxClub.TextMatrix(lngRowLocation, lngColInfo, Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("RSLOCNUMALPH")) + " " + FCConvert.ToString(rsRE.Get_Fields_String("RSLOCAPT")) + " " + FCConvert.ToString(rsRE.Get_Fields_String("RSLOCSTREET"))) + " ");
				}
				else
				{
					vsTaxClub.TextMatrix(lngRowMapLot, lngColInfo, "");
					vsTaxClub.TextMatrix(lngRowLocation, lngColInfo, "");
				}
				// set the frequency
				cmbFreq.SelectedIndex = rsAccountInfo.Get_Fields_Int16("Freq");
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "CLPreviousTaxClub", modGlobal.PadToString(FCConvert.ToInt32(Conversion.Val(txtAccount.Text)), 8));
				lngCurrentRecordID = lngID;
			}
			boolNoValidate = true;
			return GetAccountInformation_Detail;
		}

		private void vsTaxClubSummary_RowColChange(object sender, EventArgs e)
		{
			// MAL@20080925: Enable the print tax booklet with a year selected
			// Tracker Reference: 15485
			if (vsTaxClubSummary.MouseRow > 0)
			{
				if (vsTaxClubSummary.TextMatrix(vsTaxClubSummary.Row, lngDColYear) != "")
				{
					mnuFilePrint.Enabled = true;
					mnuFilePrintBooklet.Enabled = true;
					mnuFilePrintMaster.Enabled = true;
				}
			}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFileSave_Click(sender, e);
		}

		private void t_Click(object sender, EventArgs e)
		{
			this.mnuFileSave_Click(sender, e);
		}

		private void cmdFileDelete_Click(object sender, EventArgs e)
		{
			this.mnuFileDelete_Click(sender, e);
		}

		private void cmdFileNew_Click(object sender, EventArgs e)
		{
			this.mnuFileNew_Click(sender, e);
		}

		private void cmdFileSave_Click(object sender, EventArgs e)
		{
			this.mnuFileSave_Click(sender, e);
		}
		//FC:FINAL:CHN: Allow to input numbers at Grid columns on client side (javascript).
		private void vsTaxClub_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (e.Control != null)
			{
				if (vsTaxClub.MouseCol == 2)
				{
					var box = e.Control as TextBox;
					if (box != null)
					{
						if (vsTaxClub.MouseRow == 4)
						{
                            box.AllowOnlyNumericInput();
                            box.RemoveAlphaCharactersOnValueChange();
						}
						else if (vsTaxClub.MouseRow == 5)
						{
                            box.AllowOnlyNumericInput(true);
                            box.RemoveAlphaCharactersOnValueChange();
						}
					}
				}
			}
		}
	}
}
