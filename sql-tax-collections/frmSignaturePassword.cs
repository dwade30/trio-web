﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmSignaturePassword.
	/// </summary>
	public partial class frmSignaturePassword : BaseForm
	{
		public frmSignaturePassword()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmSignaturePassword InstancePtr
		{
			get
			{
				return (frmSignaturePassword)Sys.GetInstance(typeof(frmSignaturePassword));
			}
		}

		protected frmSignaturePassword _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Corey Gray
		// Date
		// 04/11/2006
		// ********************************************************
		private int lngReturn;
		private bool boolRequireCorrectPassword;

		public int Init(int lngType, int lngID = 0, bool boolForceCorrectPassword = false, int lngForceID = 0, string strMod = "")
		{
			int Init = 0;
			Init = 0;
			lngReturn = 0;
			boolRequireCorrectPassword = boolForceCorrectPassword;
			if (!FillcmbName(ref lngType, ref lngID, ref lngForceID, ref strMod))
			{
				Close();
				return Init;
			}
			this.Show(FormShowEnum.Modal);
			Init = lngReturn;
			return Init;
		}

		private void frmSignaturePassword_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = 0;
				mnuExit_Click();
			}
			else if (KeyCode == Keys.Return)
			{
				mnuSaveContinue_Click();
			}
		}

		private void frmSignaturePassword_Load(object sender, System.EventArgs e)
		{
            //Begin Unmaped Properties
            //frmSignaturePassword.Icon	= "frmSignaturePassword.frx":0000";
            //frmSignaturePassword.FillStyle	= 0;
            //frmSignaturePassword.ScaleWidth	= 3885;
            //frmSignaturePassword.ScaleHeight	= 2415;
            //frmSignaturePassword.LinkTopic	= "Form2";
            //frmSignaturePassword.LockControls	= -1  'True;
            //frmSignaturePassword.PaletteMode	= 1  'UseZOrder;
            //Font.Size	= "9";
            //Font.Name	= "Tahoma";
            //Font.Weight	= 400;
            //Font.Italic	= 0;
            //Font.Underline	= 0;
            //Font.Strikethrough	= 0;
            //Font.Charset	= 0;
            //txtPassword.IMEMode	= 3  'DISABLE;
            txtPassword.PasswordChar = '*';
			//vsElasticLight1.OleObjectBlob	= "frmSignaturePassword.frx":058A";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			lngReturn = -1;
			// indicate a cancel not a bad password
			Close();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private bool FillcmbName(ref int lngType, ref int lngID, ref int lngForceID, ref string strModule)
		{
			bool FillcmbName = false;
			clsDRWrapper rsLoad = new clsDRWrapper();
			int lngSave;
			try
			{
				// On Error GoTo ErrorHandler
				FillcmbName = false;
				if (lngForceID == 0)
				{
					if (strModule == "")
					{
						rsLoad.OpenRecordset("select * from signatures where type = " + FCConvert.ToString(lngType) + " order by name", "SystemSettings", 0, false, 0, false, "", false);
					}
					else
					{
						rsLoad.OpenRecordset("select * from signatures where module = '" + strModule + "' order by name", "SystemSettings", 0, false, 0, false, "", false);
					}
				}
				else
				{
					rsLoad.OpenRecordset("Select * from signatures where type = " + FCConvert.ToString(lngType) + " and id = " + FCConvert.ToString(lngForceID), "SystemSettings", 0, false, 0, false, "", false);
				}
				if (rsLoad.EndOfFile())
				{
					return FillcmbName;
				}
				lngSave = 0;
				cmbName.Clear();
				while (!rsLoad.EndOfFile())
				{
					cmbName.AddItem(FCConvert.ToString(rsLoad.Get_Fields_String("name")));
					cmbName.ItemData(cmbName.NewIndex, FCConvert.ToInt32(rsLoad.Get_Fields_Int32("id")));
					if (lngID == ((rsLoad.Get_Fields_Int32("id"))))
					{
						lngSave = cmbName.NewIndex;
					}
					rsLoad.MoveNext();
				}
				cmbName.SelectedIndex = lngSave;
				if (cmbName.Items.Count == 1)
				{
					cmbName.Enabled = false;
				}
				FillcmbName = true;
				return FillcmbName;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In FillcmbName", MsgBoxStyle.Critical, "Error");
			}
			return FillcmbName;
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			int lngID;
			string strPassword;
			lngID = cmbName.ItemData(cmbName.SelectedIndex);
			strPassword = Strings.Trim(txtPassword.Text);
			if (Strings.UCase(strPassword) == Strings.UCase(modSignatureFile.GetSignaturePassword(ref lngID)))
			{
				lngReturn = lngID;
				Close();
				return;
			}
			else
			{
				FCMessageBox.Show("Incorrect Password", MsgBoxStyle.Exclamation, "Incorrect");
				if (boolRequireCorrectPassword)
				{
					return;
				}
				Close();
				return;
			}
		}

		public void mnuSaveContinue_Click()
		{
			mnuSaveContinue_Click(btnProcess, new System.EventArgs());
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuSaveContinue_Click(sender, e);
		}
	}
}
