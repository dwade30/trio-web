﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmSignaturePassword.
	/// </summary>
	partial class frmSignaturePassword : BaseForm
	{
		public fecherFoundation.FCTextBox txtPassword;
		public fecherFoundation.FCComboBox cmbName;
		public fecherFoundation.FCLabel lblPassword;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSignaturePassword));
			this.txtPassword = new fecherFoundation.FCTextBox();
			this.cmbName = new fecherFoundation.FCComboBox();
			this.lblPassword = new fecherFoundation.FCLabel();
			//this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.lblName = new fecherFoundation.FCLabel();
			this.btnProcess = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 201);
			this.BottomPanel.Size = new System.Drawing.Size(356, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.lblName);
			this.ClientArea.Controls.Add(this.txtPassword);
			this.ClientArea.Controls.Add(this.cmbName);
			this.ClientArea.Controls.Add(this.lblPassword);
			this.ClientArea.Size = new System.Drawing.Size(356, 141);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(356, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(240, 35);
			this.HeaderText.Text = "Signature Password";
			// 
			// txtPassword
			// 
			this.txtPassword.AutoSize = false;
			this.txtPassword.BackColor = System.Drawing.SystemColors.Window;
			this.txtPassword.Location = new System.Drawing.Point(162, 90);
			this.txtPassword.Name = "txtPassword";
			this.txtPassword.Size = new System.Drawing.Size(176, 40);
			this.txtPassword.TabIndex = 3;
			// 
			// cmbName
			// 
			this.cmbName.AutoSize = false;
			this.cmbName.BackColor = System.Drawing.SystemColors.Window;
			this.cmbName.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbName.FormattingEnabled = true;
			this.cmbName.Location = new System.Drawing.Point(162, 30);
			this.cmbName.Name = "cmbName";
			this.cmbName.Size = new System.Drawing.Size(176, 40);
			this.cmbName.TabIndex = 1;
			// 
			// lblPassword
			// 
			this.lblPassword.AutoSize = true;
			this.lblPassword.Location = new System.Drawing.Point(30, 104);
			this.lblPassword.Name = "lblPassword";
			this.lblPassword.Size = new System.Drawing.Size(80, 16);
			this.lblPassword.TabIndex = 2;
			this.lblPassword.Text = "PASSWORD";
			this.lblPassword.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// MainMenu1
			// 
			//this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
			//this.mnuProcess});
			//this.MainMenu1.Visible = true;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = 0;
			this.mnuProcess.Text = "File";
			// 
			// lblName
			// 
			this.lblName.AutoSize = true;
			this.lblName.Location = new System.Drawing.Point(30, 44);
			this.lblName.Name = "lblName";
			this.lblName.Size = new System.Drawing.Size(43, 16);
			this.lblName.TabIndex = 0;
			this.lblName.Text = "NAME";
			this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(137, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(176, 48);
			this.btnProcess.TabIndex = 3;
			this.btnProcess.Text = "Save & Continue";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// frmSignaturePassword
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(356, 309);
			this.FillColor = 0;
			this.KeyPreview = true;
			//this.Menu = this.MainMenu1;
			this.Name = "frmSignaturePassword";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Signature Password";
			this.Load += new System.EventHandler(this.frmSignaturePassword_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmSignaturePassword_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		public FCLabel lblName;
		private FCButton btnProcess;
	}
}
