﻿using fecherFoundation;

namespace TWCL0000
{
	public class modAuditReporting
	{
		//=========================================================
		public static void CreateAuditChangesTable()
		{
			clsDRWrapper rsUpdate = new clsDRWrapper();
			rsUpdate.OpenRecordset("SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'AuditChanges'", modCollections.DEFAULTDATABASE);
			if (rsUpdate.RecordCount() == 0)
			{
				rsUpdate.Execute("CREATE TABLE AuditChanges(ID int NOT NULL IDENTITY (1, 1)) ON [PRIMARY]", modCollections.DEFAULTDATABASE);
				rsUpdate.Execute("ALTER TABLE AuditChanges ADD CONSTRAINT PK_AuditChanges PRIMARY KEY CLUSTERED(ID)", modCollections.DEFAULTDATABASE);
				rsUpdate.Execute("ALTER TABLE AuditChanges ADD Location nvarchar(255) NULL", modCollections.DEFAULTDATABASE);
				rsUpdate.Execute("ALTER TABLE AuditChanges ADD ChangeDescription nvarchar(255) NULL", modCollections.DEFAULTDATABASE);
				rsUpdate.Execute("ALTER TABLE AuditChanges ADD UserField1 nvarchar(255) NULL", modCollections.DEFAULTDATABASE);
				rsUpdate.Execute("ALTER TABLE AuditChanges ADD UserField2 nvarchar(255) NULL", modCollections.DEFAULTDATABASE);
				rsUpdate.Execute("ALTER TABLE AuditChanges ADD UserField3 nvarchar(255) NULL", modCollections.DEFAULTDATABASE);
				rsUpdate.Execute("ALTER TABLE AuditChanges ADD UserField4 nvarchar(255) NULL", modCollections.DEFAULTDATABASE);
				rsUpdate.Execute("ALTER TABLE AuditChanges ADD DateUpdated datetime NULL", modCollections.DEFAULTDATABASE);
				rsUpdate.Execute("ALTER TABLE AuditChanges ADD TimeUpdated datetime NULL", modCollections.DEFAULTDATABASE);
				rsUpdate.Execute("ALTER TABLE AuditChanges ADD UserID nvarchar(255) NULL", modCollections.DEFAULTDATABASE);
			}
			rsUpdate.OpenRecordset("SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'AuditChangesArchive'", modCollections.DEFAULTDATABASE);
			if (rsUpdate.RecordCount() == 0)
			{
				rsUpdate.Execute("CREATE TABLE AuditChangesArchive(ID int NOT NULL IDENTITY (1, 1)) ON [PRIMARY]", modCollections.DEFAULTDATABASE);
				rsUpdate.Execute("ALTER TABLE AuditChangesArchive ADD CONSTRAINT PK_AuditChangesArchive PRIMARY KEY CLUSTERED(ID)", modCollections.DEFAULTDATABASE);
				rsUpdate.Execute("ALTER TABLE AuditChangesArchive ADD Location nvarchar(255) NULL", modCollections.DEFAULTDATABASE);
				rsUpdate.Execute("ALTER TABLE AuditChangesArchive ADD ChangeDescription nvarchar(255) NULL", modCollections.DEFAULTDATABASE);
				rsUpdate.Execute("ALTER TABLE AuditChangesArchive ADD UserField1 nvarchar(255) NULL", modCollections.DEFAULTDATABASE);
				rsUpdate.Execute("ALTER TABLE AuditChangesArchive ADD UserField2 nvarchar(255) NULL", modCollections.DEFAULTDATABASE);
				rsUpdate.Execute("ALTER TABLE AuditChangesArchive ADD UserField3 nvarchar(255) NULL", modCollections.DEFAULTDATABASE);
				rsUpdate.Execute("ALTER TABLE AuditChangesArchive ADD UserField4 nvarchar(255) NULL", modCollections.DEFAULTDATABASE);
				rsUpdate.Execute("ALTER TABLE AuditChangesArchive ADD DateUpdated datetime NULL", modCollections.DEFAULTDATABASE);
				rsUpdate.Execute("ALTER TABLE AuditChangesArchive ADD TimeUpdated datetime NULL", modCollections.DEFAULTDATABASE);
				rsUpdate.Execute("ALTER TABLE AuditChangesArchive ADD UserID nvarchar(255) NULL", modCollections.DEFAULTDATABASE);
			}
		}

		public static void ReportChanges_2(int intItems, clsAuditControlInformation[] clsControlInfo, clsAuditChangesReporting clsReport)
		{
			ReportChanges(ref intItems, ref clsControlInfo, ref clsReport);
		}

		public static void ReportChanges(ref int intItems, ref clsAuditControlInformation[] clsControlInfo, ref clsAuditChangesReporting clsReport)
		{
			int counter;
			for (counter = 0; counter <= intItems; counter++)
			{
				if (clsControlInfo[counter].ControlType == clsAuditControlInformation.enuControlType.OptionButton)
				{
					if (clsControlInfo[counter].NewValue != clsControlInfo[counter].OldValue && clsControlInfo[counter].NewValue == System.Convert.ToString(true))
					{
						clsReport.AddChange(clsControlInfo[counter].DataDescription + " changed.  Old Value: " + clsControlInfo[counter].OldValue + "  New Value: " + clsControlInfo[counter].NewValue);
					}
				}
				else if (clsControlInfo[counter].ControlType != clsAuditControlInformation.enuControlType.DeletedControl)
				{
					if (clsControlInfo[counter].NewValue != clsControlInfo[counter].OldValue)
					{
						clsReport.AddChange(clsControlInfo[counter].DataDescription + " changed.  Old Value: " + clsControlInfo[counter].OldValue + "  New Value: " + clsControlInfo[counter].NewValue);
					}
				}
			}
		}
		// VBto upgrade warning: intTotalItems As short	OnWrite(int)
		// VBto upgrade warning: intDeletedRow As short	OnWrite(int)
		public static void RemoveGridRow_8(string strGridName, short intTotalItems, short intDeletedRow, clsAuditControlInformation[] clsControlInfo)
		{
			RemoveGridRow(ref strGridName, ref intTotalItems, ref intDeletedRow, ref clsControlInfo);
		}

		public static void RemoveGridRow(ref string strGridName, ref short intTotalItems, ref short intDeletedRow, ref clsAuditControlInformation[] clsControlInfo)
		{
			int counter;
			int intLen = 0;
			string strTemp = "";
			for (counter = 0; counter <= intTotalItems; counter++)
			{
				if (clsControlInfo[counter].ControlName == strGridName)
				{
					if (clsControlInfo[counter].GridRow == intDeletedRow)
					{
						clsControlInfo[counter].ControlType = clsAuditControlInformation.enuControlType.DeletedControl;
					}
					else if (clsControlInfo[counter].GridRow > intDeletedRow)
					{
						intLen = ("Row " + clsControlInfo[counter].GridRow).Length;
						strTemp = clsControlInfo[counter].DataDescription;
						strTemp = Strings.Mid(strTemp, intLen + 1);
						clsControlInfo[counter].GridRow -= 1;
						clsControlInfo[counter].DataDescription = "Row " + clsControlInfo[counter].GridRow + strTemp;
					}
				}
			}
		}

		public static void MoveGridRow(ref string strGridName, ref short intTotalItems, ref int intOldRow, ref int intNewRow, ref clsAuditControlInformation[] clsControlInfo)
		{
			int counter;
			int intLen/*unused?*/;
			string strTemp = "";
			for (counter = 0; counter <= intTotalItems; counter++)
			{
				if (clsControlInfo[counter].ControlName == strGridName)
				{
					if (clsControlInfo[counter].GridRow == intOldRow)
					{
						clsControlInfo[counter].GridRow = -10;
					}
				}
			}
			if (intOldRow > intNewRow)
			{
				for (counter = 0; counter <= intTotalItems; counter++)
				{
					if (clsControlInfo[counter].ControlName == strGridName)
					{
						if (clsControlInfo[counter].GridRow >= intNewRow && clsControlInfo[counter].GridRow < intOldRow)
						{
							clsControlInfo[counter].GridRow += 1;
						}
					}
				}
			}
			else
			{
				for (counter = 0; counter <= intTotalItems; counter++)
				{
					if (clsControlInfo[counter].ControlName == strGridName)
					{
						if (clsControlInfo[counter].GridRow > intOldRow && clsControlInfo[counter].GridRow <= intNewRow)
						{
							clsControlInfo[counter].GridRow -= 1;
						}
					}
				}
			}
			for (counter = 0; counter <= intTotalItems; counter++)
			{
				if (clsControlInfo[counter].ControlName == strGridName)
				{
					if (clsControlInfo[counter].GridRow == -10)
					{
						clsControlInfo[counter].GridRow = intNewRow;
					}
				}
			}
		}
	}
}
