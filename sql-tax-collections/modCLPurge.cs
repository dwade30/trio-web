﻿using fecherFoundation;
using Global;
using System;
using SharedApplication.TaxCollections.AccountPayment;

namespace TWCL0000
{
    public class modCLPurge
    {
        #region Static Variables
        public static StaticVariables Statics => (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
        public class StaticVariables
        {
           // public double[] dblPurgeTotals = new double[500 + 1];
            public int lngMaxPurgeAccounts;
        }
        #endregion

        #region Public Methods
        public static void PurgeCLRecords_8(short intType, DateTime dtDate)
        {
            PurgeCLRecords(ref intType, ref dtDate);
        }

        public static void PurgeCLRecords(ref short intType, ref DateTime dtDate)
        {
            try
            {
                NotifyStart(dtDate);

                var sql = GetBillSql(intType);

                var countPurged = PurgeRecords(sql);

                NotifyFinish(intType, countPurged);
            }
            catch (Exception ex)
            {
                FCMessageBox.Show($"Error # {FCConvert.ToString(Information.Err(ex).Number)} - {Information.Err(ex).Description}.", MsgBoxStyle.Critical, "Error In Purge");
            }
        }

        public static void PurgeCYAEntries_8(string strMod, DateTime dtDate)
        {
            PurgeCYAEntries(ref strMod, ref dtDate);
        }

        public static void PurgeCYAEntries(ref string strMod, ref DateTime dtDate)
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this function will purge all the CYA entries prior to this date
                var rsCYA = new clsDRWrapper();

                if (dtDate.ToOADate() < DateTime.Today.ToOADate() && Strings.Trim(strMod) != "")
                {
                    // this will create the database name with the module initials that are passed in
                    var strDatebaseName = "TW" + strMod + "0000.vb1";
                    //rsCYA.Execute("DELETE * FROM CYA WHERE Date < '" + Strings.Format(dtDate, "MM/dd/yyyy") + "'", strDatebaseName);
                }
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                FCMessageBox.Show("Error #"
                                  + FCConvert.ToString(Information.Err(ex).Number)
                                  + " - "
                                  + Information.Err(ex).Description
                                  + ".", MsgBoxStyle.Critical, "Error Purging History Entries");
            }
        }

        #endregion

        #region Private "Worker" Methods

        #region Purge Methods
        private static int PurgeRecords(string sql)
        {
            var rsBill = GetBills(sql);
            NotifySetup(rsBill);

            var countProcessed = 0;

            while (!rsBill.EndOfFile())
            {
                var lienRecordNumber = rsBill.Get_Fields_Int32("LRN");
                var billId = rsBill.Get_Fields_Int32("BillKey");
                var isPersonalProperty = rsBill.Get_Fields_String("BillingType").ToLower() == "pp";
                PurgeBillingMaster(billId);
                if (isPersonalProperty)
                {
                    PurgePaymentRec(billId, "P");
                }
                else
                {
                    PurgePaymentRec(billId, "R");
                }
                if (lienRecordNumber > 0)
                {
                    PurgeLien(lienRecordNumber);
                    PurgePaymentRec(lienRecordNumber, "L");
                }
                //PurgeBillingMaster(billId);
                //PurgePaymentRec(lienRecordNumber, "L");

                NotifyProgress();
                countProcessed++;

                rsBill.MoveNext();
            }

            return countProcessed;
        }

        #region Get Bills to Purge

        #region Create Bill Retrieval SQL

        internal static string GetBillSql(short intType)
        {
            var sql = string.Empty;
            int selectedYear = 0;
            if (frmPurge.InstancePtr.cmbYear.SelectedIndex >= 0)
            {
                selectedYear = FCConvert.ToInt32(modExtraModules.FormatYear(frmPurge.InstancePtr.cmbYear
                    .Items[frmPurge.InstancePtr.cmbYear.SelectedIndex]
                    .ToString()));
            }

            switch (intType)
            {
                case 0:
                    {
                        sql = GetBillSql_RealEstate_Nonlien(selectedYear);

                        break;
                    }
                case 1:
                    {
                        sql = GetBillSql_RealEstate_Lien(selectedYear);

                        break;
                    }
                case 2:
                    {
                        sql = GetBillSql_PersonalProperty(selectedYear);

                        break;
                    }
            }

            return sql;
        }

        private static string GetBillSql_PersonalProperty(int selectedYear)
        {
            var topString = GetTopString();

            return
                //$"SELECT {topString} Account, ID, BillingYear, Name1, Name2, TaxDue1, TaxDue2, TaxDue3, TaxDue4, InterestCharged, BillingType, 0 as LRN FROM BillingMaster WHERE ID IN (SELECT Distinct BillKey FROM PaymentRec WHERE BillCode = 'P' AND [Year] = {selectedYear}) AND BillingYear = {selectedYear} AND (TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4) = PrincipalPaid AND LienRecordNumber = 0  AND BillingType = 'PP'";
            $"SELECT {topString} Account, ID as BillKey, BillingYear, Name1, Name2, TaxDue1, TaxDue2, TaxDue3, TaxDue4, InterestCharged, BillingType, 0 as LRN FROM BillingMaster WHERE BillingYear = {selectedYear} AND (TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4) = PrincipalPaid and interestpaid = -1 * interestcharged AND LienRecordNumber = 0  AND BillingType = 'PP'";
        }

        private static string GetBillSql_RealEstate_Lien(int selectedYear)
        {
            return
                //"SELECT Account, BillingMaster.ID as BillKey, BillingYear, Name1, Name2, Principal, Interest, Costs, BillingMaster.InterestCharged AS CHGINT, LienRec.InterestCharged AS InterestCharged, LienRec.ID AS LRN, BillingType " + "FROM LienRec INNER JOIN BillingMaster ON LienRec.ID = BillingMaster.LienRecordNumber " + $"WHERE LienRec.ID IN (SELECT Distinct BillKey FROM PaymentRec WHERE BillCode = 'L' AND [Year] = {selectedYear}) " + $" AND BillingYear = {selectedYear} " + " AND Principal = LienRec.PrincipalPaid " + " AND LienRec.ID <> 0  " + " AND BillingType = 'RE'";
            "SELECT Account, BillingMaster.ID as BillKey, BillingYear, Name1, Name2, Principal, Interest, Costs, BillingMaster.InterestCharged AS CHGINT, LienRec.InterestCharged AS InterestCharged, LienRec.ID AS LRN, BillingType " + "FROM LienRec INNER JOIN BillingMaster ON LienRec.ID = BillingMaster.LienRecordNumber " + $" WHERE BillingYear = {selectedYear} " + " AND LienRec.Principal = LienRec.PrincipalPaid " + " and LienRec.costs - lienrec.maturityfee = lienrec.costspaid and lienrec.interest = lienrec.plipaid and lienrec.interestpaid + lienrec.interestcharged = 0 AND LienRec.ID <> 0  " + " AND BillingType = 'RE'";
        }

        private static string GetBillSql_RealEstate_Nonlien(int selectedYear)
        {
            return
                //"SELECT Account, ID as BillKey, BillingYear, Name1, Name2, TaxDue1, TaxDue2, TaxDue3, TaxDue4, InterestCharged , LienRecordNumber as LRN, BillingType " + "FROM BillingMaster " + $"WHERE ID IN (SELECT Distinct BillKey FROM PaymentRec WHERE BillCode = 'R' AND [Year] = {selectedYear}) " + $" AND BillingYear = {selectedYear} " + " AND (TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4) = PrincipalPaid " + " AND LienRecordNumber = 0  " + " AND BillingType = 'RE'";
            "SELECT Account, ID as BillKey, BillingYear, Name1, Name2, TaxDue1, TaxDue2, TaxDue3, TaxDue4, InterestCharged , LienRecordNumber as LRN, BillingType " + "FROM BillingMaster " + $"WHERE  BillingYear = {selectedYear} " + " AND (TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4) = PrincipalPaid and interestpaid = -1 * interestcharged and demandfeespaid = demandfees " + " AND LienRecordNumber = 0  " + " AND BillingType = 'RE'";
        }

        private static string GetTopString()
        {
            var maxAllowed = Statics.lngMaxPurgeAccounts;
            var topString = maxAllowed > 0 ? $"TOP {maxAllowed}" : string.Empty;

            return topString;
        }

        #endregion

        #region Get Bills
        private static clsDRWrapper GetBills(string sql)
        {
            var rsBill = new clsDRWrapper();
            rsBill.OpenRecordset(sql, modExtraModules.strCLDatabase);

            return rsBill;
        }

        #endregion

        #endregion

        #region Create Bill and Payment Deletion SQL
        private static void PurgeBillingMaster(int billId)
        {
            var strSQL = $"DELETE FROM BillingMaster WHERE ID = {billId}";
            DoPurge(strSQL);
        }

        private static void PurgePaymentRec(int billKey, string billCode)
        {
            var strSQL = $"DELETE FROM PaymentRec WHERE BillKey = {billKey} AND BillCode = '{billCode}'";
            DoPurge(strSQL);
        }

        private static void PurgeLien(int lienId)
        {
            var strsql = $"Delete from LienRec where ID = {lienId}";
            DoPurge(strsql);
        }

        #endregion

        #region Delete the Records
        private static void DoPurge(string strSQL)
        {
            var rsPurge = new clsDRWrapper();
            rsPurge.Execute(strSQL, modExtraModules.strCLDatabase);
        }

        #endregion

        #endregion

        #region Notifications
        private static void NotifyStart(DateTime dtDate)
        {
            modGlobalFunctions.AddCYAEntry_26("CL", "Purge of Bills", dtDate.ToString());
            frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Purging Bills");
        }

        private static void NotifyFinish(short intType, int countPurged)
        {
            frmWait.InstancePtr.Unload();
            var billTypeName = string.Empty;
            switch (intType)
            {
                case 0:
                    {
                        billTypeName = "Real Estate Nonlien";

                        break;
                    }
                case 1:
                    {
                        billTypeName = "Real Estate Lien";

                        break;
                    }
                case 2:
                    {
                        billTypeName = "Personal Property";

                        break;
                    }
            }
            FCMessageBox.Show($"{countPurged} {billTypeName} bills have been purged.", MsgBoxStyle.Information, $"{billTypeName} Bills Purged");
        }

        private static void NotifySetup(clsDRWrapper rsBill)
        {
            frmWait.InstancePtr.prgProgress.Maximum = rsBill.RecordCount();
        }

        private static void NotifyProgress()
        {
            frmWait.InstancePtr.IncrementProgress();
        }

        #endregion

        #endregion

    }
}