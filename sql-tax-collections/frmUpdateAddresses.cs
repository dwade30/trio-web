﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmUpdateAddresses.
	/// </summary>
	public partial class frmUpdateAddresses : BaseForm
	{
		public frmUpdateAddresses()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmUpdateAddresses InstancePtr
		{
			get
			{
				return (frmUpdateAddresses)Sys.GetInstance(typeof(frmUpdateAddresses));
			}
		}

		protected frmUpdateAddresses _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               05/23/2006              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               06/13/2006              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsPrev = new clsDRWrapper();
		bool boolLoaded;
		clsDRWrapper rsRE = new clsDRWrapper();
		int lngBillingYear;
		string strRateKey;
		int lngColCheckBox;
		int lngColAccount;
		int lngColName;
		int lngColAddress1;
		int lngColAddress2;
		int lngColBillKey;
		int lngColHidden;

		public void Init(string strRateKeyList = "")
		{
			strRateKey = strRateKeyList;
			this.Show(App.MainForm);
		}

		private void cmbYear_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbYear.SelectedIndex >= 0)
			{
				if (!FillGrid())
				{
					// Unload Me
				}
			}
		}

		private void frmUpdateAddresses_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				boolLoaded = true;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				this.Text = "Update Bill Addresses";
				lblValidateInstruction.Text = "If these account changes are correct, Press F12 to advance.";
				App.DoEvents();
				FillYearCombo();
				lblValidateInstruction.Visible = true;
				vsDemand.Visible = true;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			}
		}

		private void frmUpdateAddresses_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmUpdateAddresses.Icon	= "frmUpdateAddresses.frx":0000";
			//frmUpdateAddresses.FillStyle	= 0;
			//frmUpdateAddresses.ScaleWidth	= 9195;
			//frmUpdateAddresses.ScaleHeight	= 7470;
			//frmUpdateAddresses.LinkTopic	= "Form2";
			//frmUpdateAddresses.LockControls	= -1  'True;
			//frmUpdateAddresses.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "9.75";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//vsDemand.BackColor	= "-2147483643";
			//			//vsDemand.ForeColor	= "-2147483640";
			//vsDemand.BorderStyle	= 1;
			//vsDemand.FillStyle	= 0;
			//vsDemand.Appearance	= 1;
			//vsDemand.GridLines	= 1;
			//vsDemand.WordWrap	= 0;
			//vsDemand.ScrollBars	= 3;
			//vsDemand.RightToLeft	= 0;
			//vsDemand._cx	= 15584;
			//vsDemand._cy	= 8744;
			//vsDemand._ConvInfo	= 1;
			//vsDemand.MousePointer	= 0;
			//vsDemand.BackColorFixed	= -2147483633;
			//			//vsDemand.ForeColorFixed	= -2147483630;
			//vsDemand.BackColorSel	= -2147483635;
			//			//vsDemand.ForeColorSel	= -2147483634;
			//vsDemand.BackColorBkg	= -2147483636;
			//vsDemand.BackColorAlternate	= -2147483643;
			//vsDemand.GridColor	= -2147483633;
			//vsDemand.GridColorFixed	= -2147483632;
			//vsDemand.TreeColor	= -2147483632;
			//vsDemand.FloodColor	= 192;
			//vsDemand.SheetBorder	= -2147483642;
			//vsDemand.FocusRect	= 1;
			//vsDemand.HighLight	= 1;
			//vsDemand.AllowSelection	= -1  'True;
			//vsDemand.AllowBigSelection	= -1  'True;
			//vsDemand.AllowUserResizing	= 0;
			//vsDemand.SelectionMode	= 0;
			//vsDemand.GridLinesFixed	= 2;
			//vsDemand.GridLineWidth	= 1;
			//vsDemand.RowHeightMin	= 0;
			//vsDemand.RowHeightMax	= 0;
			//vsDemand.ColWidthMin	= 0;
			//vsDemand.ColWidthMax	= 0;
			//vsDemand.ExtendLastCol	= -1  'True;
			//vsDemand.FormatString	= "";
			//vsDemand.ScrollTrack	= -1  'True;
			//vsDemand.ScrollTips	= 0   'False;
			//vsDemand.MergeCells	= 0;
			//vsDemand.MergeCompare	= 0;
			//vsDemand.AutoResize	= -1  'True;
			//vsDemand.AutoSizeMode	= 0;
			//vsDemand.AutoSearch	= 0;
			//vsDemand.AutoSearchDelay	= 2;
			//vsDemand.MultiTotals	= -1  'True;
			//vsDemand.SubtotalPosition	= 1;
			//vsDemand.OutlineBar	= 0;
			//vsDemand.OutlineCol	= 0;
			//vsDemand.Ellipsis	= 0;
			//vsDemand.ExplorerBar	= 5;
			//vsDemand.PicturesOver	= 0   'False;
			//vsDemand.PictureType	= 0;
			//vsDemand.TabBehavior	= 0;
			//vsDemand.OwnerDraw	= 0;
			//vsDemand.ShowComboButton	= -1  'True;
			//vsDemand.TextStyle	= 0;
			//vsDemand.TextStyleFixed	= 0;
			//vsDemand.OleDragMode	= 0;
			//vsDemand.OleDropMode	= 0;
			//vsDemand.DataMode	= 0;
			//vsDemand.VirtualData	= -1  'True;
			//vsDemand.ComboSearch	= 3;
			//vsDemand.AutoSizeMouse	= -1  'True;
			//vsDemand.AllowUserFreezing	= 0;
			//vsDemand.BackColorFrozen	= 0;
			//			//vsDemand.ForeColorFrozen	= 0;
			//vsDemand.WallPaperAlignment	= 9;
			//vsElasticLight1.OleObjectBlob	= "frmUpdateAddresses.frx":058A";
			//End Unmaped Properties
			lngColCheckBox = 0;
			lngColAccount = 1;
			lngColName = 2;
			lngColAddress1 = 3;
			lngColAddress2 = 4;
			lngColBillKey = 5;
			lngColHidden = 6;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			FCUtils.EraseSafe(modGlobal.Statics.arrDemand);
		}

		private void frmUpdateAddresses_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				Close();
			}
			else if (KeyAscii == 13)
			{
				KeyAscii = 0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		private void frmUpdateAddresses_Resize(object sender, System.EventArgs e)
		{
			FormatGrid(true);
			lblValidateInstruction.Visible = true;
			vsDemand.Visible = true;
		}

		private void mnuFileClear_Click(object sender, System.EventArgs e)
		{
			ClearCheckBoxes();
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void FormatGrid(bool boolResize = false)
		{
			int wid = 0;
			if (!boolResize)
			{
				vsDemand.Rows = 1;
			}
			vsDemand.Cols = 7;
			wid = vsDemand.WidthOriginal;
			vsDemand.ColWidth(lngColCheckBox, FCConvert.ToInt32(wid * 0.1));
			// Checkbox
			vsDemand.ColWidth(lngColAccount, FCConvert.ToInt32(wid * 0.1));
			// Acct
			vsDemand.ColWidth(lngColName, FCConvert.ToInt32(wid * 0.25));
			// Name
			vsDemand.ColWidth(lngColAddress1, FCConvert.ToInt32(wid * 0.25));
			// Bill Address
			vsDemand.ColWidth(lngColAddress2, FCConvert.ToInt32(wid * 0.25));
			// Master Address
			vsDemand.ColWidth(lngColBillKey, 0);
			// Hidden Key Field (Bill Key)
			vsDemand.ColWidth(lngColHidden, 0);
			// Hidden Code Field - 0 is ok to process...anything else is bad
			vsDemand.ColDataType(lngColCheckBox, FCGrid.DataTypeSettings.flexDTBoolean);
			vsDemand.ColAlignment(lngColAccount, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(lngColName, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(lngColAddress1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(lngColAddress2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.TextMatrix(0, lngColAccount, "Account");
			vsDemand.TextMatrix(0, lngColName, "Name");
			vsDemand.TextMatrix(0, lngColAddress1, "Bill Address");
			vsDemand.TextMatrix(0, lngColAddress2, "Master Address");
		}

		private bool FillGrid()
		{
			bool FillGrid = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the grid with eligible accounts
				string strSQL = "";
				int lngIndex;
				double dblXInt = 0;
				// VBto upgrade warning: lngYear As int	OnWrite(string)
				int lngYear;
				string strTemp = "";
				lngYear = FCConvert.ToInt32(modExtraModules.FormatYear(cmbYear.Items[cmbYear.SelectedIndex].ToString()));
				FillGrid = true;
				if (lngYear > 10000)
				{
					frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Info");
					vsDemand.Rows = 1;
					// This function will go through any accounts that have had an address change
					// since the last sale or commitment and then update the address on the last bill

                    strSQL =
                        "SELECT BillingMaster.*, BillingMaster.ID AS BillKey, Master.OwnerPartyID ,DeedName1, DeedName2 ";
                    strSQL += "FROM BillingMaster INNER JOIN " + modGlobal.Statics.strDbRE + "Master ON BillingMaster.Account = Master.RSAccount " + "WHERE BillingType = 'RE' AND LienRecordNumber = 0 AND (TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4 - PrincipalPaid) > 0 AND RateKey > 0 AND RSCard = 1 AND BillingYear = " + FCConvert.ToString(lngYear) + " ORDER BY Account";
					// this is a list of account from non liened records that have a different address
					rsData.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
					rsData.InsertName("OwnerPartyID", "Own1", false, true, true, "", false, "", true, "Name1 <> DeedName1 AND (Address1 <> Own1Address1 OR Address2 <> Own1Address2 OR Left(Address3, 3) <> Left(Own1City, 3))");
					rsRE.OpenRecordset("SELECT * FROM Master", modExtraModules.strREDatabase);
					if (rsData.EndOfFile())
					{
						frmWait.InstancePtr.Unload();
						FCMessageBox.Show("There are no accounts eligible to have addresses updated.", MsgBoxStyle.Information, "No Eligible Accounts");
						FillGrid = false;
						return FillGrid;
					}
					lngIndex = -1;
					while (!rsData.EndOfFile())
					{
						App.DoEvents();
						dblXInt = 0;
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (modCLCalculations.CalculateAccountCL(ref rsData, FCConvert.ToInt32(rsData.Get_Fields("Account")), DateTime.Today, ref dblXInt) <= 0)
						{
							// skip this account
							goto SKIP;
						}
						if (FCUtils.IsValidDateTime(rsData.Get_Fields_DateTime("TransferFromBillingDateFirst")))
						{
							if (((DateTime)rsData.Get_Fields_DateTime("TransferFromBillingDateFirst")).Month >= 4)
							{
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								rsPrev.OpenRecordset("SELECT * FROM PreviousOwner WHERE Account = " + FCConvert.ToString(rsData.Get_Fields("Account")) + " AND DateCreated > '04/01/" + FCConvert.ToString(rsData.Get_Fields_DateTime("TransferFromBillingDateFirst").Year) + "' ORDER BY DateCreated desc", modExtraModules.strREDatabase);
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								rsPrev.OpenRecordset("SELECT * FROM PreviousOwner WHERE Account = " + FCConvert.ToString(rsData.Get_Fields("Account")) + " AND DateCreated > '04/01/" + FCConvert.ToString(rsData.Get_Fields_DateTime("TransferFromBillingDateFirst").Year - 1) + "' ORDER BY DateCreated desc", modExtraModules.strREDatabase);
							}
						}
						if (rsPrev.EndOfFile())
						{
							// no previous owner records so no need to show this bill
							goto SKIP;
						}
						// This will show what is different
						strTemp = "";
						if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address1"))) != Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("Address1"))))
						{
							if (strTemp != "")
								strTemp += "  ";
							strTemp = "Address 1 : '" + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address1"))) + "' - '" + Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("Address1"))) + "'";
						}
						if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address2"))) != Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("Address2"))))
						{
							if (strTemp != "")
								strTemp += "  ";
							strTemp += "Address 2 : '" + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address2"))) + "' - '" + Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("Address2"))) + "'";
						}
						if (Strings.Trim(Strings.Left(FCConvert.ToString(rsData.Get_Fields_String("Address3")), 3)) != Strings.Trim(Strings.Left(FCConvert.ToString(rsPrev.Get_Fields_String("City")), 3)))
						{
							if (strTemp != "")
								strTemp += "  ";
							strTemp += "Address 3 : '" + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address3"))) + "' - '" + Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("City"))) + "'";
						}
						if (strTemp != "")
						{
							// add a row/element
							vsDemand.AddItem("");
							Array.Resize(ref modGlobal.Statics.arrDemand, vsDemand.Rows - 1 + 1);
							// this will make sure that there are enough elements to use
							lngIndex = vsDemand.Rows - 2;
							//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
							modGlobal.Statics.arrDemand[lngIndex] = new modGlobal.AccountFeesAdded(0);
							modGlobal.Statics.arrDemand[lngIndex].Used = true;
							modGlobal.Statics.arrDemand[lngIndex].Processed = false;
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							vsDemand.TextMatrix(vsDemand.Rows - 1, lngColAccount, FCConvert.ToString(rsData.Get_Fields("Account")));
							// account number
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							modGlobal.Statics.arrDemand[lngIndex].Account = FCConvert.ToInt32(rsData.Get_Fields("Account"));
							vsDemand.TextMatrix(vsDemand.Rows - 1, lngColName, FCConvert.ToString(rsData.Get_Fields_String("Name1")));
							// name
							modGlobal.Statics.arrDemand[lngIndex].Name = FCConvert.ToString(rsData.Get_Fields_String("Name1"));
							vsDemand.TextMatrix(vsDemand.Rows - 1, lngColAddress1, FCConvert.ToString(rsData.Get_Fields_String("Address1")) + " " + FCConvert.ToString(rsData.Get_Fields_String("Address2")) + " " + FCConvert.ToString(rsData.Get_Fields_String("Address3")));
							modGlobal.Statics.arrDemand[lngIndex].OldAddress = FCConvert.ToString(rsData.Get_Fields_String("Address1")) + " " + FCConvert.ToString(rsData.Get_Fields_String("Address2")) + " " + FCConvert.ToString(rsData.Get_Fields_String("Address3"));
							vsDemand.TextMatrix(vsDemand.Rows - 1, lngColAddress2, FCConvert.ToString(rsPrev.Get_Fields_String("Address1")) + " " + FCConvert.ToString(rsPrev.Get_Fields_String("Address2")) + " " + FCConvert.ToString(rsPrev.Get_Fields_String("City")) + " " + FCConvert.ToString(rsPrev.Get_Fields_String("State")) + " " + FCConvert.ToString(rsPrev.Get_Fields_String("Zip")) + " " + FCConvert.ToString(rsPrev.Get_Fields_String("Zip4")));
							modGlobal.Statics.arrDemand[lngIndex].NewAddress = FCConvert.ToString(rsPrev.Get_Fields_String("Address1")) + " " + FCConvert.ToString(rsPrev.Get_Fields_String("Address2")) + " " + FCConvert.ToString(rsPrev.Get_Fields_String("City")) + " " + FCConvert.ToString(rsPrev.Get_Fields_String("State")) + " " + FCConvert.ToString(rsPrev.Get_Fields_String("Zip")) + " " + FCConvert.ToString(rsPrev.Get_Fields_String("zip4"));
							vsDemand.TextMatrix(vsDemand.Rows - 1, lngColBillKey, FCConvert.ToString(rsData.Get_Fields_Int32("BillKey")));
							// billkey
							// If Trim(rsData.Get_Fields("Address1")) <> Trim(rsData.Get_Fields("RSAddr1")) Then
							// If strTemp <> "" Then strTemp = strTemp & "  "
							// strTemp = "Address 1 : '" & Trim(rsData.Get_Fields("Address1")) & "' - '" & Trim(rsData.Get_Fields("RSAddr1")) & "'"
							// End If
							// If Trim(rsData.Get_Fields("Address2")) <> Trim(rsData.Get_Fields("RSAddr2")) Then
							// If strTemp <> "" Then strTemp = strTemp & "  "
							// strTemp = strTemp & "Address 2 : '" & Trim(rsData.Get_Fields("Address2")) & "' - '" & Trim(rsData.Get_Fields("RSAddr2")) & "'"
							// End If
							// If Trim(Left(rsData.Get_Fields("Address3"), 3)) <> Trim(Left(rsData.Get_Fields("RSAddr3"), 3)) Then
							// If strTemp <> "" Then strTemp = strTemp & "  "
							// strTemp = strTemp & "Address 3 : '" & Trim(rsData.Get_Fields("Address3")) & "' - '" & Trim(rsData.Get_Fields("RSAddr3")) & "'"
							// End If
							vsDemand.TextMatrix(vsDemand.Rows - 1, lngColHidden, strTemp);
						}
						SKIP:
						;
						rsData.MoveNext();
					}
					if (vsDemand.Rows == 1)
					{
						frmWait.InstancePtr.Unload();
						FCMessageBox.Show("There are no accounts eligible to have addresses updated.", MsgBoxStyle.Information, "No Eligible Accounts");
						FillGrid = false;
						return FillGrid;
					}
					// If lngIndex < 0 Then
					// Unload frmWait
					// MsgBox "There are no accounts eligible to have demand fees applied.", MsgBoxStyle.Information, "No Eligible Accounts"
					// Unload Me
					// End If
					// check all of the accounts
					mnuFileSelectAll_Click();
				}
				else
				{
					frmWait.InstancePtr.Unload();
					FCMessageBox.Show("Please enter a valid year.", MsgBoxStyle.Exclamation, "Valid Year");
				}
				frmWait.InstancePtr.Unload();
				return FillGrid;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Filling Grid");
			}
			return FillGrid;
		}

		private void FillYearCombo()
		{
			// this will fill the validate grid with the information from the Control_30DayNotice table
			// which is the information that was used to print the 30 Day Notices the last time
			clsDRWrapper rsValidate = new clsDRWrapper();
			cmbYear.Clear();
			rsValidate.OpenRecordset("SELECT DISTINCT BillingYear FROM BillingMaster ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
			if (rsValidate.EndOfFile())
			{
				FCMessageBox.Show("No valid years to process.", MsgBoxStyle.Information, "Valid Year");
				Close();
			}
			else
			{
				while (!rsValidate.EndOfFile())
				{
					cmbYear.AddItem(modExtraModules.FormatYear(FCConvert.ToString(rsValidate.Get_Fields_Int32("BillingYear"))));
					rsValidate.MoveNext();
				}
			}
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			// Select Case intAction
			// Case 0
			// this will say that the user accepts the parameters shown
			// MsgBox "After demand fees are applied, it will not be possible to reprint the notices, labels or certified mailers.", MsgBoxStyle.Exclamation, "Apply Demand Fees"
			// SetAct 1
			// Case 1
			// apply demand fees
			ApplyAddressChanges();
			Close();
			// End Select
		}

		private void ApplyAddressChanges()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngCT;
				int lngDemandCount = 0;
				clsDRWrapper rsCL = new clsDRWrapper();
				clsDRWrapper rsRE = new clsDRWrapper();
				frmWait.InstancePtr.Init("Processing..." + "\r\n" + "Applying Changes");
				for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
				{
					// for each account in the grid
					App.DoEvents();
					if (Conversion.Val(vsDemand.TextMatrix(lngCT, lngColCheckBox)) == -1)
					{
						// check to see if the check box if checked
						if (Conversion.Val(vsDemand.TextMatrix(lngCT, lngColHidden)) >= 0)
						{
							// then what?
							rsCL.OpenRecordset("SELECT * FROM BillingMaster WHERE BillKey = " + vsDemand.TextMatrix(lngCT, lngColBillKey), modExtraModules.strCLDatabase);
							rsRE.OpenRecordset("SELECT * FROM Master WHERE RSCard = 1 AND RSAccount = " + vsDemand.TextMatrix(lngCT, lngColAccount), modExtraModules.strREDatabase);
							if (((DateTime)rsCL.Get_Fields_DateTime("TransferFromBillingDateFirst")).Month >= 4)
							{
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								rsRE.OpenRecordset("SELECT * FROM PreviousOwner WHERE Account = " + FCConvert.ToString(rsCL.Get_Fields("Account")) + " AND DateCreated > '04/01/" + FCConvert.ToString(rsCL.Get_Fields_DateTime("TransferFromBillingDateFirst").Year) + "' ORDER BY DateCreated desc", modExtraModules.strREDatabase);
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								rsRE.OpenRecordset("SELECT * FROM PreviousOwner WHERE Account = " + FCConvert.ToString(rsCL.Get_Fields("Account")) + " AND DateCreated > '04/01/" + FCConvert.ToString(rsCL.Get_Fields_DateTime("TransferFromBillingDateFirst").Year - 1) + "' ORDER BY DateCreated desc", modExtraModules.strREDatabase);
							}
							if (rsRE.EndOfFile() || rsCL.EndOfFile())
							{
								FCMessageBox.Show("Error processing account " + vsDemand.TextMatrix(lngCT, lngColAccount) + ".  No demand fees were applied.", MsgBoxStyle.Critical, "Cannot Find BillKey - " + vsDemand.TextMatrix(lngCT, lngColBillKey));
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								modGlobal.Statics.arrDemand[FindDemandIndex_2(FCConvert.ToInt32(rsCL.Get_Fields("Account")))].Processed = true;
								// AddDemandPayment CDbl(vsDemand.TextMatrix(lngCT, 4))
								rsCL.Edit();
								// rsCL.Get_Fields("Address1") = rsRE.Get_Fields("RSAddr1")
								// rsCL.Get_Fields("Address2") = rsRE.Get_Fields("RSAddr2")
								// rsCL.Get_Fields("Address3") = rsRE.Get_Fields("RSAddr3") & " " & rsRE.Get_Fields("RSState") & " " & rsRE.Get_Fields("RSZip")
								// If Trim(rsRE.Get_Fields("RSZip4")) <> "" Then
								// rsCL.Get_Fields("Address3") = rsCL.Get_Fields("Address3") & "-" & Trim(rsRE.Get_Fields("RSZip4"))
								// End If
								rsCL.Set_Fields("Address1", rsRE.Get_Fields_String("Address1"));
								rsCL.Set_Fields("Address2", rsRE.Get_Fields_String("Address2"));
								rsCL.Set_Fields("Address3", FCConvert.ToString(rsRE.Get_Fields_String("City")) + " " + FCConvert.ToString(rsRE.Get_Fields_String("State")) + " " + FCConvert.ToString(rsRE.Get_Fields_String("Zip")));
								if (Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("Zip4"))) != "")
								{
									rsCL.Set_Fields("Address3", FCConvert.ToString(rsCL.Get_Fields_String("Address3")) + "-" + Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("Zip4"))));
								}
								rsCL.Update();
								lngDemandCount += 1;
							}
						}
						else
						{
							// not eligible...do nothing
						}
					}
					else
					{
						// not selected...do nothing
					}
				}
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show(FCConvert.ToString(lngDemandCount) + " accounts were affected.", MsgBoxStyle.Information, "Addresses Updated");
				// print the list of accounts that have been affected
				rptUpdatedAddressList.InstancePtr.Init(lngDemandCount, cmbYear.Items[cmbYear.SelectedIndex].ToString());
				frmReportViewer.InstancePtr.Init(rptUpdatedAddressList.InstancePtr);
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Updating");
			}
		}

		private int FindDemandIndex_2(int lngAcct)
		{
			return FindDemandIndex(ref lngAcct);
		}

		private int FindDemandIndex(ref int lngAcct)
		{
			int FindDemandIndex = 0;
			int lngCT;
			for (lngCT = 0; lngCT <= Information.UBound(modGlobal.Statics.arrDemand, 1); lngCT++)
			{
				App.DoEvents();
				if (lngAcct == modGlobal.Statics.arrDemand[lngCT].Account)
				{
					FindDemandIndex = lngCT;
					break;
				}
			}
			return FindDemandIndex;
		}

		private void mnuFileSelectAll_Click(object sender, System.EventArgs e)
		{
			// this will select all account in the grid
			int lngCT;
			for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
			{
				vsDemand.TextMatrix(lngCT, lngColCheckBox, FCConvert.ToString(-1));
			}
		}

		public void mnuFileSelectAll_Click()
		{
			mnuFileSelectAll_Click(cmdFileSelectAll, new System.EventArgs());
		}

		private void vsDemand_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			// this should not allow an account that already has demand fees to be checked
			if (vsDemand.Col == 0)
			{
				if (Conversion.Val(vsDemand.TextMatrix(vsDemand.Row, lngColHidden)) != 0)
				{
					vsDemand.TextMatrix(vsDemand.Row, lngColCheckBox, FCConvert.ToString(0));
				}
			}
		}

		private void vsDemand_DblClick(object sender, EventArgs e)
		{
			int lngMR;
			int lngMC;
			lngMR = vsDemand.MouseRow;
			lngMC = vsDemand.MouseCol;
			if (lngMR == 0 && lngMC == lngColCheckBox)
			{
				mnuFileSelectAll_Click();
			}
		}

		//private void vsDemand_MouseMove(object sender, DataGridViewCellMouseEventArgs e)
		//{
		//	int lngMR;
		//	int lngMC;
		//	lngMR = vsDemand.MouseRow;
		//	lngMC = vsDemand.MouseCol;
		//	if (lngMR > 0 && lngMC > 1)
		//	{
		//		ToolTip1.SetToolTip(vsDemand, Strings.Trim(vsDemand.TextMatrix(lngMR, lngColHidden)));
		//	}
		//}

        private void vsDemand_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            int lngMR;
            int lngMC;
            lngMR = vsDemand.GetFlexRowIndex(e.RowIndex);
            lngMC = vsDemand.GetFlexColIndex(e.ColumnIndex);
            if (lngMR > 0 && lngMC > 1)
            {
                DataGridViewCell cell = vsDemand[e.ColumnIndex, e.RowIndex];
                cell.ToolTipText = Strings.Trim(vsDemand.TextMatrix(lngMR, lngColHidden));
            }
        }

        private void vsDemand_RowColChange(object sender, EventArgs e)
		{
			if (vsDemand.Col == lngColCheckBox)
			{
				vsDemand.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else
			{
				vsDemand.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void ClearCheckBoxes()
		{
			// this will set all of the textboxes to unchecked
			int lngCT;
			for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
			{
				vsDemand.TextMatrix(lngCT, lngColCheckBox, FCConvert.ToString(0));
			}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFilePrint_Click(sender, e);
		}

		private void cmdFileSelectAll_Click(object sender, EventArgs e)
		{
			this.mnuFileSelectAll_Click(sender, e);
		}

		private void cmdFileClear_Click(object sender, EventArgs e)
		{
			this.mnuFileClear_Click(sender, e);
		}
	}
}
