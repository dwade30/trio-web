﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmReverseDemandFees.
	/// </summary>
	public partial class frmReverseDemandFees : BaseForm
	{
		public frmReverseDemandFees()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmReverseDemandFees InstancePtr
		{
			get
			{
				return (frmReverseDemandFees)Sys.GetInstance(typeof(frmReverseDemandFees));
			}
		}

		protected frmReverseDemandFees _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               07/29/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               06/24/2004              *
		// ********************************************************
		int intAction;
		clsDRWrapper rsData = new clsDRWrapper();
		bool boolLoaded;
		double dblDemand;
		double dblCertMailFee;
		bool boolChargeMort;
		bool boolChargeCert;
		DateTime dtMailDate;
		bool boolLoading;
		string strRateKeyList;
		// here are the col number variables
		int lngColCheck;
		int lngColAccount;
		int lngColYear;
		int lngColName;
		int lngColNotices;
		int lngColAmount;
		int lngColBillKey;
		int lngColProcess;
		int lngColIndex;

		public void Init(ref string strPassRateKeyList)
		{
			strRateKeyList = strPassRateKeyList;
			this.Show(App.MainForm);
		}

		private void frmReverseDemandFees_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				boolLoaded = true;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				if (!FillValidateInfo())
				{
					Close();
				}
				//FC:FINAL:AM: check if the form has been closed
				if (!this.IsDisposed)
				{
					this.Text = "Reverse Demand Fees";
					// lblInstructionHeader.Visible = False
					// MAL@20071011: Changed wording
					// Program Bug Reference: 6478
					// lblInstruction.Text = "To Reverse Demand Fees:" & vbCrLf & "     1. Check the box beside the account." & vbCrLf & "     2. Select Reverse Demand Fees or press F12."
					lblInstruction.Text = "To Reverse Demand Fees:" + "\r\n" + "     1. Check the box beside the account." + "\r\n" + "     2. Select Reverse Demand Fees or press F12.";
					App.DoEvents();
					FormatGrid();
					FillDemandGrid();
					if (!this.IsDisposed)
					{
						SetAct(1);
					}
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			}
		}

		private void frmReverseDemandFees_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmReverseDemandFees.Icon	= "frmReverseDemandFees.frx":0000";
			//frmReverseDemandFees.FillStyle	= 0;
			//frmReverseDemandFees.ScaleWidth	= 9195;
			//frmReverseDemandFees.ScaleHeight	= 7350;
			//frmReverseDemandFees.LinkTopic	= "Form2";
			//frmReverseDemandFees.LockControls	= -1  'True;
			//frmReverseDemandFees.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "9.75";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//vsDemand.BackColor	= "-2147483643";
			//			//vsDemand.ForeColor	= "-2147483640";
			//vsDemand.BorderStyle	= 1;
			//vsDemand.FillStyle	= 0;
			//vsDemand.Appearance	= 1;
			//vsDemand.GridLines	= 1;
			//vsDemand.WordWrap	= 0;
			//vsDemand.ScrollBars	= 3;
			//vsDemand.RightToLeft	= 0;
			//vsDemand._cx	= 15584;
			//vsDemand._cy	= 8387;
			//vsDemand._ConvInfo	= 1;
			//vsDemand.MousePointer	= 0;
			//vsDemand.BackColorFixed	= -2147483633;
			//			//vsDemand.ForeColorFixed	= -2147483630;
			//vsDemand.BackColorSel	= -2147483635;
			//			//vsDemand.ForeColorSel	= -2147483634;
			//vsDemand.BackColorBkg	= -2147483636;
			//vsDemand.BackColorAlternate	= -2147483643;
			//vsDemand.GridColor	= -2147483633;
			//vsDemand.GridColorFixed	= -2147483632;
			//vsDemand.TreeColor	= -2147483632;
			//vsDemand.FloodColor	= 192;
			//vsDemand.SheetBorder	= -2147483642;
			//vsDemand.FocusRect	= 1;
			//vsDemand.HighLight	= 1;
			//vsDemand.AllowSelection	= -1  'True;
			//vsDemand.AllowBigSelection	= -1  'True;
			//vsDemand.AllowUserResizing	= 0;
			//vsDemand.SelectionMode	= 0;
			//vsDemand.GridLinesFixed	= 2;
			//vsDemand.GridLineWidth	= 1;
			//vsDemand.RowHeightMin	= 0;
			//vsDemand.RowHeightMax	= 0;
			//vsDemand.ColWidthMin	= 0;
			//vsDemand.ColWidthMax	= 0;
			//vsDemand.ExtendLastCol	= -1  'True;
			//vsDemand.FormatString	= "";
			//vsDemand.ScrollTrack	= -1  'True;
			//vsDemand.ScrollTips	= 0   'False;
			//vsDemand.MergeCells	= 0;
			//vsDemand.MergeCompare	= 0;
			//vsDemand.AutoResize	= -1  'True;
			//vsDemand.AutoSizeMode	= 0;
			//vsDemand.AutoSearch	= 0;
			//vsDemand.AutoSearchDelay	= 2;
			//vsDemand.MultiTotals	= -1  'True;
			//vsDemand.SubtotalPosition	= 1;
			//vsDemand.OutlineBar	= 0;
			//vsDemand.OutlineCol	= 0;
			//vsDemand.Ellipsis	= 0;
			//vsDemand.ExplorerBar	= 1;
			//vsDemand.PicturesOver	= 0   'False;
			//vsDemand.PictureType	= 0;
			//vsDemand.TabBehavior	= 0;
			//vsDemand.OwnerDraw	= 0;
			//vsDemand.ShowComboButton	= -1  'True;
			//vsDemand.TextStyle	= 0;
			//vsDemand.TextStyleFixed	= 0;
			//vsDemand.OleDragMode	= 0;
			//vsDemand.OleDropMode	= 0;
			//vsDemand.DataMode	= 0;
			//vsDemand.VirtualData	= -1  'True;
			//vsDemand.ComboSearch	= 3;
			//vsDemand.AutoSizeMouse	= -1  'True;
			//vsDemand.AllowUserFreezing	= 0;
			//vsDemand.BackColorFrozen	= 0;
			//			//vsDemand.ForeColorFrozen	= 0;
			//vsDemand.WallPaperAlignment	= 9;
			//vsElasticLight1.OleObjectBlob	= "frmReverseDemandFees.frx":058A";
			//End Unmaped Properties
			boolLoading = true;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			boolLoading = false;
			// here are the col number variables
			lngColCheck = 0;
			lngColAccount = 1;
			lngColYear = 2;
			lngColName = 3;
			lngColBillKey = 4;
			lngColProcess = 5;
			lngColNotices = 6;
			lngColIndex = 7;
			lngColAmount = 8;
			// Erase arrDemand
		}

		private void frmReverseDemandFees_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				Close();
			}
			else if (KeyAscii == 13)
			{
				KeyAscii = 0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		//FC:FINAL:AM:#i145 - don't format the grid on each resize
		//private void frmReverseDemandFees_Resize(object sender, System.EventArgs e)
		//{
		//          if (!boolLoading)
		//          {
		//              FormatGrid(true);
		//          }
		//      }
		private void mnuFileClear_Click(object sender, System.EventArgs e)
		{
			ClearCheckBoxes();
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void FormatGrid(bool boolResize = false)
		{
			int wid = 0;
			if (!boolResize)
			{
				vsDemand.Rows = 1;
			}
			vsDemand.Cols = 9;
			wid = vsDemand.WidthOriginal;
			vsDemand.ColWidth(lngColCheck, FCConvert.ToInt32(wid * 0.05));
			// checkbox
			vsDemand.ColWidth(lngColAccount, FCConvert.ToInt32(wid * 0.1));
			// Acct
			vsDemand.ColWidth(lngColYear, FCConvert.ToInt32(wid * 0.1));
			// Year
			vsDemand.ColWidth(lngColName, FCConvert.ToInt32(wid * 0.45));
			// Name
			vsDemand.ColWidth(lngColNotices, FCConvert.ToInt32(wid * 0.05));
			// Total Notices Sent
			vsDemand.ColWidth(lngColAmount, FCConvert.ToInt32(wid * 0.1));
			// Total Demand Fees
			vsDemand.ColWidth(lngColBillKey, 0);
			// Hidden Key Field
			vsDemand.ColWidth(lngColProcess, 0);
			// Hidden Code Field - 0 is ok to process...anything else is bad
			vsDemand.ColWidth(lngColIndex, 0);
			// Hidden Index into the demand array
			vsDemand.ColFormat(lngColAmount, "#,##0.00");
			vsDemand.ColDataType(lngColCheck, FCGrid.DataTypeSettings.flexDTBoolean);
			vsDemand.ColAlignment(lngColAccount, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(lngColYear, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(lngColName, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(lngColNotices, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(lngColAmount, FCGrid.AlignmentSettings.flexAlignRightCenter);
			// table col titles
			vsDemand.TextMatrix(0, lngColCheck, "");
			vsDemand.TextMatrix(0, lngColAccount, "Account");
			vsDemand.TextMatrix(0, lngColYear, "Year");
			vsDemand.TextMatrix(0, lngColName, "Name");
			vsDemand.TextMatrix(0, lngColNotices, "#");
			vsDemand.TextMatrix(0, lngColAmount, "Amount");
			vsDemand.ExtendLastCol = true;
		}

		private void FillDemandGrid()
		{
			// this will fill the grid with eligible accounts
			string strSQL;
			int lngIndex = 0;
			vsDemand.Rows = 1;
			// this will get all acocunts that have have demand fees applied but have NOT had tranfer tax to lien run
			strSQL = "SELECT * FROM BillingMaster WHERE RateKey IN " + strRateKeyList + " AND (LienProcessStatus = 2 OR LienProcessStatus = 3) AND BillingType = 'RE' ORDER BY Name1";
			// this will be all the records that have had 30 Day Notices printed
			rsData.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
			if (rsData.EndOfFile())
			{
				FCMessageBox.Show("There are no accounts eligible to have demand fees reversed.", MsgBoxStyle.Information, "No Eligible Accounts");
				Close();
			}
			while (!rsData.EndOfFile())
			{
				// add a row/element
				vsDemand.AddItem("");
				Array.Resize(ref modGlobal.Statics.arrDemand, vsDemand.Rows - 1 + 1);
				// this will make sure that there are enough elements to use
				lngIndex = vsDemand.Rows - 2;
				//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
				modGlobal.Statics.arrDemand[lngIndex] = new modGlobal.AccountFeesAdded(0);
				modGlobal.Statics.arrDemand[lngIndex].Used = true;
				modGlobal.Statics.arrDemand[lngIndex].Processed = false;
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				vsDemand.TextMatrix(vsDemand.Rows - 1, lngColAccount, rsData.Get_Fields("Account"));
				// Account Number
				vsDemand.TextMatrix(vsDemand.Rows - 1, lngColYear, modExtraModules.FormatYear(FCConvert.ToString(rsData.Get_Fields_Int32("BillingYear"))));
				// Year
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				modGlobal.Statics.arrDemand[lngIndex].Account = FCConvert.ToInt32(rsData.Get_Fields("Account"));
				vsDemand.TextMatrix(vsDemand.Rows - 1, lngColName, FCConvert.ToString(rsData.Get_Fields_String("Name1")));
				// Name
				modGlobal.Statics.arrDemand[lngIndex].Name = FCConvert.ToString(rsData.Get_Fields_String("Name1"));
				// TODO Get_Fields: Check the table for the column [Copies] and replace with corresponding Get_Field method
				vsDemand.TextMatrix(vsDemand.Rows - 1, lngColNotices, rsData.Get_Fields("Copies"));
				// Number of copies
				vsDemand.TextMatrix(vsDemand.Rows - 1, lngColIndex, lngIndex);
				// fill the index of this account in the array
				// Amount
				if (((rsData.Get_Fields_Decimal("DemandFees"))) == 0)
				{
					// highlight the row and set the value = zero
					//					vsDemand.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vsDemand.Rows - 1, 0, vsDemand.Rows - 1, vsDemand.Cols - 1, System.Drawing.Color.Red);
					vsDemand.TextMatrix(vsDemand.Rows - 1, lngColAmount, FCConvert.ToString(0));
					vsDemand.TextMatrix(vsDemand.Rows - 1, lngColProcess, FCConvert.ToString(-1));
				}
				else
				{
					// use the amount in the bill record
					modGlobal.Statics.arrDemand[lngIndex].Fee = Conversion.Val(rsData.Get_Fields_Decimal("DemandFees"));
					vsDemand.TextMatrix(vsDemand.Rows - 1, lngColAmount, FCConvert.ToString(rsData.Get_Fields_Decimal("DemandFees")));
					vsDemand.TextMatrix(vsDemand.Rows - 1, lngColProcess, FCConvert.ToString(0));
				}
				vsDemand.TextMatrix(vsDemand.Rows - 1, lngColBillKey, FCConvert.ToString(rsData.Get_Fields_Int32("ID")));
				// BillKey
				rsData.MoveNext();
			}
			// check all of the accounts
			// mnuFileSelectAll_Click  'turned off 6/24 because mike wants it that way
		}

		private void SetAct(short intAct)
		{
			// this will change all of the menu options
			switch (intAct)
			{
				case 1:
					{
						cmdFileSelectAll.Visible = true;
						btnProcess.Text = "Reverse Demand Fees";
						intAction = 1;
						break;
					}
			}
			//end switch
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			switch (intAction)
			{
				case 0:
					{
						// this will say that the user accepts the parameters shown
						SetAct(1);
						break;
					}
				case 1:
					{
						// apply demand fees
						ApplyDemandFees();
						Close();
						break;
					}
			}
			//end switch
		}

		private void ApplyDemandFees()
		{
			int lngCT;
			int lngDemandCount = 0;
			string strName = "";
			if (FCMessageBox.Show("Are you sure that you would like to reverse these demand fees?", MsgBoxStyle.Question | MsgBoxStyle.YesNoCancel, "Reverse Demand Fees") == DialogResult.Yes)
			{
				GET_NAME:
				;
				strName = Interaction.InputBox("Please enter your name here", "");
				if (Strings.Trim(strName) == "")
				{
					goto GET_NAME;
				}
				// keep going and add an entry in the CYA table
				modGlobalFunctions.AddCYAEntry_8("CL", "Reversing demand fees.", strName);
			}
			else
			{
				FCMessageBox.Show("No accounts were affected.", MsgBoxStyle.Information, "Reversal Cancelled");
				return;
			}
			for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
			{
				// for each account in the grid
				if (Conversion.Val(vsDemand.TextMatrix(lngCT, lngColCheck)) == -1)
				{
					// check to see if the check box if checked
					if (Conversion.Val(vsDemand.TextMatrix(lngCT, 6)) >= 0)
					{
						// then what?
						rsData.FindFirstRecord("ID", vsDemand.TextMatrix(lngCT, lngColBillKey));
						if (rsData.NoMatch)
						{
							FCMessageBox.Show("Error processing account " + vsDemand.TextMatrix(lngCT, lngColAccount) + ".  No demand fees were applied.", MsgBoxStyle.Critical, "Cannot Find BillKey - " + vsDemand.TextMatrix(lngCT, lngColBillKey));
						}
						else
						{
							// keep going and add an entry in the CYA table
							modGlobalFunctions.AddCYAEntry_26("CL", "Reversing demand fees.", "Account : " + FCConvert.ToString(modGlobal.Statics.arrDemand[FCConvert.ToInt32(vsDemand.TextMatrix(lngCT, lngColIndex))].Account));
							modGlobal.Statics.arrDemand[FCConvert.ToInt32(vsDemand.TextMatrix(lngCT, lngColIndex))].Processed = true;
							AddDemandPayment(ref lngCT);
							lngDemandCount += 1;
						}
					}
					else
					{
						// not eligible...do nothing
					}
				}
				else
				{
					// not selected...do nothing
				}
			}
			FCMessageBox.Show(FCConvert.ToString(lngDemandCount) + " accounts were affected.", MsgBoxStyle.Information, "Demand Fees Added");
			// print the list of accounts that have been affected
			//FC:FINAL:DSE:#2001 Report not displayed if current form not closed first
			this.Unload();
			FCUtils.ApplicationUpdate(this);
			rptDemandFeeList.InstancePtr.Init(lngDemandCount, dblDemand, true);
			frmReportViewer.InstancePtr.Init(rptDemandFeeList.InstancePtr);
		}

		private void mnuFileSelectAll_Click(object sender, System.EventArgs e)
		{
			// this will select all account in the grid
			int lngCT;
			for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
			{
				vsDemand.TextMatrix(lngCT, lngColCheck, FCConvert.ToString(-1));
			}
		}

		private void AddDemandPayment(ref int lngGridRow)
		{
			double dblTotalCertMailFee;
			// VBto upgrade warning: dblTotalChange As double	OnWrite(string)
			double dblTotalChange = 0;
			double dblCurInterest/*unused?*/;
			clsDRWrapper rsPay = new clsDRWrapper();
			// this will actually create the payment line
			rsPay.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = 0", modExtraModules.strCLDatabase);
			rsPay.AddNew();
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			rsPay.Set_Fields("Account", rsData.Get_Fields("Account"));
			rsPay.Set_Fields("Year", rsData.Get_Fields_Int32("BillingYear"));
			rsPay.Set_Fields("BillKey", rsData.Get_Fields_Int32("ID"));
			// .Get_Fields("CHGINTNumber") = AddCHGINTForDemandFee    'do not charge interest (ron said so, to make it easier to reverse interest)
			rsPay.Set_Fields("CHGINTDate", DateTime.Today);
			rsPay.Set_Fields("ActualSystemDate", DateTime.Today);
			// MAL@20080603: Change to be more accurate last interest date
			// Tracker Reference: 14041
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			rsPay.Set_Fields("EffectiveInterestDate", modStatusPayments.GetLastInterestDate_CL(FCConvert.ToInt32(rsData.Get_Fields("Account")), FCConvert.ToInt32(rsData.Get_Fields_Int32("BillingYear")), DateTime.Today, FCConvert.ToInt32(rsData.Get_Fields_Int32("ID"))));
			// .Get_Fields("EffectiveInterestDate") = Date
			rsPay.Set_Fields("RecordedTransactionDate", DateTime.Today);
			rsPay.Set_Fields("Teller", "");
			rsPay.Set_Fields("Reference", "DEMAND");
			rsPay.Set_Fields("Period", "A");
			rsPay.Set_Fields("Code", "3");
			rsPay.Set_Fields("ReceiptNumber", 0);
			rsPay.Set_Fields("Principal", 0);
			rsPay.Set_Fields("PreLienInterest", 0);
			rsPay.Set_Fields("CurrentInterest", 0);
			// calculate the lien costs
			dblTotalCertMailFee = 0;
			// dblCertMailFee * rsData.Get_Fields("Copies")
			dblTotalChange = FCConvert.ToDouble(vsDemand.TextMatrix(lngGridRow, lngColAmount));
			// dblTotalCertMailFee + dblDemand
			rsPay.Set_Fields("LienCost", (dblTotalChange));
			// this would be negative if we were applying the demand fees, but this will reverse them old demand fees
			rsPay.Set_Fields("TransNumber", 0);
			rsPay.Set_Fields("PaidBy", "Automatic/Computer");
			rsPay.Set_Fields("Comments", "Reverse Demand Fees");
			rsPay.Set_Fields("CashDrawer", "N");
			rsPay.Set_Fields("GeneralLedger", "Y");
			rsPay.Set_Fields("BudgetaryAccountNumber", "");
			rsPay.Set_Fields("BillCode", "R");
			// rsPay.Get_Fields ("DailyCloseOut")
			rsPay.Update();
			// this will edit the bill record
			// .OpenRecordset "SELECT * FROM CurrentAccountBills WHERE Billkey = " & .BillKey, strCLDATABASE
			// If .RecordCount <> 0 Then
			rsData.Edit();
			// .Get_Fields("InterestCharged") = (.Get_Fields("InterestCharged") - dblCurInterest)
			rsData.Set_Fields("DemandFees", (Conversion.Val(rsData.Get_Fields_Decimal("DemandFees")) - dblTotalChange));
			// .Get_Fields("InterestAppliedThroughDate") = Date   'do not change the interest date (ron said so)
			rsData.Set_Fields("LienProcessStatus", 1);
			// set the status to '30 Day Notice Run'
			rsData.Set_Fields("LienStatusEligibility", 2);
			// Eligible for 'Apply Demand Fees'
			rsData.Update();
			// End If
		}

		//private int AddCHGINTForDemandFee()
		//{
		//	int AddCHGINTForDemandFee = 0;
		//	// this will calculate the CHGINT line for the account and then return
		//	// the CHGINT number to store in the payment record
		//	// this will use rsData which is set to the correct record
		//	double dblCurInt = 0;
		//	double dblTotalDue;
		//	clsDRWrapper rsCHGINT = new clsDRWrapper();
		//	// calculate the current interest to this date
		//	// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
		//	dblTotalDue = modCLCalculations.CalculateAccountCL(ref rsData, FCConvert.ToInt32(rsData.Get_Fields("Account")), DateTime.Today, ref dblCurInt);
		//	if (dblCurInt > 0)
		//	{
		//		// if there is interest due, then
		//		// this will actually create the payment line
		//		rsCHGINT.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = 0", modExtraModules.strCLDatabase);
		//		rsCHGINT.AddNew();
		//		// AddCHGINTForDemandFee = .Get_Fields("ID")
		//		// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
		//		rsCHGINT.Set_Fields("Account", rsData.Get_Fields("Account"));
		//		rsCHGINT.Set_Fields("Year", rsData.Get_Fields_Int32("BillingYear"));
		//		rsCHGINT.Set_Fields("BillKey", rsData.Get_Fields_Int32("BillKey"));
		//		rsCHGINT.Set_Fields("CHGINTNumber", 0);
		//		rsCHGINT.Set_Fields("CHGINTDate", DateTime.Today);
		//		rsCHGINT.Set_Fields("ActualSystemDate", DateTime.Today);
		//		rsCHGINT.Set_Fields("EffectiveInterestDate", DateTime.Today);
		//		rsCHGINT.Set_Fields("RecordedTransactionDate", DateTime.Today);
		//		rsCHGINT.Set_Fields("Teller", "");
		//		rsCHGINT.Set_Fields("Reference", "CHGINT");
		//		rsCHGINT.Set_Fields("Period", "A");
		//		rsCHGINT.Set_Fields("Code", "I");
		//		rsCHGINT.Set_Fields("ReceiptNumber", 0);
		//		rsCHGINT.Set_Fields("Principal", 0);
		//		rsCHGINT.Set_Fields("PreLienInterest", 0);
		//		rsCHGINT.Set_Fields("CurrentInterest", dblCurInt);
		//		rsCHGINT.Set_Fields("LienCost", 0);
		//		rsCHGINT.Set_Fields("TransNumber", 0);
		//		rsCHGINT.Set_Fields("PaidBy", 0);
		//		rsCHGINT.Set_Fields("Comments", "Demand Fees");
		//		rsCHGINT.Set_Fields("CashDrawer", "N");
		//		rsCHGINT.Set_Fields("GeneralLedger", "Y");
		//		rsCHGINT.Set_Fields("BudgetaryAccountNumber", "");
		//		rsCHGINT.Set_Fields("BillCode", "R");
		//		// rsPay.Get_Fields ("DailyCloseOut")
		//		rsCHGINT.Update();
		//		AddCHGINTForDemandFee = FCConvert.ToInt32(rsCHGINT.Get_Fields_Int32("ID"));
		//	}
		//	return AddCHGINTForDemandFee;
		//}

		private void vsDemand_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			// this should not allow an account that does not have demand fees to be checked
			if (vsDemand.Col == 0)
			{
				if (Conversion.Val(vsDemand.TextMatrix(vsDemand.Row, lngColProcess)) != 0)
				{
					vsDemand.TextMatrix(vsDemand.Row, lngColCheck, FCConvert.ToString(0));
				}
			}
		}

		private void vsDemand_DblClick(object sender, EventArgs e)
		{
			int lngMR;
			int lngMC;
			lngMR = vsDemand.MouseRow;
			lngMC = vsDemand.MouseCol;
			if (lngMR == 0 && lngMC == 0)
			{
				mnuFileSelectAll_Click(cmdFileSelectAll, EventArgs.Empty);
			}
		}

        //private void vsDemand_MouseMove(object sender, DataGridViewCellMouseEventArgs e)
        //{
        //	int lngMR;
        //	int lngMC;
        //	lngMR = vsDemand.MouseRow;
        //	lngMC = vsDemand.MouseCol;
        //	if (lngMR > 0 && lngMC > 0)
        //	{
        //		if (Conversion.Val(vsDemand.TextMatrix(lngMR, lngColProcess)) == -1)
        //		{
        //			ToolTip1.SetToolTip(vsDemand, "There have been no Demand Fees added to this account.  This account is not eligible.");
        //		}
        //		else
        //		{
        //			ToolTip1.SetToolTip(vsDemand, "");
        //		}
        //	}
        //}

        private void vsDemand_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            int lngMR;
            int lngMC;
            lngMR = vsDemand.GetFlexRowIndex(e.RowIndex);
            lngMC = vsDemand.GetFlexColIndex(e.ColumnIndex);
            if (lngMR > 0 && lngMC > 0)
            {
                DataGridViewCell cell = vsDemand[e.ColumnIndex, e.RowIndex];
                if (Conversion.Val(vsDemand.TextMatrix(lngMR, lngColProcess)) == -1)
                {
                    cell.ToolTipText = "There have been no Demand Fees added to this account.  This account is not eligible.";
                }
                else
                {
                    cell.ToolTipText = "";
                }
            }
        }

        private void vsDemand_RowColChange(object sender, EventArgs e)
		{
			switch (vsDemand.Col)
			{
				case 0:
					{
						vsDemand.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						break;
					}
				default:
					{
						vsDemand.Editable = FCGrid.EditableSettings.flexEDNone;
						break;
					}
			}
			//end switch
		}

		private void ClearCheckBoxes()
		{
			// this will set all of the textboxes to unchecked
			int lngCT;
			for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
			{
				vsDemand.TextMatrix(lngCT, lngColCheck, FCConvert.ToString(0));
			}
		}

		private bool FillValidateInfo()
		{
			bool FillValidateInfo = false;
			// this will fill the validate grid with the information from the Control_30DayNotice table
			// which is the information that was used to print the 30 Day Notices the last time
			clsDRWrapper rsValidate = new clsDRWrapper();
			FillValidateInfo = true;
			rsValidate.OpenRecordset("SELECT * FROM Control_30DayNotice");
			if (rsValidate.EndOfFile())
			{
				FCMessageBox.Show("Please return to and run 'Print 30 Day Notices'.", MsgBoxStyle.Information, "No Control Record");
				FillValidateInfo = false;
			}
			else
			{
				dtMailDate = (DateTime)rsValidate.Get_Fields_DateTime("MailDate");
				dblDemand = Conversion.Val(rsValidate.Get_Fields_Double("Demand"));
				boolChargeMort = FCConvert.ToBoolean(rsValidate.Get_Fields_Boolean("ChargeForMortHolder"));
				boolChargeCert = FCConvert.ToBoolean(rsValidate.Get_Fields_Boolean("PayCertMailFee"));
				if (boolChargeCert)
				{
					dblCertMailFee = Conversion.Val(rsValidate.Get_Fields_Double("CertMailFee"));
				}
				else
				{
					dblCertMailFee = 0;
				}
			}
			return FillValidateInfo;
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFilePrint_Click(sender, e);
		}

		private void cmdFileSelectAll_Click(object sender, EventArgs e)
		{
			this.mnuFileSelectAll_Click(sender, e);
		}

		private void cmdFileClear_Click(object sender, EventArgs e)
		{
			this.mnuFileClear_Click(sender, e);
		}
	}
}
