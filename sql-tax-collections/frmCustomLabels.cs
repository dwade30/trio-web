﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmCustomLabels.
	/// </summary>
	public partial class frmCustomLabels : BaseForm
	{
		public frmCustomLabels()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.chkFormPrintOptions = new System.Collections.Generic.List<fecherFoundation.FCCheckBox>();
			this.chkFormPrintOptions.AddControlArrayElement(chkFormPrintOptions_3, 3);
			this.chkFormPrintOptions.AddControlArrayElement(chkFormPrintOptions_0, 0);
			this.chkFormPrintOptions.AddControlArrayElement(chkFormPrintOptions_1, 1);
			this.chkFormPrintOptions.AddControlArrayElement(chkFormPrintOptions_2, 2);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCustomLabels InstancePtr
		{
			get
			{
				return (frmCustomLabels)Sys.GetInstance(typeof(frmCustomLabels));
			}
		}

		protected frmCustomLabels _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/13/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/21/2006              *
		// ********************************************************
		//
		// THIS FORM IS FOR THE CREATION OF CUSTOM REPORTS. IT ALLOWS THE USER
		// TO SELECT THE FIELDS TO BE DISPLAYED, THE ORDER TO DISPLAY THEM IN
		// THE SORT ORDER TO SHOW THEM IN AND THE ABILITY TO FILTER THE RECORDS
		// BY ANY FIELD EVEN IF IT IS NOT BEING SHOWN.
		//
		// THIS FORM ***MUST*** WORK WITH modCustomReport.mod and rptCustomLabels.rpt
		//
		// THE DEVELOPER NEEDS ONLY ADD A CASE STATEMENT IN THE ROUNTING
		// SetFormFieldCaptions IN modCustomReport.mod
		// NO CHANGES TO THIS FORM OR THE REPORT NEEDS TO BE DONE.
		//
		// A CALL TO THIS FORM WOULD BE:
		// frmCustomLabels.Show , MDIParent
		// Call SetFormFieldCaptions(frmCustomLabels, "Births")
		//
		//
		int intCounter;
		int intStart;
		int intEnd;
		int intID;
		string strTemp = "";
		bool boolSaveReport;
		string strPassedInSQL = "";
		int intRateType;
		bool boolLoaded;
		string strPassSortOrder = "";
		clsPrintLabel labLabelTypes = new clsPrintLabel();
		// VBto upgrade warning: intRT As short	OnWriteFCConvert.ToInt32(
		public void Init(string strSQL, int intRT)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will pass the correct SQL string into the report and show itself
				strPassedInSQL = strSQL;
				intRateType = intRT;
				this.Show(App.MainForm);
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Custom Label Init Error");
			}
		}

		private void cmbLabelType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			int intIndex;
			intIndex = labLabelTypes.Get_IndexFromID(cmbLabelType.ItemData(cmbLabelType.SelectedIndex));
			lblDescription.Text = labLabelTypes.Get_Description(intIndex);
		}

		private void cmdAdd_Click()
		{
			// THIS ALLOWS THE USER TO SAVE THE REPORT THAT WAS JUST GENERATED
			// THIS ONLY SAVES THE SQL STATEMENT THAT IS GENERATED. THE USER
			// WILL NOT SEE THE LIST BOXES AND WHERE GRID FILLED IN FOR THEM IF
			// THEY DISPLAY A SAVED REPORT SO NAMING EACH REPORT IS ***VERY***
			// IMPORTANT
			int intRow;
			int intCol;
			string strReturn;
			clsDRWrapper RSLayout = new clsDRWrapper();
			clsDRWrapper rsSave = new clsDRWrapper();
			int intLabelType = 0;
			strReturn = Interaction.InputBox("Enter name for new report", "New Custom Report");
			strReturn = modGlobalFunctions.RemoveApostrophe(strReturn);
			if (strReturn == string.Empty)
			{
				// DO NOT SAVE REPORT
			}
			else
			{
				// THIS ALLOWS FOR THE BUILDING OF THE SQL STATEMENT BUT DOES
				// NOT SHOW IT
				boolSaveReport = true;
				cmdPrint_Click();
				boolSaveReport = false;
				// SAVE THE REPORT
				rsSave.OpenRecordset("Select * from tblCustomReports where ReportName = '" + strReturn + "' and type = '" + Strings.UCase(modCustomReport.Statics.strReportType) + "'", modExtraModules.strCLDatabase);
				if (!rsSave.EndOfFile())
				{
					FCMessageBox.Show("A report by that name already exists. A different name must be selected.", MsgBoxStyle.Information, "TRIO Software");
					return;
				}
				else
				{
					intLabelType = cmbLabelType.SelectedIndex;
					rsSave.Execute("Insert into tblCustomReports (ReportName,SQL,Type,LabelType) VALUES ('" + strReturn + "','" + modCustomReport.FixQuotes(modCustomReport.Statics.strCustomSQL) + "','" + Strings.UCase(modCustomReport.Statics.strReportType) + "'," + FCConvert.ToString(intLabelType) + ")", modExtraModules.strCLDatabase);
					rsSave.OpenRecordset("Select max(ID) as ReportID from tblCustomReports", modExtraModules.strCLDatabase);
					if (!rsSave.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [ReportID] and replace with corresponding Get_Field method
						rsSave.Execute("Delete from tblReportLayout where ReportID = " + FCConvert.ToString(rsSave.Get_Fields("ReportID")), modExtraModules.strCLDatabase);
						for (intRow = 1; intRow <= frmCustomLabels.InstancePtr.vsLayout.Rows - 1; intRow++)
						{
							for (intCol = 0; intCol <= frmCustomLabels.InstancePtr.vsLayout.Cols - 1; intCol++)
							{
								// TODO Get_Fields: Check the table for the column [ReportID] and replace with corresponding Get_Field method
								RSLayout.OpenRecordset("Select * from tblReportLayout where ReportID = " + FCConvert.ToString(rsSave.Get_Fields("ReportID")), modExtraModules.strCLDatabase);
								RSLayout.AddNew();
								// TODO Get_Fields: Check the table for the column [ReportID] and replace with corresponding Get_Field method
								RSLayout.Set_Fields("ReportID", rsSave.Get_Fields("ReportID"));
								RSLayout.Set_Fields("RowID", intRow);
								RSLayout.Set_Fields("ColumnID", intCol);
								if (FCConvert.ToString(frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)) == string.Empty)
								{
									RSLayout.Set_Fields("FieldID", -1);
								}
								else
								{
									RSLayout.Set_Fields("FieldID", FCConvert.ToString(Conversion.Val(frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol))));
								}
								RSLayout.Set_Fields("Width", frmCustomLabels.InstancePtr.vsLayout.ColWidth(intCol));
								RSLayout.Set_Fields("DisplayText", frmCustomLabels.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol));
								RSLayout.Update();
							}
						}
					}
					FCMessageBox.Show("Custom Report saved as " + strReturn, MsgBoxStyle.Information, "TRIO Software");
				}
			}
		}

		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			// CLEAR THE WHERE GRID SO THE USER CAN START FROM SCRATCH
			int intCounter;
			for (intCounter = 0; intCounter <= vsWhere.Rows - 1; intCounter++)
			{
				vsWhere.TextMatrix(intCounter, 1, string.Empty);
				vsWhere.TextMatrix(intCounter, 2, string.Empty);
				vsWhere.TextMatrix(intCounter, 3, string.Empty);
			}
		}

		public void cmdClear_Click()
		{
			cmdClear_Click(cmdClear, new System.EventArgs());
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void cmdExit_Click()
		{
			cmdExit_Click(cmdExit, new System.EventArgs());
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			// THIS ROUTINE WORKS TO PRINT OUT THE REPORT
			string strSQL = "";
			string strPrinterName = "";
			int NumFonts = 0;
			bool boolUseFont = false;
			string strFont = "";
			int intCPI = 0;
			int x;
			string strDefPrinterName = "";
			bool boolCondensed;
			string strSort = "";

			if (chkFormPrintOptions[0].CheckState != Wisej.Web.CheckState.Checked && chkFormPrintOptions[1].CheckState != Wisej.Web.CheckState.Checked && chkFormPrintOptions[2].CheckState != Wisej.Web.CheckState.Checked && chkFormPrintOptions[3].CheckState != Wisej.Web.CheckState.Checked)
			{
				FCMessageBox.Show("You must choose at least one type to print labels for", MsgBoxStyle.Exclamation, "No Labels");
				return;
			}
			boolCondensed = FCConvert.CBool(chkCondensed.CheckState == Wisej.Web.CheckState.Checked);

			{
				// make sure that the grid does not have any fields that are being edited
				vsWhere.Select(0, 0);
				// CLEAR THE FIELDS WIDTH ARRAY
				for (intCounter = 0; intCounter <= 20; intCounter++)
				{
					modCustomReport.Statics.strFieldWidth[intCounter] = 0;
				}

				strSQL = BuildSQL(ref strSort);
				
				//if (intRateType == 0)
				//{
				//	// MH Labels
				//}
				//else if (intRateType >= 10 && intRateType <= 12)
				//{
				//	// CMF Labels
				//}


				if (!boolUseFont)
					strFont = "";
				if (intRateType == 0)
				{
					rptCustomMHLabels.InstancePtr.Init(strSQL, modCustomReport.Statics.strReportType, cmbLabelType.ItemData(cmbLabelType.SelectedIndex), strPrinterName, strFont);
				}
				else if (intRateType >= 10 && intRateType <= 12)
				{
					rptCustomLabels.InstancePtr.Init(strSQL, modCustomReport.Statics.strReportType, cmbLabelType.ItemData(cmbLabelType.SelectedIndex), strPrinterName, strFont, strPassSortOrder, FCConvert.ToInt16(intRateType + 10), strDefPrinterName, boolCondensed);
				}
				else if (intRateType >= 20 && intRateType <= 22)
				{
                    rptCustomForms.InstancePtr.Init(strSQL, modCustomReport.Statics.strReportType, 1, strPrinterName, strFont, frmRateRecChoice.InstancePtr.intRateType, true, strDefPrinterName, strSort, Strings.Trim(txtExtraText.Text));
				}

				Close();
				/*? On Error GoTo 0 */// this will turn the resume next code off
			}
		}

		public void cmdPrint_Click()
		{
			cmdPrint_Click(cmdPrint, new System.EventArgs());
		}

		private string BuildSQL(ref string strPassSort)
		{
			string BuildSQL = "";
			// BUILD THE SQL STATEMENT FROM THE CRITERIA THAT THE USER HAS CHOSEN
			string strSQL = "";
			string strWhere = "";
			string strOrderBy = "";
			string strWhereClause = "";
			BuildSQL = "";
			if (strPassedInSQL != "")
			{
				strSQL = strPassedInSQL;
				// XXXXXXXXX kgk 03272012
				// strOrderBy = BuildSortParameter
				// strPassSortOrder = strOrderBy
				// If intRateType >= 10 And intRateType <= 12 Then 'this will clear the sort criteria to be used later instead of on the label query
				// strOrderBy = ""
				// End If
				strPassSortOrder = BuildSortParameter();
				// XXXXXXXX
				if (Strings.Trim(strPassSortOrder) == "")
				{
					strPassSortOrder = " Name1, New.Account";
				}
				if ((Strings.UCase(modCustomReport.Statics.strReportType) == "FORMS") || (Strings.UCase(modCustomReport.Statics.strReportType) == "LABELS"))
				{
					// this will return the SQL statement for this batch of reports
					// check for a range of Names
					if (vsWhere.TextMatrix(0, 1) != "" || vsWhere.TextMatrix(0, 2) != "")
					{
						if (Strings.Trim(vsWhere.TextMatrix(0, 1)) != "")
						{
							if (Strings.Trim(vsWhere.TextMatrix(0, 2)) != "")
							{
								// both full
								strWhereClause += " AND Name1 >= '" + vsWhere.TextMatrix(0, 1) + "     ' AND Name1 <= '" + vsWhere.TextMatrix(0, 2) + "zzzzz'";
							}
							else
							{
								// first full second empty
								strWhereClause += " AND Name1 >= '" + vsWhere.TextMatrix(0, 1) + "     '";
							}
						}
						else
						{
							if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
							{
								// first empty second full
								strWhereClause += " AND Name1 <= '" + vsWhere.TextMatrix(0, 2) + "'";
							}
							else
							{
								// both empty
								strWhereClause = "";
							}
						}
					}
					// check for a range of accounts
					if (vsWhere.TextMatrix(1, 1) != "" || vsWhere.TextMatrix(1, 2) != "")
					{
						if (Strings.Trim(strWhereClause) != "")
						{
							strWhereClause += "  AND ";
						}
						else
						{
							strWhereClause += " WHERE ";
						}
						if (Strings.Trim(vsWhere.TextMatrix(1, 1)) != "")
						{
							if (Strings.Trim(vsWhere.TextMatrix(1, 2)) != "")
							{
								// both full
								strWhereClause += " Account >= " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(1, 1))) + " AND Account <= " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(1, 2)));
							}
							else
							{
								// first full second empty
								strWhereClause += " Account >= " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(1, 1)));
							}
						}
						else
						{
							if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
							{
								// first empty second full
								strWhereClause += " Account <= " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(1, 2)));
							}
							else
							{
								// both empty
								strWhereClause = Strings.Left(strWhereClause, strWhereClause.Length - 6);
								// take the last 6 chars off
							}
						}
					}
				}
				// XXXXXXXXX kgk 03272012
				// If Trim(strOrderBy) <> vbNullString Then
				// strSQL = strSQL & strWhereClause & " ORDER BY " & strOrderBy
				// Else
				// strSQL = strSQL & strWhereClause & " ORDER BY Name1, Account"
				// End If
				strSQL += strWhereClause;
				// XXXXXXXXX
			}
			else
			{
				if (Strings.UCase(modCustomReport.Statics.strReportType) == "MORTGAGEHOLDER")
				{
					strSQL = "SELECT * FROM Mortgageholders ";
				}
				else if ((Strings.UCase(modCustomReport.Statics.strReportType) == "FORMS") || (Strings.UCase(modCustomReport.Statics.strReportType) == "LABELS"))
				{
					// this will return the SQL statement for this batch of reports
					if (vsWhere.TextMatrix(0, 1) != "" || vsWhere.TextMatrix(0, 2) != "")
					{
						// range of Names
						if (Strings.Trim(vsWhere.TextMatrix(0, 1)) != "")
						{
							if (Strings.Trim(vsWhere.TextMatrix(0, 2)) != "")
							{
								// both full
								strWhereClause = " AND Name1 BETWEEN '" + vsWhere.TextMatrix(0, 1) + "' AND '" + vsWhere.TextMatrix(0, 2) + "'";
							}
							else
							{
								// first full second empty
								strWhereClause = " AND Name1 >= '" + vsWhere.TextMatrix(0, 1) + "'";
							}
						}
						else
						{
							if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
							{
								// first empty second full
								strWhereClause = " AND Name1 <= '" + vsWhere.TextMatrix(0, 2) + "'";
							}
							else
							{
								// both empty
								strWhereClause = "";
							}
						}
					}
					else if (vsWhere.TextMatrix(1, 1) != "" || vsWhere.TextMatrix(1, 2) != "")
					{
						// range of accounts
						if (Strings.Trim(vsWhere.TextMatrix(1, 1)) != "")
						{
							if (Strings.Trim(vsWhere.TextMatrix(1, 2)) != "")
							{
								// both full
								strWhereClause = " AND Account >= " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(1, 1))) + " AND Account <= " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(1, 2)));
							}
							else
							{
								// first full second empty
								strWhereClause = " AND Account >= " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(1, 1)));
							}
						}
						else
						{
							if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
							{
								// first empty second full
								strWhereClause = " AND Account <= " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(1, 2)));
							}
							else
							{
								// both empty
								strWhereClause = "";
							}
						}
					}
				}
				strWhere = BuildWhereParameter();
				strSQL += strWhere;
				// XXXXXXXXX kgk 03272012
				// BUILD A SORT CRITERIA TO APPEND TO THE SQL STATEMENT
				// strOrderBy = BuildSortParameter
				// If Trim(strOrderBy) <> vbNullString Then
				// strSQL = strSQL & " ORDER BY " & strOrderBy
				// End If
				// XXXXXXXXX
			}
			strPassSort = strPassSortOrder;
			BuildSQL = strSQL;
			return BuildSQL;
		}

		private string BuildSortParameter()
		{
			string BuildSortParameter = "";
			// BUILD THE SORT CRITERIA FOR THE SQL STATEMENT
			string strSort;
			int intCounter;
			// CLEAR OUT THE VARIABLES
			strSort = " ";
			// GET THE FIELDS TO SORT BY
			for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
			{
				if (lstSort.Selected(intCounter))
				{
					if (strSort != " ")
						strSort += ", ";
					if (vsWhere.TextMatrix(lstSort.ItemData(intCounter), 3) == "Account")
					{
						strSort += "New.Account";
					}
					else
					{
						strSort += vsWhere.TextMatrix(lstSort.ItemData(intCounter), 3);
					}
				}
			}
			if (Strings.Right(strSort, 2) == ", ")
			{
				strSort = Strings.Left(strSort, strSort.Length - 2) + " ";
			}
			BuildSortParameter = strSort;
			return BuildSortParameter;
		}

		private string BuildWhereParameter()
		{
			string BuildWhereParameter = "";
			// BUILD THE WHERE CLAUSE TO ADD TO THE SQL STATEMENT
			string strWhere;
			int intCounter;
			string strTemp = "";
			string strWord;
			// CLEAR THE VARIABLES
			strWhere = " ";
			strWord = " WHERE ";
			if (modCustomReport.Statics.strReportType == "REALESTATE")
			{
				strWord = " AND ";
				strWhere = " WHERE RSCard = 1 ";
			}
			for (intCounter = 0; intCounter <= vsWhere.Rows - 1; intCounter++)
			{
				switch (intCounter)
				{
					case 0:
						{
							// name
							// If strReportType = "REALESTATE" Then
							// strTemp = "RSName"
							// Else
							strTemp = "Name";
							// End If
							if (Strings.UCase(modCustomReport.Statics.strReportType) == "MORTGAGEHOLDER")
							{
								if (Strings.Trim(vsWhere.TextMatrix(0, 1)) != string.Empty)
								{
									if (Strings.Trim(vsWhere.TextMatrix(0, 2)) != string.Empty)
									{
										strWhere += strWord + strTemp + " > '" + Strings.Trim(vsWhere.TextMatrix(0, 1)) + "     ' AND " + strTemp + " < '" + Strings.Trim(vsWhere.TextMatrix(0, 2)) + "zzzzz' ";
									}
									else
									{
										strWhere += strWord + strTemp + " > '" + Strings.Trim(vsWhere.TextMatrix(0, 1)) + "     ' AND " + strTemp + " < '" + Strings.Trim(vsWhere.TextMatrix(0, 1)) + "zzzzz' ";
									}
									strWord = " AND ";
								}
							}
							else
							{
								if (Strings.Trim(vsWhere.TextMatrix(0, 1)) != string.Empty)
								{
									if (Strings.Trim(vsWhere.TextMatrix(0, 2)) != string.Empty)
									{
										strWhere += strWord + strTemp + " BETWEEN '" + Strings.Trim(vsWhere.TextMatrix(0, 1)) + "' AND '" + Strings.Trim(vsWhere.TextMatrix(0, 2)) + "' ";
									}
									else
									{
										strWhere += strWord + strTemp + " = '" + Strings.Trim(vsWhere.TextMatrix(0, 1)) + "' OR " + strTemp + " LIKE '" + Strings.Trim(vsWhere.TextMatrix(0, 1)) + "*' ";
									}
									strWord = " AND ";
								}
							}
							break;
						}
					case 1:
						{
							// account
							// If strReportType = "MORTGAGEHOLDER" Then
							// mh
							strTemp = "ID";
							// "MortgageHolderID"
							// ElseIf strReportType = "REALESTATE" Then
							// re
							// strTemp = "rsaccount"
							// Else
							// strTemp = "account"
							// pp
							// End If
							if (Conversion.Val(vsWhere.TextMatrix(1, 1)) > 0)
							{
								if (Conversion.Val(vsWhere.TextMatrix(1, 2)) > 0)
								{
									strWhere += strWord + strTemp + " BETWEEN " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(1, 1))) + " AND " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(1, 2))) + " ";
								}
								else
								{
									strWhere += strWord + strTemp + " = " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(1, 1))) + " ";
								}
								strWord = " AND ";
							}
							break;
						}
					case 2:
						{
							// location
							// If strReportType = "MORTGAGEHOLDER" Then
							// mh
							strTemp = "zip";
							if (Strings.Trim(vsWhere.TextMatrix(2, 1)) != string.Empty)
							{
								if (Strings.Trim(vsWhere.TextMatrix(2, 2)) != string.Empty)
								{
									strWhere += strWord + strTemp + " BETWEEN '" + Strings.Trim(vsWhere.TextMatrix(2, 1)) + "' AND '" + Strings.Trim(vsWhere.TextMatrix(2, 2)) + "' ";
								}
								else
								{
									strWhere += strWord + strTemp + " = '" + Strings.Trim(vsWhere.TextMatrix(2, 1)) + "' ";
								}
								strWord = " AND ";
							}
							// ElseIf strReportType = "REALESTATE" Then
							// re
							// strTemp = "rslocstreet"
							// Else
							// pp
							// strTemp = "street"
							// End If
							if (Strings.Trim(vsWhere.TextMatrix(2, 1)) != string.Empty)
							{
								if (Strings.Trim(vsWhere.TextMatrix(2, 2)) != string.Empty)
								{
									strWhere += strWord + strTemp + " BETWEEN '" + Strings.Trim(vsWhere.TextMatrix(2, 1)) + "' AND '" + Strings.Trim(vsWhere.TextMatrix(2, 2)) + "' ";
								}
								else
								{
									strWhere += strWord + strTemp + " = '" + Strings.Trim(vsWhere.TextMatrix(2, 1)) + "' ";
								}
								strWord = " AND ";
							}
							break;
						}
					case 3:
						{
							// zip
							strTemp = "zip";
							if (modCustomReport.Statics.strReportType == "PERSONALPROPERTY")
							{
								if (Strings.Trim(vsWhere.TextMatrix(3, 1)) != string.Empty)
								{
									if (Strings.Trim(vsWhere.TextMatrix(3, 2)) != string.Empty)
									{
										strWhere += strWord + strTemp + " BETWEEN " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(3, 1))) + " AND " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(3, 2))) + " ";
									}
									else
									{
										strWhere += strWord + strTemp + " = " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(3, 1))) + " ";
									}
									strWord = " AND ";
								}
							}
							else
							{
								if (modCustomReport.Statics.strReportType == "REALESTATE")
									strTemp = "rszip";
								if (Strings.Trim(vsWhere.TextMatrix(3, 1)) != string.Empty)
								{
									if (Strings.Trim(vsWhere.TextMatrix(3, 2)) != string.Empty)
									{
										strWhere += strWord + strTemp + " BETWEEN '" + Strings.Trim(vsWhere.TextMatrix(3, 1)) + "' AND '" + Strings.Trim(vsWhere.TextMatrix(3, 2)) + "' ";
									}
									else
									{
										strWhere += strWord + strTemp + " = '" + Strings.Trim(vsWhere.TextMatrix(3, 1)) + "' ";
									}
									strWord = " AND ";
								}
							}
							break;
						}
					case 4:
						{
							// maplot or businesscode
							if (modCustomReport.Statics.strReportType == "PERSONALPROPERTY")
							{
								if (Conversion.Val(vsWhere.TextMatrix(4, 1)) > 0)
								{
									if (Conversion.Val(vsWhere.TextMatrix(4, 2)) > 0)
									{
										strWhere += strWord + " businesscode BETWEEN " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(4, 1))) + " AND " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(4, 2))) + " ";
									}
									else
									{
										strWhere += strWord + " businesscode = " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(4, 1))) + " ";
									}
									strWord = " AND ";
								}
							}
							else
							{
								if (Strings.Trim(vsWhere.TextMatrix(4, 1)) != string.Empty)
								{
									if (Strings.Trim(vsWhere.TextMatrix(4, 2)) != string.Empty)
									{
										strWhere += strWord + " rsmaplot BETWEEN '" + Strings.Trim(vsWhere.TextMatrix(4, 1)) + "' AND '" + Strings.Trim(vsWhere.TextMatrix(4, 2)) + "' ";
									}
									else
									{
										strWhere += strWord + " rsmaplot = '" + Strings.Trim(vsWhere.TextMatrix(4, 1)) + "' ";
									}
									strWord = " AND ";
								}
							}
							break;
						}
				}
				//end switch
			}
			// intCounter
			BuildWhereParameter = strWhere;
			return BuildWhereParameter;
		}

		private void frmCustomLabels_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (boolLoaded)
				{
				}
				else
				{
					// load the form
					boolLoaded = true;
					if (intRateType >= 20)
					{
						fcLabel1.Visible = false;
						cmbFormType.Visible = false;
						fraType.Visible = false;
					}
					else
					{
						fcLabel1.Visible = false;
						cmbFormType.Visible = false;
						fraType.Visible = true;
					}
					LoadNotesText();
					if (cmbLabelType.Visible && cmbLabelType.Enabled)
					{
						cmbLabelType.Focus();
					}
					else if (cmbFormType.Visible && cmbFormType.Enabled)
					{
						cmbFormType.Focus();
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Activate Error");
			}
		}

		private void frmCustomLabels_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// Case vbKeyF2
			// KeyCode = 0
			// Call mnuAddRow_Click
			// Case vbKeyF3
			// KeyCode = 0
			// Call mnuAddColumn_Click
			// Case vbKeyF4
			// KeyCode = 0
			// Call mnuDeleteRow_Click
			// Case vbKeyF5
			// KeyCode = 0
			// Call mnuDeleteColumn_Click
			// Case vbKeyF10
			// KeyCode = 0
			// mnuPrint_Click
			if (KeyCode == Keys.Escape)
			{
				KeyCode = 0;
				mnuExit_Click();
			}
		}

		private void frmCustomLabels_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomLabels.Icon	= "frmCustomLabels.frx":0000";
			//frmCustomLabels.ScaleWidth	= 9045;
			//frmCustomLabels.ScaleHeight	= 7410;
			//frmCustomLabels.LinkTopic	= "Form1";
			//Font.Size	= "8.25";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//txtExtraText.MaxLength	= 255;
			//vsWhere.BackColor	= "-2147483643";
			//			//vsWhere.ForeColor	= "-2147483640";
			//vsWhere.BorderStyle	= 1;
			//vsWhere.FillStyle	= 0;
			//vsWhere.Appearance	= 1;
			//vsWhere.GridLines	= 1;
			//vsWhere.WordWrap	= 0;
			//vsWhere.ScrollBars	= 0;
			//vsWhere.RightToLeft	= 0;
			//vsWhere._cx	= 9816;
			//vsWhere._cy	= 3291;
			//vsWhere._ConvInfo	= 1;
			//vsWhere.MousePointer	= 0;
			//vsWhere.BackColorFixed	= -2147483633;
			//			//vsWhere.ForeColorFixed	= -2147483630;
			//vsWhere.BackColorSel	= -2147483635;
			//			//vsWhere.ForeColorSel	= -2147483634;
			//vsWhere.BackColorBkg	= -2147483636;
			//vsWhere.BackColorAlternate	= -2147483643;
			//vsWhere.GridColor	= -2147483633;
			//vsWhere.GridColorFixed	= -2147483632;
			//vsWhere.TreeColor	= -2147483632;
			//vsWhere.FloodColor	= 192;
			//vsWhere.SheetBorder	= -2147483642;
			//vsWhere.FocusRect	= 1;
			//vsWhere.HighLight	= 1;
			//vsWhere.AllowSelection	= -1  'True;
			//vsWhere.AllowBigSelection	= -1  'True;
			//vsWhere.AllowUserResizing	= 0;
			//vsWhere.SelectionMode	= 0;
			//vsWhere.GridLinesFixed	= 2;
			//vsWhere.GridLineWidth	= 1;
			//vsWhere.RowHeightMin	= 0;
			//vsWhere.RowHeightMax	= 0;
			//vsWhere.ColWidthMin	= 0;
			//vsWhere.ColWidthMax	= 0;
			//vsWhere.ExtendLastCol	= -1  'True;
			//vsWhere.FormatString	= "";
			//vsWhere.ScrollTrack	= -1  'True;
			//vsWhere.ScrollTips	= 0   'False;
			//vsWhere.MergeCells	= 0;
			//vsWhere.MergeCompare	= 0;
			//vsWhere.AutoResize	= -1  'True;
			//vsWhere.AutoSizeMode	= 0;
			//vsWhere.AutoSearch	= 0;
			//vsWhere.AutoSearchDelay	= 2;
			//vsWhere.MultiTotals	= -1  'True;
			//vsWhere.SubtotalPosition	= 1;
			//vsWhere.OutlineBar	= 0;
			//vsWhere.OutlineCol	= 0;
			//vsWhere.Ellipsis	= 0;
			//vsWhere.ExplorerBar	= 0;
			//vsWhere.PicturesOver	= 0   'False;
			//vsWhere.PictureType	= 0;
			//vsWhere.TabBehavior	= 1;
			//vsWhere.OwnerDraw	= 0;
			//vsWhere.ShowComboButton	= -1  'True;
			//vsWhere.TextStyle	= 0;
			//vsWhere.TextStyleFixed	= 0;
			//vsWhere.OleDragMode	= 0;
			//vsWhere.OleDropMode	= 0;
			//vsWhere.DataMode	= 0;
			//vsWhere.VirtualData	= -1  'True;
			//vsWhere.ComboSearch	= 3;
			//vsWhere.AutoSizeMouse	= -1  'True;
			//vsWhere.AllowUserFreezing	= 0;
			//vsWhere.BackColorFrozen	= 0;
			//			//vsWhere.ForeColorFrozen	= 0;
			//vsWhere.WallPaperAlignment	= 9;
			//vsLayout.BackColor	= "-2147483643";
			//			//vsLayout.ForeColor	= "-2147483640";
			//vsLayout.BorderStyle	= 1;
			//vsLayout.FillStyle	= 0;
			//vsLayout.Appearance	= 1;
			//vsLayout.GridLines	= 1;
			//vsLayout.WordWrap	= 0;
			//vsLayout.ScrollBars	= 2;
			//vsLayout.RightToLeft	= 0;
			//vsLayout._cx	= 10186;
			//vsLayout._cy	= 3844;
			//vsLayout._ConvInfo	= 1;
			//vsLayout.MousePointer	= 0;
			//vsLayout.BackColorFixed	= -2147483633;
			//			//vsLayout.ForeColorFixed	= -2147483630;
			//vsLayout.BackColorSel	= -2147483635;
			//			//vsLayout.ForeColorSel	= -2147483634;
			//vsLayout.BackColorBkg	= -2147483636;
			//vsLayout.BackColorAlternate	= -2147483643;
			//vsLayout.GridColor	= -2147483633;
			//vsLayout.GridColorFixed	= -2147483632;
			//vsLayout.TreeColor	= -2147483632;
			//vsLayout.FloodColor	= 192;
			//vsLayout.SheetBorder	= -2147483642;
			//vsLayout.FocusRect	= 0;
			//vsLayout.HighLight	= 0;
			//vsLayout.AllowSelection	= 0   'False;
			//vsLayout.AllowBigSelection	= 0   'False;
			//vsLayout.AllowUserResizing	= 1;
			//vsLayout.SelectionMode	= 0;
			//vsLayout.GridLinesFixed	= 2;
			//vsLayout.GridLineWidth	= 1;
			//vsLayout.RowHeightMin	= 0;
			//vsLayout.RowHeightMax	= 0;
			//vsLayout.ColWidthMin	= 0;
			//vsLayout.ColWidthMax	= 0;
			//vsLayout.ExtendLastCol	= 0   'False;
			//vsLayout.FormatString	= "";
			//vsLayout.ScrollTrack	= -1  'True;
			//vsLayout.ScrollTips	= 0   'False;
			//vsLayout.MergeCells	= 0;
			//vsLayout.MergeCompare	= 0;
			//vsLayout.AutoResize	= -1  'True;
			//vsLayout.AutoSizeMode	= 0;
			//vsLayout.AutoSearch	= 0;
			//vsLayout.AutoSearchDelay	= 2;
			//vsLayout.MultiTotals	= -1  'True;
			//vsLayout.SubtotalPosition	= 1;
			//vsLayout.OutlineBar	= 0;
			//vsLayout.OutlineCol	= 0;
			//vsLayout.Ellipsis	= 0;
			//vsLayout.ExplorerBar	= 2;
			//vsLayout.PicturesOver	= 0   'False;
			//vsLayout.PictureType	= 0;
			//vsLayout.TabBehavior	= 0;
			//vsLayout.OwnerDraw	= 0;
			//vsLayout.ShowComboButton	= -1  'True;
			//vsLayout.TextStyle	= 0;
			//vsLayout.TextStyleFixed	= 0;
			//vsLayout.OleDragMode	= 0;
			//vsLayout.OleDropMode	= 0;
			//vsLayout.DataMode	= 0;
			//vsLayout.VirtualData	= -1  'True;
			//vsLayout.ComboSearch	= 3;
			//vsLayout.AutoSizeMouse	= -1  'True;
			//vsLayout.AllowUserFreezing	= 0;
			//vsLayout.BackColorFrozen	= 0;
			//			//vsLayout.ForeColorFrozen	= 0;
			//vsLayout.WallPaperAlignment	= 9;
			//Images.NumListImages	= 1;
			//vsElasticLight1.OleObjectBlob	= "frmCustomLabels.frx":05E8";
			//End Unmaped Properties
			int intError = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				intError = 1;
				switch (intRateType)
				{
					case 0:
						{
							intError = 2;
							SetFormFieldCaptions(this, "MORTGAGEHOLDER");
							this.Text = "Mortgage Holder Labels";
							FillLabelTypeCombo();
							txtExtraText.Enabled = false;
							break;
						}
					case 10:
					case 11:
					case 12:
						{
							intError = 3;
							SetFormFieldCaptions(this, "LABELS");
							this.Text = "Lien Mail Labels";
							FillLabelTypeCombo();
							cmbFormType.Clear();
							txtExtraText.Enabled = false;
							break;
						}
					case 20:
					case 21:
					case 22:
						{
							intError = 4;
							// this is Certified Mail Forms
							SetFormFieldCaptions(this, "FORMS");
							this.Text = "Lien Certified Mail Forms";
							FillFormTypeCombo();
							cmbLabelType.Clear();
							fcLabel3.Enabled = true;
							SetCopiesChkBoxes();
							// kgk 11-29-2011 trocl-840  Set checkboxes to match options selected earlier
							break;
						}
				}
				//FC:FINAL:AM:134
				this.HeaderText.Text = this.Text;
				//end switch
				intError = 5;
				FillLayoutGrid();
				// GET THE SIZE AND ALIGNEMENT OF THE CUSTOM REPORT FORM INSIDE
				// OF THE MDI PARENT FORM
				intError = 6;
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
				intError = 7;
				modGlobalFunctions.SetTRIOColors(this);
				intError = 8;
				if (intRateType == 0)
				{
					intError = 9;
					if (cmbLabelType.Items.Count != 0)
					{
						cmbLabelType.SelectedIndex = 0;
					}
					intError = 10;
					fcLabel2.Enabled = true;
					fraFields.Enabled = true;
					fraWhere.Enabled = true;
					fraMessage.Visible = false;
					vsLayout.Visible = true;
				}
				else
				{
					intError = 12;
					if (intRateType < 20)
					{
						intError = 13;
						if (cmbLabelType.Items.Count != 0)
						{
							cmbLabelType.SelectedIndex = 0;
						}
					}
					else
					{
						intError = 14;
						if (cmbLabelType.Items.Count != 0)
						{
							cmbLabelType.SelectedIndex = 0;
						}
					}
					intError = 15;
					fraMessage.Top = vsLayout.Top + vsLayout.Height;
					intError = 16;
					if ((Frame1.Height - fraMessage.Top) - 100 > 0)
					{
						fraMessage.Height = (Frame1.Height - fraMessage.Top) - 100;
					}
					else
					{
						// don't change it, cuz it is messed up or minimized
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Load Error - " + FCConvert.ToString(intError));
			}
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			frmRateRecChoice.InstancePtr.Unload();
			//rptLienNoticeSummary.InstancePtr.Hide();
			boolLoaded = false;
		}

		private void frmCustomLabels_Resize(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// resize the grids
				// set the height of the Grid
				//FC:FINAL:AM: don't set the height; used anchoring instead
				//vsLayout.HeightOriginal = (vsLayout.Rows * vsLayout.RowHeight(0)) + 70;
				// turn off the scroll bars
				vsLayout.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				// set the width of the columns
				vsLayout.ColWidth(0, vsLayout.WidthOriginal);
				vsLayout.ExtendLastCol = true;
				// set the height of the Grid
				//if (vsWhere.Rows > 0)
				//	vsWhere.HeightOriginal = (vsWhere.Rows * vsWhere.RowHeight(0)) + 70;
				// turn off the scroll bars
				vsWhere.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				// set the width of the columns
				vsWhere.ColWidth(0, FCConvert.ToInt32(vsWhere.WidthOriginal / 3.0));
				vsWhere.ColWidth(1, FCConvert.ToInt32(vsWhere.WidthOriginal / 3.0));
				vsWhere.ColWidth(2, FCConvert.ToInt32(vsWhere.WidthOriginal / 3.0));
				vsWhere.ExtendLastCol = true;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Resize Error");
			}
		}

		private void fraFields_DoubleClick(object sender, System.EventArgs e)
		{
			int intCounter;
			for (intCounter = 0; intCounter <= lstFields.Items.Count - 1; intCounter++)
			{
				lstFields.SetSelected(intCounter, true);
			}
		}

		private void fraSort_DoubleClick(object sender, System.EventArgs e)
		{
			int intCounter;
			for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
			{
				lstSort.SetSelected(intCounter, true);
			}
		}

		private void fraWhere_DoubleClick(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}

		private void lstFields_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (lstFields.SelectedIndex < 0)
				return;
			switch (lstFields.SelectedIndex)
			{
				case 0:
					{
						// mortgage holders
						modCustomReport.Statics.strReportType = "MORTGAGEHOLDER";
						LoadWhereGrid(this);
						LoadSortList(this);
						// Case 1
						// Real Estate
						// strReportType = "REALESTATE"
						// Call LoadWhereGrid(Me)
						// Call LoadSortList(Me)
						// 
						// Case 2
						// Personal Property
						// strReportType = "PERSONALPROPERTY"
						// Call LoadWhereGrid(Me)
						// Call LoadSortList(Me)
						break;
					}
			}
			//end switch
		}

		private void FillLayoutGrid()
		{
			int GridWidth/*unused?*/;
			vsLayout.Cols = 1;
			vsLayout.Rows = 4;
			vsLayout.ColWidth(0, 4 * 1440);
			// four inches
			vsLayout.TextMatrix(0, 0, "Name");
			vsLayout.TextMatrix(1, 0, "Address 1");
			vsLayout.TextMatrix(2, 0, "Address 2");
			vsLayout.TextMatrix(3, 0, "City, State 00000");
		}

		//private void lstFields_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS IS USED SO THAT WHEN THE USER CLICKS ON AN ITEM IN THIS
		//	// CONTROL AND DRAGS IT TO A DIFFERENT LOCATION THEN WE WILL KNOW
		//	// WHERE TO SWAP THE TWO ITEMS.
		//	// 
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	// intStart = lstFields.ListIndex
		//}

		//private void lstFields_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS WILL CHANGE THE CAPTION AND THE ITEM DATA NUMBER FOR THE TWO
		//	// ITEMS THAT ARE TO BE SWAPED
		//	// 
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	// IF THIS IS A CHANGE (DRAG AND DROP) AND NOT JUST A CLICK THEN...
		//	// If intStart <> lstFields.ListIndex And lstFields.ListIndex > 0 Then
		//	// SAVE THE CAPTION AND ID FOR THE NEW ITEM
		//	// strtemp = lstFields.List(lstFields.ListIndex)
		//	// intID = lstFields.ItemData(lstFields.ListIndex)
		//	// 
		//	// CHANGE THE NEW ITEM
		//	// lstFields.List(lstFields.ListIndex) = lstFields.List(intStart)
		//	// lstFields.ItemData(lstFields.ListIndex) = lstFields.ItemData(intStart)
		//	// 
		//	// SAVE THE OLD ITEM
		//	// lstFields.List(intStart) = strtemp
		//	// lstFields.ItemData(intStart) = intID
		//	// 
		//	// SET BOTH ITEMS TO BE SELECTED
		//	// lstFields.Selected(lstFields.ListIndex) = True
		//	// lstFields.Selected(intStart) = True
		//	// End If
		//}
		//private void lstSort_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS IS USED SO THAT WHEN THE USER CLICKS ON AN ITEM IN THIS
		//	// CONTROL AND DRAGS IT TO A DIFFERENT LOCATION THEN WE WILL KNOW
		//	// WHERE TO SWAP THE TWO ITEMS.
		//	//
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	intStart = lstSort.SelectedIndex;
		//}
		//private void lstSort_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS WILL CHANGE THE CAPTION AND THE ITEM DATA NUMBER FOR THE TWO
		//	// ITEMS THAT ARE TO BE SWAPED
		//	//
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	// IF THIS IS A CHANGE (DRAG AND DROP) AND NOT JUST A CLICK THEN...
		//	if (intStart != lstSort.SelectedIndex)
		//	{
		//		// SAVE THE CAPTION AND ID FOR THE NEW ITEM
		//		ListViewItem itemTemp = lstSort.Items[lstSort.SelectedIndex];
		//		intID = lstSort.ItemData(lstSort.SelectedIndex);
		//		// CHANGE THE NEW ITEM
		//		lstSort.Items[lstSort.ListIndex] = lstSort.Items[intStart];
		//		lstSort.ItemData(lstSort.ListIndex, lstSort.ItemData(intStart));
		//		// SAVE THE OLD ITEM
		//		lstSort.Items[intStart] = itemTemp;
		//		lstSort.ItemData(intStart, intID);
		//		// SET BOTH ITEMS TO BE SELECTED
		//		lstSort.SetSelected(lstSort.ListIndex, true);
		//		lstSort.SetSelected(intStart, true);
		//	}
		//}
		private void mnuAddColumn_Click()
		{
			if (vsLayout.Row < 0)
				vsLayout.Row = 0;
			vsLayout.Cols += 1;
			vsLayout.Select(vsLayout.Row, vsLayout.Cols - 1);
			vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Cols - 1, -1);
		}

		private void mnuAddRow_Click()
		{
			vsLayout.Rows += 1;
            //FC:FINAL:AM:#i2291 - don't merge the row because the text in the columns won't be visible
            //vsLayout.MergeRow(vsLayout.Rows - 1, true);
            vsLayout.Select(vsLayout.Rows - 1, 0);
		}

		private void mnuClear_Click(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}

		private void mnuDeleteColumn_Click()
		{
			if (vsLayout.Cols > 1)
			{
				vsLayout.ColPosition(vsLayout.Col, vsLayout.Cols - 1);
				vsLayout.Cols -= 1;
			}
			else
			{
				FCMessageBox.Show("You must have at least one column in a report.");
			}
		}

		private void mnuDeleteRow_Click()
		{
			if (vsLayout.Rows > 2)
			{
				vsLayout.RemoveItem(vsLayout.Row);
			}
			else
			{
				FCMessageBox.Show("You must have at least one row in a report.");
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			cmdPrint_Click();
		}

		private void optType_Click(ref short Index)
		{
			// NUMBER OF ROWS IS HIGHT / 240 (HEIGHT OF ONE ROW) PLUS ONE FOR
			// THE HEADER ROW   1440 = 1 INCH
			int intCounter;
			// THESE DIMENTIONS ARE A BIT DIFFERENT THEN THE NUMBERS OUTLINED
			// IN MS WORD BECAUSE WE ARE USING A DIFFERENT FONT HERE THEN
			// THE MS WORD DEFAULT FONT.
			// 
			vsLayout.Clear();
			switch (Index)
			{
				case 0:
					{
						// Avery 5160 (3 X 10)  Height = 1" Width = 2.63"
						// 1440/240(row height)
						vsLayout.Rows = 7;
						vsLayout.Cols = 1;
						//Line1.X1 = FCConvert.ToSingle((2.8 * 1440));
						//Line1.X2 = FCConvert.ToSingle((2.8 * 1440));
						//Line1.Y1 = 0;
						//Line1.Y2 = vsLayout.Height;
						break;
					}
				case 1:
					{
						// Avery 5161 (2 X 10)  Height = 1" Width = 4"
						// 1440/240(row height)
						vsLayout.Rows = 7;
						vsLayout.Cols = 1;
						//Line1.X1 = (4 * 1440);
						//Line1.X2 = (4 * 1440);
						//Line1.Y1 = 0;
						//Line1.Y2 = vsLayout.Height;
						break;
					}
				case 2:
					{
						// Avery 5163 (2 X 5)  Height = 2" Width = 4"
						// 1440/240(row height)
						vsLayout.Rows = 13;
						vsLayout.Cols = 1;
						//Line1.X1 = (4 * 1440);
						//Line1.X2 = (4 * 1440);
						//Line1.Y1 = 0;
						//Line1.Y2 = vsLayout.Height;
						break;
					}
				case 3:
					{
						// Avery 5262 (2 X 7)  Height = 1 1/3" Width = 4"
						// 1440/240(row height)
						vsLayout.Rows = 9;
						vsLayout.Cols = 1;
						//Line1.X1 = (4 * 1440);
						//Line1.X2 = (4 * 1440);
						//Line1.Y1 = 0;
						//Line1.Y2 = vsLayout.Height;
						break;
					}
			}
			//end switch
			for (intCounter = 0; intCounter <= vsLayout.Rows - 1; intCounter++)
			{
				vsLayout.MergeRow(intCounter, true);
			}
		}

		private void vsLayout_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (FCConvert.ToString(vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsLayout.Row, vsLayout.Col)) == string.Empty)
			{
				vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Col, -1);
			}
		}

		private void vsLayout_MouseDown(ref short Button, ref short Shift, ref float x, ref float y)
		{
			// If vsLayout.MouseRow = 0 Then vsLayout.Row = 1
		}

		private void vsWhere_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			// AFTER THE USER SELECTS AN ITEM IN COMBO FOR A FIELD IN THE WHERE
			// GIRD THEN WE NEE TO SAVE THE ***ID*** IN A FIELD FOR THAT SELECTION
			// 
			// THIS NEEDS TO BE SAVED AS THE COMBOITEMLIST IS PER FIELD AND BUILT
			// AND USED DYNAMICALLY INT HE BEFOREEDIT ROUTINE
			// 
			// THIS ID WILL BE USED TO PULL OUT THE CORRECT DATA IN THE WHERE CLAUSE
			vsWhere.TextMatrix(vsWhere.Row, 3, vsWhere.ComboData());
			if (Strings.Trim(vsWhere.TextMatrix(vsWhere.Row, 1)) == "__/__/____")
			{
				vsWhere.TextMatrix(vsWhere.Row, 1, string.Empty);
			}
			if (Strings.Trim(vsWhere.TextMatrix(vsWhere.Row, 2)) == "__/__/____")
			{
				vsWhere.TextMatrix(vsWhere.Row, 2, string.Empty);
			}
		}

		private void vsWhere_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			// SET THE FORMAT FOR THE TYPE OF FIELD THAT THIS IS
			// IF TEXT...ALLOW ANYTHING
			// IF DATE...ADD A MASK TO FORCE THE USE TO ENTER CORRECT DATA
			// IF COMBO...ADD THE LIST OF OPTIONS
			vsWhere.EditMask = string.Empty;
			vsWhere.ComboList = string.Empty;
			if (modCustomReport.Statics.strWhereType[vsWhere.Row] == modCustomReport.GRIDTEXT.ToString())
			{
			}
			else if (modCustomReport.Statics.strWhereType[vsWhere.Row] == modCustomReport.GRIDDATE.ToString())
			{
				vsWhere.EditMask = "##/##/####";
			}
			else if (modCustomReport.Statics.strWhereType[vsWhere.Row] == modCustomReport.GRIDCOMBOIDTEXT.ToString() || modCustomReport.Statics.strWhereType[vsWhere.Row] == modCustomReport.GRIDCOMBOIDNUM.ToString() || modCustomReport.Statics.strWhereType[vsWhere.Row] == modCustomReport.GRIDCOMBOTEXT.ToString())
			{
				vsWhere.ComboList = modCustomReport.Statics.strComboList[vsWhere.Row, 0];
			}
		}

		private void vsWhere_RowColChange(object sender, EventArgs e)
		{
			// SET THE FORMAT FOR THE TYPE OF FIELD THAT THIS IS
			// IF COMBO...ADD THE LIST OF OPTIONS
			if (vsWhere.Row >= 0)
			{
				if (modCustomReport.Statics.strComboList[vsWhere.Row, 0] != string.Empty)
				{
					vsWhere.Editable = FCGrid.EditableSettings.flexEDKbd;
					vsWhere.ComboList = modCustomReport.Statics.strComboList[vsWhere.Row, 0];
				}
				//FC:FINAL:AM: condition not needed
				//if (vsWhere.Col == 2)
				//{
				//	if (vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsWhere.Row, vsWhere.Col) == ColorTranslator.ToOle(this.BackColor))
				//	{
				//		vsWhere.Col = 1;
				//	}
				//}
			}
		}

		private void vsWhere_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (vsWhere.CurrentCell.IsInEditMode)
			{
				//FC:FINAL:MSH - save and use correct indexes of the cell
				int row = vsWhere.GetFlexRowIndex(e.RowIndex);
				int col = vsWhere.GetFlexColIndex(e.ColumnIndex);
				// THIS WILL VALIDATE THE DATA THAT THE USER PUTS INTO THE WHERE
				// GIRD THAT WILL FILTER OUT RECORDS
				if (modCustomReport.Statics.strWhereType[row] == modCustomReport.GRIDTEXT.ToString())
				{
					// ANYTHING GOES IF IT IS A TEXT FIELD
				}
				else if (modCustomReport.Statics.strWhereType[row] == modCustomReport.GRIDDATE.ToString())
				{
					// MAKE SURE THAT IT IS A VALID DATE
					if (Strings.Trim(vsWhere.EditText) == "/  /" || vsWhere.EditText == "__/__/____")
					{
						vsWhere.EditMask = string.Empty;
						vsWhere.EditText = string.Empty;
						vsWhere.TextMatrix(row, col, string.Empty);
						vsWhere.Refresh();
						return;
					}
					if (Strings.Trim(vsWhere.EditText).Length == 0)
					{
					}
					else if (Strings.Trim(vsWhere.EditText).Length != 10)
					{
						FCMessageBox.Show("Invalid date.", MsgBoxStyle.Information | MsgBoxStyle.OkOnly, "TRIO Software");
						e.Cancel = true;
						return;
					}
					if (!modCustomReport.IsValidDate(vsWhere.EditText))
					{
						e.Cancel = true;
						return;
					}
				}
				else if (modCustomReport.Statics.strWhereType[row] == modCustomReport.GRIDCOMBOIDTEXT.ToString())
				{
					// ASSIGN THE LIST TO THE COMBO IN THE GRID
					vsWhere.ComboList = modCustomReport.Statics.strComboList[row, 0];
				}
			}
		}

		private void SetColumnCaptions(dynamic FormName)
		{
			// THIS ROUTINE GETS THE CAPTIONS THAT ARE TO BE DISPLAYED ON
			// THE REPORT ITSELF
			// THIS ARRAY (strFieldCaptions) HOLDS ONLY THE FIELD NAMES THAT ARE
			// TO BE DISPLAYED WHERE THE ARRAY (strCaptions) HOLDS ALL POSSIBLE
			// FIELD NAMES
			int intCounter;
			int intCount;
			intCount = 0;
			for (intCounter = 0; intCounter <= FormName.lstFields.Items.Count - 1; intCounter++)
			{
				if (FormName.lstFields.Selected(intCounter))
				{
					modCustomReport.Statics.strFieldCaptions[intCount] = modCustomReport.Statics.strCaptions[FormName.lstFields.ItemData(intCounter)];
					intCount += 1;
				}
			}
		}

		private void GetNumberOfFields(string strSQL)
		{
			// THIS ROUTINE GETS THE NUMBER OF FIELDS TO REPORT ON.
			// ALSO...THIS FILLS THE ARRAY (strFieldNames) WITH ALL OF THE FIELDS
			// TO SHOW IN THE REPORT. THESE ARE NOT THE DISPLAY CAPTIONS BUT THE
			// ACTUAL DATABASE FIELD NAMES TO BE USED IN THE SQL STATEMENT
			// VBto upgrade warning: strTemp As Variant --> As string()
			string[] strTemp = null;
			object strTemp2/*unused?*/;
			int intCount;
			int intCounter;
			if (Strings.Trim(strSQL) == string.Empty)
			{
				modCustomReport.Statics.intNumberOfSQLFields = -1;
				return;
			}
			strTemp = Strings.Split(strSQL, "from", -1, CompareConstants.vbBinaryCompare);
			strSQL = Strings.Mid(strTemp[0], 7, FCConvert.ToString(strTemp[0]).Length - 6);
			strTemp = Strings.Split(strSQL, ",", -1, CompareConstants.vbBinaryCompare);
			modCustomReport.Statics.intNumberOfSQLFields = Information.UBound(strTemp, 1);
			for (intCounter = 0; intCounter <= modCustomReport.Statics.intNumberOfSQLFields; intCounter++)
			{
				if (Strings.Trim(strTemp[intCounter]) != string.Empty)
				{
					// CHECK TO SEE IF AN ALIAS WAS USED IN THE SQL STATEMENT
					modCustomReport.Statics.strFieldNames[intCounter] = FCConvert.ToString(CheckForAS(strTemp[intCounter]));
					// NEED TO SET THE CAPTIONS ARRAY SO THAT IF SHOWING A SAVED
					// REPORT THE CORRECT CAPTION WILL SHOW ON THE REPORT
					for (intCount = 0; intCount <= modCustomReport.Statics.intNumberOfSQLFields; intCount++)
					{
						if (modCustomReport.Statics.strFields[intCount] == Strings.Trim(strTemp[intCounter]))
						{
							modCustomReport.Statics.strFieldCaptions[intCounter] = modCustomReport.Statics.strCaptions[intCount];
							break;
						}
					}
				}
			}
		}
		// VBto upgrade warning: 'Return' As object	OnWrite(object, string)
		private object CheckForAS(string strFieldName, short intSegment = 2)
		{
			object CheckForAS = null;
			// THIS FUNCTION WILL RETURN THE SQL REFERENCE FIELD NAME.
			// IF AN ALIAS WAS USED THEN IT GETS THE ALIAS NAME AND NOT THE
			// DATABASE FIELD NAME
			// VBto upgrade warning: strTemp As Variant --> As string()
			string[] strTemp = null;
			if (Strings.InStr(1, strFieldName, " AS ", CompareConstants.vbTextCompare) != 0)
			{
				// SEE IF THE WORD AS IS USED TO INDICATE AN ALIAS
				strTemp = Strings.Split(Strings.Trim(strFieldName), " ", -1, CompareConstants.vbBinaryCompare);
				if (Strings.UCase(strTemp[1]) == "AS")
				{
					// RETURN THE SQL REFERENCE FIELD NAME
					if (intSegment == 1)
					{
						CheckForAS = strTemp[0];
					}
					else
					{
						CheckForAS = strTemp[2];
					}
				}
				else
				{
					// RETURN THE ACTUAL DATABASE FIELD NAME
					CheckForAS = strTemp[0];
				}
			}
			else
			{
				// AN ALIAS WAS NOT USED SO JUST USE THE DATABASE FIELD NAME
				strTemp = Strings.Split(Strings.Trim(strFieldName), " ", -1, CompareConstants.vbBinaryCompare);
				CheckForAS = Strings.Trim(strTemp[0]);
			}
			return CheckForAS;
		}

		private void ClearComboListArray()
		{
			// CLEAR THE COMBO LIST ARRAY.
			// THIS IS THE ARRAY THAT HOLDS THE COMBO LIST IN THE WHERE GRID
			int intCount;
			for (intCount = 0; intCount <= 20; intCount++)
			{
				modCustomReport.Statics.strComboList[intCount, 0] = string.Empty;
				modCustomReport.Statics.strComboList[intCount, 1] = string.Empty;
			}
		}
		// VBto upgrade warning: FormName As Form	OnWrite(frmCustomLabels)
		private void SetFormFieldCaptions(Form FormName, string ReportType)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// THIS IS THE MAIN ROUTINE THAT FILLS THE CUSTOM REPORT FORM.
				// 
				// ****************************************************************
				// THIS IS THE ONLY ROUTINE THAT NEEDS TO BE ALTERED WHEN YOU WANT
				// TO ADD A NEW REPORT TYPE.
				// ****************************************************************
				modCustomReport.Statics.strReportType = Strings.UCase(ReportType);
				// CLEAR THE COMBO LIST ARRAY
				ClearComboListArray();
				if (Strings.UCase(ReportType) == "MORTGAGEHOLDER")
				{
					SetMortgageHolderParameters(FormName);
				}
				else if (Strings.UCase(ReportType) == "LABELS")
				{
					LoadWhereGrid(FormName);
					LoadSortList(FormName);
					fraFields.Enabled = true;
					fraFields.Visible = true;
				}
				else if (Strings.UCase(ReportType) == "FORMS")
				{
					LoadWhereGrid(FormName);
					LoadSortList(FormName);
					fraFields.Enabled = true;
					fraFields.Visible = true;
				}
				// POPULATE THE SORT LIST WITH THE SAME FIELDS AS THE FIELD LIST BOX
				// Call LoadSortList(FormName)
				// POPULATE THE WHERE GRID WITH THE FIELDS FROM THE FIELD LIST BOX
				// Call LoadWhereGrid(FormName)
				// LOAD THE SAVED REPORT COMBO ON THE FORM
				// CallByName FormName, "LoadCombo", VbMethod
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Set Form Field Captions Error");
			}
		}
		// VBto upgrade warning: FormName As Form	OnWrite(Form)
		private void SetMortgageHolderParameters(dynamic FormName)
		{
			modCustomReport.Statics.strCustomTitle = "Mortgage Holder Labels";
			FormName.lstFields.AddItem("Mortgage Holder");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 0);
			// FormName.lstFields.AddItem ("Real Estate")
			// FormName.lstFields.ItemData(FormName.lstFields.NewIndex) = 1
			// FormName.lstFields.AddItem ("Personal Property")
			// FormName.lstFields.ItemData(FormName.lstFields.NewIndex) = 2
			LoadWhereGrid(FormName);
			LoadSortList(FormName);
		}

		private void SetHeaderGridProperties(ref object GridName)
		{
			// THIS ROUTINE CAN BE USED IF YOU WANT SPECIFIC GRID FORMATTING DONE
			// THIS IS FORMATTING FOR THE GRID THAT DISPLAYS THE HEADINGS
			switch (Strings.UCase(modCustomReport.Statics.strReportType))
			{
			// Case "DOGS"
			// 
			// 
			// Case "BIRTHS"
				default:
					{
						// GridName.BackColor = vbBlue
						//						// GridName.ForeColor = System.Drawing.Color.White
						break;
					}
			}
			//end switch
			// GridName.Select 0, 0, 0, GridName.Cols - 1
			// GridName.CellFontBold = True
		}

		private void SetDataGridProperties(ref object GridName)
		{
			// THIS ROUTINE CAN BE USED IF YOU WANT SPECIFIC GRID FORMATTING DONE
			// THIS IS FORMATTING FOR THE GRID THAT DISPLAYS THE REPORTS DATA
			if (Strings.UCase(modCustomReport.Statics.strReportType) == "DOGS")
			{
			}
			else if (Strings.UCase(modCustomReport.Statics.strReportType) == "BIRTHS")
			{
			}
		}
		// VBto upgrade warning: FormName As Form	OnWrite(frmCustomLabels)
		private void LoadSortList(dynamic FormName)
		{
			// LOAD THE SORT LIST BOX WITH THE FIELDS FROM THE FIELDS LIST BOX
			int intCounter;
			FormName.lstSort.Clear();
			for (intCounter = 0; intCounter <= FormName.vsWhere.Rows - 1; intCounter++)
			{
				FormName.lstSort.AddItem(FormName.vsWhere.TextMatrix(intCounter, 0));
				FormName.lstSort.ItemData(FormName.lstSort.NewIndex, intCounter);
			}
			// set teh default sort list
			if (intRateType == 0)
			{
				// do nothing
			}
			else if ((intRateType >= 10 && intRateType <= 12) || (intRateType >= 20 && intRateType <= 22))
			{
				if (lstSort.Items.Count > 1)
				{
					lstSort.SetSelected(0, true);
					lstSort.SetSelected(1, true);
				}
			}
		}
		// VBto upgrade warning: FormName As Form	OnWrite(frmCustomLabels)
		private void LoadWhereGrid(dynamic FormName)
		{
			// LOAD THE WHERE GRID WITH FIELDS FROM THE FIELD LIST BOX
			int intCounter/*unused?*/;
			int GridWidth;
			FormName.vsWhere.Rows = 0;
			// SET THE NUMBER OF COLUMNS AND THE WIDTH OF THOSE COLUMNS
			GridWidth = FormName.vsWhere.WidthOriginal;
			FormName.vsWhere.Cols = 4;
			FormName.vsWhere.ColWidth(0, FCConvert.ToInt32(0.36 * GridWidth));
			FormName.vsWhere.ColWidth(1, FCConvert.ToInt32(0.31 * GridWidth));
			FormName.vsWhere.ColWidth(2, FCConvert.ToInt32(0.31 * GridWidth));
			FormName.vsWhere.ColWidth(3, 0);
			// hidden for the field name
			if (modCustomReport.Statics.strReportType == "MORTGAGEHOLDER")
			{
				FormName.vsWhere.AddItem("Name" + "\t" + "\t" + "\t" + "Name");
				FormName.vsWhere.AddItem("Holder Number" + "\t" + "\t" + "\t" + "MortgageHolderID");
				FormName.vsWhere.AddItem("Zip" + "\t" + "\t" + "\t" + "Zip");
			}
			else if ((modCustomReport.Statics.strReportType == "LABELS") || (modCustomReport.Statics.strReportType == "FORMS"))
			{
				FormName.vsWhere.AddItem("Name" + "\t" + "\t" + "\t" + "Name1");
				FormName.vsWhere.AddItem("Account" + "\t" + "\t" + "\t" + "Account");
				FormName.vsWhere.Visible = true;
			}
			// set the height of the Grid
			//FC:FINAL:AM: don't set the height; used anchoring instead
			//vsWhere.HeightOriginal = (vsWhere.Rows * vsWhere.RowHeight(0)) + 70;
			// turn off the scroll bars
			vsWhere.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			// set the width of the columns
			vsWhere.ColWidth(0, FCConvert.ToInt32(vsWhere.WidthOriginal / 3.0));
			vsWhere.ColWidth(1, FCConvert.ToInt32(vsWhere.WidthOriginal / 3.0));
			vsWhere.ColWidth(2, FCConvert.ToInt32(vsWhere.WidthOriginal / 3.0));
			vsWhere.ExtendLastCol = true;
		}

		private void LoadGridCellAsCombo(ref Form FormName, int intRowNumber, string strSQL, string FieldName = "", string IDField = "", string DatabaseFieldName = "")
		{
			// THIS ROUTINE BUILDS A COMBO LIST THAT MAKE THE CELL WORK LIKE A
			// COMBO BOX ONCE THE USER CLICKS ON IT. THE LIST ARE GOVERNED BY THE
			// ROW THAT IT IS IN THE GIRD.
			// THE SECOND ELEMENT IN THE ARRAY HOLDS THE DATABASE FIELD NAME SO
			// WHEN THE WHERE CLAUSE IS BUILT IT WILL KNOW WHAT FIELD NAME TO USE
			if (Information.IsNothing(FieldName) || FieldName == string.Empty)
			{
				modCustomReport.Statics.strComboList[intRowNumber, 0] = strSQL;
				// ElseIf UCase(FieldName) = "LISTTAB" Then
			}
			else
			{
				int intCounter/*unused?*/;
				clsDRWrapper rsCombo = new clsDRWrapper();
				rsCombo.OpenRecordset(strSQL, "TWRE0000.vb1");
				while (!rsCombo.EndOfFile())
				{
					modCustomReport.Statics.strComboList[intRowNumber, 0] += "|";
					modCustomReport.Statics.strComboList[intRowNumber, 0] += "#" + FCConvert.ToString(rsCombo.Get_Fields(IDField)) + ";" + FCConvert.ToString(rsCombo.Get_Fields(FieldName));
					modCustomReport.Statics.strComboList[intRowNumber, 1] = DatabaseFieldName;
					rsCombo.MoveNext();
				}
			}
		}

		private void FillLabelTypeCombo()
		{
			int counter;
			// hide labels that are not supported by BD because of the 5 lines I need to print for an address
			for (counter = 0; counter <= labLabelTypes.TypeCount - 1; counter++)
			{
				if (labLabelTypes.Get_ID(counter) == modLabels.CNSTLBLTYPE5066)
				{
					labLabelTypes.Set_Visible(counter, false);
					break;
				}
			}
			// fill combo box with all available types of labels
			for (counter = 0; counter <= labLabelTypes.TypeCount - 1; counter++)
			{
				if (labLabelTypes.Get_Visible(counter))
				{
					cmbLabelType.AddItem(labLabelTypes.Get_Caption(counter));
					cmbLabelType.ItemData(cmbLabelType.NewIndex, labLabelTypes.Get_ID(counter));
				}
				else
				{
					if (labLabelTypes.Get_ID(counter) == modLabels.CNSTLBLTYPEDYMO30256 || labLabelTypes.Get_ID(counter) == modLabels.CNSTLBLTYPE4065 || labLabelTypes.Get_ID(counter) == modLabels.CNSTLBLTYPEDYMO4150)
					{
						cmbLabelType.AddItem(labLabelTypes.Get_Caption(counter));
						cmbLabelType.ItemData(cmbLabelType.NewIndex, labLabelTypes.Get_ID(counter));
					}
				}
			}
			// With cmbLabelType
			// .AddItem ("Avery 4013")
			// .AddItem ("Avery 4014")
			// .AddItem ("Avery 5160,5260,5970")
			// .AddItem ("Avery 5161,5261,5661")
			// .AddItem ("Avery 5162,5262,5662")
			// .AddItem ("Avery 5163,5263,5663")
			// End With
		}

		private void FillFormTypeCombo()
		{
			cmbFormType.AddItem("Hygrade Certified Mailer (Laser Format)");
			cmbFormType.ItemData(cmbFormType.NewIndex, 0);
			cmbFormType.SelectedIndex = 0;
		}

		private void LoadNotesText()
		{
			// this will put the text in the note textbox
			string strText = "";
			if (Strings.UCase(modCustomReport.Statics.strReportType) == "MORTGAGEHOLDER")
			{
				strTemp = "1. Create a new report by from the drop down box on the right of the screen." + "\r\n" + "\r\n";
				strTemp += "2. Choose the fields that you would like on your report from the 'Fields to display on Report'." + "\r\n" + "\r\n";
				strTemp += "3. The 'Fields to Sort By' section provides a way to choose the order of the report.  If you select Account, then the report will have accounts listed by their account number." + "\r\n" + "\r\n";
				strTemp += "4. Set any criteria needed in the 'Select Search Criteria' list." + "\r\n" + "\r\n";
				strTemp += "Other Notes:" + "\r\n" + "    Some fields will be printed automatically." + "\r\n" + "\r\n";
				strTemp += "\r\n" + "    If you choose a default report, only the account number and the tax year criteria will effect the report.  Any changes in the other fields will not be used in the creation of the report." + "\r\n";
			}
			else if (Strings.UCase(modCustomReport.Statics.strReportType) == "LABELS")
			{
				strTemp = "1. Select the type of ." + "\r\n" + "\r\n";
				strTemp += "2. Choose the fields that you would like on your report from the 'Fields to display on Report'." + "\r\n" + "\r\n";
				strTemp += "3. The 'Fields to Sort By' section provides a way to choose the order of the report.  If you select Account, then the report will have accounts listed by their account number." + "\r\n" + "\r\n";
				strTemp += "4. Set any criteria needed in the 'Select Search Criteria' list." + "\r\n" + "\r\n";
				strTemp += "Other Notes:" + "\r\n" + "    Some fields will be printed automatically." + "\r\n" + "\r\n";
				strTemp += "\r\n" + "    If you choose a default report, only the account number and the tax year criteria will effect the report.  Any changes in the other fields will not be used in the creation of the report." + "\r\n";
			}
			else if (Strings.UCase(modCustomReport.Statics.strReportType) == "FORMS")
			{
				strTemp = "1. Create a new report by from the drop down box on the right of the screen." + "\r\n" + "\r\n";
				strTemp += "2. Choose the fields that you would like on your report from the 'Fields to display on Report'." + "\r\n" + "\r\n";
				strTemp += "3. The 'Fields to Sort By' section provides a way to choose the order of the report.  If you select Account, then the report will have accounts listed by their account number." + "\r\n" + "\r\n";
				strTemp += "4. Set any criteria needed in the 'Select Search Criteria' list." + "\r\n" + "\r\n";
				strTemp += "Other Notes:" + "\r\n" + "    Some fields will be printed automatically." + "\r\n" + "\r\n";
				strTemp += "\r\n" + "    If you choose a default report, only the account number and the tax year criteria will effect the report.  Any changes in the other fields will not be used in the creation of the report." + "\r\n";
			}
			//txtNotes.Text = strText;
		}

		private void SetCopiesChkBoxes()
		{
			// kgk trocl-840  Set forms so print checkboxes to match the selections made when printing 30-Day Notices
			clsDRWrapper rsControl = new clsDRWrapper();
			string strCntlTbl = "";
			switch (intRateType)
			{
				case 20:
					{
						strCntlTbl = "Control_30DayNotice";
						break;
					}
				case 21:
					{
						strCntlTbl = "Control_LienProcess";
						break;
					}
				case 22:
					{
						strCntlTbl = "Control_LienMaturity";
						break;
					}
			}
			//end switch
			rsControl.OpenRecordset("SELECT * FROM " + strCntlTbl, modExtraModules.strCLDatabase);
			if (FCConvert.ToBoolean(rsControl.Get_Fields_Boolean("PayCertMailFee")))
			{
				chkFormPrintOptions[0].CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkFormPrintOptions[0].CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(rsControl.Get_Fields_Boolean("SendCopyToMortHolder")))
			{
				if (FCConvert.ToBoolean(rsControl.Get_Fields_Boolean("ChargeForMortHolder")))
				{
					chkFormPrintOptions[1].CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkFormPrintOptions[1].CheckState = Wisej.Web.CheckState.Unchecked;
				}
			}
			else
			{
				chkFormPrintOptions[1].CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (FCConvert.ToString(rsControl.Get_Fields_String("SendCopyToNewOwner")) != "")
			{
				if (Strings.InStr(FCConvert.ToString(rsControl.Get_Fields_String("SendCopyToNewOwner")), "Yes, charge", CompareConstants.vbTextCompare/*?*/) != 0)
				{
					chkFormPrintOptions[2].CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkFormPrintOptions[2].CheckState = Wisej.Web.CheckState.Unchecked;
				}
			}
			else
			{
				chkFormPrintOptions[2].CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(rsControl.Get_Fields_Boolean("SendCopyToIntParty")))
			{
				if (FCConvert.ToBoolean(rsControl.Get_Fields_Boolean("ChargeForIntParty")))
				{
					chkFormPrintOptions[3].CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkFormPrintOptions[3].CheckState = Wisej.Web.CheckState.Unchecked;
				}
			}
			else
			{
				chkFormPrintOptions[3].CheckState = Wisej.Web.CheckState.Unchecked;
			}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuPrint_Click(sender, e);
		}

		private void cmdSear_Click(object sender, EventArgs e)
		{
			this.mnuClear_Click(sender, e);
		}
	}
}
