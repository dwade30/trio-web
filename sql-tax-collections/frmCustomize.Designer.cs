﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmCustomize.
	/// </summary>
	partial class frmCustomize : BaseForm
	{
		public fecherFoundation.FCComboBox cmbAssessmentValues;
		public fecherFoundation.FCComboBox cmbBasis;
		public fecherFoundation.FCComboBox cmbOptions;
		public fecherFoundation.FCComboBox cmbTCOrder;
		public fecherFoundation.FCComboBox cmbLoadbackOrder;
		public fecherFoundation.FCComboBox cmbReceiptSize;
		public fecherFoundation.FCComboBox cmbSeq;
		public fecherFoundation.FCFrame fraAdjustments;
		public fecherFoundation.FCFrame frmReminderAdjustments;
		public fecherFoundation.FCTextBox txtReminderFormAdjustH;
		public fecherFoundation.FCTextBox txtReminderFormAdjustV;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCFrame fraLienAdjustment;
		public fecherFoundation.FCTextBox txtLDNSideAdjust;
		public fecherFoundation.FCTextBox txtLienSideAdjust;
		public fecherFoundation.FCTextBox txtLDNBottomAdjust;
		public fecherFoundation.FCTextBox txtLienBottomAdjust;
		public fecherFoundation.FCTextBox txtLienTopAdjust;
		public fecherFoundation.FCTextBox txtLDNTopAdjust;
		public fecherFoundation.FCLabel lblLDNSideAdj;
		public fecherFoundation.FCLabel lblLienSideAdj;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCFrame frmLaserAdjustment;
		public fecherFoundation.FCTextBox txtSigAdjust_Lien;
		public fecherFoundation.FCTextBox txtCMFAdjust;
		public fecherFoundation.FCTextBox txtLabelAdjustment;
		public fecherFoundation.FCLabel lblSigAdjust_Lien;
		public fecherFoundation.FCLabel lblCMFAdjust;
		public fecherFoundation.FCLabel lblLabelAdjustment;
		public fecherFoundation.FCFrame fraPaymentOptions;
		public fecherFoundation.FCCheckBox chkDisableAutoPmtFill;
		public fecherFoundation.FCCheckBox chkDefaultRTDDate;
		public fecherFoundation.FCCheckBox chkDefaultToAuto;
		public fecherFoundation.FCCheckBox chkDoNotAutofillPrepay;
		public fecherFoundation.FCFrame fraRateInfo;
		public fecherFoundation.FCFrame frmRate;
		public fecherFoundation.FCCheckBox chkUseRKTaxRate;
		public fecherFoundation.FCTextBox txtAbatementInterest;
		public fecherFoundation.FCTextBox txtDiscountPercent;
		public fecherFoundation.FCTextBox txtPPayInterestRate;
		public fecherFoundation.FCLabel lblAbatementInterest;
		public fecherFoundation.FCLabel lblDiscountPercent;
		public fecherFoundation.FCLabel lblPPayInterestRate;
		public fecherFoundation.FCFrame fraBasis;
		public fecherFoundation.FCLabel lblBasis;
		public fecherFoundation.FCFrame fraOtherOptions;
		public fecherFoundation.FCCheckBox chkShowReceiptOnAcctDetail;
		public fecherFoundation.FCFrame fraTownInformation;
		public fecherFoundation.FCTextBox txtTaxInfoSheetMsg;
		public fecherFoundation.FCCheckBox chkExtraTCPage;
		public fecherFoundation.FCFrame fraTownSeal;
		public fecherFoundation.FCCheckBox chkTownSealLienDischarge;
		public fecherFoundation.FCCheckBox chkTownSealLienMaturity;
		public fecherFoundation.FCCheckBox chkTownSealLien;
		public fecherFoundation.FCCheckBox chkTownSeal30DN;
		public fecherFoundation.FCTextBox txtLienAbate;
		public fecherFoundation.FCLabel lblTaxInfoMsg;
		public fecherFoundation.FCLabel lblLienAbate;
		public fecherFoundation.FCFrame fraAuditOptions;
		public fecherFoundation.FCFrame fraReportSequence;
		public fecherFoundation.FCCheckBox chkMapLot;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCFrame fraDefaultAccount;
		public fecherFoundation.FCCheckBox chkDefaultAccount;
		public fecherFoundation.FCFrame fraTaxService;
		public fecherFoundation.FCCheckBox chkTSReminders;
		public fecherFoundation.FCGrid vsTSEmailGrid;
		public fecherFoundation.FCLabel lblTaxSrvcEmail;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomize));
            this.cmbAssessmentValues = new fecherFoundation.FCComboBox();
            this.cmbBasis = new fecherFoundation.FCComboBox();
            this.cmbOptions = new fecherFoundation.FCComboBox();
            this.cmbTCOrder = new fecherFoundation.FCComboBox();
            this.cmbLoadbackOrder = new fecherFoundation.FCComboBox();
            this.cmbReceiptSize = new fecherFoundation.FCComboBox();
            this.cmbSeq = new fecherFoundation.FCComboBox();
            this.fraAdjustments = new fecherFoundation.FCFrame();
            this.frmLaserAdjustment = new fecherFoundation.FCFrame();
            this.txtSigAdjust_Lien = new fecherFoundation.FCTextBox();
            this.txtCMFAdjust = new fecherFoundation.FCTextBox();
            this.txtLabelAdjustment = new fecherFoundation.FCTextBox();
            this.lblSigAdjust_Lien = new fecherFoundation.FCLabel();
            this.lblCMFAdjust = new fecherFoundation.FCLabel();
            this.lblLabelAdjustment = new fecherFoundation.FCLabel();
            this.frmReminderAdjustments = new fecherFoundation.FCFrame();
            this.txtReminderFormAdjustH = new fecherFoundation.FCTextBox();
            this.txtReminderFormAdjustV = new fecherFoundation.FCTextBox();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.fraLienAdjustment = new fecherFoundation.FCFrame();
            this.txtLDNSideAdjust = new fecherFoundation.FCTextBox();
            this.txtLienTopAdjust = new fecherFoundation.FCTextBox();
            this.txtLDNBottomAdjust = new fecherFoundation.FCTextBox();
            this.txtLienSideAdjust = new fecherFoundation.FCTextBox();
            this.txtLienBottomAdjust = new fecherFoundation.FCTextBox();
            this.Label7 = new fecherFoundation.FCLabel();
            this.lblLDNSideAdj = new fecherFoundation.FCLabel();
            this.txtLDNTopAdjust = new fecherFoundation.FCTextBox();
            this.lblLienSideAdj = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.fraPaymentOptions = new fecherFoundation.FCFrame();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.chkDisableAutoPmtFill = new fecherFoundation.FCCheckBox();
            this.chkDefaultRTDDate = new fecherFoundation.FCCheckBox();
            this.chkDefaultToAuto = new fecherFoundation.FCCheckBox();
            this.chkDoNotAutofillPrepay = new fecherFoundation.FCCheckBox();
            this.fraRateInfo = new fecherFoundation.FCFrame();
            this.frmRate = new fecherFoundation.FCFrame();
            this.chkUseRKTaxRate = new fecherFoundation.FCCheckBox();
            this.txtAbatementInterest = new fecherFoundation.FCTextBox();
            this.txtDiscountPercent = new fecherFoundation.FCTextBox();
            this.txtPPayInterestRate = new fecherFoundation.FCTextBox();
            this.lblAbatementInterest = new fecherFoundation.FCLabel();
            this.lblDiscountPercent = new fecherFoundation.FCLabel();
            this.lblPPayInterestRate = new fecherFoundation.FCLabel();
            this.fraBasis = new fecherFoundation.FCFrame();
            this.lblBasis = new fecherFoundation.FCLabel();
            this.fraDefaultAccount = new fecherFoundation.FCFrame();
            this.chkDefaultAccount = new fecherFoundation.FCCheckBox();
            this.fcLabel3 = new fecherFoundation.FCLabel();
            this.fraOtherOptions = new fecherFoundation.FCFrame();
            this.chkShowReceiptOnAcctDetail = new fecherFoundation.FCCheckBox();
            this.fraTownInformation = new fecherFoundation.FCFrame();
            this.txtTaxInfoSheetMsg = new fecherFoundation.FCTextBox();
            this.lblReceiptSize = new fecherFoundation.FCLabel();
            this.lblLoadbackOrder = new fecherFoundation.FCLabel();
            this.chkExtraTCPage = new fecherFoundation.FCCheckBox();
            this.fraTownSeal = new fecherFoundation.FCFrame();
            this.chkTownSealLienDischarge = new fecherFoundation.FCCheckBox();
            this.chkTownSealLienMaturity = new fecherFoundation.FCCheckBox();
            this.chkTownSealLien = new fecherFoundation.FCCheckBox();
            this.chkTownSeal30DN = new fecherFoundation.FCCheckBox();
            this.txtLienAbate = new fecherFoundation.FCTextBox();
            this.lblTaxInfoMsg = new fecherFoundation.FCLabel();
            this.lblLienAbate = new fecherFoundation.FCLabel();
            this.fraAuditOptions = new fecherFoundation.FCFrame();
            this.fraReportSequence = new fecherFoundation.FCFrame();
            this.chkMapLot = new fecherFoundation.FCCheckBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.fraTaxService = new fecherFoundation.FCFrame();
            this.chkTSReminders = new fecherFoundation.FCCheckBox();
            this.vsTSEmailGrid = new fecherFoundation.FCGrid();
            this.lblTaxSrvcEmail = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.fcLabel2 = new fecherFoundation.FCLabel();
            this.lblOptions = new fecherFoundation.FCLabel();
            this.btnProcess = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraAdjustments)).BeginInit();
            this.fraAdjustments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.frmLaserAdjustment)).BeginInit();
            this.frmLaserAdjustment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.frmReminderAdjustments)).BeginInit();
            this.frmReminderAdjustments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraLienAdjustment)).BeginInit();
            this.fraLienAdjustment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraPaymentOptions)).BeginInit();
            this.fraPaymentOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDisableAutoPmtFill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDefaultRTDDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDefaultToAuto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDoNotAutofillPrepay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraRateInfo)).BeginInit();
            this.fraRateInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.frmRate)).BeginInit();
            this.frmRate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseRKTaxRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraBasis)).BeginInit();
            this.fraBasis.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraDefaultAccount)).BeginInit();
            this.fraDefaultAccount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDefaultAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraOtherOptions)).BeginInit();
            this.fraOtherOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowReceiptOnAcctDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraTownInformation)).BeginInit();
            this.fraTownInformation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkExtraTCPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraTownSeal)).BeginInit();
            this.fraTownSeal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkTownSealLienDischarge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTownSealLienMaturity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTownSealLien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTownSeal30DN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraAuditOptions)).BeginInit();
            this.fraAuditOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraReportSequence)).BeginInit();
            this.fraReportSequence.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkMapLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraTaxService)).BeginInit();
            this.fraTaxService.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkTSReminders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsTSEmailGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 705);
            this.BottomPanel.Size = new System.Drawing.Size(929, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraTaxService);
            this.ClientArea.Controls.Add(this.fraAuditOptions);
            this.ClientArea.Controls.Add(this.fraPaymentOptions);
            this.ClientArea.Controls.Add(this.fraOtherOptions);
            this.ClientArea.Controls.Add(this.fraRateInfo);
            this.ClientArea.Controls.Add(this.fraAdjustments);
            this.ClientArea.Controls.Add(this.lblOptions);
            this.ClientArea.Controls.Add(this.cmbOptions);
            this.ClientArea.Controls.Add(this.fraTownInformation);
            this.ClientArea.Size = new System.Drawing.Size(929, 645);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(929, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(128, 30);
            this.HeaderText.Text = "Customize";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbAssessmentValues
            // 
            this.cmbAssessmentValues.Items.AddRange(new object[] {
            "Current Assessment",
            "Last Billed Assessment",
            "Prompt"});
            this.cmbAssessmentValues.Location = new System.Drawing.Point(242, 200);
            this.cmbAssessmentValues.Name = "cmbAssessmentValues";
            this.cmbAssessmentValues.Size = new System.Drawing.Size(204, 40);
            this.cmbAssessmentValues.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.cmbAssessmentValues, null);
            // 
            // cmbBasis
            // 
            this.cmbBasis.Items.AddRange(new object[] {
            "365 Days",
            "360 Days"});
            this.cmbBasis.Location = new System.Drawing.Point(220, 30);
            this.cmbBasis.Name = "cmbBasis";
            this.cmbBasis.Size = new System.Drawing.Size(121, 40);
            this.cmbBasis.TabIndex = 51;
            this.ToolTip1.SetToolTip(this.cmbBasis, null);
            this.cmbBasis.SelectedIndexChanged += new System.EventHandler(this.optBasis_Click);
            // 
            // cmbOptions
            // 
            this.cmbOptions.Items.AddRange(new object[] {
            "Rate Information",
            "Printing Adjustments",
            "Audit and Receipt Options",
            "Town Information",
            "Status and Payment Options",
            "Tax Service Options",
            "Other Options"});
            this.cmbOptions.Location = new System.Drawing.Point(190, 8);
            this.cmbOptions.Name = "cmbOptions";
            this.cmbOptions.Size = new System.Drawing.Size(269, 40);
            this.cmbOptions.TabIndex = 5;
            this.cmbOptions.Text = "Rate Information";
            this.ToolTip1.SetToolTip(this.cmbOptions, null);
            this.cmbOptions.SelectedIndexChanged += new System.EventHandler(this.optOptions_Click);
            // 
            // cmbTCOrder
            // 
            this.cmbTCOrder.Items.AddRange(new object[] {
            "Account",
            "Name"});
            this.cmbTCOrder.Location = new System.Drawing.Point(211, 375);
            this.cmbTCOrder.Name = "cmbTCOrder";
            this.cmbTCOrder.Size = new System.Drawing.Size(185, 40);
            this.cmbTCOrder.TabIndex = 7;
            this.ToolTip1.SetToolTip(this.cmbTCOrder, null);
            // 
            // cmbLoadbackOrder
            // 
            this.cmbLoadbackOrder.Items.AddRange(new object[] {
            "Year, Account",
            "Account, Year",
            "Name, Year",
            "Year, Name"});
            this.cmbLoadbackOrder.Location = new System.Drawing.Point(211, 324);
            this.cmbLoadbackOrder.Name = "cmbLoadbackOrder";
            this.cmbLoadbackOrder.Size = new System.Drawing.Size(185, 40);
            this.cmbLoadbackOrder.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.cmbLoadbackOrder, null);
            // 
            // cmbReceiptSize
            // 
            this.cmbReceiptSize.Items.AddRange(new object[] {
            "Wide",
            "Narrow"});
            this.cmbReceiptSize.Location = new System.Drawing.Point(154, 191);
            this.cmbReceiptSize.Name = "cmbReceiptSize";
            this.cmbReceiptSize.Size = new System.Drawing.Size(154, 40);
            this.cmbReceiptSize.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.cmbReceiptSize, null);
            this.cmbReceiptSize.SelectedIndexChanged += new System.EventHandler(this.optReceiptSize_Click);
            // 
            // cmbSeq
            // 
            this.cmbSeq.Items.AddRange(new object[] {
            "Receipt Number",
            "Account "});
            this.cmbSeq.Location = new System.Drawing.Point(134, 30);
            this.cmbSeq.Name = "cmbSeq";
            this.cmbSeq.Size = new System.Drawing.Size(154, 40);
            this.cmbSeq.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.cmbSeq, null);
            this.cmbSeq.SelectedIndexChanged += new System.EventHandler(this.optSeq_Click);
            // 
            // fraAdjustments
            // 
            this.fraAdjustments.AppearanceKey = "groupBoxLeftBorder";
            this.fraAdjustments.Controls.Add(this.frmLaserAdjustment);
            this.fraAdjustments.Controls.Add(this.frmReminderAdjustments);
            this.fraAdjustments.Controls.Add(this.fraLienAdjustment);
            this.fraAdjustments.Location = new System.Drawing.Point(30, 65);
            this.fraAdjustments.Name = "fraAdjustments";
            this.fraAdjustments.Size = new System.Drawing.Size(874, 587);
            this.fraAdjustments.TabIndex = 53;
            this.fraAdjustments.Text = "Printing Adjustments";
            this.ToolTip1.SetToolTip(this.fraAdjustments, null);
            this.fraAdjustments.UseMnemonic = false;
            this.fraAdjustments.Visible = false;
            // 
            // frmLaserAdjustment
            // 
            this.frmLaserAdjustment.Controls.Add(this.txtSigAdjust_Lien);
            this.frmLaserAdjustment.Controls.Add(this.txtCMFAdjust);
            this.frmLaserAdjustment.Controls.Add(this.txtLabelAdjustment);
            this.frmLaserAdjustment.Controls.Add(this.lblSigAdjust_Lien);
            this.frmLaserAdjustment.Controls.Add(this.lblCMFAdjust);
            this.frmLaserAdjustment.Controls.Add(this.lblLabelAdjustment);
            this.frmLaserAdjustment.Location = new System.Drawing.Point(20, 40);
            this.frmLaserAdjustment.Name = "frmLaserAdjustment";
            this.frmLaserAdjustment.Size = new System.Drawing.Size(346, 183);
            this.frmLaserAdjustment.Text = "Laser Adjustment";
            this.ToolTip1.SetToolTip(this.frmLaserAdjustment, null);
            this.frmLaserAdjustment.UseMnemonic = false;
            // 
            // txtSigAdjust_Lien
            // 
            this.txtSigAdjust_Lien.BackColor = System.Drawing.SystemColors.Window;
            this.txtSigAdjust_Lien.Location = new System.Drawing.Point(250, 126);
            this.txtSigAdjust_Lien.MaxLength = 5;
            this.txtSigAdjust_Lien.Name = "txtSigAdjust_Lien";
            this.txtSigAdjust_Lien.Size = new System.Drawing.Size(70, 40);
            this.txtSigAdjust_Lien.TabIndex = 5;
            this.txtSigAdjust_Lien.Text = "0";
            this.txtSigAdjust_Lien.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtSigAdjust_Lien, null);
            this.txtSigAdjust_Lien.Enter += new System.EventHandler(this.txtSigAdjust_Lien_Enter);
            this.txtSigAdjust_Lien.TextChanged += new System.EventHandler(this.txtSigAdjust_Lien_TextChanged);
            this.txtSigAdjust_Lien.Validating += new System.ComponentModel.CancelEventHandler(this.txtSigAdjust_Lien_Validating);
            this.txtSigAdjust_Lien.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtSigAdjust_Lien_KeyPress);
            // 
            // txtCMFAdjust
            // 
            this.txtCMFAdjust.BackColor = System.Drawing.SystemColors.Window;
            this.txtCMFAdjust.Location = new System.Drawing.Point(250, 30);
            this.txtCMFAdjust.MaxLength = 5;
            this.txtCMFAdjust.Name = "txtCMFAdjust";
            this.txtCMFAdjust.Size = new System.Drawing.Size(70, 40);
            this.txtCMFAdjust.TabIndex = 1;
            this.txtCMFAdjust.Text = "0";
            this.txtCMFAdjust.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtCMFAdjust, null);
            this.txtCMFAdjust.Enter += new System.EventHandler(this.txtCMFAdjust_Enter);
            this.txtCMFAdjust.Validating += new System.ComponentModel.CancelEventHandler(this.txtCMFAdjust_Validating);
            this.txtCMFAdjust.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCMFAdjust_KeyPress);
            // 
            // txtLabelAdjustment
            // 
            this.txtLabelAdjustment.BackColor = System.Drawing.SystemColors.Window;
            this.txtLabelAdjustment.Location = new System.Drawing.Point(250, 78);
            this.txtLabelAdjustment.MaxLength = 5;
            this.txtLabelAdjustment.Name = "txtLabelAdjustment";
            this.txtLabelAdjustment.Size = new System.Drawing.Size(70, 40);
            this.txtLabelAdjustment.TabIndex = 3;
            this.txtLabelAdjustment.Text = "0";
            this.txtLabelAdjustment.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtLabelAdjustment, null);
            this.txtLabelAdjustment.Enter += new System.EventHandler(this.txtLabelAdjustment_Enter);
            this.txtLabelAdjustment.TextChanged += new System.EventHandler(this.txtLabelAdjustment_TextChanged);
            this.txtLabelAdjustment.Validating += new System.ComponentModel.CancelEventHandler(this.txtLabelAdjustment_Validating);
            this.txtLabelAdjustment.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtLabelAdjustment_KeyPress);
            // 
            // lblSigAdjust_Lien
            // 
            this.lblSigAdjust_Lien.AutoSize = true;
            this.lblSigAdjust_Lien.Location = new System.Drawing.Point(20, 140);
            this.lblSigAdjust_Lien.Name = "lblSigAdjust_Lien";
            this.lblSigAdjust_Lien.Size = new System.Drawing.Size(160, 15);
            this.lblSigAdjust_Lien.TabIndex = 4;
            this.lblSigAdjust_Lien.Text = "LIEN NOTICE SIGNATURE";
            this.lblSigAdjust_Lien.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblSigAdjust_Lien, null);
            // 
            // lblCMFAdjust
            // 
            this.lblCMFAdjust.AutoSize = true;
            this.lblCMFAdjust.Location = new System.Drawing.Point(20, 44);
            this.lblCMFAdjust.Name = "lblCMFAdjust";
            this.lblCMFAdjust.Size = new System.Drawing.Size(153, 15);
            this.lblCMFAdjust.TabIndex = 6;
            this.lblCMFAdjust.Text = "CERTIFIED MAIL FORMS";
            this.lblCMFAdjust.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblCMFAdjust, null);
            // 
            // lblLabelAdjustment
            // 
            this.lblLabelAdjustment.AutoSize = true;
            this.lblLabelAdjustment.Location = new System.Drawing.Point(20, 92);
            this.lblLabelAdjustment.Name = "lblLabelAdjustment";
            this.lblLabelAdjustment.Size = new System.Drawing.Size(196, 15);
            this.lblLabelAdjustment.TabIndex = 2;
            this.lblLabelAdjustment.Text = "LABEL VERTICAL ADJUSTMENT";
            this.lblLabelAdjustment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblLabelAdjustment, null);
            // 
            // frmReminderAdjustments
            // 
            this.frmReminderAdjustments.Controls.Add(this.txtReminderFormAdjustH);
            this.frmReminderAdjustments.Controls.Add(this.txtReminderFormAdjustV);
            this.frmReminderAdjustments.Controls.Add(this.Label5);
            this.frmReminderAdjustments.Controls.Add(this.Label4);
            this.frmReminderAdjustments.Location = new System.Drawing.Point(20, 263);
            this.frmReminderAdjustments.Name = "frmReminderAdjustments";
            this.frmReminderAdjustments.Size = new System.Drawing.Size(346, 138);
            this.frmReminderAdjustments.TabIndex = 2;
            this.frmReminderAdjustments.Text = "Reminder Notice Form Adjustment (Inches)";
            this.ToolTip1.SetToolTip(this.frmReminderAdjustments, null);
            this.frmReminderAdjustments.UseMnemonic = false;
            // 
            // txtReminderFormAdjustH
            // 
            this.txtReminderFormAdjustH.BackColor = System.Drawing.SystemColors.Window;
            this.txtReminderFormAdjustH.Location = new System.Drawing.Point(250, 30);
            this.txtReminderFormAdjustH.MaxLength = 5;
            this.txtReminderFormAdjustH.Name = "txtReminderFormAdjustH";
            this.txtReminderFormAdjustH.Size = new System.Drawing.Size(70, 40);
            this.txtReminderFormAdjustH.TabIndex = 1;
            this.txtReminderFormAdjustH.Text = "0";
            this.txtReminderFormAdjustH.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtReminderFormAdjustH, null);
            this.txtReminderFormAdjustH.Enter += new System.EventHandler(this.txtReminderFormAdjustH_Enter);
            this.txtReminderFormAdjustH.TextChanged += new System.EventHandler(this.txtReminderFormAdjustH_TextChanged);
            this.txtReminderFormAdjustH.Validating += new System.ComponentModel.CancelEventHandler(this.txtReminderFormAdjustH_Validating);
            this.txtReminderFormAdjustH.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtReminderFormAdjustH_KeyPress);
            // 
            // txtReminderFormAdjustV
            // 
            this.txtReminderFormAdjustV.BackColor = System.Drawing.SystemColors.Window;
            this.txtReminderFormAdjustV.Location = new System.Drawing.Point(250, 81);
            this.txtReminderFormAdjustV.MaxLength = 5;
            this.txtReminderFormAdjustV.Name = "txtReminderFormAdjustV";
            this.txtReminderFormAdjustV.Size = new System.Drawing.Size(70, 40);
            this.txtReminderFormAdjustV.TabIndex = 3;
            this.txtReminderFormAdjustV.Text = "0";
            this.txtReminderFormAdjustV.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtReminderFormAdjustV, null);
            this.txtReminderFormAdjustV.Enter += new System.EventHandler(this.txtReminderFormAdjustV_Enter);
            this.txtReminderFormAdjustV.TextChanged += new System.EventHandler(this.txtReminderFormAdjustV_TextChanged);
            this.txtReminderFormAdjustV.Validating += new System.ComponentModel.CancelEventHandler(this.txtReminderFormAdjustV_Validating);
            this.txtReminderFormAdjustV.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtReminderFormAdjustV_KeyPress);
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(20, 44);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(174, 15);
            this.Label5.TabIndex = 4;
            this.Label5.Text = "HORIZONTAL ADJUSTMENT";
            this.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.Label5, null);
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(20, 95);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(154, 15);
            this.Label4.TabIndex = 2;
            this.Label4.Text = "VERTICAL ADJUSTMENT";
            this.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.Label4, null);
            // 
            // fraLienAdjustment
            // 
            this.fraLienAdjustment.Controls.Add(this.txtLDNSideAdjust);
            this.fraLienAdjustment.Controls.Add(this.txtLienTopAdjust);
            this.fraLienAdjustment.Controls.Add(this.txtLDNBottomAdjust);
            this.fraLienAdjustment.Controls.Add(this.txtLienSideAdjust);
            this.fraLienAdjustment.Controls.Add(this.txtLienBottomAdjust);
            this.fraLienAdjustment.Controls.Add(this.Label7);
            this.fraLienAdjustment.Controls.Add(this.lblLDNSideAdj);
            this.fraLienAdjustment.Controls.Add(this.txtLDNTopAdjust);
            this.fraLienAdjustment.Controls.Add(this.lblLienSideAdj);
            this.fraLienAdjustment.Controls.Add(this.Label6);
            this.fraLienAdjustment.Controls.Add(this.Label3);
            this.fraLienAdjustment.Controls.Add(this.Label2);
            this.fraLienAdjustment.Location = new System.Drawing.Point(404, 40);
            this.fraLienAdjustment.Name = "fraLienAdjustment";
            this.fraLienAdjustment.Size = new System.Drawing.Size(371, 332);
            this.fraLienAdjustment.TabIndex = 3;
            this.fraLienAdjustment.Text = "Registry Adjustments (Inches)";
            this.ToolTip1.SetToolTip(this.fraLienAdjustment, null);
            this.fraLienAdjustment.UseMnemonic = false;
            // 
            // txtLDNSideAdjust
            // 
            this.txtLDNSideAdjust.BackColor = System.Drawing.SystemColors.Window;
            this.txtLDNSideAdjust.Location = new System.Drawing.Point(280, 280);
            this.txtLDNSideAdjust.MaxLength = 5;
            this.txtLDNSideAdjust.Name = "txtLDNSideAdjust";
            this.txtLDNSideAdjust.Size = new System.Drawing.Size(70, 40);
            this.txtLDNSideAdjust.TabIndex = 11;
            this.txtLDNSideAdjust.Text = "0";
            this.txtLDNSideAdjust.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtLDNSideAdjust, null);
            this.txtLDNSideAdjust.Enter += new System.EventHandler(this.txtLDNSideAdjust_Enter);
            this.txtLDNSideAdjust.TextChanged += new System.EventHandler(this.txtLDNSideAdjust_TextChanged);
            this.txtLDNSideAdjust.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtLDNSideAdjust_KeyPress);
            // 
            // txtLienTopAdjust
            // 
            this.txtLienTopAdjust.BackColor = System.Drawing.SystemColors.Window;
            this.txtLienTopAdjust.Location = new System.Drawing.Point(280, 30);
            this.txtLienTopAdjust.MaxLength = 5;
            this.txtLienTopAdjust.Name = "txtLienTopAdjust";
            this.txtLienTopAdjust.Size = new System.Drawing.Size(70, 40);
            this.txtLienTopAdjust.TabIndex = 1;
            this.txtLienTopAdjust.Text = "0";
            this.txtLienTopAdjust.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtLienTopAdjust, null);
            this.txtLienTopAdjust.Enter += new System.EventHandler(this.txtLienTopAdjust_Enter);
            this.txtLienTopAdjust.TextChanged += new System.EventHandler(this.txtLienTopAdjust_TextChanged);
            this.txtLienTopAdjust.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtLienTopAdjust_KeyPress);
            // 
            // txtLDNBottomAdjust
            // 
            this.txtLDNBottomAdjust.BackColor = System.Drawing.SystemColors.Window;
            this.txtLDNBottomAdjust.Location = new System.Drawing.Point(280, 230);
            this.txtLDNBottomAdjust.MaxLength = 5;
            this.txtLDNBottomAdjust.Name = "txtLDNBottomAdjust";
            this.txtLDNBottomAdjust.Size = new System.Drawing.Size(70, 40);
            this.txtLDNBottomAdjust.TabIndex = 9;
            this.txtLDNBottomAdjust.Text = "0";
            this.txtLDNBottomAdjust.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtLDNBottomAdjust, null);
            this.txtLDNBottomAdjust.Enter += new System.EventHandler(this.txtLDNBottomAdjust_Enter);
            this.txtLDNBottomAdjust.TextChanged += new System.EventHandler(this.txtLDNBottomAdjust_TextChanged);
            this.txtLDNBottomAdjust.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtLDNBottomAdjust_KeyPress);
            // 
            // txtLienSideAdjust
            // 
            this.txtLienSideAdjust.BackColor = System.Drawing.SystemColors.Window;
            this.txtLienSideAdjust.Location = new System.Drawing.Point(280, 130);
            this.txtLienSideAdjust.MaxLength = 5;
            this.txtLienSideAdjust.Name = "txtLienSideAdjust";
            this.txtLienSideAdjust.Size = new System.Drawing.Size(70, 40);
            this.txtLienSideAdjust.TabIndex = 5;
            this.txtLienSideAdjust.Text = "0";
            this.txtLienSideAdjust.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtLienSideAdjust, null);
            this.txtLienSideAdjust.Enter += new System.EventHandler(this.txtLienSideAdjust_Enter);
            this.txtLienSideAdjust.TextChanged += new System.EventHandler(this.txtLienSideAdjust_TextChanged);
            this.txtLienSideAdjust.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtLienSideAdjust_KeyPress);
            // 
            // txtLienBottomAdjust
            // 
            this.txtLienBottomAdjust.BackColor = System.Drawing.SystemColors.Window;
            this.txtLienBottomAdjust.Location = new System.Drawing.Point(280, 80);
            this.txtLienBottomAdjust.MaxLength = 5;
            this.txtLienBottomAdjust.Name = "txtLienBottomAdjust";
            this.txtLienBottomAdjust.Size = new System.Drawing.Size(70, 40);
            this.txtLienBottomAdjust.TabIndex = 3;
            this.txtLienBottomAdjust.Text = "0";
            this.txtLienBottomAdjust.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtLienBottomAdjust, null);
            this.txtLienBottomAdjust.Enter += new System.EventHandler(this.txtLienBottomAdjust_Enter);
            this.txtLienBottomAdjust.TextChanged += new System.EventHandler(this.txtLienBottomAdjust_TextChanged);
            this.txtLienBottomAdjust.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtLienBottomAdjust_KeyPress);
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(20, 244);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(254, 15);
            this.Label7.TabIndex = 8;
            this.Label7.Text = "LIEN DISCHARGE BOTTOM ADJUSTMENT";
            this.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.Label7, null);
            // 
            // lblLDNSideAdj
            // 
            this.lblLDNSideAdj.AutoSize = true;
            this.lblLDNSideAdj.Location = new System.Drawing.Point(20, 294);
            this.lblLDNSideAdj.Name = "lblLDNSideAdj";
            this.lblLDNSideAdj.Size = new System.Drawing.Size(230, 15);
            this.lblLDNSideAdj.TabIndex = 10;
            this.lblLDNSideAdj.Text = "LIEN DISCHARGE SIDE ADJUSTMENT";
            this.lblLDNSideAdj.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblLDNSideAdj, null);
            // 
            // txtLDNTopAdjust
            // 
            this.txtLDNTopAdjust.BackColor = System.Drawing.SystemColors.Window;
            this.txtLDNTopAdjust.Location = new System.Drawing.Point(280, 180);
            this.txtLDNTopAdjust.MaxLength = 5;
            this.txtLDNTopAdjust.Name = "txtLDNTopAdjust";
            this.txtLDNTopAdjust.Size = new System.Drawing.Size(70, 40);
            this.txtLDNTopAdjust.TabIndex = 7;
            this.txtLDNTopAdjust.Text = "0";
            this.txtLDNTopAdjust.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtLDNTopAdjust, null);
            this.txtLDNTopAdjust.Enter += new System.EventHandler(this.txtLDNTopAdjust_Enter);
            this.txtLDNTopAdjust.TextChanged += new System.EventHandler(this.txtLDNTopAdjust_TextChanged);
            this.txtLDNTopAdjust.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtLDNTopAdjust_KeyPress);
            // 
            // lblLienSideAdj
            // 
            this.lblLienSideAdj.AutoSize = true;
            this.lblLienSideAdj.Location = new System.Drawing.Point(20, 144);
            this.lblLienSideAdj.Name = "lblLienSideAdj";
            this.lblLienSideAdj.Size = new System.Drawing.Size(153, 15);
            this.lblLienSideAdj.TabIndex = 4;
            this.lblLienSideAdj.Text = "LIEN SIDE ADJUSTMENT";
            this.lblLienSideAdj.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblLienSideAdj, null);
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(20, 94);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(177, 15);
            this.Label6.TabIndex = 2;
            this.Label6.Text = "LIEN BOTTOM ADJUSTMENT";
            this.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.Label6, null);
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(20, 44);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(150, 15);
            this.Label3.TabIndex = 12;
            this.Label3.Text = "LIEN TOP ADJUSTMENT";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.Label3, null);
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(20, 194);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(227, 15);
            this.Label2.TabIndex = 6;
            this.Label2.Text = "LIEN DISCHARGE TOP ADJUSTMENT";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.Label2, null);
            // 
            // fraPaymentOptions
            // 
            this.fraPaymentOptions.Controls.Add(this.cmbAssessmentValues);
            this.fraPaymentOptions.Controls.Add(this.fcLabel1);
            this.fraPaymentOptions.Controls.Add(this.chkDisableAutoPmtFill);
            this.fraPaymentOptions.Controls.Add(this.chkDefaultRTDDate);
            this.fraPaymentOptions.Controls.Add(this.chkDefaultToAuto);
            this.fraPaymentOptions.Controls.Add(this.chkDoNotAutofillPrepay);
            this.fraPaymentOptions.Location = new System.Drawing.Point(30, 76);
            this.fraPaymentOptions.Name = "fraPaymentOptions";
            this.fraPaymentOptions.Size = new System.Drawing.Size(465, 259);
            this.fraPaymentOptions.TabIndex = 78;
            this.fraPaymentOptions.Text = "Status And Payment Options";
            this.ToolTip1.SetToolTip(this.fraPaymentOptions, null);
            this.fraPaymentOptions.UseMnemonic = false;
            this.fraPaymentOptions.Visible = false;
            // 
            // fcLabel1
            // 
            this.fcLabel1.AutoSize = true;
            this.fcLabel1.Location = new System.Drawing.Point(20, 214);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(203, 15);
            this.fcLabel1.TabIndex = 4;
            this.fcLabel1.Text = "ACCOUNT DETAIL ASSESSMENT";
            this.fcLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.fcLabel1, null);
            // 
            // chkDisableAutoPmtFill
            // 
            this.chkDisableAutoPmtFill.Location = new System.Drawing.Point(20, 160);
            this.chkDisableAutoPmtFill.Name = "chkDisableAutoPmtFill";
            this.chkDisableAutoPmtFill.Size = new System.Drawing.Size(305, 27);
            this.chkDisableAutoPmtFill.TabIndex = 3;
            this.chkDisableAutoPmtFill.Text = "Disable autofill payment on Insert key.";
            this.ToolTip1.SetToolTip(this.chkDisableAutoPmtFill, "Disable using the Insert key or double click to automatically fill the payment am" +
        "ount.");
            this.chkDisableAutoPmtFill.Click += new System.EventHandler(this.chkDisableAutoPmtFill_Click);
            // 
            // chkDefaultRTDDate
            // 
            this.chkDefaultRTDDate.Location = new System.Drawing.Point(20, 120);
            this.chkDefaultRTDDate.Name = "chkDefaultRTDDate";
            this.chkDefaultRTDDate.Size = new System.Drawing.Size(297, 27);
            this.chkDefaultRTDDate.TabIndex = 2;
            this.chkDefaultRTDDate.Text = "Automatically change RTD with ETD.";
            this.ToolTip1.SetToolTip(this.chkDefaultRTDDate, "Automatically change Recorded Transaction Date when changing the Effective Transa" +
        "ction Date on the Payment Screen.");
            this.chkDefaultRTDDate.Click += new System.EventHandler(this.chkDefaultRTDDate_Click);
            // 
            // chkDefaultToAuto
            // 
            this.chkDefaultToAuto.Location = new System.Drawing.Point(20, 80);
            this.chkDefaultToAuto.Name = "chkDefaultToAuto";
            this.chkDefaultToAuto.Size = new System.Drawing.Size(216, 27);
            this.chkDefaultToAuto.TabIndex = 1;
            this.chkDefaultToAuto.Text = "Default payments to Auto.";
            this.ToolTip1.SetToolTip(this.chkDefaultToAuto, null);
            // 
            // chkDoNotAutofillPrepay
            // 
            this.chkDoNotAutofillPrepay.Location = new System.Drawing.Point(20, 40);
            this.chkDoNotAutofillPrepay.Name = "chkDoNotAutofillPrepay";
            this.chkDoNotAutofillPrepay.Size = new System.Drawing.Size(340, 27);
            this.chkDoNotAutofillPrepay.TabIndex = 6;
            this.chkDoNotAutofillPrepay.Text = "Do not autofill prepayment reference fields.";
            this.ToolTip1.SetToolTip(this.chkDoNotAutofillPrepay, null);
            // 
            // fraRateInfo
            // 
            this.fraRateInfo.AppearanceKey = "groupBoxLeftBorder";
            this.fraRateInfo.Controls.Add(this.frmRate);
            this.fraRateInfo.Controls.Add(this.fraBasis);
            this.fraRateInfo.Location = new System.Drawing.Point(30, 70);
            this.fraRateInfo.Name = "fraRateInfo";
            this.fraRateInfo.Size = new System.Drawing.Size(440, 412);
            this.fraRateInfo.TabIndex = 6;
            this.fraRateInfo.Text = "Rate Information";
            this.ToolTip1.SetToolTip(this.fraRateInfo, null);
            this.fraRateInfo.UseMnemonic = false;
            // 
            // frmRate
            // 
            this.frmRate.Controls.Add(this.chkUseRKTaxRate);
            this.frmRate.Controls.Add(this.txtAbatementInterest);
            this.frmRate.Controls.Add(this.txtDiscountPercent);
            this.frmRate.Controls.Add(this.txtPPayInterestRate);
            this.frmRate.Controls.Add(this.lblAbatementInterest);
            this.frmRate.Controls.Add(this.lblDiscountPercent);
            this.frmRate.Controls.Add(this.lblPPayInterestRate);
            this.frmRate.Location = new System.Drawing.Point(20, 155);
            this.frmRate.Name = "frmRate";
            this.frmRate.Size = new System.Drawing.Size(359, 229);
            this.frmRate.TabIndex = 50;
            this.frmRate.Text = "Rates (%)";
            this.ToolTip1.SetToolTip(this.frmRate, null);
            this.frmRate.UseMnemonic = false;
            // 
            // chkUseRKTaxRate
            // 
            this.chkUseRKTaxRate.Cursor = Wisej.Web.Cursors.Default;
            this.chkUseRKTaxRate.Location = new System.Drawing.Point(20, 180);
            this.chkUseRKTaxRate.Name = "chkUseRKTaxRate";
            this.chkUseRKTaxRate.Size = new System.Drawing.Size(324, 27);
            this.chkUseRKTaxRate.TabIndex = 12;
            this.chkUseRKTaxRate.Text = "Use rate key interest rate for abatements";
            this.ToolTip1.SetToolTip(this.chkUseRKTaxRate, null);
            this.chkUseRKTaxRate.Click += new System.EventHandler(this.chkUseRKTaxRate_Click);
            // 
            // txtAbatementInterest
            // 
            this.txtAbatementInterest.BackColor = System.Drawing.SystemColors.Window;
            this.txtAbatementInterest.Location = new System.Drawing.Point(220, 130);
            this.txtAbatementInterest.Name = "txtAbatementInterest";
            this.txtAbatementInterest.Size = new System.Drawing.Size(121, 40);
            this.txtAbatementInterest.TabIndex = 11;
            this.txtAbatementInterest.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtAbatementInterest, null);
            this.txtAbatementInterest.Enter += new System.EventHandler(this.txtAbatementInterest_Enter);
            this.txtAbatementInterest.TextChanged += new System.EventHandler(this.txtAbatementInterest_TextChanged);
            this.txtAbatementInterest.Validating += new System.ComponentModel.CancelEventHandler(this.txtAbatementInterest_Validating);
            this.txtAbatementInterest.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtAbatementInterest_KeyPress);
            // 
            // txtDiscountPercent
            // 
            this.txtDiscountPercent.BackColor = System.Drawing.SystemColors.Window;
            this.txtDiscountPercent.Location = new System.Drawing.Point(220, 80);
            this.txtDiscountPercent.Name = "txtDiscountPercent";
            this.txtDiscountPercent.Size = new System.Drawing.Size(121, 40);
            this.txtDiscountPercent.TabIndex = 10;
            this.txtDiscountPercent.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtDiscountPercent, null);
            this.txtDiscountPercent.Enter += new System.EventHandler(this.txtDiscountPercent_Enter);
            this.txtDiscountPercent.TextChanged += new System.EventHandler(this.txtDiscountPercent_TextChanged);
            this.txtDiscountPercent.Validating += new System.ComponentModel.CancelEventHandler(this.txtDiscountPercent_Validating);
            this.txtDiscountPercent.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtDiscountPercent_KeyPress);
            // 
            // txtPPayInterestRate
            // 
            this.txtPPayInterestRate.BackColor = System.Drawing.SystemColors.Window;
            this.txtPPayInterestRate.Location = new System.Drawing.Point(220, 30);
            this.txtPPayInterestRate.Name = "txtPPayInterestRate";
            this.txtPPayInterestRate.Size = new System.Drawing.Size(121, 40);
            this.txtPPayInterestRate.TabIndex = 9;
            this.txtPPayInterestRate.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtPPayInterestRate, null);
            this.txtPPayInterestRate.Enter += new System.EventHandler(this.txtPPayInterestRate_Enter);
            this.txtPPayInterestRate.TextChanged += new System.EventHandler(this.txtPPayInterestRate_TextChanged);
            this.txtPPayInterestRate.Validating += new System.ComponentModel.CancelEventHandler(this.txtPPayInterestRate_Validating);
            this.txtPPayInterestRate.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtPPayInterestRate_KeyPress);
            // 
            // lblAbatementInterest
            // 
            this.lblAbatementInterest.AutoSize = true;
            this.lblAbatementInterest.Location = new System.Drawing.Point(20, 144);
            this.lblAbatementInterest.Name = "lblAbatementInterest";
            this.lblAbatementInterest.Size = new System.Drawing.Size(184, 15);
            this.lblAbatementInterest.TabIndex = 77;
            this.lblAbatementInterest.Text = "ABATEMENT INTEREST RATE";
            this.lblAbatementInterest.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblAbatementInterest, null);
            // 
            // lblDiscountPercent
            // 
            this.lblDiscountPercent.AutoSize = true;
            this.lblDiscountPercent.Location = new System.Drawing.Point(20, 94);
            this.lblDiscountPercent.Name = "lblDiscountPercent";
            this.lblDiscountPercent.Size = new System.Drawing.Size(160, 15);
            this.lblDiscountPercent.TabIndex = 52;
            this.lblDiscountPercent.Text = "DISCOUNT PERCENTAGE";
            this.lblDiscountPercent.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblDiscountPercent, null);
            // 
            // lblPPayInterestRate
            // 
            this.lblPPayInterestRate.AutoSize = true;
            this.lblPPayInterestRate.Location = new System.Drawing.Point(20, 44);
            this.lblPPayInterestRate.Name = "lblPPayInterestRate";
            this.lblPPayInterestRate.Size = new System.Drawing.Size(193, 15);
            this.lblPPayInterestRate.TabIndex = 51;
            this.lblPPayInterestRate.Text = "PREPAYMENT INTEREST RATE";
            this.lblPPayInterestRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblPPayInterestRate, null);
            // 
            // fraBasis
            // 
            this.fraBasis.Controls.Add(this.lblBasis);
            this.fraBasis.Controls.Add(this.cmbBasis);
            this.fraBasis.Location = new System.Drawing.Point(20, 40);
            this.fraBasis.Name = "fraBasis";
            this.fraBasis.Size = new System.Drawing.Size(359, 87);
            this.fraBasis.TabIndex = 48;
            this.fraBasis.Text = "Interest Calculation";
            this.ToolTip1.SetToolTip(this.fraBasis, null);
            this.fraBasis.UseMnemonic = false;
            // 
            // lblBasis
            // 
            this.lblBasis.Location = new System.Drawing.Point(20, 44);
            this.lblBasis.Name = "lblBasis";
            this.lblBasis.Size = new System.Drawing.Size(41, 23);
            this.lblBasis.TabIndex = 49;
            this.lblBasis.Text = "BASIS";
            this.ToolTip1.SetToolTip(this.lblBasis, null);
            // 
            // fraDefaultAccount
            // 
            this.fraDefaultAccount.Controls.Add(this.chkDefaultAccount);
            this.fraDefaultAccount.Location = new System.Drawing.Point(20, 184);
            this.fraDefaultAccount.Name = "fraDefaultAccount";
            this.fraDefaultAccount.Size = new System.Drawing.Size(548, 79);
            this.fraDefaultAccount.TabIndex = 65;
            this.fraDefaultAccount.Text = "Default Account";
            this.ToolTip1.SetToolTip(this.fraDefaultAccount, null);
            this.fraDefaultAccount.UseMnemonic = false;
            // 
            // chkDefaultAccount
            // 
            this.chkDefaultAccount.Location = new System.Drawing.Point(18, 27);
            this.chkDefaultAccount.Name = "chkDefaultAccount";
            this.chkDefaultAccount.ResizableEdges = Wisej.Web.AnchorStyles.Bottom;
            this.chkDefaultAccount.Size = new System.Drawing.Size(505, 27);
            this.chkDefaultAccount.TabIndex = 66;
            this.chkDefaultAccount.Text = "Show last account accessed when entering from Cash Receipting";
            this.ToolTip1.SetToolTip(this.chkDefaultAccount, null);
            this.chkDefaultAccount.Click += new System.EventHandler(this.chkDefaultAccount_Click);
            // 
            // fcLabel3
            // 
            this.fcLabel3.AutoSize = true;
            this.fcLabel3.Location = new System.Drawing.Point(20, 205);
            this.fcLabel3.Name = "fcLabel3";
            this.fcLabel3.Size = new System.Drawing.Size(111, 15);
            this.fcLabel3.TabIndex = 1;
            this.fcLabel3.Text = "CL RECEIPT SIZE";
            this.ToolTip1.SetToolTip(this.fcLabel3, null);
            // 
            // fraOtherOptions
            // 
            this.fraOtherOptions.Controls.Add(this.chkShowReceiptOnAcctDetail);
            this.fraOtherOptions.Location = new System.Drawing.Point(30, 74);
            this.fraOtherOptions.Name = "fraOtherOptions";
            this.fraOtherOptions.Size = new System.Drawing.Size(403, 70);
            this.fraOtherOptions.TabIndex = 86;
            this.fraOtherOptions.Text = "Other Options";
            this.ToolTip1.SetToolTip(this.fraOtherOptions, null);
            this.fraOtherOptions.UseMnemonic = false;
            this.fraOtherOptions.Visible = false;
            // 
            // chkShowReceiptOnAcctDetail
            // 
            this.chkShowReceiptOnAcctDetail.Location = new System.Drawing.Point(20, 30);
            this.chkShowReceiptOnAcctDetail.Name = "chkShowReceiptOnAcctDetail";
            this.chkShowReceiptOnAcctDetail.Size = new System.Drawing.Size(379, 27);
            this.chkShowReceiptOnAcctDetail.TabIndex = 87;
            this.chkShowReceiptOnAcctDetail.Text = "Show Receipt Number on Account Detail Report";
            this.ToolTip1.SetToolTip(this.chkShowReceiptOnAcctDetail, null);
            this.chkShowReceiptOnAcctDetail.Click += new System.EventHandler(this.chkShowReceiptOnAcctDetail_Click);
            // 
            // fraTownInformation
            // 
            this.fraTownInformation.AppearanceKey = "groupBoxLeftBorder";
            this.fraTownInformation.Controls.Add(this.txtTaxInfoSheetMsg);
            this.fraTownInformation.Controls.Add(this.lblReceiptSize);
            this.fraTownInformation.Controls.Add(this.lblLoadbackOrder);
            this.fraTownInformation.Controls.Add(this.cmbTCOrder);
            this.fraTownInformation.Controls.Add(this.chkExtraTCPage);
            this.fraTownInformation.Controls.Add(this.cmbLoadbackOrder);
            this.fraTownInformation.Controls.Add(this.fraTownSeal);
            this.fraTownInformation.Controls.Add(this.txtLienAbate);
            this.fraTownInformation.Controls.Add(this.lblTaxInfoMsg);
            this.fraTownInformation.Controls.Add(this.lblLienAbate);
            this.fraTownInformation.Location = new System.Drawing.Point(30, 79);
            this.fraTownInformation.Name = "fraTownInformation";
            this.fraTownInformation.Size = new System.Drawing.Size(541, 520);
            this.fraTownInformation.TabIndex = 69;
            this.fraTownInformation.Text = "Town Information";
            this.ToolTip1.SetToolTip(this.fraTownInformation, null);
            this.fraTownInformation.UseMnemonic = false;
            this.fraTownInformation.Visible = false;
            // 
            // txtTaxInfoSheetMsg
            // 
            this.txtTaxInfoSheetMsg.BackColor = System.Drawing.SystemColors.Window;
            this.txtTaxInfoSheetMsg.Location = new System.Drawing.Point(211, 429);
            this.txtTaxInfoSheetMsg.MaxLength = 125;
            this.txtTaxInfoSheetMsg.Multiline = true;
            this.txtTaxInfoSheetMsg.Name = "txtTaxInfoSheetMsg";
            this.txtTaxInfoSheetMsg.ScrollBars = Wisej.Web.ScrollBars.Vertical;
            this.txtTaxInfoSheetMsg.Size = new System.Drawing.Size(309, 81);
            this.txtTaxInfoSheetMsg.TabIndex = 9;
            this.ToolTip1.SetToolTip(this.txtTaxInfoSheetMsg, "This text will be printed on the Tax Information Sheet.");
            // 
            // lblReceiptSize
            // 
            this.lblReceiptSize.AutoSize = true;
            this.lblReceiptSize.Location = new System.Drawing.Point(20, 387);
            this.lblReceiptSize.Name = "lblReceiptSize";
            this.lblReceiptSize.Size = new System.Drawing.Size(170, 15);
            this.lblReceiptSize.TabIndex = 6;
            this.lblReceiptSize.Text = "TAX CLUB REPORT ORDER";
            this.lblReceiptSize.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblReceiptSize, null);
            // 
            // lblLoadbackOrder
            // 
            this.lblLoadbackOrder.AutoSize = true;
            this.lblLoadbackOrder.Location = new System.Drawing.Point(20, 338);
            this.lblLoadbackOrder.Name = "lblLoadbackOrder";
            this.lblLoadbackOrder.Size = new System.Drawing.Size(178, 15);
            this.lblLoadbackOrder.TabIndex = 4;
            this.lblLoadbackOrder.Text = "LOADBACK REPORT ORDER";
            this.lblLoadbackOrder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblLoadbackOrder, null);
            // 
            // chkExtraTCPage
            // 
            this.chkExtraTCPage.Location = new System.Drawing.Point(20, 76);
            this.chkExtraTCPage.Name = "chkExtraTCPage";
            this.chkExtraTCPage.Size = new System.Drawing.Size(349, 27);
            this.chkExtraTCPage.TabIndex = 2;
            this.chkExtraTCPage.Text = "Force page break between tax club booklets";
            this.ToolTip1.SetToolTip(this.chkExtraTCPage, null);
            // 
            // fraTownSeal
            // 
            this.fraTownSeal.Controls.Add(this.chkTownSealLienDischarge);
            this.fraTownSeal.Controls.Add(this.chkTownSealLienMaturity);
            this.fraTownSeal.Controls.Add(this.chkTownSealLien);
            this.fraTownSeal.Controls.Add(this.chkTownSeal30DN);
            this.fraTownSeal.Location = new System.Drawing.Point(20, 124);
            this.fraTownSeal.Name = "fraTownSeal";
            this.fraTownSeal.Size = new System.Drawing.Size(323, 179);
            this.fraTownSeal.TabIndex = 3;
            this.fraTownSeal.Text = "Show Town Seal";
            this.ToolTip1.SetToolTip(this.fraTownSeal, null);
            this.fraTownSeal.UseMnemonic = false;
            // 
            // chkTownSealLienDischarge
            // 
            this.chkTownSealLienDischarge.Location = new System.Drawing.Point(20, 140);
            this.chkTownSealLienDischarge.Name = "chkTownSealLienDischarge";
            this.chkTownSealLienDischarge.Size = new System.Drawing.Size(196, 27);
            this.chkTownSealLienDischarge.TabIndex = 3;
            this.chkTownSealLienDischarge.Text = "Lien Discharge Notices";
            this.ToolTip1.SetToolTip(this.chkTownSealLienDischarge, null);
            // 
            // chkTownSealLienMaturity
            // 
            this.chkTownSealLienMaturity.Location = new System.Drawing.Point(20, 100);
            this.chkTownSealLienMaturity.Name = "chkTownSealLienMaturity";
            this.chkTownSealLienMaturity.Size = new System.Drawing.Size(181, 27);
            this.chkTownSealLienMaturity.TabIndex = 2;
            this.chkTownSealLienMaturity.Text = "Lien Maturity Notices";
            this.ToolTip1.SetToolTip(this.chkTownSealLienMaturity, null);
            // 
            // chkTownSealLien
            // 
            this.chkTownSealLien.Location = new System.Drawing.Point(20, 65);
            this.chkTownSealLien.Name = "chkTownSealLien";
            this.chkTownSealLien.Size = new System.Drawing.Size(118, 27);
            this.chkTownSealLien.TabIndex = 1;
            this.chkTownSealLien.Text = "Lien Notices";
            this.ToolTip1.SetToolTip(this.chkTownSealLien, null);
            // 
            // chkTownSeal30DN
            // 
            this.chkTownSeal30DN.Location = new System.Drawing.Point(20, 30);
            this.chkTownSeal30DN.Name = "chkTownSeal30DN";
            this.chkTownSeal30DN.Size = new System.Drawing.Size(138, 27);
            this.chkTownSeal30DN.TabIndex = 4;
            this.chkTownSeal30DN.Text = "30 Day Notices";
            this.ToolTip1.SetToolTip(this.chkTownSeal30DN, null);
            // 
            // txtLienAbate
            // 
            this.txtLienAbate.BackColor = System.Drawing.SystemColors.Window;
            this.txtLienAbate.Location = new System.Drawing.Point(187, 29);
            this.txtLienAbate.MaxLength = 255;
            this.txtLienAbate.Name = "txtLienAbate";
            this.txtLienAbate.Size = new System.Drawing.Size(209, 40);
            this.txtLienAbate.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.txtLienAbate, "This text will be printed on a Lien Discharge Notice when the lien is abated to s" +
        "how who authorized the abatement.");
            // 
            // lblTaxInfoMsg
            // 
            this.lblTaxInfoMsg.Location = new System.Drawing.Point(20, 463);
            this.lblTaxInfoMsg.Name = "lblTaxInfoMsg";
            this.lblTaxInfoMsg.Size = new System.Drawing.Size(159, 23);
            this.lblTaxInfoMsg.TabIndex = 8;
            this.lblTaxInfoMsg.Text = "TAX INFO SHEET MESSAGE";
            this.ToolTip1.SetToolTip(this.lblTaxInfoMsg, "This text will be printed on the Tax Information Sheet.");
            // 
            // lblLienAbate
            // 
            this.lblLienAbate.AutoSize = true;
            this.lblLienAbate.Location = new System.Drawing.Point(20, 42);
            this.lblLienAbate.Name = "lblLienAbate";
            this.lblLienAbate.Size = new System.Drawing.Size(149, 15);
            this.lblLienAbate.TabIndex = 10;
            this.lblLienAbate.Text = "LIEN ABATEMENT TEXT";
            this.lblLienAbate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblLienAbate, "This text will be printed on a Lien Discharge Notice when the lien is abated to s" +
        "how who authorized the abatement.");
            // 
            // fraAuditOptions
            // 
            this.fraAuditOptions.AppearanceKey = "groupBoxLeftBorder";
            this.fraAuditOptions.Controls.Add(this.fraReportSequence);
            this.fraAuditOptions.Controls.Add(this.cmbReceiptSize);
            this.fraAuditOptions.Controls.Add(this.fcLabel3);
            this.fraAuditOptions.Controls.Add(this.fraDefaultAccount);
            this.fraAuditOptions.Location = new System.Drawing.Point(30, 65);
            this.fraAuditOptions.Name = "fraAuditOptions";
            this.fraAuditOptions.Size = new System.Drawing.Size(601, 294);
            this.fraAuditOptions.TabIndex = 63;
            this.fraAuditOptions.Text = "Audit And Receipt Options";
            this.ToolTip1.SetToolTip(this.fraAuditOptions, null);
            this.fraAuditOptions.UseMnemonic = false;
            this.fraAuditOptions.Visible = false;
            // 
            // fraReportSequence
            // 
            this.fraReportSequence.Controls.Add(this.cmbSeq);
            this.fraReportSequence.Controls.Add(this.chkMapLot);
            this.fraReportSequence.Controls.Add(this.Label1);
            this.fraReportSequence.Location = new System.Drawing.Point(20, 40);
            this.fraReportSequence.Name = "fraReportSequence";
            this.fraReportSequence.Size = new System.Drawing.Size(308, 124);
            this.fraReportSequence.Text = "Cl Audit";
            this.ToolTip1.SetToolTip(this.fraReportSequence, null);
            this.fraReportSequence.UseMnemonic = false;
            // 
            // chkMapLot
            // 
            this.chkMapLot.Location = new System.Drawing.Point(20, 80);
            this.chkMapLot.Name = "chkMapLot";
            this.chkMapLot.Size = new System.Drawing.Size(277, 27);
            this.chkMapLot.TabIndex = 2;
            this.chkMapLot.Text = "Show Name and Map Lot on Audit";
            this.ToolTip1.SetToolTip(this.chkMapLot, null);
            this.chkMapLot.Click += new System.EventHandler(this.chkMapLot_Click);
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(20, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(97, 15);
            this.Label1.TabIndex = 3;
            this.Label1.Text = "SEQUENCE BY";
            this.ToolTip1.SetToolTip(this.Label1, null);
            // 
            // fraTaxService
            // 
            this.fraTaxService.Controls.Add(this.chkTSReminders);
            this.fraTaxService.Controls.Add(this.vsTSEmailGrid);
            this.fraTaxService.Controls.Add(this.lblTaxSrvcEmail);
            this.fraTaxService.Location = new System.Drawing.Point(19, 70);
            this.fraTaxService.Name = "fraTaxService";
            this.fraTaxService.Size = new System.Drawing.Size(370, 252);
            this.fraTaxService.TabIndex = 83;
            this.fraTaxService.Text = "Tax Service Options";
            this.ToolTip1.SetToolTip(this.fraTaxService, null);
            this.fraTaxService.UseMnemonic = false;
            this.fraTaxService.Visible = false;
            // 
            // chkTSReminders
            // 
            this.chkTSReminders.Location = new System.Drawing.Point(20, 30);
            this.chkTSReminders.Name = "chkTSReminders";
            this.chkTSReminders.Size = new System.Drawing.Size(242, 27);
            this.chkTSReminders.TabIndex = 84;
            this.chkTSReminders.Text = "Show File Creation Reminder";
            this.ToolTip1.SetToolTip(this.chkTSReminders, null);
            // 
            // vsTSEmailGrid
            // 
            this.vsTSEmailGrid.Cols = 1;
            this.vsTSEmailGrid.ColumnHeadersVisible = false;
            this.vsTSEmailGrid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsTSEmailGrid.FixedCols = 0;
            this.vsTSEmailGrid.FixedRows = 0;
            this.vsTSEmailGrid.Location = new System.Drawing.Point(20, 110);
            this.vsTSEmailGrid.Name = "vsTSEmailGrid";
            this.vsTSEmailGrid.ReadOnly = false;
            this.vsTSEmailGrid.RowHeadersVisible = false;
            this.vsTSEmailGrid.Rows = 3;
            this.vsTSEmailGrid.Size = new System.Drawing.Size(330, 122);
            this.vsTSEmailGrid.TabIndex = 95;
            this.ToolTip1.SetToolTip(this.vsTSEmailGrid, null);
            // 
            // lblTaxSrvcEmail
            // 
            this.lblTaxSrvcEmail.Location = new System.Drawing.Point(20, 75);
            this.lblTaxSrvcEmail.Name = "lblTaxSrvcEmail";
            this.lblTaxSrvcEmail.Size = new System.Drawing.Size(122, 15);
            this.lblTaxSrvcEmail.TabIndex = 96;
            this.lblTaxSrvcEmail.Text = "TAX SERVICE EMAIL";
            this.ToolTip1.SetToolTip(this.lblTaxSrvcEmail, null);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // fcLabel2
            // 
            this.fcLabel2.Name = "fcLabel2";
            this.fcLabel2.Size = new System.Drawing.Size(100, 17);
            // 
            // lblOptions
            // 
            this.lblOptions.AutoSize = true;
            this.lblOptions.Location = new System.Drawing.Point(30, 21);
            this.lblOptions.Name = "lblOptions";
            this.lblOptions.Size = new System.Drawing.Size(145, 15);
            this.lblOptions.TabIndex = 4;
            this.lblOptions.Text = "COLLECTION OPTIONS";
            this.lblOptions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Location = new System.Drawing.Point(415, 30);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcess.Size = new System.Drawing.Size(98, 48);
            this.btnProcess.TabIndex = 1;
            this.btnProcess.Text = "Save";
            this.btnProcess.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // frmCustomize
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(929, 813);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmCustomize";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Customize";
            this.ToolTip1.SetToolTip(this, null);
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmCustomize_Load);
            this.Activated += new System.EventHandler(this.frmCustomize_Activated);
            this.Resize += new System.EventHandler(this.frmCustomize_Resize);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCustomize_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraAdjustments)).EndInit();
            this.fraAdjustments.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.frmLaserAdjustment)).EndInit();
            this.frmLaserAdjustment.ResumeLayout(false);
            this.frmLaserAdjustment.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.frmReminderAdjustments)).EndInit();
            this.frmReminderAdjustments.ResumeLayout(false);
            this.frmReminderAdjustments.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraLienAdjustment)).EndInit();
            this.fraLienAdjustment.ResumeLayout(false);
            this.fraLienAdjustment.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraPaymentOptions)).EndInit();
            this.fraPaymentOptions.ResumeLayout(false);
            this.fraPaymentOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDisableAutoPmtFill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDefaultRTDDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDefaultToAuto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDoNotAutofillPrepay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraRateInfo)).EndInit();
            this.fraRateInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.frmRate)).EndInit();
            this.frmRate.ResumeLayout(false);
            this.frmRate.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseRKTaxRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraBasis)).EndInit();
            this.fraBasis.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraDefaultAccount)).EndInit();
            this.fraDefaultAccount.ResumeLayout(false);
            this.fraDefaultAccount.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDefaultAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraOtherOptions)).EndInit();
            this.fraOtherOptions.ResumeLayout(false);
            this.fraOtherOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowReceiptOnAcctDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraTownInformation)).EndInit();
            this.fraTownInformation.ResumeLayout(false);
            this.fraTownInformation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkExtraTCPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraTownSeal)).EndInit();
            this.fraTownSeal.ResumeLayout(false);
            this.fraTownSeal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkTownSealLienDischarge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTownSealLienMaturity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTownSealLien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTownSeal30DN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraAuditOptions)).EndInit();
            this.fraAuditOptions.ResumeLayout(false);
            this.fraAuditOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraReportSequence)).EndInit();
            this.fraReportSequence.ResumeLayout(false);
            this.fraReportSequence.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkMapLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraTaxService)).EndInit();
            this.fraTaxService.ResumeLayout(false);
            this.fraTaxService.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkTSReminders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsTSEmailGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCLabel fcLabel1;
		private FCLabel fcLabel2;
		private FCLabel lblOptions;
		private FCLabel lblReceiptSize;
		private FCLabel lblLoadbackOrder;
		public FCLabel fcLabel3;
		private FCButton btnProcess;
	}
}
