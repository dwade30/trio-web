﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TWCL0000
{
    public class SelectableItem<TObject> 
    {
        public bool IsSelected { get; set; } = false;
        public TObject Item { get; set; }
    }
}
