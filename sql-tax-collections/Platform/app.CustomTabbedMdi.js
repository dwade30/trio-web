﻿

/**
 * app.MForm
 * 
 * Mixin class that overrides certain methods in
 * wisej.web.Form to suppress the MdiChild main menu. 
 */
qx.Mixin.define("app.MForm", {

	members: {

		/**
		 * Assigns the main menu bar to the form.
		 *
		 * Overridden to prevent the menu bar from being added to the
		 * form above the client area.
		 */
		_applyMainMenu: function (value, old) {

			// standard processing for top-level forms.
			if (this.isTopLevel()) {
				this.base(arguments, value, old);
				return;
			}


		},

	}

});

// applies the patch.
qx.Class.patch(wisej.web.Form, app.MForm);


/**
 * app.MTabbedMdiView
 * 
 * Mixin class that overrides certain methods in
 * wisej.web.TabMdiView to show/hide a menu button in the 
 * TabPage button when the page is active and synchronize
 * the button's menu with the MdiChild menu.
 */
qx.Mixin.define("app.MTabbedMdiView", {

	construct: function () {

		this.addListener("changeSelection", this.__updateTabMenuButton);
	},
	members: {

		// Handles the changeSelectiom event to show/hide the
		// new menu button using the MdiChild menu items.
		__updateTabMenuButton: function (e) {

			if (wisej.web.DesignMode)
				return;

			// retrieve the new and previous selection.
			// not that the selection is always an array.
			var newPage = e.getData();
			var oldPage = e.getOldData();

			if (oldPage.length > 0 && oldPage[0]) {
				oldPage[0].getButton()._excludeChildControl("menu");
			}

			if (newPage.length > 0 && newPage[0]) {
				var page = newPage[0];
				var button = page.getButton();
				var form = page.getChildren()[0];
				if (form) {
					if (form.getMainMenu() && form.getMainMenu().hasChildren()) {
						button._showChildControl("menu");
						button.setUserData("form", form);
					}
				}
			}
		},
	}
});

// applies the patch.
qx.Class.patch(wisej.web.TabMdiView, app.MTabbedMdiView);


/**
 * app.MTabbedMdiPage
 * 
 * Extends wisej.web.tabmdiview.TabPage to manage the layout
 * of the button with the new menu icon.
 */
qx.Mixin.define("app.MTabbedMdiPage", {

	members: {

		// overridden to update the icon size of the new
		// menu icon to match the icon of the tab button.
		_updateIconSize: function (size) {

			this.base(arguments, size);

			if (size) {

				var button = this.getButton();
				var menu = button.getChildControl("menu");

				var width = size.width;
				var height = size.height;

				if (this.__orientation == "vertical") {
					width = size.height;
					height = size.width;
				}

				menu.setWidth(width);
				menu.setHeight(height);
				menu.setMinWidth(width);
				menu.setMinHeight(height);
				menu.getContentElement().setStyle("backgroundSize", width + "px " + height + "px");

				// update the layout of the tab-button.
				qx.ui.core.queue.Widget.add(this, "layout");
			}
		},

		// copied from Wisej and modified. doesn't call the base.
		//
		// rotates the tab button and updates the layout
		// according to the bar position and the orientation.
		__updateButtonLayout: function () {

			var button = this.getButton();
			var layout = button._getLayout();

			layout.setSpacingX(0);
			layout.setSpacingY(0);

			var icon = button.getChildControl("icon");
			var menu = button.getChildControl("menu");
			var label = button.getChildControl("label");
			var close = button.getChildControl("close-button");

			// rearrange the close button or the icon.
			switch (this.__orientation) {

				case "vertical":

					layout.setColumnFlex(0, 1);
					layout.setColumnFlex(1, 0);
					layout.setColumnFlex(2, 0);
					layout.setRowFlex(0, 0);
					layout.setRowFlex(1, 0);
					layout.setRowFlex(2, 1);
					layout.setRowFlex(3, 0);

					if (this.__barPosition == "right" || this.__barPosition == "bottom") {
						menu.setLayoutProperties({ row: 0, column: 0 });
						icon.setLayoutProperties({ row: 1, column: 0 });
						label.setLayoutProperties({ row: 2, column: 0 });
						close.setLayoutProperties({ row: 3, column: 0 });
					}
					else {
						layout.setRowFlex(1, 1);
						layout.setRowFlex(2, 0);

						close.setLayoutProperties({ row: 0, column: 0 });
						label.setLayoutProperties({ row: 1, column: 0 });
						icon.setLayoutProperties({ row: 2, column: 0 });
						menu.setLayoutProperties({ row: 3, column: 0 });
					}
					break;

				case "horizontal":

					layout.setColumnFlex(0, 0);
					layout.setColumnFlex(1, 0);
					layout.setColumnFlex(2, 1);
					layout.setColumnFlex(3, 0);
					layout.setRowFlex(0, 1);
					layout.setRowFlex(1, 0);
					layout.setRowFlex(2, 0);
					menu.setLayoutProperties({ row: 0, column: 0 });
					icon.setLayoutProperties({ row: 0, column: 1 });
					label.setLayoutProperties({ row: 0, column: 2 });
					close.setLayoutProperties({ row: 0, column: 3 });

					break;
			}

			label.resetWidth();
			label.resetHeight();

			this.__rotateWidget(menu);
			this.__rotateWidget(icon);
			this.__rotateWidget(label);
			this.__rotateWidget(close);
		},
	}
});

// applies the patch.
qx.Class.patch(wisej.web.tabmdiview.TabPage, app.MTabbedMdiPage);


/**
 * app.MMenuTabButton
 * 
 * Mixin class that adds the "menu" child control
 * to the wisej.web.tabcontrol.TabButton class.
 */
qx.Mixin.define("app.MMenuTabButton", {

	members: {

		// handles pointerdown events on the menu icon to
		// show the related menu.
		_onMenuPointerDown: function (e) {

			var form = this.getUserData("form");
			var formMenu = form.getMainMenu();

			if (!formMenu || !formMenu.hasChildren())
				return;

			// create the menu to show and shows it.
			// the menu is created new every time to make
			// sure it is i sync with the form's main menu.
			this.__createMenu(formMenu.getMenuItems()).open();
		},

		// creates a new menu containing all the clones items and child items
		// from the form's main menu.
		__createMenu: function (items) {

			var menu = new wisej.web.menu.ContextMenu().set({
				opener: this
			});

			if (items.length > 0) {
				var newItems = [];
				for (var i = 0; i < items.length; i++) {
					newItems.push(this.__cloneMenuItem(items[i]));
				}
				menu.setMenuItems(newItems);
			}

			menu.addListenerOnce("disappear", function (e) {
				menu.destroy();
			});

			return menu;
		},

		// clones the item and its children.
		// the events are redirected to the original item.
		__cloneMenuItem: function (item) {

			var newItem = new wisej.web.menu.MenuItem().set({
				icon: item.getIcon(),
				label: item.getLabel(),
				enabled: item.getEnabled(),
				mnemonic: item.getMnemonic(),
				shortcut: item.getShortcut()
			});

			if (item instanceof wisej.web.menu.MenuItem) {
				newItem.set({
					checked: item.getChecked(),
					radioCheck: item.getRadioCheck(),
					showCommandLabel: item.getShowCommandLabel()
				});
			}

			newItem.setUserData("item", item);
			newItem.addListener("popup", function () { newItem.getUserData("item").fireEvent("popup"); });
			newItem.addListener("execute", function () { newItem.getUserData("item").fireEvent("execute"); });

			// recurse.
			var items = item.getMenuItems();
			if (items.length > 0) {
				var newItems = [];
				for (var i = 0; i < items.length; i++) {
					newItems.push(this.__cloneMenuItem(items[i]));
				}
				newItem.setMenuItems(newItems);
			}

			return newItem;
		},

		// overridden
		_createChildControlImpl: function (id, hash) {
			var control;

			switch (id) {
				case "menu":

					control = new qx.ui.basic.Image().set({
						focusable: false,
						keepActive: true,
						alignY: "middle",
						alignX: "center",
						visibility: "excluded",
						source: "menu-overflow",
					});

					control.addListener("pointerdown", this._onMenuPointerDown, this);
					this._add(control, { row: 0, column: 0 });

					break;
			}

			return control || this.base(arguments, id);
		},
	}
});

// applies the patch.
qx.Class.patch(wisej.web.tabcontrol.TabButton, app.MMenuTabButton);
