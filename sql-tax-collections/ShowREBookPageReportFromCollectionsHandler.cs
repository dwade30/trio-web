﻿using Global;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections;

namespace TWCL0000
{
    public class ShowREBookPageReportFromCollectionsHandler : CommandHandler<ShowREBookPageReportFromCollections>
    {
        protected override void Handle(ShowREBookPageReportFromCollections command)
        {
            arBookPageAccount.InstancePtr.Init(command.Account,command.Account);
        }
    }
}