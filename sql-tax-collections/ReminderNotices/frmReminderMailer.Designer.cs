﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmReminderMailer.
	/// </summary>
	partial class frmReminderMailer : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCFrame> fraText;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtNotPastDue;
		public fecherFoundation.FCFrame fraText_1;
		public fecherFoundation.FCTextBox txtPastDue;
		public fecherFoundation.FCLabel lblBreakDownAmounts;
		public fecherFoundation.FCLabel lblBreakDown;
		public fecherFoundation.FCLabel lblMessage;
		public fecherFoundation.FCLabel lblHeader;
		public fecherFoundation.FCFrame fraText_0;
		public fecherFoundation.FCTextBox txtNotPastDue_1;
		public fecherFoundation.FCTextBox txtNotPastDue_0;
		public fecherFoundation.FCLabel lblNotAmountDue;
		public fecherFoundation.FCLabel lblNotMessage;
		public fecherFoundation.FCLabel lblNotHeader;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReminderMailer));
            this.fraText_1 = new fecherFoundation.FCFrame();
            this.txtPastDue = new fecherFoundation.FCTextBox();
            this.lblBreakDownAmounts = new fecherFoundation.FCLabel();
            this.lblBreakDown = new fecherFoundation.FCLabel();
            this.lblMessage = new fecherFoundation.FCLabel();
            this.lblHeader = new fecherFoundation.FCLabel();
            this.fraText_0 = new fecherFoundation.FCFrame();
            this.txtNotPastDue_1 = new fecherFoundation.FCTextBox();
            this.txtNotPastDue_0 = new fecherFoundation.FCTextBox();
            this.lblNotAmountDue = new fecherFoundation.FCLabel();
            this.lblNotMessage = new fecherFoundation.FCLabel();
            this.lblNotHeader = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.btnProcess = new fecherFoundation.FCButton();
            this.cmdFileClear = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraText_1)).BeginInit();
            this.fraText_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraText_0)).BeginInit();
            this.fraText_0.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileClear)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 675);
            this.BottomPanel.Size = new System.Drawing.Size(757, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraText_1);
            this.ClientArea.Controls.Add(this.fraText_0);
            this.ClientArea.Size = new System.Drawing.Size(757, 615);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdFileClear);
            this.TopPanel.Size = new System.Drawing.Size(757, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileClear, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(284, 30);
            this.HeaderText.Text = "Reminder Notice - Mailer";
            // 
            // fraText_1
            // 
            this.fraText_1.Controls.Add(this.txtPastDue);
            this.fraText_1.Controls.Add(this.lblBreakDownAmounts);
            this.fraText_1.Controls.Add(this.lblBreakDown);
            this.fraText_1.Controls.Add(this.lblMessage);
            this.fraText_1.Controls.Add(this.lblHeader);
            this.fraText_1.Location = new System.Drawing.Point(30, 323);
            this.fraText_1.Name = "fraText_1";
            this.fraText_1.Size = new System.Drawing.Size(697, 277);
            this.fraText_1.TabIndex = 2;
            this.fraText_1.Text = "Message For Past Due Accounts";
            this.fraText_1.UseMnemonic = false;
            // 
            // txtPastDue
            // 
            this.txtPastDue.BackColor = System.Drawing.SystemColors.Window;
            this.txtPastDue.Location = new System.Drawing.Point(20, 114);
            this.txtPastDue.MaxLength = 1028;
            this.txtPastDue.Multiline = true;
            this.txtPastDue.Name = "txtPastDue";
            this.txtPastDue.Size = new System.Drawing.Size(658, 73);
            this.txtPastDue.TabIndex = 1;
            this.txtPastDue.KeyDown += new Wisej.Web.KeyEventHandler(this.txtPastDue_KeyDown);
            // 
            // lblBreakDownAmounts
            // 
            this.lblBreakDownAmounts.Location = new System.Drawing.Point(260, 197);
            this.lblBreakDownAmounts.Name = "lblBreakDownAmounts";
            this.lblBreakDownAmounts.Size = new System.Drawing.Size(200, 60);
            this.lblBreakDownAmounts.TabIndex = 4;
            this.lblBreakDownAmounts.Text = "BREAKDOWN OF AMOUNTS";
            // 
            // lblBreakDown
            // 
            this.lblBreakDown.Location = new System.Drawing.Point(20, 197);
            this.lblBreakDown.Name = "lblBreakDown";
            this.lblBreakDown.Size = new System.Drawing.Size(200, 60);
            this.lblBreakDown.TabIndex = 3;
            this.lblBreakDown.Text = "BREAKDOWN OF AMOUNTS TITLES";
            // 
            // lblMessage
            // 
            this.lblMessage.Location = new System.Drawing.Point(20, 56);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(658, 52);
            this.lblMessage.TabIndex = 1;
            this.lblMessage.Text = "THIS IS TO REMIND YOU THAT YOU HAVE PAST DUE AMOUNTS ON YOUR 20XX REAL ESTATE TAX" +
    " BILL AND THAT THE SECOND PAYMENT IS DUE ON XX/XX/XXXX";
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.lblHeader.Location = new System.Drawing.Point(20, 30);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(380, 16);
            this.lblHeader.TabIndex = 5;
            this.lblHeader.Text = "******  20XX REAL ESTATE TAX - REMINDER NOTICE  ******";
            this.lblHeader.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // fraText_0
            // 
            this.fraText_0.Controls.Add(this.txtNotPastDue_1);
            this.fraText_0.Controls.Add(this.txtNotPastDue_0);
            this.fraText_0.Controls.Add(this.lblNotAmountDue);
            this.fraText_0.Controls.Add(this.lblNotMessage);
            this.fraText_0.Controls.Add(this.lblNotHeader);
            this.fraText_0.Location = new System.Drawing.Point(30, 30);
            this.fraText_0.Name = "fraText_0";
            this.fraText_0.Size = new System.Drawing.Size(697, 283);
            this.fraText_0.TabIndex = 1;
            this.fraText_0.Text = "Message For Not Past Due Accounts";
            this.fraText_0.UseMnemonic = false;
            // 
            // txtNotPastDue_1
            // 
            this.txtNotPastDue_1.BackColor = System.Drawing.SystemColors.Window;
            this.txtNotPastDue_1.Location = new System.Drawing.Point(20, 190);
            this.txtNotPastDue_1.MaxLength = 70;
            this.txtNotPastDue_1.Multiline = true;
            this.txtNotPastDue_1.Name = "txtNotPastDue_1";
            this.txtNotPastDue_1.Size = new System.Drawing.Size(658, 73);
            this.txtNotPastDue_1.TabIndex = 2;
            this.txtNotPastDue_1.KeyDown += new Wisej.Web.KeyEventHandler(this.txtNotPastDue_KeyDown);
            // 
            // txtNotPastDue_0
            // 
            this.txtNotPastDue_0.BackColor = System.Drawing.SystemColors.Window;
            this.txtNotPastDue_0.Location = new System.Drawing.Point(20, 114);
            this.txtNotPastDue_0.MaxLength = 1028;
            this.txtNotPastDue_0.Name = "txtNotPastDue_0";
            this.txtNotPastDue_0.Size = new System.Drawing.Size(658, 40);
            this.txtNotPastDue_0.TabIndex = 1;
            this.txtNotPastDue_0.KeyDown += new Wisej.Web.KeyEventHandler(this.txtNotPastDue_KeyDown);
            // 
            // lblNotAmountDue
            // 
            this.lblNotAmountDue.AutoSize = true;
            this.lblNotAmountDue.Location = new System.Drawing.Point(20, 164);
            this.lblNotAmountDue.Name = "lblNotAmountDue";
            this.lblNotAmountDue.Size = new System.Drawing.Size(255, 16);
            this.lblNotAmountDue.TabIndex = 3;
            this.lblNotAmountDue.Text = "AMOUNT DUE XX/XX/XXXX  ###,###.##";
            this.lblNotAmountDue.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblNotMessage
            // 
            this.lblNotMessage.Location = new System.Drawing.Point(20, 56);
            this.lblNotMessage.Name = "lblNotMessage";
            this.lblNotMessage.Size = new System.Drawing.Size(658, 48);
            this.lblNotMessage.TabIndex = 1;
            this.lblNotMessage.Text = "THIS IS TO REMIND YOU THAT THE SECOND PAYMENT ON YOUR 20XX REAL ESTATE TAX BILL I" +
    "S DUE ON XX/XX/XXXX";
            // 
            // lblNotHeader
            // 
            this.lblNotHeader.AutoSize = true;
            this.lblNotHeader.Location = new System.Drawing.Point(20, 30);
            this.lblNotHeader.Name = "lblNotHeader";
            this.lblNotHeader.Size = new System.Drawing.Size(380, 16);
            this.lblNotHeader.TabIndex = 5;
            this.lblNotHeader.Text = "******  20XX REAL ESTATE TAX - REMINDER NOTICE  ******";
            this.lblNotHeader.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Location = new System.Drawing.Point(340, 30);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcess.Size = new System.Drawing.Size(101, 48);
            this.btnProcess.TabIndex = 1;
            this.btnProcess.Text = "Print";
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // cmdFileClear
            // 
            this.cmdFileClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileClear.Location = new System.Drawing.Point(653, 30);
            this.cmdFileClear.Name = "cmdFileClear";
            this.cmdFileClear.Size = new System.Drawing.Size(55, 24);
            this.cmdFileClear.TabIndex = 66;
            this.cmdFileClear.Text = "Clear";
            this.cmdFileClear.Click += new System.EventHandler(this.cmdFileClear_Click);
            // 
            // frmReminderMailer
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(757, 783);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmReminderMailer";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Reminder Notice - Mailer";
            this.Load += new System.EventHandler(this.frmReminderMailer_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmReminderMailer_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmReminderMailer_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraText_1)).EndInit();
            this.fraText_1.ResumeLayout(false);
            this.fraText_1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraText_0)).EndInit();
            this.fraText_0.ResumeLayout(false);
            this.fraText_0.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileClear)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton btnProcess;
		public FCButton cmdFileClear;
	}
}
