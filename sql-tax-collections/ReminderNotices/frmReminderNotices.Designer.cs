﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Wisej.Web;
using Global;

namespace TWCL0000
{
    /// <summary>
    /// Summary description for frmReminderNotices.
    /// </summary>
    partial class frmReminderNotices : BaseForm
    {
        public fecherFoundation.FCComboBox cmbOrder;
        public fecherFoundation.FCComboBox cmbPeriod;
        public fecherFoundation.FCComboBox cmbRN;
        public fecherFoundation.FCFrame fraMessage;
        public fecherFoundation.FCFrame Frame1;
        public fecherFoundation.FCTextBox txtPastDue;
        public fecherFoundation.FCLabel Label1;
        public fecherFoundation.FCFrame fraNOPastDue;
        public fecherFoundation.FCTextBox txtNoPastDue;
        public fecherFoundation.FCLabel lblNoPastDue;
        public fecherFoundation.FCTextBox txtMinimumAmount;
        public fecherFoundation.FCTextBox txtReportTitle;
        public fecherFoundation.FCFrame fraType;
        public fecherFoundation.FCCheckBox chkGetPreviousYears;
        public fecherFoundation.FCCheckBox chkShowPastDueAmounts;
        public fecherFoundation.FCCheckBox chkInterest;
        public fecherFoundation.FCCheckBox chkUseTownLogo;
        public fecherFoundation.FCCheckBox chkUseSig;
        public fecherFoundation.FCCheckBox chkBulkMailing;
        public fecherFoundation.FCFrame fraRN;
        public fecherFoundation.FCLabel lblMinAmount;
        public fecherFoundation.FCPictureBox imgSig;
        //private fecherFoundation.FCMenuStrip MainMenu1;
        public fecherFoundation.FCToolStripMenuItem mnuFile;
        private Wisej.Web.ToolTip ToolTip1;

        protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
            {
                _InstancePtr = null;
                Sys.ClearInstance(this);
            }
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReminderNotices));
            this.javaScriptNumbersOnly = new Wisej.Web.JavaScript(this.components);
            this.cmbOrder = new fecherFoundation.FCComboBox();
            this.cmbPeriod = new fecherFoundation.FCComboBox();
            this.cmbRN = new fecherFoundation.FCComboBox();
            this.fraMessage = new fecherFoundation.FCFrame();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.txtPastDue = new fecherFoundation.FCTextBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.fraNOPastDue = new fecherFoundation.FCFrame();
            this.txtNoPastDue = new fecherFoundation.FCTextBox();
            this.lblNoPastDue = new fecherFoundation.FCLabel();
            this.lblPeriod = new fecherFoundation.FCLabel();
            this.lblOrder = new fecherFoundation.FCLabel();
            this.txtMinimumAmount = new fecherFoundation.FCTextBox();
            this.txtReportTitle = new fecherFoundation.FCTextBox();
            this.fraType = new fecherFoundation.FCFrame();
            this.lblRN = new fecherFoundation.FCLabel();
            this.chkGetPreviousYears = new fecherFoundation.FCCheckBox();
            this.chkShowPastDueAmounts = new fecherFoundation.FCCheckBox();
            this.chkInterest = new fecherFoundation.FCCheckBox();
            this.chkUseTownLogo = new fecherFoundation.FCCheckBox();
            this.chkUseSig = new fecherFoundation.FCCheckBox();
            this.chkBulkMailing = new fecherFoundation.FCCheckBox();
            this.fraRN = new fecherFoundation.FCFrame();
            this.CriteriaPanel = new Wisej.Web.FlexLayoutPanel();
            this.signaturePanel = new Wisej.Web.TableLayoutPanel();
            this.imgSig = new fecherFoundation.FCPictureBox();
            this.ReportTitlePanel = new Wisej.Web.TableLayoutPanel();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.LabelTypePanel = new Wisej.Web.TableLayoutPanel();
            this.cmbLabelType = new Wisej.Web.ComboBox();
            this.lblLabelType = new fecherFoundation.FCLabel();
            this.NamePanel = new Wisej.Web.TableLayoutPanel();
            this.txtNameStart = new Wisej.Web.TextBox();
            this.txtNameEnd = new Wisej.Web.TextBox();
            this.lblName = new fecherFoundation.FCLabel();
            this.AccountPanel = new Wisej.Web.TableLayoutPanel();
            this.txtAccountStart = new Wisej.Web.TextBox();
            this.txtAccountEnd = new Wisej.Web.TextBox();
            this.lblAccount = new fecherFoundation.FCLabel();
            this.MessagePanel = new Wisej.Web.TableLayoutPanel();
            this.txtMessage = new Wisej.Web.TextBox();
            this.lblMessage = new fecherFoundation.FCLabel();
            this.MapLotPanel = new Wisej.Web.TableLayoutPanel();
            this.cmbShowMapLot = new Wisej.Web.ComboBox();
            this.lblShowMapLot = new fecherFoundation.FCLabel();
            this.UseReturnAddressPanel = new Wisej.Web.TableLayoutPanel();
            this.cmbUseReturnAddress = new Wisej.Web.ComboBox();
            this.lblUseReturnAddress = new fecherFoundation.FCLabel();
            this.MailDatePanel = new Wisej.Web.TableLayoutPanel();
            this.lblInterestDate = new fecherFoundation.FCLabel();
            this.txtInterestDate = new Global.T2KDateBox();
            this.txtMailDate = new Global.T2KDateBox();
            this.lblMailDate = new fecherFoundation.FCLabel();
            this.InterestDeptPanel = new Wisej.Web.TableLayoutPanel();
            this.lblInterestDept = new fecherFoundation.FCLabel();
            this.txtInterestDeptPhone = new Wisej.Web.TextBox();
            this.lblInterestDeptPhone = new fecherFoundation.FCLabel();
            this.txtInterestDept = new Wisej.Web.TextBox();
            this.MaturedYearDeptPanel = new Wisej.Web.TableLayoutPanel();
            this.lblMaturedYearDept = new fecherFoundation.FCLabel();
            this.lblMaturedYearDeptPhone = new fecherFoundation.FCLabel();
            this.txtMaturedYearDeptPhone = new Wisej.Web.TextBox();
            this.txtMaturedYearDept = new Wisej.Web.TextBox();
            this.HighestYearToIncludePanel = new Wisej.Web.TableLayoutPanel();
            this.cmbHighestYearToInclude = new Wisej.Web.ComboBox();
            this.cmbLastYearMatured = new Wisej.Web.ComboBox();
            this.lblHighestYearToInclude = new fecherFoundation.FCLabel();
            this.lblLastYearMatured = new fecherFoundation.FCLabel();
            this.ShowNamePanel = new Wisej.Web.TableLayoutPanel();
            this.cmbShowName = new Wisej.Web.ComboBox();
            this.lblShowName = new fecherFoundation.FCLabel();
            this.HideDupNamesPanel = new Wisej.Web.TableLayoutPanel();
            this.cmbHideDupNames = new Wisej.Web.ComboBox();
            this.lblHideDupNames = new fecherFoundation.FCLabel();
            this.LineBeforeSigPanel = new Wisej.Web.TableLayoutPanel();
            this.lblLineBeforeSig = new fecherFoundation.FCLabel();
            this.txtLineBeforeSig = new Wisej.Web.TextBox();
            this.FirstLineAfterSigPanel = new Wisej.Web.TableLayoutPanel();
            this.lblFirstLineAfterSig = new fecherFoundation.FCLabel();
            this.txtFirstLineAfterSig = new Wisej.Web.TextBox();
            this.SecondLineAfterSigPanel = new Wisej.Web.TableLayoutPanel();
            this.lblSecondLineAfterSig = new fecherFoundation.FCLabel();
            this.txtSecondLineAfterSig = new Wisej.Web.TextBox();
            this.ReturnAddressPanel = new Wisej.Web.TableLayoutPanel();
            this.lblReturnAddress = new fecherFoundation.FCLabel();
            this.txtReturnAddress4 = new Wisej.Web.TextBox();
            this.txtReturnAddress3 = new Wisej.Web.TextBox();
            this.txtReturnAddress2 = new Wisej.Web.TextBox();
            this.txtReturnAddress1 = new Wisej.Web.TextBox();
            this.lblMinAmount = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.btnProcess = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraMessage)).BeginInit();
            this.fraMessage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraNOPastDue)).BeginInit();
            this.fraNOPastDue.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraType)).BeginInit();
            this.fraType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkGetPreviousYears)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowPastDueAmounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseTownLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseSig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBulkMailing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraRN)).BeginInit();
            this.CriteriaPanel.SuspendLayout();
            this.signaturePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgSig)).BeginInit();
            this.ReportTitlePanel.SuspendLayout();
            this.LabelTypePanel.SuspendLayout();
            this.NamePanel.SuspendLayout();
            this.AccountPanel.SuspendLayout();
            this.MessagePanel.SuspendLayout();
            this.MapLotPanel.SuspendLayout();
            this.UseReturnAddressPanel.SuspendLayout();
            this.MailDatePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInterestDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMailDate)).BeginInit();
            this.InterestDeptPanel.SuspendLayout();
            this.MaturedYearDeptPanel.SuspendLayout();
            this.HighestYearToIncludePanel.SuspendLayout();
            this.ShowNamePanel.SuspendLayout();
            this.HideDupNamesPanel.SuspendLayout();
            this.LineBeforeSigPanel.SuspendLayout();
            this.FirstLineAfterSigPanel.SuspendLayout();
            this.SecondLineAfterSigPanel.SuspendLayout();
            this.ReturnAddressPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 1337);
            this.BottomPanel.Size = new System.Drawing.Size(1168, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.AutoSize = true;
            this.ClientArea.AutoSizeMode = Wisej.Web.AutoSizeMode.GrowAndShrink;
            this.ClientArea.Controls.Add(this.CriteriaPanel);
            this.ClientArea.Controls.Add(this.cmbPeriod);
            this.ClientArea.Controls.Add(this.fraType);
            this.ClientArea.Controls.Add(this.lblPeriod);
            this.ClientArea.Controls.Add(this.lblOrder);
            this.ClientArea.Controls.Add(this.txtMinimumAmount);
            this.ClientArea.Controls.Add(this.cmbOrder);
            this.ClientArea.Controls.Add(this.lblMinAmount);
            this.ClientArea.Controls.Add(this.fraRN);
            this.ClientArea.Controls.Add(this.fraMessage);
            this.ClientArea.Size = new System.Drawing.Size(1188, 770);
            this.ClientArea.Controls.SetChildIndex(this.fraMessage, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraRN, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblMinAmount, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbOrder, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtMinimumAmount, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblOrder, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblPeriod, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraType, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbPeriod, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.CriteriaPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(1188, 60);
            this.TopPanel.TabIndex = 0;
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(26, 26);
            this.HeaderText.Size = new System.Drawing.Size(208, 30);
            this.HeaderText.Text = "Reminder Notices";
            // 
            // cmbOrder
            // 
            this.cmbOrder.Items.AddRange(new object[] {
            "Account",
            "Owner Name"});
            this.cmbOrder.Location = new System.Drawing.Point(118, 150);
            this.cmbOrder.Name = "cmbOrder";
            this.cmbOrder.Size = new System.Drawing.Size(156, 40);
            this.cmbOrder.TabIndex = 2;
            this.cmbOrder.Text = "Account";
            // 
            // cmbPeriod
            // 
            this.cmbPeriod.Location = new System.Drawing.Point(456, 151);
            this.cmbPeriod.Name = "cmbPeriod";
            this.cmbPeriod.Size = new System.Drawing.Size(60, 40);
            this.cmbPeriod.TabIndex = 3;
            // 
            // cmbRN
            // 
            this.cmbRN.Location = new System.Drawing.Point(20, 30);
            this.cmbRN.Name = "cmbRN";
            this.cmbRN.Size = new System.Drawing.Size(240, 40);
            this.cmbRN.TabIndex = 1;
            this.cmbRN.SelectedIndexChanged += new System.EventHandler(this.optRN_Click);
            // 
            // fraMessage
            // 
            this.fraMessage.AppearanceKey = "groupBoxLeftBorder";
            this.fraMessage.Controls.Add(this.Frame1);
            this.fraMessage.Controls.Add(this.fraNOPastDue);
            this.fraMessage.Location = new System.Drawing.Point(30, 324);
            this.fraMessage.Name = "fraMessage";
            this.fraMessage.Size = new System.Drawing.Size(700, 330);
            this.fraMessage.TabIndex = 1;
            this.fraMessage.Text = "Form Messages";
            this.fraMessage.UseMnemonic = false;
            this.fraMessage.Visible = false;
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.txtPastDue);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.Location = new System.Drawing.Point(20, 175);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(660, 135);
            this.Frame1.TabIndex = 1;
            this.Frame1.Text = "Past Due Message";
            this.Frame1.UseMnemonic = false;
            // 
            // txtPastDue
            // 
            this.txtPastDue.BackColor = System.Drawing.SystemColors.Window;
            this.txtPastDue.Cursor = Wisej.Web.Cursors.Default;
            this.txtPastDue.Location = new System.Drawing.Point(20, 75);
            this.txtPastDue.Name = "txtPastDue";
            this.txtPastDue.Size = new System.Drawing.Size(620, 40);
            this.txtPastDue.TabIndex = 1;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(620, 35);
            this.Label1.TabIndex = 2;
            // 
            // fraNOPastDue
            // 
            this.fraNOPastDue.Controls.Add(this.txtNoPastDue);
            this.fraNOPastDue.Controls.Add(this.lblNoPastDue);
            this.fraNOPastDue.Location = new System.Drawing.Point(20, 30);
            this.fraNOPastDue.Name = "fraNOPastDue";
            this.fraNOPastDue.Size = new System.Drawing.Size(660, 135);
            this.fraNOPastDue.TabIndex = 2;
            this.fraNOPastDue.Text = "No Past Due Message";
            this.fraNOPastDue.UseMnemonic = false;
            // 
            // txtNoPastDue
            // 
            this.txtNoPastDue.BackColor = System.Drawing.SystemColors.Window;
            this.txtNoPastDue.Cursor = Wisej.Web.Cursors.Default;
            this.txtNoPastDue.Location = new System.Drawing.Point(20, 75);
            this.txtNoPastDue.Name = "txtNoPastDue";
            this.txtNoPastDue.Size = new System.Drawing.Size(620, 40);
            this.txtNoPastDue.TabIndex = 1;
            // 
            // lblNoPastDue
            // 
            this.lblNoPastDue.Location = new System.Drawing.Point(20, 30);
            this.lblNoPastDue.Name = "lblNoPastDue";
            this.lblNoPastDue.Size = new System.Drawing.Size(620, 35);
            this.lblNoPastDue.TabIndex = 2;
            // 
            // lblPeriod
            // 
            this.lblPeriod.AutoSize = true;
            this.lblPeriod.Location = new System.Drawing.Point(319, 163);
            this.lblPeriod.Name = "lblPeriod";
            this.lblPeriod.Size = new System.Drawing.Size(131, 17);
            this.lblPeriod.TabIndex = 8;
            this.lblPeriod.Text = "NEXT PERIOD DUE";
            this.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblOrder
            // 
            this.lblOrder.AutoSize = true;
            this.lblOrder.Location = new System.Drawing.Point(33, 163);
            this.lblOrder.Name = "lblOrder";
            this.lblOrder.Size = new System.Drawing.Size(79, 17);
            this.lblOrder.TabIndex = 6;
            this.lblOrder.Text = "ORDER BY";
            this.lblOrder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMinimumAmount
            // 
            this.txtMinimumAmount.BackColor = System.Drawing.SystemColors.Window;
            this.txtMinimumAmount.Cursor = Wisej.Web.Cursors.Default;
            this.txtMinimumAmount.Location = new System.Drawing.Point(762, 151);
            this.txtMinimumAmount.Name = "txtMinimumAmount";
            this.txtMinimumAmount.Size = new System.Drawing.Size(70, 40);
            this.txtMinimumAmount.TabIndex = 4;
            this.txtMinimumAmount.Text = "0.00";
            this.txtMinimumAmount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtMinimumAmount, "Minimum principal due in order to process.");
            // 
            // txtReportTitle
            // 
            this.txtReportTitle.BackColor = System.Drawing.SystemColors.Window;
            this.txtReportTitle.Cursor = Wisej.Web.Cursors.Default;
            this.txtReportTitle.Location = new System.Drawing.Point(243, 3);
            this.txtReportTitle.MaxLength = 35;
            this.txtReportTitle.Name = "txtReportTitle";
            this.txtReportTitle.Size = new System.Drawing.Size(554, 40);
            this.txtReportTitle.TabIndex = 5;
            // 
            // fraType
            // 
            this.fraType.Controls.Add(this.lblRN);
            this.fraType.Controls.Add(this.chkGetPreviousYears);
            this.fraType.Controls.Add(this.chkShowPastDueAmounts);
            this.fraType.Controls.Add(this.chkInterest);
            this.fraType.Controls.Add(this.chkUseTownLogo);
            this.fraType.Controls.Add(this.chkUseSig);
            this.fraType.Controls.Add(this.cmbRN);
            this.fraType.Controls.Add(this.chkBulkMailing);
            this.fraType.Location = new System.Drawing.Point(30, 21);
            this.fraType.Name = "fraType";
            this.fraType.Size = new System.Drawing.Size(803, 112);
            this.fraType.TabIndex = 1;
            this.fraType.Text = "Type";
            this.fraType.UseMnemonic = false;
            // 
            // lblRN
            // 
            this.lblRN.AutoSize = true;
            this.lblRN.Location = new System.Drawing.Point(20, 44);
            this.lblRN.Name = "lblRN";
            this.lblRN.Size = new System.Drawing.Size(27, 17);
            this.lblRN.Text = "RN";
            this.lblRN.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblRN.Visible = false;
            // 
            // chkGetPreviousYears
            // 
            this.chkGetPreviousYears.Location = new System.Drawing.Point(523, 75);
            this.chkGetPreviousYears.Name = "chkGetPreviousYears";
            this.chkGetPreviousYears.Size = new System.Drawing.Size(149, 19);
            this.chkGetPreviousYears.TabIndex = 7;
            this.chkGetPreviousYears.Text = "Get Previous Years";
            this.chkGetPreviousYears.Click += new System.EventHandler(this.chkGetPreviousYears_Click);
            // 
            // chkShowPastDueAmounts
            // 
            this.chkShowPastDueAmounts.Enabled = false;
            this.chkShowPastDueAmounts.Location = new System.Drawing.Point(523, 50);
            this.chkShowPastDueAmounts.Name = "chkShowPastDueAmounts";
            this.chkShowPastDueAmounts.Size = new System.Drawing.Size(123, 19);
            this.chkShowPastDueAmounts.TabIndex = 6;
            this.chkShowPastDueAmounts.Text = "Show Past Due";
            // 
            // chkInterest
            // 
            this.chkInterest.Enabled = false;
            this.chkInterest.Location = new System.Drawing.Point(306, 25);
            this.chkInterest.Name = "chkInterest";
            this.chkInterest.Size = new System.Drawing.Size(114, 19);
            this.chkInterest.TabIndex = 2;
            this.chkInterest.Text = "Show Interest";
            // 
            // chkUseTownLogo
            // 
            this.chkUseTownLogo.Enabled = false;
            this.chkUseTownLogo.Location = new System.Drawing.Point(306, 75);
            this.chkUseTownLogo.Name = "chkUseTownLogo";
            this.chkUseTownLogo.Size = new System.Drawing.Size(125, 19);
            this.chkUseTownLogo.TabIndex = 4;
            this.chkUseTownLogo.Text = "Use Town Logo";
            // 
            // chkUseSig
            // 
            this.chkUseSig.Enabled = false;
            this.chkUseSig.Location = new System.Drawing.Point(306, 50);
            this.chkUseSig.Name = "chkUseSig";
            this.chkUseSig.Size = new System.Drawing.Size(143, 19);
            this.chkUseSig.TabIndex = 3;
            this.chkUseSig.Text = "Use Signature File";
            this.chkUseSig.Click += new System.EventHandler(this.chkUseSig_Click);
            // 
            // chkBulkMailing
            // 
            this.chkBulkMailing.Enabled = false;
            this.chkBulkMailing.Location = new System.Drawing.Point(523, 25);
            this.chkBulkMailing.Name = "chkBulkMailing";
            this.chkBulkMailing.Size = new System.Drawing.Size(167, 19);
            this.chkBulkMailing.TabIndex = 5;
            this.chkBulkMailing.Text = "Print Bulk Mailing Info";
            // 
            // fraRN
            // 
            this.fraRN.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraRN.AppearanceKey = "groupBoxNoBorders";
            this.fraRN.AutoSize = true;
            this.fraRN.AutoSizeMode = Wisej.Web.AutoSizeMode.GrowAndShrink;
            this.fraRN.Location = new System.Drawing.Point(30, 220);
            this.fraRN.Name = "fraRN";
            this.fraRN.Size = new System.Drawing.Size(6, 22);
            this.fraRN.TabIndex = 6;
            this.fraRN.UseMnemonic = false;
            // 
            // CriteriaPanel
            // 
            this.CriteriaPanel.AutoSize = true;
            this.CriteriaPanel.AutoSizeMode = Wisej.Web.AutoSizeMode.GrowAndShrink;
            this.CriteriaPanel.Controls.Add(this.signaturePanel);
            this.CriteriaPanel.Controls.Add(this.ReportTitlePanel);
            this.CriteriaPanel.Controls.Add(this.LabelTypePanel);
            this.CriteriaPanel.Controls.Add(this.NamePanel);
            this.CriteriaPanel.Controls.Add(this.AccountPanel);
            this.CriteriaPanel.Controls.Add(this.MessagePanel);
            this.CriteriaPanel.Controls.Add(this.MapLotPanel);
            this.CriteriaPanel.Controls.Add(this.UseReturnAddressPanel);
            this.CriteriaPanel.Controls.Add(this.MailDatePanel);
            this.CriteriaPanel.Controls.Add(this.InterestDeptPanel);
            this.CriteriaPanel.Controls.Add(this.MaturedYearDeptPanel);
            this.CriteriaPanel.Controls.Add(this.HighestYearToIncludePanel);
            this.CriteriaPanel.Controls.Add(this.ShowNamePanel);
            this.CriteriaPanel.Controls.Add(this.HideDupNamesPanel);
            this.CriteriaPanel.Controls.Add(this.LineBeforeSigPanel);
            this.CriteriaPanel.Controls.Add(this.FirstLineAfterSigPanel);
            this.CriteriaPanel.Controls.Add(this.SecondLineAfterSigPanel);
            this.CriteriaPanel.Controls.Add(this.ReturnAddressPanel);
            this.CriteriaPanel.LayoutStyle = Wisej.Web.FlexLayoutStyle.Vertical;
            this.CriteriaPanel.Location = new System.Drawing.Point(30, 219);
            this.CriteriaPanel.Margin = new Wisej.Web.Padding(0, 3, 3, 3);
            this.CriteriaPanel.Name = "CriteriaPanel";
            this.CriteriaPanel.Size = new System.Drawing.Size(803, 1118);
            this.CriteriaPanel.Spacing = 1;
            this.CriteriaPanel.TabIndex = 5;
            this.CriteriaPanel.TabStop = true;
            // 
            // signaturePanel
            // 
            this.signaturePanel.ColumnCount = 1;
            this.signaturePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 30F));
            this.signaturePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 70F));
            this.signaturePanel.Controls.Add(this.imgSig, 0, 0);
            this.signaturePanel.Location = new System.Drawing.Point(0, 3);
            this.signaturePanel.Margin = new Wisej.Web.Padding(0, 3, 3, 3);
            this.signaturePanel.Name = "signaturePanel";
            this.signaturePanel.RowCount = 1;
            this.signaturePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.signaturePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.signaturePanel.ScrollBars = Wisej.Web.ScrollBars.None;
            this.signaturePanel.Size = new System.Drawing.Size(800, 69);
            this.signaturePanel.TabIndex = 1003;
            this.signaturePanel.TabStop = true;
            this.signaturePanel.Visible = false;
            // 
            // imgSig
            // 
            this.imgSig.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgSig.Cursor = Wisej.Web.Cursors.Default;
            this.imgSig.Location = new System.Drawing.Point(3, 3);
            this.imgSig.Name = "imgSig";
            this.imgSig.Size = new System.Drawing.Size(261, 60);
            this.imgSig.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            // 
            // ReportTitlePanel
            // 
            this.ReportTitlePanel.ColumnCount = 2;
            this.ReportTitlePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 30F));
            this.ReportTitlePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 70F));
            this.ReportTitlePanel.Controls.Add(this.fcLabel1, 0, 0);
            this.ReportTitlePanel.Controls.Add(this.txtReportTitle, 1, 0);
            this.ReportTitlePanel.Location = new System.Drawing.Point(0, 79);
            this.ReportTitlePanel.Margin = new Wisej.Web.Padding(0, 3, 3, 3);
            this.ReportTitlePanel.Name = "ReportTitlePanel";
            this.ReportTitlePanel.RowCount = 1;
            this.ReportTitlePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.ReportTitlePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.ReportTitlePanel.ScrollBars = Wisej.Web.ScrollBars.None;
            this.ReportTitlePanel.Size = new System.Drawing.Size(800, 46);
            this.ReportTitlePanel.TabIndex = 5;
            this.ReportTitlePanel.TabStop = true;
            this.ReportTitlePanel.Visible = false;
            // 
            // fcLabel1
            // 
            this.fcLabel1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.fcLabel1.AutoSize = true;
            this.fcLabel1.Location = new System.Drawing.Point(3, 3);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(105, 40);
            this.fcLabel1.TabIndex = 14;
            this.fcLabel1.Text = "REPORT TITLE";
            this.fcLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LabelTypePanel
            // 
            this.CriteriaPanel.SetAlignY(this.LabelTypePanel, Wisej.Web.VerticalAlignment.Top);
            this.LabelTypePanel.ColumnCount = 2;
            this.LabelTypePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 30F));
            this.LabelTypePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 70F));
            this.LabelTypePanel.Controls.Add(this.cmbLabelType);
            this.LabelTypePanel.Controls.Add(this.lblLabelType, 0, 0);
            this.LabelTypePanel.Location = new System.Drawing.Point(0, 132);
            this.LabelTypePanel.Margin = new Wisej.Web.Padding(0, 3, 3, 3);
            this.LabelTypePanel.Name = "LabelTypePanel";
            this.LabelTypePanel.RowCount = 1;
            this.LabelTypePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.LabelTypePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.LabelTypePanel.ScrollBars = Wisej.Web.ScrollBars.None;
            this.LabelTypePanel.Size = new System.Drawing.Size(800, 46);
            this.LabelTypePanel.TabIndex = 6;
            this.LabelTypePanel.TabStop = true;
            // 
            // cmbLabelType
            // 
            this.cmbLabelType.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.cmbLabelType.DisplayMember = "Description";
            this.cmbLabelType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbLabelType.Location = new System.Drawing.Point(243, 3);
            this.cmbLabelType.Name = "cmbLabelType";
            this.cmbLabelType.Size = new System.Drawing.Size(554, 40);
            this.cmbLabelType.TabIndex = 1;
            this.cmbLabelType.ValueMember = "ID";
            // 
            // lblLabelType
            // 
            this.lblLabelType.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.lblLabelType.AutoSize = true;
            this.lblLabelType.Location = new System.Drawing.Point(3, 3);
            this.lblLabelType.Name = "lblLabelType";
            this.lblLabelType.Size = new System.Drawing.Size(91, 40);
            this.lblLabelType.TabIndex = 14;
            this.lblLabelType.Text = "LABEL TYPE";
            this.lblLabelType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // NamePanel
            // 
            this.CriteriaPanel.SetAlignY(this.NamePanel, Wisej.Web.VerticalAlignment.Top);
            this.NamePanel.ColumnCount = 3;
            this.NamePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 30F));
            this.NamePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 35F));
            this.NamePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 35F));
            this.NamePanel.Controls.Add(this.txtNameStart, 1, -1);
            this.NamePanel.Controls.Add(this.txtNameEnd, 2, -1);
            this.NamePanel.Controls.Add(this.lblName, 0, 0);
            this.NamePanel.Location = new System.Drawing.Point(0, 185);
            this.NamePanel.Margin = new Wisej.Web.Padding(0, 3, 3, 3);
            this.NamePanel.Name = "NamePanel";
            this.NamePanel.RowCount = 1;
            this.NamePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.NamePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.NamePanel.Size = new System.Drawing.Size(800, 46);
            this.NamePanel.TabIndex = 7;
            this.NamePanel.TabStop = true;
            // 
            // txtNameStart
            // 
            this.txtNameStart.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.txtNameStart.Location = new System.Drawing.Point(243, 3);
            this.txtNameStart.Name = "txtNameStart";
            this.txtNameStart.Size = new System.Drawing.Size(274, 40);
            this.txtNameStart.TabIndex = 1;
            // 
            // txtNameEnd
            // 
            this.txtNameEnd.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.txtNameEnd.Location = new System.Drawing.Point(523, 3);
            this.txtNameEnd.Name = "txtNameEnd";
            this.txtNameEnd.Size = new System.Drawing.Size(274, 40);
            this.txtNameEnd.TabIndex = 2;
            // 
            // lblName
            // 
            this.lblName.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(3, 3);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(47, 40);
            this.lblName.TabIndex = 10;
            this.lblName.Text = "NAME";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AccountPanel
            // 
            this.CriteriaPanel.SetAlignY(this.AccountPanel, Wisej.Web.VerticalAlignment.Top);
            this.AccountPanel.ColumnCount = 3;
            this.AccountPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 30F));
            this.AccountPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 35F));
            this.AccountPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 35F));
            this.AccountPanel.Controls.Add(this.txtAccountStart, 1, -1);
            this.AccountPanel.Controls.Add(this.txtAccountEnd, 2, -1);
            this.AccountPanel.Controls.Add(this.lblAccount, 0, 0);
            this.AccountPanel.Location = new System.Drawing.Point(0, 238);
            this.AccountPanel.Margin = new Wisej.Web.Padding(0, 3, 3, 3);
            this.AccountPanel.Name = "AccountPanel";
            this.AccountPanel.RowCount = 1;
            this.AccountPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.AccountPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.AccountPanel.Size = new System.Drawing.Size(800, 46);
            this.AccountPanel.TabIndex = 8;
            this.AccountPanel.TabStop = true;
            // 
            // txtAccountStart
            // 
            this.txtAccountStart.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.txtAccountStart.Location = new System.Drawing.Point(243, 3);
            this.txtAccountStart.Name = "txtAccountStart";
            this.txtAccountStart.Size = new System.Drawing.Size(274, 40);
            this.txtAccountStart.TabIndex = 1;
            // 
            // txtAccountEnd
            // 
            this.txtAccountEnd.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.txtAccountEnd.Location = new System.Drawing.Point(523, 3);
            this.txtAccountEnd.Name = "txtAccountEnd";
            this.txtAccountEnd.Size = new System.Drawing.Size(274, 40);
            this.txtAccountEnd.TabIndex = 2;
            // 
            // lblAccount
            // 
            this.lblAccount.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.lblAccount.AutoSize = true;
            this.lblAccount.Location = new System.Drawing.Point(3, 3);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(75, 40);
            this.lblAccount.TabIndex = 13;
            this.lblAccount.Text = "ACCOUNT";
            this.lblAccount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // MessagePanel
            // 
            this.CriteriaPanel.SetAlignY(this.MessagePanel, Wisej.Web.VerticalAlignment.Top);
            this.MessagePanel.ColumnCount = 2;
            this.MessagePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 30F));
            this.MessagePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 70F));
            this.MessagePanel.Controls.Add(this.txtMessage);
            this.MessagePanel.Controls.Add(this.lblMessage, 0, 0);
            this.MessagePanel.Location = new System.Drawing.Point(0, 291);
            this.MessagePanel.Margin = new Wisej.Web.Padding(0, 3, 3, 3);
            this.MessagePanel.Name = "MessagePanel";
            this.MessagePanel.RowCount = 1;
            this.MessagePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.MessagePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.MessagePanel.ScrollBars = Wisej.Web.ScrollBars.None;
            this.MessagePanel.Size = new System.Drawing.Size(800, 46);
            this.MessagePanel.TabIndex = 9;
            this.MessagePanel.TabStop = true;
            this.MessagePanel.Visible = false;
            // 
            // txtMessage
            // 
            this.txtMessage.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.txtMessage.Location = new System.Drawing.Point(243, 3);
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.Size = new System.Drawing.Size(554, 40);
            this.txtMessage.TabIndex = 1;
            // 
            // lblMessage
            // 
            this.lblMessage.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(3, 3);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(75, 40);
            this.lblMessage.TabIndex = 16;
            this.lblMessage.Text = "MESSAGE";
            this.lblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // MapLotPanel
            // 
            this.CriteriaPanel.SetAlignY(this.MapLotPanel, Wisej.Web.VerticalAlignment.Top);
            this.MapLotPanel.ColumnCount = 2;
            this.MapLotPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 30F));
            this.MapLotPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 70F));
            this.MapLotPanel.Controls.Add(this.cmbShowMapLot);
            this.MapLotPanel.Controls.Add(this.lblShowMapLot, 0, 0);
            this.MapLotPanel.Location = new System.Drawing.Point(0, 344);
            this.MapLotPanel.Margin = new Wisej.Web.Padding(0, 3, 3, 3);
            this.MapLotPanel.Name = "MapLotPanel";
            this.MapLotPanel.RowCount = 1;
            this.MapLotPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.MapLotPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.MapLotPanel.ScrollBars = Wisej.Web.ScrollBars.None;
            this.MapLotPanel.Size = new System.Drawing.Size(800, 46);
            this.MapLotPanel.TabIndex = 10;
            this.MapLotPanel.TabStop = true;
            this.MapLotPanel.Visible = false;
            // 
            // cmbShowMapLot
            // 
            this.cmbShowMapLot.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.cmbShowMapLot.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbShowMapLot.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbShowMapLot.Location = new System.Drawing.Point(243, 3);
            this.cmbShowMapLot.Name = "cmbShowMapLot";
            this.cmbShowMapLot.Size = new System.Drawing.Size(554, 40);
            this.cmbShowMapLot.TabIndex = 1;
            // 
            // lblShowMapLot
            // 
            this.lblShowMapLot.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.lblShowMapLot.AutoSize = true;
            this.lblShowMapLot.Location = new System.Drawing.Point(3, 3);
            this.lblShowMapLot.Name = "lblShowMapLot";
            this.lblShowMapLot.Size = new System.Drawing.Size(146, 40);
            this.lblShowMapLot.TabIndex = 16;
            this.lblShowMapLot.Text = "SHOW MAP AND LOT";
            this.lblShowMapLot.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // UseReturnAddressPanel
            // 
            this.CriteriaPanel.SetAlignY(this.UseReturnAddressPanel, Wisej.Web.VerticalAlignment.Top);
            this.UseReturnAddressPanel.ColumnCount = 2;
            this.UseReturnAddressPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 30F));
            this.UseReturnAddressPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 70F));
            this.UseReturnAddressPanel.Controls.Add(this.cmbUseReturnAddress);
            this.UseReturnAddressPanel.Controls.Add(this.lblUseReturnAddress, 0, 0);
            this.UseReturnAddressPanel.Location = new System.Drawing.Point(0, 397);
            this.UseReturnAddressPanel.Margin = new Wisej.Web.Padding(0, 3, 3, 3);
            this.UseReturnAddressPanel.Name = "UseReturnAddressPanel";
            this.UseReturnAddressPanel.RowCount = 1;
            this.UseReturnAddressPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.UseReturnAddressPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.UseReturnAddressPanel.ScrollBars = Wisej.Web.ScrollBars.None;
            this.UseReturnAddressPanel.Size = new System.Drawing.Size(800, 46);
            this.UseReturnAddressPanel.TabIndex = 11;
            this.UseReturnAddressPanel.TabStop = true;
            this.UseReturnAddressPanel.Visible = false;
            // 
            // cmbUseReturnAddress
            // 
            this.cmbUseReturnAddress.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.cmbUseReturnAddress.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbUseReturnAddress.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbUseReturnAddress.Location = new System.Drawing.Point(243, 3);
            this.cmbUseReturnAddress.Name = "cmbUseReturnAddress";
            this.cmbUseReturnAddress.Size = new System.Drawing.Size(554, 40);
            this.cmbUseReturnAddress.TabIndex = 1;
            // 
            // lblUseReturnAddress
            // 
            this.lblUseReturnAddress.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.lblUseReturnAddress.AutoSize = true;
            this.lblUseReturnAddress.Location = new System.Drawing.Point(3, 3);
            this.lblUseReturnAddress.Name = "lblUseReturnAddress";
            this.lblUseReturnAddress.Size = new System.Drawing.Size(164, 40);
            this.lblUseReturnAddress.TabIndex = 16;
            this.lblUseReturnAddress.Text = "USE RETURN ADDRESS";
            this.lblUseReturnAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // MailDatePanel
            // 
            this.CriteriaPanel.SetAlignY(this.MailDatePanel, Wisej.Web.VerticalAlignment.Top);
            this.MailDatePanel.ColumnCount = 4;
            this.MailDatePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 30F));
            this.MailDatePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 21.375F));
            this.MailDatePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 28.625F));
            this.MailDatePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 20F));
            this.MailDatePanel.Controls.Add(this.lblInterestDate);
            this.MailDatePanel.Controls.Add(this.txtInterestDate);
            this.MailDatePanel.Controls.Add(this.txtMailDate, 1, 0);
            this.MailDatePanel.Controls.Add(this.lblMailDate, 0, 0);
            this.MailDatePanel.Location = new System.Drawing.Point(0, 450);
            this.MailDatePanel.Margin = new Wisej.Web.Padding(0, 3, 3, 3);
            this.MailDatePanel.Name = "MailDatePanel";
            this.MailDatePanel.RowCount = 1;
            this.MailDatePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.MailDatePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.MailDatePanel.ScrollBars = Wisej.Web.ScrollBars.None;
            this.MailDatePanel.Size = new System.Drawing.Size(800, 46);
            this.MailDatePanel.TabIndex = 12;
            this.MailDatePanel.TabStop = true;
            this.MailDatePanel.Visible = false;
            // 
            // lblInterestDate
            // 
            this.lblInterestDate.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Right)));
            this.lblInterestDate.AutoSize = true;
            this.lblInterestDate.Location = new System.Drawing.Point(516, 3);
            this.lblInterestDate.Margin = new Wisej.Web.Padding(3, 3, 10, 3);
            this.lblInterestDate.Name = "lblInterestDate";
            this.lblInterestDate.Size = new System.Drawing.Size(114, 40);
            this.lblInterestDate.TabIndex = 26;
            this.lblInterestDate.Text = "INTEREST DATE";
            this.lblInterestDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblInterestDate.Visible = false;
            // 
            // txtInterestDate
            // 
            this.txtInterestDate.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.txtInterestDate.Location = new System.Drawing.Point(643, 3);
            this.txtInterestDate.Name = "txtInterestDate";
            this.txtInterestDate.Size = new System.Drawing.Size(154, 40);
            this.txtInterestDate.TabIndex = 2;
            this.txtInterestDate.Visible = false;
            // 
            // txtMailDate
            // 
            this.txtMailDate.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.txtMailDate.Location = new System.Drawing.Point(243, 3);
            this.txtMailDate.Name = "txtMailDate";
            this.txtMailDate.Size = new System.Drawing.Size(165, 40);
            this.txtMailDate.TabIndex = 1;
            this.txtMailDate.Visible = false;
            // 
            // lblMailDate
            // 
            this.lblMailDate.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.lblMailDate.AutoSize = true;
            this.lblMailDate.Location = new System.Drawing.Point(3, 3);
            this.lblMailDate.Name = "lblMailDate";
            this.lblMailDate.Size = new System.Drawing.Size(81, 40);
            this.lblMailDate.TabIndex = 16;
            this.lblMailDate.Text = "MAIL DATE";
            this.lblMailDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblMailDate.Visible = false;
            // 
            // InterestDeptPanel
            // 
            this.CriteriaPanel.SetAlignY(this.InterestDeptPanel, Wisej.Web.VerticalAlignment.Top);
            this.InterestDeptPanel.ColumnCount = 4;
            this.InterestDeptPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 30F));
            this.InterestDeptPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 35F));
            this.InterestDeptPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 15F));
            this.InterestDeptPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 20F));
            this.InterestDeptPanel.Controls.Add(this.lblInterestDept, 0, 0);
            this.InterestDeptPanel.Controls.Add(this.txtInterestDeptPhone, 3, 0);
            this.InterestDeptPanel.Controls.Add(this.lblInterestDeptPhone, 2, 0);
            this.InterestDeptPanel.Controls.Add(this.txtInterestDept, 1, 0);
            this.InterestDeptPanel.GrowStyle = Wisej.Web.TableLayoutPanelGrowStyle.FixedSize;
            this.InterestDeptPanel.Location = new System.Drawing.Point(0, 503);
            this.InterestDeptPanel.Margin = new Wisej.Web.Padding(0, 3, 3, 3);
            this.InterestDeptPanel.Name = "InterestDeptPanel";
            this.InterestDeptPanel.RowCount = 1;
            this.InterestDeptPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.InterestDeptPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.InterestDeptPanel.ScrollBars = Wisej.Web.ScrollBars.None;
            this.InterestDeptPanel.Size = new System.Drawing.Size(800, 46);
            this.InterestDeptPanel.TabIndex = 13;
            this.InterestDeptPanel.TabStop = true;
            this.InterestDeptPanel.Visible = false;
            // 
            // lblInterestDept
            // 
            this.lblInterestDept.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.lblInterestDept.AutoSize = true;
            this.lblInterestDept.Location = new System.Drawing.Point(3, 3);
            this.lblInterestDept.Name = "lblInterestDept";
            this.lblInterestDept.Size = new System.Drawing.Size(171, 40);
            this.lblInterestDept.TabIndex = 16;
            this.lblInterestDept.Text = "INTEREST DEPARTMENT";
            this.lblInterestDept.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtInterestDeptPhone
            // 
            this.txtInterestDeptPhone.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.txtInterestDeptPhone.Location = new System.Drawing.Point(643, 3);
            this.txtInterestDeptPhone.Name = "txtInterestDeptPhone";
            this.txtInterestDeptPhone.Size = new System.Drawing.Size(154, 40);
            this.txtInterestDeptPhone.TabIndex = 2;
            // 
            // lblInterestDeptPhone
            // 
            this.lblInterestDeptPhone.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Right)));
            this.lblInterestDeptPhone.AutoSize = true;
            this.lblInterestDeptPhone.Location = new System.Drawing.Point(575, 3);
            this.lblInterestDeptPhone.Margin = new Wisej.Web.Padding(3, 3, 10, 3);
            this.lblInterestDeptPhone.Name = "lblInterestDeptPhone";
            this.lblInterestDeptPhone.Size = new System.Drawing.Size(55, 40);
            this.lblInterestDeptPhone.TabIndex = 21;
            this.lblInterestDeptPhone.Text = "PHONE";
            this.lblInterestDeptPhone.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtInterestDept
            // 
            this.txtInterestDept.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.txtInterestDept.Location = new System.Drawing.Point(243, 3);
            this.txtInterestDept.Name = "txtInterestDept";
            this.txtInterestDept.Size = new System.Drawing.Size(274, 40);
            this.txtInterestDept.TabIndex = 1;
            // 
            // MaturedYearDeptPanel
            // 
            this.CriteriaPanel.SetAlignY(this.MaturedYearDeptPanel, Wisej.Web.VerticalAlignment.Top);
            this.MaturedYearDeptPanel.ColumnCount = 4;
            this.MaturedYearDeptPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 30F));
            this.MaturedYearDeptPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 35F));
            this.MaturedYearDeptPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 15F));
            this.MaturedYearDeptPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 20F));
            this.MaturedYearDeptPanel.Controls.Add(this.lblMaturedYearDept, 0, 0);
            this.MaturedYearDeptPanel.Controls.Add(this.lblMaturedYearDeptPhone, 2, 0);
            this.MaturedYearDeptPanel.Controls.Add(this.txtMaturedYearDeptPhone, 3, 0);
            this.MaturedYearDeptPanel.Controls.Add(this.txtMaturedYearDept, 1, 0);
            this.MaturedYearDeptPanel.Location = new System.Drawing.Point(0, 556);
            this.MaturedYearDeptPanel.Margin = new Wisej.Web.Padding(0, 3, 3, 3);
            this.MaturedYearDeptPanel.Name = "MaturedYearDeptPanel";
            this.MaturedYearDeptPanel.RowCount = 1;
            this.MaturedYearDeptPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.MaturedYearDeptPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.MaturedYearDeptPanel.ScrollBars = Wisej.Web.ScrollBars.None;
            this.MaturedYearDeptPanel.Size = new System.Drawing.Size(800, 46);
            this.MaturedYearDeptPanel.TabIndex = 14;
            this.MaturedYearDeptPanel.TabStop = true;
            this.MaturedYearDeptPanel.Visible = false;
            // 
            // lblMaturedYearDept
            // 
            this.lblMaturedYearDept.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.lblMaturedYearDept.AutoSize = true;
            this.lblMaturedYearDept.Font = new System.Drawing.Font("default", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblMaturedYearDept.Location = new System.Drawing.Point(3, 3);
            this.lblMaturedYearDept.Name = "lblMaturedYearDept";
            this.lblMaturedYearDept.Size = new System.Drawing.Size(210, 40);
            this.lblMaturedYearDept.TabIndex = 16;
            this.lblMaturedYearDept.Text = "MATURED YEAR DEPARTMENT";
            this.lblMaturedYearDept.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMaturedYearDeptPhone
            // 
            this.lblMaturedYearDeptPhone.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Right)));
            this.lblMaturedYearDeptPhone.AutoSize = true;
            this.lblMaturedYearDeptPhone.Location = new System.Drawing.Point(575, 3);
            this.lblMaturedYearDeptPhone.Margin = new Wisej.Web.Padding(3, 3, 10, 3);
            this.lblMaturedYearDeptPhone.Name = "lblMaturedYearDeptPhone";
            this.lblMaturedYearDeptPhone.Size = new System.Drawing.Size(55, 40);
            this.lblMaturedYearDeptPhone.TabIndex = 22;
            this.lblMaturedYearDeptPhone.Text = "PHONE";
            this.lblMaturedYearDeptPhone.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtMaturedYearDeptPhone
            // 
            this.txtMaturedYearDeptPhone.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.txtMaturedYearDeptPhone.Location = new System.Drawing.Point(643, 3);
            this.txtMaturedYearDeptPhone.Name = "txtMaturedYearDeptPhone";
            this.txtMaturedYearDeptPhone.Size = new System.Drawing.Size(154, 40);
            this.txtMaturedYearDeptPhone.TabIndex = 2;
            // 
            // txtMaturedYearDept
            // 
            this.txtMaturedYearDept.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.txtMaturedYearDept.Location = new System.Drawing.Point(243, 3);
            this.txtMaturedYearDept.Name = "txtMaturedYearDept";
            this.txtMaturedYearDept.Size = new System.Drawing.Size(274, 40);
            this.txtMaturedYearDept.TabIndex = 1;
            // 
            // HighestYearToIncludePanel
            // 
            this.CriteriaPanel.SetAlignY(this.HighestYearToIncludePanel, Wisej.Web.VerticalAlignment.Top);
            this.HighestYearToIncludePanel.ColumnCount = 4;
            this.HighestYearToIncludePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 30F));
            this.HighestYearToIncludePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 25F));
            this.HighestYearToIncludePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 25F));
            this.HighestYearToIncludePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 20F));
            this.HighestYearToIncludePanel.Controls.Add(this.cmbHighestYearToInclude);
            this.HighestYearToIncludePanel.Controls.Add(this.cmbLastYearMatured);
            this.HighestYearToIncludePanel.Controls.Add(this.lblHighestYearToInclude, 0, 0);
            this.HighestYearToIncludePanel.Controls.Add(this.lblLastYearMatured, 2, 0);
            this.HighestYearToIncludePanel.Location = new System.Drawing.Point(0, 609);
            this.HighestYearToIncludePanel.Margin = new Wisej.Web.Padding(0, 3, 3, 3);
            this.HighestYearToIncludePanel.Name = "HighestYearToIncludePanel";
            this.HighestYearToIncludePanel.RowCount = 1;
            this.HighestYearToIncludePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.HighestYearToIncludePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.HighestYearToIncludePanel.ScrollBars = Wisej.Web.ScrollBars.None;
            this.HighestYearToIncludePanel.Size = new System.Drawing.Size(800, 46);
            this.HighestYearToIncludePanel.TabIndex = 15;
            this.HighestYearToIncludePanel.TabStop = true;
            this.HighestYearToIncludePanel.Visible = false;
            // 
            // cmbHighestYearToInclude
            // 
            this.cmbHighestYearToInclude.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.cmbHighestYearToInclude.Location = new System.Drawing.Point(243, 3);
            this.cmbHighestYearToInclude.Name = "cmbHighestYearToInclude";
            this.cmbHighestYearToInclude.Size = new System.Drawing.Size(112, 40);
            this.cmbHighestYearToInclude.TabIndex = 1;
            // 
            // cmbLastYearMatured
            // 
            this.cmbLastYearMatured.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.cmbLastYearMatured.Location = new System.Drawing.Point(643, 3);
            this.cmbLastYearMatured.Name = "cmbLastYearMatured";
            this.cmbLastYearMatured.Size = new System.Drawing.Size(112, 40);
            this.cmbLastYearMatured.TabIndex = 2;
            // 
            // lblHighestYearToInclude
            // 
            this.lblHighestYearToInclude.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.lblHighestYearToInclude.AutoSize = true;
            this.lblHighestYearToInclude.Location = new System.Drawing.Point(3, 3);
            this.lblHighestYearToInclude.Name = "lblHighestYearToInclude";
            this.lblHighestYearToInclude.Size = new System.Drawing.Size(194, 40);
            this.lblHighestYearToInclude.TabIndex = 16;
            this.lblHighestYearToInclude.Text = "HIGHEST YEAR TO INCLUDE";
            this.lblHighestYearToInclude.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLastYearMatured
            // 
            this.lblLastYearMatured.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Right)));
            this.lblLastYearMatured.AutoSize = true;
            this.lblLastYearMatured.Location = new System.Drawing.Point(476, 3);
            this.lblLastYearMatured.Margin = new Wisej.Web.Padding(3, 3, 10, 3);
            this.lblLastYearMatured.Name = "lblLastYearMatured";
            this.lblLastYearMatured.Size = new System.Drawing.Size(154, 40);
            this.lblLastYearMatured.TabIndex = 17;
            this.lblLastYearMatured.Text = "LAST YEAR MATURED";
            this.lblLastYearMatured.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ShowNamePanel
            // 
            this.CriteriaPanel.SetAlignY(this.ShowNamePanel, Wisej.Web.VerticalAlignment.Top);
            this.ShowNamePanel.ColumnCount = 2;
            this.ShowNamePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 30F));
            this.ShowNamePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 70F));
            this.ShowNamePanel.Controls.Add(this.cmbShowName);
            this.ShowNamePanel.Controls.Add(this.lblShowName, 0, 0);
            this.ShowNamePanel.Location = new System.Drawing.Point(0, 662);
            this.ShowNamePanel.Margin = new Wisej.Web.Padding(0, 3, 3, 3);
            this.ShowNamePanel.Name = "ShowNamePanel";
            this.ShowNamePanel.RowCount = 1;
            this.ShowNamePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.ShowNamePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.ShowNamePanel.ScrollBars = Wisej.Web.ScrollBars.None;
            this.ShowNamePanel.Size = new System.Drawing.Size(800, 46);
            this.ShowNamePanel.TabIndex = 16;
            this.ShowNamePanel.TabStop = true;
            // 
            // cmbShowName
            // 
            this.cmbShowName.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.cmbShowName.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbShowName.Items.AddRange(new object[] {
            "Owner/Address at time of Billing.",
            "Owner at time of Billing, C/O Current Owner/Address.",
            "Owner at time of Billing at Last Recorded Address."});
            this.cmbShowName.Location = new System.Drawing.Point(243, 3);
            this.cmbShowName.Name = "cmbShowName";
            this.cmbShowName.Size = new System.Drawing.Size(554, 40);
            this.cmbShowName.TabIndex = 1;
            // 
            // lblShowName
            // 
            this.lblShowName.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.lblShowName.AutoSize = true;
            this.lblShowName.Location = new System.Drawing.Point(3, 3);
            this.lblShowName.Name = "lblShowName";
            this.lblShowName.Size = new System.Drawing.Size(93, 40);
            this.lblShowName.TabIndex = 16;
            this.lblShowName.Text = "SHOW NAME";
            this.lblShowName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // HideDupNamesPanel
            // 
            this.CriteriaPanel.SetAlignY(this.HideDupNamesPanel, Wisej.Web.VerticalAlignment.Top);
            this.HideDupNamesPanel.ColumnCount = 2;
            this.HideDupNamesPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 30F));
            this.HideDupNamesPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 70F));
            this.HideDupNamesPanel.Controls.Add(this.cmbHideDupNames);
            this.HideDupNamesPanel.Controls.Add(this.lblHideDupNames, 0, 0);
            this.HideDupNamesPanel.Location = new System.Drawing.Point(0, 715);
            this.HideDupNamesPanel.Margin = new Wisej.Web.Padding(0, 3, 3, 3);
            this.HideDupNamesPanel.Name = "HideDupNamesPanel";
            this.HideDupNamesPanel.RowCount = 1;
            this.HideDupNamesPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.HideDupNamesPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.HideDupNamesPanel.ScrollBars = Wisej.Web.ScrollBars.None;
            this.HideDupNamesPanel.Size = new System.Drawing.Size(800, 46);
            this.HideDupNamesPanel.TabIndex = 17;
            this.HideDupNamesPanel.TabStop = true;
            // 
            // cmbHideDupNames
            // 
            this.cmbHideDupNames.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.cmbHideDupNames.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbHideDupNames.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbHideDupNames.Location = new System.Drawing.Point(243, 3);
            this.cmbHideDupNames.Name = "cmbHideDupNames";
            this.cmbHideDupNames.Size = new System.Drawing.Size(554, 40);
            this.cmbHideDupNames.TabIndex = 1;
            // 
            // lblHideDupNames
            // 
            this.lblHideDupNames.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.lblHideDupNames.AutoSize = true;
            this.lblHideDupNames.Location = new System.Drawing.Point(3, 3);
            this.lblHideDupNames.Name = "lblHideDupNames";
            this.lblHideDupNames.Size = new System.Drawing.Size(172, 40);
            this.lblHideDupNames.TabIndex = 16;
            this.lblHideDupNames.Text = "HIDE DUPLICATE NAMES";
            this.lblHideDupNames.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LineBeforeSigPanel
            // 
            this.CriteriaPanel.SetAlignY(this.LineBeforeSigPanel, Wisej.Web.VerticalAlignment.Top);
            this.LineBeforeSigPanel.ColumnCount = 2;
            this.LineBeforeSigPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 30F));
            this.LineBeforeSigPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 70F));
            this.LineBeforeSigPanel.Controls.Add(this.lblLineBeforeSig, 0, 0);
            this.LineBeforeSigPanel.Controls.Add(this.txtLineBeforeSig, 1, -1);
            this.LineBeforeSigPanel.Location = new System.Drawing.Point(0, 768);
            this.LineBeforeSigPanel.Margin = new Wisej.Web.Padding(0, 3, 3, 3);
            this.LineBeforeSigPanel.Name = "LineBeforeSigPanel";
            this.LineBeforeSigPanel.RowCount = 1;
            this.LineBeforeSigPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.LineBeforeSigPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.LineBeforeSigPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Absolute, 20F));
            this.LineBeforeSigPanel.ScrollBars = Wisej.Web.ScrollBars.None;
            this.LineBeforeSigPanel.Size = new System.Drawing.Size(800, 46);
            this.LineBeforeSigPanel.TabIndex = 18;
            this.LineBeforeSigPanel.TabStop = true;
            this.LineBeforeSigPanel.Visible = false;
            // 
            // lblLineBeforeSig
            // 
            this.lblLineBeforeSig.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.lblLineBeforeSig.AutoSize = true;
            this.lblLineBeforeSig.Location = new System.Drawing.Point(3, 3);
            this.lblLineBeforeSig.Name = "lblLineBeforeSig";
            this.lblLineBeforeSig.Size = new System.Drawing.Size(180, 40);
            this.lblLineBeforeSig.TabIndex = 16;
            this.lblLineBeforeSig.Text = "LINE BEFORE SIGNATURE";
            this.lblLineBeforeSig.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtLineBeforeSig
            // 
            this.txtLineBeforeSig.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.txtLineBeforeSig.Location = new System.Drawing.Point(243, 3);
            this.txtLineBeforeSig.Name = "txtLineBeforeSig";
            this.txtLineBeforeSig.Size = new System.Drawing.Size(554, 40);
            this.txtLineBeforeSig.TabIndex = 1;
            // 
            // FirstLineAfterSigPanel
            // 
            this.CriteriaPanel.SetAlignY(this.FirstLineAfterSigPanel, Wisej.Web.VerticalAlignment.Top);
            this.FirstLineAfterSigPanel.ColumnCount = 2;
            this.FirstLineAfterSigPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 30F));
            this.FirstLineAfterSigPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 70F));
            this.FirstLineAfterSigPanel.Controls.Add(this.lblFirstLineAfterSig);
            this.FirstLineAfterSigPanel.Controls.Add(this.txtFirstLineAfterSig);
            this.FirstLineAfterSigPanel.Location = new System.Drawing.Point(0, 821);
            this.FirstLineAfterSigPanel.Margin = new Wisej.Web.Padding(0, 3, 3, 3);
            this.FirstLineAfterSigPanel.Name = "FirstLineAfterSigPanel";
            this.FirstLineAfterSigPanel.RowCount = 1;
            this.FirstLineAfterSigPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.FirstLineAfterSigPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.FirstLineAfterSigPanel.ScrollBars = Wisej.Web.ScrollBars.None;
            this.FirstLineAfterSigPanel.Size = new System.Drawing.Size(800, 46);
            this.FirstLineAfterSigPanel.TabIndex = 19;
            this.FirstLineAfterSigPanel.TabStop = true;
            this.FirstLineAfterSigPanel.Visible = false;
            // 
            // lblFirstLineAfterSig
            // 
            this.lblFirstLineAfterSig.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.lblFirstLineAfterSig.AutoSize = true;
            this.lblFirstLineAfterSig.Location = new System.Drawing.Point(3, 3);
            this.lblFirstLineAfterSig.Name = "lblFirstLineAfterSig";
            this.lblFirstLineAfterSig.Size = new System.Drawing.Size(212, 40);
            this.lblFirstLineAfterSig.TabIndex = 51;
            this.lblFirstLineAfterSig.Text = "FIRST LINE AFTER SIGNATURE";
            this.lblFirstLineAfterSig.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFirstLineAfterSig
            // 
            this.txtFirstLineAfterSig.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.txtFirstLineAfterSig.Location = new System.Drawing.Point(243, 3);
            this.txtFirstLineAfterSig.Name = "txtFirstLineAfterSig";
            this.txtFirstLineAfterSig.Size = new System.Drawing.Size(554, 40);
            this.txtFirstLineAfterSig.TabIndex = 1;
            // 
            // SecondLineAfterSigPanel
            // 
            this.CriteriaPanel.SetAlignY(this.SecondLineAfterSigPanel, Wisej.Web.VerticalAlignment.Top);
            this.SecondLineAfterSigPanel.ColumnCount = 2;
            this.SecondLineAfterSigPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 30F));
            this.SecondLineAfterSigPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 70F));
            this.SecondLineAfterSigPanel.Controls.Add(this.lblSecondLineAfterSig);
            this.SecondLineAfterSigPanel.Controls.Add(this.txtSecondLineAfterSig);
            this.SecondLineAfterSigPanel.Location = new System.Drawing.Point(0, 874);
            this.SecondLineAfterSigPanel.Margin = new Wisej.Web.Padding(0, 3, 3, 3);
            this.SecondLineAfterSigPanel.Name = "SecondLineAfterSigPanel";
            this.SecondLineAfterSigPanel.RowCount = 1;
            this.SecondLineAfterSigPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.SecondLineAfterSigPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.SecondLineAfterSigPanel.ScrollBars = Wisej.Web.ScrollBars.None;
            this.SecondLineAfterSigPanel.Size = new System.Drawing.Size(800, 46);
            this.SecondLineAfterSigPanel.TabIndex = 15;
            this.SecondLineAfterSigPanel.TabStop = true;
            this.SecondLineAfterSigPanel.Visible = false;
            // 
            // lblSecondLineAfterSig
            // 
            this.lblSecondLineAfterSig.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.lblSecondLineAfterSig.Location = new System.Drawing.Point(3, 3);
            this.lblSecondLineAfterSig.Name = "lblSecondLineAfterSig";
            this.lblSecondLineAfterSig.Size = new System.Drawing.Size(230, 40);
            this.lblSecondLineAfterSig.TabIndex = 52;
            this.lblSecondLineAfterSig.Text = "SECOND LINE AFTER SIGNATURE";
            this.lblSecondLineAfterSig.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSecondLineAfterSig
            // 
            this.txtSecondLineAfterSig.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.txtSecondLineAfterSig.Location = new System.Drawing.Point(243, 3);
            this.txtSecondLineAfterSig.Name = "txtSecondLineAfterSig";
            this.txtSecondLineAfterSig.Size = new System.Drawing.Size(554, 40);
            this.txtSecondLineAfterSig.TabIndex = 1;
            // 
            // ReturnAddressPanel
            // 
            this.ReturnAddressPanel.BorderStyle = Wisej.Web.BorderStyle.Solid;
            this.ReturnAddressPanel.ColumnCount = 2;
            this.ReturnAddressPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 30F));
            this.ReturnAddressPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 70F));
            this.ReturnAddressPanel.Controls.Add(this.lblReturnAddress, 0, 0);
            this.ReturnAddressPanel.Controls.Add(this.txtReturnAddress4, 1, 3);
            this.ReturnAddressPanel.Controls.Add(this.txtReturnAddress3, 1, 2);
            this.ReturnAddressPanel.Controls.Add(this.txtReturnAddress2, 1, 1);
            this.ReturnAddressPanel.Controls.Add(this.txtReturnAddress1, 1, 0);
            this.ReturnAddressPanel.Location = new System.Drawing.Point(0, 927);
            this.ReturnAddressPanel.Margin = new Wisej.Web.Padding(0, 3, 3, 3);
            this.ReturnAddressPanel.Name = "ReturnAddressPanel";
            this.ReturnAddressPanel.RowCount = 4;
            this.ReturnAddressPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 25F));
            this.ReturnAddressPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 25F));
            this.ReturnAddressPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 25F));
            this.ReturnAddressPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 25F));
            this.ReturnAddressPanel.Size = new System.Drawing.Size(800, 188);
            this.ReturnAddressPanel.TabIndex = 20;
            this.ReturnAddressPanel.TabStop = true;
            this.ReturnAddressPanel.Visible = false;
            // 
            // lblReturnAddress
            // 
            this.lblReturnAddress.AutoSize = true;
            this.lblReturnAddress.Location = new System.Drawing.Point(3, 12);
            this.lblReturnAddress.Margin = new Wisej.Web.Padding(3, 12, 3, 3);
            this.lblReturnAddress.Name = "lblReturnAddress";
            this.lblReturnAddress.Size = new System.Drawing.Size(133, 17);
            this.lblReturnAddress.TabIndex = 57;
            this.lblReturnAddress.Text = "RETURN ADDRESS";
            this.lblReturnAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtReturnAddress4
            // 
            this.txtReturnAddress4.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.txtReturnAddress4.AutoSize = false;
            this.txtReturnAddress4.Location = new System.Drawing.Point(242, 141);
            this.txtReturnAddress4.Margin = new Wisej.Web.Padding(3, 3, 3, 6);
            this.txtReturnAddress4.Name = "txtReturnAddress4";
            this.txtReturnAddress4.Size = new System.Drawing.Size(553, 39);
            this.txtReturnAddress4.TabIndex = 4;
            this.txtReturnAddress4.WordWrap = false;
            // 
            // txtReturnAddress3
            // 
            this.txtReturnAddress3.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.txtReturnAddress3.AutoSize = false;
            this.txtReturnAddress3.Location = new System.Drawing.Point(242, 98);
            this.txtReturnAddress3.Margin = new Wisej.Web.Padding(3, 6, 3, 6);
            this.txtReturnAddress3.Name = "txtReturnAddress3";
            this.txtReturnAddress3.Size = new System.Drawing.Size(553, 34);
            this.txtReturnAddress3.TabIndex = 3;
            this.txtReturnAddress3.WordWrap = false;
            // 
            // txtReturnAddress2
            // 
            this.txtReturnAddress2.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.txtReturnAddress2.AutoSize = false;
            this.txtReturnAddress2.Location = new System.Drawing.Point(242, 49);
            this.txtReturnAddress2.Margin = new Wisej.Web.Padding(3, 3, 3, 6);
            this.txtReturnAddress2.Name = "txtReturnAddress2";
            this.txtReturnAddress2.Size = new System.Drawing.Size(553, 37);
            this.txtReturnAddress2.TabIndex = 2;
            this.txtReturnAddress2.WordWrap = false;
            // 
            // txtReturnAddress1
            // 
            this.txtReturnAddress1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.txtReturnAddress1.AutoSize = false;
            this.txtReturnAddress1.Location = new System.Drawing.Point(242, 3);
            this.txtReturnAddress1.Margin = new Wisej.Web.Padding(3, 3, 3, 6);
            this.txtReturnAddress1.Name = "txtReturnAddress1";
            this.txtReturnAddress1.Size = new System.Drawing.Size(553, 37);
            this.txtReturnAddress1.TabIndex = 1;
            this.txtReturnAddress1.WordWrap = false;
            // 
            // lblMinAmount
            // 
            this.lblMinAmount.AutoSize = true;
            this.lblMinAmount.Location = new System.Drawing.Point(553, 163);
            this.lblMinAmount.Name = "lblMinAmount";
            this.lblMinAmount.Size = new System.Drawing.Size(203, 17);
            this.lblMinAmount.TabIndex = 2;
            this.lblMinAmount.Text = "MIN. PRINCIPAL TO PROCESS";
            this.lblMinAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblMinAmount.WordWrap = true;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Cursor = Wisej.Web.Cursors.Default;
            this.btnProcess.Location = new System.Drawing.Point(363, 30);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcess.Size = new System.Drawing.Size(159, 48);
            this.btnProcess.TabIndex = 2;
            this.btnProcess.Text = "Save & Preview";
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // frmReminderNotices
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1188, 830);
            this.Cursor = Wisej.Web.Cursors.Default;
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmReminderNotices";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Reminder Notices";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmReminderNotices_Load);
            this.Activated += new System.EventHandler(this.frmReminderNotices_Activated);
            this.Resize += new System.EventHandler(this.frmReminderNotices_Resize);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmReminderNotices_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraMessage)).EndInit();
            this.fraMessage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraNOPastDue)).EndInit();
            this.fraNOPastDue.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraType)).EndInit();
            this.fraType.ResumeLayout(false);
            this.fraType.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkGetPreviousYears)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowPastDueAmounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseTownLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseSig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBulkMailing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraRN)).EndInit();
            this.CriteriaPanel.ResumeLayout(false);
            this.signaturePanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgSig)).EndInit();
            this.ReportTitlePanel.ResumeLayout(false);
            this.ReportTitlePanel.PerformLayout();
            this.LabelTypePanel.ResumeLayout(false);
            this.LabelTypePanel.PerformLayout();
            this.NamePanel.ResumeLayout(false);
            this.NamePanel.PerformLayout();
            this.AccountPanel.ResumeLayout(false);
            this.AccountPanel.PerformLayout();
            this.MessagePanel.ResumeLayout(false);
            this.MessagePanel.PerformLayout();
            this.MapLotPanel.ResumeLayout(false);
            this.MapLotPanel.PerformLayout();
            this.UseReturnAddressPanel.ResumeLayout(false);
            this.UseReturnAddressPanel.PerformLayout();
            this.MailDatePanel.ResumeLayout(false);
            this.MailDatePanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInterestDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMailDate)).EndInit();
            this.InterestDeptPanel.ResumeLayout(false);
            this.InterestDeptPanel.PerformLayout();
            this.MaturedYearDeptPanel.ResumeLayout(false);
            this.MaturedYearDeptPanel.PerformLayout();
            this.HighestYearToIncludePanel.ResumeLayout(false);
            this.HighestYearToIncludePanel.PerformLayout();
            this.ShowNamePanel.ResumeLayout(false);
            this.ShowNamePanel.PerformLayout();
            this.HideDupNamesPanel.ResumeLayout(false);
            this.HideDupNamesPanel.PerformLayout();
            this.LineBeforeSigPanel.ResumeLayout(false);
            this.LineBeforeSigPanel.PerformLayout();
            this.FirstLineAfterSigPanel.ResumeLayout(false);
            this.FirstLineAfterSigPanel.PerformLayout();
            this.SecondLineAfterSigPanel.ResumeLayout(false);
            this.SecondLineAfterSigPanel.PerformLayout();
            this.ReturnAddressPanel.ResumeLayout(false);
            this.ReturnAddressPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.ComponentModel.IContainer components;
        private FCLabel lblPeriod;
        private FCLabel lblOrder;
        private FCLabel lblRN;
        private FCButton btnProcess;
        private JavaScript javaScriptNumbersOnly;
        Wisej.Web.JavaScript.ClientEvent clientEventNumbersOnly;
        private FlexLayoutPanel CriteriaPanel;
        public FCLabel lblName;
        public FCLabel lblAccount;
        public FCLabel lblShowName;
        private TableLayoutPanel NamePanel;
        private TableLayoutPanel AccountPanel;
        private TableLayoutPanel ShowNamePanel;
        private TableLayoutPanel HideDupNamesPanel;
        public FCLabel lblHideDupNames;
        private TableLayoutPanel MessagePanel;
        public FCLabel lblMessage;
        private TableLayoutPanel UseReturnAddressPanel;
        public FCLabel lblUseReturnAddress;
        private TableLayoutPanel MailDatePanel;
        public FCLabel lblMailDate;
        private TableLayoutPanel InterestDeptPanel;
        public FCLabel lblInterestDept;
        private TableLayoutPanel MaturedYearDeptPanel;
        public FCLabel lblMaturedYearDept;
        private TableLayoutPanel HighestYearToIncludePanel;
        public FCLabel lblHighestYearToInclude;
        private TableLayoutPanel MapLotPanel;
        public FCLabel lblShowMapLot;
        private TableLayoutPanel LineBeforeSigPanel;
        public FCLabel lblLineBeforeSig;
        private TableLayoutPanel LabelTypePanel;
        public FCLabel lblLabelType;
        public T2KDateBox txtMailDate;
        private TextBox txtNameStart;
        private TextBox txtNameEnd;
        private TextBox txtAccountStart;
        private TextBox txtAccountEnd;
        private ComboBox cmbShowName;
        private ComboBox cmbHideDupNames;
        private TextBox txtMessage;
        private ComboBox cmbUseReturnAddress;
        private TextBox txtInterestDept;
        private TextBox txtMaturedYearDept;
        private ComboBox cmbShowMapLot;
        private TextBox txtLineBeforeSig;
        private ComboBox cmbLabelType;
        private TableLayoutPanel FirstLineAfterSigPanel;
        public FCLabel lblFirstLineAfterSig;
        private TextBox txtFirstLineAfterSig;
        private TableLayoutPanel SecondLineAfterSigPanel;
        public FCLabel lblSecondLineAfterSig;
        private TextBox txtSecondLineAfterSig;
        private TextBox txtReturnAddress2;
        private TextBox txtReturnAddress1;
        private TextBox txtReturnAddress4;
        public FCLabel lblInterestDate;
        public T2KDateBox txtInterestDate;
        public FCLabel lblMaturedYearDeptPhone;
        private TextBox txtMaturedYearDeptPhone;
        private TextBox txtInterestDeptPhone;
        public FCLabel lblInterestDeptPhone;
        public FCLabel lblLastYearMatured;
        private ComboBox cmbHighestYearToInclude;
        private ComboBox cmbLastYearMatured;
        private TableLayoutPanel ReturnAddressPanel;
        public FCLabel lblReturnAddress;
        private TextBox txtReturnAddress3;
        private TableLayoutPanel ReportTitlePanel;
        public FCLabel fcLabel1;
        private TableLayoutPanel signaturePanel;
    }
}
