﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using Chilkat;
using fecherFoundation;
using fecherFoundation.Extensions;
using SharedApplication.TaxCollections.Interfaces;
using Wisej.Web;

namespace TWCL0000
{
    public class ReminderNoticeOptions : ITaxCollectionNoticeOptions
    {
        public enum PeriodEnum
        {
            Period1 = 0,
            Period2 = 1,
            Period3 = 2,
            Period4 = 3,
            Outstanding = 4,
            PastDueOnly = 5
        }
        public enum AddressEnum
        {
            AddressAtBilling = 0,     // Owner/Address at time of Billing
            CareOfCurrentOwner = 1,   // Owner at time of Billing, C/O Current Owner/Address
            LastKnownAddress = 2      // Owner at time of Billing at Last Recorded Address
        }
        public string Query { get; set; }               // strPassRNFORMSQL
        public string ReportTitle { get; set; }         // frmReminderNotices.InstancePtr.txtReportTitle.Text
        public DateTime InterestDate { get; set; }      // frmReminderNotices.InstancePtr.vsGrid.TextMatrix(3, 1)
        public DateTime MailDate { get; set; }          // frmReminderNotices.InstancePtr.vsGrid.TextMatrix(4, 1)
        public string rtbData { get; set; }               // rtbData.Text 
        public double MinimumAmount { get; set; }       // dblMinimumAmount
        public string InterestDept { get; set; }        // frmReminderNotices.InstancePtr.vsGrid.TextMatrix(5, 1);
		public string InterestDeptPhone { get; set; }   // frmReminderNotices.InstancePtr.vsGrid.TextMatrix(6, 1);
		public string MaturedDept { get; set; }         // frmReminderNotices.InstancePtr.vsGrid.TextMatrix(7, 1);
		public string MaturedDeptPhone { get; set; }    // frmReminderNotices.InstancePtr.vsGrid.TextMatrix(8, 1);
        public string HighYear { get; set; }            // Strings.Left(frmReminderNotices.InstancePtr.vsGrid.TextMatrix(9, 1), 4);
		public string LastMaturedYear { get; set; }     // Strings.Left(frmReminderNotices.InstancePtr.vsGrid.TextMatrix(10, 1), 4);
		public string SignOut { get; set; }             // frmReminderNotices.InstancePtr.vsGrid.TextMatrix(12, 1);
		public string SignerName { get; set; }          // frmReminderNotices.InstancePtr.vsGrid.TextMatrix(13, 1);
		public string SignerTitle { get; set; }         // frmReminderNotices.InstancePtr.vsGrid.TextMatrix(14, 1);
        public string OriginalString { get; set; }
        public bool LienedRecords { get; set; }         // frmReminderNotices.InstancePtr.boolLienedRecords;
		public bool PreviousYears { get; set; }         // FCConvert.CBool(frmReminderNotices.InstancePtr.chkGetPreviousYears.CheckState == Wisej.Web.CheckState.Checked);
        public bool UseTownLogo { get; set; }
        public PeriodEnum Period { get; set; }
        public string RateKeyList { get; set; }
        public AddressEnum AddressType { get; set; }
        public bool UseReturnAddress { get; set; }
        public string Printer { get; set; }
        public bool BulkMailing { get; set; }
        public string ReturnAddress1 { get; set; }
        public string ReturnAddress2 { get; set; }
        public string ReturnAddress3 { get; set; }
        public string ReturnAddress4 { get; set; }
        public string Message { get; set; }
        public int LabelType { get; set; }
        public bool ShowPastYears { get; set; }
        public bool ShowPending { get; set; }
        public string Font { get; set; }
        public bool HideDuplicateNames { get; set; }
        public bool UseSignature { get; set; }
        public string PreMessage { get; set; }
        public string PastDueMessage { get; set; }
    }
}
