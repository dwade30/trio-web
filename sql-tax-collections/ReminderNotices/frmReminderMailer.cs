﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmReminderMailer.
	/// </summary>
	public partial class frmReminderMailer : BaseForm
	{
		public frmReminderMailer()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.fraText = new System.Collections.Generic.List<fecherFoundation.FCFrame>();
			this.txtNotPastDue = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.fraText.AddControlArrayElement(fraText_1, 1);
			this.fraText.AddControlArrayElement(fraText_0, 0);
			this.txtNotPastDue.AddControlArrayElement(txtNotPastDue_1, 1);
			this.txtNotPastDue.AddControlArrayElement(txtNotPastDue_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmReminderMailer InstancePtr
		{
			get
			{
				return (frmReminderMailer)Sys.GetInstance(typeof(frmReminderMailer));
			}
		}

		protected frmReminderMailer _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               12/23/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/24/2004              *
		// ********************************************************
		string strSQL;
		string strPayment;
		DateTime dtDate;
		string strYear;
		bool boolLienedRecords;
		double dblMinimumAmount;
        private ReminderNoticeOptions reminderNoticeOptions;

		// MAL@20071207
		public void Init(ReminderNoticeOptions options)
		{
            //   Init(string strPassSQL, string strPassYear, string strPassPayment, DateTime dtPassDate, bool boolPassLienedRecords, double dblPassMinAmount)
            // collect date fronm the last form
            reminderNoticeOptions = options;
            strSQL = reminderNoticeOptions.Query;
			dblMinimumAmount = reminderNoticeOptions.MinimumAmount;
			dtDate = reminderNoticeOptions.MailDate;
			strYear = reminderNoticeOptions.HighYear;       // strPassYear;
			boolLienedRecords = reminderNoticeOptions.LienedRecords;
			fraText[0].Enabled = !boolLienedRecords;
			// setup the labels
			SetupLabels();
			// show this form
			this.Show(App.MainForm);
		}

		private void frmReminderMailer_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = 0;
				mnuFileExit_Click();
			}
		}

		private void frmReminderMailer_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmReminderMailer.Icon	= "frmReminderMailer.frx":0000";
			//frmReminderMailer.FillStyle	= 0;
			//frmReminderMailer.ScaleWidth	= 9045;
			//frmReminderMailer.ScaleHeight	= 7320;
			//frmReminderMailer.LinkTopic	= "Form2";
			//frmReminderMailer.LockControls	= -1  'True;
			//frmReminderMailer.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "8.25";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//txtPastDue.MaxLength	= 1028;
			//txtNotPastDue_1.MaxLength	= 1028;
			//txtNotPastDue_0.MaxLength	= 70;
			//vsElasticLight1.OleObjectBlob	= "frmReminderMailer.frx":0616";
			//End Unmaped Properties
			string strTemp = "";
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			this.Text = "Reminder Notices - Mailer";
			// get this information from the registry to default the boxes
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "REMINDERMAILERNOTPAST1", ref strTemp);
			txtNotPastDue[0].Text = strTemp;
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "REMINDERMAILERNOTPAST2", ref strTemp);
			txtNotPastDue[1].Text = strTemp;
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "REMINDERMAILERPAST", ref strTemp);
			txtPastDue.Text = strTemp;
		}

		private void frmReminderMailer_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				Close();
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void mnuFileClear_Click(object sender, System.EventArgs e)
		{
			// this will clear the textboxes out
			txtNotPastDue[0].Text = "";
			txtNotPastDue[1].Text = "";
			txtPastDue.Text = "";
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuFileExit_Click()
		{
			//mnuFileExit_Click(mnuFileExit, new System.EventArgs());
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will call the report initialize routine
				string strNotMessage;
				string strMessage;
				string strPreMessage;
				
				frmWait.InstancePtr.Init("Please Wait...");
				strPreMessage = txtNotPastDue[0].Text;
				strNotMessage = txtNotPastDue[1].Text;
				strMessage = txtPastDue.Text;
				// save this information into the registry
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "REMINDERMAILERNOTPAST1", strPreMessage);
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "REMINDERMAILERNOTPAST2", strNotMessage);
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "REMINDERMAILERPAST", strMessage);
				if (modStatusPayments.Statics.boolRE)
				{
                    // RE
                    reminderNoticeOptions.PreMessage = strPreMessage;
                    reminderNoticeOptions.Message = strNotMessage;
                    reminderNoticeOptions.PastDueMessage = strMessage;
                    reminderNoticeOptions.ReportTitle = "******  " + strYear + " Real Estate Tax - Reminder Notice  ******";
                    // rptReminderMailer.InstancePtr.Init(strSQL, "******  " + strYear + " Real Estate Tax - Reminder Notice  ******", strYear, strPayment, strNotMessage, strMessage, strPreMessage, dtDate, boolLienedRecords, dblMinimumAmount);
                    rptReminderMailer.InstancePtr.Init(reminderNoticeOptions);
                    // frmReportViewer.Init rptReminderMailer
                }
				else
				{
                    // PP
                    reminderNoticeOptions.PreMessage = strPreMessage;
                    reminderNoticeOptions.Message = strNotMessage;
                    reminderNoticeOptions.PastDueMessage = strMessage;
                    reminderNoticeOptions.ReportTitle = "******  " + strYear + " Personal Property Tax - Reminder Notice  ******";
                    //rptReminderMailer.InstancePtr.Init(strSQL, "******  " + strYear + " Personal Property Tax - Reminder Notice  ******", strYear, strPayment, strNotMessage, strMessage, strPreMessage, dtDate, false, dblMinimumAmount);
                    rptReminderMailer.InstancePtr.Init(reminderNoticeOptions);
                    // frmReportViewer.Init rptReminderMailer
                }
				frmWait.InstancePtr.Unload();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Loading Report");
			}
		}

		private void SetupLabels()
		{
			// this routine will make the labels look similar to what will be seen on the notice
			if (modStatusPayments.Statics.boolRE)
			{
				if (boolLienedRecords)
				{
					lblNotHeader.Text = "";
					lblNotMessage.Text = "";
					lblNotAmountDue.Text = "";
					lblHeader.Text = "******  " + strYear + " Real Estate Tax Lien - Reminder Notice  ******";
					lblMessage.Text = "This is to remind you that you have past due amounts on your " + strYear + " tax liens.";
					lblBreakDown.Text = "Tax Lien" + "\r\n" + "Interest" + "\r\n" + "Due  " + Strings.Format(dtDate, "MM/dd/yyyy") + "\r\n" + "\r\n" + "Total Due";
					lblBreakDownAmounts.Text = "###,###.##" + "\r\n" + "\r\n" + "###,###.##" + "\r\n" + "--------------------" + "\r\n" + "###,###.##";
				}
				else
				{
					lblNotHeader.Text = "******  " + strYear + " Real Estate Tax - Reminder Notice  ******";
					lblNotMessage.Text = "This is to remind you that the " + strPayment + " payment on your " + strYear + " Real Estate tax bill is due on " + Strings.Format(dtDate, "MM/dd/yyyy") + ".";
					lblNotAmountDue.Text = "Amount Due " + Strings.Format(dtDate, "MM/dd/yyyy") + "   ###,###.##";
					lblHeader.Text = "******  " + strYear + " Real Estate Tax - Reminder Notice  ******";
					lblMessage.Text = "This is to remind you that you have past due amounts on your " + strYear + " Real Estate tax bill and that the second payment is due on " + Strings.Format(dtDate, "MM/dd/yyyy") + ".";
					lblBreakDown.Text = "Past Due" + "\r\n" + "Interest" + "\r\n" + "Due  " + Strings.Format(dtDate, "MM/dd/yyyy") + "\r\n" + "\r\n" + "Total Due";
					lblBreakDownAmounts.Text = "###,###.##" + "\r\n" + "\r\n" + "###,###.##" + "\r\n" + "--------------------" + "\r\n" + "###,###.##";
				}
			}
			else
			{
				lblNotHeader.Text = "******  " + strYear + " Personal Property Tax - Reminder Notice  ******";
				lblNotMessage.Text = "This is to remind you that the second payment on your " + strYear + " Personal Property tax bill is due on " + Strings.Format(dtDate, "MM/dd/yyyy") + ".";
				lblNotAmountDue.Text = "Amount Due " + Strings.Format(dtDate, "MM/dd/yyyy") + "   ###,###.##";
				lblHeader.Text = "******  " + strYear + " Personal Property Tax - Reminder Notice  ******";
				lblMessage.Text = "This is to remind you that you have past due amounts on your " + strYear + " Personal Property tax bill and that the second payment is due on " + Strings.Format(dtDate, "MM/dd/yyyy") + ".";
				lblBreakDown.Text = "Past Due" + "\r\n" + "Interest" + "\r\n" + "Due  " + Strings.Format(dtDate, "MM/dd/yyyy") + "\r\n" + "\r\n" + "Total Due";
				lblBreakDownAmounts.Text = "###,###.##" + "\r\n" + "\r\n" + "###,###.##" + "\r\n" + "--------------------" + "\r\n" + "###,###.##";
			}
		}

		private void txtNotPastDue_KeyDown(int Index, object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Return)
			{
				if (Index == 0)
				{
					txtNotPastDue[1].Focus();
				}
				else
				{
					txtPastDue.Focus();
				}
			}
		}

		private void txtNotPastDue_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			int index = txtNotPastDue.IndexOf((FCTextBox)sender);
			txtNotPastDue_KeyDown(index, sender, e);
		}

		private void txtPastDue_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Return)
			{
				if (fraText[0].Enabled)
				{
					txtNotPastDue[0].Focus();
				}
			}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFilePrint_Click(sender, e);
		}

		private void cmdFileClear_Click(object sender, EventArgs e)
		{
			this.mnuFileClear_Click(sender, e);
		}
	}
}
