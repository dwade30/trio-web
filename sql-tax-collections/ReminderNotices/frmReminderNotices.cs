﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Wisej.Web;
using Global;
using Microsoft.Extensions.Options;
using SharedApplication;

namespace TWCL0000 
{
    #region Designer Support
    /// <summary>
    /// Summary description for frmReminderNotices.
    /// </summary>
    public partial class frmReminderNotices : BaseForm
	{
		public frmReminderNotices()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.Shown += FrmReminderNotices_Shown;
            this.CriteriaPanel.SizeChanged += CriteriaPanel_SizeChanged;       
		}

        private void CriteriaPanel_SizeChanged(object sender, EventArgs e)
        {           
            PositionBottomPanel();            
        }

        private void PositionBottomPanel()
        {
            if (boolLoaded)
            {
                BottomPanel.Top = CriteriaPanel.Bottom + 10;
            }
        }

        private void FrmReminderNotices_Shown(object sender, EventArgs e)
        {
            FillStringArray();
            ShowAllFrames();
            boolLoaded = true;
           // PositionBottomPanel();
        }

        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmReminderNotices InstancePtr
		{
			get
			{
				return (frmReminderNotices)Sys.GetInstance(typeof(frmReminderNotices));
			}
		}
        #endregion
        protected frmReminderNotices _InstancePtr = null;

        const int Labels = 0;
        const int PostCards = 1;
        const int FullPageForms = 2;
        const int Mailers = 3;
        const int Outprint = 4;
        const int LaserPostCards = 5;

		int intAction;
		clsDRWrapper rsData = new clsDRWrapper();
		bool boolLoaded;
		double dblDemand;
		double dblCertMailFee;
		bool boolChargeMort;
		//FC:FINAL:CHN: Incorrect working of ComboBox after redesign from RadioButtons.
		public string auxItem0CmbPeriod = "1";
		public string auxItem1CmbPeriod = "2";
		public string auxItem2CmbPeriod = "3";
		public string auxItem3CmbPeriod = "4";
        public string auxItem4CmbPeriod = "Outstanding";
		public string auxItem5CmbPeriod = "Past Due Only";
		private string[] auxItemcmbRN = new string[] {
			"Labels",
			"Post Cards",
			"Full Page",
			"Mailer",
			"Outprinting File",
			"Laser Post Cards"
		};
        private Dictionary<string,int> noticeTypeDictionary = new Dictionary<string, int>()
        {
            {"Labels",Labels },
            {"Post Cards", PostCards },
            {"Full Page",FullPageForms },
            {"Mailer", Mailers },
            {"Outprinting File", Outprint },
            {"Laser Post Cards",LaserPostCards }
        };
		bool boolChargeCert;
		DateTime dtMailDate;
		string[,] strList = new string[20 + 1, 5 + 1];
		bool boolChange;
		// to tell if intAction changed for the vsGrid_BeforeEdit
		int intReminderType;
		public string strRateKeyList = string.Empty;
		string strYear;
		public string strPassRNFORMSQL = "";
		public bool boolLienedRecords;
		public int lngNameType;
		// 0 - Owner at time of billing C/O current, 1 - Owner and Address at time of billing
		string strNameType;
		int lngRowOwnerNameType;
		public string strSigPassword = "";
		clsPrintLabel labLabelTypes = new clsPrintLabel();
		// VBto upgrade warning: intLabelType As int	OnWrite(short, string)
		int intLabelType;
		double dblMinimumAmount;
        string strQuerySQL = "";
        private int numberOfPeriods = 1;
        public void Init(string strPassRKList, string strPassYear, bool boolPassLienedRecords)
		{
			try
			{				
				
				cmbOrder.SelectedIndex = 0;
				// On Error GoTo ERROR_HANDLER
				// this will pass a list of the rate keys that will determine which accounts to use
				strRateKeyList = strPassRKList;
				strYear = strPassYear;
				boolLienedRecords = boolPassLienedRecords;
                FillLabelCombo();
                FillHighestYearToIncludeCombo();
                FillLastYearMaturedCombo();
				if (boolLienedRecords)
                {
                    numberOfPeriods = 1;
					//FC:FINAL:CHN: Incorrect working of ComboBox after redesign from RadioButtons.
					//cmbPeriod.Clear();
					//cmbPeriod.Items.Add(auxItem0CmbPeriod);
					//cmbPeriod.SelectedIndex = 0;
					// cmbPeriod.Enabled = false;
					//cmbRN.Items.Remove(auxItemcmbRN[4]);
                    FillNoticeTypeComboForLiens();
                }
				else
                {
                    FillNoticeTypeComboForRegular();
					// check the highest period allowed
					clsDRWrapper clsTemp = new clsDRWrapper();
					int intHigh = 0;
                    intHigh = 1;
					clsTemp.OpenRecordset("select * from raterec where ID in " + strRateKeyList, "twcl0000.vb1");
					while (!clsTemp.EndOfFile())
					{
						if (clsTemp.Get_Fields_Int16("numberofperiods") > intHigh)
						{
							intHigh = clsTemp.Get_Fields_Int16("numberofperiods");
						}
						clsTemp.MoveNext();
					}

                    numberOfPeriods = intHigh;
                   
                }
                UpdatePeriodChoices();
                cmbPeriod.SelectedIndex = 0;
                cmbRN.SelectedIndex = 0;
                this.Show(App.MainForm);
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Initializing Form");
			}
		}

        private void FillLabelCombo()
        {
            var labelList = GetLabelTypes();
            cmbLabelType.Items.Clear();
            cmbLabelType.Items.AddRange(labelList.ToArray());
        }

        private void FillHighestYearToIncludeCombo()
        {
            var yearList = GetBillingYears();
            cmbHighestYearToInclude.Items.Clear();
            cmbHighestYearToInclude.Items.AddRange(yearList.ToArray());
        }

        private void FillLastYearMaturedCombo()
        {
            var yearList = GetLienYears();
            cmbLastYearMatured.Items.Clear();
            cmbLastYearMatured.Items.AddRange(yearList.ToArray());
        }

        private void FillNoticeTypeComboForRegular()
        {
            this.cmbRN.Items.Clear();
            this.cmbRN.Items.AddRange(new object[] {
                "Labels",
                "Laser Post Cards",
                "Full Page",
                "Mailer",
                "Outprinting File"});
        }

        private void FillNoticeTypeComboForLiens()
        {
            this.cmbRN.Items.Clear();
            this.cmbRN.Items.AddRange(new object[] {
                "Labels",
                "Laser Post Cards",
                "Full Page",
                "Mailer"});
        }

        private void chkGetPreviousYears_Click(object sender, System.EventArgs e)
		{
			// MAL@20071015: Added to check the 'Show Past Due' when this option is selected
			if (chkGetPreviousYears.CheckState == Wisej.Web.CheckState.Checked)
			{
				chkShowPastDueAmounts.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkShowPastDueAmounts.CheckState = Wisej.Web.CheckState.Unchecked;
			}
		}

		private void chkUseSig_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				bool boolPasswordAccept/*unused?*/;
				int lngAnswer = 0;
				if (chkUseSig.CheckState == Wisej.Web.CheckState.Checked)
				{
					COLLECTORTRYAGAIN:
					;
					lngAnswer = frmSignaturePassword.InstancePtr.Init(0, default(int), default(bool), default(int), "CL");
					if (lngAnswer > 0)
					{
						// yes the password is correct
						modSignatureFile.Statics.gstrCollectorSigPath = Path.Combine(FCFileSystem.Statics.UserDataFolder, modSignatureFile.GetSigFileFromID(ref lngAnswer));
						// yes the password is correct
						if (File.Exists(modSignatureFile.Statics.gstrCollectorSigPath))
						{
							// rock on
							modGlobal.Statics.gboolUseSigFile = true;
                            imgSig.Visible = true;
							imgSig.Image = FCUtils.LoadPicture(modSignatureFile.Statics.gstrCollectorSigPath);
                            signaturePanel.Visible = true;
							modGlobalFunctions.AddCYAEntry_26("CL", "Use signature file for reminder notices.", "Reminder Notice");
						}
						else
						{
							modGlobal.Statics.gboolUseSigFile = false;
							chkUseSig.CheckState = Wisej.Web.CheckState.Unchecked;
							if (Strings.Trim(modSignatureFile.Statics.gstrCollectorSigPath) == "")
							{
								FCMessageBox.Show("No path setup.  Please set it up in General Entry > File Maintenance > Customize.", MsgBoxStyle.Exclamation, "Invalid Path");
							}
							else
							{
								FCMessageBox.Show(modSignatureFile.Statics.gstrCollectorSigPath + " is not a valid path.  Please set it up in General Entry > File Maintenance > Customize.", MsgBoxStyle.Exclamation, "Invalid Path");
							}
							imgSig.Visible = false;
                            signaturePanel.Visible = false;
                        }
                    }
					else
					{
						modGlobal.Statics.gboolUseSigFile = false;
						imgSig.Visible = false;
                        signaturePanel.Visible = false;
						if (FCMessageBox.Show("The password entered is invalid.  Would you like to try again?", MsgBoxStyle.Question | MsgBoxStyle.YesNo, "Invalid Password") == DialogResult.Yes)
						{
							goto COLLECTORTRYAGAIN;
						}
						else
						{
							chkUseSig.CheckState = Wisej.Web.CheckState.Unchecked;
							// this will set it opposite then it will be unset
							modGlobal.Statics.gboolUseSigFile = false;
							imgSig.Visible = false;
                            signaturePanel.Visible = false;
                        }
                    }
				}
				else
				{
					modGlobal.Statics.gboolUseSigFile = false;
					imgSig.Visible = false;
                    signaturePanel.Visible = false;
                }
                return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				chkUseSig.CheckState = Wisej.Web.CheckState.Unchecked;
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Password Error");
			}
		}

		private void frmReminderNotices_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				//if (!boolLoaded)
				//{
				//	boolLoaded = true;
				//	//FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				//	//this.Text = "Reminder Notices";
				//	FillStringArray();
				//	ShowAllFrames();
				//	//FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				//}
				//return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Loading Form");
			}
		}

		private void frmReminderNotices_Load(object sender, System.EventArgs e)
		{			
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsUpdate = new clsDRWrapper();
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
				modGlobalFunctions.SetTRIOColors(this);
                this.Text = "Reminder Notices";
                intLabelType = 0;
				frmWait.InstancePtr.Unload();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Loading Reminder Notices");
				Close();
			}
		}

		private void frmReminderNotices_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				Close();
			}
			else if (KeyAscii == 13)
			{
				KeyAscii = 0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		private void frmReminderNotices_Resize(object sender, System.EventArgs e)
        {           
            PositionBottomPanel();
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			string strSQL = "";
			int intType = 0;
			bool boolUseFont = false;
			bool blnHideDup = false;
			string strFont = "";
			string strPrinterName = "";
			int lngNumFonts = 0;
			int intCPI = 0;
			int lngCT;
			string strDesc = "";
			string strTemp = "";
			string strPayment = "";
			string strOldDefault = "";
			if (ValidateAnswers())
			{
                // validate the criteria selected by the user
                if (intAction != Outprint)
                {
                    if (cmbShowName.Text != "")
                    {
                        if (cmbShowName.Text.ToUpper() == "LAST")
                        { // trocls-119 Billed Owner at Last Recorded addr
                            lngNameType = 2; // trocls-147 04.16.2018 kjr, 5.21.18
                        }
                        else
                        {
                            if (Strings.Mid(cmbShowName.Text, 27, 3) == "C/O")
                            {
                                lngNameType = 1; // trocls-147 04.16.2018 kjr, 5.21.18
                            }
                            else
                            {
                                lngNameType = 0;
                            }
                        }
                    }
                    else
                    {
                        lngNameType = 0;
                    }
                }
                else
                {
                    lngNameType = 0;
                }

				if (chkGetPreviousYears.CheckState == Wisej.Web.CheckState.Checked)
				{
					strRateKeyList = GetAllPreviousRateKeys();
				}
				else
				{
					strRateKeyList = strRateKeyList;
				}
				// get the reminder period type
				//for (lngCT = 0; lngCT <= 5; lngCT++)
				//{
				//	if (cmbPeriod.SelectedIndex == lngCT)
				//	{
				//		intReminderType = lngCT;
				//		break;
				//	}
				//} 
                intReminderType = cmbPeriod.ItemData(cmbPeriod.SelectedIndex);
				// MAL@20071207: Check minimum amount
				if (Convert.ToDouble(txtMinimumAmount.Text) > 0)
				{
					dblMinimumAmount = Convert.ToDouble(FCConvert.ToDouble(txtMinimumAmount.Text));
				}
				else
				{
					dblMinimumAmount = 0;
				}
				// SHOW THE REPORT
				if (!boolUseFont)
					strFont = "";
				SaveDefaults(intAction);
				intType = intLabelType;
				strSQL = BuildSQL();
				switch (intAction)
				{
					case Labels:
						{
							// Labels
							if (cmbHideDupNames.Text.ToLower() == "yes")
							{
								blnHideDup = true;
							}
							else
							{
								blnHideDup = false;
							}
							App.DoEvents();
                            ReminderNoticeOptions options = new ReminderNoticeOptions
                            {
                                Query = strSQL,
                                RateKeyList = strRateKeyList,
                                AddressType = GetSelectedAddressType()
							};
                            var selectedLabelType =(DescriptionIDPair) cmbLabelType.SelectedItem;
                            if (selectedLabelType != null)
                            {
                                options.LabelType = selectedLabelType.ID;
                                rptReminderNoticeLabels.InstancePtr.Init(options);
                            }

                            break;
						}
					case PostCards:
						{
							// Post Cards
							App.DoEvents();
                            ReminderNoticeOptions options = new ReminderNoticeOptions
                            {
                                AddressType = GetSelectedAddressType(),
                                LienedRecords = boolLienedRecords,
                                Message = txtMessage.Text,
                                MinimumAmount = dblMinimumAmount,
                                Period = GetSelectedPeriod(),
                                Printer = strPrinterName,
                                Query = strSQL,
                                RateKeyList = strRateKeyList,
                                ReturnAddress1 = txtReturnAddress1.Text,
                                ReturnAddress2 = txtReturnAddress2.Text,
                                ReturnAddress3 = txtReturnAddress3.Text,
                                ReturnAddress4 = txtReturnAddress4.Text,
                                UseReturnAddress = cmbUseReturnAddress.Text.ToLower() == "yes"
                            };
                            rptReminderPostCard.InstancePtr.Init(options);
							break;
						}
					case FullPageForms:
						{
							// Forms
							strPassRNFORMSQL = strSQL;
							modRhymalReporting.Statics.strFreeReportType = "RNFORM";
							modCustomReport.Statics.strReportType = "RNFORM";
                            ReminderNoticeOptions options = new ReminderNoticeOptions
                            {
                                AddressType = GetSelectedAddressType(),
                                HighYear = strYear,
                                InterestDate = DateAndTime.DateValue(txtInterestDate.Text),
                                InterestDept = txtInterestDept.Text,
                                InterestDeptPhone = txtInterestDeptPhone.Text,
                                LastMaturedYear = cmbLastYearMatured.SelectedItem.ToString(),
                                LienedRecords = boolLienedRecords,
                                MailDate = DateAndTime.DateValue(txtMailDate.Text),
                                MaturedDept = txtMaturedYearDept.Text,
                                MaturedDeptPhone = txtMaturedYearDeptPhone.Text,
                                MinimumAmount = dblMinimumAmount,
                                Period = GetSelectedPeriod(),
                                Query = strSQL,
                                RateKeyList = strRateKeyList,
								ReportTitle = txtReportTitle.Text,
                                SignOut = txtLineBeforeSig.Text,
                                SignerName = txtFirstLineAfterSig.Text,
                                SignerTitle = txtSecondLineAfterSig.Text,
								PreviousYears = chkGetPreviousYears.Checked,
								ShowPastYears = chkShowPastDueAmounts.Checked
								
                            };
                            frmFreeReport.InstancePtr.NoticeOptions = options;
							frmFreeReport.InstancePtr.Show(App.MainForm);
							App.DoEvents();
							frmFreeReport.InstancePtr.Focus();
							break;
						}
					case Mailers:
						{
							// Mailers
							App.DoEvents();
                            ReminderNoticeOptions options = new ReminderNoticeOptions
                            {
                                AddressType = GetSelectedAddressType(),
                                HighYear = strYear,
                                LienedRecords = boolLienedRecords,
                                MailDate = DateAndTime.DateValue(txtMailDate.Text),
                                MinimumAmount = dblMinimumAmount,
                                Period = GetSelectedPeriod(),
                                Query = strSQL,
                                RateKeyList = strRateKeyList,
                                ReturnAddress1 = txtReturnAddress1.Text,
                                ReturnAddress2 = txtReturnAddress2.Text,
                                ReturnAddress3 = txtReturnAddress3.Text,
                                ReturnAddress4 = txtReturnAddress4.Text,
                                UseReturnAddress = cmbUseReturnAddress.Text.ToLower() == "yes"
                            };
    						frmReminderMailer.InstancePtr.Init(options);
							break;
						}
					case Outprint:
						{
							// Outprinting File
							App.DoEvents();
                            ReminderNoticeOptions options = new ReminderNoticeOptions
                            {
                                MinimumAmount = dblMinimumAmount,
                                Query = strSQL,
                                RateKeyList = strRateKeyList
                            };
							BuildOutPrintingFile(options);
							break;
						}
					case LaserPostCards:
						{
							App.DoEvents();
                            ReminderNoticeOptions options = new ReminderNoticeOptions
                            {
                                AddressType = GetSelectedAddressType(),
                                BulkMailing = FCConvert.CBool(chkBulkMailing.CheckState == Wisej.Web.CheckState.Checked),
                                LienedRecords = boolLienedRecords,
                                Message = txtMessage.Text,
                                MinimumAmount = dblMinimumAmount,
                                Period = GetSelectedPeriod(),
                                Printer = strPrinterName,
                                Query = strSQL,
                                RateKeyList = strRateKeyList,
                                ReturnAddress1 = txtReturnAddress1.Text,
                                ReturnAddress2 = txtReturnAddress2.Text,
                                ReturnAddress3 = txtReturnAddress3.Text,
                                ReturnAddress4 = txtReturnAddress4.Text,
                                UseReturnAddress = cmbUseReturnAddress.Text.ToLower() == "yes"
                            };
    						rptReminderLaserPostCard.InstancePtr.Init(options);
							break;
						}
				}
				//end switch
			}
		}

        private ReminderNoticeOptions.PeriodEnum GetSelectedPeriod()
        {
            switch (cmbPeriod.SelectedIndex)
            {
                case 0:
                    return ReminderNoticeOptions.PeriodEnum.Period1;
                    break;
                case 1:
                    return ReminderNoticeOptions.PeriodEnum.Period2;
                    break;
                case 2:
                    return ReminderNoticeOptions.PeriodEnum.Period3;
                    break;
                case 3:
                    return ReminderNoticeOptions.PeriodEnum.Period4;
                    break;
                case 4:
                    return ReminderNoticeOptions.PeriodEnum.Outstanding;
                    break;
                default:
                    return ReminderNoticeOptions.PeriodEnum.PastDueOnly;
                    break;
            }
        }

        private ReminderNoticeOptions.AddressEnum GetSelectedAddressType()
        {
            switch (cmbShowName.SelectedIndex)
            {
                case 0:
                    return ReminderNoticeOptions.AddressEnum.AddressAtBilling;
                    break;
                case 1:
                    return ReminderNoticeOptions.AddressEnum.CareOfCurrentOwner;
                    break;
                default:
                    return ReminderNoticeOptions.AddressEnum.LastKnownAddress;
                    break;
            }
        }

        private void optRN_Click(int Index, object sender, System.EventArgs e)
		{
			OptionChoice(Index);
		}

		private void optRN_Click(object sender, System.EventArgs e)
		{
            if (noticeTypeDictionary.TryGetValue(cmbRN.Text, out var noticeType))
            {
                optRN_Click(noticeType,sender,e);
            }
		}

        private void UpdatePeriodChoices()
        {
            var previousChoice = 0;
            if (cmbPeriod.SelectedIndex >= 0)
            {
                previousChoice = cmbPeriod.ItemData(cmbPeriod.SelectedIndex);
            }
            var optionChoice = 0;
            if (noticeTypeDictionary.TryGetValue(cmbRN.Text, out var noticeType))
            {
                optionChoice = noticeType;
            }
            cmbPeriod.Clear();
            for (int x = 0; x < numberOfPeriods; x++)
            {
                cmbPeriod.AddItem((x + 1).ToString());
                cmbPeriod.ItemData(x, x);
            }
            cmbPeriod.AddItem("Outstanding");
            cmbPeriod.ItemData(cmbPeriod.ListCount - 1, 4);
            if (optionChoice == FullPageForms)
            {
                cmbPeriod.AddItem("Past Due Only");
                cmbPeriod.ItemData(cmbPeriod.ListCount - 1, 5);
            }

            for (int x = 0; x < cmbPeriod.ListCount; x++)
            {
                if (cmbPeriod.ItemData(x) == previousChoice)
                {
                    cmbPeriod.SelectedIndex = 0;
                    return;
                }
            }

            cmbPeriod.SelectedIndex = 0;
        }

		private void OptionChoice(int intIndex)
		{
            // this will show all of the questions for the selection
            intAction = intIndex;
			switch (intIndex)
			{
				case Labels:
					{
						// Labels
						// this will show the options for labels in the grid
						FillGrid_2(0);
						chkBulkMailing.Enabled = false;
						chkUseSig.Enabled = false;
						chkUseTownLogo.Enabled = false;
						chkInterest.Enabled = false;
						chkShowPastDueAmounts.Enabled = false;
                        ReportTitlePanel.Visible = false;
						fraRN.Top = 145;
						//if (cmbPeriod.Items.Contains(auxItem5CmbPeriod))
						//{
						//	cmbPeriod.Items.Remove(auxItem5CmbPeriod);
						//}
						intLabelType = -1;
						break;
					}
				case PostCards:
				case LaserPostCards:
					{
						// Post Cards
						// this will show the options for the Post Cards in the grid
						FillGrid_2(1);
						chkBulkMailing.Enabled = true;
						chkUseSig.Enabled = false;
						chkUseTownLogo.Enabled = false;
						chkInterest.Enabled = false;
						chkShowPastDueAmounts.Enabled = false;
                        ReportTitlePanel.Visible = false;
						fraRN.Top = 145;
						//if (cmbPeriod.Items.Contains(auxItem5CmbPeriod))
						//{
						//	cmbPeriod.Items.Remove(auxItem5CmbPeriod);
						//}
						break;
					}
				case FullPageForms:
					{
						// Forms (Full Page All Years)
						// this will show the options for Reminder Notice Forms
						FillGrid_2(2);
						chkBulkMailing.Enabled = false;
						chkUseSig.Enabled = true;
						chkUseTownLogo.Enabled = true;
						chkInterest.Enabled = false;
						chkShowPastDueAmounts.Enabled = true;
						// If Trim(gstrCollectorSigPath) <> "" Then
						// chkUseSig.Enabled = True
						// Else
						// chkUseSig.Enabled = False
						// End If
						if (Strings.Trim(modGlobalConstants.Statics.gstrTownSealPath) != "")
						{
							chkUseTownLogo.Enabled = true;
						}
						else
						{
							chkUseTownLogo.Enabled = false;
						}
						ReportTitlePanel.Visible = true;
						fraRN.Top = 190;
						txtReportTitle.Text = "Past Due Reminder Statement";
						//if (!cmbPeriod.Items.Contains(auxItem5CmbPeriod))
						//{
						//	cmbPeriod.AddItem(auxItem5CmbPeriod);
						//}
						break;
					}
				case Mailers:
					{
						// Mailer
						FillGrid_2(3);
						chkBulkMailing.Enabled = true;
						chkUseSig.Enabled = false;
						chkUseTownLogo.Enabled = false;
						chkInterest.Enabled = true;
						chkShowPastDueAmounts.Enabled = false;
						ReportTitlePanel.Visible = false;
						fraRN.Top = 145;
						//if (cmbPeriod.Items.Contains(auxItem5CmbPeriod))
						//{
						//	cmbPeriod.Items.Remove(auxItem5CmbPeriod);
						//}
						break;
					}
				case Outprint:
					{
						// Outprinting File
						FillGrid_2(4);
						chkBulkMailing.Enabled = false;
						chkUseSig.Enabled = false;
						chkUseTownLogo.Enabled = false;
						chkInterest.Enabled = false;
						chkShowPastDueAmounts.Enabled = true;
						ReportTitlePanel.Visible = false;
						fraRN.Top = 145;
						//if (cmbPeriod.Items.Contains(auxItem5CmbPeriod))
						//{
						//	cmbPeriod.Items.Remove(auxItem5CmbPeriod);
						//}
						break;
					}
			}
            UpdatePeriodChoices();
			//end switch
			//FC:FINAL:AM:#i266 - call here ChangeEdit
		}

		private void BuildOutPrintingFile(ReminderNoticeOptions options)
		{
            //       BuildOutPrintingFile(ref string strSQL, double dblMinimumAmount)
            string strSQL = options.Query;
            double dblMinimumAmount = options.MinimumAmount;
            StreamWriter ts = null;

			try
            {
				// On Error GoTo ERROR_HANDLER
				// this will create a file with the reminder notice information
				bool boolOK;
				clsDRWrapper rsFile = new clsDRWrapper();
				clsDRWrapper rsRE = new clsDRWrapper();
				clsDRWrapper rsLien = new clsDRWrapper();
				string strBuildString = "";
				// this is the string that will be built and placed as one record in the file
				double dblCurrentDue = 0;
				// this is the total tax due up to this period
				double dblPeriodDue = 0;
				// this is the tax that will be due next period
				double dblCurrentPaid;
				// total Principal Paid
				int lngLandVal = 0;
				// Land Value
				int lngBuildingVal = 0;
				// Building Value
				int lngExemptVal = 0;
				// Exemption Value
				string strLocation = "";
				// Location string that will be built
				string strPayment = "";
				// this is set to the value of the payment field in the grid "First", "Second", "Third" or "Fourth"
				clsDRWrapper rsTemp = new clsDRWrapper();
				int intPeriod;
				double dblInterestDue/*unused?*/;
				double dblTotalDue = 0;
				double dblTotalAccountDue = 0;
				string strType = "";
				DateTime dtChargeInterestUntil;
				double dblCurInterest = 0;
				string strFields = "";
                string str1 = "";
                string str2 = "";
                string str3 = "";
                string str4 = "";
                string str5 = "";
                string str6 = "";
                string str7 = "";
                int lngAcct;
                bool boolNewOwner; // trocls-119 10.6.17 kjr
                DateTime dtBillDate;
                bool boolSameName;
                bool boolREMatch;
                bool boolNewAddress;
                string strCOName = "";
                string strPrevSecOwnerName = "";
                string strPrevAddress1 = "";
                string strPrevAddress2 = "";
                string strPrevAddress3 = "";
                string strPrevAddrCity = "";
                string strPrevAddrState = "";
                string strPrevAddrZip = "";
                string strPrevAddrZip4 = "";
                string strSecOwnerName = "";
                string strSO1 = ""; // Second Owner Address  trocls-139 1.9.18 kjr
                string strSO2 = "";
                string strSO3 = "";
                string strSO4 = "";
                string strSO5 = "";
                string strSO6 = "";
                string strSO7 = "";
                string strDeedName1 = "";
                string strDeedName2 = "";

                cPartyController pCont = new cPartyController();
                cParty pInfo = new cParty();
                cPartyAddress pAdd;
                clsDRWrapper rsMastOwner = new clsDRWrapper();
                cParty pInfo2 = new cParty();
                cPartyAddress pAdd2;
                
				boolOK = true;
				// get the interest date for all of the accounts
                dtChargeInterestUntil = DateAndTime.DateValue(txtInterestDate.Text);
				// strPayment = vsGrid.TextMatrix(4, 1)            'this is the payment line of the outprinting grid
				for (intPeriod = 0; intPeriod <= 5; intPeriod++)
				{
					if (cmbPeriod.SelectedIndex == intPeriod)
					{
						break;
					}
				}
				string tempFileName = Path.Combine(FCFileSystem.Statics.UserDataFolder, "Temp", Path.GetRandomFileName());
				tempFileName = Path.ChangeExtension(tempFileName, "txt");
				if (!Directory.Exists(Path.GetDirectoryName(tempFileName)))
				{
					Directory.CreateDirectory(Path.GetDirectoryName(tempFileName));
				}

				//FC:FINAL:SBE - #i337 - save file to temp folder, and download to client
				//if (App.MainForm.CommonDialog1.FileName != "")
				if (tempFileName != "")
				{
					rsFile.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
					if (rsFile.RecordCount() > 0)
					{
						frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Saving Records", true, rsFile.RecordCount(), true);
						// open the file
						//FC:FINAL:SBE - #i337 - save file to temp folder, and download to client
						//FCFileSystem.FileOpen(1, App.MainForm.CommonDialog1.FileName, OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
                        ts = new StreamWriter(tempFileName);
						while (!rsFile.EndOfFile())
						{
							App.DoEvents();
							frmWait.InstancePtr.IncrementProgress();
							strBuildString = "";
							// start with a clean string
                            if (modStatusPayments.Statics.boolRE)
                            {
                                strFields = "Account,Name1,Name2,BillingYear,RateKey,LienRecordNumber,InterestAppliedThroughDate," + "TaxDue1,TaxDue2,TaxDue3,TaxDue4,PrincipalPaid,InterestCharged,InterestPaid,DemandFees,DemandFeesPaid," + "OwnerPartyID,SecOwnerPartyID,RSMapLot,RRLandVal,RLBLDGVal,RLExemption,RSLocNumAlph,RSLocAPT,RSLocStreet," + "BillingMaster.Address1,BillingMaster.Address2,BillingMaster.Address3,TransferFromBillingDateFirst,TransferFromBillingDateLast";
                                rsTemp.OpenRecordset("SELECT * FROM BillingMaster INNER JOIN " + modGlobal.Statics.strDbRE + "Master ON BillingMaster.Account = Master.RSAccount WHERE BillingType = 'RE' AND BillingMaster.ID = " + rsFile.Get_Fields("ID"), modExtraModules.strCLDatabase);
                            }
                            else
                            {
                                strFields = "BillingMaster.Account,Name1,Name2,BillingYear,RateKey,LienRecordNumber,InterestAppliedThroughDate," + "TaxDue1,TaxDue2,TaxDue3,TaxDue4,PrincipalPaid,InterestCharged,InterestPaid,DemandFees,DemandFeesPaid," + "PartyID,Street,PPMaster.StreetNumber," + "BillingMaster.Address1,BillingMaster.Address2,BillingMaster.Address3,TransferFromBillingDateFirst,TransferFromBillingDateLast";
                                rsTemp.OpenRecordset("SELECT * FROM BillingMaster INNER JOIN " + modGlobal.Statics.strDbPP + "PPMaster ON BillingMaster.Account = PPMaster.Account WHERE BillingType = 'PP' AND BillingMaster.ID = " + rsFile.Get_Fields("ID"), modExtraModules.strCLDatabase);
                            }
                            if (!rsTemp.EndOfFile())
							{
								// MAL@20071207: Check for minimum amount
								if ((Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue4")) - Conversion.Val(rsTemp.Get_Fields_Decimal("PrincipalPaid"))) >= dblMinimumAmount)
								{
									// set all of the variables
									switch (intPeriod)
									{
									// UCase(Left$(strPayment, 2))
									// this will find all of the taxes due prior to the upcoming period and also how much is due for the next period
										case 1:
											{
												// "FI"
												dblCurrentDue = 0;
												// rsTemp.Get_Fields("TaxDue1")
												dblPeriodDue = Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue1"));
												break;
											}
										case 2:
											{
												// "SE"
												dblCurrentDue = Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue1"));
												// + rsTemp.Get_Fields("TaxDue2")
												dblPeriodDue = Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue2"));
												break;
											}
										case 3:
											{
												// "TH"
												dblCurrentDue = Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue2"));
												// + rsTemp.Get_Fields("TaxDue3")
												dblPeriodDue = Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue3"));
												break;
											}
										case 4:
											{
												// "FO"
												dblCurrentDue = Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue3"));
												// + .Get_Fields("TaxDue4")
												dblPeriodDue = Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue4"));
												break;
											}
									}
									//end switch
									// set the values
									dblCurrentPaid = Conversion.Val(rsTemp.Get_Fields_Decimal("PrincipalPaid"));
									if (modStatusPayments.Statics.boolRE)
									{
										lngLandVal = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("RLLandVal"));
										lngBuildingVal = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("RLBLDGVal"));
										lngExemptVal = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("RLExemption"));
									}

                                    str1 = fecherFoundation.Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("Name1")));

                                    // get the mailing address
                                    // If (Trim(.Fields("Address1")) = "" And Trim(.Fields("Address2")) = "") Or frmReminderNotices.lngNameType = 1 Or (boolSameName And boolREMatch) Then

                                    // this will fill the address from the RE or PP DB for the mail address

                                    // We used to get the owner info in inner join that was slow.  I moved that down to here to speed it up.  We now use party classes to get infor to use in labels
                                    if (modStatusPayments.Statics.boolRE)
                                    {
                                        rsMastOwner.OpenRecordset("Select OwnerPartyID as OwnerID, SecOwnerPartyID as SecOwnerID,DeedName1,DeedName2 FROM Master WHERE RSAccount = " + rsFile.Get_Fields("Account"), modExtraModules.strREDatabase);
                                    }
                                    else
                                    {
                                        rsMastOwner.OpenRecordset("Select PartyID as OwnerID FROM PPMaster WHERE Account = " + rsFile.Get_Fields("Account"), modExtraModules.strPPDatabase);
                                    }

                                    // If we find the master record in PP or RE then we try to get party info
                                    if (rsMastOwner.EndOfFile() != true && rsMastOwner.BeginningOfFile() != true)
                                    {
                                        pInfo = pCont.GetParty(rsMastOwner.Get_Fields("OwnerID")); // kk07152015 trocls-58 .Fields("OwnerPartyID")
                                        if (modStatusPayments.Statics.boolRE)
                                        {
                                            pInfo2 = pCont.GetParty(rsMastOwner.Get_Fields("SecOwnerID")); // trocls-139 1.9.18 kjr
                                            strDeedName1 = rsMastOwner.Get_Fields_String("DeedName1");
                                            strDeedName2 = rsMastOwner.Get_Fields_String("DeedName2");
                                        }
                                        else
                                        {
                                            strDeedName2 = "";
                                            strDeedName1 = pInfo.FullNameLastFirst;
                                        }
                                    }

                                    // If we find part info then we attempt to get address info
                                    if (!(pInfo == null))
                                    {
                                        pAdd = pInfo.GetAddress("CL", rsMastOwner.Get_Fields("OwnerID")); // kk07152015 trocls-58 .Fields("OwnerPartyID")

                                        if (rsTemp.Get_Fields("Name1") != strDeedName1)
                                        {
                                            strCOName = @"C\O " + strDeedName1;
                                        }
                                        else
                                        {
                                            strCOName = "";
                                        }

                                        if (!(pAdd == null))
                                        {
                                            str2 = fecherFoundation.Strings.Trim(pAdd.Address1);
                                            str3 = fecherFoundation.Strings.Trim(pAdd.Address2);
                                            str4 = fecherFoundation.Strings.Trim(pAdd.City);
                                            str5 = fecherFoundation.Strings.Trim(pAdd.State);
                                            str6 = fecherFoundation.Strings.Trim(pAdd.Zip);
                                            str7 = "";
                                        }
                                        else
                                        {
                                            str2 = "";
                                            str3 = "";
                                            str4 = "";
                                            str5 = "";
                                            str6 = "";
                                            str7 = "";
                                        }
                                    }

                                    if (modStatusPayments.Statics.boolRE)
                                    {
                                        if (!(pInfo2 == null))
                                        { // trocls-139 1.9.18 kjr
                                            strSecOwnerName = strDeedName2.Trim();
                                            pAdd2 = pInfo2.GetAddress("CL", rsMastOwner.Get_Fields("SecOwnerID")); // kk07152015 trocls-58 .Fields("OwnerPartyID")
                                            if (!(pAdd2 == null))
                                            {
                                                strSO2 = fecherFoundation.Strings.Trim(pAdd2.Address1);
                                                strSO3 = fecherFoundation.Strings.Trim(pAdd2.Address2);
                                                strSO4 = fecherFoundation.Strings.Trim(pAdd2.City);
                                                strSO5 = fecherFoundation.Strings.Trim(pAdd2.State);
                                                strSO6 = fecherFoundation.Strings.Trim(pAdd2.Zip);
                                                strSO7 = "";
                                            }
                                            else
                                            {
                                                strSO2 = "";
                                                strSO3 = "";
                                                strSO4 = "";
                                                strSO5 = "";
                                                strSO6 = "";
                                                strSO7 = "";
                                            }
                                        }
                                    }


                                    if (modStatusPayments.Statics.boolRE)
									{
										// create the location string
										if (Conversion.Val(FCConvert.ToString(rsTemp.Get_Fields_String("RSLocNumAlph"))) != 0)
										{
											if (FCConvert.ToString(rsTemp.Get_Fields_String("RSLocAPT")) != "")
											{
												strLocation = FCConvert.ToString(Conversion.Val(FCConvert.ToString(rsTemp.Get_Fields_String("RSLocNumAlph")))) + " " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("RSLocAPT"))) + " " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("RSLocStreet")));
											}
											else
											{
												strLocation = FCConvert.ToString(Conversion.Val(FCConvert.ToString(rsTemp.Get_Fields_String("RSLocNumAlph")))) + " " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("RSLocStreet")));
											}
										}
										else
										{
											strLocation = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("RSLocStreet")));
										}
										// Create the string by filling in the information
										// Size of field - Name of field
										// 6  - Account
										// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
										strBuildString = modGlobal.PadToString(rsTemp.Get_Fields("Account"), 6);
                                        // 38 - RE Name
                                        if (strCOName != "")
                                        { // 5.22.18
                                            strBuildString += FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces( strCOName, 38, false));
                                        }
                                        else
                                        {
                                            strBuildString += FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces("", 38, false));
                                        }
                                        // 34 - Addr1
                                        strBuildString += modGlobalFunctions.PadStringWithSpaces(str2, 34, false);
                                        // 34 - Addr2
                                        // TODO Get_Fields: Field [Own1Address2] not found!! (maybe it is an alias?)
                                        strBuildString += modGlobalFunctions.PadStringWithSpaces(str3, 34, false);
										// 24 - Addr3
										// TODO Get_Fields: Field [Own1City] not found!! (maybe it is an alias?)
										strBuildString += modGlobalFunctions.PadStringWithSpaces(str4, 24, false);
										// 2  - State
										// TODO Get_Fields: Field [Own1State] not found!! (maybe it is an alias?)
										strBuildString += modGlobalFunctions.PadStringWithSpaces(str5, 2, false);
										// 5  - Zip
										// TODO Get_Fields: Field [Own1Zip] not found!! (maybe it is an alias?)
										strBuildString += modGlobalFunctions.PadStringWithSpaces(str6, 5, false);
                                        if (fecherFoundation.Strings.Trim(str7) != "")
                                        {
                                            strBuildString += modGlobalFunctions.PadStringWithSpaces("-" + str7, 5, false);
                                        }
                                        else
                                        {
                                            strBuildString += modGlobalFunctions.PadStringWithSpaces("", 5);
                                        }
                                        if (((rsTemp.Get_Fields_Int32("LienRecordNumber"))) != 0)
										{
											strType = "L";
											rsLien.OpenRecordset("SELECT * FROM LienRec WHERE Id = " + FCConvert.ToString(rsTemp.Get_Fields_Int32("LienRecordNumber")));
											if (!rsLien.EndOfFile())
											{
												dblCurInterest = 0;
												dblTotalDue = modCLCalculations.CalculateAccountCLLien(rsLien, dtChargeInterestUntil, ref dblCurInterest);
											}
											else
											{
												dblTotalDue = 0;
												dblCurInterest = 0;
											}
										}
										else
										{
											strType = "R";
											dblCurInterest = 0;
											// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
											dblTotalDue = modCLCalculations.CalculateAccountCL(ref rsTemp, FCConvert.ToInt32(rsTemp.Get_Fields("Account")), dtChargeInterestUntil, ref dblCurInterest);
										}
									}
									else
									{
										// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
										if (Conversion.Val(FCConvert.ToString(rsTemp.Get_Fields("StreetNumber"))) != 0)
										{
											if (FCConvert.ToString(rsTemp.Get_Fields_String("Street")) != "")
											{
												// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
												strLocation = FCConvert.ToString(Conversion.Val(FCConvert.ToString(rsTemp.Get_Fields("StreetNumber")))) + " " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Street")));
											}
											else
											{
												strLocation = "";
											}
										}
										else
										{
											strLocation = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Street")));
										}
										// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
										strBuildString = modGlobal.PadToString(rsTemp.Get_Fields("Account"), 6);
                                        // 38 - RE Name
                                        if (strCOName != "")
                                        { // 5.22.18
                                            strBuildString += FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces( strCOName, 38, false));
                                        }
                                        else
                                        {
                                            strBuildString += FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces("", 38, false));
                                        }

                                        // 34 - Addr1
                                        strBuildString += FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces( str2, 34, false));

                                        // 34 - Addr2
                                        strBuildString += FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces( str3, 34, false));

                                        // 24 - Addr3
                                        strBuildString += FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces( str4, 24, false));

                                        // 2  - State
                                        strBuildString += FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces( str5, 2, false));

                                        // 5  - Zip
                                        strBuildString += FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces( str6, 5, false));

                                        if (fecherFoundation.Strings.Trim(str7) != "")
                                        {
                                            strBuildString += FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces("-" + str7, 5, false));
                                        }
                                        else
                                        {
                                            strBuildString += FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces("", 5));
                                        }
                                        strType = "P";
										dblCurInterest = 0;
										// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
										dblTotalDue = modCLCalculations.CalculateAccountCL(ref rsTemp, FCConvert.ToInt32(rsTemp.Get_Fields("Account")), dtChargeInterestUntil, ref dblCurInterest, true);
									}
									// 34 - Name
									strBuildString += modGlobalFunctions.PadStringWithSpaces(str1, 34, false);
									// 12 - Land Val
									strBuildString += modGlobalFunctions.PadStringWithSpaces(lngLandVal.ToString(), 12);
									// 12 - Building Val
									strBuildString += modGlobalFunctions.PadStringWithSpaces(lngBuildingVal.ToString(), 12);
									// 12 - Land + Building
									strBuildString += modGlobalFunctions.PadStringWithSpaces((lngBuildingVal + lngLandVal).ToString(), 12);
									// 12 - Exempt Val
									strBuildString += modGlobalFunctions.PadStringWithSpaces(lngExemptVal.ToString(), 12);
									// 12 - Land + Building - Exempt
									strBuildString += modGlobalFunctions.PadStringWithSpaces(FCConvert.ToString((lngBuildingVal + lngLandVal - lngExemptVal)), 12);
									// 31 - Location
									strBuildString += modGlobalFunctions.PadStringWithSpaces(Strings.Trim(strLocation), 31, false);
									// 17 - Map Lot
									if (modStatusPayments.Statics.boolRE)
									{
										strBuildString += modGlobalFunctions.PadStringWithSpaces(Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("RSMapLot"))), 17, false);
									}
									else
									{
										strBuildString += modGlobalFunctions.PadStringWithSpaces("", 17, false);
									}
									// 10 - Interest Date
									strBuildString += modGlobalFunctions.PadStringWithSpaces(Strings.Format(dtChargeInterestUntil, "MM/dd/yyyy"), 10);
									// 15 - Past Due
									strBuildString += modGlobalFunctions.PadStringWithSpaces(Strings.Format(dblCurrentDue, "####0.00"), 15);
									// 15 - Amount Coming Due
									strBuildString += modGlobalFunctions.PadStringWithSpaces(Strings.Format(dblPeriodDue, "####0.00"), 15);
									// 15 - Interest and Costs Due
									if (strType == "L")
									{
										if (dblTotalDue - Conversion.Val(rsTemp.Get_Fields_Decimal("PrincipalPaid")) - (Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue4"))) > 0)
										{
											strBuildString += modGlobalFunctions.PadStringWithSpaces(Strings.Format(dblTotalDue - (Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue4"))), "####0.00"), 15);
										}
										else
										{
											strBuildString += modGlobalFunctions.PadStringWithSpaces("0.00", 15, true);
										}
									}
									else
									{
										if ((dblTotalDue + Conversion.Val(rsTemp.Get_Fields_Decimal("PrincipalPaid"))) - (Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue4"))) > 0)
										{
											strBuildString += modGlobalFunctions.PadStringWithSpaces(Strings.Format((dblTotalDue + Conversion.Val(rsTemp.Get_Fields_Decimal("PrincipalPaid"))) - (Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue4"))), "####0.00"), 15);
										}
										else
										{
											strBuildString += modGlobalFunctions.PadStringWithSpaces("0.00", 15, true);
										}
									}
									// 15 - Total Due
									strBuildString += modGlobalFunctions.PadStringWithSpaces(Strings.Format(dblTotalDue, "####0.00"), 15);
									// 15 - Total Account Due
									if (modStatusPayments.Statics.boolRE)
									{
										// MAL@20080305: Change to include the interest date
										// Tracker Reference: 12613
										// dblTotalAccountDue = CalculateAccountTotal(rsTemp.Get_Fields("RSAccount"), boolRE)
										// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
										dblTotalAccountDue = modCLCalculations.CalculateAccountTotal(FCConvert.ToInt32(rsTemp.Get_Fields("Account")), modStatusPayments.Statics.boolRE, false, dtChargeInterestUntil);
									}
									else
									{
										// MAL@20080320: Personal Property Fix for Interest Date
										// Tracker Reference: 12867
										// dblTotalAccountDue = CalculateAccountTotal(rsTemp.Get_Fields("PPMaster.Account"), boolRE)
										// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
										dblTotalAccountDue = modCLCalculations.CalculateAccountTotal(FCConvert.ToInt32(rsTemp.Get_Fields("Account")), modStatusPayments.Statics.boolRE, false, dtChargeInterestUntil);
									}
									strBuildString += modGlobalFunctions.PadStringWithSpaces(Strings.Format(dblTotalAccountDue, "####0.00"), 15);
									// 1 - Type
									strBuildString += modGlobalFunctions.PadStringWithSpaces(strType, 1);
                                    if (modStatusPayments.Statics.boolRE && strCOName != "")
                                    {
                                        // 38 - Second Owner Name     trocls-139 1.9.18 kjr - 2nd owner name & addr
                                        strBuildString += modGlobalFunctions.PadStringWithSpaces( strSecOwnerName, 38, false);

                                        // 34 - Addr1
                                        strBuildString += modGlobalFunctions.PadStringWithSpaces( strSO2, 34, false);

                                        // 34 - Addr2
                                        strBuildString += modGlobalFunctions.PadStringWithSpaces( strSO3, 34, false);

                                        // 24 - Addr3
                                        strBuildString += modGlobalFunctions.PadStringWithSpaces( strSO4, 24, false);

                                        // 2  - State
                                        strBuildString += modGlobalFunctions.PadStringWithSpaces(strSO5, 2, false);

                                        // 5  - Zip
                                        strBuildString += modGlobalFunctions.PadStringWithSpaces( strSO6, 5, false);
                                    }
                                    else
                                    {
                                        strBuildString += modGlobalFunctions.PadStringWithSpaces("", 137, false);
                                    }
                                    if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("Name2"))) != "")
                                    { // trocls-139  6.4.18
                                        strBuildString += FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces(fecherFoundation.Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("Name2"))), 38, false));
                                    }
                                    else
                                    {
                                        strBuildString += FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces("", 38, false));
                                    }
                                    ts.WriteLine(strBuildString);
								}
							}
							rsFile.MoveNext();
						}
                        ts.Close();
						boolOK = true;
						if (boolOK)
						{
							frmWait.InstancePtr.Unload();
							//FC:FINAL:SBE - #i337 - save file to temp folder, and download to client
							//FCMessageBox.Show(App.MainForm.CommonDialog1.FileName + " was saved successfully.", MsgBoxStyle.Information, "Save Complete");
							FCUtils.Download(tempFileName, "ReminderNotices.txt");
							FCMessageBox.Show("File was saved successfully.", MsgBoxStyle.Information, "Save Complete");
							frmReportViewer.InstancePtr.Init(rptOutprintingFormat.InstancePtr);
							// this will show the format of the file that was printed
						}
					}
					else
					{
						FCMessageBox.Show("No eligible records found.", MsgBoxStyle.Information, "No Records");
					}
				}
				else
				{
					FCMessageBox.Show("Please name the file to save.", MsgBoxStyle.Information, "Save Complete");
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
                ts.Close();
				frmWait.InstancePtr.Unload();
				if (Information.Err(ex).Number == 32755)
				{
					// cancel was selected
				}
				else
				{
					FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Out Printing Error");
				}
			}
		}

		private string BuildSQL()
		{
			string BuildSQL = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strTemp;
				string strMain;
				string strOrderBy = "";
				strTemp = "";
				strMain = "";
				// check all of the questions and
				switch (intAction)
				{
					case Labels:
						{
							// 3 - Name
							if (Strings.Trim(txtNameStart.Text) != "" || Strings.Trim(txtNameEnd.Text) != "")
							{
								// one of the fields has data in it
								if ((Strings.Trim(txtNameStart.Text) != "" && Strings.Trim(txtNameEnd.Text) == "") || (Strings.Trim(txtNameStart.Text) == Strings.Trim(txtNameEnd.Text)))
								{
									// if the first field has data and the second does not OR they both have the same thing that is not ""
									// strTemp = strTemp & " AND Name1 = '" & Trim(vsGrid.TextMatrix(3, 1)) & "'"
									strTemp += " AND Name1 > '" + Strings.Trim(txtNameStart.Text) + "     ' AND Name1 < '" + Strings.Trim(txtNameStart.Text) + "zzzzz'";
								}
								else if (Strings.Trim(txtNameEnd.Text) != "" && Strings.Trim(txtNameStart.Text) == "")
								{
									// if only the second has data
									strTemp += " AND Name1 = '" + Strings.Trim(txtNameEnd.Text) + "'";
								}
								else if (Strings.Trim(txtNameStart.Text) != "" || Strings.Trim(txtNameEnd.Text) == "" || (Strings.Trim(txtNameStart.Text) == Strings.Trim(txtNameEnd.Text) && Strings.Trim(txtNameStart.Text) != ""))
								{
									// both fields have different data
									strTemp += " AND Name1 > '" + Strings.Trim(txtNameStart.Text) + "     ' AND Name1 < '" + Strings.Trim(txtNameEnd.Text) + "zzzzz'";
								}
							}
							// 4 - Account
							if (Strings.Trim(txtAccountStart.Text) != "" || Strings.Trim(txtAccountEnd.Text) != "")
							{
								// one of the fields has data in it
								if ((Strings.Trim(txtAccountStart.Text) != "" && Strings.Trim(txtAccountEnd.Text) == "") || (Strings.Trim(txtAccountStart.Text) == Strings.Trim(txtAccountEnd.Text)))
								{
									// if the first field has data and the second does not OR they both have the same thing that is not ""
									strTemp += " AND Account = " + Strings.Trim(txtAccountStart.Text);
								}
								else if (Strings.Trim(txtAccountEnd.Text) != "" && Strings.Trim(txtAccountStart.Text) == "")
								{
									// if only the second has data
									strTemp += " AND Account = " + Strings.Trim(txtAccountEnd.Text);
								}
								else if ((Strings.Trim(txtAccountStart.Text) != "" || Strings.Trim(txtAccountEnd.Text) == "") || (Strings.Trim(txtAccountStart.Text) == Strings.Trim(txtAccountEnd.Text) && Strings.Trim(txtAccountStart.Text) != ""))
								{
									// both fields have different data
									strTemp += " AND Account >= " + Strings.Trim(txtAccountStart.Text) + " AND Account <= " + Strings.Trim(txtAccountEnd.Text);
								}
							}
							break;
						}
					case FullPageForms:
						{
							// Name
							if (Strings.Trim(txtNameStart.Text) != "" || Strings.Trim(txtNameEnd.Text) != "")
							{
								// one of the fields has data in it
								if ((Strings.Trim(txtNameStart.Text) != "" && Strings.Trim(txtNameEnd.Text) == "") || (Strings.Trim(txtNameStart.Text) == Strings.Trim(txtNameEnd.Text)))
								{
									// if the first field has data and the second does not OR they both have the same thing that is not ""
									// strTemp = strTemp & " AND Name1 = '" & Trim(vsGrid.TextMatrix(1, 1)) & "'"
									strTemp += " AND Name1 > '" + Strings.Trim(txtNameStart.Text) + "     ' AND Name1 < '" + Strings.Trim(txtNameStart.Text) + "zzzzz'";
								}
								else if (Strings.Trim(txtNameEnd.Text) != "" && Strings.Trim(txtNameStart.Text) == "")
								{
									// if only the second has data
									strTemp += " AND Name1 = '" + Strings.Trim(txtNameEnd.Text) + "'";
								}
								else if ((Strings.Trim(txtNameStart.Text) != "" || Strings.Trim(txtNameEnd.Text) == "") || (Strings.Trim(txtNameStart.Text) == Strings.Trim(txtNameEnd.Text) && Strings.Trim(txtNameStart.Text) != ""))
								{
									// both fields have different data
									strTemp += " AND Name1 > '" + Strings.Trim(txtNameStart.Text) + "     ' AND Name1 < '" + Strings.Trim(txtNameEnd.Text) + "zzzzz'";
								}
							}
							// Account
							if (Strings.Trim(txtAccountStart.Text) != "" || Strings.Trim(txtAccountEnd.Text) != "")
							{
								// one of the fields has data in it
								if ((Strings.Trim(txtAccountStart.Text) != "" && Strings.Trim(txtAccountEnd.Text) == "") || (Strings.Trim(txtAccountStart.Text) == Strings.Trim(txtAccountEnd.Text)))
								{
									// if the first field has data and the second does not OR they both have the same thing that is not ""
									strTemp += " AND Account = " + Strings.Trim(txtAccountStart.Text);
								}
								else if (Strings.Trim(txtAccountEnd.Text) != "" && Strings.Trim(txtAccountStart.Text) == "")
								{
									// if only the second has data
									strTemp += " AND Account = " + Strings.Trim(txtAccountEnd.Text);
								}
								else if ((Strings.Trim(txtAccountStart.Text) != "" || Strings.Trim(txtAccountEnd.Text) == "") || (Strings.Trim(txtAccountStart.Text) == Strings.Trim(txtAccountEnd.Text) && Strings.Trim(txtAccountStart.Text) != ""))
								{
									// both fields have different data
									strTemp += " AND Account >= " + Strings.Trim(txtAccountStart.Text) + " AND Account <= " + Strings.Trim(txtAccountEnd.Text);
								}
							}
							break;
						}
					case PostCards:
					case Mailers:
					case LaserPostCards:
						{
							// 2 - Name
							if (Strings.Trim(txtNameStart.Text) != "" || Strings.Trim(txtNameEnd.Text) != "")
							{
								// one of the fields has data in it
								if ((Strings.Trim(txtNameStart.Text) != "" && Strings.Trim(txtNameEnd.Text) == "") || (Strings.Trim(txtNameStart.Text) == Strings.Trim(txtNameEnd.Text)))
								{
									// if the first field has data and the second does not OR they both have the same thing that is not ""
									// strTemp = strTemp & " AND Name1 = '" & Trim(vsGrid.TextMatrix(2, 1)) & "'"
									strTemp += " AND Name1 > '" + Strings.Trim(txtNameStart.Text) + "     ' AND Name1 < '" + Strings.Trim(txtNameStart.Text) + "zzzzz'";
								}
								else if (Strings.Trim(txtNameEnd.Text) != "" && Strings.Trim(txtNameStart.Text) == "")
								{
									// if only the second has data
									strTemp += " AND Name1 = '" + Strings.Trim(txtNameEnd.Text) + "'";
								}
								else if ((Strings.Trim(txtNameStart.Text) != "" || Strings.Trim(txtNameEnd.Text) == "") || (Strings.Trim(txtNameStart.Text) == Strings.Trim(txtNameEnd.Text) && Strings.Trim(txtNameStart.Text) != ""))
								{
									// both fields have different data
									strTemp += " AND Name1 > '" + Strings.Trim(txtNameStart.Text) + "     ' AND Name1 < '" + Strings.Trim(txtNameEnd.Text) + "zzzzz'";
								}
							}
							// 3 - Account
							if (Strings.Trim(txtAccountStart.Text) != "" || Strings.Trim(txtAccountEnd.Text) != "")
							{
								// one of the fields has data in it
								if ((Strings.Trim(txtAccountStart.Text) != "" && Strings.Trim(txtAccountEnd.Text) == "") || (Strings.Trim(txtAccountStart.Text) == Strings.Trim(txtAccountEnd.Text)))
								{
									// if the first field has data and the second does not OR they both have the same thing that is not ""
									strTemp += " AND Account = " + Strings.Trim(txtAccountStart.Text);
								}
								else if (Strings.Trim(txtAccountEnd.Text) != "" && Strings.Trim(txtAccountStart.Text) == "")
								{
									// if only the second has data
									strTemp += " AND Account = " + Strings.Trim(txtAccountEnd.Text);
								}
								else if ((Strings.Trim(txtAccountStart.Text) != "" || Strings.Trim(txtAccountEnd.Text) == "") || (Strings.Trim(txtAccountStart.Text) == Strings.Trim(txtAccountEnd.Text) && Strings.Trim(txtAccountStart.Text) != ""))
								{
									// both fields have different data
									strTemp += " AND Account >= " + Strings.Trim(txtAccountStart.Text) + " AND Account <= " + Strings.Trim(txtAccountEnd.Text);
								}
							}
							break;
						}
					case Outprint:
						{
							if (Strings.Trim(txtNameStart.Text) != "" || Strings.Trim(txtNameEnd.Text) != "")
							{
								// one of the fields has data in it
								if ((Strings.Trim(txtNameStart.Text) != "" && Strings.Trim(txtNameEnd.Text) == "") || (Strings.Trim(txtNameStart.Text) == Strings.Trim(txtNameEnd.Text)))
								{
									// if the first field has data and the second does not OR they both have the same thing
									// strTemp = strTemp & " AND Name1 = '" & Trim(vsGrid.TextMatrix(2, 1)) & "'"
									strTemp += " AND Name1 > '" + Strings.Trim(txtNameStart.Text) + "     ' AND Name1 < '" + Strings.Trim(txtNameStart.Text) + "zzzzz'";
								}
								else if (Strings.Trim(txtNameEnd.Text) != "" && Strings.Trim(txtNameStart.Text) == "")
								{
									// if only the second has data
									// strTemp = strTemp & " AND Name1 = '" & Trim(vsGrid.TextMatrix(2, 2)) & "'"
									strTemp += " AND Name1 > '" + Strings.Trim(txtNameStart.Text) + "     ' AND Name1 < '" + Strings.Trim(txtNameStart.Text) + "zzzzz'";
								}
								else if ((Strings.Trim(txtNameStart.Text) != "" || Strings.Trim(txtNameEnd.Text) == "") || (Strings.Trim(txtNameStart.Text) == Strings.Trim(txtNameEnd.Text) && Strings.Trim(txtNameStart.Text) != ""))
								{
									// both fields have different data
									strTemp += " AND Name1 > '" + Strings.Trim(txtNameStart.Text) + "     ' AND Name1 < '" + Strings.Trim(txtNameEnd.Text) + "zzzzz'";
								}
							}
							// Account
							if (Strings.Trim(txtAccountStart.Text) != "" || Strings.Trim(txtAccountEnd.Text) != "")
							{
								// one of the fields has data in it
								if ((Strings.Trim(txtAccountStart.Text) != "" && Strings.Trim(txtAccountEnd.Text) == "") || (Strings.Trim(txtAccountStart.Text) == Strings.Trim(txtAccountEnd.Text)))
								{
									// if the first field has data and the second does not OR they both have the same thing that is not ""
									strTemp += " AND Account = " + Strings.Trim(txtAccountStart.Text);
								}
								else if (Strings.Trim(txtAccountEnd.Text) != "" && Strings.Trim(txtAccountStart.Text) == "")
								{
									// if only the second has data
									strTemp += " AND Account = " + Strings.Trim(txtAccountEnd.Text);
								}
								else if ((Strings.Trim(txtAccountStart.Text) != "" || Strings.Trim(txtAccountEnd.Text) == "") || (Strings.Trim(txtAccountStart.Text) == Strings.Trim(txtAccountEnd.Text) && Strings.Trim(txtAccountStart.Text) != ""))
								{
									// both fields have different data
									strTemp += " AND Account >= " + Strings.Trim(txtAccountStart.Text) + " AND Account <= " + Strings.Trim(txtAccountEnd.Text);
								}
							}
							if (!FCConvert.CBool(chkShowPastDueAmounts.CheckState == Wisej.Web.CheckState.Checked))
							{
								// force a single year
								strTemp += " AND Billingyear / 10 = " + strYear;
							}
							break;
						}
				}
				//end switch
				// add on the check to the period type
				if (intReminderType == 5)
				{
					// past due...let this roll
				}
				else
				{
					if (modStatusPayments.Statics.boolRE)
					{
						// check for the next period having taxes due
						switch (intReminderType)
						{
							case 0:
								{
									// strTemp = strTemp & " AND ((TaxDue1 - BillingMaster.PrincipalPaid) > 0 OR AbatementPaymentMade = TRUE) "
									strTemp += " AND (TaxDue1 - BillingMaster.PrincipalPaid) > 0 ";
									break;
								}
							case 1:
								{
									// strTemp = strTemp & " AND ((TaxDue1 + TaxDue2 - BillingMaster.PrincipalPaid) > 0 OR AbatementPaymentMade = TRUE) "
									strTemp += " AND (TaxDue1 + TaxDue2 - BillingMaster.PrincipalPaid) > 0  ";
									break;
								}
							case 2:
								{
									// strTemp = strTemp & " AND ((TaxDue1 + TaxDue2 + TaxDue3 - BillingMaster.PrincipalPaid) > 0 OR AbatementPaymentMade = TRUE) "
									strTemp += " AND (TaxDue1 + TaxDue2 + TaxDue3 - BillingMaster.PrincipalPaid) > 0 ";
									break;
								}
							case 3:
								{
									strTemp += " AND (TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4 - BillingMaster.PrincipalPaid) > 0 ";
									break;
								}
						}
						//end switch
					}
					else
					{
						// check for the next period having taxes due
						switch (intReminderType)
						{
							case 0:
								{
									// strTemp = strTemp & " AND ((TaxDue1 - PrincipalPaid) > 0 OR AbatementPaymentMade = TRUE) "
									strTemp += " AND (TaxDue1 - PrincipalPaid) > 0  ";
									break;
								}
							case 1:
								{
									// strTemp = strTemp & " AND ((TaxDue1 + TaxDue2 - PrincipalPaid) > 0 OR AbatementPaymentMade = TRUE) "
									strTemp += " AND (TaxDue1 + TaxDue2 - PrincipalPaid) > 0  ";
									break;
								}
							case 2:
								{
									// strTemp = strTemp & " AND ((TaxDue1 + TaxDue2 + TaxDue3 - PrincipalPaid) > 0 OR AbatementPaymentMade = TRUE) "
									strTemp += " AND (TaxDue1 + TaxDue2 + TaxDue3 - PrincipalPaid) > 0 ";
									break;
								}
							case 3:
								{
									strTemp += " AND (TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4 - PrincipalPaid) > 0 ";
									break;
								}
						}
						//end switch
					}
				}
				switch (intAction)
				{
					case 4:
						{
							if (modStatusPayments.Statics.boolRE)
							{
								if (boolLienedRecords)
								{
									// set the beginning of the string
									if (cmbOrder.SelectedIndex == 0)
									{
										strOrderBy = " ORDER BY Account";
									}
									else if (cmbOrder.SelectedIndex == 1)
									{
										strOrderBy = " ORDER BY Name";
										// ElseIf optOrder(2).Value Then
										// strOrderBy = " ORDER BY Name"
									}
									strMain = "SELECT * FROM (SELECT BillingMaster.ID, Account, Name FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.ID WHERE BillingType = 'RE' AND Principal - LienRec.PrincipalPaid > 0 " + strTemp + " AND LienRec.RateKey IN" + strRateKeyList + ") AS Acct" + strOrderBy;
								}
								else
								{
									if (cmbOrder.SelectedIndex == 0)
									{
										strOrderBy = " ORDER BY RSAccount";
									}
									// set the beginning of the string
									strMain = "SELECT * FROM (SELECT BillingMaster.ID, Account FROM BillingMaster WHERE BillingType = 'RE' AND (TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4 - PrincipalPaid) > 0 AND ISNULL(LienRecordNumber,0) = 0" + strTemp + " AND BillingMaster.RateKey IN" + strRateKeyList;
									strMain += " UNION SELECT BillingMaster.ID, Account FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.ID WHERE BillingType = 'RE' AND Principal - LienRec.PrincipalPaid > 0 " + strTemp + " AND LienRec.RateKey IN" + strRateKeyList + ") AS Acct INNER JOIN " + modGlobal.Statics.strDbRE + "Master ON Acct.Account = Master.RSAccount WHERE RSCard = 1 " + strOrderBy;
									// strTemp = "SELECT (TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4 - PrincipalPaid) AS SumAmount, * FROM BillingMaster INNER JOIN Master ON BillingMaster.Account = Master.RSAccount WHERE BillingType = 'RE' AND (TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4 - PrincipalPaid) > 0"
								}
							}
							else
							{
								if (cmbOrder.SelectedIndex == 0)
								{
									strOrderBy = " ORDER BY Account";
								}
								else if (cmbOrder.SelectedIndex == 1)
								{
									strOrderBy = " ORDER BY Name1";
								}
								strMain = "SELECT BillingMaster.ID, Account FROM BillingMaster WHERE BillingType = 'PP' AND (TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4 - PrincipalPaid) > 0" + strTemp + " AND BillingMaster.RateKey IN" + strRateKeyList + " " + strOrderBy;
							}
							break;
						}
					default:
						{
							if (modStatusPayments.Statics.boolRE)
							{
								if (boolLienedRecords)
								{
									if (cmbOrder.SelectedIndex == 0)
									{
										strOrderBy = " ORDER BY Account";
									}
                                    else
                                    {
                                        strOrderBy = " ORDER BY Name1";
                                    }
									// set the beginning of the string...inner join the master record so I can order by the owner name
									strMain = "SELECT * FROM (SELECT Distinct Account,Name1 FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.ID WHERE BillingType = 'RE' AND Principal - LienRec.PrincipalPaid > 0" + strTemp + " AND LienRec.RateKey IN" + strRateKeyList + ") AS Acct INNER JOIN " + modGlobal.Statics.strDbRE + "Master ON Acct.Account = Master.RSAccount WHERE RSCard = 1 " + strOrderBy;
								}
								else
								{
									if (cmbOrder.SelectedIndex == 0)
									{
										strOrderBy = " ORDER BY RSAccount";
                                        strMain = "SELECT * FROM (SELECT Distinct Account FROM BillingMaster WHERE BillingType = 'RE' AND (TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4 - PrincipalPaid) > 0 AND ISNULL(LienRecordNumber,0) = 0" + strTemp + " AND BillingMaster.RateKey IN" + strRateKeyList;
                                        strMain += " UNION SELECT Distinct Account FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.ID WHERE BillingType = 'RE' AND Principal - LienRec.PrincipalPaid > 0" + strTemp + " AND LienRec.RateKey IN" + strRateKeyList + ") AS Acct INNER JOIN " + modGlobal.Statics.strDbRE + "Master ON Acct.Account = Master.RSAccount WHERE RSCard = 1 " + strOrderBy;
									}
                                    else if (cmbOrder.SelectedIndex == 1)
                                    {
                                        strMain = "SELECT Account,Name1 FROM (" +
                                                  "SELECT Distinct Account,Name1 FROM BillingMaster WHERE BillingType = 'RE' AND (TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4 - PrincipalPaid) > 0 AND ISNULL(LienRecordNumber,0) = 0" + strTemp + " AND BillingMaster.RateKey IN" + strRateKeyList + " " +
                                                  "UNION SELECT Distinct Account,Name1 FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.ID WHERE BillingType = 'RE' AND Principal - LienRec.PrincipalPaid > 0" +
                                                  strTemp + " AND LienRec.RateKey IN" + strRateKeyList +
                                                  ") AS Acct INNER JOIN " + modGlobal.Statics.strDbRE +
                                                  "Master ON Acct.Account = Master.RSAccount WHERE RSCard = 1 ORDER BY Name1";
                                    }
                                }
                            }
							else
							{
								if (cmbOrder.SelectedIndex == 0)
								{
									strOrderBy = " ORDER BY Acct.Account";
								}
								else if (cmbOrder.SelectedIndex == 1)
								{
									strOrderBy = " ORDER BY FullNameLF";
								}
								strMain = "SELECT Acct.*, PPMaster.*, p.FullNameLF FROM (SELECT Distinct Account FROM BillingMaster WHERE BillingType = 'PP' AND (TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4 - PrincipalPaid) > 0" + strTemp + " AND BillingMaster.RateKey IN" + strRateKeyList + ") AS Acct INNER JOIN " + modGlobal.Statics.strDbPP + "PPMaster ON Acct.Account = PPMaster.Account INNER JOIN " + modGlobal.Statics.strDbCP + "PartyAndAddressView p ON p.ID = PPMaster.PartyID " + strOrderBy;
							}
							break;
						}
				}
				//end switch
				BuildSQL = strMain;
				return BuildSQL;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Building SQL");
			}
			return BuildSQL;
		}

		private void FillGrid_2(short intType)
		{
			FillGrid(ref intType);
		}

		private void FillGrid(ref short intType)
		{
			// this will fill the grid with the questions and potential answers for the user to choose from
			string strTemp = "";
            string strNameOptionDefault; // trocls-119 10.6.17 kjr
            strNameOptionDefault = "Owner/Address at time of Billing."; // 5.21.18

            LabelTypePanel.Visible = (intType == Labels);
            NamePanel.Visible = true;
            AccountPanel.Visible = true;
            ShowNamePanel.Visible = (intType != Outprint);
            HideDupNamesPanel.Visible = (intType == Labels);
            MessagePanel.Visible = (intType == PostCards || intType == LaserPostCards);
            UseReturnAddressPanel.Visible = (intType == PostCards || intType == LaserPostCards || intType == Mailers);
            MailDatePanel.Visible = (intType == FullPageForms || intType == Mailers || intType == Outprint);
            InterestDeptPanel.Visible = (intType == FullPageForms);
            MaturedYearDeptPanel.Visible = (intType == FullPageForms);
            HighestYearToIncludePanel.Visible = (intType == FullPageForms);
            LineBeforeSigPanel.Visible = (intType == FullPageForms);
            FirstLineAfterSigPanel.Visible = (intType == FullPageForms);
            SecondLineAfterSigPanel.Visible = (intType == FullPageForms);
            MapLotPanel.Visible = (intType == Mailers);
            ReturnAddressPanel.Visible = (intType == PostCards || intType == Mailers);

            ConfigureMailDatePanel(intType);

            switch (intType)
			{
                case Labels:
                {
                    cmbHideDupNames.SelectedIndex = cmbHideDupNames.Items.IndexOf("No");
                    cmbShowName.SelectedIndex = 0;
                    break;
                }
                case PostCards:              // Postcards and Laser Postcards
                {
                    cmbUseReturnAddress.SelectedIndex = cmbUseReturnAddress.Items.IndexOf("Yes");
                    cmbShowName.SelectedIndex = 0;
                    //modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "ReminderNoticeReturnAddress1", ref strTemp);
                    strTemp = modGlobal.Statics.setCont.GetSettingValue("ReminderNoticeReturnAddress1", "", "", "", "");
                    txtReturnAddress1.Text = strTemp;
					//modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "ReminderNoticeReturnAddress2", ref strTemp);
					strTemp = modGlobal.Statics.setCont.GetSettingValue("ReminderNoticeReturnAddress2", "", "", "", "");
                    txtReturnAddress2.Text = strTemp;
					//modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "ReminderNoticeReturnAddress3", ref strTemp);
                    strTemp = modGlobal.Statics.setCont.GetSettingValue("ReminderNoticeReturnAddress3", "", "", "", "");
                    txtReturnAddress3.Text = strTemp;
					//modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "ReminderNoticeReturnAddress4", ref strTemp);
                    strTemp = modGlobal.Statics.setCont.GetSettingValue("ReminderNoticeReturnAddress4", "", "", "", "");
                    txtReturnAddress4.Text = strTemp;
					//modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "ReminderNoticeMessage", ref strTemp);
                    strTemp = modGlobal.Statics.setCont.GetSettingValue("ReminderNoticeMessage", "", "", "", "");
					txtMessage.Text = strTemp;                        
					break;
				}
				case FullPageForms:
    			{
					// this is to see how far back the sub report will show account balances for
					if (modStatusPayments.Statics.boolRE)
					{
                        // not relevant for PP
                        lblLastYearMatured.Text = "Last Year Matured";
						// this is to output the last year that has matured
					}
					else
					{
						lblLastYearMatured.Text = "Last Year To Show";
						// this is to output the last year that to be shown
					}
                    cmbShowName.SelectedIndex = 0;                    
                    //modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "ReminderNoticeInterestDate", ref strTemp);
                    strTemp = modGlobal.Statics.setCont.GetSettingValue("ReminderNoticeInterestDate", "", "", "", "");
					txtInterestDate.Text = strTemp;
					//modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "ReminderNoticeMailDate2", ref strTemp);
					strTemp = modGlobal.Statics.setCont.GetSettingValue("ReminderNoticeMailDate2", "", "", "", "");
					txtMailDate.Text = strTemp;
					//modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "ReminderNoticeIntDept", ref strTemp);
					strTemp = modGlobal.Statics.setCont.GetSettingValue("ReminderNoticeIntDept", "", "", "", "");
					txtInterestDept.Text = strTemp;
					//modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "ReminderNoticeIntDeptPhone", ref strTemp);
					strTemp = modGlobal.Statics.setCont.GetSettingValue("ReminderNoticeIntDeptPhone", "", "", "", "");
					txtInterestDeptPhone.Text = strTemp;
					//modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "ReminderNoticeMatDept", ref strTemp);
					strTemp = modGlobal.Statics.setCont.GetSettingValue("ReminderNoticeMatDept", "", "", "", "");
					txtMaturedYearDept.Text = strTemp;
					//modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "ReminderNoticeMatDeptPhone", ref strTemp);
					strTemp = modGlobal.Statics.setCont.GetSettingValue("ReminderNoticeMatDeptPhone", "", "", "", "");
					txtMaturedYearDeptPhone.Text = strTemp;
					//modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "ReminderNoticeHighYear", ref strTemp);
					strTemp = modGlobal.Statics.setCont.GetSettingValue("ReminderNoticeHighYear", "", "", "", "");
                    cmbHighestYearToInclude.SelectedIndex = cmbHighestYearToInclude.Items.IndexOf(strTemp);
					//modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "ReminderNoticeLastYear", ref strTemp);
					strTemp = modGlobal.Statics.setCont.GetSettingValue("ReminderNoticeLastYear", "", "", "", "");
                    cmbLastYearMatured.SelectedIndex = cmbLastYearMatured.Items.IndexOf(strTemp);
                    break;
				}
				case Mailers:
                {
                    cmbShowMapLot.SelectedIndex = cmbShowMapLot.Items.IndexOf("Yes");
                    cmbUseReturnAddress.SelectedIndex = cmbUseReturnAddress.Items.IndexOf("Yes");
                    cmbShowName.SelectedIndex = 0;
                    //modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "ReminderNoticeReturnAddress1", ref strTemp);
                    strTemp = modGlobal.Statics.setCont.GetSettingValue("ReminderNoticeReturnAddress1", "", "", "", "");
                    txtReturnAddress1.Text = strTemp;
					//modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "ReminderNoticeReturnAddress2", ref strTemp);
                    strTemp = modGlobal.Statics.setCont.GetSettingValue("ReminderNoticeReturnAddress2", "", "", "", "");
                    txtReturnAddress2.Text = strTemp;
					//modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "ReminderNoticeReturnAddress3", ref strTemp);
                    strTemp = modGlobal.Statics.setCont.GetSettingValue("ReminderNoticeReturnAddress3", "", "", "", "");
                    txtReturnAddress3.Text = strTemp;
					//modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "ReminderNoticeReturnAddress4", ref strTemp);
                    strTemp = modGlobal.Statics.setCont.GetSettingValue("ReminderNoticeReturnAddress4", "", "", "", "");
                    txtReturnAddress4.Text = strTemp;
					break;
				}
				case Outprint:
				{
				    // OutPrinting File

					break;
				}
			}
		}

        private void ConfigureMailDatePanel(short intType)
        {
            lblInterestDate.Visible = (intType == FullPageForms || intType == Outprint);
            txtInterestDate.Visible = (intType == FullPageForms || intType == Outprint);
            txtInterestDate.Enabled = (intType == FullPageForms || intType == Outprint);
            lblMailDate.Visible = (intType == FullPageForms || intType == Mailers);
            txtMailDate.Visible = (intType == FullPageForms || intType == Mailers);
            txtMailDate.Enabled = (intType == FullPageForms || intType == Mailers);
            if (intType == Outprint)
            {
                lblInterestDate.Anchor &= ~AnchorStyles.Right;
                lblInterestDate.Anchor |= AnchorStyles.Left;
                lblInterestDate.TextAlign = ContentAlignment.MiddleLeft;
            }
            else
            {
                lblInterestDate.Anchor &= ~AnchorStyles.Left;
                lblInterestDate.Anchor |= AnchorStyles.Right;
                lblInterestDate.TextAlign = ContentAlignment.MiddleRight;
            }
        }

		private void FillStringArray()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the string array of choices that will dropdown in the boxes
				string strYear = "";
				string strNonLienYears = "";
				string strLabels = "";
				int lngLastLienedYear = 0;
				clsDRWrapper rsYear = new clsDRWrapper();
				int lngMCT = 0;
				int lngListCT/*unused?*/;
				int lngListCK;
				string[] strMessList = new string[10 + 1];
				bool boolSaveMessage = false;
				string strMessages = "";
				string strDuplicate;
				FCUtils.EraseSafe(strList);
				// clear the list
				// fill the years array     'kk05012014 trocl-560  Only fill the years that have been billed, no NoRK/PrePay years
				rsYear.OpenRecordset("SELECT DISTINCT BillingYear FROM BillingMaster WHERE RateKey <> 0 ORDER BY BillingYear", modExtraModules.strCLDatabase);
				if (!rsYear.EndOfFile())
				{
					rsYear.MoveLast();
					while (!rsYear.BeginningOfFile())
					{
						strYear += modExtraModules.FormatYear(FCConvert.ToString(rsYear.Get_Fields_Int32("BillingYear"))) + "|";
						rsYear.MovePrevious();
					}
				}
				// fill the years array with non liened years only
				rsYear.OpenRecordset("SELECT Top 1 BillingYear FROM LienRec INNER JOIN BillingMaster ON LienRec.ID = BillingMaster.LienRecordNumber ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
				if (!rsYear.EndOfFile())
				{
					lngLastLienedYear = FCConvert.ToInt32(rsYear.Get_Fields_Int32("BillingYear"));
				}
				else
				{
					lngLastLienedYear = 0;
				}
				rsYear.OpenRecordset("SELECT DISTINCT BillingYear FROM BillingMaster ORDER BY BillingYear", modExtraModules.strCLDatabase);
				if (!rsYear.EndOfFile())
				{
					rsYear.MoveLast();
					while (!rsYear.BeginningOfFile())
					{
						if (FCConvert.ToInt32(rsYear.Get_Fields_Int32("BillingYear")) > lngLastLienedYear)
						{
							strNonLienYears += modExtraModules.FormatYear(FCConvert.ToString(rsYear.Get_Fields_Int32("BillingYear"))) + "|";
						}
						rsYear.MovePrevious();
					}
				}
				rsYear.OpenRecordset("SELECT * FROM ReminderNoticeMessages WHERE RTRIM(Message) <> '' ORDER BY ReminderNoticeMessagesDate desc", modExtraModules.strCLDatabase);
				if (!rsYear.EndOfFile())
				{
					FCUtils.EraseSafe(strMessList);
					while (!rsYear.EndOfFile())
					{
						boolSaveMessage = false;
						for (lngListCK = 0; lngListCK <= 10; lngListCK++)
						{
							if (strMessList[lngListCK] != "")
							{
								if (strMessList[lngListCK] == Strings.Trim(FCConvert.ToString(rsYear.Get_Fields_String("Message"))))
								{
									// This is already int he list so exit and do not save it
									boolSaveMessage = false;
									break;
								}
							}
							else
							{
								// found an empty index in the list so this message has not been saved yet
								strMessList[lngListCK] = Strings.Trim(FCConvert.ToString(rsYear.Get_Fields_String("Message")));
								boolSaveMessage = true;
								break;
							}
						}
						if (boolSaveMessage)
						{
							lngMCT += 1;
							if (lngMCT <= 10)
							{
								strMessages += "|" + Strings.Trim(FCConvert.ToString(rsYear.Get_Fields_String("Message")));
							}
							else
							{
								break;
							}
						}
						rsYear.MoveNext();
					}
				}
				// this should take off the last '|'
				if (strYear.Length > 0)
					strYear = Strings.Left(strYear, strYear.Length - 1);
                strNameType = "#0;Owner/Address at time of Billing.|#1;Owner at time of Billing, C/O Current Owner/Address.|#2;Owner at time of Billing at Last Recorded Address."; // trocls-119 10.6.17 kjr  4.23.18, 5.21.18 CODE FREEZE
                //strLabels = BuildLabelsComboString();
				strDuplicate = "#0;Yes|#1;No";
				strList[1, 0] = strLabels;
				// Label Types
				// strList(2, 0) = strYear             'Year
				strList[2, 0] = "";
				// Name Range
				strList[3, 0] = "";
				// Account Range
				strList[4, 0] = strNameType;
				strList[5, 0] = strDuplicate;
				// Post Cards
				// strList(1, 1) = strNonLienYears     'Year
				strList[1, 1] = "";
				// Name Range
				strList[2, 1] = "";
				// Account Range
				strList[3, 1] = strMessages;
				// Message
				strList[4, 1] = "#0;Yes|#1;No";
				// Return Address Question
				strList[5, 1] = strNameType;
				strLabels = "#0;Plain Paper (1 per page)|#1;Plain Paper (2 per page)|#2;Full Page Notice";
				// Forms
				strList[1, 2] = "";
				// Name Range
				strList[2, 2] = "";
				// Account Range
				strList[3, 2] = "";
				// Int Date
				strList[4, 2] = "";
				// Mail Date
				strList[5, 2] = "";
				// Interest Department
				strList[6, 2] = "";
				// Interest Department Phone
				strList[7, 2] = "";
				// Matured Year Department
				strList[8, 2] = "";
				// Matured Year Department Phone
				// MAL@20080103: Changed to allow liened years to be in the high year list
				// Tracker Reference: 11460
				// strList(9, 2) = strNonLienYears     'High Year
				strList[9, 2] = strYear;
				// High Year
				strList[10, 2] = strYear;
				// Last Year Matured
				strList[11, 2] = strNameType;
				// Mailer
				// strList(1, 3) = strNonLienYears     'Year
				strList[1, 3] = "";
				// Name Range
				strList[2, 3] = "";
				// Account Range
				strList[3, 3] = "#0;Yes|#1;No";
				// Show Map Lot?
				strList[4, 3] = "#0;Yes|#1;No";
				// Return Address
				strList[5, 3] = "";
				// Mailing Date
				strList[6, 3] = strNameType;
				// OutPrinting File
				// strList(1, 4) = strNonLienYears     'Year
				strList[1, 4] = "";
				// Name Range
				strList[2, 4] = "";
				// Account Range
				strList[3, 4] = "";
				// strList(4, 4) = "#0;First|#1;Second|#2;Third|#3;Fourth" 'Which Payment
				// Laser Post Cards
				strList[1, 5] = "";
				// Name Range
				strList[2, 5] = "";
				// Account Range
				strList[3, 5] = strMessages;
				// Message
				strList[4, 5] = "#0;Yes|#1;No";
				// Return Address Question
				strList[5, 5] = strNameType;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Filling String Array");
			}
		}

		private void ShowAllFrames()
		{
			//ZZZ vsGrid.Visible = true;
			fraRN.Visible = true;
			fraType.Visible = true;
		}

		private bool ValidateAnswers()
		{
			bool ValidateAnswers = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intCT;
				bool boolOK;
				// this function will check to make sure that the user answered the required questions correctly
				//ZZZ vsGrid.Select(0, 0);
				boolOK = false;
				// check to make sure that the user selected a period
				if (cmbPeriod.SelectedIndex < 0)
				{
					ValidateAnswers = false;
					FCMessageBox.Show("Please select the period for the reminder notice.", MsgBoxStyle.Information, "Data Input");
					if (cmbPeriod.Items[1].ToString() == auxItem1CmbPeriod)
					{
						cmbPeriod.Focus();
					}
					return ValidateAnswers;
				}
				// Force Show Past Due Amounts to Be Checked
				if (chkGetPreviousYears.CheckState == Wisej.Web.CheckState.Checked)
				{
					chkShowPastDueAmounts.CheckState = Wisej.Web.CheckState.Checked;
				}
				switch (intAction)
				{
					case Labels:
						{
							// Labels
							if (cmbLabelType.Text != "")
							{
								ValidateAnswers = true;
							}
							else
							{
								ValidateAnswers = false;
								FCMessageBox.Show("Please select the type of label to print.", MsgBoxStyle.Information, "Data Input");
								return ValidateAnswers;
							}
							break;
						}
					case PostCards:
					case LaserPostCards:
						{
							// Post Cards
							ValidateAnswers = true;
							break;
						}
					case FullPageForms:
						{
							// Forms
							if (txtInterestDate.Text != "")
							{
							 	if (txtMailDate.Text != "")
								{
							 		if (txtInterestDept.Text != "")
							 		{
							 			if (txtInterestDeptPhone.Text != "")
							 			{
											if (txtMaturedYearDept.Text != "")
							 				{
							 					if (txtMaturedYearDeptPhone.Text != "")
							 					{
							   					if (true)   //XXX   NEED TO Validate the combobox  (txtHighestYearToInclude.Text != "")
													{
							 							// If boolRE Then  'only for RE since PP does not mature
							 							if (true)   //XXX   NEED TO Validate the combobox  (txtLastYearMatured.Text != "")
                                                        {
							 								ValidateAnswers = true;
							 							}
							 							else
							 							{
															// last year matured
															ValidateAnswers = false;
															if (modStatusPayments.Statics.boolRE)
															{
																FCMessageBox.Show("Please enter the last year matured.", MsgBoxStyle.Exclamation, "Invalid Data");
															}
															else
															{
																FCMessageBox.Show("Please enter the last year to show.", MsgBoxStyle.Exclamation, "Invalid Data");
															}
															//XXX txtLastYearMatured.Select();
														}
														// Else
														// ValidateAnswers = True
														// End If
													}
													else
													{
														// high year
														ValidateAnswers = false;
														FCMessageBox.Show("Please enter the highest year that is to be shown.", MsgBoxStyle.Exclamation, "Invalid Data");
														//XXX txtHighestYearToInclude.Select();
													}
												}
												else
												{
													// matured year dept phone
													ValidateAnswers = false;
													FCMessageBox.Show("Please enter the phone number for the matured year department.", MsgBoxStyle.Exclamation, "Invalid Data");
													txtMaturedYearDeptPhone.Select();
												}
											}
											else
											{
												// matured year dept
												ValidateAnswers = false;
												FCMessageBox.Show("Please enter the name for the matured year department.", MsgBoxStyle.Exclamation, "Invalid Data");
												txtMaturedYearDept.Select();
											}
										}
										else
										{
											// interest dept phone
											ValidateAnswers = false;
											FCMessageBox.Show("Please enter the phone number for the department that collects interest.", MsgBoxStyle.Exclamation, "Invalid Data");
											txtInterestDeptPhone.Select();
										}
									}
									else
									{
										// Interest Department
										ValidateAnswers = false;
										FCMessageBox.Show("Please enter the name for the department that collects interest.", MsgBoxStyle.Exclamation, "Invalid Data");
										txtInterestDept.Select();
									}
								}
								else
								{
									// mail date
									ValidateAnswers = false;
									FCMessageBox.Show("Please enter a valid date into the Mail Date field.", MsgBoxStyle.Exclamation, "Invalid Data");
									txtMailDate.Select();
								}
							}
							else
							{
							 	if (!Information.IsDate(txtInterestDate.Text))
							 	{
							 		// interest date
							 		ValidateAnswers = false;
							 		FCMessageBox.Show("Please enter a valid date into the Interest Date field.", MsgBoxStyle.Exclamation, "Invalid Data");
							 		txtInterestDate.Select();
							 	}
							}
							if (ValidateAnswers)
							{
								if (!Information.IsDate(txtInterestDate.Text))
								{
									ValidateAnswers = false;
									FCMessageBox.Show("Please enter a valid date into the Interest Date field.", MsgBoxStyle.Exclamation, "Invalid Data");
                                    txtInterestDate.Select();
									return ValidateAnswers;
								}
								if (!Information.IsDate(txtMailDate.Text))
							 	{
							 		ValidateAnswers = false;
							 		FCMessageBox.Show("Please enter a valid date into the Mail Date field.", MsgBoxStyle.Exclamation, "Invalid Data");
							 		txtMailDate.Select();
							 		return ValidateAnswers;
							 	}
							}
							break;
						}
					case Mailers:
						{
							// Mailer
							// If vsGrid.TextMatrix(1, 1) <> "" Then               'year    THIS WAS ALREADY COMMENTED OUT
							if (txtMailDate.Text != "")
							{
								// date
								if (Information.IsDate(txtMailDate.Text))
								{
									ValidateAnswers = true;
								}
								else
								{
									FCMessageBox.Show("Please enter a valid date into the Date Field.", MsgBoxStyle.Exclamation, "Invalid Data");
									txtMailDate.Select();
								}
							}
							else
							{
								FCMessageBox.Show("Please enter a value into the Date Field.", MsgBoxStyle.Exclamation, "Invalid Data");
								txtMailDate.Select();
							}
							// Else
							// MsgBox "Please select a value in the Year Field.", MsgBoxStyle.Exclamation, "Invalid Data"
							// vsGrid.Select 1, 1
							// End If
							break;
						}
					case Outprint:
						{
							// OutPrinting File
							if (txtInterestDate.Text != "")
							{
								// date
								if (Information.IsDate(txtInterestDate.Text))
								{
									ValidateAnswers = true;
								}
								else
								{
									FCMessageBox.Show("Please enter a valid date into the Interest Date Field.", MsgBoxStyle.Exclamation, "Invalid Data");
									txtInterestDate.Select();
								}
							}
							else
							{
								FCMessageBox.Show("Please enter a value into the Interest Date Field.", MsgBoxStyle.Exclamation, "Invalid Data");
								txtInterestDate.Select();
							}
							break;
						}
				}
				//end switch
				return ValidateAnswers;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Validating Selection");
			}
			return ValidateAnswers;
		}

        private void txtInterestDate_Validate(object sender,bool cancel)
        {
            if (!Information.IsDate(txtInterestDate.Text))
            {
                FCMessageBox.Show("Please enter a valid date.", MsgBoxStyle.Exclamation, "Invalid Date");
                cancel = true;
            }
        }

        private void txtMailDate_Validate(object sender, bool cancel)
        {
            if (!Information.IsDate(txtMailDate.Text))
            {
                FCMessageBox.Show("Please enter a valid date.", MsgBoxStyle.Exclamation, "Invalid Date");
                cancel = true;
            }
        }

        private void cmbLabelType_Validate(object sender, bool cancel)
        {
            intLabelType = FCConvert.ToInt32(cmbLabelType.ValueMember);
        }       

		// VBto upgrade warning: intAct As short	OnWriteFCConvert.ToInt32(
		private void SaveDefaults(int intAct)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strTemp = "";
				clsDRWrapper rsPC = new clsDRWrapper();
				switch (intAct)
				{
					case Labels:
						{
							break;
						}
					case PostCards:
					case LaserPostCards:
						{
                            strTemp = txtReturnAddress1.Text;
                            modGlobal.Statics.setCont.SaveSetting(strTemp, "ReminderNoticeReturnAddress1", "", "", "", "");
                            strTemp = txtReturnAddress2.Text;
                            modGlobal.Statics.setCont.SaveSetting(strTemp, "ReminderNoticeReturnAddress2", "", "", "", "");
                            strTemp = txtReturnAddress3.Text;
                            modGlobal.Statics.setCont.SaveSetting(strTemp, "ReminderNoticeReturnAddress3", "", "", "", "");
                            strTemp = txtReturnAddress4.Text;
                            modGlobal.Statics.setCont.SaveSetting(strTemp, "ReminderNoticeReturnAddress4", "", "", "", "");
							strTemp = txtMessage.Text;
							modGlobal.Statics.setCont.SaveSetting(strTemp, "ReminderNoticeMessage", "", "", "", "");
							rsPC.Execute("INSERT INTO ReminderNoticeMessages (Message, ReminderNoticeMessagesDate) VALUES ('" + strTemp + "', NULL)", modExtraModules.strCLDatabase);
							FillStringArray();
							break;
						}
					case FullPageForms:
						{
							strTemp = txtInterestDate.Text;
							modGlobal.Statics.setCont.SaveSetting(strTemp, "ReminderNoticeInterestDate", "", "", "", "");
							strTemp = txtMailDate.Text;
							modGlobal.Statics.setCont.SaveSetting(strTemp, "ReminderNoticeMailDate2", "", "", "", "");
							strTemp = txtInterestDept.Text;
							modGlobal.Statics.setCont.SaveSetting(strTemp, "ReminderNoticeIntDept", "", "", "", "");
							strTemp = txtInterestDeptPhone.Text;
							modGlobal.Statics.setCont.SaveSetting(strTemp, "ReminderNoticeIntDeptPhone", "", "", "", "");
							strTemp = txtMaturedYearDept.Text;
							modGlobal.Statics.setCont.SaveSetting(strTemp, "ReminderNoticeMatDept", "", "", "", "");
							strTemp = txtMaturedYearDeptPhone.Text;
							modGlobal.Statics.setCont.SaveSetting(strTemp, "ReminderNoticeMatDeptPhone", "", "", "", "");
                            strTemp = cmbHighestYearToInclude.SelectedItem.ToString(); 
							modGlobal.Statics.setCont.SaveSetting(strTemp, "ReminderNoticeHighYear", "", "", "", "");
                            strTemp = cmbLastYearMatured.SelectedItem.ToString();
							modGlobal.Statics.setCont.SaveSetting(strTemp, "ReminderNoticeLastYear", "", "", "", "");
							break;
						}
					case Mailers:
						{
                            strTemp = txtReturnAddress1.Text;
                            modGlobal.Statics.setCont.SaveSetting(strTemp, "ReminderNoticeReturnAddress1", "", "", "", "");
                            strTemp = txtReturnAddress2.Text;
                            modGlobal.Statics.setCont.SaveSetting(strTemp, "ReminderNoticeReturnAddress2", "", "", "", "");
                            strTemp = txtReturnAddress3.Text;
                            modGlobal.Statics.setCont.SaveSetting(strTemp, "ReminderNoticeReturnAddress3", "", "", "", "");
                            strTemp = txtReturnAddress4.Text;
                            modGlobal.Statics.setCont.SaveSetting(strTemp, "ReminderNoticeReturnAddress4", "", "", "", "");
							break;
						}
					case Outprint:
						{
							break;
						}
				}
				//end switch
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Saving Defaults");
			}
		}

		private bool CheckSigPassword(ref string strPW)
		{
			bool CheckSigPassword = false;
			// this will validate the password signature
			if (Strings.UCase(Strings.Trim(modSignatureFile.Statics.gstrCollectorSigPassword)) == Strings.UCase(Strings.Trim(strPW)))
			{
				CheckSigPassword = true;
			}
			else
			{
				CheckSigPassword = false;
			}
			return CheckSigPassword;
		}

		private string GetAllPreviousRateKeys()
		{
			string GetAllPreviousRateKeys = "";
			// MAL@20071008: Created to get all rate keys that come before currently selected key(s)
			// Call Reference: 116618- Item #3
			clsDRWrapper rsRK = new clsDRWrapper();
			clsDRWrapper rsRKNew = new clsDRWrapper();
			DateTime dtBillDate;
			int lngRK/*unused?*/;
			int lngYear = 0;
			string strNewList = "";
			rsRK.OpenRecordset("SELECT * FROM RateRec WHERE ID IN " + strRateKeyList, modExtraModules.strCLDatabase);
			if (rsRK.RecordCount() > 0)
			{
				rsRK.MoveFirst();
				while (!rsRK.EndOfFile())
				{
					dtBillDate = (DateTime)rsRK.Get_Fields_DateTime("BillingDate");
					// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
					lngYear = FCConvert.ToInt32(rsRK.Get_Fields("Year"));
					if (strNewList == "")
					{
						strNewList = FCConvert.ToString(rsRK.Get_Fields_Int32("ID"));
					}
					else
					{
						strNewList += "," + FCConvert.ToString(rsRK.Get_Fields_Int32("ID"));
					}
					rsRKNew.OpenRecordset("SELECT * FROM RateRec WHERE [Year] <= " + FCConvert.ToString(lngYear) + " AND BillingDate <= '" + FCConvert.ToString(dtBillDate) + "' AND ID NOT IN(" + strNewList + ")", modExtraModules.strCLDatabase);
					if (rsRKNew.RecordCount() > 0)
					{
						rsRKNew.MoveFirst();
						while (!rsRKNew.EndOfFile())
						{
							if (strNewList == "")
							{
								strNewList = FCConvert.ToString(rsRKNew.Get_Fields_Int32("ID"));
							}
							else
							{
								strNewList += "," + FCConvert.ToString(rsRKNew.Get_Fields_Int32("ID"));
							}
							rsRKNew.MoveNext();
						}
					}
					rsRK.MoveNext();
				}
			}
			GetAllPreviousRateKeys = "(" + strNewList + ")";
			return GetAllPreviousRateKeys;
		}

        private IEnumerable<DescriptionIDPair> GetLabelTypes()
        {
            var labelList = new List<DescriptionIDPair>();
            
            for (var counter = 0; counter <= labLabelTypes.TypeCount - 1; counter++)
            {
                if (labLabelTypes.Get_ID(counter) == modLabels.CNSTLBLTYPE5066)
                {
                    labLabelTypes.Set_Visible(counter, false);
                    break;
                }
            }
            for (var counter = 0; counter <= labLabelTypes.TypeCount - 1; counter++)
            {
                if (labLabelTypes.Get_Visible(counter) ||
                    labLabelTypes.Get_ID(counter) == modLabels.CNSTLBLTYPEDYMO30256 ||
                    labLabelTypes.Get_ID(counter) == modLabels.CNSTLBLTYPE4065 ||
                    labLabelTypes.Get_ID(counter) == modLabels.CNSTLBLTYPEDYMO4150)
                {
                    labelList.Add(new DescriptionIDPair()
                    {
                        Description = labLabelTypes.Get_Caption(counter),
                        ID = labLabelTypes.Get_ID(counter)
                    });
                }
            }

            return labelList;
        }

        private IEnumerable<string> GetBillingYears()
        {
            clsDRWrapper rsYear = new clsDRWrapper();
            string strSQL = "";
            int lastYear = 0;
            var yearList = new List<string>();


            strSQL = "SELECT DISTINCT BillingYear FROM BillingMaster ORDER BY BillingYear DESC";
            rsYear.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
            while (!rsYear.EndOfFile())
            {
                if (lastYear != FCUtils.iDiv(FCConvert.ToInt32(rsYear.Get_Fields("BillingYear")), 10))
                {
                    yearList.Add(FCConvert.ToString(FCUtils.iDiv(FCConvert.ToInt32(rsYear.Get_Fields("BillingYear")), 10)));
                    lastYear = FCUtils.iDiv(FCConvert.ToInt32(rsYear.Get_Fields("BillingYear")), 10);
                }
                rsYear.MoveNext();
            }
            return (yearList);
        }

        private IEnumerable<string> GetLienYears()
        {
            clsDRWrapper rsYear = new clsDRWrapper();
            string strSQL = "";
            int lastYear = 0;
            var yearList = new List<string>();


            strSQL = "SELECT DISTINCT BillingYear FROM BillingMaster WHERE ISNULL(LienRecordNumber,0) > 0 ORDER BY BillingYear DESC";
            rsYear.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
            while (!rsYear.EndOfFile())
            {
                if (lastYear != FCUtils.iDiv(FCConvert.ToInt32(rsYear.Get_Fields("BillingYear")), 10))
                {
                    yearList.Add(FCConvert.ToString(FCUtils.iDiv(FCConvert.ToInt32(rsYear.Get_Fields("BillingYear")), 10)));
                    lastYear = FCUtils.iDiv(FCConvert.ToInt32(rsYear.Get_Fields("BillingYear")), 10);
                }
                rsYear.MoveNext();
            }
            return (yearList);
        }

        private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFilePrint_Click(sender, e);
		}
    }
}
