﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmDatabaseExtract.
	/// </summary>
	partial class frmDatabaseExtract : BaseForm
	{
		public fecherFoundation.FCComboBox cmbPeriod;
		public fecherFoundation.FCFrame fraDataEntry;
		public fecherFoundation.FCGrid vsFields;
		public T2KDateBox txtDate;
		public fecherFoundation.FCTextBox lblDate;
		public fecherFoundation.FCComboBox cmbYear;
		public fecherFoundation.FCButton cmdBrowse;
		public fecherFoundation.FCTextBox txtTSFilePath;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDatabaseExtract));
			this.cmbPeriod = new fecherFoundation.FCComboBox();
			this.fraDataEntry = new fecherFoundation.FCFrame();
			this.fcLabel4 = new fecherFoundation.FCLabel();
			this.cmdBrowse = new fecherFoundation.FCButton();
			this.fcLabel3 = new fecherFoundation.FCLabel();
			this.txtTSFilePath = new fecherFoundation.FCTextBox();
			this.cmbYear = new fecherFoundation.FCComboBox();
			this.fcLabel2 = new fecherFoundation.FCLabel();
			this.lblDate = new fecherFoundation.FCTextBox();
			this.txtDate = new Global.T2KDateBox();
			this.fcLabel1 = new fecherFoundation.FCLabel();
			this.vsFields = new fecherFoundation.FCGrid();
			this.lblPeriod = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.btnProcess = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraDataEntry)).BeginInit();
			this.fraDataEntry.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsFields)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 623);
			this.BottomPanel.Size = new System.Drawing.Size(742, 10);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.btnProcess);
			this.ClientArea.Controls.Add(this.fraDataEntry);
			this.ClientArea.Size = new System.Drawing.Size(742, 563);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(742, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(26, 26);
			this.HeaderText.Size = new System.Drawing.Size(201, 35);
			this.HeaderText.Text = "Database Extract";
			// 
			// cmbPeriod
			// 
			this.cmbPeriod.AutoSize = false;
			this.cmbPeriod.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbPeriod.FormattingEnabled = true;
			this.cmbPeriod.Items.AddRange(new object[] {
				"1",
				"2",
				"3",
				"4"
			});
			this.cmbPeriod.Location = new System.Drawing.Point(164, 150);
			this.cmbPeriod.Name = "cmbPeriod";
			this.cmbPeriod.Size = new System.Drawing.Size(140, 40);
			this.cmbPeriod.TabIndex = 5;
			this.cmbPeriod.Text = "1";
			this.cmbPeriod.SelectedIndexChanged += new System.EventHandler(this.optPeriod_Click);
			// 
			// fraDataEntry
			// 
			this.fraDataEntry.Controls.Add(this.fcLabel4);
			this.fraDataEntry.Controls.Add(this.cmdBrowse);
			this.fraDataEntry.Controls.Add(this.fcLabel3);
			this.fraDataEntry.Controls.Add(this.txtTSFilePath);
			this.fraDataEntry.Controls.Add(this.cmbYear);
			this.fraDataEntry.Controls.Add(this.fcLabel2);
			this.fraDataEntry.Controls.Add(this.lblDate);
			this.fraDataEntry.Controls.Add(this.txtDate);
			this.fraDataEntry.Controls.Add(this.fcLabel1);
			this.fraDataEntry.Controls.Add(this.vsFields);
			this.fraDataEntry.Controls.Add(this.lblPeriod);
			this.fraDataEntry.Controls.Add(this.cmbPeriod);
			this.fraDataEntry.Location = new System.Drawing.Point(30, 25);
			this.fraDataEntry.Name = "fraDataEntry";
			this.fraDataEntry.Size = new System.Drawing.Size(651, 444);
			this.fraDataEntry.TabIndex = 0;
			this.fraDataEntry.Text = "Enter Extract Information";
			this.fraDataEntry.UseMnemonic = false;
			// 
			// fcLabel4
			// 
			this.fcLabel4.AutoSize = true;
			this.fcLabel4.Location = new System.Drawing.Point(20, 222);
			this.fcLabel4.Name = "fcLabel4";
			this.fcLabel4.Size = new System.Drawing.Size(95, 16);
			this.fcLabel4.TabIndex = 6;
			this.fcLabel4.Text = "EXTRACT FILE";
			// 
			// cmdBrowse
			// 
			this.cmdBrowse.AppearanceKey = "toolbarButton";
			this.cmdBrowse.Location = new System.Drawing.Point(182, 296);
			this.cmdBrowse.Name = "cmdBrowse";
			this.cmdBrowse.Size = new System.Drawing.Size(75, 33);
			this.cmdBrowse.TabIndex = 9;
			this.cmdBrowse.Text = "Browse";
			this.cmdBrowse.Visible = false;
			this.cmdBrowse.Click += new System.EventHandler(this.cmdBrowse_Click);
			// 
			// fcLabel3
			// 
			this.fcLabel3.AutoSize = true;
			this.fcLabel3.Location = new System.Drawing.Point(20, 44);
			this.fcLabel3.Name = "fcLabel3";
			this.fcLabel3.Size = new System.Drawing.Size(41, 16);
			this.fcLabel3.TabIndex = 0;
			this.fcLabel3.Text = "YEAR";
			this.fcLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtTSFilePath
			// 
			this.txtTSFilePath.AutoSize = false;
			this.txtTSFilePath.BackColor = System.Drawing.SystemColors.Window;
			this.txtTSFilePath.Location = new System.Drawing.Point(164, 208);
			this.txtTSFilePath.LockedOriginal = true;
			this.txtTSFilePath.Name = "txtTSFilePath";
			this.txtTSFilePath.ReadOnly = true;
			this.txtTSFilePath.Size = new System.Drawing.Size(140, 40);
			this.txtTSFilePath.TabIndex = 7;
			// 
			// cmbYear
			// 
			this.cmbYear.AutoSize = false;
			this.cmbYear.BackColor = System.Drawing.SystemColors.Window;
			this.cmbYear.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbYear.FormattingEnabled = true;
			this.cmbYear.Location = new System.Drawing.Point(164, 30);
			this.cmbYear.Name = "cmbYear";
			this.cmbYear.Size = new System.Drawing.Size(140, 40);
			this.cmbYear.TabIndex = 1;
			this.cmbYear.SelectedIndexChanged += new System.EventHandler(this.cmbYear_SelectedIndexChanged);
			// 
			// fcLabel2
			// 
			this.fcLabel2.AutoSize = true;
			this.fcLabel2.Location = new System.Drawing.Point(20, 104);
			this.fcLabel2.Name = "fcLabel2";
			this.fcLabel2.Size = new System.Drawing.Size(105, 16);
			this.fcLabel2.TabIndex = 2;
			this.fcLabel2.Text = "INTEREST DATE";
			this.fcLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDate
			// 
			this.lblDate.AutoSize = false;
			this.lblDate.Location = new System.Drawing.Point(164, 90);
			this.lblDate.LockedOriginal = true;
			this.lblDate.Name = "lblDate";
			this.lblDate.ReadOnly = true;
			this.lblDate.Size = new System.Drawing.Size(140, 40);
			this.lblDate.TabIndex = 3;
			// 
			// txtDate
			// 
			this.txtDate.Location = new System.Drawing.Point(20, 289);
			this.txtDate.Mask = "##/##/####";
			this.txtDate.Name = "txtDate";
			this.txtDate.Size = new System.Drawing.Size(140, 40);
			this.txtDate.TabIndex = 8;
			this.txtDate.Text = "  /  /";
			this.txtDate.Visible = false;
			// 
			// fcLabel1
			// 
			this.fcLabel1.AutoSize = true;
			this.fcLabel1.Location = new System.Drawing.Point(366, 30);
			this.fcLabel1.Name = "fcLabel1";
			this.fcLabel1.Size = new System.Drawing.Size(51, 16);
			this.fcLabel1.TabIndex = 10;
			this.fcLabel1.Text = "FIELDS";
			this.fcLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// vsFields
			// 
			this.vsFields.AllowSelection = false;
			this.vsFields.AllowUserToResizeColumns = false;
			this.vsFields.AllowUserToResizeRows = false;
			this.vsFields.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsFields.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsFields.BackColorBkg = System.Drawing.Color.Empty;
			this.vsFields.BackColorFixed = System.Drawing.Color.Empty;
			this.vsFields.BackColorSel = System.Drawing.Color.Empty;
			this.vsFields.Cols = 3;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsFields.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsFields.ColumnHeadersHeight = 30;
			this.vsFields.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.vsFields.ColumnHeadersVisible = false;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsFields.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsFields.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsFields.FixedCols = 0;
			this.vsFields.FixedRows = 0;
			this.vsFields.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsFields.FrozenCols = 0;
			this.vsFields.FrozenRows = 0;
			this.vsFields.GridColor = System.Drawing.Color.Empty;
			this.vsFields.Location = new System.Drawing.Point(361, 56);
			this.vsFields.Name = "vsFields";
			this.vsFields.ReadOnly = true;
			this.vsFields.RowHeadersVisible = false;
			this.vsFields.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsFields.RowHeightMin = 0;
			this.vsFields.Rows = 0;
			this.vsFields.ShowColumnVisibilityMenu = false;
			this.vsFields.Size = new System.Drawing.Size(270, 369);
			this.vsFields.StandardTab = true;
			this.vsFields.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsFields.TabIndex = 11;
			this.vsFields.Click += new System.EventHandler(this.vsFields_Click);
			// 
			// lblPeriod
			// 
			this.lblPeriod.AutoSize = true;
			this.lblPeriod.Location = new System.Drawing.Point(20, 164);
			this.lblPeriod.Name = "lblPeriod";
			this.lblPeriod.Size = new System.Drawing.Size(55, 16);
			this.lblPeriod.TabIndex = 4;
			this.lblPeriod.Text = "PERIOD";
			this.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.Text = "File";
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(30, 490);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(214, 48);
			this.btnProcess.TabIndex = 1;
			this.btnProcess.Text = "Create Database Extract";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// frmDatabaseExtract
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(742, 633);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmDatabaseExtract";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Database Extract";
			this.Load += new System.EventHandler(this.frmDatabaseExtract_Load);
			this.Activated += new System.EventHandler(this.frmDatabaseExtract_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmDatabaseExtract_KeyPress);
			this.Resize += new System.EventHandler(this.frmDatabaseExtract_Resize);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraDataEntry)).EndInit();
			this.fraDataEntry.ResumeLayout(false);
			this.fraDataEntry.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsFields)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCLabel lblPeriod;
		private System.ComponentModel.IContainer components;
		private FCLabel fcLabel3;
		private FCLabel fcLabel2;
		private FCLabel fcLabel1;
		private FCLabel fcLabel4;
		private FCButton btnProcess;
	}
}
