﻿using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using fecherFoundation.VisualBasicLayer;
using TWSharedLibrary;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmDatabaseExtract.
	/// </summary>
	public partial class frmDatabaseExtract : BaseForm
	{
		public frmDatabaseExtract()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmDatabaseExtract InstancePtr
		{
			get
			{
				return (frmDatabaseExtract)Sys.GetInstance(typeof(frmDatabaseExtract));
			}
		}

		protected frmDatabaseExtract _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               06/09/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               01/31/2005              *
		// ********************************************************
		public string auxCmbPeriodItem = "";
		clsDRWrapper rsAccounts = new clsDRWrapper();
		bool boolLoaded;
		int lngTCNumber;
		int lngRateKey;
		string strFileName = "";
		public int lngColCheck;
		public int lngColHidden;
		public int lngColDescription;
		public int lngRowAccount;
		public int lngRowName;
		public int lngRowLocation;
		public int lngRowMapLot;
		public int lngRowBookPage;
		public int lngRowOrigTax;
		public int lngRowOutstanding;
		public int lngRowAddress;
		public int lngRowYear;

		private void cmbYear_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			CheckInterestDate_2(true);
		}

		private void cmdBrowse_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// MAL@20080121: Added flag to stop CurDir from being changed
				// Tracker Reference: 12037
				// App.MainForm.CommonDialog1.Flags = cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				// MAL@20080204: Added default extension and file name
				// Tracker Reference: 12037
				App.MainForm.CommonDialog1.DefaultExt = ".csv";
				App.MainForm.CommonDialog1.FileName = "CLExtract.csv";
				App.MainForm.CommonDialog1.ShowSave();
				strFileName = Path.GetFileNameWithoutExtension(App.MainForm.CommonDialog1.FileName);
				if (Path.GetExtension(App.MainForm.CommonDialog1.FileName) == "")
				{
					strFileName += ".CSV";
					App.MainForm.CommonDialog1.FileName = App.MainForm.CommonDialog1.FileName + ".CSV";
				}
				if (strFileName != "")
				{
					txtTSFilePath.Text = App.MainForm.CommonDialog1.FileName;
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				// this will reset the default path/file name
				txtTSFilePath.Text = FCFileSystem.Statics.UserDataFolder + "\\CLExtract.csv";
			}
		}

		private void frmDatabaseExtract_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				if (!FillYearCombo())
				{
					FCMessageBox.Show("There are no billing years found in the database.", MsgBoxStyle.Exclamation, "Filling Year Combo");
					Close();
				}
				//FC:FINAL:AM: use UserData folder
				//txtTSFilePath.Text = FCFileSystem.Statics.UserDataFolder + "\\CLExtract.CSV";
				txtTSFilePath.Text = "CLExtract.CSV";
				boolLoaded = true;
			}
			else
			{
			}
		}

		private void frmDatabaseExtract_Load(object sender, System.EventArgs e)
		{			
			lngColCheck = 0;
			lngColHidden = 1;
			lngColDescription = 2;
			// rows
			lngRowAccount = 0;
			lngRowName = 1;
			lngRowYear = 2;
			lngRowLocation = 3;
			lngRowMapLot = 4;
			lngRowBookPage = 5;
			lngRowOrigTax = 6;
			lngRowOutstanding = 7;
			lngRowAddress = 8;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			FormatFieldGrid();
			FillFieldGrid();
			SetToolTipText();
		}

		private void frmDatabaseExtract_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
            FormUtilities.KeyPressHandler(e, this);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		private void frmDatabaseExtract_Resize(object sender, System.EventArgs e)
		{
			//FormatFieldGrid();
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			// create extract
			if (CreateExtract())
			{
				Close();
			}
		}

		private bool CreateExtract()
		{
			bool CreateExtract = false;
			// this will fill calculate the accounts and put it in a grid
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsLien = new clsDRWrapper();
				int lngYear = 0;
				int lngPeriod;
				double dblAmount/*unused?*/;
				double dblInterest/*unused?*/;
				string strTSFilePath = "";
				// VBto upgrade warning: dtTSDate As DateTime	OnWrite(string)
				DateTime dtTSDate;
				int lngLastRowUsed;
				lngPeriod = 0;
				if (cmbYear.SelectedIndex >= 0)
				{
					// get the year
					lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(modExtraModules.FormatYear(FCConvert.ToString(cmbYear.ItemData(cmbYear.SelectedIndex))))));
					for (lngLastRowUsed = vsFields.Rows - 1; lngLastRowUsed >= 0; lngLastRowUsed--)
					{
						App.DoEvents();
						if (Conversion.Val(vsFields.TextMatrix(lngLastRowUsed, lngColCheck)) == -1)
						{
							break;
						}
					}
					if (lngLastRowUsed < 0)
					{
						FCMessageBox.Show("Please select some fields to add to the extract.", MsgBoxStyle.Critical, "No Fields");
						if (vsFields.Enabled && vsFields.Visible)
						{
							vsFields.Focus();
						}
						return CreateExtract;
					}
					// get the period
					if (cmbPeriod.SelectedIndex == 0)
					{
						lngPeriod = 1;
					}
					else
					{
						if (cmbPeriod.SelectedIndex == 1)
						{
							lngPeriod = 2;
						}
						else
						{
							if (cmbPeriod.SelectedIndex == 2)
							{
								lngPeriod = 3;
							}
							else
							{
								lngPeriod = 4;
							}
						}
					}

                    var fullTempFileName = GetTempFileName(".csv");
                    var tempFileName = Path.GetFileName(fullTempFileName);
                    strTSFilePath = Path.GetDirectoryName(fullTempFileName);
                    //strTSFilePath = FCFileSystem.Statics.UserDataFolder;
					if (Strings.Trim(lblDate.Text) == "")
					{
						FCMessageBox.Show("There is an error finding the interest date.", MsgBoxStyle.Critical, "Invalid Date");
						return CreateExtract;
					}
					if (!Information.IsDate(lblDate.Text))
					{
						FCMessageBox.Show("There is an error finding the interest date.", MsgBoxStyle.Critical, "Invalid Date");
						return CreateExtract;
					}
					else if (DateAndTime.DateValue(lblDate.Text).ToOADate() == 0 || DateAndTime.DateValue(lblDate.Text).Year < 1900)
					{
						FCMessageBox.Show("There is an error finding the interest date.", MsgBoxStyle.Critical, "Invalid Date");
						return CreateExtract;
					}
					// get the date
					dtTSDate = FCConvert.ToDateTime(lblDate.Text);
					rsLien.OpenRecordset("SELECT * FROM LienRec", modExtraModules.strCLDatabase);
					// get all the list of accounts from the year selected
					if (modStatusPayments.Statics.boolRE)
					{
						rsAccounts.OpenRecordset("SELECT Distinct Account FROM BillingMaster WHERE BillingYear <= " + FCConvert.ToString(lngYear) + "9 AND BillingType = 'RE'");
					}
					else
					{
						rsAccounts.OpenRecordset("SELECT Distinct Account FROM BillingMaster WHERE BillingYear <= " + FCConvert.ToString(lngYear) + "9 AND BillingType = 'PP'");
					}
					if (!rsAccounts.EndOfFile())
					{
						CreateExtract = true;
						strFileName = Path.GetFileName(txtTSFilePath.Text);
						if (Strings.Trim(strFileName) == "")
						{
							strFileName = "CLEXTRCT.CSV";
						}
						if (Path.GetExtension(txtTSFilePath.Text) == "")
						{
							strFileName = Path.GetFileNameWithoutExtension(txtTSFilePath.Text) + ".CSV";
						}
						modTaxService.CreateExtractFile(rsAccounts, ref strTSFilePath, lngYear, FCConvert.ToInt16(lngPeriod), dtTSDate, false, ref lngLastRowUsed, ref lngYear, false, tempFileName);
						//FC:FINAL:AM:#i194 - download the file
						//FCUtils.DownloadAndOpen("_blank", Path.Combine(strTSFilePath, strFileName),strFileName);
                        FCUtils.Download( Path.Combine(strTSFilePath, tempFileName), strFileName);
                        
                    }
					else
					{
						FCMessageBox.Show("There are no eligible accounts.", MsgBoxStyle.Exclamation, "No Accounts");
					}
				}
				else
				{
					// no year entered
					FCMessageBox.Show("Please select a valid year.", MsgBoxStyle.Exclamation, "Select Year");
					if (cmbYear.Enabled && cmbYear.Visible)
					{
						cmbYear.Focus();
					}
				}
				return CreateExtract;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				CreateExtract = false;
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Creating Extract");
			}
			return CreateExtract;
		}

		private void ShowFrame(GroupBox fraFrame)
		{
			int wid;
			int hei;
			wid = this.Width;
			hei = this.Height;
			fraFrame.Left = FCConvert.ToInt32((wid - fraFrame.Width) / 2.0);
			fraFrame.Top = FCConvert.ToInt32((hei - fraFrame.Height) / 3.0);
			fraFrame.Visible = true;
		}

		private bool FillYearCombo()
		{
			bool FillYearCombo = false;
			// this will fill the year combo with eligible years
			clsDRWrapper rsYear = new clsDRWrapper();
			rsYear.OpenRecordset("SELECT Distinct (BillingYear / 10) AS Year FROM BillingMaster where ratekey > 0 ORDER BY BillingYear / 10 desc");
			while (!rsYear.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
				cmbYear.AddItem(FCConvert.ToString(rsYear.Get_Fields("Year")));
				// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
				cmbYear.ItemData(cmbYear.NewIndex, FCConvert.ToInt32(rsYear.Get_Fields("Year")));
				rsYear.MoveNext();
			}
			if (cmbYear.Items.Count > 0)
			{
				FillYearCombo = true;
			}
			else
			{
				FillYearCombo = false;
			}
			return FillYearCombo;
		}

		private void SetToolTipText()
		{
			// this routine will set the tool tip text for all the fields on the screen
			ToolTip1.SetToolTip(fcLabel2, "Date that the interest will be calculated until.");
			txtDate.ToolTipText = "Date that the interest will be calculated until.";
			ToolTip1.SetToolTip(lblDate, "Date that the interest will be calculated until.");
			ToolTip1.SetToolTip(fcLabel3, "Select the year to extract.");
			ToolTip1.SetToolTip(cmbYear, "Select the year to extract.");
			ToolTip1.SetToolTip(fcLabel4, "Path that the file will be created at.");
			ToolTip1.SetToolTip(txtTSFilePath, "This is the file that will be created.");
		}

		private void CheckInterestDate_2(bool boolYear)
		{
			CheckInterestDate(ref boolYear);
		}

		private void CheckInterestDate(ref bool boolYear)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this routine will check to see if a year and period are choosen
				// if so, then it will calculate the correct date and enter it into the
				// lblDate object  if not then it will blank out the date
				// VBto upgrade warning: lngYear As int	OnWrite(string)
				int lngYear = 0;
				int intPeriod = 0;
				clsDRWrapper rsDate = new clsDRWrapper();
				int intPer;
				if (cmbYear.SelectedIndex >= 0)
				{
					// is a year selected
					lngYear = FCConvert.ToInt32(cmbYear.Items[cmbYear.SelectedIndex].ToString());
					if (lngYear > 1950)
					{
						// is the year > 1950
						if (cmbPeriod.SelectedIndex == 0)
						{
							// is a period is selected?
							intPeriod = 1;
						}
						else if (cmbPeriod.SelectedIndex == 1)
						{
							intPeriod = 2;
						}
						else if (cmbPeriod.SelectedIndex == 2)
						{
							intPeriod = 3;
						}
						else if (cmbPeriod.SelectedIndex == 3)
						{
							intPeriod = 4;
						}
						else
						{
							lblDate.Text = "";
							return;
						}
					}
					else
					{
						lblDate.Text = "";
						return;
					}
				}
				else
				{
					lblDate.Text = "";
					return;
				}
				if (boolYear)
				{
					rsDate.OpenRecordset("SELECT * FROM RateRec WHERE [Year] = " + FCConvert.ToString(lngYear) + " AND (RateType = 'R' OR RateType = 'S')");
					if (!rsDate.EndOfFile())
					{
						if (rsDate.RecordCount() == 1)
						{
							lngRateKey = FCConvert.ToInt32(rsDate.Get_Fields_Int32("id"));
						}
						else if (rsDate.RecordCount() > 1)
						{
							lngRateKey = frmSelectRateKey.InstancePtr.Init(ref rsDate);
							rsDate.OpenRecordset("SELECT * FROM RateRec WHERE id = " + FCConvert.ToString(lngRateKey));
						}
					}
				}
				else
				{
					rsDate.OpenRecordset("SELECT * FROM RateRec WHERE id = " + FCConvert.ToString(lngRateKey));
				}
				// find the rate record and the dates
				if (!rsDate.EndOfFile())
				{
					if (boolYear)
					{
                        var oldPeriod = cmbPeriod.ListIndex;
                        cmbPeriod.Clear();
                        for (intPer = 1; intPer <= FCConvert.ToInt32(rsDate.Get_Fields_Int16("NumberOfPeriods"));intPer++)
                        {
                            cmbPeriod.AddItem(intPer.ToString());
                        }
                        if (oldPeriod >= 0)
                        {
                            if (oldPeriod + 1 > FCConvert.ToInt32(rsDate.Get_Fields_Int16("NumberOfPeriods")))
                            {
                                oldPeriod = 1;
                            }
                        }
                        else
                        {
                            oldPeriod = 1;
                        }
                        cmbPeriod.ListIndex = oldPeriod;
                    }
					if (intPeriod == 1)
					{
						// use the correct due date from the rate record
						lblDate.Text = Strings.Format(rsDate.Get_Fields_DateTime("DueDate1"), "MM/dd/yyyy");
					}
					else if (intPeriod == 2)
					{
						if (rsDate.Get_Fields_DateTime("DueDate2") is DateTime)
						{
							lblDate.Text = Strings.Format(rsDate.Get_Fields_DateTime("DueDate2"), "MM/dd/yyyy");
						}
						else
						{
							FCMessageBox.Show("Period " + FCConvert.ToString(intPeriod) + " has no due date please use another period.", MsgBoxStyle.Exclamation, "Invalid Due Date");
							cmbPeriod.SelectedIndex = 0;
						}
					}
					else if (intPeriod == 3)
					{
						if (rsDate.Get_Fields_DateTime("DueDate3") is DateTime)
						{
							lblDate.Text = Strings.Format(rsDate.Get_Fields_DateTime("DueDate3"), "MM/dd/yyyy");
						}
						else
						{
							FCMessageBox.Show("Period " + FCConvert.ToString(intPeriod) + " has no due date please use another period.", MsgBoxStyle.Exclamation, "Invalid Due Date");
							cmbPeriod.SelectedIndex = 0;
						}
					}
					else if (intPeriod == 4)
					{
						if (rsDate.Get_Fields_DateTime("DueDate4") is DateTime)
						{
							lblDate.Text = Strings.Format(rsDate.Get_Fields_DateTime("DueDate4"), "MM/dd/yyyy");
						}
						else
						{
							FCMessageBox.Show("Period " + FCConvert.ToString(intPeriod) + " has no due date please use another period.", MsgBoxStyle.Exclamation, "Invalid Due Date");
							cmbPeriod.SelectedIndex = 0;
						}
					}
				}
				else
				{
					FCMessageBox.Show("There is no rate record for this year.", MsgBoxStyle.Exclamation, "Missing Rate Record");
					lblDate.Text = "";
					lngRateKey = 0;
					return;
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				lblDate.Text = "";
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Calculate Interest Date");
			}
		}

		private void optPeriod_Click(int Index, object sender, System.EventArgs e)
		{
			CheckInterestDate_2(false);
		}

		private void optPeriod_Click(object sender, System.EventArgs e)
		{
			int index = cmbPeriod.SelectedIndex;
			optPeriod_Click(index, sender, e);
		}

		private void FormatFieldGrid()
		{
			int lngWid = 0;
			lngWid = vsFields.WidthOriginal;
			vsFields.Rows = 9;
			vsFields.Cols = 3;
			vsFields.ColWidth(lngColCheck, FCConvert.ToInt32(lngWid * 0.2));
			vsFields.ColWidth(lngColHidden, 0);
			vsFields.ColWidth(lngColDescription, FCConvert.ToInt32(lngWid * 0.8));
			vsFields.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			vsFields.ColDataType(lngColCheck, FCGrid.DataTypeSettings.flexDTBoolean);
		}

		private void FillFieldGrid()
		{
			vsFields.TextMatrix(lngRowAccount, lngColDescription, "Account");
			vsFields.TextMatrix(lngRowName, lngColDescription, "Name");
			vsFields.TextMatrix(lngRowYear, lngColDescription, "Year");
			vsFields.TextMatrix(lngRowLocation, lngColDescription, "Location");
			vsFields.TextMatrix(lngRowMapLot, lngColDescription, "Map Lot");
			vsFields.TextMatrix(lngRowBookPage, lngColDescription, "Book Page");
			vsFields.TextMatrix(lngRowOrigTax, lngColDescription, "Original Tax");
			vsFields.TextMatrix(lngRowOutstanding, lngColDescription, "Outstanding Tax");
			vsFields.TextMatrix(lngRowAddress, lngColDescription, "Address");
			vsFields.TextMatrix(lngRowAccount, lngColCheck, FCConvert.ToString(-1));
			vsFields.TextMatrix(lngRowName, lngColCheck, FCConvert.ToString(-1));
			vsFields.TextMatrix(lngRowYear, lngColCheck, FCConvert.ToString(-1));
			vsFields.TextMatrix(lngRowLocation, lngColCheck, FCConvert.ToString(-1));
			vsFields.TextMatrix(lngRowMapLot, lngColCheck, FCConvert.ToString(-1));
			vsFields.TextMatrix(lngRowBookPage, lngColCheck, FCConvert.ToString(-1));
			vsFields.TextMatrix(lngRowOrigTax, lngColCheck, FCConvert.ToString(-1));
			vsFields.TextMatrix(lngRowOutstanding, lngColCheck, FCConvert.ToString(-1));
			vsFields.TextMatrix(lngRowAddress, lngColCheck, FCConvert.ToString(-1));
		}

		private void vsFields_Click(object sender, EventArgs e)
		{
			if (vsFields.Col == lngColCheck)
			{
				if (Conversion.Val(vsFields.TextMatrix(vsFields.Row, vsFields.Col)) != -1)
				{
					vsFields.TextMatrix(vsFields.Row, vsFields.Col, FCConvert.ToString(-1));
				}
				else
				{
					vsFields.TextMatrix(vsFields.Row, vsFields.Col, FCConvert.ToString(0));
				}
			}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFileSaveExit_Click(sender, e);
		}

        private string GetTempFileName(string extension)
        {
            string applicationName = Path.GetFileName(Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory));
            string tempFolder = Path.Combine(FCFileSystem.Statics.UserDataFolder, "Temp");
            if (!Directory.Exists(tempFolder))
            {
                Directory.CreateDirectory(tempFolder);
            }
            Guid guid = Guid.NewGuid();
            string tempFileName = guid.ToString() + extension;
            string tempFilePath = Path.Combine(tempFolder, tempFileName);
            while (File.Exists(tempFilePath))
            {
                guid = Guid.NewGuid();
                tempFileName = guid.ToString() + extension;
                tempFilePath = Path.Combine(tempFolder, tempFileName);
            }
            return tempFilePath;
        }
    }
}
