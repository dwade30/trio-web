using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;


namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmCLEditBillInfo.
	/// </summary>
	public partial class frmCLEditBillInfo : fecherFoundation.FCForm
	{
		public frmCLEditBillInfo()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null && Wisej.Web.Application.OpenForms.Count == 0)
				_InstancePtr = this;
		}

		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCLEditBillInfo InstancePtr
		{
			get
			{
				if (_InstancePtr == null) // || _InstancePtr.IsDisposed
					_InstancePtr = new frmCLEditBillInfo();
				return _InstancePtr;
			}
		}
		protected static frmCLEditBillInfo _InstancePtr = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>



		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               04/27/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/14/2006              *
		// ********************************************************

		public string auxCmbSearchTypeItem2 = "Map / Lot";
		int lngYear;
		bool boolResultsScreen; // this is true when the result grid is being shown
		bool boolSearch;
		string strSearchString;
		bool boolDirty;
		int lngLastRow;
		string strCopyName1;
		string strCopyName2;
		string strCopyAddr1;
		string strCopyAddr2;
		string strCopyAddr3;
		string strCopyMailingAddr3;
		string strCopyMapLot;
		string strCopyBookPage;
		string strCopyStreetNumber;
		string strCopyStreetName;
		string strCopyRef1;
		bool boolCopyUseRef1;

		// these constants are for the grid columns
		// edit grid
		int lngColEDYear;
		int lngColEDName1;
		int lngColEDName2;
		int lngColEDBillkey;
		// Dim lngColMapLot                As Long

		// search grid
		int lngColAcct;
		int lngColName;
		int lngcolLocationNumber;
		int lngColLocation;
		int lngColMapLot;
		int lngColBookPage;
		int lngColStreetNumber;
		int lngColStreetName;

		int lngRowOwner;
		int lngRowSecondOwner;
		int lngRowAddress1;
		int lngRowAddress2;
		int lngRowAddress3;
		int lngRowMailingAddress3;
		int lngRowMapLot;
		int lngRowBookPage;
		int lngRowStreetNumber;
		int lngRowStreetName;
		int lngRowRef1;
		int lngRowUseRef1OnLien;

		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			if (boolResultsScreen)
			{
				vsSearch.Visible = false;
				lblSearchListInstruction.Visible = false;
				fraSearchCriteria.Visible = true;
				fraSearch.Visible = true;
				fraGetAccount.Visible = true;
				boolResultsScreen = false;
				mnuFileSave.Enabled = true;
				mnuProcessGetAccount.Enabled = true;
				mnuProcessSearch.Enabled = true;
			}
			else
			{
				cmbSearchType.SelectedIndex = 0;
				txtSearch.Text = "";
			}
		}
		public void cmdClear_Click()
		{
			cmdClear_Click(cmdClear, new System.EventArgs());
		}


		private void cmdGetAccountNumber_Click(object sender, System.EventArgs e)
		{
			int intError = 0;
			try
			{   // On Error GoTo ERROR_HANDLER
				int holdaccount = 0;
				clsDRWrapper rsCL = new clsDRWrapper();
				clsDRWrapper rsRE = new clsDRWrapper();
				string strSQL = "";
				int intAns/*unused?*/;

				lblSearchListInstruction.Visible = false;

				TryAgain:;
				if (Conversion.Val(txtGetAccountNumber.Text) != 0)
				{
					holdaccount = Convert.ToInt32(double.Parse(txtGetAccountNumber.Text));
					intError = 1;
					if (modStatusPayments.boolRE)
					{
						modExtraModules.CurrentAccountRE = holdaccount;
						strSQL = "SELECT * FROM BillingMaster WHERE Account = " + Convert.ToString(modExtraModules.CurrentAccountRE) + " AND BillingType = 'RE'";
					}
					else
					{
						modExtraModules.CurrentAccountPP = holdaccount;
						strSQL = "SELECT * FROM BillingMaster WHERE Account = " + Convert.ToString(modExtraModules.CurrentAccountPP) + " AND BillingType = 'PP'";
					}
					intError = 2;
					rsCL.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
					intError = 3;
					if (rsCL.EndOfFile() != true && rsCL.BeginningOfFile() != true)
					{
						// Create Queries on the database for the next screen to use
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						ShowEditAccount_2(Convert.ToInt32(rsCL.Get_Fields("Account")));
					}
					else
					{
						FCMessageBox.Show("The account selected does not have any billing records.", MsgBoxStyle.Information, "Error");
						return;
					}
					intError = 50;
					// set the last account field in the registry to the current account
					if (cmbRE.SelectedIndex == 0)
					{
						intError = 51;
						modRegistry.SaveKeyValue_80(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "RELastAccountNumber", modGNBas.PadToString(modExtraModules.CurrentAccountRE, 6));
					}
					else
					{
						intError = 52;
						modRegistry.SaveKeyValue_80(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "PPLastAccountNumber", modGNBas.PadToString(modExtraModules.CurrentAccountPP, 6));
					}
					intError = 53;
					mnuFileSave.Enabled = true;
					mnuProcessGetAccount.Enabled = true;
					mnuProcessSearch.Enabled = true;
				}
				else
				{
					frmWait.InstancePtr.Hide();
					FCMessageBox.Show("Please enter a valid account number.", MsgBoxStyle.Information, "Invalid Account");
					return;
				}
				return;
			}
			catch(Exception ex)
			{   // ERROR_HANDLER:
				frmWait.InstancePtr.Hide();
				FCMessageBox.Show("ERROR #" + Convert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MsgBoxStyle.Critical, "Get Account Error - " + Convert.ToString(intError));
				Close();
			}
		}
		public void cmdGetAccountNumber_Click()
		{
			cmdGetAccountNumber_Click(cmdGetAccountNumber, new System.EventArgs());
		}


		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			try
			{   // On Error GoTo ERROR_HANDLER
				int lngSelection = 0;
				string strAcctList = "";
				string strLeftOverAccts = "";
				string SQL = "";
				clsDRWrapper rs = new clsDRWrapper();
				clsDRWrapper rsBook = new clsDRWrapper();
				clsDRWrapper rsCLAccounts = new clsDRWrapper();
				string strList = "";
				string strBook = "";
				string strLastName = "";
				string strDoubleSearch = "";

				// find out which search option has been checked
				if (cmbSearchType.SelectedIndex == 0)
				{
					lngSelection = 0;
				}
				else if (cmbSearchType.SelectedIndex == 1)
				{
					lngSelection = 1;
				}
				else if (cmbSearchType.SelectedIndex == 2)
				{
					lngSelection = 2;
				}
				else if (cmbSearchType.SelectedIndex == 3)
				{
					lngSelection = 3;
				}
				else
				{
					FCMessageBox.Show("You must select a field to search by from the 'Search By' box.", MsgBoxStyle.Information, "Search By");
					return;
				}

				// make sure that the user typed search criteria in
				if (Strings.Trim(txtSearch.Text) == "")
				{
					FCMessageBox.Show("You must type a search criteria in the white box.", MsgBoxStyle.Information, "Search Criteria");
					return;
				}

				frmWait.InstancePtr.Init("Please Wait...." + "\n" + "Searching");
				App.DoEvents();

				switch (lngSelection)
				{

					case 0:
						{
							// Name

							// check for the type of account to look for Real Estate or Personal Property
							if (modStatusPayments.boolRE)
							{
								SQL = "SELECT * FROM BillingMaster INNER JOIN (SELECT ID AS BK FROM BillingMaster WHERE BillingType = 'RE' AND ((Name1 >= '" + txtSearch.Text + "  ' AND Name1 < '" + txtSearch.Text + "ZZZ') OR (Name2 >= '" + txtSearch.Text + "  ' AND Name2 < '" + txtSearch.Text + "ZZZ'))) AS List ON BillingMaster.ID = List.BK ORDER BY Name1";
							}
							else
							{
								SQL = "SELECT * FROM BillingMaster INNER JOIN (SELECT ID AS BK FROM BillingMaster WHERE BillingType = 'PP' AND ((Name1 >= '" + txtSearch.Text + "  ' AND Name1 < '" + txtSearch.Text + "ZZZ') OR (Name2 >= '" + txtSearch.Text + "  ' AND Name2 < '" + txtSearch.Text + "ZZZ'))) AS List ON BillingMaster.ID = List.BK ORDER BY Name1";
							}

							// this will create a string of accounts that will be added in the grid as well as the RE matches (for old collection files with that name)
							rsCLAccounts.OpenRecordset(SQL, modExtraModules.strCLDatabase);

							strAcctList = ",";
							while (!rsCLAccounts.EndOfFile())
							{
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								strAcctList += FCConvert.ToString(rsCLAccounts.Get_Fields("Account")) + ",";
								rsCLAccounts.MoveNext();
							}


							if (modStatusPayments.boolRE)
							{
								// If strAcctList = "" Then
								// kgk SQL = "SELECT *, RSAccount AS Account, RSName AS Name FROM Master WHERE (RSName >= '" & txtSearch.Text & "' AND RSName < '" & txtSearch.Text & "ZZZ') OR (RSSecOwner >= '" & txtSearch.Text & "' AND RSSecOwner < '" & txtSearch.Text & "ZZZ') ORDER BY RSName, RSAccount"
								SQL = "SELECT OwnerPartyID, SecOwnerPartyID, RSLocStreet, RSLocNumAlph, RSMapLot, RSAccount AS Account FROM Master";

								strDoubleSearch = "SELECT RSAccount AS Account, p1.FullNameLF AS Name, p2.FullNameLF " + "FROM Master CROSS APPLY " + modCollections.strDbCP + "GetPartyNameAndAddress(Master.OwnerPartyID, NULL, 'RE', Master.RSAccount) As p1 OUTER APPLY " + modCollections.strDbCP + "GetPartyNameAndAddress(Master.SecOwnerPartyID, NULL, 'RE', Master.RSAccount) AS p2 " + "WHERE (p1.FullNameLF >= '" + txtSearch.Text + "' AND p1.FullNameLF < '" + txtSearch.Text + "ZZZ') OR (RSSecOwner >= '" + txtSearch.Text + "' AND RSSecOwner < '" + txtSearch.Text + "ZZZ')";
								strDoubleSearch += "UNION ALL (SELECT Account, Name1 AS Name FROM BillingMaster WHERE BillingType = 'RE' AND Name1 >= '" + txtSearch.Text + "' AND Name1 < '" + txtSearch.Text + "ZZZ')";
								strDoubleSearch = "SELECT DISTINCT Account, Name FROM (" + strDoubleSearch + ") As qNew ORDER BY Name";
								// Else
								// strAcctList = Left(strAcctList, Len(strAcctList) - 1)       'remove the last comma
								// SQL = "SELECT * FROM Master WHERE (RSName >= '" & txtSearch.Text & "' AND RSName < '" & txtSearch.Text & "ZZZ') OR RSAccount IN (" & strAcctList & ") ORDER BY RSName, RSAccount"
								// End If

								rs.OpenRecordset(SQL, modExtraModules.strREDatabase);
								rs.InsertName("OwnerPartyID,SecOwnerPartyID", "Own1,Own2", false, true, true, "", false, "Own1FullName,Account", true, "(Own1FullName >= '" + txtSearch.Text + "' AND Own1FullName < '" + txtSearch.Text + "ZZZ') OR (Own2FullName >= '" + txtSearch.Text + "' AND Own2FullName < '" + txtSearch.Text + "ZZZ')");
								rs.MoveFirst();
							}
							else
							{
								// If strAcctList = "" Then
								SQL = "SELECT PartyID, Street, StreetNumber, Account FROM PPMaster";

								strDoubleSearch = "(SELECT Account, p.FullNameLF AS Name " + "FROM PPMaster CROSS APPLY " + modCollections.strDbCP + "GetPartyNameAndAddress(PPMaster.PartyID,NULL,'PP',PPMaster.Account) AS p1 " + "WHERE (p1.FullNameLF >= '" + txtSearch.Text + "' AND p1.FullNameLF < '" + txtSearch.Text + "ZZZ') ";
								strDoubleSearch += "UNION ALL (SELECT Account, Name1 AS Name FROM BillingMaster WHERE BillingType = 'PP' AND Name1 >= '" + txtSearch.Text + "' AND Name1 < '" + txtSearch.Text + "ZZZ')";
								strDoubleSearch = "SELECT DISTINCT Account, Name FROM (" + strDoubleSearch + ")) As qNew ORDER BY Name";
								// Else
								// strAcctList = Left(strAcctList, Len(strAcctList) - 1)       'remove the last comma
								// SQL = "SELECT * FROM PPMaster WHERE (Name >= '" & txtSearch.Text & "' AND Name < '" & txtSearch.Text & "ZZZ') OR Account IN (" & strAcctList & ") ORDER BY Name, Account"
								// End If

								rs.OpenRecordset(SQL, modExtraModules.strREDatabase);
								rs.InsertName("PartyID", "Own1", false, true, true, "", false, "Own1FullName,Account", true, "(Own1FullName >= '" + txtSearch.Text + "' AND Own1FullName < '" + txtSearch.Text + "ZZZ')");
								rs.MoveFirst();
							}
							strSearchString = strDoubleSearch;


							break;
						}
					case 1:
						{
							// Street Name

							rsCLAccounts.OpenRecordset("SELECT * FROM BillingMaster WHERE ID = 0", modExtraModules.strCLDatabase);
							if (modStatusPayments.boolRE)
							{ // XXXXXXXX this was ORDER BY Val(RSLocNumAlph & '')
								SQL = "SELECT OwnerPartyID, SecOwnerPartyID, RSLocStreet, RSLocNumAlph, RSMapLot, RSAccount AS Account FROM Master WHERE (RSLOCSTREET >= '" + txtSearch.Text + "' AND RSLOCSTREET < '" + txtSearch.Text + "ZZZ') ORDER BY RSLOCSTREET, RSLOCNUMALPH, RSAccount";
								rs.OpenRecordset(SQL, modExtraModules.strREDatabase);
								rs.InsertName("OwnerPartyID,SecOwnerPartyID", "Own1,Own2", false, true, true, "", false, "", true, "");
							}
							else
							{
								SQL = "SELECT PartyID, Street, StreetNumber, Account FROM PPMaster WHERE (STREET >= '" + txtSearch.Text + "' AND STREET < '" + txtSearch.Text + "ZZZ') ORDER BY STREET, CONVERT(int, STREETNUMBER), Account";
								rs.OpenRecordset(SQL, modExtraModules.strREDatabase);
								rs.InsertName("PartyID", "Own1", false, true, true, "", false, "", true, "");
							}
							strSearchString = SQL;
							break;
						}
					case 2:
						{
							// Map Lot

							rsCLAccounts.OpenRecordset("SELECT * FROM BillingMaster WHERE ID = 0", modExtraModules.strCLDatabase);
							if (modStatusPayments.boolRE)
							{
								SQL = "SELECT OwnerPartyID, SecOwnerPartyID, RSLocStreet, RSLocNumAlph, RSMapLot, RSAccount AS Account FROM Master WHERE (RSMAPLOT >= '" + txtSearch.Text + "' AND RSMAPLOT < '" + txtSearch.Text + "ZZZ') ORDER BY RSMAPLOT, RSAccount";
								rs.OpenRecordset(SQL, modExtraModules.strREDatabase);
								rs.InsertName("OwnerPartyID,SecOwnerPartyID", "Own1,Own2", false, true, true, "", false, "", true, "");
							}
							else
							{
								// SQL = "SELECT * FROM Master WHERE (RSMAPLOT >= '" & txtSearch.Text & "' AND RSMAPLOT < '" & txtSearch.Text & "ZZZ') ORDER BY RSMAPLOT, Account"
								return;
							}
							strSearchString = SQL;
							break;
						}
				} //end switch

				if (modStatusPayments.boolRE)
				{ // RE
				  // moved up   'rs.OpenRecordset SQL, strREDatabase
					if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
					{

						frmWait.InstancePtr.Init("Please Wait...." + "\n" + "Searching", true, rs.RecordCount(), true);
						this.Refresh();

						rs.MoveLast();
						rs.MoveFirst();

						// clear the listbox
						vsSearch.Rows = 1;

						FormatSearchGrid();

						rsBook.OpenRecordset("SELECT * FROM BookPage WHERE [Current] = 1 ORDER BY Line desc", modExtraModules.strREDatabase);

						// fill the listbox with all the records returned
						while (!rs.EndOfFile())
						{
							frmWait.InstancePtr.IncrementProgress();
							App.DoEvents();
							// get any book page info from
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							rsBook.FindFirstRecord2("Account,Card", FCConvert.ToString(rs.Get_Fields("Account")) + ",1", ",");
							if (rsBook.NoMatch)
							{
								strBook = "";
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
								strBook = FCConvert.ToString(rsBook.Get_Fields("Book")) + " " + FCConvert.ToString(rsBook.Get_Fields("Page"));
							}

							if (lngSelection == 0)
							{ // is this from CL and RE
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								if (Conversion.Val(rs.Get_Fields("Account")) != Conversion.Val(Strings.Left(vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct), 6)))
								{
									vsSearch.AddItem("");
									// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct, Convert.ToString(rs.Get_Fields("Account")));
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBookPage, strBook);
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocation, Convert.ToString(rs.Get_Fields_String("RSLOCSTREET")));
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngcolLocationNumber, Strings.Trim(Convert.ToString(rs.Get_Fields_String("RSLOCNUMALPH"))));
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngColMapLot, Strings.Trim(Convert.ToString(rs.Get_Fields_String("RSMAPLOT"))));
									// TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
									if ((Strings.CompareString(Strings.UCase(Convert.ToString(rs.Get_Fields("Own1FullName"))), ">=", Strings.UCase(txtSearch.Text) + "   ") && Strings.CompareString(Strings.UCase(Convert.ToString(rs.Get_Fields("Own1FullName"))), "<=", Strings.UCase(txtSearch.Text + "ZZZ"))) || lngSelection != 0)
									{
										// TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
										// TODO Get_Fields: Field [Own2FullName] not found!! (maybe it is an alias?)
										vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rs.Get_Fields("Own1FullName")) + " & " + FCConvert.ToString(rs.Get_Fields("Own2FullName")));
									}
									else
									{
										// TODO Get_Fields: Field [Own2FullName] not found!! (maybe it is an alias?)
										// TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
										vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rs.Get_Fields("Own2FullName")) + ", " + FCConvert.ToString(rs.Get_Fields("Own1FullName")));
									}
									// vsSearch.TextMatrix(vsSearch.rows - 1, lngColName) = .Get_Fields("Name") & " " & .Get_Fields("RSSECOWNER")
								}
							}
							else
							{ // data from RE
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								if (Conversion.Val(rs.Get_Fields("Account")) != Conversion.Val(Strings.Left(vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct), 6)))
								{
									// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
									// TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
									strList = FCConvert.ToString(rs.Get_Fields("Account")) + "\t" + FCConvert.ToString(rs.Get_Fields("Own1FullName")) + "\t" + Strings.Trim(Convert.ToString(rs.Get_Fields_String("RSLOCNUMALPH"))) + "\t" + FCConvert.ToString(rs.Get_Fields_String("RSLOCSTREET")) + "\t" + strBook + "\t" + Strings.Trim(Convert.ToString(rs.Get_Fields_String("RSMAPLOT")));

									vsSearch.AddItem("");
									// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct, Convert.ToString(rs.Get_Fields("Account")));
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBookPage, strBook);
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocation, Convert.ToString(rs.Get_Fields_String("RSLOCSTREET")));
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngcolLocationNumber, Strings.Trim(Convert.ToString(rs.Get_Fields_String("RSLOCNUMALPH"))));
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngColMapLot, Strings.Trim(Convert.ToString(rs.Get_Fields_String("RSMAPLOT"))));
									if (lngSelection == 0)
									{
										// TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
										// TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
										if ((Strings.CompareString(Strings.UCase(Convert.ToString(rsCLAccounts.Get_Fields("Own1FullName"))), ">=", Strings.UCase(txtSearch.Text) + "   ") && Strings.CompareString(Strings.UCase(Convert.ToString(rsCLAccounts.Get_Fields("Own1FullName"))), "<=", Strings.UCase(txtSearch.Text + "ZZZ"))) || lngSelection != 0)
										{
											// TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
											// TODO Get_Fields: Field [Own2FullName] not found!! (maybe it is an alias?)
											vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rsCLAccounts.Get_Fields("Own1FullName")) + " & " + FCConvert.ToString(rsCLAccounts.Get_Fields("Own2FullName")));
										}
										else
										{
											// TODO Get_Fields: Field [Own2FullName] not found!! (maybe it is an alias?)
											// TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
											vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rsCLAccounts.Get_Fields("Own2FullName")) + ", " + FCConvert.ToString(rsCLAccounts.Get_Fields("Own1FullName")));
										}
									}
									else
									{
										// TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
										// TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
										if ((Strings.CompareString(Strings.UCase(Convert.ToString(rs.Get_Fields("Own1FullName"))), ">=", Strings.UCase(txtSearch.Text) + "   ") && Strings.CompareString(Strings.UCase(Convert.ToString(rs.Get_Fields("Own1FullName"))), "<=", Strings.UCase(txtSearch.Text + "ZZZ"))) || lngSelection != 0)
										{
											// TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
											// TODO Get_Fields: Field [Own2FullName] not found!! (maybe it is an alias?)
											vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rs.Get_Fields("Own1FullName")) + " & " + FCConvert.ToString(rs.Get_Fields("Own2FullName")));
										}
										else
										{
											// TODO Get_Fields: Field [Own2FullName] not found!! (maybe it is an alias?)
											// TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
											vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rs.Get_Fields("Own2FullName")) + ", " + FCConvert.ToString(rs.Get_Fields("Own1FullName")));
										}
									}
									// vsSearch.TextMatrix(vsSearch.rows - 1, lngColName) = Trim(.Get_Fields("Name") & " " & .Get_Fields("RSSECOWNER"))

								}
							}
							rs.MoveNext();
						}


						// now check all of the billing records to see if any did not match the master record,
						// if they did not, then add a row for that bill as well
						rsCLAccounts.MoveFirst();
						while (!rsCLAccounts.EndOfFile())
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							rs.FindFirstRecord("Account", rsCLAccounts.Get_Fields("Account"));
							if (!rs.NoMatch)
							{
								// TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
								if (Strings.Trim(Strings.UCase(Convert.ToString(rsCLAccounts.Get_Fields_String("Name1")))) == Strings.Trim(Strings.UCase(Convert.ToString(rs.Get_Fields("Own1FullName")))) || Strings.Trim(Strings.UCase(Convert.ToString(rsCLAccounts.Get_Fields_String("Name1")))) == Strings.Trim(Strings.UCase(strLastName)))
								{
									// if this bill has the same name as its master account or has the
									// same name as the last bill then do nothing
								}
								else
								{
									// add the row
									vsSearch.AddItem("");
									// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct, Convert.ToString(rsCLAccounts.Get_Fields("Account")));
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBookPage, Convert.ToString(rsCLAccounts.Get_Fields_String("BookPage")));
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocation, Convert.ToString(rsCLAccounts.Get_Fields_String("StreetName")));
									// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngcolLocationNumber, Convert.ToString(rsCLAccounts.Get_Fields("StreetNumber")));
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngColMapLot, Strings.Trim(Convert.ToString(rsCLAccounts.Get_Fields_String("MapLot"))));
									if ((Strings.CompareString(Strings.UCase(Convert.ToString(rsCLAccounts.Get_Fields_String("Name1"))), ">=", Strings.UCase(txtSearch.Text) + "   ") && Strings.CompareString(Strings.UCase(Convert.ToString(rsCLAccounts.Get_Fields_String("Name1"))), "<=", Strings.UCase(txtSearch.Text + "ZZZ"))) || lngSelection != 0)
									{
										vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")) + " & " + FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name2")));
									}
									else
									{
										vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name2")) + ", " + FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")));
									}
									// highlight the added rows that are only past owners
									vsSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsSearch.Rows - 1, 0, vsSearch.Rows - 1, vsSearch.Cols - 1, modGlobalConstants.TRIOCOLORHIGHLIGHT);
									strLastName = Convert.ToString(rsCLAccounts.Get_Fields_String("Name1"));
								}
							}
							else if (Strings.Trim(Strings.UCase(Convert.ToString(rsCLAccounts.Get_Fields_String("Name1")))) != Strings.Trim(Strings.UCase(strLastName)))
							{
								// add the row
								vsSearch.AddItem("");
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct, Convert.ToString(rsCLAccounts.Get_Fields("Account")));
								vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBookPage, Convert.ToString(rsCLAccounts.Get_Fields_String("BookPage")));
								vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocation, Convert.ToString(rsCLAccounts.Get_Fields_String("StreetName")));
								// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
								vsSearch.TextMatrix(vsSearch.Rows - 1, lngcolLocationNumber, Convert.ToString(rsCLAccounts.Get_Fields("StreetNumber")));
								vsSearch.TextMatrix(vsSearch.Rows - 1, lngColMapLot, Strings.Trim(Convert.ToString(rsCLAccounts.Get_Fields_String("MapLot"))));
								if ((Strings.CompareString(Strings.UCase(Convert.ToString(rsCLAccounts.Get_Fields_String("Name1"))), ">=", Strings.UCase(txtSearch.Text) + "   ") && Strings.CompareString(Strings.UCase(Convert.ToString(rsCLAccounts.Get_Fields_String("Name1"))), "<=", Strings.UCase(txtSearch.Text + "ZZZ"))) || lngSelection != 0)
								{
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")) + " & " + FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name2")));
								}
								else
								{
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name2")) + ", " + FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")));
								}
								// highlight the added rows that are only past owners
								vsSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsSearch.Rows - 1, 0, vsSearch.Rows - 1, vsSearch.Cols - 1, modGlobalConstants.TRIOCOLORHIGHLIGHT);
								strLastName = Convert.ToString(rsCLAccounts.Get_Fields_String("Name1"));
							}

							rsCLAccounts.MoveNext();
						}

						// set the height of the grid
						if ((vsSearch.Rows * vsSearch.RowHeight(0)) + 70 < (this.Height - vsSearch.Top) - 1000)
						{
							vsSearch.Height = (vsSearch.Rows * vsSearch.RowHeight(0)) + 70;
							vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
						}
						else
						{
							vsSearch.Height = (this.Height - vsSearch.Top) - 1000;
							vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
						}

						vsSearch.Visible = true;
						fraSearch.Visible = false;
						fraGetAccount.Visible = false;
						fraSearchCriteria.Visible = false;

						boolResultsScreen = true;
						mnuProcessGetAccount.Enabled = false;
						mnuProcessSearch.Enabled = false;

						if (lngSelection == 0)
						{
							// force the name order
							vsSearch.Col = lngColName;
							vsSearch.Sort = FCGrid.SortSettings.flexSortGenericAscending;
						}

						frmWait.InstancePtr.Hide();
					}
					else
					{
						frmWait.InstancePtr.Hide();
						FCMessageBox.Show("No results were matched to the criteria.  Please try again.", MsgBoxStyle.Information, "No RE Results");
					}

				}
				else
				{ // PP
					FormatSearchGrid();

					rs.OpenRecordset(SQL, modExtraModules.strPPDatabase);

					if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
					{
						frmWait.InstancePtr.Init("Please Wait...." + "\n" + "Searching", true, rs.RecordCount(), true);
						this.Refresh();

						rs.MoveLast();
						rs.MoveFirst();
						// clear the listbox
						vsSearch.Rows = 1;

						// fill the listbox with all the records returned
						while (!rs.EndOfFile())
						{
							frmWait.InstancePtr.IncrementProgress();
							App.DoEvents();
							if (lngSelection == 0)
							{ // is this from CL and PP
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								if (Conversion.Val(rs.Get_Fields("Account")) != Conversion.Val(Strings.Left(vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct), 6)))
								{
									vsSearch.AddItem("");
									// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct, Convert.ToString(rs.Get_Fields("Account")));
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBookPage, ""); // rsCLAccounts.Get_Fields_String("BookPage")
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocation, Convert.ToString(rs.Get_Fields_String("Street")));
									// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngcolLocationNumber, Convert.ToString(rs.Get_Fields("StreetNumber")));
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngColMapLot, ""); // Trim(rsCLAccounts.Get_Fields_String("MapLot"))
									// TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, Convert.ToString(rs.Get_Fields("Own1FullName")));
								}
							}
							rs.MoveNext();
						}

						// now check all of the billing records to see if any did not match the master record,
						// if they did not, then add a row for that bill as well
						rsCLAccounts.MoveFirst();
						while (!rsCLAccounts.EndOfFile())
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							rs.FindFirstRecord("Account", rsCLAccounts.Get_Fields("Account"));
							if (!rs.NoMatch)
							{
								// TODO Get_Fields: Field [Own1FullName] not found!! (maybe it is an alias?)
								if (Strings.Trim(Strings.UCase(Convert.ToString(rsCLAccounts.Get_Fields_String("Name1")))) == Strings.Trim(Strings.UCase(Convert.ToString(rs.Get_Fields("Own1FullName")))) || Strings.Trim(Strings.UCase(Convert.ToString(rsCLAccounts.Get_Fields_String("Name1")))) == Strings.Trim(Strings.UCase(strLastName)))
								{
									// if this bill has the same name as its master account or has the
									// same name as the last bill then do nothing
								}
								else
								{
									// add the row
									vsSearch.AddItem("");
									// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct, Convert.ToString(rsCLAccounts.Get_Fields("Account")));
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBookPage, Convert.ToString(rsCLAccounts.Get_Fields_String("BookPage")));
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocation, Convert.ToString(rsCLAccounts.Get_Fields_String("StreetName")));
									// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngcolLocationNumber, Convert.ToString(rsCLAccounts.Get_Fields("StreetNumber")));
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngColMapLot, Strings.Trim(Convert.ToString(rsCLAccounts.Get_Fields_String("MapLot"))));
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")) + " " + FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name2")));
									// highlight the added rows that are only past owners
									vsSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsSearch.Rows - 1, 0, vsSearch.Rows - 1, vsSearch.Cols - 1, modGlobalConstants.TRIOCOLORHIGHLIGHT);
								}
							}
							else if (Strings.Trim(Strings.UCase(Convert.ToString(rsCLAccounts.Get_Fields_String("Name1")))) != Strings.Trim(Strings.UCase(strLastName)))
							{
								// add the row
								vsSearch.AddItem("");
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct, Convert.ToString(rsCLAccounts.Get_Fields("Account")));
								vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBookPage, ""); // rsCLAccounts.Get_Fields_String("BookPage")
								vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocation, Convert.ToString(rsCLAccounts.Get_Fields_String("StreetName")));
								// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
								vsSearch.TextMatrix(vsSearch.Rows - 1, lngcolLocationNumber, Convert.ToString(rsCLAccounts.Get_Fields("StreetNumber")));
								vsSearch.TextMatrix(vsSearch.Rows - 1, lngColMapLot, ""); // Trim(rsCLAccounts.Get_Fields_String("MapLot"))
								vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")) + " " + FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name2")));
								// highlight the added rows that are only past owners
								vsSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsSearch.Rows - 1, 0, vsSearch.Rows - 1, vsSearch.Cols - 1, modGlobalConstants.TRIOCOLORHIGHLIGHT);
							}

							strLastName = Convert.ToString(rsCLAccounts.Get_Fields_String("Name1"));
							rsCLAccounts.MoveNext();
						}

						// set the height of the grid
						if ((vsSearch.Rows * vsSearch.RowHeight(0)) + 70 < (this.Height - vsSearch.Top) - 1000)
						{
							vsSearch.Height = (vsSearch.Rows * vsSearch.RowHeight(0)) + 70;
							vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
						}
						else
						{
							vsSearch.Height = (this.Height - vsSearch.Top) - 1000;
							vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
						}

						vsSearch.Visible = true;
						boolResultsScreen = true;
						mnuProcessGetAccount.Enabled = false;
						mnuProcessSearch.Enabled = false;
						fraSearch.Visible = false;
						frmWait.InstancePtr.Hide();
					}
					else
					{
						frmWait.InstancePtr.Hide();
						FCMessageBox.Show("No results were matched to the criteria.  Please try again.", MsgBoxStyle.Information, "No PP Results");
					}

				}

				return;
			}
			catch(Exception ex)
			{   // ERROR_HANDLER:
				frmWait.InstancePtr.Hide();
				FCMessageBox.Show("ERROR #" + Convert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MsgBoxStyle.Critical, "Open Recordset ERROR");
				Close();
			}
		}
		public void cmdSearch_Click()
		{
			cmdSearch_Click(cmdSearch, new System.EventArgs());
		}


		private void frmCLEditBillInfo_Activated(object sender, System.EventArgs e)
		{
			try
			{   // On Error GoTo ErrorTag
				// If FormExist(Me) Then Exit Sub

				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				lblInstructions1.Text = "Please enter the account number or hit Enter to view the account shown.";

				if (modStatusPayments.boolRE)
				{
					// set the menu options on for RE

					if (modExtraModules.IsThisCR())
					{
						if (modCollections.gboolShowLastCLAccountInCR)
						{
							txtGetAccountNumber.Text = Convert.ToString(modExtraModules.CurrentAccountRE);
						}
					}
					else
					{
						txtGetAccountNumber.Text = Convert.ToString(modExtraModules.CurrentAccountRE);
					}
					cmbRE.SelectedIndex = 0;
				}
				else
				{
					if (modExtraModules.IsThisCR())
					{
						if (modCollections.gboolShowLastCLAccountInCR)
						{
							txtGetAccountNumber.Text = Convert.ToString(modExtraModules.CurrentAccountPP);
						}
					}
					else
					{
						txtGetAccountNumber.Text = Convert.ToString(modExtraModules.CurrentAccountPP);
					}
					cmbRE.SelectedIndex = 1;
				}

				ShowSearch();

				txtGetAccountNumber.SelectionStart = 0;
				txtGetAccountNumber.SelectionLength = txtGetAccountNumber.Text.Length;
				txtGetAccountNumber.Focus();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;

				App.DoEvents();
				return;
			}
			catch(Exception ex)
			{   // ErrorTag:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
		}

		private void frmCLEditBillInfo_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = ((int)e.KeyData) / 0x10000;

			if (fraEditInfo.Visible)
			{

				if (KeyCode == Keys.C)
				{
					if (Shift == 2)
					{
						CopyInformation();
					}
				}
				else if (KeyCode == Keys.V)
				{
					if (Shift == 2)
					{
						PasteInformation();
					}
				}
			}
		}

		private void frmCLEditBillInfo_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);

			if (KeyAscii == 27)
			{
				if (vsSearch.Visible == true)
				{
					vsSearch.Visible = false;
					fraSearchCriteria.Visible = true;
					fraSearch.Visible = true;
					fraGetAccount.Visible = true;
					lblSearchListInstruction.Visible = false;
					boolSearch = false;
				}
				else if (fraEditInfo.Visible)
				{
					fraEditInfo.Visible = false;
					fraSearch.Visible = true;
					fraSearchCriteria.Visible = true;
					fraGetAccount.Visible = true;
					mnuFileSave.Visible = false;
					mnuProcessSearch.Visible = true;
					mnuProcessGetAccount.Text = "&Process";
					lblSearchListInstruction.Visible = false;
					boolSearch = false;
				}
				else
				{
					KeyAscii = 0;
					Close();
				}
			}
			else if (KeyAscii == 13)
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void frmCLEditBillInfo_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCLEditBillInfo.Icon	= "frmCLEditBillInfo.frx":0000";
			//frmCLEditBillInfo.ScaleWidth	= 9045;
			//frmCLEditBillInfo.ScaleHeight	= 7170;
			//frmCLEditBillInfo.LinkTopic	= "Form1";
			//frmCLEditBillInfo.LockControls	= -1  'True;

			//Font.Size	= "8.25";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;

			//vsEditInfo.BackColor	= "-2147483643";
			//vsEditInfo.ForeColor	= "-2147483640";
			//vsEditInfo.BorderStyle	= 1;
			//vsEditInfo.FillStyle	= 0;
			//vsEditInfo.Appearance	= 1;
			//vsEditInfo.GridLines	= 1;
			//vsEditInfo.WordWrap	= 0;
			//vsEditInfo.ScrollBars	= 2;
			//vsEditInfo.RightToLeft	= 0;
			//vsEditInfo._cx	= 10398;
			//vsEditInfo._cy	= 4978;
			//vsEditInfo._ConvInfo	= 1;
			//vsEditInfo.MousePointer	= 0;
			//vsEditInfo.BackColorFixed	= -2147483633;
			//vsEditInfo.ForeColorFixed	= -2147483630;
			//vsEditInfo.BackColorSel	= -2147483635;
			//vsEditInfo.ForeColorSel	= -2147483634;
			//vsEditInfo.BackColorBkg	= -2147483636;
			//vsEditInfo.BackColorAlternate	= -2147483643;
			//vsEditInfo.GridColor	= -2147483633;
			//vsEditInfo.GridColorFixed	= -2147483632;
			//vsEditInfo.TreeColor	= -2147483632;
			//vsEditInfo.FloodColor	= 192;
			//vsEditInfo.SheetBorder	= -2147483642;
			//vsEditInfo.FocusRect	= 1;
			//vsEditInfo.HighLight	= 1;
			//vsEditInfo.AllowSelection	= 0   'False;
			//vsEditInfo.AllowBigSelection	= 0   'False;
			//vsEditInfo.AllowUserResizing	= 0;
			//vsEditInfo.SelectionMode	= 0;
			//vsEditInfo.GridLinesFixed	= 2;
			//vsEditInfo.GridLineWidth	= 1;
			//vsEditInfo.RowHeightMin	= 0;
			//vsEditInfo.RowHeightMax	= 0;
			//vsEditInfo.ColWidthMin	= 0;
			//vsEditInfo.ColWidthMax	= 0;
			//vsEditInfo.ExtendLastCol	= -1  'True;
			//vsEditInfo.FormatString	= "";
			//vsEditInfo.ScrollTrack	= 0   'False;
			//vsEditInfo.ScrollTips	= 0   'False;
			//vsEditInfo.MergeCells	= 0;
			//vsEditInfo.MergeCompare	= 0;
			//vsEditInfo.AutoResize	= -1  'True;
			//vsEditInfo.AutoSizeMode	= 0;
			//vsEditInfo.AutoSearch	= 0;
			//vsEditInfo.AutoSearchDelay	= 2;
			//vsEditInfo.MultiTotals	= -1  'True;
			//vsEditInfo.SubtotalPosition	= 1;
			//vsEditInfo.OutlineBar	= 0;
			//vsEditInfo.OutlineCol	= 0;
			//vsEditInfo.Ellipsis	= 0;
			//vsEditInfo.ExplorerBar	= 0;
			//vsEditInfo.PicturesOver	= 0   'False;
			//vsEditInfo.PictureType	= 0;
			//vsEditInfo.TabBehavior	= 0;
			//vsEditInfo.OwnerDraw	= 0;
			//vsEditInfo.ShowComboButton	= -1  'True;
			//vsEditInfo.TextStyle	= 0;
			//vsEditInfo.TextStyleFixed	= 0;
			//vsEditInfo.OleDragMode	= 0;
			//vsEditInfo.OleDropMode	= 0;
			//vsEditInfo.ComboSearch	= 3;
			//vsEditInfo.AutoSizeMouse	= -1  'True;
			//vsEditInfo.AllowUserFreezing	= 0;
			//vsEditInfo.BackColorFrozen	= 0;
			//vsEditInfo.ForeColorFrozen	= 0;
			//vsEditInfo.WallPaperAlignment	= 9;

			//vsYearInfo.BackColor	= "-2147483643";
			//vsYearInfo.ForeColor	= "-2147483640";
			//vsYearInfo.BorderStyle	= 1;
			//vsYearInfo.FillStyle	= 0;
			//vsYearInfo.Appearance	= 1;
			//vsYearInfo.GridLines	= 1;
			//vsYearInfo.WordWrap	= 0;
			//vsYearInfo.ScrollBars	= 2;
			//vsYearInfo.RightToLeft	= 0;
			//vsYearInfo._cx	= 12197;
			//vsYearInfo._cy	= 3789;
			//vsYearInfo._ConvInfo	= 1;
			//vsYearInfo.MousePointer	= 0;
			//vsYearInfo.BackColorFixed	= -2147483633;
			//vsYearInfo.ForeColorFixed	= -2147483630;
			//vsYearInfo.BackColorSel	= -2147483635;
			//vsYearInfo.ForeColorSel	= -2147483634;
			//vsYearInfo.BackColorBkg	= -2147483636;
			//vsYearInfo.BackColorAlternate	= -2147483643;
			//vsYearInfo.GridColor	= -2147483633;
			//vsYearInfo.GridColorFixed	= -2147483632;
			//vsYearInfo.TreeColor	= -2147483632;
			//vsYearInfo.FloodColor	= 192;
			//vsYearInfo.SheetBorder	= -2147483642;
			//vsYearInfo.FocusRect	= 1;
			//vsYearInfo.HighLight	= 1;
			//vsYearInfo.AllowSelection	= 0   'False;
			//vsYearInfo.AllowBigSelection	= 0   'False;
			//vsYearInfo.AllowUserResizing	= 0;
			//vsYearInfo.SelectionMode	= 1;
			//vsYearInfo.GridLinesFixed	= 2;
			//vsYearInfo.GridLineWidth	= 1;
			//vsYearInfo.RowHeightMin	= 0;
			//vsYearInfo.RowHeightMax	= 0;
			//vsYearInfo.ColWidthMin	= 0;
			//vsYearInfo.ColWidthMax	= 0;
			//vsYearInfo.ExtendLastCol	= 0   'False;
			//vsYearInfo.FormatString	= "";
			//vsYearInfo.ScrollTrack	= 0   'False;
			//vsYearInfo.ScrollTips	= 0   'False;
			//vsYearInfo.MergeCells	= 0;
			//vsYearInfo.MergeCompare	= 0;
			//vsYearInfo.AutoResize	= -1  'True;
			//vsYearInfo.AutoSizeMode	= 0;
			//vsYearInfo.AutoSearch	= 0;
			//vsYearInfo.AutoSearchDelay	= 2;
			//vsYearInfo.MultiTotals	= -1  'True;
			//vsYearInfo.SubtotalPosition	= 1;
			//vsYearInfo.OutlineBar	= 0;
			//vsYearInfo.OutlineCol	= 0;
			//vsYearInfo.Ellipsis	= 0;
			//vsYearInfo.ExplorerBar	= 0;
			//vsYearInfo.PicturesOver	= 0   'False;
			//vsYearInfo.PictureType	= 0;
			//vsYearInfo.TabBehavior	= 0;
			//vsYearInfo.OwnerDraw	= 0;
			//vsYearInfo.ShowComboButton	= -1  'True;
			//vsYearInfo.TextStyle	= 0;
			//vsYearInfo.TextStyleFixed	= 0;
			//vsYearInfo.OleDragMode	= 0;
			//vsYearInfo.OleDropMode	= 0;
			//vsYearInfo.ComboSearch	= 3;
			//vsYearInfo.AutoSizeMouse	= -1  'True;
			//vsYearInfo.AllowUserFreezing	= 0;
			//vsYearInfo.BackColorFrozen	= 0;
			//vsYearInfo.ForeColorFrozen	= 0;
			//vsYearInfo.WallPaperAlignment	= 9;

			//vsSearch.BackColor	= "-2147483643";
			//vsSearch.ForeColor	= "-2147483640";
			//vsSearch.BorderStyle	= 1;
			//vsSearch.FillStyle	= 0;
			//vsSearch.Appearance	= 1;
			//vsSearch.GridLines	= 1;
			//vsSearch.WordWrap	= 0;
			//vsSearch.ScrollBars	= 2;
			//vsSearch.RightToLeft	= 0;
			//vsSearch._cx	= 2990;
			//vsSearch._cy	= 1466;
			//vsSearch._ConvInfo	= 1;
			//vsSearch.MousePointer	= 0;
			//vsSearch.BackColorFixed	= -2147483633;
			//vsSearch.ForeColorFixed	= -2147483630;
			//vsSearch.BackColorSel	= -2147483635;
			//vsSearch.ForeColorSel	= -2147483634;
			//vsSearch.BackColorBkg	= -2147483636;
			//vsSearch.BackColorAlternate	= -2147483643;
			//vsSearch.GridColor	= -2147483633;
			//vsSearch.GridColorFixed	= -2147483632;
			//vsSearch.TreeColor	= -2147483632;
			//vsSearch.FloodColor	= 192;
			//vsSearch.SheetBorder	= -2147483642;
			//vsSearch.FocusRect	= 1;
			//vsSearch.HighLight	= 1;
			//vsSearch.AllowSelection	= -1  'True;
			//vsSearch.AllowBigSelection	= -1  'True;
			//vsSearch.AllowUserResizing	= 1;
			//vsSearch.SelectionMode	= 0;
			//vsSearch.GridLinesFixed	= 2;
			//vsSearch.GridLineWidth	= 1;
			//vsSearch.RowHeightMin	= 0;
			//vsSearch.RowHeightMax	= 0;
			//vsSearch.ColWidthMin	= 0;
			//vsSearch.ColWidthMax	= 0;
			//vsSearch.ExtendLastCol	= 0   'False;
			//vsSearch.FormatString	= "";
			//vsSearch.ScrollTrack	= -1  'True;
			//vsSearch.ScrollTips	= 0   'False;
			//vsSearch.MergeCells	= 0;
			//vsSearch.MergeCompare	= 0;
			//vsSearch.AutoResize	= -1  'True;
			//vsSearch.AutoSizeMode	= 0;
			//vsSearch.AutoSearch	= 0;
			//vsSearch.AutoSearchDelay	= 2;
			//vsSearch.MultiTotals	= -1  'True;
			//vsSearch.SubtotalPosition	= 1;
			//vsSearch.OutlineBar	= 0;
			//vsSearch.OutlineCol	= 0;
			//vsSearch.Ellipsis	= 0;
			//vsSearch.ExplorerBar	= 1;
			//vsSearch.PicturesOver	= 0   'False;
			//vsSearch.PictureType	= 0;
			//vsSearch.TabBehavior	= 0;
			//vsSearch.OwnerDraw	= 0;
			//vsSearch.ShowComboButton	= -1  'True;
			//vsSearch.TextStyle	= 0;
			//vsSearch.TextStyleFixed	= 0;
			//vsSearch.OleDragMode	= 0;
			//vsSearch.OleDropMode	= 0;
			//vsSearch.DataMode	= 0;
			//vsSearch.VirtualData	= -1  'True;
			//vsSearch.ComboSearch	= 3;
			//vsSearch.AutoSizeMouse	= -1  'True;
			//vsSearch.AllowUserFreezing	= 0;
			//vsSearch.BackColorFrozen	= 0;
			//vsSearch.ForeColorFrozen	= 0;
			//vsSearch.WallPaperAlignment	= 9;

			//vsElasticLight1.OleObjectBlob	= "frmCLEditBillInfo.frx":058A";
			//End Unmaped Properties

			try
			{   // On Error GoTo ERROR_HANDLER
				string strTemp = "";

				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
				// this just sets the columns constant values
				// edit grid
				lngColEDBillkey = 0;
				lngColEDYear = 1;
				lngColEDName1 = 2;
				lngColEDName2 = 3;

				// search grid
				lngColAcct = 0;
				lngColName = 1;
				lngcolLocationNumber = 2;
				lngColLocation = 3;
				lngColBookPage = 4;
				lngColMapLot = 5;

				lngRowOwner = 0;
				lngRowSecondOwner = 1;
				lngRowAddress1 = 2;
				lngRowAddress2 = 3;
				lngRowMailingAddress3 = 4;
				lngRowAddress3 = 5;
				lngRowMapLot = 6;
				lngRowBookPage = 7;
				lngRowStreetNumber = 8;
				lngRowStreetName = 9;
				lngRowRef1 = 10;
				lngRowUseRef1OnLien = 11;

				strSearchString = "";

				if (modStatusPayments.boolRE)
				{
					if (cmbSearchType.Items[2].ToString() != auxCmbSearchTypeItem2)
					{
						cmbSearchType.Items.Insert(2, auxCmbSearchTypeItem2);
					}
				}
				else
				{
					cmbSearchType.Items.RemoveAt(2);
				}

				modGlobalFunctions.SetTRIOColors(this);
				return;
			}
			catch(Exception ex)
			{   // ERROR_HANDLER:
				FCMessageBox.Show("Error #" + Convert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MsgBoxStyle.Critical, "Form Load Error");
			}
		}

		private void frmCLEditBillInfo_Resize(object sender, System.EventArgs e)
		{
			if (fraSearch.Visible == true && vsSearch.Visible == false)
			{
				ShowSearch();
			}
			else if (vsSearch.Visible == true)
			{
				FormatSearchGrid();
				// set the height of the grid
				if ((vsSearch.Rows * vsSearch.RowHeight(0)) + 70 < (this.Height - vsSearch.Top) - 1000)
				{
					vsSearch.Height = (vsSearch.Rows * vsSearch.RowHeight(0)) + 70;
					vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				}
				else
				{
					vsSearch.Height = Math.Abs((this.Height - vsSearch.Top) - 1000);
					vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
				}
			}
			else
			{
				AdjustGridHeight();
			}
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			SaveInformation();
		}

		private void vsEditInfo_AfterEdit(int Row, int Col)
		{
			boolDirty = true;
		}

		private void vsEditInfo_ChangeEdit()
		{
			boolDirty = true;
		}

		private void vsEditInfo_GotFocus()
		{

			if (vsEditInfo.Col > 0)
			{
				vsEditInfo.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsEditInfo.EditCell();
			}
			else
			{
				vsEditInfo.Editable = FCGrid.EditableSettings.flexEDNone;
			}

			vsEditInfo.Row = 1;
			vsEditInfo.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;

		}

		private void vsEditInfo_KeyDownEdit(int Row, int Col, ref short KeyCode, int Shift)
		{
			// Select Case KeyCode
			// Case vbKeyC
			// If Shift = 2 And Col > 0 Then
			// KeyCode = 0
			// CopyInformation Row
			// End If
			// End Select
		}

		private void vsEditInfo_MouseMove(ref short Button, ref short Shift, ref float x, ref float y)
		{
			int lngMR;

			lngMR = vsEditInfo.MouseRow;

			if (lngMR == lngRowAddress1 ||
				lngMR == lngRowAddress2 ||
				lngMR == lngRowAddress3 ||
				lngMR == lngRowMailingAddress3)
			{
				ToolTip1.SetToolTip(vsEditInfo, "");
			}
			else if (lngMR == lngRowBookPage)
			{
				ToolTip1.SetToolTip(vsEditInfo, "Book and Page");
			}
			else if (lngMR == lngRowMapLot)
			{
				ToolTip1.SetToolTip(vsEditInfo, "Map and Lot");
			}
			else if (lngMR == lngRowOwner)
			{
				ToolTip1.SetToolTip(vsEditInfo, "");
			}
			else if (lngMR == lngRowRef1)
			{
				ToolTip1.SetToolTip(vsEditInfo, "Reference 1");
			}
			else if (lngMR == lngRowSecondOwner)
			{
				ToolTip1.SetToolTip(vsEditInfo, "");
			}
			else if (lngMR == lngRowStreetName)
			{
				ToolTip1.SetToolTip(vsEditInfo, "");
			}
			else if (lngMR == lngRowStreetNumber)
			{
				ToolTip1.SetToolTip(vsEditInfo, "");
			}
			else if (lngMR == lngRowUseRef1OnLien)
			{
				ToolTip1.SetToolTip(vsEditInfo, "Use the Ref 1 field instead of the Book and Page fields on 30 DN and Liens.");
			}
		}

		private void vsEditInfo_RowColChange(object sender, EventArgs e)
		{

			if (vsEditInfo.Col > 0)
			{
				if (vsEditInfo.Row == lngRowUseRef1OnLien)
				{
					vsEditInfo.ComboList = "1;Yes|2;No";
				}
				else
				{
					vsEditInfo.ComboList = "";
				}
				vsEditInfo.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsEditInfo.EditCell();
			}
			else
			{
				vsEditInfo.Editable = FCGrid.EditableSettings.flexEDNone;
			}

			if (vsEditInfo.Row == vsEditInfo.Rows - 1)
			{
				vsEditInfo.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				vsEditInfo.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}

		}

		private void vsSearch_AfterUserResize(int Row, int Col)
		{
			int intCT;
			int lngWid = 0;

			for (intCT = 0; intCT <= vsSearch.Cols - 1; intCT++)
			{
				lngWid += vsSearch.ColWidth(intCT);
			}

			if (lngWid > vsSearch.Width)
			{
				vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollBoth;
				// set the height of the grid
				if ((vsSearch.Rows * vsSearch.RowHeight(0)) + 70 < (this.Height - vsSearch.Top) - 1000)
				{
					vsSearch.Height = ((vsSearch.Rows + 1) * vsSearch.RowHeight(0));
					vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollHorizontal;
				}
				else
				{
					vsSearch.Height = (this.Height - vsSearch.Top) - 1000;
					vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollBoth;
				}
			}
			else
			{
				// set the height of the grid
				if ((vsSearch.Rows * vsSearch.RowHeight(0)) + 70 < (this.Height - vsSearch.Top) - 1000)
				{
					vsSearch.Height = (vsSearch.Rows * vsSearch.RowHeight(0)) + 70;
					vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				}
				else
				{
					vsSearch.Height = (this.Height - vsSearch.Top) - 1000;
					vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
				}
			}
		}

		private void vsSearch_BeforeSort(int Col, ref short Order)
		{
			if (Col == lngColLocation || Col == lngcolLocationNumber)
			{
				if (vsSearch.Rows > 1)
				{
					vsSearch.Select(1, lngcolLocationNumber, 1, lngColLocation);
				}
			}
		}

		private void vsSearch_Click()
		{
			txtGetAccountNumber.Text = Strings.Left(vsSearch.TextMatrix(vsSearch.Row, lngColAcct), 6);
		}

		private void vsSearch_DblClick(object sender, EventArgs e)
		{
			if (vsSearch.Row > 0)
			{
				txtGetAccountNumber.Text = Strings.Left(vsSearch.TextMatrix(vsSearch.Row, lngColAcct), 6);
				cmdGetAccountNumber_Click();
			}
		}

		private void vsSearch_KeyDown(ref Keys KeyCode, ref short Shift)
		{
			// captures the return key to accept the account highlighted in the listbox
			if (KeyCode == Keys.Return)
			{
				cmdGetAccountNumber_Click();
			}
		}

		private void mnuProcessClearSearch_Click(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}

		private void mnuProcessGetAccount_Click(object sender, System.EventArgs e)
		{
			if (fraEditInfo.Visible)
			{
				// this is a save and exit
				SaveInformation();
				Close();
			}
			else
			{
				if (Strings.Trim(txtSearch.Text) != "")
				{
					cmdSearch_Click();
				}
				else
				{
					cmdGetAccountNumber_Click();
				}
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSearch_Click(object sender, System.EventArgs e)
		{
			if (fraGetAccount.Visible)
			{
				cmdSearch_Click();
			}
			else
			{
				// go back to that screen
				if (boolDirty)
				{

					if (FCMessageBox.Show("Changes have been made.  Would you like to save the information?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Save Changes") == DialogResult.Yes)
					{
						SaveInformation();
					}
					else if (FCMessageBox.Show("Changes have been made.  Would you like to save the information?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Save Changes") == DialogResult.No)
					{
						// do nothing
					}
					else if (FCMessageBox.Show("Changes have been made.  Would you like to save the information?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Save Changes") == DialogResult.Cancel)
					{
						return;
					}
				}

				mnuProcessGetAccount.Text = "&Process";
				mnuProcessClearSearch.Visible = true;
				fraSearch.Visible = true;
				fraGetAccount.Visible = true;
				fraSearchCriteria.Visible = true;
				fraEditInfo.Visible = false;
			}
		}

		private void optPP_Click(object sender, System.EventArgs e)
		{
			if (modExtraModules.IsThisCR() && !modCollections.gboolShowLastCLAccountInCR)
			{

			}
			else
			{
				txtGetAccountNumber.Text = Convert.ToString(modExtraModules.CurrentAccountPP);
			}
		}

		private void optRE_Click(object sender, System.EventArgs e)
		{
			if (modExtraModules.IsThisCR() && !modCollections.gboolShowLastCLAccountInCR)
			{

			}
			else
			{
				txtGetAccountNumber.Text = Convert.ToString(modExtraModules.CurrentAccountRE);
			}
		}

		private void txtGetAccountNumber_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = ((int)e.KeyData) / 0x10000;

			if (KeyCode == Keys.Return)
			{
				KeyCode = 0;
				cmdGetAccountNumber_Click();
			}
		}

		private void txtGetAccountNumber_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);

			if (KeyAscii < 48 || KeyAscii > 57)
			{
				if (KeyAscii == 8)
				{

				}
				else
				{
					KeyAscii = 0;
				}
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtSearch_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = ((int)e.KeyData) / 0x10000;

			if (KeyCode == Keys.Return)
			{
				KeyCode = 0;
				cmdSearch_Click();
			}
		}

		private void ShowSearch()
		{
			// this will align the Search Screen and Show it
			boolSearch = true;


			fraSearch.Left = Convert.ToInt32((this.Width - fraSearch.Width) / 2.0);
			fraSearch.Top = Convert.ToInt32((this.Height - fraSearch.Height) / 2.0);
			fraSearch.Visible = true;

		}

		private void vsSearch_MouseMove(ref short Button, ref short Shift, ref float x, ref float y)
		{
			int lngR;
			int lngC;

			lngC = vsSearch.MouseCol;
			lngR = vsSearch.MouseRow;

			// vssearch.ToolTipText =
		}

		private void vsSearch_RowColChange(object sender, EventArgs e)
		{
			if (Conversion.Val(Strings.Left(vsSearch.TextMatrix(vsSearch.Row, lngColAcct), 6)) != 0)
			{
				txtGetAccountNumber.Text = Strings.Left(vsSearch.TextMatrix(vsSearch.Row, lngColAcct), 6);
			}
		}

		private void FormatSearchGrid()
		{
			int lngWid;

			vsSearch.Cols = 6;

			// set the height of the grid
			vsSearch.Height = Convert.ToInt32(this.Height * 0.8);
			vsSearch.Width = Convert.ToInt32(this.Width * 0.9);
			vsSearch.Left = Convert.ToInt32(((this.Width - vsSearch.Width) / 2.0));
			vsSearch.Top = Convert.ToInt32(this.Height * 0.1);

			vsSearch.ExtendLastCol = true;
			lngWid = vsSearch.Width;

			// set the column widths
			if (modStatusPayments.boolRE)
			{
				vsSearch.ColWidth(lngColAcct, Convert.ToInt32(lngWid * 0.09));
				vsSearch.ColWidth(lngColName, Convert.ToInt32(lngWid * 0.32));
				vsSearch.ColWidth(lngcolLocationNumber, Convert.ToInt32(lngWid * 0.06));
				vsSearch.ColWidth(lngColLocation, Convert.ToInt32(lngWid * 0.31));
				vsSearch.ColWidth(lngColBookPage, 0);
				vsSearch.ColWidth(lngColMapLot, Convert.ToInt32(lngWid * 0.18));
			}
			else
			{
				vsSearch.ColWidth(lngColAcct, Convert.ToInt32(lngWid * 0.09));
				vsSearch.ColWidth(lngColName, Convert.ToInt32(lngWid * 0.4));
				vsSearch.ColWidth(lngcolLocationNumber, Convert.ToInt32(lngWid * 0.06));
				vsSearch.ColWidth(lngColLocation, Convert.ToInt32(lngWid * 0.36));
				vsSearch.ColWidth(lngColBookPage, 0);
				vsSearch.ColWidth(lngColMapLot, 0);
			}

			vsSearch.ColAlignment(lngColAcct, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSearch.ColAlignment(lngColName, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSearch.ColAlignment(lngcolLocationNumber, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSearch.ColAlignment(lngColLocation, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSearch.ColAlignment(lngColBookPage, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSearch.ColAlignment(lngColMapLot, FCGrid.AlignmentSettings.flexAlignLeftCenter);

			vsSearch.TextMatrix(0, lngColAcct, "Account");
			vsSearch.TextMatrix(0, lngColName, "Name");
			vsSearch.TextMatrix(0, lngcolLocationNumber, "#");
			vsSearch.TextMatrix(0, lngColLocation, "Street");
			vsSearch.TextMatrix(0, lngColBookPage, "Book/Page");
			vsSearch.TextMatrix(0, lngColMapLot, "Map/Lot");

			lblSearchListInstruction.Text = "Select an account by double clicking or pressing 'Enter' while the account is highlighted.";
			lblSearchListInstruction.Top = 0;
			lblSearchListInstruction.Width = lngWid;
			lblSearchListInstruction.Left = vsSearch.Left;
			lblSearchListInstruction.Visible = true;
		}

		private void ShowEditAccount_2(int lngShowAccount) { ShowEditAccount(ref lngShowAccount); }
		private void ShowEditAccount(ref int lngShowAccount)
		{
			try
			{   // On Error GoTo ERROR_HANDLER
				// this routine will show the years that this account has been billed
				// and let the user edit the name and address information
				clsDRWrapper rsAcct = new clsDRWrapper();

				vsYearInfo.Rows = 1;

				mnuFileSave.Visible = true;
				mnuProcessGetAccount.Text = "&Save and Exit";
				mnuProcessClearSearch.Visible = false;
				FormatGrids();

				if (modStatusPayments.boolRE)
				{
					rsAcct.OpenRecordset("SELECT BillingMaster.ID, BillingYear, Name1, Name2 FROM BillingMaster INNER JOIN " + modCollections.strDbRE + "Master ON BillingMaster.Account = Master.RSAccount WHERE RSCard = 1 AND BillingMaster.Account = " + Convert.ToString(lngShowAccount) + " AND BillingType = 'RE' ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
				}
				else
				{
					rsAcct.OpenRecordset("SELECT BillingMaster.ID, BillingYear, Name1, Name2 FROM BillingMaster INNER JOIN " + modCollections.strDbPP + "PPMaster ON BillingMaster.Account = PPMaster.Account WHERE BillingMaster.Account = " + Convert.ToString(lngShowAccount) + " AND BillingType = 'PP' ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
				}
				if (!rsAcct.EndOfFile())
				{

					while (!rsAcct.EndOfFile())
					{
						vsYearInfo.AddItem("");
						vsYearInfo.TextMatrix(vsYearInfo.Rows - 1, lngColEDBillkey, Convert.ToString(rsAcct.Get_Fields_Int32("ID")));
						vsYearInfo.TextMatrix(vsYearInfo.Rows - 1, lngColEDYear, modExtraModules.FormatYear(FCConvert.ToString(rsAcct.Get_Fields_Int32("BillingYear"))));
						vsYearInfo.TextMatrix(vsYearInfo.Rows - 1, lngColEDName1, Convert.ToString(rsAcct.Get_Fields_String("Name1")));
						vsYearInfo.TextMatrix(vsYearInfo.Rows - 1, lngColEDName2, Convert.ToString(rsAcct.Get_Fields_String("Name2")));

						rsAcct.MoveNext();
					}

					vsYearInfo.Select(1, 0, 1, vsYearInfo.Cols - 1);
					EditBillInformation(Convert.ToInt32(vsYearInfo.TextMatrix(vsYearInfo.Row, lngColEDBillkey)));
					fraEditInfo.Visible = true;
					fraSearch.Visible = false;
					vsSearch.Visible = false;


					fraEditInfo.Text = "Edit Account " + Convert.ToString(lngShowAccount) + " Information";
				}
				else
				{
					FCMessageBox.Show("Cannot load account #" + Convert.ToString(lngShowAccount) + ".", MsgBoxStyle.Exclamation, "Missing Account");
					return;
				}

				AdjustGridHeight();
				return;
			}
			catch(Exception ex)
			{   // ERROR_HANDLER:
				FCMessageBox.Show("Error #" + Convert.ToString(Information.Err(ex).Number) + " - ", MsgBoxStyle.Critical, "Error Showing Account");
			}
		}

		private void FormatGrids()
		{
			// this will format the two grids vsYearInfo and vsEditinfo
			int wid = 0;


			wid = vsYearInfo.Width;
			vsYearInfo.ExtendLastCol = true;
			vsYearInfo.ColWidth(lngColEDBillkey, 0);
			vsYearInfo.ColWidth(lngColEDYear, Convert.ToInt32(wid * 0.15));
			vsYearInfo.ColWidth(lngColEDName1, Convert.ToInt32(wid * 0.4));
			vsYearInfo.ColWidth(lngColEDName2, Convert.ToInt32(wid * 0.4));

			vsYearInfo.TextMatrix(0, lngColEDYear, "Year");
			vsYearInfo.TextMatrix(0, lngColEDName1, "Owner");
			vsYearInfo.TextMatrix(0, lngColEDName2, "Second Owner");



			vsEditInfo.Cols = 2;
			vsEditInfo.Rows = 12;
			vsEditInfo.ColWidth(lngColEDBillkey, Convert.ToInt32(wid * 0.3));
			vsEditInfo.ColWidth(lngColEDYear, Convert.ToInt32(wid * 0.65));
			vsEditInfo.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);

			vsEditInfo.TextMatrix(lngRowOwner, 0, "Owner");
			vsEditInfo.TextMatrix(lngRowSecondOwner, 0, "Second Owner");
			vsEditInfo.TextMatrix(lngRowAddress1, 0, "Address 1");
			vsEditInfo.TextMatrix(lngRowAddress2, 0, "Address 2");
			vsEditInfo.TextMatrix(lngRowAddress3, 0, "City St Zip");
			vsEditInfo.TextMatrix(lngRowMailingAddress3, 0, "Address 3");
			vsEditInfo.TextMatrix(lngRowMapLot, 0, "Map Lot");
			vsEditInfo.TextMatrix(lngRowBookPage, 0, "Book Page");
			vsEditInfo.TextMatrix(lngRowStreetNumber, 0, "Street Number");
			vsEditInfo.TextMatrix(lngRowStreetName, 0, "Street Name");
			vsEditInfo.TextMatrix(lngRowRef1, 0, "Ref 1");
			vsEditInfo.TextMatrix(lngRowUseRef1OnLien, 0, "Use Ref 1 on Liens");

		}

		// VBto upgrade warning: lngBK As int	OnWrite(string)
		private void EditBillInformation(int lngBK)
		{
			try
			{   // On Error GoTo ERROR_HANDLER
				// this will show the information for the bill that has been selected
				clsDRWrapper rsAcct = new clsDRWrapper();


				rsAcct.OpenRecordset("SELECT * FROM BillingMaster INNER JOIN " + modCollections.strDbRE + "Master ON BillingMaster.Account = Master.RSAccount WHERE RSCard = 1 AND BillingMaster.ID = " + Convert.ToString(lngBK) + " ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
				if (!rsAcct.EndOfFile())
				{
					vsEditInfo.TextMatrix(lngRowOwner, 1, Convert.ToString(rsAcct.Get_Fields_String("Name1")));
					vsEditInfo.TextMatrix(lngRowSecondOwner, 1, Convert.ToString(rsAcct.Get_Fields_String("Name2")));
					vsEditInfo.TextMatrix(lngRowAddress1, 1, Convert.ToString(rsAcct.Get_Fields_String("Address1")));
					vsEditInfo.TextMatrix(lngRowAddress2, 1, Convert.ToString(rsAcct.Get_Fields_String("Address2")));
					vsEditInfo.TextMatrix(lngRowAddress3, 1, Convert.ToString(rsAcct.Get_Fields_String("Address3")));
					vsEditInfo.TextMatrix(lngRowMailingAddress3, 1, Convert.ToString(rsAcct.Get_Fields_String("MailingAddress3")));
					vsEditInfo.TextMatrix(lngRowMapLot, 1, Convert.ToString(rsAcct.Get_Fields_String("MapLot")));
					vsEditInfo.TextMatrix(lngRowBookPage, 1, Convert.ToString(rsAcct.Get_Fields_String("BookPage")));
					// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
					vsEditInfo.TextMatrix(lngRowStreetNumber, 1, Convert.ToString(rsAcct.Get_Fields("StreetNumber")));
					vsEditInfo.TextMatrix(lngRowStreetName, 1, Convert.ToString(rsAcct.Get_Fields_String("StreetName")));
					vsEditInfo.TextMatrix(lngRowRef1, 1, Convert.ToString(rsAcct.Get_Fields_String("Ref1")));
					if (Convert.ToBoolean(rsAcct.Get_Fields_Boolean("ShowRef1")))
					{
						vsEditInfo.TextMatrix(lngRowUseRef1OnLien, 1, "Yes");
					}
					else
					{
						vsEditInfo.TextMatrix(lngRowUseRef1OnLien, 1, "No");
					}
				}
				else
				{
					vsEditInfo.TextMatrix(lngRowOwner, 1, "");
					vsEditInfo.TextMatrix(lngRowSecondOwner, 1, "");
					vsEditInfo.TextMatrix(lngRowAddress1, 1, "");
					vsEditInfo.TextMatrix(lngRowAddress2, 1, "");
					vsEditInfo.TextMatrix(lngRowMailingAddress3, 1, "");
					vsEditInfo.TextMatrix(lngRowAddress3, 1, "");
					vsEditInfo.TextMatrix(lngRowMapLot, 1, "");
					vsEditInfo.TextMatrix(lngRowBookPage, 1, "");
					vsEditInfo.TextMatrix(lngRowStreetNumber, 1, "");
					vsEditInfo.TextMatrix(lngRowStreetName, 1, "");
					vsEditInfo.TextMatrix(lngRowRef1, 1, "");
					vsEditInfo.TextMatrix(lngRowUseRef1OnLien, 1, "");
				}
				boolDirty = false;

				return;
			}
			catch(Exception ex)
			{   // ERROR_HANDLER:
				FCMessageBox.Show("Error #" + Convert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MsgBoxStyle.Critical, "Error Editing Bill Information");
			}
		}

		private void vsYearInfo_GotFocus()
		{

			vsYearInfo.Row = 1;
			vsYearInfo.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;

		}

		private void vsYearInfo_RowColChange(object sender, EventArgs e)
		{

			if (vsYearInfo.Row > 0 && lngLastRow != vsYearInfo.Row)
			{
				if (boolDirty)
				{

					if (FCMessageBox.Show("Changes have been made.  Would you like to save the information?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Save Changes") == DialogResult.Yes)
					{
						SaveInformation();
						EditBillInformation(Convert.ToInt32(vsYearInfo.TextMatrix(vsYearInfo.Row, lngColEDBillkey)));
					}
					else if (FCMessageBox.Show("Changes have been made.  Would you like to save the information?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Save Changes") == DialogResult.No)
					{
						EditBillInformation(Convert.ToInt32(vsYearInfo.TextMatrix(vsYearInfo.Row, lngColEDBillkey)));
					}
					else if (FCMessageBox.Show("Changes have been made.  Would you like to save the information?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Save Changes") == DialogResult.Cancel)
					{
						// do nothing
					}
				}
				else
				{
					EditBillInformation(Convert.ToInt32(vsYearInfo.TextMatrix(vsYearInfo.Row, lngColEDBillkey)));
				}
				lngLastRow = vsYearInfo.Row;
			}

			if (vsYearInfo.Row == vsYearInfo.Rows - 1)
			{
				vsYearInfo.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				vsYearInfo.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}

		}

		private void SaveInformation()
		{
			try
			{   // On Error GoTo ERROR_HANDLER
				string strName1 = "";
				string strName2 = "";
				string strAddr1 = "";
				string strAddr2 = "";
				string strAddr3 = "";
				string strMailingAddr3 = "";
				string strMapLot = "";
				string strBookPage = "";
				int lngStreetNumber = 0;
				string strStreetName = "";
				string strRef1 = "";
				bool boolUseRef1 = false;
				clsDRWrapper rsAcct = new clsDRWrapper();

				vsEditInfo.Select(0, 0);

				// use lngLastRow to get the from from the grid, this is the last row selected and is the way that I will find the billkey
				if (lngLastRow > 0)
				{
					rsAcct.OpenRecordset("SELECT * FROM BillingMaster WHERE ID = " + vsYearInfo.TextMatrix(lngLastRow, lngColEDBillkey), modExtraModules.strCLDatabase);
					if (!rsAcct.EndOfFile())
					{
						// this will save the information into the correct bill
						strName1 = Strings.Trim(vsEditInfo.TextMatrix(lngRowOwner, 1));
						strName2 = Strings.Trim(vsEditInfo.TextMatrix(lngRowSecondOwner, 1));
						strAddr1 = Strings.Trim(vsEditInfo.TextMatrix(lngRowAddress1, 1));
						strAddr2 = Strings.Trim(vsEditInfo.TextMatrix(lngRowAddress2, 1));
						strAddr3 = Strings.Trim(vsEditInfo.TextMatrix(lngRowAddress3, 1));
						strMailingAddr3 = Strings.Trim(vsEditInfo.TextMatrix(lngRowMailingAddress3, 1));
						strMapLot = Strings.Trim(vsEditInfo.TextMatrix(lngRowMapLot, 1));
						strBookPage = Strings.Trim(vsEditInfo.TextMatrix(lngRowBookPage, 1));
						lngStreetNumber = (int)Math.Round(Conversion.Val("0" + Strings.Trim(vsEditInfo.TextMatrix(lngRowStreetNumber, 1))));
						strStreetName = Strings.Trim(vsEditInfo.TextMatrix(lngRowStreetName, 1));
						strRef1 = Strings.Trim(vsEditInfo.TextMatrix(lngRowRef1, 1));
						boolUseRef1 = FCConvert.CBool(Strings.Left(vsEditInfo.TextMatrix(lngRowUseRef1OnLien, 1), 1) == "Y");

						rsAcct.Edit();
						rsAcct.Set_Fields("Name1", strName1);
						rsAcct.Set_Fields("Name2", strName2);
						rsAcct.Set_Fields("Address1", strAddr1);
						rsAcct.Set_Fields("Address2", strAddr2);
						rsAcct.Set_Fields("Address3", strAddr3);
						rsAcct.Set_Fields("MailingAddress3", strMailingAddr3);
						rsAcct.Set_Fields("MapLot", strMapLot);
						rsAcct.Set_Fields("BookPage", strBookPage);
						rsAcct.Set_Fields("StreetNumber", lngStreetNumber);
						rsAcct.Set_Fields("StreetName", strStreetName);
						rsAcct.Set_Fields("Ref1", strRef1);
						rsAcct.Set_Fields("ShowRef1", boolUseRef1);
						rsAcct.Update();

						vsYearInfo.TextMatrix(lngLastRow, lngColEDName1, strName1);
						vsYearInfo.TextMatrix(lngLastRow, lngColEDName2, strName2);

						modGlobalFunctions.AddCYAEntry_26("CL", "Editing Bill information.", "Billkey = " + vsYearInfo.TextMatrix(lngLastRow, lngColEDBillkey));
					}
					boolDirty = false;
				}
				FCMessageBox.Show("Save Successful.", MsgBoxStyle.Exclamation, "Success");
				return;
			}
			catch(Exception ex)
			{   // ERROR_HANDLER:
				FCMessageBox.Show("Error #" + Convert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MsgBoxStyle.Critical, "Error Saving Bill");
			}
		}

		private void CopyInformation(int lngRow = -1)
		{
			// this will copy the information into the virtual clipboard from the grid
			// Select Case lngRow
			// Case -1
			strCopyName1 = Strings.Trim(vsEditInfo.TextMatrix(lngRowOwner, 1));
			strCopyName2 = Strings.Trim(vsEditInfo.TextMatrix(lngRowSecondOwner, 1));
			strCopyAddr1 = Strings.Trim(vsEditInfo.TextMatrix(lngRowAddress1, 1));
			strCopyAddr2 = Strings.Trim(vsEditInfo.TextMatrix(lngRowAddress2, 1));
			strCopyAddr3 = Strings.Trim(vsEditInfo.TextMatrix(lngRowAddress3, 1));
			strCopyMailingAddr3 = Strings.Trim(vsEditInfo.TextMatrix(lngRowMailingAddress3, 1));
			strCopyMapLot = Strings.Trim(vsEditInfo.TextMatrix(lngRowMapLot, 1));
			strCopyBookPage = Strings.Trim(vsEditInfo.TextMatrix(lngRowBookPage, 1));
			strCopyStreetNumber = Convert.ToString(Conversion.Val("0" + Strings.Trim(vsEditInfo.TextMatrix(lngRowStreetNumber, 1))));
			strCopyStreetName = Strings.Trim(vsEditInfo.TextMatrix(lngRowStreetName, 1));
			strCopyRef1 = Strings.Trim(vsEditInfo.TextMatrix(lngRowRef1, 1));
			boolCopyUseRef1 = FCConvert.CBool(Strings.Left(vsEditInfo.TextMatrix(lngRowUseRef1OnLien, 1), 1) == "Y");
			// Case 0
			// strCopyName1 = Trim(vsEditInfo.EditSelText)
			// Case 1
			// strCopyName2 = Trim(vsEditInfo.EditSelText)
			// Case 2
			// strCopyAddr1 = Trim(vsEditInfo.EditSelText)
			// Case 3
			// strCopyAddr2 = Trim(vsEditInfo.EditSelText)
			// Case 4
			// strCopyAddr3 = Trim(vsEditInfo.EditSelText)
			// End Select
		}

		private void PasteInformation()
		{
			// this will paste the information into the grid from the virtual clipboard
			if (strCopyName1 != "" && (strCopyName2 != "" || strCopyAddr1 != "" || strCopyAddr2 != "" || strCopyAddr3 != ""))
			{
				vsEditInfo.TextMatrix(lngRowOwner, 1, strCopyName1);
				vsEditInfo.TextMatrix(lngRowSecondOwner, 1, strCopyName2);
				vsEditInfo.TextMatrix(lngRowAddress1, 1, strCopyAddr1);
				vsEditInfo.TextMatrix(lngRowAddress2, 1, strCopyAddr2);
				vsEditInfo.TextMatrix(lngRowAddress3, 1, strCopyAddr3);
				vsEditInfo.TextMatrix(lngRowMailingAddress3, 1, strCopyMailingAddr3);
				vsEditInfo.TextMatrix(lngRowMapLot, 1, strCopyMapLot);
				vsEditInfo.TextMatrix(lngRowBookPage, 1, strCopyBookPage);
				vsEditInfo.TextMatrix(lngRowStreetNumber, 1, strCopyStreetNumber);
				vsEditInfo.TextMatrix(lngRowStreetName, 1, strCopyStreetName);
				vsEditInfo.TextMatrix(lngRowRef1, 1, strCopyRef1);
				if (boolCopyUseRef1)
				{
					vsEditInfo.TextMatrix(lngRowUseRef1OnLien, 1, "Yes");
				}
				else
				{
					vsEditInfo.TextMatrix(lngRowUseRef1OnLien, 1, "No");
				}
			}
		}

		private void AdjustGridHeight()
		{

			vsEditInfo.Height = Convert.ToInt32((vsEditInfo.Rows * vsEditInfo.RowHeight(0)) + (vsEditInfo.RowHeight(0) * 0.15));



			vsYearInfo.Height = Convert.ToInt32((vsYearInfo.Rows * vsYearInfo.RowHeight(0)) + (vsYearInfo.RowHeight(0) * 0.15));

		}

	}
}
