﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmCLEditBillInfo.
	/// </summary>
	partial class frmCLEditBillInfo : BaseForm
	{
		public fecherFoundation.FCComboBox cmbRE;
		public fecherFoundation.FCComboBox cmbSearchType;
		public fecherFoundation.FCFrame fraEditInfo;
		public fecherFoundation.FCGrid vsEditInfo;
		public fecherFoundation.FCGrid vsYearInfo;
		public fecherFoundation.FCGrid vsSearch;
		public fecherFoundation.FCPanel fraSearch;
		public fecherFoundation.FCFrame fraGetAccount;
		public fecherFoundation.FCButton cmdGetAccountNumber;
		public fecherFoundation.FCTextBox txtGetAccountNumber;
		public fecherFoundation.FCLabel lblInstructions1;
		public fecherFoundation.FCFrame fraSearchCriteria;
		public fecherFoundation.FCTextBox txtSearch;
		public fecherFoundation.FCButton cmdSearch;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCLabel lblLastAccount;
		public fecherFoundation.FCTextBox txtHold;
		public fecherFoundation.FCLabel lblSearchListInstruction;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCLEditBillInfo));
			this.cmbRE = new fecherFoundation.FCComboBox();
			this.cmbSearchType = new fecherFoundation.FCComboBox();
			this.fraEditInfo = new fecherFoundation.FCFrame();
			this.vsYearInfo = new fecherFoundation.FCGrid();
			this.vsEditInfo = new fecherFoundation.FCGrid();
			this.fraSearch = new fecherFoundation.FCPanel();
			this.cmdSearch = new fecherFoundation.FCButton();
			this.cmdClear = new fecherFoundation.FCButton();
			this.fraSearchCriteria = new fecherFoundation.FCFrame();
			this.fcLabel1 = new fecherFoundation.FCLabel();
			this.fcLabel3 = new fecherFoundation.FCLabel();
			this.txtSearch = new fecherFoundation.FCTextBox();
			this.lblSearchListInstruction = new fecherFoundation.FCLabel();
			this.fcLabel2 = new fecherFoundation.FCLabel();
			this.lblLastAccount = new fecherFoundation.FCLabel();
			this.fraGetAccount = new fecherFoundation.FCFrame();
			this.cmdGetAccountNumber = new fecherFoundation.FCButton();
			this.txtHold = new fecherFoundation.FCTextBox();
			this.txtGetAccountNumber = new fecherFoundation.FCTextBox();
			this.lblInstructions1 = new fecherFoundation.FCLabel();
			this.vsSearch = new fecherFoundation.FCGrid();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.btnProcess = new fecherFoundation.FCButton();
			this.cmdProcessSearch = new fecherFoundation.FCButton();
			this.cmdProcessClearSearch = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraEditInfo)).BeginInit();
			this.fraEditInfo.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsYearInfo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsEditInfo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSearch)).BeginInit();
			this.fraSearch.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSearchCriteria)).BeginInit();
			this.fraSearchCriteria.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraGetAccount)).BeginInit();
			this.fraGetAccount.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdGetAccountNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessClearSearch)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 589);
			this.BottomPanel.Size = new System.Drawing.Size(600, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraEditInfo);
			this.ClientArea.Controls.Add(this.fraSearch);
			this.ClientArea.Controls.Add(this.vsSearch);
			this.ClientArea.Location = new System.Drawing.Point(0, 75);
			this.ClientArea.Size = new System.Drawing.Size(620, 593);
			this.ClientArea.Controls.SetChildIndex(this.vsSearch, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraSearch, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraEditInfo, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdProcessClearSearch);
			this.TopPanel.Controls.Add(this.cmdProcessSearch);
			this.TopPanel.Size = new System.Drawing.Size(620, 75);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdProcessSearch, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdProcessClearSearch, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(225, 30);
			this.HeaderText.Text = "Edit Bill Information";
			// 
			// cmbRE
			// 
			this.cmbRE.Items.AddRange(new object[] {
            "Real Estate",
            "Personal Property"});
			this.cmbRE.Location = new System.Drawing.Point(261, 41);
			this.cmbRE.Name = "cmbRE";
			this.cmbRE.Size = new System.Drawing.Size(121, 40);
			this.cmbRE.TabIndex = 15;
			this.cmbRE.Text = "Real Estate";
			this.cmbRE.Visible = false;
			this.cmbRE.Click += new System.EventHandler(this.optRE_Click);
			// 
			// cmbSearchType
			// 
			this.cmbSearchType.Items.AddRange(new object[] {
            "Name",
            "Street Name",
            "Map / Lot"});
			this.cmbSearchType.Location = new System.Drawing.Point(125, 30);
			this.cmbSearchType.Name = "cmbSearchType";
			this.cmbSearchType.Size = new System.Drawing.Size(162, 40);
			this.cmbSearchType.TabIndex = 16;
			this.cmbSearchType.Text = "Name";
			// 
			// fraEditInfo
			// 
			this.fraEditInfo.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.fraEditInfo.AppearanceKey = "groupBoxNoBorders";
			this.fraEditInfo.Controls.Add(this.vsYearInfo);
			this.fraEditInfo.Controls.Add(this.vsEditInfo);
			this.fraEditInfo.Location = new System.Drawing.Point(30, 17);
			this.fraEditInfo.Name = "fraEditInfo";
			this.fraEditInfo.Size = new System.Drawing.Size(587, 572);
			this.fraEditInfo.TabIndex = 19;
			this.fraEditInfo.Text = "Edit Account Information";
			this.fraEditInfo.UseMnemonic = false;
			this.fraEditInfo.Visible = false;
			// 
			// vsYearInfo
			// 
			this.vsYearInfo.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsYearInfo.Cols = 4;
			this.vsYearInfo.Location = new System.Drawing.Point(0, 30);
			this.vsYearInfo.Name = "vsYearInfo";
			this.vsYearInfo.Rows = 1;
			this.vsYearInfo.Size = new System.Drawing.Size(558, 193);
			this.vsYearInfo.TabIndex = 20;
			this.vsYearInfo.CurrentCellChanged += new System.EventHandler(this.vsYearInfo_RowColChange);
			// 
			// vsEditInfo
			// 
			this.vsEditInfo.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsEditInfo.ColumnHeadersVisible = false;
			this.vsEditInfo.ExtendLastCol = true;
			this.vsEditInfo.FixedRows = 0;
			this.vsEditInfo.Location = new System.Drawing.Point(0, 265);
			this.vsEditInfo.Name = "vsEditInfo";
			this.vsEditInfo.Rows = 11;
			this.vsEditInfo.Size = new System.Drawing.Size(558, 287);
			this.vsEditInfo.TabIndex = 21;
			this.vsEditInfo.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsEditInfo_AfterEdit);
			this.vsEditInfo.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.VsEditInfo_CellFormatting);
			this.vsEditInfo.CurrentCellChanged += new System.EventHandler(this.vsEditInfo_RowColChange);
			this.vsEditInfo.GotFocus += new System.EventHandler(this.vsEditInfo_GotFocus);
			// 
			// fraSearch
			// 
			this.fraSearch.Controls.Add(this.cmdSearch);
			this.fraSearch.Controls.Add(this.cmdClear);
			this.fraSearch.Controls.Add(this.fraSearchCriteria);
			this.fraSearch.Controls.Add(this.cmbRE);
			this.fraSearch.Controls.Add(this.lblLastAccount);
			this.fraSearch.Controls.Add(this.fraGetAccount);
			this.fraSearch.Name = "fraSearch";
			this.fraSearch.Size = new System.Drawing.Size(567, 377);
			this.fraSearch.TabIndex = 4;
			this.fraSearch.Visible = false;
			// 
			// cmdSearch
			// 
			this.cmdSearch.ImageSource = "button-search";
			this.cmdSearch.Location = new System.Drawing.Point(444, 305);
			this.cmdSearch.Name = "cmdSearch";
			this.cmdSearch.Size = new System.Drawing.Size(81, 28);
			this.cmdSearch.TabIndex = 17;
			this.cmdSearch.Text = "Search";
			this.cmdSearch.Visible = false;
			this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
			// 
			// cmdClear
			// 
			this.cmdClear.Location = new System.Drawing.Point(332, 305);
			this.cmdClear.Name = "cmdClear";
			this.cmdClear.Size = new System.Drawing.Size(101, 28);
			this.cmdClear.TabIndex = 16;
			this.cmdClear.Text = "Clear Search";
			this.cmdClear.Visible = false;
			this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
			// 
			// fraSearchCriteria
			// 
			this.fraSearchCriteria.Controls.Add(this.fcLabel1);
			this.fraSearchCriteria.Controls.Add(this.fcLabel3);
			this.fraSearchCriteria.Controls.Add(this.txtSearch);
			this.fraSearchCriteria.Controls.Add(this.lblSearchListInstruction);
			this.fraSearchCriteria.Controls.Add(this.cmbSearchType);
			this.fraSearchCriteria.Controls.Add(this.fcLabel2);
			this.fraSearchCriteria.Location = new System.Drawing.Point(30, 167);
			this.fraSearchCriteria.Name = "fraSearchCriteria";
			this.fraSearchCriteria.Size = new System.Drawing.Size(520, 142);
			this.fraSearchCriteria.TabIndex = 5;
			this.fraSearchCriteria.Text = "Search";
			this.fraSearchCriteria.UseMnemonic = false;
			// 
			// fcLabel1
			// 
			this.fcLabel1.AutoSize = true;
			this.fcLabel1.Location = new System.Drawing.Point(20, 99);
			this.fcLabel1.Name = "fcLabel1";
			this.fcLabel1.Size = new System.Drawing.Size(96, 16);
			this.fcLabel1.TabIndex = 24;
			this.fcLabel1.Text = "SEARCH FOR";
			// 
			// fcLabel3
			// 
			this.fcLabel3.AutoSize = true;
			this.fcLabel3.Location = new System.Drawing.Point(20, 44);
			this.fcLabel3.Name = "fcLabel3";
			this.fcLabel3.Size = new System.Drawing.Size(85, 16);
			this.fcLabel3.TabIndex = 23;
			this.fcLabel3.Text = "SEARCH BY";
			// 
			// txtSearch
			// 
			this.txtSearch.BackColor = System.Drawing.SystemColors.Window;
			this.txtSearch.Location = new System.Drawing.Point(125, 85);
			this.txtSearch.Name = "txtSearch";
			this.txtSearch.Size = new System.Drawing.Size(162, 40);
			this.txtSearch.TabIndex = 18;
			this.txtSearch.KeyDown += new Wisej.Web.KeyEventHandler(this.txtSearch_KeyDown);
			// 
			// lblSearchListInstruction
			// 
			this.lblSearchListInstruction.Location = new System.Drawing.Point(85, -11);
			this.lblSearchListInstruction.Name = "lblSearchListInstruction";
			this.lblSearchListInstruction.Size = new System.Drawing.Size(236, 22);
			this.lblSearchListInstruction.TabIndex = 22;
			this.lblSearchListInstruction.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.lblSearchListInstruction.Visible = false;
			// 
			// fcLabel2
			// 
			this.fcLabel2.AutoSize = true;
			this.fcLabel2.Location = new System.Drawing.Point(-14, 235);
			this.fcLabel2.Name = "fcLabel2";
			this.fcLabel2.Size = new System.Drawing.Size(85, 16);
			this.fcLabel2.TabIndex = 22;
			this.fcLabel2.Text = "SEARCH BY";
			// 
			// lblLastAccount
			// 
			this.lblLastAccount.AutoSize = true;
			this.lblLastAccount.Location = new System.Drawing.Point(330, 11);
			this.lblLastAccount.Name = "lblLastAccount";
			this.lblLastAccount.Size = new System.Drawing.Size(4, 15);
			this.lblLastAccount.TabIndex = 7;
			// 
			// fraGetAccount
			// 
			this.fraGetAccount.Controls.Add(this.cmdGetAccountNumber);
			this.fraGetAccount.Controls.Add(this.txtHold);
			this.fraGetAccount.Controls.Add(this.txtGetAccountNumber);
			this.fraGetAccount.Controls.Add(this.lblInstructions1);
			this.fraGetAccount.Location = new System.Drawing.Point(30, 25);
			this.fraGetAccount.Name = "fraGetAccount";
			this.fraGetAccount.Size = new System.Drawing.Size(520, 117);
			this.fraGetAccount.TabIndex = 9;
			this.fraGetAccount.Text = "Last Account Accessed";
			this.fraGetAccount.UseMnemonic = false;
			// 
			// cmdGetAccountNumber
			// 
			this.cmdGetAccountNumber.Location = new System.Drawing.Point(307, 105);
			this.cmdGetAccountNumber.Name = "cmdGetAccountNumber";
			this.cmdGetAccountNumber.Size = new System.Drawing.Size(92, 28);
			this.cmdGetAccountNumber.TabIndex = 14;
			this.cmdGetAccountNumber.Text = "Get Account";
			this.cmdGetAccountNumber.Visible = false;
			this.cmdGetAccountNumber.Click += new System.EventHandler(this.cmdGetAccountNumber_Click);
			// 
			// txtHold
			// 
			this.txtHold.BackColor = System.Drawing.SystemColors.Window;
			this.txtHold.Location = new System.Drawing.Point(85, 93);
			this.txtHold.Name = "txtHold";
			this.txtHold.Size = new System.Drawing.Size(132, 40);
			this.txtHold.TabIndex = 3;
			this.txtHold.Visible = false;
			// 
			// txtGetAccountNumber
			// 
			this.txtGetAccountNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtGetAccountNumber.Location = new System.Drawing.Point(314, 30);
			this.txtGetAccountNumber.Name = "txtGetAccountNumber";
			this.txtGetAccountNumber.Size = new System.Drawing.Size(175, 40);
			this.txtGetAccountNumber.TabIndex = 10;
			this.txtGetAccountNumber.KeyDown += new Wisej.Web.KeyEventHandler(this.txtGetAccountNumber_KeyDown);
			this.txtGetAccountNumber.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtGetAccountNumber_KeyPress);
			// 
			// lblInstructions1
			// 
			this.lblInstructions1.Location = new System.Drawing.Point(20, 44);
			this.lblInstructions1.Name = "lblInstructions1";
			this.lblInstructions1.Size = new System.Drawing.Size(272, 40);
			this.lblInstructions1.TabIndex = 13;
			this.lblInstructions1.Text = "PLEASE ENTER THE ACCOUNT NUMBER OR HIT ENTER TO VIEW THE ACCOUNT SHOWN";
			// 
			// vsSearch
			// 
			this.vsSearch.AllowUserToResizeColumns = true;
			this.vsSearch.Cols = 5;
			this.vsSearch.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vsSearch.FixedCols = 0;
			this.vsSearch.Location = new System.Drawing.Point(400, 3);
			this.vsSearch.Name = "vsSearch";
			this.vsSearch.RowHeadersVisible = false;
			this.vsSearch.Rows = 1;
			this.vsSearch.Size = new System.Drawing.Size(158, 52);
			this.vsSearch.TabIndex = 8;
			this.vsSearch.Visible = false;
			this.vsSearch.CurrentCellChanged += new System.EventHandler(this.vsSearch_RowColChange);
			this.vsSearch.Click += new System.EventHandler(this.vsSearch_Click);
			this.vsSearch.DoubleClick += new System.EventHandler(this.vsSearch_DblClick);
			this.vsSearch.KeyDown += new Wisej.Web.KeyEventHandler(this.vsSearch_KeyDown);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(250, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(120, 48);
			this.btnProcess.TabIndex = 1;
			this.btnProcess.Text = "Process";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// cmdProcessSearch
			// 
			this.cmdProcessSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdProcessSearch.ImageSource = "button-search";
			this.cmdProcessSearch.Location = new System.Drawing.Point(457, 29);
			this.cmdProcessSearch.Name = "cmdProcessSearch";
			this.cmdProcessSearch.Size = new System.Drawing.Size(81, 24);
			this.cmdProcessSearch.TabIndex = 52;
			this.cmdProcessSearch.Text = "Search";
			this.cmdProcessSearch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.cmdProcessSearch.Click += new System.EventHandler(this.fcButton1_Click);
			// 
			// cmdProcessClearSearch
			// 
			this.cmdProcessClearSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdProcessClearSearch.Location = new System.Drawing.Point(359, 29);
			this.cmdProcessClearSearch.Name = "cmdProcessClearSearch";
			this.cmdProcessClearSearch.Size = new System.Drawing.Size(93, 24);
			this.cmdProcessClearSearch.TabIndex = 53;
			this.cmdProcessClearSearch.Text = "Clear Search";
			this.cmdProcessClearSearch.Click += new System.EventHandler(this.cmdProcessClearSearch_Click);
			// 
			// frmCLEditBillInfo
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(620, 668);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmCLEditBillInfo";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Edit Bill Information";
			this.Load += new System.EventHandler(this.frmCLEditBillInfo_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCLEditBillInfo_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCLEditBillInfo_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraEditInfo)).EndInit();
			this.fraEditInfo.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsYearInfo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsEditInfo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSearch)).EndInit();
			this.fraSearch.ResumeLayout(false);
			this.fraSearch.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSearchCriteria)).EndInit();
			this.fraSearchCriteria.ResumeLayout(false);
			this.fraSearchCriteria.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraGetAccount)).EndInit();
			this.fraGetAccount.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdGetAccountNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessClearSearch)).EndInit();
			this.ResumeLayout(false);

		}


        #endregion

        private System.ComponentModel.IContainer components;
		private FCLabel fcLabel2;
		private FCLabel fcLabel3;
		private FCButton btnProcess;
		public FCButton cmdProcessSearch;
		public FCButton cmdProcessClearSearch;
		private FCLabel fcLabel1;
	}
}
