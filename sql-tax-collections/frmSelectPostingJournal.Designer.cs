﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for frmSelectPostingJournal.
	/// </summary>
	partial class frmSelectPostingJournal : fecherFoundation.FCForm
	{
		public fecherFoundation.FCComboBox cboJournals;
		public fecherFoundation.FCButton CancelButton;
		public fecherFoundation.FCButton OKButton;
		public fecherFoundation.FCLabel lblJournals;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSelectPostingJournal));
			this.cboJournals = new fecherFoundation.FCComboBox();
			this.CancelButton = new fecherFoundation.FCButton();
			this.OKButton = new fecherFoundation.FCButton();
			this.lblJournals = new fecherFoundation.FCLabel();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			((System.ComponentModel.ISupportInitialize)(this.CancelButton)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.OKButton)).BeginInit();
			this.SuspendLayout();
			// 
			// cboJournals
			// 
			this.cboJournals.AutoSize = false;
			this.cboJournals.BackColor = System.Drawing.SystemColors.Window;
			this.cboJournals.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboJournals.FormattingEnabled = true;
			this.cboJournals.Location = new System.Drawing.Point(30, 66);
			this.cboJournals.Name = "cboJournals";
			this.cboJournals.Size = new System.Drawing.Size(332, 25);
			this.cboJournals.TabIndex = 1;
			this.cboJournals.SelectedIndexChanged += new System.EventHandler(this.cboJournals_SelectedIndexChanged);
			// 
			// CancelButton
			// 
			this.CancelButton.AppearanceKey = "actionButton";
			this.CancelButton.Location = new System.Drawing.Point(188, 111);
			this.CancelButton.Name = "CancelButton";
			this.CancelButton.Size = new System.Drawing.Size(82, 40);
			this.CancelButton.TabIndex = 3;
			this.CancelButton.Text = "Cancel";
			this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
			// 
			// OKButton
			// 
			this.OKButton.AppearanceKey = "actionButton";
			this.OKButton.Location = new System.Drawing.Point(85, 111);
			this.OKButton.Name = "OKButton";
			this.OKButton.Size = new System.Drawing.Size(82, 40);
			this.OKButton.TabIndex = 2;
			this.OKButton.Text = "OK";
			this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
			// 
			// lblJournals
			// 
			this.lblJournals.AutoSize = true;
			this.lblJournals.Location = new System.Drawing.Point(30, 30);
			this.lblJournals.Name = "lblJournals";
			this.lblJournals.Size = new System.Drawing.Size(352, 16);
			this.lblJournals.TabIndex = 0;
			this.lblJournals.Text = "PLEASE SELECT A JOURNAL AND CLICK THE OK BUTTON";
			this.lblJournals.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// frmSelectPostingJournal
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(380, 174);
			this.Controls.Add(this.cboJournals);
			this.Controls.Add(this.CancelButton);
			this.Controls.Add(this.OKButton);
			this.Controls.Add(this.lblJournals);
			this.FillColor = 16777215;
			this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmSelectPostingJournal";
			this.ShowInTaskbar = false;
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Journal Selection";
			this.Load += new System.EventHandler(this.frmSelectPostingJournal_Load);
			this.Activated += new System.EventHandler(this.frmSelectPostingJournal_Activated);
			((System.ComponentModel.ISupportInitialize)(this.CancelButton)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.OKButton)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
